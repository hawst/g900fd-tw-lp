.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;
.super Ljava/lang/Object;
.source "KnoxFolderAppInfoAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 219
    .local v0, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-boolean v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isMoreLayout:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-boolean v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    if-eqz v5, :cond_1

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Z

    move-result v5

    if-ne v5, v6, :cond_4

    .line 225
    iget v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    if-eq v5, v6, :cond_0

    .line 229
    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v5

    if-ne v5, v6, :cond_2

    .line 230
    invoke-virtual {v0, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIsSelected(I)V

    .line 233
    :goto_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 235
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v5}, Landroid/widget/GridView;->refreshDrawableState()V

    .line 236
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->notifyDataSetChanged()V

    .line 237
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v5}, Landroid/widget/GridView;->invalidateViews()V

    .line 238
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v2

    .line 239
    .local v2, "count":I
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setSpinnerItems(I)V

    .line 240
    if-nez v2, :cond_3

    .line 241
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->enableDoneBtn(Z)V

    goto :goto_0

    .line 232
    .end local v2    # "count":I
    :cond_2
    invoke-virtual {v0, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIsSelected(I)V

    goto :goto_1

    .line 243
    .restart local v2    # "count":I
    :cond_3
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->enableDoneBtn(Z)V

    goto :goto_0

    .line 249
    .end local v2    # "count":I
    :cond_4
    :try_start_0
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-object v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v6

    iget v6, v6, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v5, v6}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v5

    sget-object v6, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    invoke-virtual {v5, v6}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 251
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 252
    .local v4, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v5, "com.sec.knox.containeragent2"

    const-string v6, "com.sec.knox.containeragent2.ui.settings.TransparentActivity"

    invoke-direct {v1, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    .local v1, "component":Landroid/content/ComponentName;
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 254
    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    new-instance v6, Landroid/os/UserHandle;

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v7

    iget v7, v7, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-direct {v6, v7}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v5, v4, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 256
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mUnlockReq:Z

    .line 257
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    iput-object v6, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tPkgName:Ljava/lang/String;

    .line 258
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    iput-object v6, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->tActivityName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 267
    .end local v1    # "component":Landroid/content/ComponentName;
    .end local v4    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v3

    .line 268
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 260
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_5
    :try_start_1
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 261
    .restart local v4    # "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    invoke-direct {v1, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    .restart local v1    # "component":Landroid/content/ComponentName;
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 263
    const-string v5, "android.intent.action.MAIN"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    const/high16 v5, 0x10200000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 265
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    new-instance v6, Landroid/os/UserHandle;

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v7

    iget v7, v7, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-direct {v6, v7}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v5, v4, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
