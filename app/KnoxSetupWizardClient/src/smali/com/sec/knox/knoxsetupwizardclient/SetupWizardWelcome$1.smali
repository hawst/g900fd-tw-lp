.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;
.super Ljava/lang/Object;
.source "SetupWizardWelcome.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)I

    move-result v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$100()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 96
    :cond_0
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eula_result:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$200()I

    move-result v0

    if-nez v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;->startHideWelcomeAnimation()V

    .line 98
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;->startEulaAnimation()V

    .line 99
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->EULA_STAGE:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$400()I

    move-result v1

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I
    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$002(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;I)I

    .line 100
    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 101
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$500(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 113
    :goto_0
    return-void

    .line 106
    :cond_1
    const-string v0, "Already shown EULA. go to next step."

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setResult(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->finish()V

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-virtual {v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setResult(I)V

    goto :goto_0
.end method
