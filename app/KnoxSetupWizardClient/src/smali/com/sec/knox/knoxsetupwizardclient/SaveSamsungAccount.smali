.class public Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;
.super Ljava/lang/Object;
.source "SaveSamsungAccount.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    .line 19
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->signInOrCreateSA()V

    return-void
.end method

.method private signInOrCreateSA()V
    .locals 6

    .prologue
    .line 74
    const-string v0, "9g3975zg63"

    .line 75
    .local v0, "client_id":Ljava/lang/String;
    const-string v1, "99004120A4150A4555E45E878846957A"

    .line 77
    .local v1, "client_secret":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "packageName":Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.osp.app.signin.action.ADD_SAMSUNG_ACCOUNT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 80
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "client_id"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 81
    const-string v4, "client_secret"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    const-string v4, "mypackage"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string v4, "OSP_VER"

    const-string v5, "OSP_02"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string v4, "MODE"

    const-string v5, "ADD_ACCOUNT"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    const v5, 0x4dc80

    invoke-virtual {v4, v2, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 86
    return-void
.end method


# virtual methods
.method public checkSamsungAccountSignUp(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 111
    const/4 v1, 0x0

    .line 113
    .local v1, "rtn_val":Z
    :try_start_0
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 114
    .local v0, "manager":Landroid/accounts/AccountManager;
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v0, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 115
    .local v2, "samsungAccnts":[Landroid/accounts/Account;
    array-length v3, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lez v3, :cond_0

    .line 116
    const/4 v1, 0x1

    .line 120
    .end local v0    # "manager":Landroid/accounts/AccountManager;
    .end local v2    # "samsungAccnts":[Landroid/accounts/Account;
    :cond_0
    :goto_0
    return v1

    .line 118
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public getSamsungAccountName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    const-string v2, ""

    .line 97
    .local v2, "rtn_val":Ljava/lang/String;
    const/4 v0, 0x0

    .line 99
    .local v0, "FIRST_INDEX":I
    :try_start_0
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 100
    .local v1, "manager":Landroid/accounts/AccountManager;
    const-string v4, "com.osp.app.signin"

    invoke-virtual {v1, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 102
    .local v3, "samsungAccnts":[Landroid/accounts/Account;
    array-length v4, v3

    if-lez v4, :cond_0

    .line 103
    const/4 v4, 0x0

    aget-object v4, v3, v4

    iget-object v2, v4, Landroid/accounts/Account;->name:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    .end local v1    # "manager":Landroid/accounts/AccountManager;
    .end local v3    # "samsungAccnts":[Landroid/accounts/Account;
    :cond_0
    :goto_0
    return-object v2

    .line 105
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public saveSamsungAccount()V
    .locals 9

    .prologue
    const v8, 0x7f0c00b8

    const v7, 0x104000a

    .line 23
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->checkSamsungAccountSignUp(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 24
    const/4 v1, 0x0

    .line 26
    .local v1, "mAlertDialog":Landroid/app/AlertDialog;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->getSamsungAccountName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 27
    .local v3, "samsungAccountName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    const v5, 0x7f0c00b9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 29
    .local v2, "message":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 30
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 31
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 32
    new-instance v4, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$1;

    invoke-direct {v4, p0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;)V

    invoke-virtual {v0, v7, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 40
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 42
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 71
    .end local v3    # "samsungAccountName":Ljava/lang/String;
    :goto_0
    return-void

    .line 44
    .end local v0    # "b":Landroid/app/AlertDialog$Builder;
    .end local v1    # "mAlertDialog":Landroid/app/AlertDialog;
    .end local v2    # "message":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .line 46
    .restart local v1    # "mAlertDialog":Landroid/app/AlertDialog;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    const v5, 0x7f0c00ba

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 48
    .restart local v2    # "message":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 49
    .restart local v0    # "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 51
    new-instance v4, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$2;

    invoke-direct {v4, p0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;)V

    invoke-virtual {v0, v7, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    const/high16 v4, 0x1040000

    new-instance v5, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$3;

    invoke-direct {v5, p0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 69
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public saveSamsungAccount(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "samsungAccount"    # Ljava/lang/String;

    .prologue
    .line 89
    const-string v2, "samsungAccount"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 90
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 91
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "accountId"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 92
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 93
    return-void
.end method

.method public showConfirmUnuseDialog()V
    .locals 5

    .prologue
    .line 124
    const/4 v1, 0x0

    .line 126
    .local v1, "mAlertDialog":Landroid/app/AlertDialog;
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    const v4, 0x7f0c00bb

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 128
    .local v2, "message":Ljava/lang/String;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 129
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0c00b8

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 130
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 131
    const v3, 0x104000a

    new-instance v4, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$4;

    invoke-direct {v4, p0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 139
    const/high16 v3, 0x1040000

    new-instance v4, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$5;

    invoke-direct {v4, p0}, Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount$5;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SaveSamsungAccount;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 147
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 149
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 150
    return-void
.end method
