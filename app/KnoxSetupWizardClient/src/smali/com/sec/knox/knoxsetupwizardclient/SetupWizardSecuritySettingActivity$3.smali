.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$3;
.super Ljava/lang/Object;
.source "SetupWizardSecuritySettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setCheckedEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 297
    const-string v0, "KnoxSetupWizardClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 315
    :goto_0
    const-string v0, "KnoxSetupWizardClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checked index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$000()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setConfirmButton()V
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V

    .line 317
    return-void

    .line 300
    :sswitch_0
    const/4 v0, 0x0

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$002(I)I

    goto :goto_0

    .line 303
    :sswitch_1
    const/4 v0, 0x2

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$002(I)I

    goto :goto_0

    .line 306
    :sswitch_2
    const/4 v0, 0x3

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$002(I)I

    goto :goto_0

    .line 309
    :sswitch_3
    const/4 v0, 0x1

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$002(I)I

    goto :goto_0

    .line 312
    :sswitch_4
    const/4 v0, 0x4

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$002(I)I

    goto :goto_0

    .line 298
    :sswitch_data_0
    .sparse-switch
        0x7f0f008a -> :sswitch_0
        0x7f0f008b -> :sswitch_1
        0x7f0f008c -> :sswitch_2
        0x7f0f00ab -> :sswitch_3
        0x7f0f00ac -> :sswitch_4
    .end sparse-switch
.end method
