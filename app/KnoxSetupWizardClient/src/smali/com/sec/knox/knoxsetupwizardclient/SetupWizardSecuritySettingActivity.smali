.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;
.super Landroid/app/Activity;
.source "SetupWizardSecuritySettingActivity.java"


# static fields
.field private static DEFAULT_TIME_OUT_INDEX:I

.field private static currentLockTypeIdx:I

.field private static timeOutIndex:I


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private btnConfirm:Landroid/widget/LinearLayout;

.field private btn_timeOut:Landroid/widget/RelativeLayout;

.field private btn_title:Landroid/widget/LinearLayout;

.field private isDarkTheme:Z

.field private isPhone:Z

.field private lockTypes:Landroid/widget/RadioGroup;

.field revisedEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field revisedValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private supportFinerPrint:Z

.field private timeOutEntries:[Ljava/lang/String;

.field private timeOutValues:[Ljava/lang/String;

.field private timeoutDialog:Landroid/app/AlertDialog;

.field private tvTimeout:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, -0x1

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    .line 39
    const/4 v0, 0x2

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->DEFAULT_TIME_OUT_INDEX:I

    .line 41
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->DEFAULT_TIME_OUT_INDEX:I

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 33
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    .line 47
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    .line 49
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    .line 51
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeoutDialog:Landroid/app/AlertDialog;

    .line 52
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btn_timeOut:Landroid/widget/RelativeLayout;

    .line 54
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    .line 56
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 30
    sput p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    return p0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setConfirmButton()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeoutDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeoutDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isDarkTheme:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isPhone:Z

    return v0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    return v0
.end method

.method static synthetic access$502(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 30
    sput p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    return p0
.end method

.method static synthetic access$600(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setTimeoutText(I)V

    return-void
.end method

.method private disableUnusableTimeouts(I)V
    .locals 25
    .param p1, "maximumTimeToLock"    # I

    .prologue
    .line 401
    move/from16 v0, p1

    int-to-long v14, v0

    .line 402
    .local v14, "maxTimeout":J
    move-wide v4, v14

    .line 405
    .local v4, "adminTimeout":J
    const/4 v11, -0x1

    .line 406
    .local v11, "limit":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x2

    aget-object v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 408
    int-to-long v0, v11

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-static {v14, v15, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    .line 409
    const-wide/16 v20, 0x1

    cmp-long v20, v14, v20

    if-gez v20, :cond_0

    .line 410
    int-to-long v14, v11

    .line 413
    :cond_0
    const-wide/16 v20, 0x0

    cmp-long v20, v14, v20

    if-nez v20, :cond_1

    .line 464
    :goto_0
    return-void

    .line 417
    :cond_1
    const/4 v10, -0x1

    .line 419
    .local v10, "idx":I
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    .line 420
    .local v8, "entries":[Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    move-object/from16 v17, v0

    .line 421
    .local v17, "values":[Ljava/lang/CharSequence;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    .line 422
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    .line 424
    const-wide/16 v20, 0x0

    cmp-long v20, v4, v20

    if-lez v20, :cond_3

    const/16 v16, 0x1

    .line 425
    .local v16, "startIndex":I
    :goto_1
    const-wide/16 v20, 0x0

    cmp-long v20, v4, v20

    if-lez v20, :cond_4

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v20, v0

    add-int/lit8 v7, v20, -0x1

    .line 426
    .local v7, "endIndex":I
    :goto_2
    move/from16 v9, v16

    .local v9, "i":I
    :goto_3
    if-ge v9, v7, :cond_5

    .line 427
    aget-object v20, v17, v9

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v18

    .line 428
    .local v18, "timeout":J
    cmp-long v20, v18, v14

    if-gtz v20, :cond_2

    .line 429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    aget-object v21, v8, v9

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    aget-object v21, v17, v9

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    move-object/from16 v20, v0

    sget v21, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    aget-object v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isInDefaultTimeoutList(J)Z

    move-result v20

    if-nez v20, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    move-object/from16 v20, v0

    sget v21, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    aget-object v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    cmp-long v20, v20, v18

    if-gez v20, :cond_2

    .line 435
    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v10, v0, :cond_2

    move v10, v9

    .line 426
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 424
    .end local v7    # "endIndex":I
    .end local v9    # "i":I
    .end local v16    # "startIndex":I
    .end local v18    # "timeout":J
    :cond_3
    const/16 v16, 0x0

    goto :goto_1

    .line 425
    .restart local v16    # "startIndex":I
    :cond_4
    move-object/from16 v0, v17

    array-length v7, v0

    goto :goto_2

    .line 440
    .restart local v7    # "endIndex":I
    .restart local v9    # "i":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-eqz v20, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-nez v20, :cond_9

    .line 441
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    const v21, 0x7f0c0046

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getTimeoutNewEntry(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    :cond_7
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    move-object/from16 v20, v0

    sget v21, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    aget-object v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 454
    .local v6, "defaultTimeoutValue":I
    if-eqz v6, :cond_8

    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v6, v0, :cond_a

    :cond_8
    const-wide/16 v20, 0x0

    cmp-long v20, v4, v20

    if-lez v20, :cond_a

    .line 455
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    sput v20, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    goto/16 :goto_0

    .line 445
    .end local v6    # "defaultTimeoutValue":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/CharSequence;

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    sub-long v12, v14, v20

    .line 447
    .local v12, "last_timeout":J
    const-wide/16 v20, 0x0

    cmp-long v20, v12, v20

    if-lez v20, :cond_7

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x2

    aget-object v20, v17, v20

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    cmp-long v20, v14, v20

    if-gez v20, :cond_7

    .line 448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    const v21, 0x7f0c0046

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getTimeoutNewEntry(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, p0

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    .line 456
    .end local v12    # "last_timeout":J
    .restart local v6    # "defaultTimeoutValue":I
    :cond_a
    int-to-long v0, v6

    move-wide/from16 v20, v0

    cmp-long v20, v20, v14

    if-gtz v20, :cond_b

    .line 457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    move-object/from16 v21, v0

    sget v22, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    aget-object v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v20

    sput v20, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    goto/16 :goto_0

    .line 458
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-lez v20, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/CharSequence;

    invoke-interface/range {v20 .. v20}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v20

    cmp-long v20, v20, v14

    if-nez v20, :cond_c

    .line 460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    sput v20, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    goto/16 :goto_0

    .line 462
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    sput v20, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    goto/16 :goto_0
.end method

.method private getTimeoutNewEntry(J)Ljava/lang/String;
    .locals 11
    .param p1, "time"    # J

    .prologue
    .line 467
    const-wide/16 v6, 0x3e8

    div-long v4, p1, v6

    .line 468
    .local v4, "second":J
    const-wide/16 v6, 0x3c

    div-long v0, v4, v6

    .line 469
    .local v0, "minute":J
    const-wide/16 v6, 0x3c

    rem-long/2addr v4, v6

    .line 471
    const-string v2, ""

    .line 472
    .local v2, "result":Ljava/lang/String;
    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-lez v3, :cond_0

    .line 473
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v6, 0x7f0d0000

    long-to-int v7, v0

    invoke-virtual {v3, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    long-to-int v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 475
    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-lez v3, :cond_1

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    .line 476
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 478
    :cond_1
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 479
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0d0001

    long-to-int v8, v4

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    long-to-int v9, v4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 482
    :cond_2
    const-string v3, "KnoxSetupWizardClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getTimeoutNewEntry : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    return-object v2
.end method

.method private initTimeOutEntries()V
    .locals 7

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "idx":I
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    array-length v2, v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    .line 153
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    add-int/lit8 v1, v0, 0x1

    .end local v0    # "idx":I
    .local v1, "idx":I
    const v3, 0x7f0c0045

    invoke-virtual {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    move v0, v1

    .line 154
    .end local v1    # "idx":I
    .restart local v0    # "idx":I
    :goto_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 155
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    const v3, 0x7f0c0044

    invoke-virtual {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    aget-object v6, v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    div-int/lit16 v6, v6, 0x3e8

    div-int/lit8 v6, v6, 0x3c

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    const v3, 0x7f0c0043

    invoke-virtual {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 158
    return-void
.end method

.method private isCheckedLocktype()Z
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 347
    const/4 v0, 0x1

    .line 349
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInDefaultTimeoutList(J)Z
    .locals 7
    .param p1, "timeout"    # J

    .prologue
    const/4 v2, 0x0

    .line 488
    const/4 v1, 0x0

    .line 490
    .local v1, "values":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f070000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 492
    if-nez v1, :cond_1

    .line 497
    :cond_0
    :goto_0
    return v2

    .line 494
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 495
    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-nez v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    .line 494
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setCheckedEvent()V
    .locals 2

    .prologue
    .line 290
    const-string v1, "setCheckedEvent"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 292
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setConfirmButton()V

    .line 294
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V

    .line 320
    .local v0, "listener":Landroid/view/View$OnClickListener;
    const v1, 0x7f0f008a

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    const v1, 0x7f0f008b

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    const v1, 0x7f0f008c

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    const v1, 0x7f0f00ab

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 324
    const v1, 0x7f0f00ac

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    return-void
.end method

.method private setConfirmButton()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 328
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isCheckedLocktype()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 329
    const-string v2, "Confirm button enable"

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 330
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 331
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 332
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 333
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 331
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 336
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    :cond_0
    const-string v2, "Confirm button diable"

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 337
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 338
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 339
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 340
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 338
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 343
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private setEventCallBack()V
    .locals 2

    .prologue
    .line 173
    const-string v0, "setEventCallBack"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btn_title:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btn_title:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 227
    :cond_0
    return-void
.end method

.method private setLockTimeoutButton()V
    .locals 2

    .prologue
    .line 353
    const-string v0, "setLockTimeoutButton"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 354
    const v0, 0x7f0f00ae

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btn_timeOut:Landroid/widget/RelativeLayout;

    .line 356
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btn_timeOut:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 398
    return-void
.end method

.method private setPasswordEnabledByPolicy(I)V
    .locals 10
    .param p1, "passWordQuality"    # I

    .prologue
    .line 230
    const/4 v1, 0x0

    .line 231
    .local v1, "isMultifactorAuthEnforced":Z
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMultifactorAuthenticationEnabled()Z

    move-result v1

    .line 232
    const-string v7, "KnoxSetupWizardClient"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isMultifactorAuthEnforced : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    if-eqz v1, :cond_0

    iget-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->supportFinerPrint:Z

    if-eqz v7, :cond_0

    .line 235
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f008a

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 236
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f008b

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 237
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f00ab

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 238
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f00ac

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 239
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f008c

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 287
    :goto_0
    return-void

    .line 244
    :cond_0
    const/4 v2, 0x0

    .line 245
    .local v2, "isSupportFP":Z
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getBiometricAuthenticationEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getResetPasswordKey()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getBiometricAuthenticationEnabledType()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 247
    const/4 v2, 0x1

    .line 251
    :cond_1
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f008a

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 252
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f008b

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/high16 v7, 0x20000

    if-gt p1, v7, :cond_2

    const/4 v7, 0x1

    :goto_1
    invoke-virtual {v8, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 256
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getResetPasswordKey()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 257
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f00ab

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 261
    :goto_2
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f008c

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/high16 v7, 0x10000

    if-gt p1, v7, :cond_5

    const/4 v7, 0x1

    :goto_3
    invoke-virtual {v8, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 262
    sget v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    packed-switch v7, :pswitch_data_0

    .line 284
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "currentLockTypeIdx in onResume : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 252
    :cond_2
    const/4 v7, 0x0

    goto :goto_1

    .line 259
    :cond_3
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v8, 0x7f0f00ab

    invoke-virtual {v7, v8}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    if-eqz v2, :cond_4

    const/4 v7, 0x1

    :goto_4
    invoke-virtual {v8, v7}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2

    :cond_4
    const/4 v7, 0x0

    goto :goto_4

    .line 261
    :cond_5
    const/4 v7, 0x0

    goto :goto_3

    .line 264
    :pswitch_0
    const v7, 0x7f0f008c

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    .line 265
    .local v4, "pattern":Landroid/widget/RadioButton;
    const/high16 v7, 0x10000

    if-gt p1, v7, :cond_6

    const/4 v7, 0x1

    :goto_5
    invoke-virtual {v4, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    :cond_6
    const/4 v7, 0x0

    goto :goto_5

    .line 268
    .end local v4    # "pattern":Landroid/widget/RadioButton;
    :pswitch_1
    const v7, 0x7f0f008b

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    .line 269
    .local v5, "pin":Landroid/widget/RadioButton;
    const/high16 v7, 0x20000

    if-gt p1, v7, :cond_7

    const/4 v7, 0x1

    :goto_6
    invoke-virtual {v5, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    :cond_7
    const/4 v7, 0x0

    goto :goto_6

    .line 272
    .end local v5    # "pin":Landroid/widget/RadioButton;
    :pswitch_2
    const v7, 0x7f0f008a

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 273
    .local v3, "password":Landroid/widget/RadioButton;
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 276
    .end local v3    # "password":Landroid/widget/RadioButton;
    :pswitch_3
    const v7, 0x7f0f00ab

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 277
    .local v0, "fingerPrint":Landroid/widget/RadioButton;
    if-eqz v2, :cond_8

    const/4 v7, 0x1

    :goto_7
    invoke-virtual {v0, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    :cond_8
    const/4 v7, 0x0

    goto :goto_7

    .line 280
    .end local v0    # "fingerPrint":Landroid/widget/RadioButton;
    :pswitch_4
    const v7, 0x7f0f00ac

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioButton;

    .line 281
    .local v6, "twoStep":Landroid/widget/RadioButton;
    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private setTimeoutText(I)V
    .locals 9
    .param p1, "which"    # I

    .prologue
    const v8, 0x7f0c005d

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 501
    const-string v1, "setTimeoutText"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 502
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 503
    const v1, 0x7f0f00b0

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    .line 507
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setSecurityTimeOut(I)V

    .line 509
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumTimeToLock()I

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    .line 511
    .local v0, "isRevisedList":Z
    :goto_0
    if-nez p1, :cond_2

    if-nez v0, :cond_2

    .line 512
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    :goto_1
    return-void

    .end local v0    # "isRevisedList":Z
    :cond_1
    move v0, v3

    .line 509
    goto :goto_0

    .line 513
    .restart local v0    # "isRevisedList":Z
    :cond_2
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutEntries:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_3

    if-nez v0, :cond_3

    .line 514
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    const v2, 0x7f0c003f

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 516
    :cond_3
    if-eqz v0, :cond_4

    .line 517
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedValues:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-long v6, v1

    invoke-direct {p0, v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getTimeoutNewEntry(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-virtual {p0, v8, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 519
    :cond_4
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    int-to-long v4, v4

    invoke-direct {p0, v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getTimeoutNewEntry(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v8, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 527
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 528
    const/4 v0, -0x1

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    .line 529
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setResult(I)V

    .line 530
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->finish()V

    .line 531
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const v13, 0x7f0f00b4

    const/16 v10, 0x400

    const/16 v12, 0x8

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 68
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, " : onCreate"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 69
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    const/16 v5, 0x2000

    .line 72
    .local v5, "windowFlags":I
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v9, 0x7f080000

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isPhone:Z

    .line 73
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f080001

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isDarkTheme:Z

    .line 75
    iget-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isPhone:Z

    if-eqz v6, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v10, v10}, Landroid/view/Window;->setFlags(II)V

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    new-instance v9, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09000d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-direct {v9, v10}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v6, v9}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/view/Window;->addFlags(I)V

    .line 84
    sget v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->DEFAULT_TIME_OUT_INDEX:I

    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    .line 86
    const v6, 0x7f03001e

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setContentView(I)V

    .line 88
    const v6, 0x7f0f0089

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RadioGroup;

    iput-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    .line 89
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->lockTypes:Landroid/widget/RadioGroup;

    invoke-virtual {v6}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    .line 91
    const v6, 0x7f0f00b0

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->tvTimeout:Landroid/widget/TextView;

    .line 93
    const v6, 0x7f0f006c

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    .line 94
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 95
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 96
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 97
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    iget-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isPhone:Z

    if-eqz v6, :cond_2

    .line 101
    const v6, 0x7f0f009a

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->btn_title:Landroid/widget/LinearLayout;

    .line 103
    :cond_2
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v9, 0x7f070000

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;

    .line 104
    invoke-static {v8}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintPlus(Z)V

    .line 105
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->initTimeOutEntries()V

    .line 107
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumTimeToLock()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->disableUnusableTimeouts(I)V

    .line 109
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setEventCallBack()V

    .line 111
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v9, "com.sec.feature.fingerprint_manager_service"

    invoke-virtual {v6, v9}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    move v6, v7

    :goto_1
    iput-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->supportFinerPrint:Z

    .line 113
    iget-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->supportFinerPrint:Z

    if-nez v6, :cond_3

    .line 114
    const v6, 0x7f0f00ab

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/view/View;->setVisibility(I)V

    .line 115
    const v6, 0x7f0f00ac

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/view/View;->setVisibility(I)V

    .line 116
    invoke-virtual {p0, v13}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    const v6, 0x7f0f00b5

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 117
    invoke-virtual {p0, v13}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/view/View;->setVisibility(I)V

    .line 118
    const v6, 0x7f0f00b5

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/view/View;->setVisibility(I)V

    .line 123
    :cond_3
    sget v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    const/4 v9, -0x1

    if-ne v6, v9, :cond_9

    .line 124
    const/4 v3, 0x0

    .line 125
    .local v3, "isSupportFP":Z
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getBiometricAuthenticationEnabled()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getResetPasswordKey()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getBiometricAuthenticationEnabledType()I

    move-result v6

    if-ne v6, v7, :cond_4

    .line 129
    const/4 v3, 0x1

    .line 131
    :cond_4
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v4

    .line 132
    .local v4, "passWordQuality":I
    const/high16 v6, 0x10000

    if-gt v4, v6, :cond_7

    .line 133
    const/4 v6, 0x3

    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    .line 139
    :goto_2
    if-eqz v3, :cond_5

    iget-boolean v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->supportFinerPrint:Z

    if-eqz v6, :cond_5

    .line 140
    sput v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    .line 146
    .end local v3    # "isSupportFP":Z
    .end local v4    # "passWordQuality":I
    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/os/PersonaManager;->getKnoxContainerVersion(Landroid/content/Context;)Landroid/os/PersonaManager$KnoxContainerVersion;

    move-result-object v1

    .line 147
    .local v1, "currentVersion":Landroid/os/PersonaManager$KnoxContainerVersion;
    const-string v6, "KnoxSetupWizardClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "KnoxContainerVersion currentVersion : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void

    .end local v1    # "currentVersion":Landroid/os/PersonaManager$KnoxContainerVersion;
    :cond_6
    move v6, v8

    .line 111
    goto/16 :goto_1

    .line 134
    .restart local v3    # "isSupportFP":Z
    .restart local v4    # "passWordQuality":I
    :cond_7
    const/high16 v6, 0x20000

    if-gt v4, v6, :cond_8

    .line 135
    const/4 v6, 0x2

    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    goto :goto_2

    .line 137
    :cond_8
    sput v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    goto :goto_2

    .line 143
    .end local v3    # "isSupportFP":Z
    .end local v4    # "passWordQuality":I
    :cond_9
    const-string v6, "KnoxSetupWizardClient"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "currentLockTypeIdx in onCreate : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->currentLockTypeIdx:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public onMenuItemSelected(ILandroid/view/MenuItem;)Z
    .locals 2
    .param p1, "featureId"    # I
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 536
    const v0, 0x102002c

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->onBackPressed()V

    .line 538
    const/4 v0, 0x1

    .line 541
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onResume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 165
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setLockTimeoutButton()V

    .line 166
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I

    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setTimeoutText(I)V

    .line 168
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setPasswordEnabledByPolicy(I)V

    .line 169
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setCheckedEvent()V

    .line 170
    return-void
.end method
