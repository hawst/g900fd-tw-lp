.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;
.super Ljava/lang/Object;
.source "SetupWizardResetPasswordActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->identifyFinger()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 344
    const-string v0, "finger identify dialog dismissed"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 345
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfirmed:Z
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIsPrimaryLockSet:Z
    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$202(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z

    .line 349
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(I)V

    .line 350
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->finish()V

    .line 352
    :cond_0
    return-void
.end method
