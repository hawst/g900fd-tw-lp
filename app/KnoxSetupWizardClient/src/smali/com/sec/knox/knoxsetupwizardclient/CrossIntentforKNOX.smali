.class public Lcom/sec/knox/knoxsetupwizardclient/CrossIntentforKNOX;
.super Ljava/lang/Object;
.source "CrossIntentforKNOX.java"


# direct methods
.method public static setFilters(Landroid/os/UserManager;Landroid/content/pm/PackageManager;II)V
    .locals 22
    .param p0, "um"    # Landroid/os/UserManager;
    .param p1, "pm"    # Landroid/content/pm/PackageManager;
    .param p2, "parentUserId"    # I
    .param p3, "personaId"    # I

    .prologue
    .line 25
    const-string v19, "setFilters started"

    invoke-static/range {v19 .. v19}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 32
    new-instance v14, Landroid/content/IntentFilter;

    invoke-direct {v14}, Landroid/content/IntentFilter;-><init>()V

    .line 33
    .local v14, "mimeTypeTelephony":Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.DIAL"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 34
    const-string v19, "android.intent.action.VIEW"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 35
    const-string v19, "android.intent.action.CALL_EMERGENCY"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 36
    const-string v19, "android.intent.action.CALL_PRIVILEGED"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 37
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 38
    const-string v19, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 40
    :try_start_0
    const-string v19, "vnd.android.cursor.item/phone"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 41
    const-string v19, "vnd.android.cursor.item/phone_v2"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 42
    const-string v19, "vnd.android.cursor.item/person"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 43
    const-string v19, "vnd.android.cursor.dir/calls"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V

    .line 44
    const-string v19, "vnd.android.cursor.item/calls"

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v14, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 50
    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    .line 51
    .local v8, "callEmergency":Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.CALL_EMERGENCY"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 52
    const-string v19, "android.intent.action.CALL_PRIVILEGED"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 53
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 54
    const-string v19, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 55
    const-string v19, "tel"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 56
    const-string v19, "sip"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 57
    const-string v19, "voicemail"

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 58
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 60
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 61
    .local v9, "callVoicemail":Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.DIAL"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 62
    const-string v19, "android.intent.action.CALL"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 63
    const-string v19, "android.intent.action.VIEW"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 64
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 65
    const-string v19, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 66
    const-string v19, "voicemail"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 67
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 72
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 73
    .local v6, "callDial":Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.DIAL"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 74
    const-string v19, "android.intent.action.CALL"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    const-string v19, "android.intent.action.VIEW"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 77
    const-string v19, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 78
    const-string v19, "tel"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 79
    const-string v19, "sip"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 80
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 82
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    .line 83
    .local v5, "callButton":Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.CALL_BUTTON"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 84
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 85
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 87
    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    .line 88
    .local v7, "callDialNoData":Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.DIAL"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 89
    const-string v19, "android.intent.action.CALL"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 90
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 91
    const-string v19, "android.intent.category.BROWSABLE"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 92
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v7, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 94
    new-instance v18, Landroid/content/IntentFilter;

    invoke-direct/range {v18 .. v18}, Landroid/content/IntentFilter;-><init>()V

    .line 95
    .local v18, "smsMms":Landroid/content/IntentFilter;
    const-string v19, "android.intent.action.VIEW"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 96
    const-string v19, "android.intent.action.SENDTO"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 97
    const-string v19, "android.intent.category.DEFAULT"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 98
    const-string v19, "android.intent.category.BROWSABLE"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 99
    const-string v19, "sms"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 100
    const-string v19, "smsto"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 101
    const-string v19, "mms"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 102
    const-string v19, "mmsto"

    invoke-virtual/range {v18 .. v19}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 103
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    move/from16 v2, p3

    move/from16 v3, p2

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 105
    new-instance v15, Landroid/content/IntentFilter;

    invoke-direct {v15}, Landroid/content/IntentFilter;-><init>()V

    .line 106
    .local v15, "mobileNetworkSettings":Landroid/content/IntentFilter;
    const-string v19, "android.settings.DATA_ROAMING_SETTINGS"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    const-string v19, "android.settings.NETWORK_OPERATOR_SETTINGS"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 109
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, p3

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v15, v1, v2, v3}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 188
    const-string v19, "setFilters Done!!"

    invoke-static/range {v19 .. v19}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 193
    const-string v19, " Setting up intent forwarding for KNOX."

    invoke-static/range {v19 .. v19}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 198
    new-instance v13, Landroid/content/Intent;

    invoke-direct {v13}, Landroid/content/Intent;-><init>()V

    .line 199
    .local v13, "intent":Landroid/content/Intent;
    const-string v19, "android.intent.category.DEFAULT"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    const-string v19, "com.android.settings"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    const/16 v19, 0xc1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v13, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v16

    .line 205
    .local v16, "resolvedIntents":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v10

    .line 206
    .local v10, "count":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v10, :cond_1

    .line 207
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/pm/ResolveInfo;

    .line 208
    .local v12, "info":Landroid/content/pm/ResolveInfo;
    iget-object v0, v12, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    iget-object v0, v12, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    iget-object v0, v12, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 209
    iget-object v0, v12, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v19, v0

    const-string v20, "com.sec.android.PRIMARY_KNOX_CONTROLLED"

    invoke-virtual/range {v19 .. v20}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v17

    .line 210
    .local v17, "shouldForward":Z
    if-eqz v17, :cond_0

    .line 211
    iget-object v0, v12, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, p3

    move/from16 v3, v20

    move/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/pm/PackageManager;->addCrossProfileIntentFilter(Landroid/content/IntentFilter;III)V

    .line 206
    .end local v17    # "shouldForward":Z
    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 216
    .end local v12    # "info":Landroid/content/pm/ResolveInfo;
    :cond_1
    return-void

    .line 45
    .end local v5    # "callButton":Landroid/content/IntentFilter;
    .end local v6    # "callDial":Landroid/content/IntentFilter;
    .end local v7    # "callDialNoData":Landroid/content/IntentFilter;
    .end local v8    # "callEmergency":Landroid/content/IntentFilter;
    .end local v9    # "callVoicemail":Landroid/content/IntentFilter;
    .end local v10    # "count":I
    .end local v11    # "i":I
    .end local v13    # "intent":Landroid/content/Intent;
    .end local v15    # "mobileNetworkSettings":Landroid/content/IntentFilter;
    .end local v16    # "resolvedIntents":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v18    # "smsMms":Landroid/content/IntentFilter;
    :catch_0
    move-exception v19

    goto/16 :goto_0
.end method
