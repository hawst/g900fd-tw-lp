.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;
.super Landroid/content/BroadcastReceiver;
.source "SetupWizardWelcome.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->registerBRReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V
    .locals 0

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 445
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 446
    .local v0, "action":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 448
    const-string v1, "com.sec.knox.start.SETUP_WIZARD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 449
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v1

    if-nez v1, :cond_1

    .line 450
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->saveContainerCreationParam(Landroid/content/Intent;)V
    invoke-static {v1, p2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$1200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;Landroid/content/Intent;)V

    .line 451
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 454
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd_timer:Landroid/os/CountDownTimer;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$1300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/os/CountDownTimer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 455
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd_timer:Landroid/os/CountDownTimer;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->access$1300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/os/CountDownTimer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/CountDownTimer;->cancel()V

    .line 459
    :cond_1
    return-void
.end method
