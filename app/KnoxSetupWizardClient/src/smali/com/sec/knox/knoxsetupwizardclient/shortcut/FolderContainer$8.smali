.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setCallBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 985
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 989
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v2

    sget-object v3, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 991
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 992
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.knox.containeragent2"

    const-string v3, "com.sec.knox.containeragent2.ui.settings.TransparentActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    .local v0, "component":Landroid/content/ComponentName;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 994
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 995
    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/os/UserHandle;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v4, v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 996
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v5, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mUnlockReq:Z

    .line 997
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v5, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddApp:Z

    .line 1008
    .end local v0    # "component":Landroid/content/ComponentName;
    :goto_0
    return-void

    .line 1001
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1002
    .restart local v1    # "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1003
    const-string v2, "userid"

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1004
    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1006
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$8;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showMoreLayout(Z)V

    goto :goto_0
.end method
