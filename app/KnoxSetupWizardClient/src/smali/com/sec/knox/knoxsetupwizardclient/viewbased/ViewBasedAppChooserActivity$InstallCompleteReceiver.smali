.class Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ViewBasedAppChooserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InstallCompleteReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;


# direct methods
.method private constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;
    .param p2, "x1"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;

    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;-><init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 235
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "knox.lwc.action.APP_INSTALLED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    const-string v0, "ADD"

    const-string v1, "App Installation Complete - Inside App Chooser Activity !!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$800(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 239
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->finish()V

    .line 241
    :cond_0
    return-void
.end method
