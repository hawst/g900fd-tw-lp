.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setCallBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 1027
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1030
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v6, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 1031
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v2, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setSpinnerItems(I)V

    .line 1032
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1033
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1034
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v2

    sget-object v3, Landroid/content/pm/PersonaState;->ACTIVE:Landroid/content/pm/PersonaState;

    invoke-virtual {v2, v3}, Landroid/os/PersonaManager$StateManager;->inState(Landroid/content/pm/PersonaState;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1036
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1037
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.knox.containeragent2"

    const-string v3, "com.sec.knox.containeragent2.ui.settings.TransparentActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    .local v0, "component":Landroid/content/ComponentName;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1039
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1040
    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/os/UserHandle;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v4, v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1041
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v6, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mUnlockReq:Z

    .line 1042
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v6, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mRemoveApp:Z

    .line 1057
    .end local v0    # "component":Landroid/content/ComponentName;
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 1046
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->loadAppList()V

    .line 1048
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-direct {v3, v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Ljava/util/List;)V

    iput-object v3, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    .line 1049
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-boolean v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    invoke-virtual {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->setUninstallMode(Z)V

    .line 1050
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1052
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v2, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showMoreLayout(Z)V

    .line 1053
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$9;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showRemoveLayout(Z)V
    invoke-static {v2, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$900(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Z)V

    goto :goto_0
.end method
