.class public Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;
.super Landroid/widget/BaseAdapter;
.source "AddIconAdapter.java"


# instance fields
.field private height:I

.field private iconCount:I

.field private mContext:Landroid/content/Context;

.field private width:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->mContext:Landroid/content/Context;

    .line 24
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->width:I

    .line 25
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->height:I

    .line 26
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->iconCount:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 35
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 40
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 65
    const/4 v0, 0x0

    .line 66
    .local v0, "view":Landroid/widget/ImageView;
    if-nez p2, :cond_0

    .line 67
    new-instance v0, Landroid/widget/ImageView;

    .end local v0    # "view":Landroid/widget/ImageView;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 68
    .restart local v0    # "view":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->width:I

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->height:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    :goto_0
    const v1, 0x7f020007

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 77
    return-object v0

    :cond_0
    move-object v0, p2

    .line 70
    check-cast v0, Landroid/widget/ImageView;

    goto :goto_0
.end method

.method public setIconCount(II)V
    .locals 2
    .param p1, "count"    # I
    .param p2, "maxGridXCount"    # I

    .prologue
    .line 44
    add-int/lit8 v0, p2, -0x1

    .line 45
    .local v0, "columnCount":I
    rem-int v1, p1, p2

    if-nez v1, :cond_0

    .line 46
    div-int v1, p1, p2

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->iconCount:I

    .line 50
    :goto_0
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->iconCount:I

    mul-int/2addr v1, v0

    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->iconCount:I

    .line 51
    return-void

    .line 48
    :cond_0
    div-int v1, p1, p2

    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->iconCount:I

    goto :goto_0
.end method
