.class Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "KnoxSetupWizardDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatabaseHelper"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;Landroid/content/Context;)V
    .locals 3
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;->this$0:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    .line 81
    const-string v0, "KnoxSetupWizard.db"

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 82
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 85
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 86
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 89
    const-string v0, "KnoxSetupWizardDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upgrading database from version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", which will destroy all old data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    const/4 v0, 0x4

    if-ne p3, v0, :cond_0

    .line 93
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->alterTable3to4(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->dropTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 98
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0
.end method
