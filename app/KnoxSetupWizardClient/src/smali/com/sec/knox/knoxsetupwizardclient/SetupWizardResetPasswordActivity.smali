.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;
.super Landroid/app/Activity;
.source "SetupWizardResetPasswordActivity.java"


# static fields
.field public static called_pass_type:I

.field private static lockQuality:I


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private mDPM:Landroid/app/admin/DevicePolicyManager;

.field private mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

.field private mFingerprintConfimationStarted:Z

.field private mFingerprintConfirmed:Z

.field private mIdentifyDialog:Landroid/app/Dialog;

.field private mIsPrimaryLockSet:Z

.field private mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->lockQuality:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIdentifyDialog:Landroid/app/Dialog;

    .line 60
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfirmed:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIsPrimaryLockSet:Z

    .line 66
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V

    invoke-direct {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfirmed:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfirmed:Z

    return p1
.end method

.method static synthetic access$102(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIsPrimaryLockSet:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)Lcom/samsung/android/fingerprint/FingerprintManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->showSensorErrorDialog()V

    return-void
.end method

.method private identifyFinger()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 329
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIdentifyDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIdentifyDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIdentifyDialog:Landroid/app/Dialog;

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    if-eqz v0, :cond_1

    .line 335
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfirmed:Z

    .line 337
    iput-boolean v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIsPrimaryLockSet:Z

    .line 339
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    const-string v2, "com.android.settings.permission.unlock"

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/samsung/android/fingerprint/FingerprintManager;->showIdentifyDialog(Landroid/content/Context;Lcom/samsung/android/fingerprint/FingerprintIdentifyDialog$FingerprintListener;Ljava/lang/String;Z)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIdentifyDialog:Landroid/app/Dialog;

    .line 340
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIdentifyDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 355
    :cond_1
    return-void
.end method

.method private showSensorErrorDialog()V
    .locals 4

    .prologue
    .line 358
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0c00bf

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0c00c0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$4;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$3;

    invoke-direct {v2, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 372
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 373
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/16 v8, 0x3f6

    const/16 v4, 0x3f3

    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult requestCode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resultCode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 125
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 126
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p2, :cond_1

    if-eq p1, v4, :cond_0

    const/16 v2, 0x3f4

    if-ne p1, v2, :cond_1

    .line 127
    :cond_0
    const-string v2, "KnoxSetupWizardClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "go to SetupwizardRootctivity "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintSupplement(Z)V

    .line 129
    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPasswordType(I)V

    .line 130
    const-string v2, "called_pass_type"

    sget v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 132
    invoke-virtual {p0, v7, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(ILandroid/content/Intent;)V

    .line 175
    :goto_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->finish()V

    .line 176
    return-void

    .line 133
    :cond_1
    if-eqz p2, :cond_2

    const/16 v2, 0x3f5

    if-ne p1, v2, :cond_2

    .line 138
    const-string v2, "KnoxSetupWizardClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "go to SetupwizardRootctivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintSupplement(Z)V

    .line 140
    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPasswordType(I)V

    .line 141
    const-string v2, "called_pass_type"

    sget v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 143
    invoke-virtual {p0, v7, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 144
    :cond_2
    if-eqz p2, :cond_3

    if-eq p2, v6, :cond_3

    if-ne p1, v8, :cond_3

    .line 146
    const-string v2, "KnoxSetupWizardClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "go to SetupwizardRootctivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " resultCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPasswordType(I)V

    .line 149
    const-string v2, "knoxPin"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "pin":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPassword(Ljava/lang/String;)V

    .line 151
    invoke-static {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintSupplement(Z)V

    .line 152
    const-string v2, "knoxPin"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    iput-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIsPrimaryLockSet:Z

    .line 155
    const-string v2, "called_pass_type"

    sget v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0, v7, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 157
    .end local v1    # "pin":Ljava/lang/String;
    :cond_3
    if-eqz p2, :cond_4

    if-eq p2, v6, :cond_4

    const/16 v2, 0x3f7

    if-ne p1, v2, :cond_4

    .line 159
    const-string v2, "KnoxSetupWizardClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "go to SetupwizardRootctivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " resultCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintSupplement(Z)V

    .line 162
    const-string v2, "called_pass_type"

    sget v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0, v7, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 164
    :cond_4
    if-nez p2, :cond_5

    if-ge p1, v4, :cond_6

    :cond_5
    if-ne p2, v6, :cond_7

    if-ne p1, v8, :cond_7

    .line 166
    :cond_6
    const-string v2, "KnoxSetupWizardClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "canceled, go to SetupwizardRootctivity"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p0, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(I)V

    goto/16 :goto_0

    .line 168
    :cond_7
    if-eqz p2, :cond_8

    if-ne p2, v6, :cond_9

    :cond_8
    const/16 v2, 0x3f7

    if-ne p1, v2, :cond_9

    .line 170
    invoke-virtual {p0, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(I)V

    goto/16 :goto_0

    .line 172
    :cond_9
    const-string v2, "KnoxSetupWizardClient"

    const-string v3, "I don\'t know where I have to go."

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p0, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(I)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    .line 94
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : onCreate"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 97
    const-string v1, "inside ResetPasswordActivity::onCreate()"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 99
    const-string v1, "device_policy"

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 100
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/samsung/android/fingerprint/FingerprintManager;->getInstance(Landroid/content/Context;I)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    .line 102
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "called_pass_type"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    .line 104
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getContainerCreationParams()Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v0

    .line 106
    .local v0, "param":Lcom/sec/knox/container/ContainerCreationParams;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "called_pass_type is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x2000

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 110
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showing lock quality "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v3, 0x0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "LOCK_QUALITY"

    const/high16 v3, 0x60000

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->lockQuality:I

    .line 114
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showing lock quality "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->lockQuality:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 116
    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    if-eq v1, v5, :cond_0

    .line 117
    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->called_pass_type:I

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->updateUnlockMethodAndFinish(I)V

    .line 119
    :cond_0
    return-void
.end method

.method updateUnlockMethodAndFinish(I)V
    .locals 12
    .param p1, "quality"    # I

    .prologue
    const/16 v11, 0x100

    const/4 v10, 0x2

    const/high16 v9, 0x60000

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 190
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "lockscreen.biometric_weak_fallback"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 192
    .local v3, "isFallback":Z
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "Block"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 193
    .local v2, "isBlock":Z
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "lockscreen.signature_fallback"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 196
    .local v4, "isSignature":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateUnlockMethodAndFinish : quality : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " isFallback : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 199
    if-eqz p1, :cond_0

    if-ne p1, v10, :cond_8

    .line 201
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 202
    .local v1, "intent":Landroid/content/Intent;
    const-class v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardChooseLockPassword;

    invoke-virtual {v1, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 204
    const-string v5, "confirm_credentials"

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 207
    iget-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIsPrimaryLockSet:Z

    if-eqz v5, :cond_4

    .line 208
    const-string v5, "mIsPrimaryLockSet is true, and call password set(primary) again"

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 209
    const/high16 v0, 0x60000

    .line 210
    .local v0, "configQuality":I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v5

    const/high16 v6, 0x50000

    if-ge v5, v6, :cond_3

    .line 211
    const-string v5, "FINGER-REGISTERED: PASSWORD_QUALITY_ALPHABETIC"

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 212
    const/high16 v0, 0x40000

    .line 217
    :cond_1
    :goto_0
    const-string v5, "lockscreen.password_type"

    invoke-virtual {v1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 218
    const-string v5, "fromFinger"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 219
    const/16 v5, 0x3f6

    invoke-virtual {p0, v1, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 326
    .end local v0    # "configQuality":I
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    :goto_1
    return-void

    .line 213
    .restart local v0    # "configQuality":I
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v5

    if-ge v5, v9, :cond_1

    .line 214
    const-string v5, "FINGER-REGISTERED: PASSWORD_QUALITY_ALPHANUMERIC"

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 215
    const/high16 v0, 0x50000

    goto :goto_0

    .line 220
    .end local v0    # "configQuality":I
    :cond_4
    if-eq p1, v10, :cond_7

    .line 222
    const/high16 v0, 0x60000

    .line 223
    .restart local v0    # "configQuality":I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v5

    const/high16 v6, 0x50000

    if-ge v5, v6, :cond_6

    .line 224
    const-string v5, "PWD quality policy : PASSWORD_QUALITY_ALPHABETIC"

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 225
    const/high16 v0, 0x40000

    .line 230
    :cond_5
    :goto_2
    const-string v5, "lockscreen.password_type"

    invoke-virtual {v1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 231
    const/16 v5, 0x3f3

    invoke-virtual {p0, v1, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 226
    :cond_6
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v5

    if-ge v5, v9, :cond_5

    .line 227
    const-string v5, "PWD quality policy: PASSWORD_QUALITY_ALPHANUMERIC"

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 228
    const/high16 v0, 0x50000

    goto :goto_2

    .line 233
    .end local v0    # "configQuality":I
    :cond_7
    const-string v5, "lockscreen.password_type"

    const/high16 v6, 0x20000

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 234
    const/16 v5, 0x3f4

    invoke-virtual {p0, v1, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 237
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_8
    const/4 v5, 0x3

    if-ne p1, v5, :cond_9

    .line 238
    const/4 v3, 0x1

    .line 239
    const/4 v2, 0x1

    .line 240
    const/4 v4, 0x0

    .line 241
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 242
    .restart local v1    # "intent":Landroid/content/Intent;
    const-class v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;

    invoke-virtual {v1, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 244
    const-string v5, "key_lock_method"

    const-string v6, "pattern"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    const-string v5, "confirm_credentials"

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 246
    const-string v5, "lockscreen.biometric_weak_fallback"

    invoke-virtual {v1, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 247
    const-string v5, "lockscreen.signature_fallback"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 248
    const-string v5, "Block"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 250
    if-eqz v3, :cond_2

    .line 251
    const/16 v5, 0x3f5

    invoke-virtual {p0, v1, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 255
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_9
    if-ne p1, v7, :cond_e

    .line 257
    const-string v5, "updateUnlockMethodAndFinish : PASSWORD_QUALITY_FINGERPRINT "

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 259
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/FingerprintManager;->getEnrolledFingers()I

    move-result v5

    if-nez v5, :cond_c

    .line 260
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/FingerprintManager;->notifyEnrollBegin()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 261
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 262
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v5, "com.android.settings"

    const-string v6, "com.android.settings.fingerprint.FingerprintDisclaimer"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 263
    const-string v5, "fingerIndex"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 264
    const-string v5, "previousStage"

    const-string v6, "ChooseLockGeneric"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    const-string v5, "isFromKnoxSetupWizard"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 267
    const-string v5, "lockscreen.password_min"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinimumLength()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 268
    const-string v5, "lockscreen.password_max"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMaximumLength()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 269
    const-string v5, "lockscreen.password_type"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 270
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v5

    if-ne v5, v9, :cond_a

    .line 271
    const-string v5, "lockscreen.password_min_letters"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinLetters()I

    move-result v6

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 272
    const-string v5, "lockscreen.password_min_lowercase"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinLowerCase()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 273
    const-string v5, "lockscreen.password_min_uppercase"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinUpperCase()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 274
    const-string v5, "lockscreen.password_min_numeric"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinNumeric()I

    move-result v6

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 275
    const-string v5, "lockscreen.password_min_symbols"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinSymbols()I

    move-result v6

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 276
    const-string v5, "lockscreen.password_min_nonletter"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordMinNonLetter()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 280
    :cond_a
    const-string v5, "startactivity : ChooseLockFingerprint "

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 281
    const/16 v5, 0x3f6

    invoke-virtual {p0, v1, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 283
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_b
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->showSensorErrorDialog()V

    goto/16 :goto_1

    .line 287
    :cond_c
    iget-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z

    if-nez v5, :cond_2

    .line 288
    iput-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z

    .line 289
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/FingerprintManager;->notifyEnrollBegin()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 290
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->identifyFinger()V

    goto/16 :goto_1

    .line 292
    :cond_d
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->showSensorErrorDialog()V

    goto/16 :goto_1

    .line 296
    :cond_e
    const/4 v5, 0x4

    if-ne p1, v5, :cond_2

    .line 297
    const-string v5, "updateUnlockMethodAndFinish : PASSWORD_QUALITY_FINGERPRINT_PLUS "

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 299
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/FingerprintManager;->getEnrolledFingers()I

    move-result v5

    if-nez v5, :cond_10

    .line 300
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/FingerprintManager;->notifyEnrollBegin()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 301
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 302
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v5, "com.android.settings"

    const-string v6, "com.android.settings.fingerprint.FingerprintDisclaimer"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    const-string v5, "fingerIndex"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 304
    const-string v5, "previousStage"

    const-string v6, "ChooseLockGeneric"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 306
    const-string v5, "startactivity : ChooseLockFingerprint "

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 307
    const/16 v5, 0x3f7

    invoke-virtual {p0, v1, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 309
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_f
    invoke-virtual {p0, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(I)V

    .line 310
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->finish()V

    goto/16 :goto_1

    .line 314
    :cond_10
    iget-boolean v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z

    if-nez v5, :cond_2

    .line 315
    iput-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z

    .line 316
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    invoke-virtual {v5}, Lcom/samsung/android/fingerprint/FingerprintManager;->notifyEnrollBegin()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 317
    const/16 v5, 0x1000

    invoke-virtual {p0, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(I)V

    .line 318
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->finish()V

    goto/16 :goto_1

    .line 320
    :cond_11
    invoke-virtual {p0, v11}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->setResult(I)V

    .line 321
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->finish()V

    goto/16 :goto_1
.end method
