.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setCallBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 1060
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 1063
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1064
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1066
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-boolean v1, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    if-eqz v1, :cond_1

    .line 1067
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v1, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v1

    if-nez v1, :cond_2

    .line 1068
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v3, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 1069
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->loadAppList()V

    .line 1075
    :cond_1
    :goto_0
    return-void

    .line 1071
    :cond_2
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$10;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {v0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    .line 1072
    .local v0, "task":Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
