.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$1;
.super Ljava/lang/Object;
.source "SetupWizardSecuritySettingActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$1;->this$1:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 374
    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I
    invoke-static {p2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$502(I)I

    .line 376
    const-string v0, "KnoxSetupWizardClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Timeout : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$1;->this$1:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutValues:[Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$600(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)[Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$500()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$1;->this$1:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setTimeoutText(I)V
    invoke-static {v0, p2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;I)V

    .line 379
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 380
    return-void
.end method
