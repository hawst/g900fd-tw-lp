.class public Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;
.super Ljava/lang/Object;
.source "KnoxShortcutUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil$PackageDeleteObserver;
    }
.end annotation


# static fields
.field public static ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

.field public static ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

.field private static DEFAUTLT_HOME_PACKAGE:Ljava/lang/String;

.field private static DEFAUTLT_HOME_PACKAGE2:Ljava/lang/String;

.field private static FIRST_PERSONA_ACTIVITY:Ljava/lang/String;

.field public static PERSONA_SHORTCUT:Ljava/lang/String;

.field private static SECOND_PERSONA_ACTIVITY:Ljava/lang/String;

.field private static instance:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;


# instance fields
.field private app_icon_height:I

.field private app_icon_width:I

.field private iPackageManager:Landroid/content/pm/IPackageManager;

.field knoxshortcutfolder:Landroid/content/SharedPreferences;

.field mContext:Landroid/content/Context;

.field mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

.field private mPM:Landroid/os/PersonaManager;

.field private mPersonaInfo:Landroid/content/pm/PersonaInfo;

.field private mPreviousVerifyAppsValue:I

.field private mRes:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-string v0, "android.intent.action.LAUNCH_PERSONA_SHORTCUT"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->PERSONA_SHORTCUT:Ljava/lang/String;

    .line 59
    const-string v0, "com.android.launcher.action.UNINSTALL_SHORTCUT"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

    .line 60
    const-string v0, "com.android.launcher.action.INSTALL_SHORTCUT"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

    .line 61
    const-string v0, "KnoxShortcutActivity"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->FIRST_PERSONA_ACTIVITY:Ljava/lang/String;

    .line 62
    const-string v0, "KnoxIIShortcutActivity"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->SECOND_PERSONA_ACTIVITY:Ljava/lang/String;

    .line 63
    const-string v0, "com.sec.android.app.launcher"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->DEFAUTLT_HOME_PACKAGE:Ljava/lang/String;

    .line 64
    const-string v0, "android"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->DEFAUTLT_HOME_PACKAGE2:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->instance:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPreviousVerifyAppsValue:I

    .line 76
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    .line 77
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPersonaInfo:Landroid/content/pm/PersonaInfo;

    .line 85
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    .line 86
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    .line 87
    const-string v0, "package"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->iPackageManager:Landroid/content/pm/IPackageManager;

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mRes:Landroid/content/res/Resources;

    .line 90
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f0b0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->app_icon_width:I

    .line 91
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mRes:Landroid/content/res/Resources;

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->app_icon_height:I

    .line 92
    return-void
.end method

.method private checkForExistingFolder(I)Ljava/lang/String;
    .locals 6
    .param p1, "personaId"    # I

    .prologue
    .line 237
    const-string v3, "KnoxShortcutUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " checkForExistingFolder personaId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v4, "ShortcutFolderName"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    .line 239
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 240
    .local v2, "keys":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    .line 241
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 242
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "knox1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 243
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 253
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v3

    .line 246
    :cond_1
    const/4 v3, 0x2

    if-ne p1, v3, :cond_3

    .line 247
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 248
    .restart local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "knox2"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 249
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 253
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;*>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static createBadgeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 28
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "badgeCount"    # I

    .prologue
    .line 766
    new-instance v13, Landroid/graphics/Paint;

    invoke-direct {v13}, Landroid/graphics/Paint;-><init>()V

    .line 767
    .local v13, "mPaint":Landroid/graphics/Paint;
    const/16 v24, 0x1

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 769
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    .line 770
    .local v18, "res":Landroid/content/res/Resources;
    const v24, 0x7f0a000d

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 771
    .local v21, "sFontSizeDefault":I
    const v24, 0x7f0a000e

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 773
    .local v22, "sFontSizeSmall":I
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 774
    const/16 v24, -0x1

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 775
    sget-object v24, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 777
    if-nez p1, :cond_0

    const/16 v24, 0x0

    .line 828
    :goto_0
    return-object v24

    .line 779
    :cond_0
    const/16 v24, 0x64

    move/from16 v0, p1

    move/from16 v1, v24

    if-lt v0, v1, :cond_3

    .line 780
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 785
    :goto_1
    const v24, 0x7f02000e

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v19

    .line 786
    .local v19, "sBadgeDrawable":Landroid/graphics/drawable/Drawable;
    new-instance v20, Landroid/graphics/Rect;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/Rect;-><init>()V

    .line 787
    .local v20, "sBoundsRect":Landroid/graphics/Rect;
    invoke-virtual/range {v19 .. v20}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 788
    move-object/from16 v0, v20

    iget v15, v0, Landroid/graphics/Rect;->left:I

    .line 789
    .local v15, "paddingLeft":I
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    .line 790
    .local v16, "paddingTop":I
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    add-int v17, v15, v24

    .line 791
    .local v17, "paddingWidth":I
    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    add-int v14, v16, v24

    .line 792
    .local v14, "paddingHeight":I
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v24

    sub-int v11, v24, v17

    .line 793
    .local v11, "defaultContentsWidth":I
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v24

    sub-int v10, v24, v14

    .line 795
    .local v10, "defaultContentsHeight":I
    const-string v24, "%d"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 796
    .local v5, "badgeText":Ljava/lang/String;
    move-object/from16 v6, v20

    .line 797
    .local v6, "bounds":Landroid/graphics/Rect;
    const/16 v24, 0x0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v13, v5, v0, v1, v6}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 802
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v24

    add-int/lit8 v9, v24, 0xa

    .line 803
    .local v9, "contentsWidth":I
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v8

    .line 804
    .local v8, "contentsHeight":I
    if-ge v9, v11, :cond_1

    .line 806
    move v9, v11

    .line 809
    :cond_1
    if-ge v8, v10, :cond_2

    .line 811
    sub-int v24, v10, v8

    div-int/lit8 v24, v24, 0x2

    add-int v16, v16, v24

    .line 812
    move v8, v10

    .line 814
    :cond_2
    add-int v23, v9, v17

    .line 815
    .local v23, "width":I
    add-int v12, v8, v14

    .line 817
    .local v12, "height":I
    sget-object v24, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v12, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 818
    .local v4, "b":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 820
    .local v7, "c":Landroid/graphics/Canvas;
    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 821
    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 827
    div-int/lit8 v24, v9, 0x2

    add-int v24, v24, v15

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    iget v0, v6, Landroid/graphics/Rect;->top:I

    move/from16 v25, v0

    sub-int v25, v16, v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v7, v5, v0, v1, v13}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 828
    new-instance v24, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FastBitmapDrawable;

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 782
    .end local v4    # "b":Landroid/graphics/Bitmap;
    .end local v5    # "badgeText":Ljava/lang/String;
    .end local v6    # "bounds":Landroid/graphics/Rect;
    .end local v7    # "c":Landroid/graphics/Canvas;
    .end local v8    # "contentsHeight":I
    .end local v9    # "contentsWidth":I
    .end local v10    # "defaultContentsHeight":I
    .end local v11    # "defaultContentsWidth":I
    .end local v12    # "height":I
    .end local v14    # "paddingHeight":I
    .end local v15    # "paddingLeft":I
    .end local v16    # "paddingTop":I
    .end local v17    # "paddingWidth":I
    .end local v19    # "sBadgeDrawable":Landroid/graphics/drawable/Drawable;
    .end local v20    # "sBoundsRect":Landroid/graphics/Rect;
    .end local v23    # "width":I
    :cond_3
    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    invoke-virtual {v13, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    goto/16 :goto_1
.end method

.method private createNewFolder(I)V
    .locals 5
    .param p1, "personaId"    # I

    .prologue
    .line 315
    const-string v2, "KnoxShortcutUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " createNewFolder personaId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->saveFolderName(I)V

    .line 317
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 319
    .local v0, "launchIntent":Landroid/content/Intent;
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->isFirstPersonaId(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 320
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0c00f3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getShortcutIntent(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 321
    .local v1, "shortcutintent":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.knox.knoxsetupwizardclient"

    const-string v4, "com.sec.knox.knoxsetupwizardclient.shortcut.ShortcutActivityI"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 326
    :goto_0
    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->ACTION_INSTALL_SHORTCUT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 327
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setCreateShortcutFlag(II)V

    .line 328
    const-string v2, "personaId"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 329
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 330
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 331
    return-void

    .line 323
    .end local v1    # "shortcutintent":Landroid/content/Intent;
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0c00f4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getShortcutIntent(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 324
    .restart local v1    # "shortcutintent":Landroid/content/Intent;
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.knox.knoxsetupwizardclient"

    const-string v4, "com.sec.knox.knoxsetupwizardclient.shortcut.ShortcutActivityII"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private deleteExistingFolder(ILjava/lang/String;)V
    .locals 5
    .param p1, "personaId"    # I
    .param p2, "folderToDelete"    # Ljava/lang/String;

    .prologue
    .line 257
    const-string v2, "KnoxShortcutUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " deleteExistingFolder personaId : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " folderToDelete : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getShortcutIntent(ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 259
    .local v1, "shortcutintent":Landroid/content/Intent;
    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->ACTION_UNINSTALL_SHORTCUT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setCreateShortcutFlag(II)V

    .line 261
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 262
    .local v0, "launchIntent":Landroid/content/Intent;
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->isFirstPersonaId(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 263
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.knox.knoxsetupwizardclient"

    const-string v4, "com.sec.knox.knoxsetupwizardclient.shortcut.ShortcutActivityI"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 268
    :goto_0
    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 269
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 270
    return-void

    .line 266
    :cond_0
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.sec.knox.knoxsetupwizardclient"

    const-string v4, "com.sec.knox.knoxsetupwizardclient.shortcut.ShortcutActivityII"

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private getCreateShortcutFlag(I)I
    .locals 5
    .param p1, "knox"    # I

    .prologue
    const/4 v1, 0x0

    .line 426
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v3, "shortcut"

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 427
    .local v0, "pref":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 429
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 431
    const-string v2, "createShortcuti"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 438
    :cond_0
    :goto_0
    return v1

    .line 435
    :cond_1
    const-string v2, "createShortcutii"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method private getCreateShortcutWidgetFlag(I)I
    .locals 5
    .param p1, "knox"    # I

    .prologue
    const/4 v1, 0x0

    .line 388
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v3, "shortcut"

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 389
    .local v0, "pref":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 391
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 393
    const-string v2, "createShortcutWidgeti"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 400
    :cond_0
    :goto_0
    return v1

    .line 397
    :cond_1
    const-string v2, "createShortcutWidgetii"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method private getDefaultHomeApplicationPackage()Ljava/lang/String;
    .locals 5

    .prologue
    .line 227
    const/4 v0, 0x0

    .line 228
    .local v0, "defaultHome":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 229
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.category.HOME"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/high16 v4, 0x10000

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 231
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v2, :cond_0

    .line 232
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 233
    :cond_0
    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->instance:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->instance:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    .line 100
    :cond_0
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->instance:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getShortcutIntent(ILjava/lang/String;)Landroid/content/Intent;
    .locals 6
    .param p1, "personaId"    # I
    .param p2, "folderName"    # Ljava/lang/String;

    .prologue
    .line 273
    const-string v3, "KnoxShortcutUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " getShortcutIntent personaId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " folderName : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 276
    .local v2, "shortcutintent":Landroid/content/Intent;
    if-nez p2, :cond_1

    .line 278
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->isFirstPersonaId(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 279
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v4, 0x7f0c00f3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 283
    .local v0, "fName":Ljava/lang/String;
    :goto_0
    const-string v3, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    .end local v0    # "fName":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->isFirstPersonaId(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 289
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v4, 0x7f02000c

    invoke-static {v3, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .line 293
    .local v1, "icon":Landroid/content/Intent$ShortcutIconResource;
    :goto_2
    const-string v3, "duplicate"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 294
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 295
    return-object v2

    .line 281
    .end local v1    # "icon":Landroid/content/Intent$ShortcutIconResource;
    :cond_0
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v4, 0x7f0c00f4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "fName":Ljava/lang/String;
    goto :goto_0

    .line 285
    .end local v0    # "fName":Ljava/lang/String;
    :cond_1
    const-string v3, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 291
    :cond_2
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v4, 0x7f02000d

    invoke-static {v3, v4}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v1

    .restart local v1    # "icon":Landroid/content/Intent$ShortcutIconResource;
    goto :goto_2
.end method

.method private isVerifierInstalled(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 140
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 141
    .local v0, "pm":Landroid/content/pm/PackageManager;
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.PACKAGE_NEEDS_VERIFICATION"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 142
    .local v2, "verification":Landroid/content/Intent;
    const-string v5, "application/vnd.android.package-archive"

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 144
    invoke-virtual {v0, v2, v4}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 145
    .local v1, "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method private processFolderCreation(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 334
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " processFolderCreation personaId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->checkForExistingFolder(I)Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "isFolderExists":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00f3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00f4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 340
    invoke-direct {p0, p1, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->deleteExistingFolder(ILjava/lang/String;)V

    .line 342
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->createNewFolder(I)V

    .line 343
    return-void
.end method

.method private resizeToDisplay(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p1, "din"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 843
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->app_icon_width:I

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->app_icon_height:I

    if-ne v2, v3, :cond_0

    .line 850
    .end local p1    # "din":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object p1

    .line 847
    .restart local p1    # "din":Landroid/graphics/drawable/Drawable;
    :cond_0
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "din":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 848
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mRes:Landroid/content/res/Resources;

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->app_icon_width:I

    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->app_icon_height:I

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .local v1, "dnew":Landroid/graphics/drawable/Drawable;
    move-object p1, v1

    .line 850
    goto :goto_0
.end method

.method private saveFolderName(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 299
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " saveFolderName personaId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 301
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v2, "ShortcutFolderName"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    .line 303
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 304
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_2

    .line 305
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    .line 306
    const-string v1, "knox1"

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0c00f3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 310
    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 312
    :cond_2
    return-void

    .line 307
    :cond_3
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 308
    const-string v1, "knox2"

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const v3, 0x7f0c00f4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private setEnableWidget(IZ)V
    .locals 0
    .param p1, "knox"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 510
    return-void
.end method

.method private setEnabledState(IZ)V
    .locals 4
    .param p1, "knox"    # I
    .param p2, "value"    # Z

    .prologue
    .line 513
    const/4 v0, 0x0

    .line 515
    .local v0, "userid":I
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setEnableWidget(IZ)V

    .line 516
    const/4 v1, 0x1

    if-ne p2, v1, :cond_0

    .line 518
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setVerifyAppFeatureDisable(Landroid/content/Context;)V

    .line 521
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->revertVerifyAppFeatureDisable(Landroid/content/Context;)V

    .line 529
    :goto_0
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkB2B() KNOX:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    return-void

    .line 525
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->removeAppShortcutWidgetToPersonalHome(I)V

    goto :goto_0
.end method


# virtual methods
.method public addContainer(I)V
    .locals 5
    .param p1, "userId"    # I

    .prologue
    const/4 v4, 0x0

    .line 176
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    if-nez v1, :cond_0

    .line 177
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v2, "persona"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPersonaInfo:Landroid/content/pm/PersonaInfo;

    .line 179
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getKnoxid(I)I

    move-result v0

    .line 180
    .local v0, "personaId":I
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " removeContainer :  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPersonaInfo:Landroid/content/pm/PersonaInfo;

    iget-boolean v1, v1, Landroid/content/pm/PersonaInfo;->isLightWeightContainer:Z

    if-eqz v1, :cond_1

    .line 182
    invoke-direct {p0, v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setEnabledState(IZ)V

    .line 188
    :goto_0
    return-void

    .line 184
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setEnabledState(IZ)V

    .line 185
    invoke-virtual {p0, v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setCreateShortcutFlag(II)V

    .line 186
    invoke-virtual {p0, v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setCreateShortcutWidgetFlag(II)V

    goto :goto_0
.end method

.method public addRemoveAppShortcutToPersonalHome(IZ)V
    .locals 5
    .param p1, "personaId"    # I
    .param p2, "add"    # Z

    .prologue
    .line 346
    const-string v2, "KnoxShortcutUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " addRemoveAppShortcutToPersonalHome :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const-string v2, "KnoxShortcutUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " add :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getDefaultHomeApplicationPackage()Ljava/lang/String;

    move-result-object v0

    .line 350
    .local v0, "homePackage":Ljava/lang/String;
    const-string v2, "KnoxShortcutUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "homePackage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getCreateShortcutWidgetFlag(I)I

    move-result v2

    if-nez v2, :cond_2

    if-eqz p2, :cond_2

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->DEFAUTLT_HOME_PACKAGE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->DEFAUTLT_HOME_PACKAGE2:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getCreateShortcutFlag(I)I

    move-result v2

    if-nez v2, :cond_2

    .line 356
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->processFolderCreation(I)V

    .line 363
    :cond_1
    :goto_0
    return-void

    .line 357
    :cond_2
    if-nez p2, :cond_1

    .line 358
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->checkForExistingFolder(I)Ljava/lang/String;

    move-result-object v1

    .line 359
    .local v1, "isFolderExists":Ljava/lang/String;
    invoke-direct {p0, p1, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->deleteExistingFolder(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public deleteShortctDB(I)V
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 837
    const-string v1, "NAVEEN"

    const-string v2, "deleteshortcutDB"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 838
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getKnoxid(I)I

    move-result v0

    .line 839
    .local v0, "personaId":I
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->deleteShortcuts(I)I

    .line 840
    return-void
.end method

.method public deleteShortcutFolderPref(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 192
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " deleteShortcutFolderPref :  personaID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 194
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v2, "ShortcutFolderName"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->knoxshortcutfolder:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 197
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_2

    .line 198
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    .line 199
    const-string v1, "knox1"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 203
    :cond_1
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 205
    :cond_2
    return-void

    .line 200
    :cond_3
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 201
    const-string v1, "knox2"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v5, 0x0

    .line 752
    instance-of v2, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_0

    .line 753
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 762
    :goto_0
    return-object v0

    .line 756
    .restart local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 758
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 759
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p1, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 760
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public getBadgedIcon(Landroid/content/pm/ResolveInfo;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;
    .locals 8
    .param p1, "rInfo"    # Landroid/content/pm/ResolveInfo;
    .param p2, "user"    # Landroid/os/UserHandle;

    .prologue
    .line 855
    invoke-virtual {p1}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    move-result v0

    .line 856
    .local v0, "iconRes":I
    const/4 v3, 0x0

    .line 857
    .local v3, "resources":Landroid/content/res/Resources;
    const/4 v1, 0x0

    .line 858
    .local v1, "originalIcon":Landroid/graphics/drawable/Drawable;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 859
    .local v2, "pm":Landroid/content/pm/PackageManager;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v4

    .line 862
    .local v4, "um":Landroid/os/UserManager;
    :try_start_0
    iget-object v5, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v5}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 864
    :try_start_1
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v3, v0, v5}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 870
    :goto_0
    if-nez v1, :cond_0

    .line 871
    iget-object v5, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v5, v2}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 874
    :cond_0
    instance-of v5, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_1

    .line 875
    invoke-direct {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->resizeToDisplay(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 876
    invoke-virtual {v4, v1, p2}, Landroid/os/UserManager;->getBadgedIconForUser(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 880
    :goto_1
    return-object v5

    .line 878
    :cond_1
    const-string v5, "KnoxShortcutUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unable to create badged icon for "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v1

    .line 880
    goto :goto_1

    .line 867
    :catch_0
    move-exception v5

    goto :goto_0

    .line 865
    :catch_1
    move-exception v5

    goto :goto_0
.end method

.method public getBadgedIcon(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p2, "user"    # Landroid/os/UserHandle;

    .prologue
    .line 884
    move-object v0, p1

    .line 885
    .local v0, "originalIcon":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/os/UserManager;->get(Landroid/content/Context;)Landroid/os/UserManager;

    move-result-object v1

    .line 887
    .local v1, "um":Landroid/os/UserManager;
    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->resizeToDisplay(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 888
    invoke-virtual {v1, v0, p2}, Landroid/os/UserManager;->getBadgedIconForUser(Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    return-object v2
.end method

.method public getIsShortcutMigrationFor_2_3_0_Done()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 892
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v3, "ShortcutInPersonalHome"

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 894
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 895
    const-string v2, "isShortcutMigrationFor_2_3_0_Done"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 897
    .local v0, "isShortcutMigrationDone":Z
    const-string v2, "KnoxShortcutUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getIsShortcutMigrationFor_2_3_0_Done "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    .end local v0    # "isShortcutMigrationDone":Z
    :goto_0
    return v0

    .line 900
    :cond_0
    const-string v2, "KnoxShortcutUtil"

    const-string v3, " pref is null "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getKnoxid(I)I
    .locals 7
    .param p1, "userId"    # I

    .prologue
    const/4 v5, 0x0

    .line 114
    :try_start_0
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    if-nez v4, :cond_0

    .line 115
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v6, "persona"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/PersonaManager;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    .line 116
    :cond_0
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v4, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPersonaInfo:Landroid/content/pm/PersonaInfo;

    .line 118
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v6, "user"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 119
    .local v3, "um":Landroid/os/UserManager;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPersonaInfo:Landroid/content/pm/PersonaInfo;

    iget v4, v4, Landroid/content/pm/PersonaInfo;->id:I

    invoke-virtual {v3, v4}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v2

    .line 121
    .local v2, "ui":Landroid/content/pm/UserInfo;
    const/4 v1, 0x0

    .line 122
    .local v1, "knoxName":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 123
    iget-object v1, v2, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 125
    :cond_1
    if-nez v1, :cond_2

    move v4, v5

    .line 135
    .end local v1    # "knoxName":Ljava/lang/String;
    .end local v2    # "ui":Landroid/content/pm/UserInfo;
    .end local v3    # "um":Landroid/os/UserManager;
    :goto_0
    return v4

    .line 127
    .restart local v1    # "knoxName":Ljava/lang/String;
    .restart local v2    # "ui":Landroid/content/pm/UserInfo;
    .restart local v3    # "um":Landroid/os/UserManager;
    :cond_2
    const-string v4, "KNOX"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 128
    const/4 v4, 0x1

    goto :goto_0

    .line 129
    :cond_3
    const-string v4, "KNOX II"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_4

    .line 130
    const/4 v4, 0x2

    goto :goto_0

    :cond_4
    move v4, v5

    .line 131
    goto :goto_0

    .line 133
    .end local v1    # "knoxName":Ljava/lang/String;
    .end local v2    # "ui":Landroid/content/pm/UserInfo;
    .end local v3    # "um":Landroid/os/UserManager;
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v4, v5

    .line 135
    goto :goto_0
.end method

.method public getPixelbyDp(I)I
    .locals 3
    .param p1, "dp"    # I

    .prologue
    .line 832
    const/4 v0, 0x1

    int-to-float v1, p1

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public isFirstPersonaId(I)Z
    .locals 1
    .param p1, "personaId"    # I

    .prologue
    const/4 v0, 0x1

    .line 717
    if-ne p1, v0, :cond_0

    .line 720
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAppShortcutWidgetToPersonalHome(I)V
    .locals 12
    .param p1, "personaId"    # I

    .prologue
    const/4 v11, 0x1

    .line 534
    const-string v8, "KnoxShortcutUtil"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " removeAppShortcutToPersonalHome :  "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    if-ne p1, v11, :cond_1

    .line 539
    const-string v4, "com.sec.knox.shortcuti"

    .line 547
    .local v4, "pkgName":Ljava/lang/String;
    :goto_0
    :try_start_0
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v9, "enterprise_policy"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 549
    .local v3, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseDeviceManager;->getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;

    move-result-object v0

    .line 550
    .local v0, "appPolicy":Landroid/app/enterprise/ApplicationPolicy;
    invoke-virtual {v0, v4}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationUninstallationEnabled(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 555
    .end local v0    # "appPolicy":Landroid/app/enterprise/ApplicationPolicy;
    .end local v3    # "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    :goto_1
    :try_start_1
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 556
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v8, 0x3

    new-array v7, v8, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-class v9, Landroid/content/pm/IPackageDeleteObserver;

    aput-object v9, v7, v8

    const/4 v8, 0x2

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    .line 560
    .local v7, "uninstalltypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const-string v9, "deletePackage"

    invoke-virtual {v8, v9, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 562
    .local v6, "uninstallmethod":Ljava/lang/reflect/Method;
    if-eqz v6, :cond_0

    .line 564
    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil$PackageDeleteObserver;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil$PackageDeleteObserver;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;)V

    .line 566
    .local v1, "deleteObserver":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil$PackageDeleteObserver;
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v8, v9

    const/4 v9, 0x1

    aput-object v1, v8, v9

    const/4 v9, 0x2

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v5, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 574
    .end local v1    # "deleteObserver":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil$PackageDeleteObserver;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "uninstallmethod":Ljava/lang/reflect/Method;
    .end local v7    # "uninstalltypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :cond_0
    :goto_2
    return-void

    .line 541
    .end local v4    # "pkgName":Ljava/lang/String;
    :cond_1
    const-string v4, "com.sec.knox.shortcutii"

    .restart local v4    # "pkgName":Ljava/lang/String;
    goto :goto_0

    .line 551
    :catch_0
    move-exception v2

    .line 552
    .local v2, "e1":Ljava/lang/SecurityException;
    :try_start_2
    const-string v8, "KnoxShortcutUtil"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SecurityException: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 570
    .end local v2    # "e1":Ljava/lang/SecurityException;
    :catch_1
    move-exception v8

    goto :goto_2
.end method

.method public removeContainer(I)V
    .locals 5
    .param p1, "userId"    # I

    .prologue
    const/4 v4, 0x0

    .line 209
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getKnoxid(I)I

    move-result v0

    .line 211
    .local v0, "personaId":I
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " removeContainer :  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    if-nez v1, :cond_0

    .line 213
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v2, "persona"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    .line 214
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPersonaInfo:Landroid/content/pm/PersonaInfo;

    .line 215
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPersonaInfo:Landroid/content/pm/PersonaInfo;

    iget-boolean v1, v1, Landroid/content/pm/PersonaInfo;->isLightWeightContainer:Z

    if-nez v1, :cond_1

    .line 216
    invoke-direct {p0, v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setEnabledState(IZ)V

    .line 217
    invoke-virtual {p0, v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->addRemoveAppShortcutToPersonalHome(IZ)V

    .line 218
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->deleteShortcuts(I)I

    .line 219
    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->deleteShortcutFolderPref(I)V

    .line 220
    invoke-virtual {p0, v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setCreateShortcutWidgetFlag(II)V

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v1, p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->removeShortcuts(I)I

    goto :goto_0
.end method

.method public revertVerifyAppFeatureDisable(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->isVerifierInstalled(Landroid/content/Context;)Z

    move-result v0

    .line 165
    .local v0, "bVerifierInstalled":Z
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVerifyAppFeatureDisable: isVerifierInstalled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    if-eqz v0, :cond_0

    .line 167
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "revertVerifyAppFeatureDisable: Previous Verifier value ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPreviousVerifyAppsValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPreviousVerifyAppsValue:I

    if-eqz v1, :cond_0

    .line 169
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPreviousVerifyAppsValue:I

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 172
    :cond_0
    return-void
.end method

.method public setCreateShortcutFlag(II)V
    .locals 5
    .param p1, "knox"    # I
    .param p2, "flag"    # I

    .prologue
    .line 405
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v3, "shortcut"

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 406
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 408
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 409
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 411
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 413
    const-string v2, "createShortcuti"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 419
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 422
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void

    .line 417
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    const-string v2, "createShortcutii"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public setCreateShortcutWidgetFlag(II)V
    .locals 5
    .param p1, "knox"    # I
    .param p2, "flag"    # I

    .prologue
    .line 367
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v3, "shortcut"

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 368
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 370
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 371
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v0, :cond_0

    .line 373
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 375
    const-string v2, "createShortcutWidgeti"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 381
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 384
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return-void

    .line 379
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    const-string v2, "createShortcutWidgetii"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method public setIsShortcutMigrationFor_2_3_0_Done(Z)Z
    .locals 6
    .param p1, "isShortcutMigrationDone"    # Z

    .prologue
    const/4 v2, 0x0

    .line 907
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    const-string v4, "ShortcutInPersonalHome"

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 909
    .local v1, "pref":Landroid/content/SharedPreferences;
    if-eqz v1, :cond_0

    .line 910
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 911
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "isShortcutMigrationFor_2_3_0_Done"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 912
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 913
    const-string v2, "KnoxShortcutUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " setIsShortcutMigrationFor_2_3_0_Done  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 915
    const/4 v2, 0x1

    .line 918
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_0
    return v2
.end method

.method public setVerifyAppFeatureDisable(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->isVerifierInstalled(Landroid/content/Context;)Z

    move-result v0

    .line 151
    .local v0, "bVerifierInstalled":Z
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVerifyAppFeatureDisable: isVerifierInstalled = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPreviousVerifyAppsValue:I

    .line 155
    const-string v1, "KnoxShortcutUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setVerifyAppFeatureDisable : current value is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPreviousVerifyAppsValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", set to 0"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->mPreviousVerifyAppsValue:I

    if-eqz v1, :cond_0

    .line 157
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 160
    :cond_0
    return-void
.end method

.method public updateWidgetStatus()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 725
    invoke-direct {p0, v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->deleteExistingFolder(ILjava/lang/String;)V

    .line 726
    invoke-direct {p0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->deleteExistingFolder(ILjava/lang/String;)V

    .line 727
    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->removeAppShortcutWidgetToPersonalHome(I)V

    .line 728
    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->removeAppShortcutWidgetToPersonalHome(I)V

    .line 729
    return-void
.end method

.method public visualizeIcon(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 733
    const/16 v6, 0x14

    .line 734
    .local v6, "margin":I
    div-int/lit8 v5, v6, 0x2

    .line 735
    .local v5, "halfMargin":I
    const/16 v4, 0xa

    .line 736
    .local v4, "glowRadius":I
    const/4 v3, -0x1

    .line 737
    .local v3, "glowColor":I
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 738
    .local v8, "src":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->extractAlpha()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 739
    .local v0, "alpha":Landroid/graphics/Bitmap;
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    add-int/2addr v9, v6

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    add-int/2addr v10, v6

    sget-object v11, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v10, v11}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 742
    .local v1, "bmp":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 743
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 744
    .local v7, "paint":Landroid/graphics/Paint;
    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 745
    const/4 v9, 0x1

    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 746
    new-instance v9, Landroid/graphics/BlurMaskFilter;

    int-to-float v10, v4

    sget-object v11, Landroid/graphics/BlurMaskFilter$Blur;->OUTER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v9, v10, v11}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v7, v9}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 747
    int-to-float v9, v5

    int-to-float v10, v5

    invoke-virtual {v2, v0, v9, v10, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 748
    int-to-float v9, v5

    int-to-float v10, v5

    const/4 v11, 0x0

    invoke-virtual {v2, v8, v9, v10, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 749
    return-object v1
.end method
