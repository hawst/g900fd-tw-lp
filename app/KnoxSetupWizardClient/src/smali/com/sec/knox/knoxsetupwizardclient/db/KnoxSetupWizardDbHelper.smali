.class public Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;
.super Ljava/lang/Object;
.source "KnoxSetupWizardDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;
    }
.end annotation


# static fields
.field private static instance:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;


# instance fields
.field allField:[Ljava/lang/String;

.field public allShortcutList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;",
            ">;"
        }
    .end annotation
.end field

.field destListIndex:I

.field private final mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;

.field sourceListIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allField:[Ljava/lang/String;

    .line 128
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mContext:Landroid/content/Context;

    .line 129
    return-void
.end method

.method public static alterTable3to4(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 103
    const-string v0, "ALTER TABLE shortcuts ADD COLUMN badgeCount INTEGER"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public static createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 111
    const-string v0, "CREATE TABLE shortcuts (_id INTEGER PRIMARY KEY AUTOINCREMENT,personaId INTEGER,packageName TEXT,shortcutName TEXT,intent TEXT,contentIntent TEXT,commandType TEXT,uri TEXT,listOrder INTEGER, icon BLOB, badgeCount INTEGER)"

    .line 125
    .local v0, "createConferenceTableSQL":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public static dropTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 108
    const-string v0, "DROP TABLE IF EXISTS shortcuts"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method private getAllShortcutFromDb(JJ)V
    .locals 19
    .param p1, "sourceID"    # J
    .param p3, "destID"    # J

    .prologue
    .line 393
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 395
    :cond_0
    move-wide/from16 v16, p1

    .line 396
    .local v16, "source":J
    move-wide/from16 v14, p3

    .line 397
    .local v14, "dest":J
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    .line 398
    const/4 v11, 0x0

    .line 400
    .local v11, "currentOrder":I
    const/4 v10, 0x0

    .line 403
    .local v10, "c":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "shortcuts"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "listOrder"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 405
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 406
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getShortcutInfoforReorder(Landroid/database/Cursor;)Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    move-result-object v12

    .line 407
    .local v12, "currentShortCutModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v11}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->setShortCutOrder(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;I)V

    .line 409
    iget v2, v12, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->_id:I

    int-to-long v2, v2

    cmp-long v2, v2, v16

    if-nez v2, :cond_1

    .line 410
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sourceListIndex:I

    .line 411
    :cond_1
    iget v2, v12, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->_id:I

    int-to-long v2, v2

    cmp-long v2, v2, v14

    if-nez v2, :cond_2

    .line 412
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->destListIndex:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 418
    .end local v12    # "currentShortCutModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :cond_3
    if-eqz v10, :cond_4

    .line 419
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 424
    :cond_4
    :goto_1
    return-void

    .line 415
    :catch_0
    move-exception v2

    .line 418
    if-eqz v10, :cond_4

    .line 419
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 418
    :catchall_0
    move-exception v2

    if-eqz v10, :cond_5

    .line 419
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    if-nez v0, :cond_0

    .line 140
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    .line 141
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->open()Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    .line 143
    :cond_0
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    return-object v0
.end method

.method private reOrderList()V
    .locals 4

    .prologue
    .line 434
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sourceListIndex:I

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->destListIndex:I

    if-ge v1, v2, :cond_0

    .line 435
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sourceListIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->destListIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    iget v2, v2, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    iput v2, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    .line 437
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sourceListIndex:I

    add-int/lit8 v0, v1, 0x1

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->destListIndex:I

    if-gt v0, v1, :cond_1

    .line 438
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    iget v2, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    .line 437
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 440
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sourceListIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->destListIndex:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    iget v2, v2, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    iput v2, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    .line 442
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->destListIndex:I

    .restart local v0    # "i":I
    :goto_1
    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sourceListIndex:I

    if-ge v0, v1, :cond_1

    .line 443
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    iget v2, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    .line 442
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 446
    :cond_1
    return-void
.end method

.method private setShortCutOrder(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;I)V
    .locals 0
    .param p1, "shortCutModel"    # Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    .param p2, "order"    # I

    .prologue
    .line 427
    iput p2, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    .line 428
    return-void
.end method

.method private updateShortCutDb()V
    .locals 7

    .prologue
    .line 450
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 451
    .local v2, "values":Landroid/content/ContentValues;
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 452
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->allShortcutList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    .line 454
    .local v1, "shortModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    const-string v3, "listOrder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "shortcuts"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->_id:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 459
    .end local v1    # "shortModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :cond_0
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 461
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 462
    return-void
.end method


# virtual methods
.method public checkDuplicatedShortcut(ILcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;)J
    .locals 13
    .param p1, "personaId"    # I
    .param p2, "model"    # Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    .prologue
    .line 333
    const/4 v12, 0x0

    .line 334
    .local v12, "mCursor":Landroid/database/Cursor;
    const-wide/16 v10, 0x0

    .line 335
    .local v10, "dbId":J
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x1

    const-string v2, "shortcuts"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "personaId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p2, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "uri"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->uri:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 340
    if-eqz v12, :cond_1

    .line 341
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 342
    const/4 v0, 0x0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v10, v0

    .line 344
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 346
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-eqz v0, :cond_2

    .line 347
    const-wide/16 v10, -0x1

    .line 348
    .end local v10    # "dbId":J
    :cond_2
    return-wide v10
.end method

.method public createShortcut(Landroid/content/ContentValues;)J
    .locals 3
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "shortcuts"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public createShortcut(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;)J
    .locals 8
    .param p1, "mModel"    # Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    .prologue
    .line 206
    const-wide/16 v2, 0x0

    .line 208
    .local v2, "rowId":J
    if-nez p1, :cond_0

    .line 209
    const-wide/16 v4, 0x0

    .line 229
    :goto_0
    return-wide v4

    .line 212
    :cond_0
    iget v1, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-virtual {p0, v1, p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->checkDuplicatedShortcut(ILcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_1

    .line 214
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 216
    .local v0, "initialValues":Landroid/content/ContentValues;
    const-string v1, "personaId"

    iget v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 217
    const-string v1, "packageName"

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v1, "shortcutName"

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v1, "intent"

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->intent:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v1, "contentIntent"

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->contentIntent:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v1, "commandType"

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->commandType:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const-string v1, "uri"

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->uri:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string v1, "listOrder"

    iget v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 224
    iget-object v1, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->writeBitmap(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V

    .line 225
    const-string v1, "badgeCount"

    iget v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->badgeCount:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 226
    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->createShortcut(Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 227
    iget v1, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sendDBChangeNoti(I)V

    .end local v0    # "initialValues":Landroid/content/ContentValues;
    :cond_1
    move-wide v4, v2

    .line 229
    goto :goto_0
.end method

.method public deleteShortcut(IJ)I
    .locals 6
    .param p1, "personaId"    # I
    .param p2, "rowId"    # J

    .prologue
    .line 167
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "shortcuts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "personaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 169
    .local v0, "result":I
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sendDBChangeNoti(I)V

    .line 170
    return v0
.end method

.method public deleteShortcuts(I)I
    .locals 5
    .param p1, "personaId"    # I

    .prologue
    .line 151
    const/4 v0, 0x0

    .line 152
    .local v0, "rows":I
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "shortcuts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "personaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 153
    return v0
.end method

.method public fetchAllShortcuts()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 162
    const-string v0, "KnoxSetupWizardDbHelper"

    const-string v1, " fetch all shortcuts "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "shortcuts"

    const-string v7, "listOrder"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public fetchAllShortcuts(I)Landroid/database/Cursor;
    .locals 8
    .param p1, "personaId"    # I

    .prologue
    const/4 v2, 0x0

    .line 156
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "shortcuts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "personaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "listOrder"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method flattenBitmap(Landroid/graphics/Bitmap;)[B
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 354
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    mul-int/lit8 v2, v3, 0x4

    .line 355
    .local v2, "size":I
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 357
    .local v1, "out":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {p1, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 358
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->flush()V

    .line 359
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 360
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 363
    :goto_0
    return-object v3

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "Favorite"

    const-string v4, "Could not write icon"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    const/4 v3, 0x0

    goto :goto_0
.end method

.method getBitmap([B)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "byteArray"    # [B

    .prologue
    .line 376
    const/4 v1, 0x0

    array-length v2, p1

    invoke-static {p1, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 377
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public getShortcutInfo(Landroid/database/Cursor;)Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 275
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    invoke-direct {v0}, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;-><init>()V

    .line 277
    .local v0, "model":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->_id:I

    .line 278
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    .line 279
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    .line 280
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    .line 281
    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->intent:Ljava/lang/String;

    .line 282
    const/4 v1, 0x5

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->contentIntent:Ljava/lang/String;

    .line 283
    const/4 v1, 0x6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->commandType:Ljava/lang/String;

    .line 284
    const/4 v1, 0x7

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->uri:Ljava/lang/String;

    .line 285
    const/16 v1, 0x8

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    .line 286
    const/16 v1, 0x9

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getBitmap([B)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->icon:Landroid/graphics/Bitmap;

    .line 287
    const/16 v1, 0xa

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->badgeCount:I

    .line 289
    return-object v0
.end method

.method public getShortcutInfoforReorder(Landroid/database/Cursor;)Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 293
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    invoke-direct {v0}, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;-><init>()V

    .line 295
    .local v0, "model":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->_id:I

    .line 296
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    .line 297
    const/16 v1, 0x8

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->order:I

    .line 299
    return-object v0
.end method

.method public modifyShortcut(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;Z)I
    .locals 12
    .param p1, "mModel"    # Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    .param p2, "isLwc"    # Z

    .prologue
    const/4 v2, 0x0

    .line 233
    const/4 v10, -0x1

    .line 236
    .local v10, "result":I
    const/4 v3, 0x0

    .line 239
    .local v3, "sq":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "personaId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "shortcutName"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 244
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "shortcuts"

    const-string v7, "_id"

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 248
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_1

    .line 252
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    invoke-virtual {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getShortcutInfo(Landroid/database/Cursor;)Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    move-result-object v11

    .line 259
    .local v11, "tModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 260
    .local v9, "modifyValues":Landroid/content/ContentValues;
    const-string v0, "badgeCount"

    iget v1, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->badgeCount:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 262
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "shortcuts"

    invoke-virtual {v0, v1, v9, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v10

    .line 263
    iget v0, v11, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sendDBChangeNoti(I)V

    .line 268
    .end local v9    # "modifyValues":Landroid/content/ContentValues;
    .end local v11    # "tModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 271
    :cond_1
    return v10
.end method

.method public open()Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/SQLException;
        }
    .end annotation

    .prologue
    .line 131
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;-><init>(Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;

    .line 132
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 133
    return-object p0
.end method

.method public reOrderAfterDrop(JJ)V
    .locals 5
    .param p1, "sourceID"    # J
    .param p3, "destID"    # J

    .prologue
    .line 382
    const-string v0, "KnoxSetupWizardDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getAllShortcutFromDb(JJ)V

    .line 384
    const-string v0, "KnoxSetupWizardDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->reOrderList()V

    .line 386
    const-string v0, "KnoxSetupWizardDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->updateShortCutDb()V

    .line 388
    const-string v0, "KnoxSetupWizardDbHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Time:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    return-void
.end method

.method public removeShortcut(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;)I
    .locals 5
    .param p1, "mModel"    # Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    .prologue
    .line 174
    if-nez p1, :cond_0

    .line 175
    const/4 v0, 0x0

    .line 183
    :goto_0
    return v0

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "shortcuts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "personaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "packageName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "shortcutName"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 182
    .local v0, "result":I
    iget v1, p1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sendDBChangeNoti(I)V

    goto :goto_0
.end method

.method public removeShortcuts(I)I
    .locals 5
    .param p1, "userId"    # I

    .prologue
    .line 189
    if-nez p1, :cond_0

    .line 190
    const/4 v0, 0x0

    .line 196
    :goto_0
    return v0

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "shortcuts"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "personaId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 195
    .local v0, "result":I
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->sendDBChangeNoti(I)V

    goto :goto_0
.end method

.method public sendDBChangeNoti(I)V
    .locals 0
    .param p1, "personaId"    # I

    .prologue
    .line 324
    return-void
.end method

.method public writeBitmap(Landroid/content/ContentValues;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 368
    if-eqz p2, :cond_0

    .line 369
    invoke-virtual {p0, p2}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->flattenBitmap(Landroid/graphics/Bitmap;)[B

    move-result-object v0

    .line 370
    .local v0, "data":[B
    const-string v1, "icon"

    invoke-virtual {p1, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 372
    .end local v0    # "data":[B
    :cond_0
    return-void
.end method
