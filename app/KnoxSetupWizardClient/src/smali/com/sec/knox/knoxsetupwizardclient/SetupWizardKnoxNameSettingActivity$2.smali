.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;
.super Ljava/lang/Object;
.source "SetupWizardKnoxNameSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 298
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "next button clicked"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.musicservicecommand"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 302
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "command"

    const-string v2, "pause"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 303
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-virtual {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 306
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->btn_next:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 308
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->lockRotation()V
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V

    .line 309
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 310
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    const/4 v3, 0x0

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->createPersona(Landroid/content/Context;Z)V
    invoke-static {v1, v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;Landroid/content/Context;Z)V

    .line 312
    :cond_0
    return-void
.end method
