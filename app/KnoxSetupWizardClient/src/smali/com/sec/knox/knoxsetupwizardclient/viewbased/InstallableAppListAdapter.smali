.class public Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;
.super Landroid/widget/BaseAdapter;
.source "InstallableAppListAdapter.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private appList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;


# direct methods
.method public constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;Ljava/util/List;)V
    .locals 0
    .param p1, "context"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .line 36
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    .line 37
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 51
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSelectedItemCount()I
    .locals 6

    .prologue
    .line 150
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 151
    .local v2, "size":I
    const/4 v3, 0x0

    .line 154
    .local v3, "totalCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 155
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 156
    .local v1, "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 157
    add-int/lit8 v3, v3, 0x1

    .line 154
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    .end local v1    # "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :cond_1
    return v3
.end method

.method public getSelectedItemList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v2, "rList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 139
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 140
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 141
    .local v1, "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 142
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 146
    .end local v1    # "kInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    :cond_1
    return-object v2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    .line 65
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 67
    .local v0, "app":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    if-nez p2, :cond_1

    .line 69
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 70
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f03000b

    invoke-virtual {v4, v6, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .end local p2    # "convertView":Landroid/view/View;
    iput-object p2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    .line 77
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .restart local p2    # "convertView":Landroid/view/View;
    :goto_0
    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v7, 0x7f0f005c

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 78
    .local v3, "icon":Landroid/widget/ImageView;
    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 80
    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v7, 0x7f0f0060

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 81
    .local v1, "appName":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    iget-boolean v6, v6, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->usingWhiteTheme:Z

    if-eqz v6, :cond_0

    .line 83
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090003

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 86
    :cond_0
    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    const v7, 0x7f0f005d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 87
    .local v5, "selectorIcon":Landroid/widget/ImageView;
    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 91
    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    .line 92
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020034

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 96
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    :goto_1
    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 99
    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 101
    iget-object v6, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    return-object v6

    .line 74
    .end local v1    # "appName":Landroid/widget/TextView;
    .end local v2    # "d":Landroid/graphics/drawable/Drawable;
    .end local v3    # "icon":Landroid/widget/ImageView;
    .end local v5    # "selectorIcon":Landroid/widget/ImageView;
    :cond_1
    iput-object p2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    goto :goto_0

    .line 94
    .restart local v1    # "appName":Landroid/widget/TextView;
    .restart local v3    # "icon":Landroid/widget/ImageView;
    .restart local v5    # "selectorIcon":Landroid/widget/ImageView;
    :cond_2
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020033

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .restart local v2    # "d":Landroid/graphics/drawable/Drawable;
    goto :goto_1
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 107
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 108
    .local v1, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 110
    .local v0, "action":I
    const/4 v4, 0x2

    if-eq v0, v4, :cond_2

    .line 112
    if-ne v0, v2, :cond_0

    .line 114
    iget v4, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    if-eq v4, v2, :cond_0

    .line 115
    invoke-virtual {p1, v3}, Landroid/view/View;->playSoundEffect(I)V

    .line 116
    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIsSelected()I

    move-result v4

    if-ne v4, v2, :cond_1

    .line 117
    invoke-virtual {v1, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIsSelected(I)V

    .line 122
    :goto_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 123
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-virtual {v3}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->invalidateView()V

    .line 130
    :cond_0
    :goto_1
    return v2

    .line 119
    :cond_1
    invoke-virtual {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIsSelected(I)V

    goto :goto_0

    :cond_2
    move v2, v3

    .line 130
    goto :goto_1
.end method

.method public setAppSelectionStatus(I)V
    .locals 2
    .param p1, "status"    # I

    .prologue
    .line 56
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->appList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    invoke-virtual {v1, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->setIsSelected(I)V

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method
