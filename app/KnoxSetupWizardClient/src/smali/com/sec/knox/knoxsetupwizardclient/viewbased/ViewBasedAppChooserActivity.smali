.class public Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;
.super Landroid/app/Activity;
.source "ViewBasedAppChooserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;
    }
.end annotation


# static fields
.field static MACTIVITY:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;


# instance fields
.field private allAppsSelected:I

.field private defaultbg:Landroid/graphics/drawable/Drawable;

.field private dialog:Landroid/app/ProgressDialog;

.field private icr:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCurrentUserId:I

.field private mDeselectAll:Landroid/widget/TextView;

.field private mDivider:Landroid/widget/ImageView;

.field private mDone:Landroid/widget/TextView;

.field private mFilter:Landroid/content/IntentFilter;

.field private mGridMain:Landroid/widget/GridView;

.field private mPopUp:Landroid/widget/PopupWindow;

.field private mSelectAll:Landroid/widget/TextView;

.field private pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

.field private presseBg:Landroid/graphics/drawable/Drawable;

.field private selection:Landroid/widget/TextView;

.field public usingWhiteTheme:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->allAppsSelected:I

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->usingWhiteTheme:Z

    .line 230
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->presseBg:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mSelectAll:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDeselectAll:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDivider:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private getInstallComponentList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 192
    .local v0, "app":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 193
    .local v1, "component":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 194
    const/4 v4, 0x1

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 195
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    .end local v0    # "app":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v1    # "component":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    return-object v3
.end method

.method private getPopupDisplay()Landroid/widget/PopupWindow;
    .locals 6

    .prologue
    const/4 v5, -0x2

    .line 203
    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 204
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 206
    .local v2, "view":Landroid/view/View;
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-direct {v1, v2, v5, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    .line 208
    .local v1, "popupWindow":Landroid/widget/PopupWindow;
    const v3, 0x7f0f0076

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mSelectAll:Landroid/widget/TextView;

    .line 209
    const v3, 0x7f0f0078

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDeselectAll:Landroid/widget/TextView;

    .line 210
    const v3, 0x7f0f0077

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDivider:Landroid/widget/ImageView;

    .line 212
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mSelectAll:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDeselectAll:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 217
    return-object v1
.end method

.method private setDoneBtnStatus()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getSelectedItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDone:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 271
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDone:Landroid/widget/TextView;

    const v1, -0xbbbbbc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 276
    :goto_0
    return-void

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDone:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 274
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDone:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private setSpinnerItems(I)V
    .locals 5
    .param p1, "count"    # I

    .prologue
    const/4 v4, 0x0

    .line 179
    const v1, 0x7f0c00f7

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "selectedCount":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->defaultbg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 182
    iput v4, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->allAppsSelected:I

    .line 184
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 186
    :cond_0
    return-void
.end method


# virtual methods
.method public invalidateView()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->refreshDrawableState()V

    .line 223
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->notifyDataSetChanged()V

    .line 224
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 226
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getSelectedItemCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->setSpinnerItems(I)V

    .line 227
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->setDoneBtnStatus()V

    .line 228
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0c00f7

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 142
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->defaultbg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 143
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0f001a

    if-ne v1, v2, :cond_2

    .line 145
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->dialog:Landroid/app/ProgressDialog;

    const v2, 0x7f0c0101

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 147
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 150
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "knox.lwc.action.INSTALL_APK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 151
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "from_app"

    const-string v2, "KnoxSetupWizardClient"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    const-string v1, "userid"

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mCurrentUserId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 153
    const-string v1, "packages"

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getSelectedItemList()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getInstallComponentList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 154
    new-instance v1, Landroid/os/UserHandle;

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mCurrentUserId:I

    invoke-direct {v1, v2}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 176
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 156
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0f0076

    if-ne v1, v2, :cond_3

    .line 158
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v1, v7}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->setAppSelectionStatus(I)V

    .line 159
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getSelectedItemCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iput v7, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->allAppsSelected:I

    .line 162
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 163
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->notifyDataSetChanged()V

    .line 164
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->setDoneBtnStatus()V

    goto :goto_0

    .line 166
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0f0078

    if-ne v1, v2, :cond_1

    .line 168
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v1, v6}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->setAppSelectionStatus(I)V

    .line 169
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "   "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->allAppsSelected:I

    .line 172
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 173
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->notifyDataSetChanged()V

    .line 174
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->setDoneBtnStatus()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 75
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 76
    sput-object p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->MACTIVITY:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    .line 77
    const v2, 0x7f03001f

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->setContentView(I)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 80
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "userid"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mCurrentUserId:I

    .line 82
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    iget v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mCurrentUserId:I

    invoke-direct {v2, p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    .line 83
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->pkgInstall:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->getPackagesToInstall()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->list:Ljava/util/List;

    .line 85
    const v2, 0x7f0f001c

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mGridMain:Landroid/widget/GridView;

    .line 87
    const v2, 0x7f0f001a

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDone:Landroid/widget/TextView;

    .line 88
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDone:Landroid/widget/TextView;

    const v3, -0xbbbbbc

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 89
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->defaultbg:Landroid/graphics/drawable/Drawable;

    .line 90
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200b3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->presseBg:Landroid/graphics/drawable/Drawable;

    .line 91
    const v2, 0x7f0f0015

    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    .line 92
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->defaultbg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 93
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getPopupDisplay()Landroid/widget/PopupWindow;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;

    .line 94
    invoke-direct {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->setSpinnerItems(I)V

    .line 95
    iget-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->usingWhiteTheme:Z

    if-eqz v2, :cond_0

    .line 96
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setBackgroundColor(I)V

    .line 99
    :cond_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->dialog:Landroid/app/ProgressDialog;

    .line 101
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDone:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;-><init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;)V

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->icr:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;

    .line 132
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 133
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "knox.lwc.action.APP_INSTALLED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->icr:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->list:Ljava/util/List;

    invoke-direct {v2, p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;-><init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;Ljava/util/List;)V

    iput-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    .line 137
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mGridMain:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 246
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 247
    const-string v0, "TEST"

    const-string v1, "VBADAP : onDestroy();"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->icr:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$InstallCompleteReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 249
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 254
    const-string v1, "TEST"

    const-string v2, "VBACA : onKeyDown "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 256
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->finish()V

    .line 257
    const-string v1, "TEST"

    const-string v2, "VBACA : KEYCODE_BACK "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 259
    .local v0, "fromAddApp":Landroid/content/Intent;
    const-string v1, "knox.lwc.action.ADD_APP_CALLBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 260
    const-string v1, "userid"

    iget v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mCurrentUserId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 261
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 263
    .end local v0    # "fromAddApp":Landroid/content/Intent;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 311
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 312
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 313
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 281
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 282
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mFilter:Landroid/content/IntentFilter;

    .line 283
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 305
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 306
    return-void
.end method
