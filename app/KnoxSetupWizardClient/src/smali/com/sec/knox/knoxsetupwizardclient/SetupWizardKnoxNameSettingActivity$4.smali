.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$4;
.super Ljava/lang/Object;
.source "SetupWizardKnoxNameSettingActivity.java"

# interfaces
.implements Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->createPersona(Landroid/content/Context;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V
    .locals 0

    .prologue
    .line 466
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreationComplete(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 470
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 471
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "creationResult"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 472
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->initialiseHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 473
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 474
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->initialiseHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 475
    return-void
.end method
