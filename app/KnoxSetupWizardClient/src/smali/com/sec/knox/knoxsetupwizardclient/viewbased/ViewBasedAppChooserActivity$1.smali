.class Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;
.super Ljava/lang/Object;
.source "ViewBasedAppChooserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 105
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$100(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->presseBg:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 106
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getSelectedItemCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 108
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mSelectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDeselectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDivider:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$500(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 121
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$600(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    .line 122
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$600(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->selection:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$100(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, -0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 125
    :goto_1
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getSelectedItemCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mSelectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDeselectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDivider:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$500(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/InstallableAppListAdapter;->getSelectedItemCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mSelectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDeselectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mDivider:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$500(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 124
    :cond_3
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->access$600(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_1
.end method
