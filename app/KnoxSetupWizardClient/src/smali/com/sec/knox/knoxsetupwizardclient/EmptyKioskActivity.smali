.class public Lcom/sec/knox/knoxsetupwizardclient/EmptyKioskActivity;
.super Landroid/app/Activity;
.source "EmptyKioskActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 16
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const-string v0, "EmptyKioskActivity"

    const-string v1, "Not allowed to launch the activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 19
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/EmptyKioskActivity;->finish()V

    .line 20
    return-void
.end method
