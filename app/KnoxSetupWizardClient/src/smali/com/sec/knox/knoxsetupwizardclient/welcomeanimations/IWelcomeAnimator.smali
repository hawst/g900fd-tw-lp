.class public interface abstract Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;
.super Ljava/lang/Object;
.source "IWelcomeAnimator.java"


# virtual methods
.method public abstract initAnimations(Landroid/content/Context;)V
.end method

.method public abstract showBackgroundImg()V
.end method

.method public abstract showEulaWithoutAnimation()V
.end method

.method public abstract startBackAnimation()V
.end method

.method public abstract startDummyPanelFadeOut()V
.end method

.method public abstract startEulaAnimation()V
.end method

.method public abstract startHideWelcomeAnimation()V
.end method

.method public abstract startShowWelcomeAnimation()V
.end method
