.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;
.super Landroid/os/Handler;
.source "SetupWizardKnoxNameSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x0

    .line 489
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 532
    :goto_0
    :pswitch_0
    return-void

    .line 493
    :pswitch_1
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->lockStatusBar(Z)V
    invoke-static {v4, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$500(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;Z)V

    .line 494
    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->isCreationInProgress:Z
    invoke-static {v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$602(Z)Z

    .line 495
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->wakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 497
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mIsDestroyed:Z
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$800(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 498
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->dismiss()V

    .line 501
    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "creationResult"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 502
    .local v2, "personaId":I
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 504
    .local v1, "i":Landroid/content/Intent;
    const/16 v4, 0x64

    if-lt v2, v4, :cond_2

    .line 505
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 506
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v3

    .line 507
    .local v3, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    if-eqz v3, :cond_1

    instance-of v4, v3, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    if-nez v4, :cond_1

    .line 509
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->mPersona:Landroid/os/PersonaManager;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$900(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/PersonaManager;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/os/PersonaManager;->launchPersonaHome(I)Z

    .line 513
    :cond_1
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->initialiseHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5$1;

    invoke-direct {v5, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 522
    .end local v3    # "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    :cond_2
    const-string v4, "CREATE_PERSONA_FAIL"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 523
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-virtual {v4, v7, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->setResult(ILandroid/content/Intent;)V

    .line 524
    const-string v4, "KnoxSetupWizardClient"

    const-string v5, "Creation failure"

    invoke-static {v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 528
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "personaId":I
    :pswitch_2
    const-string v4, "Creation Success, KnoxNameSettings is finished"

    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 529
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    const-string v5, "activity"

    invoke-virtual {v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 530
    .local v0, "activityManager":Landroid/app/ActivityManager;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$5;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-virtual {v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 489
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
