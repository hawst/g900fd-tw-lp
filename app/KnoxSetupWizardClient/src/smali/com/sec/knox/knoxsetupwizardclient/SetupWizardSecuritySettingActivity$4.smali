.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;
.super Ljava/lang/Object;
.source "SetupWizardSecuritySettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->setLockTimeoutButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 360
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeoutDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    if-nez v2, :cond_2

    .line 362
    const/4 v1, 0x4

    .line 363
    .local v1, "dialogTheme":I
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isDarkTheme:Z
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->isPhone:Z
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 364
    :cond_0
    const/4 v1, 0x5

    .line 367
    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    invoke-direct {v0, v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 368
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    const v3, 0x7f0c003e

    invoke-virtual {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 369
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 370
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->revisedEntries:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/CharSequence;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeOutIndex:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$500()I

    move-result v3

    new-instance v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$1;

    invoke-direct {v4, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;)V

    invoke-virtual {v0, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 383
    const v2, 0x7f0c0041

    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$2;

    invoke-direct {v3, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 392
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v3

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeoutDialog:Landroid/app/AlertDialog;
    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$202(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "dialogTheme":I
    :goto_0
    monitor-exit p0

    return-void

    .line 394
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity$4;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->timeoutDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
