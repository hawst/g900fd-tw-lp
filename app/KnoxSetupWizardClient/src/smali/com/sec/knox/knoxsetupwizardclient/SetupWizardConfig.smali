.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;
.super Ljava/lang/Object;
.source "SetupWizardConfig.java"


# static fields
.field public static final excludedComponents:[[Ljava/lang/String;

.field public static final excludedComponents_noncom:[[Ljava/lang/String;

.field public static final excludedPackages:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 28
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.android.settings"

    aput-object v1, v0, v4

    const-string v1, "com.sec.knox.knoxsetupwizardclient"

    aput-object v1, v0, v5

    const-string v1, "com.sec.knox.containeragent2"

    aput-object v1, v0, v6

    const-string v1, "com.android.mms"

    aput-object v1, v0, v3

    const-string v1, "com.sec.knox.setupwizardstub"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "com.sec.chaton"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.sec.pcw"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.google.android.gm"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.google.android.apps.docs"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.google.android.talk"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedPackages:[Ljava/lang/String;

    .line 44
    const/16 v0, 0x12

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.sec.android.gallery3d"

    aput-object v2, v1, v4

    const-string v2, "com.sec.android.gallery3d.app.LockScreen"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.sec.android.gallery3d"

    aput-object v2, v1, v4

    const-string v2, "com.sec.android.gallery3d.app.BothScreen"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.contacts"

    aput-object v2, v1, v4

    const-string v2, "com.sec.android.app.contacts.RecntcallEntryActivity"

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.contacts"

    aput-object v2, v1, v4

    const-string v2, "com.android.dialer.DialtactsActivity"

    aput-object v2, v1, v5

    aput-object v1, v0, v3

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.settings"

    aput-object v2, v1, v4

    const-string v2, "com.android.settings.dualsimwidget.DualsimWidget"

    aput-object v2, v1, v5

    aput-object v1, v0, v7

    const/4 v1, 0x5

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.powersavingmode.ConnectivityLocationWidgetProvider"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.google.android.gm"

    aput-object v3, v2, v4

    const-string v3, "com.google.android.gm.widget.GoogleMailWidgetProvider"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.google.android.gm"

    aput-object v3, v2, v4

    const-string v3, "com.google.android.gm.CreateShortcutActivityGoogleMail"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.nfc"

    aput-object v3, v2, v4

    const-string v3, "com.android.nfc.BeamShareActivity"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.bluetooth"

    aput-object v3, v2, v4

    const-string v3, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.Settings"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.GridSettings"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xc

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.CryptKeeper"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xd

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.accessibilitywidget.AccessibilityEasyWidgetProviderAssistiveLight"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xe

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.accessibilitywidget.AccessibilityWidgetProviderAssistiveLight"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xf

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.Settings$WifiApSettingsActivity"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x10

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.TetherSettings"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x11

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "com.android.settings"

    aput-object v3, v2, v4

    const-string v3, "com.android.settings.Settings$TetherSettingsActivity2"

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents:[[Ljava/lang/String;

    .line 75
    new-array v0, v6, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.settings"

    aput-object v2, v1, v4

    const-string v2, "com.android.settings.TetherSettings"

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, "com.android.settings"

    aput-object v2, v1, v4

    const-string v2, "com.android.settings.Settings$TetherSettingsActivity2"

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents_noncom:[[Ljava/lang/String;

    return-void
.end method

.method public static isShipMode()Z
    .locals 7

    .prologue
    .line 81
    const/4 v3, 0x0

    .line 83
    .local v3, "result":Z
    const/4 v1, 0x0

    .line 85
    .local v1, "method":Ljava/lang/reflect/Method;
    :try_start_0
    const-class v4, Landroid/os/Debug;

    const-string v5, "isProductShip"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 86
    const/4 v4, 0x0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 87
    .local v2, "mode":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isShipMode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 88
    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 89
    const/4 v3, 0x1

    .line 101
    .end local v2    # "mode":I
    :cond_0
    :goto_0
    return v3

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 95
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 96
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 97
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method
