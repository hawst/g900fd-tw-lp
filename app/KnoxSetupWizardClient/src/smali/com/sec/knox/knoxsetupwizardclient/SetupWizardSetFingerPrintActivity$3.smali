.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$3;
.super Ljava/lang/Object;
.source "SetupWizardSetFingerPrintActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->showSensorErrorDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v0, 0x0

    .line 172
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mActionBarSwitch.setChecked(false) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Landroid/widget/Switch;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Switch;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 174
    return-void
.end method
