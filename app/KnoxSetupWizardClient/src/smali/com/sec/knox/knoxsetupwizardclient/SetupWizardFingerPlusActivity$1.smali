.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardFingerPlusActivity.java"

# interfaces
.implements Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public resultFailed()V
    .locals 2

    .prologue
    .line 70
    invoke-static {}, Lcom/samsung/android/fingerprint/FingerprintManager;->isEnrolling()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintManager;->notifyEnrollEnd()Z

    .line 73
    :cond_0
    return-void
.end method

.method public resultSuccess()V
    .locals 4

    .prologue
    .line 56
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x103012b

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0108

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 61
    const/4 v1, 0x4

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPasswordType(I)V

    .line 62
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 63
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    const-class v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 64
    const-string v1, "called_pass_type"

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$000()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 65
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 66
    return-void
.end method

.method public sensorError()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->showSensorErrorDialog()V
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    .line 78
    return-void
.end method
