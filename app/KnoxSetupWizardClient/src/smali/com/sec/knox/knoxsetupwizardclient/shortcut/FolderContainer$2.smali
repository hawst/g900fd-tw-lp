.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 508
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->presseBg:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1000(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 510
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v1, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 512
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSelectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDeselectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 514
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopupDivider:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1300(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 529
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-nez v0, :cond_3

    .line 530
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, -0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    .line 533
    :goto_1
    return-void

    .line 516
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 518
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSelectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 519
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDeselectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 520
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopupDivider:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1300(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 522
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->allAppsSelected:I
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1400(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v0

    if-nez v0, :cond_0

    .line 524
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mSelectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 525
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDeselectAll:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 526
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopupDivider:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1300(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 532
    :cond_3
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_1
.end method
