.class public Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;
.super Landroid/app/Activity;
.source "LicenseExpiredActivity.java"


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private btn_disagree:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 12
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->btn_disagree:Landroid/widget/Button;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onCreate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 18
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->requestWindowFeature(I)Z

    .line 20
    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->setContentView(I)V

    .line 22
    const v0, 0x7f0f0005

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->btn_disagree:Landroid/widget/Button;

    .line 25
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;->btn_disagree:Landroid/widget/Button;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/LicenseExpiredActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 32
    return-void
.end method
