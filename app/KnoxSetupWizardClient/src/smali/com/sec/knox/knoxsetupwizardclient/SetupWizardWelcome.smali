.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
.super Landroid/app/Activity;
.source "SetupWizardWelcome.java"


# static fields
.field private static EULA_STAGE:I

.field private static WELCOME_STAGE:I

.field private static alreadyPlayed:Z

.field private static eula_result:I

.field private static isCreationInProgress:Z


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private buttonNext:Landroid/widget/LinearLayout;

.field private confirmButton:Landroid/widget/LinearLayout;

.field private currentStage:I

.field private eulaCheckbox:Landroid/widget/CheckBox;

.field private eulaCheckbox2:Landroid/widget/CheckBox;

.field private eulaLayout:Landroid/widget/RelativeLayout;

.field private isEulaChecked:Z

.field private isEulaChecked2:Z

.field private isPdShown:Z

.field private knoxBackground:Landroid/widget/LinearLayout;

.field private knoxTitle:Landroid/widget/TextView;

.field private lisBtnNext:Landroid/view/View$OnClickListener;

.field private mIsPhone:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mVideoSize:I

.field private mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

.field private pd:Landroid/app/ProgressDialog;

.field private pd_timer:Landroid/os/CountDownTimer;

.field private tickCount:I

.field private titleButton:Landroid/widget/LinearLayout;

.field private txt_eula1:Landroid/widget/TextView;

.field private video:Landroid/widget/VideoView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    const/4 v0, 0x1

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    .line 58
    const/16 v0, 0x10

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->EULA_STAGE:I

    .line 60
    sput-boolean v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    .line 61
    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eula_result:I

    .line 67
    sput-boolean v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isCreationInProgress:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->ACTIVITY_NAME:Ljava/lang/String;

    .line 63
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->buttonNext:Landroid/widget/LinearLayout;

    .line 64
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxTitle:Landroid/widget/TextView;

    .line 66
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    .line 68
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 69
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd_timer:Landroid/os/CountDownTimer;

    .line 71
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->video:Landroid/widget/VideoView;

    .line 72
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxBackground:Landroid/widget/LinearLayout;

    .line 74
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    .line 77
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    .line 78
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    .line 79
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    .line 80
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    .line 89
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mVideoSize:I

    .line 91
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->lisBtnNext:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    return p1
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxBackground:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 44
    sput-boolean p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isCreationInProgress:Z

    return p0
.end method

.method static synthetic access$1200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->saveContainerCreationParam(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd_timer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->enableConfirmButtonIfPossible()V

    return-void
.end method

.method static synthetic access$1702(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eula_result:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    return-object v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->EULA_STAGE:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    return p1
.end method

.method static synthetic access$608(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;
    .param p1, "x1"    # Z

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    return p1
.end method

.method static synthetic access$902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 44
    sput-boolean p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    return p0
.end method

.method private createPersona()V
    .locals 2

    .prologue
    .line 409
    sget-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isCreationInProgress:Z

    if-nez v0, :cond_0

    .line 410
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isCreationInProgress:Z

    .line 411
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$4;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 438
    :cond_0
    return-void
.end method

.method private enableConfirmButtonIfPossible()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 536
    const-string v2, "KnoxSetupWizardClient"

    const-string v3, "enableConfirmButtonIfPossible"

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    if-nez v2, :cond_0

    .line 538
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 539
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 540
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 541
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 542
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 540
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 547
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 548
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 549
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 550
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 551
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 549
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 557
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 558
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 559
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 560
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 558
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 562
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-void
.end method

.method private init()V
    .locals 7

    .prologue
    const/high16 v6, 0x7f060000

    .line 289
    const-string v4, "KnoxSetupWizardClient"

    const-string v5, "init"

    invoke-static {v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "android.resource://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/raw/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 294
    .local v3, "videoUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 296
    .local v1, "ins":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStream;->available()I

    move-result v4

    iput v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mVideoSize:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :goto_0
    const v4, 0x7f0f0091

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/VideoView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->video:Landroid/widget/VideoView;

    .line 302
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->video:Landroid/widget/VideoView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/VideoView;->setStopMusic(Z)V

    .line 304
    iget v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mVideoSize:I

    if-eqz v4, :cond_0

    .line 305
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->video:Landroid/widget/VideoView;

    invoke-virtual {v4, v3}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 306
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->video:Landroid/widget/VideoView;

    new-instance v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$3;

    invoke-direct {v5, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    invoke-virtual {v4, v5}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 317
    :cond_0
    const v4, 0x7f0f0092

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxBackground:Landroid/widget/LinearLayout;

    .line 319
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mIsPhone:Z

    .line 320
    const/4 v2, 0x0

    .line 321
    .local v2, "typeface":Landroid/graphics/Typeface;
    const v4, 0x7f0f0004

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxTitle:Landroid/widget/TextView;

    .line 323
    iget-boolean v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mIsPhone:Z

    if-eqz v4, :cond_1

    .line 325
    :try_start_1
    const-string v4, "/system/fonts/SamsungSans-Bold.ttf"

    invoke-static {v4}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    .line 326
    if-eqz v2, :cond_1

    .line 327
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxTitle:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 335
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->buttonNext:Landroid/widget/LinearLayout;

    if-nez v4, :cond_2

    .line 336
    const v4, 0x7f0f006c

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->buttonNext:Landroid/widget/LinearLayout;

    .line 337
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->buttonNext:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 338
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->buttonNext:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->lisBtnNext:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    :cond_2
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    invoke-interface {v4}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;->startDummyPanelFadeOut()V

    .line 342
    return-void

    .line 297
    .end local v2    # "typeface":Landroid/graphics/Typeface;
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 329
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "typeface":Landroid/graphics/Typeface;
    :catch_1
    move-exception v0

    .line 330
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 331
    const-string v4, "KnoxSetupWizardClient"

    const-string v5, "SamsungSans-Bold not available"

    invoke-static {v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private initEula()V
    .locals 3

    .prologue
    const v2, 0x7f0c0020

    .line 345
    const v0, 0x7f0f0008

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    .line 346
    const v0, 0x7f0f00a3

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    .line 347
    const v0, 0x7f0f00a5

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    .line 348
    const v0, 0x7f0f009a

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->titleButton:Landroid/widget/LinearLayout;

    .line 349
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setText(I)V

    .line 350
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 351
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setText(I)V

    .line 353
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 355
    :cond_0
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->enableConfirmButtonIfPossible()V

    .line 356
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setCheckedEvent()V

    .line 357
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setClickedEvent()V

    .line 360
    const v0, 0x7f0f0007

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    .line 361
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const v1, 0x7f0c0025

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 362
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 363
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const v1, 0x7f0c0026

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const v1, 0x7f0c0027

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 366
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 368
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerType()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 371
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const v1, 0x7f0c0028

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 372
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const v1, 0x7f0c0029

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 374
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const v1, 0x7f0c002a

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 377
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const v1, 0x7f0c002b

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 378
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->txt_eula1:Landroid/widget/TextView;

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    .line 380
    :cond_3
    return-void
.end method

.method private registerBRReceiver()V
    .locals 2

    .prologue
    .line 441
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 442
    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$5;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 461
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.knox.start.SETUP_WIZARD"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 463
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 465
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private restoreStateFromBundle(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 270
    if-eqz p1, :cond_0

    .line 271
    const-string v0, "currentStage"

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    .line 272
    const-string v0, "videoAlreadyPlayed"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    .line 273
    const-string v0, "eulaChecked"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    .line 274
    const-string v0, "eulaChecked2"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    .line 275
    const-string v0, "progress_dialog_shown"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    .line 276
    const-string v0, "timer_tick_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    .line 286
    :goto_0
    return-void

    .line 278
    :cond_0
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    .line 279
    sput-boolean v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    .line 280
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    .line 281
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    .line 282
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    .line 283
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    goto :goto_0
.end method

.method private saveContainerCreationParam(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 475
    const-string v1, "creation_params"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/container/ContainerCreationParams;

    .line 476
    .local v0, "param":Lcom/sec/knox/container/ContainerCreationParams;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setContainerCreationParams(Lcom/sec/knox/container/ContainerCreationParams;)V

    .line 477
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationParam(Lcom/sec/knox/container/ContainerCreationParams;)V

    .line 478
    return-void
.end method

.method public static sendRequestKeyStatus(Landroid/content/Context;I)V
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "container_id"    # I

    .prologue
    .line 481
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendRequestKeyStatus() id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 483
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.knox.containeragent.klms.licensekey.check"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 484
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "container_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 486
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 487
    return-void
.end method

.method private setCheckedEvent()V
    .locals 2

    .prologue
    .line 490
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "setCheckedEvent"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$6;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$6;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 501
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$7;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$7;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 510
    :cond_0
    return-void
.end method

.method private setClickedEvent()V
    .locals 2

    .prologue
    .line 513
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "setClickEvent"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->titleButton:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->titleButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$8;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$8;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 524
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->confirmButton:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$9;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$9;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 533
    return-void
.end method

.method private unregisterReceiver()V
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 471
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 472
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 384
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    if-ne v0, v1, :cond_1

    .line 386
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "current stage is welcome stage. so finish activity"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-virtual {p0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setResult(I)V

    .line 388
    sput-boolean v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    .line 389
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->finish()V

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->EULA_STAGE:I

    if-ne v0, v1, :cond_0

    .line 391
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "current stage is eula stage. so go back to welcome stage"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    invoke-interface {v0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;->startBackAnimation()V

    .line 393
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    .line 394
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 395
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    if-eqz v0, :cond_2

    .line 396
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 398
    :cond_2
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    .line 399
    iput-boolean v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    .line 400
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->enableConfirmButtonIfPossible()V

    .line 404
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->buttonNext:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onCreate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "knox_container_eula_shown"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eula_result:I

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "eula_result : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eula_result:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f080000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mIsPhone:Z

    .line 126
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mIsPhone:Z

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setRequestedOrientation(I)V

    .line 129
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 131
    const v0, 0x1030128

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setTheme(I)V

    .line 133
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 135
    const v0, 0x7f03001d

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->setContentView(I)V

    .line 136
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x2400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 137
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 139
    if-eqz p1, :cond_1

    .line 140
    const-string v0, "videoAlreadyPlayed"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    .line 141
    const-string v0, "progress_dialog_shown"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    .line 142
    const-string v0, "timer_tick_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    .line 143
    const-string v0, "eulaChecked"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    .line 144
    const-string v0, "eulaChecked2"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    .line 146
    :cond_1
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mIsPhone:Z

    if-eqz v0, :cond_3

    .line 147
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    .line 152
    :goto_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    invoke-interface {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;->initAnimations(Landroid/content/Context;)V

    .line 153
    const v0, 0x7f0f0098

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaLayout:Landroid/widget/RelativeLayout;

    .line 155
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->init()V

    .line 156
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->initEula()V

    .line 158
    invoke-static {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->sendRequestKeyStatus(Landroid/content/Context;I)V

    .line 160
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 163
    new-instance v0, Landroid/app/ProgressDialog;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x103012b

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    .line 167
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 168
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 170
    const/4 v6, 0x0

    .line 171
    .local v6, "tickCountToRun":I
    iget-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    if-eqz v0, :cond_4

    .line 172
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    rsub-int/lit8 v6, v0, 0xf

    .line 177
    :goto_1
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;

    mul-int/lit16 v1, v6, 0x3e8

    int-to-long v2, v1

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;JJ)V

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome$2;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd_timer:Landroid/os/CountDownTimer;

    .line 203
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 204
    iput-boolean v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    .line 205
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->registerBRReceiver()V

    .line 206
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->createPersona()V

    .line 208
    .end local v6    # "tickCountToRun":I
    :cond_2
    return-void

    .line 149
    :cond_3
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    goto :goto_0

    .line 174
    .restart local v6    # "tickCountToRun":I
    :cond_4
    const/16 v6, 0xf

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->unregisterReceiver()V

    .line 243
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd_timer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->pd_timer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 249
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 250
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 235
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 238
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 265
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 266
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->restoreStateFromBundle(Landroid/os/Bundle;)V

    .line 267
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onResume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 213
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 215
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->video:Landroid/widget/VideoView;

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->WELCOME_STAGE:I

    if-ne v0, v1, :cond_3

    .line 216
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mVideoSize:I

    if-nez v0, :cond_2

    .line 217
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    .line 218
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxBackground:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 223
    :goto_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    invoke-interface {v0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;->startShowWelcomeAnimation()V

    .line 227
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 228
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->eulaCheckbox2:Landroid/widget/CheckBox;

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 231
    :cond_1
    return-void

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->knoxBackground:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->video:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    goto :goto_0

    .line 224
    :cond_3
    iget v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->EULA_STAGE:I

    if-ne v0, v1, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->mWelcomeAnimation:Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;

    invoke-interface {v0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;->showEulaWithoutAnimation()V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 254
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 255
    const-string v0, "videoAlreadyPlayed"

    sget-boolean v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->alreadyPlayed:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 256
    const-string v0, "currentStage"

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->currentStage:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    const-string v0, "eulaChecked"

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 258
    const-string v0, "eulaChecked2"

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isEulaChecked2:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 259
    const-string v0, "progress_dialog_shown"

    iget-boolean v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->isPdShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 260
    const-string v0, "timer_tick_count"

    iget v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;->tickCount:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 261
    return-void
.end method
