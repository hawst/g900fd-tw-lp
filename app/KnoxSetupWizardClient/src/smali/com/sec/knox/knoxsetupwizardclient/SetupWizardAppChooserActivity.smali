.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;
.super Landroid/app/Activity;
.source "SetupWizardAppChooserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$1;,
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$AppLabelComparator;,
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    }
.end annotation


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field apkList:Landroid/widget/ListView;

.field appList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;",
            ">;"
        }
    .end annotation
.end field

.field btnNext:Landroid/widget/LinearLayout;

.field checkedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field excludePackages:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private img_prev:Landroid/widget/ImageView;

.field mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

.field mPackageManager:Landroid/content/pm/PackageManager;

.field private text_prev:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    .line 255
    return-void
.end method

.method private addSystemApps(Landroid/content/Intent;)V
    .locals 13
    .param p1, "queryIntent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x0

    const/4 v12, 0x1

    .line 135
    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 136
    .local v9, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v9, p1, v10}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    .line 138
    .local v7, "launchableApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 139
    .local v1, "app":Landroid/content/pm/ResolveInfo;
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v10, :cond_0

    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v10, :cond_0

    .line 140
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 141
    .local v8, "packageName":Ljava/lang/String;
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v10, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 143
    .local v4, "flags":I
    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->excludePackages:Ljava/util/HashSet;

    invoke-virtual {v10, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 145
    const-string v10, "com.sec.android.app.samsungapps"

    iget-object v11, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 147
    new-instance v2, Landroid/content/ComponentName;

    const-string v10, "com.sec.android.app.samsungapps"

    const-string v11, "com.sec.android.app.samsungapps.KnoxAppsMainActivity"

    invoke-direct {v2, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .local v2, "componentKnoxApps":Landroid/content/ComponentName;
    invoke-virtual {v9, v2, v12, v12}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 155
    const/4 v10, 0x0

    :try_start_0
    invoke-virtual {v9, v2, v10}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 156
    .local v0, "ai":Landroid/content/pm/ActivityInfo;
    new-instance v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    invoke-direct {v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;-><init>()V

    .line 157
    .local v6, "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    iget-object v10, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    .line 158
    iget-object v10, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v10, v9}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    .line 159
    invoke-virtual {v0, v9}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 160
    invoke-virtual {v0, v9}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    .line 161
    iget-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    if-nez v10, :cond_1

    iget-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    .line 162
    :cond_1
    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    .end local v0    # "ai":Landroid/content/pm/ActivityInfo;
    .end local v6    # "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    :goto_1
    const/4 v10, 0x2

    invoke-virtual {v9, v2, v10, v12}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0

    .line 164
    :catch_0
    move-exception v3

    .line 166
    .local v3, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v3}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 175
    .end local v2    # "componentKnoxApps":Landroid/content/ComponentName;
    .end local v3    # "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    new-instance v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    invoke-direct {v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;-><init>()V

    .line 176
    .restart local v6    # "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    .line 177
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v10, v9}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    .line 178
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v10, v9}, Landroid/content/pm/ActivityInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->icon:Landroid/graphics/drawable/Drawable;

    .line 179
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v10, v9}, Landroid/content/pm/ActivityInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v10

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    .line 180
    iget-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    if-nez v10, :cond_3

    iget-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    iput-object v10, v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    .line 181
    :cond_3
    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    invoke-interface {v10, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 186
    .end local v1    # "app":Landroid/content/pm/ResolveInfo;
    .end local v4    # "flags":I
    .end local v6    # "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    .end local v8    # "packageName":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method private isSystemPackage(Landroid/content/pm/PackageInfo;)Z
    .locals 3
    .param p1, "pkgInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    const/4 v1, 0x0

    .line 266
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-nez v2, :cond_1

    .line 272
    :cond_0
    :goto_0
    return v1

    .line 267
    :cond_1
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 268
    .local v0, "flags":I
    and-int/lit8 v2, v0, 0x1

    if-nez v2, :cond_2

    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_0

    .line 270
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private makeExcludedList()V
    .locals 10

    .prologue
    .line 190
    const-string v7, "input_method"

    invoke-virtual {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    .line 191
    .local v5, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v4

    .line 193
    .local v4, "imis":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    .line 195
    .local v3, "imi":Landroid/view/inputmethod/InputMethodInfo;
    :try_start_0
    invoke-virtual {v3, p0}, Landroid/view/inputmethod/InputMethodInfo;->isDefault(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->isSystemPackage(Landroid/content/pm/PackageInfo;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 196
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->excludePackages:Ljava/util/HashSet;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 198
    :catch_0
    move-exception v7

    goto :goto_0

    .line 205
    .end local v3    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :cond_1
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedPackages:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v6, :cond_2

    aget-object v1, v0, v2

    .line 206
    .local v1, "excludedApp":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->excludePackages:Ljava/util/HashSet;

    invoke-virtual {v7, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 205
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 208
    .end local v1    # "excludedApp":Ljava/lang/String;
    :cond_2
    return-void

    .line 200
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v6    # "len$":I
    .local v2, "i$":Ljava/util/Iterator;
    .restart local v3    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :catch_1
    move-exception v7

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 242
    :goto_0
    return-void

    .line 213
    :pswitch_0
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    invoke-virtual {v3}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->getChecked()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    .line 215
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 216
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 217
    .local v2, "tmpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    invoke-virtual {v3}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->getChecked()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 219
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 221
    .local v1, "selectedItem":Ljava/lang/String;
    const-string v3, "com.sec.android.app.samsungapps"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    const-string v4, "com.sec.android.widget.samsungapps"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 223
    :cond_1
    const-string v3, "com.sec.android.app.clockpackage"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 224
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    const-string v4, "com.sec.android.widgetapp.alarmwidget"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 225
    :cond_2
    const-string v3, "com.samsung.everglades.video"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 226
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    const-string v4, "com.sec.android.app.videoplayer"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 227
    :cond_3
    const-string v3, "com.sec.android.quickmemo"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 228
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    const-string v4, "com.sec.android.quickmemowidget"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 229
    :cond_4
    const-string v3, "com.android.calendar"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 230
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    const-string v4, "com.sec.android.widgetapp.SPlannerAppWidget"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 231
    :cond_5
    const-string v3, "com.samsung.android.app.episodes"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 232
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    const-string v4, "com.samsung.android.app.storyalbumwidget"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 235
    .end local v1    # "selectedItem":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->checkedList:Ljava/util/List;

    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setContainerAppList(Ljava/util/List;)V

    .line 238
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "tmpList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_7
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->setResult(I)V

    .line 239
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->finish()V

    goto/16 :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x7f0f006c
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x4

    .line 57
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " : onCreate"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 58
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const v8, 0x7f030016

    invoke-virtual {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->setContentView(I)V

    .line 61
    const v8, 0x7f0f006c

    invoke-virtual {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->btnNext:Landroid/widget/LinearLayout;

    .line 62
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->btnNext:Landroid/widget/LinearLayout;

    invoke-virtual {v8, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 65
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    .line 66
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->excludePackages:Ljava/util/HashSet;

    .line 68
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->makeExcludedList()V

    .line 71
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v5, "launcherIntent":Landroid/content/Intent;
    const-string v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v5, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    invoke-direct {p0, v5}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->addSystemApps(Landroid/content/Intent;)V

    .line 76
    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    .local v7, "widgetIntent":Landroid/content/Intent;
    invoke-direct {p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->addSystemApps(Landroid/content/Intent;)V

    .line 80
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    new-instance v9, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$AppLabelComparator;

    const/4 v10, 0x0

    invoke-direct {v9, v10}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$AppLabelComparator;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$1;)V

    invoke-static {v8, v9}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 83
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 84
    .local v1, "dedupPackageSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v2, v8, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 85
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    .line 87
    .local v4, "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "+"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "both":Ljava/lang/String;
    iget-object v8, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    iget-object v8, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 91
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    invoke-interface {v8, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 84
    :goto_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 93
    :cond_0
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 98
    .end local v0    # "both":Ljava/lang/String;
    .end local v4    # "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    :cond_1
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 99
    .local v6, "packageMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;>;"
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    .line 100
    .restart local v4    # "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    iget-object v8, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 101
    iget-object v8, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    iput-object v8, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->masterEntry:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    goto :goto_2

    .line 103
    :cond_2
    iget-object v8, v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v8, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 107
    .end local v4    # "info":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    :cond_3
    const v8, 0x7f0f007a

    invoke-virtual {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ListView;

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->apkList:Landroid/widget/ListView;

    .line 108
    new-instance v8, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->appList:Ljava/util/List;

    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v8, p0, v9, v10}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;-><init>(Landroid/app/Activity;Ljava/util/List;Landroid/content/pm/PackageManager;)V

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    .line 109
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->apkList:Landroid/widget/ListView;

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    invoke-virtual {v8, v9}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->apkList:Landroid/widget/ListView;

    invoke-virtual {v8, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 112
    const v8, 0x7f0f0036

    invoke-virtual {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->img_prev:Landroid/widget/ImageView;

    .line 113
    const v8, 0x7f0f0072

    invoke-virtual {p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->text_prev:Landroid/widget/TextView;

    .line 115
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->img_prev:Landroid/widget/ImageView;

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->text_prev:Landroid/widget/TextView;

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "row"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 247
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    invoke-virtual {v0, p3}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->setCheckBox(I)V

    .line 248
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;->mAdapter:Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/AppListAdapter;->notifyDataSetChanged()V

    .line 249
    return-void
.end method
