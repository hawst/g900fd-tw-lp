.class public Lcom/sec/knox/knoxsetupwizardclient/detector/SetupWizardDetectorReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SetupWizardDetectorReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private cancelAndFinish()V
    .locals 1

    .prologue
    .line 113
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getContainerCreationParams()Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->cancelCreateContainer(Lcom/sec/knox/container/ContainerCreationParams;)Z

    .line 115
    const-string v0, "cancel Creation"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 117
    :cond_0
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->resetCreationInfo()V

    .line 118
    return-void
.end method

.method private saveContainerCreationParam(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 107
    const-string v1, "creation_params"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/container/ContainerCreationParams;

    .line 108
    .local v0, "param":Lcom/sec/knox/container/ContainerCreationParams;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setContainerCreationParams(Lcom/sec/knox/container/ContainerCreationParams;)V

    .line 109
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationParam(Lcom/sec/knox/container/ContainerCreationParams;)V

    .line 110
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/high16 v5, 0x10000000

    const/16 v4, 0x64

    const/4 v6, 0x0

    .line 40
    if-nez p2, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 46
    .local v0, "action":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 48
    const-string v2, "com.sec.knox.start.SETUP_WIZARD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 49
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v2

    if-nez v2, :cond_0

    .line 50
    const-string v2, "Container is not initialized"

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p2}, Lcom/sec/knox/knoxsetupwizardclient/detector/SetupWizardDetectorReceiver;->saveContainerCreationParam(Landroid/content/Intent;)V

    .line 52
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerType()Z

    move-result v2

    if-nez v2, :cond_3

    .line 53
    const-string v2, "B2B Container is launching"

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 54
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    if-lt v2, v4, :cond_2

    .line 55
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardNotification;->showSetupWizardNotification(Landroid/content/Context;)V

    goto :goto_0

    .line 57
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 58
    .local v1, "i":Landroid/content/Intent;
    const-class v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 59
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 60
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 63
    .end local v1    # "i":Landroid/content/Intent;
    :cond_3
    const-string v2, "Cancel B2C Container creation"

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 64
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/detector/SetupWizardDetectorReceiver;->cancelAndFinish()V

    goto :goto_0

    .line 67
    :cond_4
    const-string v2, "com.sec.knox.continue.SETUP_WIZARD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 68
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 69
    .restart local v1    # "i":Landroid/content/Intent;
    const-class v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;

    invoke-virtual {v1, p1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 70
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 71
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 72
    .end local v1    # "i":Landroid/content/Intent;
    :cond_5
    const-string v2, "com.sec.knox.died.SETUP_WIZARD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 73
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 74
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getContainerCreationParams()Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->cancelCreateContainer(Lcom/sec/knox/container/ContainerCreationParams;)Z

    .line 75
    const-string v2, "died cancel Creation"

    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 77
    :cond_6
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->resetCreationInfo()V

    .line 78
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardNotification;->hideSetupWizardNotification(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 79
    :cond_7
    const-string v2, "android.intent.action.USER_ADDED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 80
    const-string v2, "android.intent.extra.user_handle"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-lt v2, v4, :cond_0

    .line 81
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardNotification;->hideSetupWizardNotification(Landroid/content/Context;)V

    .line 82
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;

    const-string v3, "android.intent.action.USER_ADDED"

    invoke-direct {v2, p1, v3}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->start()V

    goto/16 :goto_0

    .line 84
    :cond_8
    const-string v2, "android.intent.action.USER_REMOVED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 85
    const-string v2, "android.intent.extra.user_handle"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-lt v2, v4, :cond_0

    .line 86
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;

    const-string v3, "android.intent.action.USER_REMOVED"

    invoke-direct {v2, p1, v3}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->start()V

    .line 89
    new-instance v2, Landroid/view/ContextThemeWrapper;

    const v3, 0x103012b

    invoke-direct {v2, p1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const v3, 0x7f0c00fe

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const v5, 0x7f0c0057

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 98
    :cond_9
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 99
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;

    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v2, p1, v3}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->start()V

    .line 100
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;

    invoke-direct {v2, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->start()V

    goto/16 :goto_0

    .line 101
    :cond_a
    const-string v2, "com.sec.knox.migrationagent.REMOVE_MIGRATION_AGENT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;

    const-string v3, "com.sec.knox.migrationagent.REMOVE_MIGRATION_AGENT"

    const-string v4, "persona_id"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-direct {v2, p1, v3, v4}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/detector/StubPackageThread;->start()V

    goto/16 :goto_0
.end method
