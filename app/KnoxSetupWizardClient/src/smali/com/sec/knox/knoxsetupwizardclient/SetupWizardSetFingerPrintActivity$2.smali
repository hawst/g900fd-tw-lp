.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;
.super Ljava/lang/Object;
.source "SetupWizardSetFingerPrintActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Landroid/widget/Switch;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintSupplement(Z)V

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContainerCreationInfo.setFingerprintSupplement : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mActionBarSwitch:Landroid/widget/Switch;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Landroid/widget/Switch;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->setResult(I)V

    .line 107
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->finish()V

    .line 108
    return-void
.end method
