.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;
.super Landroid/app/Activity;
.source "SetupWizardRootActivity.java"


# static fields
.field private static called_pass_type:I

.field private static eula_result:I

.field private static mNextStage:I

.field private static personaState:I

.field private static stageIntent:Landroid/content/Intent;


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

.field private mPersona:Landroid/os/PersonaManager;

.field private mPreviousStage:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->stageIntent:Landroid/content/Intent;

    .line 58
    const/4 v0, -0x1

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    .line 59
    const/4 v0, -0x2

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->personaState:I

    .line 60
    const/4 v0, 0x0

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->eula_result:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mPreviousStage:I

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mPersona:Landroid/os/PersonaManager;

    return-void
.end method

.method public static setNextStage(I)V
    .locals 0
    .param p0, "nextStage"    # I

    .prologue
    .line 339
    sput p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    .line 340
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x10

    const/high16 v5, 0x10000

    const/16 v4, 0x100

    const/4 v3, -0x1

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult requestCode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resultCode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 227
    sparse-switch p1, :sswitch_data_0

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 229
    :sswitch_0
    if-ne p2, v3, :cond_1

    .line 230
    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 232
    :cond_1
    const-string v1, "setupwizard canceled. finishing..."

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 233
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1, p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 234
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V

    goto :goto_0

    .line 238
    :sswitch_1
    if-ne p2, v3, :cond_3

    if-eqz p3, :cond_3

    .line 239
    const-string v1, "LOCK_QUALITY"

    invoke-virtual {p3, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    .line 240
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getFingerprintPlus()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 241
    const/high16 v1, 0x1000000

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 243
    :cond_2
    sput v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 247
    :cond_3
    const/4 v1, 0x1

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 251
    :sswitch_2
    if-ne p2, v3, :cond_5

    .line 252
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerType()Z

    move-result v1

    if-eqz v1, :cond_4

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    if-nez v1, :cond_4

    .line 254
    const/16 v1, 0x1000

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 256
    :cond_4
    sput v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 259
    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ACTIVITY_CODE_RESET_PASSWORD resultCode : RESULT_CANCEL, called_pass_type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 260
    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 265
    :sswitch_3
    if-ne p2, v3, :cond_6

    .line 266
    const v1, 0x1000001

    iput v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mPreviousStage:I

    .line 267
    sput v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 269
    :cond_6
    sput v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto :goto_0

    .line 274
    :sswitch_4
    if-ne p2, v3, :cond_7

    .line 275
    sput v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    .line 277
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FINGERPRINTS_PLUS called_pass_type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 279
    :cond_7
    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 284
    :sswitch_5
    if-ne p2, v3, :cond_8

    .line 285
    sput v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 287
    :cond_8
    sput v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 292
    :sswitch_6
    if-ne p2, v3, :cond_9

    .line 293
    sput v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 295
    :cond_9
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getContainerType()Z

    move-result v1

    if-eqz v1, :cond_a

    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    if-nez v1, :cond_a

    .line 297
    const/16 v1, 0x1000

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 299
    :cond_a
    sput v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 305
    :sswitch_7
    if-eq p2, v3, :cond_e

    if-eqz p3, :cond_e

    .line 306
    const-string v1, "CREATE_PERSONA_FAIL"

    invoke-virtual {p3, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 307
    .local v0, "createPersonaFail":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_d

    .line 308
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getPasswordType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_b

    .line 309
    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 311
    :cond_b
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getFingerprintPlus()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 312
    const/high16 v1, 0x1000000

    sput v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 314
    :cond_c
    sput v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    goto/16 :goto_0

    .line 319
    :cond_d
    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x103012b

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    const v2, 0x7f0c002e

    invoke-static {v1, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 325
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v1

    invoke-virtual {v1, p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 326
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V

    goto/16 :goto_0

    .line 328
    .end local v0    # "createPersonaFail":I
    :cond_e
    if-ne p2, v3, :cond_0

    .line 329
    const-string v1, "finish SetupWizardRootActivity"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V

    goto/16 :goto_0

    .line 227
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x10 -> :sswitch_1
        0x100 -> :sswitch_2
        0x1000 -> :sswitch_5
        0x10000 -> :sswitch_7
        0x100000 -> :sswitch_6
        0x1000000 -> :sswitch_4
        0x1000001 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 64
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " : onCreate"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 65
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const-string v6, "persona"

    invoke-virtual {p0, v6}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/PersonaManager;

    iput-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mPersona:Landroid/os/PersonaManager;

    .line 67
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 69
    .local v5, "pm":Landroid/content/pm/PackageManager;
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mPersona:Landroid/os/PersonaManager;

    if-eqz v6, :cond_3

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->isContainerCreationInitialized()Z

    move-result v6

    if-nez v6, :cond_3

    .line 71
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v6, v9}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 72
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v6, v9}, Landroid/os/PersonaManager;->getPersonas(Z)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    .line 73
    .local v2, "containerCount":I
    if-lez v2, :cond_0

    .line 74
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6, p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V

    .line 119
    .end local v2    # "containerCount":I
    :goto_0
    return-void

    .line 80
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 81
    .local v4, "intent":Landroid/content/Intent;
    const-string v6, "package"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    if-nez v6, :cond_1

    .line 82
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6, p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 83
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V

    goto :goto_0

    .line 85
    :cond_1
    const-string v6, "package"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    const-string v7, "com.sec.knox.setupwizardstub"

    invoke-virtual {v6, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 86
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    invoke-virtual {v6, p0, v8}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V

    goto :goto_0

    .line 92
    :cond_2
    :try_start_0
    const-string v6, "package"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 93
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 95
    .local v1, "b":Landroid/os/Bundle;
    const-string v6, "com.sec.knox.approvedcaller"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    .line 96
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "b":Landroid/os/Bundle;
    :catch_0
    move-exception v3

    .line 106
    .local v3, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 113
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "knox_container_eula_shown"

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    sput v6, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->eula_result:I

    .line 114
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "eula_result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->eula_result:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 115
    const-string v6, "KnoxSetupWizardClient"

    const-string v7, "there are no persona or calling from settings. show setupwizard welcome activity"

    invoke-static {v6, v7}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    invoke-static {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardNotification;->showSetupWizardNotification(Landroid/content/Context;)V

    .line 118
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mEkm:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    goto/16 :goto_0

    .line 99
    .restart local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v1    # "b":Landroid/os/Bundle;
    .restart local v4    # "intent":Landroid/content/Intent;
    :cond_4
    :try_start_1
    const-string v6, "com.sec.knox.approvedcaller"

    invoke-virtual {v1, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "knoxsetupwizardapproval"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 100
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, p0, v7}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->setContainerCreationResult(Landroid/content/Context;Z)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->finish()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 135
    const-string v0, "RootActivity onDestroy"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 136
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 137
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onResume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 125
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 127
    const-string v0, "KnoxSetupWizardClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get : setupwizard current stage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->mNextStage:I

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->stageIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->updateStage(ILandroid/content/Intent;)V

    .line 131
    :cond_0
    return-void
.end method

.method protected updateStage(ILandroid/content/Intent;)V
    .locals 4
    .param p1, "activityCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 159
    const/4 v0, 0x0

    .line 160
    .local v0, "i":Landroid/content/Intent;
    if-nez p2, :cond_0

    .line 161
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "i":Landroid/content/Intent;
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 165
    .restart local v0    # "i":Landroid/content/Intent;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ActivityCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 167
    sparse-switch p1, :sswitch_data_0

    .line 218
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "I don\'t know where I have to go."

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :goto_1
    return-void

    .line 163
    :cond_0
    move-object v0, p2

    goto :goto_0

    .line 169
    :sswitch_0
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardWelcome;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 170
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : welcome activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 175
    :sswitch_1
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSecuritySettingActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 176
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : locktype activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 181
    :sswitch_2
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 182
    sget v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const-string v1, "called_pass_type"

    sget v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->called_pass_type:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 183
    :cond_1
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : reset password activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 188
    :sswitch_3
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 189
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : fingerprint supplement activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 194
    :sswitch_4
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 195
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : reset password activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 200
    :sswitch_5
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 201
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : Backup pin activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 206
    :sswitch_6
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 207
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : App chooser activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 212
    :sswitch_7
    const-class v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 213
    const-string v1, "KnoxSetupWizardClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const-string v1, "KnoxSetupWizardClient"

    const-string v2, "start activity : knox name activity"

    invoke-static {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0, v0, p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 167
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x10 -> :sswitch_1
        0x100 -> :sswitch_2
        0x1000 -> :sswitch_5
        0x10000 -> :sswitch_7
        0x100000 -> :sswitch_6
        0x1000000 -> :sswitch_4
        0x1000001 -> :sswitch_3
    .end sparse-switch
.end method
