.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;
.super Landroid/app/Activity;
.source "SetupWizardFingerPlusActivity.java"


# static fields
.field private static mCurrentLockTypeIdx:I


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private btnConfirm:Landroid/widget/LinearLayout;

.field private dialog:Landroid/app/AlertDialog;

.field private dialogBuilder:Landroid/app/AlertDialog$Builder;

.field private img_confirm:Landroid/widget/ImageView;

.field private isPhone:Z

.field private lockTypes:Landroid/widget/RadioGroup;

.field private mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

.field private mIdentifyDialog:Landroid/app/Dialog;

.field private mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

.field private text_confirm:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, -0x1

    sput v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 29
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->ACTIVITY_NAME:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->btnConfirm:Landroid/widget/LinearLayout;

    .line 39
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->lockTypes:Landroid/widget/RadioGroup;

    .line 44
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mIdentifyDialog:Landroid/app/Dialog;

    .line 45
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 46
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialog:Landroid/app/AlertDialog;

    .line 52
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    invoke-direct {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 27
    sput p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I

    return p0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)Lcom/samsung/android/fingerprint/FingerprintManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->showSensorErrorDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setConfirmButton()V

    return-void
.end method

.method private identifyFinger()V
    .locals 4

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mIdentifyDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mIdentifyDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 241
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mIdentifyDialog:Landroid/app/Dialog;

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    if-eqz v0, :cond_1

    .line 245
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mKFPListener:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;

    const-string v2, "com.android.settings.permission.unlock"

    const/4 v3, 0x1

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/samsung/android/fingerprint/FingerprintManager;->showIdentifyDialog(Landroid/content/Context;Lcom/samsung/android/fingerprint/FingerprintIdentifyDialog$FingerprintListener;Ljava/lang/String;Z)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mIdentifyDialog:Landroid/app/Dialog;

    .line 246
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mIdentifyDialog:Landroid/app/Dialog;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$4;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 253
    :cond_1
    return-void
.end method

.method private isCheckedLocktype()Z
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->lockTypes:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 233
    const/4 v0, 0x1

    .line 235
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setCheckedEvent()V
    .locals 2

    .prologue
    .line 182
    const-string v1, "setCheckedEvent"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 184
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setConfirmButton()V

    .line 186
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$3;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    .line 206
    .local v0, "listener":Landroid/view/View$OnClickListener;
    const v1, 0x7f0f008a

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    const v1, 0x7f0f008b

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    const v1, 0x7f0f008c

    invoke-virtual {p0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    return-void
.end method

.method private setConfirmButton()V
    .locals 3

    .prologue
    .line 212
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->isCheckedLocktype()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const-string v0, "Confirm button enable"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->btnConfirm:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 215
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->text_confirm:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 216
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->img_confirm:Landroid/widget/ImageView;

    const v1, 0x7f02002d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 223
    :goto_0
    return-void

    .line 218
    :cond_0
    const-string v0, "Confirm button diable"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->btnConfirm:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 220
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->text_confirm:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->img_confirm:Landroid/widget/ImageView;

    const v1, 0x7f02002e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private setPasswordEnabledByPolicy(I)V
    .locals 4
    .param p1, "passWordQuality"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 226
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v3, 0x7f0f008a

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 227
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v3, 0x7f0f008b

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/high16 v0, 0x20000

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 228
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->lockTypes:Landroid/widget/RadioGroup;

    const v3, 0x7f0f008c

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v3, 0x10000

    if-gt p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 229
    return-void

    :cond_0
    move v0, v2

    .line 227
    goto :goto_0

    :cond_1
    move v1, v2

    .line 228
    goto :goto_1
.end method

.method private showSensorErrorDialog()V
    .locals 3

    .prologue
    .line 257
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x103012b

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 261
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialogBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c00bf

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 262
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialogBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0c00c0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 263
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialogBuilder:Landroid/app/AlertDialog$Builder;

    const v1, 0x104000a

    new-instance v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$5;

    invoke-direct {v2, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$5;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 268
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$6;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$6;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 274
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialog:Landroid/app/AlertDialog;

    .line 275
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 276
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityResult requestCode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " resultCode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " data "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 143
    sparse-switch p1, :sswitch_data_0

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 145
    :sswitch_0
    const/16 v0, 0x100

    if-ne p2, v0, :cond_1

    .line 146
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->showSensorErrorDialog()V

    goto :goto_0

    .line 147
    :cond_1
    const/16 v0, 0x1000

    if-ne p2, v0, :cond_0

    .line 148
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->identifyFinger()V

    goto :goto_0

    .line 152
    :sswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 153
    invoke-virtual {p0, p2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setResult(I)V

    .line 154
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->finish()V

    goto :goto_0

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x10 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/16 v6, 0x400

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 83
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : onCreate"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 84
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    const v4, 0x7f03001b

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setContentView(I)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/16 v5, 0x2000

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 89
    invoke-static {p0, v9}, Lcom/samsung/android/fingerprint/FingerprintManager;->getInstance(Landroid/content/Context;I)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;

    .line 91
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f080000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->isPhone:Z

    .line 92
    iget-boolean v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->isPhone:Z

    if-eqz v4, :cond_0

    .line 93
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v6, v6}, Landroid/view/Window;->setFlags(II)V

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 97
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 100
    const v4, 0x7f0f006c

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->btnConfirm:Landroid/widget/LinearLayout;

    .line 101
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->btnConfirm:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 103
    const v4, 0x7f0f0089

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioGroup;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->lockTypes:Landroid/widget/RadioGroup;

    .line 104
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->lockTypes:Landroid/widget/RadioGroup;

    invoke-virtual {v4}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    .line 106
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v0

    .line 108
    .local v0, "passWordQuality":I
    const/high16 v4, 0x10000

    if-gt v0, v4, :cond_1

    .line 109
    const v4, 0x7f0f008c

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 110
    .local v2, "pattern":Landroid/widget/RadioButton;
    invoke-virtual {v2, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 111
    const/4 v4, 0x3

    sput v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I

    .line 123
    .end local v2    # "pattern":Landroid/widget/RadioButton;
    :goto_0
    const v4, 0x7f0f006d

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->text_confirm:Landroid/widget/TextView;

    .line 124
    const v4, 0x7f0f003a

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->img_confirm:Landroid/widget/ImageView;

    .line 126
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->text_confirm:Landroid/widget/TextView;

    const v5, 0x7f0c0042

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 128
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->btnConfirm:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$2;

    invoke-direct {v5, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    return-void

    .line 112
    :cond_1
    const/high16 v4, 0x20000

    if-gt v0, v4, :cond_2

    .line 113
    const v4, 0x7f0f008b

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 114
    .local v3, "pin":Landroid/widget/RadioButton;
    invoke-virtual {v3, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 115
    sput v9, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I

    goto :goto_0

    .line 117
    .end local v3    # "pin":Landroid/widget/RadioButton;
    :cond_2
    const v4, 0x7f0f008a

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 118
    .local v1, "password":Landroid/widget/RadioButton;
    invoke-virtual {v1, v7}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 119
    sput v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->mCurrentLockTypeIdx:I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 162
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 168
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 164
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setResult(I)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->finish()V

    .line 166
    const/4 v0, 0x1

    goto :goto_0

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 173
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onResume"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 174
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 176
    const v0, 0x7f0c00fb

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 177
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setCheckedEvent()V

    .line 178
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getPasswordQuality()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerPlusActivity;->setPasswordEnabledByPolicy(I)V

    .line 179
    return-void
.end method
