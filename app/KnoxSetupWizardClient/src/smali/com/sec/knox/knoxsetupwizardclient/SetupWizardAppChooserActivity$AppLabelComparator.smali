.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$AppLabelComparator;
.super Ljava/lang/Object;
.source "SetupWizardAppChooserActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppLabelComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$1;

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$AppLabelComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;)I
    .locals 4
    .param p1, "lhs"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;
    .param p2, "rhs"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    .prologue
    .line 259
    iget-object v2, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "lhsLabel":Ljava/lang/String;
    iget-object v2, p2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;->activityName:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 261
    .local v1, "rhsLabel":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 255
    check-cast p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$AppLabelComparator;->compare(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;Lcom/sec/knox/knoxsetupwizardclient/SetupWizardAppChooserActivity$SelectableAppInfo;)I

    move-result v0

    return v0
.end method
