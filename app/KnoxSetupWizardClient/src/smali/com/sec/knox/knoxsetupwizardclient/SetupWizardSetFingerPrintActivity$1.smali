.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardSetFingerPrintActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v3, 0x1

    .line 73
    const-string v1, "mActionBarSwitch is Clicked"

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 75
    if-eqz p2, :cond_0

    .line 76
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/FingerprintManager;->getEnrolledFingers()I

    move-result v1

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/fingerprint/FingerprintManager;->notifyEnrollBegin()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 79
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.fingerprint.FingerprintDisclaimer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    const-string v1, "fingerIndex"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 81
    const-string v1, "previousStage"

    const-string v2, "ChooseLockGeneric"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    const-string v1, "confirm_credentials"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    const-string v1, "is_knox"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 84
    const-string v1, "startactivity : ChooseLockFingerprint "

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    const/16 v2, 0x3f6

    invoke-virtual {v1, v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 91
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->showSensorErrorDialog()V
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;->access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetFingerPrintActivity;)V

    goto :goto_0
.end method
