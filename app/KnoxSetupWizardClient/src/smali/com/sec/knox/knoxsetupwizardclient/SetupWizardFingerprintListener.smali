.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;
.super Ljava/lang/Object;
.source "SetupWizardFingerprintListener.java"

# interfaces
.implements Lcom/samsung/android/fingerprint/FingerprintIdentifyDialog$FingerprintListener;


# instance fields
.field private mCallback:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method public constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;->mCallback:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;

    .line 15
    return-void
.end method


# virtual methods
.method public onEvent(Lcom/samsung/android/fingerprint/FingerprintEvent;)V
    .locals 5
    .param p1, "evt"    # Lcom/samsung/android/fingerprint/FingerprintEvent;

    .prologue
    .line 20
    move-object v1, p1

    .line 22
    .local v1, "event":Lcom/samsung/android/fingerprint/FingerprintEvent;
    :try_start_0
    iget v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventId:I

    packed-switch v2, :pswitch_data_0

    .line 41
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 28
    :pswitch_1
    iget v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    if-nez v2, :cond_1

    .line 29
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;->mCallback:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;

    invoke-interface {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;->resultSuccess()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "KnoxSetupWizardClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFingerprintEvent: Error : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 30
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    iget v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventResult:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 31
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;->mCallback:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;

    invoke-interface {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;->resultFailed()V

    .line 32
    iget v2, v1, Lcom/samsung/android/fingerprint/FingerprintEvent;->eventStatus:I

    const/4 v3, 0x7

    if-ne v2, v3, :cond_0

    .line 33
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListener;->mCallback:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;

    invoke-interface {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;->sensorError()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 22
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
