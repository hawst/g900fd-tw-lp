.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$2;
.super Ljava/lang/Object;
.source "KnoxFolderAppInfoAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 279
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mIsUninstallMode:Z
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 280
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 281
    .local v0, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    const v2, 0x7f0f005c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 283
    .local v1, "ivIcon":Landroid/widget/ImageView;
    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 284
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->mActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->access$100(Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->ItemLongClick(Landroid/view/View;)Z

    .line 286
    .end local v0    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v1    # "ivIcon":Landroid/widget/ImageView;
    :cond_0
    const/4 v2, 0x0

    return v2
.end method
