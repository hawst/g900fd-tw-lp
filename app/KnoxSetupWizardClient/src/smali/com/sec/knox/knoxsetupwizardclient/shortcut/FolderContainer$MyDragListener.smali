.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyDragListener"
.end annotation


# instance fields
.field appInfo:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

.field destPosition:I

.field exited:Z

.field glowColor:I

.field isAdapterSet:Z

.field ivIcon:Landroid/widget/ImageView;

.field startScrollTime:J

.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 1558
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1559
    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->exited:Z

    .line 1560
    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->isAdapterSet:Z

    .line 1561
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    .line 1562
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->appInfo:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1563
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->ivIcon:Landroid/widget/ImageView;

    .line 1565
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->startScrollTime:J

    .line 1567
    iput v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->glowColor:I

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 26
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 1574
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v22

    packed-switch v22, :pswitch_data_0

    .line 1780
    :cond_0
    :goto_0
    const/16 v22, 0x1

    return v22

    .line 1577
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v23 .. v23}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v23

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    .line 1579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v22

    if-nez v22, :cond_1

    .line 1581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v23 .. v23}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v23

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->appInfo:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    const v23, 0x7f0f005c

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->ivIcon:Landroid/widget/ImageView;

    .line 1589
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->appInfo:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->isDragedItem:Z

    .line 1591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->appInfo:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->drawableToBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->createOutline(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->ivIcon:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->glowColor:I

    move/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 1593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    goto/16 :goto_0

    .line 1586
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v23 .. v23}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v23

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->appInfo:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->appInfo:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->pView:Landroid/view/View;

    move-object/from16 v22, v0

    const v23, 0x7f0f005c

    invoke-virtual/range {v22 .. v23}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ImageView;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->ivIcon:Landroid/widget/ImageView;

    goto/16 :goto_1

    .line 1597
    :pswitch_1
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->exited:Z

    goto/16 :goto_0

    .line 1601
    :pswitch_2
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->exited:Z

    goto/16 :goto_0

    .line 1606
    :pswitch_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v19

    .line 1607
    .local v19, "x1":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v21

    .line 1609
    .local v21, "y1":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1610
    .local v6, "curTime":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->startScrollTime:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0xfa

    add-long v22, v22, v24

    cmp-long v22, v6, v22

    if-ltz v22, :cond_0

    .line 1613
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move/from16 v1, v19

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/GridView;->pointToPosition(II)I

    move-result v4

    .line 1615
    .local v4, "curPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->topPosition:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    .line 1617
    move v10, v4

    .line 1618
    .local v10, "getSelection":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridYCount:I

    move/from16 v22, v0

    sub-int v10, v10, v22

    .line 1619
    if-ltz v10, :cond_3

    .line 1624
    move v12, v10

    .line 1626
    .local v12, "movePosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    new-instance v23, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;I)V

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    .line 1633
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->startScrollTime:J

    .line 1635
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v22

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "y1<topPosition getSelection:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1639
    .end local v10    # "getSelection":I
    .end local v12    # "movePosition":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->bottomPosition:I

    move/from16 v22, v0

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_3

    .line 1641
    move v10, v4

    .line 1642
    .restart local v10    # "getSelection":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridYCount:I

    move/from16 v22, v0

    add-int v10, v10, v22

    .line 1643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    if-gt v10, v0, :cond_3

    .line 1645
    move v12, v10

    .line 1647
    .restart local v12    # "movePosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    new-instance v23, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;I)V

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->post(Ljava/lang/Runnable;)Z

    .line 1656
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v22

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "y1>bottomPosition getSelection:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1657
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->startScrollTime:J

    goto/16 :goto_0

    .line 1662
    .end local v10    # "getSelection":I
    .end local v12    # "movePosition":I
    :cond_3
    if-ltz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v4, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-eq v4, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v22

    move/from16 v0, v22

    if-lt v4, v0, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/GridView;->getLastVisiblePosition()I

    move-result v22

    move/from16 v0, v22

    if-gt v4, v0, :cond_0

    .line 1671
    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    .line 1672
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_7

    .line 1673
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v22

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "1111 destPosition:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ",prevDestPosition:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ",movedAppPosition:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v24 .. v24}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v25

    invoke-virtual/range {v22 .. v25}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->displayGridIconsAnimation(III)V

    .line 1677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, -0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    invoke-virtual/range {v22 .. v24}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v22, v0

    add-int/lit8 v11, v22, -0x1

    .local v11, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-lt v11, v0, :cond_5

    .line 1680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    add-int/lit8 v23, v11, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v24 .. v24}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v24

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v11, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->displayGridIconsAnimation(III)V

    .line 1682
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v23, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    add-int/lit8 v25, v11, -0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    if-nez v22, :cond_4

    const/16 v22, 0x0

    :goto_3
    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1683
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v22

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "destPosition:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ",prevDestPosition:"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    add-int/lit8 v24, v11, -0x1

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ",dragOffset.get(i-1):"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v24, v0

    add-int/lit8 v25, v11, -0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1678
    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_2

    .line 1682
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    add-int/lit8 v25, v11, -0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    move-object/from16 v0, v22

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/Integer;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    move-result v22

    goto :goto_3

    .line 1686
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v24 .. v24}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v22 .. v24}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1703
    .end local v11    # "i":I
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    goto/16 :goto_0

    .line 1688
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_6

    .line 1691
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v25 .. v25}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v25

    invoke-virtual/range {v22 .. v25}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->displayGridIconsAnimation(III)V

    .line 1693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v25, v0

    add-int/lit8 v25, v25, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    invoke-virtual/range {v22 .. v24}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    move/from16 v22, v0

    add-int/lit8 v11, v22, 0x1

    .restart local v11    # "i":I
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    move/from16 v0, v22

    if-gt v11, v0, :cond_8

    .line 1696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    add-int/lit8 v23, v11, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v24 .. v24}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v24

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v11, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->displayGridIconsAnimation(III)V

    .line 1697
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v24, v0

    add-int/lit8 v25, v11, 0x1

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    invoke-virtual/range {v22 .. v24}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1694
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 1701
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v23, v0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v24 .. v24}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v22 .. v24}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 1717
    .end local v4    # "curPosition":I
    .end local v6    # "curTime":J
    .end local v11    # "i":I
    .end local v19    # "x1":I
    .end local v21    # "y1":I
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;
    invoke-static/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/GridView;

    move-result-object v22

    if-eqz v22, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;
    invoke-static/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/GridView;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/widget/GridView;->getVisibility()I

    move-result v22

    if-nez v22, :cond_9

    .line 1718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;
    invoke-static/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/GridView;

    move-result-object v22

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1719
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v18

    .line 1720
    .local v18, "x":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Math;->round(F)I

    move-result v20

    .line 1721
    .local v20, "y":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/GridView;->pointToPosition(II)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    .line 1723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v23 .. v23}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v23

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1725
    .local v13, "sourceAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    if-ltz v22, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->movedAppPosition:I
    invoke-static/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)I

    move-result v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_b

    .line 1726
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v23, v0

    invoke-interface/range {v22 .. v23}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 1728
    .local v5, "destinationAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getDbId()J

    move-result-wide v16

    .line 1729
    .local v16, "sourceDbID":J
    invoke-virtual {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getDbId()J

    move-result-wide v8

    .line 1730
    .local v8, "destDbID":J
    invoke-virtual {v13}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getPersonaId()I

    move-result v22

    move/from16 v0, v22

    int-to-long v14, v0

    .line 1731
    .local v14, "personaID":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2, v8, v9}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->reOrderAfterDrop(JJ)V

    .line 1733
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->loadAppList()V

    .line 1734
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1738
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/GridView;->invalidate()V

    .line 1739
    const/16 v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->isAdapterSet:Z

    .line 1740
    const/16 v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    .line 1748
    .end local v5    # "destinationAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v8    # "destDbID":J
    .end local v14    # "personaID":J
    .end local v16    # "sourceDbID":J
    :cond_a
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    .line 1750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setOriginalGridView()V

    .line 1752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDragDropBG:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1753
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    goto/16 :goto_0

    .line 1743
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->exited:Z

    move/from16 v22, v0

    if-nez v22, :cond_a

    .line 1744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_6

    .line 1757
    .end local v13    # "sourceAppInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v18    # "x":I
    .end local v20    # "y":I
    :pswitch_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->exited:Z

    move/from16 v22, v0

    if-nez v22, :cond_d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->destPosition:I

    move/from16 v22, v0

    if-gez v22, :cond_e

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->isAdapterSet:Z

    move/from16 v22, v0

    if-nez v22, :cond_e

    .line 1760
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->loadAppList()V

    .line 1761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1762
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/GridView;->invalidate()V

    .line 1764
    :cond_e
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->exited:Z

    .line 1765
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->isAdapterSet:Z

    .line 1766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    const/16 v23, -0x1

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->prevDestPosition:I

    .line 1767
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v11, v0, :cond_f

    .line 1768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->dragOffset:Ljava/util/HashMap;

    move-object/from16 v22, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    invoke-virtual/range {v22 .. v24}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1767
    add-int/lit8 v11, v11, 0x1

    goto :goto_7

    .line 1771
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, v22

    iput-boolean v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->isDragMode:Z

    .line 1772
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setOriginalGridView()V

    .line 1773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;
    invoke-static/range {v22 .. v22}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/GridView;

    move-result-object v22

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->setVisibility(I)V

    .line 1774
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDragDropBG:Landroid/widget/ImageView;

    move-object/from16 v22, v0

    const/16 v23, 0x8

    invoke-virtual/range {v22 .. v23}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1775
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$MyDragListener;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-virtual/range {v22 .. v23}, Landroid/widget/GridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    goto/16 :goto_0

    .line 1574
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
