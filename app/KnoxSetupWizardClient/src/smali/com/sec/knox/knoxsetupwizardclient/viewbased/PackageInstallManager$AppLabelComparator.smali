.class Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$AppLabelComparator;
.super Ljava/lang/Object;
.source "PackageInstallManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppLabelComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$AppLabelComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;)I
    .locals 4
    .param p1, "lhs"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;
    .param p2, "rhs"    # Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    .prologue
    .line 79
    iget-object v0, p1, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/String;

    .line 80
    .local v0, "lhsLabel":Ljava/lang/String;
    iget-object v1, p2, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->appName:Ljava/lang/CharSequence;

    check-cast v1, Ljava/lang/String;

    .line 81
    .local v1, "rhsLabel":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 75
    check-cast p1, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$AppLabelComparator;->compare(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;)I

    move-result v0

    return v0
.end method
