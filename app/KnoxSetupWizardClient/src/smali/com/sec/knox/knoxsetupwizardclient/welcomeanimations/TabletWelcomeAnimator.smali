.class public Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;
.super Ljava/lang/Object;
.source "TabletWelcomeAnimator.java"

# interfaces
.implements Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;


# instance fields
.field private KnoxBackground:Landroid/widget/LinearLayout;

.field private agreeChkbxSlideInLeft:Landroid/view/animation/AnimationSet;

.field private backFadeIn:Landroid/view/animation/AlphaAnimation;

.field private bottomButtonLayout:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;

.field private bottomPanelSlideUpAndFadeIn:Landroid/view/animation/AnimationSet;

.field private confirmBtnSlideInLeft:Landroid/view/animation/AnimationSet;

.field private eulaAgree:Landroid/widget/LinearLayout;

.field private eulaContentLayout:Landroid/widget/ScrollView;

.field private eulaLayout:Landroid/widget/RelativeLayout;

.field private eulaSlideIn:Landroid/view/animation/AnimationSet;

.field private eulaSlideOut:Landroid/view/animation/Animation;

.field private eulaStartButton:Landroid/widget/LinearLayout;

.field private knoxAndIconFadeIn:Landroid/view/animation/AnimationSet;

.field private knoxBottomDummyPanel:Landroid/widget/RelativeLayout;

.field private knoxIcon:Landroid/widget/ImageView;

.field private knoxNextButton:Landroid/widget/LinearLayout;

.field private knoxTitle:Landroid/widget/TextView;

.field private knoxTitlePanel:Landroid/widget/LinearLayout;

.field private txt_eula:Landroid/widget/TextView;

.field private welcomeFadeIn:Landroid/view/animation/AnimationSet;

.field private welcomeMessage:Landroid/widget/TextView;

.field private welcomeSlideIn:Landroid/view/animation/Animation;

.field private welcomeSlideOutLeft:Landroid/view/animation/AnimationSet;

.field private welcomeTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->initViews(Landroid/app/Activity;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->initAnimations(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private initEulaAnimations(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v4, 0x7f040001

    .line 102
    invoke-static {p1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideIn:Landroid/view/animation/AnimationSet;

    .line 103
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x21

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 104
    invoke-static {p1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->agreeChkbxSlideInLeft:Landroid/view/animation/AnimationSet;

    .line 105
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->agreeChkbxSlideInLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x21

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 106
    invoke-static {p1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->confirmBtnSlideInLeft:Landroid/view/animation/AnimationSet;

    .line 107
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->confirmBtnSlideInLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x21

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 108
    const v0, 0x7f040002

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideOut:Landroid/view/animation/Animation;

    .line 109
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideOut:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator$1;

    invoke-direct {v1, p0}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 115
    return-void
.end method

.method private initViews(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x4

    .line 63
    const v0, 0x7f0f0094

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/RelativeLayout;

    .line 64
    const v0, 0x7f0f006c

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxNextButton:Landroid/widget/LinearLayout;

    .line 65
    const v0, 0x7f0f0093

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxTitlePanel:Landroid/widget/LinearLayout;

    .line 66
    const v0, 0x7f0f0092

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->KnoxBackground:Landroid/widget/LinearLayout;

    .line 67
    const v0, 0x7f0f00a8

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxTitle:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0f00a9

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxIcon:Landroid/widget/ImageView;

    .line 69
    const v0, 0x7f0f0096

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeTitle:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0f0097

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeMessage:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0f0098

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    .line 73
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 74
    const v0, 0x7f0f000c

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->bottomButtonLayout:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;

    .line 75
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->bottomButtonLayout:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;->setVisibility(I)V

    .line 76
    const v0, 0x7f0f00a5

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    .line 77
    const v0, 0x7f0f009c

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaContentLayout:Landroid/widget/ScrollView;

    .line 78
    const v0, 0x7f0f009e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaAgree:Landroid/widget/LinearLayout;

    .line 79
    const v0, 0x7f0f0007

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->txt_eula:Landroid/widget/TextView;

    .line 80
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->txt_eula:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 81
    return-void
.end method

.method private initWelcomeAnimations(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v1, 0x7f040004

    .line 90
    const v0, 0x7f040009

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideOutLeft:Landroid/view/animation/AnimationSet;

    .line 91
    const v0, 0x7f040006

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->bottomPanelSlideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    .line 92
    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxAndIconFadeIn:Landroid/view/animation/AnimationSet;

    .line 93
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxAndIconFadeIn:Landroid/view/animation/AnimationSet;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 94
    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeFadeIn:Landroid/view/animation/AnimationSet;

    .line 95
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeFadeIn:Landroid/view/animation/AnimationSet;

    const-wide/16 v2, 0x8fc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 96
    const v0, 0x7f040008

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideIn:Landroid/view/animation/Animation;

    .line 97
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->backFadeIn:Landroid/view/animation/AlphaAnimation;

    .line 98
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->backFadeIn:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x294

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 99
    return-void
.end method


# virtual methods
.method public initAnimations(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->initWelcomeAnimations(Landroid/content/Context;)V

    .line 86
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->initEulaAnimations(Landroid/content/Context;)V

    .line 87
    return-void
.end method

.method public showBackgroundImg()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->KnoxBackground:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->backFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 191
    return-void
.end method

.method public showEulaWithoutAnimation()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 173
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 175
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    const/high16 v1, 0x43480000    # 200.0f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setX(F)V

    .line 180
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    .line 182
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->bottomButtonLayout:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;

    invoke-virtual {v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;->setVisibility(I)V

    .line 185
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaContentLayout:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 186
    return-void
.end method

.method public startBackAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxNextButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 152
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeMessage:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaContentLayout:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 157
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 158
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaAgree:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 159
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaAgree:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 160
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaContentLayout:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setClickable(Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 164
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 165
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 167
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxNextButton:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 168
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxNextButton:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 169
    return-void
.end method

.method public startDummyPanelFadeOut()V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public startEulaAnimation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 137
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->confirmBtnSlideInLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 138
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaContentLayout:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaSlideIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 139
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaAgree:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->agreeChkbxSlideInLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 140
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaStartButton:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaAgree:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->agreeChkbxSlideInLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 142
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 143
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->bottomButtonLayout:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;

    invoke-virtual {v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBottomBar;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->eulaContentLayout:Landroid/widget/ScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 147
    return-void
.end method

.method public startHideWelcomeAnimation()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxNextButton:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideOutLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 130
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideOutLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 131
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideOutLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 132
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeSlideOutLeft:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 133
    return-void
.end method

.method public startShowWelcomeAnimation()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->bottomPanelSlideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 120
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxAndIconFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 121
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->knoxAndIconFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 122
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 123
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeMessage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/TabletWelcomeAnimator;->welcomeFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124
    return-void
.end method
