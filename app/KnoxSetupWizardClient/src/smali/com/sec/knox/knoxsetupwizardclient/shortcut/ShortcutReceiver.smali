.class public Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ShortcutReceiver.java"


# static fields
.field private static ACTION_BADGE_UPDATE:Ljava/lang/String;

.field private static ACTION_KNOX_INSTALL_SHORTCUT:Ljava/lang/String;

.field private static ACTION_KNOX_UNINSTALL_SHORTCUT:Ljava/lang/String;


# instance fields
.field private final ACTION_INSTALL_WIDGET:Ljava/lang/String;

.field private final ACTION_SHORTCUT_MIGRATION_DONE:Ljava/lang/String;

.field private final ACTION_SHORTUCT_MIGRATION:Ljava/lang/String;

.field private final ACTION_USER_ADDED:Ljava/lang/String;

.field private final ACTION_USER_REMOVED:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

.field mPM:Landroid/os/PersonaManager;

.field mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "com.sec.knox.action.UNINSTALL_SHORTCUT"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_KNOX_UNINSTALL_SHORTCUT:Ljava/lang/String;

    .line 38
    const-string v0, "com.sec.knox.action.INSTALL_SHORTCUT"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_KNOX_INSTALL_SHORTCUT:Ljava/lang/String;

    .line 39
    const-string v0, "com.sec.knox.action.badge_update"

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_BADGE_UPDATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 41
    const-string v0, "android.intent.action.USER_ADDED"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_USER_ADDED:Ljava/lang/String;

    .line 42
    const-string v0, "android.intent.action.USER_REMOVED"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_USER_REMOVED:Ljava/lang/String;

    .line 43
    const-string v0, "com.sec.knox.action.INSTALL_WIDGET"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_INSTALL_WIDGET:Ljava/lang/String;

    .line 45
    const-string v0, "com.sec.knox.action.SHORTCUT_MIGRATION_FOR_2_3_0"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_SHORTUCT_MIGRATION:Ljava/lang/String;

    .line 46
    const-string v0, "com.sec.knox.action.SHORTCUT_MIGRATION_DONE"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_SHORTCUT_MIGRATION_DONE:Ljava/lang/String;

    return-void
.end method

.method private doCreateShortcut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;I)V
    .locals 5
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "KnoxName"    # Ljava/lang/String;
    .param p3, "componentName"    # Ljava/lang/String;
    .param p4, "shortcutName"    # Ljava/lang/String;
    .param p5, "shortcutUri"    # Ljava/lang/String;
    .param p6, "icon"    # Landroid/graphics/Bitmap;
    .param p7, "badgeClassName"    # Ljava/lang/String;
    .param p8, "badgeCount"    # I

    .prologue
    .line 344
    new-instance v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    invoke-direct {v1}, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;-><init>()V

    .line 345
    .local v1, "scModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    const-string v2, "ShortcutReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doCreateShortcut "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    iput-object p3, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    .line 348
    iput-object p4, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    .line 349
    iput-object p5, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->uri:Ljava/lang/String;

    .line 350
    iput-object p6, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->icon:Landroid/graphics/Bitmap;

    .line 352
    const/4 v0, 0x1

    .line 354
    .local v0, "personaId":I
    const-string v2, "KNOX"

    invoke-virtual {p2, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 355
    const/4 v0, 0x1

    .line 359
    :goto_0
    iput v0, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    .line 360
    iput p8, v1, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->badgeCount:I

    .line 362
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v2, v1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->createShortcut(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;)J

    .line 363
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->addRemoveAppShortcutToPersonalHome(IZ)V

    .line 365
    return-void

    .line 357
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private doRemoveShortcut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "componentName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;

    .prologue
    .line 369
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    invoke-direct {v0}, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;-><init>()V

    .line 370
    .local v0, "scModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    const-string v1, "ShortcutReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "doRemoveShortcut:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    if-eqz p2, :cond_0

    const-string v1, "KnoxShortcutActivity"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 373
    const/4 v1, 0x1

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    .line 378
    :goto_0
    iput-object p2, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    .line 379
    iput-object p3, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    .line 381
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    invoke-virtual {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->removeShortcut(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;)I

    .line 384
    return-void

    .line 375
    :cond_0
    const/4 v1, 0x2

    iput v1, v0, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    goto :goto_0
.end method

.method private doUpdateBadgeCount(ILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[I)V
    .locals 6
    .param p1, "UserId"    # I
    .param p2, "KnoxName"    # Ljava/lang/String;
    .param p3, "pkgNames"    # [Ljava/lang/String;
    .param p4, "classNames"    # [Ljava/lang/String;
    .param p5, "counts"    # [I

    .prologue
    .line 387
    const-string v4, "ShortcutReceiver"

    const-string v5, "doUpdateBadgeCount"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v4, p1}, Landroid/os/PersonaManager;->getPersonaInfo(I)Landroid/content/pm/PersonaInfo;

    move-result-object v2

    .line 396
    .local v2, "info":Landroid/content/pm/PersonaInfo;
    :try_start_0
    new-instance v3, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    invoke-direct {v3}, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;-><init>()V

    .line 397
    .local v3, "scModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    iput p1, v3, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->personaId:I

    .line 398
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p3

    if-ge v1, v4, :cond_0

    .line 399
    aget-object v4, p3, v1

    iput-object v4, v3, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    .line 400
    aget v4, p5, v1

    iput v4, v3, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->badgeCount:I

    .line 401
    aget-object v4, p4, v1

    iput-object v4, v3, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    .line 403
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->modifyShortcut(Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;Z)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 405
    .end local v1    # "i":I
    .end local v3    # "scModel":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :catch_0
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 426
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method

.method private enableDisableShortcutSMS(Ljava/lang/String;)V
    .locals 10
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    .line 205
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v5

    .line 206
    .local v5, "pkgState":I
    const-string v6, "ShortcutReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "app state="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    const-string v6, "package"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v4

    .line 208
    .local v4, "iPM":Landroid/content/pm/IPackageManager;
    new-instance v0, Landroid/content/ComponentName;

    const-string v6, "com.sec.knox.shortcutsms"

    const-string v7, "com.sec.knox.shortcutsms.PhoneShortcut"

    invoke-direct {v0, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    .local v0, "componentCallShortcut":Landroid/content/ComponentName;
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mPM:Landroid/os/PersonaManager;

    invoke-virtual {v6}, Landroid/os/PersonaManager;->getPersonaIds()[I

    move-result-object v2

    .line 212
    .local v2, "existingPersonas":[I
    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 213
    :try_start_0
    const-string v6, "ShortcutReceiver"

    const-string v7, "disable ShortcutSms-phone component"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, v2

    if-ge v3, v6, :cond_2

    .line 216
    const/4 v6, 0x2

    const/4 v7, 0x1

    aget v8, v2, v3

    invoke-interface {v4, v0, v6, v7, v8}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V

    .line 215
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 218
    .end local v3    # "i":I
    :cond_0
    if-eq v5, v9, :cond_1

    if-nez v5, :cond_2

    .line 219
    :cond_1
    const-string v6, "ShortcutReceiver"

    const-string v7, "enable ShortcutSms-phone component"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    array-length v6, v2

    if-ge v3, v6, :cond_2

    .line 221
    const/4 v6, 0x1

    const/4 v7, 0x1

    aget v8, v2, v3

    invoke-interface {v4, v0, v6, v7, v8}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 224
    .end local v3    # "i":I
    :catch_0
    move-exception v1

    .line 225
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 227
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    return-void
.end method

.method private onCreateShortcut(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 230
    const-string v0, "ShortcutReceiver"

    const-string v1, "onCreateShortcut() START"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const/4 v7, 0x0

    .line 233
    .local v7, "badgeClassName":Ljava/lang/String;
    const/4 v8, 0x0

    .line 234
    .local v8, "badgeCount":I
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    .line 237
    .local v9, "bundle":Landroid/os/Bundle;
    const-string v0, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Intent;

    .line 239
    .local v10, "intentLaunch":Landroid/content/Intent;
    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 240
    .local v3, "componentName":Ljava/lang/String;
    const-string v0, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 241
    .local v4, "name":Ljava/lang/String;
    const-string v0, "android.intent.extra.shortcut.ICON"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 243
    .local v6, "icon":Landroid/graphics/Bitmap;
    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 244
    .local v11, "shortcutInfo":Landroid/os/Bundle;
    const-string v0, "shortcutIntent"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 245
    .local v5, "shortcutUri":Ljava/lang/String;
    const-string v0, "KnoxName"

    invoke-virtual {v11, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 247
    .local v2, "KnoxName":Ljava/lang/String;
    const-string v0, "class_names"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 248
    const-string v0, "counts"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 250
    if-nez v2, :cond_1

    .line 251
    if-eqz v3, :cond_0

    const-string v0, "KnoxShortcutActivity"

    invoke-virtual {v3, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const-string v2, "KNOX"

    .line 256
    :goto_0
    if-nez v6, :cond_2

    .line 257
    const-string v0, "ShortcutReceiver"

    const-string v1, "this is not intent expected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :goto_1
    return-void

    .line 254
    :cond_0
    const-string v2, "KNOX II"

    goto :goto_0

    .line 261
    :cond_1
    const-string v0, "ShortcutReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "knox name : "

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object v0, p0

    move-object v1, p1

    .line 266
    invoke-direct/range {v0 .. v8}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->doCreateShortcut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/lang/String;I)V

    .line 268
    const-string v0, "ShortcutReceiver"

    const-string v1, "onCreateShortcut() END"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private onRemoveShortcut(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 273
    const-string v3, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 274
    .local v1, "intentLaunch":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, "componentName":Ljava/lang/String;
    const-string v3, "android.intent.extra.shortcut.NAME"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 277
    .local v2, "name":Ljava/lang/String;
    invoke-direct {p0, p1, v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->doRemoveShortcut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    return-void
.end method

.method private onUpdateBadgeCount(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 282
    const/4 v5, 0x0

    .line 283
    .local v5, "count":[I
    const/4 v3, 0x0

    .line 284
    .local v3, "pkgname":[Ljava/lang/String;
    const/4 v4, 0x0

    .line 285
    .local v4, "clsName":[Ljava/lang/String;
    const/4 v2, 0x0

    .line 286
    .local v2, "KnoxName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 288
    .local v1, "UserId":I
    const-string v0, "com.sec.knox.bridge.BadgeData"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 290
    .local v6, "bundle":Landroid/os/Bundle;
    const-string v0, "ShortcutReceiver"

    const-string v7, "onUpdateBadgeCount() START"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    if-nez v6, :cond_0

    .line 293
    const-string v0, "ShortcutReceiver"

    const-string v7, "onUpdateBadgeCount() invalid param!"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :goto_0
    return-void

    .line 297
    :cond_0
    const-string v0, "pkg_names"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 298
    const-string v0, "class_names"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 299
    const-string v0, "counts"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v5

    .line 300
    const-string v0, "KnoxName"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 301
    const-string v0, "UserId"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move-object v0, p0

    .line 303
    invoke-direct/range {v0 .. v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->doUpdateBadgeCount(ILjava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[I)V

    .line 305
    const-string v0, "ShortcutReceiver"

    const-string v7, "onUpdateBadgeCount() END"

    invoke-static {v0, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 52
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mContext:Landroid/content/Context;

    .line 53
    invoke-static/range {p1 .. p1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    .line 54
    invoke-static/range {p1 .. p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getInstance(Landroid/content/Context;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    .line 55
    const-string v17, "persona"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/os/PersonaManager;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mPM:Landroid/os/PersonaManager;

    .line 56
    if-eqz p2, :cond_1

    .line 57
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " ShortcutReceiver onReceive() intent: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :try_start_0
    sget-object v17, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_KNOX_INSTALL_SHORTCUT:Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 61
    invoke-direct/range {p0 .. p2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->onCreateShortcut(Landroid/content/Context;Landroid/content/Intent;)V

    .line 192
    :cond_0
    :goto_0
    const-string v17, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 193
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v11

    .line 195
    .local v11, "packageName":Ljava/lang/String;
    const-string v17, "com.android.contacts"

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 196
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->enableDisableShortcutSMS(Ljava/lang/String;)V

    .line 203
    .end local v11    # "packageName":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 62
    :cond_2
    sget-object v17, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_KNOX_UNINSTALL_SHORTCUT:Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 63
    invoke-direct/range {p0 .. p2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->onRemoveShortcut(Landroid/content/Context;Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 199
    :catch_0
    move-exception v5

    .line 200
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 64
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    const-string v17, "android.intent.action.USER_ADDED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 65
    const-string v17, "ShortcutReceiver"

    const-string v18, "ACTION_USER_ADDED() comming!!!!!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-string v17, "android.intent.extra.user_handle"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 67
    .local v15, "userId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->addContainer(I)V

    goto :goto_0

    .line 69
    .end local v15    # "userId":I
    :cond_4
    const-string v17, "android.intent.action.USER_REMOVED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 70
    const-string v17, "ShortcutReceiver"

    const-string v18, "ACTION_USER_REMOVED() comming!!!!!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    const-string v17, "android.intent.extra.user_handle"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 72
    .restart local v15    # "userId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->removeContainer(I)V

    goto/16 :goto_0

    .line 73
    .end local v15    # "userId":I
    :cond_5
    const-string v17, "com.sec.knox.action.INSTALL_WIDGET"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 75
    const-string v17, "ShortcutReceiver"

    const-string v18, "ACTION_INSTALL_WIDGET() comming!!!!!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const-string v17, "personaId"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 79
    .local v12, "personaId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v12, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setCreateShortcutFlag(II)V

    goto/16 :goto_0

    .line 81
    .end local v12    # "personaId":I
    :cond_6
    sget-object v17, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->ACTION_BADGE_UPDATE:Ljava/lang/String;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 82
    invoke-direct/range {p0 .. p2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->onUpdateBadgeCount(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 83
    :cond_7
    const-string v17, "com.sec.knox.action.KNOX_LAYOUT_CHANGED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 85
    const-string v17, "android.intent.extra.user_handle"

    const/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 86
    .restart local v15    # "userId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->addContainer(I)V

    .line 87
    const-string v17, "ShortcutReceiver"

    const-string v18, "ACTION_KNOX_LAYOUT_CHANGED"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->deleteShortctDB(I)V

    goto/16 :goto_0

    .line 90
    .end local v15    # "userId":I
    :cond_8
    const-string v17, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f

    .line 97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getIsShortcutMigrationFor_2_3_0_Done()Z

    move-result v9

    .line 99
    .local v9, "isShortcutMigrationDone":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/os/PersonaManager;->getKnoxContainerVersion(Landroid/content/Context;)Landroid/os/PersonaManager$KnoxContainerVersion;

    move-result-object v16

    .line 100
    .local v16, "version":Landroid/os/PersonaManager$KnoxContainerVersion;
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " KnoxContainerVersion "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    sget-object v17, Landroid/os/PersonaManager$KnoxContainerVersion;->KNOX_CONTAINER_VERSION_2_3_0:Landroid/os/PersonaManager$KnoxContainerVersion;

    invoke-virtual/range {v16 .. v17}, Landroid/os/PersonaManager$KnoxContainerVersion;->compareTo(Ljava/lang/Enum;)I

    move-result v17

    if-ltz v17, :cond_e

    if-nez v9, :cond_e

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mPM:Landroid/os/PersonaManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-nez v17, :cond_9

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setIsShortcutMigrationFor_2_3_0_Done(Z)Z

    goto/16 :goto_1

    .line 109
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->updateWidgetStatus()V

    .line 111
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v15

    .line 112
    .restart local v15    # "userId":I
    const-string v17, "ShortcutReceiver"

    const-string v18, " SHORTCUT MIGRATION REQUIRED "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 114
    :try_start_2
    new-instance v14, Landroid/content/Intent;

    invoke-direct {v14}, Landroid/content/Intent;-><init>()V

    .line 115
    .local v14, "shortcutIntent":Landroid/content/Intent;
    const-string v17, "com.sec.knox.action.SHORTCUT_MIGRATION_FOR_2_3_0"

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 121
    .local v13, "shortcutInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const/4 v4, 0x0

    .line 122
    .local v4, "c":Landroid/database/Cursor;
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " USER ID "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 124
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->fetchAllShortcuts()Landroid/database/Cursor;

    move-result-object v4

    .line 125
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " SETUP WIZARD DB ENTRIES "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_a
    :goto_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->getShortcutInfo(Landroid/database/Cursor;)Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;

    move-result-object v10

    .line 129
    .local v10, "m":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 130
    .local v3, "bundle":Landroid/os/Bundle;
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " SHORTCUT NAME "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget-object v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const-string v17, "shortcutName"

    iget-object v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " uri fetched  "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget-object v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->uri:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-string v17, "uri"

    iget-object v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->uri:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v17, "badgeCount"

    iget v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->badgeCount:I

    move/from16 v18, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 136
    iget-object v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "Settings"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 137
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "adding shortcut "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget-object v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->shortcutName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " package:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget-object v0, v10, Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_2

    .line 141
    .end local v3    # "bundle":Landroid/os/Bundle;
    .end local v10    # "m":Lcom/sec/knox/knoxsetupwizardclient/db/ShortCutModel;
    :catch_1
    move-exception v17

    .line 144
    if-eqz v4, :cond_b

    .line 145
    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 149
    :cond_b
    :goto_3
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 150
    .restart local v3    # "bundle":Landroid/os/Bundle;
    const-string v17, "userId"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    const-string v17, "shortcutInfo"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v13}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 152
    const-string v17, "movefromfolder"

    const-string v18, "move"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {v14, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 157
    const-string v17, "ShortcutReceiver"

    const-string v18, " SENDING BROADCAST TO MIGRATE SHORTCUTS "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 160
    .end local v3    # "bundle":Landroid/os/Bundle;
    .end local v4    # "c":Landroid/database/Cursor;
    .end local v13    # "shortcutInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v14    # "shortcutIntent":Landroid/content/Intent;
    :catch_2
    move-exception v5

    .line 165
    .restart local v5    # "e":Ljava/lang/Exception;
    :try_start_5
    const-string v17, "ShortcutReceiver"

    const-string v18, " EXCEPTION "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setIsShortcutMigrationFor_2_3_0_Done(Z)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    .line 144
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v4    # "c":Landroid/database/Cursor;
    .restart local v13    # "shortcutInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .restart local v14    # "shortcutIntent":Landroid/content/Intent;
    :cond_c
    if-eqz v4, :cond_b

    .line 145
    :try_start_6
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    goto :goto_3

    .line 144
    :catchall_0
    move-exception v17

    if-eqz v4, :cond_d

    .line 145
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_d
    throw v17
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    .line 169
    .end local v4    # "c":Landroid/database/Cursor;
    .end local v13    # "shortcutInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v14    # "shortcutIntent":Landroid/content/Intent;
    .end local v15    # "userId":I
    :cond_e
    :try_start_7
    const-string v17, "ShortcutReceiver"

    const-string v18, " shortcut migration not required "

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 172
    .end local v9    # "isShortcutMigrationDone":Z
    .end local v16    # "version":Landroid/os/PersonaManager$KnoxContainerVersion;
    :cond_f
    const-string v17, "com.sec.android.app.camera.CAMERA_STOP_BY_EAS"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_10

    .line 173
    const-string v17, "ShortcutReceiver"

    const-string v18, "com.sec.android.app.camera.CAMERA_STOP_BY_EAS comming!!!!!"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v17

    const/16 v18, 0x64

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_0

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0c0106

    const/16 v19, 0x0

    invoke-static/range {v17 .. v19}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 177
    :cond_10
    const-string v17, "com.sec.knox.action.SHORTCUT_MIGRATION_DONE"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 178
    const-string v17, "ShortcutReceiver"

    const-string v18, "ACTION_SHORTCUT_MIGRATION_DONE"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mPM:Landroid/os/PersonaManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/os/PersonaManager;->getPersonaIds()[I

    move-result-object v8

    .line 181
    .local v8, "ids":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_4
    array-length v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v6, v0, :cond_11

    .line 183
    const-string v17, "ShortcutReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " deleting folders for  "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    aget v19, v8, v6

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    aget v18, v8, v6

    invoke-virtual/range {v17 .. v18}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getKnoxid(I)I

    move-result v7

    .line 185
    .local v7, "id":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->deleteShortcuts(I)I

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->deleteShortcutFolderPref(I)V

    .line 181
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 190
    .end local v7    # "id":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/ShortcutReceiver;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->setIsShortcutMigrationFor_2_3_0_Done(Z)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_0
.end method
