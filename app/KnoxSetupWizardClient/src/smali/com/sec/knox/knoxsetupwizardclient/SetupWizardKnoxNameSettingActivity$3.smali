.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;
.super Ljava/lang/Object;
.source "SetupWizardKnoxNameSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->init(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 319
    const-string v0, "KnoxSetupWizardClient"

    const-string v1, "restore button clicked"

    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->lockRotation()V
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)V

    .line 322
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 323
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;

    const/4 v2, 0x1

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->createPersona(Landroid/content/Context;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardKnoxNameSettingActivity;Landroid/content/Context;Z)V

    .line 325
    return-void
.end method
