.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;
.super Landroid/app/Fragment;
.source "SetupWizardSetPatternActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ChooseLockPatternFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;,
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;,
        Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;
    }
.end annotation


# instance fields
.field private final ACTIVITY_NAME:Ljava/lang/String;

.field private final mAnimatePattern:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;"
        }
    .end annotation
.end field

.field protected mChooseNewLockPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

.field protected mChosenPattern:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/internal/widget/LockPatternView$Cell;",
            ">;"
        }
    .end annotation
.end field

.field private final mClearPatternRunnable:Ljava/lang/Runnable;

.field private mFooterLeftButton:Landroid/widget/LinearLayout;

.field private mFooterLeftText:Landroid/widget/TextView;

.field private mFooterRightButton:Landroid/widget/LinearLayout;

.field private mFooterRightText:Landroid/widget/TextView;

.field protected mFooterText:Landroid/widget/TextView;

.field protected mHeaderText:Landroid/widget/TextView;

.field protected mLockPatternView:Lcom/android/internal/widget/LockPatternView;

.field private mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 99
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 101
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->ACTIVITY_NAME:Ljava/lang/String;

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChosenPattern:Ljava/util/List;

    .line 127
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/internal/widget/LockPatternView$Cell;

    invoke-static {v3, v3}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v3, v2}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v2, v2}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x3

    invoke-static {v4, v2}, Lcom/android/internal/widget/LockPatternView$Cell;->of(II)Lcom/android/internal/widget/LockPatternView$Cell;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/collect/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mAnimatePattern:Ljava/util/List;

    .line 154
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$1;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChooseNewLockPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    .line 309
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    .line 311
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$2;-><init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mClearPatternRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mClearPatternRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;)Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftButton:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterRightButton:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private postClearPatternRunnable()V
    .locals 4

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mClearPatternRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 548
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mClearPatternRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/widget/LockPatternView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 549
    return-void
.end method

.method private saveChosenPatternAndFinish()V
    .locals 2

    .prologue
    .line 565
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChosenPattern:Ljava/util/List;

    invoke-static {v0}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPassword(Ljava/lang/String;)V

    .line 566
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 567
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 568
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 139
    packed-switch p1, :pswitch_data_0

    .line 148
    :goto_0
    return-void

    .line 141
    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 142
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 143
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 145
    :cond_0
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V

    goto :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x37
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftButton:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_3

    .line 400
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->leftMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;->Retry:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    if-ne v0, v1, :cond_1

    .line 401
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChosenPattern:Ljava/util/List;

    .line 402
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView;->clearPattern()V

    .line 403
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->leftMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;->Cancel:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    if-ne v0, v1, :cond_2

    .line 406
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 407
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 409
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "left footer button pressed, but stage of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " doesn\'t make sense"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 412
    :cond_3
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterRightButton:Landroid/widget/LinearLayout;

    if-ne p1, v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->rightMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Continue:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    if-ne v0, v1, :cond_5

    .line 415
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->FirstChoiceValid:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_4

    .line 416
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "expected ui stage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->FirstChoiceValid:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when button is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Continue:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 419
    :cond_4
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V

    goto :goto_0

    .line 420
    :cond_5
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->rightMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Confirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    if-ne v0, v1, :cond_7

    .line 421
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->ChoiceConfirmed:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_6

    .line 422
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "expected ui stage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->ChoiceConfirmed:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when button is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Confirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 425
    :cond_6
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->saveChosenPatternAndFinish()V

    goto/16 :goto_0

    .line 426
    :cond_7
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->rightMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->Ok:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    if-ne v0, v1, :cond_0

    .line 427
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->HelpScreen:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    if-eq v0, v1, :cond_8

    .line 428
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Help screen is only mode with ok button, but stage is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 431
    :cond_8
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v0}, Lcom/android/internal/widget/LockPatternView;->clearPattern()V

    .line 432
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    sget-object v1, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {v0, v1}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    .line 433
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 326
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->ACTIVITY_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : onCreate"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 327
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 329
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 339
    const v4, 0x7f030011

    invoke-virtual {p1, v4, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 341
    .local v3, "view":Landroid/view/View;
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 342
    instance-of v4, p2, Landroid/preference/PreferenceFrameLayout;

    if-eqz v4, :cond_0

    .line 343
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/preference/PreferenceFrameLayout$LayoutParams;

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/preference/PreferenceFrameLayout$LayoutParams;->removeBorders:Z

    .line 346
    :cond_0
    const v4, 0x7f0f006f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mHeaderText:Landroid/widget/TextView;

    .line 347
    const v4, 0x7f0f0070

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/internal/widget/LockPatternView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    .line 348
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChooseNewLockPatternListener:Lcom/android/internal/widget/LockPatternView$OnPatternListener;

    invoke-virtual {v4, v5}, Lcom/android/internal/widget/LockPatternView;->setOnPatternListener(Lcom/android/internal/widget/LockPatternView$OnPatternListener;)V

    .line 352
    const v4, 0x7f0f0071

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterText:Landroid/widget/TextView;

    .line 354
    const v4, 0x7f0f006b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftButton:Landroid/widget/LinearLayout;

    .line 355
    const v4, 0x7f0f006c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterRightButton:Landroid/widget/LinearLayout;

    .line 356
    const v4, 0x7f0f0072

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftText:Landroid/widget/TextView;

    .line 357
    const v4, 0x7f0f006d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterRightText:Landroid/widget/TextView;

    .line 359
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterRightButton:Landroid/widget/LinearLayout;

    invoke-virtual {v4, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 364
    const v4, 0x7f0f006e

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;

    .line 367
    .local v2, "topLayout":Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v2, v4}, Lcom/android/internal/widget/LinearLayoutWithDefaultTouchRecepient;->setDefaultTouchRecepient(Landroid/view/View;)V

    .line 369
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "confirm_credentials"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 372
    .local v0, "confirmCredentials":Z
    if-nez p3, :cond_2

    .line 373
    if-eqz v0, :cond_1

    .line 376
    sget-object v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->NeedToConfirm:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V

    .line 394
    :goto_0
    return-object v3

    .line 384
    :cond_1
    sget-object v4, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->Introduction:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V

    goto :goto_0

    .line 388
    :cond_2
    const-string v4, "chosenPattern"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 389
    .local v1, "patternString":Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 390
    invoke-static {v1}, Lcom/android/internal/widget/LockPatternUtils;->stringToPattern(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChosenPattern:Ljava/util/List;

    .line 392
    :cond_3
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->values()[Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    move-result-object v4

    const-string v5, "uiStage"

    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {p0, v4}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 454
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 456
    const-string v0, "uiStage"

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 457
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChosenPattern:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 458
    const-string v0, "chosenPattern"

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mChosenPattern:Ljava/util/List;

    invoke-static {v1}, Lcom/android/internal/widget/LockPatternUtils;->patternToString(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    :cond_0
    return-void
.end method

.method protected updateStage(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;)V
    .locals 7
    .param p1, "stage"    # Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    .prologue
    const/4 v6, 0x0

    .line 470
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    .line 472
    .local v0, "previousStage":Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    .line 476
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->ChoiceTooShort:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    if-ne p1, v1, :cond_1

    .line 477
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->headerMessage:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 484
    :goto_0
    iget v1, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->footerMessage:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 485
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    :goto_1
    iget-object v1, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->leftMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;->Gone:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    if-ne v1, v2, :cond_3

    .line 491
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftButton:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 498
    :goto_2
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterRightText:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->rightMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    iget v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->text:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 499
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterRightButton:Landroid/widget/LinearLayout;

    iget-object v2, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->rightMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;

    iget-boolean v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$RightButtonMode;->enabled:Z

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 502
    iget-boolean v1, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->patternEnabled:Z

    if-eqz v1, :cond_4

    .line 503
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternView;->enableInput()V

    .line 510
    :goto_3
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    sget-object v2, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Correct:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    .line 512
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$1;->$SwitchMap$com$sec$knox$knoxsetupwizardclient$SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage:[I

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mUiStage:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 538
    :goto_4
    :pswitch_0
    if-eq v0, p1, :cond_0

    .line 539
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mHeaderText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 541
    :cond_0
    return-void

    .line 482
    :cond_1
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mHeaderText:Landroid/widget/TextView;

    iget v2, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->headerMessage:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 487
    :cond_2
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterText:Landroid/widget/TextView;

    iget v2, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->footerMessage:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 493
    :cond_3
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftButton:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 494
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftText:Landroid/widget/TextView;

    iget-object v2, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->leftMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    iget v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;->text:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 495
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mFooterLeftButton:Landroid/widget/LinearLayout;

    iget-object v2, p1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$Stage;->leftMode:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;

    iget-boolean v2, v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment$LeftButtonMode;->enabled:Z

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_2

    .line 505
    :cond_4
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternView;->disableInput()V

    goto :goto_3

    .line 514
    :pswitch_1
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternView;->clearPattern()V

    goto :goto_4

    .line 517
    :pswitch_2
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    sget-object v2, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Animate:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mAnimatePattern:Ljava/util/List;

    invoke-virtual {v1, v2, v3}, Lcom/android/internal/widget/LockPatternView;->setPattern(Lcom/android/internal/widget/LockPatternView$DisplayMode;Ljava/util/List;)V

    goto :goto_4

    .line 520
    :pswitch_3
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    sget-object v2, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    .line 521
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->postClearPatternRunnable()V

    goto :goto_4

    .line 526
    :pswitch_4
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternView;->clearPattern()V

    goto :goto_4

    .line 529
    :pswitch_5
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->mLockPatternView:Lcom/android/internal/widget/LockPatternView;

    sget-object v2, Lcom/android/internal/widget/LockPatternView$DisplayMode;->Wrong:Lcom/android/internal/widget/LockPatternView$DisplayMode;

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternView;->setDisplayMode(Lcom/android/internal/widget/LockPatternView$DisplayMode;)V

    .line 530
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardSetPatternActivity$ChooseLockPatternFragment;->postClearPatternRunnable()V

    goto :goto_4

    .line 512
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
