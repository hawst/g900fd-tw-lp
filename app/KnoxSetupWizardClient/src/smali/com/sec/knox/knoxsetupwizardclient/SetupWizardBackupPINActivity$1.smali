.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardBackupPINActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)V
    .locals 0

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 105
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[^0-9.,]+"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "result":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 112
    :goto_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->updateUI()V
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->access$100(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)V

    .line 113
    return-void

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->mPINField:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;->access$000(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardBackupPINActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 120
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 125
    return-void
.end method
