.class public Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;
.super Ljava/lang/Object;
.source "PhoneWelcomeAnimator.java"

# interfaces
.implements Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/IWelcomeAnimator;


# instance fields
.field private KnoxBackground:Landroid/widget/LinearLayout;

.field private backFadeIn:Landroid/view/animation/AlphaAnimation;

.field private bottomFadeIn:Landroid/view/animation/AlphaAnimation;

.field private dummyPanelFadeOut:Landroid/view/animation/AlphaAnimation;

.field private eulaContentFadeIn:Landroid/view/animation/AlphaAnimation;

.field private eulaLayout:Landroid/widget/RelativeLayout;

.field private eulaScaleupAndFadeIn:Landroid/view/animation/AnimationSet;

.field private eulaSlideUpAndFadeIn:Landroid/view/animation/AnimationSet;

.field private fadeIn:Landroid/view/animation/AlphaAnimation;

.field private fadeOut:Landroid/view/animation/AlphaAnimation;

.field private knoxBottomDummyPanel:Landroid/widget/LinearLayout;

.field private knoxBottomPanel:Landroid/widget/RelativeLayout;

.field private knoxTitlePanel:Landroid/widget/LinearLayout;

.field private scaleUpAndFadeIn:Landroid/view/animation/AnimationSet;

.field private slideUpAndFadeIn:Landroid/view/animation/AnimationSet;

.field private slideUpAndFadeIn2:Landroid/view/animation/AnimationSet;

.field private txt_eula:Landroid/widget/TextView;

.field private txt_eula2:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const v0, 0x7f0f0093

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxTitlePanel:Landroid/widget/LinearLayout;

    .line 50
    const v0, 0x7f0f0095

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomPanel:Landroid/widget/RelativeLayout;

    .line 51
    const v0, 0x7f0f0094

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/LinearLayout;

    .line 52
    const v0, 0x7f0f0092

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->KnoxBackground:Landroid/widget/LinearLayout;

    .line 54
    const v0, 0x7f0f0098

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    .line 55
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 56
    const v0, 0x7f0f0007

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->txt_eula:Landroid/widget/TextView;

    .line 57
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->txt_eula:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 58
    const v0, 0x7f0f00a2

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->txt_eula2:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->txt_eula2:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->txt_eula2:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 63
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->initAnimations(Landroid/content/Context;)V

    .line 64
    return-void
.end method


# virtual methods
.method public initAnimations(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    .line 68
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v6, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeIn:Landroid/view/animation/AlphaAnimation;

    .line 69
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeIn:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 70
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 72
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v6, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->backFadeIn:Landroid/view/animation/AlphaAnimation;

    .line 73
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->backFadeIn:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x294

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 75
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeOut:Landroid/view/animation/AlphaAnimation;

    .line 76
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeOut:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x14a

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 77
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 79
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-direct {v0, v5, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->dummyPanelFadeOut:Landroid/view/animation/AlphaAnimation;

    .line 80
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->dummyPanelFadeOut:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 81
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->dummyPanelFadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 83
    const v0, 0x7f040007

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    .line 84
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 86
    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn2:Landroid/view/animation/AnimationSet;

    .line 87
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn2:Landroid/view/animation/AnimationSet;

    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 88
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn2:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 89
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn2:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 91
    const v0, 0x7f040005

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->scaleUpAndFadeIn:Landroid/view/animation/AnimationSet;

    .line 92
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->scaleUpAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 94
    const v0, 0x7f040003

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaSlideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    .line 95
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaSlideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 97
    const/high16 v0, 0x7f040000

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    check-cast v0, Landroid/view/animation/AnimationSet;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaScaleupAndFadeIn:Landroid/view/animation/AnimationSet;

    .line 98
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaScaleupAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v4}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 100
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v6, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->bottomFadeIn:Landroid/view/animation/AlphaAnimation;

    .line 101
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->bottomFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 102
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->bottomFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 104
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v6, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaContentFadeIn:Landroid/view/animation/AlphaAnimation;

    .line 105
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaContentFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 106
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaContentFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 107
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaContentFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 109
    return-void
.end method

.method public showBackgroundImg()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->KnoxBackground:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->backFadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 149
    return-void
.end method

.method public showEulaWithoutAnimation()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method

.method public startBackAnimation()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxTitlePanel:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 128
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->clearAnimation()V

    .line 130
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/LinearLayout;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 131
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomPanel:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 132
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 133
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 134
    return-void
.end method

.method public startDummyPanelFadeOut()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->dummyPanelFadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 154
    return-void
.end method

.method public startEulaAnimation()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->eulaScaleupAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 139
    return-void
.end method

.method public startHideWelcomeAnimation()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxTitlePanel:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 121
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->scaleUpAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 122
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomPanel:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeOut:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 123
    return-void
.end method

.method public startShowWelcomeAnimation()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxTitlePanel:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->fadeIn:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 114
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomPanel:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn2:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 115
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->knoxBottomDummyPanel:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/welcomeanimations/PhoneWelcomeAnimator;->slideUpAndFadeIn:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 116
    return-void
.end method
