.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->initAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 1447
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1459
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGrayArea:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 1460
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->finish()V

    .line 1461
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsFadeout:Z

    .line 1462
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1455
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 1451
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$12;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsFadeout:Z

    .line 1452
    return-void
.end method
