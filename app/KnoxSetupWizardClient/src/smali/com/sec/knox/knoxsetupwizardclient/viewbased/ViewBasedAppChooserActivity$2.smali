.class Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "ViewBasedAppChooserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 288
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    const-string v2, "reason"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "reason":Ljava/lang/String;
    :try_start_0
    const-string v2, "homekey"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 292
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.knox.action.VIEW_BASED_CONTAINER_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 293
    .local v1, "serviceIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-virtual {v2, v1}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->stopService(Landroid/content/Intent;)Z

    .line 294
    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity$2;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/ViewBasedAppChooserActivity;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    .end local v0    # "reason":Ljava/lang/String;
    .end local v1    # "serviceIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 297
    .restart local v0    # "reason":Ljava/lang/String;
    :catch_0
    move-exception v2

    goto :goto_0
.end method
