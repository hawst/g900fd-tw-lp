.class public Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;
.super Ljava/lang/Thread;
.source "SetupWizardFotaAgent.java"


# instance fields
.field private VersionKey:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private currentVersion:Ljava/lang/String;

.field private defaultValue:Ljava/lang/String;

.field private mPm:Landroid/os/PersonaManager;

.field private personaIds:[I

.field private previousVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 14
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    .line 15
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->currentVersion:Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->previousVersion:Ljava/lang/String;

    .line 17
    const-string v0, "version"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->VersionKey:Ljava/lang/String;

    .line 18
    const-string v0, "none"

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->defaultValue:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->mPm:Landroid/os/PersonaManager;

    .line 20
    iput-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    .line 23
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    .line 24
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->deviceVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->currentVersion:Ljava/lang/String;

    .line 25
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->VersionKey:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->defaultValue:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->readPreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->previousVersion:Ljava/lang/String;

    .line 26
    const-string v0, "persona"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->mPm:Landroid/os/PersonaManager;

    .line 27
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->mPm:Landroid/os/PersonaManager;

    invoke-virtual {v0}, Landroid/os/PersonaManager;->getPersonaIds()[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    .line 28
    return-void
.end method

.method private disableComponent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "compName"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const-string v4, "package"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v3

    .line 81
    .local v3, "packageManager":Landroid/content/pm/IPackageManager;
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    .local v0, "componentName":Landroid/content/ComponentName;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 84
    const/4 v4, 0x2

    const/4 v5, 0x1

    :try_start_0
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    aget v6, v6, v2

    invoke-interface {v3, v0, v4, v5, v6}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pkg :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", component :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not installed."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 91
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 31
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    array-length v7, v7

    if-nez v7, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->currentVersion:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->previousVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 38
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->defaultValue:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->previousVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->currentVersion:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->previousVersion:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 41
    :cond_2
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    const-string v8, "user"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/UserManager;

    .line 42
    .local v5, "um":Landroid/os/UserManager;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    array-length v7, v7

    if-ge v0, v7, :cond_4

    .line 44
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    aget v8, v8, v0

    invoke-static {v5, v7, v10, v8}, Lcom/sec/knox/knoxsetupwizardclient/CrossIntentforKNOX;->setFilters(Landroid/os/UserManager;Landroid/content/pm/PackageManager;II)V

    .line 47
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    aget v7, v7, v0

    invoke-virtual {v5, v7}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v6

    .line 49
    .local v6, "userInfo":Landroid/content/pm/UserInfo;
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "caller_id_to_show_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, -0x1

    invoke-static {v7, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 50
    .local v1, "originalValue":I
    if-gez v1, :cond_3

    .line 51
    const-string v7, "SetupWizardFotaAgent"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Caller id setting originalValue = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " | put default value"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "caller_id_to_show_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v6, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v11, v10}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 42
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57
    .end local v1    # "originalValue":I
    .end local v6    # "userInfo":Landroid/content/pm/UserInfo;
    :cond_4
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "notification_panel_active_app_list_for_reset_rollback"

    invoke-static {v7, v8, v10}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 58
    .local v3, "rollback":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->mPm:Landroid/os/PersonaManager;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->personaIds:[I

    aget v8, v8, v10

    invoke-virtual {v7, v8}, Landroid/os/PersonaManager;->isKioskModeEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_5

    if-nez v3, :cond_5

    .line 59
    const-string v4, "Wifi;Location;SilentMode;AutoRotate;Bluetooth;SmartStay;PowerSaving;AirplaneMode;"

    .line 61
    .local v4, "tileListForKiosk":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "notification_panel_active_app_list_for_reset"

    invoke-static {v7, v8, v10}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 62
    .local v2, "resetList":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "notification_panel_active_app_list_for_reset_rollback"

    invoke-static {v7, v8, v2, v10}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 63
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "notification_panel_active_app_list_for_reset"

    invoke-static {v7, v8, v4, v10}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 64
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "notification_panel_active_app_list"

    invoke-static {v7, v8, v4, v10}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 68
    .end local v2    # "resetList":Ljava/lang/String;
    .end local v4    # "tileListForKiosk":Ljava/lang/String;
    :cond_5
    const/4 v0, 0x0

    :goto_2
    sget-object v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents:[[Ljava/lang/String;

    array-length v7, v7

    if-ge v0, v7, :cond_6

    .line 69
    sget-object v7, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents:[[Ljava/lang/String;

    aget-object v7, v7, v0

    aget-object v7, v7, v10

    sget-object v8, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents:[[Ljava/lang/String;

    aget-object v8, v8, v0

    aget-object v8, v8, v11

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    invoke-direct {p0, v7, v8, v9}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->disableComponent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 74
    :cond_6
    iget-object v7, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->context:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->VersionKey:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->VersionKey:Ljava/lang/String;

    iget-object v10, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFotaAgent;->currentVersion:Ljava/lang/String;

    invoke-static {v7, v8, v9, v10}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->savePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
