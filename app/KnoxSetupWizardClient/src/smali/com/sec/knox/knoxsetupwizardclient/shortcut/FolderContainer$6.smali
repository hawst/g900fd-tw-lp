.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setCallBack()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 941
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 945
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->onUserInteraction()V

    .line 947
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v1, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPM:Landroid/os/PersonaManager;

    iget-object v2, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    invoke-virtual {v1, v2}, Landroid/os/PersonaManager;->lockPersona(I)V

    .line 948
    const/4 v0, 0x0

    .line 949
    .local v0, "knoxName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v1, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentPersonaId:I

    if-ne v1, v5, :cond_0

    .line 950
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00f5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 955
    :goto_0
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0010

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 956
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->moveTaskToBackground()V

    .line 957
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v4, v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 958
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$6;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v1, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showMoreLayout(Z)V

    .line 961
    return-void

    .line 952
    :cond_0
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    const v2, 0x7f0c00f6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
