.class public Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;
.super Ljava/lang/Thread;
.source "ContainerCreator.java"


# static fields
.field private static SHORTCUT_INIT:Z

.field private static mPersona:Landroid/os/PersonaManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCreationListener:Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;

.field private mStartRestore:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    .line 59
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->SHORTCUT_INIT:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "creationListener"    # Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mStartRestore:Z

    .line 62
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mCreationListener:Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;

    .line 64
    const-string v0, "persona"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    .line 65
    return-void
.end method

.method private callCreateContainerInternal()I
    .locals 3

    .prologue
    .line 105
    const/4 v1, -0x1

    .line 107
    .local v1, "result":I
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getContainerCreationParams()Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v0

    .line 108
    .local v0, "param":Lcom/sec/knox/container/ContainerCreationParams;
    if-eqz v0, :cond_0

    .line 109
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/ContainerCreationParams;->setPassword(Ljava/lang/String;)V

    .line 110
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getBackupPin()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/ContainerCreationParams;->setBackupPin(Ljava/lang/String;)V

    .line 111
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getPasswordType()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/ContainerCreationParams;->setLockType(I)V

    .line 112
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/ContainerCreationParams;->setName(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getRestore()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/ContainerCreationParams;->setRestoreSelection(Z)V

    .line 114
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getFingerprintPlus()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/ContainerCreationParams;->setFingerprintPlus(Z)V

    .line 117
    :cond_0
    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainerInternal(Lcom/sec/knox/container/ContainerCreationParams;)I

    move-result v1

    .line 119
    return v1
.end method

.method private disableComponent(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "compName"    # Ljava/lang/String;
    .param p3, "personaId"    # I
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 304
    const-string v3, "package"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v2

    .line 306
    .local v2, "packageManager":Landroid/content/pm/IPackageManager;
    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    .local v0, "componentName":Landroid/content/ComponentName;
    const/4 v3, 0x2

    const/4 v4, 0x1

    :try_start_0
    invoke-interface {v2, v0, v3, v4, p3}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :goto_0
    return-void

    .line 310
    :catch_0
    move-exception v1

    .line 311
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pkg :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", component :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not installed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 312
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private disableContainerInsideComponent(I)V
    .locals 5
    .param p1, "personaId"    # I

    .prologue
    const/4 v4, 0x0

    .line 380
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents:[[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 381
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents:[[Ljava/lang/String;

    aget-object v1, v1, v0

    aget-object v1, v1, v4

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardConfig;->excludedComponents:[[Ljava/lang/String;

    aget-object v2, v2, v0

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, v2, p1, v3}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->disableComponent(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V

    .line 380
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 385
    :cond_0
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1}, Landroid/os/PersonaManager;->isKioskModeEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 386
    const-string v1, "com.sec.knox.switcher"

    const-string v2, "com.sec.knox.switcher.MainActivity"

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, v2, p1, v3}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->disableComponent(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V

    .line 387
    const-string v1, "com.samsung.android.MtpApplication"

    const-string v2, "com.samsung.android.MtpApplication.MtpReceiver"

    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1, v2, v4, v3}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->disableComponent(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V

    .line 389
    :cond_1
    return-void
.end method

.method private installExistingApps(I)V
    .locals 5
    .param p1, "personaId"    # I

    .prologue
    .line 230
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    const-string v4, "persona"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PersonaManager;

    sput-object v3, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    .line 231
    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1}, Landroid/os/PersonaManager;->exists(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 232
    const-string v3, "eng"

    const-string v4, "ro.build.type"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 233
    .local v0, "BUILD_TYPE":Z
    const/4 v1, 0x0

    .line 234
    .local v1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v2

    .line 235
    .local v2, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getAppInstallationList()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 236
    invoke-virtual {v2}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getAppInstallationList()Ljava/util/List;

    move-result-object v1

    .line 237
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "applist : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 239
    :cond_0
    if-nez v1, :cond_1

    .line 240
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .restart local v1    # "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    if-eqz v0, :cond_2

    .line 243
    const-string v3, "com.sec.keystringscreen"

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BUILE_TYPE ENG IS :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 247
    :cond_2
    if-eqz v2, :cond_3

    instance-of v3, v2, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    if-nez v3, :cond_3

    .line 248
    const-string v3, "com.sec.android.app.launcher"

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 252
    sget-object v3, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v3, p1, v1}, Landroid/os/PersonaManager;->installApplications(ILjava/util/List;)Z

    .line 262
    .end local v0    # "BUILD_TYPE":Z
    .end local v1    # "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    :cond_4
    return-void
.end method

.method private processContainerWork(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->setKnoxSettingsIcon(I)V

    .line 97
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->installExistingApps(I)V

    .line 100
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->disableContainerInsideComponent(I)V

    .line 101
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->setupComplete(I)V

    .line 102
    return-void
.end method

.method private processOwnerWork()I
    .locals 2

    .prologue
    .line 82
    const/4 v0, -0x1

    .line 84
    .local v0, "result":I
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->callCreateContainerInternal()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 85
    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->setSetSamsungAccount(I)V

    .line 86
    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->setKnoxBackupPin(I)V

    .line 87
    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->setSettingsValue(I)V

    .line 88
    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->setFingerprintSupplement(I)V

    .line 91
    :cond_0
    return v0
.end method

.method private setFingerprintSupplement(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    const/4 v3, 0x1

    .line 144
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getFingerprintSupplement()Z

    move-result v0

    .line 145
    .local v0, "isFingerAsSupplement":Z
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isFingerAsSupplement : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getFingerprintSupplement()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 146
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1, v0}, Landroid/os/PersonaManager;->setIsFingerAsSupplement(IZ)V

    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPersona.setIsFingerAsSupplement : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v2}, Landroid/os/PersonaManager;->getIsFingerAsSupplement()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 148
    if-eqz v0, :cond_0

    .line 149
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1, v3}, Landroid/os/PersonaManager;->setIsUnlockedAfterTurnOn(IZ)V

    .line 150
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v1, p1, v3}, Landroid/os/PersonaManager;->setIsFingerReset(IZ)V

    .line 152
    :cond_0
    return-void
.end method

.method private setKnoxBackupPin(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 132
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getContainerCreationParams()Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v0

    .line 134
    .local v0, "param":Lcom/sec/knox/container/ContainerCreationParams;
    invoke-static {p1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v1

    .line 135
    .local v1, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->isUserManaged()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v0}, Lcom/sec/knox/container/ContainerCreationParams;->getBackupPin()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Landroid/os/PersonaManager;->setKnoxBackupPin(ILjava/lang/String;)V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_0
    if-eqz v1, :cond_1

    const-string v2, "it is a b2b container"

    :goto_1
    invoke-static {v2}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v2, "typeObj is null"

    goto :goto_1
.end method

.method private setKnoxSettingsIcon(I)V
    .locals 7
    .param p1, "personaId"    # I

    .prologue
    .line 209
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    const-string v6, "user"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 210
    .local v3, "um":Landroid/os/UserManager;
    invoke-virtual {v3, p1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v4

    .line 212
    .local v4, "userInfo":Landroid/content/pm/UserInfo;
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v2

    .line 213
    .local v2, "ipm":Landroid/content/pm/IPackageManager;
    const/4 v0, 0x0

    .line 214
    .local v0, "disableComponent":Landroid/content/ComponentName;
    if-eqz v4, :cond_1

    iget-object v5, v4, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    const-string v6, "KNOX"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 215
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "disableComponent":Landroid/content/ComponentName;
    const-string v5, "com.sec.knox.containeragent2"

    const-string v6, "com.sec.knox.containeragent2.ui.settings.KnoxSettingsActivity2"

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    .restart local v0    # "disableComponent":Landroid/content/ComponentName;
    :goto_0
    if-eqz v0, :cond_0

    .line 221
    :try_start_0
    const-string v5, "disable useless icon componenet"

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 222
    const/4 v5, 0x2

    const/4 v6, 0x1

    invoke-interface {v2, v0, v5, v6, p1}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :cond_0
    :goto_1
    return-void

    .line 217
    :cond_1
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "disableComponent":Landroid/content/ComponentName;
    const-string v5, "com.sec.knox.containeragent2"

    const-string v6, "com.sec.knox.containeragent2.ui.settings.KnoxSettingsActivity"

    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v0    # "disableComponent":Landroid/content/ComponentName;
    goto :goto_0

    .line 223
    :catch_0
    move-exception v1

    .line 224
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private setSetSamsungAccount(I)V
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 124
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    const-string v2, "samsungAccount"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 125
    .local v0, "prefs":Landroid/content/SharedPreferences;
    if-eqz v0, :cond_0

    .line 126
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    const-string v2, "accountId"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/os/PersonaManager;->setPersonaSamsungAccount(ILjava/lang/String;)V

    .line 129
    :goto_0
    return-void

    .line 128
    :cond_0
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    const-string v2, ""

    invoke-virtual {v1, p1, v2}, Landroid/os/PersonaManager;->setPersonaSamsungAccount(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private setSettingsValue(I)V
    .locals 10
    .param p1, "personaId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 155
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    const-string v8, "user"

    invoke-virtual {v5, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 156
    .local v3, "um":Landroid/os/UserManager;
    invoke-virtual {v3, p1}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v4

    .line 159
    .local v4, "userInfo":Landroid/content/pm/UserInfo;
    invoke-direct {p0, v7, p1}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->settingsSync(II)V

    .line 162
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getSecurityTimeOut()I

    move-result v2

    .line 163
    .local v2, "timeOutValue":I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Setting security timeout value for user : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " value : "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    .line 164
    sget-object v5, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v5, p1, v2}, Landroid/os/PersonaManager;->setKnoxSecurityTimeout(II)V

    .line 165
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "knox_finger_print_plus"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getFingerprintPlus()Z

    move-result v5

    if-eqz v5, :cond_1

    move v5, v6

    :goto_0
    invoke-static {v8, v9, v5, p1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 167
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "is_smpw_key"

    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getSimplePassword()Z

    move-result v5

    if-eqz v5, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v8, v9, v5, p1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 170
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "caller_id_to_show_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v4, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v6, v7}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 172
    sget-object v5, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v5, p1}, Landroid/os/PersonaManager;->isKioskModeEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 173
    const-string v1, "Wifi;Location;SilentMode;AutoRotate;Bluetooth;SmartStay;PowerSaving;AirplaneMode;"

    .line 175
    .local v1, "tileListForKiosk":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "notification_panel_active_app_list_for_reset"

    invoke-static {v5, v8, v7}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "resetList":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "notification_panel_active_app_list_for_reset_rollback"

    invoke-static {v5, v8, v0, v7}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 177
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "notification_panel_active_app_list_for_reset"

    invoke-static {v5, v8, v1, v7}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 178
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "notification_panel_active_app_list"

    invoke-static {v5, v8, v1, v7}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 179
    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v7, "knox.power_button_instantly_locks"

    invoke-static {v5, v7, v6, p1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 181
    .end local v0    # "resetList":Ljava/lang/String;
    .end local v1    # "tileListForKiosk":Ljava/lang/String;
    :cond_0
    return-void

    :cond_1
    move v5, v7

    .line 165
    goto :goto_0

    :cond_2
    move v5, v7

    .line 167
    goto :goto_1
.end method

.method private settingsSync(II)V
    .locals 8
    .param p1, "oldUserId"    # I
    .param p2, "personaId"    # I

    .prologue
    .line 184
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 185
    .local v0, "SHARE_WITH_KNOX":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 186
    .local v1, "SHARE_WITH_KNOX_TO_SECURE":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-static {v0}, Landroid/os/PersonaPolicyManager;->getKnoxKeys(Ljava/util/HashSet;)V

    .line 187
    invoke-static {v1}, Landroid/os/PersonaPolicyManager;->getKnoxKeysToSecure(Ljava/util/HashSet;)V

    .line 189
    const-string v6, "PersonaManager"

    const-string v7, "PersonaSettings value Sycronize"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    if-eqz v0, :cond_1

    .line 192
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 194
    .local v4, "name":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 195
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v4, p1}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 196
    .local v5, "value":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v4, v5, p2}, Landroid/provider/Settings$Secure;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    .end local v5    # "value":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 202
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 198
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v4, p1}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 199
    .restart local v5    # "value":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, v4, v5, p2}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 206
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private setupComplete(I)V
    .locals 12
    .param p1, "personaId"    # I

    .prologue
    const/4 v11, 0x1

    .line 393
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    const-string v9, "user"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/UserManager;

    .line 394
    .local v7, "um":Landroid/os/UserManager;
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v9, p1}, Lcom/sec/knox/knoxsetupwizardclient/CrossIntentforKNOX;->setFilters(Landroid/os/UserManager;Landroid/content/pm/PackageManager;II)V

    .line 396
    sget-object v8, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    const-string v9, "persona_policy"

    invoke-virtual {v8, v9}, Landroid/os/PersonaManager;->getPersonaService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PersonaPolicyManager;

    .line 397
    .local v5, "personaPolicy":Landroid/os/PersonaPolicyManager;
    if-eqz v5, :cond_0

    .line 398
    invoke-virtual {v5, p1, v11}, Landroid/os/PersonaPolicyManager;->setPasswordLockPolicy(IZ)V

    .line 402
    :cond_0
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "user_setup_complete"

    invoke-static {v8, v9, v11, p1}, Landroid/provider/Settings$Secure;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 404
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "lock_screen_option_category"

    const-string v10, "multiple_widget_category"

    invoke-static {v8, v9, v10, p1}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 405
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "kg_multiple_lockscreen"

    invoke-static {v8, v9, v11, p1}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 408
    sget-object v8, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    invoke-virtual {v8, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v8

    sget-object v9, Landroid/content/pm/PersonaNewEvent;->USER_UNLOCK:Landroid/content/pm/PersonaNewEvent;

    invoke-virtual {v8, v9}, Landroid/os/PersonaManager$StateManager;->fireEvent(Landroid/content/pm/PersonaNewEvent;)Landroid/content/pm/PersonaState;

    .line 411
    sget-boolean v8, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->SHORTCUT_INIT:Z

    if-nez v8, :cond_1

    .line 412
    sput-boolean v11, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->SHORTCUT_INIT:Z

    .line 414
    :try_start_0
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.sec.knox.SETUP_COMPLETE"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 415
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "userid"

    invoke-virtual {v3, v8, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 416
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    sget-object v9, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v8, v3, v9}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 417
    const-string v8, "send setup complete"

    invoke-static {v8}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_0
    invoke-static {p1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v6

    .line 428
    .local v6, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    instance-of v8, v6, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    if-nez v8, :cond_2

    .line 429
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    const-string v9, "CONTAINER_TYPE_AT_FIRST"

    const-string v10, "type"

    const-string v11, "launcher"

    invoke-static {v8, v9, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->savePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    :goto_1
    return-void

    .line 418
    .end local v6    # "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    :catch_0
    move-exception v2

    .line 419
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 422
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v8, "shortcut already created"

    invoke-static {v8}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 431
    .restart local v6    # "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    :cond_2
    iget-object v8, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    const-string v9, "CONTAINER_TYPE_AT_FIRST"

    const-string v10, "type"

    const-string v11, "folder"

    invoke-static {v8, v9, v10, v11}, Lcom/sec/knox/knoxsetupwizardclient/Utils;->savePreference(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const-string v8, " switching back to owner for LWC"

    invoke-static {v8}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 434
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v4

    .line 436
    .local v4, "ipm":Landroid/content/pm/IPackageManager;
    new-instance v1, Landroid/content/ComponentName;

    const-string v8, "com.android.systemui"

    const-string v9, "com.android.systemui.ImageWallpaper"

    invoke-direct {v1, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    .local v1, "componentNameIW":Landroid/content/ComponentName;
    new-instance v0, Landroid/content/ComponentName;

    const-string v8, "com.sec.android.gallery3d"

    const-string v9, "com.sec.android.gallery3d.app.Wallpaper"

    invoke-direct {v0, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    .local v0, "componentNameGallery":Landroid/content/ComponentName;
    const/4 v8, 0x2

    const/4 v9, 0x1

    :try_start_1
    invoke-interface {v4, v1, v8, v9, p1}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V

    .line 441
    const/4 v8, 0x2

    const/4 v9, 0x1

    invoke-interface {v4, v0, v8, v9, p1}, Landroid/content/pm/IPackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;III)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 442
    :catch_1
    move-exception v2

    .line 444
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 68
    const/4 v0, -0x1

    .line 70
    .local v0, "result":I
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mPersona:Landroid/os/PersonaManager;

    if-nez v1, :cond_1

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mCreationListener:Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;

    const/4 v2, -0x1

    invoke-interface {v1, v2}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;->onCreationComplete(I)V

    .line 79
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-direct {p0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->processOwnerWork()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_2

    .line 76
    invoke-direct {p0, v0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->processContainerWork(I)V

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreator;->mCreationListener:Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;

    invoke-interface {v1, v0}, Lcom/sec/knox/knoxsetupwizardclient/ContainerCreationListener;->onCreationComplete(I)V

    goto :goto_0
.end method
