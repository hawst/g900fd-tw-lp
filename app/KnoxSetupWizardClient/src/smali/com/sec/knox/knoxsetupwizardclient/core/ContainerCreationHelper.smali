.class public Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;
.super Ljava/lang/Object;
.source "ContainerCreationHelper.java"


# static fields
.field private static containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

.field private static instance:Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

.field private static knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    .line 32
    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    .line 33
    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->getContainerCreationParams()Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    .line 37
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    if-eqz v0, :cond_0

    .line 38
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    invoke-virtual {v0}, Lcom/sec/knox/container/ContainerCreationParams;->getConfigurationType()Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 40
    :cond_0
    return-void
.end method

.method private computeLevenshteinDistance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I
    .locals 9
    .param p1, "str1"    # Ljava/lang/CharSequence;
    .param p2, "str2"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v4, 0x0

    .line 394
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    filled-new-array {v3, v5}, [I

    move-result-object v3

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    .line 396
    .local v0, "distance":[[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-gt v1, v3, :cond_0

    .line 397
    aget-object v3, v0, v1

    aput v1, v3, v4

    .line 396
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 398
    :cond_0
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 399
    aget-object v3, v0, v4

    aput v2, v3, v2

    .line 398
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 401
    :cond_1
    const/4 v1, 0x1

    :goto_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-gt v1, v3, :cond_4

    .line 402
    const/4 v2, 0x1

    :goto_3
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-gt v2, v3, :cond_3

    .line 403
    aget-object v5, v0, v1

    add-int/lit8 v3, v1, -0x1

    aget-object v3, v0, v3

    aget v3, v3, v2

    add-int/lit8 v3, v3, 0x1

    aget-object v6, v0, v1

    add-int/lit8 v7, v2, -0x1

    aget v6, v6, v7

    add-int/lit8 v6, v6, 0x1

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    add-int/lit8 v3, v1, -0x1

    aget-object v3, v0, v3

    add-int/lit8 v7, v2, -0x1

    aget v7, v3, v7

    add-int/lit8 v3, v1, -0x1

    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    add-int/lit8 v8, v2, -0x1

    invoke-interface {p2, v8}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    if-ne v3, v8, :cond_2

    move v3, v4

    :goto_4
    add-int/2addr v3, v7

    invoke-static {v6, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    aput v3, v5, v2

    .line 402
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 403
    :cond_2
    const/4 v3, 0x1

    goto :goto_4

    .line 401
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 407
    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    aget-object v3, v0, v3

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    aget v3, v3, v4

    return v3
.end method

.method private containsForbiddenCharacterSequence(Ljava/lang/String;)Z
    .locals 12
    .param p1, "pwd"    # Ljava/lang/String;

    .prologue
    .line 336
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterSequenceLength()I

    move-result v4

    .line 338
    .local v4, "length":I
    if-nez v4, :cond_0

    .line 339
    const/4 v10, 0x0

    .line 390
    :goto_0
    return v10

    .line 341
    :cond_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "\\w(?=\\w{"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ",})"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    .line 343
    .local v6, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v6, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 344
    .local v5, "match":Ljava/util/regex/Matcher;
    :cond_1
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 346
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->start()I

    move-result v9

    .line 350
    .local v9, "start":I
    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 353
    .local v0, "c":C
    invoke-static {v0}, Ljava/lang/Character;->isAlphabetic(I)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 357
    const/4 v2, 0x0

    .line 358
    .local v2, "flag":I
    add-int/lit8 v10, v9, 0x1

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    sub-int v1, v10, v0

    .line 359
    .local v1, "charDiff":I
    if-nez v1, :cond_4

    const/4 v8, 0x0

    .line 362
    .local v8, "sequencePattern":I
    :goto_1
    new-instance v7, Ljava/lang/StringBuilder;

    add-int/lit8 v10, v4, 0x5

    invoke-direct {v7, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 363
    .local v7, "sb":Ljava/lang/StringBuilder;
    const/16 v10, 0x5c

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 364
    const/16 v10, 0x51

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 365
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 366
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v4, :cond_3

    .line 367
    add-int v10, v0, v8

    int-to-char v0, v10

    .line 370
    add-int v10, v9, v3

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v10

    invoke-static {v10}, Ljava/lang/Character;->isAlphabetic(I)Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-static {v0}, Ljava/lang/Character;->isAlphabetic(I)Z

    move-result v10

    if-nez v10, :cond_6

    .line 371
    :cond_2
    const/4 v2, 0x1

    .line 380
    :cond_3
    const/4 v10, 0x1

    if-eq v2, v10, :cond_1

    .line 384
    const/16 v10, 0x5c

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 385
    const/16 v10, 0x45

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 387
    add-int v10, v9, v4

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 388
    const/4 v10, 0x1

    goto/16 :goto_0

    .line 359
    .end local v3    # "i":I
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v8    # "sequencePattern":I
    :cond_4
    if-lez v1, :cond_5

    const/4 v8, 0x1

    goto :goto_1

    :cond_5
    const/4 v8, -0x1

    goto :goto_1

    .line 375
    .restart local v3    # "i":I
    .restart local v7    # "sb":Ljava/lang/StringBuilder;
    .restart local v8    # "sequencePattern":I
    :cond_6
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 366
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 390
    .end local v0    # "c":C
    .end local v1    # "charDiff":I
    .end local v2    # "flag":I
    .end local v3    # "i":I
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v8    # "sequencePattern":I
    .end local v9    # "start":I
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method private containsForbiddenData(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pwd"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 423
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getForbiddenStrings()Ljava/util/List;

    move-result-object v0

    .line 425
    .local v0, "forbiddenStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 433
    :cond_0
    :goto_0
    return v3

    .line 428
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 429
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 430
    const/4 v3, 0x1

    goto :goto_0
.end method

.method private containsForbiddenNumericSequence(Ljava/lang/String;)Z
    .locals 13
    .param p1, "pwd"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 303
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumNumericSequenceLength()I

    move-result v3

    .line 305
    .local v3, "length":I
    if-nez v3, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v9

    .line 308
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "\\d(?=\\d{"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ",})"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 309
    .local v5, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 310
    .local v4, "match":Ljava/util/regex/Matcher;
    :cond_2
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 312
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->start()I

    move-result v8

    .line 316
    .local v8, "start":I
    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 317
    .local v0, "c":C
    add-int/lit8 v11, v8, 0x1

    invoke-virtual {p1, v11}, Ljava/lang/String;->charAt(I)C

    move-result v11

    sub-int v1, v11, v0

    .line 318
    .local v1, "charDiff":I
    if-nez v1, :cond_3

    move v7, v9

    .line 320
    .local v7, "sequencePattern":I
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    add-int/lit8 v11, v3, 0x1

    invoke-direct {v6, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 321
    .local v6, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 322
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v3, :cond_5

    .line 323
    add-int v11, v0, v7

    int-to-char v0, v11

    .line 324
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 322
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 318
    .end local v2    # "i":I
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    .end local v7    # "sequencePattern":I
    :cond_3
    if-lez v1, :cond_4

    move v7, v10

    goto :goto_1

    :cond_4
    const/4 v7, -0x1

    goto :goto_1

    .line 328
    .restart local v2    # "i":I
    .restart local v6    # "sb":Ljava/lang/StringBuilder;
    .restart local v7    # "sequencePattern":I
    :cond_5
    add-int v11, v8, v3

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {p1, v8, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    move v9, v10

    .line 329
    goto :goto_0
.end method

.method private containsForbiddenStringDistance(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "pwd"    # Ljava/lang/String;
    .param p2, "storedPwd"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 412
    if-nez p2, :cond_1

    .line 419
    :cond_0
    :goto_0
    return v1

    .line 415
    :cond_1
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMinimumCharacterChangeLength()I

    move-result v0

    .line 416
    .local v0, "minDistance":I
    if-eqz v0, :cond_0

    .line 419
    invoke-direct {p0, p2, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->computeLevenshteinDistance(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v2

    if-ge v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private containsMaxRepeatedCharacters(Ljava/lang/String;)Z
    .locals 12
    .param p1, "pwd"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 437
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getMaximumCharacterOccurences()I

    move-result v7

    .line 439
    .local v7, "rptCount":I
    if-nez v7, :cond_0

    move v8, v9

    .line 455
    :goto_0
    return v8

    .line 442
    :cond_0
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 443
    .local v4, "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Character;Ljava/lang/Integer;>;"
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 444
    .local v2, "ca":[C
    move-object v0, v2

    .local v0, "arr$":[C
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_3

    aget-char v1, v0, v5

    .line 445
    .local v1, "c":C
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 446
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 447
    .local v3, "cnt":I
    if-ne v3, v7, :cond_1

    move v8, v10

    .line 448
    goto :goto_0

    .line 450
    :cond_1
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v8, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    .end local v3    # "cnt":I
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 452
    :cond_2
    invoke-static {v1}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v4, v8, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .end local v1    # "c":C
    :cond_3
    move v8, v9

    .line 455
    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    invoke-direct {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;-><init>()V

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    .line 47
    :cond_0
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    return-object v0
.end method


# virtual methods
.method public getBiometricAuthenticationEnabled()Z
    .locals 2

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "isEnabled":Z
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 78
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getBiometricAuthenticationEnabledValue()Z

    move-result v0

    .line 80
    :cond_0
    return v0
.end method

.method public getBiometricAuthenticationEnabledType()I
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x0

    .line 84
    .local v0, "type":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 85
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getBiometricAuthenticationEnabledType()I

    move-result v0

    .line 87
    :cond_0
    return v0
.end method

.method public getCOMEnabled()Z
    .locals 2

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 92
    .local v0, "isEnabled":Z
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    instance-of v1, v1, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    if-eqz v1, :cond_0

    .line 93
    const/4 v0, 0x1

    .line 95
    :cond_0
    return v0
.end method

.method public getContainerAppList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 295
    const/4 v0, 0x0

    .line 296
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 297
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getAppInstallationList()Ljava/util/List;

    move-result-object v0

    .line 299
    :cond_0
    return-object v0
.end method

.method public getContainerType()Z
    .locals 2

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 101
    .local v0, "isB2CContainer":Z
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 102
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->isUserManaged()Z

    move-result v0

    .line 104
    :cond_0
    return v0
.end method

.method public getForbiddenStrings()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    const/4 v0, 0x0

    .line 189
    .local v0, "forbiddenStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 190
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getForbiddenStrings()Ljava/util/List;

    move-result-object v0

    .line 192
    :cond_0
    return-object v0
.end method

.method public getMaximumCharacterOccurences()I
    .locals 2

    .prologue
    .line 196
    const/4 v0, 0x0

    .line 197
    .local v0, "max":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 198
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getMaximumCharacterOccurences()I

    move-result v0

    .line 200
    :cond_0
    return v0
.end method

.method public getMaximumCharacterSequenceLength()I
    .locals 2

    .prologue
    .line 204
    const/4 v0, 0x0

    .line 205
    .local v0, "max":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 206
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getMaximumCharacterSequenceLength()I

    move-result v0

    .line 208
    :cond_0
    return v0
.end method

.method public getMaximumNumericSequenceLength()I
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "max":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 182
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getMaximumNumericSequenceLength()I

    move-result v0

    .line 184
    :cond_0
    return v0
.end method

.method public getMaximumTimeToLock()I
    .locals 2

    .prologue
    .line 220
    const/4 v0, 0x0

    .line 221
    .local v0, "maxTime":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 222
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getMaximumTimeToLock()I

    move-result v0

    .line 224
    :cond_0
    return v0
.end method

.method public getMinimumCharacterChangeLength()I
    .locals 2

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    .local v0, "min":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 214
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getMaximumCharacterSequenceLength()I

    move-result v0

    .line 216
    :cond_0
    return v0
.end method

.method public getMultifactorAuthenticationEnabled()Z
    .locals 2

    .prologue
    .line 244
    const/4 v0, 0x0

    .line 245
    .local v0, "isEnabled":Z
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 246
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->isMultifactorAuthenticationEnforced()Z

    move-result v0

    .line 248
    :cond_0
    return v0
.end method

.method public getPasswordMaximumLength()I
    .locals 2

    .prologue
    .line 116
    const/16 v0, 0x10

    .line 117
    .local v0, "passwordMaxLength":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 120
    :cond_0
    return v0
.end method

.method public getPasswordMinLetters()I
    .locals 2

    .prologue
    .line 132
    const/4 v0, 0x0

    .line 133
    .local v0, "passwordMinLetter":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 134
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordMinimumLetters()I

    move-result v0

    .line 136
    :cond_0
    return v0
.end method

.method public getPasswordMinLowerCase()I
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 149
    .local v0, "passwordMinLowerCase":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 150
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordMinimumLowerCase()I

    move-result v0

    .line 152
    :cond_0
    return v0
.end method

.method public getPasswordMinNonLetter()I
    .locals 2

    .prologue
    .line 172
    const/4 v0, 0x0

    .line 173
    .local v0, "passwordMinNonLetter":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 174
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordMinimumNonLetters()I

    move-result v0

    .line 176
    :cond_0
    return v0
.end method

.method public getPasswordMinNumeric()I
    .locals 2

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "passwordMinNumeric":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 166
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordMinimumNumeric()I

    move-result v0

    .line 168
    :cond_0
    return v0
.end method

.method public getPasswordMinSymbols()I
    .locals 2

    .prologue
    .line 156
    const/4 v0, 0x0

    .line 157
    .local v0, "passwordMinSymbols":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 158
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordMinimumSymbols()I

    move-result v0

    .line 160
    :cond_0
    return v0
.end method

.method public getPasswordMinUpperCase()I
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 141
    .local v0, "passwordMinUpperCase":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 142
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordMinimumUpperCase()I

    move-result v0

    .line 144
    :cond_0
    return v0
.end method

.method public getPasswordMinimumLength()I
    .locals 2

    .prologue
    .line 124
    const/4 v0, 0x4

    .line 125
    .local v0, "passwordMinLength":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 126
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordMinimumLength()I

    move-result v0

    .line 128
    :cond_0
    return v0
.end method

.method public getPasswordQuality()I
    .locals 2

    .prologue
    .line 108
    const/high16 v0, 0x20000

    .line 109
    .local v0, "pq":I
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 110
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getPasswordQuality()I

    move-result v0

    .line 112
    :cond_0
    return v0
.end method

.method public getRequiredPwdPatternRestrictions()Ljava/lang/String;
    .locals 2

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "reqPatternRestriction":Ljava/lang/String;
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 230
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getRequiredPwdPatternRestrictions()Ljava/lang/String;

    move-result-object v0

    .line 232
    :cond_0
    return-object v0
.end method

.method public getResetPasswordKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    if-eqz v0, :cond_0

    .line 473
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    invoke-virtual {v0}, Lcom/sec/knox/container/ContainerCreationParams;->getResetPasswordKey()Ljava/lang/String;

    move-result-object v0

    .line 475
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSimplePasswordEnabled()Z
    .locals 2

    .prologue
    .line 236
    const/4 v0, 0x0

    .line 237
    .local v0, "simplePasswordEnabled":Z
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    if-eqz v1, :cond_0

    .line 238
    sget-object v1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getSimplePasswordEnabled()Z

    move-result v0

    .line 240
    :cond_0
    return v0
.end method

.method public hasForbiddenCharacterSequence(Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 258
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containsForbiddenCharacterSequence(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const/4 v0, 0x1

    .line 260
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasForbiddenData(Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 270
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containsForbiddenData(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const/4 v0, 0x1

    .line 272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasForbiddenNumericSequence(Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 252
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containsForbiddenNumericSequence(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    const/4 v0, 0x1

    .line 254
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasForbiddenStringDistance(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 264
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containsForbiddenStringDistance(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    const/4 v0, 0x1

    .line 266
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMaxRepeatedCharacters(Ljava/lang/String;)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-direct {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containsMaxRepeatedCharacters(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    const/4 v0, 0x1

    .line 278
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContainerCreationInitialized()Z
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x1

    .line 61
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPasswordPatternMatched(Ljava/lang/String;)Z
    .locals 4
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 460
    const/4 v3, 0x1

    .line 462
    .local v3, "result":Z
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->getRequiredPwdPatternRestrictions()Ljava/lang/String;

    move-result-object v2

    .line 463
    .local v2, "patternSet":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 464
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 465
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 466
    .local v0, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    .line 468
    .end local v0    # "match":Ljava/util/regex/Matcher;
    .end local v1    # "pattern":Ljava/util/regex/Pattern;
    :cond_0
    return v3
.end method

.method public resetCreationInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 282
    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->instance:Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;

    .line 283
    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    .line 284
    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 285
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setName(Ljava/lang/String;)V

    .line 286
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPassword(Ljava/lang/String;)V

    .line 287
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setPasswordType(I)V

    .line 288
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setBackupPin(Ljava/lang/String;)V

    .line 289
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setContainerCreationParams(Lcom/sec/knox/container/ContainerCreationParams;)V

    .line 290
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setFingerprintPlus(Z)V

    .line 291
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationInfo;->setSimplePassword(Z)V

    .line 292
    return-void
.end method

.method public setContainerCreationParam(Lcom/sec/knox/container/ContainerCreationParams;)V
    .locals 1
    .param p1, "param"    # Lcom/sec/knox/container/ContainerCreationParams;

    .prologue
    .line 51
    sput-object p1, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    .line 52
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    invoke-virtual {v0}, Lcom/sec/knox/container/ContainerCreationParams;->getConfigurationType()Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v0

    sput-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->knoxConfigurationtype:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 55
    :cond_0
    return-void
.end method

.method public setContainerCreationResult(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "result"    # Z

    .prologue
    .line 479
    if-eqz p2, :cond_1

    .line 480
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    if-eqz v0, :cond_0

    .line 481
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainerMarkSuccess(Lcom/sec/knox/container/ContainerCreationParams;)Z

    .line 490
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->resetCreationInfo()V

    .line 491
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardRootActivity;->setNextStage(I)V

    .line 492
    invoke-static {p1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardNotification;->hideSetupWizardNotification(Landroid/content/Context;)V

    .line 493
    return-void

    .line 484
    :cond_1
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    if-eqz v0, :cond_0

    .line 485
    sget-object v0, Lcom/sec/knox/knoxsetupwizardclient/core/ContainerCreationHelper;->containerCreationParams:Lcom/sec/knox/container/ContainerCreationParams;

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->cancelCreateContainer(Lcom/sec/knox/container/ContainerCreationParams;)Z

    .line 486
    const-string v0, "cancel Creation"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->i(Ljava/lang/String;)V

    goto :goto_0
.end method
