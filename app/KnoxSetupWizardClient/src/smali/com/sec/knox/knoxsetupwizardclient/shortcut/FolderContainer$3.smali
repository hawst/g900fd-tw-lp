.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;
.super Ljava/lang/Object;
.source "FolderContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 0

    .prologue
    .line 582
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const v3, 0x7f0c00f7

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 586
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->defaultbg:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1600(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 588
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0f0076

    if-ne v0, v1, :cond_1

    .line 590
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v0, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->setAppSelectionStatus(I)V

    .line 591
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v4, v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->allAppsSelected:I
    invoke-static {v0, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1402(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;I)I

    .line 594
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 595
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->notifyDataSetChanged()V

    .line 596
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v0, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->enableDoneBtn(Z)V

    .line 608
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0f0078

    if-ne v0, v1, :cond_0

    .line 600
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v0, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->setAppSelectionStatus(I)V

    .line 601
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "   "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 602
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    const/4 v1, 0x2

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->allAppsSelected:I
    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1402(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;I)I

    .line 604
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mPopUp:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$1500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 605
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v0, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->notifyDataSetChanged()V

    .line 606
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$3;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v0, v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->enableDoneBtn(Z)V

    goto :goto_0
.end method
