.class Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;
.super Landroid/content/pm/IPackageStatsObserver$Stub;
.source "PackageInstallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->getPackagesToInstall()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

.field final synthetic val$info:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    iput-object p2, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;->val$info:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    invoke-direct {p0}, Landroid/content/pm/IPackageStatsObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    .locals 9
    .param p1, "pStats"    # Landroid/content/pm/PackageStats;
    .param p2, "succeeded"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/high16 v6, 0x44800000    # 1024.0f

    .line 192
    const-string v3, "PackageInstallManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGetStatsCompleted : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    iget-wide v4, p1, Landroid/content/pm/PackageStats;->codeSize:J

    long-to-float v3, v4

    div-float v0, v3, v6

    .line 194
    .local v0, "sizeKb":F
    div-float v2, v0, v6

    .line 195
    .local v2, "sizeMb":F
    float-to-int v1, v0

    .line 196
    .local v1, "sizeKbint":I
    cmpg-float v3, v0, v6

    if-gez v3, :cond_0

    .line 197
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;->val$info:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\u00a0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->access$000(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0125

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->storage:Ljava/lang/String;

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;->val$info:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "%.2f"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\u00a0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;->access$000(Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0126

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/knox/knoxsetupwizardclient/viewbased/PackageInstallManager$SelectableAppInfo;->storage:Ljava/lang/String;

    goto :goto_0
.end method
