.class Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;
.super Landroid/os/AsyncTask;
.source "FolderContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackgroundTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private dialog:Landroid/app/ProgressDialog;

.field private observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;


# direct methods
.method public constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V
    .locals 3
    .param p2, "activity"    # Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 245
    new-instance v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$1;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    .line 246
    new-instance v0, Landroid/app/ProgressDialog;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x1030128

    invoke-direct {v1, p2, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->dialog:Landroid/app/ProgressDialog;

    .line 247
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 240
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 19
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v10

    .line 303
    .local v10, "count":I
    if-lez v10, :cond_6

    .line 305
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemList()Ljava/util/List;

    move-result-object v15

    .line 307
    .local v15, "rList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    if-eqz v15, :cond_6

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 310
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v18

    .line 312
    .local v18, "size":I
    move/from16 v0, v18

    new-array v8, v0, [Z

    .line 313
    .local v8, "alreadyRemoved":[Z
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move/from16 v0, v18

    if-ge v14, v0, :cond_0

    .line 314
    const/4 v2, 0x0

    aput-boolean v2, v8, v14

    .line 313
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 317
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->setNumberOfShouldBeRemoved(I)V

    .line 320
    const/4 v14, 0x0

    :goto_1
    move/from16 v0, v18

    if-ge v14, v0, :cond_3

    .line 321
    invoke-interface {v15, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;

    .line 322
    .local v9, "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    invoke-virtual {v9}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getPersonaId()I

    move-result v6

    .line 323
    .local v6, "personaId":I
    invoke-virtual {v9}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->getDbId()J

    move-result-wide v16

    .line 324
    .local v16, "rowId":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mDbHelper:Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;

    move-wide/from16 v0, v16

    invoke-virtual {v2, v6, v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/db/KnoxSetupWizardDbHelper;->deleteShortcut(IJ)I

    .line 327
    :try_start_0
    aget-boolean v2, v8, v14

    if-nez v2, :cond_2

    iget v2, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 328
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "disableFlag("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const/4 v2, 0x1

    aput-boolean v2, v8, v14

    .line 330
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iPackageManager:Landroid/content/pm/IPackageManager;

    iget-object v3, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    const/4 v4, 0x3

    const/4 v5, 0x1

    sget-object v7, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-interface/range {v2 .. v7}, Landroid/content/pm/IPackageManager;->setApplicationEnabledSetting(Ljava/lang/String;IIILjava/lang/String;)V

    .line 334
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    invoke-virtual {v2}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->disableTheApp()V

    .line 320
    :cond_1
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 336
    :cond_2
    aget-boolean v2, v8, v14

    if-nez v2, :cond_1

    iget v2, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->removeableFlag:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 337
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removableFlag("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->activityName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    const/4 v2, 0x1

    aput-boolean v2, v8, v14

    .line 339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->iPackageManager:Landroid/content/pm/IPackageManager;

    iget-object v3, v9, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;->mPkgName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mCurrentUserId:I

    const/4 v7, 0x0

    invoke-interface {v2, v3, v4, v5, v7}, Landroid/content/pm/IPackageManager;->deletePackageAsUser(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_2

    .line 342
    :catch_0
    move-exception v12

    .line 344
    .local v12, "e1":Landroid/os/RemoteException;
    invoke-virtual {v12}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 346
    .end local v12    # "e1":Landroid/os/RemoteException;
    :catch_1
    move-exception v13

    .line 348
    .local v13, "e2":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 352
    .end local v6    # "personaId":I
    .end local v9    # "appInfo":Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;
    .end local v13    # "e2":Ljava/lang/Exception;
    .end local v16    # "rowId":J
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    monitor-enter v3

    .line 353
    :goto_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    iget-boolean v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->bFinished:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v2, :cond_4

    .line 355
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 356
    :catch_2
    move-exception v11

    .line 357
    .local v11, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v11}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 364
    .end local v11    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 361
    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->observer:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;

    iget v2, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$PackageDeleteObserver;->result:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    .line 362
    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Multiple apps were removed or disabled successfully."

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_5
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 370
    .end local v8    # "alreadyRemoved":[Z
    .end local v14    # "i":I
    .end local v15    # "rList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    .end local v18    # "size":I
    :cond_6
    const/4 v2, 0x0

    return-object v2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 240
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 9
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 257
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemCount()I

    move-result v0

    .line 258
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 259
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->getSelectedItemList()Ljava/util/List;

    move-result-object v2

    .line 260
    .local v2, "rList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 261
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->loadAppList()V

    .line 262
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->setMoreLayoutBtnEnabling()V
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$200(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)V

    .line 266
    .end local v2    # "rList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxAppInfo;>;"
    :cond_0
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 268
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEmptyListText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$300(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 269
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3, v8}, Landroid/widget/GridView;->setVisibility(I)V

    .line 271
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$400(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 272
    .local v1, "lparam":Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mShortcutUtil:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v4, v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->matrixLayoutInfos:[I

    const/4 v5, 0x4

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxShortcutUtil;->getPixelbyDp(I)I

    move-result v3

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 273
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mFolderArea:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$400(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 289
    .end local v1    # "lparam":Landroid/view/ViewGroup$LayoutParams;
    :goto_0
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->itemSelector:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$800(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 290
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 291
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->showRemoveLayout(Z)V
    invoke-static {v3, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$900(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Z)V

    .line 293
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 294
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 297
    :cond_1
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iput-boolean v7, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mIsUninstall:Z

    .line 298
    return-void

    .line 277
    :cond_2
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    new-instance v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxFolderActivity:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;
    invoke-static {v5}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$500(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v6, v6, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-direct {v4, v5, v6}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;-><init>(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;Ljava/util/List;)V

    iput-object v4, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    .line 278
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v3, v7}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;->setUninstallMode(Z)V

    .line 279
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v4, v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mKnoxAppAdapter:Lcom/sec/knox/knoxsetupwizardclient/shortcut/KnoxFolderAppInfoAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 280
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->addIconAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$600(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v4, v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->listAppInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v5, v5, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;->setIconCount(II)V

    .line 281
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/GridView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget v4, v4, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->maxGridXCount:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 282
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mAddGrid:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$700(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/GridView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->addIconAdapter:Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;
    invoke-static {v4}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$600(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Lcom/sec/knox/knoxsetupwizardclient/viewbased/AddIconAdapter;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 284
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mEmptyListText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->access$300(Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3, v7}, Landroid/widget/GridView;->setVisibility(I)V

    .line 286
    iget-object v3, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    iget-object v3, v3, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->mGridMain:Landroid/widget/GridView;

    invoke-virtual {v3}, Landroid/widget/GridView;->invalidate()V

    goto/16 :goto_0
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->dialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->this$0:Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;

    invoke-virtual {v1}, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0100

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/shortcut/FolderContainer$BackgroundTask;->dialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 253
    return-void
.end method
