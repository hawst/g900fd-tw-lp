.class Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardResetPasswordActivity.java"

# interfaces
.implements Lcom/sec/knox/knoxsetupwizardclient/SetupWizardFingerprintListenerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;


# direct methods
.method constructor <init>(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public resultFailed()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z
    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$102(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z

    .line 81
    invoke-static {}, Lcom/samsung/android/fingerprint/FingerprintManager;->isEnrolling()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    # getter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFPM:Lcom/samsung/android/fingerprint/FingerprintManager;
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$300(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)Lcom/samsung/android/fingerprint/FingerprintManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/fingerprint/FingerprintManager;->notifyEnrollEnd()Z

    .line 84
    :cond_0
    return-void
.end method

.method public resultSuccess()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfirmed:Z
    invoke-static {v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$002(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z

    .line 70
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mFingerprintConfimationStarted:Z
    invoke-static {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$102(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z

    .line 72
    const-string v0, "mIsPrimarylockSet become true in mKFPListener"

    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/KnoxLog;->d(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    # setter for: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->mIsPrimaryLockSet:Z
    invoke-static {v0, v2}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$202(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;Z)Z

    .line 74
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    invoke-virtual {v0, v1}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->updateUnlockMethodAndFinish(I)V

    .line 76
    return-void
.end method

.method public sensorError()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity$1;->this$0:Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;

    # invokes: Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->showSensorErrorDialog()V
    invoke-static {v0}, Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;->access$400(Lcom/sec/knox/knoxsetupwizardclient/SetupWizardResetPasswordActivity;)V

    .line 89
    return-void
.end method
