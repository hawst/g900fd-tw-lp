.class public Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;
.super Lorg/simalliance/openmobileapi/service/Terminal;
.source "UiccTerminal.java"


# static fields
.field public static final UICC_TERMINAL_TAG:Ljava/lang/String; = "UICCTerminal"


# instance fields
.field private channelId:[I

.field private currentSelectedFilePath:Ljava/lang/String;

.field private mAtr:[B

.field private mContentResolver:Landroid/content/ContentResolver;

.field mContext:Landroid/content/Context;

.field private manager:Lcom/android/internal/telephony/ITelephony;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 70
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_SetSecureEventType"

    const-string v4, "GSMA"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ISIS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v2, "SIM - UICC"

    :goto_0
    invoke-direct {p0, v2, p1}, Lorg/simalliance/openmobileapi/service/Terminal;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 54
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    .line 61
    const-string v2, ""

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->currentSelectedFilePath:Ljava/lang/String;

    .line 63
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mAtr:[B

    .line 73
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mContext:Landroid/content/Context;

    .line 74
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mContentResolver:Landroid/content/ContentResolver;

    .line 77
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 78
    .local v1, "salesCode":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "KDI"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 80
    const/4 v2, 0x3

    new-array v2, v2, [I

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    .line 87
    :goto_1
    :try_start_0
    const-string v2, "phone"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v2

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 93
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    const/4 v3, 0x0

    aput v3, v2, v0

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 70
    .end local v0    # "i":I
    .end local v1    # "salesCode":Ljava/lang/String;
    :cond_1
    const-string v2, "SIM1"

    goto :goto_0

    .line 82
    .restart local v1    # "salesCode":Ljava/lang/String;
    :cond_2
    const/16 v2, 0x14

    new-array v2, v2, [I

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    goto :goto_1

    .line 94
    .restart local v0    # "i":I
    :cond_3
    return-void

    .line 89
    .end local v0    # "i":I
    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method private ByteArrayToString([BI)Ljava/lang/String;
    .locals 4
    .param p1, "b"    # [B
    .param p2, "start"    # I

    .prologue
    .line 151
    if-nez p1, :cond_0

    .line 152
    const/4 v2, 0x0

    .line 157
    :goto_0
    return-object v2

    .line 153
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 154
    .local v1, "s":Ljava/lang/StringBuffer;
    move v0, p2

    .local v0, "i":I
    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 155
    aget-byte v2, p1, v0

    and-int/lit16 v2, v2, 0xff

    add-int/lit16 v2, v2, 0x100

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 157
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private StringToByteArray(Ljava/lang/String;)[B
    .locals 4
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    new-array v0, v2, [B

    .line 144
    .local v0, "b":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 145
    mul-int/lit8 v2, v1, 0x2

    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 147
    :cond_0
    return-object v0
.end method

.method private clearChannelNumber(B)B
    .locals 2
    .param p1, "cla"    # B

    .prologue
    .line 169
    and-int/lit8 v1, p1, 0x40

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 171
    .local v0, "isFirstInterindustryClassByteCoding":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 174
    and-int/lit16 v1, p1, 0xfc

    int-to-byte v1, v1

    .line 178
    :goto_1
    return v1

    .line 169
    .end local v0    # "isFirstInterindustryClassByteCoding":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 178
    .restart local v0    # "isFirstInterindustryClassByteCoding":Z
    :cond_1
    and-int/lit16 v1, p1, 0xf0

    int-to-byte v1, v1

    goto :goto_1
.end method

.method private static getSystemProperty(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "subId"    # J
    .param p3, "defaultVal"    # Ljava/lang/String;

    .prologue
    .line 98
    const/4 v2, 0x0

    .line 99
    .local v2, "propVal":Ljava/lang/String;
    invoke-static {p1, p2}, Landroid/telephony/SubscriptionManager;->getPhoneId(J)I

    move-result v0

    .line 100
    .local v0, "phoneId":I
    invoke-static {p0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 101
    .local v1, "prop":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    .line 102
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, "values":[Ljava/lang/String;
    if-ltz v0, :cond_0

    array-length v4, v3

    if-ge v0, v4, :cond_0

    aget-object v4, v3, v0

    if-eqz v4, :cond_0

    .line 104
    aget-object v2, v3, v0

    .line 107
    .end local v3    # "values":[Ljava/lang/String;
    :cond_0
    if-nez v2, :cond_1

    .end local p3    # "defaultVal":Ljava/lang/String;
    :goto_0
    return-object p3

    .restart local p3    # "defaultVal":Ljava/lang/String;
    :cond_1
    move-object p3, v2

    goto :goto_0
.end method

.method private parseChannelNumber(B)I
    .locals 2
    .param p1, "cla"    # B

    .prologue
    .line 272
    and-int/lit8 v1, p1, 0x40

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 274
    .local v0, "isFirstInterindustryClassByteCoding":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 277
    and-int/lit8 v1, p1, 0x3

    .line 281
    :goto_1
    return v1

    .line 272
    .end local v0    # "isFirstInterindustryClassByteCoding":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 281
    .restart local v0    # "isFirstInterindustryClassByteCoding":Z
    :cond_1
    and-int/lit8 v1, p1, 0xf

    add-int/lit8 v1, v1, 0x4

    goto :goto_1
.end method


# virtual methods
.method public getAtr()[B
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 355
    :try_start_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mAtr:[B

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->isCardPresent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 358
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v2}, Lcom/android/internal/telephony/ITelephony;->getAtr()[B

    move-result-object v0

    .line 359
    .local v0, "atr":[B
    array-length v2, v0

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    const/4 v2, 0x0

    aget-byte v2, v0, v2

    if-nez v2, :cond_1

    .line 361
    const-string v2, "UICCTerminal"

    const-string v4, "There was a problem getting ATR"

    invoke-static {v2, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/simalliance/openmobileapi/service/CardException; {:try_start_0 .. :try_end_0} :catch_1

    .line 380
    .end local v0    # "atr":[B
    :cond_0
    :goto_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mAtr:[B

    :goto_1
    return-object v2

    .line 365
    .restart local v0    # "atr":[B
    :cond_1
    :try_start_1
    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    check-cast v2, [B

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mAtr:[B
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/simalliance/openmobileapi/service/CardException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 370
    .end local v0    # "atr":[B
    :catch_0
    move-exception v1

    .line 372
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v2, v3

    .line 373
    goto :goto_1

    .line 375
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 377
    .local v1, "e":Lorg/simalliance/openmobileapi/service/CardException;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/CardException;->printStackTrace()V

    move-object v2, v3

    .line 378
    goto :goto_1
.end method

.method protected internalCloseLogicalChannel(I)V
    .locals 3
    .param p1, "channelNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 335
    if-nez p1, :cond_0

    .line 349
    :goto_0
    return-void

    .line 338
    :cond_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    aget v1, v1, p1

    if-nez v1, :cond_1

    .line 339
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v2, "channel not open"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 342
    :cond_1
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    aget v2, v2, p1

    invoke-interface {v1, v2}, Lcom/android/internal/telephony/ITelephony;->iccCloseLogicalChannel(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 343
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v2, "close channel failed"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "ex":Ljava/lang/Exception;
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v2, "close channel failed"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 348
    .end local v0    # "ex":Ljava/lang/Exception;
    :cond_2
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    const/4 v2, 0x0

    aput v2, v1, p1

    goto :goto_0
.end method

.method protected internalConnect()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    if-nez v0, :cond_0

    .line 128
    const-string v0, "UICCTerminal"

    const-string v1, "manager is null in internalConnect(), try to connect again"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    .line 131
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    if-nez v0, :cond_1

    .line 132
    const-string v0, "UICCTerminal"

    const-string v1, "finally, manager is null so I give up."

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    new-instance v0, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v1, "Cannot connect to Telephony Service"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mIsConnected:Z

    .line 136
    return-void
.end method

.method protected internalDisconnect()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 140
    return-void
.end method

.method protected internalOpenLogicalChannel()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 288
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mSelectResponse:[B

    .line 289
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Opening a logical channel with the UICC without specifying the AID is not authorized"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected internalOpenLogicalChannel([B)I
    .locals 7
    .param p1, "aid"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 296
    if-nez p1, :cond_0

    .line 297
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "aid must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 299
    :cond_0
    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mSelectResponse:[B

    .line 300
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 301
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    aget v3, v3, v0

    if-nez v3, :cond_4

    .line 304
    array-length v3, p1

    if-nez v3, :cond_1

    .line 305
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v3, v4}, Lcom/android/internal/telephony/ITelephony;->iccOpenLogicalChannel(Ljava/lang/String;)Landroid/telephony/IccOpenLogicalChannelResponse;

    move-result-object v2

    .line 310
    .local v2, "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    :goto_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    invoke-virtual {v2}, Landroid/telephony/IccOpenLogicalChannelResponse;->getChannel()I

    move-result v4

    aput v4, v3, v0

    .line 311
    invoke-virtual {v2}, Landroid/telephony/IccOpenLogicalChannelResponse;->getSelectResponse()[B

    move-result-object v3

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mSelectResponse:[B

    .line 313
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    aget v3, v3, v0

    if-gtz v3, :cond_6

    .line 314
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    aput v5, v3, v0

    .line 315
    invoke-virtual {v2}, Landroid/telephony/IccOpenLogicalChannelResponse;->getStatus()I

    move-result v1

    .line 316
    .local v1, "lastError":I
    const-string v3, "UICCTerminal"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "lastError : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 319
    new-instance v3, Ljava/util/MissingResourceException;

    const-string v4, "all channels are used"

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 307
    .end local v1    # "lastError":I
    .end local v2    # "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    :cond_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    invoke-direct {p0, p1, v5}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->ByteArrayToString([BI)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/internal/telephony/ITelephony;->iccOpenLogicalChannel(Ljava/lang/String;)Landroid/telephony/IccOpenLogicalChannelResponse;

    move-result-object v2

    .restart local v2    # "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    goto :goto_1

    .line 322
    .restart local v1    # "lastError":I
    :cond_2
    const/4 v3, 0x3

    if-ne v1, v3, :cond_3

    .line 323
    new-instance v3, Ljava/util/NoSuchElementException;

    const-string v4, "applet not found"

    invoke-direct {v3, v4}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 325
    :cond_3
    new-instance v3, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v4, "open channel failed"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 300
    .end local v1    # "lastError":I
    .end local v2    # "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 329
    :cond_5
    new-instance v3, Ljava/util/MissingResourceException;

    const-string v4, "out of channels"

    const-string v5, ""

    const-string v6, ""

    invoke-direct {v3, v4, v5, v6}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v3

    .line 327
    .restart local v2    # "openChannelResp":Landroid/telephony/IccOpenLogicalChannelResponse;
    :cond_6
    return v0
.end method

.method protected internalTransmit([B)[B
    .locals 13
    .param p1, "command"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v1, 0x0

    .line 184
    aget-byte v0, p1, v1

    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->clearChannelNumber(B)B

    move-result v0

    and-int/lit16 v2, v0, 0xff

    .line 185
    .local v2, "cla":I
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    and-int/lit16 v3, v0, 0xff

    .line 186
    .local v3, "ins":I
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    and-int/lit16 v4, v0, 0xff

    .line 187
    .local v4, "p1":I
    const/4 v0, 0x3

    aget-byte v0, p1, v0

    and-int/lit16 v5, v0, 0xff

    .line 188
    .local v5, "p2":I
    const/4 v6, -0x1

    .line 189
    .local v6, "p3":I
    array-length v0, p1

    if-le v0, v11, :cond_0

    .line 190
    aget-byte v0, p1, v11

    and-int/lit16 v6, v0, 0xff

    .line 192
    :cond_0
    const/4 v7, 0x0

    .line 193
    .local v7, "data":Ljava/lang/String;
    array-length v0, p1

    if-le v0, v12, :cond_1

    .line 194
    invoke-direct {p0, p1, v12}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->ByteArrayToString([BI)Ljava/lang/String;

    move-result-object v7

    .line 197
    :cond_1
    aget-byte v0, p1, v1

    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->parseChannelNumber(B)I

    move-result v8

    .line 199
    .local v8, "channelNumber":I
    if-nez v8, :cond_2

    .line 202
    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    const/4 v1, 0x0

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/ITelephony;->iccTransmitApduLogicalChannel(IIIIIILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 203
    .local v10, "response":Ljava/lang/String;
    invoke-direct {p0, v10}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->StringToByteArray(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 215
    :goto_0
    return-object v0

    .line 204
    .end local v10    # "response":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 205
    .local v9, "ex":Ljava/lang/Exception;
    new-instance v0, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v1, "transmit command failed"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    .end local v9    # "ex":Ljava/lang/Exception;
    :cond_2
    if-lez v8, :cond_3

    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    aget v0, v0, v8

    if-nez v0, :cond_3

    .line 210
    new-instance v0, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v1, "channel not open"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 214
    :cond_3
    :try_start_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->channelId:[I

    aget v1, v1, v8

    invoke-interface/range {v0 .. v7}, Lcom/android/internal/telephony/ITelephony;->iccTransmitApduLogicalChannel(IIIIIILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 215
    .restart local v10    # "response":Ljava/lang/String;
    invoke-direct {p0, v10}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->StringToByteArray(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 216
    .end local v10    # "response":Ljava/lang/String;
    :catch_1
    move-exception v9

    .line 217
    .restart local v9    # "ex":Ljava/lang/Exception;
    new-instance v0, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v1, "transmit command failed"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method isAirplaneModeOn()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 403
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isCardPresent()Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 111
    invoke-static {v3}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 112
    .local v1, "subId1":[J
    const-string v4, "gsm.sim.state"

    aget-wide v6, v1, v3

    const-string v5, ""

    invoke-static {v4, v6, v7, v5}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->getSystemProperty(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "prop":Ljava/lang/String;
    const-string v4, "UICCTerminal"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isCardPresent: SIM State - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v4, "READY"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "PIN_REQUIRED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 122
    :cond_0
    :goto_0
    return v2

    .line 118
    :cond_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->isAirplaneModeOn()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 119
    const-string v3, "UICCTerminal"

    const-string v4, "AirPlane Mode is enabled"

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v2, v3

    .line 122
    goto :goto_0
.end method

.method public openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .locals 2
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 391
    const-string v0, "UICCTerminal"

    const-string v1, "Can\'t open basic channel"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    const/4 v0, 0x0

    return-object v0
.end method

.method public openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .locals 2
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "aid"    # [B
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 397
    const-string v0, "UICCTerminal"

    const-string v1, "Can\'t open basic channel"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    const/4 v0, 0x0

    return-object v0
.end method

.method protected declared-synchronized protocolTransmit([B)[B
    .locals 1
    .param p1, "cmd"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 385
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->internalTransmit([B)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public simIOExchange(ILjava/lang/String;[B)[B
    .locals 9
    .param p1, "fileID"    # I
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "cmd"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 237
    const/4 v2, 0x0

    .line 238
    .local v2, "ins":I
    const/4 v0, 0x2

    :try_start_0
    aget-byte v0, p3, v0

    and-int/lit16 v3, v0, 0xff

    .line 239
    .local v3, "p1":I
    const/4 v0, 0x3

    aget-byte v0, p3, v0

    and-int/lit16 v4, v0, 0xff

    .line 240
    .local v4, "p2":I
    const/4 v0, 0x4

    aget-byte v0, p3, v0

    and-int/lit16 v5, v0, 0xff

    .line 241
    .local v5, "p3":I
    const/4 v0, 0x1

    aget-byte v0, p3, v0

    sparse-switch v0, :sswitch_data_0

    .line 246
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v1, "Unknown SIM_IO command"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    .end local v3    # "p1":I
    .end local v4    # "p2":I
    .end local v5    # "p3":I
    :catch_0
    move-exception v7

    .line 258
    .local v7, "e":Ljava/lang/Exception;
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "SIM IO access error"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v3    # "p1":I
    .restart local v4    # "p2":I
    .restart local v5    # "p3":I
    :sswitch_0
    const/16 v2, 0xb0

    .line 249
    :goto_0
    if-eqz p2, :cond_0

    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 250
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->currentSelectedFilePath:Ljava/lang/String;

    .line 253
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->manager:Lcom/android/internal/telephony/ITelephony;

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->currentSelectedFilePath:Ljava/lang/String;

    move v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/android/internal/telephony/ITelephony;->iccExchangeSimIO(IIIIILjava/lang/String;)[B

    move-result-object v8

    .line 254
    .local v8, "ret":[B
    const-string v0, "UICCTerminal"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "response data "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v6, 0x0

    invoke-direct {p0, v8, v6}, Lorg/simalliance/openmobileapi/service/terminals/UiccTerminal;->ByteArrayToString([BI)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 256
    return-object v8

    .line 243
    .end local v8    # "ret":[B
    :sswitch_1
    const/16 v2, 0xb2

    goto :goto_0

    .line 244
    :sswitch_2
    const/16 v2, 0xc0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xf

    goto :goto_0

    .line 241
    :sswitch_data_0
    .sparse-switch
        -0x5c -> :sswitch_2
        -0x50 -> :sswitch_0
        -0x4e -> :sswitch_1
    .end sparse-switch
.end method
