.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "Hash_REF_DO.java"


# static fields
.field public static final _SHA1_LEN:I = 0x14

.field public static final _TAG:I = 0xc1


# instance fields
.field private mHash:[B


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 49
    const/16 v0, 0xc1

    invoke-direct {p0, v1, v0, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 37
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    .line 50
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    .line 51
    return-void
.end method

.method public constructor <init>([B)V
    .locals 3
    .param p1, "hash"    # [B

    .prologue
    const/4 v1, 0x0

    .line 44
    const/16 v2, 0xc1

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, p1, v2, v1, v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    .line 45
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    .line 46
    return-void

    .line 44
    :cond_0
    array-length v0, p1

    goto :goto_0
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    .line 40
    const/16 v0, 0xc1

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    .line 41
    return-void
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 3
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    .line 124
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    array-length v1, v1

    const/16 v2, 0x14

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    array-length v1, v1

    if-nez v1, :cond_0

    .line 126
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v2, "Hash value must be 20 bytes in length!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 129
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getTag()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 131
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-nez v1, :cond_1

    .line 132
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 141
    :goto_0
    return-void

    .line 135
    :cond_1
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    array-length v1, v1

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 136
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v2, "Hash could not be written!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 145
    const/4 v0, 0x0

    .line 147
    .local v0, "equals":Z
    instance-of v4, p1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-eqz v4, :cond_0

    .line 148
    invoke-super {p0, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 150
    if-eqz v0, :cond_0

    move-object v1, p1

    .line 151
    check-cast v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .line 152
    .local v1, "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-nez v4, :cond_1

    iget-object v4, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-nez v4, :cond_1

    .line 153
    and-int/lit8 v0, v0, 0x1

    .line 166
    .end local v1    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :cond_0
    :goto_0
    return v0

    .line 155
    .restart local v1    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :cond_1
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-nez v4, :cond_3

    iget-object v4, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-eqz v4, :cond_3

    .line 156
    iget-object v4, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    array-length v4, v4

    if-nez v4, :cond_2

    :goto_1
    and-int/2addr v0, v2

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_1

    .line 157
    :cond_3
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-eqz v4, :cond_5

    iget-object v4, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    if-nez v4, :cond_5

    .line 158
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    array-length v4, v4

    if-nez v4, :cond_4

    :goto_2
    and-int/2addr v0, v2

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_2

    .line 161
    :cond_5
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    iget-object v3, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    and-int/2addr v0, v2

    goto :goto_0
.end method

.method public getHash()[B
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    return-object v0
.end method

.method public interpret()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/16 v3, 0x14

    .line 87
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    .line 89
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getRawData()[B

    move-result-object v0

    .line 90
    .local v0, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getValueIndex()I

    move-result v1

    .line 93
    .local v1, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getValueLength()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getValueLength()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 94
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Invalid value length for Hash-REF-DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 97
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getValueLength()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 98
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getValueLength()I

    move-result v2

    add-int/2addr v2, v1

    array-length v3, v0

    if-le v2, v3, :cond_1

    .line 99
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Not enough data for Hash-REF-DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 102
    :cond_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getValueLength()I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    .line 103
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->mHash:[B

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getValueLength()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 105
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .local v0, "b":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 61
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    const-string v3, "Hash_REF_DO: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    :try_start_0
    invoke-virtual {p0, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 64
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-static {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->toHex([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
