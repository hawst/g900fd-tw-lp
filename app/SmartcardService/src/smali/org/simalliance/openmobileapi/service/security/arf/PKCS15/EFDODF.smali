.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;
.super Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.source "EFDODF.java"


# static fields
.field public static final AC_OID:Ljava/lang/String; = "1.2.840.114283.200.1.1"

.field public static final TAG:Ljava/lang/String; = "ACE ARF EF_DODF"


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V
    .locals 0
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 84
    return-void
.end method

.method private decodeDER([B)[B
    .locals 7
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    const/16 v6, -0x5f

    const/16 v5, 0x30

    .line 47
    const/4 v1, 0x0

    .line 48
    .local v1, "context":[S
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 50
    .local v0, "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :goto_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v3

    if-nez v3, :cond_4

    .line 51
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v3

    if-ne v3, v6, :cond_3

    .line 53
    invoke-virtual {v0, v5}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 54
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V

    .line 56
    invoke-virtual {v0, v5}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 57
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V

    .line 59
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v2

    .line 60
    .local v2, "objectType":B
    const/16 v3, -0x60

    if-ne v2, v3, :cond_0

    .line 61
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V

    .line 62
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v2

    .line 64
    :cond_0
    if-ne v2, v6, :cond_2

    .line 65
    invoke-virtual {v0, v5}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 66
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->saveContext()[S

    move-result-object v1

    .line 67
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseOID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1.2.840.114283.200.1.1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    .line 68
    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->restoreContext([S)V

    .line 69
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V

    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parsePathAttributes()[B

    move-result-object v3

    .line 74
    .end local v2    # "objectType":B
    :goto_1
    return-object v3

    .line 71
    .restart local v2    # "objectType":B
    :cond_2
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v4, "[Parser] OID Tag expected"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 72
    .end local v2    # "objectType":B
    :cond_3
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V

    goto :goto_0

    .line 74
    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public analyseFile([B)[B
    .locals 2
    .param p1, "path"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 95
    const-string v0, "ACE ARF EF_DODF"

    const-string v1, "Analysing EF_DODF..."

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;->selectFile([B)I

    move-result v0

    const v1, 0x9000

    if-eq v0, v1, :cond_0

    .line 98
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "EF_DODF not found!"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_0
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;->readBinary(II)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;->decodeDER([B)[B

    move-result-object v0

    return-object v0
.end method
