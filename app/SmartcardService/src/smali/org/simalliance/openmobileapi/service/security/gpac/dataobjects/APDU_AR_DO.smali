.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "APDU_AR_DO.java"


# static fields
.field public static final _TAG:I = 0xd0


# instance fields
.field private mApduAllowed:Z

.field private mApduHeader:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field

.field private mFilterMask:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<[B>;",
            "Ljava/util/ArrayList",
            "<[B>;)V"
        }
    .end annotation

    .prologue
    .local p1, "apduHeader":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .local p2, "filterMask":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    const/4 v2, 0x0

    .line 52
    const/4 v0, 0x0

    const/16 v1, 0xd0

    invoke-direct {p0, v0, v1, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 38
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    .line 53
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    .line 54
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    .line 55
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3
    .param p1, "allowed"    # Z

    .prologue
    const/4 v2, 0x0

    .line 47
    const/4 v0, 0x0

    const/16 v1, 0xd0

    invoke-direct {p0, v0, v1, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 38
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    .line 48
    iput-boolean p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    .line 49
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    .line 43
    const/16 v0, 0xd0

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    .line 44
    return-void
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 9
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v5, 0x1

    .line 159
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 160
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v6, "APDU filter is invalid"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 164
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getTag()I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 167
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_2

    .line 168
    invoke-virtual {p1, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 169
    iget-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    if-eqz v6, :cond_1

    :goto_0
    invoke-virtual {p1, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 195
    :goto_1
    return-void

    .line 169
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 171
    :cond_2
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 172
    .local v4, "temp":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_5

    .line 173
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 174
    .local v0, "apduHeader":[B
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 176
    .local v2, "filterMask":[B
    array-length v5, v0

    if-ne v5, v8, :cond_3

    array-length v5, v2

    if-eq v5, v8, :cond_4

    .line 177
    :cond_3
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v6, "APDU filter is invalid!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 181
    :cond_4
    :try_start_0
    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 182
    invoke-virtual {v4, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 183
    :catch_0
    move-exception v1

    .line 184
    .local v1, "e":Ljava/io/IOException;
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "APDU Filter Memory IO problem! "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 188
    .end local v0    # "apduHeader":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "filterMask":[B
    :cond_5
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v5

    invoke-static {v5, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->encodeLength(ILjava/io/ByteArrayOutputStream;)V

    .line 190
    :try_start_1
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 191
    :catch_1
    move-exception v1

    .line 192
    .restart local v1    # "e":Ljava/io/IOException;
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "APDU Filter Memory IO problem! "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public getApduHeaderList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFilterMaskList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    return-object v0
.end method

.method public interpret()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 93
    iput-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    .line 94
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 95
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 97
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getRawData()[B

    move-result-object v1

    .line 98
    .local v1, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getValueIndex()I

    move-result v4

    .line 100
    .local v4, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getValueLength()I

    move-result v7

    add-int/2addr v7, v4

    array-length v8, v1

    if-le v7, v8, :cond_0

    .line 101
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Not enough data for APDU_AR_DO!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 107
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getValueLength()I

    move-result v7

    if-ne v7, v5, :cond_3

    .line 108
    aget-byte v7, v1, v4

    if-ne v7, v5, :cond_2

    :goto_0
    iput-boolean v5, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    .line 131
    :cond_1
    return-void

    :cond_2
    move v5, v6

    .line 108
    goto :goto_0

    .line 109
    :cond_3
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getValueLength()I

    move-result v7

    rem-int/lit8 v7, v7, 0x8

    if-nez v7, :cond_4

    .line 110
    iput-boolean v5, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    .line 112
    move v3, v4

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getValueLength()I

    move-result v7

    add-int/2addr v7, v4

    if-ge v3, v7, :cond_1

    .line 113
    new-array v0, v11, [B

    .line 114
    .local v0, "apduHeader":[B
    new-array v2, v11, [B

    .line 116
    .local v2, "filterMask":[B
    add-int/lit8 v7, v3, 0x0

    aget-byte v7, v1, v7

    aput-byte v7, v0, v6

    .line 117
    add-int/lit8 v7, v3, 0x1

    aget-byte v7, v1, v7

    aput-byte v7, v0, v5

    .line 118
    add-int/lit8 v7, v3, 0x2

    aget-byte v7, v1, v7

    aput-byte v7, v0, v9

    .line 119
    add-int/lit8 v7, v3, 0x3

    aget-byte v7, v1, v7

    aput-byte v7, v0, v10

    .line 120
    add-int/lit8 v7, v3, 0x4

    aget-byte v7, v1, v7

    aput-byte v7, v2, v6

    .line 121
    add-int/lit8 v7, v3, 0x5

    aget-byte v7, v1, v7

    aput-byte v7, v2, v5

    .line 122
    add-int/lit8 v7, v3, 0x6

    aget-byte v7, v1, v7

    aput-byte v7, v2, v9

    .line 123
    add-int/lit8 v7, v3, 0x7

    aget-byte v7, v1, v7

    aput-byte v7, v2, v10

    .line 125
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduHeader:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mFilterMask:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    add-int/lit8 v3, v3, 0x8

    goto :goto_1

    .line 129
    .end local v0    # "apduHeader":[B
    .end local v2    # "filterMask":[B
    .end local v3    # "i":I
    :cond_4
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Invalid length of APDU-AR-DO!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public isApduAllowed()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->mApduAllowed:Z

    return v0
.end method
