.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;
.super Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.source "EFACRules.java"


# static fields
.field public static final DEFAULT_APP:[B

.field public static final TAG:Ljava/lang/String; = "ACE ARF EF_ACRules"


# instance fields
.field protected mAcConditionDataCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->DEFAULT_APP:[B

    return-void
.end method

.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V
    .locals 1
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->mAcConditionDataCache:Ljava/util/Map;

    .line 111
    return-void
.end method

.method private decodeDER([B)V
    .locals 8
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    .local v0, "AID":[B
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v1, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 61
    .local v1, "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    const/4 v4, 0x0

    .line 63
    .local v4, "tag":I
    :cond_0
    :goto_0
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v6

    if-nez v6, :cond_2

    .line 64
    const/16 v6, 0x30

    invoke-virtual {v1, v6}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 65
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v6

    sparse-switch v6, :sswitch_data_0

    .line 80
    new-instance v6, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v7, "[Parser] Unexpected ACRules entry"

    invoke-direct {v6, v7}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v6

    .line 67
    :sswitch_0
    const/4 v6, 0x4

    invoke-virtual {v1, v6}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 68
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v0

    .line 69
    const/16 v4, 0x4f

    .line 82
    :goto_1
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parsePathAttributes()[B

    move-result-object v2

    .line 86
    .local v2, "path":[B
    if-eqz v2, :cond_0

    .line 87
    invoke-static {v2}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "pathString":Ljava/lang/String;
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    new-instance v7, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    invoke-direct {v7, v4, v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;-><init>(I[B)V

    invoke-direct {v5, v6, v7}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;)V

    .line 90
    .local v5, "temp":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->mAcConditionDataCache:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 92
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->mAcConditionDataCache:Ljava/util/Map;

    invoke-interface {v6, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    invoke-virtual {v5, v6}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->addRestrictedHashesFromData([B)V

    goto :goto_0

    .line 72
    .end local v2    # "path":[B
    .end local v3    # "pathString":Ljava/lang/String;
    .end local v5    # "temp":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;
    :sswitch_1
    const/4 v0, 0x0

    .line 73
    const/16 v4, 0xc0

    .line 74
    goto :goto_1

    .line 76
    :sswitch_2
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->DEFAULT_APP:[B

    .line 77
    const/16 v4, 0x4f

    .line 78
    goto :goto_1

    .line 95
    .restart local v2    # "path":[B
    .restart local v3    # "pathString":Ljava/lang/String;
    .restart local v5    # "temp":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;
    :cond_1
    invoke-virtual {v5, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->addRestrictedHashes([B)V

    .line 96
    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->getData()[B

    move-result-object v6

    if-eqz v6, :cond_0

    .line 98
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->mAcConditionDataCache:Ljava/util/Map;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->getData()[B

    move-result-object v7

    invoke-interface {v6, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 103
    .end local v2    # "path":[B
    .end local v3    # "pathString":Ljava/lang/String;
    .end local v5    # "temp":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;
    :cond_2
    return-void

    .line 65
    :sswitch_data_0
    .sparse-switch
        -0x7f -> :sswitch_1
        -0x7e -> :sswitch_2
        -0x60 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public analyseFile([B)V
    .locals 3
    .param p1, "path"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 120
    const-string v1, "ACE ARF EF_ACRules"

    const-string v2, "Analysing EF_ACRules..."

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->mAcConditionDataCache:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 125
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->selectFile([B)I

    move-result v1

    const v2, 0x9000

    if-eq v1, v2, :cond_0

    .line 126
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v2, "EF_ACRules not found!!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 129
    :cond_0
    const/4 v1, 0x0

    const/4 v2, -0x1

    :try_start_0
    invoke-virtual {p0, v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->readBinary(II)[B

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->decodeDER([B)V
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    return-void

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
    throw v0
.end method
