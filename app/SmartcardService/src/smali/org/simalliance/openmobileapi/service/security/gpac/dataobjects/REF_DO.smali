.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "REF_DO.java"


# static fields
.field public static final _TAG:I = 0xe1


# instance fields
.field private mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

.field private mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;)V
    .locals 3
    .param p1, "aid_ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    .param p2, "hash_ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    const/16 v0, 0xe1

    invoke-direct {p0, v1, v0, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 34
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 35
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .line 43
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 44
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .line 45
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    const/4 v1, 0x0

    .line 38
    const/16 v0, 0xe1

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 34
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 35
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .line 39
    return-void
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 6
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    .line 131
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 133
    .local v1, "temp":Ljava/io/ByteArrayOutputStream;
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-nez v3, :cond_1

    .line 134
    :cond_0
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v4, "REF-DO: Required DO missing!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 137
    :cond_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    invoke-virtual {v3, v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 138
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-virtual {v3, v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 140
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 141
    .local v0, "data":[B
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getTag()I

    move-result v3

    const/4 v4, 0x0

    array-length v5, v0

    invoke-direct {v2, v0, v3, v4, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 142
    .local v2, "tlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v2, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 143
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "equals":Z
    instance-of v2, p1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    if-eqz v2, :cond_0

    .line 149
    invoke-super {p0, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->equals(Ljava/lang/Object;)Z

    move-result v0

    move-object v1, p1

    .line 150
    check-cast v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 151
    .local v1, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-nez v2, :cond_1

    iget-object v2, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-nez v2, :cond_1

    .line 152
    and-int/lit8 v0, v0, 0x1

    .line 158
    :goto_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-nez v2, :cond_3

    iget-object v2, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-nez v2, :cond_3

    .line 159
    and-int/lit8 v0, v0, 0x1

    .line 166
    .end local v1    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :cond_0
    :goto_1
    return v0

    .line 153
    .restart local v1    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :cond_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-eqz v2, :cond_2

    .line 154
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    iget-object v3, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->equals(Ljava/lang/Object;)Z

    move-result v2

    and-int/2addr v0, v2

    goto :goto_0

    .line 156
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 160
    :cond_3
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-eqz v2, :cond_4

    .line 161
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    iget-object v3, v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->equals(Ljava/lang/Object;)Z

    move-result v2

    and-int/2addr v0, v2

    goto :goto_1

    .line 163
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getAidDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    return-object v0
.end method

.method public getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 171
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 173
    .local v3, "stream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    invoke-virtual {p0, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->build(Ljava/io/ByteArrayOutputStream;)V
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 178
    .local v0, "data":[B
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    .line 180
    .end local v0    # "data":[B
    :goto_0
    return v2

    .line 174
    :catch_0
    move-exception v1

    .line 175
    .local v1, "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public interpret()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 85
    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 86
    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .line 88
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getRawData()[B

    move-result-object v0

    .line 89
    .local v0, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getValueIndex()I

    move-result v1

    .line 91
    .local v1, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getValueLength()I

    move-result v3

    add-int/2addr v3, v1

    array-length v4, v0

    if-le v3, v4, :cond_0

    .line 92
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "Not enough data for AR_DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 96
    :cond_0
    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BI)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v2

    .line 98
    .local v2, "temp":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v3

    const/16 v4, 0x4f

    if-eq v3, v4, :cond_1

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v3

    const/16 v4, 0xc0

    if-ne v3, v4, :cond_3

    .line 99
    :cond_1
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v4

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v5

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v6

    invoke-direct {v3, v0, v4, v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;-><init>([BIII)V

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 100
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->interpret()V

    .line 109
    :cond_2
    :goto_0
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v3

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v4

    add-int v1, v3, v4

    .line 110
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getValueIndex()I

    move-result v3

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getValueLength()I

    move-result v4

    add-int/2addr v3, v4

    if-gt v3, v1, :cond_0

    .line 113
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-nez v3, :cond_4

    .line 114
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "Missing AID-REF-DO in REF-DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 101
    :cond_3
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v3

    const/16 v4, 0xc1

    if-ne v3, v4, :cond_2

    .line 102
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v4

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v5

    invoke-direct {v3, v0, v4, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>([BII)V

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .line 103
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->interpret()V

    goto :goto_0

    .line 117
    :cond_4
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-nez v3, :cond_5

    .line 118
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "Missing Hash-REF-DO in REF-DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 120
    :cond_5
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v0, "b":Ljava/lang/StringBuilder;
    const-string v1, "REF_DO: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mAidDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    :cond_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    if-eqz v1, :cond_1

    .line 56
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->mHashDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
