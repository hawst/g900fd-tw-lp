.class Lorg/simalliance/openmobileapi/service/SmartcardService$5;
.super Lorg/simalliance/openmobileapi/service/ISmartcardService$Stub;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simalliance/openmobileapi/service/SmartcardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;


# direct methods
.method constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V
    .locals 0

    .prologue
    .line 755
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/ISmartcardService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public getReader(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceReader;
    .locals 3
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 767
    invoke-static {p2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 768
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # invokes: Lorg/simalliance/openmobileapi/service/SmartcardService;->getTerminal(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;
    invoke-static {v1, p1, p2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$600(Lorg/simalliance/openmobileapi/service/SmartcardService;Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;

    move-result-object v0

    check-cast v0, Lorg/simalliance/openmobileapi/service/Terminal;

    .line 769
    .local v0, "terminal":Lorg/simalliance/openmobileapi/service/Terminal;
    if-eqz v0, :cond_0

    .line 770
    new-instance v1, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-direct {v1, v0, v2}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;-><init>(Lorg/simalliance/openmobileapi/service/Terminal;Lorg/simalliance/openmobileapi/service/SmartcardService;)V

    .line 773
    :goto_0
    return-object v1

    .line 772
    :cond_0
    const-class v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid reader name"

    invoke-static {p2, v1, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    .line 773
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getReaders(Lorg/simalliance/openmobileapi/service/SmartcardError;)[Ljava/lang/String;
    .locals 2
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 759
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 760
    const-string v0, "SmartcardService"

    const-string v1, "getReaders()"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # invokes: Lorg/simalliance/openmobileapi/service/SmartcardService;->updateTerminals()[Ljava/lang/String;
    invoke-static {v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$500(Lorg/simalliance/openmobileapi/service/SmartcardService;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized isAidAllowed([B)Z
    .locals 9
    .param p1, "aid"    # [B

    .prologue
    const/4 v6, 0x0

    .line 780
    monitor-enter p0

    :try_start_0
    new-instance v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5$1;

    invoke-direct {v0, p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$5$1;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService$5;)V

    .line 781
    .local v0, "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    new-instance v3, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 782
    .local v3, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    const/4 v1, 0x0

    .line 783
    .local v1, "channel":Lorg/simalliance/openmobileapi/service/Channel;
    const/4 v4, 0x0

    .line 786
    .local v4, "session":Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    :try_start_1
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    sget-object v8, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    # invokes: Lorg/simalliance/openmobileapi/service/SmartcardService;->getTerminal(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;
    invoke-static {v7, v8, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$600(Lorg/simalliance/openmobileapi/service/SmartcardService;Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 787
    .local v5, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    if-nez v5, :cond_1

    .line 798
    .end local v5    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_0
    :goto_0
    monitor-exit p0

    return v6

    .line 790
    .restart local v5    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_1
    if-eqz p1, :cond_0

    :try_start_2
    array-length v7, p1

    const/4 v8, 0x5

    if-lt v7, v8, :cond_0

    array-length v7, p1

    const/16 v8, 0x10

    if-gt v7, v8, :cond_0

    .line 793
    const/4 v7, 0x0

    invoke-interface {v5, v7, p1, v0}, Lorg/simalliance/openmobileapi/service/ITerminal;->openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v1

    .line 794
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/Channel;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 798
    const/4 v6, 0x1

    goto :goto_0

    .line 795
    .end local v5    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :catch_0
    move-exception v2

    .line 796
    .local v2, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 780
    .end local v0    # "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .end local v1    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    .end local v4    # "session":Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized isCdfAllowed(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 16
    .param p1, "SEName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 860
    monitor-enter p0

    :try_start_0
    const-string v13, "SIM"

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 861
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;
    invoke-static {v13}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$700(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    if-eqz v13, :cond_2

    .line 863
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v13}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 864
    .local v11, "pm":Landroid/content/pm/PackageManager;
    const/16 v13, 0x40

    move-object/from16 v0, p2

    invoke-virtual {v11, v0, v13}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 865
    .local v6, "pi":Landroid/content/pm/PackageInfo;
    iget-object v13, v6, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v14, 0x0

    aget-object v8, v13, v14

    .line 866
    .local v8, "pkgSig":Landroid/content/pm/Signature;
    invoke-virtual {v8}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v9

    .line 867
    .local v9, "pkgSigBytes":[B
    const-string v13, "X.509"

    invoke-static {v13}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v4

    .line 868
    .local v4, "factory":Ljava/security/cert/CertificateFactory;
    new-instance v13, Ljava/io/ByteArrayInputStream;

    invoke-direct {v13, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v4, v13}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v10

    check-cast v10, Ljava/security/cert/X509Certificate;

    .line 869
    .local v10, "pkgX509":Ljava/security/cert/X509Certificate;
    invoke-virtual {v10}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v7

    .line 870
    .local v7, "pkgCertBytes":[B
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;
    invoke-static {v13}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$700(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 871
    .local v2, "cdfCertBytes":[B
    invoke-static {v7, v2}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v13

    if-eqz v13, :cond_0

    .line 872
    const/4 v13, 0x1

    .line 902
    .end local v2    # "cdfCertBytes":[B
    .end local v4    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "pi":Landroid/content/pm/PackageInfo;
    .end local v7    # "pkgCertBytes":[B
    .end local v8    # "pkgSig":Landroid/content/pm/Signature;
    .end local v9    # "pkgSigBytes":[B
    .end local v10    # "pkgX509":Ljava/security/cert/X509Certificate;
    .end local v11    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    monitor-exit p0

    return v13

    .line 875
    .restart local v4    # "factory":Ljava/security/cert/CertificateFactory;
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v7    # "pkgCertBytes":[B
    .restart local v8    # "pkgSig":Landroid/content/pm/Signature;
    .restart local v9    # "pkgSigBytes":[B
    .restart local v10    # "pkgX509":Ljava/security/cert/X509Certificate;
    .restart local v11    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    :try_start_2
    const-string v13, "SmartcardService"

    const-string v14, "pkgCdfCert is not matched cached CdfCerts."

    invoke-static {v13, v14}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 876
    const/4 v13, 0x0

    goto :goto_0

    .line 877
    .end local v4    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "pi":Landroid/content/pm/PackageInfo;
    .end local v7    # "pkgCertBytes":[B
    .end local v8    # "pkgSig":Landroid/content/pm/Signature;
    .end local v9    # "pkgSigBytes":[B
    .end local v10    # "pkgX509":Ljava/security/cert/X509Certificate;
    .end local v11    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v3

    .line 878
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    const-string v13, "SmartcardService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ": package could not be found"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    :goto_1
    const/4 v12, 0x0

    .line 888
    .local v12, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    const-string v13, "SIM"

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 889
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;
    invoke-static {v13}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$800(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Map;

    move-result-object v13

    sget-object v14, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    check-cast v12, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 895
    .restart local v12    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :goto_2
    invoke-interface {v12}, Lorg/simalliance/openmobileapi/service/ITerminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v1

    .line 896
    .local v1, "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    if-nez v1, :cond_3

    .line 897
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .end local v1    # "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    invoke-direct {v1, v12}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;-><init>(Lorg/simalliance/openmobileapi/service/ITerminal;)V

    .line 898
    .restart local v1    # "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-interface {v12}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 899
    invoke-interface {v12}, Lorg/simalliance/openmobileapi/service/ITerminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v1

    .line 901
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v13}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    invoke-virtual {v1, v13}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->setPackageManager(Landroid/content/pm/PackageManager;)V

    .line 902
    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->isCdfAllowed(Ljava/lang/String;)Z

    move-result v13

    goto :goto_0

    .line 879
    .end local v1    # "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    .end local v12    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :catch_1
    move-exception v3

    .line 880
    .local v3, "e":Ljava/security/cert/CertificateException;
    const-string v13, "SmartcardService"

    const-string v14, "specified certificate type is not available at any provider"

    invoke-static {v13, v14}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 860
    .end local v3    # "e":Ljava/security/cert/CertificateException;
    :catchall_0
    move-exception v13

    monitor-exit p0

    throw v13

    .line 881
    :catch_2
    move-exception v3

    .line 882
    .local v3, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v13, "SmartcardService"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Exception : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 890
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v12    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_4
    const-string v13, "eSE"

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 891
    move-object/from16 v0, p0

    iget-object v13, v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;
    invoke-static {v13}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$800(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Map;

    move-result-object v13

    sget-object v14, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-interface {v13, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    check-cast v12, Lorg/simalliance/openmobileapi/service/ITerminal;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .restart local v12    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    goto :goto_2

    .line 893
    :cond_5
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method public declared-synchronized isNFCEventAllowed(Ljava/lang/String;[B[Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)[Z
    .locals 9
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "aid"    # [B
    .param p3, "packageNames"    # [Ljava/lang/String;
    .param p4, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .param p5, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x0

    .line 811
    monitor-enter p0

    :try_start_0
    invoke-static {p5}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 814
    if-nez p4, :cond_0

    .line 815
    :try_start_1
    const-class v5, Ljava/lang/NullPointerException;

    const-string v6, "callback must not be null"

    invoke-static {p5, v5, v6}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v4

    .line 852
    :goto_0
    monitor-exit p0

    return-object v2

    .line 818
    :cond_0
    :try_start_2
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # invokes: Lorg/simalliance/openmobileapi/service/SmartcardService;->getTerminal(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;
    invoke-static {v5, p1, p5}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$600(Lorg/simalliance/openmobileapi/service/SmartcardService;Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;

    move-result-object v3

    .line 819
    .local v3, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    if-nez v3, :cond_1

    move-object v2, v4

    .line 820
    goto :goto_0

    .line 822
    :cond_1
    if-eqz p2, :cond_2

    array-length v5, p2

    if-nez v5, :cond_3

    .line 823
    :cond_2
    const/4 v5, 0x5

    new-array p2, v5, [B

    .end local p2    # "aid":[B
    fill-array-data p2, :array_0

    .line 827
    .restart local p2    # "aid":[B
    :cond_3
    array-length v5, p2

    if-lt v5, v6, :cond_4

    array-length v5, p2

    const/16 v6, 0x10

    if-le v5, v6, :cond_5

    .line 828
    :cond_4
    const-class v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "AID out of range"

    invoke-static {p5, v5, v6}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v2, v4

    .line 829
    goto :goto_0

    .line 831
    :cond_5
    if-eqz p3, :cond_6

    array-length v5, p3

    if-nez v5, :cond_7

    .line 832
    :cond_6
    const-class v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "process names not specified"

    invoke-static {p5, v5, v6}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v2, v4

    .line 833
    goto :goto_0

    .line 835
    :cond_7
    const/4 v0, 0x0

    .line 837
    .local v0, "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    sget-object v6, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 838
    :try_start_3
    invoke-interface {v3}, Lorg/simalliance/openmobileapi/service/ITerminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v5

    if-nez v5, :cond_8

    .line 839
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-interface {v3}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 841
    :cond_8
    invoke-interface {v3}, Lorg/simalliance/openmobileapi/service/ITerminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v0

    .line 842
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->setPackageManager(Landroid/content/pm/PackageManager;)V

    .line 843
    sget-object v5, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v7, "ISIS"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 844
    const/4 v5, 0x1

    invoke-virtual {v0, v5, p4}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->initialize(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 846
    :cond_9
    invoke-virtual {v0, p2, p3, p4}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->isNFCEventAllowed([B[Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)[Z

    move-result-object v2

    .line 847
    .local v2, "ret":[Z
    monitor-exit v6

    goto :goto_0

    .end local v2    # "ret":[Z
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 849
    .end local v0    # "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    .end local v3    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :catch_0
    move-exception v1

    .line 850
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    invoke-static {p5, v1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    .line 851
    const-string v5, "SmartcardService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isNFCEventAllowed Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-object v2, v4

    .line 852
    goto/16 :goto_0

    .line 811
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    .line 823
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method
