.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_DO_Factory;
.super Ljava/lang/Object;
.source "Response_DO_Factory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createDO([B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    .locals 4
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    .line 24
    const/4 v2, 0x0

    invoke-static {p0, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BI)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v1

    .line 26
    .local v1, "tempTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    const/4 v0, 0x0

    .line 28
    .local v0, "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 44
    move-object v0, v1

    .line 47
    :goto_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->interpret()V

    .line 49
    return-object v0

    .line 31
    :sswitch_0
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;

    .end local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v2

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;-><init>([BII)V

    .line 32
    .restart local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    goto :goto_0

    .line 34
    :sswitch_1
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ARAC_AID_DO;

    .end local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v2

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ARAC_AID_DO;-><init>([BII)V

    .line 35
    .restart local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    goto :goto_0

    .line 38
    :sswitch_2
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;

    .end local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v2

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;-><init>([BII)V

    .line 39
    .restart local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    goto :goto_0

    .line 41
    :sswitch_3
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;

    .end local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v2

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v3

    invoke-direct {v0, p0, v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;-><init>([BII)V

    .line 42
    .restart local v0    # "retTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    goto :goto_0

    .line 28
    :sswitch_data_0
    .sparse-switch
        0xdf20 -> :sswitch_0
        0xff40 -> :sswitch_2
        0xff50 -> :sswitch_3
        0xff70 -> :sswitch_1
    .end sparse-switch
.end method
