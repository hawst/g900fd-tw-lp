.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "Response_AR_DO.java"


# static fields
.field public static final _TAG:I = 0xff50


# instance fields
.field private mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;


# direct methods
.method public constructor <init>([BII)V
    .locals 1
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    .line 41
    const v0, 0xff50

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 42
    return-void
.end method


# virtual methods
.method public getArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    return-object v0
.end method

.method public interpret()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    .line 62
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->getRawData()[B

    move-result-object v1

    .line 63
    .local v1, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->getValueIndex()I

    move-result v3

    .line 65
    .local v3, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->getValueLength()I

    move-result v5

    if-nez v5, :cond_0

    .line 90
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->getValueLength()I

    move-result v5

    add-int/2addr v5, v3

    array-length v6, v1

    if-le v5, v6, :cond_1

    .line 71
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Not enough data for Response_AR_DO!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 74
    :cond_1
    move v0, v3

    .line 75
    .local v0, "currentPos":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->getValueLength()I

    move-result v5

    add-int v2, v3, v5

    .line 77
    .local v2, "endPos":I
    :cond_2
    invoke-static {v1, v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BI)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v4

    .line 79
    .local v4, "temp":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v5

    const/16 v6, 0xe3

    if-ne v5, v6, :cond_3

    .line 80
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v6

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v7

    invoke-direct {v5, v1, v6, v7}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;-><init>([BII)V

    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 81
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->interpret()V

    .line 88
    :cond_3
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v5

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v6

    add-int v0, v5, v6

    .line 89
    if-lt v0, v2, :cond_2

    goto :goto_0
.end method
