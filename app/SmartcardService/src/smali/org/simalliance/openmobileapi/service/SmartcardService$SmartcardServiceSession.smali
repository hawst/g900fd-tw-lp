.class public final Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
.super Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession$Stub;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simalliance/openmobileapi/service/SmartcardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "SmartcardServiceSession"
.end annotation


# instance fields
.field private mAtr:[B

.field private final mChannels:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lorg/simalliance/openmobileapi/service/Channel;",
            ">;"
        }
    .end annotation
.end field

.field private mIsClosed:Z

.field private final mLock:Ljava/lang/Object;

.field private final mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;)V
    .locals 1
    .param p2, "reader"    # Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    .prologue
    .line 922
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession$Stub;-><init>()V

    .line 914
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mChannels:Ljava/util/Set;

    .line 916
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mLock:Ljava/lang/Object;

    .line 923
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    .line 924
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getAtr()[B

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mAtr:[B

    .line 925
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mIsClosed:Z

    .line 926
    return-void
.end method


# virtual methods
.method public close(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    .locals 2
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 940
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 941
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    if-nez v1, :cond_0

    .line 949
    :goto_0
    return-void

    .line 945
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v1, p0}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->closeSession(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;)V
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/CardException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 946
    :catch_0
    move-exception v0

    .line 947
    .local v0, "e":Lorg/simalliance/openmobileapi/service/CardException;
    invoke-static {p1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public closeChannels(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    .locals 8
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 953
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 955
    :try_start_0
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mChannels:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 957
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/Channel;>;"
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 958
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simalliance/openmobileapi/service/Channel;

    .line 959
    .local v0, "channel":Lorg/simalliance/openmobileapi/service/Channel;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->isClosed()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 961
    :try_start_2
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->close()V

    .line 963
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mChannels:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 967
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->setClosed()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 971
    .end local v0    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    :catch_0
    move-exception v1

    .line 972
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v4, "SmartcardService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ServiceSession closeChannels Exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_2
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 975
    return-void

    .line 964
    .restart local v0    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    :catch_1
    move-exception v2

    .line 965
    .local v2, "ignore":Ljava/lang/Exception;
    :try_start_5
    const-string v4, "SmartcardService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ServiceSession channel - close Exception "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 974
    .end local v0    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    .end local v2    # "ignore":Ljava/lang/Exception;
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/Channel;>;"
    :catchall_0
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v4

    .line 970
    .restart local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/Channel;>;"
    :cond_1
    :try_start_7
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mChannels:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2
.end method

.method public getAtr()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 935
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mAtr:[B

    return-object v0
.end method

.method public getReader()Lorg/simalliance/openmobileapi/service/ISmartcardServiceReader;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 930
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    return-object v0
.end method

.method public isClosed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 980
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mIsClosed:Z

    return v0
.end method

.method public openBasicChannel(Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    .locals 4
    .param p1, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .param p2, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 989
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 990
    .local v0, "salesCode":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "KDI"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 992
    const-string v2, "[SmartcardService]"

    const-string v3, "call openBasic"

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 997
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v1, p1, p2}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->openBasicChannelAid([BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    move-result-object v1

    goto :goto_0
.end method

.method public openBasicChannelAid([BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    .locals 11
    .param p1, "aid"    # [B
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .param p3, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x5

    const/4 v6, 0x0

    .line 1004
    invoke-static {p3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 1005
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->isClosed()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1006
    const-class v7, Ljava/lang/IllegalStateException;

    const-string v8, "session is closed"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, v6

    .line 1090
    :goto_0
    return-object v0

    .line 1009
    :cond_0
    if-nez p2, :cond_1

    .line 1010
    const-class v7, Ljava/lang/NullPointerException;

    const-string v8, "callback must not be null"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, v6

    .line 1011
    goto :goto_0

    .line 1013
    :cond_1
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    if-nez v7, :cond_2

    .line 1014
    const-class v7, Ljava/lang/NullPointerException;

    const-string v8, "reader must not be null"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, v6

    .line 1015
    goto :goto_0

    .line 1019
    :cond_2
    const/4 v4, 0x0

    .line 1020
    .local v4, "noAid":Z
    if-eqz p1, :cond_3

    :try_start_0
    array-length v7, p1

    if-nez v7, :cond_4

    .line 1021
    :cond_3
    const/4 v7, 0x5

    new-array p1, v7, [B

    .end local p1    # "aid":[B
    fill-array-data p1, :array_0

    .line 1022
    .restart local p1    # "aid":[B
    const/4 v4, 0x1

    .line 1025
    :cond_4
    array-length v7, p1

    if-lt v7, v8, :cond_5

    array-length v7, p1

    const/16 v8, 0x10

    if-le v7, v8, :cond_6

    .line 1026
    :cond_5
    const-class v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "AID out of range"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v0, v6

    .line 1027
    goto :goto_0

    .line 1031
    :cond_6
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getName(Lorg/simalliance/openmobileapi/service/SmartcardError;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "SIM"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1032
    const-string v7, "_TAG"

    const-string v8, "openBasicChannelAid returns null"

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 1033
    goto :goto_0

    .line 1037
    :cond_7
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    invoke-virtual {v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageNameFromCallingUid(I)Ljava/lang/String;

    move-result-object v5

    .line 1038
    .local v5, "packageName":Ljava/lang/String;
    const-string v7, "SmartcardService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Enable access control on basic channel for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    const/4 v2, 0x0

    .line 1042
    .local v2, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v8, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1043
    :try_start_1
    sget-object v7, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v9, "ISIS"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 1044
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v9

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/Terminal;->getName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 1047
    :cond_8
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # invokes: Lorg/simalliance/openmobileapi/service/SmartcardService;->checkAid([B)V
    invoke-static {v7, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$900(Lorg/simalliance/openmobileapi/service/SmartcardService;[B)V

    .line 1049
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v7

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v7, v9, p1, v5, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->setUpChannelAccess(Landroid/content/pm/PackageManager;[BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v2

    .line 1054
    const-string v7, "SmartcardService"

    const-string v9, "Access control successfully enabled."

    invoke-static {v7, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v2, v7}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setCallingPid(I)V

    .line 1057
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1061
    :try_start_2
    const-string v7, "SmartcardService"

    const-string v8, "OpenBasicChannel(AID)"

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    const/4 v1, 0x0

    .line 1064
    .local v1, "channel":Lorg/simalliance/openmobileapi/service/Channel;
    sget-object v8, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1065
    if-eqz v4, :cond_9

    .line 1066
    :try_start_3
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v7

    invoke-virtual {v7, p0, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v1

    .line 1070
    :goto_1
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1074
    if-nez v1, :cond_a

    move-object v0, v6

    .line 1075
    goto/16 :goto_0

    .line 1057
    .end local v1    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    :catchall_0
    move-exception v7

    :try_start_4
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v7
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 1087
    .end local v2    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .end local v5    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1088
    .local v3, "e":Ljava/lang/Exception;
    invoke-static {p3, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    .line 1089
    const-string v7, "SmartcardService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OpenBasicChannel Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v6

    .line 1090
    goto/16 :goto_0

    .line 1068
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    .restart local v2    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .restart local v5    # "packageName":Ljava/lang/String;
    :cond_9
    :try_start_6
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v7

    invoke-virtual {v7, p0, p1, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v1

    goto :goto_1

    .line 1070
    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v7

    .line 1079
    :cond_a
    invoke-virtual {v1, v2}, Lorg/simalliance/openmobileapi/service/Channel;->setChannelAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 1081
    const-string v7, "SmartcardService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Open basic channel success. Channel: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/Channel;->getChannelNumber()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1083
    new-instance v0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1, p0}, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;-><init>(Lorg/simalliance/openmobileapi/service/Channel;Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;)V

    .line 1084
    .local v0, "basicChannel":Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mChannels:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_0

    .line 1021
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public openLogicalChannel([BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    .locals 11
    .param p1, "aid"    # [B
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .param p3, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x5

    const/4 v6, 0x0

    .line 1098
    invoke-static {p3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 1100
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->isClosed()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1101
    const-class v7, Ljava/lang/IllegalStateException;

    const-string v8, "session is closed"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v3, v6

    .line 1172
    :goto_0
    return-object v3

    .line 1105
    :cond_0
    if-nez p2, :cond_1

    .line 1106
    const-class v7, Ljava/lang/NullPointerException;

    const-string v8, "callback must not be null"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v3, v6

    .line 1107
    goto :goto_0

    .line 1109
    :cond_1
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    if-nez v7, :cond_2

    .line 1110
    const-class v7, Ljava/lang/NullPointerException;

    const-string v8, "reader must not be null"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v3, v6

    .line 1111
    goto :goto_0

    .line 1115
    :cond_2
    const/4 v4, 0x0

    .line 1116
    .local v4, "noAid":Z
    if-eqz p1, :cond_3

    :try_start_0
    array-length v7, p1

    if-nez v7, :cond_4

    .line 1117
    :cond_3
    const/4 v7, 0x5

    new-array p1, v7, [B

    .end local p1    # "aid":[B
    fill-array-data p1, :array_0

    .line 1120
    .restart local p1    # "aid":[B
    const/4 v4, 0x1

    .line 1123
    :cond_4
    array-length v7, p1

    if-lt v7, v8, :cond_5

    array-length v7, p1

    const/16 v8, 0x10

    if-le v7, v8, :cond_6

    .line 1124
    :cond_5
    const-class v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "AID out of range"

    invoke-static {p3, v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    move-object v3, v6

    .line 1125
    goto :goto_0

    .line 1129
    :cond_6
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    invoke-virtual {v7, v8}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageNameFromCallingUid(I)Ljava/lang/String;

    move-result-object v5

    .line 1130
    .local v5, "packageName":Ljava/lang/String;
    const-string v7, "SmartcardService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Enable access control on logical channel for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    const/4 v1, 0x0

    .line 1134
    .local v1, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v8, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1135
    :try_start_1
    sget-object v7, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v9, "ISIS"

    invoke-virtual {v7, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 1136
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v9

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/Terminal;->getName()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 1139
    :cond_7
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # invokes: Lorg/simalliance/openmobileapi/service/SmartcardService;->checkAid([B)V
    invoke-static {v7, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$900(Lorg/simalliance/openmobileapi/service/SmartcardService;[B)V

    .line 1141
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v7

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v7, v9, p1, v5, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->setUpChannelAccess(Landroid/content/pm/PackageManager;[BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v1

    .line 1146
    const-string v7, "SmartcardService"

    const-string v9, "Access control successfully enabled."

    invoke-static {v7, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1147
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v7

    invoke-virtual {v1, v7}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setCallingPid(I)V

    .line 1148
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1151
    :try_start_2
    const-string v7, "SmartcardService"

    const-string v8, "OpenLogicalChannel"

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    const/4 v0, 0x0

    .line 1154
    .local v0, "channel":Lorg/simalliance/openmobileapi/service/Channel;
    sget-object v8, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v8
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1155
    if-eqz v4, :cond_8

    .line 1156
    :try_start_3
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v7

    invoke-virtual {v7, p0, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v0

    .line 1160
    :goto_1
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1163
    :try_start_4
    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/Channel;->setChannelAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 1165
    const-string v7, "SmartcardService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Open logical channel successfull. Channel: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->getChannelNumber()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1166
    new-instance v3, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v3, v0, p0}, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;-><init>(Lorg/simalliance/openmobileapi/service/Channel;Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;)V

    .line 1167
    .local v3, "logicalChannel":Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mChannels:Ljava/util/Set;

    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 1169
    .end local v0    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    .end local v1    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .end local v3    # "logicalChannel":Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;
    .end local v5    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 1170
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {p3, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    .line 1171
    const-string v7, "SmartcardService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "OpenLogicalChannel Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v6

    .line 1172
    goto/16 :goto_0

    .line 1148
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .restart local v5    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v7

    :try_start_5
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v7
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 1158
    .restart local v0    # "channel":Lorg/simalliance/openmobileapi/service/Channel;
    :cond_8
    :try_start_7
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mReader:Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v7

    invoke-virtual {v7, p0, p1, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v0

    goto :goto_1

    .line 1160
    :catchall_1
    move-exception v7

    monitor-exit v8
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v7
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    .line 1117
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method removeChannel(Lorg/simalliance/openmobileapi/service/Channel;)V
    .locals 1
    .param p1, "channel"    # Lorg/simalliance/openmobileapi/service/Channel;

    .prologue
    .line 1189
    if-nez p1, :cond_0

    .line 1193
    :goto_0
    return-void

    .line 1192
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mChannels:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method setClosed()V
    .locals 1

    .prologue
    .line 1177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->mIsClosed:Z

    .line 1179
    return-void
.end method
