.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "Response_RefreshTag_DO.java"


# static fields
.field public static final _TAG:I = 0xdf20


# instance fields
.field private mRefreshTag:J

.field private mRefreshTagArray:[B


# direct methods
.method public constructor <init>([BII)V
    .locals 1
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    .line 39
    const v0, 0xdf20

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTagArray:[B

    .line 40
    return-void
.end method


# virtual methods
.method public getRefreshTag()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    return-wide v0
.end method

.method public getRefreshTagArray()[B
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTagArray:[B

    return-object v0
.end method

.method public interpret()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x8

    .line 62
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 64
    invoke-super {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v3

    if-eq v3, v10, :cond_0

    .line 65
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Invalid length of RefreshTag DO!"

    invoke-direct {v3, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 68
    :cond_0
    invoke-super {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getRawData()[B

    move-result-object v0

    .line 69
    .local v0, "data":[B
    invoke-super {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v1

    .line 71
    .local v1, "index":I
    invoke-super {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v3

    add-int/2addr v3, v1

    array-length v6, v0

    if-le v3, v6, :cond_1

    .line 72
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Not enough data for RefreshTag DO!"

    invoke-direct {v3, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 74
    :cond_1
    invoke-super {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTagArray:[B

    .line 75
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTagArray:[B

    const/4 v6, 0x0

    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTagArray:[B

    array-length v7, v7

    invoke-static {v0, v1, v3, v6, v7}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 78
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aget-byte v3, v0, v1

    int-to-long v4, v3

    .line 79
    .local v4, "temp":J
    const/16 v3, 0x38

    shl-long v6, v4, v3

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 80
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget-byte v3, v0, v2

    int-to-long v4, v3

    .line 81
    iget-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    const/16 v3, 0x30

    shl-long v8, v4, v3

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 82
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget-byte v3, v0, v1

    int-to-long v4, v3

    .line 83
    iget-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    const/16 v3, 0x28

    shl-long v8, v4, v3

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 84
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget-byte v3, v0, v2

    int-to-long v4, v3

    .line 85
    iget-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    const/16 v3, 0x20

    shl-long v8, v4, v3

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 86
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget-byte v3, v0, v1

    int-to-long v4, v3

    .line 87
    iget-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    const/16 v3, 0x18

    shl-long v8, v4, v3

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 88
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget-byte v3, v0, v2

    int-to-long v4, v3

    .line 89
    iget-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    const/16 v3, 0x10

    shl-long v8, v4, v3

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 90
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget-byte v3, v0, v1

    int-to-long v4, v3

    .line 91
    iget-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    shl-long v8, v4, v10

    add-long/2addr v6, v8

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 92
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget-byte v3, v0, v2

    int-to-long v4, v3

    .line 93
    iget-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    add-long/2addr v6, v4

    iput-wide v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->mRefreshTag:J

    .line 94
    return-void
.end method
