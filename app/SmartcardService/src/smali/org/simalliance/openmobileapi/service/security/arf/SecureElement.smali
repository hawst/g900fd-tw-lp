.class public Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;
.super Ljava/lang/Object;
.source "SecureElement.java"


# static fields
.field public static final SIM_ALLIANCE:S = 0x0s

.field public static final SIM_IO:S = 0x1s

.field public static final TAG:Ljava/lang/String; = "SmartcardService ACE ARF"


# instance fields
.field private mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

.field private mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

.field private final mCallback:Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

.field private mSEInterface:S

.field private mTerminalHandle:Lorg/simalliance/openmobileapi/service/ITerminal;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/ArfController;Lorg/simalliance/openmobileapi/service/ITerminal;)V
    .locals 1
    .param p1, "arfHandler"    # Lorg/simalliance/openmobileapi/service/security/arf/ArfController;
    .param p2, "handle"    # Lorg/simalliance/openmobileapi/service/ITerminal;

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    .line 48
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mTerminalHandle:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 50
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    .line 52
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement$1;

    invoke-direct {v0, p0}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement$1;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mCallback:Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .line 59
    const/4 v0, 0x0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mSEInterface:S

    .line 68
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mTerminalHandle:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 69
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    .line 70
    return-void
.end method

.method private setUpChannelAccess(Lorg/simalliance/openmobileapi/service/IChannel;)V
    .locals 3
    .param p1, "channel"    # Lorg/simalliance/openmobileapi/service/IChannel;

    .prologue
    .line 152
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 153
    .local v0, "arfChannelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 154
    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 155
    invoke-interface {p1, v0}, Lorg/simalliance/openmobileapi/service/IChannel;->setChannelAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 157
    return-void
.end method


# virtual methods
.method public clearAccessRuleCache()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    move-result-object v0

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->clearCache()V

    .line 183
    return-void
.end method

.method public closeArfChannel()V
    .locals 4

    .prologue
    .line 131
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    invoke-interface {v1}, Lorg/simalliance/openmobileapi/service/IChannel;->close()V

    .line 134
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SmartcardService ACE ARF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error closing channel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public exchangeAPDU(Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;[B)[B
    .locals 4
    .param p1, "ef"    # Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
    .param p2, "cmd"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 88
    :try_start_0
    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mSEInterface:S

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 90
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mTerminalHandle:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->getFileId()I

    move-result v2

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, p2}, Lorg/simalliance/openmobileapi/service/ITerminal;->simIOExchange(ILjava/lang/String;[B)[B

    move-result-object v1

    .line 93
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    invoke-interface {v1, p2}, Lorg/simalliance/openmobileapi/service/IChannel;->transmit([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Secure Element access error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getRefreshTag()[B
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    move-result-object v0

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->getRefreshTag()[B

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSeInterface()S
    .locals 1

    .prologue
    .line 73
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mSEInterface:S

    return v0
.end method

.method public openLogicalArfChannel([B)Lorg/simalliance/openmobileapi/service/IChannel;
    .locals 5
    .param p1, "AID"    # [B

    .prologue
    const/4 v1, 0x0

    .line 109
    :try_start_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mTerminalHandle:Lorg/simalliance/openmobileapi/service/ITerminal;

    const/4 v3, 0x0

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mCallback:Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    invoke-interface {v2, v3, p1, v4}, Lorg/simalliance/openmobileapi/service/ITerminal;->openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v2

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    .line 110
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    invoke-direct {p0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->setUpChannelAccess(Lorg/simalliance/openmobileapi/service/IChannel;)V

    .line 111
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-object v1

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    instance-of v2, v0, Ljava/util/MissingResourceException;

    if-eqz v2, :cond_0

    .line 115
    const-string v1, "SmartcardService ACE ARF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no channels left to access ARF: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    check-cast v0, Ljava/util/MissingResourceException;

    .end local v0    # "e":Ljava/lang/Exception;
    throw v0

    .line 118
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "SmartcardService ACE ARF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error opening logical channel "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    goto :goto_0
.end method

.method public putAccessRule(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V
    .locals 2
    .param p1, "aid_ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    .param p2, "hash_ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .param p3, "channelAccess"    # Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .prologue
    .line 174
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-direct {v0, p1, p2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;-><init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;)V

    .line 175
    .local v0, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->putWithMerge(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 176
    return-void
.end method

.method public resetAccessRules()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    move-result-object v0

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->reset()V

    .line 180
    return-void
.end method

.method public setRefreshTag([B)V
    .locals 1
    .param p1, "refreshTag"    # [B

    .prologue
    .line 167
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mArfHandler:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->setRefreshTag([B)V

    .line 170
    :cond_0
    return-void
.end method

.method public setSeInterface(S)V
    .locals 0
    .param p1, "seInterface"    # S

    .prologue
    .line 77
    iput-short p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->mSEInterface:S

    .line 78
    return-void
.end method
