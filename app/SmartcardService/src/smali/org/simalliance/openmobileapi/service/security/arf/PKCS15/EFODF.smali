.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;
.super Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.source "EFODF.java"


# static fields
.field public static final EFODF_PATH:[B

.field public static final TAG:Ljava/lang/String; = "SmartcardService ACE ARF"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->EFODF_PATH:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x50t
        0x31t
    .end array-data
.end method

.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V
    .locals 0
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 69
    return-void
.end method

.method private decodeDER([B)[B
    .locals 3
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 55
    .local v0, "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :goto_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v1

    if-nez v1, :cond_1

    .line 56
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v1

    const/16 v2, -0x59

    if-ne v1, v2, :cond_0

    .line 57
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parsePathAttributes()[B

    move-result-object v1

    .line 59
    :goto_1
    return-object v1

    .line 58
    :cond_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V

    goto :goto_0

    .line 59
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private internalFindCdfPath([B)Ljava/util/Set;
    .locals 10
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 119
    const-string v4, "SmartcardService ACE ARF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "internalFindCdfPath() <<< buffer="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, p1

    const-string v7, " "

    invoke-static {p1, v8, v6, v7}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v2, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 121
    .local v2, "der":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 123
    .local v1, "cdfPaths":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    :goto_0
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v4

    if-nez v4, :cond_1

    .line 124
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v3

    .line 125
    .local v3, "type":B
    const-string v4, "SmartcardService ACE ARF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TLV type="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-array v6, v9, [B

    aput-byte v3, v6, v8

    const-string v7, ""

    invoke-static {v6, v8, v9, v7}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const/16 v4, -0x5b

    if-ne v3, v4, :cond_0

    .line 127
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parsePathAttributes()[B

    move-result-object v0

    .line 128
    .local v0, "cdfPath":[B
    const-string v4, "SmartcardService ACE ARF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "found CDF path: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v0

    const-string v7, " "

    invoke-static {v0, v8, v6, v7}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    .end local v0    # "cdfPath":[B
    :cond_0
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V

    goto :goto_0

    .line 134
    .end local v3    # "type":B
    :cond_1
    return-object v1
.end method


# virtual methods
.method public analyseFile([B)[B
    .locals 5
    .param p1, "pkcs15Path"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 77
    const-string v1, "SmartcardService ACE ARF"

    const-string v2, "Analysing EF_ODF..."

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const/4 v0, 0x0

    .line 83
    .local v0, "path":[B
    if-eqz p1, :cond_0

    .line 84
    array-length v1, p1

    sget-object v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->EFODF_PATH:[B

    array-length v2, v2

    add-int/2addr v1, v2

    new-array v0, v1, [B

    .line 85
    array-length v1, p1

    invoke-static {p1, v4, v0, v4, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 86
    sget-object v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->EFODF_PATH:[B

    array-length v2, p1

    sget-object v3, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->EFODF_PATH:[B

    array-length v3, v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 92
    :goto_0
    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->selectFile([B)I

    move-result v1

    const v2, 0x9000

    if-eq v1, v2, :cond_1

    .line 93
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v2, "EF_ODF not found!!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 88
    :cond_0
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->EFODF_PATH:[B

    goto :goto_0

    .line 95
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p0, v4, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->readBinary(II)[B

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->decodeDER([B)[B

    move-result-object v1

    return-object v1
.end method

.method public findCdfPath()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 105
    const-string v0, "SmartcardService ACE ARF"

    const-string v1, "findOdfPath() <<<"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->EFODF_PATH:[B

    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->selectFile([B)I

    move-result v0

    const v1, 0x9000

    if-eq v0, v1, :cond_0

    .line 107
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "EF_ODF not found"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->readBinary(II)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->internalFindCdfPath([B)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
