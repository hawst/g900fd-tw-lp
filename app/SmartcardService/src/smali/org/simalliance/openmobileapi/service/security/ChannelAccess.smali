.class public Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
.super Ljava/lang/Object;
.source "ChannelAccess.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
    }
.end annotation


# instance fields
.field protected CHANNEL_ACCESS_TAG:Ljava/lang/String;

.field protected mAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

.field protected mApduAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

.field protected mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

.field protected mCallingPid:I

.field protected mNFCEventAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

.field protected mPackageName:Ljava/lang/String;

.field protected mReason:Ljava/lang/String;

.field protected mUseApduFilter:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-string v0, "ChannelAccess"

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->CHANNEL_ACCESS_TAG:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mPackageName:Ljava/lang/String;

    .line 30
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .line 32
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .line 34
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mUseApduFilter:Z

    .line 36
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mCallingPid:I

    .line 38
    const-string v0, "no access by default"

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mReason:Ljava/lang/String;

    .line 40
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mNFCEventAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->clone()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 10

    .prologue
    .line 45
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 46
    .local v2, "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mReason:Ljava/lang/String;

    invoke-virtual {v2, v8, v9}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 47
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v8}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setPackageName(Ljava/lang/String;)V

    .line 48
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v2, v8}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 49
    iget v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mCallingPid:I

    invoke-virtual {v2, v8}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setCallingPid(I)V

    .line 50
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mNFCEventAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v2, v8}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 51
    iget-boolean v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mUseApduFilter:Z

    invoke-virtual {v2, v8}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setUseApduFilter(Z)V

    .line 52
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    if-eqz v8, :cond_1

    .line 53
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    array-length v8, v8

    new-array v0, v8, [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .line 54
    .local v0, "apduFilter":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    const/4 v4, 0x0

    .line 55
    .local v4, "i":I
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .local v1, "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v5, v4

    .end local v4    # "i":I
    .local v5, "i":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v3, v1, v6

    .line 56
    .local v3, "filter":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->clone()Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    move-result-object v8

    aput-object v8, v0, v5

    .line 55
    add-int/lit8 v6, v6, 0x1

    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 58
    .end local v3    # "filter":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    :cond_0
    invoke-virtual {v2, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    .line 62
    .end local v0    # "apduFilter":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v1    # "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v5    # "i":I
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    :goto_1
    return-object v2

    .line 60
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    goto :goto_1
.end method

.method public getAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    return-object v0
.end method

.method public getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    return-object v0
.end method

.method public getApduFilter()[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    return-object v0
.end method

.method public getCallingPid()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mCallingPid:I

    return v0
.end method

.method public getNFCEventAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mNFCEventAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mReason:Ljava/lang/String;

    return-object v0
.end method

.method public isUseApduFilter()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mUseApduFilter:Z

    return v0
.end method

.method public setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V
    .locals 0
    .param p1, "access"    # Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .line 88
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mReason:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V
    .locals 0
    .param p1, "apduAccess"    # Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .prologue
    .line 78
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .line 79
    return-void
.end method

.method public setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V
    .locals 0
    .param p1, "accessConditions"    # [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .prologue
    .line 115
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .line 116
    return-void
.end method

.method public setCallingPid(I)V
    .locals 0
    .param p1, "callingPid"    # I

    .prologue
    .line 100
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mCallingPid:I

    .line 101
    return-void
.end method

.method public setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V
    .locals 0
    .param p1, "access"    # Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .prologue
    .line 122
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mNFCEventAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .line 123
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mPackageName:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setUseApduFilter(Z)V
    .locals 0
    .param p1, "useApduFilter"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mUseApduFilter:Z

    .line 97
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 127
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v5, "\n [mPackageName="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    const-string v5, ", mAccess="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    const-string v5, ", mApduAccess="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    const-string v5, ", mUseApduFilter="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    iget-boolean v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mUseApduFilter:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 137
    const-string v5, ", mApduFilter="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    if-eqz v5, :cond_0

    .line 139
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mApduFilter:[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .local v0, "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 140
    .local v1, "f":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 144
    .end local v0    # "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v1    # "f":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    const-string v5, "null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    :cond_1
    const-string v5, ", mCallingPid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    iget v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mCallingPid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 148
    const-string v5, ", mReason="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mReason:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string v5, ", mNFCEventAllowed="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->mNFCEventAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
