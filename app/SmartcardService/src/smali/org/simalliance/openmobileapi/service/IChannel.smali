.class public interface abstract Lorg/simalliance/openmobileapi/service/IChannel;
.super Ljava/lang/Object;
.source "IChannel.java"


# virtual methods
.method public abstract close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method

.method public abstract getCallback()Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
.end method

.method public abstract getChannelAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
.end method

.method public abstract getChannelNumber()I
.end method

.method public abstract getSelectResponse()[B
.end method

.method public abstract getTerminal()Lorg/simalliance/openmobileapi/service/ITerminal;
.end method

.method public abstract hasSelectedAid(Z[B)V
.end method

.method public abstract hasSelectedAid()Z
.end method

.method public abstract isBasicChannel()Z
.end method

.method public abstract setCallingPid(I)V
.end method

.method public abstract setChannelAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V
.end method

.method public abstract transmit([B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method
