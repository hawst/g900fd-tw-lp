.class public final Lorg/simalliance/openmobileapi/service/SmartcardService;
.super Landroid/app/Service;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;,
        Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;,
        Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;
    }
.end annotation


# static fields
.field private static final ADMIN_PERM:Ljava/lang/String; = "android.permission.WRITE_SECURE_SETTINGS"

.field private static final ADMIN_PERM_ERROR:Ljava/lang/String; = "WRITE_SECURE_SETTINGS permission required"

.field public static final MSG_LOAD_ESE_RULES:I = 0x2

.field public static final MSG_LOAD_SD_RULES:I = 0x3

.field public static final MSG_LOAD_UICC_RULES:I = 0x1

.field public static final NUMBER_OF_TRIALS:I = 0x3

.field public static final WAIT_TIME:J = 0x3e8L

.field public static _SD_TERMINAL:Ljava/lang/String; = null

.field public static final _TAG:Ljava/lang/String; = "SmartcardService"

.field public static _UICC_TERMINAL:Ljava/lang/String;

.field public static _eSE_TERMINAL:Ljava/lang/String;

.field public static accessControlType:Ljava/lang/String;

.field public static final mLockOpenAC:Ljava/lang/Object;

.field public static secureEvtType:Ljava/lang/String;

.field public static simConnectionType:Ljava/lang/String;


# instance fields
.field private cachedCdfCertsBytes:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation
.end field

.field private eSESupport:Z

.field private isAcInitializedForSimLoaded:Z

.field private isAcInitializedForSimReady:Z

.field private mAddOnTerminals:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/simalliance/openmobileapi/service/ITerminal;",
            ">;"
        }
    .end annotation
.end field

.field mInitialiseTask:Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

.field private mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mNfcReceiver:Landroid/content/BroadcastReceiver;

.field private mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

.field private mSimReceiver:Landroid/content/BroadcastReceiver;

.field private final mSmartcardBinder:Lorg/simalliance/openmobileapi/service/ISmartcardService$Stub;

.field private mTerminals:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/simalliance/openmobileapi/service/ITerminal;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    const-string v0, "SIM1"

    sput-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    .line 102
    const-string v0, "eSE1"

    sput-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    .line 104
    const-string v0, "SD1"

    sput-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->_SD_TERMINAL:Ljava/lang/String;

    .line 116
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;

    .line 141
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    .line 147
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mAddOnTerminals:Ljava/util/Map;

    .line 160
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->eSESupport:Z

    .line 161
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimReady:Z

    .line 162
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimLoaded:Z

    .line 755
    new-instance v0, Lorg/simalliance/openmobileapi/service/SmartcardService$5;

    invoke-direct {v0, p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$5;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mSmartcardBinder:Lorg/simalliance/openmobileapi/service/ISmartcardService$Stub;

    .line 172
    return-void
.end method

.method static synthetic access$100(Lorg/simalliance/openmobileapi/service/SmartcardService;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->registerSimStateChangedEvent(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Lorg/simalliance/openmobileapi/service/SmartcardService;)Z
    .locals 1
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimReady:Z

    return v0
.end method

.method static synthetic access$202(Lorg/simalliance/openmobileapi/service/SmartcardService;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimReady:Z

    return p1
.end method

.method static synthetic access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    .locals 1
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;

    .prologue
    .line 97
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$400(Lorg/simalliance/openmobileapi/service/SmartcardService;)Z
    .locals 1
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimLoaded:Z

    return v0
.end method

.method static synthetic access$402(Lorg/simalliance/openmobileapi/service/SmartcardService;Z)Z
    .locals 0
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimLoaded:Z

    return p1
.end method

.method static synthetic access$500(Lorg/simalliance/openmobileapi/service/SmartcardService;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;

    .prologue
    .line 97
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->updateTerminals()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lorg/simalliance/openmobileapi/service/SmartcardService;Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;
    .locals 1
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lorg/simalliance/openmobileapi/service/SmartcardError;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getTerminal(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;

    .prologue
    .line 97
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$702(Lorg/simalliance/openmobileapi/service/SmartcardService;Ljava/util/Set;)Ljava/util/Set;
    .locals 0
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;
    .param p1, "x1"    # Ljava/util/Set;

    .prologue
    .line 97
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;

    return-object p1
.end method

.method static synthetic access$800(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;

    .prologue
    .line 97
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$900(Lorg/simalliance/openmobileapi/service/SmartcardService;[B)V
    .locals 0
    .param p0, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;
    .param p1, "x1"    # [B

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->checkAid([B)V

    return-void
.end method

.method private checkAid([B)V
    .locals 6
    .param p1, "aid"    # [B

    .prologue
    .line 1278
    const/4 v1, 0x0

    .line 1279
    .local v1, "flag":Z
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1281
    .local v2, "salesCode":Ljava/lang/String;
    sget-object v3, Lorg/simalliance/openmobileapi/service/Util;->ARA_M_AID:[B

    invoke-static {v3, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "CTC"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1282
    const/4 v1, 0x1

    .line 1283
    :cond_0
    sget-object v3, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v4, "ISIS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lorg/simalliance/openmobileapi/service/Util;->VZW_ARA_C_AID:[B

    invoke-static {v3, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1284
    const/4 v1, 0x1

    .line 1286
    :cond_1
    if-eqz v1, :cond_2

    .line 1288
    :try_start_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "android.permission.WRITE_SECURE_SETTINGS"

    const-string v5, "WRITE_SECURE_SETTINGS permission required"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1293
    :cond_2
    return-void

    .line 1289
    :catch_0
    move-exception v0

    .line 1290
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/security/AccessControlException;

    const-string v4, "access denied to the ARA-M"

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method static clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    .locals 0
    .param p0, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;

    .prologue
    .line 119
    if-eqz p0, :cond_0

    .line 120
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardError;->clear()V

    .line 122
    :cond_0
    return-void
.end method

.method private createBuildinTerminals()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 606
    new-array v11, v14, [Ljava/lang/Class;

    const-class v12, Landroid/content/Context;

    aput-object v12, v11, v13

    .line 609
    .local v11, "types":[Ljava/lang/Class;
    new-array v1, v14, [Ljava/lang/Object;

    aput-object p0, v1, v13

    .line 612
    .local v1, "args":[Ljava/lang/Object;
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getBuildinTerminalClasses()[Ljava/lang/Object;

    move-result-object v3

    .line 613
    .local v3, "classes":[Ljava/lang/Object;
    move-object v2, v3

    .local v2, "arr$":[Ljava/lang/Object;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v5, v2, v7

    .line 615
    .local v5, "clazzO":Ljava/lang/Object;
    :try_start_0
    move-object v0, v5

    check-cast v0, Ljava/lang/Class;

    move-object v4, v0

    .line 616
    .local v4, "clazz":Ljava/lang/Class;
    invoke-virtual {v4, v11}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v6

    .line 617
    .local v6, "constr":Ljava/lang/reflect/Constructor;
    invoke-virtual {v6, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 619
    .local v10, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    iget-boolean v12, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->eSESupport:Z

    if-nez v12, :cond_0

    sget-object v12, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-interface {v10}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 613
    .end local v4    # "clazz":Ljava/lang/Class;
    .end local v6    # "constr":Ljava/lang/reflect/Constructor;
    .end local v10    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 622
    .restart local v4    # "clazz":Ljava/lang/Class;
    .restart local v6    # "constr":Ljava/lang/reflect/Constructor;
    .restart local v10    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_0
    iget-object v12, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    invoke-interface {v10}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 623
    const-string v12, "SmartcardService"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " adding "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v10}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 625
    .end local v4    # "clazz":Ljava/lang/Class;
    .end local v6    # "constr":Ljava/lang/reflect/Constructor;
    .end local v10    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :catch_0
    move-exception v9

    .line 626
    .local v9, "t":Ljava/lang/Throwable;
    const-string v13, "SmartcardService"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v14, " CreateReaders Error: "

    invoke-virtual {v12, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_1

    invoke-virtual {v9}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v12

    :goto_2
    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v13, v12}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const-string v12, "unknown"

    goto :goto_2

    .line 631
    .end local v5    # "clazzO":Ljava/lang/Object;
    .end local v9    # "t":Ljava/lang/Throwable;
    :cond_2
    return-void
.end method

.method private createTerminals()[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 534
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->createBuildinTerminals()V

    .line 536
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 537
    .local v1, "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 539
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 540
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 542
    :cond_0
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->eSESupport:Z

    if-eqz v2, :cond_1

    .line 543
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 544
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 547
    :cond_1
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v3, "ISIS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 548
    :cond_2
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 565
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    return-object v2
.end method

.method private getBuildinTerminalClasses()[Ljava/lang/Object;
    .locals 13

    .prologue
    .line 700
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 702
    .local v3, "classes":Ljava/util/ArrayList;
    :try_start_0
    const-string v7, "org.simalliance.openmobileapi.service"

    .line 703
    .local v7, "packageName":Ljava/lang/String;
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v7, v11}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v10

    iget-object v0, v10, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 704
    .local v0, "apkName":Ljava/lang/String;
    new-instance v5, Ldalvik/system/DexClassLoader;

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v12

    invoke-direct {v5, v0, v10, v11, v12}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 707
    .local v5, "dexClassLoader":Ldalvik/system/DexClassLoader;
    const-string v10, "org.simalliance.openmobileapi.service.Terminal"

    const/4 v11, 0x1

    invoke-static {v10, v11, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v9

    .line 708
    .local v9, "terminalClass":Ljava/lang/Class;
    if-nez v9, :cond_0

    .line 709
    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v10

    .line 726
    .end local v0    # "apkName":Ljava/lang/String;
    .end local v5    # "dexClassLoader":Ldalvik/system/DexClassLoader;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v9    # "terminalClass":Ljava/lang/Class;
    :goto_0
    return-object v10

    .line 712
    .restart local v0    # "apkName":Ljava/lang/String;
    .restart local v5    # "dexClassLoader":Ldalvik/system/DexClassLoader;
    .restart local v7    # "packageName":Ljava/lang/String;
    .restart local v9    # "terminalClass":Ljava/lang/Class;
    :cond_0
    new-instance v6, Ldalvik/system/DexFile;

    invoke-direct {v6, v0}, Ldalvik/system/DexFile;-><init>(Ljava/lang/String;)V

    .line 713
    .local v6, "dexFile":Ldalvik/system/DexFile;
    invoke-virtual {v6}, Ldalvik/system/DexFile;->entries()Ljava/util/Enumeration;

    move-result-object v1

    .line 714
    .local v1, "classFileNames":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 715
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 716
    .local v2, "className":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 717
    .local v4, "clazz":Ljava/lang/Class;
    invoke-virtual {v4}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v8

    .line 718
    .local v8, "superClass":Ljava/lang/Class;
    if-eqz v8, :cond_1

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "org.simalliance.openmobileapi.service.AddonTerminal"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 720
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 723
    .end local v0    # "apkName":Ljava/lang/String;
    .end local v1    # "classFileNames":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v2    # "className":Ljava/lang/String;
    .end local v4    # "clazz":Ljava/lang/Class;
    .end local v5    # "dexClassLoader":Ldalvik/system/DexClassLoader;
    .end local v6    # "dexFile":Ldalvik/system/DexFile;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v8    # "superClass":Ljava/lang/Class;
    .end local v9    # "terminalClass":Ljava/lang/Class;
    :catch_0
    move-exception v10

    .line 726
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v10

    goto :goto_0
.end method

.method private getTerminal(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ITerminal;
    .locals 3
    .param p1, "reader"    # Ljava/lang/String;
    .param p2, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;

    .prologue
    .line 507
    if-nez p1, :cond_1

    .line 508
    const-class v1, Ljava/lang/NullPointerException;

    const-string v2, "reader must not be null"

    invoke-static {p2, v1, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    .line 509
    const/4 v0, 0x0

    .line 530
    :cond_0
    :goto_0
    return-object v0

    .line 512
    :cond_1
    const/4 v0, 0x0

    .line 513
    .local v0, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    const-string v1, "SIM"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 514
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    check-cast v0, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 517
    .restart local v0    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_2
    :goto_1
    if-nez v0, :cond_0

    .line 518
    const-class v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "unknown reader"

    invoke-static {p2, v1, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 515
    :cond_3
    const-string v1, "eSE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 516
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    check-cast v0, Lorg/simalliance/openmobileapi/service/ITerminal;

    .restart local v0    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    goto :goto_1
.end method

.method private loadFeature()V
    .locals 4

    .prologue
    .line 213
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SmartcardSvc_HideTerminalCapability"

    const-string v3, "NONE"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "hideTerminal":Ljava/lang/String;
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->eSESupport:Z

    .line 220
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SmartcardSvc_SetAccessControlType"

    const-string v3, "GPAC,GAC"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->accessControlType:Ljava/lang/String;

    .line 226
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_NFC_SetSecureEventType"

    const-string v3, "GSMA"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    .line 232
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_SmartcardSvc_ConfigInitConnection"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->simConnectionType:Ljava/lang/String;

    .line 237
    return-void
.end method

.method private registerAdapterStateChangedEvent(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 349
    const-string v1, "SmartcardService"

    const-string v2, "register ADAPTER_STATE_CHANGED event"

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.nfc.action.ADAPTER_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 352
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardService$2;

    invoke-direct {v1, p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$2;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    .line 363
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 364
    return-void
.end method

.method private registerMediaMountedEvent(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 375
    const-string v1, "SmartcardService"

    const-string v2, "register MEDIA_MOUNTED event"

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 378
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardService$3;

    invoke-direct {v1, p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$3;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 388
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 389
    return-void
.end method

.method private registerSimStateChangedEvent(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 304
    const-string v1, "SmartcardService"

    const-string v2, "register SIM_STATE_CHANGED event"

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SIM_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 307
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardService$1;

    invoke-direct {v1, p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$1;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mSimReceiver:Landroid/content/BroadcastReceiver;

    .line 337
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 338
    return-void
.end method

.method static setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0
    .param p0, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 126
    if-eqz p0, :cond_0

    .line 127
    invoke-virtual {p0, p1, p2}, Lorg/simalliance/openmobileapi/service/SmartcardError;->setError(Ljava/lang/Class;Ljava/lang/String;)V

    .line 129
    :cond_0
    return-void
.end method

.method static setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V
    .locals 2
    .param p0, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 132
    if-eqz p0, :cond_0

    .line 133
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lorg/simalliance/openmobileapi/service/SmartcardError;->setError(Ljava/lang/Class;Ljava/lang/String;)V

    .line 135
    :cond_0
    return-void
.end method

.method private unregisterAdapterStateChangedEvent(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 367
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 368
    const-string v0, "SmartcardService"

    const-string v1, "unregister ADAPTER_STATE_CHANGED event"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mNfcReceiver:Landroid/content/BroadcastReceiver;

    .line 372
    :cond_0
    return-void
.end method

.method private unregisterMediaMountedEvent(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 392
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 393
    const-string v0, "SmartcardService"

    const-string v1, "unregister MEDIA_MOUNTED event"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 395
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 397
    :cond_0
    return-void
.end method

.method private unregisterSimStateChangedEvent(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 340
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mSimReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 341
    const-string v0, "SmartcardService"

    const-string v1, "unregister SIM_STATE_CHANGED event"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mSimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 343
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mSimReceiver:Landroid/content/BroadcastReceiver;

    .line 345
    :cond_0
    return-void
.end method

.method private updateTerminals()[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 569
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 570
    .local v1, "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 573
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 574
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 576
    :cond_0
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->eSESupport:Z

    if-eqz v2, :cond_1

    .line 577
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 578
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 581
    :cond_1
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v3, "ISIS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 582
    :cond_2
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 601
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    return-object v2
.end method


# virtual methods
.method public dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "writer"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 242
    const-string v3, "SMARTCARD SERVICE (dumpsys activity service org.simalliance.openmobileapi)"

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 245
    const-string v1, "  "

    .line 247
    .local v1, "prefix":Ljava/lang/String;
    sget-boolean v3, Landroid/os/Build;->IS_DEBUGGABLE:Z

    if-nez v3, :cond_1

    .line 248
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Your build is not debuggable!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 249
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Smartcard service dump is only available for userdebug and eng build"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 270
    :cond_0
    return-void

    .line 251
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "List of terminals:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 252
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 253
    .local v2, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 255
    .end local v2    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_2
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 257
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "List of add-on terminals:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 258
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mAddOnTerminals:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 259
    .restart local v2    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 261
    .end local v2    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_3
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 263
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 264
    .restart local v2    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    invoke-interface {v2, p2, v1}, Lorg/simalliance/openmobileapi/service/ITerminal;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    goto :goto_2

    .line 266
    .end local v2    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_4
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mAddOnTerminals:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 267
    .restart local v2    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    invoke-interface {v2, p2, v1}, Lorg/simalliance/openmobileapi/service/ITerminal;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    goto :goto_3
.end method

.method public declared-synchronized getCdfCerts()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1297
    monitor-enter p0

    const/4 v2, 0x0

    .line 1298
    .local v2, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    sget-object v4, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lorg/simalliance/openmobileapi/service/ITerminal;

    move-object v2, v0

    .line 1299
    invoke-interface {v2}, Lorg/simalliance/openmobileapi/service/ITerminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v1

    .line 1300
    .local v1, "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    if-nez v1, :cond_0

    .line 1301
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .end local v1    # "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;-><init>(Lorg/simalliance/openmobileapi/service/ITerminal;)V

    .line 1302
    .restart local v1    # "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    invoke-interface {v2}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 1303
    invoke-interface {v2}, Lorg/simalliance/openmobileapi/service/ITerminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v1

    .line 1305
    :cond_0
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getCdfCertsBytes()Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    monitor-exit p0

    return-object v3

    .line 1297
    .end local v1    # "ac":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getPackageNameFromCallingUid(I)Ljava/lang/String;
    .locals 4
    .param p1, "uid"    # I

    .prologue
    .line 742
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 743
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_0

    .line 744
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v1

    .line 745
    .local v1, "packageName":[Ljava/lang/String;
    if-eqz v1, :cond_0

    array-length v2, v1

    if-lez v2, :cond_0

    .line 746
    const/4 v2, 0x0

    aget-object v2, v1, v2

    return-object v2

    .line 749
    .end local v1    # "packageName":[Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/security/AccessControlException;

    const-string v3, "Caller PackageName can not be determined"

    invoke-direct {v2, v3}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    .locals 1
    .param p1, "se"    # Ljava/lang/String;
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 407
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(ZLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized initializeAccessControl(ZLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    .locals 11
    .param p1, "reset"    # Z
    .param p2, "se"    # Ljava/lang/String;
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 411
    monitor-enter p0

    const/4 v5, 0x1

    .line 412
    .local v5, "result":Z
    :try_start_0
    const-string v7, "SmartcardService"

    const-string v8, "Initializing Access Control"

    invoke-static {v7, v8}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    if-nez p3, :cond_0

    .line 415
    new-instance v0, Lorg/simalliance/openmobileapi/service/SmartcardService$4;

    invoke-direct {v0, p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$4;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V

    .end local p3    # "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .local v0, "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    move-object p3, v0

    .line 418
    .end local v0    # "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .restart local p3    # "callback":Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    :cond_0
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    .line 419
    .local v1, "col":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/simalliance/openmobileapi/service/ITerminal;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 420
    .local v4, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/ITerminal;>;"
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 421
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 422
    .local v6, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    if-eqz v6, :cond_1

    .line 427
    if-eqz p2, :cond_2

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v7

    if-eqz v7, :cond_1

    .line 428
    :cond_2
    const/4 v3, 0x0

    .line 430
    .local v3, "isCardPresent":Z
    :try_start_1
    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->isCardPresent()Z
    :try_end_1
    .catch Lorg/simalliance/openmobileapi/service/CardException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    .line 437
    :goto_1
    :try_start_2
    sget-object v8, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 438
    if-eqz v3, :cond_4

    .line 439
    :try_start_3
    const-string v7, "SmartcardService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Initializing Access Control for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    if-eqz p1, :cond_3

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->resetAccessControl()V

    .line 441
    :cond_3
    const/4 v7, 0x1

    invoke-interface {v6, v7, p3}, Lorg/simalliance/openmobileapi/service/ITerminal;->initializeAccessControl(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    move-result v7

    and-int/2addr v5, v7

    .line 445
    :goto_2
    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 411
    .end local v1    # "col":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/simalliance/openmobileapi/service/ITerminal;>;"
    .end local v3    # "isCardPresent":Z
    .end local v4    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/ITerminal;>;"
    .end local v6    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :catchall_1
    move-exception v7

    monitor-exit p0

    throw v7

    .line 431
    .restart local v1    # "col":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/simalliance/openmobileapi/service/ITerminal;>;"
    .restart local v3    # "isCardPresent":Z
    .restart local v4    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/ITerminal;>;"
    .restart local v6    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :catch_0
    move-exception v2

    .line 432
    .local v2, "e":Lorg/simalliance/openmobileapi/service/CardException;
    const/4 v3, 0x0

    goto :goto_1

    .line 443
    .end local v2    # "e":Lorg/simalliance/openmobileapi/service/CardException;
    :cond_4
    :try_start_5
    const-string v7, "SmartcardService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "NOT initializing Access Control for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " SE not present."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 478
    .end local v3    # "isCardPresent":Z
    .end local v6    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_5
    monitor-exit p0

    return v5
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 176
    const-string v0, "SmartcardService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " smartcard service onBind"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const-class v0, Lorg/simalliance/openmobileapi/service/ISmartcardService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mSmartcardBinder:Lorg/simalliance/openmobileapi/service/ISmartcardService$Stub;

    .line 181
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 186
    const-string v1, "SmartcardService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " smartcard service onCreate"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->loadFeature()V

    .line 191
    sget-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v2, "ISIS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 192
    :cond_0
    const-string v1, "SIM - UICC"

    sput-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    .line 193
    const-string v1, "eSE - SmartMX"

    sput-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    .line 200
    :cond_1
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SmartCardServiceHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 201
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 204
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;Landroid/os/Looper;)V

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    .line 206
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->createTerminals()[Ljava/lang/String;

    .line 207
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;Lorg/simalliance/openmobileapi/service/SmartcardService$1;)V

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mInitialiseTask:Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

    .line 208
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mInitialiseTask:Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 209
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 482
    const-string v2, "SmartcardService"

    const-string v3, " smartcard service onDestroy ..."

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mTerminals:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 484
    .local v1, "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    invoke-interface {v1}, Lorg/simalliance/openmobileapi/service/ITerminal;->closeChannels()V

    goto :goto_0

    .line 485
    .end local v1    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mAddOnTerminals:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 486
    .restart local v1    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    invoke-interface {v1}, Lorg/simalliance/openmobileapi/service/ITerminal;->closeChannels()V

    goto :goto_1

    .line 489
    .end local v1    # "terminal":Lorg/simalliance/openmobileapi/service/ITerminal;
    :cond_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mInitialiseTask:Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mInitialiseTask:Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->cancel(Z)Z

    .line 490
    :cond_2
    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mInitialiseTask:Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

    .line 493
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->unregisterSimStateChangedEvent(Landroid/content/Context;)V

    .line 497
    sget-object v3, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v3

    .line 498
    const/4 v2, 0x0

    :try_start_0
    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    .line 499
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    const-string v2, "SmartcardService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ... smartcard service onDestroy"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    return-void

    .line 499
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
