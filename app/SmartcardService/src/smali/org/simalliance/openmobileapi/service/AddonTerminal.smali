.class public Lorg/simalliance/openmobileapi/service/AddonTerminal;
.super Lorg/simalliance/openmobileapi/service/Terminal;
.source "AddonTerminal.java"


# instance fields
.field private mGetAtr:Ljava/lang/reflect/Method;

.field private mGetName:Ljava/lang/reflect/Method;

.field private mGetSelectResponse:Ljava/lang/reflect/Method;

.field private mInstance:Ljava/lang/Object;

.field private mInternalCloseLogicalChannel:Ljava/lang/reflect/Method;

.field private mInternalConnect:Ljava/lang/reflect/Method;

.field private mInternalDisconnect:Ljava/lang/reflect/Method;

.field private mInternalOpenLogicalChannel:Ljava/lang/reflect/Method;

.field private mInternalOpenLogicalChannelAID:Ljava/lang/reflect/Method;

.field private mInternalTransmit:Ljava/lang/reflect/Method;

.field private mIsCardPresent:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "className"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 68
    const-string v4, "Addon"

    invoke-direct {p0, v4, p1}, Lorg/simalliance/openmobileapi/service/Terminal;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 31
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    .line 33
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mIsCardPresent:Ljava/lang/reflect/Method;

    .line 35
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalConnect:Ljava/lang/reflect/Method;

    .line 37
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalDisconnect:Ljava/lang/reflect/Method;

    .line 39
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalTransmit:Ljava/lang/reflect/Method;

    .line 41
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannel:Ljava/lang/reflect/Method;

    .line 43
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannelAID:Ljava/lang/reflect/Method;

    .line 45
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalCloseLogicalChannel:Ljava/lang/reflect/Method;

    .line 47
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetName:Ljava/lang/reflect/Method;

    .line 49
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetAtr:Ljava/lang/reflect/Method;

    .line 51
    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetSelectResponse:Ljava/lang/reflect/Method;

    .line 71
    const/4 v4, 0x3

    :try_start_0
    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 73
    .local v2, "ctx":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 74
    .local v0, "cl":Ljava/lang/ClassLoader;
    invoke-virtual {v0, p3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 75
    .local v1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    .line 80
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    if-eqz v4, :cond_0

    .line 81
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "getAtr"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetAtr:Ljava/lang/reflect/Method;

    .line 82
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "getName"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetName:Ljava/lang/reflect/Method;

    .line 83
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "isCardPresent"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mIsCardPresent:Ljava/lang/reflect/Method;

    .line 85
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "internalConnect"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalConnect:Ljava/lang/reflect/Method;

    .line 87
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "internalDisconnect"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalDisconnect:Ljava/lang/reflect/Method;

    .line 89
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "internalTransmit"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, [B

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalTransmit:Ljava/lang/reflect/Method;

    .line 93
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "internalOpenLogicalChannel"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannel:Ljava/lang/reflect/Method;

    .line 95
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "internalOpenLogicalChannel"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, [B

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannelAID:Ljava/lang/reflect/Method;

    .line 99
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "internalCloseLogicalChannel"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalCloseLogicalChannel:Ljava/lang/reflect/Method;

    .line 103
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "getSelectResponse"

    const/4 v4, 0x0

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetSelectResponse:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_0
    return-void

    .line 106
    .end local v0    # "cl":Ljava/lang/ClassLoader;
    .end local v1    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "ctx":Landroid/content/Context;
    :catch_0
    move-exception v3

    .line 107
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "plugin internal error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static getPackageNames(Landroid/content/Context;)[Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 55
    .local v2, "packageNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    .line 56
    .local v3, "pis":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 57
    .local v1, "p":Landroid/content/pm/PackageInfo;
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v6, "org.simalliance.openmobileapi.service.terminals."

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v6, "org.simalliance.openmobileapi.cts"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 59
    :cond_1
    iget-object v5, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    .end local v1    # "p":Landroid/content/pm/PackageInfo;
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    new-array v4, v5, [Ljava/lang/String;

    .line 63
    .local v4, "rstrings":[Ljava/lang/String;
    invoke-interface {v2, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 64
    return-object v4
.end method


# virtual methods
.method public getAtr()[B
    .locals 6

    .prologue
    .line 119
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetAtr:Ljava/lang/reflect/Method;

    if-nez v3, :cond_0

    .line 120
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "plugin error: Function String getAtr() not found"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 123
    :cond_0
    :try_start_0
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetAtr:Ljava/lang/reflect/Method;

    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v3, 0x0

    check-cast v3, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    move-object v0, v3

    check-cast v0, [B

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    .local v2, "resp":[B
    return-object v2

    .line 125
    .end local v2    # "resp":[B
    :catch_0
    move-exception v1

    .line 126
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "plugin internal error: getAtr() execution: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 132
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetName:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 133
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "plugin error: Function String getName() not found"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 136
    :cond_0
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetName:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    .local v1, "s":Ljava/lang/String;
    return-object v1

    .line 138
    .end local v1    # "s":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "plugin internal error: getName() execution: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method protected internalCloseLogicalChannel(I)V
    .locals 6
    .param p1, "channelNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 233
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalCloseLogicalChannel:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 234
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "plugin error: Function internalCloseLogicalChannel not found"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 238
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalCloseLogicalChannel:Ljava/lang/reflect/Method;

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    return-void

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "plugin internal error: internalOpenLogicalChannel() execution: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected internalConnect()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 159
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalConnect:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 160
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "plugin error: Function String internalConnect() not found"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 164
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalConnect:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mIsConnected:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    return-void

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "plugin internal error: internalConnect() execution: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected internalDisconnect()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 173
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalDisconnect:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    .line 174
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "plugin error: Function String internalDisconnect() not found"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 178
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalDisconnect:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v1, 0x0

    check-cast v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mIsConnected:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    return-void

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v2, "plugin internal error: internalDisconnect() execution"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected internalOpenLogicalChannel()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 202
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannel:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 203
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "plugin error: Function String internalOpenLogicalChannel() not found"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 207
    :cond_0
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannel:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 209
    .local v0, "channel":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 210
    .end local v0    # "channel":Ljava/lang/Integer;
    :catch_0
    move-exception v1

    .line 211
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    check-cast v2, Ljava/lang/Exception;

    throw v2
.end method

.method protected internalOpenLogicalChannel([B)I
    .locals 6
    .param p1, "aid"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 216
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannelAID:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 217
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "plugin error: Function internalOpenLogicalChannelAID() not found"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 221
    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalOpenLogicalChannelAID:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 225
    .local v0, "channel":Ljava/lang/Integer;
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mGetSelectResponse:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    check-cast v2, [B

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mSelectResponse:[B

    .line 226
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 227
    .end local v0    # "channel":Ljava/lang/Integer;
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    check-cast v2, Ljava/lang/Exception;

    throw v2
.end method

.method protected internalTransmit([B)[B
    .locals 7
    .param p1, "command"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 186
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalTransmit:Ljava/lang/reflect/Method;

    if-nez v3, :cond_0

    .line 187
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "plugin error: Function String internalTransmit() not found"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 191
    :cond_0
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInternalTransmit:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    move-object v0, v3

    check-cast v0, [B

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .local v2, "resp":[B
    return-object v2

    .line 195
    .end local v2    # "resp":[B
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lorg/simalliance/openmobileapi/service/CardException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "plugin internal error: internalTransmit() execution: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public isCardPresent()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 145
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mIsCardPresent:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 146
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "plugin error: Function String isCardPresent() not found"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 150
    :cond_0
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mIsCardPresent:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/AddonTerminal;->mInstance:Ljava/lang/Object;

    const/4 v2, 0x0

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 151
    .local v1, "v":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    return v2

    .line 152
    .end local v1    # "v":Ljava/lang/Boolean;
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lorg/simalliance/openmobileapi/service/CardException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "plugin internal error: isCardPresent() execution: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
