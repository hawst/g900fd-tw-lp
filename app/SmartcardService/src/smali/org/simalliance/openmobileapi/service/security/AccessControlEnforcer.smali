.class public Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
.super Ljava/lang/Object;
.source "AccessControlEnforcer.java"


# instance fields
.field private final ACCESS_CONTROL_ENFORCER:Ljava/lang/String;

.field private mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

.field private mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;

.field private mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

.field private mFullAccess:Z

.field private mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

.field protected mNfcEventFlags:[Z

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mRulesRead:Z

.field private mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

.field private mUseAra:Z

.field private mUseArf:Z


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/ITerminal;)V
    .locals 3
    .param p1, "terminal"    # Lorg/simalliance/openmobileapi/service/ITerminal;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 60
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    .line 63
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    .line 64
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    .line 66
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    .line 67
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mRulesRead:Z

    .line 69
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 71
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .line 72
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    .line 74
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mNfcEventFlags:[Z

    .line 76
    const-string v0, "Access Control Enforcer: "

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->ACCESS_CONTROL_ENFORCER:Ljava/lang/String;

    .line 80
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 81
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    .line 82
    return-void
.end method

.method private debugInfo([B[Ljava/security/cert/Certificate;)V
    .locals 9
    .param p1, "aid"    # [B
    .param p2, "appCerts"    # [Ljava/security/cert/Certificate;

    .prologue
    .line 596
    const-string v6, "SmartcardService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Secure Element: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v8}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v6, "SmartcardService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AID: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    move-object v1, p2

    .local v1, "arr$":[Ljava/security/cert/Certificate;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v1, v4

    .line 600
    .local v0, "appCert":Ljava/security/cert/Certificate;
    :try_start_0
    invoke-static {v0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAppCertHash(Ljava/security/cert/Certificate;)[B

    move-result-object v3

    .line 601
    .local v3, "hash":[B
    const-string v6, "SmartcardService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HASH: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 598
    .end local v3    # "hash":[B
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 602
    :catch_0
    move-exception v2

    .line 603
    .local v2, "e":Ljava/security/cert/CertificateEncodingException;
    const-string v6, "SmartcardService"

    const-string v7, "Problem with Application Certificate."

    invoke-static {v6, v7}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 606
    .end local v0    # "appCert":Ljava/security/cert/Certificate;
    .end local v2    # "e":Ljava/security/cert/CertificateEncodingException;
    :cond_0
    return-void
.end method

.method public static decodeCertificate([B)Ljava/security/cert/Certificate;
    .locals 3
    .param p0, "certData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 249
    const-string v2, "X.509"

    invoke-static {v2}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 250
    .local v1, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 253
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    return-object v0
.end method

.method public static getAppCertHash(Ljava/security/cert/Certificate;)[B
    .locals 4
    .param p0, "appCert"    # Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;
        }
    .end annotation

    .prologue
    .line 479
    const/4 v1, 0x0

    .line 480
    .local v1, "md":Ljava/security/MessageDigest;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 482
    :try_start_0
    const-string v2, "SHA"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 487
    :cond_0
    if-nez v1, :cond_1

    .line 488
    new-instance v2, Ljava/security/AccessControlException;

    const-string v3, "Hash can not be computed"

    invoke-direct {v2, v3}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 484
    :catch_0
    move-exception v2

    .line 480
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 490
    :cond_1
    invoke-virtual {p0}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    return-object v2
.end method

.method public static getDefaultAccessControlAid()[B
    .locals 1

    .prologue
    .line 101
    invoke-static {}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->getAraMAid()[B

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized internal_isNFCEventAllowed([B[Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)[Z
    .locals 12
    .param p1, "aid"    # [B
    .param p2, "packageNames"    # [Ljava/lang/String;
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 519
    monitor-enter p0

    :try_start_0
    array-length v8, p2

    new-array v8, v8, [Z

    iput-object v8, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mNfcEventFlags:[Z

    .line 520
    const/4 v4, 0x0

    .line 521
    .local v4, "i":I
    const/4 v2, 0x0

    .line 522
    .local v2, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    move-object v1, p2

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_3

    aget-object v7, v1, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    .local v7, "packageName":Ljava/lang/String;
    :try_start_1
    invoke-virtual {p0, v7}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAPPCerts(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object v0

    .line 529
    .local v0, "appCerts":[Ljava/security/cert/Certificate;
    if-eqz v0, :cond_0

    array-length v8, v0

    if-nez v8, :cond_1

    .line 530
    :cond_0
    new-instance v8, Ljava/security/AccessControlException;

    const-string v10, "Application certificates are invalid or do not exist."

    invoke-direct {v8, v10}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 536
    .end local v0    # "appCerts":[Ljava/security/cert/Certificate;
    :catch_0
    move-exception v3

    .line 537
    .local v3, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v8, "SmartcardService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, " Access Rules for NFC: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mNfcEventFlags:[Z

    const/4 v10, 0x0

    aput-boolean v10, v8, v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 540
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 522
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 533
    .restart local v0    # "appCerts":[Ljava/security/cert/Certificate;
    :cond_1
    :try_start_3
    invoke-virtual {p0, p1, v0, p3}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAccessRule([B[Ljava/security/cert/Certificate;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v2

    .line 534
    iget-object v10, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mNfcEventFlags:[Z

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getNFCEventAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v8

    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-ne v8, v11, :cond_2

    const/4 v8, 0x1

    :goto_2
    aput-boolean v8, v10, v4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 519
    .end local v0    # "appCerts":[Ljava/security/cert/Certificate;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .end local v4    # "i":I
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .restart local v0    # "appCerts":[Ljava/security/cert/Certificate;
    .restart local v1    # "arr$":[Ljava/lang/String;
    .restart local v2    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .restart local v4    # "i":I
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v7    # "packageName":Ljava/lang/String;
    :cond_2
    move v8, v9

    .line 534
    goto :goto_2

    .line 542
    .end local v0    # "appCerts":[Ljava/security/cert/Certificate;
    .end local v7    # "packageName":Ljava/lang/String;
    :cond_3
    :try_start_4
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mNfcEventFlags:[Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-object v8
.end method

.method private declared-synchronized internal_setUpChannelAccess([BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 5
    .param p1, "aid"    # [B
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 382
    monitor-enter p0

    :try_start_0
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 383
    .local v1, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 384
    :cond_0
    new-instance v3, Ljava/security/AccessControlException;

    const-string v4, "package names must be specified"

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 382
    .end local v1    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 386
    .restart local v1    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :cond_1
    if-eqz p1, :cond_2

    :try_start_1
    array-length v3, p1

    if-nez v3, :cond_3

    .line 387
    :cond_2
    new-instance v3, Ljava/security/AccessControlException;

    const-string v4, "AID must be specified"

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 389
    :cond_3
    array-length v3, p1

    const/4 v4, 0x5

    if-lt v3, v4, :cond_4

    array-length v3, p1

    const/16 v4, 0x10

    if-le v3, v4, :cond_5

    .line 390
    :cond_4
    new-instance v3, Ljava/security/AccessControlException;

    const-string v4, "AID has an invalid length"

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 395
    :cond_5
    :try_start_2
    invoke-virtual {p0, p2}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAPPCerts(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object v0

    .line 398
    .local v0, "appCerts":[Ljava/security/cert/Certificate;
    if-eqz v0, :cond_6

    array-length v3, v0

    if-nez v3, :cond_7

    .line 399
    :cond_6
    new-instance v3, Ljava/security/AccessControlException;

    const-string v4, "Application certificates are invalid or do not exist."

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 402
    .end local v0    # "appCerts":[Ljava/security/cert/Certificate;
    :catch_0
    move-exception v2

    .line 403
    .local v2, "exp":Ljava/lang/Throwable;
    :try_start_3
    new-instance v3, Ljava/security/AccessControlException;

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 401
    .end local v2    # "exp":Ljava/lang/Throwable;
    .restart local v0    # "appCerts":[Ljava/security/cert/Certificate;
    :cond_7
    :try_start_4
    invoke-virtual {p0, p1, v0, p3}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAccessRule([B[Ljava/security/cert/Certificate;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    .line 406
    monitor-exit p0

    return-object v1
.end method

.method private readSecurityProfile()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 562
    sget-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->accessControlType:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 563
    sget-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->accessControlType:Ljava/lang/String;

    const-string v1, "GAC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    .line 567
    :goto_0
    sget-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->accessControlType:Ljava/lang/String;

    const-string v1, "GPAC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 568
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    .line 575
    :goto_1
    iput-boolean v3, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    .line 591
    const-string v0, "SmartcardService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Allowed ACE mode: ara="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arf="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fullaccess="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    return-void

    .line 566
    :cond_0
    iput-boolean v3, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    goto :goto_0

    .line 570
    :cond_1
    iput-boolean v3, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    goto :goto_1

    .line 572
    :cond_2
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    .line 573
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized checkCommand(Lorg/simalliance/openmobileapi/service/IChannel;[B)V
    .locals 10
    .param p1, "channel"    # Lorg/simalliance/openmobileapi/service/IChannel;
    .param p2, "command"    # [B

    .prologue
    .line 258
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lorg/simalliance/openmobileapi/service/IChannel;->getChannelAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v3

    .line 259
    .local v3, "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    if-nez v3, :cond_0

    .line 261
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "Access Control Enforcer: Channel access not set"

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    .end local v3    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 264
    .restart local v3    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :cond_0
    :try_start_1
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getReason()Ljava/lang/String;

    move-result-object v6

    .line 265
    .local v6, "reason":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 266
    const-string v6, "Command not allowed!"

    .line 269
    :cond_1
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v7

    sget-object v8, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-eq v7, v8, :cond_2

    .line 271
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Access Control Enforcer: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 273
    :cond_2
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->isUseApduFilter()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 274
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduFilter()[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    move-result-object v1

    .line 275
    .local v1, "accessConditions":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    if-eqz v1, :cond_3

    array-length v7, v1

    if-nez v7, :cond_4

    .line 277
    :cond_3
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Access Control Enforcer: Access Rule not available: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 279
    :cond_4
    move-object v2, v1

    .local v2, "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_7

    aget-object v0, v2, v4

    .line 280
    .local v0, "ac":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->getMask()[B

    move-result-object v7

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->getApdu()[B

    move-result-object v8

    invoke-static {p2, v7, v8}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->compareHeaders([B[B[B)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    if-eqz v7, :cond_6

    .line 290
    .end local v0    # "ac":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v1    # "accessConditions":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v2    # "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_5
    monitor-exit p0

    return-void

    .line 279
    .restart local v0    # "ac":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .restart local v1    # "accessConditions":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .restart local v2    # "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 286
    .end local v0    # "ac":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    :cond_7
    :try_start_2
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Access Control Enforcer: Access Rule does not match: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 288
    .end local v1    # "accessConditions":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v2    # "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_8
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v7

    sget-object v8, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-eq v7, v8, :cond_5

    .line 293
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "Access Control Enforcer: APDU access NOT allowed"

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 2
    .param p1, "writer"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    .line 547
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SmartcardService"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 550
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mUseArf: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 551
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mUseAra: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 552
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mInitialChannelAccess:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 553
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 554
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 557
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    invoke-virtual {v0, p1, p2}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 558
    :cond_0
    return-void
.end method

.method public getAPPCerts(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/AccessControlException;
        }
    .end annotation

    .prologue
    .line 446
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-nez v7, :cond_1

    .line 447
    :cond_0
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "Package Name not defined"

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 452
    :cond_1
    :try_start_0
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v8, 0x40

    invoke-virtual {v7, p1, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 458
    .local v2, "foundPkgInfo":Landroid/content/pm/PackageInfo;
    if-nez v2, :cond_2

    .line 459
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "Package does not exist"

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 454
    .end local v2    # "foundPkgInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v5

    .line 455
    .local v5, "ne":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "Package does not exist"

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 462
    .end local v5    # "ne":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v2    # "foundPkgInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 464
    .local v0, "appCerts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/security/cert/Certificate;>;"
    iget-object v1, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .local v1, "arr$":[Landroid/content/pm/Signature;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v6, v1, v3

    .line 465
    .local v6, "signature":Landroid/content/pm/Signature;
    invoke-virtual {v6}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-static {v7}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->decodeCertificate([B)Ljava/security/cert/Certificate;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 467
    .end local v6    # "signature":Landroid/content/pm/Signature;
    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v7, v7, [Ljava/security/cert/Certificate;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/security/cert/Certificate;

    return-object v7
.end method

.method public getAccessRule([B[Ljava/security/cert/Certificate;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 3
    .param p1, "aid"    # [B
    .param p2, "appCerts"    # [Ljava/security/cert/Certificate;
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;,
            Lorg/simalliance/openmobileapi/service/CardException;,
            Ljava/security/cert/CertificateEncodingException;,
            Ljava/util/MissingResourceException;
        }
    .end annotation

    .prologue
    .line 411
    const/4 v0, 0x0

    .line 413
    .local v0, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    invoke-direct {p0, p1, p2}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->debugInfo([B[Ljava/security/cert/Certificate;)V

    .line 416
    iget-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mRulesRead:Z

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    invoke-virtual {v1, p1, p2}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->findAccessRule([B[Ljava/security/cert/Certificate;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v0

    .line 423
    :cond_0
    if-nez v0, :cond_1

    .line 424
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .end local v0    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 425
    .restart local v0    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v2, "no access rule found!"

    invoke-virtual {v0, v1, v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 426
    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 427
    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 429
    :cond_1
    return-object v0
.end method

.method public getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    return-object v0
.end method

.method public declared-synchronized getCdfCertsBytes()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    if-nez v0, :cond_0

    .line 325
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-direct {v0, p0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;-><init>(Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;)V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    .line 326
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->initialize(Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 328
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->getCdfCertsBytes()Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPackageManager()Landroid/content/pm/PackageManager;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method public declared-synchronized getPkgCertBytes(Ljava/lang/String;)[B
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 332
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->getPkgCertBytes(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTerminal()Lorg/simalliance/openmobileapi/service/ITerminal;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    return-object v0
.end method

.method public declared-synchronized initialize(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    .locals 8
    .param p1, "loadAtStartup"    # Z
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 114
    monitor-enter p0

    const/4 v3, 0x1

    .line 115
    .local v3, "status":Z
    :try_start_0
    const-string v0, ""

    .line 116
    .local v0, "denyMsg":Ljava/lang/String;
    const-string v4, "ro.csc.sales_code"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "salesCode":Ljava/lang/String;
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 119
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 120
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 122
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->readSecurityProfile()V

    .line 135
    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;

    if-nez v4, :cond_0

    .line 136
    new-instance v4, Lorg/simalliance/openmobileapi/service/security/ara/AraController;

    invoke-direct {v4, p0}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;-><init>(Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;)V

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;

    .line 138
    :cond_0
    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v4, :cond_1

    .line 140
    :try_start_1
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;

    invoke-virtual {v4, p1, p2}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->initialize(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 143
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ARA applet is used for:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    .line 145
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    :cond_1
    :goto_0
    :try_start_2
    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v4}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 192
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Disable ARF for terminal: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " (ARF is only available for UICC)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    .line 196
    :cond_2
    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    if-nez v4, :cond_3

    .line 197
    new-instance v4, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-direct {v4, p0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;-><init>(Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;)V

    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    .line 199
    :cond_3
    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v4, :cond_4

    .line 201
    :try_start_3
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v4, p2}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->initialize(Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 203
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ARF rules are used for:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 225
    :cond_4
    :goto_1
    :try_start_4
    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    if-eqz v4, :cond_5

    .line 226
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 227
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 228
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 230
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Full access granted for:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    :cond_5
    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    if-nez v4, :cond_6

    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    if-nez v4, :cond_6

    iget-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    if-nez v4, :cond_6

    .line 235
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 236
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 237
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v4, v5, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 239
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Deny any access to:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    :cond_6
    iput-boolean v3, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mRulesRead:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 242
    monitor-exit p0

    return v3

    .line 147
    :catch_0
    move-exception v1

    .line 150
    .local v1, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    :try_start_5
    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    .line 151
    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    .line 153
    instance-of v4, v1, Ljava/util/MissingResourceException;

    if-eqz v4, :cond_8

    .line 154
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v4}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 159
    const-string v4, "SmartcardService"

    const-string v5, "Got MissingResourceException: Does the UICC support logical channel?"

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Full message: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 243
    .end local v0    # "denyMsg":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "salesCode":Ljava/lang/String;
    :catchall_0
    move-exception v4

    :try_start_6
    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 114
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    .line 164
    .restart local v0    # "denyMsg":Ljava/lang/String;
    .restart local v1    # "e":Ljava/lang/Exception;
    .restart local v2    # "salesCode":Ljava/lang/String;
    :cond_7
    :try_start_7
    new-instance v4, Ljava/util/MissingResourceException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const-string v7, ""

    invoke-direct {v4, v5, v6, v7}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v4

    .line 166
    :cond_8
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->isNoSuchElement()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 167
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "No ARA applet found in: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 172
    :cond_9
    sget-object v4, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v5, "ISIS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "CHU"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "CTC"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "CHM"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 173
    :cond_a
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MissingResourceException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    new-instance v4, Ljava/util/MissingResourceException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const-string v7, ""

    invoke-direct {v4, v5, v6, v7}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v4

    .line 180
    :cond_b
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AccessControlEnforcer - Problem accessing ARA, Access DENIED. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    .line 184
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    .line 185
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 205
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 209
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v4, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v5, "ISIS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, "CHU"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, "CTC"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, "CHM"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 210
    :cond_c
    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MissingResourceException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v6}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    instance-of v4, v1, Ljava/util/MissingResourceException;

    if-eqz v4, :cond_d

    .line 212
    check-cast v1, Ljava/util/MissingResourceException;

    .end local v1    # "e":Ljava/lang/Exception;
    throw v1

    .line 217
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_d
    const/4 v4, 0x0

    iput-boolean v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    .line 218
    const/4 v3, 0x0

    .line 219
    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    .line 220
    const-string v4, "SmartcardService"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1
.end method

.method public declared-synchronized isCdfAllowed(Ljava/lang/String;)Z
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    if-nez v0, :cond_0

    .line 301
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-direct {v0, p0}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;-><init>(Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;)V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    .line 302
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->initialize(Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    .line 304
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;

    invoke-virtual {v0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->isCdfAllowed(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 318
    :goto_0
    monitor-exit p0

    return v0

    .line 306
    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 311
    const/4 v0, 0x1

    goto :goto_0

    .line 318
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isNFCEventAllowed([B[Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)[Z
    .locals 3
    .param p1, "aid"    # [B
    .param p2, "packageNames"    # [Ljava/lang/String;
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 499
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    if-eqz v2, :cond_2

    .line 500
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->internal_isNFCEventAllowed([B[Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)[Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 509
    :cond_1
    monitor-exit p0

    return-object v1

    .line 505
    :cond_2
    :try_start_1
    array-length v2, p2

    new-array v1, v2, [Z

    .line 506
    .local v1, "ret":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 507
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    aput-boolean v2, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 506
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 499
    .end local v0    # "i":I
    .end local v1    # "ret":[Z
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized reset()V
    .locals 3

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    const-string v0, "SmartcardService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reset the ACE for terminal:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v2}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mAraController:Lorg/simalliance/openmobileapi/service/security/ara/AraController;

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mArfController:Lorg/simalliance/openmobileapi/service/security/arf/ArfController;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setPackageManager(Landroid/content/pm/PackageManager;)V
    .locals 0
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 89
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 90
    return-void
.end method

.method public setUpChannelAccess([BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 6
    .param p1, "aid"    # [B
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 340
    const/4 v0, 0x0

    .line 343
    .local v0, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v2

    sget-object v3, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-ne v2, v3, :cond_0

    .line 344
    new-instance v2, Ljava/security/AccessControlException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Access Control Enforcer: access denied: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getReason()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 347
    :cond_0
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseAra:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mUseArf:Z

    if-eqz v2, :cond_2

    .line 350
    :cond_1
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->internal_setUpChannelAccess([BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 361
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v2

    sget-object v3, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-eq v2, v3, :cond_4

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->isUseApduFilter()Z

    move-result v2

    if-nez v2, :cond_4

    .line 365
    :cond_3
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mFullAccess:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 368
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->mInitialChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .line 374
    :cond_4
    invoke-virtual {v0, p2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setPackageName(Ljava/lang/String;)V

    .line 376
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->clone()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v2

    return-object v2

    .line 351
    :catch_0
    move-exception v1

    .line 352
    .local v1, "e":Ljava/lang/Exception;
    instance-of v2, v1, Ljava/util/MissingResourceException;

    if-eqz v2, :cond_5

    .line 353
    new-instance v2, Ljava/util/MissingResourceException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Access Control Enforcer: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    const-string v5, ""

    invoke-direct {v2, v3, v4, v5}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v2

    .line 356
    :cond_5
    new-instance v2, Ljava/security/AccessControlException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Access Control Enforcer: access denied: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 370
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    new-instance v2, Ljava/security/AccessControlException;

    const-string v3, "Access Control Enforcer: no APDU access allowed!"

    invoke-direct {v2, v3}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
