.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.super Ljava/lang/Object;
.source "EF.java"


# static fields
.field public static final APDU_SUCCESS:I = 0x9000

.field private static final BUFFER_LEN:I = 0xfd

.field private static final EF:S = 0x4s

.field private static final LINEAR_FIXED:S = 0x1s

.field public static final TAG:Ljava/lang/String; = "SmartcardService ACE ARF"

.field private static final TRANSPARENT:S = 0x0s

.field private static final UNKNOWN:S = 0xffs


# instance fields
.field private mFileID:I

.field private mFileNbRecords:S

.field private mFilePath:Ljava/lang/String;

.field private mFileRecordSize:I

.field private mFileSize:I

.field private mFileStructure:S

.field private mFileType:S

.field protected mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V
    .locals 1
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .prologue
    const/16 v0, 0xff

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .line 42
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .line 43
    return-void
.end method

.method private decodeFileProperties([B)V
    .locals 2
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 55
    if-eqz p1, :cond_0

    .line 58
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    const/16 v1, 0x62

    if-ne v0, v1, :cond_1

    .line 59
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->decodeUSIMFileProps([B)V

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->decodeSIMFileProps([B)V

    goto :goto_0
.end method

.method private decodeSIMFileProps([B)V
    .locals 6
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/16 v5, 0xff

    const/16 v4, 0xd

    const/4 v3, 0x1

    const/4 v2, 0x4

    .line 74
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v1, 0xf

    if-ge v0, v1, :cond_1

    .line 75
    :cond_0
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v1, "Invalid Response data"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_1
    const/4 v0, 0x6

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    if-ne v0, v2, :cond_3

    .line 80
    iput-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    .line 84
    :goto_0
    aget-byte v0, p1, v4

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    if-nez v0, :cond_4

    .line 85
    const/4 v0, 0x0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    .line 91
    :goto_1
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x3

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    iput v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileSize:I

    .line 94
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    if-ne v0, v2, :cond_2

    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    if-eqz v0, :cond_2

    .line 96
    const/16 v0, 0xe

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    .line 97
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileSize:I

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    div-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    .line 99
    :cond_2
    return-void

    .line 82
    :cond_3
    iput-short v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    goto :goto_0

    .line 86
    :cond_4
    aget-byte v0, p1, v4

    and-int/lit16 v0, v0, 0xff

    int-to-short v0, v0

    if-ne v0, v3, :cond_5

    .line 87
    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    goto :goto_1

    .line 89
    :cond_5
    iput-short v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    goto :goto_1
.end method

.method private decodeUSIMFileProps([B)V
    .locals 7
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 109
    const/4 v1, 0x0

    .line 110
    .local v1, "buffer":[B
    :try_start_0
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 112
    .local v0, "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    const/16 v3, 0x62

    invoke-virtual {v0, v3}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 113
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v3

    if-nez v3, :cond_4

    .line 114
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 145
    :pswitch_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->skipTLVData()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    .end local v0    # "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :catch_0
    move-exception v2

    .line 149
    .local v2, "e":Ljava/lang/Exception;
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v4, "Invalid GetResponse"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 116
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :pswitch_1
    :try_start_1
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v1

    .line 117
    if-eqz v1, :cond_0

    array-length v3, v1

    if-lt v3, v6, :cond_0

    .line 118
    const/4 v3, 0x0

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    const/4 v4, 0x1

    aget-byte v4, v1, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v3, v4

    iput v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileSize:I

    goto :goto_0

    .line 121
    :pswitch_2
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v1

    .line 122
    if-eqz v1, :cond_0

    array-length v3, v1

    if-lt v3, v6, :cond_0

    .line 123
    const/4 v3, 0x0

    aget-byte v3, v1, v3

    and-int/lit8 v3, v3, 0x7

    int-to-short v3, v3

    if-ne v3, v5, :cond_1

    .line 124
    const/4 v3, 0x0

    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    .line 134
    :goto_1
    const/4 v3, 0x0

    aget-byte v3, v1, v3

    and-int/lit8 v3, v3, 0x38

    int-to-short v3, v3

    const/16 v4, 0x38

    if-ne v3, v4, :cond_3

    .line 135
    const/16 v3, 0xff

    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    .line 139
    :goto_2
    array-length v3, v1

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    .line 140
    const/4 v3, 0x3

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    iput v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    .line 141
    const/4 v3, 0x4

    aget-byte v3, v1, v3

    and-int/lit16 v3, v3, 0xff

    int-to-short v3, v3

    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    goto :goto_0

    .line 125
    :cond_1
    const/4 v3, 0x0

    aget-byte v3, v1, v3

    and-int/lit8 v3, v3, 0x7

    int-to-short v3, v3

    if-ne v3, v6, :cond_2

    .line 126
    const/4 v3, 0x1

    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    goto :goto_1

    .line 128
    :cond_2
    const/16 v3, 0xff

    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    goto :goto_1

    .line 137
    :cond_3
    const/4 v3, 0x4

    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 151
    :cond_4
    return-void

    .line 114
    :pswitch_data_0
    .packed-switch -0x80
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getFileId()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileID:I

    return v0
.end method

.method public getFileNbRecords()S
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 280
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    if-gez v0, :cond_0

    .line 281
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v1, "Incorrect file type"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_0
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    return v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public readBinary(II)[B
    .locals 8
    .param p1, "offset"    # I
    .param p2, "nbBytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    .line 222
    iget v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileSize:I

    if-nez v4, :cond_1

    const/4 v3, 0x0

    .line 247
    :cond_0
    return-object v3

    .line 223
    :cond_1
    const/4 v4, -0x1

    if-ne p2, v4, :cond_2

    iget p2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileSize:I

    .line 224
    :cond_2
    iget-short v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    if-eq v4, v7, :cond_3

    .line 225
    new-instance v4, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v5, "Incorrect file type"

    invoke-direct {v4, v5}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 226
    :cond_3
    iget-short v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    if-eqz v4, :cond_4

    .line 227
    new-instance v4, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v5, "Incorrect file structure"

    invoke-direct {v4, v5}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 229
    :cond_4
    const/4 v2, 0x0

    .line 230
    .local v2, "pos":I
    new-array v3, p2, [B

    .line 231
    .local v3, "result":[B
    const/4 v4, 0x5

    new-array v0, v4, [B

    fill-array-data v0, :array_0

    .line 233
    .local v0, "cmd":[B
    :goto_0
    if-eqz p2, :cond_0

    .line 234
    const/16 v4, 0xfd

    if-ge p2, v4, :cond_5

    .line 235
    move v1, p2

    .line 239
    .local v1, "length":I
    :goto_1
    const-string v4, "SmartcardService ACE ARF"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ReadBinary ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "b]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const/4 v4, 0x2

    shr-int/lit8 v5, p1, 0x8

    int-to-byte v5, v5

    aput-byte v5, v0, v4

    .line 242
    const/4 v4, 0x3

    int-to-byte v5, p1

    aput-byte v5, v0, v4

    .line 243
    int-to-byte v4, v1

    aput-byte v4, v0, v7

    .line 244
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v4, p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->exchangeAPDU(Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;[B)[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5, v3, v2, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 245
    sub-int/2addr p2, v1

    add-int/2addr p1, v1

    add-int/2addr v2, v1

    goto :goto_0

    .line 237
    .end local v1    # "length":I
    :cond_5
    const/16 v1, 0xfd

    .restart local v1    # "length":I
    goto :goto_1

    .line 231
    :array_0
    .array-data 1
        0x0t
        -0x50t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public readRecord(S)[B
    .locals 7
    .param p1, "record"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 258
    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    if-eq v1, v4, :cond_0

    .line 259
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v2, "Incorrect file type"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 260
    :cond_0
    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    if-eq v1, v6, :cond_1

    .line 261
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v2, "Incorrect file structure"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 264
    :cond_1
    if-ltz p1, :cond_2

    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    if-le p1, v1, :cond_3

    .line 265
    :cond_2
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v2, "Incorrect record number"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 267
    :cond_3
    const-string v1, "SmartcardService ACE ARF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ReadRecord ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "b]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v1, 0x5

    new-array v0, v1, [B

    aput-byte v5, v0, v5

    const/16 v1, -0x4e

    aput-byte v1, v0, v6

    const/4 v1, 0x2

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    aput-byte v4, v0, v1

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    int-to-byte v1, v1

    aput-byte v1, v0, v4

    .line 270
    .local v0, "cmd":[B
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v1, p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->exchangeAPDU(Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;[B)[B

    move-result-object v1

    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    return-object v1
.end method

.method public selectFile([B)I
    .locals 9
    .param p1, "path"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/16 v8, 0xff

    const/4 v7, 0x0

    .line 162
    if-eqz p1, :cond_0

    array-length v5, p1

    if-eqz v5, :cond_0

    array-length v5, p1

    rem-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_1

    .line 163
    :cond_0
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;

    const-string v6, "Incorrect path"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 166
    :cond_1
    array-length v3, p1

    .line 167
    .local v3, "length":I
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->getSeInterface()S

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    const/4 v5, 0x2

    if-le v3, v5, :cond_2

    .line 168
    add-int/lit8 v2, v3, -0x2

    .line 169
    .local v2, "index":I
    const-string v5, ""

    invoke-static {p1, v7, v2, v5}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFilePath:Ljava/lang/String;

    .line 176
    :goto_0
    const/4 v1, 0x0

    .line 177
    .local v1, "data":[B
    const/4 v5, 0x7

    new-array v0, v5, [B

    fill-array-data v0, :array_0

    .line 179
    .local v0, "cmd":[B
    iput-short v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileType:S

    .line 180
    iput-short v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileStructure:S

    .line 181
    iput v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileSize:I

    .line 182
    iput v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    .line 183
    iput-short v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    .line 186
    :goto_1
    if-ge v2, v3, :cond_4

    .line 187
    aget-byte v5, p1, v2

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    add-int/lit8 v6, v2, 0x1

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    iput v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileID:I

    .line 188
    const/4 v5, 0x5

    iget v6, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileID:I

    shr-int/lit8 v6, v6, 0x8

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 189
    const/4 v5, 0x6

    iget v6, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileID:I

    int-to-byte v6, v6

    aput-byte v6, v0, v5

    .line 191
    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v5, p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->exchangeAPDU(Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;[B)[B

    move-result-object v1

    .line 194
    array-length v5, v1

    add-int/lit8 v5, v5, -0x2

    aget-byte v5, v1, v5

    and-int/lit16 v4, v5, 0xff

    .line 195
    .local v4, "sw1":I
    const/16 v5, 0x62

    if-eq v4, v5, :cond_3

    const/16 v5, 0x63

    if-eq v4, v5, :cond_3

    const/16 v5, 0x90

    if-eq v4, v5, :cond_3

    const/16 v5, 0x91

    if-eq v4, v5, :cond_3

    .line 200
    shl-int/lit8 v5, v4, 0x8

    array-length v6, v1

    add-int/lit8 v6, v6, -0x1

    aget-byte v6, v1, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    .line 211
    .end local v4    # "sw1":I
    :goto_2
    return v5

    .line 171
    .end local v0    # "cmd":[B
    .end local v1    # "data":[B
    .end local v2    # "index":I
    :cond_2
    const/4 v2, 0x0

    .line 172
    .restart local v2    # "index":I
    const-string v5, ""

    iput-object v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFilePath:Ljava/lang/String;

    goto :goto_0

    .line 186
    .restart local v0    # "cmd":[B
    .restart local v1    # "data":[B
    .restart local v4    # "sw1":I
    :cond_3
    add-int/lit8 v2, v2, 0x2

    goto :goto_1

    .line 205
    .end local v4    # "sw1":I
    :cond_4
    invoke-direct {p0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->decodeFileProperties([B)V

    .line 207
    iget-short v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    if-nez v5, :cond_5

    .line 208
    const-string v5, "SmartcardService ACE ARF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SelectFile ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileSize:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "b]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_3
    const v5, 0x9000

    goto :goto_2

    .line 210
    :cond_5
    const-string v5, "SmartcardService ACE ARF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SelectFile ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-short v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileNbRecords:S

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "*"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;->mFileRecordSize:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "b]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 177
    nop

    :array_0
    .array-data 1
        0x0t
        -0x5ct
        0x0t
        0x4t
        0x2t
        0x0t
        0x0t
    .end array-data
.end method
