.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "REF_AR_DO.java"


# static fields
.field public static final _TAG:I = 0xe2


# instance fields
.field private mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

.field private mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 42
    const/16 v0, 0xe2

    invoke-direct {p0, v1, v0, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 34
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 35
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 43
    return-void
.end method

.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;)V
    .locals 3
    .param p1, "ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .param p2, "ar_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 46
    const/16 v0, 0xe2

    invoke-direct {p0, v1, v0, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 34
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 35
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 47
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 48
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 49
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    const/4 v1, 0x0

    .line 38
    const/16 v0, 0xe2

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 34
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 35
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 39
    return-void
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 6
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    .line 121
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 123
    .local v2, "temp":Ljava/io/ByteArrayOutputStream;
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    if-nez v3, :cond_1

    .line 124
    :cond_0
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v4, "REF-AR-DO: Required DO missing!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 126
    :cond_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getTag()I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 128
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-virtual {v3, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 129
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    invoke-virtual {v3, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 131
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 132
    .local v0, "data":[B
    array-length v3, v0

    invoke-static {v3, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->encodeLength(ILjava/io/ByteArrayOutputStream;)V

    .line 134
    :try_start_0
    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    return-void

    .line 135
    :catch_0
    move-exception v1

    .line 136
    .local v1, "e":Ljava/io/IOException;
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "REF-AR-DO Memory IO problem! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    return-object v0
.end method

.method public getRefDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    return-object v0
.end method

.method public interpret()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 76
    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 77
    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 79
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getRawData()[B

    move-result-object v0

    .line 80
    .local v0, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getValueIndex()I

    move-result v1

    .line 82
    .local v1, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getValueLength()I

    move-result v3

    add-int/2addr v3, v1

    array-length v4, v0

    if-le v3, v4, :cond_0

    .line 83
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "Not enough data for AR_DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 87
    :cond_0
    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BI)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v2

    .line 88
    .local v2, "temp":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v3

    const/16 v4, 0xe1

    if-ne v3, v4, :cond_2

    .line 89
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v4

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v5

    invoke-direct {v3, v0, v4, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;-><init>([BII)V

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 90
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->interpret()V

    .line 99
    :cond_1
    :goto_0
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v3

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v4

    add-int v1, v3, v4

    .line 100
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getValueIndex()I

    move-result v3

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getValueLength()I

    move-result v4

    add-int/2addr v3, v4

    if-gt v3, v1, :cond_0

    .line 103
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mRefDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    if-nez v3, :cond_3

    .line 104
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "Missing Ref-DO in REF-AR-DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 91
    :cond_2
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v3

    const/16 v4, 0xe3

    if-ne v3, v4, :cond_1

    .line 92
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v4

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v5

    invoke-direct {v3, v0, v4, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;-><init>([BII)V

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .line 93
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->interpret()V

    goto :goto_0

    .line 106
    :cond_3
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->mArDo:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    if-nez v3, :cond_4

    .line 107
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "Missing AR-DO in REF-AR-DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 109
    :cond_4
    return-void
.end method
