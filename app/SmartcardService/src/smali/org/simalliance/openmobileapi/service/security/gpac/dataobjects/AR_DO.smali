.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "AR_DO.java"


# static fields
.field public static final _TAG:I = 0xe3


# instance fields
.field private mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

.field private mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;)V
    .locals 3
    .param p1, "apdu_ar_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;
    .param p2, "nfc_ar_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 43
    const/16 v0, 0xe3

    invoke-direct {p0, v1, v0, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 35
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    .line 36
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    .line 44
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    .line 45
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    .line 46
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 2
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    const/4 v1, 0x0

    .line 39
    const/16 v0, 0xe3

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 35
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    .line 36
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    .line 40
    return-void
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 5
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    .line 117
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getTag()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 119
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 120
    .local v1, "temp":Ljava/io/ByteArrayOutputStream;
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    if-eqz v2, :cond_0

    .line 121
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    invoke-virtual {v2, v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 124
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    if-eqz v2, :cond_1

    .line 125
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    invoke-virtual {v2, v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 128
    :cond_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v2

    invoke-static {v2, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->encodeLength(ILjava/io/ByteArrayOutputStream;)V

    .line 130
    :try_start_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AR-DO Memory IO problem! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getApduArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    return-object v0
.end method

.method public getNfcArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    return-object v0
.end method

.method public interpret()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 70
    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    .line 71
    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    .line 73
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getRawData()[B

    move-result-object v0

    .line 74
    .local v0, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getValueIndex()I

    move-result v1

    .line 76
    .local v1, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getValueLength()I

    move-result v3

    add-int/2addr v3, v1

    array-length v4, v0

    if-le v3, v4, :cond_0

    .line 77
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "Not enough data for AR_DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 81
    :cond_0
    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BI)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v2

    .line 83
    .local v2, "temp":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v3

    const/16 v4, 0xd0

    if-ne v3, v4, :cond_2

    .line 84
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v4

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v5

    invoke-direct {v3, v0, v4, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;-><init>([BII)V

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    .line 85
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->interpret()V

    .line 94
    :cond_1
    :goto_0
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v3

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v4

    add-int v1, v3, v4

    .line 95
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getValueIndex()I

    move-result v3

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getValueLength()I

    move-result v4

    add-int/2addr v3, v4

    if-gt v3, v1, :cond_0

    .line 97
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mApduAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    if-nez v3, :cond_3

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    if-nez v3, :cond_3

    .line 98
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v4, "No valid DO in AR-DO!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 86
    :cond_2
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v3

    const/16 v4, 0xd1

    if-ne v3, v4, :cond_1

    .line 87
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v4

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v5

    invoke-direct {v3, v0, v4, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;-><init>([BII)V

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    .line 88
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->mNfcAr:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->interpret()V

    goto :goto_0

    .line 100
    :cond_3
    return-void
.end method
