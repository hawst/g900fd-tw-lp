.class public Lorg/simalliance/openmobileapi/service/security/CommandApdu;
.super Ljava/lang/Object;
.source "CommandApdu.java"


# instance fields
.field protected mCla:I

.field protected mData:[B

.field protected mIns:I

.field protected mLc:I

.field protected mLe:I

.field protected mLeUsed:Z

.field protected mP1:I

.field protected mP2:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 23
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 25
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 27
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 29
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 31
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 33
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 35
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 46
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 2
    .param p1, "cla"    # I
    .param p2, "ins"    # I
    .param p3, "p1"    # I
    .param p4, "p2"    # I

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 23
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 25
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 27
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 29
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 31
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 33
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 35
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 38
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 39
    iput p2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 40
    iput p3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 41
    iput p4, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 42
    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 2
    .param p1, "cla"    # I
    .param p2, "ins"    # I
    .param p3, "p1"    # I
    .param p4, "p2"    # I
    .param p5, "le"    # I

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 23
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 25
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 27
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 29
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 31
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 33
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 35
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 69
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 70
    iput p2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 71
    iput p3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 72
    iput p4, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 73
    iput p5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 75
    return-void
.end method

.method public constructor <init>(IIII[B)V
    .locals 2
    .param p1, "cla"    # I
    .param p2, "ins"    # I
    .param p3, "p1"    # I
    .param p4, "p2"    # I
    .param p5, "data"    # [B

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 23
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 25
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 27
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 29
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 31
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 33
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 35
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 49
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 50
    iput p2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 51
    array-length v0, p5

    iput v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 52
    iput p3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 53
    iput p4, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 54
    iput-object p5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 55
    return-void
.end method

.method public constructor <init>(IIII[BI)V
    .locals 2
    .param p1, "cla"    # I
    .param p2, "ins"    # I
    .param p3, "p1"    # I
    .param p4, "p2"    # I
    .param p5, "data"    # [B
    .param p6, "le"    # I

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 23
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 25
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 27
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 29
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 31
    new-array v0, v1, [B

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 33
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 35
    iput-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 58
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 59
    iput p2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 60
    array-length v0, p5

    iput v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 61
    iput p3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 62
    iput p4, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 63
    iput-object p5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 64
    iput p6, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 66
    return-void
.end method

.method public constructor <init>([B)V
    .locals 8
    .param p1, "apdu"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x6

    const/4 v3, 0x4

    const/4 v4, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 23
    iput v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 25
    iput v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 27
    iput v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 29
    iput v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 31
    new-array v2, v5, [B

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 33
    iput v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 35
    iput-boolean v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 194
    array-length v0, p1

    .line 195
    .local v0, "len":I
    if-ge v0, v3, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "apdu must be at least 4 bytes long"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 198
    :cond_0
    aget-byte v2, p1, v5

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 199
    aget-byte v2, p1, v6

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 200
    const/4 v2, 0x2

    aget-byte v2, p1, v2

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 201
    const/4 v2, 0x3

    aget-byte v2, p1, v2

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 203
    if-le v0, v3, :cond_1

    .line 204
    aget-byte v2, p1, v3

    and-int/lit16 v1, v2, 0xff

    .line 207
    .local v1, "p3":I
    if-ne v0, v4, :cond_2

    .line 209
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 210
    iput-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 269
    .end local v1    # "p3":I
    :cond_1
    :goto_0
    return-void

    .line 215
    .restart local v1    # "p3":I
    :cond_2
    if-nez v1, :cond_8

    .line 216
    if-ne v0, v7, :cond_3

    .line 217
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "wrong apdu"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 220
    :cond_3
    const/4 v2, 0x7

    if-ne v0, v2, :cond_4

    .line 221
    aget-byte v2, p1, v4

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    aget-byte v3, p1, v7

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 222
    iput-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    goto :goto_0

    .line 225
    :cond_4
    aget-byte v2, p1, v4

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    aget-byte v3, p1, v7

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 227
    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    if-nez v2, :cond_5

    .line 228
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "wrong apdu"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 230
    :cond_5
    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 232
    add-int/lit8 v2, v0, -0x7

    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    if-ne v2, v3, :cond_6

    .line 233
    const/4 v2, 0x7

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    iget v4, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    invoke-static {p1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0

    .line 236
    :cond_6
    add-int/lit8 v2, v0, -0x9

    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    if-ne v2, v3, :cond_7

    .line 237
    const/4 v2, 0x7

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    iget v4, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    invoke-static {p1, v2, v3, v5, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 238
    add-int/lit8 v2, v0, -0x2

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/lit8 v3, v0, -0x1

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 239
    iput-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    goto :goto_0

    .line 242
    :cond_7
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "command length is not correct"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 249
    :cond_8
    aget-byte v2, p1, v3

    and-int/lit16 v2, v2, 0xff

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 250
    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    if-nez v2, :cond_9

    .line 251
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "wrong apdu"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 253
    :cond_9
    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 255
    add-int/lit8 v2, v0, -0x5

    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    if-ne v2, v3, :cond_a

    .line 256
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    invoke-static {p1, v4, v2, v5, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto/16 :goto_0

    .line 259
    :cond_a
    add-int/lit8 v2, v0, -0x6

    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    if-ne v2, v3, :cond_b

    .line 260
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    invoke-static {p1, v4, v2, v5, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 261
    add-int/lit8 v2, v0, -0x2

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/lit8 v3, v0, -0x1

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 262
    iput-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    goto/16 :goto_0

    .line 265
    :cond_b
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "command length is not correct"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static compareHeaders([B[B[B)Z
    .locals 7
    .param p0, "header1"    # [B
    .param p1, "mask"    # [B
    .param p2, "header2"    # [B

    .prologue
    const/4 v4, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    array-length v3, p0

    if-lt v3, v4, :cond_0

    array-length v3, p2

    if-ge v3, v4, :cond_2

    :cond_0
    move v1, v2

    .line 165
    :cond_1
    :goto_0
    return v1

    .line 153
    :cond_2
    new-array v0, v4, [B

    .line 154
    .local v0, "compHeader":[B
    aget-byte v3, p0, v2

    aget-byte v4, p1, v2

    and-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    .line 155
    aget-byte v3, p0, v1

    aget-byte v4, p1, v1

    and-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 156
    aget-byte v3, p0, v5

    aget-byte v4, p1, v5

    and-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v5

    .line 157
    aget-byte v3, p0, v6

    aget-byte v4, p1, v6

    and-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v6

    .line 159
    aget-byte v3, v0, v2

    aget-byte v4, p2, v2

    if-ne v3, v4, :cond_3

    aget-byte v3, v0, v1

    aget-byte v4, p2, v1

    if-ne v3, v4, :cond_3

    aget-byte v3, v0, v5

    aget-byte v4, p2, v5

    if-ne v3, v4, :cond_3

    aget-byte v3, v0, v6

    aget-byte v4, p2, v6

    if-eq v3, v4, :cond_1

    :cond_3
    move v1, v2

    .line 165
    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->clone()Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/simalliance/openmobileapi/service/security/CommandApdu;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 169
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;-><init>()V

    .line 170
    .local v0, "apdu":Lorg/simalliance/openmobileapi/service/security/CommandApdu;
    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    iput v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    .line 171
    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    iput v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    .line 172
    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    iput v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 173
    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    iput v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 174
    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    iput v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 175
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    array-length v1, v1

    new-array v1, v1, [B

    iput-object v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 176
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    iget-object v2, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    array-length v3, v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 177
    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    iput v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 178
    iget-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    iput-boolean v1, v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 179
    return-object v0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    return-object v0
.end method

.method public getLc()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    return v0
.end method

.method public getLe()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    return v0
.end method

.method public getP1()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    return v0
.end method

.method public getP2()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    return v0
.end method

.method public setData([B)V
    .locals 1
    .param p1, "data"    # [B

    .prologue
    .line 86
    array-length v0, p1

    iput v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    .line 87
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    .line 88
    return-void
.end method

.method public setLe(I)V
    .locals 1
    .param p1, "le"    # I

    .prologue
    .line 91
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    .line 93
    return-void
.end method

.method public setP1(I)V
    .locals 0
    .param p1, "p1"    # I

    .prologue
    .line 78
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    .line 79
    return-void
.end method

.method public setP2(I)V
    .locals 0
    .param p1, "p2"    # I

    .prologue
    .line 82
    iput p1, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    .line 83
    return-void
.end method

.method public toBytes()[B
    .locals 6

    .prologue
    .line 116
    const/4 v2, 0x4

    .line 117
    .local v2, "length":I
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    array-length v3, v3

    if-eqz v3, :cond_0

    .line 118
    add-int/lit8 v2, v2, 0x1

    .line 119
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    array-length v3, v3

    add-int/lit8 v2, v3, 0x5

    .line 121
    :cond_0
    iget-boolean v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    if-eqz v3, :cond_1

    .line 122
    add-int/lit8 v2, v2, 0x1

    .line 125
    :cond_1
    new-array v0, v2, [B

    .line 127
    .local v0, "apdu":[B
    const/4 v1, 0x0

    .line 128
    .local v1, "index":I
    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mCla:I

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 129
    add-int/lit8 v1, v1, 0x1

    .line 130
    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mIns:I

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 131
    add-int/lit8 v1, v1, 0x1

    .line 132
    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP1:I

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 133
    add-int/lit8 v1, v1, 0x1

    .line 134
    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mP2:I

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 135
    add-int/lit8 v1, v1, 0x1

    .line 136
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    array-length v3, v3

    if-eqz v3, :cond_2

    .line 137
    iget v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLc:I

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 138
    add-int/lit8 v1, v1, 0x1

    .line 139
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    const/4 v4, 0x0

    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    array-length v5, v5

    invoke-static {v3, v4, v0, v1, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 140
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mData:[B

    array-length v3, v3

    add-int/lit8 v1, v3, 0x5

    .line 142
    :cond_2
    iget-boolean v3, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLeUsed:Z

    if-eqz v3, :cond_3

    .line 143
    aget-byte v3, v0, v1

    iget v4, p0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->mLe:I

    int-to-byte v4, v4

    add-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v1

    .line 146
    :cond_3
    return-object v0
.end method
