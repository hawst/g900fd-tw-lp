.class Lorg/simalliance/openmobileapi/service/SmartcardService$3;
.super Landroid/content/BroadcastReceiver;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/simalliance/openmobileapi/service/SmartcardService;->registerMediaMountedEvent(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;


# direct methods
.method constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$3;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 381
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 382
    .local v0, "mediaMounted":Z
    if-eqz v0, :cond_0

    .line 383
    const-string v1, "SmartcardService"

    const-string v2, "New Media is mounted. Checking access rules for updates."

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$3;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->sendMessage(II)V

    .line 386
    :cond_0
    return-void
.end method
