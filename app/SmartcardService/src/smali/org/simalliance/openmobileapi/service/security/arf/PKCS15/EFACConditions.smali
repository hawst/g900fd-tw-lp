.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;
.super Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.source "EFACConditions.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "ACE ARF EF_ACConditions"


# instance fields
.field private mAid_Ref_Do:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

.field private mData:[B


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;)V
    .locals 1
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;
    .param p2, "Aid_Ref_Do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 47
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mAid_Ref_Do:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 49
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mData:[B

    .line 60
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mAid_Ref_Do:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 61
    return-void
.end method

.method private decodeDER([B)V
    .locals 19
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v6, 0x0

    .line 71
    .local v6, "certificateHash":[B
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 74
    .local v3, "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    new-instance v7, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v7}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 75
    .local v7, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    new-instance v13, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-direct {v13}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>()V

    .line 78
    .local v13, "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    const/4 v2, 0x0

    .line 79
    .local v2, "ACConditions":[B
    const/4 v4, 0x0

    .line 80
    .local v4, "TagCertHashOrACRules":B
    const-string v17, "ro.csc.sales_code"

    invoke-static/range {v17 .. v17}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 84
    .local v14, "salesCode":Ljava/lang/String;
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 86
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v18, "access denied because of empty condition file"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 87
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 88
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mAid_Ref_Do:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v13, v7}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->putAccessRule(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 234
    :cond_0
    return-void

    .line 232
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mAid_Ref_Do:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v13, v7}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->putAccessRule(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 120
    :cond_2
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v17

    if-nez v17, :cond_0

    .line 128
    new-instance v7, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .end local v7    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    invoke-direct {v7}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 129
    .restart local v7    # "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 130
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 131
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 132
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setUseApduFilter(Z)V

    .line 134
    const/16 v17, 0x30

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    move-result v17

    if-lez v17, :cond_1

    .line 137
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v2

    .line 138
    new-instance v12, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v12, v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 140
    .local v12, "derRule":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    if-eqz v2, :cond_3

    array-length v0, v2

    move/from16 v17, v0

    if-lez v17, :cond_4

    const/16 v17, 0x0

    aget-byte v4, v2, v17

    .line 142
    :cond_3
    :goto_0
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ne v4, v0, :cond_6

    .line 143
    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 144
    invoke-virtual {v12}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v6

    .line 146
    array-length v0, v6

    move/from16 v17, v0

    const/16 v18, 0x14

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_5

    array-length v0, v6

    move/from16 v17, v0

    if-eqz v17, :cond_5

    .line 149
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v18, "Invalid hash found!"

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v17

    .line 140
    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    .line 151
    :cond_5
    new-instance v13, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .end local v13    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    invoke-direct {v13, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>([B)V

    .line 164
    .restart local v13    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :goto_1
    invoke-virtual {v12}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v17

    if-nez v17, :cond_1

    .line 166
    invoke-virtual {v12}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v17

    const/16 v18, -0x60

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 168
    new-instance v8, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-virtual {v12}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v8, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 170
    .local v8, "derAccessRules":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :goto_2
    invoke-virtual {v8}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v17

    if-nez v17, :cond_1

    .line 171
    invoke-virtual {v8}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v17

    packed-switch v17, :pswitch_data_0

    .line 221
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v18, "Invalid element found!"

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v17

    .line 153
    .end local v8    # "derAccessRules":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :cond_6
    const/16 v17, -0x60

    move/from16 v0, v17

    if-ne v4, v0, :cond_7

    .line 155
    const-string v17, "ACE ARF EF_ACConditions"

    const-string v18, "decodeDER certificateHash is null"

    invoke-static/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    new-instance v13, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    .end local v13    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    invoke-direct {v13}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>()V

    .restart local v13    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    goto :goto_1

    .line 158
    :cond_7
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v18, "Invalid element found!"

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v17

    .line 174
    .restart local v8    # "derAccessRules":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :pswitch_0
    new-instance v10, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-virtual {v8}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v10, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 175
    .local v10, "derApduRule":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    invoke-virtual {v10}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v16

    .line 177
    .local v16, "tagApduAccessRule":B
    const/16 v17, -0x80

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_9

    .line 179
    invoke-virtual {v10}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v17

    const/16 v18, 0x0

    aget-byte v17, v17, v18

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    :goto_3
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    goto :goto_2

    :cond_8
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    goto :goto_3

    .line 182
    :cond_9
    const/16 v17, -0x5f

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_d

    .line 184
    new-instance v9, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-virtual {v10}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 185
    .local v9, "derApduFilter":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v15

    .line 187
    .local v15, "tag":B
    const/16 v17, 0x4

    move/from16 v0, v17

    if-ne v15, v0, :cond_c

    .line 189
    new-instance v5, Ljava/util/Vector;

    invoke-direct {v5}, Ljava/util/Vector;-><init>()V

    .line 192
    .local v5, "apduFilter":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/simalliance/openmobileapi/service/security/ApduFilter;>;"
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;-><init>([B)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_a
    :goto_4
    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v17

    if-nez v17, :cond_b

    .line 195
    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v17

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 196
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    invoke-virtual {v9}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;-><init>([B)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 199
    :cond_b
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setUseApduFilter(Z)V

    .line 200
    invoke-virtual {v5}, Ljava/util/Vector;->size()I

    move-result v17

    move/from16 v0, v17

    new-array v0, v0, [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    goto/16 :goto_2

    .line 202
    .end local v5    # "apduFilter":Ljava/util/Vector;, "Ljava/util/Vector<Lorg/simalliance/openmobileapi/service/security/ApduFilter;>;"
    :cond_c
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v18, "Invalid element found!"

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v17

    .line 206
    .end local v9    # "derApduFilter":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    .end local v15    # "tag":B
    :cond_d
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v18, "Invalid element found!"

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v17

    .line 211
    .end local v10    # "derApduRule":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    .end local v16    # "tagApduAccessRule":B
    :pswitch_1
    new-instance v11, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-virtual {v8}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v11, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 213
    .local v11, "derNfc":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    invoke-virtual {v11}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v17

    const/16 v18, -0x80

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_f

    .line 214
    invoke-virtual {v11}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v17

    const/16 v18, 0x0

    aget-byte v17, v17, v18

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_e

    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    :goto_5
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    goto/16 :goto_2

    :cond_e
    sget-object v17, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    goto :goto_5

    .line 217
    :cond_f
    new-instance v17, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v18, "Invalid element found!"

    invoke-direct/range {v17 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v17

    .line 171
    :pswitch_data_0
    .packed-switch -0x60
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addRestrictedHashes([B)V
    .locals 6
    .param p1, "path"    # [B

    .prologue
    .line 243
    :try_start_0
    const-string v3, "ACE ARF EF_ACConditions"

    const-string v4, "Reading and analysing EF_ACConditions..."

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->selectFile([B)I

    move-result v3

    const v4, 0x9000

    if-ne v3, v4, :cond_0

    .line 245
    const/4 v3, 0x0

    const/4 v4, -0x1

    invoke-virtual {p0, v3, v4}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->readBinary(II)[B

    move-result-object v3

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mData:[B

    .line 246
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mData:[B

    invoke-direct {p0, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->decodeDER([B)V

    .line 263
    :goto_0
    return-void

    .line 248
    :cond_0
    const-string v3, "ACE ARF EF_ACConditions"

    const-string v4, "EF_ACConditions not found!"

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v4, "Invalid element found!"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/lang/Exception;
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 253
    .local v0, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-direct {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>()V

    .line 255
    .local v2, "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    const-string v3, "ACE ARF EF_ACConditions"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    sget-object v3, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v4, "access denied because of empty condition file"

    invoke-virtual {v0, v3, v4}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 258
    sget-object v3, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v0, v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 259
    sget-object v3, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v0, v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 260
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mAid_Ref_Do:Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    invoke-virtual {v3, v4, v2, v0}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->putAccessRule(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    goto :goto_0
.end method

.method public addRestrictedHashesFromData([B)V
    .locals 4
    .param p1, "data"    # [B

    .prologue
    .line 271
    :try_start_0
    const-string v1, "ACE ARF EF_ACConditions"

    const-string v2, "Analysing cached EF_ACConditions data..."

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    if-eqz p1, :cond_0

    .line 273
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mData:[B

    .line 274
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mData:[B

    invoke-direct {p0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->decodeDER([B)V

    .line 282
    :goto_0
    return-void

    .line 276
    :cond_0
    const-string v1, "ACE ARF EF_ACConditions"

    const-string v2, "EF_ACConditions data not available!"

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 278
    :catch_0
    move-exception v0

    .line 280
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ACE ARF EF_ACConditions"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACConditions;->mData:[B

    return-object v0
.end method
