.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "NFC_AR_DO.java"


# static fields
.field public static final _TAG:I = 0xd1


# instance fields
.field private mNfcAllowed:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 3
    .param p1, "allowed"    # Z

    .prologue
    const/4 v2, 0x0

    .line 50
    const/4 v0, 0x0

    const/16 v1, 0xd1

    invoke-direct {p0, v0, v1, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 43
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->mNfcAllowed:Z

    .line 51
    iput-boolean p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->mNfcAllowed:Z

    .line 52
    return-void
.end method

.method public constructor <init>([BII)V
    .locals 1
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    .line 46
    const/16 v0, 0xd1

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->mNfcAllowed:Z

    .line 47
    return-void
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 2
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 100
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->getTag()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 101
    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 102
    iget-boolean v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->mNfcAllowed:Z

    if-eqz v1, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 103
    return-void

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public interpret()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 71
    iput-boolean v3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->mNfcAllowed:Z

    .line 73
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->getRawData()[B

    move-result-object v0

    .line 74
    .local v0, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->getValueIndex()I

    move-result v1

    .line 76
    .local v1, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->getValueLength()I

    move-result v4

    add-int/2addr v4, v1

    array-length v5, v0

    if-le v4, v5, :cond_0

    .line 77
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Not enough data for NFC_AR_DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 80
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->getValueLength()I

    move-result v4

    if-eq v4, v2, :cond_1

    .line 81
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Invalid length of NFC-AR-DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 83
    :cond_1
    aget-byte v4, v0, v1

    if-ne v4, v2, :cond_2

    :goto_0
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->mNfcAllowed:Z

    .line 84
    return-void

    :cond_2
    move v2, v3

    .line 83
    goto :goto_0
.end method

.method public isNfcAllowed()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->mNfcAllowed:Z

    return v0
.end method
