.class Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;
.super Landroid/os/AsyncTask;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simalliance/openmobileapi/service/SmartcardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitialiseTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;


# direct methods
.method private constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;Lorg/simalliance/openmobileapi/service/SmartcardService$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/simalliance/openmobileapi/service/SmartcardService;
    .param p2, "x1"    # Lorg/simalliance/openmobileapi/service/SmartcardService$1;

    .prologue
    .line 273
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 273
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v3, 0x0

    .line 285
    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :goto_0
    return-object v3

    .line 286
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 273
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 294
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 295
    const-string v0, "SmartcardService"

    const-string v1, "OnPostExecute()"

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    # invokes: Lorg/simalliance/openmobileapi/service/SmartcardService;->registerSimStateChangedEvent(Landroid/content/Context;)V
    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$100(Lorg/simalliance/openmobileapi/service/SmartcardService;Landroid/content/Context;)V

    .line 299
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    const/4 v1, 0x0

    iput-object v1, v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->mInitialiseTask:Lorg/simalliance/openmobileapi/service/SmartcardService$InitialiseTask;

    .line 300
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 277
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 279
    return-void
.end method
