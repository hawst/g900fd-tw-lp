.class public Lorg/simalliance/openmobileapi/service/security/ApduFilter;
.super Ljava/lang/Object;
.source "ApduFilter.java"


# static fields
.field public static final LENGTH:I = 0x8


# instance fields
.field protected mApdu:[B

.field protected mMask:[B


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>([B)V
    .locals 3
    .param p1, "apduAndMask"    # [B

    .prologue
    const/4 v2, 0x4

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    array-length v0, p1

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "filter length must be 8 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0, v2}, Lorg/simalliance/openmobileapi/service/Util;->getMid([BII)[B

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mApdu:[B

    .line 59
    invoke-static {p1, v2, v2}, Lorg/simalliance/openmobileapi/service/Util;->getMid([BII)[B

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mMask:[B

    .line 60
    return-void
.end method

.method public constructor <init>([B[B)V
    .locals 2
    .param p1, "apdu"    # [B
    .param p2, "mask"    # [B

    .prologue
    const/4 v1, 0x4

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    array-length v0, p1

    if-eq v0, v1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "apdu length must be 4 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    array-length v0, p2

    if-eq v0, v1, :cond_1

    .line 38
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mask length must be 4 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mApdu:[B

    .line 42
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mMask:[B

    .line 43
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->clone()Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;-><init>()V

    .line 47
    .local v0, "apduFilter":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mApdu:[B

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->setApdu([B)V

    .line 48
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mMask:[B

    invoke-virtual {v1}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->setMask([B)V

    .line 50
    return-object v0
.end method

.method public getApdu()[B
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mApdu:[B

    return-object v0
.end method

.method public getLength()I
    .locals 1

    .prologue
    .line 95
    const/16 v0, 0x8

    return v0
.end method

.method public getMask()[B
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mMask:[B

    return-object v0
.end method

.method public setApdu([B)V
    .locals 2
    .param p1, "apdu"    # [B

    .prologue
    .line 67
    array-length v0, p1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "apdu length must be 4 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mApdu:[B

    .line 71
    return-void
.end method

.method public setMask([B)V
    .locals 2
    .param p1, "mask"    # [B

    .prologue
    .line 78
    array-length v0, p1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mask length must be 4 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mMask:[B

    .line 82
    return-void
.end method

.method public toBytes()[B
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mApdu:[B

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mMask:[B

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util;->mergeBytes([B[B)[B

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "APDU Filter [apdu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mApdu:[B

    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/ApduFilter;->mMask:[B

    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
