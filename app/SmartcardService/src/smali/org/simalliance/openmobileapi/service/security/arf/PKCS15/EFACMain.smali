.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;
.super Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.source "EFACMain.java"


# static fields
.field public static final REFRESH_TAG_LEN:S = 0x8s

.field public static final TAG:Ljava/lang/String; = "ACE ARF EF_ACMain"


# instance fields
.field private mACMainPath:[B


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;[B)V
    .locals 1
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;
    .param p2, "path"    # [B

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->mACMainPath:[B

    .line 74
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->mACMainPath:[B

    .line 75
    return-void
.end method

.method private decodeDER([B)[B
    .locals 4
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 55
    .local v0, "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    const/16 v2, 0x30

    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 56
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 57
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v3, "[Parser] RefreshTag length not valid"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 59
    :cond_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v1

    .line 60
    .local v1, "refreshTag":[B
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->getRefreshTag()[B

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_1

    .line 61
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2, v1}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->setRefreshTag([B)V

    .line 62
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parsePathAttributes()[B

    move-result-object v2

    .line 64
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public analyseFile()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 85
    const-string v1, "ACE ARF EF_ACMain"

    const-string v2, "Analysing EF_ACMain..."

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->mACMainPath:[B

    .line 100
    .local v0, "path":[B
    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->selectFile([B)I

    move-result v1

    const v2, 0x9000

    if-eq v1, v2, :cond_1

    .line 102
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->getRefreshTag()[B

    move-result-object v1

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->setRefreshTag([B)V

    .line 105
    :cond_0
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v2, "EF_ACMain not found!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 107
    :cond_1
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->readBinary(II)[B

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->decodeDER([B)[B

    move-result-object v1

    return-object v1
.end method
