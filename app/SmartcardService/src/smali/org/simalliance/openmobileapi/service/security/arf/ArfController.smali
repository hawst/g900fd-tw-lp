.class public Lorg/simalliance/openmobileapi/service/security/arf/ArfController;
.super Ljava/lang/Object;
.source "ArfController.java"


# instance fields
.field protected _TAG:Ljava/lang/String;

.field private mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

.field private mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

.field private mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

.field private mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

.field private mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;)V
    .locals 1
    .param p1, "master"    # Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    .line 52
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .line 54
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .line 55
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    .line 56
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 58
    const-string v0, "SmartcardService ACE ARF"

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    .line 61
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .line 62
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    .line 63
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getTerminal()Lorg/simalliance/openmobileapi/service/ITerminal;

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 65
    return-void
.end method


# virtual methods
.method public getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    return-object v0
.end method

.method public declared-synchronized getCdfCertsBytes()Ljava/util/Set;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    if-nez v8, :cond_0

    .line 148
    new-instance v8, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-direct {v8, p0, v9}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/ArfController;Lorg/simalliance/openmobileapi/service/ITerminal;)V

    iput-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .line 150
    :cond_0
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    if-nez v8, :cond_1

    .line 151
    new-instance v8, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v8, v9}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    iput-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :cond_1
    :try_start_1
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    invoke-virtual {v8}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->findCdf()Ljava/util/Set;

    move-result-object v1

    .line 155
    .local v1, "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 157
    .local v4, "certsByte":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    if-eqz v1, :cond_2

    .line 158
    const-string v8, "X.509"

    invoke-static {v8}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v6

    .line 159
    .local v6, "factory":Ljava/security/cert/CertificateFactory;
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 160
    .local v0, "cdfCert":[B
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cdfCert="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v0}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    new-instance v8, Ljava/io/ByteArrayInputStream;

    invoke-direct {v8, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v6, v8}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v2

    check-cast v2, Ljava/security/cert/X509Certificate;
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    .local v2, "cdfX509":Ljava/security/cert/X509Certificate;
    :try_start_2
    invoke-virtual {v2}, Ljava/security/cert/X509Certificate;->checkValidity()V

    .line 164
    invoke-virtual {v2}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v3

    .line 165
    .local v3, "cdfX509Bytes":[B
    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 166
    .end local v3    # "cdfX509Bytes":[B
    :catch_0
    move-exception v5

    .line 167
    .local v5, "e":Ljava/security/cert/CertificateExpiredException;
    :try_start_3
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v9, "getCdfCertsBytes() : specified certificate is expired"

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 174
    .end local v0    # "cdfCert":[B
    .end local v1    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v2    # "cdfX509":Ljava/security/cert/X509Certificate;
    .end local v4    # "certsByte":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v5    # "e":Ljava/security/cert/CertificateExpiredException;
    .end local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v7    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v5

    .line 175
    .local v5, "e":Ljava/security/cert/CertificateException;
    :try_start_4
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v9, "getCdfCertsBytes() : specified certificate type is not available at any provider"

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 178
    const/4 v4, 0x0

    .end local v5    # "e":Ljava/security/cert/CertificateException;
    :cond_2
    monitor-exit p0

    return-object v4

    .line 168
    .restart local v0    # "cdfCert":[B
    .restart local v1    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .restart local v2    # "cdfX509":Ljava/security/cert/X509Certificate;
    .restart local v4    # "certsByte":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .restart local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .restart local v7    # "i$":Ljava/util/Iterator;
    :catch_2
    move-exception v5

    .line 169
    .local v5, "e":Ljava/security/cert/CertificateNotYetValidException;
    :try_start_5
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v9, "getCdfCertsBytes() : specified certificate is not valid yet"

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/security/cert/CertificateException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 147
    .end local v0    # "cdfCert":[B
    .end local v1    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v2    # "cdfX509":Ljava/security/cert/X509Certificate;
    .end local v4    # "certsByte":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v5    # "e":Ljava/security/cert/CertificateNotYetValidException;
    .end local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v7    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8
.end method

.method public declared-synchronized getPkgCertBytes(Ljava/lang/String;)[B
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v8}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 184
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const/16 v8, 0x40

    invoke-virtual {v7, p1, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 186
    .local v2, "pi":Landroid/content/pm/PackageInfo;
    iget-object v8, v2, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v9, 0x0

    aget-object v3, v8, v9

    .line 187
    .local v3, "pkgSig":Landroid/content/pm/Signature;
    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v4

    .line 188
    .local v4, "pkgSigBytes":[B
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getPkgCertBytes() : pkgSigBytes="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string v8, "X.509"

    invoke-static {v8}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 190
    .local v1, "factory":Ljava/security/cert/CertificateFactory;
    new-instance v8, Ljava/io/ByteArrayInputStream;

    invoke-direct {v8, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1, v8}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v5

    check-cast v5, Ljava/security/cert/X509Certificate;

    .line 191
    .local v5, "pkgX509":Ljava/security/cert/X509Certificate;
    invoke-virtual {v5}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v6

    .line 192
    .local v6, "pkgX509Bytes":[B
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getPkgCertBytes() : pkgX509Bytes="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v6}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    .end local v1    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v2    # "pi":Landroid/content/pm/PackageInfo;
    .end local v3    # "pkgSig":Landroid/content/pm/Signature;
    .end local v4    # "pkgSigBytes":[B
    .end local v5    # "pkgX509":Ljava/security/cert/X509Certificate;
    .end local v6    # "pkgX509Bytes":[B
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    monitor-exit p0

    return-object v6

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getPkgCertBytes() : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ": package could not be found"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_1
    const/4 v6, 0x0

    goto :goto_0

    .line 196
    :catch_1
    move-exception v0

    .line 197
    .local v0, "e":Ljava/security/cert/CertificateException;
    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v9, "getPkgCertBytes() : specified certificate type is not available at any provider"

    invoke-static {v8, v9}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 183
    .end local v0    # "e":Ljava/security/cert/CertificateException;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8
.end method

.method public declared-synchronized initialize(Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    .locals 2
    .param p1, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-direct {v0, p0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/ArfController;Lorg/simalliance/openmobileapi/service/ITerminal;)V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .line 73
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    if-nez v0, :cond_1

    .line 74
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    .line 76
    :cond_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-interface {v1}, Lorg/simalliance/openmobileapi/service/ITerminal;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->loadAccessControlRules(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isCdfAllowed(Ljava/lang/String;)Z
    .locals 17
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "isCdfAllowed <<< ("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ")"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    if-nez v14, :cond_0

    .line 90
    new-instance v14, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v15}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/ArfController;Lorg/simalliance/openmobileapi/service/ITerminal;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .line 92
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    if-nez v14, :cond_1

    .line 93
    new-instance v14, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    move-object/from16 v0, p0

    iget-object v15, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mSecureElement:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v14, v15}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v14}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 98
    .local v13, "pm":Landroid/content/pm/PackageManager;
    const/16 v14, 0x40

    move-object/from16 v0, p1

    invoke-virtual {v13, v0, v14}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    .line 101
    .local v8, "pi":Landroid/content/pm/PackageInfo;
    iget-object v14, v8, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v15, 0x0

    aget-object v9, v14, v15

    .line 102
    .local v9, "pkgSig":Landroid/content/pm/Signature;
    invoke-virtual {v9}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v10

    .line 103
    .local v10, "pkgSigBytes":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "pkgSigBytes="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v10}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const-string v14, "X.509"

    invoke-static {v14}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v6

    .line 106
    .local v6, "factory":Ljava/security/cert/CertificateFactory;
    new-instance v14, Ljava/io/ByteArrayInputStream;

    invoke-direct {v14, v10}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v6, v14}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v11

    check-cast v11, Ljava/security/cert/X509Certificate;

    .line 107
    .local v11, "pkgX509":Ljava/security/cert/X509Certificate;
    invoke-virtual {v11}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v12

    .line 108
    .local v12, "pkgX509Bytes":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "pkgX509Bytes="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v12}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->mPkcs15Handler:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;

    invoke-virtual {v14}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->findCdf()Ljava/util/Set;

    move-result-object v2

    .line 111
    .local v2, "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    if-eqz v2, :cond_3

    .line 112
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 113
    .local v1, "cdfCert":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "cdfCert="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    new-instance v14, Ljava/io/ByteArrayInputStream;

    invoke-direct {v14, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v6, v14}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v3

    check-cast v3, Ljava/security/cert/X509Certificate;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    .local v3, "cdfX509":Ljava/security/cert/X509Certificate;
    :try_start_2
    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->checkValidity()V

    .line 117
    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v4

    .line 118
    .local v4, "cdfX509Bytes":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "cdfX509Bytes="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-static {v4}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-static {v12, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 121
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v15, "signatures match"

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    const/4 v14, 0x1

    .line 142
    .end local v1    # "cdfCert":[B
    .end local v2    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v3    # "cdfX509":Ljava/security/cert/X509Certificate;
    .end local v4    # "cdfX509Bytes":[B
    .end local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "pi":Landroid/content/pm/PackageInfo;
    .end local v9    # "pkgSig":Landroid/content/pm/Signature;
    .end local v10    # "pkgSigBytes":[B
    .end local v11    # "pkgX509":Ljava/security/cert/X509Certificate;
    .end local v12    # "pkgX509Bytes":[B
    .end local v13    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    monitor-exit p0

    return v14

    .line 125
    .restart local v1    # "cdfCert":[B
    .restart local v2    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .restart local v3    # "cdfX509":Ljava/security/cert/X509Certificate;
    .restart local v4    # "cdfX509Bytes":[B
    .restart local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v8    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v9    # "pkgSig":Landroid/content/pm/Signature;
    .restart local v10    # "pkgSigBytes":[B
    .restart local v11    # "pkgX509":Ljava/security/cert/X509Certificate;
    .restart local v12    # "pkgX509Bytes":[B
    .restart local v13    # "pm":Landroid/content/pm/PackageManager;
    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v15, "signatures don\'t match"

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 127
    .end local v4    # "cdfX509Bytes":[B
    :catch_0
    move-exception v5

    .line 128
    .local v5, "e":Ljava/security/cert/CertificateExpiredException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v15, "specified certificate is expired"

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 135
    .end local v1    # "cdfCert":[B
    .end local v2    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v3    # "cdfX509":Ljava/security/cert/X509Certificate;
    .end local v5    # "e":Ljava/security/cert/CertificateExpiredException;
    .end local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "pi":Landroid/content/pm/PackageInfo;
    .end local v9    # "pkgSig":Landroid/content/pm/Signature;
    .end local v10    # "pkgSigBytes":[B
    .end local v11    # "pkgX509":Ljava/security/cert/X509Certificate;
    .end local v12    # "pkgX509Bytes":[B
    .end local v13    # "pm":Landroid/content/pm/PackageManager;
    :catch_1
    move-exception v5

    .line 136
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ": package could not be found"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 142
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_2
    const/4 v14, 0x0

    goto :goto_1

    .line 129
    .restart local v1    # "cdfCert":[B
    .restart local v2    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .restart local v3    # "cdfX509":Ljava/security/cert/X509Certificate;
    .restart local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v8    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v9    # "pkgSig":Landroid/content/pm/Signature;
    .restart local v10    # "pkgSigBytes":[B
    .restart local v11    # "pkgX509":Ljava/security/cert/X509Certificate;
    .restart local v12    # "pkgX509Bytes":[B
    .restart local v13    # "pm":Landroid/content/pm/PackageManager;
    :catch_2
    move-exception v5

    .line 130
    .local v5, "e":Ljava/security/cert/CertificateNotYetValidException;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v15, "specified certificate is not valid yet"

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 137
    .end local v1    # "cdfCert":[B
    .end local v2    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v3    # "cdfX509":Ljava/security/cert/X509Certificate;
    .end local v5    # "e":Ljava/security/cert/CertificateNotYetValidException;
    .end local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "pi":Landroid/content/pm/PackageInfo;
    .end local v9    # "pkgSig":Landroid/content/pm/Signature;
    .end local v10    # "pkgSigBytes":[B
    .end local v11    # "pkgX509":Ljava/security/cert/X509Certificate;
    .end local v12    # "pkgX509Bytes":[B
    .end local v13    # "pm":Landroid/content/pm/PackageManager;
    :catch_3
    move-exception v5

    .line 138
    .local v5, "e":Ljava/security/cert/CertificateException;
    :try_start_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v15, "specified certificate type is not available at any provider"

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 87
    .end local v5    # "e":Ljava/security/cert/CertificateException;
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 134
    .restart local v2    # "cdfCerts":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .restart local v6    # "factory":Ljava/security/cert/CertificateFactory;
    .restart local v8    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v9    # "pkgSig":Landroid/content/pm/Signature;
    .restart local v10    # "pkgSigBytes":[B
    .restart local v11    # "pkgX509":Ljava/security/cert/X509Certificate;
    .restart local v12    # "pkgX509Bytes":[B
    .restart local v13    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    :try_start_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lorg/simalliance/openmobileapi/service/security/arf/ArfController;->_TAG:Ljava/lang/String;

    const-string v15, "no match has been found"

    invoke-static {v14, v15}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2
.end method
