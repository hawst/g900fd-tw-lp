.class public Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
.super Ljava/lang/Object;
.source "DERParser.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "AccessControl"


# instance fields
.field private mDERBuffer:[B

.field private mDERIndex:S

.field private mDERSize:S

.field private mTLVDataSize:S


# direct methods
.method public constructor <init>([B)V
    .locals 4
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    .line 91
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    .line 92
    iput-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iput-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    .line 93
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    if-nez v0, :cond_1

    .line 104
    :cond_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    array-length v0, v0

    int-to-short v0, v0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    .line 95
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    .line 98
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v0, v0, v1

    if-ne v0, v3, :cond_0

    .line 100
    iput-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    .line 101
    :cond_2
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    add-int/lit8 v0, v0, 0x1

    int-to-short v0, v0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    if-ge v0, v1, :cond_0

    .line 102
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v0, v0, v1

    if-eq v0, v3, :cond_2

    .line 103
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "[Parser] Incorrect file format"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getTLVSize()S
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 54
    const/4 v0, 0x0

    .line 56
    .local v0, "TLVSize":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v3, "[Parser] Cannot retreive size"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 59
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    add-int/lit8 v4, v3, 0x1

    int-to-short v4, v4

    iput-short v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v2, v2, v3

    and-int/lit16 v0, v2, 0xff

    const/16 v2, 0x80

    if-lt v0, v2, :cond_2

    .line 60
    add-int/lit8 v1, v0, -0x80

    .line 61
    .local v1, "size":I
    const/4 v0, 0x0

    :goto_0
    if-lez v1, :cond_2

    .line 62
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v2

    if-nez v2, :cond_1

    .line 63
    shl-int/lit8 v2, v0, 0x8

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    add-int/lit8 v5, v4, 0x1

    int-to-short v5, v5

    iput-short v5, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    add-int v0, v2, v3

    .line 61
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 64
    :cond_1
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v3, "[Parser] Cannot retreive size"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 68
    .end local v1    # "size":I
    :cond_2
    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    add-int/2addr v2, v0

    iget-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    if-le v2, v3, :cond_3

    .line 69
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v3, "[Parser] Not enough data"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 70
    :cond_3
    int-to-short v2, v0

    return v2
.end method

.method private getTLVType()B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "[Parser] Cannot retreive type"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    add-int/lit8 v2, v1, 0x1

    int-to-short v2, v2

    iput-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v0, v0, v1

    return v0
.end method

.method private readIntBase128()I
    .locals 4

    .prologue
    .line 40
    const/4 v0, 0x0

    .line 43
    .local v0, "value":I
    :cond_0
    shl-int/lit8 v1, v0, 0x7

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v2, v2, v3

    and-int/lit8 v2, v2, 0x7f

    add-int v0, v1, v2

    .line 44
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    add-int/lit8 v3, v2, 0x1

    int-to-short v3, v3

    iput-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_0

    .line 45
    return v0
.end method


# virtual methods
.method public getTLVData()[B
    .locals 5

    .prologue
    .line 158
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    add-int/2addr v3, v4

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 160
    .local v0, "data":[B
    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    add-int/2addr v1, v2

    int-to-short v1, v1

    iput-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    .line 161
    return-object v0
.end method

.method public isEndofBuffer()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v3, -0x1

    .line 112
    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    if-ne v1, v2, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v0

    .line 113
    :cond_1
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v1, v1, v2

    if-ne v1, v3, :cond_3

    .line 115
    :cond_2
    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    add-int/lit8 v1, v1, 0x1

    int-to-short v1, v1

    iput-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    if-ge v1, v2, :cond_0

    .line 116
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERBuffer:[B

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aget-byte v1, v1, v2

    if-eq v1, v3, :cond_2

    .line 117
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "[Parser] Incorrect file format"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parseOID()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    const/16 v5, 0x2e

    .line 193
    const/4 v3, 0x6

    invoke-virtual {p0, v3}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    move-result v3

    if-nez v3, :cond_0

    .line 194
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v4, "[Parser] OID Length is null"

    invoke-direct {v3, v4}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 196
    :cond_0
    iget-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    add-int v0, v3, v4

    .line 197
    .local v0, "end":I
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 200
    .local v1, "oid":Ljava/lang/StringBuffer;
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->readIntBase128()I

    move-result v2

    .line 203
    .local v2, "subid":I
    const/16 v3, 0x4f

    if-gt v2, v3, :cond_1

    .line 204
    div-int/lit8 v3, v2, 0x28

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    rem-int/lit8 v4, v2, 0x28

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 207
    :goto_0
    iget-short v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    if-ge v3, v0, :cond_2

    .line 208
    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->readIntBase128()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 205
    :cond_1
    const-string v3, "2."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    add-int/lit8 v4, v2, -0x50

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 209
    :cond_2
    const-string v3, "AccessControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Found OID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public parsePathAttributes()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 219
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 220
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 221
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v0

    return-object v0
.end method

.method public parseTLV()B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 128
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVType()B

    move-result v0

    .line 129
    .local v0, "type":B
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVSize()S

    move-result v1

    iput-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    .line 130
    return v0
.end method

.method public parseTLV(B)S
    .locals 2
    .param p1, "type"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVType()B

    move-result v0

    if-ne v0, p1, :cond_0

    .line 141
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVSize()S

    move-result v0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    .line 143
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    return v0

    .line 142
    :cond_0
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "[Parser] Unexpected type"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public restoreContext([S)V
    .locals 3
    .param p1, "context"    # [S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 180
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 181
    :cond_0
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "[Parser] Invalid context"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_1
    aget-short v0, p1, v2

    if-ltz v0, :cond_2

    aget-short v0, p1, v2

    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERSize:S

    if-le v0, v1, :cond_3

    .line 183
    :cond_2
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "[Parser] Index out of bound"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_3
    aget-short v0, p1, v2

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    const/4 v0, 0x1

    aget-short v0, p1, v0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    .line 185
    return-void
.end method

.method public saveContext()[S
    .locals 3

    .prologue
    .line 169
    const/4 v1, 0x2

    new-array v0, v1, [S

    .line 170
    .local v0, "context":[S
    const/4 v1, 0x0

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    aput-short v2, v0, v1

    const/4 v1, 0x1

    iget-short v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    aput-short v2, v0, v1

    .line 171
    return-object v0
.end method

.method public skipTLVData()V
    .locals 2

    .prologue
    .line 150
    iget-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    iget-short v1, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mTLVDataSize:S

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->mDERIndex:S

    .line 151
    return-void
.end method
