.class public final enum Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
.super Ljava/lang/Enum;
.source "ChannelAccess.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ACCESS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

.field public static final enum ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

.field public static final enum DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

.field public static final enum UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 23
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v1, "ALLOWED"

    invoke-direct {v0, v1, v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v1, "DENIED"

    invoke-direct {v0, v1, v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v4}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    .line 22
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    aput-object v1, v0, v2

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    aput-object v1, v0, v3

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    aput-object v1, v0, v4

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->$VALUES:[Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    return-object v0
.end method

.method public static values()[Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->$VALUES:[Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v0}, [Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    return-object v0
.end method
