.class public Lorg/simalliance/openmobileapi/service/security/ara/AraController;
.super Ljava/lang/Object;
.source "AraController.java"


# static fields
.field public static final ARA_M_AID:[B


# instance fields
.field private ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

.field private mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

.field private mApplet:Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;

.field private mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

.field private mNoSuchElement:Z

.field private mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/16 v0, 0x9

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ARA_M_AID:[B

    return-void

    :array_0
    .array-data 1
        -0x60t
        0x0t
        0x0t
        0x1t
        0x51t
        0x41t
        0x43t
        0x4ct
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;)V
    .locals 1
    .param p1, "master"    # Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .line 45
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    .line 47
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 48
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mApplet:Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mNoSuchElement:Z

    .line 53
    const-string v0, "ACE ARA"

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    .line 61
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .line 62
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAccessRuleCache()Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    .line 63
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mMaster:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getTerminal()Lorg/simalliance/openmobileapi/service/ITerminal;

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    .line 65
    return-void
.end method

.method private closeChannel(Lorg/simalliance/openmobileapi/service/IChannel;)V
    .locals 1
    .param p1, "channel"    # Lorg/simalliance/openmobileapi/service/IChannel;

    .prologue
    .line 217
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Lorg/simalliance/openmobileapi/service/IChannel;->getChannelNumber()I

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-interface {p1}, Lorg/simalliance/openmobileapi/service/IChannel;->close()V
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/CardException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 222
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static getAraMAid()[B
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ARA_M_AID:[B

    return-object v0
.end method

.method private handleOpenChannel(Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/IChannel;
    .locals 7
    .param p1, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    const/4 v6, 0x1

    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    const-string v3, ""

    .line 126
    .local v3, "reason":Ljava/lang/String;
    :try_start_0
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mTerminal:Lorg/simalliance/openmobileapi/service/ITerminal;

    invoke-static {}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->getAraMAid()[B

    move-result-object v5

    invoke-direct {p0, v4, v5, p1}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->openChannel(Lorg/simalliance/openmobileapi/service/ITerminal;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/IChannel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 156
    return-object v0

    .line 127
    :catch_0
    move-exception v1

    .line 128
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    .line 129
    .local v2, "msg":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ARA-M couldn\'t be selected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 131
    instance-of v4, v1, Ljava/util/NoSuchElementException;

    if-eqz v4, :cond_0

    .line 132
    iput-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mNoSuchElement:Z

    .line 135
    const-string v3, " No Access because ARA-M is not available"

    .line 136
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    invoke-static {v4, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    new-instance v4, Ljava/security/AccessControlException;

    invoke-direct {v4, v3}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 138
    :cond_0
    instance-of v4, v1, Ljava/util/MissingResourceException;

    if-eqz v4, :cond_1

    .line 142
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "no channels left to access ARA-M: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    check-cast v1, Ljava/util/MissingResourceException;

    .end local v1    # "e":Ljava/lang/Exception;
    throw v1

    .line 149
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_1
    iput-boolean v6, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mNoSuchElement:Z

    .line 151
    move-object v3, v2

    .line 152
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " ARA-M can not be accessed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    new-instance v4, Ljava/security/AccessControlException;

    invoke-direct {v4, v3}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method private openChannel(Lorg/simalliance/openmobileapi/service/ITerminal;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/IChannel;
    .locals 4
    .param p1, "terminal"    # Lorg/simalliance/openmobileapi/service/ITerminal;
    .param p2, "aid"    # [B
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 204
    const/4 v2, 0x0

    invoke-interface {p1, v2, p2, p3}, Lorg/simalliance/openmobileapi/service/ITerminal;->openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v1

    .line 207
    .local v1, "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 208
    .local v0, "araChannelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v2, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 209
    sget-object v2, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 210
    invoke-interface {v1, v0}, Lorg/simalliance/openmobileapi/service/IChannel;->setChannelAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 212
    return-object v1
.end method

.method private readAllAccessRules()Z
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;,
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 169
    :try_start_0
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mApplet:Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->readAllAccessRules()[B

    move-result-object v1

    .line 172
    .local v1, "data":[B
    if-nez v1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return v6

    .line 176
    :cond_1
    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_DO_Factory;->createDO([B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v5

    .line 177
    .local v5, "tlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    if-nez v5, :cond_2

    .line 178
    new-instance v6, Ljava/security/AccessControlException;

    const-string v7, "No valid data object found"

    invoke-direct {v6, v7}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 194
    .end local v1    # "data":[B
    .end local v5    # "tlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :catch_0
    move-exception v2

    .line 195
    .local v2, "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    new-instance v6, Ljava/security/AccessControlException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Parsing Data Object Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 179
    .end local v2    # "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    .restart local v1    # "data":[B
    .restart local v5    # "tlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :cond_2
    :try_start_1
    instance-of v7, v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;

    if-eqz v7, :cond_3

    .line 181
    check-cast v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;

    .end local v5    # "tlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->getRefArDos()Ljava/util/ArrayList;

    move-result-object v0

    .line 182
    .local v0, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eqz v7, :cond_0

    .line 185
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 186
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 187
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;

    .line 188
    .local v4, "ref_ar_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getRefDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    move-result-object v7

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->getArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->putWithMerge(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;)V

    goto :goto_1

    .line 192
    .end local v0    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;>;"
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;>;"
    .end local v4    # "ref_ar_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;
    .restart local v5    # "tlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :cond_3
    new-instance v6, Ljava/security/AccessControlException;

    const-string v7, "Applet returned invalid or wrong data object!"

    invoke-direct {v6, v7}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catch Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException; {:try_start_1 .. :try_end_1} :catch_0

    .line 197
    .end local v5    # "tlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    .restart local v0    # "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;>;"
    .restart local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;>;"
    :cond_4
    const/4 v6, 0x1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized initialize(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    .locals 6
    .param p1, "loadAtStartup"    # Z
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 80
    monitor-enter p0

    const/4 v0, 0x0

    .line 82
    .local v0, "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    :try_start_0
    invoke-direct {p0, p2}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->handleOpenChannel(Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/IChannel;
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 87
    :goto_0
    if-nez v0, :cond_0

    .line 88
    :try_start_1
    new-instance v3, Ljava/security/AccessControlException;

    const-string v4, "could not open channel"

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 80
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 83
    :catch_0
    move-exception v1

    .line 84
    .local v1, "e":Ljava/util/MissingResourceException;
    const/4 v0, 0x0

    goto :goto_0

    .line 93
    .end local v1    # "e":Ljava/util/MissingResourceException;
    :cond_0
    :try_start_2
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;

    invoke-direct {v3, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;-><init>(Lorg/simalliance/openmobileapi/service/IChannel;)V

    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mApplet:Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;

    .line 94
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mApplet:Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->readRefreshTag()[B

    move-result-object v2

    .line 97
    .local v2, "tag":[B
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    invoke-virtual {v3, v2}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->isRefreshTagEqual([B)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 98
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    const-string v4, "Refresh tag has not changed. Using access rules from cache."

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 99
    const/4 v3, 0x0

    .line 115
    if-eqz v0, :cond_1

    .line 116
    :try_start_3
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->closeChannel(Lorg/simalliance/openmobileapi/service/IChannel;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 118
    :cond_1
    :goto_1
    monitor-exit p0

    return v3

    .line 101
    :cond_2
    :try_start_4
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    const-string v4, "Refresh tag has changed."

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    invoke-virtual {v3, v2}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->setRefreshTag([B)V

    .line 104
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mAccessRuleCache:Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->clearCache()V

    .line 106
    if-eqz p1, :cond_3

    .line 108
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    const-string v4, "Read ARs from ARA"

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->readAllAccessRules()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 115
    :cond_3
    if-eqz v0, :cond_4

    .line 116
    :try_start_5
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->closeChannel(Lorg/simalliance/openmobileapi/service/IChannel;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 118
    :cond_4
    const/4 v3, 0x1

    goto :goto_1

    .line 111
    .end local v2    # "tag":[B
    :catch_1
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/Exception;
    :try_start_6
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->ACCESS_CONTROL_ENFORCER_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ARA error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v3, Ljava/security/AccessControlException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 115
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v3

    if-eqz v0, :cond_5

    .line 116
    :try_start_7
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->closeChannel(Lorg/simalliance/openmobileapi/service/IChannel;)V

    :cond_5
    throw v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method

.method public isNoSuchElement()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/ara/AraController;->mNoSuchElement:Z

    return v0
.end method
