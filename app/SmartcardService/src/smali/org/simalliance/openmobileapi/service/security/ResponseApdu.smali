.class public Lorg/simalliance/openmobileapi/service/security/ResponseApdu;
.super Ljava/lang/Object;
.source "ResponseApdu.java"


# instance fields
.field protected mData:[B

.field protected mSw1:I

.field protected mSw2:I


# direct methods
.method public constructor <init>([B)V
    .locals 3
    .param p1, "respApdu"    # [B

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw1:I

    .line 25
    iput v2, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw2:I

    .line 27
    new-array v0, v2, [B

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mData:[B

    .line 30
    array-length v0, p1

    if-ge v0, v1, :cond_0

    .line 40
    :goto_0
    return-void

    .line 33
    :cond_0
    array-length v0, p1

    if-le v0, v1, :cond_1

    .line 34
    array-length v0, p1

    add-int/lit8 v0, v0, -0x2

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mData:[B

    .line 35
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mData:[B

    array-length v1, p1

    add-int/lit8 v1, v1, -0x2

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 37
    :cond_1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x2

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw1:I

    .line 38
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw2:I

    goto :goto_0
.end method


# virtual methods
.method public checkLengthAndStatus(IILjava/lang/String;)V
    .locals 3
    .param p1, "length"    # I
    .param p2, "sw1sw2"    # I
    .param p3, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v0

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mData:[B

    array-length v0, v0

    if-eq v0, p1, :cond_1

    .line 61
    :cond_0
    new-instance v0, Ljava/security/AccessControlException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ResponseApdu is wrong at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    return-void
.end method

.method public checkLengthAndStatus(I[ILjava/lang/String;)V
    .locals 7
    .param p1, "length"    # I
    .param p2, "sw1sw2List"    # [I
    .param p3, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mData:[B

    array-length v4, v4

    if-eq v4, p1, :cond_0

    .line 68
    new-instance v4, Ljava/security/AccessControlException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ResponseApdu is wrong at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 70
    :cond_0
    move-object v0, p2

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_2

    aget v3, v0, v1

    .line 71
    .local v3, "sw1sw2":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v4

    if-ne v4, v3, :cond_1

    .line 72
    return-void

    .line 70
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    .end local v3    # "sw1sw2":I
    :cond_2
    new-instance v4, Ljava/security/AccessControlException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ResponseApdu is wrong at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public checkStatus(ILjava/lang/String;)V
    .locals 3
    .param p1, "sw1sw2"    # I
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 89
    new-instance v0, Ljava/security/AccessControlException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ResponseApdu is wrong at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    return-void
.end method

.method public checkStatus([ILjava/lang/String;)V
    .locals 7
    .param p1, "sw1sw2List"    # [I
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;
        }
    .end annotation

    .prologue
    .line 79
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget v3, v0, v1

    .line 80
    .local v3, "sw1sw2":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v4

    if-ne v4, v3, :cond_0

    .line 81
    return-void

    .line 79
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    .end local v3    # "sw1sw2":I
    :cond_1
    new-instance v4, Ljava/security/AccessControlException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ResponseApdu is wrong at "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mData:[B

    return-object v0
.end method

.method public getSW1()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw1:I

    return v0
.end method

.method public getSW1SW2()I
    .locals 2

    .prologue
    .line 51
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw1:I

    shl-int/lit8 v0, v0, 0x8

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw2:I

    or-int/2addr v0, v1

    return v0
.end method

.method public getSW2()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->mSw2:I

    return v0
.end method

.method public isStatus(I)Z
    .locals 1
    .param p1, "sw1sw2"    # I

    .prologue
    .line 94
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 95
    const/4 v0, 0x1

    .line 98
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
