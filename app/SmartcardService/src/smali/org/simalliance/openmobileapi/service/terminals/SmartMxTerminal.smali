.class public Lorg/simalliance/openmobileapi/service/terminals/SmartMxTerminal;
.super Lorg/simalliance/openmobileapi/service/Terminal;
.source "SmartMxTerminal.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "SmartMxTerminal"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_NFC_SetSecureEventType"

    const-string v2, "GSMA"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ISIS"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "eSE - SmartMX"

    :goto_0
    invoke-direct {p0, v0, p1}, Lorg/simalliance/openmobileapi/service/Terminal;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 38
    return-void

    .line 36
    :cond_1
    const-string v0, "eSE1"

    goto :goto_0
.end method


# virtual methods
.method protected internalCloseLogicalChannel(I)V
    .locals 0
    .param p1, "channelNumber"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 72
    return-void
.end method

.method protected internalConnect()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 47
    return-void
.end method

.method protected internalDisconnect()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 52
    return-void
.end method

.method protected internalOpenLogicalChannel()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, -0x1

    return v0
.end method

.method protected internalOpenLogicalChannel([B)I
    .locals 1
    .param p1, "aid"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, -0x1

    return v0
.end method

.method protected internalTransmit([B)[B
    .locals 1
    .param p1, "command"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public isCardPresent()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method
