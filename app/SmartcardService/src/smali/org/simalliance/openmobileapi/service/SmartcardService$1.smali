.class Lorg/simalliance/openmobileapi/service/SmartcardService$1;
.super Landroid/content/BroadcastReceiver;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/simalliance/openmobileapi/service/SmartcardService;->registerSimStateChangedEvent(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;


# direct methods
.method constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V
    .locals 0

    .prologue
    .line 307
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$1;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x5

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 310
    const-string v4, "android.intent.action.SIM_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 311
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 312
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_3

    const-string v4, "READY"

    const-string v5, "ss"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v3

    .line 313
    .local v2, "simReady":Z
    :goto_0
    if-eqz v0, :cond_0

    const-string v4, "LOADED"

    const-string v5, "ss"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v3

    .line 314
    .local v1, "simLoaded":Z
    :cond_0
    if-eqz v0, :cond_1

    const-string v4, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SIM STATE : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ss"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    :cond_1
    if-eqz v2, :cond_4

    .line 317
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$1;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimReady:Z
    invoke-static {v4}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$200(Lorg/simalliance/openmobileapi/service/SmartcardService;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 318
    const-string v4, "SmartcardService"

    const-string v5, "SIM is ready. Checking access rules for updates."

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$1;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v4}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v4

    invoke-virtual {v4, v3, v7}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->sendMessage(II)V

    .line 320
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$1;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # setter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimReady:Z
    invoke-static {v4, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$202(Lorg/simalliance/openmobileapi/service/SmartcardService;Z)Z

    .line 334
    .end local v0    # "extras":Landroid/os/Bundle;
    .end local v1    # "simLoaded":Z
    .end local v2    # "simReady":Z
    :cond_2
    :goto_1
    return-void

    .restart local v0    # "extras":Landroid/os/Bundle;
    :cond_3
    move v2, v1

    .line 312
    goto :goto_0

    .line 324
    .restart local v1    # "simLoaded":Z
    .restart local v2    # "simReady":Z
    :cond_4
    if-eqz v1, :cond_2

    .line 326
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$1;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimLoaded:Z
    invoke-static {v4}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$400(Lorg/simalliance/openmobileapi/service/SmartcardService;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 327
    const-string v4, "SmartcardService"

    const-string v5, "SIM is loaded. Checking access rules for updates."

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$1;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v4}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v4

    invoke-virtual {v4, v3, v7}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->sendMessage(II)V

    .line 329
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$1;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # setter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->isAcInitializedForSimLoaded:Z
    invoke-static {v4, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$402(Lorg/simalliance/openmobileapi/service/SmartcardService;Z)Z

    goto :goto_1
.end method
