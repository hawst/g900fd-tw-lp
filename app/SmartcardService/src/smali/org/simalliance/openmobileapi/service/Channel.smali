.class public Lorg/simalliance/openmobileapi/service/Channel;
.super Ljava/lang/Object;
.source "Channel.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;
.implements Lorg/simalliance/openmobileapi/service/IChannel;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;
    }
.end annotation


# instance fields
.field protected mAid:[B

.field protected final mBinder:Landroid/os/IBinder;

.field protected mCallback:Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

.field protected mCallingPid:I

.field protected mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

.field protected final mChannelNumber:I

.field protected mHandle:J

.field protected mHasSelectedAid:Z

.field protected mIsClosed:Z

.field protected mSelectResponse:[B

.field protected mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

.field protected mTerminal:Lorg/simalliance/openmobileapi/service/Terminal;


# direct methods
.method constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/Terminal;ILorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)V
    .locals 3
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "terminal"    # Lorg/simalliance/openmobileapi/service/Terminal;
    .param p3, "channelNumber"    # I
    .param p4, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .line 59
    iput v2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mCallingPid:I

    .line 64
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mHasSelectedAid:Z

    .line 65
    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    .line 68
    iput p3, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelNumber:I

    .line 69
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    .line 70
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mTerminal:Lorg/simalliance/openmobileapi/service/Terminal;

    .line 71
    iput-object p4, p0, Lorg/simalliance/openmobileapi/service/Channel;->mCallback:Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .line 72
    invoke-interface {p4}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mBinder:Landroid/os/IBinder;

    .line 73
    invoke-virtual {p2}, Lorg/simalliance/openmobileapi/service/Terminal;->getSelectResponse()[B

    move-result-object v1

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    .line 74
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mIsClosed:Z

    .line 76
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mBinder:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SmartcardService"

    const-string v2, "Failed to register client callback"

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkCommand([B)V
    .locals 3
    .param p1, "command"    # [B

    .prologue
    .line 313
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v0

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Terminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 316
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v0

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Terminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->checkCommand(Lorg/simalliance/openmobileapi/service/IChannel;[B)V

    .line 320
    return-void

    .line 318
    :cond_0
    new-instance v0, Ljava/security/AccessControlException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FATAL: Access Controller not set for Terminal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v2

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/Terminal;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private setChannelToClassByte(BI)B
    .locals 3
    .param p1, "cla"    # B
    .param p2, "channelNumber"    # I

    .prologue
    .line 281
    const/4 v1, 0x4

    if-ge p2, v1, :cond_1

    .line 283
    and-int/lit16 v1, p1, 0xbc

    or-int/2addr v1, p2

    int-to-byte p1, v1

    .line 293
    :cond_0
    :goto_0
    return p1

    .line 284
    :cond_1
    const/16 v1, 0x14

    if-ge p2, v1, :cond_3

    .line 286
    and-int/lit8 v1, p1, 0xc

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 287
    .local v0, "isSM":Z
    :goto_1
    and-int/lit16 v1, p1, 0xb0

    or-int/lit8 v1, v1, 0x40

    add-int/lit8 v2, p2, -0x4

    or-int/2addr v1, v2

    int-to-byte p1, v1

    .line 288
    if-eqz v0, :cond_0

    or-int/lit8 v1, p1, 0x20

    int-to-byte p1, v1

    goto :goto_0

    .line 286
    .end local v0    # "isSM":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 290
    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Channel number must be within [0..19]"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .prologue
    .line 85
    :try_start_0
    const-string v0, "SmartcardService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Client "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mBinder:Landroid/os/IBinder;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " died"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v2

    .line 96
    .local v2, "terminal":Lorg/simalliance/openmobileapi/service/Terminal;
    if-nez v2, :cond_0

    .line 97
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "channel is not attached to a terminal"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    .end local v2    # "terminal":Lorg/simalliance/openmobileapi/service/Terminal;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 100
    .restart local v2    # "terminal":Lorg/simalliance/openmobileapi/service/Terminal;
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->isBasicChannel()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->hasSelectedAid()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 102
    :try_start_2
    const-string v3, "SmartcardService"

    const-string v4, "Close basic channel - Select with out AID ..."

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/Terminal;->select()V
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 123
    :cond_1
    :goto_0
    :try_start_3
    invoke-virtual {v2, p0}, Lorg/simalliance/openmobileapi/service/Terminal;->closeChannel(Lorg/simalliance/openmobileapi/service/Channel;)V

    .line 124
    const/4 v3, 0x1

    iput-boolean v3, p0, Lorg/simalliance/openmobileapi/service/Channel;->mIsClosed:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 126
    :try_start_4
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Channel;->mBinder:Landroid/os/IBinder;

    const/4 v4, 0x0

    invoke-interface {v3, p0, v4}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 128
    monitor-exit p0

    return-void

    .line 104
    :catch_0
    move-exception v1

    .line 108
    .local v1, "exp":Ljava/util/NoSuchElementException;
    :try_start_5
    const-string v3, "SmartcardService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Close basic channel - Exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/util/NoSuchElementException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/Terminal;->getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    move-result-object v0

    .line 110
    .local v0, "access":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    if-eqz v0, :cond_1

    .line 111
    sget-object v3, Lorg/simalliance/openmobileapi/service/SmartcardService;->accessControlType:Ljava/lang/String;

    const-string v4, "GPAC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    invoke-static {}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getDefaultAccessControlAid()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/Terminal;->select([B)V
    :try_end_5
    .catch Ljava/util/NoSuchElementException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 115
    .end local v0    # "access":Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    :catch_1
    move-exception v3

    goto :goto_0

    .line 126
    .end local v1    # "exp":Ljava/util/NoSuchElementException;
    :catchall_1
    move-exception v3

    :try_start_6
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/Channel;->mBinder:Landroid/os/IBinder;

    const/4 v5, 0x0

    invoke-interface {v4, p0, v5}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public getCallback()Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mCallback:Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    return-object v0
.end method

.method public getChannelAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    return-object v0
.end method

.method public getChannelNumber()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelNumber:I

    return v0
.end method

.method getHandle()J
    .locals 2

    .prologue
    .line 153
    iget-wide v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mHandle:J

    return-wide v0
.end method

.method public getSelectResponse()[B
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    return-object v0
.end method

.method public bridge synthetic getTerminal()Lorg/simalliance/openmobileapi/service/ITerminal;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v0

    return-object v0
.end method

.method public getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mTerminal:Lorg/simalliance/openmobileapi/service/Terminal;

    return-object v0
.end method

.method public hasSelectedAid(Z[B)V
    .locals 0
    .param p1, "has"    # Z
    .param p2, "aid"    # [B

    .prologue
    .line 336
    iput-boolean p1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mHasSelectedAid:Z

    .line 337
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    .line 338
    return-void
.end method

.method public hasSelectedAid()Z
    .locals 1

    .prologue
    .line 329
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mHasSelectedAid:Z

    return v0
.end method

.method public isBasicChannel()Z
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelNumber:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isClosed()Z
    .locals 1

    .prologue
    .line 356
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mIsClosed:Z

    return v0
.end method

.method public selectNext()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v9, 0x1

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 220
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    if-nez v0, :cond_0

    .line 221
    new-instance v0, Ljava/security/AccessControlException;

    const-string v2, " Channel access not set."

    invoke-direct {v0, v2}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 223
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getCallingPid()I

    move-result v0

    iget v4, p0, Lorg/simalliance/openmobileapi/service/Channel;->mCallingPid:I

    if-eq v0, v4, :cond_1

    .line 227
    new-instance v0, Ljava/security/AccessControlException;

    const-string v2, " Wrong Caller PID. "

    invoke-direct {v0, v2}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    array-length v0, v0

    if-nez v0, :cond_3

    .line 232
    :cond_2
    new-instance v0, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v2, " no aid given"

    invoke-direct {v0, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    .line 236
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x5

    new-array v1, v0, [B

    .line 237
    .local v1, "selectCommand":[B
    aput-byte v3, v1, v3

    .line 238
    const/16 v0, -0x5c

    aput-byte v0, v1, v9

    .line 239
    aput-byte v5, v1, v2

    .line 240
    const/4 v0, 0x3

    aput-byte v2, v1, v0

    .line 241
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    array-length v0, v0

    int-to-byte v0, v0

    aput-byte v0, v1, v5

    .line 242
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    const/4 v4, 0x5

    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/Channel;->mAid:[B

    array-length v5, v5

    invoke-static {v0, v3, v1, v4, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 245
    aget-byte v0, v1, v3

    iget v4, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelNumber:I

    invoke-direct {p0, v0, v4}, Lorg/simalliance/openmobileapi/service/Channel;->setChannelToClassByte(BI)B

    move-result v0

    aput-byte v0, v1, v3

    .line 247
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v0

    const-string v5, "SELECT NEXT"

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/Terminal;->transmit([BIIILjava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    .line 251
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x2

    aget-byte v0, v0, v2

    and-int/lit16 v7, v0, 0xff

    .line 252
    .local v7, "sw1":I
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Channel;->mSelectResponse:[B

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-byte v0, v0, v2

    and-int/lit16 v8, v0, 0xff

    .line 253
    .local v8, "sw2":I
    shl-int/lit8 v0, v7, 0x8

    or-int v6, v0, v8

    .line 255
    .local v6, "sw":I
    const v0, 0xf000

    and-int/2addr v0, v6

    const v2, 0x9000

    if-ne v0, v2, :cond_5

    move v3, v9

    .line 258
    :cond_4
    return v3

    .line 257
    :cond_5
    const/16 v0, 0x6a82

    if-eq v6, v0, :cond_4

    .line 260
    new-instance v0, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v2, " invalid action"

    invoke-direct {v0, v2}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setCallingPid(I)V
    .locals 0
    .param p1, "pid"    # I

    .prologue
    .line 309
    iput p1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mCallingPid:I

    .line 310
    return-void
.end method

.method public setChannelAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V
    .locals 0
    .param p1, "channelAccess"    # Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .prologue
    .line 298
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .line 299
    return-void
.end method

.method setClosed()V
    .locals 1

    .prologue
    .line 361
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mIsClosed:Z

    .line 362
    return-void
.end method

.method setHandle(J)V
    .locals 1
    .param p1, "handle"    # J

    .prologue
    .line 171
    iput-wide p1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mHandle:J

    .line 172
    return-void
.end method

.method public transmit([B)[B
    .locals 7
    .param p1, "command"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 176
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Ljava/security/AccessControlException;

    const-string v1, " Channel access not set."

    invoke-direct {v0, v1}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelAccess:Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getCallingPid()I

    move-result v0

    iget v1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mCallingPid:I

    if-eq v0, v1, :cond_1

    .line 183
    new-instance v0, Ljava/security/AccessControlException;

    const-string v1, " Wrong Caller PID. "

    invoke-direct {v0, v1}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_1
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/Channel;->checkCommand([B)V

    .line 191
    array-length v0, p1

    if-ge v0, v5, :cond_2

    .line 192
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, " command must not be smaller than 4 bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 195
    :cond_2
    aget-byte v0, p1, v3

    and-int/lit8 v0, v0, -0x80

    if-nez v0, :cond_4

    aget-byte v0, p1, v3

    and-int/lit8 v0, v0, 0x60

    int-to-byte v0, v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_4

    .line 198
    aget-byte v0, p1, v4

    const/16 v1, 0x70

    if-ne v0, v1, :cond_3

    .line 199
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MANAGE CHANNEL command not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_3
    aget-byte v0, p1, v4

    const/16 v1, -0x5c

    if-ne v0, v1, :cond_4

    aget-byte v0, p1, v2

    if-ne v0, v5, :cond_4

    .line 203
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SELECT command not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_4
    aget-byte v0, p1, v3

    iget v1, p0, Lorg/simalliance/openmobileapi/service/Channel;->mChannelNumber:I

    invoke-direct {p0, v0, v1}, Lorg/simalliance/openmobileapi/service/Channel;->setChannelToClassByte(BI)B

    move-result v0

    aput-byte v0, p1, v3

    .line 213
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel;->getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;

    move-result-object v0

    const/4 v5, 0x0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/Terminal;->transmit([BIIILjava/lang/String;)[B

    move-result-object v6

    .line 215
    .local v6, "rsp":[B
    return-object v6
.end method
