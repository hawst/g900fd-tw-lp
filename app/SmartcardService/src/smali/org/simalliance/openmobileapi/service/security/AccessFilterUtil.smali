.class public Lorg/simalliance/openmobileapi/service/security/AccessFilterUtil;
.super Ljava/lang/Object;
.source "AccessFilterUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parseAccessConditions([B)[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .locals 7
    .param p0, "accessConditions"    # [B

    .prologue
    .line 26
    array-length v5, p0

    if-nez v5, :cond_1

    .line 27
    const/4 v5, 0x0

    new-array v0, v5, [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .line 44
    :cond_0
    return-object v0

    .line 30
    :cond_1
    array-length v5, p0

    rem-int/lit8 v5, v5, 0x8

    if-eqz v5, :cond_2

    .line 31
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Access Conditions must have a length of 8 bytes"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 34
    :cond_2
    array-length v5, p0

    div-int/lit8 v3, v5, 0x8

    .line 35
    .local v3, "numOfACs":I
    new-array v0, v3, [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .line 36
    .local v0, "acs":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    const/4 v4, 0x0

    .line 37
    .local v4, "offset":I
    const/16 v2, 0x8

    .line 38
    .local v2, "length":I
    const/4 v1, 0x0

    .line 39
    .local v1, "index":I
    :goto_0
    add-int v5, v4, v2

    array-length v6, p0

    if-gt v5, v6, :cond_0

    if-eqz v2, :cond_0

    .line 40
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    invoke-static {p0, v4, v2}, Lorg/simalliance/openmobileapi/service/Util;->getMid([BII)[B

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;-><init>([B)V

    aput-object v5, v0, v1

    .line 41
    add-int/2addr v4, v2

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
