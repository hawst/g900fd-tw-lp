.class Lorg/simalliance/openmobileapi/service/SmartcardService$2;
.super Landroid/content/BroadcastReceiver;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/simalliance/openmobileapi/service/SmartcardService;->registerAdapterStateChangedEvent(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;


# direct methods
.method constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;)V
    .locals 0

    .prologue
    .line 352
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$2;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    .line 355
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.nfc.action.ADAPTER_STATE_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 356
    .local v0, "nfcAdapterAction":Z
    if-eqz v0, :cond_1

    const-string v2, "android.nfc.extra.ADAPTER_STATE"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    .line 357
    .local v1, "nfcAdapterOn":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 358
    const-string v2, "SmartcardService"

    const-string v3, "NFC Adapter is ON. Checking access rules for updates."

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$2;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->sendMessage(II)V

    .line 361
    :cond_0
    return-void

    .line 356
    .end local v1    # "nfcAdapterOn":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
