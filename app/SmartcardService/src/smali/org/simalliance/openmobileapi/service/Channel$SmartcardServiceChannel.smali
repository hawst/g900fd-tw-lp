.class final Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;
.super Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel$Stub;
.source "Channel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simalliance/openmobileapi/service/Channel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "SmartcardServiceChannel"
.end annotation


# instance fields
.field private final mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/Channel;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/Channel;Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;)V
    .locals 0
    .param p2, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    .prologue
    .line 372
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel$Stub;-><init>()V

    .line 373
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    .line 374
    return-void
.end method


# virtual methods
.method public close(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    .locals 5
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 379
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 380
    sget-object v2, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v2

    .line 382
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/Channel;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 386
    :try_start_1
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    if-eqz v1, :cond_0

    .line 387
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v1, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->removeChannel(Lorg/simalliance/openmobileapi/service/Channel;)V

    .line 389
    :cond_0
    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 390
    return-void

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-static {p1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 386
    :try_start_3
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    if-eqz v1, :cond_0

    .line 387
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v1, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->removeChannel(Lorg/simalliance/openmobileapi/service/Channel;)V

    goto :goto_0

    .line 389
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    .line 386
    :catchall_1
    move-exception v1

    :try_start_4
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    if-eqz v3, :cond_1

    .line 387
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->mSession:Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v3, v4}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->removeChannel(Lorg/simalliance/openmobileapi/service/Channel;)V

    :cond_1
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public getSelectResponse()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->getSelectResponse()[B

    move-result-object v0

    return-object v0
.end method

.method public getSession()Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 416
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/Channel;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    return-object v0
.end method

.method public isBasicChannel()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 404
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->isBasicChannel()Z

    move-result v0

    return v0
.end method

.method public isClosed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 398
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->isClosed()Z

    move-result v0

    return v0
.end method

.method public selectNext(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z
    .locals 4
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 464
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 467
    :try_start_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 468
    const-class v2, Ljava/lang/IllegalStateException;

    const-string v3, "channel is closed"

    invoke-static {p1, v2, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    .line 479
    :goto_0
    return v1

    .line 473
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/Channel;->setCallingPid(I)V

    .line 475
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/Channel;->selectNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 476
    .local v1, "response":Z
    goto :goto_0

    .line 477
    .end local v1    # "response":Z
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public setClosed()V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Channel;->setClosed()V

    .line 394
    return-void
.end method

.method public transmit([BLorg/simalliance/openmobileapi/service/SmartcardError;)[B
    .locals 5
    .param p1, "command"    # [B
    .param p2, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 422
    invoke-static {p2}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 425
    :try_start_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 426
    const-class v2, Ljava/lang/IllegalStateException;

    const-string v3, "channel is closed"

    invoke-static {p2, v2, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V

    .line 457
    :goto_0
    return-object v1

    .line 430
    :cond_0
    if-nez p1, :cond_1

    .line 431
    const-class v2, Ljava/lang/NullPointerException;

    const-string v3, "command must not be null"

    invoke-static {p2, v2, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SmartcardService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "transmit Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (Command: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    invoke-static {p2, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    goto :goto_0

    .line 434
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    array-length v2, p1

    const/4 v3, 0x4

    if-ge v2, v3, :cond_2

    .line 435
    const-class v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "command must have at least 4 bytes"

    invoke-static {p2, v2, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 442
    :cond_2
    :try_start_2
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    invoke-direct {v2, p1}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;-><init>([B)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 448
    :goto_1
    :try_start_3
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/Channel;->setCallingPid(I)V

    .line 451
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Channel$SmartcardServiceChannel;->this$0:Lorg/simalliance/openmobileapi/service/Channel;

    invoke-virtual {v2, p1}, Lorg/simalliance/openmobileapi/service/Channel;->transmit([B)[B

    move-result-object v1

    .line 453
    .local v1, "response":[B
    goto :goto_0

    .line 443
    .end local v1    # "response":[B
    :catch_1
    move-exception v0

    .line 444
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 445
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v2

    goto :goto_1
.end method
