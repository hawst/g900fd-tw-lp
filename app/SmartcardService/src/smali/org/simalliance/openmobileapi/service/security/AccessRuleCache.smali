.class public Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;
.super Ljava/lang/Object;
.source "AccessRuleCache.java"


# instance fields
.field private mRefreshTag:[B

.field private mRuleCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;",
            "Lorg/simalliance/openmobileapi/service/security/ChannelAccess;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    return-void
.end method

.method public static buildHashMapKey([B[B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .locals 3
    .param p0, "aid"    # [B
    .param p1, "appCertHash"    # [B

    .prologue
    .line 284
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-direct {v0, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>([B)V

    .line 285
    .local v0, "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-static {p0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->getAidRefDo([B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;-><init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;)V

    .line 287
    .local v1, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    return-object v1
.end method

.method public static getAidRefDo([B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    .locals 3
    .param p0, "aid"    # [B

    .prologue
    .line 270
    const/4 v0, 0x0

    .line 271
    .local v0, "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    const/4 v2, 0x5

    new-array v1, v2, [B

    fill-array-data v1, :array_0

    .line 273
    .local v1, "defaultAid":[B
    if-eqz p0, :cond_0

    invoke-static {p0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 274
    :cond_0
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .end local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    const/16 v2, 0xc0

    invoke-direct {v0, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;-><init>(I)V

    .line 279
    .restart local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    :goto_0
    return-object v0

    .line 276
    :cond_1
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .end local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    const/16 v2, 0x4f

    invoke-direct {v0, v2, p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;-><init>(I[B)V

    .restart local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    goto :goto_0

    .line 271
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public static mapArDo2ChannelAccess(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 8
    .param p0, "ar_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .prologue
    .line 367
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 370
    .local v2, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getApduArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 372
    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 373
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setUseApduFilter(Z)V

    .line 375
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getApduArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    move-result-object v5

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->isApduAllowed()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 377
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getApduArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    move-result-object v5

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getApduHeaderList()Ljava/util/ArrayList;

    move-result-object v1

    .line 378
    .local v1, "apduHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getApduArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;

    move-result-object v5

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/APDU_AR_DO;->getFilterMaskList()Ljava/util/ArrayList;

    move-result-object v3

    .line 379
    .local v3, "filterMasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    if-eqz v1, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 384
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .line 385
    .local v0, "accessConditions":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 386
    new-instance v7, Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    invoke-direct {v7, v5, v6}, Lorg/simalliance/openmobileapi/service/security/ApduFilter;-><init>([B[B)V

    aput-object v7, v0, v4

    .line 385
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 388
    :cond_0
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setUseApduFilter(Z)V

    .line 389
    invoke-virtual {v2, v0}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    .line 403
    .end local v0    # "accessConditions":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v1    # "apduHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v3    # "filterMasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v4    # "i":I
    :goto_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getNfcArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 404
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;->getNfcArDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;

    move-result-object v5

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/NFC_AR_DO;->isNfcAllowed()Z

    move-result v5

    if-eqz v5, :cond_5

    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    :goto_2
    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 415
    :cond_1
    :goto_3
    return-object v2

    .line 392
    .restart local v1    # "apduHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .restart local v3    # "filterMasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_2
    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    goto :goto_1

    .line 396
    .end local v1    # "apduHeaders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    .end local v3    # "filterMasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    :cond_3
    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    goto :goto_1

    .line 399
    :cond_4
    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v6, "No APDU access rule available.!"

    invoke-virtual {v2, v5, v6}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    goto :goto_1

    .line 404
    :cond_5
    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    goto :goto_2

    .line 407
    :cond_6
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v5

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 410
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->isUseApduFilter()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 411
    sget-object v5, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    goto :goto_3
.end method

.method private searchForRulesWithAllAidButOtherHash()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 347
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    const/16 v4, 0x4f

    invoke-direct {v0, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;-><init>(I)V

    .line 349
    .local v0, "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 350
    .local v2, "keySet":Ljava/util/Set;, "Ljava/util/Set<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 351
    .local v1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;>;"
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 352
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 353
    .local v3, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getAidDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 355
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    move-result-object v4

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getHash()[B

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    move-result-object v4

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getHash()[B

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_0

    .line 363
    .end local v3    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private searchForRulesWithSpecificAidButOtherHash(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .locals 6
    .param p1, "aid_ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .prologue
    const/4 v3, 0x0

    .line 307
    if-nez p1, :cond_0

    move-object v2, v3

    .line 330
    :goto_0
    return-object v2

    .line 312
    :cond_0
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getTag()I

    move-result v4

    const/16 v5, 0x4f

    if-ne v4, v5, :cond_2

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getAid()[B

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getAid()[B

    move-result-object v4

    array-length v4, v4

    if-nez v4, :cond_2

    :cond_1
    move-object v2, v3

    .line 314
    goto :goto_0

    .line 317
    :cond_2
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    .line 318
    .local v1, "keySet":Ljava/util/Set;, "Ljava/util/Set<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 319
    .local v0, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;>;"
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 320
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    .line 321
    .local v2, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getAidDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    move-result-object v4

    invoke-virtual {p1, v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 322
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    move-result-object v4

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getHash()[B

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    move-result-object v4

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getHash()[B

    move-result-object v4

    array-length v4, v4

    if-lez v4, :cond_3

    goto :goto_0

    .end local v2    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :cond_4
    move-object v2, v3

    .line 330
    goto :goto_0
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 64
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 10
    .param p1, "writer"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    .line 435
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "SmartcardService"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 436
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 439
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Current refresh tag is: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 440
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    if-nez v6, :cond_1

    const-string v6, "<null>"

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 442
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 445
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "rules dump:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 446
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 448
    const/4 v2, 0x0

    .line 449
    .local v2, "i":I
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 450
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    add-int/lit8 v2, v2, 0x1

    .line 451
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "rule "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 452
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-virtual {v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 454
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  ->"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 455
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-virtual {v6}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 441
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    .local v0, "arr$":[B
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-byte v5, v0, v3

    .local v5, "oneByte":B
    const-string v6, "%02X:"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {p1, v6, v7}, Ljava/io/PrintWriter;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 458
    .end local v0    # "arr$":[B
    .end local v4    # "len$":I
    .end local v5    # "oneByte":B
    .restart local v2    # "i":I
    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 459
    return-void
.end method

.method public findAccessRule([B[Ljava/security/cert/Certificate;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 13
    .param p1, "aid"    # [B
    .param p2, "appCerts"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;
        }
    .end annotation

    .prologue
    const/16 v12, 0x4f

    .line 157
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->getAidRefDo([B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    move-result-object v0

    .line 158
    .local v0, "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    const/4 v5, 0x0

    .line 159
    .local v5, "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    const/4 v9, 0x0

    .line 163
    .local v9, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v2, p2

    .local v2, "arr$":[Ljava/security/cert/Certificate;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    move-object v10, v9

    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .local v10, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v6, v5

    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .local v6, "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v1, v2, v7

    .line 166
    .local v1, "appCert":Ljava/security/cert/Certificate;
    :try_start_0
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAppCertHash(Ljava/security/cert/Certificate;)[B

    move-result-object v11

    invoke-direct {v5, v11}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>([B)V
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :try_start_1
    new-instance v9, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-direct {v9, v0, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;-><init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;)V
    :try_end_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1 .. :try_end_1} :catch_4

    .line 169
    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :try_start_2
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 170
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :try_end_2
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_2 .. :try_end_2} :catch_5

    .line 234
    .end local v1    # "appCert":Ljava/security/cert/Certificate;
    :goto_1
    return-object v11

    .line 172
    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v1    # "appCert":Ljava/security/cert/Certificate;
    .restart local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :catch_0
    move-exception v4

    move-object v9, v10

    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v5, v6

    .line 173
    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .local v4, "e":Ljava/security/cert/CertificateEncodingException;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :goto_2
    new-instance v11, Ljava/security/AccessControlException;

    const-string v12, "Problem with Application Certificate."

    invoke-direct {v11, v12}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 163
    .end local v4    # "e":Ljava/security/cert/CertificateEncodingException;
    :cond_0
    add-int/lit8 v7, v7, 0x1

    move-object v10, v9

    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v6, v5

    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    goto :goto_0

    .line 179
    .end local v1    # "appCert":Ljava/security/cert/Certificate;
    :cond_1
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->searchForRulesWithSpecificAidButOtherHash(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 180
    const-string v11, "SmartcardService"

    const-string v12, "Conflict Resolution Case A returning access rule \'NEVER\'."

    invoke-static {v11, v12}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 182
    .local v3, "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v3, v11}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 183
    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v12, "AID has a specific access rule with a different hash. (Case A)"

    invoke-virtual {v3, v11, v12}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 184
    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v3, v11}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    move-object v9, v10

    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v5, v6

    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    move-object v11, v3

    .line 185
    goto :goto_1

    .line 190
    .end local v3    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :cond_2
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->getAidRefDo([B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    move-result-object v0

    .line 191
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-direct {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>()V

    .line 192
    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    new-instance v9, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-direct {v9, v0, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;-><init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;)V

    .line 194
    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 195
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    goto :goto_1

    .line 199
    :cond_3
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .end local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    invoke-direct {v0, v12}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;-><init>(I)V

    .line 200
    .restart local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    move-object v2, p2

    array-length v8, v2

    const/4 v7, 0x0

    move-object v10, v9

    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v6, v5

    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :goto_3
    if-ge v7, v8, :cond_5

    aget-object v1, v2, v7

    .line 202
    .restart local v1    # "appCert":Ljava/security/cert/Certificate;
    :try_start_3
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAppCertHash(Ljava/security/cert/Certificate;)[B

    move-result-object v11

    invoke-direct {v5, v11}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>([B)V
    :try_end_3
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_3 .. :try_end_3} :catch_1

    .line 203
    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :try_start_4
    new-instance v9, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-direct {v9, v0, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;-><init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;)V
    :try_end_4
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_4 .. :try_end_4} :catch_2

    .line 205
    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :try_start_5
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 206
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :try_end_5
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_1

    .line 208
    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :catch_1
    move-exception v4

    move-object v9, v10

    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v5, v6

    .line 209
    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v4    # "e":Ljava/security/cert/CertificateEncodingException;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    :goto_4
    new-instance v11, Ljava/security/AccessControlException;

    const-string v12, "Problem with Application Certificate."

    invoke-direct {v11, v12}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 200
    .end local v4    # "e":Ljava/security/cert/CertificateEncodingException;
    :cond_4
    add-int/lit8 v7, v7, 0x1

    move-object v10, v9

    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v6, v5

    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    goto :goto_3

    .line 216
    .end local v1    # "appCert":Ljava/security/cert/Certificate;
    :cond_5
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->searchForRulesWithAllAidButOtherHash()Ljava/lang/Object;

    move-result-object v11

    if-eqz v11, :cond_6

    .line 217
    const-string v11, "SmartcardService"

    const-string v12, "Conflict Resolution Case C returning access rule \'NEVER\'."

    invoke-static {v11, v12}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    invoke-direct {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;-><init>()V

    .line 219
    .restart local v3    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v3, v11}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 220
    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    const-string v12, "An access rule with a different hash and all AIDs was found. (Case C)"

    invoke-virtual {v3, v11, v12}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;Ljava/lang/String;)V

    .line 221
    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    invoke-virtual {v3, v11}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    move-object v9, v10

    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    move-object v5, v6

    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    move-object v11, v3

    .line 222
    goto/16 :goto_1

    .line 227
    .end local v3    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .end local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :cond_6
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .end local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    invoke-direct {v0, v12}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;-><init>(I)V

    .line 228
    .restart local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    invoke-direct {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;-><init>()V

    .line 229
    .end local v6    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    .restart local v5    # "hash_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;
    new-instance v9, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    invoke-direct {v9, v0, v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;-><init>(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;)V

    .line 231
    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 232
    iget-object v11, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v11, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    goto/16 :goto_1

    .line 234
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 208
    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v1    # "appCert":Ljava/security/cert/Certificate;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :catch_2
    move-exception v4

    move-object v9, v10

    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    goto :goto_4

    :catch_3
    move-exception v4

    goto :goto_4

    .line 172
    .end local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    :catch_4
    move-exception v4

    move-object v9, v10

    .end local v10    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .restart local v9    # "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    goto/16 :goto_2

    :catch_5
    move-exception v4

    goto/16 :goto_2
.end method

.method public getRefreshTag()[B
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    return-object v0
.end method

.method public declared-synchronized hasGsmaPermission([Ljava/security/cert/Certificate;)Z
    .locals 14
    .param p1, "appCerts"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;
        }
    .end annotation

    .prologue
    .line 239
    monitor-enter p0

    const/4 v10, 0x0

    .line 240
    .local v10, "ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    const/4 v4, 0x0

    .line 241
    .local v4, "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    const/4 v2, 0x0

    .line 242
    .local v2, "appCertHash":[B
    const/4 v11, 0x0

    .line 244
    .local v11, "tempCertHash":[B
    :try_start_0
    iget-object v12, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    .line 245
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;

    move-object v10, v0

    .line 246
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-object v4, v0

    .line 247
    invoke-virtual {v10}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->getHashDo()Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;

    move-result-object v12

    invoke-virtual {v12}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Hash_REF_DO;->getHash()[B

    move-result-object v11

    .line 250
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v12

    sget-object v13, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-ne v12, v13, :cond_0

    .line 251
    move-object v3, p1

    .local v3, "arr$":[Ljava/security/cert/Certificate;
    array-length v9, v3

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v1, v3, v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    .local v1, "appCert":Ljava/security/cert/Certificate;
    :try_start_1
    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->getAppCertHash(Ljava/security/cert/Certificate;)[B
    :try_end_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 258
    :try_start_2
    invoke-static {v11, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v12

    if-nez v12, :cond_1

    const/4 v12, 0x0

    invoke-static {v11, v12}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v12

    if-eqz v12, :cond_2

    .line 260
    :cond_1
    const/4 v12, 0x1

    .line 265
    .end local v1    # "appCert":Ljava/security/cert/Certificate;
    .end local v3    # "arr$":[Ljava/security/cert/Certificate;
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    :goto_1
    monitor-exit p0

    return v12

    .line 254
    .restart local v1    # "appCert":Ljava/security/cert/Certificate;
    .restart local v3    # "arr$":[Ljava/security/cert/Certificate;
    .restart local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    .restart local v8    # "i$":I
    .restart local v9    # "len$":I
    :catch_0
    move-exception v5

    .line 255
    .local v5, "e":Ljava/security/cert/CertificateEncodingException;
    :try_start_3
    new-instance v12, Ljava/security/AccessControlException;

    const-string v13, "Problem with Application Certificate."

    invoke-direct {v12, v13}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v12
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 239
    .end local v1    # "appCert":Ljava/security/cert/Certificate;
    .end local v3    # "arr$":[Ljava/security/cert/Certificate;
    .end local v5    # "e":Ljava/security/cert/CertificateEncodingException;
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    :catchall_0
    move-exception v12

    monitor-exit p0

    throw v12

    .line 251
    .restart local v1    # "appCert":Ljava/security/cert/Certificate;
    .restart local v3    # "arr$":[Ljava/security/cert/Certificate;
    .restart local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    .restart local v8    # "i$":I
    .restart local v9    # "len$":I
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 265
    .end local v1    # "appCert":Ljava/security/cert/Certificate;
    .end local v3    # "arr$":[Ljava/security/cert/Certificate;
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;>;"
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    :cond_3
    const/4 v12, 0x0

    goto :goto_1
.end method

.method public isRefreshTagEqual([B)Z
    .locals 1
    .param p1, "refreshTag"    # [B

    .prologue
    .line 419
    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    if-nez v0, :cond_1

    .line 420
    :cond_0
    const/4 v0, 0x0

    .line 422
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    invoke-static {p1, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public put(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 2
    .param p1, "ref_do_key"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .param p2, "ar_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .prologue
    .line 68
    invoke-static {p2}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mapArDo2ChannelAccess(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v0

    .line 69
    .local v0, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    return-object v0
.end method

.method public putWithMerge(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V
    .locals 13
    .param p1, "ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .param p2, "channelAccess"    # Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .prologue
    .line 81
    iget-object v10, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v10, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 82
    iget-object v10, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v10, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    .line 83
    .local v1, "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    const-string v10, "SmartcardService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Access Rule with "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " already exists."

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getNFCEventAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v10

    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-eq v10, v11, :cond_0

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getNFCEventAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v10

    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-ne v10, v11, :cond_1

    .line 97
    :cond_0
    invoke-virtual {p2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getNFCEventAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v10

    invoke-virtual {v1, v10}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setNFCEventAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 108
    :cond_1
    invoke-virtual {p2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v10

    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->DENIED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-eq v10, v11, :cond_2

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v10

    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->UNDEFINED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-ne v10, v11, :cond_3

    .line 110
    :cond_2
    invoke-virtual {p2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v10

    invoke-virtual {v1, v10}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduAccess(Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;)V

    .line 115
    :cond_3
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    move-result-object v10

    sget-object v11, Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;->ALLOWED:Lorg/simalliance/openmobileapi/service/security/ChannelAccess$ACCESS;

    if-ne v10, v11, :cond_b

    .line 116
    invoke-virtual {p2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->isUseApduFilter()Z

    move-result v10

    if-eqz v10, :cond_5

    .line 117
    const/4 v10, 0x1

    invoke-virtual {v1, v10}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setUseApduFilter(Z)V

    .line 118
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduFilter()[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    move-result-object v3

    .line 119
    .local v3, "filter":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    invoke-virtual {p2}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getApduFilter()[Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    move-result-object v4

    .line 120
    .local v4, "filter2":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    if-eqz v3, :cond_4

    array-length v10, v3

    if-nez v10, :cond_6

    .line 121
    :cond_4
    invoke-virtual {v1, v4}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    .line 141
    .end local v3    # "filter":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v4    # "filter2":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    :cond_5
    :goto_0
    const-string v10, "SmartcardService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Merged Access Rule: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    .end local v1    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :goto_1
    return-void

    .line 122
    .restart local v1    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .restart local v3    # "filter":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .restart local v4    # "filter2":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    :cond_6
    if-eqz v4, :cond_7

    array-length v10, v4

    if-nez v10, :cond_8

    .line 123
    :cond_7
    invoke-virtual {v1, v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    goto :goto_0

    .line 125
    :cond_8
    array-length v10, v3

    array-length v11, v4

    add-int/2addr v10, v11

    new-array v9, v10, [Lorg/simalliance/openmobileapi/service/security/ApduFilter;

    .line 126
    .local v9, "sum":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    const/4 v5, 0x0

    .line 127
    .local v5, "i":I
    move-object v0, v3

    .local v0, "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    move v6, v5

    .end local v5    # "i":I
    .local v6, "i":I
    :goto_2
    if-ge v7, v8, :cond_9

    aget-object v2, v0, v7

    .line 128
    .local v2, "f":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "i":I
    .restart local v5    # "i":I
    aput-object v2, v9, v6

    .line 127
    add-int/lit8 v7, v7, 0x1

    move v6, v5

    .end local v5    # "i":I
    .restart local v6    # "i":I
    goto :goto_2

    .line 130
    .end local v2    # "f":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    :cond_9
    move-object v0, v4

    array-length v8, v0

    const/4 v7, 0x0

    :goto_3
    if-ge v7, v8, :cond_a

    aget-object v2, v0, v7

    .line 131
    .restart local v2    # "f":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    add-int/lit8 v5, v6, 0x1

    .end local v6    # "i":I
    .restart local v5    # "i":I
    aput-object v2, v9, v6

    .line 130
    add-int/lit8 v7, v7, 0x1

    move v6, v5

    .end local v5    # "i":I
    .restart local v6    # "i":I
    goto :goto_3

    .line 133
    .end local v2    # "f":Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    :cond_a
    invoke-virtual {v1, v9}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    goto :goto_0

    .line 138
    .end local v0    # "arr$":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v3    # "filter":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v4    # "filter2":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    .end local v6    # "i":I
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "sum":[Lorg/simalliance/openmobileapi/service/security/ApduFilter;
    :cond_b
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setUseApduFilter(Z)V

    .line 139
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->setApduFilter([Lorg/simalliance/openmobileapi/service/security/ApduFilter;)V

    goto :goto_0

    .line 144
    .end local v1    # "ca":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    :cond_c
    iget-object v10, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v10, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public putWithMerge(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;)V
    .locals 1
    .param p1, "ref_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;
    .param p2, "ar_do"    # Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;

    .prologue
    .line 75
    invoke-static {p2}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mapArDo2ChannelAccess(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AR_DO;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v0

    .line 76
    .local v0, "channelAccess":Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    invoke-virtual {p0, p1, v0}, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->putWithMerge(Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_DO;Lorg/simalliance/openmobileapi/service/security/ChannelAccess;)V

    .line 77
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    .line 56
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRuleCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 57
    return-void
.end method

.method public setRefreshTag([B)V
    .locals 0
    .param p1, "refreshTag"    # [B

    .prologue
    .line 430
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/AccessRuleCache;->mRefreshTag:[B

    .line 431
    return-void
.end method
