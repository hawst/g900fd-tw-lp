.class public Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;
.super Ljava/lang/Object;
.source "AccessRuleApplet.java"


# static fields
.field private static final ACCESS_RULE_APPLET_TAG:Ljava/lang/String; = "AccessRuleApplet"

.field private static final _MAX_LEN:I

.field private static final mGetAll:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

.field private static final mGetNext:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

.field private static final mGetRefreshTag:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

.field private static final mGetSpecific:Lorg/simalliance/openmobileapi/service/security/CommandApdu;


# instance fields
.field private mChannel:Lorg/simalliance/openmobileapi/service/IChannel;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v3, 0xff

    const/16 v2, 0xca

    const/16 v1, 0x80

    const/4 v5, 0x0

    .line 45
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    const/16 v4, 0x40

    invoke-direct/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;-><init>(IIIII)V

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetAll:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    .line 47
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    const/16 v4, 0x50

    invoke-direct/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;-><init>(IIIII)V

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetSpecific:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    .line 49
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    const/16 v4, 0x60

    invoke-direct/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;-><init>(IIIII)V

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetNext:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    .line 51
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    const/16 v3, 0xdf

    const/16 v4, 0x20

    invoke-direct/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;-><init>(IIIII)V

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetRefreshTag:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    return-void
.end method

.method public constructor <init>(Lorg/simalliance/openmobileapi/service/IChannel;)V
    .locals 2
    .param p1, "channel"    # Lorg/simalliance/openmobileapi/service/IChannel;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    .line 57
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "salesCode":Ljava/lang/String;
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    .line 59
    return-void
.end method

.method private send(Lorg/simalliance/openmobileapi/service/security/CommandApdu;)Lorg/simalliance/openmobileapi/service/security/ResponseApdu;
    .locals 4
    .param p1, "cmdApdu"    # Lorg/simalliance/openmobileapi/service/security/CommandApdu;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 219
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->toBytes()[B

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/simalliance/openmobileapi/service/IChannel;->transmit([B)[B

    move-result-object v1

    .line 221
    .local v1, "response":[B
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;-><init>([B)V

    .line 222
    .local v0, "resApdu":Lorg/simalliance/openmobileapi/service/security/ResponseApdu;
    return-object v0
.end method


# virtual methods
.method public readAllAccessRules()[B
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;,
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const v10, 0x9000

    .line 128
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 129
    .local v5, "stream":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 132
    .local v3, "overallLen":I
    sget-object v7, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetAll:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->clone()Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    move-result-object v0

    .line 133
    .local v0, "apdu":Lorg/simalliance/openmobileapi/service/security/CommandApdu;
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->send(Lorg/simalliance/openmobileapi/service/security/CommandApdu;)Lorg/simalliance/openmobileapi/service/security/ResponseApdu;

    move-result-object v4

    .line 136
    .local v4, "response":Lorg/simalliance/openmobileapi/service/security/ResponseApdu;
    invoke-virtual {v4, v10}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->isStatus(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 139
    const/4 v6, 0x0

    .line 141
    .local v6, "tempTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :try_start_0
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getData()[B

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BIZ)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 147
    invoke-virtual {v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v7

    invoke-virtual {v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v8

    add-int v3, v7, v8

    .line 151
    :try_start_1
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getData()[B

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 158
    :goto_0
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v7

    if-ge v7, v3, :cond_2

    .line 159
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v7

    sub-int v2, v3, v7

    .line 161
    .local v2, "le":I
    if-lez v2, :cond_0

    .line 162
    const/4 v2, 0x0

    .line 165
    :cond_0
    sget-object v7, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetNext:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->clone()Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    move-result-object v0

    .line 166
    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->setLe(I)V

    .line 168
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->send(Lorg/simalliance/openmobileapi/service/security/CommandApdu;)Lorg/simalliance/openmobileapi/service/security/ResponseApdu;

    move-result-object v4

    .line 170
    invoke-virtual {v4, v10}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->isStatus(I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 172
    :try_start_2
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getData()[B

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 173
    :catch_0
    move-exception v1

    .line 174
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (next) IO problem. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 142
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "le":I
    :catch_1
    move-exception v1

    .line 143
    .local v1, "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "GET DATA (all) not successfull. Tlv encoding wrong."

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 152
    .end local v1    # "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    :catch_2
    move-exception v1

    .line 153
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (all) IO problem. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 177
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "le":I
    :cond_1
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (next) not successfull, . SW1SW2="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 181
    .end local v2    # "le":I
    :cond_2
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    .line 184
    .end local v6    # "tempTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :goto_1
    return-object v7

    .line 183
    :cond_3
    const/16 v7, 0x6a88

    invoke-virtual {v4, v7}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->isStatus(I)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 184
    const/4 v7, 0x0

    goto :goto_1

    .line 186
    :cond_4
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (all) not successfull. SW1SW2="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method public readRefreshTag()[B
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;,
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 193
    sget-object v6, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetRefreshTag:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    invoke-virtual {v6}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->clone()Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    move-result-object v1

    .line 194
    .local v1, "apdu":Lorg/simalliance/openmobileapi/service/security/CommandApdu;
    invoke-direct {p0, v1}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->send(Lorg/simalliance/openmobileapi/service/security/CommandApdu;)Lorg/simalliance/openmobileapi/service/security/ResponseApdu;

    move-result-object v4

    .line 197
    .local v4, "response":Lorg/simalliance/openmobileapi/service/security/ResponseApdu;
    const v6, 0x9000

    invoke-virtual {v4, v6}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->isStatus(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 200
    const/4 v5, 0x0

    .line 203
    .local v5, "tempTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :try_start_0
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getData()[B

    move-result-object v6

    invoke-static {v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_DO_Factory;->createDO([B)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v5

    .line 204
    instance-of v6, v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;

    if-eqz v6, :cond_0

    .line 205
    move-object v0, v5

    check-cast v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;

    move-object v3, v0

    .line 206
    .local v3, "refreshDo":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;->getRefreshTagArray()[B

    move-result-object v6

    return-object v6

    .line 208
    .end local v3    # "refreshDo":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_RefreshTag_DO;
    :cond_0
    new-instance v6, Ljava/security/AccessControlException;

    const-string v7, "GET REFRESH TAG returned invalid Tlv."

    invoke-direct {v6, v7}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :catch_0
    move-exception v2

    .line 211
    .local v2, "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    new-instance v6, Ljava/security/AccessControlException;

    const-string v7, "GET REFRESH TAG not successfull. Tlv encoding wrong."

    invoke-direct {v6, v7}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 214
    .end local v2    # "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    .end local v5    # "tempTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :cond_1
    new-instance v6, Ljava/security/AccessControlException;

    const-string v7, "GET REFRESH TAG not successfull."

    invoke-direct {v6, v7}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method public readSpecificAccessRule([B)[B
    .locals 11
    .param p1, "aid_ref_do"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/AccessControlException;,
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const v10, 0x9000

    .line 63
    if-nez p1, :cond_0

    .line 64
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "GET DATA (specific): Reference data object must not be null."

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 67
    :cond_0
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 68
    .local v5, "stream":Ljava/io/ByteArrayOutputStream;
    const/4 v3, 0x0

    .line 71
    .local v3, "overallLen":I
    sget-object v7, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetSpecific:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->clone()Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    move-result-object v0

    .line 72
    .local v0, "apdu":Lorg/simalliance/openmobileapi/service/security/CommandApdu;
    invoke-virtual {v0, p1}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->setData([B)V

    .line 73
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->send(Lorg/simalliance/openmobileapi/service/security/CommandApdu;)Lorg/simalliance/openmobileapi/service/security/ResponseApdu;

    move-result-object v4

    .line 76
    .local v4, "response":Lorg/simalliance/openmobileapi/service/security/ResponseApdu;
    invoke-virtual {v4, v10}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->isStatus(I)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 78
    const/4 v6, 0x0

    .line 80
    .local v6, "tempTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :try_start_0
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getData()[B

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BIZ)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 86
    invoke-virtual {v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v7

    invoke-virtual {v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v8

    add-int v3, v7, v8

    .line 88
    :try_start_1
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getData()[B

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 95
    :goto_0
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v7

    if-ge v7, v3, :cond_3

    .line 96
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v7

    sub-int v2, v3, v7

    .line 97
    .local v2, "le":I
    if-lez v2, :cond_1

    .line 98
    const/4 v2, 0x0

    .line 101
    :cond_1
    sget-object v7, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->mGetNext:Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->clone()Lorg/simalliance/openmobileapi/service/security/CommandApdu;

    move-result-object v0

    .line 102
    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/CommandApdu;->setLe(I)V

    .line 103
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/ara/AccessRuleApplet;->send(Lorg/simalliance/openmobileapi/service/security/CommandApdu;)Lorg/simalliance/openmobileapi/service/security/ResponseApdu;

    move-result-object v4

    .line 106
    invoke-virtual {v4, v10}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->isStatus(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 108
    :try_start_2
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getData()[B

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v1

    .line 110
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (next) IO problem. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 81
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "le":I
    :catch_1
    move-exception v1

    .line 82
    .local v1, "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    new-instance v7, Ljava/security/AccessControlException;

    const-string v8, "GET DATA (specific) not successfull. Tlv encoding wrong."

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 89
    .end local v1    # "e":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
    :catch_2
    move-exception v1

    .line 90
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (specific) IO problem. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 113
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "le":I
    :cond_2
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (next) not successfull, . SW1SW2="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 117
    .end local v2    # "le":I
    :cond_3
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    .line 120
    .end local v6    # "tempTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :goto_1
    return-object v7

    .line 119
    :cond_4
    const/16 v7, 0x6a88

    invoke-virtual {v4, v7}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->isStatus(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 120
    const/4 v7, 0x0

    goto :goto_1

    .line 122
    :cond_5
    new-instance v7, Ljava/security/AccessControlException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "GET DATA (specific) not successfull. SW1SW2="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/ResponseApdu;->getSW1SW2()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v7
.end method
