.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.super Ljava/lang/Object;
.source "BerTlv.java"


# instance fields
.field private mRawData:[B

.field private mTag:I

.field private mValueIndex:I

.field private mValueLength:I


# direct methods
.method public constructor <init>([BIII)V
    .locals 2
    .param p1, "rawData"    # [B
    .param p2, "tag"    # I
    .param p3, "valueIndex"    # I
    .param p4, "valueLength"    # I

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    .line 26
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    .line 28
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    .line 29
    iput v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    .line 32
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    .line 33
    iput p2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    .line 34
    iput p3, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    .line 35
    iput p4, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    .line 36
    return-void
.end method

.method public static decode([BI)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    .locals 1
    .param p0, "data"    # [B
    .param p1, "startIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    .line 51
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BIZ)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v0

    return-object v0
.end method

.method public static decode([BIZ)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    .locals 8
    .param p0, "data"    # [B
    .param p1, "startIndex"    # I
    .param p2, "containsAllData"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    const/16 v7, 0x80

    .line 57
    if-eqz p0, :cond_0

    array-length v5, p0

    if-nez v5, :cond_1

    .line 58
    :cond_0
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "No data given!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 61
    :cond_1
    move v0, p1

    .line 62
    .local v0, "curIndex":I
    const/4 v3, 0x0

    .line 65
    .local v3, "tag":I
    array-length v5, p0

    if-ge v0, v5, :cond_4

    .line 66
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "curIndex":I
    .local v1, "curIndex":I
    aget-byte v5, p0, v0

    and-int/lit16 v4, v5, 0xff

    .line 67
    .local v4, "temp":I
    sparse-switch v4, :sswitch_data_0

    .line 78
    move v3, v4

    move v0, v1

    .line 87
    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    :goto_0
    array-length v5, p0

    if-ge v0, v5, :cond_f

    .line 88
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "curIndex":I
    .restart local v1    # "curIndex":I
    aget-byte v5, p0, v0

    and-int/lit16 v4, v5, 0xff

    .line 89
    if-ge v4, v7, :cond_5

    .line 90
    move v2, v4

    .local v2, "length":I
    move v0, v1

    .line 141
    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    :cond_2
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    invoke-direct {v5, p0, v3, v0, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    return-object v5

    .line 70
    .end local v0    # "curIndex":I
    .end local v2    # "length":I
    .restart local v1    # "curIndex":I
    :sswitch_0
    array-length v5, p0

    if-ge v1, v5, :cond_3

    .line 71
    and-int/lit16 v5, v4, 0xff

    shl-int/lit8 v5, v5, 0x8

    add-int/lit8 v0, v1, 0x1

    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    aget-byte v6, p0, v1

    and-int/lit16 v6, v6, 0xff

    or-int v3, v5, v6

    goto :goto_0

    .line 73
    .end local v0    # "curIndex":I
    .restart local v1    # "curIndex":I
    :cond_3
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Index "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " out of range! [0..["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 82
    .end local v1    # "curIndex":I
    .end local v4    # "temp":I
    .restart local v0    # "curIndex":I
    :cond_4
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Index "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " out of range! [0..["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 91
    .end local v0    # "curIndex":I
    .restart local v1    # "curIndex":I
    .restart local v4    # "temp":I
    :cond_5
    const/16 v5, 0x81

    if-ne v4, v5, :cond_8

    .line 92
    array-length v5, p0

    if-ge v1, v5, :cond_7

    .line 93
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    aget-byte v5, p0, v1

    and-int/lit16 v2, v5, 0xff

    .line 94
    .restart local v2    # "length":I
    if-ge v2, v7, :cond_6

    .line 95
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Invalid TLV length encoding!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 97
    :cond_6
    if-eqz p2, :cond_2

    array-length v5, p0

    add-int v6, v2, v0

    if-ge v5, v6, :cond_2

    .line 99
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Not enough data provided!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 102
    .end local v0    # "curIndex":I
    .end local v2    # "length":I
    .restart local v1    # "curIndex":I
    :cond_7
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Index "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " out of range! [0..["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 104
    :cond_8
    const/16 v5, 0x82

    if-ne v4, v5, :cond_b

    .line 105
    add-int/lit8 v5, v1, 0x1

    array-length v6, p0

    if-ge v5, v6, :cond_9

    .line 106
    aget-byte v5, p0, v1

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    add-int/lit8 v6, v1, 0x1

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    or-int v2, v5, v6

    .line 110
    .restart local v2    # "length":I
    add-int/lit8 v0, v1, 0x2

    .line 111
    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    const/16 v5, 0x100

    if-ge v2, v5, :cond_a

    .line 112
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Invalid TLV length encoding!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 108
    .end local v0    # "curIndex":I
    .end local v2    # "length":I
    .restart local v1    # "curIndex":I
    :cond_9
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Index out of range! [0..["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 114
    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    .restart local v2    # "length":I
    :cond_a
    if-eqz p2, :cond_2

    array-length v5, p0

    add-int v6, v2, v0

    if-ge v5, v6, :cond_2

    .line 116
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Not enough data provided!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 118
    .end local v0    # "curIndex":I
    .end local v2    # "length":I
    .restart local v1    # "curIndex":I
    :cond_b
    const/16 v5, 0x83

    if-ne v4, v5, :cond_e

    .line 119
    add-int/lit8 v5, v1, 0x2

    array-length v6, p0

    if-ge v5, v6, :cond_c

    .line 120
    aget-byte v5, p0, v1

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    add-int/lit8 v6, v1, 0x1

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    add-int/lit8 v6, v1, 0x2

    aget-byte v6, p0, v6

    and-int/lit16 v6, v6, 0xff

    or-int v2, v5, v6

    .line 126
    .restart local v2    # "length":I
    add-int/lit8 v0, v1, 0x3

    .line 127
    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    const/high16 v5, 0x10000

    if-ge v2, v5, :cond_d

    .line 128
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Invalid TLV length encoding!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 124
    .end local v0    # "curIndex":I
    .end local v2    # "length":I
    .restart local v1    # "curIndex":I
    :cond_c
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Index out of range! [0..["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 130
    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    .restart local v2    # "length":I
    :cond_d
    if-eqz p2, :cond_2

    array-length v5, p0

    add-int v6, v2, v0

    if-ge v5, v6, :cond_2

    .line 132
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Not enough data provided!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 135
    .end local v0    # "curIndex":I
    .end local v2    # "length":I
    .restart local v1    # "curIndex":I
    :cond_e
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v6, "Unsupported TLV length encoding!"

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 138
    .end local v1    # "curIndex":I
    .restart local v0    # "curIndex":I
    :cond_f
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Index "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " out of range! [0..["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p0

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0xdf -> :sswitch_0
        0xff -> :sswitch_0
    .end sparse-switch
.end method

.method public static encodeLength(ILjava/io/ByteArrayOutputStream;)V
    .locals 2
    .param p0, "length"    # I
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;

    .prologue
    const v1, 0xff00

    .line 219
    const v0, 0xffff

    if-le p0, v0, :cond_0

    .line 220
    const/16 v0, 0x83

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 221
    const/high16 v0, 0xff0000

    and-int/2addr v0, p0

    shr-int/lit8 v0, v0, 0x10

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 222
    and-int v0, p0, v1

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 223
    and-int/lit16 v0, p0, 0xff

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 234
    :goto_0
    return-void

    .line 224
    :cond_0
    const/16 v0, 0xff

    if-le p0, v0, :cond_1

    .line 225
    const/16 v0, 0x82

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 226
    and-int v0, p0, v1

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 227
    and-int/lit16 v0, p0, 0xff

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 228
    :cond_1
    const/16 v0, 0x7f

    if-le p0, v0, :cond_2

    .line 229
    const/16 v0, 0x81

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 230
    and-int/lit16 v0, p0, 0xff

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 232
    :cond_2
    and-int/lit16 v0, p0, 0xff

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method

.method public static toHex([B)Ljava/lang/String;
    .locals 8
    .param p0, "digest"    # [B

    .prologue
    .line 39
    const-string v3, "0123456789abcdef"

    .line 40
    .local v3, "digits":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    array-length v7, p0

    mul-int/lit8 v7, v7, 0x2

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 41
    .local v6, "sb":Ljava/lang/StringBuilder;
    move-object v0, p0

    .local v0, "arr$":[B
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-byte v1, v0, v4

    .line 42
    .local v1, "b":B
    and-int/lit16 v2, v1, 0xff

    .line 43
    .local v2, "bi":I
    shr-int/lit8 v7, v2, 0x4

    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 44
    and-int/lit8 v7, v2, 0xf

    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 41
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 46
    .end local v1    # "b":B
    .end local v2    # "bi":I
    :cond_0
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 3
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    .line 161
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    const/16 v1, 0xff

    if-le v0, v1, :cond_1

    .line 162
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 163
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 169
    :goto_0
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    invoke-static {v0, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->encodeLength(ILjava/io/ByteArrayOutputStream;)V

    .line 172
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    if-lez v0, :cond_0

    .line 173
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 175
    :cond_0
    return-void

    .line 165
    :cond_1
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 238
    const/4 v1, 0x0

    .line 240
    .local v1, "equals":Z
    instance-of v4, p1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    if-eqz v4, :cond_0

    move-object v0, p1

    .line 241
    check-cast v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    .line 243
    .local v0, "berTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    iget v4, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    iget v5, v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 245
    :goto_0
    if-eqz v1, :cond_0

    .line 246
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValue()[B

    move-result-object v2

    .line 247
    .local v2, "test1":[B
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValue()[B

    move-result-object v3

    .line 249
    .local v3, "test2":[B
    if-eqz v2, :cond_2

    .line 251
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    and-int/2addr v1, v4

    .line 257
    .end local v0    # "berTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    .end local v2    # "test1":[B
    .end local v3    # "test2":[B
    :cond_0
    :goto_1
    return v1

    .line 243
    .restart local v0    # "berTlv":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 252
    .restart local v2    # "test1":[B
    .restart local v3    # "test2":[B
    :cond_2
    if-nez v2, :cond_0

    if-nez v3, :cond_0

    .line 253
    and-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected getRawData()[B
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    return-object v0
.end method

.method public getTag()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mTag:I

    return v0
.end method

.method public getValue()[B
    .locals 5

    .prologue
    .line 189
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    if-eqz v1, :cond_0

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    if-eqz v1, :cond_0

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    if-ltz v1, :cond_0

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    array-length v2, v2

    if-gt v1, v2, :cond_0

    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    array-length v2, v2

    if-le v1, v2, :cond_1

    .line 193
    :cond_0
    const/4 v0, 0x0

    .line 199
    :goto_0
    return-object v0

    .line 195
    :cond_1
    iget v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    new-array v0, v1, [B

    .line 197
    .local v0, "data":[B
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mRawData:[B

    iget v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    const/4 v3, 0x0

    iget v4, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0
.end method

.method public getValueIndex()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueIndex:I

    return v0
.end method

.method public getValueLength()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->mValueLength:I

    return v0
.end method

.method public interpret()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    .line 147
    return-void
.end method
