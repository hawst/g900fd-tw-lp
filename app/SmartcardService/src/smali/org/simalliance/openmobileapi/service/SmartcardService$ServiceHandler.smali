.class final Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
.super Landroid/os/Handler;
.source "SmartcardService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simalliance/openmobileapi/service/SmartcardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/SmartcardService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    .line 1209
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    .line 1210
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1211
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1221
    const/4 v2, 0x1

    .line 1223
    .local v2, "result":Z
    sget-object v4, Lorg/simalliance/openmobileapi/service/SmartcardService;->mLockOpenAC:Ljava/lang/Object;

    monitor-enter v4

    .line 1224
    :try_start_0
    const-string v3, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Handle msg: what="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " nbTries="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v3

    if-nez v3, :cond_0

    .line 1228
    const-string v3, "SmartcardService"

    const-string v5, "handleMessage: mServiceHandler is null!!"

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1229
    monitor-exit v4

    .line 1273
    :goto_0
    return-void

    .line 1233
    :cond_0
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 1266
    :cond_1
    :goto_1
    if-nez v2, :cond_2

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-lez v3, :cond_2

    .line 1268
    const-string v3, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Fail to load rules: Let\'s try another time ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " remaining attempt"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1269
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v3

    iget v5, p1, Landroid/os/Message;->what:I

    iget v6, p1, Landroid/os/Message;->arg1:I

    add-int/lit8 v6, v6, -0x1

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    .line 1270
    .local v1, "newMsg":Landroid/os/Message;
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v3

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v1, v6, v7}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1272
    .end local v1    # "newMsg":Landroid/os/Message;
    :cond_2
    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1236
    :pswitch_0
    :try_start_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    const/4 v5, 0x1

    sget-object v6, Lorg/simalliance/openmobileapi/service/SmartcardService;->_UICC_TERMINAL:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(ZLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z

    move-result v2

    .line 1238
    const-string v3, "SmartcardService"

    const-string v5, "Cache cdfCert Start "

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;
    invoke-static {v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$700(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Set;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1240
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/SmartcardService;->getCdfCerts()Ljava/util/Set;

    move-result-object v5

    # setter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;
    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$702(Lorg/simalliance/openmobileapi/service/SmartcardService;Ljava/util/Set;)Ljava/util/Set;

    .line 1241
    const-string v3, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cache cdfCert End. Total Cnt : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->cachedCdfCertsBytes:Ljava/util/Set;
    invoke-static {v6}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$700(Lorg/simalliance/openmobileapi/service/SmartcardService;)Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    .line 1244
    :catch_0
    move-exception v0

    .line 1245
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got exception:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 1251
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_3
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    const/4 v5, 0x1

    sget-object v6, Lorg/simalliance/openmobileapi/service/SmartcardService;->_eSE_TERMINAL:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(ZLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    goto/16 :goto_1

    .line 1252
    :catch_1
    move-exception v0

    .line 1253
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_4
    const-string v3, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got exception:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1259
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_2
    :try_start_5
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    const/4 v5, 0x1

    sget-object v6, Lorg/simalliance/openmobileapi/service/SmartcardService;->_SD_TERMINAL:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v3, v5, v6, v7}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(ZLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    goto/16 :goto_1

    .line 1260
    :catch_2
    move-exception v0

    .line 1261
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_6
    const-string v3, "SmartcardService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got exception:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 1233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public sendMessage(II)V
    .locals 3
    .param p1, "what"    # I
    .param p2, "nbTries"    # I

    .prologue
    .line 1214
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->removeMessages(I)V

    .line 1215
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 1216
    .local v0, "newMsg":Landroid/os/Message;
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->this$0:Lorg/simalliance/openmobileapi/service/SmartcardService;

    # getter for: Lorg/simalliance/openmobileapi/service/SmartcardService;->mServiceHandler:Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;
    invoke-static {v1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->access$300(Lorg/simalliance/openmobileapi/service/SmartcardService;)Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1217
    return-void
.end method
