.class public interface abstract Lorg/simalliance/openmobileapi/service/ITerminal;
.super Ljava/lang/Object;
.source "ITerminal.java"


# virtual methods
.method public abstract closeChannels()V
.end method

.method public abstract dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
.end method

.method public abstract getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
.end method

.method public abstract getAtr()[B
.end method

.method public abstract getChannel(J)Lorg/simalliance/openmobileapi/service/IChannel;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSelectResponse()[B
.end method

.method public abstract initializeAccessControl(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
.end method

.method public abstract isCardPresent()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method

.method public abstract isConnected()Z
.end method

.method public abstract openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method

.method public abstract openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method public abstract resetAccessControl()V
.end method

.method public abstract select()V
.end method

.method public abstract select([B)V
.end method

.method public abstract setUpChannelAccess(Landroid/content/pm/PackageManager;[BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
.end method

.method public abstract simIOExchange(ILjava/lang/String;[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method
