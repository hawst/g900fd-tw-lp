.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "Response_ALL_AR_DO.java"


# static fields
.field public static final _TAG:I = 0xff40


# instance fields
.field private mRefArDos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>([BII)V
    .locals 1
    .param p1, "rawData"    # [B
    .param p2, "valueIndex"    # I
    .param p3, "valueLength"    # I

    .prologue
    .line 41
    const v0, 0xff40

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->mRefArDos:Ljava/util/ArrayList;

    .line 42
    return-void
.end method


# virtual methods
.method public getRefArDos()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->mRefArDos:Ljava/util/ArrayList;

    return-object v0
.end method

.method public interpret()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->mRefArDos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 67
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->getRawData()[B

    move-result-object v1

    .line 68
    .local v1, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->getValueIndex()I

    move-result v3

    .line 70
    .local v3, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->getValueLength()I

    move-result v6

    if-nez v6, :cond_0

    .line 99
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->getValueLength()I

    move-result v6

    add-int/2addr v6, v3

    array-length v7, v1

    if-le v6, v7, :cond_1

    .line 76
    new-instance v6, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v7, "Not enough data for Response_AR_DO!"

    invoke-direct {v6, v7}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 80
    :cond_1
    move v0, v3

    .line 81
    .local v0, "currentPos":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->getValueLength()I

    move-result v6

    add-int v2, v3, v6

    .line 83
    .local v2, "endPos":I
    :cond_2
    invoke-static {v1, v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->decode([BI)Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;

    move-result-object v4

    .line 87
    .local v4, "temp":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getTag()I

    move-result v6

    const/16 v7, 0xe2

    if-ne v6, v7, :cond_3

    .line 88
    new-instance v5, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v6

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v7

    invoke-direct {v5, v1, v6, v7}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;-><init>([BII)V

    .line 89
    .local v5, "tempRefArDo":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;
    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;->interpret()V

    .line 90
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/Response_ALL_AR_DO;->mRefArDos:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    .end local v5    # "tempRefArDo":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/REF_AR_DO;
    :cond_3
    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueIndex()I

    move-result v6

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->getValueLength()I

    move-result v7

    add-int v0, v6, v7

    .line 98
    if-lt v0, v2, :cond_2

    goto :goto_0
.end method
