.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;
.super Ljava/lang/Object;
.source "PKCS15Handler.java"


# static fields
.field public static final CONTAINER_AIDS_CMCC:[[B

.field public static final CONTAINER_AIDS_DEFAULT:[[B

.field public static final CONTAINER_AIDS_SIMIO:[[B

.field public static final GPAC_ARF_AID:[B

.field public static final PKCS15_AID:[B

.field public static final TAG:Ljava/lang/String; = "SmartcardService ACE ARF"


# instance fields
.field private mACMainObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;

.field private mACMainPath:[B

.field private mACRulesObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;

.field private mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

.field private mPkcs15Path:[B

.field private mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

.field private mSELabel:Ljava/lang/String;

.field private mSimAllianceAllowed:Z

.field private mSimIoAllowed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 59
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->GPAC_ARF_AID:[B

    .line 62
    const/16 v0, 0xc

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->PKCS15_AID:[B

    .line 66
    new-array v0, v6, [[B

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->PKCS15_AID:[B

    aput-object v1, v0, v2

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->GPAC_ARF_AID:[B

    aput-object v1, v0, v3

    aput-object v5, v0, v4

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->CONTAINER_AIDS_DEFAULT:[[B

    .line 73
    new-array v0, v6, [[B

    aput-object v5, v0, v2

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->PKCS15_AID:[B

    aput-object v1, v0, v3

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->GPAC_ARF_AID:[B

    aput-object v1, v0, v4

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->CONTAINER_AIDS_SIMIO:[[B

    .line 80
    new-array v0, v4, [[B

    sget-object v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->PKCS15_AID:[B

    aput-object v1, v0, v2

    aput-object v5, v0, v3

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->CONTAINER_AIDS_CMCC:[[B

    return-void

    .line 59
    :array_0
    .array-data 1
        -0x60t
        0x0t
        0x0t
        0x0t
        0x18t
        0x47t
        0x50t
        0x41t
        0x43t
        0x2dt
        0x31t
        0x35t
    .end array-data

    .line 62
    :array_1
    .array-data 1
        -0x60t
        0x0t
        0x0t
        0x0t
        0x63t
        0x50t
        0x4bt
        0x43t
        0x53t
        0x2dt
        0x31t
        0x35t
    .end array-data
.end method

.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V
    .locals 1
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .prologue
    const/4 v0, 0x0

    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSELabel:Ljava/lang/String;

    .line 88
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    .line 91
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;

    .line 93
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACRulesObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;

    .line 95
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    .line 96
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    .line 260
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .line 261
    return-void
.end method

.method private initACEntryPoint()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 138
    const/4 v1, 0x0

    .line 141
    .local v1, "DODFPath":[B
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->readAllowedSimMode()V

    .line 143
    const-string v7, "ro.csc.sales_code"

    invoke-static {v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 144
    .local v6, "salesCode":Ljava/lang/String;
    iget-boolean v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimIoAllowed:Z

    if-eqz v7, :cond_1

    .line 145
    sget-object v4, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->CONTAINER_AIDS_SIMIO:[[B

    .line 152
    .local v4, "containerAids":[[B
    :goto_0
    const/4 v5, 0x0

    .local v5, "ind":I
    :goto_1
    array-length v7, v4

    if-ge v5, v7, :cond_0

    .line 153
    aget-object v7, v4, v5

    invoke-direct {p0, v7}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->selectACRulesContainer([B)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 155
    const/4 v3, 0x0

    .line 156
    .local v3, "acMainPath":[B
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    if-nez v7, :cond_3

    .line 157
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;

    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v2, v7}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 158
    .local v2, "ODFObject":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    invoke-virtual {v2, v7}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->analyseFile([B)[B

    move-result-object v1

    .line 159
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;

    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v0, v7}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 160
    .local v0, "DODFObject":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;
    invoke-virtual {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;->analyseFile([B)[B

    move-result-object v3

    .line 161
    iput-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    .line 171
    .end local v0    # "DODFObject":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDODF;
    .end local v2    # "ODFObject":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;
    :goto_2
    new-instance v7, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;

    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v7, v8, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;[B)V

    iput-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;

    .line 175
    .end local v3    # "acMainPath":[B
    :cond_0
    return-void

    .line 146
    .end local v4    # "containerAids":[[B
    .end local v5    # "ind":I
    :cond_1
    const-string v7, "CHM"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 147
    sget-object v4, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->CONTAINER_AIDS_CMCC:[[B

    .restart local v4    # "containerAids":[[B
    goto :goto_0

    .line 149
    .end local v4    # "containerAids":[[B
    :cond_2
    sget-object v4, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->CONTAINER_AIDS_DEFAULT:[[B

    .restart local v4    # "containerAids":[[B
    goto :goto_0

    .line 163
    .restart local v3    # "acMainPath":[B
    .restart local v5    # "ind":I
    :cond_3
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    if-eqz v7, :cond_4

    .line 164
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    array-length v7, v7

    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    array-length v8, v8

    add-int/2addr v7, v8

    new-array v3, v7, [B

    .line 165
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    array-length v8, v8

    invoke-static {v7, v10, v3, v10, v8}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 166
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    iget-object v8, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    array-length v8, v8

    iget-object v9, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    array-length v9, v9

    invoke-static {v7, v10, v3, v8, v9}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_2

    .line 168
    :cond_4
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    goto :goto_2

    .line 152
    .end local v3    # "acMainPath":[B
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private readAllowedSimMode()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 292
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimIoAllowed:Z

    .line 293
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimAllianceAllowed:Z

    .line 294
    sget-object v0, Lorg/simalliance/openmobileapi/service/SmartcardService;->simConnectionType:Ljava/lang/String;

    const-string v1, "SIMIO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimIoAllowed:Z

    .line 312
    :cond_0
    const-string v0, "SmartcardService ACE ARF"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Allowed SIM mode: SimIo="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimIoAllowed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " SimAlliance="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimAllianceAllowed:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method private selectACRulesContainer([B)Z
    .locals 6
    .param p1, "aid"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 188
    const/4 v1, 0x1

    .line 190
    .local v1, "isActiveContainer":Z
    if-nez p1, :cond_3

    .line 191
    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    .line 193
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimIoAllowed:Z

    if-nez v2, :cond_0

    .line 197
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimAllianceAllowed:Z

    if-eqz v2, :cond_0

    .line 198
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    new-array v3, v5, [B

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->openLogicalArfChannel([B)Lorg/simalliance/openmobileapi/service/IChannel;

    move-result-object v2

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    .line 201
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    if-eqz v2, :cond_1

    .line 202
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "Logical channels are used to access to PKC15"

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->setSeInterface(S)V

    .line 218
    :goto_0
    if-eqz v1, :cond_4

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    if-nez v2, :cond_4

    .line 219
    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    .line 221
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 222
    .local v0, "DIRObject":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;
    sget-object v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->PKCS15_AID:[B

    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;->lookupAID([B)[B

    move-result-object v2

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    .line 223
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    if-nez v2, :cond_4

    .line 224
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "Cannot use ARF: cannot select PKCS#15 directory via EF Dir"

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v3, "Cannot select PKCS#15 directory via EF Dir"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 206
    .end local v0    # "DIRObject":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;
    :cond_1
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimIoAllowed:Z

    if-eqz v2, :cond_2

    .line 209
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "Fall back into ARF with SIM_IO"

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->setSeInterface(S)V

    goto :goto_0

    .line 213
    :cond_2
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "SIM IO is not allowed: cannot access to ARF"

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const/4 v1, 0x0

    goto :goto_0

    .line 233
    :cond_3
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSimAllianceAllowed:Z

    if-nez v2, :cond_5

    .line 234
    const/4 v1, 0x0

    .line 252
    :cond_4
    :goto_1
    return v1

    .line 238
    :cond_5
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2, v5}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->setSeInterface(S)V

    .line 239
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2, p1}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->openLogicalArfChannel([B)Lorg/simalliance/openmobileapi/service/IChannel;

    move-result-object v2

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    if-nez v2, :cond_6

    .line 240
    const/4 v1, 0x0

    .line 241
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "GPAC/PKCS#15 ADF not found!!"

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 245
    :cond_6
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    if-eqz v2, :cond_7

    .line 246
    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainPath:[B

    .line 248
    :cond_7
    iput-object v4, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mPkcs15Path:[B

    goto :goto_1
.end method

.method private updateACRules()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x0

    .line 110
    .local v0, "ACRulesPath":[B
    :try_start_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;->analyseFile()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 117
    if-eqz v0, :cond_1

    .line 118
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "Access Rules needs to be updated..."

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACRulesObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;

    if-nez v2, :cond_0

    .line 120
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACRulesObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;

    .line 122
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->clearAccessRuleCache()V

    .line 123
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACRulesObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;

    invoke-virtual {v2, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACRules;->analyseFile([B)V

    .line 124
    const/4 v2, 0x1

    .line 127
    :goto_0
    return v2

    .line 111
    :catch_0
    move-exception v1

    .line 112
    .local v1, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mACMainObject:Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFACMain;

    .line 113
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->resetAccessRules()V

    .line 114
    throw v1

    .line 126
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "Refresh Tag has not been changed..."

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public findCdf()Ljava/util/Set;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 318
    :try_start_0
    const-string v18, "SmartcardService ACE ARF"

    const-string v19, "findCdfPath() <<<"

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    sget-object v18, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->PKCS15_AID:[B

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->selectACRulesContainer([B)Z

    move-result v18

    if-nez v18, :cond_0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->selectACRulesContainer([B)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 320
    :cond_0
    const-string v18, "SmartcardService ACE ARF"

    const-string v19, "read ODF"

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    new-instance v15, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 322
    .local v15, "odf":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;
    const-string v18, "SmartcardService ACE ARF"

    const-string v19, "searching for CDF path"

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    invoke-virtual {v15}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;->findCdfPath()Ljava/util/Set;

    move-result-object v5

    .line 324
    .local v5, "cdfPaths":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    const-string v18, "SmartcardService ACE ARF"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "found "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " CDF paths"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    .line 327
    .local v8, "certs":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_2

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    .line 328
    .local v4, "cdfPath":[B
    const-string v18, "SmartcardService ACE ARF"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "cdfPath="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x0

    array-length v0, v4

    move/from16 v21, v0

    const-string v22, " "

    move/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v4, v0, v1, v2}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    new-instance v3, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 330
    .local v3, "cdf":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;
    const-string v18, "SmartcardService ACE ARF"

    const-string v19, "searching for cert file"

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-virtual {v3, v4}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->analyseFiles([B)Ljava/util/List;

    move-result-object v13

    .line 340
    .local v13, "list":Ljava/util/List;
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v17

    .line 341
    .local v17, "size":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, v17

    if-ge v10, v0, :cond_1

    .line 342
    invoke-interface {v13, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/HashMap;

    .line 343
    .local v14, "map":Ljava/util/HashMap;
    const-string v18, "path"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [B

    move-object/from16 v0, v18

    check-cast v0, [B

    move-object v7, v0

    .line 344
    .local v7, "certPath":[B
    const-string v18, "offset"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v16

    .line 345
    .local v16, "offset":I
    const-string v18, "length"

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 346
    .local v12, "length":I
    const-string v18, "SmartcardService ACE ARF"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "path="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v7}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([B)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", offset="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", length="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    move/from16 v0, v16

    invoke-virtual {v3, v7, v0, v12}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->readCert([BII)[B

    move-result-object v6

    .line 348
    .local v6, "cert":[B
    const-string v18, "SmartcardService ACE ARF"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "cert="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x0

    array-length v0, v6

    move/from16 v21, v0

    const-string v22, " "

    move/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v6, v0, v1, v2}, Lorg/simalliance/openmobileapi/service/Util;->bytesToString([BIILjava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-interface {v8, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/util/MissingResourceException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 360
    .end local v3    # "cdf":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;
    .end local v4    # "cdfPath":[B
    .end local v6    # "cert":[B
    .end local v7    # "certPath":[B
    .end local v10    # "i":I
    .end local v12    # "length":I
    .end local v13    # "list":Ljava/util/List;
    .end local v14    # "map":Ljava/util/HashMap;
    .end local v16    # "offset":I
    .end local v17    # "size":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->closeArfChannel()V

    .line 364
    .end local v5    # "cdfPaths":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v8    # "certs":Ljava/util/Set;, "Ljava/util/Set<[B>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v15    # "odf":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFODF;
    :cond_3
    :goto_1
    return-object v8

    .line 360
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->closeArfChannel()V

    .line 363
    :cond_5
    :goto_2
    const-string v18, "SmartcardService ACE ARF"

    const-string v19, "findCdfPath() >>> CDF not found..."

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const/4 v8, 0x0

    goto :goto_1

    .line 354
    :catch_0
    move-exception v9

    .line 355
    .local v9, "e":Ljava/util/MissingResourceException;
    :try_start_1
    new-instance v18, Ljava/util/MissingResourceException;

    const-string v19, "all channels are used"

    const-string v20, ""

    const-string v21, ""

    invoke-direct/range {v18 .. v21}, Ljava/util/MissingResourceException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    throw v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    .end local v9    # "e":Ljava/util/MissingResourceException;
    :catchall_0
    move-exception v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->closeArfChannel()V

    :cond_6
    throw v18

    .line 356
    :catch_1
    move-exception v9

    .line 357
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v18, "SmartcardService ACE ARF"

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 360
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->closeArfChannel()V

    goto :goto_2
.end method

.method public declared-synchronized loadAccessControlRules(Ljava/lang/String;)Z
    .locals 4
    .param p1, "secureElement"    # Ljava/lang/String;

    .prologue
    .line 268
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSELabel:Ljava/lang/String;

    .line 269
    const-string v1, "SmartcardService ACE ARF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "- Loading "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSELabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rules..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 271
    :try_start_1
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->initACEntryPoint()V

    .line 272
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->updateACRules()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 282
    :try_start_2
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    if-eqz v2, :cond_0

    .line 283
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->closeArfChannel()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    monitor-exit p0

    return v1

    .line 273
    :catch_0
    move-exception v0

    .line 274
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    instance-of v1, v0, Ljava/util/MissingResourceException;

    if-eqz v1, :cond_2

    .line 276
    check-cast v0, Ljava/util/MissingResourceException;

    .end local v0    # "e":Ljava/lang/Exception;
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 282
    :catchall_0
    move-exception v1

    :try_start_4
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mArfChannel:Lorg/simalliance/openmobileapi/service/IChannel;

    if-eqz v2, :cond_1

    .line 283
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSEHandle:Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;->closeArfChannel()V

    :cond_1
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 268
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1

    .line 278
    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_5
    const-string v1, "SmartcardService ACE ARF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Handler;->mSELabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " rules not correctly initialized! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    new-instance v1, Ljava/security/AccessControlException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method
