.class public Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
.super Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;
.source "AID_REF_DO.java"


# static fields
.field public static final _TAG:I = 0x4f

.field public static final _TAG_DEFAULT_APPLICATION:I = 0xc0


# instance fields
.field private mAid:[B


# direct methods
.method public constructor <init>(I)V
    .locals 3
    .param p1, "tag"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 52
    invoke-direct {p0, v0, p1, v2, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 39
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    .line 53
    const/16 v1, 0xc0

    if-ne p1, v1, :cond_0

    :goto_0
    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    .line 54
    return-void

    .line 53
    :cond_0
    new-array v0, v2, [B

    goto :goto_0
.end method

.method public constructor <init>(I[B)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "aid"    # [B

    .prologue
    const/4 v1, 0x0

    .line 47
    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, p2, p1, v1, v0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    .line 48
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    .line 49
    return-void

    .line 47
    :cond_0
    array-length v0, p2

    goto :goto_0
.end method

.method public constructor <init>([BIII)V
    .locals 1
    .param p1, "rawData"    # [B
    .param p2, "tag"    # I
    .param p3, "valueIndex"    # I
    .param p4, "valueLength"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;-><init>([BIII)V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    .line 43
    return-void
.end method


# virtual methods
.method public build(Ljava/io/ByteArrayOutputStream;)V
    .locals 4
    .param p1, "stream"    # Ljava/io/ByteArrayOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 134
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getTag()I

    move-result v1

    const/16 v2, 0xc0

    if-ne v1, v2, :cond_1

    .line 135
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    if-eqz v1, :cond_0

    .line 136
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v2, "No value allowed for default selected application!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 138
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getTag()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 139
    invoke-virtual {p1, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 164
    :goto_0
    return-void

    .line 140
    :cond_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getTag()I

    move-result v1

    const/16 v2, 0x4f

    if-ne v1, v2, :cond_5

    .line 143
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v1

    if-eqz v1, :cond_3

    .line 144
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_2

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v1

    const/16 v2, 0x10

    if-le v1, v2, :cond_3

    .line 145
    :cond_2
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v2, "Invalid length of AID!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 149
    :cond_3
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getTag()I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 150
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    if-eqz v1, :cond_4

    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    array-length v1, v1

    if-lez v1, :cond_4

    .line 151
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    array-length v1, v1

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 153
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    invoke-virtual {p1, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "ioe":Ljava/io/IOException;
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v2, "AID could not be written!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 158
    .end local v0    # "ioe":Ljava/io/IOException;
    :cond_4
    invoke-virtual {p1, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 162
    :cond_5
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;

    const-string v2, "AID-REF-DO must either be C0 or 4F!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/DO_Exception;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 168
    const/4 v1, 0x0

    .line 170
    .local v1, "equals":Z
    instance-of v2, p1, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    if-eqz v2, :cond_0

    .line 171
    invoke-super {p0, p1}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 173
    if-eqz v1, :cond_0

    move-object v0, p1

    .line 174
    check-cast v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;

    .line 175
    .local v0, "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    if-nez v2, :cond_1

    iget-object v2, v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    if-nez v2, :cond_1

    .line 176
    and-int/lit8 v1, v1, 0x1

    .line 182
    .end local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    :cond_0
    :goto_0
    return v1

    .line 178
    .restart local v0    # "aid_ref_do":Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;
    :cond_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    iget-object v3, v0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    and-int/2addr v1, v2

    goto :goto_0
.end method

.method public getAid()[B
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    return-object v0
.end method

.method public interpret()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;
        }
    .end annotation

    .prologue
    .line 90
    const/4 v2, 0x0

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    .line 92
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getRawData()[B

    move-result-object v0

    .line 93
    .local v0, "data":[B
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueIndex()I

    move-result v1

    .line 95
    .local v1, "index":I
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getTag()I

    move-result v2

    const/16 v3, 0xc0

    if-ne v2, v3, :cond_0

    .line 96
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v2

    if-eqz v2, :cond_4

    .line 97
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Invalid value length for AID-REF-DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 99
    :cond_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getTag()I

    move-result v2

    const/16 v3, 0x4f

    if-ne v2, v3, :cond_5

    .line 102
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v2

    const/4 v3, 0x5

    if-lt v2, v3, :cond_1

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_2

    :cond_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v2

    if-eqz v2, :cond_2

    .line 103
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Invalid value length for AID-REF-DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 106
    :cond_2
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v2

    add-int/2addr v2, v1

    array-length v3, v0

    if-le v2, v3, :cond_3

    .line 107
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Not enough data for AID-REF-DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 110
    :cond_3
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v2

    new-array v2, v2, [B

    iput-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    .line 111
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->mAid:[B

    const/4 v3, 0x0

    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->getValueLength()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 116
    :cond_4
    return-void

    .line 114
    :cond_5
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;

    const-string v3, "Invalid Tag for AID-REF-DO!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/ParserException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .local v0, "b":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 60
    .local v2, "out":Ljava/io/ByteArrayOutputStream;
    const-string v3, "AID_REF_DO: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    :try_start_0
    invoke-virtual {p0, v2}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/AID_REF_DO;->build(Ljava/io/ByteArrayOutputStream;)V

    .line 63
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-static {v3}, Lorg/simalliance/openmobileapi/service/security/gpac/dataobjects/BerTlv;->toHex([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 64
    :catch_0
    move-exception v1

    .line 65
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
