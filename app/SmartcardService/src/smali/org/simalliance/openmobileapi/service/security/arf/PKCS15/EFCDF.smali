.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;
.super Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.source "EFCDF.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "SmartcardService ACE ARF"


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V
    .locals 0
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 105
    return-void
.end method

.method private convertByteToInt([B)I
    .locals 5
    .param p1, "b"    # [B

    .prologue
    .line 92
    const/4 v2, 0x0

    .line 93
    .local v2, "value":I
    array-length v1, p1

    .line 94
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 95
    shl-int/lit8 v3, v2, 0x8

    aget-byte v4, p1, v0

    and-int/lit16 v4, v4, 0xff

    or-int v2, v3, v4

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_0
    return v2
.end method

.method private decodeDER([B)[B
    .locals 7
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 27
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v1, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 28
    .local v1, "der":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v4

    .line 29
    .local v4, "type":B
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v0

    .line 30
    .local v0, "data":[B
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    .end local v1    # "der":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    invoke-direct {v1, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 31
    .restart local v1    # "der":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :goto_0
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v5

    if-nez v5, :cond_1

    .line 32
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v4

    .line 33
    const/16 v5, -0x5f

    if-ne v4, v5, :cond_0

    .line 35
    :try_start_0
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v4

    .line 36
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parsePathAttributes()[B
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 49
    :goto_1
    return-object v3

    .line 38
    :catch_0
    move-exception v2

    .line 40
    .local v2, "e":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
    const-string v5, "SmartcardService ACE ARF"

    const-string v6, "error occured reading CDF certificate file"

    invoke-static {v5, v6}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    .end local v2    # "e":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
    :cond_0
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    goto :goto_0

    .line 49
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private decodeDERs([B)Ljava/util/List;
    .locals 12
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    .line 54
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v5, "list":Ljava/util/List;
    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v1, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 56
    .local v1, "der":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :cond_0
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v10

    if-nez v10, :cond_2

    .line 57
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v9

    .line 58
    .local v9, "type":B
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v0

    .line 59
    .local v0, "data":[B
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v2, v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 60
    .local v2, "der1":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    :goto_0
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->isEndofBuffer()Z

    move-result v10

    if-nez v10, :cond_0

    .line 61
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v9

    .line 62
    const/16 v10, -0x5f

    if-ne v9, v10, :cond_1

    .line 64
    :try_start_0
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v9

    .line 65
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parsePathAttributes()[B

    move-result-object v8

    .line 66
    .local v8, "path":[B
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 67
    .local v6, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v10, "path"

    invoke-interface {v6, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 69
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v7

    .line 70
    .local v7, "offset":[B
    const-string v10, "offset"

    invoke-direct {p0, v7}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->convertByteToInt([B)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v6, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const/16 v10, -0x80

    invoke-virtual {v2, v10}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 72
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v4

    .line 73
    .local v4, "length":[B
    const-string v10, "length"

    invoke-direct {p0, v4}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->convertByteToInt([B)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v6, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 75
    .end local v4    # "length":[B
    .end local v6    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v7    # "offset":[B
    .end local v8    # "path":[B
    :catch_0
    move-exception v3

    .line 77
    .local v3, "e":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
    const-string v10, "SmartcardService ACE ARF"

    const-string v11, "error occured reading CDF certificate file"

    invoke-static {v10, v11}, Lorg/simalliance/openmobileapi/service/Util$Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 80
    .end local v3    # "e":Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
    :cond_1
    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    goto :goto_0

    .line 86
    .end local v0    # "data":[B
    .end local v2    # "der1":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    .end local v9    # "type":B
    :cond_2
    return-object v5
.end method


# virtual methods
.method public analyseFile([B)[B
    .locals 3
    .param p1, "path"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 112
    const-string v1, "SmartcardService ACE ARF"

    const-string v2, "Analysing EF_CDF..."

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->selectFile([B)I

    move-result v1

    const v2, 0x9000

    if-eq v1, v2, :cond_0

    new-instance v1, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v2, "EFCDF not found!!"

    invoke-direct {v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v1

    .line 114
    :cond_0
    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->readBinary(II)[B

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->decodeDER([B)[B

    move-result-object v0

    .line 115
    .local v0, "certPath":[B
    if-nez v0, :cond_1

    .line 116
    const-string v1, "SmartcardService ACE ARF"

    const-string v2, "didn\'t found certificate file path"

    invoke-static {v1, v2}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_1
    return-object v0
.end method

.method public analyseFiles([B)Ljava/util/List;
    .locals 4
    .param p1, "path"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 123
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "analyseFiles..."

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->selectFile([B)I

    move-result v2

    const v3, 0x9000

    if-eq v2, v3, :cond_0

    .line 125
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v3, "EFCDF not found!!"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v2

    .line 126
    :cond_0
    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0, v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->readBinary(II)[B

    move-result-object v0

    .line 127
    .local v0, "buffer":[B
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->decodeDERs([B)Ljava/util/List;

    move-result-object v1

    .line 128
    .local v1, "list":Ljava/util/List;
    if-nez v1, :cond_1

    .line 129
    const-string v2, "SmartcardService ACE ARF"

    const-string v3, "didn\'t found certificate file path"

    invoke-static {v2, v3}, Lorg/simalliance/openmobileapi/service/Util$Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    :cond_1
    return-object v1
.end method

.method public readCert([B)[B
    .locals 2
    .param p1, "certPath"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 134
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->selectFile([B)I

    move-result v0

    const v1, 0x9000

    if-eq v0, v1, :cond_0

    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "Certificate file not found"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->readBinary(II)[B

    move-result-object v0

    return-object v0
.end method

.method public readCert([BII)[B
    .locals 2
    .param p1, "certPath"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->selectFile([B)I

    move-result v0

    const v1, 0x9000

    if-eq v0, v1, :cond_0

    .line 142
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v1, "Certificate file not found"

    invoke-direct {v0, v1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    invoke-virtual {p0, p2, p3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFCDF;->readBinary(II)[B

    move-result-object v0

    return-object v0
.end method
