.class public Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;
.super Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;
.source "EFDIR.java"


# static fields
.field public static final EFDIR_PATH:[B

.field public static final TAG:Ljava/lang/String; = "ACE ARF EF_Dir"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;->EFDIR_PATH:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x3ft
        0x0t
        0x2ft
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V
    .locals 0
    .param p1, "handle"    # Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EF;-><init>(Lorg/simalliance/openmobileapi/service/security/arf/SecureElement;)V

    .line 74
    return-void
.end method

.method private decodeDER([B[B)[B
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "AID"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;
        }
    .end annotation

    .prologue
    const/16 v3, 0x51

    .line 48
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;

    invoke-direct {v0, p1}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;-><init>([B)V

    .line 49
    .local v0, "DER":Lorg/simalliance/openmobileapi/service/security/arf/DERParser;
    const/16 v2, 0x61

    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 51
    const/16 v2, 0x4f

    invoke-virtual {v0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 52
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v2

    invoke-static {v2, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    .line 53
    const/4 v2, 0x0

    .line 64
    :goto_0
    return-object v2

    .line 56
    :cond_0
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV()B

    move-result v1

    .line 57
    .local v1, "objectType":B
    const/16 v2, 0x50

    if-ne v1, v2, :cond_2

    .line 59
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    .line 60
    invoke-virtual {v0, v3}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->parseTLV(B)S

    .line 64
    :cond_1
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/arf/DERParser;->getTLVData()[B

    move-result-object v2

    goto :goto_0

    .line 61
    :cond_2
    if-eq v1, v3, :cond_1

    .line 62
    new-instance v2, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v3, "[Parser] Application Tag expected"

    invoke-direct {v2, v3}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public lookupAID([B)[B
    .locals 6
    .param p1, "AID"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;,
            Lorg/simalliance/openmobileapi/service/security/arf/SecureElementException;
        }
    .end annotation

    .prologue
    .line 83
    const-string v4, "ACE ARF EF_Dir"

    const-string v5, "Analysing EF_DIR..."

    invoke-static {v4, v5}, Lorg/simalliance/openmobileapi/service/Util$Log;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v4, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;->EFDIR_PATH:[B

    invoke-virtual {p0, v4}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;->selectFile([B)I

    move-result v4

    const v5, 0x9000

    if-eq v4, v5, :cond_0

    .line 86
    new-instance v4, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;

    const-string v5, "EF_DIR not found!!"

    invoke-direct {v4, v5}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/PKCS15Exception;-><init>(Ljava/lang/String;)V

    throw v4

    .line 88
    :cond_0
    const/4 v0, 0x0

    .line 89
    .local v0, "ODFPath":[B
    const/4 v2, 0x1

    .line 90
    .local v2, "index":S
    :goto_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;->getFileNbRecords()S

    move-result v4

    if-gt v2, v4, :cond_1

    .line 91
    add-int/lit8 v4, v2, 0x1

    int-to-short v3, v4

    .end local v2    # "index":S
    .local v3, "index":S
    invoke-virtual {p0, v2}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;->readRecord(S)[B

    move-result-object v1

    .line 92
    .local v1, "data":[B
    invoke-direct {p0, v1, p1}, Lorg/simalliance/openmobileapi/service/security/arf/PKCS15/EFDIR;->decodeDER([B[B)[B

    move-result-object v0

    if-eqz v0, :cond_2

    move v2, v3

    .line 95
    .end local v1    # "data":[B
    .end local v3    # "index":S
    .restart local v2    # "index":S
    :cond_1
    return-object v0

    .end local v2    # "index":S
    .restart local v1    # "data":[B
    .restart local v3    # "index":S
    :cond_2
    move v2, v3

    .end local v3    # "index":S
    .restart local v2    # "index":S
    goto :goto_0
.end method
