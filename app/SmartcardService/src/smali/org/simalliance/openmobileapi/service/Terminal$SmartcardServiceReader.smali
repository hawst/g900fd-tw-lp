.class final Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;
.super Lorg/simalliance/openmobileapi/service/ISmartcardServiceReader$Stub;
.source "Terminal.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/simalliance/openmobileapi/service/Terminal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "SmartcardServiceReader"
.end annotation


# instance fields
.field private final mLock:Ljava/lang/Object;

.field protected final mService:Lorg/simalliance/openmobileapi/service/SmartcardService;

.field private final mSessions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/simalliance/openmobileapi/service/Terminal;


# direct methods
.method public constructor <init>(Lorg/simalliance/openmobileapi/service/Terminal;Lorg/simalliance/openmobileapi/service/SmartcardService;)V
    .locals 1
    .param p2, "service"    # Lorg/simalliance/openmobileapi/service/SmartcardService;

    .prologue
    .line 622
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->this$0:Lorg/simalliance/openmobileapi/service/Terminal;

    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceReader$Stub;-><init>()V

    .line 618
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mSessions:Ljava/util/ArrayList;

    .line 620
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mLock:Ljava/lang/Object;

    .line 623
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mService:Lorg/simalliance/openmobileapi/service/SmartcardService;

    .line 624
    return-void
.end method


# virtual methods
.method closeSession(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;)V
    .locals 3
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 706
    if-nez p1, :cond_0

    .line 707
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "session is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 709
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 710
    :try_start_0
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 711
    new-instance v0, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V

    .line 712
    .local v0, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    invoke-virtual {p1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->closeChannels(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 713
    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/SmartcardError;->throwException()V

    .line 714
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->setClosed()V

    .line 716
    .end local v0    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :cond_1
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mSessions:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 717
    monitor-exit v2

    .line 718
    return-void

    .line 717
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public closeSessions(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    .locals 4
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 683
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 684
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 685
    :try_start_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mSessions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    .line 686
    .local v1, "session":Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 687
    invoke-virtual {v1, p1}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->closeChannels(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 688
    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;->setClosed()V

    goto :goto_0

    .line 692
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "session":Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 691
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mSessions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 692
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 693
    return-void
.end method

.method public getAtr()[B
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->this$0:Lorg/simalliance/openmobileapi/service/Terminal;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Terminal;->getAtr()[B

    move-result-object v0

    return-object v0
.end method

.method public getName(Lorg/simalliance/openmobileapi/service/SmartcardError;)Ljava/lang/String;
    .locals 1
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 632
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 633
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->this$0:Lorg/simalliance/openmobileapi/service/Terminal;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/Terminal;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getTerminal()Lorg/simalliance/openmobileapi/service/Terminal;
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->this$0:Lorg/simalliance/openmobileapi/service/Terminal;

    return-object v0
.end method

.method public isSecureElementPresent(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z
    .locals 2
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 639
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 641
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->this$0:Lorg/simalliance/openmobileapi/service/Terminal;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/service/Terminal;->isCardPresent()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 645
    :goto_0
    return v1

    .line 642
    :catch_0
    move-exception v0

    .line 643
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {p1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    .line 645
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public openSession(Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;
    .locals 7
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 651
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 653
    .local v1, "salesCode":Ljava/lang/String;
    invoke-static {p1}, Lorg/simalliance/openmobileapi/service/SmartcardService;->clearError(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 655
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->this$0:Lorg/simalliance/openmobileapi/service/Terminal;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/Terminal;->isCardPresent()Z

    move-result v3

    if-nez v3, :cond_0

    .line 656
    new-instance v3, Ljava/io/IOException;

    const-string v4, "Secure Element is not presented."

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v3}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V
    :try_end_0
    .catch Lorg/simalliance/openmobileapi/service/CardException; {:try_start_0 .. :try_end_0} :catch_0

    .line 676
    :goto_0
    return-object v2

    .line 659
    :catch_0
    move-exception v0

    .line 660
    .local v0, "e":Lorg/simalliance/openmobileapi/service/CardException;
    invoke-static {p1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    goto :goto_0

    .line 664
    .end local v0    # "e":Lorg/simalliance/openmobileapi/service/CardException;
    :cond_0
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 666
    :try_start_1
    sget-object v3, Lorg/simalliance/openmobileapi/service/SmartcardService;->secureEvtType:Ljava/lang/String;

    const-string v5, "ISIS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "CTC"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 667
    :cond_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mService:Lorg/simalliance/openmobileapi/service/SmartcardService;

    iget-object v5, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->this$0:Lorg/simalliance/openmobileapi/service/Terminal;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/service/Terminal;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Lorg/simalliance/openmobileapi/service/SmartcardService;->initializeAccessControl(Ljava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 673
    :cond_2
    :try_start_2
    new-instance v2, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mService:Lorg/simalliance/openmobileapi/service/SmartcardService;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v2, v3, p0}, Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService;Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;)V

    .line 674
    .local v2, "session":Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;->mSessions:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 676
    monitor-exit v4

    goto :goto_0

    .line 677
    .end local v2    # "session":Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 669
    :catch_1
    move-exception v0

    .line 670
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-static {p1, v0}, Lorg/simalliance/openmobileapi/service/SmartcardService;->setError(Lorg/simalliance/openmobileapi/service/SmartcardError;Ljava/lang/Exception;)V

    .line 671
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
