.class public abstract Lorg/simalliance/openmobileapi/service/Terminal;
.super Ljava/lang/Object;
.source "Terminal.java"

# interfaces
.implements Lorg/simalliance/openmobileapi/service/ITerminal;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simalliance/openmobileapi/service/Terminal$SmartcardServiceReader;
    }
.end annotation


# static fields
.field static mRandom:Ljava/util/Random;


# instance fields
.field private mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

.field private final mChannels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lorg/simalliance/openmobileapi/service/IChannel;",
            ">;"
        }
    .end annotation
.end field

.field protected mContext:Landroid/content/Context;

.field protected mDefaultApplicationSelectedOnBasicChannel:Z

.field public volatile mIsConnected:Z

.field private final mLock:Ljava/lang/Object;

.field protected final mName:Ljava/lang/String;

.field protected mSelectResponse:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lorg/simalliance/openmobileapi/service/Terminal;->mRandom:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mDefaultApplicationSelectedOnBasicChannel:Z

    .line 125
    iput-object p2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mContext:Landroid/content/Context;

    .line 126
    iput-object p1, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mName:Ljava/lang/String;

    .line 127
    return-void
.end method

.method static appendResponse([B[BI)[B
    .locals 3
    .param p0, "r1"    # [B
    .param p1, "r2"    # [B
    .param p2, "length"    # I

    .prologue
    const/4 v2, 0x0

    .line 85
    array-length v1, p0

    add-int/2addr v1, p2

    new-array v0, v1, [B

    .line 86
    .local v0, "rsp":[B
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 87
    array-length v1, p0

    invoke-static {p1, v2, v0, v1, p2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 88
    return-object v0
.end method

.method static createMessage(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "commandName"    # Ljava/lang/String;
    .param p1, "sw"    # I

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 101
    .local v0, "message":Ljava/lang/StringBuffer;
    if-eqz p0, :cond_0

    .line 102
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    :cond_0
    const-string v1, "SW1/2 error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 105
    const/high16 v1, 0x10000

    or-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 106
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static createMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "commandName"    # Ljava/lang/String;
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 118
    if-nez p0, :cond_0

    .line 121
    .end local p1    # "message":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "message":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private getBasicChannel()Lorg/simalliance/openmobileapi/service/IChannel;
    .locals 3

    .prologue
    .line 179
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simalliance/openmobileapi/service/IChannel;

    .line 180
    .local v0, "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/IChannel;->getChannelNumber()I

    move-result v2

    if-nez v2, :cond_0

    .line 184
    .end local v0    # "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private registerChannel(Lorg/simalliance/openmobileapi/service/Channel;)J
    .locals 6
    .param p1, "channel"    # Lorg/simalliance/openmobileapi/service/Channel;

    .prologue
    .line 488
    sget-object v2, Lorg/simalliance/openmobileapi/service/Terminal;->mRandom:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextInt()I

    move-result v2

    int-to-long v0, v2

    .line 489
    .local v0, "hChannel":J
    const/16 v2, 0x20

    shl-long/2addr v0, v2

    .line 490
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 492
    invoke-virtual {p1, v0, v1}, Lorg/simalliance/openmobileapi/service/Channel;->setHandle(J)V

    .line 494
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    return-wide v0
.end method


# virtual methods
.method public closeChannel(Lorg/simalliance/openmobileapi/service/Channel;)V
    .locals 6
    .param p1, "channel"    # Lorg/simalliance/openmobileapi/service/Channel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_0
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/Channel;->getChannelNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalCloseLogicalChannel(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :try_start_1
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/Channel;->getHandle()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mIsConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    :try_start_2
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalDisconnect()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 164
    :cond_0
    :goto_0
    :try_start_3
    monitor-exit v1

    .line 165
    return-void

    .line 156
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/Channel;->getHandle()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mIsConnected:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v2

    if-eqz v2, :cond_1

    .line 159
    :try_start_4
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalDisconnect()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 161
    :cond_1
    :goto_1
    :try_start_5
    throw v0

    .line 164
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 160
    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public declared-synchronized closeChannels()V
    .locals 7

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    .line 135
    .local v3, "col":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/simalliance/openmobileapi/service/IChannel;>;"
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v6

    new-array v6, v6, [Lorg/simalliance/openmobileapi/service/IChannel;

    invoke-interface {v3, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lorg/simalliance/openmobileapi/service/IChannel;

    .line 136
    .local v2, "channelList":[Lorg/simalliance/openmobileapi/service/IChannel;
    move-object v0, v2

    .local v0, "arr$":[Lorg/simalliance/openmobileapi/service/IChannel;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    .local v1, "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    :try_start_1
    check-cast v1, Lorg/simalliance/openmobileapi/service/Channel;

    .end local v1    # "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    invoke-virtual {p0, v1}, Lorg/simalliance/openmobileapi/service/Terminal;->closeChannel(Lorg/simalliance/openmobileapi/service/Channel;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 142
    :cond_0
    monitor-exit p0

    return-void

    .line 134
    .end local v0    # "arr$":[Lorg/simalliance/openmobileapi/service/IChannel;
    .end local v2    # "channelList":[Lorg/simalliance/openmobileapi/service/IChannel;
    .end local v3    # "col":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/simalliance/openmobileapi/service/IChannel;>;"
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 139
    .restart local v0    # "arr$":[Lorg/simalliance/openmobileapi/service/IChannel;
    .restart local v2    # "channelList":[Lorg/simalliance/openmobileapi/service/IChannel;
    .restart local v3    # "col":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/simalliance/openmobileapi/service/IChannel;>;"
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :catch_0
    move-exception v6

    goto :goto_1
.end method

.method protected createChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;ILorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .locals 1
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "channelNumber"    # I
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 175
    new-instance v0, Lorg/simalliance/openmobileapi/service/Channel;

    invoke-direct {v0, p1, p0, p2, p3}, Lorg/simalliance/openmobileapi/service/Channel;-><init>(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/Terminal;ILorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)V

    return-object v0
.end method

.method public dump(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 4
    .param p1, "writer"    # Ljava/io/PrintWriter;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    .line 726
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "SMARTCARD SERVICE TERMINAL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 727
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 729
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 731
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mIsConnected:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mIsConnected:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 732
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 735
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "List of open channels:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 737
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simalliance/openmobileapi/service/IChannel;

    .line 738
    .local v0, "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  channel "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/IChannel;->getChannelNumber()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 739
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    package      : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/IChannel;->getChannelAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v3

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 740
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    pid          : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/IChannel;->getChannelAccess()Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v3

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/security/ChannelAccess;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 741
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    aid selected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/IChannel;->hasSelectedAid()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 742
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "    basic channel: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lorg/simalliance/openmobileapi/service/IChannel;->isBasicChannel()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 745
    .end local v0    # "channel":Lorg/simalliance/openmobileapi/service/IChannel;
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 748
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v2, p1, p2}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->dump(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 749
    :cond_1
    return-void
.end method

.method public getAccessControlEnforcer()Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    return-object v0
.end method

.method public getAtr()[B
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized getChannel(J)Lorg/simalliance/openmobileapi/service/IChannel;
    .locals 3
    .param p1, "hChannel"    # J

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simalliance/openmobileapi/service/IChannel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectResponse()[B
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mSelectResponse:[B

    return-object v0
.end method

.method public declared-synchronized initializeAccessControl(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    .locals 1
    .param p1, "loadAtStartup"    # Z
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 596
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    if-nez v0, :cond_0

    .line 597
    new-instance v0, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-direct {v0, p0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;-><init>(Lorg/simalliance/openmobileapi/service/ITerminal;)V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    .line 599
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0, p1, p2}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->initialize(ZLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 596
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected abstract internalCloseLogicalChannel(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method

.method protected abstract internalConnect()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method

.method protected abstract internalDisconnect()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method

.method protected abstract internalOpenLogicalChannel()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected abstract internalOpenLogicalChannel([B)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation
.end method

.method protected abstract internalTransmit([B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 415
    iget-boolean v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mIsConnected:Z

    return v0
.end method

.method public openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .locals 4
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 298
    if-nez p2, :cond_0

    .line 299
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "callback must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 302
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 303
    :try_start_0
    iget-boolean v1, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mDefaultApplicationSelectedOnBasicChannel:Z

    if-nez v1, :cond_1

    .line 304
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v3, "default application is not selected"

    invoke-direct {v1, v3}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 318
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 306
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->getBasicChannel()Lorg/simalliance/openmobileapi/service/IChannel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 307
    new-instance v1, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v3, "basic channel in use"

    invoke-direct {v1, v3}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 309
    :cond_2
    iget-object v1, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 310
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalConnect()V

    .line 314
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->createChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;ILorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v0

    .line 315
    .local v0, "basicChannel":Lorg/simalliance/openmobileapi/service/Channel;
    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lorg/simalliance/openmobileapi/service/Channel;->hasSelectedAid(Z[B)V

    .line 316
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/Terminal;->registerChannel(Lorg/simalliance/openmobileapi/service/Channel;)J

    .line 317
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public openBasicChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .locals 5
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "aid"    # [B
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 322
    if-nez p3, :cond_0

    .line 323
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "callback must not be null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 325
    :cond_0
    if-nez p2, :cond_1

    .line 326
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "aid must not be null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 329
    :cond_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 330
    :try_start_0
    invoke-direct {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->getBasicChannel()Lorg/simalliance/openmobileapi/service/IChannel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 331
    new-instance v2, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v4, "basic channel in use"

    invoke-direct {v2, v4}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 352
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 333
    :cond_2
    :try_start_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 334
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalConnect()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 338
    :cond_3
    :try_start_2
    invoke-virtual {p0, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->select([B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 347
    const/4 v2, 0x0

    :try_start_3
    invoke-virtual {p0, p1, v2, p3}, Lorg/simalliance/openmobileapi/service/Terminal;->createChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;ILorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v0

    .line 348
    .local v0, "basicChannel":Lorg/simalliance/openmobileapi/service/Channel;
    const/4 v2, 0x1

    invoke-virtual {v0, v2, p2}, Lorg/simalliance/openmobileapi/service/Channel;->hasSelectedAid(Z[B)V

    .line 349
    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mDefaultApplicationSelectedOnBasicChannel:Z

    .line 350
    invoke-direct {p0, v0}, Lorg/simalliance/openmobileapi/service/Terminal;->registerChannel(Lorg/simalliance/openmobileapi/service/Channel;)J

    .line 351
    monitor-exit v3

    return-object v0

    .line 339
    .end local v0    # "basicChannel":Lorg/simalliance/openmobileapi/service/Channel;
    :catch_0
    move-exception v1

    .line 340
    .local v1, "e":Ljava/lang/Exception;
    iget-boolean v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mIsConnected:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 341
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalDisconnect()V

    .line 343
    :cond_4
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method public openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .locals 6
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 356
    if-nez p2, :cond_0

    .line 357
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "callback must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 360
    :cond_0
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 361
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 362
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalConnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 365
    :cond_1
    const/4 v0, 0x0

    .line 367
    .local v0, "channelNumber":I
    :try_start_1
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalOpenLogicalChannel()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 376
    :try_start_2
    invoke-virtual {p0, p1, v0, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->createChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;ILorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v2

    .line 377
    .local v2, "logicalChannel":Lorg/simalliance/openmobileapi/service/Channel;
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, Lorg/simalliance/openmobileapi/service/Channel;->hasSelectedAid(Z[B)V

    .line 378
    invoke-direct {p0, v2}, Lorg/simalliance/openmobileapi/service/Terminal;->registerChannel(Lorg/simalliance/openmobileapi/service/Channel;)J

    .line 379
    monitor-exit v4

    return-object v2

    .line 368
    .end local v2    # "logicalChannel":Lorg/simalliance/openmobileapi/service/Channel;
    :catch_0
    move-exception v1

    .line 369
    .local v1, "e":Ljava/lang/Exception;
    iget-boolean v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mIsConnected:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 370
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalDisconnect()V

    .line 372
    :cond_2
    throw v1

    .line 380
    .end local v0    # "channelNumber":I
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public openLogicalChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;[BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;
    .locals 5
    .param p1, "session"    # Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;
    .param p2, "aid"    # [B
    .param p3, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 384
    if-nez p3, :cond_0

    .line 385
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "callback must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 387
    :cond_0
    if-nez p2, :cond_1

    .line 388
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "aid must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 391
    :cond_1
    iget-object v4, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 392
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 393
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalConnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    :cond_2
    const/4 v0, 0x0

    .line 398
    .local v0, "channelNumber":I
    :try_start_1
    invoke-virtual {p0, p2}, Lorg/simalliance/openmobileapi/service/Terminal;->internalOpenLogicalChannel([B)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 407
    :try_start_2
    invoke-virtual {p0, p1, v0, p3}, Lorg/simalliance/openmobileapi/service/Terminal;->createChannel(Lorg/simalliance/openmobileapi/service/SmartcardService$SmartcardServiceSession;ILorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/Channel;

    move-result-object v2

    .line 408
    .local v2, "logicalChannel":Lorg/simalliance/openmobileapi/service/Channel;
    const/4 v3, 0x1

    invoke-virtual {v2, v3, p2}, Lorg/simalliance/openmobileapi/service/Channel;->hasSelectedAid(Z[B)V

    .line 409
    invoke-direct {p0, v2}, Lorg/simalliance/openmobileapi/service/Terminal;->registerChannel(Lorg/simalliance/openmobileapi/service/Channel;)J

    .line 410
    monitor-exit v4

    return-object v2

    .line 399
    .end local v2    # "logicalChannel":Lorg/simalliance/openmobileapi/service/Channel;
    :catch_0
    move-exception v1

    .line 400
    .local v1, "e":Ljava/lang/Exception;
    iget-boolean v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mIsConnected:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mChannels:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 401
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalDisconnect()V

    .line 403
    :cond_3
    throw v1

    .line 411
    .end local v0    # "channelNumber":I
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method protected declared-synchronized protocolTransmit([B)[B
    .locals 11
    .param p1, "cmd"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    const/16 v10, 0x61

    const/4 v9, 0x2

    .line 428
    monitor-enter p0

    move-object v0, p1

    .line 429
    .local v0, "command":[B
    const/4 v3, 0x0

    .line 430
    .local v3, "rsp":[B
    :try_start_0
    iget-object v7, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 431
    :try_start_1
    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalTransmit([B)[B

    move-result-object v3

    .line 432
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 434
    :try_start_2
    array-length v6, v3

    if-lt v6, v9, :cond_0

    .line 435
    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    aget-byte v6, v3, v6

    and-int/lit16 v4, v6, 0xff

    .line 436
    .local v4, "sw1":I
    array-length v6, v3

    add-int/lit8 v6, v6, -0x1

    aget-byte v6, v3, v6

    and-int/lit16 v5, v6, 0xff

    .line 437
    .local v5, "sw2":I
    const/16 v6, 0x6c

    if-ne v4, v6, :cond_1

    .line 438
    array-length v6, p1

    add-int/lit8 v6, v6, -0x1

    array-length v7, v3

    add-int/lit8 v7, v7, -0x1

    aget-byte v7, v3, v7

    aput-byte v7, v0, v6

    .line 439
    invoke-virtual {p0, v0}, Lorg/simalliance/openmobileapi/service/Terminal;->internalTransmit([B)[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    .line 477
    .end local v4    # "sw1":I
    .end local v5    # "sw2":I
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v3

    .line 432
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 428
    :catchall_1
    move-exception v6

    monitor-exit p0

    throw v6

    .line 440
    .restart local v4    # "sw1":I
    .restart local v5    # "sw2":I
    :cond_1
    if-ne v4, v10, :cond_3

    .line 441
    const/4 v6, 0x5

    :try_start_5
    new-array v1, v6, [B

    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-byte v7, v0, v7

    aput-byte v7, v1, v6

    const/4 v6, 0x1

    const/16 v7, -0x40

    aput-byte v7, v1, v6

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput-byte v7, v1, v6

    const/4 v6, 0x3

    const/4 v7, 0x0

    aput-byte v7, v1, v6

    const/4 v6, 0x4

    const/4 v7, 0x0

    aput-byte v7, v1, v6

    .line 444
    .local v1, "getResponseCmd":[B
    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    new-array v2, v6, [B

    .line 445
    .local v2, "response":[B
    const/4 v6, 0x0

    const/4 v7, 0x0

    array-length v8, v3

    add-int/lit8 v8, v8, -0x2

    invoke-static {v3, v6, v2, v7, v8}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 447
    :goto_1
    const/4 v6, 0x4

    array-length v7, v3

    add-int/lit8 v7, v7, -0x1

    aget-byte v7, v3, v7

    aput-byte v7, v1, v6

    .line 448
    invoke-virtual {p0, v1}, Lorg/simalliance/openmobileapi/service/Terminal;->internalTransmit([B)[B

    move-result-object v3

    .line 449
    array-length v6, v3

    if-lt v6, v9, :cond_2

    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    aget-byte v6, v3, v6

    if-ne v6, v10, :cond_2

    .line 450
    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    invoke-static {v2, v3, v6}, Lorg/simalliance/openmobileapi/service/Terminal;->appendResponse([B[BI)[B

    move-result-object v2

    goto :goto_1

    .line 452
    :cond_2
    array-length v6, v3

    invoke-static {v2, v3, v6}, Lorg/simalliance/openmobileapi/service/Terminal;->appendResponse([B[BI)[B

    move-result-object v2

    .line 456
    move-object v3, v2

    .line 457
    goto :goto_0

    .end local v1    # "getResponseCmd":[B
    .end local v2    # "response":[B
    :cond_3
    array-length v6, v3

    if-ne v6, v9, :cond_0

    const/16 v6, 0x63

    if-ne v4, v6, :cond_0

    const/16 v6, 0x10

    if-ne v5, v6, :cond_0

    .line 458
    const/4 v6, 0x5

    new-array v1, v6, [B

    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-byte v7, v0, v7

    and-int/lit8 v7, v7, 0x3

    int-to-byte v7, v7

    aput-byte v7, v1, v6

    const/4 v6, 0x1

    const/16 v7, -0x40

    aput-byte v7, v1, v6

    const/4 v6, 0x2

    const/4 v7, 0x0

    aput-byte v7, v1, v6

    const/4 v6, 0x3

    const/4 v7, 0x0

    aput-byte v7, v1, v6

    const/4 v6, 0x4

    const/4 v7, 0x0

    aput-byte v7, v1, v6

    .line 461
    .restart local v1    # "getResponseCmd":[B
    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    new-array v2, v6, [B

    .line 462
    .restart local v2    # "response":[B
    const/4 v6, 0x0

    const/4 v7, 0x0

    array-length v8, v3

    add-int/lit8 v8, v8, -0x2

    invoke-static {v3, v6, v2, v7, v8}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 463
    array-length v6, v3

    add-int/lit8 v6, v6, -0x1

    const/4 v7, 0x0

    aput-byte v7, v3, v6

    .line 465
    :goto_2
    const/4 v6, 0x4

    array-length v7, v3

    add-int/lit8 v7, v7, -0x1

    aget-byte v7, v3, v7

    aput-byte v7, v1, v6

    .line 466
    invoke-virtual {p0, v1}, Lorg/simalliance/openmobileapi/service/Terminal;->internalTransmit([B)[B

    move-result-object v3

    .line 467
    array-length v6, v3

    if-lt v6, v9, :cond_4

    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    aget-byte v6, v3, v6

    if-ne v6, v10, :cond_4

    .line 468
    array-length v6, v3

    add-int/lit8 v6, v6, -0x2

    invoke-static {v2, v3, v6}, Lorg/simalliance/openmobileapi/service/Terminal;->appendResponse([B[BI)[B

    move-result-object v2

    goto :goto_2

    .line 470
    :cond_4
    array-length v6, v3

    invoke-static {v2, v3, v6}, Lorg/simalliance/openmobileapi/service/Terminal;->appendResponse([B[BI)[B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v2

    .line 474
    move-object v3, v2

    goto/16 :goto_0
.end method

.method public declared-synchronized resetAccessControl()V
    .locals 1

    .prologue
    .line 607
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 608
    :cond_0
    monitor-exit p0

    return-void

    .line 607
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public select()V
    .locals 7

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mSelectResponse:[B

    .line 248
    const/4 v0, 0x5

    new-array v1, v0, [B

    .line 249
    .local v1, "selectCommand":[B
    aput-byte v3, v1, v3

    .line 250
    const/4 v0, 0x1

    const/16 v2, -0x5c

    aput-byte v2, v1, v0

    .line 251
    aput-byte v5, v1, v4

    .line 252
    const/4 v0, 0x3

    aput-byte v3, v1, v0

    .line 253
    aput-byte v3, v1, v5

    .line 255
    const/4 v2, 0x2

    const v3, 0x9000

    const v4, 0xffff

    :try_start_0
    const-string v5, "SELECT"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/Terminal;->transmit([BIIILjava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mSelectResponse:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 260
    return-void

    .line 257
    :catch_0
    move-exception v6

    .line 258
    .local v6, "exp":Ljava/lang/Exception;
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public select([B)V
    .locals 7
    .param p1, "aid"    # [B

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 279
    if-nez p1, :cond_0

    .line 280
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v2, "aid must not be null"

    invoke-direct {v0, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mSelectResponse:[B

    .line 283
    array-length v0, p1

    add-int/lit8 v0, v0, 0x6

    new-array v1, v0, [B

    .line 284
    .local v1, "selectCommand":[B
    aput-byte v3, v1, v3

    .line 285
    const/4 v0, 0x1

    const/16 v2, -0x5c

    aput-byte v2, v1, v0

    .line 286
    aput-byte v5, v1, v4

    .line 287
    const/4 v0, 0x3

    aput-byte v3, v1, v0

    .line 288
    array-length v0, p1

    int-to-byte v0, v0

    aput-byte v0, v1, v5

    .line 289
    const/4 v0, 0x5

    array-length v2, p1

    invoke-static {p1, v3, v1, v0, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 291
    const/4 v2, 0x2

    const v3, 0x9000

    const v4, 0xffff

    :try_start_0
    const-string v5, "SELECT"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lorg/simalliance/openmobileapi/service/Terminal;->transmit([BIIILjava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mSelectResponse:[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    return-void

    .line 292
    :catch_0
    move-exception v6

    .line 293
    .local v6, "exp":Ljava/lang/Exception;
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setUpChannelAccess(Landroid/content/pm/PackageManager;[BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;
    .locals 2
    .param p1, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p2, "aid"    # [B
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "callback"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    .prologue
    .line 588
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    if-nez v0, :cond_0

    .line 589
    new-instance v0, Ljava/security/AccessControlException;

    const-string v1, "Access Control Enforcer not properly set up"

    invoke-direct {v0, v1}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 591
    :cond_0
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0, p1}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->setPackageManager(Landroid/content/pm/PackageManager;)V

    .line 592
    iget-object v0, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mAccessControlEnforcer:Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;

    invoke-virtual {v0, p2, p3, p4}, Lorg/simalliance/openmobileapi/service/security/AccessControlEnforcer;->setUpChannelAccess([BLjava/lang/String;Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;)Lorg/simalliance/openmobileapi/service/security/ChannelAccess;

    move-result-object v0

    return-object v0
.end method

.method public simIOExchange(ILjava/lang/String;[B)[B
    .locals 2
    .param p1, "fileID"    # I
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "cmd"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 578
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "SIM IO error!"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized transmit([BIIILjava/lang/String;)[B
    .locals 8
    .param p1, "cmd"    # [B
    .param p2, "minRspLength"    # I
    .param p3, "swExpected"    # I
    .param p4, "swMask"    # I
    .param p5, "commandName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/simalliance/openmobileapi/service/CardException;
        }
    .end annotation

    .prologue
    .line 520
    monitor-enter p0

    const/4 v1, 0x0

    .line 521
    .local v1, "rsp":[B
    :try_start_0
    iget-object v6, p0, Lorg/simalliance/openmobileapi/service/Terminal;->mLock:Ljava/lang/Object;

    monitor-enter v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 523
    :try_start_1
    invoke-virtual {p0, p1}, Lorg/simalliance/openmobileapi/service/Terminal;->protocolTransmit([B)[B
    :try_end_1
    .catch Lorg/simalliance/openmobileapi/service/CardException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 531
    if-lez p2, :cond_2

    .line 532
    if-eqz v1, :cond_0

    :try_start_2
    array-length v5, v1

    if-ge v5, p2, :cond_2

    .line 533
    :cond_0
    new-instance v5, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v7, "response too small"

    invoke-static {p5, v7}, Lorg/simalliance/openmobileapi/service/Terminal;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 547
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 520
    :catchall_1
    move-exception v5

    monitor-exit p0

    throw v5

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Lorg/simalliance/openmobileapi/service/CardException;
    if-nez p5, :cond_1

    .line 526
    :try_start_4
    throw v0

    .line 528
    :cond_1
    new-instance v5, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v7, "transmit failed"

    invoke-static {p5, v7}, Lorg/simalliance/openmobileapi/service/Terminal;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7, v0}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    .line 536
    .end local v0    # "e":Lorg/simalliance/openmobileapi/service/CardException;
    :cond_2
    if-eqz p4, :cond_5

    .line 537
    if-eqz v1, :cond_3

    array-length v5, v1

    const/4 v7, 0x2

    if-ge v5, v7, :cond_4

    .line 538
    :cond_3
    new-instance v5, Lorg/simalliance/openmobileapi/service/CardException;

    const-string v7, "SW1/2 not available"

    invoke-static {p5, v7}, Lorg/simalliance/openmobileapi/service/Terminal;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 540
    :cond_4
    array-length v5, v1

    add-int/lit8 v5, v5, -0x2

    aget-byte v5, v1, v5

    and-int/lit16 v3, v5, 0xff

    .line 541
    .local v3, "sw1":I
    array-length v5, v1

    add-int/lit8 v5, v5, -0x1

    aget-byte v5, v1, v5

    and-int/lit16 v4, v5, 0xff

    .line 542
    .local v4, "sw2":I
    shl-int/lit8 v5, v3, 0x8

    or-int v2, v5, v4

    .line 543
    .local v2, "sw":I
    and-int v5, v2, p4

    and-int v7, p3, p4

    if-eq v5, v7, :cond_5

    .line 544
    new-instance v5, Lorg/simalliance/openmobileapi/service/CardException;

    invoke-static {p5, v2}, Lorg/simalliance/openmobileapi/service/Terminal;->createMessage(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lorg/simalliance/openmobileapi/service/CardException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 547
    .end local v2    # "sw":I
    .end local v3    # "sw1":I
    .end local v4    # "sw2":I
    :cond_5
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 548
    monitor-exit p0

    return-object v1
.end method
