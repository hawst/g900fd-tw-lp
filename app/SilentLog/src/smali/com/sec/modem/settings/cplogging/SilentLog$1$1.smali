.class Lcom/sec/modem/settings/cplogging/SilentLog$1$1;
.super Ljava/lang/Thread;
.source "SilentLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/modem/settings/cplogging/SilentLog$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/modem/settings/cplogging/SilentLog$1;


# direct methods
.method constructor <init>(Lcom/sec/modem/settings/cplogging/SilentLog$1;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1$1;->this$1:Lcom/sec/modem/settings/cplogging/SilentLog$1;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 537
    const/4 v3, 0x1

    invoke-super {p0, v3}, Ljava/lang/Thread;->setPriority(I)V

    .line 540
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1$1;->this$1:Lcom/sec/modem/settings/cplogging/SilentLog$1;

    iget-object v4, v4, Lcom/sec/modem/settings/cplogging/SilentLog$1;->val$command:[Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    # setter for: Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;
    invoke-static {v3}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$002(Ljava/lang/Process;)Ljava/lang/Process;

    .line 543
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v3, Ljava/io/InputStreamReader;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$000()Ljava/lang/Process;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 546
    .local v2, "reader":Ljava/io/BufferedReader;
    const-string v3, "SilentLog"

    const-string v4, "diag_mdlog reading start "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 553
    const-wide/16 v4, 0xa

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 557
    .end local v1    # "line":Ljava/lang/String;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v3, "SilentLog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 562
    # getter for: Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$000()Ljava/lang/Process;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    .line 564
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 555
    .restart local v1    # "line":Ljava/lang/String;
    .restart local v2    # "reader":Ljava/io/BufferedReader;
    :cond_0
    :try_start_2
    const-string v3, "SilentLog"

    const-string v4, "diag_mdlog reading end "

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 562
    # getter for: Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$000()Ljava/lang/Process;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Process;->destroy()V

    goto :goto_1

    .end local v1    # "line":Ljava/lang/String;
    .end local v2    # "reader":Ljava/io/BufferedReader;
    :catchall_0
    move-exception v3

    # getter for: Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$000()Ljava/lang/Process;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Process;->destroy()V

    throw v3
.end method
