.class Lcom/sec/modem/settings/cplogging/SilentLog$1;
.super Ljava/lang/Thread;
.source "SilentLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/modem/settings/cplogging/SilentLog;->startstop(Ljava/lang/Boolean;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

.field final synthetic val$command:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/modem/settings/cplogging/SilentLog;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    iput-object p2, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->val$command:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 526
    const/4 v0, 0x6

    .line 527
    .local v0, "CheckExistPidFileRetry":I
    const/4 v3, 0x0

    .line 528
    .local v3, "mdlog_started":Z
    const/4 v2, 0x1

    .line 530
    .local v2, "mdlog_keep":Z
    :goto_0
    if-eqz v2, :cond_2

    .line 532
    :try_start_0
    const-string v4, "SilentLog"

    const-string v5, "start stdoutThread!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    new-instance v5, Lcom/sec/modem/settings/cplogging/SilentLog$1$1;

    invoke-direct {v5, p0}, Lcom/sec/modem/settings/cplogging/SilentLog$1$1;-><init>(Lcom/sec/modem/settings/cplogging/SilentLog$1;)V

    iput-object v5, v4, Lcom/sec/modem/settings/cplogging/SilentLog;->stdoutThread:Ljava/lang/Thread;

    .line 567
    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    iget-object v4, v4, Lcom/sec/modem/settings/cplogging/SilentLog;->stdoutThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 568
    const/4 v0, 0x6

    .line 569
    :cond_0
    :goto_1
    if-lez v0, :cond_1

    .line 571
    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    .line 572
    const-string v4, "SilentLog"

    const-string v5, "checking diag process running.."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isDiagPID()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 576
    const-string v4, "dev.silentlog.on"

    const-string v5, "On"

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v4, "SilentLog"

    const-string v5, "diag process running ok : property set On"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 578
    const/4 v3, 0x1

    .line 592
    :cond_1
    if-ne v3, v8, :cond_8

    .line 596
    :try_start_1
    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    iget-object v4, v4, Lcom/sec/modem/settings/cplogging/SilentLog;->stdoutThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->join()V

    .line 597
    const-string v4, "SilentLog"

    const-string v5, "stdout of mdlog finished."

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 604
    :try_start_2
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isDiagPID()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 606
    const-string v4, "SilentLog"

    const-string v5, "detected unexpected termination of diag_mdlog. start again mdlog"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v2, 0x1

    .line 609
    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    # invokes: Lcom/sec/modem/settings/cplogging/SilentLog;->change_file_name()V
    invoke-static {v4}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$200(Lcom/sec/modem/settings/cplogging/SilentLog;)V

    goto :goto_0

    .line 619
    :catch_0
    move-exception v4

    .line 621
    :cond_2
    return-void

    .line 581
    :cond_3
    add-int/lit8 v0, v0, -0x1

    .line 582
    if-nez v0, :cond_0

    .line 585
    const-string v4, "SilentLog"

    const-string v5, "diag process : failed"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    const-string v4, "dev.silentlog.on"

    const-string v5, ""

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    const-string v5, "Silent Log"

    const-string v6, "Start Silent Log Failed "

    const/4 v7, 0x1

    # invokes: Lcom/sec/modem/settings/cplogging/SilentLog;->alertPopup(Ljava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v4, v5, v6, v7}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$100(Lcom/sec/modem/settings/cplogging/SilentLog;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 588
    const/4 v3, 0x0

    goto :goto_1

    .line 612
    :cond_4
    const/4 v2, 0x0

    .line 614
    goto/16 :goto_0

    .line 599
    :catch_1
    move-exception v1

    .line 600
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v4, "SilentLog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Interrupted Exception during wait for stdout from mdlog :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 604
    :try_start_4
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isDiagPID()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 606
    const-string v4, "SilentLog"

    const-string v5, "detected unexpected termination of diag_mdlog. start again mdlog"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v2, 0x1

    .line 609
    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    # invokes: Lcom/sec/modem/settings/cplogging/SilentLog;->change_file_name()V
    invoke-static {v4}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$200(Lcom/sec/modem/settings/cplogging/SilentLog;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 612
    :cond_5
    const/4 v2, 0x0

    .line 614
    goto/16 :goto_0

    .line 601
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v1

    .line 602
    .local v1, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v4, "SilentLog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 604
    :try_start_6
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isDiagPID()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 606
    const-string v4, "SilentLog"

    const-string v5, "detected unexpected termination of diag_mdlog. start again mdlog"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v2, 0x1

    .line 609
    iget-object v4, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    # invokes: Lcom/sec/modem/settings/cplogging/SilentLog;->change_file_name()V
    invoke-static {v4}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$200(Lcom/sec/modem/settings/cplogging/SilentLog;)V

    goto/16 :goto_0

    .line 612
    :cond_6
    const/4 v2, 0x0

    .line 614
    goto/16 :goto_0

    .line 604
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isDiagPID()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 606
    const-string v5, "SilentLog"

    const-string v6, "detected unexpected termination of diag_mdlog. start again mdlog"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const/4 v2, 0x1

    .line 609
    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLog;

    # invokes: Lcom/sec/modem/settings/cplogging/SilentLog;->change_file_name()V
    invoke-static {v5}, Lcom/sec/modem/settings/cplogging/SilentLog;->access$200(Lcom/sec/modem/settings/cplogging/SilentLog;)V

    .line 612
    :goto_2
    throw v4
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_2

    .line 616
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
