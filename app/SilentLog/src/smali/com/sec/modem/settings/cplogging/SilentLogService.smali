.class public Lcom/sec/modem/settings/cplogging/SilentLogService;
.super Landroid/app/Service;
.source "SilentLogService.java"


# instance fields
.field private SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

.field mHandler:Landroid/os/Handler;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

    .line 16
    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;

    .line 58
    new-instance v0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;

    invoke-direct {v0, p0}, Lcom/sec/modem/settings/cplogging/SilentLogService$1;-><init>(Lcom/sec/modem/settings/cplogging/SilentLogService;)V

    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/modem/settings/cplogging/SilentLogService;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentLogService;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/modem/settings/cplogging/SilentLogService;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentLogService;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;

    return-object p1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 20
    const-string v0, "SilentLogService"

    const-string v1, "onBind"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 26
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 27
    const-string v0, "SilentLogService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 33
    const-string v0, "SilentLogService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

    .line 39
    :cond_0
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 44
    const-string v2, "SilentLogService"

    const-string v3, "onStart"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    if-eqz p1, :cond_2

    .line 47
    const-string v2, "action"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 48
    .local v0, "nAction":I
    const-string v2, "modem_profile"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "strModemProfile":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

    if-nez v2, :cond_0

    new-instance v2, Lcom/sec/modem/settings/cplogging/SilentLog;

    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/sec/modem/settings/cplogging/SilentLog;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

    .line 51
    :cond_0
    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

    const-string v3, "default"

    invoke-virtual {v2, v0, v3}, Lcom/sec/modem/settings/cplogging/SilentLog;->doAction(ILjava/lang/String;)Z

    .line 56
    .end local v0    # "nAction":I
    .end local v1    # "strModemProfile":Ljava/lang/String;
    :goto_0
    return-void

    .line 52
    .restart local v0    # "nAction":I
    .restart local v1    # "strModemProfile":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentLogService;->SLog:Lcom/sec/modem/settings/cplogging/SilentLog;

    invoke-virtual {v2, v0, v1}, Lcom/sec/modem/settings/cplogging/SilentLog;->doAction(ILjava/lang/String;)Z

    goto :goto_0

    .line 54
    .end local v0    # "nAction":I
    .end local v1    # "strModemProfile":Ljava/lang/String;
    :cond_2
    const-string v2, "SilentLogService"

    const-string v3, "onStart : null intent"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
