.class Lcom/sec/modem/settings/cplogging/SilentRilLog$2;
.super Ljava/lang/Thread;
.source "SilentRilLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/modem/settings/cplogging/SilentRilLog;->stopLogcat()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;


# direct methods
.method constructor <init>(Lcom/sec/modem/settings/cplogging/SilentRilLog;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 133
    const-string v3, "SilentRilLog"

    const-string v4, "stopLogcat()"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogtimeLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$200(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 138
    :try_start_0
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mRunningStartedTime:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$300(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Calendar;

    move-result-object v5

    # setter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLastStartedTime:Ljava/util/Calendar;
    invoke-static {v3, v5}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$702(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 139
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    # setter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLastStoppedTime:Ljava/util/Calendar;
    invoke-static {v3, v5}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$802(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 140
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLastStoppedTime:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$800(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    # invokes: Lcom/sec/modem/settings/cplogging/SilentRilLog;->getFilenameTime(Ljava/util/Date;)Ljava/lang/String;
    invoke-static {v3, v5}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$400(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "filenameTime":Ljava/lang/String;
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 143
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v4

    monitor-enter v4

    .line 144
    :try_start_1
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/modem/settings/cplogging/LogcatProcess;

    .line 145
    .local v2, "prc":Lcom/sec/modem/settings/cplogging/LogcatProcess;
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$000(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/sec/modem/settings/cplogging/LogcatProcess;->stop(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 148
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "prc":Lcom/sec/modem/settings/cplogging/LogcatProcess;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 141
    .end local v0    # "filenameTime":Ljava/lang/String;
    :catchall_1
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 147
    .restart local v0    # "filenameTime":Ljava/lang/String;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_3
    iget-object v3, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->clear()V

    .line 148
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 151
    return-void
.end method
