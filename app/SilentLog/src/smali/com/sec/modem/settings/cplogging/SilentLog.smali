.class public Lcom/sec/modem/settings/cplogging/SilentLog;
.super Ljava/lang/Object;
.source "SilentLog.java"


# static fields
.field private static diagProcess:Ljava/lang/Process;

.field private static diagkillProcess:Ljava/lang/Process;

.field private static mRillog:Lcom/sec/modem/settings/cplogging/SilentRilLog;


# instance fields
.field ObserverThread:Ljava/lang/Thread;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mSLogThread:Ljava/lang/Thread;

.field stdoutThread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/sec/modem/settings/cplogging/SilentRilLog;

    invoke-direct {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;-><init>()V

    sput-object v0, Lcom/sec/modem/settings/cplogging/SilentLog;->mRillog:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mContext:Landroid/content/Context;

    .line 72
    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mHandler:Landroid/os/Handler;

    .line 73
    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mSLogThread:Ljava/lang/Thread;

    .line 132
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mContext:Landroid/content/Context;

    .line 133
    iput-object p2, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mHandler:Landroid/os/Handler;

    .line 134
    return-void
.end method

.method private BootupProcess(Ljava/lang/String;)V
    .locals 3
    .param p1, "strModemProfile"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 390
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isEnabled()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 391
    const-string v0, "SilentLog"

    const-string v1, "Silent Log auto start"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sec/modem/settings/cplogging/SilentLog;->startstop(Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 394
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Process;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/Process;)Ljava/lang/Process;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Process;

    .prologue
    .line 31
    sput-object p0, Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;

    return-object p0
.end method

.method static synthetic access$100(Lcom/sec/modem/settings/cplogging/SilentLog;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentLog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/modem/settings/cplogging/SilentLog;->alertPopup(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/modem/settings/cplogging/SilentLog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentLog;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/modem/settings/cplogging/SilentLog;->change_file_name()V

    return-void
.end method

.method private alertPopup(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "strTitle"    # Ljava/lang/String;
    .param p2, "strMessage"    # Ljava/lang/String;
    .param p3, "bLong"    # Z

    .prologue
    .line 154
    iget-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 155
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 156
    .local v0, "bd":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v2, "message"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    if-eqz p3, :cond_1

    const-string v2, "duration"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 162
    :goto_0
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 163
    .local v1, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 164
    iget-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 166
    .end local v0    # "bd":Landroid/os/Bundle;
    .end local v1    # "msg":Landroid/os/Message;
    :cond_0
    return-void

    .line 160
    .restart local v0    # "bd":Landroid/os/Bundle;
    :cond_1
    const-string v2, "duration"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private change_file_name()V
    .locals 12

    .prologue
    .line 703
    const-string v7, "SilentLog"

    const-string v8, "start change_file_name()"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    const-string v4, "/sdcard/log/cp/silent_logs"

    .line 705
    .local v4, "logPath":Ljava/lang/String;
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyyMMdd_HHmmss"

    invoke-direct {v5, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 707
    .local v5, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/io/File;

    const-string v7, "/sdcard/log/cp/silent_logs"

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 708
    .local v3, "logDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 710
    .local v1, "fileList":[Ljava/io/File;
    const/4 v7, 0x1

    invoke-direct {p0, v1, v7}, Lcom/sec/modem/settings/cplogging/SilentLog;->findLastModifiedIndex([Ljava/io/File;Z)I

    move-result v2

    .line 712
    .local v2, "lastModifiedIndex":I
    if-gez v2, :cond_0

    .line 713
    const-string v7, "SilentLog"

    const-string v8, "Directory is not exist."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    :goto_0
    return-void

    .line 717
    :cond_0
    const-string v7, "SilentLog"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Last Modified DIR: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v1, v2

    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    aget-object v3, v1, v2

    .line 719
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 721
    const/4 v7, 0x0

    invoke-direct {p0, v1, v7}, Lcom/sec/modem/settings/cplogging/SilentLog;->findLastModifiedIndex([Ljava/io/File;Z)I

    move-result v2

    .line 723
    if-gez v2, :cond_1

    .line 724
    const-string v7, "SilentLog"

    const-string v8, "File is not exist."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 728
    :cond_1
    new-instance v6, Ljava/io/File;

    aget-object v7, v1, v2

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 730
    .local v6, "tempFilePath":Ljava/io/File;
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/diag_log_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".qmdl"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 732
    const-string v7, "SilentLog"

    const-string v8, "File name change fail"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    :cond_2
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/sdcard/log/cp/silent_logs/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 734
    const-string v7, "SilentLog"

    const-string v8, "Directory name change fail."

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    :cond_3
    const-wide/16 v8, 0x3e8

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 738
    :catch_0
    move-exception v0

    .line 739
    .local v0, "e":Ljava/lang/Exception;
    const-string v7, "SilentLog"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public static deleteConfigFile()V
    .locals 2

    .prologue
    .line 273
    new-instance v0, Ljava/io/File;

    const-string v1, "/sdcard/SilentLogQcomm"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 276
    .local v0, "SLogConfigFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 280
    :cond_0
    return-void
.end method

.method private deleteTempFiles()V
    .locals 7

    .prologue
    .line 228
    :try_start_0
    const-string v4, "SilentLog"

    const-string v5, "Delete Temporary file Segments on boot up"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/16 v4, 0x1e

    if-ge v3, v4, :cond_1

    .line 232
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/mnt/sdcard/diag_log_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".qmdl"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 233
    .local v2, "filestring":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 234
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 235
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 238
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filestring":Ljava/lang/String;
    .end local v3    # "i":I
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SilentLog"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    return-void
.end method

.method private findLastModifiedIndex([Ljava/io/File;Z)I
    .locals 6
    .param p1, "fileList"    # [Ljava/io/File;
    .param p2, "isDirectory"    # Z

    .prologue
    .line 744
    const-wide/16 v2, -0x1

    .line 745
    .local v2, "lastModified":J
    const/4 v1, -0x1

    .line 747
    .local v1, "lastModifiedIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_2

    .line 748
    if-eqz p2, :cond_1

    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 749
    :goto_1
    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-lez v4, :cond_0

    .line 750
    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 751
    move v1, v0

    .line 747
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 748
    :cond_1
    aget-object v4, p1, v0

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_0

    goto :goto_1

    .line 755
    :cond_2
    return v1
.end method

.method public static getAvailableInternalMemorySize()J
    .locals 8

    .prologue
    .line 91
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    .line 92
    .local v4, "path":Ljava/io/File;
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 93
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    .line 94
    .local v2, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    .line 95
    .local v0, "availableBlocks":J
    mul-long v6, v0, v2

    return-wide v6
.end method

.method public static getTotalInternalMemorySize()J
    .locals 8

    .prologue
    .line 99
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v2

    .line 100
    .local v2, "path":Ljava/io/File;
    new-instance v3, Landroid/os/StatFs;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 101
    .local v3, "stat":Landroid/os/StatFs;
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v0, v6

    .line 102
    .local v0, "blockSize":J
    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockCount()I

    move-result v6

    int-to-long v4, v6

    .line 103
    .local v4, "totalBlocks":J
    mul-long v6, v4, v0

    return-wide v6
.end method

.method public static isDiagPID()Z
    .locals 4

    .prologue
    .line 375
    const/4 v2, 0x0

    .line 378
    .local v2, "is_DiagPID":Z
    new-instance v0, Ljava/io/File;

    const-string v3, "/sdcard/diag_logs/diag_pid"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 379
    .local v0, "SLogConfigFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 382
    :cond_0
    new-instance v1, Ljava/io/File;

    const-string v3, "/sdcard/log/cp/silent_logs/diag_pid"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 383
    .local v1, "SLogConfigFile2":Ljava/io/File;
    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    .line 385
    :cond_1
    return v2
.end method

.method public static isEnabled()Z
    .locals 3

    .prologue
    .line 323
    const/4 v1, 0x0

    .line 325
    .local v1, "isConfigFile":Z
    new-instance v0, Ljava/io/File;

    const-string v2, "/sdcard/SilentLogQcomm"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 327
    .local v0, "SLogConfigFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 328
    const/4 v1, 0x1

    .line 331
    :cond_0
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isEnabledAlways()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 336
    const/4 v3, 0x0

    .line 338
    .local v3, "isConfigFile":Z
    new-array v6, v7, [Ljava/lang/String;

    const-string v9, "/mnt/extSdCard"

    aput-object v9, v6, v8

    .line 340
    .local v6, "pathToTry":[Ljava/lang/String;
    move-object v1, v6

    .local v1, "arr$":[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v1, v2

    .line 342
    .local v5, "logPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "SilentLogQcomm"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 344
    .local v0, "SLogConfigFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 345
    const/4 v3, 0x1

    .line 350
    .end local v0    # "SLogConfigFile":Ljava/io/File;
    .end local v5    # "logPath":Ljava/lang/String;
    :cond_0
    if-eqz v3, :cond_2

    :goto_1
    return v7

    .line 340
    .restart local v0    # "SLogConfigFile":Ljava/io/File;
    .restart local v5    # "logPath":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "SLogConfigFile":Ljava/io/File;
    .end local v5    # "logPath":Ljava/lang/String;
    :cond_2
    move v7, v8

    .line 350
    goto :goto_1
.end method

.method public static isEnabledRIL()Z
    .locals 3

    .prologue
    .line 355
    const/4 v1, 0x0

    .line 358
    .local v1, "isConfigFile":Z
    new-instance v0, Ljava/io/File;

    const-string v2, "/sdcard/SilentLogRIL"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 359
    .local v0, "SLogConfigFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 361
    :cond_0
    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isSGLTEModel()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 426
    new-array v0, v3, [Ljava/lang/String;

    const-string v5, "sglte2"

    aput-object v5, v0, v4

    .line 427
    .local v0, "SGLTESolutionArray":[Ljava/lang/String;
    const-string v5, "ro.baseband"

    const-string v6, "NONE"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 429
    .local v2, "solution":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_1

    .line 430
    aget-object v5, v0, v1

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v3, :cond_0

    .line 434
    :goto_1
    return v3

    .line 429
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v3, v4

    .line 434
    goto :goto_1
.end method

.method public static isStarted()Z
    .locals 2

    .prologue
    .line 365
    const-string v1, "dev.silentlog.on"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 366
    .local v0, "prop":Ljava/lang/String;
    const-string v1, "On"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    const/4 v1, 0x1

    .line 369
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startStopForegroundIfPossible(Ljava/lang/Boolean;)V
    .locals 13
    .param p1, "enable"    # Ljava/lang/Boolean;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 397
    iget-object v7, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mContext:Landroid/content/Context;

    instance-of v7, v7, Landroid/app/Service;

    if-eqz v7, :cond_1

    .line 398
    iget-object v6, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Service;

    .line 399
    .local v6, "svc":Landroid/app/Service;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 400
    const/high16 v3, 0x7f020000

    .line 401
    .local v3, "icon":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 403
    .local v8, "when":J
    const-string v2, "SilentLog"

    .line 404
    .local v2, "contentTitle":Ljava/lang/CharSequence;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " started."

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 405
    .local v1, "contentText":Ljava/lang/CharSequence;
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isEnabledRIL()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 406
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " (AP log running, too)"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 409
    :cond_0
    new-instance v4, Landroid/app/Notification;

    invoke-direct {v4, v3, v1, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 411
    .local v4, "notification":Landroid/app/Notification;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 412
    .local v5, "notificationIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.servicemodeapp"

    const-string v10, "com.sec.android.app.servicemodeapp.SysDump"

    invoke-virtual {v5, v7, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 413
    iget-object v7, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mContext:Landroid/content/Context;

    invoke-static {v7, v11, v5, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 415
    .local v0, "contentIntent":Landroid/app/PendingIntent;
    iget-object v7, p0, Lcom/sec/modem/settings/cplogging/SilentLog;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7, v2, v1, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 416
    iget v7, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v4, Landroid/app/Notification;->flags:I

    .line 418
    invoke-virtual {v6, v12, v4}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    .line 423
    .end local v0    # "contentIntent":Landroid/app/PendingIntent;
    .end local v1    # "contentText":Ljava/lang/CharSequence;
    .end local v2    # "contentTitle":Ljava/lang/CharSequence;
    .end local v3    # "icon":I
    .end local v4    # "notification":Landroid/app/Notification;
    .end local v5    # "notificationIntent":Landroid/content/Intent;
    .end local v6    # "svc":Landroid/app/Service;
    .end local v8    # "when":J
    :cond_1
    :goto_0
    return-void

    .line 420
    .restart local v6    # "svc":Landroid/app/Service;
    :cond_2
    invoke-virtual {v6, v12}, Landroid/app/Service;->stopForeground(Z)V

    goto :goto_0
.end method

.method private startstop(Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 26
    .param p1, "Enable"    # Ljava/lang/Boolean;
    .param p2, "strModemProfile"    # Ljava/lang/String;

    .prologue
    .line 437
    new-instance v5, Landroid/content/Intent;

    const-string v17, "com.sec.modem.settings.cplogging.SILENTLOG"

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 438
    .local v5, "SilentLogIntent":Landroid/content/Intent;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    if-eqz v17, :cond_9

    .line 440
    const-string v17, "ro.debug_level"

    const-string v22, "Unknown"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 441
    .local v16, "ramdumpstate":Ljava/lang/String;
    const-string v17, "0x494d"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1

    const-string v17, "0x4948"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_1

    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isEnabledAlways()Z

    move-result v17

    if-nez v17, :cond_1

    .line 443
    const-string v17, "SilentLog"

    const-string v22, "SILENTLOG SKIPPED by DEBUG LEVEL"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    const-string v17, "Silent Log failed"

    const-string v22, "check DEBUG LEVEL"

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/modem/settings/cplogging/SilentLog;->alertPopup(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 700
    .end local v16    # "ramdumpstate":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 449
    .restart local v16    # "ramdumpstate":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->getTotalInternalMemorySize()J

    move-result-wide v22

    const-wide/16 v24, 0x400

    div-long v22, v22, v24

    const-wide/16 v24, 0x400

    div-long v18, v22, v24

    .line 450
    .local v18, "total1":J
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->getAvailableInternalMemorySize()J

    move-result-wide v22

    const-wide/16 v24, 0x400

    div-long v22, v22, v24

    const-wide/16 v24, 0x400

    div-long v20, v22, v24

    .line 452
    .local v20, "total2":J
    const-wide/16 v22, 0x1c2

    cmp-long v17, v20, v22

    if-gtz v17, :cond_2

    .line 454
    const-string v17, "Not enough memory!"

    const-string v22, "Logging failed"

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/modem/settings/cplogging/SilentLog;->alertPopup(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 459
    :cond_2
    const-string v17, "net.service.silentlog.size"

    invoke-static/range {v17 .. v17}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 461
    .local v15, "logSize":Ljava/lang/String;
    const-string v17, ""

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 463
    const-string v15, "100"

    .line 466
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/modem/settings/cplogging/SilentLog;->isSGLTEModel()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 467
    const/16 v17, 0x6

    move/from16 v0, v17

    new-array v10, v0, [Ljava/lang/String;

    .line 468
    .local v10, "command":[Ljava/lang/String;
    const-string v8, "-f/system/etc/Diag_SGLTE.cfg"

    .line 469
    .local v8, "cfgpath":Ljava/lang/String;
    const-string v9, "-m/system/etc/Diag_SGLTE.cfg"

    .line 471
    .local v9, "cfgpathforMDM":Ljava/lang/String;
    const/16 v17, 0x0

    const-string v22, "/system/bin/diag_mdlog_sglte"

    aput-object v22, v10, v17

    .line 472
    const/16 v17, 0x1

    aput-object v8, v10, v17

    .line 473
    const/16 v17, 0x2

    aput-object v9, v10, v17

    .line 474
    const/16 v17, 0x3

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "-s"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v10, v17

    .line 475
    const/16 v17, 0x4

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "-t"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v10, v17

    .line 476
    const/16 v17, 0x5

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "-a"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v10, v17

    .line 500
    :goto_1
    invoke-direct/range {p0 .. p1}, Lcom/sec/modem/settings/cplogging/SilentLog;->startStopForegroundIfPossible(Ljava/lang/Boolean;)V

    .line 502
    const-string v17, "SilentLog"

    const-string v22, "start silent logging"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    new-instance v12, Ljava/io/File;

    const/16 v17, 0x0

    aget-object v17, v10, v17

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 504
    .local v12, "f":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v6

    .line 505
    .local v6, "binExists":Z
    if-nez v6, :cond_7

    .line 507
    const-string v17, "SilentLog"

    const-string v22, "cannot find diag_mdlog"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 480
    .end local v6    # "binExists":Z
    .end local v8    # "cfgpath":Ljava/lang/String;
    .end local v9    # "cfgpathforMDM":Ljava/lang/String;
    .end local v10    # "command":[Ljava/lang/String;
    .end local v12    # "f":Ljava/io/File;
    :cond_4
    const/16 v17, 0x7

    move/from16 v0, v17

    new-array v10, v0, [Ljava/lang/String;

    .line 481
    .restart local v10    # "command":[Ljava/lang/String;
    const-string v8, "-f/system/etc/Diag"

    .line 482
    .restart local v8    # "cfgpath":Ljava/lang/String;
    const-string v9, "-m/system/etc/Diag"

    .line 484
    .restart local v9    # "cfgpathforMDM":Ljava/lang/String;
    const-string v17, "default"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v22, ".cfg"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 487
    :goto_2
    const-string v17, "default"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v22, ".cfg"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 490
    :goto_3
    const/16 v17, 0x0

    const-string v22, "/system/bin/diag_mdlog"

    aput-object v22, v10, v17

    .line 491
    const/16 v17, 0x1

    aput-object v8, v10, v17

    .line 492
    const/16 v17, 0x2

    aput-object v9, v10, v17

    .line 493
    const/16 v17, 0x3

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "-s"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v10, v17

    .line 494
    const/16 v17, 0x4

    const-string v22, "-n5"

    aput-object v22, v10, v17

    .line 495
    const/16 v17, 0x5

    const-string v22, "-c"

    aput-object v22, v10, v17

    .line 496
    const/16 v17, 0x6

    const-string v22, "-r"

    aput-object v22, v10, v17

    goto/16 :goto_1

    .line 485
    :cond_5
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v22, "_"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v22, ".cfg"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_2

    .line 488
    :cond_6
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v22, "_"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v22, ".cfg"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3

    .line 516
    .restart local v6    # "binExists":Z
    .restart local v12    # "f":Ljava/io/File;
    :cond_7
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isEnabledRIL()Z

    move-result v17

    if-eqz v17, :cond_8

    .line 518
    sget-object v17, Lcom/sec/modem/settings/cplogging/SilentLog;->mRillog:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->startLogcat()V

    .line 522
    :cond_8
    :try_start_0
    new-instance v17, Lcom/sec/modem/settings/cplogging/SilentLog$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v10}, Lcom/sec/modem/settings/cplogging/SilentLog$1;-><init>(Lcom/sec/modem/settings/cplogging/SilentLog;[Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/modem/settings/cplogging/SilentLog;->ObserverThread:Ljava/lang/Thread;

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/modem/settings/cplogging/SilentLog;->ObserverThread:Ljava/lang/Thread;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 626
    :catch_0
    move-exception v11

    .line 628
    .local v11, "e":Ljava/lang/Exception;
    const-string v17, "SilentLog"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Exception : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v11}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 634
    .end local v6    # "binExists":Z
    .end local v8    # "cfgpath":Ljava/lang/String;
    .end local v9    # "cfgpathforMDM":Ljava/lang/String;
    .end local v10    # "command":[Ljava/lang/String;
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v12    # "f":Ljava/io/File;
    .end local v15    # "logSize":Ljava/lang/String;
    .end local v16    # "ramdumpstate":Ljava/lang/String;
    .end local v18    # "total1":J
    .end local v20    # "total2":J
    :cond_9
    invoke-direct/range {p0 .. p1}, Lcom/sec/modem/settings/cplogging/SilentLog;->startStopForegroundIfPossible(Ljava/lang/Boolean;)V

    .line 637
    const-string v17, "GPSLogging"

    const-string v22, "STOP"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/modem/settings/cplogging/SilentLog;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 640
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->deleteConfigFile()V

    .line 644
    invoke-direct/range {p0 .. p0}, Lcom/sec/modem/settings/cplogging/SilentLog;->isSGLTEModel()Z

    move-result v17

    if-eqz v17, :cond_b

    .line 646
    const-string v14, "/system/bin/diag_mdlog_sglte"

    .line 651
    .local v14, "killprog":Ljava/lang/String;
    :goto_4
    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 652
    .local v13, "killf":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v7

    .line 654
    .local v7, "binExistskillf":Z
    if-eqz v7, :cond_d

    .line 657
    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v17

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, " --kill"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v17

    sput-object v17, Lcom/sec/modem/settings/cplogging/SilentLog;->diagkillProcess:Ljava/lang/Process;

    .line 658
    const-string v17, "SilentLog"

    const-string v22, "KILLING MDLOG\n"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    const/16 v4, 0x14

    .line 661
    .local v4, "CheckExistPidFileRetry":I
    :cond_a
    :goto_5
    if-lez v4, :cond_0

    .line 663
    const-wide/16 v22, 0x3e8

    invoke-static/range {v22 .. v23}, Ljava/lang/Thread;->sleep(J)V

    .line 664
    const-string v17, "SilentLog"

    const-string v22, "checking diag process is terminated.."

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 666
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isDiagPID()Z

    move-result v17

    if-nez v17, :cond_c

    .line 668
    const-string v17, "dev.silentlog.on"

    const-string v22, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    const-string v17, "SilentLog"

    const-string v22, "diag process none : property set null"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isEnabledRIL()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 672
    sget-object v17, Lcom/sec/modem/settings/cplogging/SilentLog;->mRillog:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->stopLogcat()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 686
    .end local v4    # "CheckExistPidFileRetry":I
    :catch_1
    move-exception v11

    .line 688
    .restart local v11    # "e":Ljava/lang/Exception;
    const-string v17, "SilentLog"

    const-string v22, "FAILED KILLING MDLOG\n"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 648
    .end local v7    # "binExistskillf":Z
    .end local v11    # "e":Ljava/lang/Exception;
    .end local v13    # "killf":Ljava/io/File;
    .end local v14    # "killprog":Ljava/lang/String;
    :cond_b
    const-string v14, "/system/bin/diag_mdlog"

    .restart local v14    # "killprog":Ljava/lang/String;
    goto/16 :goto_4

    .line 676
    .restart local v4    # "CheckExistPidFileRetry":I
    .restart local v7    # "binExistskillf":Z
    .restart local v13    # "killf":Ljava/io/File;
    :cond_c
    add-int/lit8 v4, v4, -0x1

    .line 677
    if-nez v4, :cond_a

    .line 680
    :try_start_2
    const-string v17, "SilentLog"

    const-string v22, "diag process : still alive abnormally"

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    const-string v17, "Silent Log"

    const-string v22, "Stop Silent Log failed. Please, Restart !! "

    const/16 v23, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/modem/settings/cplogging/SilentLog;->alertPopup(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_5

    .line 692
    .end local v4    # "CheckExistPidFileRetry":I
    :cond_d
    sget-object v17, Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;

    if-eqz v17, :cond_0

    .line 694
    sget-object v17, Lcom/sec/modem/settings/cplogging/SilentLog;->diagProcess:Ljava/lang/Process;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Process;->destroy()V

    .line 695
    const-string v17, "dev.silentlog.on"

    const-string v22, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private timeDelayDeleteIntent()V
    .locals 4

    .prologue
    .line 217
    const-string v1, "SilentLog"

    const-string v2, "timeDelayDeleteIntent is called by silent log delete intent"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    const-wide/16 v2, 0x1388

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    :goto_0
    return-void

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public doAction(ILjava/lang/String;)Z
    .locals 5
    .param p1, "nAction"    # I
    .param p2, "strModemProfile"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 170
    packed-switch p1, :pswitch_data_0

    .line 213
    :goto_0
    return v1

    .line 173
    :pswitch_0
    const-string v1, "SilentLog"

    const-string v3, "SILENT_LOG_BOOT_UP"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-direct {p0}, Lcom/sec/modem/settings/cplogging/SilentLog;->deleteTempFiles()V

    .line 176
    invoke-direct {p0, p2}, Lcom/sec/modem/settings/cplogging/SilentLog;->BootupProcess(Ljava/lang/String;)V

    :cond_0
    :goto_1
    move v1, v2

    .line 213
    goto :goto_0

    .line 179
    :pswitch_1
    const-string v3, "SilentLog"

    const-string v4, "SILENT_LOG_STARTSTOP"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isStarted()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 181
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/sec/modem/settings/cplogging/SilentLog;->startstop(Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_1

    .line 183
    :cond_1
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/sec/modem/settings/cplogging/SilentLog;->startstop(Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_1

    .line 187
    :pswitch_2
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isStarted()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 189
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/sec/modem/settings/cplogging/SilentLog;->startstop(Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_1

    .line 193
    :pswitch_3
    const-string v1, "SilentLog"

    const-string v3, "SILENT_LOG_DELETE_LOGS"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-direct {p0}, Lcom/sec/modem/settings/cplogging/SilentLog;->timeDelayDeleteIntent()V

    .line 195
    invoke-static {}, Lcom/sec/modem/settings/cplogging/SilentLog;->isDiagPID()Z

    move-result v1

    if-nez v1, :cond_2

    .line 198
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    const-string v3, "/system/bin/diag_mdlog --delete"

    invoke-virtual {v1, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 199
    const-string v1, "SilentLog"

    const-string v3, "exec is running"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SilentLog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 205
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v1, "SilentLog"

    const-string v3, "Diag PID is still exist"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
