.class Lcom/sec/modem/settings/cplogging/SilentRilLog$1;
.super Ljava/lang/Thread;
.source "SilentRilLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/modem/settings/cplogging/SilentRilLog;->startLogcat()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;


# direct methods
.method constructor <init>(Lcom/sec/modem/settings/cplogging/SilentRilLog;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 87
    const-string v0, "SilentRilLog"

    const-string v1, "startLogcat()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    const-wide/16 v0, 0x9c4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :goto_0
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    iget-object v1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # invokes: Lcom/sec/modem/settings/cplogging/SilentRilLog;->getTargetDir()Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$100(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$002(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/lang/String;)Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$000(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "SilentRilLog"

    const-string v1, "No directory to save"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :goto_1
    return-void

    .line 94
    :catch_0
    move-exception v6

    .line 95
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "SilentRilLog"

    const-string v1, "Sleep Exception"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 105
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_0
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogtimeLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$200(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 106
    :try_start_1
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    # setter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mRunningStartedTime:Ljava/util/Calendar;
    invoke-static {v0, v2}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$302(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 107
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    iget-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mRunningStartedTime:Ljava/util/Calendar;
    invoke-static {v2}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$300(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    # invokes: Lcom/sec/modem/settings/cplogging/SilentRilLog;->getFilenameTime(Ljava/util/Date;)Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$400(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 108
    .local v3, "filenameTime":Ljava/lang/String;
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 110
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v9

    monitor-enter v9

    .line 111
    :try_start_2
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # invokes: Lcom/sec/modem/settings/cplogging/SilentRilLog;->isInternalMemoryAvailable()Z
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$600(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v10

    new-instance v0, Lcom/sec/modem/settings/cplogging/LogcatProcess;

    iget-object v1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$000(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "main"

    const/16 v4, 0x3200

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/modem/settings/cplogging/LogcatProcess;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v10, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v10

    new-instance v0, Lcom/sec/modem/settings/cplogging/LogcatProcess;

    iget-object v1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$000(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "system"

    const/16 v4, 0x3200

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/modem/settings/cplogging/LogcatProcess;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v10, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v10

    new-instance v0, Lcom/sec/modem/settings/cplogging/LogcatProcess;

    iget-object v1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$000(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "radio"

    const/16 v4, 0x6400

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/sec/modem/settings/cplogging/LogcatProcess;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v10, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v10

    new-instance v0, Lcom/sec/modem/settings/cplogging/LogcatProcess;

    iget-object v1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$000(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "events"

    const/16 v4, 0x3200

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/modem/settings/cplogging/LogcatProcess;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v10, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 117
    const-string v0, "SilentRilLog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "vector size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v2}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentRilLog;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;
    invoke-static {v0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/modem/settings/cplogging/LogcatProcess;

    .line 119
    .local v8, "prc":Lcom/sec/modem/settings/cplogging/LogcatProcess;
    invoke-virtual {v8}, Lcom/sec/modem/settings/cplogging/LogcatProcess;->execute()Ljava/lang/Process;

    goto :goto_2

    .line 122
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "prc":Lcom/sec/modem/settings/cplogging/LogcatProcess;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 108
    .end local v3    # "filenameTime":Ljava/lang/String;
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 122
    .restart local v3    # "filenameTime":Ljava/lang/String;
    :cond_1
    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1
.end method
