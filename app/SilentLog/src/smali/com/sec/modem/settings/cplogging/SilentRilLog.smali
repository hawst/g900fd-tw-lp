.class public Lcom/sec/modem/settings/cplogging/SilentRilLog;
.super Ljava/lang/Object;
.source "SilentRilLog.java"


# instance fields
.field private mLastStartedTime:Ljava/util/Calendar;

.field private mLastStoppedTime:Ljava/util/Calendar;

.field private mLogcatProcessList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/modem/settings/cplogging/LogcatProcess;",
            ">;"
        }
    .end annotation
.end field

.field private mLogtimeLock:Ljava/lang/Object;

.field private mRunningStartedTime:Ljava/util/Calendar;

.field private mTargetDir:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogtimeLock:Ljava/lang/Object;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/util/Vector;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/Vector;-><init>(I)V

    iput-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mTargetDir:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->getTargetDir()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogtimeLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mRunningStartedTime:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;
    .param p1, "x1"    # Ljava/util/Calendar;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mRunningStartedTime:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Date;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;
    .param p1, "x1"    # Ljava/util/Date;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->getFilenameTime(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLogcatProcessList:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/modem/settings/cplogging/SilentRilLog;->isInternalMemoryAvailable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$702(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;
    .param p1, "x1"    # Ljava/util/Calendar;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLastStartedTime:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/modem/settings/cplogging/SilentRilLog;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLastStoppedTime:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/modem/settings/cplogging/SilentRilLog;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/modem/settings/cplogging/SilentRilLog;
    .param p1, "x1"    # Ljava/util/Calendar;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentRilLog;->mLastStoppedTime:Ljava/util/Calendar;

    return-object p1
.end method

.method private getFilenameTime(Ljava/util/Date;)Ljava/lang/String;
    .locals 5
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 156
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyMMddHHmmss(zzzz)"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 157
    .local v1, "formatter":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ":"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "filenameTime":Ljava/lang/String;
    return-object v0
.end method

.method private getTargetDir()Ljava/lang/String;
    .locals 5

    .prologue
    .line 46
    const-string v2, ""

    .line 47
    .local v2, "TargetDir":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    const-string v3, "/sdcard/log"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .local v0, "BaseDirectory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_0

    .line 51
    const-string v3, "SilentRilLog"

    const-string v4, "make sdcard/log dir."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 55
    :cond_0
    const-string v3, "SilentRilLog"

    const-string v4, "checking diag base Directory: /sdcard/log\n"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    const-string v2, "/sdcard/log/ap_silentlog"

    .line 58
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 59
    .local v1, "RilLogDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_1

    .line 60
    const-string v3, "SilentRilLog"

    const-string v4, "make sdcard/log/ap_silentlog dir."

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 64
    :cond_1
    return-object v2
.end method

.method private isInternalMemoryAvailable()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x400

    .line 31
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v6

    .line 32
    .local v6, "path":Ljava/io/File;
    new-instance v7, Landroid/os/StatFs;

    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 33
    .local v7, "stat":Landroid/os/StatFs;
    invoke-virtual {v7}, Landroid/os/StatFs;->getBlockSize()I

    move-result v8

    int-to-long v4, v8

    .line 34
    .local v4, "blockSize":J
    invoke-virtual {v7}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v0, v8

    .line 35
    .local v0, "availableBlocks":J
    mul-long v8, v0, v4

    div-long/2addr v8, v10

    div-long v2, v8, v10

    .line 37
    .local v2, "availableSize":J
    const-wide/16 v8, 0x1c2

    cmp-long v8, v2, v8

    if-gtz v8, :cond_0

    .line 38
    const-string v8, "SilentRilLog"

    const-string v9, "not enough available internal memory."

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    const/4 v8, 0x0

    .line 41
    :goto_0
    return v8

    :cond_0
    const/4 v8, 0x1

    goto :goto_0
.end method


# virtual methods
.method public startLogcat()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;

    invoke-direct {v0, p0}, Lcom/sec/modem/settings/cplogging/SilentRilLog$1;-><init>(Lcom/sec/modem/settings/cplogging/SilentRilLog;)V

    .line 127
    .local v0, "startThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 128
    return-void
.end method

.method public stopLogcat()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;

    invoke-direct {v0, p0}, Lcom/sec/modem/settings/cplogging/SilentRilLog$2;-><init>(Lcom/sec/modem/settings/cplogging/SilentRilLog;)V

    .line 153
    .local v0, "stopThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 154
    return-void
.end method
