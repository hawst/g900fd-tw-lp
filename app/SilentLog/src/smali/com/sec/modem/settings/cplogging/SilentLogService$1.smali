.class Lcom/sec/modem/settings/cplogging/SilentLogService$1;
.super Landroid/os/Handler;
.source "SilentLogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/modem/settings/cplogging/SilentLogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;


# direct methods
.method constructor <init>(Lcom/sec/modem/settings/cplogging/SilentLogService;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 60
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    .local v0, "bd":Landroid/os/Bundle;
    const-string v5, "duration"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 62
    .local v1, "nDuration":I
    const-string v5, "title"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 63
    .local v4, "strTitle":Ljava/lang/String;
    const-string v5, "message"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 64
    .local v2, "strMessage":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\n\n----- "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-----\n\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 66
    .local v3, "strMsgText":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/sec/modem/settings/cplogging/SilentLogService;->access$000(Lcom/sec/modem/settings/cplogging/SilentLogService;)Landroid/widget/Toast;

    move-result-object v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;

    iget-object v6, p0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;

    const-string v7, ""

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    # setter for: Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;
    invoke-static {v5, v6}, Lcom/sec/modem/settings/cplogging/SilentLogService;->access$002(Lcom/sec/modem/settings/cplogging/SilentLogService;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 67
    :cond_0
    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/sec/modem/settings/cplogging/SilentLogService;->access$000(Lcom/sec/modem/settings/cplogging/SilentLogService;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/sec/modem/settings/cplogging/SilentLogService;->access$000(Lcom/sec/modem/settings/cplogging/SilentLogService;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/Toast;->setDuration(I)V

    .line 69
    iget-object v5, p0, Lcom/sec/modem/settings/cplogging/SilentLogService$1;->this$0:Lcom/sec/modem/settings/cplogging/SilentLogService;

    # getter for: Lcom/sec/modem/settings/cplogging/SilentLogService;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/sec/modem/settings/cplogging/SilentLogService;->access$000(Lcom/sec/modem/settings/cplogging/SilentLogService;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 70
    return-void
.end method
