.class public Lcom/samsung/android/app/watchmanagerstub/MainActivity;
.super Landroid/app/Activity;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;,
        Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;,
        Lcom/samsung/android/app/watchmanagerstub/MainActivity$ProgressTimeTask;,
        Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$app$watchmanagerstub$MainActivity$VIEW_STAUS:[I = null

.field static final CHECKSERVERDIALOG:Ljava/lang/String; = "CHECK_SERVER_PROGRESSDIALOG"

.field static final CHECKSERVERDIALOGOFF:Ljava/lang/String; = "CHECK_SERVER_PROGRESSDIALOG"

.field static final DONT_NEED_TO_UPGRADE:I = 0xd

.field static final EXCEPTION:Ljava/lang/String; = "UPDATE_DOWNLOAD_EXCEPTION"

.field static final EXCEPTION_DOWNLOAD:I = 0xc

.field static final EXCEPTION_UPDATE:I = 0xb

.field static final MESSAGE_CMD:Ljava/lang/String; = "MESSAGE_CMD"

.field static final PROGRESSUPDATE:Ljava/lang/String; = "DOWNLODING_PROGRESS_UPDATE"

.field static final RATIO:Ljava/lang/String; = "DOWNLOA_RATIO"

.field public static final READY_TO_DONWLOAD:I = 0xe

.field static final SENDAPPSIZE:Ljava/lang/String; = "SEND_APPSIZE"

.field static final STATUS:Ljava/lang/String; = "DOWNLOAD_STATUS"

.field static final STATUS_DOWNLOADED:I = 0x1

.field static final STATUS_DOWNLOADING:I = 0x0

.field static final STATUS_ERROR_CANCELED:I = 0x6

.field static final STATUS_ERROR_DOWNLOAD_FAIL:I = 0xa

.field static final STATUS_ERROR_INSTALL_FAIL:I = 0x8

.field static final STATUS_ERROR_LATEST:I = 0x4

.field static final STATUS_ERROR_NETWORK:I = 0x7

.field static final STATUS_ERROR_NO_CONTENTS:I = 0x5

.field static final STATUS_ERROR_UPDATE_FAIL:I = 0x9

.field static final STATUS_INSTALLED:I = 0x3

.field static final STATUS_INSTALLING:I = 0x2

.field public static final TAG:Ljava/lang/String; = "WatchManagerStub"

.field static isSelfDownload:Z


# instance fields
.field private PREF_DONT_SHOW_AGAIN_DATA:Ljava/lang/String;

.field private PREF_DONT_SHOW_AGAIN_WIFI:Ljava/lang/String;

.field private bDoNotCheck:Z

.field private btAddress:Ljava/lang/String;

.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mAppSize:D

.field private mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private mCurrentStatus:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

.field private mEntireScrollview:Landroid/widget/ScrollView;

.field final mHandler:Landroid/os/Handler;

.field private mImageviewImage1:Landroid/widget/ImageView;

.field private mInstallButton:Landroid/widget/Button;

.field private mLayoutGearManagerDescription:Landroid/widget/LinearLayout;

.field private mLayoutImageview:Landroid/widget/LinearLayout;

.field private mMainActivity:Landroid/widget/LinearLayout;

.field private mNumberLanguage:I

.field private mProgressBarInstall:Landroid/widget/ProgressBar;

.field private mRatio:I

.field mReceiver:Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;

.field private mTextViewDownlodingSize:Landroid/widget/TextView;

.field private mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

.field private mTextViewGearManagerDescription:Landroid/widget/TextView;

.field private mTextViewGearManagerInformation:Landroid/widget/TextView;

.field private mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

.field private mTimer:Ljava/util/Timer;

.field private mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

.field private mlayout_install:Landroid/widget/RelativeLayout;

.field private postName:Ljava/lang/String;

.field private updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$app$watchmanagerstub$MainActivity$VIEW_STAUS()[I
    .locals 3

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->$SWITCH_TABLE$com$samsung$android$app$watchmanagerstub$MainActivity$VIEW_STAUS:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->values()[Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->DOWNLOADING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-virtual {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-virtual {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->INSTALLING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-virtual {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->$SWITCH_TABLE$com$samsung$android$app$watchmanagerstub$MainActivity$VIEW_STAUS:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isSelfDownload:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 99
    iput-boolean v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->bDoNotCheck:Z

    .line 102
    const-string v0, "pref_dont_show_again_wifi"

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->PREF_DONT_SHOW_AGAIN_WIFI:Ljava/lang/String;

    .line 103
    const-string v0, "pref_dont_show_again_data"

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->PREF_DONT_SHOW_AGAIN_DATA:Ljava/lang/String;

    .line 109
    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mCurrentStatus:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    .line 110
    iput-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    .line 119
    iput v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mRatio:I

    .line 132
    iput-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;

    .line 133
    iput-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    .line 134
    iput-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    .line 136
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mHandler:Landroid/os/Handler;

    .line 705
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mReceiver:Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;

    .line 60
    return-void
.end method

.method private RunUpdateCheckProgress()V
    .locals 3

    .prologue
    .line 578
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const-string v1, ""

    const v2, 0x7f090010

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    .line 579
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 580
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 581
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$6;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 588
    return-void
.end method

.method private StartInstall()V
    .locals 10

    .prologue
    .line 542
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/ConnectivityManager;

    .line 543
    .local v7, "cManager":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v8

    .line 544
    .local v8, "mobile":Landroid/net/NetworkInfo;
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v9

    .line 546
    .local v9, "wifi":Landroid/net/NetworkInfo;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 548
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V

    .line 549
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    .line 550
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;->start()V

    .line 552
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_2

    .line 553
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    .line 554
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$ProgressTimeTask;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$ProgressTimeTask;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 575
    :cond_2
    :goto_0
    return-void

    .line 559
    :cond_3
    const-string v0, "WatchManagerStub"

    const-string v1, "Connection failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const-string v0, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    :goto_1
    invoke-direct {v6, v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 561
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    const v0, 0x7f09000d

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 562
    const v0, 0x7f09000e

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 563
    const v0, 0x104000a

    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$5;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 570
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 560
    .end local v6    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method private StartSelfUpdate()V
    .locals 10

    .prologue
    .line 500
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/ConnectivityManager;

    .line 501
    .local v7, "cManager":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v8

    .line 502
    .local v8, "mobile":Landroid/net/NetworkInfo;
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v9

    .line 505
    .local v9, "wifi":Landroid/net/NetworkInfo;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 507
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V

    .line 508
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mHandler:Landroid/os/Handler;

    const-string v3, "self"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    .line 509
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;->start()V

    .line 511
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_2

    .line 512
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    .line 513
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$ProgressTimeTask;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$ProgressTimeTask;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 534
    :cond_2
    :goto_0
    return-void

    .line 518
    :cond_3
    const-string v0, "WatchManagerStub"

    const-string v1, "Connection failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const-string v0, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    :goto_1
    invoke-direct {v6, v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 520
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    const v0, 0x7f09000d

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 521
    const v0, 0x7f09000e

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 522
    const v0, 0x104000a

    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$4;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 519
    .end local v6    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_4
    const/4 v0, 0x4

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 779
    invoke-direct {p0, p1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V
    .locals 0

    .prologue
    .line 739
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateView()V

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Landroid/widget/ProgressBar;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V
    .locals 0

    .prologue
    .line 493
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->StartSelfUpdate()V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V
    .locals 0

    .prologue
    .line 535
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->StartInstall()V

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/android/app/watchmanagerstub/MainActivity;I)V
    .locals 0

    .prologue
    .line 119
    iput p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mRatio:I

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mRatio:I

    return v0
.end method

.method static synthetic access$18(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/android/app/watchmanagerstub/MainActivity;D)V
    .locals 0

    .prologue
    .line 128
    iput-wide p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mCurrentStatus:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    return-void
.end method

.method static synthetic access$20(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerInformation:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$22(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/DownloadThread;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Lcom/samsung/android/app/watchmanagerstub/DownloadThread;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/util/Timer;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Z)V
    .locals 0

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->bDoNotCheck:Z

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->bDoNotCheck:Z

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V
    .locals 0

    .prologue
    .line 755
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D

    return-wide v0
.end method

.method static synthetic access$7(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    return-void
.end method

.method private cancelDownload()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 757
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    if-eqz v0, :cond_0

    .line 759
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;->cancel()V

    .line 760
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;->interrupt()V

    .line 761
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    iput-boolean v2, v0, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;->isAlive:Z

    .line 762
    iput-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    .line 764
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    .line 766
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 767
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 768
    iput-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;

    .line 770
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    if-eqz v0, :cond_2

    .line 772
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;->cancel()V

    .line 773
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;->interrupt()V

    .line 774
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    iput-boolean v2, v0, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;->isAlive:Z

    .line 775
    iput-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    .line 777
    :cond_2
    return-void
.end method

.method private checkNetworkStatusForPopup()V
    .locals 6

    .prologue
    .line 1051
    invoke-static {p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getWifiStatus(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 1052
    .local v3, "wifi":Landroid/net/NetworkInfo;
    invoke-static {p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getMobileDataStatus(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1054
    .local v1, "mobileData":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1055
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->PREF_DONT_SHOW_AGAIN_WIFI:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getPreference(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1056
    const v2, 0x7f09002d

    .line 1057
    .local v2, "titleId":I
    const v0, 0x7f09002e

    .line 1058
    .local v0, "bodyId":I
    const-string v4, "WatchManagerStub"

    const-string v5, "WIFI_showBluetoothDialog()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1059
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->PREF_DONT_SHOW_AGAIN_WIFI:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v4}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showDataPopupForChina(IILjava/lang/String;)V

    .line 1081
    .end local v0    # "bodyId":I
    .end local v2    # "titleId":I
    :goto_0
    return-void

    .line 1062
    :cond_0
    const-string v4, "WatchManagerStub"

    const-string v5, "WIFI_launchStubApp()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateStubProcess()V

    goto :goto_0

    .line 1066
    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1067
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->PREF_DONT_SHOW_AGAIN_DATA:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getPreference(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1068
    const v2, 0x7f09002f

    .line 1069
    .restart local v2    # "titleId":I
    const v0, 0x7f09002b

    .line 1070
    .restart local v0    # "bodyId":I
    const-string v4, "WatchManagerStub"

    const-string v5, "DATA_showBluetoothDialog()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->PREF_DONT_SHOW_AGAIN_DATA:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v4}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showDataPopupForChina(IILjava/lang/String;)V

    goto :goto_0

    .line 1074
    .end local v0    # "bodyId":I
    .end local v2    # "titleId":I
    :cond_2
    const-string v4, "WatchManagerStub"

    const-string v5, "DATA_launchStubApp()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1075
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateStubProcess()V

    goto :goto_0

    .line 1079
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateStubProcess()V

    goto :goto_0
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 3
    .param p1, "szMessage"    # Ljava/lang/String;

    .prologue
    .line 783
    const-string v0, "WatchManagerStub"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "jangil::shotToast::"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    return-void
.end method

.method private updateView()V
    .locals 2

    .prologue
    .line 741
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->$SWITCH_TABLE$com$samsung$android$app$watchmanagerstub$MainActivity$VIEW_STAUS()[I

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mCurrentStatus:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-virtual {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 753
    :pswitch_0
    return-void

    .line 741
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected checkNetworkState(I)Z
    .locals 12
    .param p1, "action"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/16 v11, 0x11

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 958
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isWifiNetworkConnected()Z

    move-result v9

    if-nez v9, :cond_6

    .line 959
    const-string v0, "NetworkErrorDialog"

    .line 960
    .local v0, "DIALOG_TAG":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    const-string v10, "NetworkErrorDialog"

    invoke-virtual {v9, v10}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v6

    .line 962
    .local v6, "prev":Landroid/app/Fragment;
    if-eqz v6, :cond_0

    .line 1027
    .end local v0    # "DIALOG_TAG":Ljava/lang/String;
    .end local v6    # "prev":Landroid/app/Fragment;
    :goto_0
    return v7

    .line 966
    .restart local v0    # "DIALOG_TAG":Ljava/lang/String;
    .restart local v6    # "prev":Landroid/app/Fragment;
    :cond_0
    const/4 v1, 0x0

    .line 967
    .local v1, "airplaneMode":I
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v9, v11, :cond_1

    .line 968
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 969
    const-string v10, "airplane_mode_on"

    .line 968
    invoke-static {v9, v10, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 975
    :goto_1
    if-eqz v1, :cond_2

    .line 976
    new-instance v5, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;

    .line 977
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    .line 976
    invoke-direct {v5, v9, v8, p1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;-><init>(Landroid/content/Context;II)V

    .line 978
    .local v5, "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    invoke-virtual {v5}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->Show()V

    goto :goto_0

    .line 971
    .end local v5    # "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 972
    const-string v10, "airplane_mode_on"

    .line 971
    invoke-static {v9, v10, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    goto :goto_1

    .line 984
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const-string v10, "connectivity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 985
    .local v2, "connService":Landroid/net/ConnectivityManager;
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v9

    if-nez v9, :cond_3

    .line 986
    new-instance v5, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;

    .line 987
    iget-object v8, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const/4 v9, 0x2

    .line 986
    invoke-direct {v5, v8, v9, p1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;-><init>(Landroid/content/Context;II)V

    .line 988
    .restart local v5    # "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    invoke-virtual {v5}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->Show()V

    goto :goto_0

    .line 994
    .end local v5    # "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    :cond_3
    invoke-virtual {v2, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 995
    .local v4, "netInfo":Landroid/net/NetworkInfo;
    const/4 v3, 0x0

    .line 996
    .local v3, "dataRoaming":I
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 997
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v9, v11, :cond_4

    .line 998
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 999
    const-string v10, "data_roaming"

    .line 998
    invoke-static {v9, v10, v7}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 1005
    :goto_2
    if-eq v3, v8, :cond_5

    .line 1006
    new-instance v5, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;

    .line 1007
    iget-object v8, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const/4 v9, 0x3

    .line 1006
    invoke-direct {v5, v8, v9, p1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;-><init>(Landroid/content/Context;II)V

    .line 1008
    .restart local v5    # "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    invoke-virtual {v5}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->Show()V

    goto :goto_0

    .line 1001
    .end local v5    # "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 1002
    const-string v10, "data_roaming"

    .line 1001
    invoke-static {v9, v10, v7}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    goto :goto_2

    .line 1017
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isDataNetworkConnected()Z

    move-result v9

    if-nez v9, :cond_6

    .line 1018
    new-instance v5, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;

    .line 1019
    iget-object v8, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    const/4 v9, 0x4

    .line 1018
    invoke-direct {v5, v8, v9, p1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;-><init>(Landroid/content/Context;II)V

    .line 1020
    .restart local v5    # "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    invoke-virtual {v5}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->Show()V

    goto/16 :goto_0

    .end local v0    # "DIALOG_TAG":Ljava/lang/String;
    .end local v1    # "airplaneMode":I
    .end local v2    # "connService":Landroid/net/ConnectivityManager;
    .end local v3    # "dataRoaming":I
    .end local v4    # "netInfo":Landroid/net/NetworkInfo;
    .end local v5    # "noNetowrkDialog":Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
    .end local v6    # "prev":Landroid/app/Fragment;
    :cond_6
    move v7, v8

    .line 1027
    goto/16 :goto_0
.end method

.method protected hasMobileDataFeature()Z
    .locals 1

    .prologue
    .line 945
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    move-result v0

    return v0
.end method

.method protected isDataNetworkConnected()Z
    .locals 3

    .prologue
    .line 1044
    .line 1045
    const-string v2, "connectivity"

    .line 1044
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1046
    .local v0, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1047
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected isPhoneType()Z
    .locals 2

    .prologue
    .line 949
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 951
    .local v0, "pm":Landroid/content/pm/PackageManager;
    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method protected isWifiNetworkConnected()Z
    .locals 5

    .prologue
    .line 1031
    const/4 v2, 0x0

    .line 1033
    .local v2, "ret":Z
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    .line 1034
    const-string v4, "connectivity"

    .line 1033
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1036
    .local v0, "connManager":Landroid/net/ConnectivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 1037
    .local v1, "info":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1038
    const/4 v2, 0x1

    .line 1041
    :cond_0
    return v2
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 28
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 607
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 608
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const/high16 v26, 0x7f050000

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v11

    .line 613
    .local v11, "isTablet":Z
    if-eqz v11, :cond_0

    .line 614
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 615
    const v26, 0x7f070002

    .line 614
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 617
    .local v8, "entireLayoutBothsideMargin":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 618
    const v26, 0x7f070005

    .line 617
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 619
    .local v14, "layoutApplicationDescriptionToppadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 620
    const v26, 0x7f070003

    .line 619
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 621
    .local v12, "layoutApplicationDescriptionLeftpadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 622
    const v26, 0x7f070004

    .line 621
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v13

    .line 624
    .local v13, "layoutApplicationDescriptionRightpadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 625
    const v26, 0x7f070007

    .line 624
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 626
    .local v23, "layoutSizeinstallToppadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 627
    const v26, 0x7f070008

    .line 626
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v21

    .line 628
    .local v21, "layoutSizeinstallLeftpadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 629
    const v26, 0x7f070009

    .line 628
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v22

    .line 630
    .local v22, "layoutSizeinstallRightpadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 631
    const v26, 0x7f07000a

    .line 630
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    .line 633
    .local v20, "layoutSizeinstallBottompadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 634
    const v26, 0x7f07000e

    .line 633
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 635
    .local v6, "buttonInstallWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 636
    const v26, 0x7f07000d

    .line 635
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 638
    .local v5, "buttonInstallHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 639
    const v26, 0x7f07001d

    .line 638
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 640
    .local v16, "layoutSampleimageBottompadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 641
    const v26, 0x7f07001c

    .line 640
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 642
    .local v19, "layoutSampleimageToppadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 643
    const v26, 0x7f07001e

    .line 642
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    .line 644
    .local v17, "layoutSampleimageLeftpadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 645
    const v26, 0x7f07001f

    .line 644
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 647
    .local v18, "layoutSampleimageRightpadding":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 648
    const v26, 0x7f070020

    .line 647
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 649
    .local v9, "imageviewImageHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    .line 650
    const v26, 0x7f070021

    .line 649
    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 652
    .local v10, "imageviewImageWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mMainActivity:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v8, v1, v8, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 654
    const v25, 0x7f0b0004

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/RelativeLayout;

    .line 655
    .local v24, "layout_Install":Landroid/widget/RelativeLayout;
    const v25, 0x7f0b000f

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout;

    .line 657
    .local v15, "layoutImageview":Landroid/widget/LinearLayout;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mLayoutGearManagerDescription:Landroid/widget/LinearLayout;

    move-object/from16 v25, v0

    .line 660
    const/16 v26, 0x0

    .line 657
    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v0, v12, v14, v13, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 662
    move-object/from16 v0, v24

    move/from16 v1, v21

    move/from16 v2, v23

    move/from16 v3, v22

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mInstallButton:Landroid/widget/Button;

    move-object/from16 v25, v0

    new-instance v26, Landroid/widget/FrameLayout$LayoutParams;

    .line 668
    move-object/from16 v0, v26

    invoke-direct {v0, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 667
    invoke-virtual/range {v25 .. v26}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 670
    move/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v18

    move/from16 v3, v16

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 675
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mImageviewImage1:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 676
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mImageviewImage1:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 677
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const/high16 v26, 0x7f020000

    invoke-virtual/range {v25 .. v26}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 678
    .local v7, "d1":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mImageviewImage1:Landroid/widget/ImageView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mEntireScrollview:Landroid/widget/ScrollView;

    move-object/from16 v25, v0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const/high16 v27, 0x7f060000

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getColor(I)I

    move-result v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/ScrollView;->setBackgroundColor(I)V

    .line 684
    .end local v5    # "buttonInstallHeight":I
    .end local v6    # "buttonInstallWidth":I
    .end local v7    # "d1":Landroid/graphics/drawable/Drawable;
    .end local v8    # "entireLayoutBothsideMargin":I
    .end local v9    # "imageviewImageHeight":I
    .end local v10    # "imageviewImageWidth":I
    .end local v12    # "layoutApplicationDescriptionLeftpadding":I
    .end local v13    # "layoutApplicationDescriptionRightpadding":I
    .end local v14    # "layoutApplicationDescriptionToppadding":I
    .end local v15    # "layoutImageview":Landroid/widget/LinearLayout;
    .end local v16    # "layoutSampleimageBottompadding":I
    .end local v17    # "layoutSampleimageLeftpadding":I
    .end local v18    # "layoutSampleimageRightpadding":I
    .end local v19    # "layoutSampleimageToppadding":I
    .end local v20    # "layoutSizeinstallBottompadding":I
    .end local v21    # "layoutSizeinstallLeftpadding":I
    .end local v22    # "layoutSizeinstallRightpadding":I
    .end local v23    # "layoutSizeinstallToppadding":I
    .end local v24    # "layout_Install":Landroid/widget/RelativeLayout;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 325
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 327
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;

    .line 328
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    .line 329
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    .line 331
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "connect_wearable"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 332
    .local v4, "sp":Landroid/content/SharedPreferences;
    const-string v6, "MAC"

    const/4 v7, 0x0

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;

    .line 333
    const-string v6, "WM_MANAGER"

    const/4 v7, 0x0

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    .line 335
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    if-nez v6, :cond_0

    .line 336
    const-string v6, "watchmanager"

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    .line 337
    const-string v6, "WatchManagerStub"

    const-string v7, "jangil:: there is no WM_MANAGER"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_0
    const-string v6, "WatchManagerStub"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "jangil::onCreate::btAddress::"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "::WM_MANAGER::"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;

    if-nez v6, :cond_1

    .line 343
    const-string v6, "WatchManagerStub"

    const-string v7, "jangil::there is no BT Address!!! this is NFC!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_1
    const/high16 v6, 0x7f040000

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->setContentView(I)V

    .line 347
    iput-object p0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;

    .line 349
    const v6, 0x7f0b0001

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mMainActivity:Landroid/widget/LinearLayout;

    .line 350
    const v6, 0x7f090001

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->setTitle(I)V

    .line 351
    const v6, 0x7f0b0002

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mLayoutGearManagerDescription:Landroid/widget/LinearLayout;

    .line 352
    const v6, 0x7f0b0007

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mInstallButton:Landroid/widget/Button;

    .line 353
    const v6, 0x7f0b000f

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mLayoutImageview:Landroid/widget/LinearLayout;

    .line 355
    const v6, 0x7f0b0010

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mImageviewImage1:Landroid/widget/ImageView;

    .line 357
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 358
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    new-instance v7, Landroid/graphics/drawable/ColorDrawable;

    const/16 v8, 0xfe

    const/16 v9, 0x6f

    const/16 v10, 0xd

    invoke-static {v8, v9, v10}, Landroid/graphics/Color;->rgb(III)I

    move-result v8

    invoke-direct {v7, v8}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 360
    const/high16 v6, 0x7f0b0000

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ScrollView;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mEntireScrollview:Landroid/widget/ScrollView;

    .line 362
    const v6, 0x7f0b0007

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 363
    .local v5, "update":Landroid/widget/Button;
    const v6, 0x7f0b000e

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 365
    .local v1, "cancel":Landroid/widget/Button;
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 366
    const v6, 0x7f0b0005

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerInformation:Landroid/widget/TextView;

    .line 367
    const v6, 0x7f0b000c

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

    .line 368
    const v6, 0x7f0b000b

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    .line 369
    const v6, 0x7f0b0003

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerDescription:Landroid/widget/TextView;

    .line 372
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerInformation:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    const v8, 0x7f090005

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ":"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const v8, 0x7f09000f

    invoke-virtual {p0, v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerInformation:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 374
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;

    const-string v7, "0MB"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;

    const v7, 0x7f09000f

    invoke-virtual {p0, v7}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f080000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    iput v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mNumberLanguage:I

    .line 379
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    const-string v7, "watchmanager"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 380
    const/high16 v6, 0x7f090000

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->setTitle(I)V

    .line 381
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f020000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 382
    .local v2, "d1":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mImageviewImage1:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 383
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerDescription:Landroid/widget/TextView;

    const v7, 0x7f090028

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const/high16 v10, 0x7f090000

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const v10, 0x7f090002

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const/high16 v10, 0x7f090000

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    iget v10, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mNumberLanguage:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    .end local v2    # "d1":Landroid/graphics/drawable/Drawable;
    :cond_2
    :goto_0
    new-instance v6, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    new-instance v6, Lcom/samsung/android/app/watchmanagerstub/MainActivity$3;

    invoke-direct {v6, p0, v5}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$3;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Landroid/widget/Button;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 465
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    const-string v6, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 466
    const-string v6, "package"

    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 467
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mReceiver:Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;

    invoke-virtual {p0, v6, v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 469
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_5

    .line 470
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 471
    .local v0, "ChinaNalSecurityType":Ljava/lang/String;
    const-string v6, "WatchManagerStub"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "enable(): enable Bluetooth Connect GearStub PopUp for Bluetooth  ChinaNalSecurityType = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    const-string v6, "ChinaNalSecurity"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 473
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->checkNetworkStatusForPopup()V

    .line 491
    .end local v0    # "ChinaNalSecurityType":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 384
    .end local v3    # "intentFilter":Landroid/content/IntentFilter;
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;

    const-string v7, "wms"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 385
    const v6, 0x7f090001

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->setTitle(I)V

    .line 386
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f020000

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 387
    .restart local v2    # "d1":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mImageviewImage1:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 388
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerDescription:Landroid/widget/TextView;

    const v7, 0x7f090028

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const v10, 0x7f090001

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const v10, 0x7f090003

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const v10, 0x7f090001

    invoke-virtual {p0, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    iget v10, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mNumberLanguage:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {p0, v7, v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 478
    .end local v2    # "d1":Landroid/graphics/drawable/Drawable;
    .restart local v3    # "intentFilter":Landroid/content/IntentFilter;
    :cond_5
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->checkNetworkState(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 479
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V

    .line 480
    const v6, 0x7f0b0004

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mlayout_install:Landroid/widget/RelativeLayout;

    .line 482
    const/4 v6, 0x1

    sput-boolean v6, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isSelfDownload:Z

    .line 483
    new-instance v6, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mHandler:Landroid/os/Handler;

    const-string v9, "self"

    invoke-direct {v6, v7, v8, v9}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    .line 484
    iget-object v6, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    invoke-virtual {v6}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;->start()V

    .line 485
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->RunUpdateCheckProgress()V

    .line 486
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateView()V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 688
    const-string v0, "WatchManagerStub"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mReceiver:Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mReceiver:Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 691
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V

    .line 693
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "connect_wearable"

    invoke-static {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->removeAllPreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 694
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "gearmanager_stub"

    invoke-static {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->removeAllPreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 695
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 696
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 701
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 702
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateView()V

    .line 703
    return-void
.end method

.method public showDataPopupForChina(IILjava/lang/String;)V
    .locals 8
    .param p1, "titleId"    # I
    .param p2, "bodyId"    # I
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x100

    const/4 v6, -0x1

    .line 1084
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1085
    .local v3, "inflator":Landroid/view/LayoutInflater;
    const v4, 0x7f040001

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1087
    .local v1, "dialogView":Landroid/view/View;
    const v4, 0x7f0b0012

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 1089
    .local v2, "doNotShow":Landroid/widget/CheckBox;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1090
    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->bDoNotCheck:Z

    .line 1092
    new-instance v4, Lcom/samsung/android/app/watchmanagerstub/MainActivity$7;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$7;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1100
    const v4, 0x7f0b0011

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1101
    .local v0, "bodyView":Landroid/widget/TextView;
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 1102
    const-string v4, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1103
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1104
    invoke-virtual {v2, v6}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 1107
    :cond_0
    new-instance v5, Landroid/app/AlertDialog$Builder;

    const-string v4, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x5

    :goto_0
    invoke-direct {v5, p0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 1108
    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1109
    invoke-virtual {v4, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1110
    const v5, 0x104000a

    new-instance v6, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;

    invoke-direct {v6, p0, p3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1122
    const/high16 v5, 0x1040000

    new-instance v6, Lcom/samsung/android/app/watchmanagerstub/MainActivity$9;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$9;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1128
    new-instance v5, Lcom/samsung/android/app/watchmanagerstub/MainActivity$10;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$10;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 1134
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 1107
    iput-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 1135
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v7, v7}, Landroid/view/Window;->setFlags(II)V

    .line 1137
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 1138
    return-void

    .line 1107
    :cond_1
    const/4 v4, 0x4

    goto :goto_0
.end method

.method public updateStubProcess()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1141
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->checkNetworkState(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1142
    const-string v0, "WatchManagerStub"

    const-string v1, "NetworkErrorDialog.ACTION_INSTALL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V

    .line 1144
    const v0, 0x7f0b0004

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mlayout_install:Landroid/widget/RelativeLayout;

    .line 1146
    sput-boolean v2, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isSelfDownload:Z

    .line 1147
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mHandler:Landroid/os/Handler;

    const-string v3, "self"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    .line 1148
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;->start()V

    .line 1149
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->RunUpdateCheckProgress()V

    .line 1150
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateView()V

    .line 1156
    :goto_0
    return-void

    .line 1153
    :cond_0
    const-string v0, "WatchManagerStub"

    const-string v1, "do when network error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
