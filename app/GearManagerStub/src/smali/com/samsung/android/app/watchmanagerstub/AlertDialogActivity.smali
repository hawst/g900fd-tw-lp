.class public Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;
.super Landroid/app/Activity;
.source "AlertDialogActivity.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;)Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v3, 0x100

    .line 15
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;->requestWindowFeature(I)Z

    .line 19
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const-string v0, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    :goto_0
    invoke-direct {v1, p0, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 20
    const v0, 0x7f090022

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 21
    const v1, 0x7f090023

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 22
    const v1, 0x7f090024

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 27
    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity$2;-><init>(Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;->mAlertDialog:Landroid/app/AlertDialog;

    .line 33
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 35
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 36
    return-void

    .line 19
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method
