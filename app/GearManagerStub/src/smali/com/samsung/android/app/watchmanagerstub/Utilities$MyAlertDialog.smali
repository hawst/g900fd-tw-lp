.class public Lcom/samsung/android/app/watchmanagerstub/Utilities$MyAlertDialog;
.super Landroid/app/Activity;
.source "Utilities.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/Utilities;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyAlertDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/watchmanagerstub/Utilities;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/watchmanagerstub/Utilities;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/Utilities$MyAlertDialog;->this$0:Lcom/samsung/android/app/watchmanagerstub/Utilities;

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 340
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 343
    new-instance v0, Landroid/app/AlertDialog$Builder;

    const-string v1, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    :goto_0
    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 344
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 345
    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 346
    const v1, 0x7f090023

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 347
    const v1, 0x104000a

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/Utilities$MyAlertDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities$MyAlertDialog$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/Utilities$MyAlertDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 353
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 354
    return-void

    .line 343
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method
