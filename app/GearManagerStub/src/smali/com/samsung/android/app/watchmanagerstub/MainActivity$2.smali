.class Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/watchmanagerstub/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x4

    const/4 v5, 0x0

    .line 402
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$12(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/ConnectivityManager;

    .line 403
    .local v7, "cManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v7, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v10

    .line 404
    .local v10, "mobile":Landroid/net/NetworkInfo;
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v12

    .line 406
    .local v12, "wifi":Landroid/net/NetworkInfo;
    const/4 v11, 0x0

    .line 407
    .local v11, "modelName":Ljava/lang/String;
    sget-boolean v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isSelfDownload:Z

    if-eqz v1, :cond_4

    .line 408
    const-string v11, "self"

    .line 412
    :goto_0
    if-eqz v12, :cond_0

    invoke-virtual {v12}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-eqz v10, :cond_5

    invoke-virtual {v10}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 414
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V
    invoke-static {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$3(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    .line 415
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    iget-object v4, v4, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, v3, v4, v11}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$22(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/DownloadThread;)V

    .line 416
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;
    invoke-static {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$23(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 417
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mThread:Lcom/samsung/android/app/watchmanagerstub/DownloadThread;
    invoke-static {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$23(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Lcom/samsung/android/app/watchmanagerstub/DownloadThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/app/watchmanagerstub/DownloadThread;->start()V

    .line 418
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v2, 0x7f0b0009

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 419
    .local v9, "layout_progressing":Landroid/widget/RelativeLayout;
    invoke-virtual {v9, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 421
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v2, 0x7f0b0004

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    .line 422
    .local v8, "layout_install":Landroid/widget/RelativeLayout;
    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$4(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/util/Timer;

    move-result-object v0

    if-nez v0, :cond_3

    .line 425
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    invoke-static {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$24(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/util/Timer;)V

    .line 426
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$4(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$ProgressTimeTask;

    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-direct {v1, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$ProgressTimeTask;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 446
    .end local v8    # "layout_install":Landroid/widget/RelativeLayout;
    .end local v9    # "layout_progressing":Landroid/widget/RelativeLayout;
    :cond_3
    :goto_1
    return-void

    .line 410
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$8(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0

    .line 431
    :cond_5
    const-string v1, "WatchManagerStub"

    const-string v2, "Connection failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$12(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v0, 0x5

    :cond_6
    invoke-direct {v6, v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 433
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    const v0, 0x7f09000d

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 434
    const v0, 0x7f09000e

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 435
    const v0, 0x104000a

    new-instance v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$2$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$2;)V

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 442
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1
.end method
