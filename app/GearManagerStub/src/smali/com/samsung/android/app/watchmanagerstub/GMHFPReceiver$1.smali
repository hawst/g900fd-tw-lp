.class Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver$1;
.super Ljava/lang/Object;
.source "GMHFPReceiver.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;

    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 4
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    const/4 v0, 0x1

    .line 237
    const-string v1, "GMFPReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onServiceConnected() : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    if-ne p1, v0, :cond_0

    .line 239
    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    invoke-static {p2}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->access$0(Landroid/bluetooth/BluetoothHeadset;)V

    .line 240
    const-string v1, "GMFPReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mBluetoothHeadset = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->access$1()Landroid/bluetooth/BluetoothHeadset;

    move-result-object v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_0
    return-void

    .line 240
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 246
    const-string v0, "GMFPReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onServiceDisconnected() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 249
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->access$0(Landroid/bluetooth/BluetoothHeadset;)V

    .line 251
    :cond_0
    return-void
.end method
