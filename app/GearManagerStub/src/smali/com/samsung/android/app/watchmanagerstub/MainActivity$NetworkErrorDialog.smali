.class public Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;
.super Ljava/lang/Object;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NetworkErrorDialog"
.end annotation


# static fields
.field public static final ACTION_INSTALL:I = 0x1

.field public static final ACTION_START:I = 0x0

.field public static final TYPE_AIRPLANE_MODE:I = 0x1

.field public static final TYPE_DATA_ROAMING:I = 0x3

.field public static final TYPE_MOBILE_DATA:I = 0x2

.field public static final TYPE_NO_NETWORK:I = 0x0

.field public static final TYPE_NO_SIGNAL:I = 0x4


# instance fields
.field builder:Landroid/app/AlertDialog$Builder;

.field private mContext:Landroid/content/Context;

.field private mType:I


# direct methods
.method protected constructor <init>(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I
    .param p3, "action"    # I

    .prologue
    .line 803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 804
    iput p2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->mType:I

    .line 805
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->mContext:Landroid/content/Context;

    .line 806
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->onCreate()V

    .line 807
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method Show()V
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 938
    return-void
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const v6, 0x7f09000d

    const v5, 0x104000a

    .line 819
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->mContext:Landroid/content/Context;

    const-string v1, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    :goto_0
    invoke-direct {v2, v3, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    .line 820
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 821
    iget v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->mType:I

    packed-switch v1, :pswitch_data_0

    .line 935
    :cond_0
    :goto_1
    return-void

    .line 819
    :cond_1
    const/4 v1, 0x4

    goto :goto_0

    .line 824
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x1040014

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 826
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    .line 827
    .local v0, "activity":Lcom/samsung/android/app/watchmanagerstub/MainActivity;
    if-eqz v0, :cond_3

    .line 828
    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isPhoneType()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->hasMobileDataFeature()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 829
    :cond_2
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;->isChineseModel()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 830
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090011

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 839
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 832
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090012

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 835
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 852
    .end local v0    # "activity":Lcom/samsung/android/app/watchmanagerstub/MainActivity;
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 853
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;->isChineseModel()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 854
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090014

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 858
    :goto_3
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$2;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 856
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090015

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_3

    .line 868
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 869
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;->isChineseModel()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 870
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090016

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 874
    :goto_4
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$3;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 872
    :cond_7
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09000e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_4

    .line 884
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 885
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;->isChineseModel()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 886
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090017

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 890
    :goto_5
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$4;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 888
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090018

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_5

    .line 900
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 902
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    .line 904
    .restart local v0    # "activity":Lcom/samsung/android/app/watchmanagerstub/MainActivity;
    if-eqz v0, :cond_0

    .line 908
    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isPhoneType()Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->hasMobileDataFeature()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 909
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f090019

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 918
    :goto_6
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    new-instance v2, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog$5;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;)V

    invoke-virtual {v1, v5, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 911
    :cond_a
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;->isChineseModel()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 912
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09001a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_6

    .line 914
    :cond_b
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$NetworkErrorDialog;->builder:Landroid/app/AlertDialog$Builder;

    const v2, 0x7f09001b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    goto :goto_6

    .line 821
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
