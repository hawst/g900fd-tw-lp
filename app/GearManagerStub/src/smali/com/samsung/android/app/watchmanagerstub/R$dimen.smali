.class public final Lcom/samsung/android/app/watchmanagerstub/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final activity_horizontal_margin:I = 0x7f070000

.field public static final activity_vertical_margin:I = 0x7f070001

.field public static final button_cancel_leftpadding:I = 0x7f070018

.field public static final button_cancel_size:I = 0x7f070016

.field public static final button_cancel_toppadding:I = 0x7f070017

.field public static final button_install_height:I = 0x7f07000d

.field public static final button_install_text_size:I = 0x7f070027

.field public static final button_install_width:I = 0x7f07000e

.field public static final data_connection_dialog_bottomMargin:I = 0x7f07002b

.field public static final data_connection_dialog_leftMargin:I = 0x7f07002c

.field public static final data_connection_dialog_middleMargin:I = 0x7f07002d

.field public static final data_connection_dialog_textSize:I = 0x7f07002e

.field public static final data_connection_dialog_topMargin:I = 0x7f07002a

.field public static final downloading_inter_downloadprogress_padding:I = 0x7f070013

.field public static final entire_layout_bothside_margin:I = 0x7f070002

.field public static final image_inter_image_padding:I = 0x7f070022

.field public static final imageview_image_height:I = 0x7f070020

.field public static final imageview_image_width:I = 0x7f070021

.field public static final layout_applicationdescription_leftpadding:I = 0x7f070003

.field public static final layout_applicationdescription_rightpadding:I = 0x7f070004

.field public static final layout_applicationdescription_text_size:I = 0x7f070025

.field public static final layout_applicationdescription_toppadding:I = 0x7f070005

.field public static final layout_downloading_leftpadding:I = 0x7f070010

.field public static final layout_downloading_rightpadding:I = 0x7f070011

.field public static final layout_downloading_toppadding:I = 0x7f07000f

.field public static final layout_sampleimage_bottompadding:I = 0x7f07001d

.field public static final layout_sampleimage_height:I = 0x7f07001b

.field public static final layout_sampleimage_leftpadding:I = 0x7f07001e

.field public static final layout_sampleimage_rightpadding:I = 0x7f07001f

.field public static final layout_sampleimage_toppadding:I = 0x7f07001c

.field public static final layout_sizeinstall_bottompadding:I = 0x7f07000a

.field public static final layout_sizeinstall_downloading_height:I = 0x7f070006

.field public static final layout_sizeinstall_leftpadding:I = 0x7f070008

.field public static final layout_sizeinstall_rightpadding:I = 0x7f070009

.field public static final layout_sizeinstall_toppadding:I = 0x7f070007

.field public static final layout_warning_left_right_padding:I = 0x7f070024

.field public static final layout_warning_text_size:I = 0x7f070029

.field public static final layout_warning_top_bottom_padding:I = 0x7f070023

.field public static final line_height:I = 0x7f07001a

.field public static final progressbar_downloadprogress_height:I = 0x7f070014

.field public static final progressbar_installing_height:I = 0x7f070019

.field public static final progressbar_marginright:I = 0x7f070015

.field public static final size_inter_install_padding:I = 0x7f07000c

.field public static final textview_downloading_height:I = 0x7f070012

.field public static final textview_downloading_text_size:I = 0x7f070028

.field public static final textview_size_height:I = 0x7f07000b

.field public static final textview_size_text_size:I = 0x7f070026


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
