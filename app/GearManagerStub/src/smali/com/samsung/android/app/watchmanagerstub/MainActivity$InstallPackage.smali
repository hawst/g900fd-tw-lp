.class Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InstallPackage"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V
    .locals 0

    .prologue
    .line 707
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    .line 710
    if-eqz p2, :cond_2

    .line 711
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 712
    .local v0, "packageName":Ljava/lang/String;
    const-string v3, "WatchManagerStub"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onReceive() packageName : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 714
    const-string v3, "WatchManagerStub"

    const-string v4, "onReceive() ACTION_PACKAGE_ADDED"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    const-string v3, "com.samsung.android.app.watchmanager"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "com.samsung.android.app.gearinstaller"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 716
    const-string v3, "com.samsung.android.wms"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 717
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V
    invoke-static {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$3(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    .line 720
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$21(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 721
    const-string v3, "com.samsung.android.app.watchmanager"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 722
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$21(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3, v6}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->startGearManager(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 728
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$12(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 729
    .local v2, "temp":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x6

    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 730
    .local v1, "rootofSrc":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->DeleteDir(Ljava/lang/String;)V

    .line 731
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "connect_wearable"

    invoke-static {v3, v4}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->removeAllPreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 732
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->finish()V

    .line 736
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "rootofSrc":Ljava/lang/String;
    .end local v2    # "temp":Ljava/lang/String;
    :cond_2
    return-void

    .line 723
    .restart local v0    # "packageName":Ljava/lang/String;
    :cond_3
    const-string v3, "com.samsung.android.wms"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 724
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$InstallPackage;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->btAddress:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$21(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v3}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->startWingTipManager(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
