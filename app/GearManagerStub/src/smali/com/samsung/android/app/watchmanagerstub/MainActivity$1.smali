.class Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;
.super Landroid/os/Handler;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    .line 136
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;)Lcom/samsung/android/app/watchmanagerstub/MainActivity;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 17
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 140
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isFinishing()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 141
    const-string v8, "WatchManagerStub"

    const-string v9, "jangil::isFinishing...::return"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "MESSAGE_CMD"

    const-string v10, "DOWNLOAD_STATUS"

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "cmd":Ljava/lang/String;
    const-string v8, "DOWNLOAD_STATUS"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 149
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "DOWNLOAD_STATUS"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 265
    :goto_1
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateView()V
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$11(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    goto :goto_0

    .line 152
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Downloading..."

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    .line 153
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 155
    :try_start_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->DOWNLOADING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    .line 161
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v9, 0x7f0b0009

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 162
    .local v5, "layout_progressing":Landroid/widget/RelativeLayout;
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 164
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v9, 0x7f0b0004

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 165
    .local v4, "layout_install":Landroid/widget/RelativeLayout;
    const/4 v8, 0x4

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    .line 156
    .end local v4    # "layout_install":Landroid/widget/RelativeLayout;
    .end local v5    # "layout_progressing":Landroid/widget/RelativeLayout;
    :catch_0
    move-exception v3

    .line 157
    .local v3, "e":Ljava/lang/Exception;
    const-string v8, "WatchManagerStub"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "exception e = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 167
    .end local v3    # "e":Ljava/lang/Exception;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Downloaded"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$3(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    goto/16 :goto_1

    .line 169
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$4(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/util/Timer;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 170
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$4(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/util/Timer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Timer;->cancel()V

    .line 171
    :cond_3
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$5(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;

    move-result-object v8

    const-string v9, "%.1fMB"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v12}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v12

    const-wide v14, 0x412e848000000000L    # 1000000.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$7(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v8

    if-eqz v8, :cond_4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$7(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 173
    :cond_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->INSTALLING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 176
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Latest version"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 177
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Contents is not ready"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 178
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Download canceled"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 179
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Network unavailable"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 181
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Install fail"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 182
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Install fail"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 183
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Download fail"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    goto/16 :goto_1

    .line 185
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "Self Upgrade don\'t need"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    .line 186
    sget-boolean v8, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isSelfDownload:Z

    if-eqz v8, :cond_7

    .line 187
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    sget-object v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$2(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;)V

    .line 188
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$3(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    .line 190
    const/4 v8, 0x0

    sput-boolean v8, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isSelfDownload:Z

    .line 192
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$8(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 193
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    new-instance v9, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    iget-object v11, v11, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->postName:Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$8(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v10, v11, v12}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$9(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;)V

    .line 195
    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$10(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 196
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateCheckThread:Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$10(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/app/watchmanagerstub/UpdateCheckThread;->start()V

    .line 197
    const-string v8, "WatchManagerStub"

    const-string v9, "jangil::updateCheckThread.start()"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_6
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateView()V
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$11(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    goto/16 :goto_1

    .line 201
    :cond_7
    const-string v8, "WatchManagerStub"

    const-string v9, "jangil::there is no contents..."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    if-eqz v8, :cond_8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 204
    :try_start_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 210
    :cond_8
    :goto_3
    new-instance v1, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$12(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/content/Context;

    move-result-object v9

    const-string v8, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    const/4 v8, 0x5

    :goto_4
    invoke-direct {v1, v9, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 211
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v8, 0x7f090022

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 212
    const v8, 0x7f090029

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 213
    const v8, 0x7f090024

    new-instance v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1$1;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;)V

    invoke-virtual {v1, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    .line 222
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 205
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :catch_1
    move-exception v3

    .line 206
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string v8, "WatchManagerStub"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "exception e = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 210
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_9
    const/4 v8, 0x4

    goto :goto_4

    .line 226
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const-string v9, "READY_TO_DOWNLOAD"

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    .line 228
    sget-boolean v8, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->isSelfDownload:Z

    if-eqz v8, :cond_b

    .line 229
    new-instance v1, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$12(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/content/Context;

    move-result-object v9

    const-string v8, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    const/4 v8, 0x5

    :goto_5
    invoke-direct {v1, v9, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 230
    .restart local v1    # "builder":Landroid/app/AlertDialog$Builder;
    const v8, 0x7f090022

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 231
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v9, 0x7f09002a

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const/high16 v13, 0x7f090000

    invoke-virtual {v12, v13}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v13, 0x7f090002

    invoke-virtual {v12, v13}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 232
    const v8, 0x7f09000b

    new-instance v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1$2;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1$2;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;)V

    invoke-virtual {v1, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 244
    const v8, 0x7f09000c

    new-instance v9, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1$3;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1$3;-><init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;)V

    invoke-virtual {v1, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    .line 255
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_1

    .line 229
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_a
    const/4 v8, 0x4

    goto :goto_5

    .line 257
    :cond_b
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v10, 0x7f0b000d

    invoke-virtual {v8, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ProgressBar;

    invoke-static {v9, v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$13(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Landroid/widget/ProgressBar;)V

    .line 258
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$7(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v8

    const/16 v9, 0x64

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 259
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$7(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 260
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->StartInstall()V
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$15(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    goto/16 :goto_1

    .line 268
    :cond_c
    const-string v8, "UPDATE_DOWNLOAD_EXCEPTION"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 269
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "UPDATE_DOWNLOAD_EXCEPTION"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    packed-switch v8, :pswitch_data_1

    goto/16 :goto_0

    .line 271
    :pswitch_d
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    if-eqz v8, :cond_d

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 273
    :try_start_2
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 279
    :cond_d
    :goto_6
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->finish()V

    goto/16 :goto_0

    .line 274
    :catch_2
    move-exception v3

    .line 275
    .restart local v3    # "e":Ljava/lang/Exception;
    const-string v8, "WatchManagerStub"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "exception e = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 282
    .end local v3    # "e":Ljava/lang/Exception;
    :pswitch_e
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$12(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v7

    .line 283
    .local v7, "temp":Ljava/lang/String;
    const-string v8, "WatchManagerStub"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "temp= "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    const/4 v8, 0x0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x6

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 285
    .local v6, "rootofSrc":Ljava/lang/String;
    const-string v8, "WatchManagerStub"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "jangil::deletePath::"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-static {v6}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->DeleteDir(Ljava/lang/String;)V

    .line 289
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "connect_wearable"

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->removeAllPreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 290
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "gearmanager_stub"

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->removeAllPreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 292
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->cancelDownload()V
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$3(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)V

    .line 293
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->finish()V

    goto/16 :goto_0

    .line 298
    .end local v6    # "rootofSrc":Ljava/lang/String;
    .end local v7    # "temp":Ljava/lang/String;
    :cond_e
    const-string v8, "CHECK_SERVER_PROGRESSDIALOG"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_f

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mUpdateCheckProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$1(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/ProgressDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v8

    if-nez v8, :cond_0

    .line 300
    :cond_f
    const-string v8, "DOWNLOA_RATIO"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "DOWNLOA_RATIO"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$16(Lcom/samsung/android/app/watchmanagerstub/MainActivity;I)V

    goto/16 :goto_0

    .line 302
    :cond_10
    const-string v8, "DOWNLODING_PROGRESS_UPDATE"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 303
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "PROGRESSUPDATE::"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v10

    const-wide v12, 0x4197d78400000000L    # 1.0E8

    div-double/2addr v10, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mRatio:I
    invoke-static {v12}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$17(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)I

    move-result v12

    int-to-double v12, v12

    mul-double/2addr v10, v12

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    .line 304
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$7(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v8

    if-eqz v8, :cond_0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v8

    const-wide v10, 0x4197d78400000000L    # 1.0E8

    div-double/2addr v8, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mRatio:I
    invoke-static {v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$17(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)I

    move-result v10

    int-to-double v10, v10

    mul-double/2addr v8, v10

    const-wide/16 v10, 0x0

    cmpl-double v8, v8, v10

    if-eqz v8, :cond_0

    .line 305
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mProgressBarInstall:Landroid/widget/ProgressBar;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$7(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/ProgressBar;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mRatio:I
    invoke-static {v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$17(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 306
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSizeCurrent:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$5(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;

    move-result-object v8

    const-string v9, "%.1fMB"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v12}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v12

    const-wide v14, 0x4197d78400000000L    # 1.0E8

    div-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mRatio:I
    invoke-static {v14}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$17(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)I

    move-result v14

    int-to-double v14, v14

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$18(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;

    move-result-object v8

    const-string v9, "/%.1fMB"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v12}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v12

    const-wide v14, 0x412e848000000000L    # 1000000.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 311
    :cond_11
    const-string v8, "SEND_APPSIZE"

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 312
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "appsize"

    invoke-virtual {v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    int-to-double v9, v9

    invoke-static {v8, v9, v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$19(Lcom/samsung/android/app/watchmanagerstub/MainActivity;D)V

    .line 313
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "SENDAPPSIZE"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v10}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showToast(Ljava/lang/String;)V
    invoke-static {v8, v9}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$0(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V

    .line 314
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewGearManagerInformation:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$20(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    const v11, 0x7f090005

    invoke-virtual {v10, v11}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "%.1fMB"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v13}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v13

    const-wide v15, 0x412e848000000000L    # 1000000.0

    div-double/2addr v13, v15

    invoke-static {v13, v14}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mTextViewDownlodingSize:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$18(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/widget/TextView;

    move-result-object v8

    const-string v9, "/%.1fMB"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$1;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAppSize:D
    invoke-static {v12}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$6(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)D

    move-result-wide v12

    const-wide v14, 0x412e848000000000L    # 1000000.0

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 269
    :pswitch_data_1
    .packed-switch 0xb
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
