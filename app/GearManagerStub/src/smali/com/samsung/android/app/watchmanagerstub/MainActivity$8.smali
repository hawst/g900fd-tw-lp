.class Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/watchmanagerstub/MainActivity;->showDataPopupForChina(IILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

.field private final synthetic val$key:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/watchmanagerstub/MainActivity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    iput-object p2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;->val$key:Ljava/lang/String;

    .line 1110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1113
    const-string v0, "WatchManagerStub"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick() which="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1116
    const-string v0, "WatchManagerStub"

    const-string v1, "BUTTON_POSITIVE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1117
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;->val$key:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->bDoNotCheck:Z
    invoke-static {v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$27(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->setPreference(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1118
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->updateStubProcess()V

    .line 1120
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$8;->this$0:Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    # getter for: Lcom/samsung/android/app/watchmanagerstub/MainActivity;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/app/watchmanagerstub/MainActivity;->access$28(Lcom/samsung/android/app/watchmanagerstub/MainActivity;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1121
    return-void
.end method
