.class public Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getCountryCodeProperty()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isChineseModel()Z
    .locals 3

    .prologue
    .line 21
    const/4 v1, 0x0

    .line 22
    .local v1, "result":Z
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;->getCountryCodeProperty()Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 24
    const-string v2, "CHINA"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 25
    const/4 v1, 0x1

    .line 28
    :cond_0
    return v1
.end method

.method public static isKoreanModel()Z
    .locals 3

    .prologue
    .line 9
    const/4 v1, 0x0

    .line 10
    .local v1, "result":Z
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/DeviceInfo;->getCountryCodeProperty()Ljava/lang/String;

    move-result-object v0

    .line 11
    .local v0, "countryCode":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 12
    const-string v2, "KOREA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 13
    const/4 v1, 0x1

    .line 16
    :cond_0
    return v1
.end method
