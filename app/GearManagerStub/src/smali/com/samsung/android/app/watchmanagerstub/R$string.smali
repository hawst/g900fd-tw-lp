.class public final Lcom/samsung/android/app/watchmanagerstub/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final alert_message:I = 0x7f090023

.field public static final alert_message_title:I = 0x7f090022

.field public static final app_name:I = 0x7f090000

.field public static final app_name_fit:I = 0x7f090001

.field public static final app_size:I = 0x7f09000f

.field public static final cancel:I = 0x7f09000b

.field public static final connect_to_wlan_continue:I = 0x7f09002e

.field public static final connect_via_mobile_network_body:I = 0x7f09002b

.field public static final connecting_to_server:I = 0x7f090010

.field public static final data_connection_chn_title:I = 0x7f09002f

.field public static final dont_show_dialog:I = 0x7f09002c

.field public static final download:I = 0x7f09000c

.field public static final download_information:I = 0x7f09000a

.field public static final downloading:I = 0x7f090007

.field public static final error_message_no_network:I = 0x7f090012

.field public static final error_message_no_network_china:I = 0x7f090011

.field public static final error_message_no_network_for_non_phone:I = 0x7f090013

.field public static final gear:I = 0x7f090002

.field public static final gear_fit:I = 0x7f090003

.field public static final gearfitmanager_information:I = 0x7f090009

.field public static final gearmanager_information:I = 0x7f090008

.field public static final install:I = 0x7f090004

.field public static final install_warning:I = 0x7f09001e

.field public static final install_warning_kor:I = 0x7f090021

.field public static final installation:I = 0x7f09001f

.field public static final later:I = 0x7f09001c

.field public static final manager_download_information:I = 0x7f090028

.field public static final mb:I = 0x7f090006

.field public static final message_data_roaming_is_disabled:I = 0x7f090018

.field public static final message_data_roaming_is_disabled_china:I = 0x7f090017

.field public static final message_flight_mode_is_enabled:I = 0x7f090015

.field public static final message_flight_mode_is_enabled_china:I = 0x7f090014

.field public static final message_mobile_data_is_disabled:I = 0x7f09000e

.field public static final message_mobile_data_is_disabled_china:I = 0x7f090016

.field public static final message_no_signal_found:I = 0x7f090019

.field public static final message_no_signal_found_for_non_phone:I = 0x7f09001b

.field public static final message_no_signal_found_for_non_phone_china:I = 0x7f09001a

.field public static final no_contents:I = 0x7f090029

.field public static final no_network_connection:I = 0x7f09000d

.field public static final ok:I = 0x7f090024

.field public static final retry:I = 0x7f09001d

.field public static final samsungapps_url:I = 0x7f090020

.field public static final self_upgrade_text_1:I = 0x7f090026

.field public static final self_upgrade_text_2:I = 0x7f090027

.field public static final self_upgrade_text_popup:I = 0x7f09002a

.field public static final self_upgrade_title:I = 0x7f090025

.field public static final size:I = 0x7f090005

.field public static final wlan_connection_chn_title:I = 0x7f09002d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
