.class public final Lcom/samsung/android/app/watchmanagerstub/Utilities;
.super Ljava/lang/Object;
.source "Utilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/watchmanagerstub/Utilities$MyAlertDialog;
    }
.end annotation


# static fields
.field public static final BLE_PACKET_FORMAT_VERSION:B = 0x1t

.field public static final FLAG_DEVICETYPE_P2P_SUPPORT:B = -0x80t

.field public static final FLAG_PACKETINFO_CMD:B = 0x4t

.field public static final FLAG_PACKETINFO_CONTACT:B = 0x1t

.field public static final FLAG_PACKETINFO_RESPMODE:B = 0x2t

.field public static final FLAG_PACKETINFO_SCANMODE:B = 0x8t

.field public static final MANUFACTURER_FLAG:B = -0x1t

.field public static final MANUFACTURER_ID_SAMSUNG:B = 0x75t

.field public static final SERVICE_ID_SCONNECT:B = 0x1t

.field public static final SERVICE_ID_WEARABLE:B = 0x2t

.field static final TAG:Ljava/lang/String; = "WatchManagerStub.Utilities"

.field public static mDeviceType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static DeleteDir(Ljava/lang/String;)V
    .locals 10
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 119
    const-string v5, "WatchManagerStub.Utilities"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DeleteDir path = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 122
    .local v3, "file":Ljava/io/File;
    if-eqz v3, :cond_0

    .line 123
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 124
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 125
    .local v2, "childFileList":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 126
    const-string v5, "WatchManagerStub.Utilities"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "DeleteDir childFileList len = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    array-length v6, v2

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_1

    .line 135
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    .line 136
    .local v4, "fileDeleteCheck":Z
    const-string v5, "WatchManagerStub.Utilities"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fileDeleteCheck = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    .end local v2    # "childFileList":[Ljava/io/File;
    .end local v4    # "fileDeleteCheck":Z
    :cond_0
    :goto_1
    return-void

    .line 127
    .restart local v2    # "childFileList":[Ljava/io/File;
    :cond_1
    aget-object v0, v2, v5

    .line 128
    .local v0, "childFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 129
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->DeleteDir(Ljava/lang/String;)V

    .line 127
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 131
    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 132
    .local v1, "childFileDeleteCheck":Z
    const-string v7, "WatchManagerStub.Utilities"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "childFileDeleteCheck = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 139
    .end local v0    # "childFile":Ljava/io/File;
    .end local v1    # "childFileDeleteCheck":Z
    .end local v2    # "childFileList":[Ljava/io/File;
    :cond_3
    const-string v5, "WatchManagerStub.Utilities"

    const-string v6, "DeleteDir path not exist!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getBTName(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "BTAddress"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    .line 34
    .local v3, "mBluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v3, :cond_1

    .line 35
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 37
    const-string v0, "Bluetooth"

    .line 40
    .local v0, "btName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v3, p0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 41
    .local v1, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 46
    .end local v1    # "device":Landroid/bluetooth/BluetoothDevice;
    :goto_0
    const-string v4, "WatchManagerStub.Utilities"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getBTName = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-static {v0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getSimpleBTName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 54
    .end local v0    # "btName":Ljava/lang/String;
    :goto_1
    return-object v4

    .line 42
    .restart local v0    # "btName":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 43
    .local v2, "ie":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 49
    .end local v0    # "btName":Ljava/lang/String;
    .end local v2    # "ie":Ljava/lang/IllegalArgumentException;
    :cond_0
    const-string v4, "WatchManagerStub.Utilities"

    const-string v5, "mBluetoothAdapter is disable "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :goto_2
    const/4 v4, 0x0

    goto :goto_1

    .line 52
    :cond_1
    const-string v4, "WatchManagerStub.Utilities"

    const-string v5, "mBluetoothAdapter is null "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static getDeviceTheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    const-string v0, "ro.build.scafe.cream"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getMobileDataStatus(Landroid/content/Context;)Landroid/net/NetworkInfo;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 315
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 316
    .local v0, "manager":Landroid/net/ConnectivityManager;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 317
    .local v1, "mobile":Landroid/net/NetworkInfo;
    return-object v1
.end method

.method static getPreference(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-virtual {p0, p1, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 331
    .local v0, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static getSConnectDataFromBLE([B)Ljava/lang/String;
    .locals 10
    .param p0, "scanRecord"    # [B

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 243
    if-nez p0, :cond_1

    .line 244
    const-string v5, "WatchManagerStub.Utilities"

    const-string v6, "jangil::getSConnectDataFromBLE::scanRecord is null"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    :goto_0
    return-object v4

    .line 247
    :cond_1
    const-string v5, "WatchManagerStub.Utilities"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "jangil::BLE data::"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    array-length v5, p0

    if-ge v5, v8, :cond_2

    .line 249
    const-string v5, "WatchManagerStub.Utilities"

    const-string v6, "getSConnectDataFromBLE length < 1"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 252
    :cond_2
    const/4 v1, 0x0

    .line 254
    .local v1, "offset":I
    :cond_3
    :goto_1
    array-length v5, p0

    add-int/lit8 v5, v5, -0x2

    if-ge v1, v5, :cond_0

    .line 255
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .local v2, "offset":I
    aget-byte v0, p0, v1

    .line 256
    .local v0, "len":I
    if-nez v0, :cond_4

    move v1, v2

    .line 257
    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    goto :goto_0

    .line 260
    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    :cond_4
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    aget-byte v3, p0, v2

    .line 261
    .local v3, "type":B
    packed-switch v3, :pswitch_data_0

    goto :goto_1

    .line 263
    :pswitch_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    aget-byte v5, p0, v1

    if-nez v5, :cond_9

    .line 264
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    aget-byte v5, p0, v2

    const/16 v6, 0x75

    if-ne v5, v6, :cond_3

    .line 269
    aget-byte v5, p0, v1

    if-ne v5, v8, :cond_5

    .line 270
    add-int/lit8 v1, v1, 0x1

    .line 277
    aget-byte v5, p0, v1

    if-nez v5, :cond_3

    .line 279
    add-int/lit8 v5, v1, 0x1

    aget-byte v5, p0, v5

    if-ne v5, v9, :cond_3

    .line 283
    add-int/lit8 v5, v1, 0x2

    aget-byte v5, p0, v5

    if-nez v5, :cond_3

    .line 284
    add-int/lit8 v5, v1, 0x3

    aget-byte v5, p0, v5

    if-nez v5, :cond_6

    .line 286
    const-string v5, "WatchManagerStub.Utilities"

    const-string v6, "getSConnectDataFromBLE Unknown service/deviceID"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 272
    :cond_5
    const-string v5, "WatchManagerStub.Utilities"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "getSConnectDataFromBLE Unknown BLE version code : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 273
    aget-byte v7, p0, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 272
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 288
    :cond_6
    add-int/lit8 v4, v1, 0x3

    aget-byte v4, p0, v4

    if-ne v4, v8, :cond_7

    .line 289
    const-string v4, "WatchManagerStub.Utilities"

    const-string v5, "jangil::this is Gear2~ GearX"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    const-string v4, "watchmanager"

    goto/16 :goto_0

    .line 291
    :cond_7
    add-int/lit8 v4, v1, 0x3

    aget-byte v4, p0, v4

    if-ne v4, v9, :cond_8

    .line 292
    const-string v4, "WatchManagerStub.Utilities"

    const-string v5, "jangil::this is Gear Fit"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    const-string v4, "wms"

    goto/16 :goto_0

    .line 295
    :cond_8
    const-string v4, "WatchManagerStub.Utilities"

    const-string v5, "jangil::this is unknown. but need to update."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    const-string v4, "other"

    goto/16 :goto_0

    .end local v1    # "offset":I
    .restart local v2    # "offset":I
    :cond_9
    move v1, v2

    .end local v2    # "offset":I
    .restart local v1    # "offset":I
    goto/16 :goto_1

    .line 261
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static getSimpleBTName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "BTName"    # Ljava/lang/String;

    .prologue
    .line 57
    if-nez p0, :cond_1

    const/4 p0, 0x0

    .line 65
    .end local p0    # "BTName":Ljava/lang/String;
    .local v0, "lastPosition":I
    .local v1, "simpleName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 58
    .end local v0    # "lastPosition":I
    .end local v1    # "simpleName":Ljava/lang/String;
    .restart local p0    # "BTName":Ljava/lang/String;
    :cond_1
    const-string v2, "WatchManagerStub.Utilities"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getSimpleBTName BTName = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    const/4 v1, 0x0

    .line 60
    .restart local v1    # "simpleName":Ljava/lang/String;
    const-string v2, "("

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 61
    .restart local v0    # "lastPosition":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 63
    const/4 v2, 0x0

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 64
    const-string v2, "WatchManagerStub.Utilities"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getSimpleBTName simpleName = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object p0, v1

    .line 65
    goto :goto_0
.end method

.method public static getWatchManagerVersionCode(Landroid/content/Context;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 197
    const/4 v3, 0x0

    .line 198
    .local v3, "versionCode":I
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 201
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v5, "com.samsung.android.app.watchmanager"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 202
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    const-string v5, "WatchManagerStub.Utilities"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "watchmanager versionCode : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " versionName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move v4, v3

    .line 212
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v4

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 207
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 208
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method static getWifiStatus(Landroid/content/Context;)Landroid/net/NetworkInfo;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 309
    const-string v2, "connectivity"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 310
    .local v0, "manager":Landroid/net/ConnectivityManager;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 311
    .local v1, "wifi":Landroid/net/NetworkInfo;
    return-object v1
.end method

.method public static isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4
    .param p0, "strAppPackage"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 184
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 187
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v1, p0, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    :goto_0
    return v2

    .line 189
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/Utilities;->mDeviceType:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/Utilities;->mDeviceType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 147
    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/Utilities;->mDeviceType:Ljava/lang/String;

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 150
    :goto_0
    return v0

    .line 149
    :cond_0
    const-string v0, "ro.build.characteristics"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/watchmanagerstub/Utilities;->mDeviceType:Ljava/lang/String;

    .line 150
    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/Utilities;->mDeviceType:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/Utilities;->mDeviceType:Ljava/lang/String;

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static launchApp(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "postPkgName"    # Ljava/lang/String;
    .param p2, "state"    # Z

    .prologue
    .line 154
    const-string v3, "connect_wearable"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 155
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v3, "MAC"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "btAddress":Ljava/lang/String;
    const-string v1, "com.samsung.android.app.watchmanager"

    .line 158
    .local v1, "pkgName":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 159
    const-string v3, "WatchManagerStub.Utilities"

    const-string v4, "jangil::deviceName is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    const-string v3, "watchmanager"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 164
    const-string v1, "com.samsung.android.app.watchmanager"

    .line 166
    invoke-static {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getWatchManagerVersionCode(Landroid/content/Context;)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 167
    :cond_2
    invoke-static {p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->startMainActivity(Landroid/content/Context;)V

    goto :goto_0

    .line 169
    :cond_3
    const-string v3, "WatchManagerStub.Utilities"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "jangil::launchApp::"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "::btAddress::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-static {p0, v0, p2}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->startGearManager(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 173
    :cond_4
    const-string v3, "wms"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 174
    const-string v1, "com.samsung.android.wms"

    .line 175
    invoke-static {v1, p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 176
    const-string v3, "WatchManagerStub.Utilities"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "jangil::launchApp::"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "::btAddress::"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-static {p0, v0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->startWingTipManager(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 179
    :cond_5
    invoke-static {p0}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->startMainActivity(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static removeAllPreferences(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "_context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 358
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 359
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 360
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 361
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 362
    return-void
.end method

.method static setPreference(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 321
    .line 322
    const/4 v2, 0x0

    .line 321
    invoke-virtual {p0, p1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 323
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 324
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 325
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 326
    return-void
.end method

.method public static startGearManager(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "macAddr"    # Ljava/lang/String;
    .param p2, "state"    # Z

    .prologue
    const/4 v8, 0x0

    .line 68
    new-instance v0, Landroid/content/ComponentName;

    const-string v5, "com.samsung.android.app.watchmanager"

    .line 69
    const-string v6, "com.samsung.android.app.watchmanager.setupwizard.SetupWizardWelcomeActivity"

    .line 68
    invoke-direct {v0, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .local v0, "compName":Landroid/content/ComponentName;
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v3, "intentBT":Landroid/content/Intent;
    if-eqz p1, :cond_0

    .line 73
    const-string v5, "BT_ADD_FROM_STUB"

    invoke-virtual {v3, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getBTName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, "deviceName":Ljava/lang/String;
    const-string v5, "MODEL_NAME"

    invoke-virtual {v3, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const/high16 v5, 0x10000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 78
    const-string v5, "WatchManagerStub.Utilities"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "jangil::startGearManager::btaddr::"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "::deviceName::"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    if-nez p2, :cond_1

    .line 81
    const-string v5, "WatchManagerStub.Utilities"

    const-string v6, "jangil::startActivity(UHM)"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string v5, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 84
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 92
    :goto_0
    const-string v5, "connect_wearable"

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 93
    .local v4, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 94
    .local v2, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v5, "MAC"

    invoke-interface {v2, v5, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 95
    const-string v5, "WM_MANAGER"

    invoke-interface {v2, v5, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 96
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 97
    return-void

    .line 87
    .end local v2    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v4    # "pref":Landroid/content/SharedPreferences;
    :cond_1
    const-string v5, "WatchManagerStub.Utilities"

    const-string v6, "jangil::sendbroadcast(UHM)"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const-string v5, "com.samsung.android.action.BSSF_LAUNCH"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static startMainActivity(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 216
    const-string v1, "WatchManagerStub.Utilities"

    const-string v2, "jangil::startActivity()!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 218
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 219
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 220
    return-void
.end method

.method public static startOtherManager(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "macAddr"    # Ljava/lang/String;

    .prologue
    .line 116
    const-string v0, "WatchManagerStub.Utilities"

    const-string v1, "startOtherManager is not implemented."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    return-void
.end method

.method public static startWingTipManager(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "macAddr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 100
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 101
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.samsung.android.wms.PERMISSION_ACTION"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 102
    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    if-eqz p1, :cond_0

    .line 104
    const-string v3, "bt_addr"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    :cond_0
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 107
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 108
    const-string v3, "WatchManagerStub.Utilities"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "jangil::startWingTipManager()::macAddr::"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const-string v3, "connect_wearable"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 110
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 111
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v3, "MAC"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 112
    const-string v3, "WM_MANAGER"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 113
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 114
    return-void
.end method
