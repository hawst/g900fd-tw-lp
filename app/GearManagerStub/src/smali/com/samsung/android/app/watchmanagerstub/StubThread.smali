.class public Lcom/samsung/android/app/watchmanagerstub/StubThread;
.super Ljava/lang/Thread;
.source "StubThread.java"


# static fields
.field private static final CSC_PATH:Ljava/lang/String; = "/system/csc/sales_code.dat"

.field private static final DownloadConnectTimeOut:I = 0x2710

.field private static final DownloadSocketTimeOut:I = 0x7530

.field private static final PD_TEST_PATH:Ljava/lang/String; = "/sdcard/go_to_andromeda.test"

.field protected static final SERVER_URL_CHECK:Ljava/lang/String; = "http://vas.samsungapps.com/stub/stubUpdateCheck.as"

.field protected static final SERVER_URL_DOWNLOAD:Ljava/lang/String; = "https://vas.samsungapps.com/stub/stubDownload.as"

.field private static final TAG:Ljava/lang/String; = "StubThread"

.field private static final UpdateConnectTimeOut:I = 0x1388

.field private static final UpdateSocketTimeOut:I = 0x3a98


# instance fields
.field private _HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

.field private _Handler:Landroid/os/Handler;

.field protected mContext:Landroid/content/Context;

.field private mFlagCancel:Z

.field private mHandler:Landroid/os/Handler;

.field protected mPackageName:Ljava/lang/String;

.field protected mPackageVersion:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "postName"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 61
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageVersion:Ljava/lang/String;

    .line 63
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mFlagCancel:Z

    .line 65
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_Handler:Landroid/os/Handler;

    .line 76
    iput-object p1, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mHandler:Landroid/os/Handler;

    .line 79
    const-string v3, "com.samsung.android.app.watchmanager"

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    .line 81
    const-string v3, "self"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 82
    const-string v3, "com.samsung.android.app.watchmanagerstub"

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    .line 91
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 92
    .local v2, "manager":Landroid/content/pm/PackageManager;
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    const/16 v4, 0x1000

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 93
    .local v0, "PkgInfo":Landroid/content/pm/PackageInfo;
    new-instance v3, Ljava/lang/StringBuilder;

    iget v4, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageVersion:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    .end local v0    # "PkgInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "manager":Landroid/content/pm/PackageManager;
    :goto_1
    new-instance v3, Lcom/sec/android/applicationmgr/HFileUtil;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Lcom/sec/android/applicationmgr/HFileUtil;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    .line 101
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    invoke-virtual {v3}, Lcom/sec/android/applicationmgr/HFileUtil;->prepare()Z

    .line 102
    return-void

    .line 83
    :cond_1
    const-string v3, "watchmanager"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 84
    const-string v3, "com.samsung.android.app.watchmanager"

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    goto :goto_0

    .line 85
    :cond_2
    const-string v3, "wms"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    const-string v3, "com.samsung.android.wms"

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    goto :goto_0

    .line 94
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "0"

    iput-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageVersion:Ljava/lang/String;

    .line 97
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/app/watchmanagerstub/StubThread;)Z
    .locals 1

    .prologue
    .line 656
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->installApk()Z

    move-result v0

    return v0
.end method

.method private checkApkSignature()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 397
    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    const-string v6, "signature"

    invoke-virtual {v5, v6, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 398
    .local v3, "sp":Landroid/content/SharedPreferences;
    const-string v5, "signature"

    const-string v6, ""

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400
    .local v2, "signature":Ljava/lang/String;
    new-instance v0, Lcom/sec/android/applicationmgr/SigChecker;

    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/sec/android/applicationmgr/SigChecker;-><init>(Landroid/content/Context;)V

    .line 401
    .local v0, "c":Lcom/sec/android/applicationmgr/SigChecker;
    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    invoke-virtual {v5}, Lcom/sec/android/applicationmgr/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v1

    .line 402
    .local v1, "f":Ljava/io/File;
    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    const-string v6, "signature"

    invoke-static {v5, v6}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->removeAllPreferences(Landroid/content/Context;Ljava/lang/String;)V

    .line 403
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, v2}, Lcom/sec/android/applicationmgr/SigChecker;->validate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 404
    const/4 v4, 0x1

    .line 406
    :cond_0
    return v4
.end method

.method private checkSize(Ljava/lang/String;)Z
    .locals 21
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 419
    const/4 v15, 0x0

    .line 424
    .local v15, "result":Z
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v6

    .line 425
    .local v6, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v11

    .line 427
    .local v11, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v4, Ljava/net/URI;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 428
    .local v4, "Uri":Ljava/net/URI;
    new-instance v8, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v8}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 429
    .local v8, "httpclient":Lorg/apache/http/client/HttpClient;
    invoke-interface {v8}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v10

    .line 430
    .local v10, "params":Lorg/apache/http/params/HttpParams;
    const/16 v18, 0x1388

    move/from16 v0, v18

    invoke-static {v10, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 431
    const/16 v18, 0x3a98

    move/from16 v0, v18

    invoke-static {v10, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 433
    new-instance v7, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v7, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 434
    .local v7, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    invoke-virtual {v7, v4}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 435
    invoke-interface {v8, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v14

    .line 437
    .local v14, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v14}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 439
    .local v3, "InStream":Ljava/io/InputStream;
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-interface {v11, v3, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 441
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v12

    .line 443
    .local v12, "parserEvent":I
    const-string v9, ""

    .local v9, "id":Ljava/lang/String;
    const-string v13, ""

    .line 444
    .local v13, "res":Ljava/lang/String;
    const-string v2, ""

    .line 446
    .local v2, "DownloadURI":Ljava/lang/String;
    :goto_0
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v12, v0, :cond_0

    .line 514
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "InStream":Ljava/io/InputStream;
    .end local v4    # "Uri":Ljava/net/URI;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v9    # "id":Ljava/lang/String;
    .end local v10    # "params":Lorg/apache/http/params/HttpParams;
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v12    # "parserEvent":I
    .end local v13    # "res":Ljava/lang/String;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    :goto_1
    return v15

    .line 448
    .restart local v2    # "DownloadURI":Ljava/lang/String;
    .restart local v3    # "InStream":Ljava/io/InputStream;
    .restart local v4    # "Uri":Ljava/net/URI;
    .restart local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v9    # "id":Ljava/lang/String;
    .restart local v10    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v12    # "parserEvent":I
    .restart local v13    # "res":Ljava/lang/String;
    .restart local v14    # "response":Lorg/apache/http/HttpResponse;
    :cond_0
    const/16 v18, 0x2

    move/from16 v0, v18

    if-ne v12, v0, :cond_3

    .line 450
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v16

    .line 451
    .local v16, "tag":Ljava/lang/String;
    const-string v18, "appId"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 453
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .line 454
    .local v17, "type":I
    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    .line 456
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    .line 457
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "jangil::checkSIze::appId : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    .end local v17    # "type":I
    :cond_1
    const-string v18, "resultCode"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 462
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .line 463
    .restart local v17    # "type":I
    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 465
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v13

    .line 466
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "StubDownload result : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    .end local v17    # "type":I
    :cond_2
    const-string v18, "downloadURI"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3

    .line 471
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v17

    .line 472
    .restart local v17    # "type":I
    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 474
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->trimCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 475
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "downloadURI : "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    .end local v16    # "tag":Ljava/lang/String;
    .end local v17    # "type":I
    :cond_3
    const/16 v18, 0x3

    move/from16 v0, v18

    if-ne v12, v0, :cond_4

    .line 481
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v16

    .line 482
    .restart local v16    # "tag":Ljava/lang/String;
    const-string v18, "downloadURI"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 484
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v13, v2}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getAppSize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v15

    .line 485
    const-string v9, ""

    .line 486
    const-string v13, ""

    .line 490
    .end local v16    # "tag":Ljava/lang/String;
    :cond_4
    invoke-interface {v11}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_4

    move-result v12

    goto/16 :goto_0

    .line 492
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "InStream":Ljava/io/InputStream;
    .end local v4    # "Uri":Ljava/net/URI;
    .end local v6    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v7    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v8    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v9    # "id":Ljava/lang/String;
    .end local v10    # "params":Lorg/apache/http/params/HttpParams;
    .end local v11    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v12    # "parserEvent":I
    .end local v13    # "res":Ljava/lang/String;
    .end local v14    # "response":Lorg/apache/http/HttpResponse;
    :catch_0
    move-exception v5

    .line 493
    .local v5, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "CheckSize_XmlPullParserException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 495
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1

    .line 496
    .end local v5    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v5

    .line 497
    .local v5, "e":Ljava/net/SocketException;
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "CheckSize_SocketException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    invoke-virtual {v5}, Ljava/net/SocketException;->printStackTrace()V

    .line 499
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1

    .line 500
    .end local v5    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v5

    .line 501
    .local v5, "e":Ljava/net/UnknownHostException;
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "CheckSize_UnknownHostException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    invoke-virtual {v5}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 503
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1

    .line 504
    .end local v5    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v5

    .line 505
    .local v5, "e":Ljava/io/IOException;
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "CheckSize_IOException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1

    .line 508
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 509
    .local v5, "e":Ljava/net/URISyntaxException;
    const-string v18, "StubThread"

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "CheckSize_URISyntaxException - "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 510
    invoke-virtual {v5}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 511
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1
.end method

.method private downloadApk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 21
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "URI"    # Ljava/lang/String;

    .prologue
    .line 574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    const-string v17, "1"

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    const-string v17, ""

    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_3

    .line 576
    const/4 v7, 0x0

    .line 578
    .local v7, "file":Ljava/io/File;
    new-instance v9, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v9}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 579
    .local v9, "httpclient":Lorg/apache/http/client/HttpClient;
    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    .line 580
    .local v11, "params":Lorg/apache/http/params/HttpParams;
    const/16 v17, 0x2710

    move/from16 v0, v17

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 581
    const/16 v17, 0x7530

    move/from16 v0, v17

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 583
    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v8}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 585
    .local v8, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/applicationmgr/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v7

    .line 590
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_0

    .line 592
    const-string v17, "Range"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "bytes="

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v19

    invoke-virtual/range {v18 .. v20}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_0
    const-wide/16 v15, 0x0

    .line 598
    .local v15, "sizeTotal":J
    :try_start_0
    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v13

    .line 600
    .local v13, "sizeDownload":J
    new-instance v17, Ljava/net/URI;

    move-object/from16 v0, v17

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 602
    invoke-interface {v9, v8}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 603
    .local v12, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v4

    .line 605
    .local v4, "InStream":Ljava/io/InputStream;
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v17

    add-long v15, v17, v13

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/applicationmgr/HFileUtil;->getFileName()Ljava/lang/String;

    move-result-object v18

    const v19, 0x8001

    invoke-virtual/range {v17 .. v19}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v3

    .line 610
    .local v3, "Fout":Ljava/io/FileOutputStream;
    const/16 v17, 0x400

    move/from16 v0, v17

    new-array v5, v0, [B

    .line 611
    .local v5, "buffer":[B
    const/4 v10, 0x0

    .line 613
    .local v10, "len1":I
    const-string v17, "DOWNLOAD_STATUS"

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 615
    :goto_0
    invoke-virtual {v4, v5}, Ljava/io/InputStream;->read([B)I

    move-result v10

    if-gtz v10, :cond_1

    .line 633
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 649
    .end local v3    # "Fout":Ljava/io/FileOutputStream;
    .end local v4    # "InStream":Ljava/io/InputStream;
    .end local v5    # "buffer":[B
    .end local v10    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v13    # "sizeDownload":J
    :goto_1
    const-string v17, "StubThread"

    const-string v18, "Download complete."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    const-string v17, "DOWNLOAD_STATUS"

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 651
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mFlagCancel:Z

    .line 652
    const/16 v17, 0x1

    .line 654
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v9    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v11    # "params":Lorg/apache/http/params/HttpParams;
    .end local v15    # "sizeTotal":J
    :goto_2
    return v17

    .line 617
    .restart local v3    # "Fout":Ljava/io/FileOutputStream;
    .restart local v4    # "InStream":Ljava/io/InputStream;
    .restart local v5    # "buffer":[B
    .restart local v7    # "file":Ljava/io/File;
    .restart local v8    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v9    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v10    # "len1":I
    .restart local v11    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v12    # "response":Lorg/apache/http/HttpResponse;
    .restart local v13    # "sizeDownload":J
    .restart local v15    # "sizeTotal":J
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mFlagCancel:Z

    move/from16 v17, v0

    if-eqz v17, :cond_2

    .line 619
    const-string v17, "StubThread"

    const-string v18, "Cancel Download"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    .line 623
    const-string v17, "DOWNLOAD_STATUS"

    const/16 v18, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 624
    const/16 v17, 0x0

    goto :goto_2

    .line 627
    :cond_2
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v3, v5, v0, v10}, Ljava/io/FileOutputStream;->write([BII)V

    .line 629
    int-to-long v0, v10

    move-wide/from16 v17, v0

    add-long v13, v13, v17

    .line 631
    const-string v17, "DOWNLOA_RATIO"

    const-wide/16 v18, 0x64

    mul-long v18, v18, v13

    div-long v18, v18, v15

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 635
    .end local v3    # "Fout":Ljava/io/FileOutputStream;
    .end local v4    # "InStream":Ljava/io/InputStream;
    .end local v5    # "buffer":[B
    .end local v10    # "len1":I
    .end local v12    # "response":Lorg/apache/http/HttpResponse;
    .end local v13    # "sizeDownload":J
    :catch_0
    move-exception v6

    .line 636
    .local v6, "e":Ljava/io/IOException;
    const-string v17, "StubThread"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "IOException - "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionDownload()V

    goto :goto_1

    .line 639
    .end local v6    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 641
    .local v6, "e":Ljava/lang/Exception;
    const-string v17, "StubThread"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Download fail - "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    const-string v17, "DOWNLOAD_STATUS"

    const/16 v18, 0xa

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 644
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionDownload()V

    .line 646
    const/16 v17, 0x0

    goto/16 :goto_2

    .line 654
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v9    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v11    # "params":Lorg/apache/http/params/HttpParams;
    .end local v15    # "sizeTotal":J
    :cond_3
    const/16 v17, 0x0

    goto/16 :goto_2
.end method

.method private getAppSize(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 17
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;
    .param p3, "URI"    # Ljava/lang/String;

    .prologue
    .line 518
    const/4 v3, 0x0

    .line 520
    .local v3, "file":Ljava/io/File;
    new-instance v5, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v5}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 521
    .local v5, "httpclient":Lorg/apache/http/client/HttpClient;
    invoke-interface {v5}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v7

    .line 522
    .local v7, "params":Lorg/apache/http/params/HttpParams;
    const/16 v13, 0x1388

    invoke-static {v7, v13}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 523
    const/16 v13, 0x3a98

    invoke-static {v7, v13}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 525
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4}, Lorg/apache/http/client/methods/HttpGet;-><init>()V

    .line 527
    .local v4, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    invoke-virtual {v13}, Lcom/sec/android/applicationmgr/HFileUtil;->getFile()Ljava/io/File;

    move-result-object v3

    .line 529
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 531
    const-string v13, "Range"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "bytes="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v4, v13, v14}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :cond_0
    const-wide/16 v11, 0x0

    .line 537
    .local v11, "sizeTotal":J
    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v9

    .line 539
    .local v9, "sizeDownload":J
    new-instance v13, Ljava/net/URI;

    move-object/from16 v0, p3

    invoke-direct {v13, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v13}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 541
    invoke-interface {v5, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 543
    .local v8, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v13

    add-long v11, v13, v9

    .line 545
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 546
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v13, "appsize"

    long-to-int v14, v11

    invoke-virtual {v1, v13, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 547
    const-string v13, "MESSAGE_CMD"

    const-string v14, "SEND_APPSIZE"

    invoke-virtual {v1, v13, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    .line 549
    .local v6, "msg":Landroid/os/Message;
    invoke-virtual {v6, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 550
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v13, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_3

    .line 570
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v6    # "msg":Landroid/os/Message;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .end local v9    # "sizeDownload":J
    :goto_0
    const/4 v13, 0x1

    return v13

    .line 552
    :catch_0
    move-exception v2

    .line 553
    .local v2, "e":Ljava/net/SocketException;
    const-string v13, "StubThread"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "getAppSize_SocketException - "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    invoke-virtual {v2}, Ljava/net/SocketException;->printStackTrace()V

    .line 555
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto :goto_0

    .line 556
    .end local v2    # "e":Ljava/net/SocketException;
    :catch_1
    move-exception v2

    .line 557
    .local v2, "e":Ljava/net/UnknownHostException;
    const-string v13, "StubThread"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "getAppSize_UnknownHostException - "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    invoke-virtual {v2}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 559
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto :goto_0

    .line 560
    .end local v2    # "e":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v2

    .line 561
    .local v2, "e":Ljava/io/IOException;
    const-string v13, "StubThread"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "getAppSize_IOException - "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 563
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto :goto_0

    .line 564
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 565
    .local v2, "e":Ljava/net/URISyntaxException;
    const-string v13, "StubThread"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "getAppSize_URISyntaxException - "

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    invoke-virtual {v2}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 567
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_0
.end method

.method private getCSCVersion()Ljava/lang/String;
    .locals 9

    .prologue
    .line 855
    const/4 v6, 0x0

    .line 856
    .local v6, "s":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    const-string v8, "/system/csc/sales_code.dat"

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 857
    .local v4, "mFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 859
    const/16 v8, 0x14

    new-array v0, v8, [B

    .line 860
    .local v0, "buffer":[B
    const/4 v2, 0x0

    .line 864
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 866
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 867
    .local v5, "ret_val":I
    if-eqz v5, :cond_1

    .line 868
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .end local v6    # "s":Ljava/lang/String;
    .local v7, "s":Ljava/lang/String;
    move-object v6, v7

    .line 882
    .end local v7    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    :goto_0
    if-eqz v3, :cond_0

    .line 884
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 892
    .end local v0    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v5    # "ret_val":I
    :cond_0
    :goto_1
    return-object v6

    .line 872
    .restart local v0    # "buffer":[B
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v5    # "ret_val":I
    :cond_1
    :try_start_3
    new-instance v7, Ljava/lang/String;

    const-string v8, "FAIL"

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .end local v6    # "s":Ljava/lang/String;
    .restart local v7    # "s":Ljava/lang/String;
    move-object v6, v7

    .line 874
    .end local v7    # "s":Ljava/lang/String;
    .restart local v6    # "s":Ljava/lang/String;
    goto :goto_0

    .line 875
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v5    # "ret_val":I
    .restart local v2    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v8

    .line 882
    :goto_2
    if-eqz v2, :cond_0

    .line 884
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 885
    :catch_1
    move-exception v1

    .line 887
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 878
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    .line 882
    :goto_3
    if-eqz v2, :cond_0

    .line 884
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_1

    .line 885
    :catch_3
    move-exception v1

    .line 887
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 881
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 882
    :goto_4
    if-eqz v2, :cond_2

    .line 884
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 890
    :cond_2
    :goto_5
    throw v8

    .line 885
    :catch_4
    move-exception v1

    .line 887
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 885
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v5    # "ret_val":I
    :catch_5
    move-exception v1

    .line 887
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 881
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "ret_val":I
    :catchall_1
    move-exception v8

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 878
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_6
    move-exception v8

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 875
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_7
    move-exception v8

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_2
.end method

.method private installApk()Z
    .locals 7

    .prologue
    .line 659
    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    invoke-virtual {v5}, Lcom/sec/android/applicationmgr/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 660
    .local v1, "folder":Ljava/io/File;
    const/4 v3, 0x0

    .line 661
    .local v3, "result":Z
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 663
    const-string v5, "DOWNLOAD_STATUS"

    const/16 v6, 0x8

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    move v4, v3

    .line 695
    .end local v3    # "result":Z
    .local v4, "result":I
    :goto_0
    return v4

    .line 668
    .end local v4    # "result":I
    .restart local v3    # "result":Z
    :cond_0
    :try_start_0
    new-instance v2, Lcom/sec/android/applicationmgr/ApplicationManager;

    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5}, Lcom/sec/android/applicationmgr/ApplicationManager;-><init>(Landroid/content/Context;)V

    .line 669
    .local v2, "mgr":Lcom/sec/android/applicationmgr/ApplicationManager;
    new-instance v5, Lcom/samsung/android/app/watchmanagerstub/StubThread$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread$2;-><init>(Lcom/samsung/android/app/watchmanagerstub/StubThread;)V

    invoke-virtual {v2, v5}, Lcom/sec/android/applicationmgr/ApplicationManager;->setOnInstalledPackaged(Lcom/sec/android/applicationmgr/OnInstalledPackaged;)V

    .line 680
    const-string v5, "DOWNLOAD_STATUS"

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 681
    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_HFileUtil:Lcom/sec/android/applicationmgr/HFileUtil;

    invoke-virtual {v5}, Lcom/sec/android/applicationmgr/HFileUtil;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/android/applicationmgr/ApplicationManager;->installPackage(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 682
    const/4 v3, 0x1

    .end local v2    # "mgr":Lcom/sec/android/applicationmgr/ApplicationManager;
    :goto_1
    move v4, v3

    .line 695
    .restart local v4    # "result":I
    goto :goto_0

    .line 684
    .end local v4    # "result":I
    :catch_0
    move-exception v0

    .line 685
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1

    .line 686
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 687
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    .line 688
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 689
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 690
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 691
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    .line 692
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 693
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method private isCSCExistFile()Z
    .locals 4

    .prologue
    .line 897
    const/4 v1, 0x0

    .line 898
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string v2, "/system/csc/sales_code.dat"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 902
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 903
    if-nez v1, :cond_0

    .line 905
    const-string v2, "StubThread"

    const-string v3, "CSC is not exist"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 913
    :cond_0
    :goto_0
    return v1

    .line 908
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private runDownload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    .line 207
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 208
    const-string v9, "StubThread"

    const-string v10, "jangil::runDownload::return!!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const-string v9, "2"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 214
    const-string v9, "StubThread"

    const-string v10, "jangil::runDownload::code == 2"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 217
    .local v6, "szModel":Ljava/lang/String;
    const-string v7, "SAMSUNG-"

    .line 219
    .local v7, "szPrefix":Ljava/lang/String;
    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 221
    const-string v9, ""

    invoke-virtual {v6, v7, v9}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 225
    :cond_2
    const-string v4, ""

    .line 226
    .local v4, "szMCC":Ljava/lang/String;
    const-string v5, ""

    .line 231
    .local v5, "szMNC":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    const-string v10, "connectivity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 232
    .local v0, "cManager":Landroid/net/ConnectivityManager;
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 233
    .local v1, "mobile":Landroid/net/NetworkInfo;
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v8

    .line 235
    .local v8, "wifi":Landroid/net/NetworkInfo;
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v9

    if-nez v9, :cond_4

    :cond_3
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 237
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getMCC()Ljava/lang/String;

    move-result-object v4

    .line 238
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getMNC()Ljava/lang/String;

    move-result-object v5

    .line 247
    const-string v3, "https://vas.samsungapps.com/stub/stubDownload.as"

    .line 248
    .local v3, "server_url":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "?appId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 249
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&encImei="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getIMEI()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 250
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&deviceId="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 251
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getPD()Ljava/lang/String;

    move-result-object v9

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 252
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&mcc=000"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 255
    :goto_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&mnc="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 256
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&csc="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getCSC()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 257
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&sdkVer="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 258
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&pd="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getPD()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 260
    const/4 v2, 0x0

    .line 261
    .local v2, "result":Z
    invoke-direct {p0, v3}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->checkSize(Ljava/lang/String;)Z

    move-result v2

    .line 262
    const-string v9, "StubThread"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "end StubDownload : "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    if-nez v2, :cond_0

    .line 266
    const-string v9, "DOWNLOAD_STATUS"

    const/4 v10, 0x5

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 242
    .end local v2    # "result":Z
    .end local v3    # "server_url":Ljava/lang/String;
    :cond_5
    const-string v9, "StubThread"

    const-string v10, "Connection failed"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const-string v9, "DOWNLOAD_STATUS"

    const/4 v10, 0x7

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 254
    .restart local v3    # "server_url":Ljava/lang/String;
    :cond_6
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "&mcc="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 270
    .end local v0    # "cManager":Landroid/net/ConnectivityManager;
    .end local v1    # "mobile":Landroid/net/NetworkInfo;
    .end local v3    # "server_url":Ljava/lang/String;
    .end local v4    # "szMCC":Ljava/lang/String;
    .end local v5    # "szMNC":Ljava/lang/String;
    .end local v6    # "szModel":Ljava/lang/String;
    .end local v7    # "szPrefix":Ljava/lang/String;
    .end local v8    # "wifi":Landroid/net/NetworkInfo;
    :cond_7
    const-string v9, "0"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 273
    const-string v9, "DOWNLOAD_STATUS"

    const/16 v10, 0xd

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 276
    :cond_8
    const-string v9, "1"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 279
    const-string v9, "DOWNLOAD_STATUS"

    const/4 v10, 0x4

    invoke-virtual {p0, v9, v10}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private trimCDATA(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "szSrc"    # Ljava/lang/String;

    .prologue
    .line 412
    const-string v0, "<![CDATA["

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 413
    const-string v0, "]]>"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 416
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mFlagCancel:Z

    .line 107
    const-string v0, "DOWNLOA_RATIO"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 108
    return-void
.end method

.method protected checkDownload(Ljava/lang/String;)V
    .locals 23
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 289
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v7

    .line 290
    .local v7, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v12

    .line 292
    .local v12, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v4, Ljava/net/URI;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 293
    .local v4, "Uri":Ljava/net/URI;
    new-instance v9, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v9}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 294
    .local v9, "httpclient":Lorg/apache/http/client/HttpClient;
    invoke-interface {v9}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v11

    .line 295
    .local v11, "params":Lorg/apache/http/params/HttpParams;
    const/16 v20, 0x2710

    move/from16 v0, v20

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 296
    const/16 v20, 0x7530

    move/from16 v0, v20

    invoke-static {v11, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 298
    new-instance v8, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v8, v4}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    .line 299
    .local v8, "getRequest":Lorg/apache/http/client/methods/HttpGet;
    invoke-virtual {v8, v4}, Lorg/apache/http/client/methods/HttpGet;->setURI(Ljava/net/URI;)V

    .line 300
    invoke-interface {v9, v8}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v15

    .line 302
    .local v15, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v15}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v3

    .line 304
    .local v3, "InStream":Ljava/io/InputStream;
    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-interface {v12, v3, v0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 306
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v13

    .line 308
    .local v13, "parserEvent":I
    const-string v10, ""

    .local v10, "id":Ljava/lang/String;
    const-string v14, ""

    .line 309
    .local v14, "res":Ljava/lang/String;
    const-string v2, ""

    .line 310
    .local v2, "DownloadURI":Ljava/lang/String;
    const-string v16, ""

    .line 312
    .local v16, "signature":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->interrupted()Z

    move-result v20

    if-nez v20, :cond_0

    .line 313
    :goto_0
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v13, v0, :cond_2

    .line 360
    :cond_0
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v14, v2}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->downloadApk(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->checkApkSignature()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->_Handler:Landroid/os/Handler;

    move-object/from16 v20, v0

    new-instance v21, Lcom/samsung/android/app/watchmanagerstub/StubThread$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/StubThread$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/StubThread;)V

    invoke-virtual/range {v20 .. v21}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 370
    :cond_1
    const-string v10, ""

    .line 371
    const-string v20, ""

    .line 393
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "InStream":Ljava/io/InputStream;
    .end local v4    # "Uri":Ljava/net/URI;
    .end local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v9    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v10    # "id":Ljava/lang/String;
    .end local v11    # "params":Lorg/apache/http/params/HttpParams;
    .end local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v13    # "parserEvent":I
    .end local v14    # "res":Ljava/lang/String;
    .end local v15    # "response":Lorg/apache/http/HttpResponse;
    .end local v16    # "signature":Ljava/lang/String;
    :goto_1
    return-void

    .line 315
    .restart local v2    # "DownloadURI":Ljava/lang/String;
    .restart local v3    # "InStream":Ljava/io/InputStream;
    .restart local v4    # "Uri":Ljava/net/URI;
    .restart local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v8    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .restart local v9    # "httpclient":Lorg/apache/http/client/HttpClient;
    .restart local v10    # "id":Ljava/lang/String;
    .restart local v11    # "params":Lorg/apache/http/params/HttpParams;
    .restart local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v13    # "parserEvent":I
    .restart local v14    # "res":Ljava/lang/String;
    .restart local v15    # "response":Lorg/apache/http/HttpResponse;
    .restart local v16    # "signature":Ljava/lang/String;
    :cond_2
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v13, v0, :cond_6

    .line 317
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v18

    .line 318
    .local v18, "tag":Ljava/lang/String;
    const-string v20, "appId"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 320
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 321
    .local v19, "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 323
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v10

    .line 324
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "jangil::checkDonwload::appId : "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    .end local v19    # "type":I
    :cond_3
    const-string v20, "resultCode"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 329
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 330
    .restart local v19    # "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 332
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v14

    .line 333
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "StubDownload result : "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    .end local v19    # "type":I
    :cond_4
    const-string v20, "downloadURI"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 338
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 339
    .restart local v19    # "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    .line 341
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->trimCDATA(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 342
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "downloadURI : "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    .end local v19    # "type":I
    :cond_5
    const-string v20, "signature"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 346
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v19

    .line 347
    .restart local v19    # "type":I
    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    .line 349
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v16

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "signature"

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v17

    .line 351
    .local v17, "sp":Landroid/content/SharedPreferences;
    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    .line 352
    .local v6, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v20, "signature"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 353
    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 357
    .end local v6    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v17    # "sp":Landroid/content/SharedPreferences;
    .end local v18    # "tag":Ljava/lang/String;
    .end local v19    # "type":I
    :cond_6
    invoke-interface {v12}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_4

    move-result v13

    goto/16 :goto_0

    .line 372
    .end local v2    # "DownloadURI":Ljava/lang/String;
    .end local v3    # "InStream":Ljava/io/InputStream;
    .end local v4    # "Uri":Ljava/net/URI;
    .end local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v8    # "getRequest":Lorg/apache/http/client/methods/HttpGet;
    .end local v9    # "httpclient":Lorg/apache/http/client/HttpClient;
    .end local v10    # "id":Ljava/lang/String;
    .end local v11    # "params":Lorg/apache/http/params/HttpParams;
    .end local v12    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v13    # "parserEvent":I
    .end local v14    # "res":Ljava/lang/String;
    .end local v15    # "response":Lorg/apache/http/HttpResponse;
    .end local v16    # "signature":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 373
    .local v5, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "CheckDownload_XmlPullParserException - "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    invoke-virtual {v5}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 375
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionDownload()V

    goto/16 :goto_1

    .line 376
    .end local v5    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v5

    .line 377
    .local v5, "e":Ljava/net/SocketException;
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "CheckDownload_SocketException - "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    invoke-virtual {v5}, Ljava/net/SocketException;->printStackTrace()V

    .line 379
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionDownload()V

    goto/16 :goto_1

    .line 380
    .end local v5    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v5

    .line 381
    .local v5, "e":Ljava/net/UnknownHostException;
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "CheckDownload_UnknownHostException - "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    invoke-virtual {v5}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 383
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionDownload()V

    goto/16 :goto_1

    .line 384
    .end local v5    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v5

    .line 385
    .local v5, "e":Ljava/io/IOException;
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "CheckDownload_IOException - "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 387
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionDownload()V

    goto/16 :goto_1

    .line 388
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 389
    .local v5, "e":Ljava/net/URISyntaxException;
    const-string v20, "StubThread"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "CheckDownload_URISyntaxException - "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    invoke-virtual {v5}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 391
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionDownload()V

    goto/16 :goto_1
.end method

.method protected checkUpdate(Ljava/lang/String;)V
    .locals 14
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x4

    .line 116
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v2

    .line 117
    .local v2, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 120
    .local v4, "parser":Lorg/xmlpull/v1/XmlPullParser;
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 122
    .local v0, "Url":Ljava/net/URL;
    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v4, v10, v11}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 124
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v5

    .line 126
    .local v5, "parserEvent":I
    const-string v3, ""

    .local v3, "id":Ljava/lang/String;
    const-string v6, ""

    .local v6, "result":Ljava/lang/String;
    const/4 v9, 0x0

    .line 127
    .local v9, "version":Ljava/lang/String;
    :goto_0
    const/4 v10, 0x1

    if-ne v5, v10, :cond_0

    .line 202
    .end local v0    # "Url":Ljava/net/URL;
    .end local v2    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v5    # "parserEvent":I
    .end local v6    # "result":Ljava/lang/String;
    .end local v9    # "version":Ljava/lang/String;
    :goto_1
    return-void

    .line 129
    .restart local v0    # "Url":Ljava/net/URL;
    .restart local v2    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v3    # "id":Ljava/lang/String;
    .restart local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v5    # "parserEvent":I
    .restart local v6    # "result":Ljava/lang/String;
    .restart local v9    # "version":Ljava/lang/String;
    :cond_0
    const/4 v10, 0x2

    if-ne v5, v10, :cond_3

    .line 131
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    .line 132
    .local v7, "tag":Ljava/lang/String;
    const-string v10, "appId"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 134
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 135
    .local v8, "type":I
    if-ne v8, v13, :cond_1

    .line 137
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v3

    .line 138
    const-string v10, "StubThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "jangil::checkUpdate::appId : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    .end local v8    # "type":I
    :cond_1
    const-string v10, "resultCode"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 143
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 144
    .restart local v8    # "type":I
    if-ne v8, v13, :cond_2

    .line 146
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v6

    .line 147
    const-string v10, "StubThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "StubUpdateCheck result : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    .end local v8    # "type":I
    :cond_2
    const-string v10, "versionCode"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 151
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v8

    .line 152
    .restart local v8    # "type":I
    if-ne v8, v13, :cond_3

    .line 154
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    .line 155
    const-string v10, "StubThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "version : "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget-object v10, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mPackageVersion:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    if-ge v10, v11, :cond_5

    .line 157
    const-string v10, "DOWNLOAD_STATUS"

    const/16 v11, 0xe

    invoke-virtual {p0, v10, v11}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 164
    .end local v7    # "tag":Ljava/lang/String;
    .end local v8    # "type":I
    :cond_3
    :goto_2
    const/4 v10, 0x3

    if-ne v5, v10, :cond_4

    .line 166
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    .line 167
    .restart local v7    # "tag":Ljava/lang/String;
    const-string v10, "resultCode"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 175
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendDialogOff()V

    .line 177
    invoke-direct {p0, v3, v6}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->runDownload(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const-string v3, ""

    .line 180
    const-string v6, ""

    .line 183
    .end local v7    # "tag":Ljava/lang/String;
    :cond_4
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    goto/16 :goto_0

    .line 159
    .restart local v7    # "tag":Ljava/lang/String;
    .restart local v8    # "type":I
    :cond_5
    const-string v10, "DOWNLOAD_STATUS"

    const/16 v11, 0xd

    invoke-virtual {p0, v10, v11}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_2

    .line 185
    .end local v0    # "Url":Ljava/net/URL;
    .end local v2    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v3    # "id":Ljava/lang/String;
    .end local v4    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v5    # "parserEvent":I
    .end local v6    # "result":Ljava/lang/String;
    .end local v7    # "tag":Ljava/lang/String;
    .end local v8    # "type":I
    .end local v9    # "version":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 186
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v10, "StubThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "CheckUpdate_XmlPullParserException - "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 188
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1

    .line 189
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_1
    move-exception v1

    .line 190
    .local v1, "e":Ljava/net/SocketException;
    const-string v10, "StubThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "CheckUpdate_SocketException - "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-virtual {v1}, Ljava/net/SocketException;->printStackTrace()V

    .line 192
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1

    .line 193
    .end local v1    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v1

    .line 194
    .local v1, "e":Ljava/net/UnknownHostException;
    const-string v10, "StubThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "CheckUpdate_UnknownHostException - "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 196
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1

    .line 197
    .end local v1    # "e":Ljava/net/UnknownHostException;
    :catch_3
    move-exception v1

    .line 198
    .local v1, "e":Ljava/io/IOException;
    const-string v10, "StubThread"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "CheckUpdate_IOException - "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 200
    invoke-virtual {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessageExceptionUpdate()V

    goto/16 :goto_1
.end method

.method protected getCSC()Ljava/lang/String;
    .locals 4

    .prologue
    .line 824
    const-string v0, ""

    .line 825
    .local v0, "cscVersion":Ljava/lang/String;
    const/4 v1, 0x0

    .line 828
    .local v1, "value":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->isCSCExistFile()Z

    move-result v2

    if-nez v2, :cond_0

    .line 850
    :goto_0
    return-object v0

    .line 833
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->getCSCVersion()Ljava/lang/String;

    move-result-object v1

    .line 834
    if-nez v1, :cond_1

    .line 836
    const-string v2, "StubThread"

    const-string v3, "getCSC::getCSCVersion::value is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 840
    :cond_1
    const-string v2, "FAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 842
    const-string v2, "StubThread"

    const-string v3, "getCSC::getCSCVersion::Fail to read CSC Version"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 846
    :cond_2
    const/4 v2, 0x0

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getIMEI()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 918
    const-string v2, ""

    .line 919
    .local v2, "imei":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    const-string v6, "phone"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 920
    .local v4, "telMgr":Landroid/telephony/TelephonyManager;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 922
    .local v3, "md5":Ljava/lang/StringBuffer;
    if-eqz v4, :cond_0

    .line 927
    const/16 v5, 0xc

    :try_start_0
    new-array v0, v5, [B

    const/4 v5, 0x0

    const/4 v6, 0x1

    aput-byte v6, v0, v5

    const/4 v5, 0x1

    const/4 v6, 0x2

    aput-byte v6, v0, v5

    const/4 v5, 0x2

    const/4 v6, 0x3

    aput-byte v6, v0, v5

    const/4 v5, 0x3

    const/4 v6, 0x4

    aput-byte v6, v0, v5

    const/4 v5, 0x4

    const/4 v6, 0x5

    aput-byte v6, v0, v5

    const/4 v5, 0x5

    const/4 v6, 0x6

    aput-byte v6, v0, v5

    const/4 v5, 0x6

    const/4 v6, 0x7

    aput-byte v6, v0, v5

    const/4 v5, 0x7

    const/16 v6, 0x8

    aput-byte v6, v0, v5

    const/16 v5, 0x8

    const/16 v6, 0x9

    aput-byte v6, v0, v5

    const/16 v5, 0xa

    const/4 v6, 0x1

    aput-byte v6, v0, v5

    const/16 v5, 0xb

    const/4 v6, 0x2

    aput-byte v6, v0, v5

    .line 929
    .local v0, "digest":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-lt v1, v5, :cond_1

    .line 938
    .end local v0    # "digest":[B
    .end local v1    # "i":I
    :cond_0
    :goto_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-static {v5, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 939
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 931
    .restart local v0    # "digest":[B
    .restart local v1    # "i":I
    :cond_1
    :try_start_1
    aget-byte v5, v0, v1

    and-int/lit16 v5, v5, 0xf0

    shr-int/lit8 v5, v5, 0x4

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 932
    aget-byte v5, v0, v1

    and-int/lit8 v5, v5, 0xf

    shr-int/lit8 v5, v5, 0x0

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 929
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 935
    .end local v0    # "digest":[B
    .end local v1    # "i":I
    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method protected getMCC()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 772
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 773
    const-string v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 772
    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 774
    .local v3, "telMgr":Landroid/telephony/TelephonyManager;
    const-string v0, "000"

    .line 776
    .local v0, "mcc":Ljava/lang/String;
    if-nez v3, :cond_0

    move-object v1, v0

    .line 801
    .end local v0    # "mcc":Ljava/lang/String;
    .local v1, "mcc":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 780
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :cond_0
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 782
    .local v2, "networkOperator":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 792
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    .line 793
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 794
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 801
    .end local v0    # "mcc":Ljava/lang/String;
    .restart local v1    # "mcc":Ljava/lang/String;
    goto :goto_0

    .line 784
    .end local v1    # "mcc":Ljava/lang/String;
    .restart local v0    # "mcc":Ljava/lang/String;
    :pswitch_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    .line 785
    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 786
    goto :goto_1

    .line 788
    :cond_1
    const-string v0, ""

    .line 789
    goto :goto_1

    .line 796
    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 782
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected getMNC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 805
    const-string v0, "00"

    .line 807
    .local v0, "mnc":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    .line 809
    .local v2, "telMgr":Landroid/telephony/TelephonyManager;
    if-nez v2, :cond_1

    .line 818
    .end local v0    # "mnc":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    .line 814
    .restart local v0    # "mnc":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v1

    .line 815
    .local v1, "networkOperator":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 816
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getPD()Ljava/lang/String;
    .locals 4

    .prologue
    .line 944
    const-string v2, "0"

    .line 945
    .local v2, "rtn_str":Ljava/lang/String;
    const/4 v1, 0x0

    .line 947
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    const-string v3, "/sdcard/go_to_andromeda.test"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 951
    .local v0, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    .line 952
    if-eqz v1, :cond_0

    .line 954
    const-string v2, "1"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 960
    :cond_0
    :goto_0
    return-object v2

    .line 957
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method protected sendDialogOff()V
    .locals 4

    .prologue
    .line 731
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 732
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    const-string v3, "CHECK_SERVER_PROGRESSDIALOG"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 734
    .local v1, "message":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 735
    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 736
    return-void
.end method

.method protected sendMessage(Ljava/lang/String;I)V
    .locals 3
    .param p1, "cmd"    # Ljava/lang/String;
    .param p2, "arg"    # I

    .prologue
    .line 700
    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 701
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 702
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "MESSAGE_CMD"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 706
    const-string v2, "DOWNLOAD_STATUS"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 716
    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 717
    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/StubThread;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 718
    return-void

    .line 708
    :cond_1
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 710
    const-string v2, "DOWNLOA_RATIO"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 712
    :cond_2
    const-string v2, "UPDATE_DOWNLOAD_EXCEPTION"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 714
    const-string v2, "UPDATE_DOWNLOAD_EXCEPTION"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected sendMessageExceptionDownload()V
    .locals 2

    .prologue
    .line 727
    const-string v0, "UPDATE_DOWNLOAD_EXCEPTION"

    const/16 v1, 0xc

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 728
    return-void
.end method

.method protected sendMessageExceptionUpdate()V
    .locals 2

    .prologue
    .line 722
    const-string v0, "UPDATE_DOWNLOAD_EXCEPTION"

    const/16 v1, 0xb

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/watchmanagerstub/StubThread;->sendMessage(Ljava/lang/String;I)V

    .line 723
    return-void
.end method
