.class public Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GMHFPReceiver.java"


# static fields
.field private static final CONNECTED_WATCH_BT_MAC:Ljava/lang/String; = "connected_watch_bt_mac"

.field public static final HFP_ACTION:Ljava/lang/String; = "android.bluetooth.device.extra.SET_BSSF"

.field private static final TAG:Ljava/lang/String; = "GMFPReceiver"

.field private static mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;


# instance fields
.field private WearableDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 36
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->map:Ljava/util/HashMap;

    .line 234
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver$1;-><init>(Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;)V

    iput-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 29
    return-void
.end method

.method static synthetic access$0(Landroid/bluetooth/BluetoothHeadset;)V
    .locals 0

    .prologue
    .line 35
    sput-object p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-void
.end method

.method static synthetic access$1()Landroid/bluetooth/BluetoothHeadset;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    return-object v0
.end method

.method private getBTStatus(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 306
    const-string v1, "GMFPReceiver"

    const-string v2, "jangil::getBTStatus()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    const-string v1, "GMFPReceiver"

    const-string v2, "+++++++++++++++++++++++++++++++++++++++++++++"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :try_start_0
    const-string v2, "GMFPReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "::::::::"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "::"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const-string v2, "GMFPReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "::::::::"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "::"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :goto_0
    const-string v1, "GMFPReceiver"

    const-string v2, "+++++++++++++++++++++++++++++++++++++++++++++"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    return-void

    .line 312
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private initMapStatus()V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->map:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 256
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 258
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    const-string v1, "Wearable0001"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    iget-object v0, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    const-string v1, "Wearable0002"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 260
    return-void
.end method

.method private printBTStatus(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 319
    const-string v1, "GMFPReceiver"

    const-string v2, "jangil::printBTStatus()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 331
    return-void

    .line 321
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->map:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    .line 322
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 323
    const-string v2, "GMFPReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "::::::::"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "::false"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 326
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->map:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_1

    .line 327
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v2, v1, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 328
    const-string v2, "GMFPReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "::::::::"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->WearableDevices:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "::true"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setProperties(Ljava/lang/String;Landroid/content/Context;)V
    .locals 8
    .param p1, "mAddress"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    .line 262
    const-string v3, "GMFPReceiver"

    const-string v4, "jangil::setProperties()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    invoke-direct {p0}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->initMapStatus()V

    .line 264
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    .line 265
    .local v1, "mBluetoothAdapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v1, :cond_9

    .line 266
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 267
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    .line 269
    .local v2, "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-nez v2, :cond_0

    .line 304
    .end local v2    # "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return-void

    .line 271
    .restart local v2    # "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 302
    .end local v2    # "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_2
    :goto_2
    invoke-direct {p0, p2}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->printBTStatus(Landroid/content/Context;)V

    goto :goto_0

    .line 271
    .restart local v2    # "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 272
    .local v0, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v4

    const/16 v5, 0xc

    if-ne v4, v5, :cond_1

    .line 274
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 275
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Gear 2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Gear 3"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Gear S"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 276
    :cond_4
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->map:Ljava/util/HashMap;

    const-string v5, "Wearable0001"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    :cond_5
    :goto_3
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 292
    const-string v4, "GMFPReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onReceive() state BOND_BONDED - mAddress is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 277
    :cond_6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Gear Fit"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 278
    iget-object v4, p0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->map:Ljava/util/HashMap;

    const-string v5, "Wearable0002"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 279
    :cond_7
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Circle"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 280
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 281
    sget-object v4, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v4, :cond_5

    .line 282
    const-string v4, "GMFPReceiver"

    const-string v5, "jangil::connect to necklet.."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    sget-object v4, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v4, v0}, Landroid/bluetooth/BluetoothHeadset;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    goto :goto_3

    .line 286
    :cond_8
    const-string v4, "GMFPReceiver"

    const-string v5, "skip to connect to necklet.."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 298
    .end local v0    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v2    # "pairedDevices":Ljava/util/Set;, "Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    :cond_9
    const-string v3, "GMFPReceiver"

    const-string v4, "onItemClick() type is not device"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    if-nez p2, :cond_1

    .line 47
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::intent is null.."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    if-nez v19, :cond_2

    .line 50
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::intent.getAction() is null.."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    .line 54
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mProfileListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v19 .. v22}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 55
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    const-string v20, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 85
    const-string v19, "com.samsung.android.app.watchmanager"

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_4

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getWatchManagerVersionCode(Landroid/content/Context;)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 86
    :cond_4
    const-string v19, "android.bluetooth.profile.extra.STATE"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    .line 88
    .local v18, "state_extra":I
    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 89
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    .line 90
    .local v3, "BTAdapter":Landroid/bluetooth/BluetoothAdapter;
    const-string v19, "android.bluetooth.device.extra.DEVICE"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    check-cast v16, Landroid/bluetooth/BluetoothDevice;

    .line 92
    .local v16, "remoteDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v19

    if-eqz v19, :cond_5

    .line 93
    invoke-virtual/range {v16 .. v16}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v19

    const/16 v20, 0x704

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 94
    invoke-virtual/range {v16 .. v16}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    .line 95
    .local v15, "remoteBtMac":Ljava/lang/String;
    const-string v19, "GMFPReceiver"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "current connected remote-watch bt mac : "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const-string v19, "gearmanager_stub"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 98
    .local v14, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 99
    .local v11, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v19, "connected_watch_bt_mac"

    move-object/from16 v0, v19

    invoke-interface {v11, v0, v15}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 100
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 103
    .end local v11    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v14    # "pref":Landroid/content/SharedPreferences;
    .end local v15    # "remoteBtMac":Ljava/lang/String;
    :cond_5
    const-string v19, "GMFPReceiver"

    const-string v20, "BTAdapter is null or BTAdapter is not enable."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 108
    .end local v3    # "BTAdapter":Landroid/bluetooth/BluetoothAdapter;
    .end local v16    # "remoteDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v18    # "state_extra":I
    :cond_6
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    const-string v20, "android.bluetooth.device.extra.SET_BSSF"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 109
    const-string v19, "GMFPReceiver"

    const-string v20, "android.bluetooth.device.extra.SET_BSSF"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-string v19, "com.samsung.android.app.watchmanager"

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_7

    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getWatchManagerVersionCode(Landroid/content/Context;)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 121
    :cond_7
    new-instance v12, Landroid/content/Intent;

    const-class v19, Lcom/samsung/android/app/watchmanagerstub/MainActivity;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 122
    .local v12, "i":Landroid/content/Intent;
    const/high16 v19, 0x10000000

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 123
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 124
    const-string v19, "GMFPReceiver"

    const-string v20, "start download UI"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 126
    .end local v12    # "i":Landroid/content/Intent;
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    const-string v20, "com.samsung.android.sconnect.action.CONNECT_WEARABLE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 127
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::onReceive::com.samsung.android.sconnect.action.CONNECT_WEARABLE"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const-string v19, "connect_wearable"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 130
    .restart local v14    # "pref":Landroid/content/SharedPreferences;
    const-string v19, "MAC"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, "btAddress":Ljava/lang/String;
    const-string v19, "WM_MANAGER"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 133
    .local v13, "postName":Ljava/lang/String;
    const-string v19, "GMFPReceiver"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "jangil::MAC::"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v21, "MAC"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "::postName::"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    if-eqz v4, :cond_0

    if-eqz v13, :cond_0

    .line 137
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 138
    .restart local v11    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v19, "MAC"

    move-object/from16 v0, v19

    invoke-interface {v11, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 139
    const-string v19, "WM_MANAGER"

    move-object/from16 v0, v19

    invoke-interface {v11, v0, v13}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 140
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 143
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v13, v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->launchApp(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 147
    .end local v4    # "btAddress":Ljava/lang/String;
    .end local v11    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v13    # "postName":Ljava/lang/String;
    .end local v14    # "pref":Landroid/content/SharedPreferences;
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    const-string v20, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 148
    const-string v19, "GMFPReceiver"

    const-string v20, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const-string v19, "android.bluetooth.device.extra.BOND_STATE"

    const/16 v20, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 152
    .local v17, "state":I
    const-string v19, "android.bluetooth.device.extra.DEVICE"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    .line 153
    .local v8, "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v8, :cond_0

    .line 156
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 159
    .restart local v4    # "btAddress":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v4, v1}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->setProperties(Ljava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 160
    .end local v4    # "btAddress":Ljava/lang/String;
    .end local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v17    # "state":I
    :cond_a
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    const-string v20, "com.samsung.android.wmanger.action.CONNECT_WEARABLE"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 161
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::onReceive::com.samsung.android.wmanger.action.CONNECT_WEARABLE"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    const-string v19, "connect_wearable"

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v14

    .line 164
    .restart local v14    # "pref":Landroid/content/SharedPreferences;
    const-string v19, "MAC"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 165
    .restart local v4    # "btAddress":Ljava/lang/String;
    const-string v19, "DATA"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v7

    .line 167
    .local v7, "data":[B
    invoke-static {v7}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getSConnectDataFromBLE([B)Ljava/lang/String;

    move-result-object v13

    .line 169
    .restart local v13    # "postName":Ljava/lang/String;
    if-nez v13, :cond_b

    .line 170
    const-string v19, "WM_MANAGER"

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 173
    :cond_b
    const-string v19, "GMFPReceiver"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "jangil::MAC::"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v21, "MAC"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "::postName::"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    if-eqz v4, :cond_c

    if-nez v13, :cond_d

    .line 175
    :cond_c
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::btAddress or postName is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 179
    :cond_d
    invoke-interface {v14}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 180
    .restart local v11    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v19, "MAC"

    move-object/from16 v0, v19

    invoke-interface {v11, v0, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 181
    const-string v19, "WM_MANAGER"

    move-object/from16 v0, v19

    invoke-interface {v11, v0, v13}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 182
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 184
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->isTablet(Landroid/content/Context;)Z

    move-result v19

    if-eqz v19, :cond_13

    .line 185
    invoke-static {v4}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getBTName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 187
    .local v5, "btName":Ljava/lang/String;
    const-string v19, "Gear 3"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-nez v19, :cond_e

    const-string v19, "Gear S"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_10

    .line 188
    :cond_e
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::this is tablet & gear3.."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    new-instance v6, Landroid/app/AlertDialog$Builder;

    const-string v19, "white"

    invoke-static {}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->getDeviceTheme()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_f

    const/16 v19, 0x5

    :goto_1
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-direct {v6, v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 191
    .local v6, "builder":Landroid/app/AlertDialog$Builder;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 192
    const v19, 0x7f090022

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 193
    const v19, 0x7f090023

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 194
    const v19, 0x104000a

    new-instance v20, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver$2;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver$2;-><init>(Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 199
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    .line 200
    .local v9, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v9}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v19

    const/16 v20, 0x7d3

    invoke-virtual/range {v19 .. v20}, Landroid/view/Window;->setType(I)V

    .line 201
    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 190
    .end local v6    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v9    # "dialog":Landroid/app/AlertDialog;
    :cond_f
    const/16 v19, 0x4

    goto :goto_1

    .line 202
    :cond_10
    const-string v19, "Gear Circle"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_12

    .line 203
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::this is tablet & Necklet.."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBtAdapter:Landroid/bluetooth/BluetoothAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v8

    .line 209
    .restart local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    if-eqz v8, :cond_0

    .line 212
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v19

    const/16 v20, 0xa

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_11

    .line 213
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/bluetooth/BluetoothDevice;->setPairingConfirmation(Z)Z

    .line 214
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->createBond()Z

    .line 215
    const-string v19, "GMFPReceiver"

    const-string v20, "getBondState() == BOND_NONE->createBond()"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 216
    :cond_11
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v19

    const/16 v20, 0xc

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 218
    :try_start_0
    sget-object v19, Lcom/samsung/android/app/watchmanagerstub/GMHFPReceiver;->mBluetoothHeadset:Landroid/bluetooth/BluetoothHeadset;

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Landroid/bluetooth/BluetoothHeadset;->connect(Landroid/bluetooth/BluetoothDevice;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 219
    :catch_0
    move-exception v10

    .line 221
    .local v10, "e":Ljava/lang/Exception;
    const-string v19, "GMFPReceiver"

    const-string v20, "jangil::mBluetoothHeadset is null"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 226
    .end local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_12
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v13, v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->launchApp(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 229
    .end local v5    # "btName":Ljava/lang/String;
    :cond_13
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v13, v1}, Lcom/samsung/android/app/watchmanagerstub/Utilities;->launchApp(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
