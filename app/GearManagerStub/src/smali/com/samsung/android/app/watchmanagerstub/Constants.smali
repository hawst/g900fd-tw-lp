.class public Lcom/samsung/android/app/watchmanagerstub/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final GEAR1PLUGIN_PKG_NAME:Ljava/lang/String; = "com.samsung.android.gear1plugin"

.field public static final GEAR2PLUGIN_PKG_NAME:Ljava/lang/String; = "com.samsung.android.gear2plugin"

.field public static final PACKAGE_NAME_UHM:Ljava/lang/String; = "com.samsung.android.app.watchmanager"

.field public static final PREPACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.app."

.field public static final WATCHMANAGER:Ljava/lang/String; = "watchmanager"

.field public static final WATCHMANAGERSTUB_PKG_NAME:Ljava/lang/String; = "com.samsung.android.app.watchmanagerstub"

.field public static final WATCHMANAGER_ACTIVITY:Ljava/lang/String; = "com.samsung.android.app.watchmanager.setupwizard.SetupWizardWelcomeActivity"

.field public static final WATCHMANAGER_PKG_NAME:Ljava/lang/String; = "com.samsung.android.app.watchmanager"

.field public static final WMS:Ljava/lang/String; = "wms"

.field public static final WMS_PKG_NAME:Ljava/lang/String; = "com.samsung.android.wms"

.field public static final WM_MANAGER:Ljava/lang/String; = "WM_MANAGER"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
