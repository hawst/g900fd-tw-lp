.class final enum Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;
.super Ljava/lang/Enum;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/watchmanagerstub/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VIEW_STAUS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DOWNLOADING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

.field private static final synthetic ENUM$VALUES:[Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

.field public static final enum IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

.field public static final enum INSTALLING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 105
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    .line 106
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->DOWNLOADING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    .line 107
    new-instance v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    const-string v1, "INSTALLING"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->INSTALLING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    .line 105
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    sget-object v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->IDLE:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->DOWNLOADING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->INSTALLING:Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->ENUM$VALUES:[Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;->ENUM$VALUES:[Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/android/app/watchmanagerstub/MainActivity$VIEW_STAUS;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
