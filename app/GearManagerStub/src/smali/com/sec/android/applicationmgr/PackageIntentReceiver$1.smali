.class Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;
.super Ljava/lang/Object;
.source "PackageIntentReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/applicationmgr/PackageIntentReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/applicationmgr/PackageIntentReceiver;

.field private final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/applicationmgr/PackageIntentReceiver;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;->this$0:Lcom/sec/android/applicationmgr/PackageIntentReceiver;

    iput-object p2, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;->val$context:Landroid/content/Context;

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 62
    iget-object v2, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;->val$context:Landroid/content/Context;

    const-string v3, "gmremoving"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 63
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 64
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "finished"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 65
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 66
    iget-object v2, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "UHMVersion"

    const/4 v4, -0x2

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 67
    iget-object v2, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;->this$0:Lcom/sec/android/applicationmgr/PackageIntentReceiver;

    # invokes: Lcom/sec/android/applicationmgr/PackageIntentReceiver;->deleteAllPackages()V
    invoke-static {v2}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->access$0(Lcom/sec/android/applicationmgr/PackageIntentReceiver;)V

    .line 68
    iget-object v2, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;->this$0:Lcom/sec/android/applicationmgr/PackageIntentReceiver;

    # getter for: Lcom/sec/android/applicationmgr/PackageIntentReceiver;->BACKUP_DIRECTORY:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->access$1()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/applicationmgr/PackageIntentReceiver;->deleteDir(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->access$2(Lcom/sec/android/applicationmgr/PackageIntentReceiver;Ljava/lang/String;)V

    .line 69
    const-string v2, "finished"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 70
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 71
    return-void
.end method
