.class public Lcom/sec/android/applicationmgr/PackageIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PackageIntentReceiver.java"


# static fields
.field private static final BACKUP_DIRECTORY:Ljava/lang/String;

.field private static final CONNECTED_WATCH_BT_MAC:Ljava/lang/String; = "connected_watch_bt_mac"

.field private static final TAG:Ljava/lang/String; = "PackageIntentReceiver"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/.BManager_backup"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->BACKUP_DIRECTORY:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/applicationmgr/PackageIntentReceiver;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->deleteAllPackages()V

    return-void
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->BACKUP_DIRECTORY:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/applicationmgr/PackageIntentReceiver;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->deleteDir(Ljava/lang/String;)V

    return-void
.end method

.method private deleteAllPackages()V
    .locals 6

    .prologue
    .line 173
    const-string v3, "PackageIntentReceiver"

    const-string v4, "deleteAllPackages"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v3, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 178
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v1

    .line 180
    .local v1, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 186
    return-void

    .line 180
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 181
    .local v0, "packageInfo":Landroid/content/pm/ApplicationInfo;
    const-string v4, "com.samsung.android.app.watchmanager"

    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 182
    iget-object v4, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->deletePackage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private deleteDir(Ljava/lang/String;)V
    .locals 10
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 230
    const-string v5, "PackageIntentReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "deleteDir path = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 234
    .local v3, "file":Ljava/io/File;
    if-eqz v3, :cond_1

    .line 235
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 236
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 237
    .local v2, "childFileList":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 238
    const-string v5, "PackageIntentReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "deleteDir childFileList len = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 239
    array-length v7, v2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 238
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    array-length v6, v2

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_2

    .line 251
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    .line 252
    .local v4, "fileDeleteCheck":Z
    const-string v5, "PackageIntentReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fileDeleteCheck = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    .end local v2    # "childFileList":[Ljava/io/File;
    .end local v4    # "fileDeleteCheck":Z
    :cond_1
    :goto_1
    return-void

    .line 240
    .restart local v2    # "childFileList":[Ljava/io/File;
    :cond_2
    aget-object v0, v2, v5

    .line 241
    .local v0, "childFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 242
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->deleteDir(Ljava/lang/String;)V

    .line 240
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 244
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    .line 245
    .local v1, "childFileDeleteCheck":Z
    const-string v7, "PackageIntentReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "childFileDeleteCheck = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 246
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 245
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 254
    .end local v0    # "childFile":Ljava/io/File;
    .end local v1    # "childFileDeleteCheck":Z
    .end local v2    # "childFileList":[Ljava/io/File;
    :cond_4
    const-string v5, "PackageIntentReceiver"

    const-string v6, "deleteDir path not exist!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private deletePackage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 192
    :try_start_0
    new-instance v1, Lcom/sec/android/applicationmgr/ApplicationManager;

    iget-object v2, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/applicationmgr/ApplicationManager;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    .line 194
    .local v1, "packageController":Lcom/sec/android/applicationmgr/ApplicationManager;
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/sec/android/applicationmgr/ApplicationManager;->uninstallPackage(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3

    .line 208
    .end local v1    # "packageController":Lcom/sec/android/applicationmgr/ApplicationManager;
    :goto_0
    return-void

    .line 195
    .restart local v1    # "packageController":Lcom/sec/android/applicationmgr/ApplicationManager;
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 202
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v1    # "packageController":Lcom/sec/android/applicationmgr/ApplicationManager;
    :catch_1
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 197
    .end local v0    # "e":Ljava/lang/SecurityException;
    .restart local v1    # "packageController":Lcom/sec/android/applicationmgr/ApplicationManager;
    :catch_2
    move-exception v0

    .line 198
    .local v0, "e":Ljava/lang/IllegalAccessException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    .line 204
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v1    # "packageController":Lcom/sec/android/applicationmgr/ApplicationManager;
    :catch_3
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    .line 199
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v1    # "packageController":Lcom/sec/android/applicationmgr/ApplicationManager;
    :catch_4
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_0
.end method


# virtual methods
.method public isPackageExists(Ljava/lang/String;)Z
    .locals 8
    .param p1, "targetPackage"    # Ljava/lang/String;

    .prologue
    .line 211
    const-string v4, "PackageIntentReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "isPackageExists "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget-object v4, p0, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 214
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/16 v4, 0x80

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v2

    .line 216
    .local v2, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 226
    const/4 v4, 0x0

    :goto_1
    return v4

    .line 216
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 218
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    :try_start_0
    iget-object v5, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    .line 219
    const/4 v4, 0x1

    goto :goto_1

    .line 221
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "PackageIntentReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "jangil::this package is not installed::"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 27
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 47
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->mContext:Landroid/content/Context;

    .line 48
    new-instance v23, Landroid/os/Handler;

    invoke-direct/range {v23 .. v23}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->mHandler:Landroid/os/Handler;

    .line 50
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    .line 52
    .local v8, "action":Ljava/lang/String;
    const-string v23, "android.intent.action.PACKAGE_REMOVED"

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 54
    const-string v23, "android.intent.extra.REPLACING"

    const/16 v24, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v22

    .line 55
    .local v22, "upgrade":Z
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v15

    .line 57
    .local v15, "packageName":Ljava/lang/String;
    const-string v23, "com.samsung.android.app.watchmanager"

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    if-nez v22, :cond_0

    .line 58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    new-instance v24, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/applicationmgr/PackageIntentReceiver$1;-><init>(Lcom/sec/android/applicationmgr/PackageIntentReceiver;Landroid/content/Context;)V

    .line 73
    const-wide/16 v25, 0xc8

    .line 58
    invoke-virtual/range {v23 .. v26}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 166
    .end local v15    # "packageName":Ljava/lang/String;
    .end local v22    # "upgrade":Z
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    const-string v23, "android.intent.action.PACKAGE_ADDED"

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 78
    const-string v23, "PackageIntentReceiver"

    const-string v24, "ACTION_PACKAGE_ADDED"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v15

    .line 82
    .restart local v15    # "packageName":Ljava/lang/String;
    const-string v23, "com.samsung.android.app.watchmanager"

    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_2

    .line 83
    const-string v23, "PackageIntentReceiver"

    const-string v24, "Not GearManger package! "

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 88
    :cond_2
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 89
    .local v17, "pm":Landroid/content/pm/PackageManager;
    const-string v23, "com.samsung.android.app.watchmanager"

    const/16 v24, 0x40

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 90
    .local v4, "GMPackageInfo":Landroid/content/pm/PackageInfo;
    const-string v23, "com.samsung.android.app.watchmanagerstub"

    const/16 v24, 0x40

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 92
    .local v5, "GMSPackageInfo":Landroid/content/pm/PackageInfo;
    iget-object v7, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 93
    .local v7, "GMSignatures":[Landroid/content/pm/Signature;
    iget-object v6, v5, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 95
    .local v6, "GMSSignatures":[Landroid/content/pm/Signature;
    const-string v23, "PackageIntentReceiver"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "GM signature length: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v7

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    .line 96
    const-string v25, "GMS signature length: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    array-length v0, v6

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 95
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string v23, "PackageIntentReceiver"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "GMSignatures: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v25, 0x0

    aget-object v25, v7, v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " / "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "GMSSignatures: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const/16 v25, 0x0

    aget-object v25, v6, v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/16 v23, 0x0

    aget-object v23, v7, v23

    const/16 v24, 0x0

    aget-object v24, v6, v24

    invoke-virtual/range {v23 .. v24}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 101
    const-string v23, "PackageIntentReceiver"

    const-string v24, "GM/GMS signature verification passed! "

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 122
    .end local v4    # "GMPackageInfo":Landroid/content/pm/PackageInfo;
    .end local v5    # "GMSPackageInfo":Landroid/content/pm/PackageInfo;
    .end local v6    # "GMSSignatures":[Landroid/content/pm/Signature;
    .end local v7    # "GMSignatures":[Landroid/content/pm/Signature;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    const-string v23, "gearmanager_stub"

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v19

    .line 123
    .local v19, "pref":Landroid/content/SharedPreferences;
    const-string v23, "connected_watch_bt_mac"

    const-string v24, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 124
    .local v20, "remoteBtMac":Ljava/lang/String;
    const-string v23, "PackageIntentReceiver"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "current connected remoteBtMac : "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    if-eqz v20, :cond_0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v23

    if-lez v23, :cond_0

    .line 126
    const-string v23, "PackageIntentReceiver"

    const-string v24, "this is NFC or BSSF"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    new-instance v9, Landroid/content/ComponentName;

    const-string v23, "com.samsung.android.app.watchmanager"

    const-string v24, "com.samsung.android.app.watchmanager.setupwizard.SetupWizardWelcomeActivity"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v9, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    .local v9, "compName":Landroid/content/ComponentName;
    new-instance v14, Landroid/content/Intent;

    const-string v23, "android.intent.action.MAIN"

    move-object/from16 v0, v23

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 129
    .local v14, "intentBT":Landroid/content/Intent;
    const-string v23, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    invoke-virtual {v14, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 131
    const-string v23, "bt_addr"

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v14, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 132
    const/high16 v23, 0x10000000

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 133
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 135
    invoke-interface/range {v19 .. v19}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v12

    .line 136
    .local v12, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v23, "connected_watch_bt_mac"

    const-string v24, ""

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-interface {v12, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 137
    invoke-interface {v12}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0

    .line 103
    .end local v9    # "compName":Landroid/content/ComponentName;
    .end local v12    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v14    # "intentBT":Landroid/content/Intent;
    .end local v19    # "pref":Landroid/content/SharedPreferences;
    .end local v20    # "remoteBtMac":Ljava/lang/String;
    .restart local v4    # "GMPackageInfo":Landroid/content/pm/PackageInfo;
    .restart local v5    # "GMSPackageInfo":Landroid/content/pm/PackageInfo;
    .restart local v6    # "GMSSignatures":[Landroid/content/pm/Signature;
    .restart local v7    # "GMSignatures":[Landroid/content/pm/Signature;
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    :cond_3
    :try_start_1
    const-string v23, "PackageIntentReceiver"

    const-string v24, "GM/GMS signature verification failed! "

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v18, Landroid/content/Intent;

    const-class v23, Lcom/samsung/android/app/watchmanagerstub/AlertDialogActivity;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 106
    .local v18, "popupIntent":Landroid/content/Intent;
    const/16 v23, 0x0

    const/high16 v24, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    move/from16 v1, v23

    move-object/from16 v2, v18

    move/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v16

    .line 109
    .local v16, "pie":Landroid/app/PendingIntent;
    if-eqz v16, :cond_0

    .line 110
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Landroid/app/PendingIntent;->send()V
    :try_end_2
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 111
    :catch_0
    move-exception v10

    .line 112
    .local v10, "e":Landroid/app/PendingIntent$CanceledException;
    :try_start_3
    invoke-virtual {v10}, Landroid/app/PendingIntent$CanceledException;->printStackTrace()V
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 118
    .end local v4    # "GMPackageInfo":Landroid/content/pm/PackageInfo;
    .end local v5    # "GMSPackageInfo":Landroid/content/pm/PackageInfo;
    .end local v6    # "GMSSignatures":[Landroid/content/pm/Signature;
    .end local v7    # "GMSignatures":[Landroid/content/pm/Signature;
    .end local v10    # "e":Landroid/app/PendingIntent$CanceledException;
    .end local v16    # "pie":Landroid/app/PendingIntent;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v18    # "popupIntent":Landroid/content/Intent;
    :catch_1
    move-exception v10

    .line 119
    .local v10, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v10}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1

    .line 139
    .end local v10    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v15    # "packageName":Ljava/lang/String;
    :cond_4
    const-string v23, "android.intent.action.BOOT_COMPLETED"

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 140
    const-string v23, "PackageIntentReceiver"

    const-string v24, "ACTION_BOOT_COMPLETED"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    const-string v23, "gmremoving"

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v21

    .line 142
    .local v21, "sp":Landroid/content/SharedPreferences;
    const-string v23, "finished"

    const/16 v24, 0x1

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    move/from16 v2, v24

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    .line 143
    .local v13, "flag":Z
    if-eqz v13, :cond_5

    .line 144
    const-string v23, "PackageIntentReceiver"

    const-string v24, "jangil::ACTION_BOOT_COMPLETED::do not need pkg remove.."

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 148
    :cond_5
    const-string v23, "com.samsung.android.app.watchmanager"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->isPackageExists(Ljava/lang/String;)Z

    move-result v23

    if-nez v23, :cond_6

    .line 149
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/applicationmgr/PackageIntentReceiver;->mHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    new-instance v24, Lcom/sec/android/applicationmgr/PackageIntentReceiver$2;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/applicationmgr/PackageIntentReceiver$2;-><init>(Lcom/sec/android/applicationmgr/PackageIntentReceiver;Landroid/content/SharedPreferences;)V

    .line 159
    const-wide/16 v25, 0x7d0

    .line 149
    invoke-virtual/range {v23 .. v26}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 161
    :cond_6
    invoke-interface/range {v21 .. v21}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    .line 162
    .local v11, "edit":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 163
    invoke-interface {v11}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method
