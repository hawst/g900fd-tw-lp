.class Lcom/sec/android/applicationmgr/ApplicationManager$PackageDeleteObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "ApplicationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/applicationmgr/ApplicationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PackageDeleteObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/applicationmgr/ApplicationManager;


# direct methods
.method constructor <init>(Lcom/sec/android/applicationmgr/ApplicationManager;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/android/applicationmgr/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/applicationmgr/ApplicationManager;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 340
    iget-object v1, p0, Lcom/sec/android/applicationmgr/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/applicationmgr/ApplicationManager;

    # getter for: Lcom/sec/android/applicationmgr/ApplicationManager;->onPackageDeleted:Lcom/sec/android/applicationmgr/OnPackageDeleted;
    invoke-static {v1}, Lcom/sec/android/applicationmgr/ApplicationManager;->access$1(Lcom/sec/android/applicationmgr/ApplicationManager;)Lcom/sec/android/applicationmgr/OnPackageDeleted;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 347
    iget-object v1, p0, Lcom/sec/android/applicationmgr/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/applicationmgr/ApplicationManager;

    iput-object p1, v1, Lcom/sec/android/applicationmgr/ApplicationManager;->pkgname:Ljava/lang/String;

    .line 348
    iget-object v1, p0, Lcom/sec/android/applicationmgr/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/applicationmgr/ApplicationManager;

    iput p2, v1, Lcom/sec/android/applicationmgr/ApplicationManager;->returncode:I

    .line 349
    iget-object v1, p0, Lcom/sec/android/applicationmgr/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/applicationmgr/ApplicationManager;

    # getter for: Lcom/sec/android/applicationmgr/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/applicationmgr/ApplicationManager;->access$3(Lcom/sec/android/applicationmgr/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 350
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/applicationmgr/ApplicationManager$PackageDeleteObserver;->this$0:Lcom/sec/android/applicationmgr/ApplicationManager;

    # getter for: Lcom/sec/android/applicationmgr/ApplicationManager;->deleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/applicationmgr/ApplicationManager;->access$3(Lcom/sec/android/applicationmgr/ApplicationManager;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 352
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method
