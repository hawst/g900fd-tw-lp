.class public Lcom/sec/android/applicationmgr/SigChecker;
.super Ljava/lang/Object;
.source "SigChecker.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/applicationmgr/SigChecker;->TAG:Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lcom/sec/android/applicationmgr/SigChecker;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method

.method private checkSignature(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "apkFilePath"    # Ljava/lang/String;
    .param p2, "hashCode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 29
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/applicationmgr/SigChecker;->getCertificateBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/applicationmgr/SigChecker;->loadCertificates([B)Ljava/security/cert/Certificate;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/applicationmgr/SigChecker;->getCertStringFromCert(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v2

    .line 30
    .local v2, "hash":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/applicationmgr/SigChecker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "jangil::apk file\'s signature : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    iget-object v4, p0, Lcom/sec/android/applicationmgr/SigChecker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "jangil::downloadcheck signature : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 33
    iget-object v4, p0, Lcom/sec/android/applicationmgr/SigChecker;->TAG:Ljava/lang/String;

    const-string v5, "jangil::signature same.."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 34
    const/4 v3, 0x1

    .line 44
    .end local v2    # "hash":Ljava/lang/String;
    :goto_0
    return v3

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v0}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_0

    .line 39
    .end local v0    # "e":Ljava/security/cert/CertificateException;
    :catch_1
    move-exception v1

    .line 40
    .local v1, "e1":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 43
    .end local v1    # "e1":Ljava/lang/Exception;
    .restart local v2    # "hash":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/applicationmgr/SigChecker;->TAG:Ljava/lang/String;

    const-string v5, "jangil::signature different.."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getCertStringFromCert(Ljava/security/cert/Certificate;)Ljava/lang/String;
    .locals 6
    .param p1, "certs"    # Ljava/security/cert/Certificate;

    .prologue
    .line 48
    move-object v1, p1

    check-cast v1, Ljava/security/cert/X509Certificate;

    .line 49
    .local v1, "signInfo":Ljava/security/cert/X509Certificate;
    if-eqz v1, :cond_1

    .line 50
    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->getSignature()[B

    move-result-object v3

    .line 51
    .local v3, "temp":[B
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 52
    .local v2, "signature":Ljava/lang/StringBuffer;
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 55
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 57
    .end local v2    # "signature":Ljava/lang/StringBuffer;
    .end local v3    # "temp":[B
    :goto_1
    return-object v4

    .line 52
    .restart local v2    # "signature":Ljava/lang/StringBuffer;
    .restart local v3    # "temp":[B
    :cond_0
    aget-byte v0, v3, v4

    .line 53
    .local v0, "b":B
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 52
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 57
    .end local v0    # "b":B
    .end local v2    # "signature":Ljava/lang/StringBuffer;
    .end local v3    # "temp":[B
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private getCertificateBytes(Ljava/lang/String;)[B
    .locals 4
    .param p1, "apkFilename"    # Ljava/lang/String;

    .prologue
    .line 68
    iget-object v3, p0, Lcom/sec/android/applicationmgr/SigChecker;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 69
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/16 v3, 0x40

    invoke-virtual {v1, p1, v3}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 70
    .local v0, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v0, :cond_0

    .line 71
    iget-object v2, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 72
    .local v2, "sig":[Landroid/content/pm/Signature;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v3}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    .line 74
    .end local v2    # "sig":[Landroid/content/pm/Signature;
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private loadCertificates([B)Ljava/security/cert/Certificate;
    .locals 4
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 61
    const-string v3, "X.509"

    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 62
    .local v1, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 63
    .local v2, "in":Ljava/io/InputStream;
    invoke-virtual {v1, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 64
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    return-object v0
.end method


# virtual methods
.method public validate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "apkFilePath"    # Ljava/lang/String;
    .param p2, "signatureCodeFromServer"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/android/applicationmgr/SigChecker;->checkSignature(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
