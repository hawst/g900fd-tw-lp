.class public Lcom/sec/android/applicationmgr/HFileUtil;
.super Ljava/lang/Object;
.source "HFileUtil.java"


# instance fields
.field private _AbsolutePath:Ljava/lang/String;

.field private _Context:Landroid/content/Context;

.field private _File:Ljava/io/File;

.field private _FileName:Ljava/lang/String;

.field private _bPrepared:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_bPrepared:Z

    .line 16
    iput-object p1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_Context:Landroid/content/Context;

    .line 17
    iput-object p2, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_FileName:Ljava/lang/String;

    .line 18
    return-void
.end method

.method private createDir(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 43
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "root":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v2

    if-nez v2, :cond_0

    .line 54
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v2, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 60
    .local v1, "ret":Ljava/io/File;
    return-object v1
.end method


# virtual methods
.method public delete()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    .line 38
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exists()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    .line 88
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_FileName:Ljava/lang/String;

    return-object v0
.end method

.method public length()J
    .locals 2

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_bPrepared:Z

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 96
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public prepare()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 64
    iget-boolean v1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_bPrepared:Z

    if-eqz v1, :cond_0

    .line 74
    :goto_0
    return v0

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_Context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/applicationmgr/HFileUtil;->createDir(Ljava/lang/String;)Z

    .line 69
    iget-object v1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_FileName:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/applicationmgr/HFileUtil;->createInternalStorageFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    .line 70
    iget-object v1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    if-nez v1, :cond_1

    .line 71
    const/4 v0, 0x0

    goto :goto_0

    .line 72
    :cond_1
    iget-object v1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_AbsolutePath:Ljava/lang/String;

    .line 73
    iput-boolean v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_bPrepared:Z

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_Context:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_File:Ljava/io/File;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/applicationmgr/HFileUtil;->_bPrepared:Z

    .line 30
    return-void
.end method
