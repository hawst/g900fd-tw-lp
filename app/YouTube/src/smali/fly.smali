.class public final Lfly;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhsi;

.field public b:Lfnc;

.field public c:Lfma;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field private final f:Lfqh;

.field private g:Ljava/lang/CharSequence;

.field private h:Lhog;


# direct methods
.method public constructor <init>(Lhsi;Lfqh;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Lfic;-><init>()V

    .line 69
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsi;

    iput-object v0, p0, Lfly;->a:Lhsi;

    .line 70
    iput-object p2, p0, Lfly;->f:Lfqh;

    .line 73
    iget-object v0, p1, Lhsi;->a:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 74
    iget-object v0, p1, Lhsi;->b:Lhgz;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-wide v0, p1, Lhsi;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p1, Lhsi;->h:Lhgz;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p1, Lhsi;->g:Lhgz;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 185
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 186
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lfly;->h:Lhog;

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lfly;->a:Lhsi;

    iget-object v0, v0, Lhsi;->e:Lhog;

    iput-object v0, p0, Lfly;->h:Lhog;

    .line 150
    :cond_0
    iget-object v0, p0, Lfly;->h:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lfly;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lfly;->a:Lhsi;

    iget-object v0, v0, Lhsi;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfly;->g:Ljava/lang/CharSequence;

    .line 94
    :cond_0
    iget-object v0, p0, Lfly;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lfly;->a:Lhsi;

    iget-object v0, v0, Lhsi;->i:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lfly;->f:Lfqh;

    return-object v0
.end method
