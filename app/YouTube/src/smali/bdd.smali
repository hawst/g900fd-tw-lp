.class public final Lbdd;
.super Lbdx;
.source "SourceFile"

# interfaces
.implements Lfrz;


# instance fields
.field private X:Letc;

.field private Y:Leyt;

.field private Z:Landroid/content/res/Resources;

.field private a:Lari;

.field private aa:Lfmi;

.field private ab:Lfvh;

.field private ac:Landroid/widget/ListView;

.field private ad:Lbvc;

.field private ae:Lbvc;

.field private af:Lerv;

.field private ag:Lfqg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lbdx;-><init>()V

    return-void
.end method


# virtual methods
.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lbdd;->ag:Lfqg;

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 98
    const v0, 0x7f0400f9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbdd;->ac:Landroid/widget/ListView;

    .line 100
    iget-object v0, p0, Lbdd;->ac:Landroid/widget/ListView;

    return-object v0
.end method

.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lbdd;->Z:Landroid/content/res/Resources;

    const v1, 0x7f0901f3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 60
    invoke-super {p0, p1}, Lbdx;->a(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lbdd;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lbdd;->a:Lari;

    .line 62
    invoke-virtual {p0}, Lbdd;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lbdd;->X:Letc;

    .line 63
    invoke-virtual {p0}, Lbdd;->k()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbdd;->Z:Landroid/content/res/Resources;

    .line 64
    iget-object v0, p0, Lbdd;->a:Lari;

    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lbdd;->Y:Leyt;

    .line 67
    new-instance v0, Lerv;

    iget-object v1, p0, Lbdd;->a:Lari;

    invoke-virtual {v1}, Lari;->C()Lgjp;

    move-result-object v1

    invoke-direct {v0, v1}, Lerv;-><init>(Lgjp;)V

    iput-object v0, p0, Lbdd;->af:Lerv;

    .line 69
    invoke-virtual {p0}, Lbdd;->h()Landroid/os/Bundle;

    move-result-object v2

    .line 70
    const/4 v1, 0x0

    .line 72
    :try_start_0
    new-instance v0, Lhuo;

    invoke-direct {v0}, Lhuo;-><init>()V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :try_start_1
    const-string v1, "section_list_without_preview_proto"

    .line 74
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 73
    invoke-static {v0, v1}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_1
    .catch Lidg; {:try_start_1 .. :try_end_1} :catch_1

    .line 79
    :goto_0
    const-string v1, "section_list_without_preview_tracking_params"

    .line 80
    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 81
    new-instance v2, Lfqg;

    iget-object v3, p0, Lbdd;->X:Letc;

    .line 82
    invoke-virtual {v3}, Letc;->k()Lfac;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lfqg;-><init>(Lfac;[B)V

    iput-object v2, p0, Lbdd;->ag:Lfqg;

    .line 84
    if-eqz v0, :cond_0

    iget-object v1, v0, Lhuo;->a:[Lhup;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 86
    new-instance v1, Lhul;

    invoke-direct {v1}, Lhul;-><init>()V

    .line 87
    iget-object v0, v0, Lhuo;->a:[Lhup;

    aget-object v0, v0, v4

    iget-object v0, v0, Lhup;->b:Lhky;

    .line 88
    new-instance v2, Lhun;

    invoke-direct {v2}, Lhun;-><init>()V

    .line 89
    iput-object v0, v2, Lhun;->b:Lhky;

    .line 90
    const/4 v0, 0x1

    new-array v0, v0, [Lhun;

    aput-object v2, v0, v4

    iput-object v0, v1, Lhul;->a:[Lhun;

    .line 91
    new-instance v0, Lfmi;

    invoke-direct {v0, v1}, Lfmi;-><init>(Lhul;)V

    iput-object v0, p0, Lbdd;->aa:Lfmi;

    .line 93
    :cond_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 76
    :goto_1
    iget-object v3, p0, Lbdd;->Y:Leyt;

    invoke-interface {v3, v1}, Leyt;->c(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 75
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 18

    .prologue
    .line 105
    invoke-super/range {p0 .. p1}, Lbdx;->d(Landroid/os/Bundle;)V

    .line 107
    move-object/from16 v0, p0

    iget-object v1, v0, Lbdd;->a:Lari;

    .line 109
    invoke-virtual {v1}, Lari;->K()Ldsn;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lbdd;->Y:Leyt;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdd;->a:Lari;

    .line 111
    iget-object v3, v3, Lari;->b:Ldov;

    invoke-virtual {v3}, Ldov;->g()Ldaq;

    move-result-object v3

    .line 108
    invoke-static {v1, v2, v3}, Lbvc;->a(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lbdd;->ad:Lbvc;

    .line 112
    move-object/from16 v0, p0

    iget-object v1, v0, Lbdd;->a:Lari;

    .line 114
    invoke-virtual {v1}, Lari;->K()Ldsn;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lbdd;->Y:Leyt;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdd;->a:Lari;

    .line 116
    iget-object v3, v3, Lari;->b:Ldov;

    invoke-virtual {v3}, Ldov;->g()Ldaq;

    move-result-object v3

    .line 113
    invoke-static {v1, v2, v3}, Lbvc;->b(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lbdd;->ae:Lbvc;

    .line 118
    new-instance v1, Lfuu;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbdd;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdd;->a:Lari;

    .line 120
    invoke-virtual {v3}, Lari;->f()Larh;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdd;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 121
    iget-object v4, v4, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbdd;->X:Letc;

    .line 122
    invoke-virtual {v5}, Letc;->i()Levn;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lbdd;->a:Lari;

    .line 123
    invoke-virtual {v6}, Lari;->c()Leyp;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbdd;->a:Lari;

    .line 124
    invoke-virtual {v7}, Lari;->aD()Lcst;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lbdd;->a:Lari;

    .line 125
    invoke-virtual {v8}, Lari;->aO()Lcub;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lbdd;->af:Lerv;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbdd;->a:Lari;

    .line 127
    invoke-virtual {v10}, Lari;->F()Lffs;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lbdd;->a:Lari;

    .line 128
    iget-object v11, v11, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbdd;->Y:Leyt;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbdd;->ad:Lbvc;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbdd;->ae:Lbvc;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbdd;->a:Lari;

    .line 132
    invoke-virtual {v15}, Lari;->ae()Lfdw;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lbdd;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v16, v0

    .line 134
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->j:Lfun;

    move-object/from16 v17, v0

    move-object/from16 v16, p0

    invoke-direct/range {v1 .. v17}, Lfuu;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcyc;Lfhz;Levn;Leyp;Lgix;Lcub;Lerv;Lffs;Ljava/util/concurrent/atomic/AtomicReference;Leyt;Lbvc;Lbvc;Lfdw;Lfrz;Lfun;)V

    .line 136
    new-instance v2, Lcew;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdd;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v4, Lcie;

    invoke-direct {v4}, Lcie;-><init>()V

    move-object/from16 v0, p0

    iget-object v5, v0, Lbdd;->X:Letc;

    .line 139
    invoke-virtual {v5}, Letc;->i()Levn;

    move-result-object v5

    invoke-direct {v2, v3, v4, v5}, Lcew;-><init>(Landroid/content/Context;Lfsj;Levn;)V

    .line 140
    new-instance v10, Lawk;

    invoke-direct {v10, v2}, Lawk;-><init>(Lcew;)V

    .line 142
    new-instance v2, Lfuw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbdd;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 144
    iget-object v3, v3, Lbhz;->m:Lfus;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdd;->a:Lari;

    .line 145
    invoke-virtual {v4}, Lari;->s()Lfgk;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lbdd;->X:Letc;

    .line 146
    invoke-virtual {v5}, Letc;->i()Levn;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lbdd;->Y:Leyt;

    new-instance v9, Lbde;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lbde;-><init>(Lbdd;)V

    move-object v6, v1

    move-object/from16 v7, p0

    invoke-direct/range {v2 .. v9}, Lfuw;-><init>(Lfus;Lfdg;Levn;Lfuu;Lfrz;Leyt;Lful;)V

    .line 156
    new-instance v3, Lfvh;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbdd;->ac:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lbdd;->a:Lari;

    .line 159
    invoke-virtual {v1}, Lari;->s()Lfgk;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lbdd;->X:Letc;

    .line 160
    invoke-virtual {v1}, Letc;->i()Levn;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lbdd;->Y:Leyt;

    move-object v5, v10

    move-object v8, v2

    invoke-direct/range {v3 .. v9}, Lfvh;-><init>(Landroid/widget/ListView;Lfvd;Lfdg;Levn;Lfuw;Leyt;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lbdd;->ab:Lfvh;

    .line 164
    move-object/from16 v0, p0

    iget-object v1, v0, Lbdd;->ab:Lfvh;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbdd;->aa:Lfmi;

    invoke-virtual {v1, v2}, Lfvh;->b(Lfmi;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v1, v0, Lbdd;->ab:Lfvh;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lbdd;->a(Lezl;)V

    .line 166
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0}, Lbdx;->t()V

    .line 171
    iget-object v0, p0, Lbdd;->aa:Lfmi;

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lbdd;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->c(Z)V

    .line 178
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lbdd;->ad:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 177
    iget-object v0, p0, Lbdd;->ae:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    goto :goto_0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Lbdx;->v()V

    .line 183
    iget-object v0, p0, Lbdd;->ad:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 184
    iget-object v0, p0, Lbdd;->ae:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 185
    return-void
.end method
