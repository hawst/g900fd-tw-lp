.class public final enum Lezd;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lezd;

.field public static final enum b:Lezd;

.field public static final enum c:Lezd;

.field private static final d:Ljava/util/Map;

.field private static final synthetic f:[Lezd;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 18
    new-instance v0, Lezd;

    const-string v1, "ROBOTO_LIGHT"

    const-string v2, "Roboto-Light.ttf"

    invoke-direct {v0, v1, v3, v2}, Lezd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lezd;->a:Lezd;

    .line 19
    new-instance v0, Lezd;

    const-string v1, "ROBOTO_REGULAR"

    const-string v2, "Roboto-Regular.ttf"

    invoke-direct {v0, v1, v4, v2}, Lezd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lezd;->b:Lezd;

    .line 20
    new-instance v0, Lezd;

    const-string v1, "ROBOTO_MEDIUM"

    const-string v2, "Roboto-Medium.ttf"

    invoke-direct {v0, v1, v5, v2}, Lezd;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lezd;->c:Lezd;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Lezd;

    sget-object v1, Lezd;->a:Lezd;

    aput-object v1, v0, v3

    sget-object v1, Lezd;->b:Lezd;

    aput-object v1, v0, v4

    sget-object v1, Lezd;->c:Lezd;

    aput-object v1, v0, v5

    sput-object v0, Lezd;->f:[Lezd;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lezd;->d:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput-object p3, p0, Lezd;->e:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lezd;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lezd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lezd;

    return-object v0
.end method

.method public static values()[Lezd;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lezd;->f:[Lezd;

    invoke-virtual {v0}, [Lezd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lezd;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 3

    .prologue
    .line 30
    invoke-static {}, Lb;->a()V

    .line 31
    sget-object v0, Lezd;->d:Ljava/util/Map;

    iget-object v1, p0, Lezd;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iget-object v1, p0, Lezd;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    .line 34
    sget-object v1, Lezd;->d:Ljava/util/Map;

    iget-object v2, p0, Lezd;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :cond_0
    :goto_0
    sget-object v0, Lezd;->d:Ljava/util/Map;

    iget-object v1, p0, Lezd;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    return-object v0

    .line 37
    :catch_0
    move-exception v0

    sget-object v0, Lezd;->d:Ljava/util/Map;

    iget-object v1, p0, Lezd;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
