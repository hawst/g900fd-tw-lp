.class public final Lbxu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Comparator;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/List;

.field private final d:Ljava/lang/Runnable;

.field private final e:Ljava/lang/Runnable;

.field private f:Lbxy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lbxv;

    invoke-direct {v0}, Lbxv;-><init>()V

    sput-object v0, Lbxu;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lbxu;->b:Landroid/os/Handler;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbxu;->c:Ljava/util/List;

    .line 82
    new-instance v0, Lbxw;

    invoke-direct {v0, p0}, Lbxw;-><init>(Lbxu;)V

    iput-object v0, p0, Lbxu;->d:Ljava/lang/Runnable;

    .line 88
    new-instance v0, Lbxx;

    invoke-direct {v0, p0}, Lbxx;-><init>(Lbxu;)V

    iput-object v0, p0, Lbxu;->e:Ljava/lang/Runnable;

    .line 94
    return-void
.end method

.method private declared-synchronized a()V
    .locals 1

    .prologue
    .line 172
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbxu;->f:Lbxy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxu;->f:Lbxy;

    invoke-interface {v0}, Lbxy;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lbxu;->f:Lbxy;

    invoke-interface {v0}, Lbxy;->d()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lbxu;->f:Lbxy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    :cond_0
    monitor-exit p0

    return-void

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lbxu;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lbxu;->b()V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/Class;)Z
    .locals 2

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbxu;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxy;

    .line 101
    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x1

    .line 105
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 3

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lbxu;->a()V

    .line 182
    iget-object v0, p0, Lbxu;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxy;

    .line 183
    invoke-interface {v0}, Lbxy;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 185
    iget-object v1, p0, Lbxu;->f:Lbxy;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lbxu;->f:Lbxy;

    if-eq v1, v0, :cond_1

    .line 186
    iget-object v1, p0, Lbxu;->f:Lbxy;

    invoke-interface {v1}, Lbxy;->d()V

    .line 188
    :cond_1
    iput-object v0, p0, Lbxu;->f:Lbxy;

    .line 189
    invoke-interface {v0}, Lbxy;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    :cond_2
    monitor-exit p0

    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lbxu;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lbxu;->a()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lbxy;)V
    .locals 2

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {p0, v0}, Lbxu;->a(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    iget-object v0, p0, Lbxu;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v0, p0, Lbxu;->c:Ljava/util/List;

    sget-object v1, Lbxu;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 119
    iget-object v0, p0, Lbxu;->f:Lbxy;

    if-eqz v0, :cond_0

    sget-object v0, Lbxu;->a:Ljava/util/Comparator;

    iget-object v1, p0, Lbxu;->f:Lbxy;

    .line 120
    invoke-interface {v0, v1, p1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    .line 121
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbxu;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :cond_1
    monitor-exit p0

    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 161
    if-eqz p1, :cond_0

    .line 162
    iget-object v0, p0, Lbxu;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbxu;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 168
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lbxu;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbxu;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 165
    iget-object v0, p0, Lbxu;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbxu;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 166
    iget-object v0, p0, Lbxu;->b:Landroid/os/Handler;

    iget-object v1, p0, Lbxu;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final declared-synchronized b(Lbxy;)V
    .locals 1

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbxu;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lbxu;->f:Lbxy;

    if-ne v0, p1, :cond_0

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lbxu;->f:Lbxy;

    .line 134
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbxu;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    :cond_0
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
