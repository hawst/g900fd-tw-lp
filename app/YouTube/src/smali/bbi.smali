.class public Lbbi;
.super Lbhz;
.source "SourceFile"


# instance fields
.field private e:Lglk;

.field private f:Landroid/widget/ListView;

.field private g:Landroid/widget/TextView;

.field private h:Lfsi;

.field private i:Landroid/os/AsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lbhz;-><init>()V

    .line 147
    return-void
.end method

.method static synthetic a(Lbbi;)Lglk;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbbi;->e:Lglk;

    return-object v0
.end method

.method static synthetic a(Lbbi;Lglk;)Ljava/util/List;
    .locals 3

    .prologue
    .line 44
    invoke-virtual {p1}, Lglk;->d()Levl;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    invoke-interface {v0}, Levl;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Levl;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Levl;->a()V

    return-object v1
.end method

.method static synthetic b(Lbbi;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbbi;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lbbi;)Lfsi;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbbi;->h:Lfsi;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 54
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-static {p0}, La;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lbbi;->finish()V

    .line 61
    :cond_0
    const v0, 0x7f040057

    invoke-virtual {p0, v0}, Lbbi;->setContentView(I)V

    .line 63
    invoke-virtual {p0}, Lbbi;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 64
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    invoke-virtual {v0}, Lari;->R()Lglk;

    move-result-object v0

    iput-object v0, p0, Lbbi;->e:Lglk;

    .line 67
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lbbi;->h:Lfsi;

    .line 68
    iget-object v0, p0, Lbbi;->h:Lfsi;

    const-class v1, Ldzy;

    new-instance v2, Lbbl;

    invoke-direct {v2, p0}, Lbbl;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 71
    const v0, 0x7f080175

    invoke-virtual {p0, v0}, Lbbi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbbi;->f:Landroid/widget/ListView;

    .line 72
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lbbi;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbbi;->g:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lbbi;->f:Landroid/widget/ListView;

    iget-object v1, p0, Lbbi;->h:Lfsi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 76
    new-instance v0, Lbbj;

    invoke-direct {v0, p0}, Lbbj;-><init>(Lbbi;)V

    iput-object v0, p0, Lbbi;->i:Landroid/os/AsyncTask;

    .line 94
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 101
    invoke-super {p0}, Lbhz;->onResume()V

    .line 102
    iget-object v0, p0, Lbbi;->i:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 104
    invoke-virtual {p0}, Lbbi;->d()Lkm;

    move-result-object v0

    const-string v1, "Show offline queue"

    invoke-virtual {v0, v1}, Lkm;->a(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lbbi;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lbbi;->g:Landroid/widget/TextView;

    const-string v1, "Loading..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method
