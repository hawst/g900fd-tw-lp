.class public final Lhol;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lhgz;

.field public c:Lhgz;

.field public d:[B

.field private e:Z

.field private f:[[B


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v0, p0, Lhol;->a:I

    iput-boolean v0, p0, Lhol;->e:Z

    iput-object v1, p0, Lhol;->b:Lhgz;

    iput-object v1, p0, Lhol;->c:Lhgz;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhol;->d:[B

    sget-object v0, Lidj;->e:[[B

    iput-object v0, p0, Lhol;->f:[[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lhol;->a:I

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    iget v2, p0, Lhol;->a:I

    invoke-static {v0, v2}, Lidd;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-boolean v2, p0, Lhol;->e:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-boolean v3, p0, Lhol;->e:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhol;->b:Lhgz;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhol;->b:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhol;->c:Lhgz;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhol;->c:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhol;->d:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhol;->d:[B

    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhol;->f:[[B

    if-eqz v2, :cond_5

    iget-object v2, p0, Lhol;->f:[[B

    array-length v2, v2

    if-lez v2, :cond_5

    iget-object v3, p0, Lhol;->f:[[B

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    invoke-static {v5}, Lidd;->a([B)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    add-int/2addr v0, v2

    iget-object v1, p0, Lhol;->f:[[B

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhol;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhol;->J:I

    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhol;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhol;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhol;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhol;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhol;->e:Z

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhol;->b:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhol;->b:Lhgz;

    :cond_2
    iget-object v0, p0, Lhol;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhol;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhol;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lhol;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhol;->d:[B

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhol;->f:[[B

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [[B

    iget-object v2, p0, Lhol;->f:[[B

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhol;->f:[[B

    :goto_1
    iget-object v1, p0, Lhol;->f:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lhol;->f:[[B

    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lhol;->f:[[B

    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget v0, p0, Lhol;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhol;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    :cond_0
    iget-boolean v0, p0, Lhol;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-boolean v1, p0, Lhol;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_1
    iget-object v0, p0, Lhol;->b:Lhgz;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhol;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhol;->c:Lhgz;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lhol;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhol;->d:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhol;->d:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_4
    iget-object v0, p0, Lhol;->f:[[B

    if-eqz v0, :cond_5

    iget-object v1, p0, Lhol;->f:[[B

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lidd;->a(I[B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lhol;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhol;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhol;

    iget v2, p0, Lhol;->a:I

    iget v3, p1, Lhol;->a:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhol;->e:Z

    iget-boolean v3, p1, Lhol;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhol;->b:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhol;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhol;->c:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhol;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhol;->d:[B

    iget-object v3, p1, Lhol;->d:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhol;->f:[[B

    iget-object v3, p1, Lhol;->f:[[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhol;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhol;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhol;->b:Lhgz;

    iget-object v3, p1, Lhol;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhol;->c:Lhgz;

    iget-object v3, p1, Lhol;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhol;->I:Ljava/util/List;

    iget-object v3, p1, Lhol;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhol;->a:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhol;->e:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhol;->b:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhol;->c:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lhol;->d:[B

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhol;->f:[[B

    if-nez v0, :cond_6

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhol;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhol;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lhol;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_5
    move v2, v0

    move v0, v1

    :goto_4
    iget-object v3, p0, Lhol;->d:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhol;->d:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    move v0, v1

    :goto_5
    iget-object v3, p0, Lhol;->f:[[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    move v3, v1

    :goto_6
    iget-object v4, p0, Lhol;->f:[[B

    aget-object v4, v4, v0

    array-length v4, v4

    if-ge v3, v4, :cond_7

    mul-int/lit8 v2, v2, 0x1f

    iget-object v4, p0, Lhol;->f:[[B

    aget-object v4, v4, v0

    aget-byte v4, v4, v3

    add-int/2addr v4, v2

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_6

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_8
    iget-object v1, p0, Lhol;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
