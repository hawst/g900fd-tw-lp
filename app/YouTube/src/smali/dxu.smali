.class final Ldxu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Ljava/lang/String;

.field private synthetic b:Ljava/net/MulticastSocket;

.field private synthetic c:Ldxt;


# direct methods
.method constructor <init>(Ldxt;Ljava/lang/String;Ljava/net/MulticastSocket;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Ldxu;->c:Ldxt;

    iput-object p2, p0, Ldxu;->a:Ljava/lang/String;

    iput-object p3, p0, Ldxu;->b:Ljava/net/MulticastSocket;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 46
    :try_start_0
    iget-object v0, p0, Ldxu;->a:Ljava/lang/String;

    invoke-static {v0}, Ldxt;->b(Ljava/lang/String;)Ljava/net/DatagramPacket;

    move-result-object v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    iget-object v1, p0, Ldxu;->b:Ljava/net/MulticastSocket;

    invoke-virtual {v1, v0}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    iget-object v1, p0, Ldxu;->c:Ldxt;

    iget-object v1, v1, Ldxt;->a:Ldyg;

    const-string v2, "Error sending Magic packet: %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    :catch_1
    move-exception v0

    .line 53
    iget-object v1, p0, Ldxu;->c:Ldxt;

    iget-object v1, v1, Ldxt;->a:Ldyg;

    const-string v2, "Error parsing mac address [%s]: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Ldxu;->a:Ljava/lang/String;

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Ldyg;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
