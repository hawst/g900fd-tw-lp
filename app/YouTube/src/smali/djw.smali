.class final Ldjw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private synthetic a:Ldjv;


# direct methods
.method constructor <init>(Ldjv;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Ldjw;->a:Ldjv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 205
    iget-object v0, p0, Ldjw;->a:Ldjv;

    const/4 v1, 0x0

    iput-object v1, v0, Ldjv;->m:Leue;

    iget-object v0, p0, Ldjw;->a:Ldjv;

    new-instance v1, Lczb;

    sget-object v2, Lczc;->d:Lczc;

    const/4 v3, 0x1

    iget-object v4, p0, Ldjw;->a:Ldjv;

    iget-object v4, v4, Ldjv;->d:Leyt;

    invoke-interface {v4, p2}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, p2}, Lczb;-><init>(Lczc;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Ldjv;->a(Lczb;)V

    iget-object v0, p0, Ldjw;->a:Ldjv;

    iget-object v1, p0, Ldjw;->a:Ldjv;

    iget v1, v1, Ldjv;->s:I

    iput v1, v0, Ldjv;->r:I

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 205
    check-cast p2, Lgjh;

    iget-object v0, p0, Ldjw;->a:Ldjv;

    iput-object v5, v0, Ldjv;->m:Leue;

    iget-object v0, p0, Ldjw;->a:Ldjv;

    iget-object v0, v0, Ldjv;->n:[Ljava/lang/String;

    array-length v1, v0

    iget-object v2, p0, Ldjw;->a:Ldjv;

    iget-object v0, p0, Ldjw;->a:Ldjv;

    iget-object v0, v0, Ldjv;->n:[Ljava/lang/String;

    iget-object v3, p0, Ldjw;->a:Ldjv;

    iget-object v3, v3, Ldjv;->n:[Ljava/lang/String;

    array-length v3, v3

    iget-object v4, p2, Lgjh;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v2, Ldjv;->n:[Ljava/lang/String;

    iget-object v0, p2, Lgjh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iget-object v3, p0, Ldjw;->a:Ldjv;

    iget-object v3, v3, Ldjv;->n:[Ljava/lang/String;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p2, Lgjh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p2, Lgjh;->e:Landroid/net/Uri;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Ldjw;->a:Ldjv;

    iput-object v5, v0, Ldjv;->l:Lfxg;

    :goto_1
    iget-object v0, p0, Ldjw;->a:Ldjv;

    iget v0, v0, Ldjv;->s:I

    iget-object v1, p0, Ldjw;->a:Ldjv;

    iget-object v1, v1, Ldjv;->n:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Ldjw;->a:Ldjv;

    iget-object v0, v0, Ldjv;->l:Lfxg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldjw;->a:Ldjv;

    invoke-virtual {v0}, Ldjv;->u()V

    :goto_2
    return-void

    :cond_2
    iget-object v0, p0, Ldjw;->a:Ldjv;

    iget-object v1, p2, Lgjh;->e:Landroid/net/Uri;

    iget-object v2, p0, Ldjw;->a:Ldjv;

    iget-object v2, v2, Ldjv;->l:Lfxg;

    invoke-static {v1, v2}, Lfxg;->a(Landroid/net/Uri;Lfxg;)Lfxg;

    move-result-object v1

    iput-object v1, v0, Ldjv;->l:Lfxg;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ldjw;->a:Ldjv;

    invoke-static {v0}, Ldjv;->a(Ldjv;)V

    goto :goto_2
.end method
