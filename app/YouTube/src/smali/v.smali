.class final Lv;
.super Lt;
.source "SourceFile"

# interfaces
.implements Landroid/view/LayoutInflater$Factory;


# static fields
.field static a:Z

.field private static l:Z

.field private static y:Landroid/view/animation/Interpolator;

.field private static z:Landroid/view/animation/Interpolator;


# instance fields
.field b:Ljava/util/ArrayList;

.field c:Ljava/util/ArrayList;

.field d:Ljava/util/ArrayList;

.field e:Ljava/util/ArrayList;

.field f:Ljava/util/ArrayList;

.field g:I

.field h:Lo;

.field i:Ls;

.field j:Z

.field k:Ljava/lang/String;

.field private m:Ljava/util/ArrayList;

.field private n:[Ljava/lang/Runnable;

.field private o:Z

.field private p:Ljava/util/ArrayList;

.field private q:Ljava/util/ArrayList;

.field private r:Lj;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Landroid/os/Bundle;

.field private w:Landroid/util/SparseArray;

.field private x:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x40200000    # 2.5f

    const/high16 v3, 0x3fc00000    # 1.5f

    .line 410
    sput-boolean v0, Lv;->a:Z

    .line 413
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lv;->l:Z

    .line 746
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lv;->y:Landroid/view/animation/Interpolator;

    .line 747
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lv;->z:Landroid/view/animation/Interpolator;

    .line 748
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    .line 749
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 409
    invoke-direct {p0}, Lt;-><init>()V

    .line 436
    const/4 v0, 0x0

    iput v0, p0, Lv;->g:I

    .line 448
    iput-object v1, p0, Lv;->v:Landroid/os/Bundle;

    .line 449
    iput-object v1, p0, Lv;->w:Landroid/util/SparseArray;

    .line 451
    new-instance v0, Lw;

    invoke-direct {v0, p0}, Lw;-><init>(Lv;)V

    iput-object v0, p0, Lv;->x:Ljava/lang/Runnable;

    .line 2205
    return-void
.end method

.method private static a(FF)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    .line 769
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p0, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 770
    sget-object v1, Lv;->z:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 771
    const-wide/16 v2, 0xdc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 772
    return-object v0
.end method

.method private static a(FFFF)Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    .line 755
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 756
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 758
    sget-object v1, Lv;->y:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 759
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 760
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 761
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 762
    sget-object v1, Lv;->z:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 763
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 764
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 765
    return-object v9
.end method

.method private a(Lj;IZI)Landroid/view/animation/Animation;
    .locals 6

    .prologue
    const v5, 0x3f79999a    # 0.975f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 777
    iget v0, p1, Lj;->I:I

    invoke-static {}, Lj;->q()Landroid/view/animation/Animation;

    .line 779
    iget v0, p1, Lj;->I:I

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lv;->h:Lo;

    iget v2, p1, Lj;->I:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 785
    if-eqz v0, :cond_0

    .line 831
    :goto_0
    return-object v0

    .line 790
    :cond_0
    if-nez p2, :cond_1

    move-object v0, v1

    .line 791
    goto :goto_0

    .line 794
    :cond_1
    const/4 v0, -0x1

    sparse-switch p2, :sswitch_data_0

    .line 795
    :goto_1
    if-gez v0, :cond_5

    move-object v0, v1

    .line 796
    goto :goto_0

    .line 794
    :sswitch_0
    if-eqz p3, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    if-eqz p3, :cond_3

    const/4 v0, 0x3

    goto :goto_1

    :cond_3
    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    if-eqz p3, :cond_4

    const/4 v0, 0x5

    goto :goto_1

    :cond_4
    const/4 v0, 0x6

    goto :goto_1

    .line 799
    :cond_5
    packed-switch v0, :pswitch_data_0

    .line 814
    if-nez p4, :cond_6

    iget-object v0, p0, Lv;->h:Lo;

    invoke-virtual {v0}, Lo;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 815
    iget-object v0, p0, Lv;->h:Lo;

    invoke-virtual {v0}, Lo;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget p4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 817
    :cond_6
    if-nez p4, :cond_7

    move-object v0, v1

    .line 818
    goto :goto_0

    .line 801
    :pswitch_0
    iget-object v0, p0, Lv;->h:Lo;

    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v3, v4, v3}, Lv;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 803
    :pswitch_1
    iget-object v0, p0, Lv;->h:Lo;

    invoke-static {v3, v5, v3, v4}, Lv;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 805
    :pswitch_2
    iget-object v0, p0, Lv;->h:Lo;

    invoke-static {v5, v3, v4, v3}, Lv;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 807
    :pswitch_3
    iget-object v0, p0, Lv;->h:Lo;

    const v0, 0x3f89999a    # 1.075f

    invoke-static {v3, v0, v3, v4}, Lv;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 809
    :pswitch_4
    iget-object v0, p0, Lv;->h:Lo;

    invoke-static {v4, v3}, Lv;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 811
    :pswitch_5
    iget-object v0, p0, Lv;->h:Lo;

    invoke-static {v3, v4}, Lv;->a(FF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :cond_7
    move-object v0, v1

    .line 831
    goto :goto_0

    .line 794
    nop

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch

    .line 799
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Lj;
    .locals 5

    .prologue
    const/4 v0, -0x1

    .line 579
    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 580
    if-ne v1, v0, :cond_1

    .line 581
    const/4 v0, 0x0

    .line 592
    :cond_0
    :goto_0
    return-object v0

    .line 583
    :cond_1
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    .line 584
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment no longer exists for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lv;->a(Ljava/lang/RuntimeException;)V

    .line 587
    :cond_2
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 588
    if-nez v0, :cond_0

    .line 589
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lv;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method private a(ILc;)V
    .locals 3

    .prologue
    .line 1421
    monitor-enter p0

    .line 1422
    :try_start_0
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1423
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    .line 1425
    :cond_0
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1426
    if-ge p1, v0, :cond_1

    .line 1427
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1442
    :goto_0
    monitor-exit p0

    return-void

    .line 1430
    :cond_1
    :goto_1
    if-ge v0, p1, :cond_3

    .line 1431
    iget-object v1, p0, Lv;->e:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1432
    iget-object v1, p0, Lv;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    .line 1433
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lv;->f:Ljava/util/ArrayList;

    .line 1435
    :cond_2
    iget-object v1, p0, Lv;->f:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1439
    :cond_3
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1442
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .locals 5

    .prologue
    .line 459
    const-string v0, "FragmentManager"

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    const-string v0, "FragmentManager"

    const-string v1, "Activity state:"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 461
    new-instance v0, Ldp;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Ldp;-><init>(Ljava/lang/String;)V

    .line 462
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 463
    iget-object v0, p0, Lv;->h:Lo;

    if-eqz v0, :cond_0

    .line 465
    :try_start_0
    iget-object v0, p0, Lv;->h:Lo;

    const-string v2, "  "

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1, v4}, Lo;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 476
    :goto_0
    throw p1

    .line 466
    :catch_0
    move-exception v0

    .line 467
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 471
    :cond_0
    :try_start_1
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, Lv;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 472
    :catch_1
    move-exception v0

    .line 473
    const-string v1, "FragmentManager"

    const-string v2, "Failed dumping state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 2069
    const/4 v0, 0x0

    .line 2070
    sparse-switch p0, :sswitch_data_0

    .line 2081
    :goto_0
    return v0

    .line 2072
    :sswitch_0
    const/16 v0, 0x2002

    .line 2073
    goto :goto_0

    .line 2075
    :sswitch_1
    const/16 v0, 0x1001

    .line 2076
    goto :goto_0

    .line 2078
    :sswitch_2
    const/16 v0, 0x1003

    goto :goto_0

    .line 2070
    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private c(Lj;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1104
    iget v2, p0, Lv;->g:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    .line 1105
    return-void
.end method

.method private d(Lj;)V
    .locals 2

    .prologue
    .line 1621
    iget-object v0, p1, Lj;->L:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1634
    :cond_0
    :goto_0
    return-void

    .line 1624
    :cond_1
    iget-object v0, p0, Lv;->w:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    .line 1625
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lv;->w:Landroid/util/SparseArray;

    .line 1629
    :goto_1
    iget-object v0, p1, Lj;->L:Landroid/view/View;

    iget-object v1, p0, Lv;->w:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 1630
    iget-object v0, p0, Lv;->w:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 1631
    iget-object v0, p0, Lv;->w:Landroid/util/SparseArray;

    iput-object v0, p1, Lj;->h:Landroid/util/SparseArray;

    .line 1632
    const/4 v0, 0x0

    iput-object v0, p0, Lv;->w:Landroid/util/SparseArray;

    goto :goto_0

    .line 1627
    :cond_2
    iget-object v0, p0, Lv;->w:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_1
.end method

.method private e(Lj;)Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1637
    .line 1639
    iget-object v0, p0, Lv;->v:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 1640
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lv;->v:Landroid/os/Bundle;

    .line 1642
    :cond_0
    iget-object v0, p0, Lv;->v:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lj;->g(Landroid/os/Bundle;)V

    .line 1643
    iget-object v0, p0, Lv;->v:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1644
    iget-object v0, p0, Lv;->v:Landroid/os/Bundle;

    .line 1645
    iput-object v1, p0, Lv;->v:Landroid/os/Bundle;

    .line 1648
    :goto_0
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1649
    invoke-direct {p0, p1}, Lv;->d(Lj;)V

    .line 1651
    :cond_1
    iget-object v1, p1, Lj;->h:Landroid/util/SparseArray;

    if-eqz v1, :cond_3

    .line 1652
    if-nez v0, :cond_2

    .line 1653
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1655
    :cond_2
    const-string v1, "android:view_state"

    iget-object v2, p1, Lj;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 1658
    :cond_3
    iget-boolean v1, p1, Lj;->N:Z

    if-nez v1, :cond_5

    .line 1659
    if-nez v0, :cond_4

    .line 1660
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1663
    :cond_4
    const-string v1, "android:user_visible_hint"

    iget-boolean v2, p1, Lj;->N:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1666
    :cond_5
    return-object v0

    :cond_6
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lc;)I
    .locals 2

    .prologue
    .line 1401
    monitor-enter p0

    .line 1402
    :try_start_0
    iget-object v0, p0, Lv;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lv;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1403
    :cond_0
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1404
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    .line 1406
    :cond_1
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1407
    iget-object v1, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1409
    monitor-exit p0

    .line 1415
    :goto_0
    return v0

    .line 1412
    :cond_2
    iget-object v0, p0, Lv;->f:Ljava/util/ArrayList;

    iget-object v1, p0, Lv;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1413
    iget-object v1, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1415
    monitor-exit p0

    goto :goto_0

    .line 1417
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Laf;
    .locals 1

    .prologue
    .line 481
    new-instance v0, Lc;

    invoke-direct {v0, p0}, Lc;-><init>(Lv;)V

    return-object v0
.end method

.method public final a(I)Lj;
    .locals 3

    .prologue
    .line 1308
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 1310
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1311
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1312
    if-eqz v0, :cond_1

    iget v2, v0, Lj;->z:I

    if-ne v2, p1, :cond_1

    .line 1326
    :cond_0
    :goto_1
    return-object v0

    .line 1310
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1317
    :cond_2
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1319
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1320
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1321
    if-eqz v0, :cond_3

    iget v2, v0, Lj;->z:I

    if-eq v2, p1, :cond_0

    .line 1319
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1326
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lj;
    .locals 3

    .prologue
    .line 1330
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 1332
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 1333
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1334
    if-eqz v0, :cond_1

    iget-object v2, v0, Lj;->B:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1348
    :cond_0
    :goto_1
    return-object v0

    .line 1332
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1339
    :cond_2
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    if-eqz p1, :cond_4

    .line 1341
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    .line 1342
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1343
    if-eqz v0, :cond_3

    iget-object v2, v0, Lj;->B:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1341
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1348
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lj;)Lm;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 602
    iget v1, p1, Lj;->i:I

    if-gez v1, :cond_0

    .line 603
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not currently in the FragmentManager"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lv;->a(Ljava/lang/RuntimeException;)V

    .line 606
    :cond_0
    iget v1, p1, Lj;->d:I

    if-lez v1, :cond_1

    .line 607
    invoke-direct {p0, p1}, Lv;->e(Lj;)Landroid/os/Bundle;

    move-result-object v1

    .line 608
    if-eqz v1, :cond_1

    new-instance v0, Lm;

    invoke-direct {v0, v1}, Lm;-><init>(Landroid/os/Bundle;)V

    .line 610
    :cond_1
    return-object v0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 523
    if-gez p1, :cond_0

    .line 524
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :cond_0
    new-instance v0, Lx;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lx;-><init>(Lv;II)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lv;->a(Ljava/lang/Runnable;Z)V

    .line 531
    return-void
.end method

.method final a(IIIZ)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1112
    iget-object v0, p0, Lv;->h:Lo;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1113
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1116
    :cond_0
    if-nez p4, :cond_2

    iget v0, p0, Lv;->g:I

    if-ne v0, p1, :cond_2

    .line 1142
    :cond_1
    :goto_0
    return-void

    .line 1120
    :cond_2
    iput p1, p0, Lv;->g:I

    .line 1121
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v6, v5

    move v7, v5

    .line 1123
    :goto_1
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    .line 1124
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj;

    .line 1125
    if-eqz v1, :cond_5

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    .line 1126
    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    .line 1127
    iget-object v0, v1, Lj;->O:Lao;

    if-eqz v0, :cond_5

    .line 1128
    iget-object v0, v1, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->a()Z

    move-result v0

    or-int/2addr v7, v0

    move v1, v7

    .line 1123
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto :goto_1

    .line 1133
    :cond_3
    if-nez v7, :cond_4

    .line 1134
    invoke-virtual {p0}, Lv;->d()V

    .line 1137
    :cond_4
    iget-boolean v0, p0, Lv;->s:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lv;->h:Lo;

    if-eqz v0, :cond_1

    iget v0, p0, Lv;->g:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 1138
    iget-object v0, p0, Lv;->h:Lo;

    invoke-virtual {v0}, Lo;->b()V

    .line 1139
    iput-boolean v5, p0, Lv;->s:Z

    goto :goto_0

    :cond_5
    move v1, v7

    goto :goto_2
.end method

.method final a(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1108
    invoke-virtual {p0, p1, v0, v0, v0}, Lv;->a(IIIZ)V

    .line 1109
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 1961
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1962
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1963
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1964
    if-eqz v0, :cond_0

    .line 1965
    invoke-virtual {v0, p1}, Lj;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v2, v0, Lj;->x:Lv;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lj;->x:Lv;

    invoke-virtual {v0, p1}, Lv;->a(Landroid/content/res/Configuration;)V

    .line 1962
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1969
    :cond_1
    return-void
.end method

.method final a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 1788
    if-nez p1, :cond_1

    .line 1896
    :cond_0
    :goto_0
    return-void

    .line 1789
    :cond_1
    check-cast p1, Laa;

    .line 1790
    iget-object v0, p1, Laa;->a:[Lad;

    if-eqz v0, :cond_0

    .line 1794
    if-eqz p2, :cond_3

    move v1, v2

    .line 1795
    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1796
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1797
    iget-object v3, p1, Laa;->a:[Lad;

    iget v4, v0, Lj;->i:I

    aget-object v3, v3, v4

    .line 1799
    iput-object v0, v3, Lad;->k:Lj;

    .line 1800
    iput-object v7, v0, Lj;->h:Landroid/util/SparseArray;

    .line 1801
    iput v2, v0, Lj;->u:I

    .line 1802
    iput-boolean v2, v0, Lj;->s:Z

    .line 1803
    iput-boolean v2, v0, Lj;->o:Z

    .line 1804
    iput-object v7, v0, Lj;->l:Lj;

    .line 1805
    iget-object v4, v3, Lad;->j:Landroid/os/Bundle;

    if-eqz v4, :cond_2

    .line 1806
    iget-object v4, v3, Lad;->j:Landroid/os/Bundle;

    iget-object v5, p0, Lv;->h:Lo;

    invoke-virtual {v5}, Lo;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1807
    iget-object v4, v3, Lad;->j:Landroid/os/Bundle;

    const-string v5, "android:view_state"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v4

    iput-object v4, v0, Lj;->h:Landroid/util/SparseArray;

    .line 1809
    iget-object v3, v3, Lad;->j:Landroid/os/Bundle;

    iput-object v3, v0, Lj;->g:Landroid/os/Bundle;

    .line 1795
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1816
    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Laa;->a:[Lad;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    .line 1817
    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 1818
    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_4
    move v0, v2

    .line 1820
    :goto_2
    iget-object v1, p1, Laa;->a:[Lad;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 1821
    iget-object v1, p1, Laa;->a:[Lad;

    aget-object v3, v1, v0

    .line 1822
    if-eqz v3, :cond_8

    .line 1823
    iget-object v1, p0, Lv;->h:Lo;

    iget-object v4, p0, Lv;->r:Lj;

    iget-object v5, v3, Lad;->k:Lj;

    if-eqz v5, :cond_5

    iget-object v1, v3, Lad;->k:Lj;

    .line 1824
    :goto_3
    iget-object v4, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1829
    iput-object v7, v3, Lad;->k:Lj;

    .line 1820
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1823
    :cond_5
    iget-object v5, v3, Lad;->i:Landroid/os/Bundle;

    if-eqz v5, :cond_6

    iget-object v5, v3, Lad;->i:Landroid/os/Bundle;

    invoke-virtual {v1}, Lo;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_6
    iget-object v5, v3, Lad;->a:Ljava/lang/String;

    iget-object v6, v3, Lad;->i:Landroid/os/Bundle;

    invoke-static {v1, v5, v6}, Lj;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lj;

    move-result-object v5

    iput-object v5, v3, Lad;->k:Lj;

    iget-object v5, v3, Lad;->j:Landroid/os/Bundle;

    if-eqz v5, :cond_7

    iget-object v5, v3, Lad;->j:Landroid/os/Bundle;

    invoke-virtual {v1}, Lo;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v5, v3, Lad;->k:Lj;

    iget-object v6, v3, Lad;->j:Landroid/os/Bundle;

    iput-object v6, v5, Lj;->g:Landroid/os/Bundle;

    :cond_7
    iget-object v5, v3, Lad;->k:Lj;

    iget v6, v3, Lad;->b:I

    invoke-virtual {v5, v6, v4}, Lj;->a(ILj;)V

    iget-object v4, v3, Lad;->k:Lj;

    iget-boolean v5, v3, Lad;->c:Z

    iput-boolean v5, v4, Lj;->r:Z

    iget-object v4, v3, Lad;->k:Lj;

    iput-boolean v8, v4, Lj;->t:Z

    iget-object v4, v3, Lad;->k:Lj;

    iget v5, v3, Lad;->d:I

    iput v5, v4, Lj;->z:I

    iget-object v4, v3, Lad;->k:Lj;

    iget v5, v3, Lad;->e:I

    iput v5, v4, Lj;->A:I

    iget-object v4, v3, Lad;->k:Lj;

    iget-object v5, v3, Lad;->f:Ljava/lang/String;

    iput-object v5, v4, Lj;->B:Ljava/lang/String;

    iget-object v4, v3, Lad;->k:Lj;

    iget-boolean v5, v3, Lad;->g:Z

    iput-boolean v5, v4, Lj;->E:Z

    iget-object v4, v3, Lad;->k:Lj;

    iget-boolean v5, v3, Lad;->h:Z

    iput-boolean v5, v4, Lj;->D:Z

    iget-object v4, v3, Lad;->k:Lj;

    iget-object v1, v1, Lo;->b:Lv;

    iput-object v1, v4, Lj;->v:Lv;

    iget-object v1, v3, Lad;->k:Lj;

    goto :goto_3

    .line 1831
    :cond_8
    iget-object v1, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1832
    iget-object v1, p0, Lv;->p:Ljava/util/ArrayList;

    if-nez v1, :cond_9

    .line 1833
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lv;->p:Ljava/util/ArrayList;

    .line 1835
    :cond_9
    iget-object v1, p0, Lv;->p:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1841
    :cond_a
    if-eqz p2, :cond_d

    move v3, v2

    .line 1842
    :goto_5
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_d

    .line 1843
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1844
    iget v1, v0, Lj;->m:I

    if-ltz v1, :cond_b

    .line 1845
    iget v1, v0, Lj;->m:I

    iget-object v4, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_c

    .line 1846
    iget-object v1, p0, Lv;->b:Ljava/util/ArrayList;

    iget v4, v0, Lj;->m:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lj;

    iput-object v1, v0, Lj;->l:Lj;

    .line 1842
    :cond_b
    :goto_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 1848
    :cond_c
    const-string v1, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Re-attaching retained fragment "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " target no longer exists: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lj;->m:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1850
    iput-object v7, v0, Lj;->l:Lj;

    goto :goto_6

    .line 1857
    :cond_d
    iget-object v0, p1, Laa;->b:[I

    if-eqz v0, :cond_10

    .line 1858
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Laa;->b:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    move v1, v2

    .line 1859
    :goto_7
    iget-object v0, p1, Laa;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_11

    .line 1860
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    iget-object v3, p1, Laa;->b:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1861
    if-nez v0, :cond_e

    .line 1862
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No instantiated fragment for index #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Laa;->b:[I

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lv;->a(Ljava/lang/RuntimeException;)V

    .line 1865
    :cond_e
    iput-boolean v8, v0, Lj;->o:Z

    .line 1866
    iget-object v3, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1868
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1870
    :cond_f
    iget-object v3, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1859
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1873
    :cond_10
    iput-object v7, p0, Lv;->c:Ljava/util/ArrayList;

    .line 1877
    :cond_11
    iget-object v0, p1, Laa;->c:[Lg;

    if-eqz v0, :cond_13

    .line 1878
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Laa;->c:[Lg;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    .line 1879
    :goto_8
    iget-object v0, p1, Laa;->c:[Lg;

    array-length v0, v0

    if-ge v2, v0, :cond_0

    .line 1880
    iget-object v0, p1, Laa;->c:[Lg;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Lg;->a(Lv;)Lc;

    move-result-object v0

    .line 1881
    iget-object v1, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1889
    iget v1, v0, Lc;->g:I

    if-ltz v1, :cond_12

    .line 1890
    iget v1, v0, Lc;->g:I

    invoke-direct {p0, v1, v0}, Lv;->a(ILc;)V

    .line 1879
    :cond_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 1894
    :cond_13
    iput-object v7, p0, Lv;->d:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public final a(Lj;II)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 1212
    iget v0, p1, Lj;->u:I

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_4

    move v0, v1

    .line 1214
    :goto_1
    iget-boolean v2, p1, Lj;->D:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_2

    .line 1215
    :cond_0
    iget-object v2, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 1216
    iget-object v2, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1218
    :cond_1
    iput-boolean v5, p1, Lj;->o:Z

    .line 1222
    iput-boolean v1, p1, Lj;->p:Z

    .line 1223
    if-eqz v0, :cond_5

    move v2, v5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    .line 1226
    :cond_2
    return-void

    :cond_3
    move v0, v5

    .line 1212
    goto :goto_0

    :cond_4
    move v0, v5

    goto :goto_1

    :cond_5
    move v2, v1

    .line 1223
    goto :goto_2
.end method

.method final a(Lj;IIIZ)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 849
    iget-boolean v0, p1, Lj;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lj;->D:Z

    if-eqz v0, :cond_1

    :cond_0
    if-le p2, v5, :cond_1

    move p2, v5

    .line 852
    :cond_1
    iget-boolean v0, p1, Lj;->p:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lj;->d:I

    if-le p2, v0, :cond_2

    .line 854
    iget p2, p1, Lj;->d:I

    .line 858
    :cond_2
    iget-boolean v0, p1, Lj;->M:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lj;->d:I

    if-ge v0, v9, :cond_3

    if-le p2, v6, :cond_3

    move p2, v6

    .line 861
    :cond_3
    iget v0, p1, Lj;->d:I

    if-ge v0, p2, :cond_26

    .line 865
    iget-boolean v0, p1, Lj;->r:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lj;->s:Z

    if-nez v0, :cond_4

    .line 1101
    :goto_0
    return-void

    .line 868
    :cond_4
    iget-object v0, p1, Lj;->e:Landroid/view/View;

    if-eqz v0, :cond_5

    .line 873
    iput-object v7, p1, Lj;->e:Landroid/view/View;

    .line 874
    iget v2, p1, Lj;->f:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    .line 876
    :cond_5
    iget v0, p1, Lj;->d:I

    packed-switch v0, :pswitch_data_0

    .line 1100
    :cond_6
    :goto_1
    iput p2, p1, Lj;->d:I

    goto :goto_0

    .line 878
    :pswitch_0
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    if-eqz v0, :cond_8

    .line 880
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    iget-object v1, p0, Lv;->h:Lo;

    invoke-virtual {v1}, Lo;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 881
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Lj;->h:Landroid/util/SparseArray;

    .line 883
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-direct {p0, v0, v1}, Lv;->a(Landroid/os/Bundle;Ljava/lang/String;)Lj;

    move-result-object v0

    iput-object v0, p1, Lj;->l:Lj;

    .line 885
    iget-object v0, p1, Lj;->l:Lj;

    if-eqz v0, :cond_7

    .line 886
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Lj;->n:I

    .line 889
    :cond_7
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Lj;->N:Z

    .line 891
    iget-boolean v0, p1, Lj;->N:Z

    if-nez v0, :cond_8

    .line 892
    iput-boolean v5, p1, Lj;->M:Z

    .line 893
    if-le p2, v6, :cond_8

    move p2, v6

    .line 898
    :cond_8
    iget-object v0, p0, Lv;->h:Lo;

    iput-object v0, p1, Lj;->w:Lo;

    .line 899
    iget-object v0, p0, Lv;->r:Lj;

    iput-object v0, p1, Lj;->y:Lj;

    .line 900
    iget-object v0, p0, Lv;->r:Lj;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lv;->r:Lj;

    iget-object v0, v0, Lj;->x:Lv;

    :goto_2
    iput-object v0, p1, Lj;->v:Lv;

    .line 902
    iput-boolean v3, p1, Lj;->H:Z

    .line 903
    iget-object v0, p0, Lv;->h:Lo;

    invoke-virtual {p1, v0}, Lj;->a(Landroid/app/Activity;)V

    .line 904
    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_a

    .line 905
    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 900
    :cond_9
    iget-object v0, p0, Lv;->h:Lo;

    iget-object v0, v0, Lo;->b:Lv;

    goto :goto_2

    .line 908
    :cond_a
    iget-object v0, p1, Lj;->y:Lj;

    if-nez v0, :cond_b

    .line 909
    iget-object v0, p0, Lv;->h:Lo;

    .line 912
    :cond_b
    iget-boolean v0, p1, Lj;->F:Z

    if-nez v0, :cond_f

    .line 913
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    iget-object v1, p1, Lj;->x:Lv;

    if-eqz v1, :cond_c

    iget-object v1, p1, Lj;->x:Lv;

    iput-boolean v3, v1, Lv;->j:Z

    :cond_c
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1, v0}, Lj;->a(Landroid/os/Bundle;)V

    iget-boolean v1, p1, Lj;->H:Z

    if-nez v1, :cond_d

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    if-eqz v0, :cond_f

    const-string v1, "android:support:fragments"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v1, p1, Lj;->x:Lv;

    if-nez v1, :cond_e

    invoke-virtual {p1}, Lj;->x()V

    :cond_e
    iget-object v1, p1, Lj;->x:Lv;

    invoke-virtual {v1, v0, v7}, Lv;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->h()V

    .line 915
    :cond_f
    iput-boolean v3, p1, Lj;->F:Z

    .line 916
    iget-boolean v0, p1, Lj;->r:Z

    if-eqz v0, :cond_11

    .line 920
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lj;->b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p1, Lj;->g:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v7, v1}, Lj;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lj;->K:Landroid/view/View;

    .line 922
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    if-eqz v0, :cond_18

    .line 923
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    iput-object v0, p1, Lj;->L:Landroid/view/View;

    .line 924
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    invoke-static {v0}, Lat;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p1, Lj;->K:Landroid/view/View;

    .line 925
    iget-boolean v0, p1, Lj;->C:Z

    if-eqz v0, :cond_10

    iget-object v0, p1, Lj;->K:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 926
    :cond_10
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    invoke-static {}, Lj;->r()V

    .line 932
    :cond_11
    :goto_3
    :pswitch_1
    if-le p2, v5, :cond_1e

    .line 933
    iget-boolean v0, p1, Lj;->r:Z

    if-nez v0, :cond_16

    .line 936
    iget v0, p1, Lj;->A:I

    if-eqz v0, :cond_3c

    .line 937
    iget-object v0, p0, Lv;->i:Ls;

    iget v1, p1, Lj;->A:I

    invoke-interface {v0, v1}, Ls;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 938
    if-nez v0, :cond_12

    iget-boolean v1, p1, Lj;->t:Z

    if-nez v1, :cond_12

    .line 939
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No view found for id 0x"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Lj;->A:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lj;->k()Landroid/content/res/Resources;

    move-result-object v4

    iget v8, p1, Lj;->A:I

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") for fragment "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lv;->a(Ljava/lang/RuntimeException;)V

    .line 946
    :cond_12
    :goto_4
    iput-object v0, p1, Lj;->J:Landroid/view/ViewGroup;

    .line 947
    iget-object v1, p1, Lj;->g:Landroid/os/Bundle;

    invoke-virtual {p1, v1}, Lj;->b(Landroid/os/Bundle;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget-object v2, p1, Lj;->g:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0, v2}, Lj;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lj;->K:Landroid/view/View;

    .line 949
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    if-eqz v1, :cond_19

    .line 950
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    iput-object v1, p1, Lj;->L:Landroid/view/View;

    .line 951
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    invoke-static {v1}, Lat;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p1, Lj;->K:Landroid/view/View;

    .line 952
    if-eqz v0, :cond_14

    .line 953
    invoke-direct {p0, p1, p3, v5, p4}, Lv;->a(Lj;IZI)Landroid/view/animation/Animation;

    move-result-object v1

    .line 955
    if-eqz v1, :cond_13

    .line 956
    iget-object v2, p1, Lj;->K:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 958
    :cond_13
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 960
    :cond_14
    iget-boolean v0, p1, Lj;->C:Z

    if-eqz v0, :cond_15

    iget-object v0, p1, Lj;->K:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 961
    :cond_15
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    invoke-static {}, Lj;->r()V

    .line 967
    :cond_16
    :goto_5
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    iget-object v1, p1, Lj;->x:Lv;

    if-eqz v1, :cond_17

    iget-object v1, p1, Lj;->x:Lv;

    iput-boolean v3, v1, Lv;->j:Z

    :cond_17
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1, v0}, Lj;->d(Landroid/os/Bundle;)V

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_1a

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 928
    :cond_18
    iput-object v7, p1, Lj;->L:Landroid/view/View;

    goto/16 :goto_3

    .line 963
    :cond_19
    iput-object v7, p1, Lj;->L:Landroid/view/View;

    goto :goto_5

    .line 967
    :cond_1a
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_1b

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->i()V

    .line 968
    :cond_1b
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    if-eqz v0, :cond_1d

    .line 969
    iget-object v0, p1, Lj;->g:Landroid/os/Bundle;

    iget-object v0, p1, Lj;->h:Landroid/util/SparseArray;

    if-eqz v0, :cond_1c

    iget-object v0, p1, Lj;->L:Landroid/view/View;

    iget-object v1, p1, Lj;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    iput-object v7, p1, Lj;->h:Landroid/util/SparseArray;

    :cond_1c
    iput-boolean v3, p1, Lj;->H:Z

    iput-boolean v5, p1, Lj;->H:Z

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_1d

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 971
    :cond_1d
    iput-object v7, p1, Lj;->g:Landroid/os/Bundle;

    .line 975
    :cond_1e
    :pswitch_2
    if-le p2, v6, :cond_22

    .line 976
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_1f

    iget-object v0, p1, Lj;->x:Lv;

    iput-boolean v3, v0, Lv;->j:Z

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->f()Z

    :cond_1f
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1}, Lj;->e()V

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_20

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_20
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_21

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->j()V

    :cond_21
    iget-object v0, p1, Lj;->O:Lao;

    if-eqz v0, :cond_22

    iget-object v0, p1, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->f()V

    .line 980
    :cond_22
    :pswitch_3
    if-le p2, v9, :cond_6

    .line 981
    iput-boolean v5, p1, Lj;->q:Z

    .line 983
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_23

    iget-object v0, p1, Lj;->x:Lv;

    iput-boolean v3, v0, Lv;->j:Z

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->f()Z

    :cond_23
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1}, Lj;->t()V

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_24

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_24
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_25

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->k()V

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->f()Z

    .line 984
    :cond_25
    iput-object v7, p1, Lj;->g:Landroid/os/Bundle;

    .line 985
    iput-object v7, p1, Lj;->h:Landroid/util/SparseArray;

    goto/16 :goto_1

    .line 988
    :cond_26
    iget v0, p1, Lj;->d:I

    if-le v0, p2, :cond_6

    .line 989
    iget v0, p1, Lj;->d:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    .line 1052
    :cond_27
    :goto_6
    :pswitch_4
    if-gtz p2, :cond_6

    .line 1053
    iget-boolean v0, p0, Lv;->t:Z

    if-eqz v0, :cond_28

    .line 1054
    iget-object v0, p1, Lj;->e:Landroid/view/View;

    if-eqz v0, :cond_28

    .line 1061
    iget-object v0, p1, Lj;->e:Landroid/view/View;

    .line 1062
    iput-object v7, p1, Lj;->e:Landroid/view/View;

    .line 1063
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1066
    :cond_28
    iget-object v0, p1, Lj;->e:Landroid/view/View;

    if-eqz v0, :cond_35

    .line 1071
    iput p2, p1, Lj;->f:I

    move p2, v5

    .line 1072
    goto/16 :goto_1

    .line 991
    :pswitch_5
    const/4 v0, 0x5

    if-ge p2, v0, :cond_2b

    .line 992
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_29

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->l()V

    :cond_29
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1}, Lj;->u()V

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_2a

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 994
    :cond_2a
    iput-boolean v3, p1, Lj;->q:Z

    .line 997
    :cond_2b
    :pswitch_6
    if-ge p2, v9, :cond_2d

    .line 998
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_2c

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->m()V

    :cond_2c
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1}, Lj;->f()V

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_2d

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1002
    :cond_2d
    :pswitch_7
    if-ge p2, v6, :cond_2e

    .line 1003
    invoke-virtual {p1}, Lj;->y()V

    .line 1007
    :cond_2e
    :pswitch_8
    const/4 v0, 0x2

    if-ge p2, v0, :cond_27

    .line 1008
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    if-eqz v0, :cond_2f

    .line 1012
    iget-object v0, p0, Lv;->h:Lo;

    invoke-virtual {v0}, Lo;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2f

    iget-object v0, p1, Lj;->h:Landroid/util/SparseArray;

    if-nez v0, :cond_2f

    .line 1013
    invoke-direct {p0, p1}, Lv;->d(Lj;)V

    .line 1016
    :cond_2f
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_30

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0, v5, v3}, Lv;->a(IZ)V

    :cond_30
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1}, Lj;->g()V

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_31

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_31
    iget-object v0, p1, Lj;->O:Lao;

    if-eqz v0, :cond_32

    iget-object v0, p1, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->e()V

    .line 1017
    :cond_32
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    if-eqz v0, :cond_34

    iget-object v0, p1, Lj;->J:Landroid/view/ViewGroup;

    if-eqz v0, :cond_34

    .line 1019
    iget v0, p0, Lv;->g:I

    if-lez v0, :cond_3b

    iget-boolean v0, p0, Lv;->t:Z

    if-nez v0, :cond_3b

    .line 1020
    invoke-direct {p0, p1, p3, v3, p4}, Lv;->a(Lj;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1023
    :goto_7
    if-eqz v0, :cond_33

    .line 1025
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    iput-object v1, p1, Lj;->e:Landroid/view/View;

    .line 1026
    iput p2, p1, Lj;->f:I

    .line 1027
    new-instance v1, Ly;

    invoke-direct {v1, p0, p1}, Ly;-><init>(Lv;Lj;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1043
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1045
    :cond_33
    iget-object v0, p1, Lj;->J:Landroid/view/ViewGroup;

    iget-object v1, p1, Lj;->K:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1047
    :cond_34
    iput-object v7, p1, Lj;->J:Landroid/view/ViewGroup;

    .line 1048
    iput-object v7, p1, Lj;->K:Landroid/view/View;

    .line 1049
    iput-object v7, p1, Lj;->L:Landroid/view/View;

    goto/16 :goto_6

    .line 1074
    :cond_35
    iget-boolean v0, p1, Lj;->F:Z

    if-nez v0, :cond_37

    .line 1076
    iget-object v0, p1, Lj;->x:Lv;

    if-eqz v0, :cond_36

    iget-object v0, p1, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->n()V

    :cond_36
    iput-boolean v3, p1, Lj;->H:Z

    invoke-virtual {p1}, Lj;->v()V

    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_37

    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1079
    :cond_37
    iput-boolean v3, p1, Lj;->H:Z

    .line 1080
    invoke-virtual {p1}, Lj;->d()V

    .line 1081
    iget-boolean v0, p1, Lj;->H:Z

    if-nez v0, :cond_38

    .line 1082
    new-instance v0, Lbz;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbz;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1085
    :cond_38
    if-nez p5, :cond_6

    .line 1086
    iget-boolean v0, p1, Lj;->F:Z

    if-nez v0, :cond_3a

    .line 1087
    iget v0, p1, Lj;->i:I

    if-ltz v0, :cond_6

    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    iget v1, p1, Lj;->i:I

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    if-nez v0, :cond_39

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    :cond_39
    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    iget v1, p1, Lj;->i:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lv;->h:Lo;

    iget-object v1, p1, Lj;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lo;->a(Ljava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p1, Lj;->i:I

    iput-object v7, p1, Lj;->j:Ljava/lang/String;

    iput-boolean v3, p1, Lj;->o:Z

    iput-boolean v3, p1, Lj;->p:Z

    iput-boolean v3, p1, Lj;->q:Z

    iput-boolean v3, p1, Lj;->r:Z

    iput-boolean v3, p1, Lj;->s:Z

    iput-boolean v3, p1, Lj;->t:Z

    iput v3, p1, Lj;->u:I

    iput-object v7, p1, Lj;->v:Lv;

    iput-object v7, p1, Lj;->x:Lv;

    iput-object v7, p1, Lj;->w:Lo;

    iput v3, p1, Lj;->z:I

    iput v3, p1, Lj;->A:I

    iput-object v7, p1, Lj;->B:Ljava/lang/String;

    iput-boolean v3, p1, Lj;->C:Z

    iput-boolean v3, p1, Lj;->D:Z

    iput-boolean v3, p1, Lj;->F:Z

    iput-object v7, p1, Lj;->O:Lao;

    iput-boolean v3, p1, Lj;->P:Z

    iput-boolean v3, p1, Lj;->Q:Z

    goto/16 :goto_1

    .line 1089
    :cond_3a
    iput-object v7, p1, Lj;->w:Lo;

    .line 1090
    iput-object v7, p1, Lj;->y:Lj;

    .line 1091
    iput-object v7, p1, Lj;->v:Lv;

    .line 1092
    iput-object v7, p1, Lj;->x:Lv;

    goto/16 :goto_1

    :cond_3b
    move-object v0, v7

    goto/16 :goto_7

    :cond_3c
    move-object v0, v7

    goto/16 :goto_4

    .line 876
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 989
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lj;Z)V
    .locals 3

    .prologue
    .line 1190
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1191
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    .line 1193
    :cond_0
    iget v0, p1, Lj;->i:I

    if-gez v0, :cond_3

    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_4

    :cond_1
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lv;->r:Lj;

    invoke-virtual {p1, v0, v1}, Lj;->a(ILj;)V

    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1195
    :cond_3
    :goto_0
    iget-boolean v0, p1, Lj;->D:Z

    if-nez v0, :cond_6

    .line 1196
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1197
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1193
    :cond_4
    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    iget-object v1, p0, Lv;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lv;->r:Lj;

    invoke-virtual {p1, v0, v1}, Lj;->a(ILj;)V

    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    iget v1, p1, Lj;->i:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1199
    :cond_5
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1200
    const/4 v0, 0x1

    iput-boolean v0, p1, Lj;->o:Z

    .line 1201
    const/4 v0, 0x0

    iput-boolean v0, p1, Lj;->p:Z

    .line 1202
    if-eqz p2, :cond_6

    .line 1206
    invoke-direct {p0, p1}, Lv;->c(Lj;)V

    .line 1209
    :cond_6
    return-void
.end method

.method public final a(Ljava/lang/Runnable;Z)V
    .locals 2

    .prologue
    .line 1382
    if-nez p2, :cond_0

    .line 1383
    invoke-virtual {p0}, Lv;->e()V

    .line 1385
    :cond_0
    monitor-enter p0

    .line 1386
    :try_start_0
    iget-boolean v0, p0, Lv;->t:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lv;->h:Lo;

    if-nez v0, :cond_2

    .line 1387
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity has been destroyed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1397
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1389
    :cond_2
    :try_start_1
    iget-object v0, p0, Lv;->m:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    .line 1390
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv;->m:Ljava/util/ArrayList;

    .line 1392
    :cond_3
    iget-object v0, p0, Lv;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1393
    iget-object v0, p0, Lv;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 1394
    iget-object v0, p0, Lv;->h:Lo;

    iget-object v0, v0, Lo;->a:Landroid/os/Handler;

    iget-object v1, p0, Lv;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1395
    iget-object v0, p0, Lv;->h:Lo;

    iget-object v0, v0, Lo;->a:Landroid/os/Handler;

    iget-object v1, p0, Lv;->x:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1397
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 635
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 638
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    .line 639
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 640
    if-lez v4, :cond_e

    .line 641
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 642
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 643
    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 644
    :goto_0
    if-ge v2, v4, :cond_e

    .line 645
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 646
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 647
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 648
    if-eqz v0, :cond_d

    .line 649
    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mFragmentId=#"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->z:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " mContainerId=#"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->A:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " mTag="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->B:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->d:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, " mIndex="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->i:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, " mWho="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->j:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " mBackStackNesting="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->u:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mAdded="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->o:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mRemoving="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->p:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mResumed="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->q:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mFromLayout="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->r:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mInLayout="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->s:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mHidden="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->C:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mDetached="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->D:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mMenuVisible="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->G:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mHasMenu="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mRetainInstance="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->E:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mRetaining="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->F:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mUserVisibleHint="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lj;->N:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v5, v0, Lj;->v:Lv;

    if-eqz v5, :cond_0

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mFragmentManager="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->v:Lv;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_0
    iget-object v5, v0, Lj;->w:Lo;

    if-eqz v5, :cond_1

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mActivity="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->w:Lo;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_1
    iget-object v5, v0, Lj;->y:Lj;

    if-eqz v5, :cond_2

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mParentFragment="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->y:Lj;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_2
    iget-object v5, v0, Lj;->k:Landroid/os/Bundle;

    if-eqz v5, :cond_3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mArguments="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->k:Landroid/os/Bundle;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_3
    iget-object v5, v0, Lj;->g:Landroid/os/Bundle;

    if-eqz v5, :cond_4

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mSavedFragmentState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->g:Landroid/os/Bundle;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_4
    iget-object v5, v0, Lj;->h:Landroid/util/SparseArray;

    if-eqz v5, :cond_5

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mSavedViewState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->h:Landroid/util/SparseArray;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_5
    iget-object v5, v0, Lj;->l:Lj;

    if-eqz v5, :cond_6

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mTarget="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->l:Lj;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v5, " mTargetRequestCode="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->n:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    :cond_6
    iget v5, v0, Lj;->I:I

    if-eqz v5, :cond_7

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mNextAnim="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->I:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    :cond_7
    iget-object v5, v0, Lj;->J:Landroid/view/ViewGroup;

    if-eqz v5, :cond_8

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mContainer="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->J:Landroid/view/ViewGroup;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_8
    iget-object v5, v0, Lj;->K:Landroid/view/View;

    if-eqz v5, :cond_9

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mView="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->K:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_9
    iget-object v5, v0, Lj;->L:Landroid/view/View;

    if-eqz v5, :cond_a

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mInnerView="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->K:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_a
    iget-object v5, v0, Lj;->e:Landroid/view/View;

    if-eqz v5, :cond_b

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mAnimatingAway="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->e:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mStateAfterAnimating="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lj;->f:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    :cond_b
    iget-object v5, v0, Lj;->O:Lao;

    if-eqz v5, :cond_c

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "Loader Manager:"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v5, v0, Lj;->O:Lao;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, p2, p3, p4}, Lao;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_c
    iget-object v5, v0, Lj;->x:Lv;

    if-eqz v5, :cond_d

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Child "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lj;->x:Lv;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lj;->x:Lv;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, p2, p3, p4}, Lv;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 644
    :cond_d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 655
    :cond_e
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    .line 656
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 657
    if-lez v4, :cond_f

    .line 658
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 659
    :goto_1
    if-ge v2, v4, :cond_f

    .line 660
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 661
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 662
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lj;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 659
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 667
    :cond_f
    iget-object v0, p0, Lv;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_10

    .line 668
    iget-object v0, p0, Lv;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 669
    if-lez v4, :cond_10

    .line 670
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Fragments Created Menus:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 671
    :goto_2
    if-ge v2, v4, :cond_10

    .line 672
    iget-object v0, p0, Lv;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 673
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 674
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lj;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 671
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 679
    :cond_10
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    .line 680
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 681
    if-lez v4, :cond_11

    .line 682
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 683
    :goto_3
    if-ge v2, v4, :cond_11

    .line 684
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    .line 685
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 686
    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lc;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 687
    invoke-virtual {v0, v3, p3}, Lc;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 683
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 692
    :cond_11
    monitor-enter p0

    .line 693
    :try_start_0
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_12

    .line 694
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 695
    if-lez v3, :cond_12

    .line 696
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    .line 697
    :goto_4
    if-ge v2, v3, :cond_12

    .line 698
    iget-object v0, p0, Lv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    .line 699
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "  #"

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 700
    const-string v4, ": "

    invoke-virtual {p3, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 697
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 705
    :cond_12
    iget-object v0, p0, Lv;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lv;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_13

    .line 706
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 707
    iget-object v0, p0, Lv;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 709
    :cond_13
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 711
    iget-object v0, p0, Lv;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_14

    .line 712
    iget-object v0, p0, Lv;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 713
    if-lez v2, :cond_14

    .line 714
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Pending Actions:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 715
    :goto_5
    if-ge v1, v2, :cond_14

    .line 716
    iget-object v0, p0, Lv;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 717
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    .line 718
    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 715
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 709
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 723
    :cond_14
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 724
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lv;->h:Lo;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 725
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lv;->i:Ls;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 726
    iget-object v0, p0, Lv;->r:Lj;

    if-eqz v0, :cond_15

    .line 727
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lv;->r:Lj;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 729
    :cond_15
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lv;->g:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 730
    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lv;->j:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 731
    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lv;->t:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 732
    iget-boolean v0, p0, Lv;->s:Z

    if-eqz v0, :cond_16

    .line 733
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 734
    iget-boolean v0, p0, Lv;->s:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 736
    :cond_16
    iget-object v0, p0, Lv;->k:Ljava/lang/String;

    if-eqz v0, :cond_17

    .line 737
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNoTransactionsBecause="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 738
    iget-object v0, p0, Lv;->k:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 740
    :cond_17
    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_18

    .line 741
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mAvailIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 742
    iget-object v0, p0, Lv;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 744
    :cond_18
    return-void
.end method

.method public final a(Lo;Ls;Lj;)V
    .locals 2

    .prologue
    .line 1900
    iget-object v0, p0, Lv;->h:Lo;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1901
    :cond_0
    iput-object p1, p0, Lv;->h:Lo;

    .line 1902
    iput-object p2, p0, Lv;->i:Ls;

    .line 1903
    iput-object p3, p0, Lv;->r:Lj;

    .line 1904
    return-void
.end method

.method public final a(Landroid/view/Menu;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2015
    .line 2016
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v1, v2

    move v3, v2

    .line 2017
    :goto_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2018
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 2019
    if-eqz v0, :cond_0

    .line 2020
    iget-boolean v4, v0, Lj;->C:Z

    if-nez v4, :cond_3

    iget-object v4, v0, Lj;->x:Lv;

    if-eqz v4, :cond_3

    iget-object v0, v0, Lj;->x:Lv;

    invoke-virtual {v0, p1}, Lv;->a(Landroid/view/Menu;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    :goto_1
    if-eqz v0, :cond_0

    .line 2021
    const/4 v3, 0x1

    .line 2017
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v3, v2

    .line 2026
    :cond_2
    return v3

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public final a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1983
    .line 1984
    const/4 v1, 0x0

    .line 1985
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    move v3, v4

    move v2, v4

    .line 1986
    :goto_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 1987
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1988
    if-eqz v0, :cond_1

    .line 1989
    iget-boolean v5, v0, Lj;->C:Z

    if-nez v5, :cond_7

    iget-object v5, v0, Lj;->x:Lv;

    if-eqz v5, :cond_7

    iget-object v5, v0, Lj;->x:Lv;

    invoke-virtual {v5, p1, p2}, Lv;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v5

    or-int/lit8 v5, v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    .line 1990
    const/4 v2, 0x1

    .line 1991
    if-nez v1, :cond_0

    .line 1992
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1994
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move v0, v2

    .line 1986
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v4

    .line 2000
    :cond_3
    iget-object v0, p0, Lv;->q:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 2001
    :goto_2
    iget-object v0, p0, Lv;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 2002
    iget-object v0, p0, Lv;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 2003
    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2004
    :cond_4
    invoke-static {}, Lj;->w()V

    .line 2001
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2009
    :cond_6
    iput-object v1, p0, Lv;->q:Ljava/util/ArrayList;

    .line 2011
    return v2

    :cond_7
    move v5, v4

    goto :goto_1
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2030
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    move v1, v2

    .line 2031
    :goto_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2032
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 2033
    if-eqz v0, :cond_2

    .line 2034
    iget-boolean v4, v0, Lj;->C:Z

    if-nez v4, :cond_1

    iget-object v4, v0, Lj;->x:Lv;

    if-eqz v4, :cond_1

    iget-object v0, v0, Lj;->x:Lv;

    invoke-virtual {v0, p1}, Lv;->a(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    move v2, v3

    .line 2040
    :cond_0
    return v2

    :cond_1
    move v0, v2

    .line 2034
    goto :goto_1

    .line 2031
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;II)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1529
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1598
    :cond_0
    :goto_0
    return v3

    .line 1532
    :cond_1
    if-gez p2, :cond_3

    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_3

    .line 1533
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1534
    if-ltz v0, :cond_0

    .line 1537
    iget-object v1, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    .line 1538
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 1539
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 1540
    invoke-virtual {v0, v1, v3}, Lc;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 1541
    invoke-virtual {v0, v2, v4, v1, v3}, Lc;->a(ZLf;Landroid/util/SparseArray;Landroid/util/SparseArray;)Lf;

    :cond_2
    move v3, v2

    .line 1598
    goto :goto_0

    .line 1544
    :cond_3
    const/4 v0, -0x1

    .line 1545
    if-ltz p2, :cond_7

    .line 1548
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    .line 1549
    :goto_1
    if-ltz v1, :cond_5

    .line 1550
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    .line 1551
    if-ltz p2, :cond_4

    iget v0, v0, Lc;->g:I

    if-eq p2, v0, :cond_5

    .line 1555
    :cond_4
    add-int/lit8 v1, v1, -0x1

    .line 1558
    goto :goto_1

    .line 1559
    :cond_5
    if-ltz v1, :cond_0

    .line 1562
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_6

    .line 1563
    add-int/lit8 v1, v1, -0x1

    .line 1565
    :goto_2
    if-ltz v1, :cond_6

    .line 1566
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    .line 1567
    if-ltz p2, :cond_6

    iget v0, v0, Lc;->g:I

    if-ne p2, v0, :cond_6

    .line 1569
    add-int/lit8 v1, v1, -0x1

    .line 1570
    goto :goto_2

    :cond_6
    move v0, v1

    .line 1576
    :cond_7
    iget-object v1, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1579
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1581
    iget-object v1, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_3
    if-le v1, v0, :cond_8

    .line 1582
    iget-object v5, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1581
    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    .line 1584
    :cond_8
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .line 1585
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    .line 1586
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    move v1, v3

    .line 1587
    :goto_4
    if-gt v1, v7, :cond_9

    .line 1588
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    invoke-virtual {v0, v8, v9}, Lc;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 1587
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_9
    move-object v5, v4

    move v4, v3

    .line 1591
    :goto_5
    if-gt v4, v7, :cond_2

    .line 1592
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    if-ne v4, v7, :cond_a

    move v1, v2

    :goto_6
    invoke-virtual {v0, v1, v5, v8, v9}, Lc;->a(ZLf;Landroid/util/SparseArray;Landroid/util/SparseArray;)Lf;

    move-result-object v1

    .line 1591
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-object v5, v1

    goto :goto_5

    :cond_a
    move v1, v3

    .line 1592
    goto :goto_6
.end method

.method public final b(Landroid/view/Menu;)V
    .locals 3

    .prologue
    .line 2058
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 2059
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2060
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 2061
    if-eqz v0, :cond_0

    .line 2062
    iget-boolean v2, v0, Lj;->C:Z

    if-nez v2, :cond_0

    iget-object v2, v0, Lj;->x:Lv;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lj;->x:Lv;

    invoke-virtual {v0, p1}, Lv;->b(Landroid/view/Menu;)V

    .line 2059
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2066
    :cond_1
    return-void
.end method

.method public final b(Lj;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 835
    iget-boolean v0, p1, Lj;->M:Z

    if-eqz v0, :cond_0

    .line 836
    iget-boolean v0, p0, Lv;->o:Z

    if-eqz v0, :cond_1

    .line 838
    const/4 v0, 0x1

    iput-boolean v0, p0, Lv;->u:Z

    .line 844
    :cond_0
    :goto_0
    return-void

    .line 841
    :cond_1
    iput-boolean v3, p1, Lj;->M:Z

    .line 842
    iget v2, p0, Lv;->g:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    goto :goto_0
.end method

.method public final b(Lj;II)V
    .locals 2

    .prologue
    .line 1229
    iget-boolean v0, p1, Lj;->C:Z

    if-nez v0, :cond_2

    .line 1231
    const/4 v0, 0x1

    iput-boolean v0, p1, Lj;->C:Z

    .line 1232
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1233
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lv;->a(Lj;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1235
    if-eqz v0, :cond_0

    .line 1236
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1238
    :cond_0
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1240
    :cond_1
    iget-boolean v0, p1, Lj;->o:Z

    .line 1241
    invoke-static {}, Lj;->o()V

    .line 1245
    :cond_2
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p0}, Lv;->f()Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2044
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    move v1, v2

    .line 2045
    :goto_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2046
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 2047
    if-eqz v0, :cond_2

    .line 2048
    iget-boolean v4, v0, Lj;->C:Z

    if-nez v4, :cond_1

    iget-object v4, v0, Lj;->x:Lv;

    if-eqz v4, :cond_1

    iget-object v0, v0, Lj;->x:Lv;

    invoke-virtual {v0, p1}, Lv;->b(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    move v2, v3

    .line 2054
    :cond_0
    return v2

    :cond_1
    move v0, v2

    .line 2048
    goto :goto_1

    .line 2045
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final c(Lj;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1248
    iget-boolean v0, p1, Lj;->C:Z

    if-eqz v0, :cond_2

    .line 1250
    iput-boolean v2, p1, Lj;->C:Z

    .line 1251
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1252
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lv;->a(Lj;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1254
    if-eqz v0, :cond_0

    .line 1255
    iget-object v1, p1, Lj;->K:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1257
    :cond_0
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1259
    :cond_1
    iget-boolean v0, p1, Lj;->o:Z

    .line 1260
    invoke-static {}, Lj;->o()V

    .line 1264
    :cond_2
    return-void
.end method

.method final d()V
    .locals 2

    .prologue
    .line 1145
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1153
    :cond_0
    return-void

    .line 1147
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1148
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1149
    if-eqz v0, :cond_2

    .line 1150
    invoke-virtual {p0, v0}, Lv;->b(Lj;)V

    .line 1147
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final d(Lj;II)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1267
    iget-boolean v0, p1, Lj;->D:Z

    if-nez v0, :cond_1

    .line 1269
    iput-boolean v2, p1, Lj;->D:Z

    .line 1270
    iget-boolean v0, p1, Lj;->o:Z

    if-eqz v0, :cond_1

    .line 1272
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1273
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1276
    :cond_0
    iput-boolean v5, p1, Lj;->o:Z

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    .line 1280
    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    .line 1283
    :cond_1
    return-void
.end method

.method e()V
    .locals 3

    .prologue
    .line 1364
    iget-boolean v0, p0, Lv;->j:Z

    if-eqz v0, :cond_0

    .line 1365
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1368
    :cond_0
    iget-object v0, p0, Lv;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1369
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can not perform this action inside of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lv;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1372
    :cond_1
    return-void
.end method

.method public final e(Lj;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1286
    iget-boolean v0, p1, Lj;->D:Z

    if-eqz v0, :cond_2

    .line 1288
    iput-boolean v5, p1, Lj;->D:Z

    .line 1289
    iget-boolean v0, p1, Lj;->o:Z

    if-nez v0, :cond_2

    .line 1290
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    .line 1293
    :cond_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1294
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1296
    :cond_1
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1298
    const/4 v0, 0x1

    iput-boolean v0, p1, Lj;->o:Z

    .line 1299
    iget v2, p0, Lv;->g:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    .line 1305
    :cond_2
    return-void
.end method

.method public final f()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1460
    iget-boolean v1, p0, Lv;->o:Z

    if-eqz v1, :cond_0

    .line 1461
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursive entry to executePendingTransactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1464
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v3, p0, Lv;->h:Lo;

    iget-object v3, v3, Lo;->a:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v1, v3, :cond_1

    .line 1465
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v1, v2

    .line 1473
    :goto_0
    monitor-enter p0

    .line 1474
    :try_start_0
    iget-object v3, p0, Lv;->m:Ljava/util/ArrayList;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lv;->m:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_4

    .line 1475
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1496
    iget-boolean v0, p0, Lv;->u:Z

    if-eqz v0, :cond_9

    move v3, v2

    move v4, v2

    .line 1498
    :goto_1
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_8

    .line 1499
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1500
    if-eqz v0, :cond_3

    iget-object v5, v0, Lj;->O:Lao;

    if-eqz v5, :cond_3

    .line 1501
    iget-object v0, v0, Lj;->O:Lao;

    invoke-virtual {v0}, Lao;->a()Z

    move-result v0

    or-int/2addr v4, v0

    .line 1498
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1478
    :cond_4
    :try_start_1
    iget-object v1, p0, Lv;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1479
    iget-object v1, p0, Lv;->n:[Ljava/lang/Runnable;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lv;->n:[Ljava/lang/Runnable;

    array-length v1, v1

    if-ge v1, v3, :cond_6

    .line 1480
    :cond_5
    new-array v1, v3, [Ljava/lang/Runnable;

    iput-object v1, p0, Lv;->n:[Ljava/lang/Runnable;

    .line 1482
    :cond_6
    iget-object v1, p0, Lv;->m:Ljava/util/ArrayList;

    iget-object v4, p0, Lv;->n:[Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 1483
    iget-object v1, p0, Lv;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1484
    iget-object v1, p0, Lv;->h:Lo;

    iget-object v1, v1, Lo;->a:Landroid/os/Handler;

    iget-object v4, p0, Lv;->x:Ljava/lang/Runnable;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1485
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1487
    iput-boolean v0, p0, Lv;->o:Z

    move v1, v2

    .line 1488
    :goto_2
    if-ge v1, v3, :cond_7

    .line 1489
    iget-object v4, p0, Lv;->n:[Ljava/lang/Runnable;

    aget-object v4, v4, v1

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 1490
    iget-object v4, p0, Lv;->n:[Ljava/lang/Runnable;

    const/4 v5, 0x0

    aput-object v5, v4, v1

    .line 1488
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1485
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1492
    :cond_7
    iput-boolean v2, p0, Lv;->o:Z

    move v1, v0

    .line 1494
    goto :goto_0

    .line 1504
    :cond_8
    if-nez v4, :cond_9

    .line 1505
    iput-boolean v2, p0, Lv;->u:Z

    .line 1506
    invoke-virtual {p0}, Lv;->d()V

    .line 1509
    :cond_9
    return v1
.end method

.method final g()Landroid/os/Parcelable;
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1672
    invoke-virtual {p0}, Lv;->f()Z

    .line 1674
    sget-boolean v0, Lv;->l:Z

    if-eqz v0, :cond_0

    .line 1684
    iput-boolean v1, p0, Lv;->j:Z

    .line 1687
    :cond_0
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    .line 1782
    :cond_1
    :goto_0
    return-object v3

    .line 1692
    :cond_2
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1693
    new-array v7, v6, [Lad;

    move v5, v4

    move v2, v4

    .line 1695
    :goto_1
    if-ge v5, v6, :cond_9

    .line 1696
    iget-object v0, p0, Lv;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1697
    if-eqz v0, :cond_e

    .line 1698
    iget v2, v0, Lj;->i:I

    if-gez v2, :cond_3

    .line 1699
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failure saving state: active "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has cleared index: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Lj;->i:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lv;->a(Ljava/lang/RuntimeException;)V

    .line 1706
    :cond_3
    new-instance v2, Lad;

    invoke-direct {v2, v0}, Lad;-><init>(Lj;)V

    .line 1707
    aput-object v2, v7, v5

    .line 1709
    iget v8, v0, Lj;->d:I

    if-lez v8, :cond_7

    iget-object v8, v2, Lad;->j:Landroid/os/Bundle;

    if-nez v8, :cond_7

    .line 1710
    invoke-direct {p0, v0}, Lv;->e(Lj;)Landroid/os/Bundle;

    move-result-object v8

    iput-object v8, v2, Lad;->j:Landroid/os/Bundle;

    .line 1712
    iget-object v8, v0, Lj;->l:Lj;

    if-eqz v8, :cond_8

    .line 1713
    iget-object v8, v0, Lj;->l:Lj;

    iget v8, v8, Lj;->i:I

    if-gez v8, :cond_4

    .line 1714
    new-instance v8, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failure saving state: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lj;->l:Lj;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lv;->a(Ljava/lang/RuntimeException;)V

    .line 1718
    :cond_4
    iget-object v8, v2, Lad;->j:Landroid/os/Bundle;

    if-nez v8, :cond_5

    .line 1719
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    iput-object v8, v2, Lad;->j:Landroid/os/Bundle;

    .line 1721
    :cond_5
    iget-object v8, v2, Lad;->j:Landroid/os/Bundle;

    const-string v9, "android:target_state"

    iget-object v10, v0, Lj;->l:Lj;

    iget v11, v10, Lj;->i:I

    if-gez v11, :cond_6

    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Fragment "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is not currently in the FragmentManager"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v11}, Lv;->a(Ljava/lang/RuntimeException;)V

    :cond_6
    iget v10, v10, Lj;->i:I

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1723
    iget v8, v0, Lj;->n:I

    if-eqz v8, :cond_8

    .line 1724
    iget-object v2, v2, Lad;->j:Landroid/os/Bundle;

    const-string v8, "android:target_req_state"

    iget v0, v0, Lj;->n:I

    invoke-virtual {v2, v8, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v0, v1

    .line 1734
    :goto_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v2, v0

    goto/16 :goto_1

    .line 1731
    :cond_7
    iget-object v0, v0, Lj;->g:Landroid/os/Bundle;

    iput-object v0, v2, Lad;->j:Landroid/os/Bundle;

    :cond_8
    move v0, v1

    goto :goto_2

    .line 1739
    :cond_9
    if-eqz v2, :cond_1

    .line 1748
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    .line 1749
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1750
    if-lez v5, :cond_b

    .line 1751
    new-array v1, v5, [I

    move v2, v4

    .line 1752
    :goto_3
    if-ge v2, v5, :cond_c

    .line 1753
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    iget v0, v0, Lj;->i:I

    aput v0, v1, v2

    .line 1754
    aget v0, v1, v2

    if-gez v0, :cond_a

    .line 1755
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Failure saving state: active "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " has cleared index: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v8, v1, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lv;->a(Ljava/lang/RuntimeException;)V

    .line 1759
    :cond_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_b
    move-object v1, v3

    .line 1766
    :cond_c
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_d

    .line 1767
    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1768
    if-lez v5, :cond_d

    .line 1769
    new-array v3, v5, [Lg;

    move v2, v4

    .line 1770
    :goto_4
    if-ge v2, v5, :cond_d

    .line 1771
    new-instance v4, Lg;

    iget-object v0, p0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc;

    invoke-direct {v4, v0}, Lg;-><init>(Lc;)V

    aput-object v4, v3, v2

    .line 1772
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 1778
    :cond_d
    new-instance v0, Laa;

    invoke-direct {v0}, Laa;-><init>()V

    .line 1779
    iput-object v7, v0, Laa;->a:[Lad;

    .line 1780
    iput-object v1, v0, Laa;->b:[I

    .line 1781
    iput-object v3, v0, Laa;->c:[Lg;

    move-object v3, v0

    .line 1782
    goto/16 :goto_0

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method

.method public final h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1911
    iput-boolean v1, p0, Lv;->j:Z

    .line 1912
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lv;->a(IZ)V

    .line 1913
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1916
    iput-boolean v1, p0, Lv;->j:Z

    .line 1917
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lv;->a(IZ)V

    .line 1918
    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1921
    iput-boolean v1, p0, Lv;->j:Z

    .line 1922
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lv;->a(IZ)V

    .line 1923
    return-void
.end method

.method public final k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1926
    iput-boolean v1, p0, Lv;->j:Z

    .line 1927
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v1}, Lv;->a(IZ)V

    .line 1928
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 1931
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lv;->a(IZ)V

    .line 1932
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 1938
    const/4 v0, 0x1

    iput-boolean v0, p0, Lv;->j:Z

    .line 1940
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lv;->a(IZ)V

    .line 1941
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1952
    const/4 v0, 0x1

    iput-boolean v0, p0, Lv;->t:Z

    .line 1953
    invoke-virtual {p0}, Lv;->f()Z

    .line 1954
    invoke-virtual {p0, v2, v2}, Lv;->a(IZ)V

    .line 1955
    iput-object v1, p0, Lv;->h:Lo;

    .line 1956
    iput-object v1, p0, Lv;->i:Ls;

    .line 1957
    iput-object v1, p0, Lv;->r:Lj;

    .line 1958
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 1972
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1973
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1974
    iget-object v0, p0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1975
    if-eqz v0, :cond_0

    .line 1976
    invoke-virtual {v0}, Lj;->onLowMemory()V

    iget-object v2, v0, Lj;->x:Lv;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lj;->x:Lv;

    invoke-virtual {v0}, Lv;->o()V

    .line 1973
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1980
    :cond_1
    return-void
.end method

.method public final onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2110
    const-string v0, "fragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2198
    :goto_0
    return-object v0

    .line 2114
    :cond_0
    const-string v0, "class"

    invoke-interface {p3, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2115
    sget-object v4, Lz;->a:[I

    invoke-virtual {p2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 2116
    if-nez v0, :cond_d

    .line 2117
    invoke-virtual {v4, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 2119
    :goto_1
    invoke-virtual {v4, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    .line 2120
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 2121
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 2123
    iget-object v0, p0, Lv;->h:Lo;

    invoke-static {v0, v6}, Lj;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2126
    goto :goto_0

    .line 2129
    :cond_1
    if-eq v7, v5, :cond_2

    invoke-virtual {p0, v7}, Lv;->a(I)Lj;

    move-result-object v1

    .line 2140
    :cond_2
    if-nez v1, :cond_3

    if-eqz v8, :cond_3

    .line 2141
    invoke-virtual {p0, v8}, Lv;->a(Ljava/lang/String;)Lj;

    move-result-object v1

    .line 2143
    :cond_3
    if-nez v1, :cond_4

    .line 2144
    invoke-virtual {p0, v3}, Lv;->a(I)Lj;

    move-result-object v1

    .line 2147
    :cond_4
    if-nez v1, :cond_7

    .line 2151
    invoke-static {p2, v6}, Lj;->a(Landroid/content/Context;Ljava/lang/String;)Lj;

    move-result-object v1

    .line 2152
    iput-boolean v2, v1, Lj;->r:Z

    .line 2153
    if-eqz v7, :cond_6

    move v0, v7

    :goto_2
    iput v0, v1, Lj;->z:I

    .line 2154
    iput v3, v1, Lj;->A:I

    .line 2155
    iput-object v8, v1, Lj;->B:Ljava/lang/String;

    .line 2156
    iput-boolean v2, v1, Lj;->s:Z

    .line 2157
    iput-object p0, v1, Lj;->v:Lv;

    .line 2158
    iget-object v0, p0, Lv;->h:Lo;

    iget-object v0, v1, Lj;->g:Landroid/os/Bundle;

    iput-boolean v2, v1, Lj;->H:Z

    .line 2159
    invoke-virtual {p0, v1, v2}, Lv;->a(Lj;Z)V

    .line 2182
    :cond_5
    :goto_3
    iget v0, p0, Lv;->g:I

    if-gtz v0, :cond_9

    iget-boolean v0, v1, Lj;->r:Z

    if-eqz v0, :cond_9

    move-object v0, p0

    move v4, v3

    move v5, v3

    .line 2183
    invoke-virtual/range {v0 .. v5}, Lv;->a(Lj;IIIZ)V

    .line 2188
    :goto_4
    iget-object v0, v1, Lj;->K:Landroid/view/View;

    if-nez v0, :cond_a

    .line 2189
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v0, v3

    .line 2153
    goto :goto_2

    .line 2161
    :cond_7
    iget-boolean v0, v1, Lj;->s:Z

    if-eqz v0, :cond_8

    .line 2164
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Duplicate id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", or parent id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with another fragment for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2171
    :cond_8
    iput-boolean v2, v1, Lj;->s:Z

    .line 2175
    iget-boolean v0, v1, Lj;->F:Z

    if-nez v0, :cond_5

    .line 2176
    iget-object v0, p0, Lv;->h:Lo;

    iget-object v0, v1, Lj;->g:Landroid/os/Bundle;

    iput-boolean v2, v1, Lj;->H:Z

    goto/16 :goto_3

    .line 2185
    :cond_9
    invoke-direct {p0, v1}, Lv;->c(Lj;)V

    goto :goto_4

    .line 2192
    :cond_a
    if-eqz v7, :cond_b

    .line 2193
    iget-object v0, v1, Lj;->K:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 2195
    :cond_b
    iget-object v0, v1, Lj;->K:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_c

    .line 2196
    iget-object v0, v1, Lj;->K:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2198
    :cond_c
    iget-object v0, v1, Lj;->K:Landroid/view/View;

    goto/16 :goto_0

    :cond_d
    move-object v6, v0

    goto/16 :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 620
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 621
    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    iget-object v1, p0, Lv;->r:Lj;

    if-eqz v1, :cond_0

    .line 625
    iget-object v1, p0, Lv;->r:Lj;

    invoke-static {v1, v0}, La;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    .line 629
    :goto_0
    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 627
    :cond_0
    iget-object v1, p0, Lv;->h:Lo;

    invoke-static {v1, v0}, La;->a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V

    goto :goto_0
.end method
