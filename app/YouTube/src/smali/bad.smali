.class public Lbad;
.super Lbhz;
.source "SourceFile"


# instance fields
.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/ListView;

.field private h:Landroid/widget/TextView;

.field private i:Lfsi;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:Landroid/widget/Button;

.field private o:Landroid/widget/Button;

.field private p:Lcnu;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/Button;

.field private s:Landroid/widget/Button;

.field private t:Lgnd;

.field private u:Lezj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lbhz;-><init>()V

    .line 508
    return-void
.end method

.method static synthetic a(Lbad;)Lezj;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbad;->u:Lezj;

    return-object v0
.end method

.method static synthetic a(Lbad;J)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbad;->p:Lcnu;

    invoke-virtual {v0, p1, p2}, Lcnu;->a(J)V

    invoke-direct {p0}, Lbad;->f()V

    return-void
.end method

.method static synthetic b(Lbad;)Lgnd;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbad;->t:Lgnd;

    return-object v0
.end method

.method static synthetic c(Lbad;)Ljava/util/List;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lbad;->t:Lgnd;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbad;->u:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v4

    iget-object v0, p0, Lbad;->t:Lgnd;

    invoke-interface {v0}, Lgnd;->i()Ljava/util/Map;

    move-result-object v6

    new-instance v7, Ljava/util/TreeMap;

    invoke-direct {v7}, Ljava/util/TreeMap;-><init>()V

    iget-object v0, p0, Lbad;->t:Lgnd;

    invoke-interface {v0}, Lgnd;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->j:Ljava/lang/String;

    new-instance v3, Lbap;

    invoke-direct {v3, v0, v2}, Lbap;-><init>(Lgmb;Ljava/util/List;)V

    invoke-virtual {v7, v1, v3}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhpt;

    iget-object v10, v1, Lhpt;->b:[Lhps;

    array-length v10, v10

    if-lez v10, :cond_2

    new-instance v10, Lbam;

    invoke-direct {v10, v1, v4, v5}, Lbam;-><init>(Lhpt;J)V

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v9, v1, Lgcd;->j:Ljava/lang/String;

    new-instance v10, Lbap;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v1, v2

    :goto_3
    invoke-direct {v10, v0, v1}, Lbap;-><init>(Lgmb;Ljava/util/List;)V

    invoke-virtual {v7, v9, v10}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    move-object v1, v3

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_0
.end method

.method static synthetic d(Lbad;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbad;->h:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lbad;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbad;->g:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic f(Lbad;)Lfsi;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbad;->i:Lfsi;

    return-object v0
.end method

.method private f()V
    .locals 8

    .prologue
    .line 185
    iget-object v0, p0, Lbad;->p:Lcnu;

    invoke-virtual {v0}, Lcnu;->b()J

    move-result-wide v0

    .line 186
    iget-object v2, p0, Lbad;->u:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 187
    const-wide/16 v2, 0x9

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    div-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 188
    iget-object v2, p0, Lbad;->q:Landroid/widget/TextView;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, ">="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "min"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 95
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 98
    invoke-static {p0}, La;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-virtual {p0}, Lbad;->finish()V

    .line 102
    :cond_0
    const v0, 0x7f040051

    invoke-virtual {p0, v0}, Lbad;->setContentView(I)V

    .line 103
    const v0, 0x7f08016d

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbad;->e:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f080176

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbad;->f:Landroid/widget/TextView;

    .line 107
    invoke-virtual {p0}, Lbad;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v1, v0, Lckz;->a:Letc;

    .line 108
    invoke-virtual {p0}, Lbad;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v2

    .line 109
    invoke-virtual {v2}, Lari;->aD()Lcst;

    move-result-object v3

    .line 110
    invoke-virtual {v1}, Letc;->f()Lezj;

    move-result-object v0

    iput-object v0, p0, Lbad;->u:Lezj;

    .line 112
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lbad;->i:Lfsi;

    .line 113
    iget-object v0, p0, Lbad;->i:Lfsi;

    const-class v1, Lbap;

    new-instance v4, Lbar;

    invoke-direct {v4, p0}, Lbar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v4}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 117
    const v0, 0x7f080175

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbad;->g:Landroid/widget/ListView;

    .line 118
    const v0, 0x1020004

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbad;->h:Landroid/widget/TextView;

    .line 119
    iget-object v0, p0, Lbad;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lbad;->i:Lfsi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 121
    const v0, 0x7f08016e

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbad;->j:Landroid/widget/Button;

    .line 122
    iget-object v0, p0, Lbad;->j:Landroid/widget/Button;

    new-instance v1, Lbae;

    invoke-direct {v1, p0}, Lbae;-><init>(Lbad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    const v0, 0x7f08016f

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbad;->k:Landroid/widget/Button;

    .line 129
    iget-object v0, p0, Lbad;->k:Landroid/widget/Button;

    new-instance v1, Lbaf;

    invoke-direct {v1, p0}, Lbaf;-><init>(Lbad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    const v0, 0x7f080170

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbad;->l:Landroid/widget/Button;

    .line 137
    iget-object v0, p0, Lbad;->l:Landroid/widget/Button;

    new-instance v1, Lbag;

    invoke-direct {v1, p0}, Lbag;-><init>(Lbad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    const v0, 0x7f080171

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbad;->o:Landroid/widget/Button;

    .line 144
    iget-object v0, p0, Lbad;->o:Landroid/widget/Button;

    new-instance v1, Lbah;

    invoke-direct {v1, p0}, Lbah;-><init>(Lbad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    invoke-virtual {v2}, Lari;->G()Lcnu;

    move-result-object v0

    iput-object v0, p0, Lbad;->p:Lcnu;

    .line 153
    const v0, 0x7f080172

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbad;->q:Landroid/widget/TextView;

    .line 154
    const v0, 0x7f080174

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbad;->r:Landroid/widget/Button;

    .line 155
    iget-object v0, p0, Lbad;->r:Landroid/widget/Button;

    new-instance v1, Lbai;

    invoke-direct {v1, p0}, Lbai;-><init>(Lbad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v0, 0x7f080173

    invoke-virtual {p0, v0}, Lbad;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbad;->s:Landroid/widget/Button;

    .line 162
    iget-object v0, p0, Lbad;->s:Landroid/widget/Button;

    new-instance v1, Lbaj;

    invoke-direct {v1, p0}, Lbaj;-><init>(Lbad;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    invoke-interface {v3}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    invoke-interface {v3}, Lgix;->d()Lgit;

    move-result-object v1

    .line 172
    iget-object v3, p0, Lbad;->e:Landroid/widget/TextView;

    const-string v4, "Signed in as "

    invoke-virtual {v1}, Lgit;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {v2}, Lari;->O()Lgng;

    move-result-object v0

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    iput-object v0, p0, Lbad;->t:Lgnd;

    .line 181
    :goto_1
    invoke-direct {p0}, Lbad;->f()V

    .line 182
    return-void

    .line 172
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_2
    iget-object v0, p0, Lbad;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lbad;->j:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 177
    iget-object v0, p0, Lbad;->k:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 178
    iget-object v0, p0, Lbad;->l:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 179
    iget-object v0, p0, Lbad;->o:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 364
    invoke-super {p0}, Lbhz;->onResume()V

    .line 366
    new-instance v0, Lbal;

    invoke-direct {v0, p0}, Lbal;-><init>(Lbad;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lbal;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 368
    invoke-virtual {p0}, Lbad;->d()Lkm;

    move-result-object v0

    const v1, 0x7f0901d3

    invoke-virtual {v0, v1}, Lkm;->b(I)V

    .line 371
    iget-object v0, p0, Lbad;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lbad;->h:Landroid/widget/TextView;

    const-string v1, "Loading..."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    return-void
.end method
