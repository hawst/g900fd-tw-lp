.class public final Lgjh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Iterable;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Landroid/net/Uri;

.field public final e:Landroid/net/Uri;

.field public final f:Ljava/util/List;


# direct methods
.method public constructor <init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lgjh;->a:I

    .line 65
    iput p2, p0, Lgjh;->b:I

    .line 66
    iput p3, p0, Lgjh;->c:I

    .line 67
    iput-object p4, p0, Lgjh;->d:Landroid/net/Uri;

    .line 68
    iput-object p5, p0, Lgjh;->e:Landroid/net/Uri;

    .line 69
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgjh;->f:Ljava/util/List;

    .line 70
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lgji;

    invoke-direct {v0}, Lgji;-><init>()V

    iget v1, p0, Lgjh;->a:I

    iput v1, v0, Lgji;->a:I

    iget v1, p0, Lgjh;->b:I

    iput v1, v0, Lgji;->b:I

    iget v1, p0, Lgjh;->c:I

    iput v1, v0, Lgji;->c:I

    iget-object v1, p0, Lgjh;->d:Landroid/net/Uri;

    iput-object v1, v0, Lgji;->d:Landroid/net/Uri;

    iget-object v1, p0, Lgjh;->e:Landroid/net/Uri;

    iput-object v1, v0, Lgji;->e:Landroid/net/Uri;

    iget-object v1, p0, Lgjh;->f:Ljava/util/List;

    invoke-virtual {v0, v1}, Lgji;->a(Ljava/util/List;)Lgji;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 97
    if-ne p0, p1, :cond_1

    .line 98
    const/4 v0, 0x1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 100
    :cond_1
    instance-of v1, p1, Lgjh;

    if-eqz v1, :cond_0

    .line 103
    check-cast p1, Lgjh;

    .line 105
    iget v1, p0, Lgjh;->a:I

    iget v2, p1, Lgjh;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lgjh;->b:I

    iget v2, p1, Lgjh;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lgjh;->c:I

    iget v2, p1, Lgjh;->c:I

    if-ne v1, v2, :cond_0

    .line 111
    iget-object v1, p0, Lgjh;->d:Landroid/net/Uri;

    if-nez v1, :cond_4

    .line 112
    iget-object v1, p1, Lgjh;->d:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 119
    :cond_2
    iget-object v1, p0, Lgjh;->e:Landroid/net/Uri;

    if-nez v1, :cond_5

    .line 120
    iget-object v1, p1, Lgjh;->e:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 127
    :cond_3
    iget-object v0, p0, Lgjh;->f:Ljava/util/List;

    iget-object v1, p1, Lgjh;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 115
    :cond_4
    iget-object v1, p0, Lgjh;->d:Landroid/net/Uri;

    iget-object v2, p1, Lgjh;->d:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 123
    :cond_5
    iget-object v1, p0, Lgjh;->e:Landroid/net/Uri;

    iget-object v2, p1, Lgjh;->e:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lgjh;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 132
    iget v0, p0, Lgjh;->a:I

    iget v1, p0, Lgjh;->b:I

    iget v2, p0, Lgjh;->c:I

    iget-object v3, p0, Lgjh;->d:Landroid/net/Uri;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lgjh;->e:Landroid/net/Uri;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lgjh;->f:Ljava/util/List;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x77

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "{totalResults: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ", elementsPerPage: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startIndex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", previousUri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nextUri: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", entries: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
