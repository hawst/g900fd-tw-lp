.class public final Lgzv;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9726
    invoke-direct {p0}, Lidf;-><init>()V

    .line 9735
    const/4 v0, 0x0

    iput v0, p0, Lgzv;->a:I

    .line 9726
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 9772
    const/4 v0, 0x0

    .line 9773
    iget v1, p0, Lgzv;->a:I

    if-eqz v1, :cond_0

    .line 9774
    const/4 v0, 0x1

    iget v1, p0, Lgzv;->a:I

    .line 9775
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9777
    :cond_0
    iget-object v1, p0, Lgzv;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9778
    iput v0, p0, Lgzv;->J:I

    .line 9779
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 9722
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzv;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzv;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzv;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lgzv;->a:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lgzv;->a:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 9763
    iget v0, p0, Lgzv;->a:I

    if-eqz v0, :cond_0

    .line 9764
    const/4 v0, 0x1

    iget v1, p0, Lgzv;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 9766
    :cond_0
    iget-object v0, p0, Lgzv;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 9768
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9746
    if-ne p1, p0, :cond_1

    .line 9750
    :cond_0
    :goto_0
    return v0

    .line 9747
    :cond_1
    instance-of v2, p1, Lgzv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 9748
    :cond_2
    check-cast p1, Lgzv;

    .line 9749
    iget v2, p0, Lgzv;->a:I

    iget v3, p1, Lgzv;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzv;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzv;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 9750
    goto :goto_0

    .line 9749
    :cond_4
    iget-object v2, p0, Lgzv;->I:Ljava/util/List;

    iget-object v3, p1, Lgzv;->I:Ljava/util/List;

    .line 9750
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 9754
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9756
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lgzv;->a:I

    add-int/2addr v0, v1

    .line 9757
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lgzv;->I:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 9758
    return v0

    .line 9757
    :cond_0
    iget-object v0, p0, Lgzv;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0
.end method
