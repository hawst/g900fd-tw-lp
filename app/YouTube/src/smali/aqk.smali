.class public final Laqk;
.super Laly;
.source "SourceFile"

# interfaces
.implements Laqa;


# instance fields
.field private c:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Lapz;Lait;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p2}, Laly;-><init>(Lait;)V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iget-object v1, p1, Lapz;->a:Laqb;

    const-string v0, "listener cannot be null"

    invoke-static {p0, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqa;

    iput-object v0, v1, Laqb;->a:Laqa;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 63
    iput-object p1, p0, Laqk;->c:Landroid/view/Surface;

    .line 64
    iget-object v0, p0, Laqk;->b:Lged;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Laqk;->b:Lged;

    invoke-interface {v0}, Lged;->a()V

    .line 67
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public final e()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Laqk;->c:Landroid/view/Surface;

    return-object v0
.end method

.method public final f()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Laqk;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Laqk;->c:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 54
    :cond_0
    return-void
.end method

.method public final g_()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Laqk;->b:Lged;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Laqk;->b:Lged;

    invoke-interface {v0}, Lged;->b()V

    .line 74
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Laqk;->c:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h_()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Laqk;->b:Lged;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Laqk;->b:Lged;

    invoke-interface {v0}, Lged;->b()V

    .line 81
    :cond_0
    return-void
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x2

    return v0
.end method

.method public final i_()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Laqk;->c:Landroid/view/Surface;

    .line 86
    iget-object v0, p0, Laqk;->b:Lged;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Laqk;->b:Lged;

    invoke-interface {v0}, Lged;->c()V

    .line 89
    :cond_0
    return-void
.end method

.method public final j()Landroid/view/View;
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return-object v0
.end method
