.class public Lcek;
.super Lcby;
.source "SourceFile"


# instance fields
.field public final f:Landroid/content/res/Resources;

.field public final g:Landroid/widget/LinearLayout;

.field public final h:Landroid/widget/RelativeLayout;

.field private final i:Lboi;

.field private j:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;ILfhz;Lboi;Lfdw;Lfrz;)V
    .locals 8

    .prologue
    .line 49
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move-object v4, p3

    move v5, p4

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcby;-><init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;ILfdw;Lfrz;)V

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcek;->f:Landroid/content/res/Resources;

    .line 58
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcek;->j:Lfsj;

    .line 59
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcek;->i:Lboi;

    .line 61
    iget-object v0, p0, Lcby;->c:Landroid/view/View;

    const v1, 0x7f080138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcek;->g:Landroid/widget/LinearLayout;

    .line 62
    iget-object v0, p0, Lcek;->g:Landroid/widget/LinearLayout;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcek;->h:Landroid/widget/RelativeLayout;

    .line 63
    return-void
.end method


# virtual methods
.method public final a(Lfsg;Lfkc;)Landroid/view/View;
    .locals 5

    .prologue
    .line 67
    invoke-super {p0, p1, p2}, Lcby;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 69
    invoke-virtual {p0, p1}, Lcek;->a(Lfsg;)V

    .line 70
    invoke-virtual {p2}, Lfkc;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcek;->a(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p2, Lfkc;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p2, Lfkc;->a:Lhhn;

    iget-object v0, v0, Lhhn;->h:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfkc;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p2, Lfkc;->e:Ljava/lang/CharSequence;

    iget-object v1, p2, Lfkc;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->d:Lhgz;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->d:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkc;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfkc;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->l:Lhgz;

    if-eqz v1, :cond_6

    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->l:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkc;->b:Ljava/lang/CharSequence;

    :cond_2
    :goto_0
    iget-object v1, p2, Lfkc;->b:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p2, Lfkc;->c:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    iget-object v4, p2, Lfkc;->c:Ljava/lang/CharSequence;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, " \u00b7 "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p2, Lfkc;->c:Ljava/lang/CharSequence;

    :cond_3
    :goto_1
    iget-object v1, p2, Lfkc;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcek;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p2, Lfkc;->f:Lfnc;

    if-nez v0, :cond_4

    new-instance v0, Lfnc;

    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p2, Lfkc;->f:Lfnc;

    :cond_4
    iget-object v0, p2, Lfkc;->f:Lfnc;

    iget-object v1, p2, Lfkc;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_5

    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->f:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkc;->d:Ljava/lang/CharSequence;

    :cond_5
    iget-object v1, p2, Lfkc;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcek;->a(Lfnc;Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcek;->j:Lfsj;

    invoke-interface {v0}, Lfsj;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcek;->i:Lboi;

    iget-object v2, p0, Lcby;->e:Landroid/view/View;

    invoke-static {v0, v1, v2, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Lcek;->j:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 71
    :cond_6
    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->e:Lhgz;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lfkc;->a:Lhhn;

    iget-object v1, v1, Lhhn;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkc;->b:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_7
    iput-object v1, p2, Lfkc;->c:Ljava/lang/CharSequence;

    goto :goto_1
.end method

.method public bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    check-cast p2, Lfkc;

    invoke-virtual {p0, p1, p2}, Lcek;->a(Lfsg;Lfkc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    check-cast p2, Lfkc;

    invoke-virtual {p0, p1, p2}, Lcek;->a(Lfsg;Lfkc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    check-cast p2, Lfkc;

    invoke-virtual {p0, p1, p2}, Lcek;->a(Lfsg;Lfkc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lfsg;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 82
    iget-object v0, p0, Lcek;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 83
    iget-object v1, p0, Lcek;->f:Landroid/content/res/Resources;

    const v2, 0x7f0b0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 86
    iget-object v1, p0, Lcby;->a:Landroid/content/Context;

    invoke-static {v1, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcek;->g:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 88
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 93
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v1, p0, Lcek;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 91
    iget-object v1, p0, Lcek;->f:Landroid/content/res/Resources;

    const v2, 0x7f0a008e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method
