.class public final enum Lghn;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lghn;

.field public static final enum b:Lghn;

.field public static final enum c:Lghn;

.field public static final enum d:Lghn;

.field private static final synthetic f:[Lghn;


# instance fields
.field final e:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lghn;

    const-string v1, "PRODUCTION"

    const-string v2, "https://www.googleapis.com"

    invoke-direct {v0, v1, v3, v2}, Lghn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lghn;->a:Lghn;

    .line 17
    new-instance v0, Lghn;

    const-string v1, "STAGING"

    const-string v2, "https://www-googleapis-staging.sandbox.google.com"

    invoke-direct {v0, v1, v4, v2}, Lghn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lghn;->b:Lghn;

    .line 18
    new-instance v0, Lghn;

    const-string v1, "TEST"

    const-string v2, "https://www-googleapis-test.sandbox.google.com"

    invoke-direct {v0, v1, v5, v2}, Lghn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lghn;->c:Lghn;

    .line 19
    new-instance v0, Lghn;

    const-string v1, "PPG"

    const-string v2, "http://127.0.0.1:8787"

    invoke-direct {v0, v1, v6, v2}, Lghn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lghn;->d:Lghn;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lghn;

    sget-object v1, Lghn;->a:Lghn;

    aput-object v1, v0, v3

    sget-object v1, Lghn;->b:Lghn;

    aput-object v1, v0, v4

    sget-object v1, Lghn;->c:Lghn;

    aput-object v1, v0, v5

    sget-object v1, Lghn;->d:Lghn;

    aput-object v1, v0, v6

    sput-object v0, Lghn;->f:[Lghn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lghn;->e:Landroid/net/Uri;

    .line 39
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public static a(Landroid/content/SharedPreferences;)Z
    .locals 2

    .prologue
    .line 66
    invoke-static {p0}, Lghn;->b(Landroid/content/SharedPreferences;)Lghn;

    move-result-object v0

    sget-object v1, Lghn;->a:Lghn;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/SharedPreferences;)Lghn;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 73
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string v0, "ApiaryHostSelection"

    .line 77
    invoke-static {}, Lghn;->values()[Lghn;

    move-result-object v1

    aget-object v1, v1, v5

    invoke-virtual {v1}, Lghn;->toString()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 81
    :try_start_0
    invoke-static {v0}, Lghn;->valueOf(Ljava/lang/String;)Lghn;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    .line 83
    :catch_0
    move-exception v1

    const-string v1, "Bogus value in shared preferences for key ApiaryHostSelection value "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " returning default value."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 86
    invoke-static {}, Lghn;->values()[Lghn;

    move-result-object v0

    aget-object v0, v0, v5

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lghn;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lghn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lghn;

    return-object v0
.end method

.method public static values()[Lghn;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lghn;->f:[Lghn;

    invoke-virtual {v0}, [Lghn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lghn;

    return-object v0
.end method
