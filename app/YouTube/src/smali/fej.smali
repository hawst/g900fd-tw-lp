.class public final Lfej;
.super Lww;
.source "SourceFile"


# instance fields
.field private final b:Lghr;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lghr;)V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lfek;

    invoke-direct {v0, p1}, Lfek;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, v0, p2}, Lfej;-><init>(Ljava/util/concurrent/Executor;Lghr;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Lghr;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lww;-><init>(Ljava/util/concurrent/Executor;)V

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghr;

    iput-object v0, p0, Lfej;->b:Lghr;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lwp;Lwt;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Lww;->a(Lwp;Lwt;Ljava/lang/Runnable;)V

    .line 47
    instance-of v0, p1, Lfta;

    if-eqz v0, :cond_1

    check-cast p1, Lfta;

    iget-object v0, p1, Lfta;->j:Lftd;

    invoke-interface {v0}, Lftd;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfej;->b:Lghr;

    invoke-interface {v0}, Lghr;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lfta;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lfej;->b:Lghr;

    invoke-interface {v0}, Lghr;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p2, Lwt;->b:Lwd;

    if-eqz v0, :cond_1

    new-instance v0, Lwm;

    iget-object v1, p2, Lwt;->b:Lwd;

    iget-object v1, v1, Lwd;->a:[B

    iget-object v2, p2, Lwt;->b:Lwd;

    iget-object v2, v2, Lwd;->f:Ljava/util/Map;

    invoke-direct {v0, v1, v2}, Lwm;-><init>([BLjava/util/Map;)V

    const-string v1, "Logging response for YouTube API call."

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lfta;->b(Lwm;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :cond_1
    return-void
.end method
