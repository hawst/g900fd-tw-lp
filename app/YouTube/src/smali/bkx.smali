.class public abstract Lbkx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lbkz;

.field private b:Ldzz;

.field private c:Ljava/lang/Object;

.field private synthetic d:Lbku;


# direct methods
.method constructor <init>(Lbku;Lbkz;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lbkx;->d:Lbku;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p2, p0, Lbkx;->a:Lbkz;

    .line 142
    return-void
.end method

.method private final declared-synchronized b()V
    .locals 2

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbkx;->a:Lbkz;

    iget-object v1, v0, Lbkz;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lbkz;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 208
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbkx;->b:Ldzz;

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lbkx;->c:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()Ldzz;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Lbkx;->a:Lbkz;

    iget-object v2, v0, Lbkz;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 220
    :goto_0
    if-eqz v0, :cond_1

    .line 222
    :try_start_0
    new-instance v2, Ldzz;

    invoke-direct {v2}, Ldzz;-><init>()V

    invoke-virtual {v2, v0}, Ldzz;->b([B)Lida;

    move-result-object v0

    check-cast v0, Ldzz;
    :try_end_0
    .catch Licz; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_1
    return-object v0

    .line 219
    :cond_0
    iget-object v0, v0, Lbkz;->a:Ljava/io/File;

    invoke-static {v0}, La;->c(Ljava/io/File;)[B

    move-result-object v0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lbkx;->b()V

    :cond_1
    move-object v0, v1

    .line 230
    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lbkx;->b:Ldzz;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbkx;->c:Ljava/lang/Object;

    if-nez v1, :cond_3

    .line 178
    :cond_0
    invoke-direct {p0}, Lbkx;->c()Ldzz;

    move-result-object v1

    iput-object v1, p0, Lbkx;->b:Ldzz;

    .line 179
    iget-object v1, p0, Lbkx;->b:Ldzz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    .line 203
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 183
    :cond_2
    :try_start_1
    iget-object v1, p0, Lbkx;->b:Ldzz;

    iget-object v1, v1, Ldzz;->b:Licw;

    invoke-virtual {v1}, Licw;->a()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lbkx;->a([B)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Lbkx;->c:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 192
    :cond_3
    :try_start_2
    iget-object v1, p0, Lbkx;->b:Ldzz;

    iget-object v1, v1, Ldzz;->a:Ljava/lang/String;

    .line 193
    iget-object v2, p0, Lbkx;->d:Lbku;

    iget-object v2, v2, Lbku;->a:Lgix;

    invoke-interface {v2}, Lgix;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 194
    iget-object v2, p0, Lbkx;->d:Lbku;

    iget-object v2, v2, Lbku;->a:Lgix;

    invoke-interface {v2}, Lgix;->d()Lgit;

    move-result-object v2

    iget-object v2, v2, Lgit;->c:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    iget-object v0, p0, Lbkx;->c:Ljava/lang/Object;

    goto :goto_0

    .line 187
    :catch_0
    move-exception v1

    invoke-direct {p0}, Lbkx;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 198
    :cond_4
    :try_start_3
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    iget-object v0, p0, Lbkx;->c:Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected abstract a([B)Ljava/lang/Object;
.end method

.method protected abstract a(Ljava/lang/Object;)[B
.end method

.method public final declared-synchronized b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    new-instance v0, Ldzz;

    invoke-direct {v0}, Ldzz;-><init>()V

    .line 153
    iget-object v1, p0, Lbkx;->d:Lbku;

    iget-object v1, v1, Lbku;->a:Lgix;

    invoke-interface {v1}, Lgix;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lbkx;->d:Lbku;

    iget-object v1, v1, Lbku;->a:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    iget-object v1, v1, Lgit;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ldzz;->a(Ljava/lang/String;)Ldzz;

    .line 156
    :cond_0
    iget-object v1, p0, Lbkx;->d:Lbku;

    iget-object v1, v1, Lbku;->b:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ldzz;->a(J)Ldzz;

    .line 157
    invoke-virtual {p0, p1}, Lbkx;->a(Ljava/lang/Object;)[B

    move-result-object v1

    invoke-static {v1}, Licw;->a([B)Licw;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldzz;->a(Licw;)Ldzz;

    .line 159
    iput-object v0, p0, Lbkx;->b:Ldzz;

    .line 160
    iput-object p1, p0, Lbkx;->c:Ljava/lang/Object;

    .line 162
    iget-object v1, p0, Lbkx;->d:Lbku;

    iget-object v1, v1, Lbku;->c:Ljava/util/concurrent/Executor;

    new-instance v2, Lbky;

    invoke-direct {v2, p0, v0}, Lbky;-><init>(Lbkx;Ldzz;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 174
    monitor-exit p0

    return-void

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
