.class public abstract Lgpq;
.super Li;
.source "SourceFile"


# instance fields
.field private W:Lfmn;

.field private X:Lfhz;

.field private Y:Lfdw;

.field private Z:Lfrz;

.field private aa:Lgpy;

.field private ab:Lgpy;


# direct methods
.method public constructor <init>(Lfmn;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Li;-><init>()V

    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v1, "SHARE_PANEL"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 61
    invoke-virtual {p0, v0}, Lgpq;->f(Landroid/os/Bundle;)V

    .line 62
    return-void
.end method

.method private C()I
    .locals 3

    .prologue
    .line 192
    invoke-virtual {p0}, Lgpq;->k()Landroid/content/res/Resources;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/high16 v1, 0x7f0b0000

    .line 194
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 195
    :goto_0
    return v0

    .line 194
    :cond_0
    const v1, 0x7f0b0001

    .line 195
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lgpq;)Lfmn;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lgpq;->W:Lfmn;

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/util/Map;Landroid/content/pm/PackageManager;Lhog;)Ljava/util/List;
    .locals 4

    .prologue
    .line 230
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 231
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmp;

    .line 232
    iget-object v1, v0, Lfmp;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, v0, Lfmp;->a:Lhvg;

    iget-object v1, v1, Lhvg;->b:Lhog;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lfmp;->a:Lhvg;

    iget-object v1, v1, Lhvg;->b:Lhog;

    iget-object v1, v1, Lhog;->x:Lgzp;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lfmp;->a:Lhvg;

    iget-object v1, v1, Lhvg;->b:Lhog;

    iget-object v1, v1, Lhog;->x:Lgzp;

    iget-object v1, v1, Lgzp;->a:Ljava/lang/String;

    iput-object v1, v0, Lfmp;->b:Ljava/lang/String;

    :cond_1
    iget-object v1, v0, Lfmp;->b:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 233
    if-eqz v1, :cond_0

    .line 234
    invoke-virtual {v0, p2, v1, p3}, Lfmp;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lhog;)V

    .line 237
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 239
    :cond_2
    return-object v2
.end method

.method static synthetic a(Lgpq;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 45
    invoke-virtual {p0}, Lgpq;->j()Lo;

    move-result-object v1

    const-string v0, "clipboard"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    const-string v2, "text/plain"

    invoke-static {v2, p1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    const v0, 0x7f090002

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Leze;->a(Landroid/content/Context;II)V

    return-void
.end method


# virtual methods
.method public abstract A()Lfdw;
.end method

.method public abstract B()Lfrz;
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    .line 81
    const v0, 0x7f040102

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 83
    const v0, 0x7f08008b

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    iget-object v1, p0, Lgpq;->W:Lfmn;

    iget-object v2, v1, Lfmn;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    iget-object v2, v1, Lfmn;->a:Lhvm;

    iget-object v2, v2, Lhvm;->d:Lhgz;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lfmn;->a:Lhvm;

    iget-object v2, v2, Lhvm;->d:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfmn;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, v1, Lfmn;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    const v0, 0x7f0802e9

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 87
    iget-object v1, p0, Lgpq;->W:Lfmn;

    iget-object v2, v1, Lfmn;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_1

    iget-object v2, v1, Lfmn;->a:Lhvm;

    iget-object v2, v2, Lhvm;->e:Lhgz;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lfmn;->a:Lhvm;

    iget-object v2, v2, Lhvm;->e:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfmn;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, v1, Lfmn;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    new-instance v1, Lgpr;

    invoke-direct {v1, p0}, Lgpr;-><init>(Lgpq;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    const v0, 0x7f080200

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 97
    new-instance v0, Lgps;

    invoke-direct {v0, p0}, Lgps;-><init>(Lgpq;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    const v0, 0x7f0801de

    .line 105
    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/libraries/youtube/common/ui/TopPeekingScrollView;

    .line 106
    invoke-virtual {p0}, Lgpq;->k()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0003

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v7, Lcom/google/android/libraries/youtube/common/ui/TopPeekingScrollView;->d:I

    invoke-virtual {v7}, Lcom/google/android/libraries/youtube/common/ui/TopPeekingScrollView;->requestLayout()V

    .line 107
    iput-object v1, v7, Lcom/google/android/libraries/youtube/common/ui/TopPeekingScrollView;->e:Landroid/view/View;

    .line 109
    invoke-virtual {p0}, Lgpq;->j()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 111
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-static {v1}, La;->a(Landroid/content/pm/PackageManager;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 112
    :cond_2
    iget-object v0, p0, Lgpq;->W:Lfmn;

    .line 113
    iget-object v0, v0, Lfmn;->a:Lhvm;

    iget-object v3, v0, Lhvm;->f:Lhog;

    .line 114
    iget-object v0, p0, Lgpq;->W:Lfmn;

    .line 115
    invoke-virtual {v0}, Lfmn;->a()Ljava/util/List;

    move-result-object v0

    .line 114
    invoke-static {v0, v2, v1, v3}, Lgpq;->a(Ljava/util/List;Ljava/util/Map;Landroid/content/pm/PackageManager;Lhog;)Ljava/util/List;

    move-result-object v9

    .line 119
    iget-object v0, p0, Lgpq;->W:Lfmn;

    .line 120
    invoke-virtual {v0}, Lfmn;->b()Ljava/util/List;

    move-result-object v0

    .line 119
    invoke-static {v0, v2, v1, v3}, Lgpq;->a(Ljava/util/List;Ljava/util/Map;Landroid/content/pm/PackageManager;Lhog;)Ljava/util/List;

    move-result-object v10

    .line 126
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 127
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 128
    new-instance v5, Lfmp;

    invoke-direct {v5, v1, v0, v3}, Lfmp;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lhog;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 131
    :cond_3
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 132
    new-instance v1, Lgpt;

    invoke-direct {v1, p0, v0}, Lgpt;-><init>(Lgpq;Ljava/text/Collator;)V

    invoke-static {v4, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 140
    invoke-interface {v10, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 142
    new-instance v0, Lgpy;

    const v1, 0x7f0802eb

    .line 143
    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lgpq;->X:Lfhz;

    iget-object v3, p0, Lgpq;->Y:Lfdw;

    iget-object v4, p0, Lgpq;->Z:Lfrz;

    .line 148
    invoke-direct {p0}, Lgpq;->C()I

    move-result v6

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lgpy;-><init>(Landroid/view/ViewGroup;Lfhz;Lfdw;Lfrz;Li;I)V

    iput-object v0, p0, Lgpq;->aa:Lgpy;

    .line 149
    iget-object v0, p0, Lgpq;->aa:Lgpy;

    invoke-virtual {v0, v9}, Lgpy;->a(Ljava/util/List;)V

    .line 151
    new-instance v0, Lgpy;

    const v1, 0x7f0802ec

    .line 152
    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lgpq;->X:Lfhz;

    iget-object v3, p0, Lgpq;->Y:Lfdw;

    iget-object v4, p0, Lgpq;->Z:Lfrz;

    .line 157
    invoke-direct {p0}, Lgpq;->C()I

    move-result v6

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lgpy;-><init>(Landroid/view/ViewGroup;Lfhz;Lfdw;Lfrz;Li;I)V

    iput-object v0, p0, Lgpq;->ab:Lgpy;

    .line 158
    iget-object v0, p0, Lgpq;->ab:Lgpy;

    invoke-virtual {v0, v10}, Lgpy;->a(Ljava/util/List;)V

    .line 160
    const v0, 0x7f0802ea

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 161
    iput-object v0, v7, Lcom/google/android/libraries/youtube/common/ui/TopPeekingScrollView;->f:Landroid/view/View;

    .line 163
    iget-object v0, p0, Lgpq;->Y:Lfdw;

    iget-object v1, p0, Lgpq;->Z:Lfrz;

    .line 164
    invoke-interface {v1}, Lfrz;->B()Lfqg;

    move-result-object v1

    iget-object v2, p0, Lgpq;->W:Lfmn;

    const/4 v3, 0x0

    .line 163
    invoke-virtual {v0, v1, v2, v3}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    .line 168
    return-object v8
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1}, Li;->a(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lgpq;->h()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 69
    const-string v1, "SHARE_PANEL"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmn;

    iput-object v0, p0, Lgpq;->W:Lfmn;

    .line 70
    invoke-virtual {p0}, Lgpq;->z()Lfhz;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lgpq;->X:Lfhz;

    .line 71
    invoke-virtual {p0}, Lgpq;->A()Lfdw;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lgpq;->Y:Lfdw;

    .line 73
    invoke-virtual {p0}, Lgpq;->B()Lfrz;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lgpq;->Z:Lfrz;

    .line 75
    const/4 v0, 0x2

    const v1, 0x1030010

    invoke-virtual {p0, v0, v1}, Lgpq;->a(II)V

    .line 76
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 173
    invoke-super {p0, p1}, Li;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 175
    iget-object v0, p0, Lgpq;->aa:Lgpy;

    invoke-direct {p0}, Lgpq;->C()I

    move-result v1

    invoke-virtual {v0, v1}, Lgpy;->a(I)V

    .line 176
    iget-object v0, p0, Lgpq;->ab:Lgpy;

    invoke-direct {p0}, Lgpq;->C()I

    move-result v1

    invoke-virtual {v0, v1}, Lgpy;->a(I)V

    .line 177
    return-void
.end method

.method public abstract z()Lfhz;
.end method
