.class public Lexw;
.super Lexu;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final d:Lexd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lexw;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lexw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Levn;Lexd;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lexu;-><init>(Levn;)V

    .line 22
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lexw;->d:Lexd;

    .line 24
    invoke-virtual {p1, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-static {v0}, Lb;->c(Z)V

    .line 35
    iget-object v0, p0, Lexw;->d:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lexw;->a:Ljava/lang/String;

    return-object v0
.end method

.method public handleConnectivityChangedEvent(Lewk;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 40
    iget-boolean v0, p1, Lewk;->a:Z

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p0}, Lexw;->a()V

    .line 43
    :cond_0
    return-void
.end method
