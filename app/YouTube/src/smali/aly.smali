.class public abstract Laly;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgec;


# instance fields
.field a:Lait;

.field public b:Lged;


# direct methods
.method public constructor <init>(Lait;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "client cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lait;

    iput-object v0, p0, Laly;->a:Lait;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Access to video frame dimensions not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Laly;->a:Lait;

    if-eqz v0, :cond_0

    .line 89
    :try_start_0
    iget-object v0, p0, Laly;->a:Lait;

    invoke-interface {v0, p1}, Lait;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Laly;->a:Lait;

    if-eqz v0, :cond_0

    .line 42
    :try_start_0
    iget-object v0, p0, Laly;->a:Lait;

    invoke-interface {v0, p1, p2}, Lait;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lged;)V
    .locals 1

    .prologue
    .line 35
    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lged;

    iput-object v0, p0, Laly;->b:Lged;

    .line 36
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Laly;->a:Lait;

    if-eqz v0, :cond_0

    .line 78
    :try_start_0
    iget-object v0, p0, Laly;->a:Lait;

    invoke-interface {v0, p1}, Lait;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Access to video frame dimensions not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Laly;->a:Lait;

    if-eqz v0, :cond_0

    .line 100
    :try_start_0
    iget-object v0, p0, Laly;->a:Lait;

    invoke-interface {v0}, Lait;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Laly;->a:Lait;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Laly;->b:Lged;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Laly;->b:Lged;

    invoke-interface {v0}, Lged;->c()V

    .line 118
    :cond_0
    :try_start_0
    iget-object v0, p0, Laly;->a:Lait;

    invoke-interface {v0}, Lait;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
