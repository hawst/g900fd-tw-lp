.class public final Lcoo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;


# instance fields
.field public final a:Lezj;

.field final b:Lfdo;

.field final c:Lcou;

.field public final d:Ljava/lang/String;

.field public final e:J

.field public final f:J

.field public final g:Ljava/lang/String;

.field final h:Ljava/lang/String;

.field public volatile i:J

.field volatile j:I

.field volatile k:Z

.field private final l:Ljava/util/concurrent/Executor;

.field private final m:Levn;

.field private final n:Landroid/os/Handler;

.field private final o:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lezj;Ljava/util/concurrent/Executor;Levn;Landroid/os/Handler;Lfdo;Lcou;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcoo;->a:Lezj;

    .line 83
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcoo;->l:Ljava/util/concurrent/Executor;

    .line 84
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcoo;->m:Levn;

    .line 85
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcoo;->n:Landroid/os/Handler;

    .line 86
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdo;

    iput-object v0, p0, Lcoo;->b:Lfdo;

    .line 87
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcou;

    iput-object v0, p0, Lcoo;->c:Lcou;

    .line 88
    invoke-static {p7}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcoo;->d:Ljava/lang/String;

    .line 89
    iput-wide p8, p0, Lcoo;->e:J

    .line 90
    iput-wide p10, p0, Lcoo;->f:J

    .line 91
    invoke-static {p12}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcoo;->g:Ljava/lang/String;

    .line 92
    invoke-static {p13}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcoo;->h:Ljava/lang/String;

    .line 93
    new-instance v0, Lcot;

    invoke-direct {v0, p0}, Lcot;-><init>(Lcoo;)V

    iput-object v0, p0, Lcoo;->o:Ljava/lang/Runnable;

    .line 94
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcoo;->i:J

    .line 95
    const/4 v0, 0x0

    iput v0, p0, Lcoo;->j:I

    .line 96
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcoo;->k:Z

    .line 97
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcoo;->m:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 383
    return-void
.end method

.method final a(Lczb;)V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcoo;->n:Landroid/os/Handler;

    new-instance v1, Lcop;

    invoke-direct {v1, p0, p1}, Lcop;-><init>(Lcoo;Lczb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 206
    return-void
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 118
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_0

    .line 119
    iget-wide v0, p0, Lcoo;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 121
    iget-boolean v0, p0, Lcoo;->k:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcoo;->i:J

    iget-object v2, p0, Lcoo;->a:Lezj;

    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcoo;->k:Z

    .line 123
    iget-object v0, p0, Lcoo;->l:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcoo;->o:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 126
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 387
    iget-object v0, p0, Lcoo;->m:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 388
    return-void
.end method
