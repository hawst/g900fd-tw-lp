.class public final Lazd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lbxo;
.implements Leyo;


# instance fields
.field public a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

.field public b:Lfvh;

.field public c:Z

.field public d:Ljava/util/HashSet;

.field private e:Levn;


# direct methods
.method public constructor <init>(Levn;Lcom/google/android/apps/youtube/app/ui/TabbedView;Lfvh;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lazd;->e:Levn;

    .line 51
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iput-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    .line 52
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvh;

    iput-object v0, p0, Lazd;->b:Lfvh;

    .line 53
    return-void
.end method

.method private a(Levd;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lazd;->e:Levn;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lazd;->e:Levn;

    invoke-virtual {v0, p1}, Levn;->d(Ljava/lang/Object;)V

    .line 72
    :cond_0
    invoke-static {p0}, Leyi;->b(Leyo;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lazd;->c:Z

    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lazd;->c()V

    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->h:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iput-object v1, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    :cond_1
    iget-object v0, p0, Lazd;->b:Lfvh;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lazd;->b:Lfvh;

    iget-object v0, v0, Lfvh;->d:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iput-object v1, p0, Lazd;->b:Lfvh;

    :cond_2
    iget-object v0, p0, Lazd;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iput-object v1, p0, Lazd;->e:Levn;

    .line 73
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    .line 94
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    .line 95
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 97
    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    iget-object v0, p0, Lazd;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lazs;

    invoke-direct {v0}, Lazs;-><init>()V

    invoke-direct {p0, v0}, Lazd;->a(Levd;)V

    .line 66
    return-void
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lazd;->c:Z

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lazd;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 113
    :cond_0
    return-void
.end method

.method public final a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 116
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 154
    new-instance v0, Lazs;

    invoke-direct {v0}, Lazs;-><init>()V

    invoke-direct {p0, v0}, Lazd;->a(Levd;)V

    .line 155
    return-void
.end method

.method public final b(Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lazd;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Lazd;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lazd;->c:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 122
    new-instance v0, Lazq;

    invoke-direct {v0}, Lazq;-><init>()V

    invoke-direct {p0, v0}, Lazd;->a(Levd;)V

    .line 124
    :cond_0
    return-void

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Lazr;

    invoke-direct {v0}, Lazr;-><init>()V

    invoke-direct {p0, v0}, Lazd;->a(Levd;)V

    .line 129
    return-void
.end method

.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Lazd;->c()V

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lazd;->c:Z

    .line 138
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 146
    if-nez p2, :cond_0

    .line 150
    :goto_0
    return-void

    .line 149
    :cond_0
    new-instance v0, Lazs;

    invoke-direct {v0}, Lazs;-><init>()V

    invoke-direct {p0, v0}, Lazd;->a(Levd;)V

    goto :goto_0
.end method
