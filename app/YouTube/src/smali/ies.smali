.class public final enum Lies;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lies;

.field public static final enum b:Lies;

.field public static final enum c:Lies;

.field public static final enum d:Lies;

.field public static final enum e:Lies;

.field public static final enum f:Lies;

.field public static final enum g:Lies;

.field public static final enum h:Lies;

.field public static final enum i:Lies;

.field private static enum j:Lies;

.field private static final synthetic k:[Lies;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 35
    new-instance v0, Lies;

    const-string v1, "OTHER"

    const-class v2, Ljava/lang/Object;

    invoke-direct {v0, v1, v4, v2}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->j:Lies;

    .line 41
    new-instance v0, Lies;

    const-string v1, "PURE_BARCODE"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v5, v2}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->a:Lies;

    .line 47
    new-instance v0, Lies;

    const-string v1, "POSSIBLE_FORMATS"

    const-class v2, Ljava/util/List;

    invoke-direct {v0, v1, v6, v2}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->b:Lies;

    .line 53
    new-instance v0, Lies;

    const-string v1, "TRY_HARDER"

    const-class v2, Ljava/lang/Void;

    invoke-direct {v0, v1, v7, v2}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->c:Lies;

    .line 58
    new-instance v0, Lies;

    const-string v1, "CHARACTER_SET"

    const-class v2, Ljava/lang/String;

    invoke-direct {v0, v1, v8, v2}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->d:Lies;

    .line 63
    new-instance v0, Lies;

    const-string v1, "ALLOWED_LENGTHS"

    const/4 v2, 0x5

    const-class v3, [I

    invoke-direct {v0, v1, v2, v3}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->e:Lies;

    .line 69
    new-instance v0, Lies;

    const-string v1, "ASSUME_CODE_39_CHECK_DIGIT"

    const/4 v2, 0x6

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->f:Lies;

    .line 76
    new-instance v0, Lies;

    const-string v1, "ASSUME_GS1"

    const/4 v2, 0x7

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->g:Lies;

    .line 83
    new-instance v0, Lies;

    const-string v1, "RETURN_CODABAR_START_END"

    const/16 v2, 0x8

    const-class v3, Ljava/lang/Void;

    invoke-direct {v0, v1, v2, v3}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->h:Lies;

    .line 89
    new-instance v0, Lies;

    const-string v1, "NEED_RESULT_POINT_CALLBACK"

    const/16 v2, 0x9

    const-class v3, Lifc;

    invoke-direct {v0, v1, v2, v3}, Lies;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Lies;->i:Lies;

    .line 30
    const/16 v0, 0xa

    new-array v0, v0, [Lies;

    sget-object v1, Lies;->j:Lies;

    aput-object v1, v0, v4

    sget-object v1, Lies;->a:Lies;

    aput-object v1, v0, v5

    sget-object v1, Lies;->b:Lies;

    aput-object v1, v0, v6

    sget-object v1, Lies;->c:Lies;

    aput-object v1, v0, v7

    sget-object v1, Lies;->d:Lies;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lies;->e:Lies;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lies;->f:Lies;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lies;->g:Lies;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lies;->h:Lies;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lies;->i:Lies;

    aput-object v2, v0, v1

    sput-object v0, Lies;->k:[Lies;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 105
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lies;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lies;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lies;

    return-object v0
.end method

.method public static values()[Lies;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lies;->k:[Lies;

    invoke-virtual {v0}, [Lies;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lies;

    return-object v0
.end method
