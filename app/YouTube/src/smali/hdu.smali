.class public final Lhdu;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhbt;

.field public c:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34304
    invoke-direct {p0}, Lidf;-><init>()V

    .line 34307
    iput-object v0, p0, Lhdu;->a:Lhgz;

    .line 34310
    iput-object v0, p0, Lhdu;->b:Lhbt;

    .line 34313
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhdu;->c:[B

    .line 34304
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 34367
    const/4 v0, 0x0

    .line 34368
    iget-object v1, p0, Lhdu;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 34369
    const/4 v0, 0x1

    iget-object v1, p0, Lhdu;->a:Lhgz;

    .line 34370
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 34372
    :cond_0
    iget-object v1, p0, Lhdu;->b:Lhbt;

    if-eqz v1, :cond_1

    .line 34373
    const/4 v1, 0x2

    iget-object v2, p0, Lhdu;->b:Lhbt;

    .line 34374
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34376
    :cond_1
    iget-object v1, p0, Lhdu;->c:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 34377
    const/4 v1, 0x4

    iget-object v2, p0, Lhdu;->c:[B

    .line 34378
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 34380
    :cond_2
    iget-object v1, p0, Lhdu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34381
    iput v0, p0, Lhdu;->J:I

    .line 34382
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 34300
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhdu;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhdu;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhdu;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhdu;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdu;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhdu;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhdu;->b:Lhbt;

    if-nez v0, :cond_3

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhdu;->b:Lhbt;

    :cond_3
    iget-object v0, p0, Lhdu;->b:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhdu;->c:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 34352
    iget-object v0, p0, Lhdu;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 34353
    const/4 v0, 0x1

    iget-object v1, p0, Lhdu;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 34355
    :cond_0
    iget-object v0, p0, Lhdu;->b:Lhbt;

    if-eqz v0, :cond_1

    .line 34356
    const/4 v0, 0x2

    iget-object v1, p0, Lhdu;->b:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 34358
    :cond_1
    iget-object v0, p0, Lhdu;->c:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 34359
    const/4 v0, 0x4

    iget-object v1, p0, Lhdu;->c:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 34361
    :cond_2
    iget-object v0, p0, Lhdu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 34363
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 34326
    if-ne p1, p0, :cond_1

    .line 34332
    :cond_0
    :goto_0
    return v0

    .line 34327
    :cond_1
    instance-of v2, p1, Lhdu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 34328
    :cond_2
    check-cast p1, Lhdu;

    .line 34329
    iget-object v2, p0, Lhdu;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdu;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhdu;->b:Lhbt;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdu;->b:Lhbt;

    if-nez v2, :cond_3

    .line 34330
    :goto_2
    iget-object v2, p0, Lhdu;->c:[B

    iget-object v3, p1, Lhdu;->c:[B

    .line 34331
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhdu;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhdu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 34332
    goto :goto_0

    .line 34329
    :cond_4
    iget-object v2, p0, Lhdu;->a:Lhgz;

    iget-object v3, p1, Lhdu;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhdu;->b:Lhbt;

    iget-object v3, p1, Lhdu;->b:Lhbt;

    .line 34330
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 34331
    :cond_6
    iget-object v2, p0, Lhdu;->I:Ljava/util/List;

    iget-object v3, p1, Lhdu;->I:Ljava/util/List;

    .line 34332
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 34336
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 34338
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdu;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 34339
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdu;->b:Lhbt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 34340
    iget-object v2, p0, Lhdu;->c:[B

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 34346
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhdu;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_2
    add-int/2addr v0, v1

    .line 34347
    return v0

    .line 34338
    :cond_1
    iget-object v0, p0, Lhdu;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 34339
    :cond_2
    iget-object v0, p0, Lhdu;->b:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    .line 34342
    :goto_3
    iget-object v3, p0, Lhdu;->c:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 34343
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhdu;->c:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 34342
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 34346
    :cond_4
    iget-object v1, p0, Lhdu;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
