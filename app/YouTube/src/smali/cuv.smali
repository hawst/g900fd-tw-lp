.class public final Lcuv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcuv;

.field private static final d:Ljava/util/Map;


# instance fields
.field public final b:Lcuw;

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    new-instance v0, Lcuv;

    sget-object v1, Lcuw;->a:Lcuw;

    invoke-direct {v0, v1}, Lcuv;-><init>(Lcuw;)V

    sput-object v0, Lcuv;->a:Lcuv;

    .line 99
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 100
    const-string v1, "net.dns"

    sget-object v2, Lcuw;->c:Lcuw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    const-string v1, "net.connect"

    sget-object v2, Lcuw;->d:Lcuw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string v1, "net.timeout"

    sget-object v2, Lcuw;->e:Lcuw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v1, "net.closed"

    sget-object v2, Lcuw;->e:Lcuw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    const-string v1, "net.nomobiledata"

    sget-object v2, Lcuw;->b:Lcuw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    const-string v1, "fmt.noneavailable"

    sget-object v2, Lcuw;->g:Lcuw;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcuv;->d:Ljava/util/Map;

    .line 114
    return-void
.end method

.method private constructor <init>(Lcuw;)V
    .locals 1

    .prologue
    .line 125
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcuv;-><init>(Lcuw;Ljava/lang/String;)V

    .line 126
    return-void
.end method

.method public constructor <init>(Lcuw;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcuw;

    iput-object v0, p0, Lcuv;->b:Lcuw;

    .line 136
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lgoo;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcuv;->c:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public static a(Lggp;)Lcuv;
    .locals 3

    .prologue
    .line 162
    sget-object v0, Lcuv;->d:Ljava/util/Map;

    iget-object v1, p0, Lggp;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcuw;

    .line 163
    new-instance v1, Lcuv;

    if-eqz v0, :cond_0

    .line 165
    :goto_0
    iget-object v2, p0, Lggp;->c:Ljava/lang/Object;

    invoke-static {v2}, Lgoo;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcuv;-><init>(Lcuw;Ljava/lang/String;)V

    return-object v1

    .line 163
    :cond_0
    sget-object v0, Lcuw;->f:Lcuw;

    goto :goto_0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 158
    iget-object v0, p0, Lcuv;->b:Lcuw;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcuv;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "AdError: type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " message="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
