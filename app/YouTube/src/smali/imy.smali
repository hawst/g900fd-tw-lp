.class final Limy;
.super Limu;
.source "SourceFile"

# interfaces
.implements Liql;


# instance fields
.field private b:J

.field private synthetic c:Limt;


# direct methods
.method public constructor <init>(Limt;Lu;J)V
    .locals 5

    .prologue
    .line 442
    iput-object p1, p0, Limy;->c:Limt;

    .line 443
    invoke-direct {p0, p1, p2}, Limu;-><init>(Limt;Lu;)V

    .line 444
    iput-wide p3, p0, Limy;->b:J

    .line 445
    iget-wide v0, p0, Limy;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 446
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Limy;->a(Z)V

    .line 448
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Liqm;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Limy;->c:Limt;

    invoke-static {v0}, Limt;->f(Limt;)Lipu;

    move-result-object v0

    invoke-interface {v0}, Lipu;->a()Liqm;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lipq;J)J
    .locals 8

    .prologue
    const-wide/16 v0, -0x1

    const-wide/16 v6, 0x0

    .line 452
    cmp-long v2, p2, v6

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 453
    :cond_0
    iget-boolean v2, p0, Limy;->a:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_1
    iget-wide v2, p0, Limy;->b:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    .line 467
    :goto_0
    return-wide v0

    .line 456
    :cond_2
    iget-object v2, p0, Limy;->c:Limt;

    invoke-static {v2}, Limt;->f(Limt;)Lipu;

    move-result-object v2

    iget-wide v4, p0, Limy;->b:J

    invoke-static {v4, v5, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Lipu;->b(Lipq;J)J

    move-result-wide v2

    .line 457
    cmp-long v0, v2, v0

    if-nez v0, :cond_3

    .line 458
    invoke-virtual {p0}, Limy;->A_()V

    .line 459
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 462
    :cond_3
    iget-wide v0, p0, Limy;->b:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Limy;->b:J

    .line 463
    invoke-virtual {p0, p1, v2, v3}, Limy;->a(Lipq;J)V

    .line 464
    iget-wide v0, p0, Limy;->b:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_4

    .line 465
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Limy;->a(Z)V

    :cond_4
    move-wide v0, v2

    .line 467
    goto :goto_0
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 475
    iget-boolean v0, p0, Limy;->a:Z

    if-eqz v0, :cond_0

    .line 482
    :goto_0
    return-void

    .line 477
    :cond_0
    iget-wide v0, p0, Limy;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Limy;->c:Limt;

    const/16 v1, 0x64

    invoke-virtual {v0, p0, v1}, Limt;->a(Liql;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 478
    invoke-virtual {p0}, Limy;->A_()V

    .line 481
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Limy;->a:Z

    goto :goto_0
.end method
