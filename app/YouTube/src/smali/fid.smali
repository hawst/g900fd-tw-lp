.class public Lfid;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public final a:Lgxt;

.field public b:Lfnc;

.field private final c:Lfqh;

.field private d:Landroid/text/Spanned;

.field private e:Lfnc;


# direct methods
.method public constructor <init>(Lgxt;Lfqh;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lfid;->a:Lgxt;

    .line 29
    iput-object p2, p0, Lfid;->c:Lfqh;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lfid;->d:Landroid/text/Spanned;

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lfid;->a:Lgxt;

    iget-object v0, v0, Lgxt;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfid;->d:Landroid/text/Spanned;

    .line 40
    :cond_0
    iget-object v0, p0, Lfid;->d:Landroid/text/Spanned;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lfid;->a:Lgxt;

    iget-object v0, v0, Lgxt;->b:Ljava/lang/String;

    .line 45
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const/4 v0, 0x0

    .line 48
    :cond_0
    return-object v0
.end method

.method public final c()Lfnc;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lfid;->e:Lfnc;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfid;->a:Lgxt;

    iget-object v1, v1, Lgxt;->d:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfid;->e:Lfnc;

    .line 55
    :cond_0
    iget-object v0, p0, Lfid;->e:Lfnc;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lfid;->a:Lgxt;

    iget-object v0, v0, Lgxt;->a:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfid;->c:Lfqh;

    return-object v0
.end method
