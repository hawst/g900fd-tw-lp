.class public final Lacd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgir;


# instance fields
.field final a:Lfws;

.field final b:Ljava/util/concurrent/Executor;

.field final c:Landroid/content/SharedPreferences;

.field final d:Landroid/os/Handler;

.field final e:Ljava/lang/String;

.field f:Lgbl;

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lfws;Ljava/util/concurrent/Executor;Landroid/os/Handler;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const-string v0, "appVersion cannot be null or empty"

    invoke-static {p6, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    const-string v0, "appDeveloperKey cannot be null or empty"

    invoke-static {p7, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 54
    const-string v0, "deviceRegistrationClient cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfws;

    iput-object v0, p0, Lacd;->a:Lfws;

    .line 56
    const-string v0, "executor cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lacd;->b:Ljava/util/concurrent/Executor;

    .line 57
    const-string v0, "uiHandler cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lacd;->d:Landroid/os/Handler;

    .line 58
    const-string v0, "preferences cannot be null"

    invoke-static {p4, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lacd;->c:Landroid/content/SharedPreferences;

    .line 59
    const-string v0, "appPackage cannot be null or empty"

    invoke-static {p5, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacd;->g:Ljava/lang/String;

    .line 60
    const-string v0, "_%s_%s_%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    const/4 v2, 0x1

    aput-object p6, v1, v2

    const/4 v2, 0x2

    aput-object p7, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacd;->e:Ljava/lang/String;

    .line 61
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Lacd;->f:Lgbl;

    if-nez v0, :cond_0

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Need to call init() first"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_0
    iget-object v0, p0, Lacd;->f:Lgbl;

    invoke-virtual {v0, p1}, Lgbl;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 122
    const-string v1, "%s, client-id=\"%s\""

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lacd;->g:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
