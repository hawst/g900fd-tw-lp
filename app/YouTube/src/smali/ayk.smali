.class public final Layk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Laym;

.field private final b:Landroid/content/IntentFilter;

.field private final c:Layl;

.field private final d:Landroid/content/Context;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Laym;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Layk;->b:Landroid/content/IntentFilter;

    .line 50
    new-instance v0, Layl;

    invoke-direct {v0, p0}, Layl;-><init>(Layk;)V

    iput-object v0, p0, Layk;->c:Layl;

    .line 51
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laym;

    iput-object v0, p0, Layk;->a:Laym;

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Layk;->d:Landroid/content/Context;

    .line 53
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Layk;->e:Z

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Layk;->d:Landroid/content/Context;

    iget-object v1, p0, Layk;->c:Layl;

    iget-object v2, p0, Layk;->b:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Layk;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :cond_0
    monitor-exit p0

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
