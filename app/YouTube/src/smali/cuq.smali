.class final Lcuq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private synthetic a:Lgiv;

.field private synthetic b:Landroid/app/Activity;

.field private synthetic c:Lcup;


# direct methods
.method constructor <init>(Lcup;Lgiv;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcuq;->c:Lcup;

    iput-object p2, p0, Lcuq;->a:Lgiv;

    iput-object p3, p0, Lcuq;->b:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcuq;->c:Lcup;

    invoke-static {v0, p1}, Lcup;->a(Lcup;Ljava/lang/Exception;)V

    .line 141
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 122
    check-cast p1, Lfig;

    invoke-virtual {p1}, Lfig;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfid;

    iget-object v1, p0, Lcuq;->c:Lcup;

    new-instance v2, Lgit;

    iget-object v3, p0, Lcuq;->a:Lgiv;

    invoke-virtual {v3}, Lgiv;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcuq;->a:Lgiv;

    invoke-virtual {v4}, Lgiv;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lfid;->a:Lgxt;

    iget-object v5, v5, Lgxt;->f:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lgit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcua;

    iget-object v4, p0, Lcuq;->a:Lgiv;

    invoke-virtual {v4}, Lgiv;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcua;-><init>(Ljava/lang/String;Lfid;)V

    invoke-static {v1, v2, v3}, Lcup;->a(Lcup;Lgit;Lcua;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcuq;->c:Lcup;

    iget-object v2, p0, Lcuq;->b:Landroid/app/Activity;

    iget-object v3, p0, Lcuq;->a:Lgiv;

    invoke-virtual {v3}, Lgiv;->c()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcus;

    iget-object v5, v1, Lcup;->a:Leyp;

    invoke-direct {v4, v2, v0, v5}, Lcus;-><init>(Landroid/app/Activity;Ljava/util/List;Leyp;)V

    new-instance v5, Landroid/app/AlertDialog$Builder;

    new-instance v6, Landroid/view/ContextThemeWrapper;

    const v7, 0x7f0d0104

    invoke-direct {v6, v2, v7}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090113

    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-static {v0}, Lcup;->a(Ljava/util/List;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v4, 0x7f0900a1

    new-instance v5, Lcur;

    invoke-direct {v5, v1, v0, v3}, Lcur;-><init>(Lcup;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method
