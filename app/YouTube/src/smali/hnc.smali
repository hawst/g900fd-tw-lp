.class public final Lhnc;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhnc;


# instance fields
.field public b:Lhnd;

.field public c:Lhnf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhnc;

    sput-object v0, Lhnc;->a:[Lhnc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Lhnc;->b:Lhnd;

    iput-object v0, p0, Lhnc;->c:Lhnf;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhnc;->b:Lhnd;

    if-eqz v1, :cond_0

    const v0, 0x3f5cf94

    iget-object v1, p0, Lhnc;->b:Lhnd;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhnc;->c:Lhnf;

    if-eqz v1, :cond_1

    const v1, 0x3f5cfc3

    iget-object v2, p0, Lhnc;->c:Lhnf;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhnc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhnc;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhnc;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhnc;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhnc;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhnc;->b:Lhnd;

    if-nez v0, :cond_2

    new-instance v0, Lhnd;

    invoke-direct {v0}, Lhnd;-><init>()V

    iput-object v0, p0, Lhnc;->b:Lhnd;

    :cond_2
    iget-object v0, p0, Lhnc;->b:Lhnd;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhnc;->c:Lhnf;

    if-nez v0, :cond_3

    new-instance v0, Lhnf;

    invoke-direct {v0}, Lhnf;-><init>()V

    iput-object v0, p0, Lhnc;->c:Lhnf;

    :cond_3
    iget-object v0, p0, Lhnc;->c:Lhnf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1fae7ca2 -> :sswitch_1
        0x1fae7e1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Lhnc;->b:Lhnd;

    if-eqz v0, :cond_0

    const v0, 0x3f5cf94

    iget-object v1, p0, Lhnc;->b:Lhnd;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhnc;->c:Lhnf;

    if-eqz v0, :cond_1

    const v0, 0x3f5cfc3

    iget-object v1, p0, Lhnc;->c:Lhnf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhnc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhnc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhnc;

    iget-object v2, p0, Lhnc;->b:Lhnd;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhnc;->b:Lhnd;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhnc;->c:Lhnf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhnc;->c:Lhnf;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhnc;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhnc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhnc;->b:Lhnd;

    iget-object v3, p1, Lhnc;->b:Lhnd;

    invoke-virtual {v2, v3}, Lhnd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhnc;->c:Lhnf;

    iget-object v3, p1, Lhnc;->c:Lhnf;

    invoke-virtual {v2, v3}, Lhnf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhnc;->I:Ljava/util/List;

    iget-object v3, p1, Lhnc;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhnc;->b:Lhnd;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhnc;->c:Lhnf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhnc;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhnc;->b:Lhnd;

    invoke-virtual {v0}, Lhnd;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhnc;->c:Lhnf;

    invoke-virtual {v0}, Lhnf;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lhnc;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
