.class public final Lgem;
.super Lgee;
.source "SourceFile"

# interfaces
.implements Lgec;


# instance fields
.field private a:Lgec;

.field private b:Landroid/view/View;

.field private c:Z

.field private d:Z

.field private e:Lged;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lgee;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method private k()V
    .locals 2

    .prologue
    .line 199
    invoke-direct {p0}, Lgem;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaView method called before surface created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    return-void
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lgem;->a:Lgec;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Lgem;->k()V

    .line 116
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->a()I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 145
    invoke-direct {p0}, Lgem;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgem;->d:Z

    .line 147
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0, p1}, Lgec;->a(I)V

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgem;->d:Z

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Lgem;->k()V

    .line 110
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0, p1, p2}, Lgec;->a(II)V

    .line 111
    return-void
.end method

.method public final a(Lged;)V
    .locals 1

    .prologue
    .line 82
    iput-object p1, p0, Lgem;->e:Lged;

    .line 83
    iget-object v0, p0, Lgem;->a:Lgec;

    if-eqz v0, :cond_0

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgem;->c:Z

    .line 85
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0, p1}, Lgec;->a(Lged;)V

    .line 90
    :goto_0
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgem;->c:Z

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lgem;->k()V

    .line 122
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->b()I

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 186
    invoke-direct {p0}, Lgem;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0, p1}, Lgec;->b(I)V

    return-void

    .line 189
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SafeTextureMediaView not initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Lgem;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->c()V

    .line 158
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgem;->d:Z

    .line 159
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0}, Lgem;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->d()V

    .line 175
    :cond_0
    return-void
.end method

.method public final e()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lgem;->a:Lgec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->e()Landroid/view/Surface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lgem;->a:Lgec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->f()Landroid/view/SurfaceHolder;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 179
    invoke-direct {p0}, Lgem;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->g()V

    .line 182
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lgem;->a:Lgec;

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 166
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->h()Z

    move-result v0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lgem;->a:Lgec;

    invoke-interface {v0}, Lgec;->i()I

    move-result v0

    return v0
.end method

.method public final j()Landroid/view/View;
    .locals 0

    .prologue
    .line 99
    return-object p0
.end method

.method protected final onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 34
    invoke-super {p0}, Lgee;->onAttachedToWindow()V

    .line 36
    iget-object v0, p0, Lgem;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lgem;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lgem;->removeView(Landroid/view/View;)V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lgem;->b:Landroid/view/View;

    .line 41
    :cond_0
    invoke-virtual {p0}, Lgem;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42
    new-instance v0, Lgfg;

    invoke-virtual {p0}, Lgem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgfg;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object v0, p0, Lgem;->a:Lgec;

    .line 44
    iput-object v0, p0, Lgem;->b:Landroid/view/View;

    .line 50
    :goto_0
    iget-object v0, p0, Lgem;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lgem;->addView(Landroid/view/View;)V

    .line 52
    iget-boolean v0, p0, Lgem;->c:Z

    if-eqz v0, :cond_1

    .line 53
    iput-boolean v2, p0, Lgem;->c:Z

    .line 54
    iget-object v0, p0, Lgem;->a:Lgec;

    iget-object v1, p0, Lgem;->e:Lged;

    invoke-interface {v0, v1}, Lgec;->a(Lged;)V

    .line 55
    iget-boolean v0, p0, Lgem;->d:Z

    if-eqz v0, :cond_1

    .line 56
    invoke-virtual {p0, v2}, Lgem;->a(I)V

    .line 59
    :cond_1
    return-void

    .line 46
    :cond_2
    new-instance v0, Lgeu;

    invoke-virtual {p0}, Lgem;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lgeu;-><init>(Landroid/content/Context;)V

    .line 47
    iput-object v0, p0, Lgem;->a:Lgec;

    .line 48
    iput-object v0, p0, Lgem;->b:Landroid/view/View;

    goto :goto_0
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    invoke-virtual {p0}, Lgem;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 75
    invoke-virtual {p0, v3}, Lgem;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 78
    :cond_0
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-virtual {p0}, Lgem;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 64
    invoke-virtual {p0, v1}, Lgem;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 65
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 66
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lgem;->setMeasuredDimension(II)V

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-virtual {p0, v1, v1}, Lgem;->setMeasuredDimension(II)V

    goto :goto_0
.end method
