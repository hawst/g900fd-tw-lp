.class final Leeq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leet;


# static fields
.field private static final a:[I


# instance fields
.field private final b:[B

.field private final c:Ljava/util/Stack;

.field private d:Lees;

.field private e:I

.field private f:J

.field private g:J

.field private h:I

.field private i:I

.field private j:J

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Leeq;->a:[I

    return-void

    :array_0
    .array-data 4
        0x80
        0x40
        0x20
        0x10
        0x8
        0x4
        0x2
        0x1
    .end array-data
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Leeq;->b:[B

    .line 49
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Leeq;->c:Ljava/util/Stack;

    .line 525
    return-void
.end method

.method private a(II)I
    .locals 4

    .prologue
    .line 489
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 490
    const/4 v0, 0x2

    .line 497
    :goto_0
    return v0

    .line 492
    :cond_0
    iget v0, p0, Leeq;->n:I

    add-int/2addr v0, p1

    iput v0, p0, Leeq;->n:I

    .line 493
    iget-wide v0, p0, Leeq;->f:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Leeq;->f:J

    .line 494
    iget v0, p0, Leeq;->n:I

    if-ge v0, p2, :cond_1

    .line 495
    const/4 v0, 0x1

    goto :goto_0

    .line 497
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IZ)J
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 510
    if-eqz p2, :cond_0

    .line 511
    iget-object v1, p0, Leeq;->b:[B

    aget-byte v2, v1, v0

    sget-object v3, Leeq;->a:[I

    iget v4, p0, Leeq;->m:I

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 513
    :cond_0
    const-wide/16 v2, 0x0

    .line 514
    :goto_0
    if-ge v0, p1, :cond_1

    .line 516
    const/16 v1, 0x8

    shl-long/2addr v2, v1

    iget-object v1, p0, Leeq;->b:[B

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 514
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 518
    :cond_1
    return-wide v2
.end method

.method private b(Lefx;I)I
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Leeq;->n:I

    if-lt v0, p2, :cond_0

    .line 473
    const/4 v0, 0x0

    .line 477
    :goto_0
    return v0

    .line 475
    :cond_0
    iget v0, p0, Leeq;->n:I

    sub-int v0, p2, v0

    .line 476
    invoke-virtual {p1, v0}, Lefx;->a(I)I

    move-result v0

    .line 477
    invoke-direct {p0, v0, p2}, Leeq;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method private b(Lefx;[BI)I
    .locals 2

    .prologue
    .line 448
    iget v0, p0, Leeq;->n:I

    if-nez v0, :cond_0

    array-length v0, p2

    if-le p3, v0, :cond_0

    .line 449
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Byte array not large enough"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451
    :cond_0
    iget v0, p0, Leeq;->n:I

    if-lt v0, p3, :cond_1

    .line 452
    const/4 v0, 0x0

    .line 456
    :goto_0
    return v0

    .line 454
    :cond_1
    iget v0, p0, Leeq;->n:I

    sub-int v0, p3, v0

    .line 455
    iget v1, p0, Leeq;->n:I

    invoke-virtual {p1, p2, v1, v0}, Lefx;->a([BII)I

    move-result v0

    .line 456
    invoke-direct {p0, v0, p3}, Leeq;->a(II)I

    move-result v0

    goto :goto_0
.end method

.method private c(Lefx;)I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 372
    iget v0, p0, Leeq;->l:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 406
    :cond_0
    :goto_0
    return v1

    .line 377
    :cond_1
    iget v0, p0, Leeq;->l:I

    if-nez v0, :cond_5

    .line 378
    iput v1, p0, Leeq;->n:I

    .line 379
    iget-object v0, p0, Leeq;->b:[B

    invoke-direct {p0, p1, v0, v3}, Leeq;->b(Lefx;[BI)I

    move-result v0

    .line 380
    if-eqz v0, :cond_2

    move v1, v0

    .line 381
    goto :goto_0

    .line 383
    :cond_2
    iput v3, p0, Leeq;->l:I

    .line 385
    iget-object v0, p0, Leeq;->b:[B

    aget-byte v0, v0, v1

    and-int/lit16 v2, v0, 0xff

    .line 386
    iput v4, p0, Leeq;->m:I

    move v0, v1

    .line 387
    :goto_1
    sget-object v3, Leeq;->a:[I

    const/16 v3, 0x8

    if-ge v0, v3, :cond_3

    .line 388
    sget-object v3, Leeq;->a:[I

    aget v3, v3, v0

    and-int/2addr v3, v2

    if-eqz v3, :cond_4

    .line 389
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Leeq;->m:I

    .line 393
    :cond_3
    iget v0, p0, Leeq;->m:I

    if-ne v0, v4, :cond_5

    .line 394
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-wide v2, p0, Leeq;->f:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x45

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No valid varint length mask found at bytesRead = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 400
    :cond_5
    iget-object v0, p0, Leeq;->b:[B

    iget v2, p0, Leeq;->m:I

    invoke-direct {p0, p1, v0, v2}, Leeq;->b(Lefx;[BI)I

    move-result v0

    .line 401
    if-eqz v0, :cond_0

    move v1, v0

    .line 402
    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 298
    iput v0, p0, Leeq;->e:I

    .line 299
    iput v0, p0, Leeq;->i:I

    .line 300
    iput v0, p0, Leeq;->k:I

    .line 301
    iget-wide v0, p0, Leeq;->f:J

    iput-wide v0, p0, Leeq;->g:J

    .line 302
    return-void
.end method


# virtual methods
.method public final a(Lefx;)I
    .locals 12

    .prologue
    const/16 v10, 0x3f

    const-wide/32 v8, 0x7fffffff

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 126
    iget-object v0, p0, Leeq;->d:Lees;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 128
    :goto_1
    iget-object v0, p0, Leeq;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-wide v2, p0, Leeq;->f:J

    iget-object v0, p0, Leeq;->c:Ljava/util/Stack;

    .line 129
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leer;

    iget-wide v4, v0, Leer;->b:J

    cmp-long v0, v2, v4

    if-ltz v0, :cond_2

    .line 130
    iget-object v1, p0, Leeq;->d:Lees;

    iget-object v0, p0, Leeq;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leer;

    iget v0, v0, Leer;->a:I

    invoke-interface {v1, v0}, Lees;->b(I)V

    .line 234
    :cond_0
    :goto_2
    return v7

    :cond_1
    move v0, v7

    .line 126
    goto :goto_0

    .line 134
    :cond_2
    iget v0, p0, Leeq;->e:I

    if-nez v0, :cond_b

    .line 135
    iget v0, p0, Leeq;->i:I

    if-ne v0, v6, :cond_4

    move v0, v7

    .line 136
    :cond_3
    :goto_3
    if-eqz v0, :cond_6

    move v7, v0

    .line 137
    goto :goto_2

    .line 135
    :cond_4
    iget v0, p0, Leeq;->i:I

    if-nez v0, :cond_5

    iput v7, p0, Leeq;->l:I

    iput v1, p0, Leeq;->i:I

    :cond_5
    invoke-direct {p0, p1}, Leeq;->c(Lefx;)I

    move-result v0

    if-nez v0, :cond_3

    iget v0, p0, Leeq;->m:I

    invoke-direct {p0, v0, v7}, Leeq;->a(IZ)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, p0, Leeq;->h:I

    iput v6, p0, Leeq;->i:I

    move v0, v7

    goto :goto_3

    .line 139
    :cond_6
    iget v0, p0, Leeq;->k:I

    if-ne v0, v6, :cond_8

    move v0, v7

    .line 140
    :cond_7
    :goto_4
    if-eqz v0, :cond_a

    move v7, v0

    .line 141
    goto :goto_2

    .line 139
    :cond_8
    iget v0, p0, Leeq;->k:I

    if-nez v0, :cond_9

    iput v7, p0, Leeq;->l:I

    iput v1, p0, Leeq;->k:I

    :cond_9
    invoke-direct {p0, p1}, Leeq;->c(Lefx;)I

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Leeq;->m:I

    invoke-direct {p0, v0, v1}, Leeq;->a(IZ)J

    move-result-wide v2

    iput-wide v2, p0, Leeq;->j:J

    iput v6, p0, Leeq;->k:I

    move v0, v7

    goto :goto_4

    .line 143
    :cond_a
    iput v1, p0, Leeq;->e:I

    .line 144
    iput v7, p0, Leeq;->n:I

    .line 147
    :cond_b
    iget-object v0, p0, Leeq;->d:Lees;

    iget v2, p0, Leeq;->h:I

    invoke-interface {v0, v2}, Lees;->a(I)I

    move-result v0

    .line 148
    packed-switch v0, :pswitch_data_0

    .line 239
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid element type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 150
    :pswitch_0
    iget-wide v0, p0, Leeq;->f:J

    iget-wide v2, p0, Leeq;->g:J

    sub-long/2addr v0, v2

    long-to-int v4, v0

    .line 151
    iget-object v0, p0, Leeq;->c:Ljava/util/Stack;

    new-instance v1, Leer;

    iget v2, p0, Leeq;->h:I

    iget-wide v8, p0, Leeq;->f:J

    iget-wide v10, p0, Leeq;->j:J

    add-long/2addr v8, v10

    invoke-direct {v1, v2, v8, v9}, Leer;-><init>(IJ)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Leeq;->d:Lees;

    iget v1, p0, Leeq;->h:I

    iget-wide v2, p0, Leeq;->g:J

    iget-wide v5, p0, Leeq;->j:J

    invoke-interface/range {v0 .. v6}, Lees;->a(IJIJ)V

    .line 154
    invoke-direct {p0}, Leeq;->c()V

    goto/16 :goto_2

    .line 157
    :pswitch_1
    iget-wide v0, p0, Leeq;->j:J

    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-lez v0, :cond_c

    .line 158
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-wide v2, p0, Leeq;->j:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x29

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid integer size "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_c
    iget-object v0, p0, Leeq;->b:[B

    iget-wide v2, p0, Leeq;->j:J

    long-to-int v1, v2

    .line 161
    invoke-direct {p0, p1, v0, v1}, Leeq;->b(Lefx;[BI)I

    move-result v1

    .line 162
    if-eqz v1, :cond_d

    move v7, v1

    .line 163
    goto/16 :goto_2

    .line 165
    :cond_d
    iget-wide v0, p0, Leeq;->j:J

    long-to-int v0, v0

    invoke-direct {p0, v0, v7}, Leeq;->a(IZ)J

    move-result-wide v0

    .line 166
    iget-object v2, p0, Leeq;->d:Lees;

    iget v3, p0, Leeq;->h:I

    invoke-interface {v2, v3, v0, v1}, Lees;->a(IJ)V

    .line 167
    invoke-direct {p0}, Leeq;->c()V

    goto/16 :goto_2

    .line 170
    :pswitch_2
    iget-wide v0, p0, Leeq;->j:J

    const-wide/16 v2, 0x4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    iget-wide v0, p0, Leeq;->j:J

    const-wide/16 v2, 0x8

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    .line 172
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-wide v2, p0, Leeq;->j:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid float size "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_e
    iget-object v0, p0, Leeq;->b:[B

    iget-wide v2, p0, Leeq;->j:J

    long-to-int v1, v2

    .line 175
    invoke-direct {p0, p1, v0, v1}, Leeq;->b(Lefx;[BI)I

    move-result v1

    .line 176
    if-eqz v1, :cond_f

    move v7, v1

    .line 177
    goto/16 :goto_2

    .line 179
    :cond_f
    iget-wide v0, p0, Leeq;->j:J

    long-to-int v0, v0

    invoke-direct {p0, v0, v7}, Leeq;->a(IZ)J

    move-result-wide v0

    .line 181
    iget-wide v2, p0, Leeq;->j:J

    const-wide/16 v4, 0x4

    cmp-long v2, v2, v4

    if-nez v2, :cond_10

    .line 182
    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    float-to-double v0, v0

    .line 186
    :goto_5
    iget-object v2, p0, Leeq;->d:Lees;

    iget v3, p0, Leeq;->h:I

    invoke-interface {v2, v3, v0, v1}, Lees;->a(ID)V

    .line 187
    invoke-direct {p0}, Leeq;->c()V

    goto/16 :goto_2

    .line 184
    :cond_10
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    goto :goto_5

    .line 190
    :pswitch_3
    iget-wide v0, p0, Leeq;->j:J

    cmp-long v0, v0, v8

    if-lez v0, :cond_11

    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-wide v2, p0, Leeq;->j:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "String element size "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is larger than MAX_INT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 194
    :cond_11
    iget-object v0, p0, Leeq;->o:[B

    if-nez v0, :cond_12

    .line 195
    iget-wide v0, p0, Leeq;->j:J

    long-to-int v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Leeq;->o:[B

    .line 197
    :cond_12
    iget-object v0, p0, Leeq;->o:[B

    iget-wide v2, p0, Leeq;->j:J

    long-to-int v1, v2

    .line 198
    invoke-direct {p0, p1, v0, v1}, Leeq;->b(Lefx;[BI)I

    move-result v1

    .line 199
    if-eqz v1, :cond_13

    move v7, v1

    .line 200
    goto/16 :goto_2

    .line 202
    :cond_13
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Leeq;->o:[B

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    .line 203
    const/4 v1, 0x0

    iput-object v1, p0, Leeq;->o:[B

    .line 204
    iget-object v1, p0, Leeq;->d:Lees;

    iget v2, p0, Leeq;->h:I

    invoke-interface {v1, v2, v0}, Lees;->a(ILjava/lang/String;)V

    .line 205
    invoke-direct {p0}, Leeq;->c()V

    goto/16 :goto_2

    .line 208
    :pswitch_4
    iget-wide v2, p0, Leeq;->j:J

    cmp-long v0, v2, v8

    if-lez v0, :cond_14

    .line 209
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-wide v2, p0, Leeq;->j:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Binary element size "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is larger than MAX_INT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 212
    :cond_14
    invoke-virtual {p1}, Lefx;->a()J

    move-result-wide v2

    iget-wide v4, p0, Leeq;->j:J

    cmp-long v0, v2, v4

    if-gez v0, :cond_15

    move v7, v1

    .line 213
    goto/16 :goto_2

    .line 215
    :cond_15
    iget-wide v0, p0, Leeq;->f:J

    iget-wide v2, p0, Leeq;->g:J

    sub-long/2addr v0, v2

    long-to-int v4, v0

    .line 216
    iget-object v0, p0, Leeq;->d:Lees;

    iget v1, p0, Leeq;->h:I

    iget-wide v2, p0, Leeq;->g:J

    iget-wide v8, p0, Leeq;->j:J

    long-to-int v5, v8

    move-object v6, p1

    invoke-interface/range {v0 .. v6}, Lees;->a(IJIILefx;)Z

    move-result v0

    .line 218
    if-eqz v0, :cond_0

    .line 219
    iget-wide v0, p0, Leeq;->g:J

    int-to-long v2, v4

    add-long/2addr v0, v2

    iget-wide v2, p0, Leeq;->j:J

    add-long/2addr v0, v2

    .line 220
    iget-wide v2, p0, Leeq;->f:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_16

    .line 221
    new-instance v2, Ljava/lang/IllegalStateException;

    iget-wide v4, p0, Leeq;->f:J

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v6, 0x5b

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Incorrect total bytes read. Expected "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " but actually "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 224
    :cond_16
    invoke-direct {p0}, Leeq;->c()V

    goto/16 :goto_2

    .line 228
    :pswitch_5
    iget-wide v2, p0, Leeq;->j:J

    cmp-long v0, v2, v8

    if-lez v0, :cond_17

    .line 229
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-wide v2, p0, Leeq;->j:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x40

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unknown element size "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is larger than MAX_INT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_17
    iget-wide v2, p0, Leeq;->j:J

    long-to-int v0, v2

    invoke-direct {p0, p1, v0}, Leeq;->b(Lefx;I)I

    move-result v0

    .line 233
    if-eqz v0, :cond_18

    move v7, v0

    .line 234
    goto/16 :goto_2

    .line 236
    :cond_18
    invoke-direct {p0}, Leeq;->c()V

    goto/16 :goto_1

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public final a()J
    .locals 2

    .prologue
    .line 246
    iget-wide v0, p0, Leeq;->f:J

    return-wide v0
.end method

.method public final a(Lees;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Leeq;->d:Lees;

    .line 122
    return-void
.end method

.method public final a(Lefx;I)V
    .locals 2

    .prologue
    .line 286
    const/4 v0, 0x0

    iput v0, p0, Leeq;->n:I

    .line 287
    invoke-direct {p0, p1, p2}, Leeq;->b(Lefx;I)I

    move-result v0

    .line 288
    if-eqz v0, :cond_0

    .line 289
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t skip bytes"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    return-void
.end method

.method public final a(Lefx;Ljava/nio/ByteBuffer;I)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 268
    iput v0, p0, Leeq;->n:I

    .line 269
    iget v1, p0, Leeq;->n:I

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-le p3, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Byte buffer not large enough"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v1, p0, Leeq;->n:I

    if-lt v1, p3, :cond_1

    .line 270
    :goto_0
    if-eqz v0, :cond_2

    .line 271
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t read bytes into buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_1
    iget v0, p0, Leeq;->n:I

    sub-int v0, p3, v0

    invoke-virtual {p1, p2, v0}, Lefx;->a(Ljava/nio/ByteBuffer;I)I

    move-result v0

    invoke-direct {p0, v0, p3}, Leeq;->a(II)I

    move-result v0

    goto :goto_0

    .line 273
    :cond_2
    return-void
.end method

.method public final a(Lefx;[BI)V
    .locals 2

    .prologue
    .line 277
    const/4 v0, 0x0

    iput v0, p0, Leeq;->n:I

    .line 278
    invoke-direct {p0, p1, p2, p3}, Leeq;->b(Lefx;[BI)I

    move-result v0

    .line 279
    if-eqz v0, :cond_0

    .line 280
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t read bytes into array"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_0
    return-void
.end method

.method public final b(Lefx;)J
    .locals 2

    .prologue
    .line 258
    const/4 v0, 0x0

    iput v0, p0, Leeq;->l:I

    .line 259
    invoke-direct {p0, p1}, Leeq;->c(Lefx;)I

    move-result v0

    .line 260
    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Couldn\'t read varint"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_0
    iget v0, p0, Leeq;->m:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Leeq;->a(IZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 251
    invoke-direct {p0}, Leeq;->c()V

    .line 252
    iget-object v0, p0, Leeq;->c:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 253
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Leeq;->f:J

    .line 254
    return-void
.end method
