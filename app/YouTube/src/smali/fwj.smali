.class public final Lfwj;
.super Lghl;
.source "SourceFile"

# interfaces
.implements Lfws;


# instance fields
.field private final a:Lgku;

.field private final b:Lfap;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfap;Lgho;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    .line 51
    :try_start_0
    new-instance v0, Ljava/lang/String;

    .line 52
    invoke-interface {p4}, Lgho;->a()[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfwj;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    const-string v0, "serial cannot be null or empty"

    invoke-static {p5, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfwj;->d:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lfwj;->b:Lfap;

    .line 61
    invoke-interface {p4}, Lgho;->b()[B

    move-result-object v0

    new-instance v1, Lgik;

    sget-object v2, Lewv;->c:Lewv;

    invoke-direct {v1, v2}, Lgik;-><init>(Lewv;)V

    new-instance v2, Lfwt;

    invoke-direct {v2, v0}, Lfwt;-><init>([B)V

    invoke-virtual {p0, v1, v2}, Lfwj;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iput-object v0, p0, Lfwj;->a:Lgku;

    .line 62
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 55
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Leuc;)V
    .locals 4

    .prologue
    .line 72
    iget-object v0, p0, Lfwj;->b:Lfap;

    if-nez v0, :cond_0

    const-string v0, "https://www.google.com/youtube/accounts/registerDevice"

    .line 74
    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "?developer=%s&serialNumber=%s"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 76
    :goto_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lfwj;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lfwj;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lfwj;->a:Lgku;

    invoke-interface {v1, v0, p1}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 78
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lfwj;->b:Lfap;

    const-string v1, "https://www.google.com/youtube/accounts/registerDevice"

    .line 73
    invoke-virtual {v0, v1}, Lfap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
