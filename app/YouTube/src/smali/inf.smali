.class public final Linf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Linn;


# instance fields
.field private final a:Linc;

.field private final b:Limt;


# direct methods
.method public constructor <init>(Linc;Limt;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Linf;->a:Linc;

    .line 31
    iput-object p2, p0, Linf;->b:Limt;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Limb;)Liqk;
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/16 v6, 0x12

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 35
    invoke-static {p1}, Ling;->a(Limb;)J

    move-result-wide v2

    .line 37
    iget-object v0, p0, Linf;->a:Linc;

    iget-boolean v0, v0, Linc;->j:Z

    if-eqz v0, :cond_2

    .line 38
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    cmp-long v0, v2, v8

    if-eqz v0, :cond_1

    .line 45
    invoke-virtual {p0, p1}, Linf;->b(Limb;)V

    .line 46
    new-instance v0, Lini;

    long-to-int v1, v2

    invoke-direct {v0, v1}, Lini;-><init>(I)V

    .line 64
    :goto_0
    return-object v0

    .line 51
    :cond_1
    new-instance v0, Lini;

    invoke-direct {v0}, Lini;-><init>()V

    goto :goto_0

    .line 55
    :cond_2
    const-string v0, "chunked"

    const-string v1, "Transfer-Encoding"

    invoke-virtual {p1, v1}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 57
    invoke-virtual {p0, p1}, Linf;->b(Limb;)V

    .line 58
    iget-object v1, p0, Linf;->b:Limt;

    iget v0, v1, Limt;->e:I

    if-eq v0, v4, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, v1, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput v5, v1, Limt;->e:I

    new-instance v0, Limv;

    invoke-direct {v0, v1}, Limv;-><init>(Limt;)V

    goto :goto_0

    .line 61
    :cond_4
    cmp-long v0, v2, v8

    if-eqz v0, :cond_6

    .line 63
    invoke-virtual {p0, p1}, Linf;->b(Limb;)V

    .line 64
    iget-object v1, p0, Linf;->b:Limt;

    iget v0, v1, Limt;->e:I

    if-eq v0, v4, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, v1, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iput v5, v1, Limt;->e:I

    new-instance v0, Limx;

    invoke-direct {v0, v1, v2, v3}, Limx;-><init>(Limt;J)V

    goto :goto_0

    .line 67
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lu;)Liql;
    .locals 7

    .prologue
    const/16 v6, 0x12

    const/4 v5, 0x5

    const/4 v4, 0x4

    .line 134
    iget-object v0, p0, Linf;->a:Linc;

    invoke-virtual {v0}, Linc;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Linf;->b:Limt;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Limt;->a(Lu;J)Liql;

    move-result-object v0

    .line 150
    :goto_0
    return-object v0

    .line 138
    :cond_0
    const-string v0, "chunked"

    iget-object v1, p0, Linf;->a:Linc;

    invoke-virtual {v1}, Linc;->e()Limd;

    move-result-object v1

    const-string v2, "Transfer-Encoding"

    invoke-virtual {v1, v2}, Limd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    iget-object v1, p0, Linf;->b:Limt;

    iget-object v2, p0, Linf;->a:Linc;

    iget v0, v1, Limt;->e:I

    if-eq v0, v4, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, v1, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput v5, v1, Limt;->e:I

    new-instance v0, Limw;

    invoke-direct {v0, v1, p1, v2}, Limw;-><init>(Limt;Lu;Linc;)V

    goto :goto_0

    .line 142
    :cond_2
    iget-object v0, p0, Linf;->a:Linc;

    invoke-virtual {v0}, Linc;->e()Limd;

    move-result-object v0

    invoke-static {v0}, Ling;->a(Limd;)J

    move-result-wide v0

    .line 143
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    .line 144
    iget-object v2, p0, Linf;->b:Limt;

    invoke-virtual {v2, p1, v0, v1}, Limt;->a(Lu;J)Liql;

    move-result-object v0

    goto :goto_0

    .line 150
    :cond_3
    iget-object v1, p0, Linf;->b:Limt;

    iget v0, v1, Limt;->e:I

    if-eq v0, v4, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, v1, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iput v5, v1, Limt;->e:I

    new-instance v0, Limz;

    invoke-direct {v0, v1, p1}, Limz;-><init>(Limt;Lu;)V

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Linf;->b:Limt;

    invoke-virtual {v0}, Limt;->a()V

    .line 73
    return-void
.end method

.method public final a(Linc;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Linf;->b:Limt;

    sget-object v1, Limh;->a:Limh;

    iget-object v0, v0, Limt;->b:Liln;

    invoke-virtual {v1, v0, p1}, Limh;->a(Liln;Ljava/lang/Object;)V

    .line 155
    return-void
.end method

.method public final a(Lini;)V
    .locals 4

    .prologue
    .line 76
    iget-object v0, p0, Linf;->b:Limt;

    iget v1, v0, Limt;->e:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    iget v0, v0, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x3

    iput v1, v0, Limt;->e:I

    iget-object v0, v0, Limt;->d:Lipt;

    iget-object v1, p1, Lini;->a:Lipq;

    invoke-virtual {v1}, Lipq;->o()Lipq;

    move-result-object v1

    invoke-interface {v0, v1}, Lipt;->a(Liql;)J

    .line 77
    return-void
.end method

.method public final b()Lime;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Linf;->b:Limt;

    invoke-virtual {v0}, Limt;->c()Lime;

    move-result-object v0

    return-object v0
.end method

.method public final b(Limb;)V
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 92
    iget-object v0, p0, Linf;->a:Linc;

    invoke-virtual {v0}, Linc;->a()V

    .line 93
    iget-object v0, p0, Linf;->a:Linc;

    iget-object v0, v0, Linc;->c:Liln;

    iget-object v0, v0, Liln;->b:Limg;

    iget-object v0, v0, Limg;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    iget-object v1, p0, Linf;->a:Linc;

    iget-object v1, v1, Linc;->c:Liln;

    iget-object v1, v1, Liln;->g:Lima;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p1, Limb;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Limb;->e()Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Limb;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v1}, La;->a(Lima;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    iget-object v1, p0, Linf;->b:Limt;

    iget-object v2, p1, Limb;->c:Lils;

    invoke-virtual {v1, v2, v0}, Limt;->a(Lils;Ljava/lang/String;)V

    .line 97
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Limb;->a()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, La;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 104
    invoke-virtual {p0}, Linf;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Linf;->b:Limt;

    const/4 v1, 0x1

    iput v1, v0, Limt;->f:I

    iget v1, v0, Limt;->e:I

    if-nez v1, :cond_0

    const/4 v1, 0x0

    iput v1, v0, Limt;->f:I

    sget-object v1, Limh;->a:Limh;

    iget-object v2, v0, Limt;->a:Lilo;

    iget-object v0, v0, Limt;->b:Liln;

    invoke-virtual {v1, v2, v0}, Limh;->a(Lilo;Liln;)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Linf;->b:Limt;

    const/4 v1, 0x2

    iput v1, v0, Limt;->f:I

    iget v1, v0, Limt;->e:I

    if-nez v1, :cond_0

    const/4 v1, 0x6

    iput v1, v0, Limt;->e:I

    iget-object v0, v0, Limt;->b:Liln;

    iget-object v0, v0, Liln;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 113
    const-string v2, "close"

    iget-object v3, p0, Linf;->a:Linc;

    iget-object v3, v3, Linc;->k:Limb;

    const-string v4, "Connection"

    invoke-virtual {v3, v4}, Limb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    const-string v2, "close"

    iget-object v3, p0, Linf;->a:Linc;

    invoke-virtual {v3}, Linc;->e()Limd;

    move-result-object v3

    const-string v4, "Connection"

    invoke-virtual {v3, v4}, Limd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    iget-object v2, p0, Linf;->b:Limt;

    iget v2, v2, Limt;->e:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_1
    if-nez v2, :cond_0

    move v0, v1

    .line 126
    goto :goto_0

    :cond_2
    move v2, v0

    .line 122
    goto :goto_1
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Linf;->b:Limt;

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Limt;->a(Lu;J)Liql;

    .line 131
    return-void
.end method
