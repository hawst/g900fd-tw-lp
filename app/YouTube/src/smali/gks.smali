.class public Lgks;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgib;


# instance fields
.field public final a:Lewv;

.field private b:Lgir;

.field private c:Ljava/util/List;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lewv;Lgir;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgir;

    iput-object v0, p0, Lgks;->b:Lgir;

    .line 66
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgks;->c:Ljava/util/List;

    .line 67
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewv;

    iput-object v0, p0, Lgks;->a:Lewv;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lgks;->d:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public constructor <init>(Lewv;Ljava/lang/String;Lgir;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgir;

    iput-object v0, p0, Lgks;->b:Lgir;

    .line 78
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgks;->c:Ljava/util/List;

    .line 79
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewv;

    iput-object v0, p0, Lgks;->a:Lewv;

    .line 80
    const-string v0, "contentType can\'t be null or empty"

    .line 81
    invoke-static {p2, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgks;->d:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public constructor <init>(Lewv;Ljava/util/List;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object v1, p0, Lgks;->b:Lgir;

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lgks;->c:Ljava/util/List;

    .line 48
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewv;

    iput-object v0, p0, Lgks;->a:Lewv;

    .line 49
    iput-object v1, p0, Lgks;->d:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method public synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, Lgkr;

    invoke-virtual {p0, p1}, Lgks;->b(Lgkr;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgkr;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lgks;->a:Lewv;

    iget-object v1, p1, Lgkr;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lewv;->a(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public b(Lgkr;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4

    .prologue
    .line 86
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v0, p1, Lgkr;->c:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgks;->a:Lewv;

    sget-object v1, Lewv;->c:Lewv;

    if-eq v0, v1, :cond_0

    sget-object v1, Lewv;->d:Lewv;

    if-eq v0, v1, :cond_0

    sget-object v1, Lewv;->e:Lewv;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget-object v1, p0, Lgks;->a:Lewv;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Content not allowed [method="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 91
    :cond_2
    invoke-virtual {p0, p1}, Lgks;->a(Lgkr;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    .line 92
    const-string v0, "Accept-Encoding"

    const-string v1, "gzip"

    invoke-interface {v2, v0, v1}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p1, Lgkr;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 95
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 98
    :cond_3
    iget-object v0, p0, Lgks;->b:Lgir;

    if-eqz v0, :cond_4

    .line 101
    :try_start_0
    invoke-interface {v2}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 102
    const-string v1, "X-GData-Device"

    iget-object v3, p0, Lgks;->b:Lgir;

    invoke-interface {v3, v0}, Lgir;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lgis; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :cond_4
    iget-object v0, p0, Lgks;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkv;

    .line 110
    invoke-interface {v0, v2}, Lgkv;->a(Lorg/apache/http/client/methods/HttpUriRequest;)V

    goto :goto_2

    .line 103
    :catch_0
    move-exception v0

    .line 104
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 113
    :cond_5
    iget-object v0, p1, Lgkr;->c:[B

    if-eqz v0, :cond_6

    .line 114
    new-instance v1, Lorg/apache/http/entity/ByteArrayEntity;

    iget-object v0, p1, Lgkr;->c:[B

    invoke-direct {v1, v0}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 115
    iget-object v0, p0, Lgks;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lorg/apache/http/entity/ByteArrayEntity;->setContentType(Ljava/lang/String;)V

    move-object v0, v2

    .line 118
    check-cast v0, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 120
    :cond_6
    return-object v2
.end method
