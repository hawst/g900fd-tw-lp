.class public final Lekh;
.super Lj;

# interfaces
.implements Lan;
.implements Landroid/content/DialogInterface$OnCancelListener;


# instance fields
.field private W:I

.field private X:Leiz;

.field private final Y:Landroid/os/Handler;

.field final a:Landroid/util/SparseArray;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lj;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lekh;->W:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lekh;->Y:Landroid/os/Handler;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lekh;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public static a(Lo;)Lekh;
    .locals 4

    const-string v0, "Must be called from main thread of process"

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lo;->b:Lv;

    :try_start_0
    const-string v0, "GmsSupportLifecycleFragment"

    invoke-virtual {v1, v0}, Lt;->a(Ljava/lang/String;)Lj;

    move-result-object v0

    check-cast v0, Lekh;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lj;->p:Z

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Lekh;

    invoke-direct {v0}, Lekh;-><init>()V

    invoke-virtual {v1}, Lt;->a()Laf;

    move-result-object v2

    const-string v3, "GmsSupportLifecycleFragment"

    invoke-virtual {v2, v0, v3}, Laf;->a(Lj;Ljava/lang/String;)Laf;

    move-result-object v2

    invoke-virtual {v2}, Laf;->a()I

    invoke-virtual {v1}, Lt;->b()Z

    :cond_2
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Fragment with tag GmsSupportLifecycleFragment is not a SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a()V
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lekh;->b:Z

    const/4 v0, -0x1

    iput v0, p0, Lekh;->W:I

    iput-object v6, p0, Lekh;->X:Leiz;

    invoke-virtual {p0}, Lekh;->p()Lam;

    move-result-object v2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lekh;->b(I)Leki;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-boolean v5, v4, Leki;->g:Z

    if-eqz v5, :cond_0

    iput-boolean v1, v4, Leki;->g:Z

    iget-boolean v5, v4, Lcj;->c:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Lcj;->d:Z

    if-nez v5, :cond_0

    iget-object v4, v4, Leki;->f:Lejr;

    invoke-interface {v4}, Lejr;->a()V

    :cond_0
    invoke-virtual {v2, v3, v6, p0}, Lam;->a(ILandroid/os/Bundle;Lan;)Lcj;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(ILeiz;)V
    .locals 2

    const-string v0, "GmsSupportLifecycleFragment"

    const-string v1, "Unresolved error while connecting client. Stopping auto-manage."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekj;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lekh;->p()Lam;

    move-result-object v1

    invoke-virtual {v1, p1}, Lam;->a(I)V

    iget-object v1, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    iget-object v0, v0, Lekj;->b:Lejc;

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lejc;->a(Leiz;)V

    :cond_0
    invoke-direct {p0}, Lekh;->a()V

    return-void
.end method

.method static synthetic a(Lekh;)V
    .locals 0

    invoke-direct {p0}, Lekh;->a()V

    return-void
.end method

.method static synthetic a(Lekh;ILeiz;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lekh;->a(ILeiz;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lj;->a(Landroid/app/Activity;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lekh;->b(I)Leki;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekj;

    iget-object v0, v0, Lekj;->a:Lejr;

    iget-object v3, v3, Leki;->f:Lejr;

    if-eq v0, v3, :cond_0

    invoke-virtual {p0}, Lekh;->p()Lam;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lam;->b(ILandroid/os/Bundle;Lan;)Lcj;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lekh;->p()Lam;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lam;->a(ILandroid/os/Bundle;Lan;)Lcj;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lj;->a(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lekh;->b:Z

    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lekh;->W:I

    iget v0, p0, Lekh;->W:I

    if-ltz v0, :cond_0

    new-instance v1, Leiz;

    const-string v0, "failed_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "failed_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Leiz;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, Lekh;->X:Leiz;

    :cond_0
    return-void
.end method

.method public final a(Lcj;)V
    .locals 2

    iget v0, p1, Lcj;->a:I

    iget v1, p0, Lekh;->W:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lekh;->a()V

    :cond_0
    return-void
.end method

.method public final synthetic a(Lcj;Ljava/lang/Object;)V
    .locals 3

    const/4 v1, 0x1

    check-cast p2, Leiz;

    iget v0, p2, Leiz;->c:I

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    iget v0, p1, Lcj;->a:I

    iget v1, p0, Lekh;->W:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lekh;->a()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget v0, p1, Lcj;->a:I

    iget-boolean v2, p0, Lekh;->b:Z

    if-nez v2, :cond_0

    iput-boolean v1, p0, Lekh;->b:Z

    iput v0, p0, Lekh;->W:I

    iput-object p2, p0, Lekh;->X:Leiz;

    iget-object v1, p0, Lekh;->Y:Landroid/os/Handler;

    new-instance v2, Lekk;

    invoke-direct {v2, p0, v0, p2}, Lekk;-><init>(Lekh;ILeiz;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method

.method final b(I)Leki;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lekh;->p()Lam;

    move-result-object v0

    invoke-virtual {v0, p1}, Lam;->b(I)Lcj;

    move-result-object v0

    check-cast v0, Leki;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown loader in SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(II)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lekh;->a()V

    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, Lj;->w:Lo;

    invoke-static {v2}, Lejf;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    goto :goto_0

    :cond_1
    iget v0, p0, Lekh;->W:I

    iget-object v1, p0, Lekh;->X:Leiz;

    invoke-direct {p0, v0, v1}, Lekh;->a(ILeiz;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b_(I)Lcj;
    .locals 3

    new-instance v1, Leki;

    iget-object v2, p0, Lj;->w:Lo;

    iget-object v0, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekj;

    iget-object v0, v0, Lekj;->a:Lejr;

    invoke-direct {v1, v2, v0}, Leki;-><init>(Landroid/content/Context;Lejr;)V

    return-object v1
.end method

.method public final e()V
    .locals 4

    invoke-super {p0}, Lj;->e()V

    iget-boolean v0, p0, Lekh;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lekh;->p()Lam;

    move-result-object v1

    iget-object v2, p0, Lekh;->a:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lam;->a(ILandroid/os/Bundle;Lan;)Lcj;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lj;->e(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lekh;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v0, p0, Lekh;->W:I

    if-ltz v0, :cond_0

    const-string v0, "failed_client_id"

    iget v1, p0, Lekh;->W:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_status"

    iget-object v1, p0, Lekh;->X:Leiz;

    iget v1, v1, Leiz;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_resolution"

    iget-object v1, p0, Lekh;->X:Leiz;

    iget-object v1, v1, Leiz;->b:Landroid/app/PendingIntent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget v0, p0, Lekh;->W:I

    iget-object v1, p0, Lekh;->X:Leiz;

    invoke-direct {p0, v0, v1}, Lekh;->a(ILeiz;)V

    return-void
.end method
