.class public final Lhhz;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhjx;

.field public b:Z

.field private c:[Lgyk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64229
    invoke-direct {p0}, Lidf;-><init>()V

    .line 64232
    const/4 v0, 0x0

    iput-object v0, p0, Lhhz;->a:Lhjx;

    .line 64235
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhhz;->b:Z

    .line 64238
    sget-object v0, Lgyk;->a:[Lgyk;

    iput-object v0, p0, Lhhz;->c:[Lgyk;

    .line 64229
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 64296
    .line 64297
    iget-object v0, p0, Lhhz;->a:Lhjx;

    if-eqz v0, :cond_3

    .line 64298
    const/4 v0, 0x1

    iget-object v2, p0, Lhhz;->a:Lhjx;

    .line 64299
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 64301
    :goto_0
    iget-boolean v2, p0, Lhhz;->b:Z

    if-eqz v2, :cond_0

    .line 64302
    const/4 v2, 0x3

    iget-boolean v3, p0, Lhhz;->b:Z

    .line 64303
    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 64305
    :cond_0
    iget-object v2, p0, Lhhz;->c:[Lgyk;

    if-eqz v2, :cond_2

    .line 64306
    iget-object v2, p0, Lhhz;->c:[Lgyk;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 64307
    if-eqz v4, :cond_1

    .line 64308
    const/4 v5, 0x4

    .line 64309
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 64306
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 64313
    :cond_2
    iget-object v1, p0, Lhhz;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64314
    iput v0, p0, Lhhz;->J:I

    .line 64315
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64225
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhhz;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhhz;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhhz;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhhz;->a:Lhjx;

    if-nez v0, :cond_2

    new-instance v0, Lhjx;

    invoke-direct {v0}, Lhjx;-><init>()V

    iput-object v0, p0, Lhhz;->a:Lhjx;

    :cond_2
    iget-object v0, p0, Lhhz;->a:Lhjx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhhz;->b:Z

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhhz;->c:[Lgyk;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgyk;

    iget-object v3, p0, Lhhz;->c:[Lgyk;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhhz;->c:[Lgyk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhhz;->c:[Lgyk;

    :goto_2
    iget-object v2, p0, Lhhz;->c:[Lgyk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhhz;->c:[Lgyk;

    new-instance v3, Lgyk;

    invoke-direct {v3}, Lgyk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhz;->c:[Lgyk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhhz;->c:[Lgyk;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhhz;->c:[Lgyk;

    new-instance v3, Lgyk;

    invoke-direct {v3}, Lgyk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhz;->c:[Lgyk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 64277
    iget-object v0, p0, Lhhz;->a:Lhjx;

    if-eqz v0, :cond_0

    .line 64278
    const/4 v0, 0x1

    iget-object v1, p0, Lhhz;->a:Lhjx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64280
    :cond_0
    iget-boolean v0, p0, Lhhz;->b:Z

    if-eqz v0, :cond_1

    .line 64281
    const/4 v0, 0x3

    iget-boolean v1, p0, Lhhz;->b:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 64283
    :cond_1
    iget-object v0, p0, Lhhz;->c:[Lgyk;

    if-eqz v0, :cond_3

    .line 64284
    iget-object v1, p0, Lhhz;->c:[Lgyk;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 64285
    if-eqz v3, :cond_2

    .line 64286
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 64284
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64290
    :cond_3
    iget-object v0, p0, Lhhz;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 64292
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64251
    if-ne p1, p0, :cond_1

    .line 64257
    :cond_0
    :goto_0
    return v0

    .line 64252
    :cond_1
    instance-of v2, p1, Lhhz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 64253
    :cond_2
    check-cast p1, Lhhz;

    .line 64254
    iget-object v2, p0, Lhhz;->a:Lhjx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhz;->a:Lhjx;

    if-nez v2, :cond_3

    :goto_1
    iget-boolean v2, p0, Lhhz;->b:Z

    iget-boolean v3, p1, Lhhz;->b:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhhz;->c:[Lgyk;

    iget-object v3, p1, Lhhz;->c:[Lgyk;

    .line 64256
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhz;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhz;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 64257
    goto :goto_0

    .line 64254
    :cond_4
    iget-object v2, p0, Lhhz;->a:Lhjx;

    iget-object v3, p1, Lhhz;->a:Lhjx;

    invoke-virtual {v2, v3}, Lhjx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 64256
    :cond_5
    iget-object v2, p0, Lhhz;->I:Ljava/util/List;

    iget-object v3, p1, Lhhz;->I:Ljava/util/List;

    .line 64257
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 64261
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 64263
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhz;->a:Lhjx;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 64264
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhhz;->b:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    add-int/2addr v0, v2

    .line 64265
    iget-object v2, p0, Lhhz;->c:[Lgyk;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 64271
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhhz;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_2
    add-int/2addr v0, v1

    .line 64272
    return v0

    .line 64263
    :cond_1
    iget-object v0, p0, Lhhz;->a:Lhjx;

    invoke-virtual {v0}, Lhjx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 64264
    :cond_2
    const/4 v0, 0x2

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    .line 64267
    :goto_3
    iget-object v3, p0, Lhhz;->c:[Lgyk;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 64268
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhhz;->c:[Lgyk;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 64267
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 64268
    :cond_4
    iget-object v2, p0, Lhhz;->c:[Lgyk;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    .line 64271
    :cond_5
    iget-object v1, p0, Lhhz;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
