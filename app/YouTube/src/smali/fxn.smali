.class public final enum Lfxn;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfxn;

.field public static final enum b:Lfxn;

.field public static final enum c:Lfxn;

.field public static final enum d:Lfxn;

.field private static enum e:Lfxn;

.field private static final synthetic f:[Lfxn;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 227
    new-instance v0, Lfxn;

    const-string v1, "LIVE"

    const v2, 0x7f0900cf

    invoke-direct {v0, v1, v3, v2}, Lfxn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfxn;->a:Lfxn;

    .line 228
    new-instance v0, Lfxn;

    const-string v1, "TODAY"

    const v2, 0x7f0900d0

    invoke-direct {v0, v1, v4, v2}, Lfxn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfxn;->b:Lfxn;

    .line 229
    new-instance v0, Lfxn;

    const-string v1, "THIS_WEEK"

    const v2, 0x7f0900d1

    invoke-direct {v0, v1, v5, v2}, Lfxn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfxn;->c:Lfxn;

    .line 230
    new-instance v0, Lfxn;

    const-string v1, "THIS_MONTH"

    const v2, 0x7f0900d2

    invoke-direct {v0, v1, v6, v2}, Lfxn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfxn;->d:Lfxn;

    .line 231
    new-instance v0, Lfxn;

    const-string v1, "ALL_TIME"

    const v2, 0x7f0900d3

    invoke-direct {v0, v1, v7, v2}, Lfxn;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfxn;->e:Lfxn;

    .line 226
    const/4 v0, 0x5

    new-array v0, v0, [Lfxn;

    sget-object v1, Lfxn;->a:Lfxn;

    aput-object v1, v0, v3

    sget-object v1, Lfxn;->b:Lfxn;

    aput-object v1, v0, v4

    sget-object v1, Lfxn;->c:Lfxn;

    aput-object v1, v0, v5

    sget-object v1, Lfxn;->d:Lfxn;

    aput-object v1, v0, v6

    sget-object v1, Lfxn;->e:Lfxn;

    aput-object v1, v0, v7

    sput-object v0, Lfxn;->f:[Lfxn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 238
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfxn;
    .locals 1

    .prologue
    .line 226
    const-class v0, Lfxn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfxn;

    return-object v0
.end method

.method public static values()[Lfxn;
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lfxn;->f:[Lfxn;

    invoke-virtual {v0}, [Lfxn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfxn;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
