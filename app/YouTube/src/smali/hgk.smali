.class public final Lhgk;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhgk;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50651
    const/4 v0, 0x0

    new-array v0, v0, [Lhgk;

    sput-object v0, Lhgk;->a:[Lhgk;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50652
    invoke-direct {p0}, Lidf;-><init>()V

    .line 50655
    const-string v0, ""

    iput-object v0, p0, Lhgk;->b:Ljava/lang/String;

    .line 50658
    const-string v0, ""

    iput-object v0, p0, Lhgk;->c:Ljava/lang/String;

    .line 50652
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 50701
    const/4 v0, 0x0

    .line 50702
    iget-object v1, p0, Lhgk;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 50703
    const/4 v0, 0x1

    iget-object v1, p0, Lhgk;->b:Ljava/lang/String;

    .line 50704
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50706
    :cond_0
    iget-object v1, p0, Lhgk;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 50707
    const/4 v1, 0x2

    iget-object v2, p0, Lhgk;->c:Ljava/lang/String;

    .line 50708
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50710
    :cond_1
    iget-object v1, p0, Lhgk;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50711
    iput v0, p0, Lhgk;->J:I

    .line 50712
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 50648
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgk;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgk;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgk;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgk;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgk;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 50689
    iget-object v0, p0, Lhgk;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50690
    const/4 v0, 0x1

    iget-object v1, p0, Lhgk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 50692
    :cond_0
    iget-object v0, p0, Lhgk;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 50693
    const/4 v0, 0x2

    iget-object v1, p0, Lhgk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 50695
    :cond_1
    iget-object v0, p0, Lhgk;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 50697
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50670
    if-ne p1, p0, :cond_1

    .line 50675
    :cond_0
    :goto_0
    return v0

    .line 50671
    :cond_1
    instance-of v2, p1, Lhgk;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 50672
    :cond_2
    check-cast p1, Lhgk;

    .line 50673
    iget-object v2, p0, Lhgk;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgk;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhgk;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgk;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 50674
    :goto_2
    iget-object v2, p0, Lhgk;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhgk;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 50675
    goto :goto_0

    .line 50673
    :cond_4
    iget-object v2, p0, Lhgk;->b:Ljava/lang/String;

    iget-object v3, p1, Lhgk;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgk;->c:Ljava/lang/String;

    iget-object v3, p1, Lhgk;->c:Ljava/lang/String;

    .line 50674
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhgk;->I:Ljava/util/List;

    iget-object v3, p1, Lhgk;->I:Ljava/util/List;

    .line 50675
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 50679
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 50681
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgk;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 50682
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgk;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 50683
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhgk;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 50684
    return v0

    .line 50681
    :cond_0
    iget-object v0, p0, Lhgk;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 50682
    :cond_1
    iget-object v0, p0, Lhgk;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 50683
    :cond_2
    iget-object v1, p0, Lhgk;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
