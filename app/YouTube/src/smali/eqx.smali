.class public abstract Leqx;
.super Ljava/lang/Object;


# instance fields
.field public a:Landroid/content/Intent;

.field private b:Lera;

.field private c:Leqz;

.field private d:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Leqx;->a:Landroid/content/Intent;

    iget-object v0, p0, Leqx;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Leqx;->a:Landroid/content/Intent;

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Leqx;->d:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->a()Leqz;

    move-result-object v0

    iget-object v1, p0, Leqx;->d:Landroid/os/Bundle;

    iget-object v2, v0, Leqz;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object v1, v2, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d:Landroid/os/Bundle;

    iput-object v0, p0, Leqx;->c:Leqz;

    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a()Lera;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lera;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v1, v2, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d:Ljava/lang/String;

    iget-object v1, v0, Lera;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p3, v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e:Ljava/lang/String;

    iput-object v0, p0, Leqx;->b:Lera;

    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    iget-object v0, p0, Leqx;->b:Lera;

    iget-object v1, p0, Leqx;->c:Leqz;

    iget-object v1, v1, Leqz;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iget-object v2, v0, Lera;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v1, v2, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iget-object v1, v0, Lera;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, v0, Lera;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b:Ljava/lang/String;

    :cond_0
    iget-object v0, v0, Lera;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Leqx;->a:Landroid/content/Intent;

    const-string v2, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Leqx;->a:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Leqx;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    return-object p1
.end method

.method public final a(I)Leqx;
    .locals 1

    iget-object v0, p0, Leqx;->c:Leqz;

    iget-object v0, v0, Leqz;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b:I

    return-object p0
.end method

.method public final a(Landroid/accounts/Account;)Leqx;
    .locals 1

    iget-object v0, p0, Leqx;->c:Leqz;

    iget-object v0, v0, Leqz;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c:Landroid/accounts/Account;

    return-object p0
.end method

.method public final b(I)Leqx;
    .locals 2

    iget-object v0, p0, Leqx;->c:Leqz;

    const/4 v1, 0x1

    iget-object v0, v0, Leqz;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput v1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f:I

    return-object p0
.end method
