.class public final Lfcl;
.super Li;
.source "SourceFile"


# instance fields
.field W:Lfsi;

.field private X:Landroid/view/View;

.field private Y:Lfci;

.field private Z:Lfcd;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Li;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 58
    const v0, 0x7f04001b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lfcl;->X:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lfcl;->X:Landroid/view/View;

    const v1, 0x7f0800a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "loading. . ."

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lfcl;->X:Landroid/view/View;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1}, Li;->a(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 67
    invoke-super {p0, p1}, Li;->d(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lfcl;->j()Lo;

    move-result-object v6

    .line 69
    invoke-virtual {v6}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    move-object v0, v1

    .line 70
    check-cast v0, Letz;

    .line 71
    invoke-interface {v0}, Letz;->p()Letc;

    move-result-object v7

    move-object v0, v1

    .line 72
    check-cast v0, Lghd;

    .line 73
    invoke-interface {v0}, Lghd;->k()Lghc;

    move-result-object v0

    invoke-interface {v0}, Lghc;->c()Leyp;

    move-result-object v3

    move-object v0, v1

    .line 74
    check-cast v0, Lfdu;

    .line 75
    invoke-interface {v0}, Lfdu;->i()Lfdt;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Lfdt;->ae()Lfdw;

    move-result-object v4

    .line 79
    check-cast v1, Lfby;

    .line 80
    invoke-interface {v1}, Lfby;->j()Lfbx;

    move-result-object v1

    invoke-interface {v1}, Lfbx;->az()Lfcd;

    move-result-object v1

    iput-object v1, p0, Lfcl;->Z:Lfcd;

    .line 84
    iget-object v1, p0, Lfcl;->X:Landroid/view/View;

    const v2, 0x7f0800a7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 87
    new-instance v1, Lfci;

    .line 88
    invoke-interface {v0}, Lfdt;->q()Lfcs;

    move-result-object v0

    .line 89
    invoke-virtual {v7}, Letc;->h()Ljava/util/concurrent/Executor;

    move-result-object v8

    .line 90
    invoke-virtual {v7}, Letc;->q()Ljava/util/concurrent/Executor;

    move-result-object v7

    invoke-direct {v1, v0, v8, v7}, Lfci;-><init>(Lfcs;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    iput-object v1, p0, Lfcl;->Y:Lfci;

    .line 92
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lfcl;->W:Lfsi;

    .line 94
    iget-object v7, p0, Lfcl;->W:Lfsi;

    const-class v8, Lfid;

    new-instance v0, Lfcf;

    .line 97
    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-direct/range {v0 .. v5}, Lfcf;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Leyp;Lfdw;Lfrz;)V

    .line 94
    invoke-virtual {v7, v8, v0}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 103
    iget-object v0, p0, Lfcl;->W:Lfsi;

    const-class v1, Lfie;

    new-instance v3, Lfch;

    .line 106
    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    invoke-direct {v3, v6, v2, v4, v5}, Lfch;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Lfdw;Lfrz;)V

    .line 103
    invoke-virtual {v0, v1, v3}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 111
    iget-object v0, p0, Lfcl;->W:Lfsi;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 112
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 116
    invoke-super {p0}, Li;->e()V

    .line 117
    iget-object v0, p0, Lfcl;->Y:Lfci;

    iget-object v1, p0, Lfcl;->Z:Lfcd;

    invoke-virtual {v1}, Lfcd;->a()[Landroid/accounts/Account;

    move-result-object v1

    new-instance v2, Lfcm;

    invoke-direct {v2, p0}, Lfcm;-><init>(Lfcl;)V

    iget-object v3, v0, Lfci;->b:Ljava/util/concurrent/Executor;

    new-instance v4, Lfcj;

    invoke-direct {v4, v0, v1, v2}, Lfcj;-><init>(Lfci;[Landroid/accounts/Account;Lwv;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 118
    return-void
.end method
