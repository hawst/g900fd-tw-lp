.class public final Ldhu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lftg;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Z

.field private final d:Z

.field private final e:Lexd;


# direct methods
.method public constructor <init>(Landroid/telephony/TelephonyManager;Landroid/content/pm/PackageManager;Lexd;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    new-instance v3, Landroid/content/Intent;

    const-string v0, "android.intent.action.DIAL"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    iput v0, p0, Ldhu;->a:I

    .line 39
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    iput v0, p0, Ldhu;->b:I

    .line 40
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ldhu;->c:Z

    .line 41
    invoke-virtual {p2, v3, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Ldhu;->d:Z

    .line 42
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Ldhu;->e:Lexd;

    .line 43
    return-void

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0

    :cond_1
    move v1, v2

    .line 41
    goto :goto_1
.end method


# virtual methods
.method public final a(Lftj;)V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lb;->b()V

    .line 49
    iget-object v0, p0, Ldhu;->e:Lexd;

    .line 50
    invoke-interface {v0}, Lexd;->i()I

    move-result v0

    iput v0, p1, Lftj;->r:I

    iget v0, p0, Ldhu;->a:I

    .line 51
    iput v0, p1, Lftj;->x:I

    iget v0, p0, Ldhu;->b:I

    .line 52
    iput v0, p1, Lftj;->y:I

    iget-boolean v0, p0, Ldhu;->c:Z

    .line 53
    iput-boolean v0, p1, Lftj;->z:Z

    iget-boolean v0, p0, Ldhu;->d:Z

    .line 54
    iput-boolean v0, p1, Lftj;->A:Z

    const/4 v0, 0x0

    .line 56
    iput v0, p1, Lftj;->w:I

    .line 59
    sget-object v0, Lfsq;->a:Lfsq;

    invoke-virtual {p1, v0}, Lftj;->a(Lfsq;)V

    .line 60
    return-void
.end method
