.class public final Laaa;
.super Likv;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:Lzx;

.field private f:J

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 49
    const-string v0, "tfhd"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 41
    iput-wide v2, p0, Laaa;->b:J

    .line 43
    iput-wide v2, p0, Laaa;->c:J

    .line 44
    iput-wide v2, p0, Laaa;->d:J

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    const/high16 v3, 0x10000

    const/4 v2, 0x1

    .line 97
    invoke-virtual {p0, p1}, Laaa;->c(Ljava/nio/ByteBuffer;)J

    .line 98
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaa;->a:J

    .line 99
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    .line 100
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaa;->b:J

    .line 102
    :cond_0
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 103
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaa;->f:J

    .line 105
    :cond_1
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 106
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaa;->c:J

    .line 108
    :cond_2
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 109
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaa;->d:J

    .line 111
    :cond_3
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 112
    new-instance v0, Lzx;

    invoke-direct {v0, p1}, Lzx;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v0, p0, Laaa;->e:Lzx;

    .line 114
    :cond_4
    iget v0, p0, Likv;->p:I

    and-int/2addr v0, v3

    if-ne v0, v3, :cond_5

    .line 115
    iput-boolean v2, p0, Laaa;->g:Z

    .line 117
    :cond_5
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Laaa;->d(Ljava/nio/ByteBuffer;)V

    .line 76
    iget-wide v0, p0, Laaa;->a:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 78
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 79
    iget-wide v0, p0, Laaa;->b:J

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 81
    :cond_0
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 82
    iget-wide v0, p0, Laaa;->f:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 84
    :cond_1
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 85
    iget-wide v0, p0, Laaa;->c:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 87
    :cond_2
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_3

    .line 88
    iget-wide v0, p0, Laaa;->d:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 90
    :cond_3
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    .line 91
    iget-object v0, p0, Laaa;->e:Lzx;

    invoke-virtual {v0, p1}, Lzx;->a(Ljava/nio/ByteBuffer;)V

    .line 93
    :cond_4
    return-void
.end method

.method protected final d_()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x4

    .line 53
    const-wide/16 v0, 0x8

    .line 54
    iget v2, p0, Likv;->p:I

    .line 55
    and-int/lit8 v3, v2, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 56
    const-wide/16 v0, 0x10

    .line 58
    :cond_0
    and-int/lit8 v3, v2, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 59
    add-long/2addr v0, v6

    .line 61
    :cond_1
    and-int/lit8 v3, v2, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_2

    .line 62
    add-long/2addr v0, v6

    .line 64
    :cond_2
    and-int/lit8 v3, v2, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_3

    .line 65
    add-long/2addr v0, v6

    .line 67
    :cond_3
    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_4

    .line 68
    add-long/2addr v0, v6

    .line 70
    :cond_4
    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 211
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    const-string v1, "TrackFragmentHeaderBox"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    const-string v1, "{trackId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Laaa;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 214
    const-string v1, ", baseDataOffset="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Laaa;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 215
    const-string v1, ", sampleDescriptionIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Laaa;->f:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 216
    const-string v1, ", defaultSampleDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Laaa;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 217
    const-string v1, ", defaultSampleSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Laaa;->d:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 218
    const-string v1, ", defaultSampleFlags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Laaa;->e:Lzx;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 219
    const-string v1, ", durationIsEmpty="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Laaa;->g:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 220
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 221
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
