.class public final enum Lgwi;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgwi;

.field public static final enum b:Lgwi;

.field public static final enum c:Lgwi;

.field private static final synthetic d:[Lgwi;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 268
    new-instance v0, Lgwi;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, Lgwi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwi;->a:Lgwi;

    .line 270
    new-instance v0, Lgwi;

    const-string v1, "MINIMAL"

    invoke-direct {v0, v1, v3}, Lgwi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwi;->b:Lgwi;

    .line 272
    new-instance v0, Lgwi;

    const-string v1, "CHROMELESS"

    invoke-direct {v0, v1, v4}, Lgwi;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwi;->c:Lgwi;

    .line 266
    const/4 v0, 0x3

    new-array v0, v0, [Lgwi;

    sget-object v1, Lgwi;->a:Lgwi;

    aput-object v1, v0, v2

    sget-object v1, Lgwi;->b:Lgwi;

    aput-object v1, v0, v3

    sget-object v1, Lgwi;->c:Lgwi;

    aput-object v1, v0, v4

    sput-object v0, Lgwi;->d:[Lgwi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgwi;
    .locals 1

    .prologue
    .line 266
    const-class v0, Lgwi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgwi;

    return-object v0
.end method

.method public static values()[Lgwi;
    .locals 1

    .prologue
    .line 266
    sget-object v0, Lgwi;->d:[Lgwi;

    invoke-virtual {v0}, [Lgwi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgwi;

    return-object v0
.end method
