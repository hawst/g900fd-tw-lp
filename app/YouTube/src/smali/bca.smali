.class public final Lbca;
.super Levy;
.source "SourceFile"

# interfaces
.implements Lbcb;


# instance fields
.field private a:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Levy;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbca;->a:Ljava/util/List;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Letc;Lari;)Ljava/util/List;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lbca;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lbca;->a:Ljava/util/List;

    .line 77
    :goto_0
    return-object v0

    .line 55
    :cond_0
    invoke-static {p1}, La;->n(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const-string v0, "com.google.android.apps.youtube.app.extension.DevConfiguration"

    .line 64
    :goto_1
    :try_start_0
    const-class v1, Levy;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 66
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 67
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcb;

    .line 68
    invoke-interface {v0, p1, p2, p3}, Lbcb;->a(Landroid/content/Context;Letc;Lari;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbca;->a:Ljava/util/List;

    .line 72
    iget-object v0, p0, Lbca;->a:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 57
    :cond_1
    invoke-static {p1}, La;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    const-string v0, "com.google.android.apps.youtube.app.extension.DogfoodConfiguration"

    goto :goto_1

    .line 60
    :cond_2
    const-string v0, "com.google.android.apps.youtube.app.extension.ReleaseConfiguration"

    goto :goto_1

    .line 76
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbca;->a:Ljava/util/List;

    .line 77
    iget-object v0, p0, Lbca;->a:Ljava/util/List;

    goto :goto_0

    .line 79
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 81
    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 83
    :catch_3
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 85
    :catch_4
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method
