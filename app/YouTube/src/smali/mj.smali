.class final Lmj;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private synthetic a:I

.field private synthetic b:Lmk;


# direct methods
.method constructor <init>(ILmk;)V
    .locals 0

    .prologue
    .line 176
    iput p1, p0, Lmj;->a:I

    iput-object p2, p0, Lmj;->b:Lmk;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 176
    check-cast p1, [Landroid/graphics/Bitmap;

    aget-object v1, p1, v5

    iget v2, p0, Lmj;->a:I

    invoke-static {v1}, Lmi;->a(Landroid/graphics/Bitmap;)V

    invoke-static {v2}, Lmi;->a(I)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v3, 0x64

    if-gt v0, v3, :cond_1

    move-object v0, v1

    :goto_0
    invoke-static {v0, v2}, Lme;->a(Landroid/graphics/Bitmap;I)Lme;

    move-result-object v2

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    new-instance v0, Lmi;

    iget-object v1, v2, Lme;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Lmi;-><init>(Ljava/util/List;)V

    return-object v0

    :cond_1
    const/high16 v3, 0x42c80000    # 100.0f

    int-to-float v0, v0

    div-float v0, v3, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v1, v3, v0, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 176
    check-cast p1, Lmi;

    iget-object v0, p0, Lmj;->b:Lmk;

    invoke-interface {v0, p1}, Lmk;->a(Lmi;)V

    return-void
.end method
