.class public final Lcpo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;
.implements Ljava/util/Observer;


# static fields
.field public static final a:Landroid/util/SparseArray;


# instance fields
.field private final A:Lcpp;

.field private final B:Lggm;

.field private final C:Lcpr;

.field private final D:Lgfx;

.field private final E:Lcpq;

.field private final F:Ldmc;

.field private G:J

.field public final b:Landroid/net/Uri;

.field public final c:J

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Lcpt;

.field public final h:Lcpp;

.field public final i:Lcpx;

.field public j:Lcpu;

.field public k:I

.field public l:I

.field public m:Ljava/lang/String;

.field public n:I

.field public o:Ljava/lang/String;

.field public p:I

.field public q:J

.field public r:Z

.field private final s:Lgjp;

.field private final t:Lezj;

.field private final u:Levn;

.field private final v:Lexd;

.field private final w:Lezi;

.field private final x:Lggr;

.field private final y:Lefj;

.field private final z:Lefj;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 132
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 133
    sput-object v0, Lcpo;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "i"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 134
    sget-object v0, Lcpo;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "m"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 135
    sget-object v0, Lcpo;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "a"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 136
    return-void
.end method

.method private constructor <init>(Levn;Lezj;Lgjp;Lexd;Lezi;Lggr;Lefj;Lefj;Lggm;Lgfx;JJJLdmc;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcpt;JZLjava/lang/String;)V
    .locals 11

    .prologue
    .line 414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    const/4 v2, -0x1

    iput v2, p0, Lcpo;->k:I

    .line 415
    iput-object p1, p0, Lcpo;->u:Levn;

    .line 416
    iput-object p2, p0, Lcpo;->t:Lezj;

    .line 417
    iput-object p3, p0, Lcpo;->s:Lgjp;

    .line 418
    iput-object p4, p0, Lcpo;->v:Lexd;

    .line 419
    move-object/from16 v0, p5

    iput-object v0, p0, Lcpo;->w:Lezi;

    .line 420
    move-object/from16 v0, p6

    iput-object v0, p0, Lcpo;->x:Lggr;

    .line 421
    move-object/from16 v0, p7

    iput-object v0, p0, Lcpo;->y:Lefj;

    .line 422
    move-object/from16 v0, p8

    iput-object v0, p0, Lcpo;->z:Lefj;

    .line 423
    new-instance v2, Lcpp;

    invoke-direct {v2, p0}, Lcpp;-><init>(Lcpo;)V

    iput-object v2, p0, Lcpo;->h:Lcpp;

    .line 424
    new-instance v2, Lcpp;

    invoke-direct {v2, p0}, Lcpp;-><init>(Lcpo;)V

    iput-object v2, p0, Lcpo;->A:Lcpp;

    .line 425
    move-object/from16 v0, p9

    iput-object v0, p0, Lcpo;->B:Lggm;

    .line 426
    new-instance v3, Lcpr;

    move-wide/from16 v4, p11

    move-wide/from16 v6, p13

    move-wide/from16 v8, p15

    invoke-direct/range {v3 .. v9}, Lcpr;-><init>(JJJ)V

    iput-object v3, p0, Lcpo;->C:Lcpr;

    .line 428
    move-object/from16 v0, p10

    iput-object v0, p0, Lcpo;->D:Lgfx;

    .line 429
    new-instance v2, Lcpq;

    invoke-direct {v2}, Lcpq;-><init>()V

    iput-object v2, p0, Lcpo;->E:Lcpq;

    .line 430
    move-object/from16 v0, p17

    iput-object v0, p0, Lcpo;->F:Ldmc;

    .line 432
    invoke-static/range {p18 .. p18}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcpo;->b:Landroid/net/Uri;

    .line 433
    move-object/from16 v0, p19

    iput-object v0, p0, Lcpo;->d:Ljava/lang/String;

    .line 434
    invoke-static/range {p20 .. p20}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcpo;->e:Ljava/lang/String;

    .line 435
    move-object/from16 v0, p21

    iput-object v0, p0, Lcpo;->g:Lcpt;

    .line 437
    new-instance v2, Lcpx;

    invoke-direct {v2}, Lcpx;-><init>()V

    iput-object v2, p0, Lcpo;->i:Lcpx;

    .line 439
    const-wide/16 v2, 0x0

    cmp-long v2, p22, v2

    if-gez v2, :cond_0

    .line 441
    invoke-virtual {p2}, Lezj;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lcpo;->c:J

    .line 443
    iget-object v2, p0, Lcpo;->i:Lcpx;

    const-string v3, "vps"

    invoke-virtual {v2, v3}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v2

    sget-object v3, Lcpu;->d:Lcpu;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "0.000:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    sget-object v2, Lcpu;->d:Lcpu;

    iput-object v2, p0, Lcpo;->j:Lcpu;

    .line 450
    :goto_0
    move/from16 v0, p24

    iput-boolean v0, p0, Lcpo;->r:Z

    .line 451
    move-object/from16 v0, p25

    iput-object v0, p0, Lcpo;->f:Ljava/lang/String;

    .line 453
    invoke-virtual {p2}, Lezj;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x7530

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcpo;->G:J

    .line 454
    invoke-direct {p0}, Lcpo;->e()V

    .line 455
    return-void

    .line 447
    :cond_0
    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcpo;->c:J

    .line 448
    sget-object v2, Lcpu;->e:Lcpu;

    iput-object v2, p0, Lcpo;->j:Lcpu;

    goto :goto_0
.end method

.method public synthetic constructor <init>(Levn;Lezj;Lgjp;Lexd;Lezi;Lggr;Lefj;Lefj;Lggm;Lgfx;JJJLdmc;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcpt;JZLjava/lang/String;B)V
    .locals 1

    .prologue
    .line 52
    invoke-direct/range {p0 .. p25}, Lcpo;-><init>(Levn;Lezj;Lgjp;Lexd;Lezi;Lggr;Lefj;Lefj;Lggm;Lgfx;JJJLdmc;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcpt;JZLjava/lang/String;)V

    return-void
.end method

.method public static synthetic a(Lcpo;I)I
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcpo;->l:I

    return p1
.end method

.method public static synthetic a(Lcpo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcpo;->m:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcpo;Z)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcpo;->a(Z)V

    return-void
.end method

.method public static synthetic b(Lcpo;I)I
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcpo;->n:I

    return p1
.end method

.method public static synthetic b(Lcpo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcpo;->o:Ljava/lang/String;

    return-object p1
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 739
    iget-object v2, p0, Lcpo;->w:Lezi;

    invoke-virtual {v2}, Lezi;->a()F

    move-result v2

    .line 740
    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_0

    .line 741
    iget-object v3, p0, Lcpo;->i:Lcpx;

    const-string v4, "bat"

    invoke-virtual {v3, v4}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s:%.3f:%d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    .line 742
    invoke-virtual {p0}, Lcpo;->c()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    .line 743
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v6, v0

    const/4 v7, 0x2

    iget-object v2, p0, Lcpo;->w:Lezi;

    .line 744
    invoke-virtual {v2}, Lezi;->b()Landroid/os/Bundle;

    move-result-object v2

    const-string v8, "plugged"

    invoke-virtual {v2, v8, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v7

    .line 741
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 746
    :cond_0
    return-void

    :cond_1
    move v2, v1

    .line 744
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 522
    sget-object v0, Lcpu;->d:Lcpu;

    invoke-virtual {p0, v0}, Lcpo;->a(Lcpu;)V

    .line 524
    iget-boolean v0, p0, Lcpo;->r:Z

    if-nez v0, :cond_0

    .line 525
    invoke-virtual {p0}, Lcpo;->d()V

    .line 527
    :cond_0
    return-void
.end method

.method public a(Lcpu;)V
    .locals 6

    .prologue
    .line 731
    iget-object v0, p0, Lcpo;->j:Lcpu;

    invoke-virtual {v0, p1}, Lcpu;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 736
    :goto_0
    return-void

    .line 734
    :cond_0
    iget-object v0, p0, Lcpo;->i:Lcpx;

    const-string v1, "vps"

    invoke-virtual {v0, v1}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0}, Lcpo;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    iput-object p1, p0, Lcpo;->j:Lcpu;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 705
    iget-object v0, p0, Lcpo;->t:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    .line 706
    if-nez p1, :cond_0

    iget-wide v2, p0, Lcpo;->G:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 707
    :cond_0
    invoke-virtual {p0}, Lcpo;->c()Ljava/lang/String;

    move-result-object v2

    .line 708
    iget-object v3, p0, Lcpo;->h:Lcpp;

    iget-object v4, p0, Lcpo;->i:Lcpx;

    const-string v5, "bwm"

    .line 709
    invoke-virtual {v4, v5}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 708
    invoke-virtual {v3, v2, v4}, Lcpp;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 710
    iget-object v3, p0, Lcpo;->A:Lcpp;

    iget-object v4, p0, Lcpo;->i:Lcpx;

    const-string v5, "obwm"

    .line 711
    invoke-virtual {v4, v5}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 710
    invoke-virtual {v3, v2, v4}, Lcpp;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 712
    iget-object v3, p0, Lcpo;->C:Lcpr;

    iget-object v4, p0, Lcpo;->i:Lcpx;

    const-string v5, "cache_bytes"

    .line 713
    invoke-virtual {v4, v5}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v4

    .line 712
    iget-wide v6, v3, Lcpr;->f:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Lcpr;->f:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-wide v8, v3, Lcpr;->f:J

    .line 714
    :cond_1
    iget v3, p0, Lcpo;->p:I

    if-lez v3, :cond_2

    .line 715
    iget-object v3, p0, Lcpo;->i:Lcpx;

    const-string v4, "df"

    invoke-virtual {v3, v4}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 716
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    .line 717
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcpo;->p:I

    .line 718
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 719
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 715
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 720
    const/4 v2, 0x0

    iput v2, p0, Lcpo;->p:I

    .line 722
    :cond_2
    const-wide/16 v2, 0x7530

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcpo;->G:J

    .line 724
    :cond_3
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 673
    iget-object v0, p0, Lcpo;->y:Lefj;

    iget-object v1, p0, Lcpo;->h:Lcpp;

    invoke-virtual {v0, v1}, Lefj;->a(Lggl;)V

    .line 674
    iget-object v0, p0, Lcpo;->z:Lefj;

    iget-object v1, p0, Lcpo;->A:Lcpp;

    invoke-virtual {v0, v1}, Lefj;->a(Lggl;)V

    .line 675
    iget-object v0, p0, Lcpo;->B:Lggm;

    iget-object v1, p0, Lcpo;->C:Lcpr;

    iget-object v0, v0, Lggm;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 676
    iget-object v0, p0, Lcpo;->D:Lgfx;

    iget-object v1, p0, Lcpo;->E:Lcpq;

    iget-object v0, v0, Lgfx;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 677
    iget-object v0, p0, Lcpo;->u:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 678
    iget-object v0, p0, Lcpo;->F:Ldmc;

    invoke-virtual {v0, p0}, Ldmc;->addObserver(Ljava/util/Observer;)V

    .line 679
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 8

    .prologue
    .line 727
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.3f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcpo;->t:Lezj;

    invoke-virtual {v4}, Lezj;->b()J

    move-result-wide v4

    iget-wide v6, p0, Lcpo;->c:J

    sub-long/2addr v4, v6

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-wide/16 v8, 0x0

    .line 758
    iget-object v0, p0, Lcpo;->i:Lcpx;

    invoke-virtual {v0}, Lcpx;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 759
    const-string v0, "No ping as there is nothing new to report"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 804
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 758
    goto :goto_0

    .line 763
    :cond_2
    invoke-direct {p0}, Lcpo;->e()V

    .line 765
    iget-object v0, p0, Lcpo;->b:Landroid/net/Uri;

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v3

    .line 767
    const-string v0, "event"

    const-string v4, "streamingstats"

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v4, "cpn"

    iget-object v5, p0, Lcpo;->d:Ljava/lang/String;

    .line 768
    invoke-virtual {v0, v4, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v4, "ns"

    const-string v5, "yt"

    .line 769
    invoke-virtual {v0, v4, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v4, "docid"

    iget-object v5, p0, Lcpo;->e:Ljava/lang/String;

    .line 770
    invoke-virtual {v0, v4, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v4, "conn"

    iget-object v5, p0, Lcpo;->v:Lexd;

    .line 771
    invoke-interface {v5}, Lexd;->i()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lfao;->a(Ljava/lang/String;I)Lfao;

    .line 772
    iget v0, p0, Lcpo;->l:I

    if-lez v0, :cond_3

    .line 773
    const-string v0, "fmt"

    iget v4, p0, Lcpo;->l:I

    invoke-virtual {v3, v0, v4}, Lfao;->a(Ljava/lang/String;I)Lfao;

    .line 775
    :cond_3
    iget-object v0, p0, Lcpo;->g:Lcpt;

    sget-object v4, Lcpt;->a:Lcpt;

    if-eq v0, v4, :cond_9

    move v0, v2

    :goto_2
    if-eqz v0, :cond_4

    .line 776
    const-string v0, "live"

    iget-object v1, p0, Lcpo;->g:Lcpt;

    invoke-virtual {v1}, Lcpt;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 778
    :cond_4
    iget-object v0, p0, Lcpo;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 779
    const-string v0, "adformat"

    iget-object v1, p0, Lcpo;->f:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 781
    :cond_5
    iget-object v0, p0, Lcpo;->x:Lggr;

    invoke-virtual {v0, v3}, Lggr;->a(Lfao;)Lfao;

    .line 782
    iget-object v0, p0, Lcpo;->C:Lcpr;

    iget-boolean v1, v0, Lcpr;->d:Z

    if-nez v1, :cond_6

    iget-wide v4, v0, Lcpr;->a:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    iget-wide v4, v0, Lcpr;->b:J

    cmp-long v1, v4, v6

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v4, v0, Lcpr;->e:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcpr;->c:J

    iget-wide v6, v0, Lcpr;->e:J

    sub-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcpr;->b:J

    iget-wide v6, v0, Lcpr;->a:J

    sub-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ":"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcpr;->a:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "cache_info"

    const-string v5, ",:"

    invoke-virtual {v3, v4, v1, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    iput-boolean v2, v0, Lcpr;->d:Z

    .line 783
    :cond_6
    iget-object v0, p0, Lcpo;->E:Lcpq;

    iget-wide v4, v0, Lcpq;->a:J

    cmp-long v1, v4, v8

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "nr:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, v0, Lcpq;->a:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ",nrw:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcpq;->b:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ",nrww:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcpq;->c:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ",nrb:"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v4, v0, Lcpq;->d:J

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "be_info"

    const-string v5, ",:"

    invoke-virtual {v3, v4, v1, v5}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    iput-wide v8, v0, Lcpq;->a:J

    iput-wide v8, v0, Lcpq;->b:J

    iput-wide v8, v0, Lcpq;->c:J

    iput-wide v8, v0, Lcpq;->d:J

    .line 787
    :cond_7
    iget-object v0, p0, Lcpo;->i:Lcpx;

    invoke-virtual {v0}, Lcpx;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 788
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 789
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 790
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_8

    .line 791
    const-string v5, ","

    invoke-static {v5, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ",:"

    invoke-virtual {v3, v1, v5, v6}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 793
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_3

    :cond_9
    move v0, v1

    .line 775
    goto/16 :goto_2

    .line 797
    :cond_a
    iget-object v0, v3, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 798
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x8

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Pinging "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    .line 799
    iget-object v1, p0, Lcpo;->s:Lgjp;

    const-string v1, "qoe"

    const v3, 0x323467f

    invoke-static {v1, v3}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v1

    .line 801
    invoke-virtual {v1, v0}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    .line 802
    iput-boolean v2, v1, Lgjt;->d:Z

    .line 803
    iget-object v0, p0, Lcpo;->s:Lgjp;

    sget-object v2, Lggu;->a:Lwu;

    invoke-virtual {v0, v1, v2}, Lgjp;->a(Lgjt;Lwu;)V

    goto/16 :goto_1
.end method

.method public final handleMedialibErrorEvent(Lggp;)V
    .locals 8
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 491
    invoke-virtual {p1}, Lggp;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    sget-object v0, Lcpu;->b:Lcpu;

    invoke-virtual {p0, v0}, Lcpo;->a(Lcpu;)V

    .line 494
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s:%s:%.3f"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 495
    invoke-virtual {p0}, Lcpo;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    .line 496
    iget-object v4, p1, Lggp;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    .line 497
    iget v4, p1, Lggp;->b:I

    int-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    .line 494
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 498
    iget-object v1, p1, Lggp;->c:Ljava/lang/Object;

    .line 499
    if-eqz v1, :cond_1

    .line 500
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Lgoo;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 502
    :cond_1
    iget-object v1, p0, Lcpo;->i:Lcpx;

    const-string v2, "error"

    invoke-virtual {v1, v2}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 504
    invoke-virtual {p1}, Lggp;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 505
    sget-object v0, Lcpu;->b:Lcpu;

    invoke-virtual {p0, v0}, Lcpo;->a(Lcpu;)V

    .line 506
    invoke-virtual {p0}, Lcpo;->d()V

    .line 508
    :cond_2
    return-void
.end method

.method public final handleUserReportedPlaybackEvent(Lczz;)V
    .locals 10
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 512
    iget-object v0, p0, Lcpo;->i:Lcpx;

    const-string v1, "error"

    invoke-virtual {v0, v1}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s:%s:%.3f"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 514
    invoke-virtual {p0}, Lcpo;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    .line 515
    iget-object v5, p1, Lczz;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    .line 516
    iget-wide v6, p1, Lczz;->b:J

    long-to-double v6, v6

    const-wide v8, 0x408f400000000000L    # 1000.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    .line 513
    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 512
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    invoke-virtual {p0}, Lcpo;->d()V

    .line 519
    return-void
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 484
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_0

    .line 485
    iget-wide v0, p1, Ldad;->c:J

    iget-wide v2, p1, Ldad;->a:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcpo;->q:J

    .line 487
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 683
    iget-object v0, p0, Lcpo;->F:Ldmc;

    invoke-virtual {v0, p0}, Ldmc;->deleteObserver(Ljava/util/Observer;)V

    .line 684
    iget-object v0, p0, Lcpo;->y:Lefj;

    iget-object v1, p0, Lcpo;->h:Lcpp;

    invoke-virtual {v0, v1}, Lefj;->b(Lggl;)V

    .line 685
    iget-object v0, p0, Lcpo;->z:Lefj;

    iget-object v1, p0, Lcpo;->A:Lcpp;

    invoke-virtual {v0, v1}, Lefj;->b(Lggl;)V

    .line 686
    iget-object v0, p0, Lcpo;->B:Lggm;

    iget-object v1, p0, Lcpo;->C:Lcpr;

    iget-object v0, v0, Lggm;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 687
    iget-object v0, p0, Lcpo;->D:Lgfx;

    iget-object v1, p0, Lcpo;->E:Lcpq;

    iget-object v0, v0, Lgfx;->a:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 688
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcpo;->a(Z)V

    .line 689
    iget-object v0, p0, Lcpo;->j:Lcpu;

    sget-object v1, Lcpu;->d:Lcpu;

    if-eq v0, v1, :cond_0

    .line 690
    const-string v0, "QoE client released unexpectedly"

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 691
    sget-object v0, Lcpu;->d:Lcpu;

    invoke-virtual {p0, v0}, Lcpo;->a(Lcpu;)V

    .line 693
    :cond_0
    invoke-virtual {p0}, Lcpo;->d()V

    .line 694
    iget-object v0, p0, Lcpo;->u:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 695
    return-void
.end method

.method public final update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 666
    instance-of v0, p1, Ldmc;

    if-eqz v0, :cond_0

    .line 667
    iget-object v0, p0, Lcpo;->i:Lcpx;

    const-string v1, "qoealert"

    invoke-virtual {v0, v1}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 669
    :cond_0
    return-void
.end method
