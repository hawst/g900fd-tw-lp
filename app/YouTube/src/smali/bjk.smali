.class public final Lbjk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lhog;

.field private final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/notification/GcmBroadcastReceiver;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lhog;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-object p2, p0, Lbjk;->a:Landroid/content/Context;

    .line 160
    iput-object p3, p0, Lbjk;->b:Ljava/lang/String;

    .line 161
    iput-object p4, p0, Lbjk;->c:Ljava/lang/String;

    .line 162
    iput-object p5, p0, Lbjk;->d:Lhog;

    .line 163
    iput-object p6, p0, Lbjk;->e:Landroid/content/res/Resources;

    .line 164
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 145
    const-string v0, "Error retrieving thumbnail image for notification: "

    invoke-static {v0, p2}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lbjk;->e:Landroid/content/res/Resources;

    const/high16 v1, 0x7f030000

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lbjk;->a:Landroid/content/Context;

    iget-object v2, p0, Lbjk;->b:Ljava/lang/String;

    iget-object v3, p0, Lbjk;->c:Ljava/lang/String;

    iget-object v4, p0, Lbjk;->d:Lhog;

    invoke-static {v1, v2, v3, v0, v4}, La;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lhog;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 145
    check-cast p2, Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lbjk;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1050006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    const v2, 0x1050005

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    const/4 v2, 0x1

    invoke-static {p2, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p2

    :cond_0
    iget-object v0, p0, Lbjk;->a:Landroid/content/Context;

    iget-object v1, p0, Lbjk;->b:Ljava/lang/String;

    iget-object v2, p0, Lbjk;->c:Ljava/lang/String;

    iget-object v3, p0, Lbjk;->d:Lhog;

    invoke-static {v0, v1, v2, p2, v3}, La;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lhog;)V

    return-void
.end method
