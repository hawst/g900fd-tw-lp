.class public final Lfwt;
.super Lghv;
.source "SourceFile"


# instance fields
.field private final a:[B


# direct methods
.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lghv;-><init>()V

    .line 36
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lfwt;->a:[B

    .line 37
    return-void
.end method

.method private a(Ljava/lang/String;)[B
    .locals 6

    .prologue
    const/16 v5, 0x14

    const/4 v2, 0x0

    .line 55
    :try_start_0
    const-string v0, "AES/ECB/PKCS5Padding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 66
    iget-object v1, p0, Lfwt;->a:[B

    invoke-static {v1, v2}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v1

    .line 67
    invoke-static {p1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 70
    :try_start_1
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v4, "AES"

    invoke-direct {v3, v1, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 71
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 72
    invoke-virtual {v0, v2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 73
    array-length v0, v1

    if-le v0, v5, :cond_0

    .line 74
    const/16 v0, 0x14

    new-array v0, v0, [B

    .line 75
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x14

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_4

    .line 78
    :goto_0
    return-object v0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 62
    :catch_1
    move-exception v0

    .line 63
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move-object v0, v1

    .line 78
    goto :goto_0

    .line 80
    :catch_2
    move-exception v0

    .line 81
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 82
    :catch_3
    move-exception v0

    .line 83
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 84
    :catch_4
    move-exception v0

    .line 85
    new-instance v1, Lfax;

    invoke-direct {v1, v0}, Lfax;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v1, "DeviceId"

    invoke-virtual {v0, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "DeviceKey"

    invoke-virtual {v0, v2}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lfwt;->a(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v2, Lgbl;

    invoke-direct {v2, v1, v0}, Lgbl;-><init>(Ljava/lang/String;[B)V

    return-object v2

    :cond_0
    new-instance v0, Lfax;

    const-string v1, "invalid device registration response"

    invoke-direct {v0, v1}, Lfax;-><init>(Ljava/lang/String;)V

    throw v0
.end method
