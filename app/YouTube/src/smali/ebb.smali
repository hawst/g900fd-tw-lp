.class final Lebb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:J

.field c:J


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static b(J)J
    .locals 4

    .prologue
    .line 63
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long/2addr v0, p0

    return-wide v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 41
    iget-boolean v0, p0, Lebb;->a:Z

    if-eqz v0, :cond_0

    .line 42
    iget-wide v0, p0, Lebb;->c:J

    invoke-static {v0, v1}, Lebb;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, Lebb;->b:J

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lebb;->a:Z

    .line 45
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 51
    iput-wide p1, p0, Lebb;->b:J

    .line 52
    invoke-static {p1, p2}, Lebb;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, Lebb;->c:J

    .line 53
    return-void
.end method
