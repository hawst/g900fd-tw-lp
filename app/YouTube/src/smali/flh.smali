.class public final Lflh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field private final b:Lhpn;

.field private c:Ljava/util/Map;

.field private d:Lfki;


# direct methods
.method public constructor <init>(Lhpn;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhpn;

    iput-object v0, p0, Lflh;->b:Lhpn;

    .line 27
    iget-boolean v0, p1, Lhpn;->a:Z

    iput-boolean v0, p0, Lflh;->a:Z

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 6

    .prologue
    .line 35
    iget-object v0, p0, Lflh;->c:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflh;->c:Ljava/util/Map;

    .line 37
    iget-object v0, p0, Lflh;->b:Lhpn;

    iget-object v1, v0, Lhpn;->c:[Lhpo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 38
    new-instance v4, Lfli;

    invoke-direct {v4, v3}, Lfli;-><init>(Lhpo;)V

    .line 39
    iget-object v3, v4, Lfli;->c:Lflj;

    .line 40
    if-eqz v3, :cond_0

    .line 41
    iget-object v5, p0, Lflh;->c:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    iget-object v0, p0, Lflh;->c:Ljava/util/Map;

    return-object v0
.end method

.method public final b()Lfki;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lflh;->d:Lfki;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflh;->b:Lhpn;

    iget-object v0, v0, Lhpn;->b:Lhpp;

    if-nez v0, :cond_1

    .line 50
    :cond_0
    iget-object v0, p0, Lflh;->d:Lfki;

    .line 56
    :goto_0
    return-object v0

    .line 51
    :cond_1
    iget-object v0, p0, Lflh;->b:Lhpn;

    iget-object v0, v0, Lhpn;->b:Lhpp;

    iget-object v0, v0, Lhpp;->a:Lhod;

    if-eqz v0, :cond_3

    .line 52
    new-instance v0, Lfla;

    iget-object v1, p0, Lflh;->b:Lhpn;

    iget-object v1, v1, Lhpn;->b:Lhpp;

    iget-object v1, v1, Lhpp;->a:Lhod;

    invoke-direct {v0, v1, v2}, Lfla;-><init>(Lhod;Lfqh;)V

    iput-object v0, p0, Lflh;->d:Lfki;

    .line 56
    :cond_2
    :goto_1
    iget-object v0, p0, Lflh;->d:Lfki;

    goto :goto_0

    .line 53
    :cond_3
    iget-object v0, p0, Lflh;->b:Lhpn;

    iget-object v0, v0, Lhpn;->b:Lhpp;

    iget-object v0, v0, Lhpp;->b:Lhfz;

    if-eqz v0, :cond_2

    .line 54
    new-instance v0, Lfjt;

    iget-object v1, p0, Lflh;->b:Lhpn;

    iget-object v1, v1, Lhpn;->b:Lhpp;

    iget-object v1, v1, Lhpp;->b:Lhfz;

    invoke-direct {v0, v1, v2}, Lfjt;-><init>(Lhfz;Lfqh;)V

    iput-object v0, p0, Lflh;->d:Lfki;

    goto :goto_1
.end method

.method public final c()[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflh;->b:Lhpn;

    iget-object v0, v0, Lhpn;->d:[B

    if-nez v0, :cond_0

    .line 61
    sget-object v0, Lfhy;->a:[B

    .line 63
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lflh;->b:Lhpn;

    iget-object v0, v0, Lhpn;->d:[B

    goto :goto_0
.end method
