.class public final Lhbq;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhul;

.field public b:Lhwa;

.field private c:Lhle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16586
    invoke-direct {p0}, Lidf;-><init>()V

    .line 16589
    iput-object v0, p0, Lhbq;->a:Lhul;

    .line 16592
    iput-object v0, p0, Lhbq;->b:Lhwa;

    .line 16595
    iput-object v0, p0, Lhbq;->c:Lhle;

    .line 16586
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 16644
    const/4 v0, 0x0

    .line 16645
    iget-object v1, p0, Lhbq;->a:Lhul;

    if-eqz v1, :cond_0

    .line 16646
    const v0, 0x2f1c7f5

    iget-object v1, p0, Lhbq;->a:Lhul;

    .line 16647
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16649
    :cond_0
    iget-object v1, p0, Lhbq;->b:Lhwa;

    if-eqz v1, :cond_1

    .line 16650
    const v1, 0x377a9fd

    iget-object v2, p0, Lhbq;->b:Lhwa;

    .line 16651
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16653
    :cond_1
    iget-object v1, p0, Lhbq;->c:Lhle;

    if-eqz v1, :cond_2

    .line 16654
    const v1, 0x3df8ce4

    iget-object v2, p0, Lhbq;->c:Lhle;

    .line 16655
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16657
    :cond_2
    iget-object v1, p0, Lhbq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16658
    iput v0, p0, Lhbq;->J:I

    .line 16659
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 16582
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbq;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbq;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbq;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbq;->a:Lhul;

    if-nez v0, :cond_2

    new-instance v0, Lhul;

    invoke-direct {v0}, Lhul;-><init>()V

    iput-object v0, p0, Lhbq;->a:Lhul;

    :cond_2
    iget-object v0, p0, Lhbq;->a:Lhul;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhbq;->b:Lhwa;

    if-nez v0, :cond_3

    new-instance v0, Lhwa;

    invoke-direct {v0}, Lhwa;-><init>()V

    iput-object v0, p0, Lhbq;->b:Lhwa;

    :cond_3
    iget-object v0, p0, Lhbq;->b:Lhwa;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhbq;->c:Lhle;

    if-nez v0, :cond_4

    new-instance v0, Lhle;

    invoke-direct {v0}, Lhle;-><init>()V

    iput-object v0, p0, Lhbq;->c:Lhle;

    :cond_4
    iget-object v0, p0, Lhbq;->c:Lhle;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x178e3faa -> :sswitch_1
        0x1bbd4fea -> :sswitch_2
        0x1efc6722 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 16629
    iget-object v0, p0, Lhbq;->a:Lhul;

    if-eqz v0, :cond_0

    .line 16630
    const v0, 0x2f1c7f5

    iget-object v1, p0, Lhbq;->a:Lhul;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16632
    :cond_0
    iget-object v0, p0, Lhbq;->b:Lhwa;

    if-eqz v0, :cond_1

    .line 16633
    const v0, 0x377a9fd

    iget-object v1, p0, Lhbq;->b:Lhwa;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16635
    :cond_1
    iget-object v0, p0, Lhbq;->c:Lhle;

    if-eqz v0, :cond_2

    .line 16636
    const v0, 0x3df8ce4

    iget-object v1, p0, Lhbq;->c:Lhle;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16638
    :cond_2
    iget-object v0, p0, Lhbq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 16640
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 16608
    if-ne p1, p0, :cond_1

    .line 16614
    :cond_0
    :goto_0
    return v0

    .line 16609
    :cond_1
    instance-of v2, p1, Lhbq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 16610
    :cond_2
    check-cast p1, Lhbq;

    .line 16611
    iget-object v2, p0, Lhbq;->a:Lhul;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbq;->a:Lhul;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbq;->b:Lhwa;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbq;->b:Lhwa;

    if-nez v2, :cond_3

    .line 16612
    :goto_2
    iget-object v2, p0, Lhbq;->c:Lhle;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbq;->c:Lhle;

    if-nez v2, :cond_3

    .line 16613
    :goto_3
    iget-object v2, p0, Lhbq;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhbq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 16614
    goto :goto_0

    .line 16611
    :cond_4
    iget-object v2, p0, Lhbq;->a:Lhul;

    iget-object v3, p1, Lhbq;->a:Lhul;

    invoke-virtual {v2, v3}, Lhul;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbq;->b:Lhwa;

    iget-object v3, p1, Lhbq;->b:Lhwa;

    .line 16612
    invoke-virtual {v2, v3}, Lhwa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbq;->c:Lhle;

    iget-object v3, p1, Lhbq;->c:Lhle;

    .line 16613
    invoke-virtual {v2, v3}, Lhle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhbq;->I:Ljava/util/List;

    iget-object v3, p1, Lhbq;->I:Ljava/util/List;

    .line 16614
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 16618
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 16620
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbq;->a:Lhul;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 16621
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbq;->b:Lhwa;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 16622
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbq;->c:Lhle;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 16623
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbq;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 16624
    return v0

    .line 16620
    :cond_0
    iget-object v0, p0, Lhbq;->a:Lhul;

    invoke-virtual {v0}, Lhul;->hashCode()I

    move-result v0

    goto :goto_0

    .line 16621
    :cond_1
    iget-object v0, p0, Lhbq;->b:Lhwa;

    invoke-virtual {v0}, Lhwa;->hashCode()I

    move-result v0

    goto :goto_1

    .line 16622
    :cond_2
    iget-object v0, p0, Lhbq;->c:Lhle;

    invoke-virtual {v0}, Lhle;->hashCode()I

    move-result v0

    goto :goto_2

    .line 16623
    :cond_3
    iget-object v1, p0, Lhbq;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
