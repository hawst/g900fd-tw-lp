.class Lidv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liea;


# static fields
.field private static synthetic q:Z


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lidn;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:Lidl;

.field private final g:Lidm;

.field private final h:J

.field private i:D

.field private j:I

.field private k:J

.field private l:Ljava/util/Random;

.field private m:I

.field private n:Liea;

.field private o:Liee;

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lidv;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lidv;->q:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lidn;Lidl;Ljava/lang/String;Lidm;Lief;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput v2, p0, Lidv;->j:I

    .line 189
    if-nez p8, :cond_0

    .line 190
    iput-object p1, p0, Lidv;->a:Ljava/lang/String;

    .line 191
    iput-object p2, p0, Lidv;->b:Ljava/lang/String;

    .line 192
    iput-object p3, p0, Lidv;->c:Lidn;

    .line 193
    iput-object p5, p0, Lidv;->d:Ljava/lang/String;

    .line 197
    :goto_0
    iput-object p6, p0, Lidv;->g:Lidm;

    .line 198
    iput-object p4, p0, Lidv;->f:Lidl;

    .line 199
    if-nez p7, :cond_1

    const-wide/16 v0, 0x3c

    :goto_1
    iput-wide v0, p0, Lidv;->h:J

    .line 200
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lidv;->i:D

    .line 201
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lidv;->k:J

    .line 202
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lidv;->l:Ljava/util/Random;

    .line 203
    iput v2, p0, Lidv;->m:I

    .line 204
    return-void

    .line 195
    :cond_0
    iput-object p1, p0, Lidv;->e:Ljava/lang/String;

    goto :goto_0

    .line 199
    :cond_1
    iget-wide v0, p7, Lief;->a:J

    goto :goto_1
.end method

.method private a(Lidn;Ljava/lang/String;Lidl;)Lido;
    .locals 4

    .prologue
    .line 617
    invoke-direct {p0}, Lidv;->j()V

    .line 618
    invoke-direct {p0, p1, p2, p3}, Lidv;->b(Lidn;Ljava/lang/String;Lidl;)Liea;

    move-result-object v0

    .line 619
    monitor-enter p0

    .line 622
    :try_start_0
    iput-object v0, p0, Lidv;->n:Liea;

    .line 623
    invoke-interface {v0}, Liea;->a()Ljava/util/concurrent/Future;

    move-result-object v0

    .line 624
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 626
    :try_start_1
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lied;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    .line 635
    invoke-virtual {v0}, Lied;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 637
    iget-object v1, v0, Lied;->a:Lieb;

    iget-object v1, v1, Lieb;->a:Liec;

    sget-object v2, Liec;->b:Liec;

    if-eq v1, v2, :cond_2

    .line 638
    iget-object v0, v0, Lied;->a:Lieb;

    throw v0

    .line 624
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 627
    :catch_0
    move-exception v0

    .line 629
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected error occurred: "

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 630
    :catch_1
    move-exception v0

    .line 632
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected error occurred: "

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 642
    :cond_2
    invoke-direct {p0}, Lidv;->j()V

    .line 644
    new-instance v0, Lieb;

    sget-object v1, Liec;->d:Liec;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0

    .line 646
    :cond_3
    iget-object v0, v0, Lied;->b:Lido;

    return-object v0
.end method

.method static synthetic a(Lidv;Z)Lido;
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lidv;->a(Z)Lido;

    move-result-object v0

    return-object v0
.end method

.method private a(Z)Lido;
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 344
    move v0, p1

    :goto_0
    if-eqz v0, :cond_8

    .line 345
    invoke-direct {p0}, Lidv;->e()Lido;

    move-result-object v0

    .line 346
    if-eqz v0, :cond_1

    .line 400
    :cond_0
    return-object v0

    .line 349
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    .line 353
    :goto_1
    invoke-direct {p0}, Lidv;->f()Lidy;

    move-result-object v1

    .line 354
    iget-object v0, v1, Lidy;->a:Ljava/lang/Object;

    check-cast v0, Lidl;

    .line 355
    iget-object v1, v1, Lidy;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 356
    invoke-direct {p0}, Lidv;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 358
    if-eqz v4, :cond_2

    .line 359
    const-string v1, "upload, finalize"

    .line 368
    :goto_2
    new-instance v5, Lidn;

    invoke-direct {v5}, Lidn;-><init>()V

    .line 369
    const-string v6, "X-Goog-Upload-Offset"

    iget-object v7, p0, Lidv;->f:Lidl;

    invoke-interface {v7}, Lidl;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lidn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    :try_start_0
    invoke-direct {p0, v5, v1, v0}, Lidv;->a(Lidn;Ljava/lang/String;Lidl;)Lido;
    :try_end_0
    .catch Lieb; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 383
    invoke-static {v0}, Lidv;->a(Lido;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 387
    invoke-static {v0}, Lidv;->b(Lido;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 388
    if-eqz v4, :cond_5

    .line 389
    new-instance v0, Lieb;

    sget-object v1, Liec;->e:Liec;

    const-string v2, "Finalize call returned active state."

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0

    .line 361
    :cond_2
    const-string v1, "upload"

    goto :goto_2

    .line 364
    :cond_3
    const-string v1, "finalize"

    goto :goto_2

    .line 373
    :catch_0
    move-exception v0

    .line 374
    invoke-virtual {v0}, Lieb;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 375
    throw v0

    .line 379
    :cond_4
    invoke-direct {p0, v0}, Lidv;->a(Lieb;)V

    move v0, v3

    .line 380
    goto :goto_0

    .line 391
    :cond_5
    invoke-direct {p0}, Lidv;->h()V

    move v0, v2

    .line 392
    goto :goto_0

    .line 395
    :cond_6
    invoke-static {v0}, Lidv;->c(Lido;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 399
    iget v1, v0, Lido;->a:I

    const/16 v2, 0x190

    if-ne v1, v2, :cond_0

    .line 405
    :cond_7
    new-instance v1, Lieb;

    sget-object v2, Liec;->e:Liec;

    invoke-virtual {v0}, Lido;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lidv;->a(Lieb;)V

    move v0, v3

    .line 406
    goto/16 :goto_0

    :cond_8
    move v2, v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Lidl;Lidm;Lief;)Lidv;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 166
    new-instance v0, Lidv;

    const-string v2, "PUT"

    const/4 v8, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, v3

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v8}, Lidv;-><init>(Ljava/lang/String;Ljava/lang/String;Lidn;Lidl;Ljava/lang/String;Lidm;Lief;Z)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lidn;Lidl;Ljava/lang/String;Lidm;Lief;)Lidv;
    .locals 9

    .prologue
    .line 141
    new-instance v0, Lidv;

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lidv;-><init>(Ljava/lang/String;Ljava/lang/String;Lidn;Lidl;Ljava/lang/String;Lidm;Lief;Z)V

    return-object v0
.end method

.method static synthetic a(Lidv;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lidv;->e:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lieb;)V
    .locals 6

    .prologue
    .line 739
    iget-wide v0, p0, Lidv;->i:D

    iget-wide v2, p0, Lidv;->h:J

    long-to-double v2, v2

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 740
    throw p1

    .line 744
    :cond_0
    iget-object v0, p0, Lidv;->l:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    .line 746
    :try_start_0
    iget-wide v2, p0, Lidv;->i:D

    iget-wide v4, p0, Lidv;->k:J

    long-to-double v4, v4

    mul-double/2addr v4, v0

    add-double/2addr v2, v4

    iput-wide v2, p0, Lidv;->i:D

    .line 747
    iget-wide v2, p0, Lidv;->k:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    long-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 752
    :goto_0
    iget-wide v0, p0, Lidv;->k:J

    iget-wide v2, p0, Lidv;->k:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lidv;->k:J

    .line 753
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static a(Lido;)Z
    .locals 2

    .prologue
    .line 681
    iget-object v0, p0, Lido;->b:Lidn;

    if-nez v0, :cond_0

    .line 682
    const/4 v0, 0x0

    .line 686
    :goto_0
    return v0

    .line 685
    :cond_0
    iget-object v0, p0, Lido;->b:Lidn;

    const-string v1, "X-Goog-Upload-Status"

    invoke-virtual {v0, v1}, Lidn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 686
    const-string v1, "final"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lidv;)Lido;
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lidv;->d()Lido;

    move-result-object v0

    return-object v0
.end method

.method private b(Lidn;Ljava/lang/String;Lidl;)Liea;
    .locals 5

    .prologue
    .line 656
    new-instance v2, Lidn;

    invoke-direct {v2}, Lidn;-><init>()V

    .line 657
    const-string v0, "X-Goog-Upload-Protocol"

    const-string v1, "resumable"

    invoke-virtual {v2, v0, v1}, Lidn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 658
    const-string v0, "X-Goog-Upload-Command"

    invoke-virtual {v2, v0, p2}, Lidn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    invoke-virtual {p1}, Lidn;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660
    invoke-virtual {p1, v0}, Lidn;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 661
    invoke-virtual {v2, v0, v1}, Lidn;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 664
    :cond_1
    const-string v0, "start"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lidv;->a:Ljava/lang/String;

    .line 665
    :goto_1
    const-string v1, "upload"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lidv;->b:Ljava/lang/String;

    .line 666
    :goto_2
    iget-object v3, p0, Lidv;->g:Lidm;

    .line 667
    invoke-interface {v3, v0, v1, v2, p3}, Lidm;->a(Ljava/lang/String;Ljava/lang/String;Lidn;Lidl;)Liea;

    move-result-object v0

    .line 668
    const-string v1, "start"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 669
    monitor-enter p0

    .line 670
    :try_start_0
    new-instance v1, Lidx;

    iget-object v2, p0, Lidv;->o:Liee;

    invoke-direct {v1, p0, p0, v2}, Lidx;-><init>(Lidv;Liea;Liee;)V

    iget v2, p0, Lidv;->p:I

    invoke-interface {v0, v1, v2}, Liea;->a(Liee;I)V

    .line 672
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 674
    :cond_2
    return-object v0

    .line 664
    :cond_3
    iget-object v0, p0, Lidv;->e:Ljava/lang/String;

    goto :goto_1

    .line 665
    :cond_4
    const-string v1, "PUT"

    goto :goto_2

    .line 672
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static b(Lido;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 693
    iget-object v1, p0, Lido;->b:Lidn;

    if-nez v1, :cond_1

    .line 698
    :cond_0
    :goto_0
    return v0

    .line 697
    :cond_1
    iget-object v1, p0, Lido;->b:Lidn;

    const-string v2, "X-Goog-Upload-Status"

    invoke-virtual {v1, v2}, Lidn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 698
    const-string v2, "active"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lido;->a:I

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic c(Lidv;)Liee;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lidv;->o:Liee;

    return-object v0
.end method

.method private static c(Lido;)Z
    .locals 2

    .prologue
    .line 705
    iget v0, p0, Lido;->a:I

    div-int/lit8 v0, v0, 0x64

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Lido;
    .locals 4

    .prologue
    .line 262
    monitor-enter p0

    .line 263
    :try_start_0
    iget-object v0, p0, Lidv;->o:Liee;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lidv;->o:Liee;

    .line 266
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    invoke-direct {p0}, Lidv;->i()V

    .line 269
    :goto_0
    :try_start_1
    iget-object v1, p0, Lidv;->c:Lidn;

    const-string v2, "start"

    new-instance v3, Lidz;

    iget-object v0, p0, Lidv;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-direct {v3, v0}, Lidz;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, v2, v3}, Lidv;->a(Lidn;Ljava/lang/String;Lidl;)Lido;
    :try_end_1
    .catch Lieb; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 285
    invoke-static {v0}, Lidv;->a(Lido;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 334
    :cond_1
    :goto_2
    return-object v0

    .line 266
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 269
    :cond_2
    :try_start_3
    iget-object v0, p0, Lidv;->d:Ljava/lang/String;
    :try_end_3
    .catch Lieb; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 276
    :catch_0
    move-exception v0

    .line 277
    invoke-virtual {v0}, Lieb;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 278
    throw v0

    .line 281
    :cond_3
    invoke-direct {p0, v0}, Lidv;->a(Lieb;)V

    goto :goto_0

    .line 289
    :cond_4
    invoke-static {v0}, Lidv;->b(Lido;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 290
    invoke-static {v0}, Lidv;->c(Lido;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 297
    new-instance v1, Lieb;

    sget-object v2, Liec;->e:Liec;

    invoke-virtual {v0}, Lido;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lidv;->a(Lieb;)V

    goto :goto_0

    .line 303
    :cond_5
    iget-object v0, v0, Lido;->b:Lidn;

    .line 304
    const-string v1, "X-Goog-Upload-URL"

    invoke-virtual {v0, v1}, Lidn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 306
    :try_start_4
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 307
    iput-object v1, p0, Lidv;->e:Ljava/lang/String;

    .line 310
    monitor-enter p0
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_1

    .line 311
    :try_start_5
    iget-object v1, p0, Lidv;->o:Liee;

    if-eqz v1, :cond_6

    .line 312
    iget-object v1, p0, Lidv;->o:Liee;

    invoke-virtual {v1, p0}, Liee;->a(Liea;)V

    .line 313
    iget-object v1, p0, Lidv;->o:Liee;

    invoke-virtual {v1, v0}, Liee;->a(Lidn;)V

    .line 315
    :cond_6
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 322
    const-string v1, "X-Goog-Upload-Chunk-Granularity"

    .line 323
    invoke-virtual {v0, v1}, Lidn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 324
    if-eqz v0, :cond_7

    .line 326
    :try_start_6
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lidv;->j:I
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_2

    .line 334
    :cond_7
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lidv;->a(Z)Lido;

    move-result-object v0

    goto :goto_2

    .line 315
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/net/MalformedURLException; {:try_start_8 .. :try_end_8} :catch_1

    .line 317
    :catch_1
    move-exception v0

    new-instance v0, Lieb;

    sget-object v1, Liec;->e:Liec;

    const-string v2, "Server returned an invalid upload url."

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0

    .line 327
    :catch_2
    move-exception v0

    .line 329
    new-instance v1, Lieb;

    sget-object v2, Liec;->e:Liec;

    const-string v3, "Server returned an invalid chunk granularity."

    invoke-direct {v1, v2, v3, v0}, Lieb;-><init>(Liec;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private e()Lido;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 415
    :goto_0
    :try_start_0
    new-instance v0, Lidn;

    invoke-direct {v0}, Lidn;-><init>()V

    const-string v2, "query"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v2, v3}, Lidv;->a(Lidn;Ljava/lang/String;Lidl;)Lido;
    :try_end_0
    .catch Lieb; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 428
    invoke-static {v0}, Lidv;->a(Lido;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 505
    :cond_0
    :goto_1
    return-object v0

    .line 419
    :catch_0
    move-exception v0

    .line 420
    invoke-virtual {v0}, Lieb;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 421
    throw v0

    .line 424
    :cond_1
    invoke-direct {p0, v0}, Lidv;->a(Lieb;)V

    goto :goto_0

    .line 432
    :cond_2
    invoke-static {v0}, Lidv;->b(Lido;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 433
    invoke-static {v0}, Lidv;->c(Lido;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 440
    new-instance v2, Lieb;

    sget-object v3, Liec;->e:Liec;

    invoke-virtual {v0}, Lido;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lidv;->a(Lieb;)V

    goto :goto_0

    .line 447
    :cond_3
    iget-object v2, v0, Lido;->b:Lidn;

    const-string v3, "X-Goog-Upload-Chunk-Granularity"

    .line 448
    invoke-virtual {v2, v3}, Lidn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 449
    if-eqz v2, :cond_4

    .line 451
    :try_start_1
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lidv;->j:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    .line 464
    :cond_4
    :try_start_2
    iget-object v0, v0, Lido;->b:Lidn;

    const-string v2, "X-Goog-Upload-Size-Received"

    .line 465
    invoke-virtual {v0, v2}, Lidn;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 462
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide v2

    .line 471
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->b()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    .line 473
    new-instance v0, Lieb;

    sget-object v1, Liec;->e:Liec;

    iget-object v4, p0, Lidv;->f:Lidl;

    .line 476
    invoke-interface {v4}, Lidl;->b()J

    move-result-wide v4

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x7b

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "The server lost bytes that were previously committed. Our offset: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", server offset: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0

    .line 452
    :catch_1
    move-exception v0

    .line 454
    new-instance v1, Lieb;

    sget-object v2, Liec;->e:Liec;

    const-string v3, "Server returned an invalid chunk granularity."

    invoke-direct {v1, v2, v3, v0}, Lieb;-><init>(Liec;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 466
    :catch_2
    move-exception v0

    .line 467
    new-instance v1, Lieb;

    sget-object v2, Liec;->e:Liec;

    const-string v3, "Failed to parse X-Goog-Upload-Size-Received header"

    invoke-direct {v1, v2, v3, v0}, Lieb;-><init>(Liec;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 479
    :cond_5
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->c()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_6

    .line 482
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->e()V

    .line 485
    :cond_6
    :goto_2
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->c()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-gez v0, :cond_8

    .line 486
    invoke-direct {p0}, Lidv;->g()Z

    move-result v0

    if-nez v0, :cond_7

    .line 487
    new-instance v0, Lieb;

    sget-object v1, Liec;->c:Liec;

    const-string v4, "Upload stream does not have more data but it should. Maybe the caller passed in a data stream to upload with a mark position that didn\'t match the transfer handle? Confirmed offset from server: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lidv;->f:Lidl;

    .line 492
    invoke-interface {v5}, Lidl;->c()J

    move-result-wide v6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x2f

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0

    .line 495
    :cond_7
    :try_start_3
    iget-object v0, p0, Lidv;->f:Lidl;

    iget-object v4, p0, Lidv;->f:Lidl;

    invoke-interface {v4}, Lidl;->c()J

    move-result-wide v4

    sub-long v4, v2, v4

    invoke-interface {v0, v4, v5}, Lidl;->a(J)J

    .line 496
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->a()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_2

    .line 497
    :catch_3
    move-exception v0

    .line 500
    new-instance v1, Lieb;

    sget-object v2, Liec;->c:Liec;

    const-string v3, "Could not skip in the data stream."

    invoke-direct {v1, v2, v3, v0}, Lieb;-><init>(Liec;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 504
    :cond_8
    invoke-direct {p0}, Lidv;->h()V

    move-object v0, v1

    .line 505
    goto/16 :goto_1
.end method

.method private f()Lidy;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 512
    invoke-direct {p0}, Lidv;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 513
    new-instance v0, Lidy;

    iget-object v1, p0, Lidv;->f:Lidl;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lidy;-><init>(Lidv;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 539
    :goto_0
    return-object v0

    .line 516
    :cond_0
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->d()J

    move-result-wide v4

    const-wide v6, 0x7fffffffffffffffL

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 517
    new-instance v0, Lidy;

    iget-object v1, p0, Lidv;->f:Lidl;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lidy;-><init>(Lidv;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 520
    :cond_1
    :try_start_0
    new-instance v3, Lidk;

    iget-object v0, p0, Lidv;->f:Lidl;

    iget v4, p0, Lidv;->j:I

    invoke-direct {v3, v0, v4}, Lidk;-><init>(Lidl;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 529
    invoke-interface {v3}, Lidl;->f()J

    move-result-wide v4

    iget-object v0, p0, Lidv;->f:Lidl;

    .line 530
    invoke-interface {v0}, Lidl;->d()J

    move-result-wide v6

    iget v0, p0, Lidv;->j:I

    int-to-long v8, v0

    div-long/2addr v6, v8

    iget v0, p0, Lidv;->j:I

    int-to-long v8, v0

    mul-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-ltz v0, :cond_2

    iget-object v0, p0, Lidv;->f:Lidl;

    .line 531
    invoke-interface {v0}, Lidl;->c()J

    move-result-wide v4

    invoke-interface {v3}, Lidl;->f()J

    move-result-wide v6

    add-long/2addr v4, v6

    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->f()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    .line 532
    :goto_1
    if-eqz v0, :cond_4

    .line 536
    new-instance v0, Lidy;

    iget-object v1, p0, Lidv;->f:Lidl;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lidy;-><init>(Lidv;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 524
    :catch_0
    move-exception v0

    new-instance v0, Lieb;

    sget-object v1, Liec;->c:Liec;

    const-string v2, "Could not create chunked data stream."

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v1

    .line 531
    goto :goto_1

    .line 539
    :cond_4
    new-instance v0, Lidy;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, v3, v1}, Lidy;-><init>(Lidv;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private g()Z
    .locals 4

    .prologue
    .line 544
    :try_start_0
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->g()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 545
    :catch_0
    move-exception v0

    .line 546
    new-instance v1, Lieb;

    sget-object v2, Liec;->c:Liec;

    const-string v3, "Could not call hasMoreData() on upload stream."

    invoke-direct {v1, v2, v3, v0}, Lieb;-><init>(Liec;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private h()V
    .locals 4

    .prologue
    .line 552
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->c()J

    move-result-wide v0

    iget-object v2, p0, Lidv;->f:Lidl;

    invoke-interface {v2}, Lidl;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 553
    iget-object v0, p0, Lidv;->f:Lidl;

    invoke-interface {v0}, Lidl;->a()V

    .line 554
    invoke-direct {p0}, Lidv;->i()V

    .line 556
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 709
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lidv;->k:J

    .line 710
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lidv;->i:D

    .line 711
    return-void
.end method

.method private declared-synchronized j()V
    .locals 3

    .prologue
    .line 717
    monitor-enter p0

    :goto_0
    :try_start_0
    iget v0, p0, Lidv;->m:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 720
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 723
    :catch_0
    move-exception v0

    goto :goto_0

    .line 726
    :cond_0
    :try_start_2
    iget v0, p0, Lidv;->m:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 728
    new-instance v0, Lieb;

    sget-object v1, Liec;->b:Liec;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 731
    :cond_1
    :try_start_3
    sget-boolean v0, Lidv;->q:Z

    if-nez v0, :cond_2

    iget v0, p0, Lidv;->m:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 732
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Future;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lidw;

    invoke-direct {v1, p0}, Lidw;-><init>(Lidv;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 251
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 252
    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 253
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 254
    return-object v0
.end method

.method public final declared-synchronized a(Liee;I)V
    .locals 2

    .prologue
    .line 208
    monitor-enter p0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "Progress threshold must be greater than 0"

    invoke-static {v0, v1}, Lk;->a(ZLjava/lang/Object;)V

    .line 209
    iput-object p1, p0, Lidv;->o:Liee;

    .line 210
    iput p2, p0, Lidv;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    monitor-exit p0

    return-void

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 609
    iget-object v0, p0, Lidv;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 592
    monitor-enter p0

    .line 593
    :try_start_0
    iget-object v0, p0, Lidv;->n:Liea;

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lidv;->n:Liea;

    invoke-interface {v0}, Liea;->c()V

    .line 595
    const/4 v0, 0x0

    iput-object v0, p0, Lidv;->n:Liea;

    .line 597
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lidv;->m:I

    .line 598
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 599
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
