.class public final Lcar;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Landroid/view/View;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:Ljava/util/LinkedHashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcar;->g:Ljava/util/LinkedHashMap;

    .line 107
    return-void
.end method


# virtual methods
.method public final a()Lcaq;
    .locals 8

    .prologue
    .line 160
    new-instance v0, Lcaq;

    iget-object v1, p0, Lcar;->a:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcar;->b:Landroid/view/View;

    iget v3, p0, Lcar;->c:I

    iget v4, p0, Lcar;->d:I

    iget v5, p0, Lcar;->e:I

    iget v6, p0, Lcar;->f:I

    iget-object v7, p0, Lcar;->g:Ljava/util/LinkedHashMap;

    .line 167
    invoke-virtual {v7}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcaq;-><init>(Ljava/lang/CharSequence;Landroid/view/View;IIIILjava/util/Collection;)V

    return-object v0
.end method

.method public final a(Lcam;)Lcar;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcar;->g:Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Lcam;->g()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    return-object p0
.end method

.method public final a(Ljava/util/Collection;)Lcar;
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, Lcar;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 153
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcam;

    .line 154
    iget-object v2, p0, Lcar;->g:Ljava/util/LinkedHashMap;

    invoke-interface {v0}, Lcam;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 156
    :cond_0
    return-object p0
.end method
