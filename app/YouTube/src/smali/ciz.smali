.class public final Lciz;
.super Lfsa;
.source "SourceFile"


# instance fields
.field final a:Levn;

.field private final b:Landroid/content/Context;

.field private final c:Lfsj;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Lfvi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Levn;Lfsj;Lfdw;Lfrz;)V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0, p5, p6}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 53
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lciz;->b:Landroid/content/Context;

    .line 54
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lciz;->a:Levn;

    .line 55
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lciz;->c:Lfsj;

    .line 57
    const v0, 0x7f040148

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 58
    const v0, 0x7f08034d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 59
    new-instance v2, Lfvi;

    invoke-direct {v2, p2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lciz;->g:Lfvi;

    .line 61
    const v0, 0x7f08034f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lciz;->d:Landroid/widget/TextView;

    .line 63
    const v0, 0x7f080350

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lciz;->e:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f080351

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lciz;->f:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lciz;->f:Landroid/widget/TextView;

    new-instance v2, Lcja;

    invoke-direct {v2, p0}, Lcja;-><init>(Lciz;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v0, p0, Lciz;->c:Lfsj;

    invoke-interface {v0, v1}, Lfsj;->a(Landroid/view/View;)V

    .line 73
    return-void
.end method

.method private a(Lfsg;Lfoh;)Landroid/view/View;
    .locals 4

    .prologue
    const/high16 v3, 0x41b00000    # 22.0f

    const/high16 v2, 0x41900000    # 18.0f

    const/4 v1, 0x2

    .line 77
    invoke-super {p0, p1, p2}, Lfsa;->a(Lfsg;Lfqh;)Landroid/view/View;

    .line 79
    iget-object v0, p0, Lciz;->b:Landroid/content/Context;

    invoke-static {v0, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 80
    iget-object v0, p0, Lciz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 81
    iget-object v0, p0, Lciz;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 82
    iget-object v0, p0, Lciz;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLines(I)V

    .line 88
    :goto_0
    iget-object v0, p0, Lciz;->g:Lfvi;

    iget-object v1, p2, Lfoh;->b:Lfnc;

    if-nez v1, :cond_0

    new-instance v1, Lfnc;

    iget-object v2, p2, Lfoh;->a:Licj;

    iget-object v2, v2, Licj;->a:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p2, Lfoh;->b:Lfnc;

    :cond_0
    iget-object v1, p2, Lfoh;->b:Lfnc;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    .line 89
    iget-object v0, p0, Lciz;->d:Landroid/widget/TextView;

    iget-object v1, p2, Lfoh;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfoh;->a:Licj;

    iget-object v1, v1, Licj;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfoh;->d:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfoh;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lciz;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lfoh;->e:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfoh;->a:Licj;

    iget-object v1, v1, Licj;->c:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfoh;->e:Ljava/lang/CharSequence;

    :cond_2
    iget-object v1, p2, Lfoh;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lciz;->f:Landroid/widget/TextView;

    iget-object v1, p2, Lfoh;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p2, Lfoh;->a:Licj;

    iget-object v1, v1, Licj;->d:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfoh;->c:Ljava/lang/CharSequence;

    :cond_3
    iget-object v1, p2, Lfoh;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lciz;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 84
    :cond_4
    iget-object v0, p0, Lciz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 85
    iget-object v0, p0, Lciz;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    check-cast p2, Lfoh;

    invoke-direct {p0, p1, p2}, Lciz;->a(Lfsg;Lfoh;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 31
    check-cast p2, Lfoh;

    invoke-direct {p0, p1, p2}, Lciz;->a(Lfsg;Lfoh;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
