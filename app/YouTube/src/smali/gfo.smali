.class final Lgfo;
.super Lgkx;
.source "SourceFile"


# instance fields
.field private final j:Ljava/util/Map;

.field private final k:[B

.field private final l:Lwv;


# direct methods
.method public constructor <init>(Ljava/lang/String;[B[Lgkl;Lwv;)V
    .locals 4

    .prologue
    .line 231
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p4}, Lgkx;-><init>(ILjava/lang/String;Lwu;)V

    .line 232
    iput-object p2, p0, Lgfo;->k:[B

    .line 233
    iput-object p4, p0, Lgfo;->l:Lwv;

    .line 234
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgfo;->j:Ljava/util/Map;

    .line 235
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p3, v0

    .line 236
    iget-object v3, p0, Lgfo;->j:Ljava/util/Map;

    invoke-interface {v2, v3, p0}, Lgkl;->a(Ljava/util/Map;Lgkt;)V

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lwm;)Lwt;
    .locals 11

    .prologue
    const/4 v0, 0x1

    .line 261
    :try_start_0
    iget-object v1, p1, Lwm;->c:Ljava/util/Map;

    invoke-static {v1}, La;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 262
    new-instance v2, Ljava/io/ByteArrayInputStream;

    iget-object v3, p1, Lwm;->b:[B

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 263
    invoke-static {v2}, Lgfl;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    .line 264
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 265
    const-string v1, "GLS/1."

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 266
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid response from server. Expected GLS/1.x"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    :catch_0
    move-exception v0

    .line 306
    new-instance v1, Lwo;

    invoke-direct {v1, v0}, Lwo;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lwt;->a(Lxa;)Lwt;

    move-result-object v0

    :goto_0
    return-object v0

    .line 270
    :cond_0
    :try_start_1
    const-string v1, "\r\n\r\n"

    .line 271
    invoke-virtual {v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 272
    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    .line 273
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Invalid response from server. Could not locate DRM message"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_1
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 278
    const-string v5, "\r\n"

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 279
    const/4 v5, 0x0

    aget-object v5, v3, v5

    .line 280
    const-string v6, "GLS/[0-9]+\\.[0-9]+ ([0-9]+) (.*)"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 281
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->find()Z

    .line 282
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 283
    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    .line 284
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 285
    :goto_1
    array-length v8, v3

    if-ge v0, v8, :cond_2

    .line 286
    aget-object v8, v3, v0

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 287
    const/4 v9, 0x0

    aget-object v9, v8, v9

    const/4 v10, 0x1

    aget-object v8, v8, v10

    invoke-interface {v7, v9, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 293
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v4

    array-length v1, v2

    .line 291
    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 295
    new-instance v1, Lgfp;

    invoke-direct {v1, v6, v5, v0, v7}, Lgfp;-><init>(ILjava/lang/String;[BLjava/util/Map;)V

    .line 300
    iget v0, v1, Lgfp;->a:I

    if-nez v0, :cond_3

    .line 301
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lwt;->a(Ljava/lang/Object;Lwd;)Lwt;

    move-result-object v0

    goto :goto_0

    .line 303
    :cond_3
    new-instance v0, Lgfq;

    invoke-direct {v0, v1}, Lgfq;-><init>(Lgfp;)V

    invoke-static {v0}, Lwt;->a(Lxa;)Lwt;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 220
    check-cast p1, Lgfp;

    iget-object v0, p0, Lgfo;->l:Lwv;

    invoke-interface {v0, p1}, Lwv;->onResponse(Ljava/lang/Object;)V

    return-void
.end method

.method public final c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lgfo;->j:Ljava/util/Map;

    return-object v0
.end method

.method public final h()[B
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lgfo;->k:[B

    return-object v0
.end method

.method public final i()Lwr;
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lwr;->d:Lwr;

    return-object v0
.end method
