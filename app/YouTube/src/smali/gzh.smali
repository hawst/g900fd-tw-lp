.class public final Lgzh;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgzh;


# instance fields
.field private b:Lgzk;

.field private c:Lgzl;

.field private d:Lgze;

.field private e:Lgza;

.field private f:Lgzb;

.field private g:Lgzf;

.field private h:Lgzi;

.field private i:Lgyz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7717
    const/4 v0, 0x0

    new-array v0, v0, [Lgzh;

    sput-object v0, Lgzh;->a:[Lgzh;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7718
    invoke-direct {p0}, Lidf;-><init>()V

    .line 7721
    iput-object v0, p0, Lgzh;->b:Lgzk;

    .line 7724
    iput-object v0, p0, Lgzh;->c:Lgzl;

    .line 7727
    iput-object v0, p0, Lgzh;->d:Lgze;

    .line 7730
    iput-object v0, p0, Lgzh;->e:Lgza;

    .line 7733
    iput-object v0, p0, Lgzh;->f:Lgzb;

    .line 7736
    iput-object v0, p0, Lgzh;->g:Lgzf;

    .line 7739
    iput-object v0, p0, Lgzh;->h:Lgzi;

    .line 7742
    iput-object v0, p0, Lgzh;->i:Lgyz;

    .line 7718
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 7821
    const/4 v0, 0x0

    .line 7822
    iget-object v1, p0, Lgzh;->b:Lgzk;

    if-eqz v1, :cond_0

    .line 7823
    const v0, 0x3ec6d57

    iget-object v1, p0, Lgzh;->b:Lgzk;

    .line 7824
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7826
    :cond_0
    iget-object v1, p0, Lgzh;->c:Lgzl;

    if-eqz v1, :cond_1

    .line 7827
    const v1, 0x3ec6d64

    iget-object v2, p0, Lgzh;->c:Lgzl;

    .line 7828
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7830
    :cond_1
    iget-object v1, p0, Lgzh;->d:Lgze;

    if-eqz v1, :cond_2

    .line 7831
    const v1, 0x3ec6d71

    iget-object v2, p0, Lgzh;->d:Lgze;

    .line 7832
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7834
    :cond_2
    iget-object v1, p0, Lgzh;->e:Lgza;

    if-eqz v1, :cond_3

    .line 7835
    const v1, 0x3ec6d82

    iget-object v2, p0, Lgzh;->e:Lgza;

    .line 7836
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7838
    :cond_3
    iget-object v1, p0, Lgzh;->f:Lgzb;

    if-eqz v1, :cond_4

    .line 7839
    const v1, 0x4092887

    iget-object v2, p0, Lgzh;->f:Lgzb;

    .line 7840
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7842
    :cond_4
    iget-object v1, p0, Lgzh;->g:Lgzf;

    if-eqz v1, :cond_5

    .line 7843
    const v1, 0x40943cf

    iget-object v2, p0, Lgzh;->g:Lgzf;

    .line 7844
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7846
    :cond_5
    iget-object v1, p0, Lgzh;->h:Lgzi;

    if-eqz v1, :cond_6

    .line 7847
    const v1, 0x4a88a65

    iget-object v2, p0, Lgzh;->h:Lgzi;

    .line 7848
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7850
    :cond_6
    iget-object v1, p0, Lgzh;->i:Lgyz;

    if-eqz v1, :cond_7

    .line 7851
    const v1, 0x4b19227

    iget-object v2, p0, Lgzh;->i:Lgyz;

    .line 7852
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7854
    :cond_7
    iget-object v1, p0, Lgzh;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7855
    iput v0, p0, Lgzh;->J:I

    .line 7856
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 7714
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzh;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzh;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzh;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzh;->b:Lgzk;

    if-nez v0, :cond_2

    new-instance v0, Lgzk;

    invoke-direct {v0}, Lgzk;-><init>()V

    iput-object v0, p0, Lgzh;->b:Lgzk;

    :cond_2
    iget-object v0, p0, Lgzh;->b:Lgzk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzh;->c:Lgzl;

    if-nez v0, :cond_3

    new-instance v0, Lgzl;

    invoke-direct {v0}, Lgzl;-><init>()V

    iput-object v0, p0, Lgzh;->c:Lgzl;

    :cond_3
    iget-object v0, p0, Lgzh;->c:Lgzl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgzh;->d:Lgze;

    if-nez v0, :cond_4

    new-instance v0, Lgze;

    invoke-direct {v0}, Lgze;-><init>()V

    iput-object v0, p0, Lgzh;->d:Lgze;

    :cond_4
    iget-object v0, p0, Lgzh;->d:Lgze;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgzh;->e:Lgza;

    if-nez v0, :cond_5

    new-instance v0, Lgza;

    invoke-direct {v0}, Lgza;-><init>()V

    iput-object v0, p0, Lgzh;->e:Lgza;

    :cond_5
    iget-object v0, p0, Lgzh;->e:Lgza;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lgzh;->f:Lgzb;

    if-nez v0, :cond_6

    new-instance v0, Lgzb;

    invoke-direct {v0}, Lgzb;-><init>()V

    iput-object v0, p0, Lgzh;->f:Lgzb;

    :cond_6
    iget-object v0, p0, Lgzh;->f:Lgzb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lgzh;->g:Lgzf;

    if-nez v0, :cond_7

    new-instance v0, Lgzf;

    invoke-direct {v0}, Lgzf;-><init>()V

    iput-object v0, p0, Lgzh;->g:Lgzf;

    :cond_7
    iget-object v0, p0, Lgzh;->g:Lgzf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lgzh;->h:Lgzi;

    if-nez v0, :cond_8

    new-instance v0, Lgzi;

    invoke-direct {v0}, Lgzi;-><init>()V

    iput-object v0, p0, Lgzh;->h:Lgzi;

    :cond_8
    iget-object v0, p0, Lgzh;->h:Lgzi;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lgzh;->i:Lgyz;

    if-nez v0, :cond_9

    new-instance v0, Lgyz;

    invoke-direct {v0}, Lgyz;-><init>()V

    iput-object v0, p0, Lgzh;->i:Lgyz;

    :cond_9
    iget-object v0, p0, Lgzh;->i:Lgyz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f636aba -> :sswitch_1
        0x1f636b22 -> :sswitch_2
        0x1f636b8a -> :sswitch_3
        0x1f636c12 -> :sswitch_4
        0x2049443a -> :sswitch_5
        0x204a1e7a -> :sswitch_6
        0x2544532a -> :sswitch_7
        0x258c913a -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 7791
    iget-object v0, p0, Lgzh;->b:Lgzk;

    if-eqz v0, :cond_0

    .line 7792
    const v0, 0x3ec6d57

    iget-object v1, p0, Lgzh;->b:Lgzk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7794
    :cond_0
    iget-object v0, p0, Lgzh;->c:Lgzl;

    if-eqz v0, :cond_1

    .line 7795
    const v0, 0x3ec6d64

    iget-object v1, p0, Lgzh;->c:Lgzl;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7797
    :cond_1
    iget-object v0, p0, Lgzh;->d:Lgze;

    if-eqz v0, :cond_2

    .line 7798
    const v0, 0x3ec6d71

    iget-object v1, p0, Lgzh;->d:Lgze;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7800
    :cond_2
    iget-object v0, p0, Lgzh;->e:Lgza;

    if-eqz v0, :cond_3

    .line 7801
    const v0, 0x3ec6d82

    iget-object v1, p0, Lgzh;->e:Lgza;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7803
    :cond_3
    iget-object v0, p0, Lgzh;->f:Lgzb;

    if-eqz v0, :cond_4

    .line 7804
    const v0, 0x4092887

    iget-object v1, p0, Lgzh;->f:Lgzb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7806
    :cond_4
    iget-object v0, p0, Lgzh;->g:Lgzf;

    if-eqz v0, :cond_5

    .line 7807
    const v0, 0x40943cf

    iget-object v1, p0, Lgzh;->g:Lgzf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7809
    :cond_5
    iget-object v0, p0, Lgzh;->h:Lgzi;

    if-eqz v0, :cond_6

    .line 7810
    const v0, 0x4a88a65

    iget-object v1, p0, Lgzh;->h:Lgzi;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7812
    :cond_6
    iget-object v0, p0, Lgzh;->i:Lgyz;

    if-eqz v0, :cond_7

    .line 7813
    const v0, 0x4b19227

    iget-object v1, p0, Lgzh;->i:Lgyz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7815
    :cond_7
    iget-object v0, p0, Lgzh;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 7817
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7760
    if-ne p1, p0, :cond_1

    .line 7771
    :cond_0
    :goto_0
    return v0

    .line 7761
    :cond_1
    instance-of v2, p1, Lgzh;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 7762
    :cond_2
    check-cast p1, Lgzh;

    .line 7763
    iget-object v2, p0, Lgzh;->b:Lgzk;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzh;->b:Lgzk;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzh;->c:Lgzl;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzh;->c:Lgzl;

    if-nez v2, :cond_3

    .line 7764
    :goto_2
    iget-object v2, p0, Lgzh;->d:Lgze;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzh;->d:Lgze;

    if-nez v2, :cond_3

    .line 7765
    :goto_3
    iget-object v2, p0, Lgzh;->e:Lgza;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzh;->e:Lgza;

    if-nez v2, :cond_3

    .line 7766
    :goto_4
    iget-object v2, p0, Lgzh;->f:Lgzb;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgzh;->f:Lgzb;

    if-nez v2, :cond_3

    .line 7767
    :goto_5
    iget-object v2, p0, Lgzh;->g:Lgzf;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgzh;->g:Lgzf;

    if-nez v2, :cond_3

    .line 7768
    :goto_6
    iget-object v2, p0, Lgzh;->h:Lgzi;

    if-nez v2, :cond_a

    iget-object v2, p1, Lgzh;->h:Lgzi;

    if-nez v2, :cond_3

    .line 7769
    :goto_7
    iget-object v2, p0, Lgzh;->i:Lgyz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lgzh;->i:Lgyz;

    if-nez v2, :cond_3

    .line 7770
    :goto_8
    iget-object v2, p0, Lgzh;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lgzh;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 7771
    goto :goto_0

    .line 7763
    :cond_4
    iget-object v2, p0, Lgzh;->b:Lgzk;

    iget-object v3, p1, Lgzh;->b:Lgzk;

    invoke-virtual {v2, v3}, Lgzk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzh;->c:Lgzl;

    iget-object v3, p1, Lgzh;->c:Lgzl;

    .line 7764
    invoke-virtual {v2, v3}, Lgzl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgzh;->d:Lgze;

    iget-object v3, p1, Lgzh;->d:Lgze;

    .line 7765
    invoke-virtual {v2, v3}, Lgze;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzh;->e:Lgza;

    iget-object v3, p1, Lgzh;->e:Lgza;

    .line 7766
    invoke-virtual {v2, v3}, Lgza;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lgzh;->f:Lgzb;

    iget-object v3, p1, Lgzh;->f:Lgzb;

    .line 7767
    invoke-virtual {v2, v3}, Lgzb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgzh;->g:Lgzf;

    iget-object v3, p1, Lgzh;->g:Lgzf;

    .line 7768
    invoke-virtual {v2, v3}, Lgzf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lgzh;->h:Lgzi;

    iget-object v3, p1, Lgzh;->h:Lgzi;

    .line 7769
    invoke-virtual {v2, v3}, Lgzi;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lgzh;->i:Lgyz;

    iget-object v3, p1, Lgzh;->i:Lgyz;

    .line 7770
    invoke-virtual {v2, v3}, Lgyz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lgzh;->I:Ljava/util/List;

    iget-object v3, p1, Lgzh;->I:Ljava/util/List;

    .line 7771
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7775
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 7777
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->b:Lgzk;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 7778
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->c:Lgzl;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 7779
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->d:Lgze;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 7780
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->e:Lgza;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 7781
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->f:Lgzb;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 7782
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->g:Lgzf;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 7783
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->h:Lgzi;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 7784
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzh;->i:Lgyz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 7785
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzh;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 7786
    return v0

    .line 7777
    :cond_0
    iget-object v0, p0, Lgzh;->b:Lgzk;

    invoke-virtual {v0}, Lgzk;->hashCode()I

    move-result v0

    goto :goto_0

    .line 7778
    :cond_1
    iget-object v0, p0, Lgzh;->c:Lgzl;

    invoke-virtual {v0}, Lgzl;->hashCode()I

    move-result v0

    goto :goto_1

    .line 7779
    :cond_2
    iget-object v0, p0, Lgzh;->d:Lgze;

    invoke-virtual {v0}, Lgze;->hashCode()I

    move-result v0

    goto :goto_2

    .line 7780
    :cond_3
    iget-object v0, p0, Lgzh;->e:Lgza;

    invoke-virtual {v0}, Lgza;->hashCode()I

    move-result v0

    goto :goto_3

    .line 7781
    :cond_4
    iget-object v0, p0, Lgzh;->f:Lgzb;

    invoke-virtual {v0}, Lgzb;->hashCode()I

    move-result v0

    goto :goto_4

    .line 7782
    :cond_5
    iget-object v0, p0, Lgzh;->g:Lgzf;

    invoke-virtual {v0}, Lgzf;->hashCode()I

    move-result v0

    goto :goto_5

    .line 7783
    :cond_6
    iget-object v0, p0, Lgzh;->h:Lgzi;

    invoke-virtual {v0}, Lgzi;->hashCode()I

    move-result v0

    goto :goto_6

    .line 7784
    :cond_7
    iget-object v0, p0, Lgzh;->i:Lgyz;

    invoke-virtual {v0}, Lgyz;->hashCode()I

    move-result v0

    goto :goto_7

    .line 7785
    :cond_8
    iget-object v1, p0, Lgzh;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_8
.end method
