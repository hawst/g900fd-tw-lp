.class public final Lcdz;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Leyp;

.field private final c:Lfhz;

.field private final d:Lfdw;

.field private final e:Lfrz;

.field private final f:Lfsj;

.field private g:Lceb;

.field private h:Lceb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p4, p5, p6}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 47
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcdz;->a:Landroid/content/Context;

    .line 48
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcdz;->b:Leyp;

    .line 49
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcdz;->c:Lfhz;

    .line 50
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lcdz;->d:Lfdw;

    .line 52
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lcdz;->e:Lfrz;

    .line 53
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcdz;->f:Lfsj;

    .line 55
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Lfsj;->a(Z)V

    .line 56
    invoke-interface {p3, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method

.method private a(Lfsg;Lfjz;)Landroid/view/View;
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcdz;->a:Landroid/content/Context;

    invoke-static {v0, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 63
    iget-object v0, p0, Lcdz;->h:Lceb;

    if-nez v0, :cond_0

    .line 64
    const v0, 0x7f040071

    invoke-direct {p0, v0}, Lcdz;->a(I)Lceb;

    move-result-object v0

    iput-object v0, p0, Lcdz;->h:Lceb;

    .line 66
    :cond_0
    iget-object v0, p0, Lcdz;->h:Lceb;

    .line 75
    :goto_0
    iget-object v1, p2, Lfjz;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfjz;->a:Lhhj;

    iget-object v1, v1, Lhhj;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjz;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfjz;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lceb;->a(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v1, p2, Lfjz;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfjz;->a:Lhhj;

    iget-object v1, v1, Lhhj;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjz;->d:Ljava/lang/CharSequence;

    :cond_2
    iget-object v1, p2, Lfjz;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lceb;->b(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v1, p2, Lfjz;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p2, Lfjz;->a:Lhhj;

    iget-object v1, v1, Lhhj;->c:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjz;->c:Ljava/lang/CharSequence;

    :cond_3
    iget-object v1, p2, Lfjz;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lceb;->c(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v1, p2, Lfjz;->e:Lfnc;

    if-nez v1, :cond_4

    new-instance v1, Lfnc;

    iget-object v2, p2, Lfjz;->a:Lhhj;

    iget-object v2, v2, Lhhj;->a:Lhxf;

    invoke-direct {v1, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v1, p2, Lfjz;->e:Lfnc;

    :cond_4
    iget-object v1, p2, Lfjz;->e:Lfnc;

    invoke-virtual {v0, v1}, Lceb;->a(Lfnc;)V

    .line 79
    iget-object v1, p0, Lcdz;->f:Lfsj;

    invoke-virtual {v0, v1, p2}, Lceb;->a(Lfsj;Lfjz;)V

    .line 81
    iget-object v0, p0, Lcdz;->f:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 68
    :cond_5
    iget-object v0, p0, Lcdz;->g:Lceb;

    if-nez v0, :cond_6

    .line 69
    const v0, 0x7f0400c5

    invoke-direct {p0, v0}, Lcdz;->a(I)Lceb;

    move-result-object v0

    iput-object v0, p0, Lcdz;->g:Lceb;

    .line 71
    :cond_6
    iget-object v0, p0, Lcdz;->g:Lceb;

    .line 72
    invoke-virtual {v0}, Lceb;->a()V

    goto :goto_0
.end method

.method private a(I)Lceb;
    .locals 7

    .prologue
    .line 85
    new-instance v0, Lceb;

    iget-object v1, p0, Lcdz;->a:Landroid/content/Context;

    iget-object v2, p0, Lcdz;->b:Leyp;

    iget-object v3, p0, Lcdz;->c:Lfhz;

    iget-object v5, p0, Lcdz;->d:Lfdw;

    iget-object v6, p0, Lcdz;->e:Lfrz;

    move v4, p1

    invoke-direct/range {v0 .. v6}, Lceb;-><init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    check-cast p2, Lfjz;

    invoke-direct {p0, p1, p2}, Lcdz;->a(Lfsg;Lfjz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    check-cast p2, Lfjz;

    invoke-direct {p0, p1, p2}, Lcdz;->a(Lfsg;Lfjz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 26
    check-cast p2, Lfjz;

    invoke-direct {p0, p1, p2}, Lcdz;->a(Lfsg;Lfjz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
