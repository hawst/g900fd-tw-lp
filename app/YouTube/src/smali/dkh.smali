.class public Ldkh;
.super Ldjp;
.source "SourceFile"


# instance fields
.field public n:[Ljava/lang/String;

.field public final o:[B

.field public p:Ljava/lang/String;

.field public q:Leue;

.field public r:I

.field public s:I


# direct methods
.method public constructor <init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ldlh;)V
    .locals 8

    .prologue
    .line 77
    iget-object v7, p7, Ldlh;->h:Ldkz;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    move-object v6, p4

    invoke-direct/range {v0 .. v7}, Ldjp;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldaq;Ldkz;)V

    .line 84
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object v0, p7, Ldlh;->a:[Ljava/lang/String;

    iput-object v0, p0, Ldkh;->n:[Ljava/lang/String;

    .line 86
    iget-object v0, p7, Ldlh;->b:[B

    iput-object v0, p0, Ldkh;->o:[B

    .line 87
    iget-object v0, p7, Ldlh;->c:Ljava/lang/String;

    iput-object v0, p0, Ldkh;->p:Ljava/lang/String;

    .line 88
    iget v0, p7, Ldlh;->d:I

    iput v0, p0, Ldkh;->r:I

    .line 89
    iget v0, p7, Ldlh;->e:I

    iput v0, p0, Ldkh;->s:I

    .line 90
    iget-object v0, p7, Ldlh;->f:Lfrl;

    iput-object v0, p0, Ldkh;->g:Lfrl;

    .line 91
    iget-boolean v0, p7, Ldlh;->g:Z

    iput-boolean v0, p0, Ldkh;->j:Z

    .line 92
    iget-object v0, p0, Ldkh;->g:Lfrl;

    if-eqz v0, :cond_0

    .line 93
    sget-object v0, Lgok;->d:Lgok;

    invoke-virtual {p0, v0}, Ldkh;->a(Lgok;)V

    .line 97
    :goto_0
    invoke-virtual {p0}, Ldkh;->h()V

    .line 98
    return-void

    .line 95
    :cond_0
    sget-object v0, Lgok;->a:Lgok;

    invoke-virtual {p0, v0}, Ldkh;->a(Lgok;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ljava/util/List;I[BLjava/lang/String;)V
    .locals 8

    .prologue
    .line 52
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Ldjp;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldaq;)V

    .line 53
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Ldkh;->o:[B

    .line 55
    move-object/from16 v0, p10

    iput-object v0, p0, Ldkh;->p:Ljava/lang/String;

    .line 56
    invoke-interface {p7}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {p7, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    .line 59
    move/from16 v0, p8

    invoke-virtual {p0, v0}, Ldkh;->b(I)I

    move-result v1

    .line 60
    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Ldkh;->r:I

    .line 61
    iput v1, p0, Ldkh;->s:I

    .line 62
    sget-object v1, Lgok;->a:Lgok;

    invoke-virtual {p0, v1}, Ldkh;->a(Lgok;)V

    .line 63
    invoke-virtual {p0}, Ldkh;->h()V

    .line 64
    return-void
.end method

.method private u()V
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Ldkh;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ldkh;->w_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldkh;->c(I)V

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    iget v0, p0, Ldkh;->r:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ldkh;->c(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 205
    invoke-super {p0}, Ldjp;->a()V

    .line 206
    iget-object v0, p0, Ldkh;->q:Leue;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Ldkh;->q:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Ldkh;->q:Leue;

    .line 210
    :cond_0
    iget-object v0, p0, Ldkh;->f:Lgok;

    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Ldkh;->r:I

    if-ltz v0, :cond_1

    iget v0, p0, Ldkh;->r:I

    iget-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 212
    :cond_1
    sget-object v0, Lgok;->a:Lgok;

    invoke-virtual {p0, v0}, Ldkh;->a(Lgok;)V

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_2
    sget-object v0, Lgok;->d:Lgok;

    invoke-virtual {p0, v0}, Ldkh;->a(Lgok;)V

    goto :goto_0
.end method

.method protected final a(Lgok;)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1}, Ldjp;->a(Lgok;)V

    .line 116
    invoke-virtual {p0}, Ldkh;->g()V

    .line 117
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 185
    return-void
.end method

.method protected b(I)I
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Ldkh;->n:[Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    const/4 v0, 0x0

    iget-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Ldkh;->f:Lgok;

    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    iget v0, p0, Ldkh;->r:I

    if-ltz v0, :cond_0

    iget v0, p0, Ldkh;->r:I

    iget-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 237
    iget-object v0, p0, Ldkh;->n:[Ljava/lang/String;

    iget v1, p0, Ldkh;->r:I

    aget-object v0, v0, v1

    .line 242
    :goto_1
    return-object v0

    .line 236
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 238
    :cond_1
    iget v0, p0, Ldkh;->s:I

    if-ltz v0, :cond_2

    iget v0, p0, Ldkh;->s:I

    iget-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 239
    iget-object v0, p0, Ldkh;->n:[Ljava/lang/String;

    iget v1, p0, Ldkh;->s:I

    aget-object v0, v0, v1

    goto :goto_1

    .line 242
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 189
    iput-boolean p1, p0, Ldkh;->j:Z

    .line 190
    invoke-virtual {p0}, Ldkh;->g()V

    .line 191
    return-void
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 252
    const/4 v0, -0x1

    return v0
.end method

.method protected c(I)V
    .locals 8

    .prologue
    const/4 v5, -0x1

    .line 275
    invoke-virtual {p0, p1}, Ldkh;->b(I)I

    move-result v0

    iput v0, p0, Ldkh;->s:I

    .line 276
    iget-object v0, p0, Ldkh;->n:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    if-ltz p1, :cond_1

    .line 277
    new-instance v0, Ldki;

    invoke-direct {v0, p0}, Ldki;-><init>(Ldkh;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldkh;->q:Leue;

    .line 279
    iget-object v0, p0, Ldkh;->a:Ldaq;

    iget-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    aget-object v1, v1, p1

    iget-object v2, p0, Ldkh;->o:[B

    iget-object v3, p0, Ldkh;->p:Ljava/lang/String;

    const-string v4, ""

    iget-object v7, p0, Ldkh;->q:Leue;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    .line 287
    sget-object v0, Lgok;->b:Lgok;

    invoke-virtual {p0, v0}, Ldkh;->a(Lgok;)V

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 288
    :cond_1
    iget-object v0, p0, Ldkh;->n:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 289
    iget v0, p0, Ldkh;->s:I

    iput v0, p0, Ldkh;->r:I

    .line 290
    sget-object v0, Lgok;->f:Lgok;

    invoke-virtual {p0, v0}, Ldkh;->a(Lgok;)V

    goto :goto_0
.end method

.method protected d()[B
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Ldkh;->o:[B

    return-object v0
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Ldkh;->p:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ldkg;
    .locals 9

    .prologue
    .line 102
    new-instance v0, Ldlh;

    iget-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    iget-object v2, p0, Ldkh;->o:[B

    iget-object v3, p0, Ldkh;->p:Ljava/lang/String;

    iget v4, p0, Ldkh;->r:I

    iget v5, p0, Ldkh;->s:I

    iget-object v6, p0, Ldkh;->g:Lfrl;

    iget-boolean v7, p0, Ldkh;->j:Z

    .line 110
    invoke-super {p0}, Ldjp;->f()Ldkg;

    move-result-object v8

    check-cast v8, Ldkz;

    invoke-direct/range {v0 .. v8}, Ldlh;-><init>([Ljava/lang/String;[BLjava/lang/String;IILfrl;ZLdkz;)V

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0}, Ldkh;->a()V

    .line 122
    invoke-super {p0}, Ldjp;->k()V

    .line 123
    iget v0, p0, Ldkh;->s:I

    invoke-virtual {p0, v0}, Ldkh;->c(I)V

    .line 124
    return-void
.end method

.method protected m()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0}, Ldkh;->a()V

    .line 129
    invoke-super {p0}, Ldjp;->n()V

    .line 130
    invoke-direct {p0}, Ldkh;->u()V

    .line 131
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0}, Ldkh;->a()V

    .line 136
    invoke-super {p0}, Ldjp;->o()V

    .line 137
    iget v0, p0, Ldkh;->r:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Ldkh;->c(I)V

    .line 138
    return-void
.end method

.method public final p()V
    .locals 0

    .prologue
    .line 142
    invoke-virtual {p0}, Ldkh;->a()V

    .line 143
    invoke-super {p0}, Ldjp;->p()V

    .line 144
    invoke-direct {p0}, Ldkh;->u()V

    .line 145
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 157
    invoke-virtual {p0}, Ldkh;->a()V

    .line 158
    invoke-super {p0}, Ldjp;->q()V

    .line 159
    iget v0, p0, Ldkh;->s:I

    invoke-virtual {p0, v0}, Ldkh;->c(I)V

    .line 160
    return-void
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    const-string v0, ""

    return-object v0
.end method

.method public s_()Z
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Ldkh;->r:I

    return v0
.end method

.method public final t_()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 169
    iget-boolean v1, p0, Ldkh;->j:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ldkh;->w_()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u_()Z
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Ldkh;->r:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v_()Z
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Ldkh;->t_()Z

    move-result v0

    return v0
.end method

.method protected w_()Z
    .locals 2

    .prologue
    .line 266
    iget v0, p0, Ldkh;->r:I

    iget-object v1, p0, Ldkh;->n:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
