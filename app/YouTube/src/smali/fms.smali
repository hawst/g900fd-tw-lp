.class public Lfms;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;
.implements Lfrx;


# instance fields
.field public final a:Lhvs;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field private final f:Lfqh;

.field private g:Lhog;

.field private h:Lhog;

.field private i:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lhvs;Lfqh;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvs;

    iput-object v0, p0, Lfms;->a:Lhvs;

    .line 37
    iput-object p2, p0, Lfms;->f:Lfqh;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lfms;->i:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lfms;->i:Ljava/util/Set;

    .line 130
    :goto_0
    return-object v0

    .line 122
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfms;->i:Ljava/util/Set;

    .line 123
    invoke-virtual {p0}, Lfms;->c()Lhog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Lfms;->i:Ljava/util/Set;

    invoke-virtual {p0}, Lfms;->c()Lhog;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 126
    :cond_1
    invoke-virtual {p0}, Lfms;->b()Lhog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lfms;->i:Ljava/util/Set;

    invoke-virtual {p0}, Lfms;->b()Lhog;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_2
    iget-object v0, p0, Lfms;->i:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfms;->i:Ljava/util/Set;

    .line 130
    iget-object v0, p0, Lfms;->i:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 135
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 136
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfms;->g:Lhog;

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lfms;->a:Lhvs;

    iget-object v0, v0, Lhvs;->c:Lhog;

    iput-object v0, p0, Lfms;->g:Lhog;

    .line 92
    :cond_0
    iget-object v0, p0, Lfms;->g:Lhog;

    return-object v0
.end method

.method public final c()Lhog;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lfms;->h:Lhog;

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lfms;->a:Lhvs;

    iget-object v0, v0, Lhvs;->f:Lhog;

    iput-object v0, p0, Lfms;->h:Lhog;

    .line 103
    :cond_0
    iget-object v0, p0, Lfms;->h:Lhog;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lfms;->a:Lhvs;

    iget-object v0, v0, Lhvs;->g:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lfms;->f:Lfqh;

    return-object v0
.end method
