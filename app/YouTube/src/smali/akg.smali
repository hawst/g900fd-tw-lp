.class public final Lakg;
.super Lajj;
.source "SourceFile"


# instance fields
.field private final a:Ldeo;

.field private final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ldeo;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lajj;-><init>()V

    .line 24
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lakg;->a:Ldeo;

    .line 25
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lakg;->b:Landroid/os/Handler;

    .line 26
    return-void
.end method

.method static synthetic a(Lakg;)Ldeo;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lakg;->a:Ldeo;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lakg;->b:Landroid/os/Handler;

    new-instance v1, Laki;

    invoke-direct {v1, p0}, Laki;-><init>(Lakg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 46
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lakg;->b:Landroid/os/Handler;

    new-instance v1, Lakk;

    invoke-direct {v1, p0, p1}, Lakk;-><init>(Lakg;F)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 66
    return-void
.end method

.method public final a(Lgpo;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lakg;->b:Landroid/os/Handler;

    new-instance v1, Lakl;

    invoke-direct {v1, p0, p1}, Lakl;-><init>(Lakg;Lgpo;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 76
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lakg;->b:Landroid/os/Handler;

    new-instance v1, Lakh;

    invoke-direct {v1, p0, p1}, Lakh;-><init>(Lakg;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 36
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lakg;->b:Landroid/os/Handler;

    new-instance v1, Lakj;

    invoke-direct {v1, p0}, Lakj;-><init>(Lakg;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 56
    return-void
.end method
