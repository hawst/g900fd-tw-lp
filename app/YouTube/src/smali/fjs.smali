.class public Lfjs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;
.implements Lfrx;


# instance fields
.field public final a:Lhfw;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field private final d:Lfqh;

.field private e:Lhog;

.field private f:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lhfw;Lfqh;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfw;

    iput-object v0, p0, Lfjs;->a:Lhfw;

    .line 32
    iput-object p2, p0, Lfjs;->d:Lfqh;

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lfjs;->f:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lfjs;->b()Lhog;

    move-result-object v0

    if-nez v0, :cond_1

    .line 82
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfjs;->f:Ljava/util/Set;

    .line 87
    :cond_0
    :goto_0
    iget-object v0, p0, Lfjs;->f:Ljava/util/Set;

    return-object v0

    .line 84
    :cond_1
    invoke-virtual {p0}, Lfjs;->b()Lhog;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfjs;->f:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 92
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 93
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lfjs;->e:Lhog;

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lfjs;->a:Lhfw;

    iget-object v0, v0, Lhfw;->c:Lhog;

    iput-object v0, p0, Lfjs;->e:Lhog;

    .line 65
    :cond_0
    iget-object v0, p0, Lfjs;->e:Lhog;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lfjs;->a:Lhfw;

    iget-object v0, v0, Lhfw;->d:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lfjs;->d:Lfqh;

    return-object v0
.end method
