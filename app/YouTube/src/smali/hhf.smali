.class public final Lhhf;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhva;

.field public b:[Lhpk;

.field private c:Lhtx;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59153
    invoke-direct {p0}, Lidf;-><init>()V

    .line 59156
    const/4 v0, 0x0

    iput-object v0, p0, Lhhf;->c:Lhtx;

    .line 59159
    sget-object v0, Lhva;->a:[Lhva;

    iput-object v0, p0, Lhhf;->a:[Lhva;

    .line 59162
    sget-object v0, Lhpk;->a:[Lhpk;

    iput-object v0, p0, Lhhf;->b:[Lhpk;

    .line 59165
    const-string v0, ""

    iput-object v0, p0, Lhhf;->d:Ljava/lang/String;

    .line 59153
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 59238
    .line 59239
    iget-object v0, p0, Lhhf;->c:Lhtx;

    if-eqz v0, :cond_5

    .line 59240
    const/4 v0, 0x1

    iget-object v2, p0, Lhhf;->c:Lhtx;

    .line 59241
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 59243
    :goto_0
    iget-object v2, p0, Lhhf;->a:[Lhva;

    if-eqz v2, :cond_1

    .line 59244
    iget-object v3, p0, Lhhf;->a:[Lhva;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 59245
    if-eqz v5, :cond_0

    .line 59246
    const/4 v6, 0x3

    .line 59247
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 59244
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 59251
    :cond_1
    iget-object v2, p0, Lhhf;->b:[Lhpk;

    if-eqz v2, :cond_3

    .line 59252
    iget-object v2, p0, Lhhf;->b:[Lhpk;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 59253
    if-eqz v4, :cond_2

    .line 59254
    const/4 v5, 0x4

    .line 59255
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 59252
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 59259
    :cond_3
    iget-object v1, p0, Lhhf;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 59260
    const/4 v1, 0x5

    iget-object v2, p0, Lhhf;->d:Ljava/lang/String;

    .line 59261
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59263
    :cond_4
    iget-object v1, p0, Lhhf;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 59264
    iput v0, p0, Lhhf;->J:I

    .line 59265
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59149
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhhf;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhhf;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhhf;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhhf;->c:Lhtx;

    if-nez v0, :cond_2

    new-instance v0, Lhtx;

    invoke-direct {v0}, Lhtx;-><init>()V

    iput-object v0, p0, Lhhf;->c:Lhtx;

    :cond_2
    iget-object v0, p0, Lhhf;->c:Lhtx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhhf;->a:[Lhva;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhva;

    iget-object v3, p0, Lhhf;->a:[Lhva;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhhf;->a:[Lhva;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhhf;->a:[Lhva;

    :goto_2
    iget-object v2, p0, Lhhf;->a:[Lhva;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhhf;->a:[Lhva;

    new-instance v3, Lhva;

    invoke-direct {v3}, Lhva;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhf;->a:[Lhva;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhhf;->a:[Lhva;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhhf;->a:[Lhva;

    new-instance v3, Lhva;

    invoke-direct {v3}, Lhva;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhf;->a:[Lhva;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhhf;->b:[Lhpk;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhpk;

    iget-object v3, p0, Lhhf;->b:[Lhpk;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhhf;->b:[Lhpk;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhhf;->b:[Lhpk;

    :goto_4
    iget-object v2, p0, Lhhf;->b:[Lhpk;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhhf;->b:[Lhpk;

    new-instance v3, Lhpk;

    invoke-direct {v3}, Lhpk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhf;->b:[Lhpk;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhhf;->b:[Lhpk;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lhhf;->b:[Lhpk;

    new-instance v3, Lhpk;

    invoke-direct {v3}, Lhpk;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhf;->b:[Lhpk;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhf;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 59212
    iget-object v1, p0, Lhhf;->c:Lhtx;

    if-eqz v1, :cond_0

    .line 59213
    const/4 v1, 0x1

    iget-object v2, p0, Lhhf;->c:Lhtx;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 59215
    :cond_0
    iget-object v1, p0, Lhhf;->a:[Lhva;

    if-eqz v1, :cond_2

    .line 59216
    iget-object v2, p0, Lhhf;->a:[Lhva;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 59217
    if-eqz v4, :cond_1

    .line 59218
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 59216
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 59222
    :cond_2
    iget-object v1, p0, Lhhf;->b:[Lhpk;

    if-eqz v1, :cond_4

    .line 59223
    iget-object v1, p0, Lhhf;->b:[Lhpk;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 59224
    if-eqz v3, :cond_3

    .line 59225
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 59223
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 59229
    :cond_4
    iget-object v0, p0, Lhhf;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 59230
    const/4 v0, 0x5

    iget-object v1, p0, Lhhf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 59232
    :cond_5
    iget-object v0, p0, Lhhf;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 59234
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59179
    if-ne p1, p0, :cond_1

    .line 59186
    :cond_0
    :goto_0
    return v0

    .line 59180
    :cond_1
    instance-of v2, p1, Lhhf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 59181
    :cond_2
    check-cast p1, Lhhf;

    .line 59182
    iget-object v2, p0, Lhhf;->c:Lhtx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhf;->c:Lhtx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhhf;->a:[Lhva;

    iget-object v3, p1, Lhhf;->a:[Lhva;

    .line 59183
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhf;->b:[Lhpk;

    iget-object v3, p1, Lhhf;->b:[Lhpk;

    .line 59184
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhf;->d:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhf;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 59185
    :goto_2
    iget-object v2, p0, Lhhf;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhhf;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 59186
    goto :goto_0

    .line 59182
    :cond_4
    iget-object v2, p0, Lhhf;->c:Lhtx;

    iget-object v3, p1, Lhhf;->c:Lhtx;

    invoke-virtual {v2, v3}, Lhtx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 59184
    :cond_5
    iget-object v2, p0, Lhhf;->d:Ljava/lang/String;

    iget-object v3, p1, Lhhf;->d:Ljava/lang/String;

    .line 59185
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhhf;->I:Ljava/util/List;

    iget-object v3, p1, Lhhf;->I:Ljava/util/List;

    .line 59186
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 59190
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 59192
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhf;->c:Lhtx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 59193
    iget-object v2, p0, Lhhf;->a:[Lhva;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 59199
    :cond_0
    iget-object v0, p0, Lhhf;->b:[Lhpk;

    if-nez v0, :cond_5

    mul-int/lit8 v2, v2, 0x1f

    .line 59205
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhhf;->d:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 59206
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhhf;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_2
    add-int/2addr v0, v1

    .line 59207
    return v0

    .line 59192
    :cond_2
    iget-object v0, p0, Lhhf;->c:Lhtx;

    invoke-virtual {v0}, Lhtx;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    move v2, v0

    move v0, v1

    .line 59195
    :goto_3
    iget-object v3, p0, Lhhf;->a:[Lhva;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 59196
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhhf;->a:[Lhva;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 59195
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 59196
    :cond_4
    iget-object v2, p0, Lhhf;->a:[Lhva;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhva;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_5
    move v0, v1

    .line 59201
    :goto_5
    iget-object v3, p0, Lhhf;->b:[Lhpk;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 59202
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhhf;->b:[Lhpk;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_6
    add-int/2addr v2, v3

    .line 59201
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 59202
    :cond_6
    iget-object v2, p0, Lhhf;->b:[Lhpk;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhpk;->hashCode()I

    move-result v2

    goto :goto_6

    .line 59205
    :cond_7
    iget-object v0, p0, Lhhf;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 59206
    :cond_8
    iget-object v1, p0, Lhhf;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
