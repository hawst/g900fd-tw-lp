.class public final Lfbr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Labw;


# instance fields
.field private final a:Lws;

.field private final b:Lexd;

.field private final c:Lggr;

.field private d:Lgkl;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lws;Lexd;Lggr;)V
    .locals 4

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lfbr;->a:Lws;

    .line 43
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lfbr;->b:Lexd;

    .line 44
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggr;

    iput-object v0, p0, Lfbr;->c:Lggr;

    .line 45
    invoke-static {}, Lacc;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Lacc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Lacc;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "%s %s app_version/%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    const/4 v0, 0x2

    aput-object p2, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v1, Lfbs;

    invoke-direct {v1, v0}, Lfbs;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lfbr;->d:Lgkl;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 50
    invoke-static {p1, p2}, Lacc;->a(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 51
    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lfbr;->c:Lggr;

    invoke-virtual {v1, v0}, Lggr;->a(Lfao;)Lfao;

    move-result-object v0

    .line 53
    new-instance v1, Lfbq;

    .line 54
    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lfbr;->d:Lgkl;

    sget-object v3, Lggu;->a:Lwu;

    invoke-direct {v1, v0, v2, v3}, Lfbq;-><init>(Ljava/lang/String;Lgkl;Lwu;)V

    .line 59
    iget-object v0, p0, Lfbr;->b:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const-string v2, "Pinging "

    invoke-virtual {v1}, Lfbq;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lfbr;->a:Lws;

    invoke-virtual {v0, v1}, Lws;->a(Lwp;)Lwp;

    .line 63
    :cond_0
    return-void

    .line 60
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
