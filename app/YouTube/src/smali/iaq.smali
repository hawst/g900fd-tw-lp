.class public final Liaq;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Liaq;


# instance fields
.field public b:Lhxf;

.field public c:Lhgz;

.field public d:Lhgz;

.field public e:Lhog;

.field private f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Liaq;

    sput-object v0, Liaq;->a:[Liaq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Liaq;->b:Lhxf;

    iput-object v0, p0, Liaq;->c:Lhgz;

    iput-object v0, p0, Liaq;->d:Lhgz;

    iput-object v0, p0, Liaq;->e:Lhog;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Liaq;->f:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Liaq;->b:Lhxf;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liaq;->b:Lhxf;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Liaq;->c:Lhgz;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Liaq;->c:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Liaq;->d:Lhgz;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Liaq;->d:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Liaq;->e:Lhog;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Liaq;->e:Lhog;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Liaq;->f:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Liaq;->f:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Liaq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Liaq;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Liaq;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Liaq;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Liaq;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Liaq;->b:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Liaq;->b:Lhxf;

    :cond_2
    iget-object v0, p0, Liaq;->b:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Liaq;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Liaq;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Liaq;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Liaq;->d:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Liaq;->d:Lhgz;

    :cond_4
    iget-object v0, p0, Liaq;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Liaq;->e:Lhog;

    if-nez v0, :cond_5

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Liaq;->e:Lhog;

    :cond_5
    iget-object v0, p0, Liaq;->e:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Liaq;->f:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Liaq;->b:Lhxf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liaq;->b:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Liaq;->c:Lhgz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Liaq;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Liaq;->d:Lhgz;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Liaq;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Liaq;->e:Lhog;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Liaq;->e:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Liaq;->f:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Liaq;->f:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_4
    iget-object v0, p0, Liaq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liaq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liaq;

    iget-object v2, p0, Liaq;->b:Lhxf;

    if-nez v2, :cond_4

    iget-object v2, p1, Liaq;->b:Lhxf;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Liaq;->c:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Liaq;->c:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Liaq;->d:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Liaq;->d:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Liaq;->e:Lhog;

    if-nez v2, :cond_7

    iget-object v2, p1, Liaq;->e:Lhog;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Liaq;->f:[B

    iget-object v3, p1, Liaq;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Liaq;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Liaq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liaq;->b:Lhxf;

    iget-object v3, p1, Liaq;->b:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Liaq;->c:Lhgz;

    iget-object v3, p1, Liaq;->c:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Liaq;->d:Lhgz;

    iget-object v3, p1, Liaq;->d:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Liaq;->e:Lhog;

    iget-object v3, p1, Liaq;->e:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Liaq;->I:Ljava/util/List;

    iget-object v3, p1, Liaq;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liaq;->b:Lhxf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liaq;->c:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liaq;->d:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liaq;->e:Lhog;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    iget-object v2, p0, Liaq;->f:[B

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Liaq;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Liaq;->b:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Liaq;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Liaq;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Liaq;->e:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_5
    move v2, v0

    move v0, v1

    :goto_5
    iget-object v3, p0, Liaq;->f:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Liaq;->f:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    iget-object v1, p0, Liaq;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
