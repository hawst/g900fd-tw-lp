.class final Lcsy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lgit;

.field final b:Landroid/os/ConditionVariable;

.field volatile c:Lcua;

.field final synthetic d:Lcsx;


# direct methods
.method constructor <init>(Lcsx;Lgit;)V
    .locals 2

    .prologue
    .line 329
    iput-object p1, p0, Lcsy;->d:Lcsx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    iput-object p2, p0, Lcsy;->a:Lgit;

    .line 331
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcsy;->b:Landroid/os/ConditionVariable;

    .line 333
    iget-object v0, p1, Lcsx;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcsz;

    invoke-direct {v1, p0, p1}, Lcsz;-><init>(Lcsy;Lcsx;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 340
    return-void
.end method

.method constructor <init>(Lcsx;Lgit;Lcua;)V
    .locals 2

    .prologue
    .line 342
    iput-object p1, p0, Lcsy;->d:Lcsx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 343
    iput-object p2, p0, Lcsy;->a:Lgit;

    .line 344
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcsy;->b:Landroid/os/ConditionVariable;

    .line 345
    iput-object p3, p0, Lcsy;->c:Lcua;

    .line 346
    return-void
.end method

.method static a(Landroid/content/ContentValues;Ljava/lang/String;Lfnc;)V
    .locals 6

    .prologue
    .line 547
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 548
    iget-object v0, p2, Lfnc;->a:Lhxf;

    if-nez v0, :cond_2

    new-instance v1, Lhxf;

    invoke-direct {v1}, Lhxf;-><init>()V

    iget-object v0, p2, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    new-array v4, v3, [Lhxg;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_0

    new-instance v5, Lhxg;

    invoke-direct {v5}, Lhxg;-><init>()V

    iget-object v0, p2, Lfnc;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    iget-object v0, v0, Lfnb;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lhxg;->b:Ljava/lang/String;

    aput-object v5, v4, v2

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iput-object v4, v1, Lhxf;->b:[Lhxg;

    :cond_1
    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 552
    :goto_2
    return-void

    .line 548
    :cond_2
    iget-object v0, p2, Lfnc;->a:Lhxf;

    goto :goto_1

    .line 550
    :cond_3
    invoke-virtual {p0, p1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic a(Lcsy;Lcua;)V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p0, Lcsy;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    if-nez p1, :cond_0

    sget-object v0, Lcua;->a:Lcua;

    iput-object v0, p0, Lcsy;->c:Lcua;

    :goto_0
    iget-object v0, p0, Lcsy;->d:Lcsx;

    iget-object v0, v0, Lcsx;->c:Ljava/util/concurrent/Executor;

    new-instance v1, Lcta;

    invoke-direct {v1, p0, p1}, Lcta;-><init>(Lcsy;Lcua;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    iput-object p1, p0, Lcsy;->c:Lcua;

    goto :goto_0
.end method
