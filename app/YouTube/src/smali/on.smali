.class public abstract Lon;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field private static final f:Landroid/view/animation/Interpolator;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Landroid/support/v7/widget/ActionMenuView;

.field public c:Lub;

.field public d:I

.field public e:Lhj;

.field private g:Loo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lon;->f:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Loo;

    invoke-direct {v0, p0}, Loo;-><init>(Lon;)V

    iput-object v0, p0, Lon;->g:Loo;

    .line 67
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010052

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v1, :cond_0

    .line 70
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v1, p1, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lon;->a:Landroid/content/Context;

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_0
    iput-object p1, p0, Lon;->a:Landroid/content/Context;

    goto :goto_0
.end method

.method public static a(IIZ)I
    .locals 1

    .prologue
    .line 238
    if-eqz p2, :cond_0

    sub-int v0, p0, p1

    :goto_0
    return v0

    :cond_0
    add-int v0, p0, p1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;III)I
    .locals 2

    .prologue
    .line 228
    const/high16 v0, -0x80000000

    invoke-static {p1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0, v0, p2}, Landroid/view/View;->measure(II)V

    .line 231
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, p1, v0

    .line 234
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;IIIZ)I
    .locals 4

    .prologue
    .line 242
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 243
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 244
    sub-int v2, p3, v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p2

    .line 246
    if-eqz p4, :cond_1

    .line 247
    sub-int v3, p1, v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v3, v2, p1, v1}, Landroid/view/View;->layout(IIII)V

    .line 252
    :goto_0
    if-eqz p4, :cond_0

    neg-int v0, v0

    :cond_0
    return v0

    .line 249
    :cond_1
    add-int v3, p1, v0

    add-int/2addr v1, v2

    invoke-virtual {p0, p1, v2, v3, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 111
    iput p1, p0, Lon;->d:I

    .line 112
    invoke-virtual {p0}, Lon;->requestLayout()V

    .line 113
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lon;->c:Lub;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lon;->c:Lub;

    invoke-virtual {v0}, Lub;->b()Z

    move-result v0

    .line 180
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0xc8

    const/4 v1, 0x0

    .line 134
    iget-object v0, p0, Lon;->e:Lhj;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lon;->e:Lhj;

    invoke-virtual {v0}, Lhj;->a()V

    .line 137
    :cond_0
    if-nez p1, :cond_2

    .line 138
    invoke-virtual {p0}, Lon;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    invoke-static {p0, v1}, Lfz;->c(Landroid/view/View;F)V

    .line 140
    :cond_1
    invoke-static {p0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lhj;->a(F)Lhj;

    move-result-object v0

    .line 145
    invoke-virtual {v0, v2, v3}, Lhj;->a(J)Lhj;

    .line 146
    sget-object v1, Lon;->f:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lhj;->a(Landroid/view/animation/Interpolator;)Lhj;

    .line 147
    iget-object v1, p0, Lon;->g:Loo;

    invoke-virtual {v1, v0, p1}, Loo;->a(Lhj;I)Loo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhj;->a(Lhv;)Lhj;

    .line 156
    invoke-virtual {v0}, Lhj;->b()V

    .line 174
    :goto_0
    return-void

    .line 159
    :cond_2
    invoke-static {p0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lhj;->a(F)Lhj;

    move-result-object v0

    .line 160
    invoke-virtual {v0, v2, v3}, Lhj;->a(J)Lhj;

    .line 161
    sget-object v1, Lon;->f:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Lhj;->a(Landroid/view/animation/Interpolator;)Lhj;

    .line 162
    iget-object v1, p0, Lon;->g:Loo;

    invoke-virtual {v1, v0, p1}, Loo;->a(Lhj;I)Loo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhj;->a(Lhv;)Lhj;

    .line 171
    invoke-virtual {v0}, Lhj;->b()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 78
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 79
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 84
    :cond_0
    invoke-virtual {p0}, Lon;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lmd;->a:[I

    const v3, 0x7f010053

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 86
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lon;->a(I)V

    .line 87
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 89
    iget-object v0, p0, Lon;->c:Lub;

    if-eqz v0, :cond_2

    .line 90
    iget-object v0, p0, Lon;->c:Lub;

    iget-boolean v1, v0, Lub;->h:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lub;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, v0, Lub;->g:I

    :cond_1
    iget-object v1, v0, Lub;->c:Lnr;

    if-eqz v1, :cond_2

    iget-object v0, v0, Lub;->c:Lnr;

    invoke-virtual {v0, v5}, Lnr;->b(Z)V

    .line 92
    :cond_2
    return-void
.end method
