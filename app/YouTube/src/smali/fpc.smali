.class public Lfpc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgjg;


# instance fields
.field A:Ljava/util/List;

.field B:Ljava/util/List;

.field public C:Ljava/util/List;

.field D:Ljava/util/List;

.field E:Ljava/util/List;

.field F:Ljava/util/List;

.field G:Ljava/util/List;

.field H:Ljava/util/List;

.field I:Ljava/util/List;

.field J:Ljava/util/List;

.field public K:Ljava/util/List;

.field public L:Ljava/util/List;

.field public M:Ljava/util/List;

.field N:Ljava/util/List;

.field public O:Ljava/util/List;

.field public P:Landroid/net/Uri;

.field public Q:Landroid/net/Uri;

.field public R:J

.field public S:I

.field public T:Z

.field public U:Z

.field public V:Z

.field public W:Z

.field public X:Lhqz;

.field public Y:Lfkg;

.field public Z:J

.field public final a:I

.field public aa:Z

.field public ab:Z

.field public ac:Landroid/net/Uri;

.field public ad:Lfoy;

.field public ae:Lfoy;

.field public af:Ljava/util/List;

.field public ag:J

.field public ah:Ljava/util/List;

.field public ai:Lfoo;

.field public aj:Ljava/lang/String;

.field public ak:Ljava/util/List;

.field private al:Lhwn;

.field public b:Ljava/util/List;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[B

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field l:Landroid/net/Uri;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Lfpb;

.field public p:Ljava/lang/String;

.field public q:I

.field public r:Lfrf;

.field public s:Lflp;

.field public t:Lfqy;

.field public u:Lflr;

.field public v:Landroid/net/Uri;

.field w:Ljava/util/List;

.field x:Ljava/util/List;

.field y:Ljava/util/List;

.field z:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1201
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lfpc;-><init>(I)V

    .line 1202
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1205
    iput p1, p0, Lfpc;->a:I

    .line 1206
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfpc;->U:Z

    .line 1207
    sget-object v0, Lfpb;->d:Lfpb;

    iput-object v0, p0, Lfpc;->o:Lfpb;

    .line 1208
    const/4 v0, -0x1

    iput v0, p0, Lfpc;->S:I

    .line 1209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfpc;->ab:Z

    .line 1210
    return-void
.end method


# virtual methods
.method public final a()Lfoy;
    .locals 65

    .prologue
    .line 1728
    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->r:Lfrf;

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->al:Lhwn;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->al:Lhwn;

    iget-object v2, v2, Lhwn;->b:[Lhgy;

    array-length v2, v2

    if-gtz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->al:Lhwn;

    iget-object v2, v2, Lhwn;->c:[Lhgy;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 1731
    :cond_0
    new-instance v3, Lfri;

    const/4 v2, 0x0

    new-array v2, v2, [Lfrj;

    invoke-direct {v3, v2}, Lfri;-><init>([Lfrj;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lfpc;->al:Lhwn;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfpc;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v2, v0, Lfpc;->q:I

    int-to-long v6, v2

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    move-object/from16 v0, p0

    iget-wide v8, v0, Lfpc;->ag:J

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Lfri;->a(Lhwn;Ljava/lang/String;JJZ)Lfrf;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lfpc;->r:Lfrf;

    .line 1735
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->r:Lfrf;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->r:Lfrf;

    iget-object v2, v2, Lfrf;->a:Ljava/util/List;

    if-eqz v2, :cond_3

    .line 1736
    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->r:Lfrf;

    iget-object v2, v2, Lfrf;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfqj;

    .line 1737
    iget-object v4, v2, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v4}, Lfoy;->a(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "modules"

    .line 1738
    iget-object v2, v2, Lfqj;->d:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1739
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lfpc;->W:Z

    goto :goto_0

    .line 1744
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->s:Lflp;

    if-nez v2, :cond_4

    .line 1745
    new-instance v2, Lflp;

    invoke-direct {v2}, Lflp;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfpc;->s:Lflp;

    .line 1748
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lfpc;->t:Lfqy;

    if-nez v2, :cond_5

    .line 1749
    new-instance v2, Lfqy;

    invoke-direct {v2}, Lfqy;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfpc;->t:Lfqy;

    .line 1752
    :cond_5
    new-instance v2, Lfoy;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfpc;->b:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfpc;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lfpc;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lfpc;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfpc;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lfpc;->f:[B

    move-object/from16 v0, p0

    iget-object v9, v0, Lfpc;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lfpc;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lfpc;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lfpc;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lfpc;->l:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v14, v0, Lfpc;->m:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lfpc;->n:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->o:Lfpb;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->p:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lfpc;->q:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->r:Lfrf;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->s:Lflp;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->t:Lfqy;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->u:Lflr;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->v:Landroid/net/Uri;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->w:Ljava/util/List;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->x:Ljava/util/List;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->y:Ljava/util/List;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->z:Ljava/util/List;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->A:Ljava/util/List;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->B:Ljava/util/List;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->C:Ljava/util/List;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->D:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->E:Ljava/util/List;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->F:Ljava/util/List;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->G:Ljava/util/List;

    move-object/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->H:Ljava/util/List;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->I:Ljava/util/List;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->J:Ljava/util/List;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->K:Ljava/util/List;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->L:Ljava/util/List;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->M:Ljava/util/List;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->N:Ljava/util/List;

    move-object/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->O:Ljava/util/List;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->P:Landroid/net/Uri;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->Q:Landroid/net/Uri;

    move-object/from16 v44, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lfpc;->T:Z

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lfpc;->U:Z

    move/from16 v46, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lfpc;->R:J

    move-wide/from16 v47, v0

    move-object/from16 v0, p0

    iget v0, v0, Lfpc;->S:I

    move/from16 v49, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lfpc;->V:Z

    move/from16 v50, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lfpc;->W:Z

    move/from16 v51, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->X:Lhqz;

    move-object/from16 v52, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->Y:Lfkg;

    move-object/from16 v53, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lfpc;->Z:J

    move-wide/from16 v54, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lfpc;->aa:Z

    move/from16 v56, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lfpc;->ab:Z

    move/from16 v57, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->ac:Landroid/net/Uri;

    move-object/from16 v58, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->ad:Lfoy;

    move-object/from16 v59, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->ae:Lfoy;

    move-object/from16 v60, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->af:Ljava/util/List;

    move-object/from16 v61, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->ah:Ljava/util/List;

    move-object/from16 v62, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->ai:Lfoo;

    move-object/from16 v63, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lfpc;->ak:Ljava/util/List;

    move-object/from16 v64, v0

    invoke-direct/range {v2 .. v64}, Lfoy;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lfpb;Ljava/lang/String;ILfrf;Lflp;Lfqy;Lflr;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Landroid/net/Uri;Landroid/net/Uri;ZZJIZZLhqz;Lfkg;JZZLandroid/net/Uri;Lfoy;Lfoy;Ljava/util/List;Ljava/util/List;Lfoo;Ljava/util/List;)V

    .line 1813
    return-object v2
.end method

.method public final a(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1213
    iget-object v0, p0, Lfpc;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->b:Ljava/util/List;

    .line 1216
    :cond_0
    iget-object v0, p0, Lfpc;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1217
    return-object p0
.end method

.method public final a(Lfpf;)Lfpc;
    .locals 1

    .prologue
    .line 1279
    iget-object v0, p0, Lfpc;->A:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1280
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->A:Ljava/util/List;

    .line 1282
    :cond_0
    iget-object v0, p0, Lfpc;->A:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1283
    return-object p0
.end method

.method public final a(Lhgy;)Lfpc;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1229
    iget-object v0, p0, Lfpc;->al:Lhwn;

    if-nez v0, :cond_0

    .line 1230
    new-instance v0, Lhwn;

    invoke-direct {v0}, Lhwn;-><init>()V

    iput-object v0, p0, Lfpc;->al:Lhwn;

    .line 1232
    :cond_0
    invoke-static {}, Lfqm;->l()Ljava/util/Set;

    move-result-object v0

    iget v1, p1, Lhgy;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1233
    iget-object v1, p0, Lfpc;->al:Lhwn;

    iget-object v0, p0, Lfpc;->al:Lhwn;

    iget-object v0, v0, Lhwn;->b:[Lhgy;

    new-array v2, v2, [Lhgy;

    aput-object p1, v2, v3

    invoke-static {v0, v2}, La;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhgy;

    iput-object v0, v1, Lhwn;->b:[Lhgy;

    .line 1238
    :goto_0
    return-object p0

    .line 1235
    :cond_1
    iget-object v1, p0, Lfpc;->al:Lhwn;

    iget-object v0, p0, Lfpc;->al:Lhwn;

    iget-object v0, v0, Lhwn;->c:[Lhgy;

    new-array v2, v2, [Lhgy;

    aput-object p1, v2, v3

    .line 1236
    invoke-static {v0, v2}, La;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhgy;

    iput-object v0, v1, Lhwn;->c:[Lhgy;

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1247
    iget-object v0, p0, Lfpc;->w:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->w:Ljava/util/List;

    .line 1250
    :cond_0
    iget-object v0, p0, Lfpc;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1251
    return-object p0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1120
    invoke-virtual {p0}, Lfpc;->a()Lfoy;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1255
    iget-object v0, p0, Lfpc;->x:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->x:Ljava/util/List;

    .line 1258
    :cond_0
    iget-object v0, p0, Lfpc;->x:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1259
    return-object p0
.end method

.method public final d(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1263
    iget-object v0, p0, Lfpc;->y:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1264
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->y:Ljava/util/List;

    .line 1266
    :cond_0
    iget-object v0, p0, Lfpc;->y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1267
    return-object p0
.end method

.method public final e(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1271
    iget-object v0, p0, Lfpc;->z:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1272
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->z:Ljava/util/List;

    .line 1274
    :cond_0
    iget-object v0, p0, Lfpc;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1275
    return-object p0
.end method

.method public final f(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1287
    iget-object v0, p0, Lfpc;->B:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->B:Ljava/util/List;

    .line 1290
    :cond_0
    iget-object v0, p0, Lfpc;->B:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1291
    return-object p0
.end method

.method public final g(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1303
    iget-object v0, p0, Lfpc;->D:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1304
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->D:Ljava/util/List;

    .line 1306
    :cond_0
    iget-object v0, p0, Lfpc;->D:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1307
    return-object p0
.end method

.method public final h(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1311
    iget-object v0, p0, Lfpc;->E:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1312
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->E:Ljava/util/List;

    .line 1314
    :cond_0
    iget-object v0, p0, Lfpc;->E:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1315
    return-object p0
.end method

.method public final i(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1319
    iget-object v0, p0, Lfpc;->F:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1320
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->F:Ljava/util/List;

    .line 1322
    :cond_0
    iget-object v0, p0, Lfpc;->F:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1323
    return-object p0
.end method

.method public final j(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1327
    iget-object v0, p0, Lfpc;->G:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1328
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->G:Ljava/util/List;

    .line 1330
    :cond_0
    iget-object v0, p0, Lfpc;->G:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1331
    return-object p0
.end method

.method public final k(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lfpc;->H:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1336
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->H:Ljava/util/List;

    .line 1338
    :cond_0
    iget-object v0, p0, Lfpc;->H:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1339
    return-object p0
.end method

.method public final l(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1343
    iget-object v0, p0, Lfpc;->I:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1344
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->I:Ljava/util/List;

    .line 1346
    :cond_0
    iget-object v0, p0, Lfpc;->I:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1347
    return-object p0
.end method

.method public final m(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1351
    iget-object v0, p0, Lfpc;->J:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1352
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->J:Ljava/util/List;

    .line 1354
    :cond_0
    iget-object v0, p0, Lfpc;->J:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1355
    return-object p0
.end method

.method public final n(Landroid/net/Uri;)Lfpc;
    .locals 1

    .prologue
    .line 1383
    iget-object v0, p0, Lfpc;->N:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1384
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfpc;->N:Ljava/util/List;

    .line 1386
    :cond_0
    iget-object v0, p0, Lfpc;->N:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1387
    return-object p0
.end method
