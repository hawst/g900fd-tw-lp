.class public final Lfxg;
.super Lgkr;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/util/Map;[B)V
    .locals 2

    .prologue
    .line 49
    const-string v0, "http"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "stage.gdata.youtube.com"

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "dev.gdata.youtube.com"

    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lgkr;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    .line 50
    return-void

    .line 49
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)Lfxg;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    new-instance v0, Lfxg;

    invoke-direct {v0, p0, v1, v1}, Lfxg;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Lfxg;)Lfxg;
    .locals 3

    .prologue
    .line 111
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    new-instance v0, Lfxg;

    iget-object v1, p1, Lfxg;->b:Ljava/util/Map;

    .line 115
    invoke-static {p1}, Lfxg;->a(Lgkr;)[B

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lfxg;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method

.method public static a(Landroid/net/Uri;[B)Lfxg;
    .locals 2

    .prologue
    .line 77
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance v0, Lfxg;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lfxg;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/net/Uri;)Lfxg;
    .locals 3

    .prologue
    .line 132
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    new-instance v0, Lfxg;

    iget-object v1, p0, Lfxg;->b:Ljava/util/Map;

    invoke-static {p0}, Lfxg;->a(Lgkr;)[B

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lfxg;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    return-object v0
.end method
