.class final Leec;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private synthetic a:Ledx;


# direct methods
.method public constructor <init>(Ledx;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Leec;->a:Ledx;

    .line 352
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 353
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 359
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 367
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :catch_0
    move-exception v0

    .line 372
    :goto_0
    iget-object v1, p0, Leec;->a:Ledx;

    iget-object v1, v1, Ledx;->e:Leed;

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2, v0}, Leed;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 373
    return-void

    .line 361
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Leec;->a:Ledx;

    iget-object v1, v0, Ledx;->d:Ledw;

    iget-object v0, p0, Leec;->a:Ledx;

    iget-object v0, v0, Ledx;->f:Ljava/util/UUID;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaDrm$ProvisionRequest;

    invoke-interface {v1, v0}, Ledw;->a(Landroid/media/MediaDrm$ProvisionRequest;)[B

    move-result-object v0

    goto :goto_0

    .line 364
    :pswitch_1
    iget-object v0, p0, Leec;->a:Ledx;

    iget-object v1, v0, Ledx;->d:Ledw;

    iget-object v0, p0, Leec;->a:Ledx;

    iget-object v0, v0, Ledx;->f:Ljava/util/UUID;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaDrm$KeyRequest;

    invoke-interface {v1, v0}, Ledw;->a(Landroid/media/MediaDrm$KeyRequest;)[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 359
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
