.class public Lcbo;
.super Lfsa;
.source "SourceFile"


# instance fields
.field final a:Lfhz;

.field b:Z

.field private final c:Landroid/content/Context;

.field private final d:Leyp;

.field private final e:Lfsj;

.field private final f:Landroid/content/res/Resources;

.field private final g:Landroid/view/LayoutInflater;

.field private final h:Landroid/widget/LinearLayout;

.field private final i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/FrameLayout;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/LinearLayout;

.field private p:Z

.field private q:Z

.field private r:I

.field private s:Lfio;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 83
    invoke-direct {p0, p5, p6}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 84
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcbo;->c:Landroid/content/Context;

    .line 85
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcbo;->d:Leyp;

    .line 86
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcbo;->e:Lfsj;

    .line 87
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcbo;->a:Lfhz;

    .line 89
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcbo;->f:Landroid/content/res/Resources;

    .line 90
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    .line 91
    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcbo;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 92
    const v0, 0x7f080290

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    .line 93
    const v0, 0x7f080291

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    .line 95
    iput-boolean v3, p0, Lcbo;->b:Z

    .line 96
    iput-boolean v3, p0, Lcbo;->p:Z

    .line 97
    iput-boolean v3, p0, Lcbo;->q:Z

    .line 99
    invoke-interface {p3, v1}, Lfsj;->a(Landroid/view/View;)V

    .line 100
    return-void
.end method

.method private a(Lfsg;Lfio;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 104
    iget-boolean v0, p0, Lcbo;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcbo;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iget v3, p0, Lcbo;->r:I

    if-ne v0, v3, :cond_0

    .line 105
    iget-object v0, p0, Lcbo;->e:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    .line 189
    :goto_0
    return-object v0

    .line 108
    :cond_0
    iget-boolean v0, p0, Lcbo;->p:Z

    if-nez v0, :cond_1

    .line 109
    iput-object p2, p0, Lcbo;->s:Lfio;

    .line 110
    iget-object v0, p0, Lcbo;->s:Lfio;

    iget-object v0, v0, Lfio;->a:Lhas;

    iget-boolean v0, v0, Lhas;->e:Z

    if-nez v0, :cond_8

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcbo;->b:Z

    .line 113
    :cond_1
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v3, 0x7f080299

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 116
    :cond_2
    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcbo;->b()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 117
    iget-object v3, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 119
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f08029d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 120
    iget-object v1, p0, Lcbo;->s:Lfio;

    iget-object v3, v1, Lfio;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_3

    iget-object v3, v1, Lfio;->a:Lhas;

    iget-object v3, v3, Lhas;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfio;->b:Ljava/lang/CharSequence;

    :cond_3
    iget-object v1, v1, Lfio;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f08029e

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbo;->n:Landroid/widget/TextView;

    .line 123
    iget-object v0, p0, Lcbo;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcbo;->s:Lfio;

    iget-object v3, v1, Lfio;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_4

    iget-object v3, v1, Lfio;->a:Lhas;

    iget-object v3, v3, Lhas;->f:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v1, Lfio;->c:Ljava/lang/CharSequence;

    :cond_4
    iget-object v1, v1, Lfio;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f08029c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcbo;->m:Landroid/widget/ImageView;

    .line 126
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f08029a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcbo;->j:Landroid/widget/FrameLayout;

    .line 128
    iget-object v0, p0, Lcbo;->j:Landroid/widget/FrameLayout;

    new-instance v1, Lcbp;

    invoke-direct {v1, p0}, Lcbp;-><init>(Lcbo;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f080293

    .line 137
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 138
    iget-object v1, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v3, 0x7f080294

    .line 139
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/youtube/common/ui/FixedAspectRatioRelativeLayout;

    .line 140
    iget-object v3, p0, Lcbo;->f:Landroid/content/res/Resources;

    const v4, 0x7f0c0005

    invoke-virtual {v3, v4, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v3

    iput v3, v1, Lcom/google/android/libraries/youtube/common/ui/FixedAspectRatioRelativeLayout;->a:F

    .line 141
    const v1, 0x7f080296

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_5

    .line 143
    const v1, 0x7f080295

    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 145
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 147
    :cond_5
    iget-object v1, p0, Lcbo;->s:Lfio;

    invoke-virtual {v1}, Lfio;->a()Lfnq;

    move-result-object v3

    .line 148
    const v1, 0x7f080337

    .line 151
    invoke-virtual {v3}, Lfnq;->c()Lfnc;

    move-result-object v4

    .line 148
    invoke-direct {p0, v0, v1, v4}, Lcbo;->a(Landroid/view/View;ILfnc;)V

    .line 152
    const v1, 0x7f080338

    .line 155
    invoke-virtual {v3}, Lfnq;->d()Lfnc;

    move-result-object v4

    .line 152
    invoke-direct {p0, v0, v1, v4}, Lcbo;->a(Landroid/view/View;ILfnc;)V

    .line 156
    const v1, 0x7f080339

    .line 159
    invoke-virtual {v3}, Lfnq;->e()Lfnc;

    move-result-object v4

    .line 156
    invoke-direct {p0, v0, v1, v4}, Lcbo;->a(Landroid/view/View;ILfnc;)V

    .line 161
    const v1, 0x7f080298

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 162
    invoke-virtual {v3}, Lfnq;->f()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    new-instance v1, Lcbq;

    invoke-direct {v1, p0, v3}, Lcbq;-><init>(Lcbo;Lfnq;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    iget-boolean v1, p0, Lcbo;->p:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcbo;->q:Z

    if-eqz v1, :cond_6

    .line 175
    invoke-direct {p0}, Lcbo;->e()V

    .line 177
    :cond_6
    invoke-direct {p0}, Lcbo;->f()V

    .line 178
    iput-boolean v2, p0, Lcbo;->p:Z

    .line 179
    iget-object v1, p0, Lcbo;->f:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcbo;->r:I

    .line 180
    iget v1, p0, Lcbo;->r:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 182
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 183
    const/high16 v2, 0x40000000    # 2.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 184
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 185
    iget-object v0, p0, Lcbo;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 186
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 187
    iget-object v1, p0, Lcbo;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    :cond_7
    iget-object v0, p0, Lcbo;->e:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 110
    goto/16 :goto_1
.end method

.method private a(Landroid/view/View;ILfnc;)V
    .locals 3

    .prologue
    .line 372
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 374
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfvi;

    .line 375
    if-nez v1, :cond_0

    .line 376
    new-instance v1, Lfvi;

    iget-object v2, p0, Lcbo;->d:Leyp;

    invoke-direct {v1, v2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    .line 377
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 380
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {v1, p3, v0}, Lfvi;->a(Lfnc;Leyo;)V

    .line 381
    invoke-virtual {p3}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lfvi;->a(I)V

    .line 382
    return-void

    .line 381
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic a(Lcbo;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcbo;->f()V

    return-void
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v8, -0x1

    const/4 v3, 0x0

    .line 232
    iget-object v0, p0, Lcbo;->s:Lfio;

    invoke-virtual {v0}, Lfio;->g()Ljava/util/List;

    move-result-object v4

    .line 233
    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 234
    iget-boolean v0, p0, Lcbo;->q:Z

    if-nez v0, :cond_1

    .line 235
    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcbo;->c()I

    move-result v1

    iget-object v2, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 236
    iget-object v0, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 237
    iget-object v1, p0, Lcbo;->s:Lfio;

    iget-object v2, v1, Lfio;->g:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    iget-object v2, v1, Lfio;->a:Lhas;

    iget-object v2, v2, Lhas;->c:Liaz;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lfio;->a:Lhas;

    iget-object v2, v2, Lhas;->c:Liaz;

    iget-object v2, v2, Liaz;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfio;->g:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, v1, Lfio;->g:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcbo;->d()I

    move-result v1

    iget-object v2, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 241
    :cond_1
    iget-object v0, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    .line 242
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcbo;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    .line 243
    iget-object v0, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v8, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 245
    iget-object v0, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 246
    iget-object v0, p0, Lcbo;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    .line 247
    iget-object v1, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 248
    iget-object v0, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 253
    :goto_0
    iget-object v0, p0, Lcbo;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 255
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 256
    iget-object v1, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move v2, v3

    .line 258
    :goto_1
    if-ge v2, v5, :cond_3

    .line 259
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnu;

    iget-object v1, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    const v6, 0x7f040136

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v1, 0x7f08008b

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lfnu;->c()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0800a4

    invoke-virtual {v0}, Lfnu;->d()Lfnc;

    move-result-object v7

    invoke-direct {p0, v6, v1, v7}, Lcbo;->a(Landroid/view/View;ILfnc;)V

    iget-object v0, v0, Lfnu;->a:Liba;

    iget-object v0, v0, Liba;->d:Lhog;

    new-instance v1, Lcbt;

    invoke-direct {v1, p0, v0}, Lcbt;-><init>(Lcbo;Lhog;)V

    invoke-virtual {v6, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    iget-object v0, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 261
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v8, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 264
    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 258
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 250
    :cond_2
    iget-object v0, p0, Lcbo;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0

    .line 267
    :cond_3
    return-void
.end method

.method private f()V
    .locals 13

    .prologue
    const v12, 0x7f08008b

    const/16 v2, 0x8

    const/4 v11, 0x5

    const/4 v3, 0x0

    const/4 v10, 0x1

    .line 270
    iget-boolean v0, p0, Lcbo;->b:Z

    if-eqz v0, :cond_8

    .line 271
    iget-boolean v0, p0, Lcbo;->q:Z

    if-nez v0, :cond_6

    .line 272
    iget-object v0, p0, Lcbo;->s:Lfio;

    invoke-virtual {v0}, Lfio;->b()V

    invoke-virtual {p0}, Lcbo;->c()I

    move-result v4

    invoke-virtual {p0}, Lcbo;->d()I

    move-result v5

    iget-object v0, p0, Lcbo;->s:Lfio;

    invoke-virtual {v0}, Lfio;->c()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcbo;->s:Lfio;

    iget-boolean v2, v1, Lfio;->e:Z

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lfio;->b()V

    :cond_0
    iget-object v1, v1, Lfio;->d:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move v2, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v2, v0, :cond_2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_1
    iget-object v7, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lfnw;

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    const v8, 0x7f040139

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lfnw;->d()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f08012f

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lfnw;->e()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0800a4

    invoke-virtual {v1}, Lfnw;->f()Lfnc;

    move-result-object v9

    invoke-direct {p0, v8, v0, v9}, Lcbo;->a(Landroid/view/View;ILfnc;)V

    iget-object v0, v1, Lfnw;->a:Libd;

    iget-object v0, v0, Libd;->f:Lhog;

    const v1, 0x7f08033c

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v9, Lcbr;

    invoke-direct {v9, p0, v0}, Lcbr;-><init>(Lcbo;Lhog;)V

    invoke-virtual {v1, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcbo;->s:Lfio;

    invoke-virtual {v0}, Lfio;->f()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcbo;->s:Lfio;

    iget-boolean v2, v1, Lfio;->e:Z

    if-nez v2, :cond_3

    invoke-virtual {v1}, Lfio;->b()V

    :cond_3
    iget-object v1, v1, Lfio;->f:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v2, v0, :cond_5

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_4
    iget-object v4, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lfnp;

    iget-object v0, p0, Lcbo;->g:Landroid/view/LayoutInflater;

    const v7, 0x7f04012e

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lfnp;->c()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f080335

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Lfnp;->d()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0800a4

    invoke-virtual {v1}, Lfnp;->e()Lfnc;

    move-result-object v8

    invoke-direct {p0, v7, v0, v8}, Lcbo;->a(Landroid/view/View;ILfnc;)V

    iget-object v0, v1, Lfnp;->a:Liaq;

    iget-object v0, v0, Liaq;->e:Lhog;

    const v1, 0x7f080334

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v8, Lcbs;

    invoke-direct {v8, p0, v0}, Lcbs;-><init>(Lcbo;Lhog;)V

    invoke-virtual {v1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 273
    :cond_5
    invoke-direct {p0}, Lcbo;->e()V

    .line 274
    iput-boolean v10, p0, Lcbo;->q:Z

    .line 276
    :cond_6
    iget-object v0, p0, Lcbo;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v10, :cond_7

    .line 277
    iget-object v0, p0, Lcbo;->n:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcbo;->j:Landroid/widget/FrameLayout;

    const v1, 0x7f08029b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 280
    invoke-virtual {v0, v1, v1, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 284
    :goto_2
    iget-object v0, p0, Lcbo;->m:Landroid/widget/ImageView;

    const v1, 0x7f0200fa

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 285
    iget-object v0, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 298
    :goto_3
    return-void

    .line 282
    :cond_7
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f080292

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 287
    :cond_8
    iget-object v0, p0, Lcbo;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v10, :cond_9

    .line 288
    iget-object v0, p0, Lcbo;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    iget-object v0, p0, Lcbo;->j:Landroid/widget/FrameLayout;

    const v1, 0x7f08029b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 290
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 291
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 295
    :goto_4
    iget-object v0, p0, Lcbo;->m:Landroid/widget/ImageView;

    const v1, 0x7f02011d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 296
    iget-object v0, p0, Lcbo;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 293
    :cond_9
    iget-object v0, p0, Lcbo;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f080292

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 385
    const v0, 0x7f04012d

    return v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    check-cast p2, Lfio;

    invoke-direct {p0, p1, p2}, Lcbo;->a(Lfsg;Lfio;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    check-cast p2, Lfio;

    invoke-direct {p0, p1, p2}, Lcbo;->a(Lfsg;Lfio;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 389
    const v0, 0x7f040131

    return v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 393
    const v0, 0x7f040138

    return v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 397
    const v0, 0x7f040133

    return v0
.end method
