.class public abstract Lecc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leav;


# instance fields
.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 338
    return-void
.end method

.method public abstract a(J)V
.end method

.method public abstract a(JJ)V
.end method

.method public a(JZ)V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method final b(JZ)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 134
    iget v1, p0, Lecc;->g:I

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 135
    const/4 v0, 0x2

    iput v0, p0, Lecc;->g:I

    .line 136
    invoke-virtual {p0, p1, p2, p3}, Lecc;->a(JZ)V

    .line 137
    return-void

    .line 134
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    .prologue
    .line 193
    return-void
.end method

.method public abstract d()Z
.end method

.method public abstract e()Z
.end method

.method public abstract f()J
.end method

.method public g()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public abstract h()I
.end method

.method public l()V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public abstract m()J
.end method

.method public abstract n()J
.end method

.method final o()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 103
    iget v0, p0, Lecc;->g:I

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 104
    invoke-virtual {p0}, Lecc;->h()I

    move-result v0

    iput v0, p0, Lecc;->g:I

    .line 105
    iget v0, p0, Lecc;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, Lecc;->g:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lecc;->g:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-static {v2}, La;->c(Z)V

    .line 108
    iget v0, p0, Lecc;->g:I

    return v0

    :cond_2
    move v0, v2

    .line 103
    goto :goto_0
.end method

.method final p()V
    .locals 2

    .prologue
    .line 159
    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 160
    const/4 v0, 0x3

    iput v0, p0, Lecc;->g:I

    .line 161
    invoke-virtual {p0}, Lecc;->b()V

    .line 162
    return-void

    .line 159
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final q()V
    .locals 2

    .prologue
    .line 179
    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 180
    const/4 v0, 0x2

    iput v0, p0, Lecc;->g:I

    .line 181
    invoke-virtual {p0}, Lecc;->c()V

    .line 182
    return-void

    .line 179
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final r()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 199
    iget v0, p0, Lecc;->g:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 200
    iput v1, p0, Lecc;->g:I

    .line 201
    invoke-virtual {p0}, Lecc;->g()V

    .line 202
    return-void

    .line 199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final s()V
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 219
    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lecc;->g:I

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 222
    iput v2, p0, Lecc;->g:I

    .line 223
    invoke-virtual {p0}, Lecc;->l()V

    .line 224
    return-void

    .line 219
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
