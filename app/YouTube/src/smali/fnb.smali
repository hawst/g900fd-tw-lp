.class public final Lfnb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/net/Uri;

.field final b:I

.field final c:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iput-object p1, p0, Lfnb;->a:Landroid/net/Uri;

    .line 31
    iput v0, p0, Lfnb;->b:I

    .line 32
    iput v0, p0, Lfnb;->c:I

    .line 33
    return-void
.end method

.method public constructor <init>(Lhxg;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    iget-object v0, p1, Lhxg;->b:Ljava/lang/String;

    invoke-static {v0}, La;->M(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 23
    iput-object v0, p0, Lfnb;->a:Landroid/net/Uri;

    .line 24
    iget v0, p1, Lhxg;->c:I

    iput v0, p0, Lfnb;->b:I

    .line 25
    iget v0, p1, Lhxg;->d:I

    iput v0, p0, Lfnb;->c:I

    .line 26
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 59
    if-ne p0, p1, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v0

    .line 63
    :cond_1
    instance-of v2, p1, Lfnb;

    if-eqz v2, :cond_4

    .line 64
    check-cast p1, Lfnb;

    .line 65
    iget-object v2, p0, Lfnb;->a:Landroid/net/Uri;

    if-nez v2, :cond_2

    .line 66
    iget-object v2, p1, Lfnb;->a:Landroid/net/Uri;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 68
    :cond_2
    iget-object v2, p0, Lfnb;->a:Landroid/net/Uri;

    iget-object v3, p1, Lfnb;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lfnb;->b:I

    iget v3, p1, Lfnb;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lfnb;->c:I

    iget v3, p1, Lfnb;->c:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 73
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lfnb;->a:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 52
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfnb;->b:I

    add-int/2addr v0, v1

    .line 53
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lfnb;->c:I

    add-int/2addr v0, v1

    .line 54
    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lfnb;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    goto :goto_0
.end method
