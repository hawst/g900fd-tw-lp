.class public abstract Lida;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([BII)Lida;
    .locals 2

    .prologue
    .line 115
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Licx;

    invoke-direct {v1, p1, v0, p3}, Licx;-><init>([BII)V

    .line 116
    invoke-virtual {p0, v1}, Lida;->a(Licx;)Lida;

    .line 117
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Licx;->a(I)V
    :try_end_0
    .catch Licz; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 118
    return-object p0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    throw v0

    .line 122
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(Licx;)Lida;
.end method

.method public abstract a(Licy;)V
.end method

.method public abstract b()I
.end method

.method public final b([B)Lida;
    .locals 2

    .prologue
    .line 105
    const/4 v0, 0x0

    array-length v1, p1

    invoke-direct {p0, p1, v0, v1}, Lida;->a([BII)Lida;

    move-result-object v0

    return-object v0
.end method

.method public final c()[B
    .locals 4

    .prologue
    .line 75
    invoke-virtual {p0}, Lida;->b()I

    move-result v0

    new-array v0, v0, [B

    .line 76
    array-length v1, v0

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Licy;

    invoke-direct {v3, v0, v2, v1}, Licy;-><init>([BII)V

    invoke-virtual {p0, v3}, Lida;->a(Licy;)V

    iget v1, v3, Licy;->a:I

    iget v2, v3, Licy;->b:I

    sub-int/2addr v1, v2

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Did not write as much data as expected."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_0
    return-object v0
.end method
