.class public final Ldgo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldhi;


# instance fields
.field private final a:Ldif;

.field private final b:Ldaq;

.field private final c:Lgeh;

.field private final d:Lffd;

.field private final e:Ldhv;

.field private final f:Ldgp;

.field private final g:Landroid/content/SharedPreferences;

.field private final h:Lcyd;

.field private final i:Lfac;

.field private final j:Levn;

.field private final k:Lezj;

.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldif;Ldaq;Lgeh;Lffd;Ldhv;Ldgp;Landroid/content/SharedPreferences;Lcyd;Levn;Lfac;Lezj;)V
    .locals 13

    .prologue
    .line 99
    .line 110
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 111
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcyd;

    invoke-interface {v1}, Lcyd;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f09010c

    .line 110
    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object v0, p0

    move-object v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    .line 99
    invoke-direct/range {v0 .. v12}, Ldgo;-><init>(Ldif;Ldaq;Lgeh;Lffd;Ldhv;Ldgp;Landroid/content/SharedPreferences;Lcyd;Levn;Lfac;Lezj;Ljava/lang/String;)V

    .line 113
    return-void

    .line 111
    :cond_0
    const v1, 0x7f09010b

    goto :goto_0
.end method

.method private constructor <init>(Ldif;Ldaq;Lgeh;Lffd;Ldhv;Ldgp;Landroid/content/SharedPreferences;Lcyd;Levn;Lfac;Lezj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldif;

    iput-object v0, p0, Ldgo;->a:Ldif;

    .line 131
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    iput-object v0, p0, Ldgo;->b:Ldaq;

    .line 132
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeh;

    iput-object v0, p0, Ldgo;->c:Lgeh;

    .line 133
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffd;

    iput-object v0, p0, Ldgo;->d:Lffd;

    .line 134
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldhv;

    iput-object v0, p0, Ldgo;->e:Ldhv;

    .line 136
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgp;

    iput-object v0, p0, Ldgo;->f:Ldgp;

    .line 137
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Ldgo;->g:Landroid/content/SharedPreferences;

    .line 138
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyd;

    iput-object v0, p0, Ldgo;->h:Lcyd;

    .line 139
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldgo;->j:Levn;

    .line 140
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfac;

    iput-object v0, p0, Ldgo;->i:Lfac;

    .line 141
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Ldgo;->k:Lezj;

    .line 142
    invoke-static {p12}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldgo;->l:Ljava/lang/String;

    .line 143
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Ldgo;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "preload_videos_last_sync_millis"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 264
    return-void
.end method

.method private c()J
    .locals 4

    .prologue
    .line 255
    iget-object v0, p0, Ldgo;->g:Landroid/content/SharedPreferences;

    const-string v1, "preload_videos_last_sync_millis"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Ldgo;->e:Ldhv;

    invoke-interface {v0}, Ldhv;->b()V

    .line 270
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Ldgo;->a(J)V

    .line 271
    return-void
.end method


# virtual methods
.method public final a(Lgjm;Ldnb;)Ldhh;
    .locals 14

    .prologue
    .line 213
    new-instance v9, Lhsx;

    invoke-direct {v9}, Lhsx;-><init>()V

    .line 216
    :try_start_0
    iget-object v0, p1, Lgjm;->g:Lgje;

    const-string v1, "preloadVideoRendererProto"

    .line 218
    invoke-virtual {v0, v1}, Lgje;->b(Ljava/lang/String;)[B

    move-result-object v0

    .line 216
    invoke-static {v9, v0}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    new-instance v0, Ldhh;

    iget-object v1, p0, Ldgo;->a:Ldif;

    .line 224
    invoke-virtual {v1}, Ldif;->a()Ldix;

    move-result-object v1

    iget-object v2, p0, Ldgo;->b:Ldaq;

    iget-object v3, p0, Ldgo;->c:Lgeh;

    iget-object v4, p0, Ldgo;->j:Levn;

    iget-object v5, p0, Ldgo;->k:Lezj;

    iget-object v7, p1, Lgjm;->a:Ljava/lang/String;

    iget-object v6, p1, Lgjm;->g:Lgje;

    const-string v8, "preloadId"

    .line 231
    const/4 v10, 0x0

    invoke-virtual {v6, v8, v10}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v6, p1, Lgjm;->g:Lgje;

    const-string v10, "taskCreationWallClockMillis"

    .line 233
    const-wide/16 v12, 0x0

    invoke-virtual {v6, v10, v12, v13}, Lgje;->b(Ljava/lang/String;J)J

    move-result-wide v10

    move-object/from16 v6, p2

    invoke-direct/range {v0 .. v11}, Ldhh;-><init>(Ldix;Ldaq;Lgeh;Levn;Lezj;Ldnb;Ljava/lang/String;Ljava/lang/String;Lhsx;J)V

    :goto_0
    return-object v0

    .line 219
    :catch_0
    move-exception v0

    .line 220
    const-string v1, "Invalid preload video renderer proto for the task."

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 221
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Ldgo;->h:Lcyd;

    invoke-interface {v0}, Lcyd;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    invoke-direct {p0}, Ldgo;->d()V

    .line 163
    :goto_0
    return-void

    .line 151
    :cond_0
    invoke-direct {p0}, Ldgo;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 154
    iget-object v0, p0, Ldgo;->e:Ldhv;

    invoke-interface {v0}, Ldhv;->a()V

    .line 157
    :cond_1
    iget-object v0, p0, Ldgo;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "preload_videos_network_policy_string"

    iget-object v2, p0, Ldgo;->l:Ljava/lang/String;

    .line 158
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "preload_videos_charging_only"

    iget-object v0, p0, Ldgo;->h:Lcyd;

    .line 161
    invoke-interface {v0}, Lcyd;->g()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 159
    :goto_1
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 161
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 167
    invoke-static {}, Lb;->b()V

    .line 169
    iget-object v0, p0, Ldgo;->h:Lcyd;

    invoke-interface {v0}, Lcyd;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    invoke-direct {p0}, Ldgo;->d()V

    .line 204
    :goto_0
    return v1

    .line 175
    :cond_0
    iget-object v0, p0, Ldgo;->j:Levn;

    new-instance v3, Ldgu;

    invoke-direct {v3}, Ldgu;-><init>()V

    invoke-virtual {v0, v3}, Levn;->d(Ljava/lang/Object;)V

    .line 178
    iget-object v0, p0, Ldgo;->i:Lfac;

    const/16 v3, 0xc

    invoke-virtual {v0, v3}, Lfac;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 180
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Ldgo;->k:Lezj;

    invoke-virtual {v3}, Lezj;->a()J

    move-result-wide v6

    invoke-direct {p0}, Ldgo;->c()J

    move-result-wide v8

    sub-long/2addr v6, v8

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v6

    long-to-int v3, v6

    .line 181
    iget-object v0, p0, Ldgo;->j:Levn;

    new-instance v5, Ldgs;

    invoke-direct {v5, v4, v3}, Ldgs;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v5}, Levn;->d(Ljava/lang/Object;)V

    .line 184
    iget-object v0, p0, Ldgo;->d:Lffd;

    new-instance v5, Lfff;

    iget-object v6, v0, Lffd;->b:Lfsz;

    iget-object v7, v0, Lffd;->c:Lgix;

    invoke-interface {v7}, Lgix;->d()Lgit;

    move-result-object v7

    iget-object v8, v0, Lffd;->g:Lfth;

    invoke-direct {v5, v6, v7, v8}, Lfff;-><init>(Lfsz;Lgit;Lfth;)V

    iget-object v0, v0, Lffd;->e:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lfff;->a:Ljava/lang/String;

    .line 185
    if-ltz v3, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lb;->b(Z)V

    iput v3, v5, Lfff;->b:I

    .line 188
    :try_start_0
    iget-object v0, p0, Ldgo;->d:Lffd;

    iget-object v0, v0, Lffd;->f:Lffe;

    invoke-virtual {v0, v5}, Lffe;->c(Lfsp;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsw;

    .line 189
    iget-object v3, p0, Ldgo;->j:Levn;

    new-instance v5, Ldgt;

    iget-object v6, v0, Lhsw;->a:[Lhsy;

    array-length v6, v6

    invoke-direct {v5, v6}, Ldgt;-><init>(I)V

    invoke-virtual {v3, v5}, Levn;->d(Ljava/lang/Object;)V

    .line 193
    iget-object v3, p0, Ldgo;->k:Lezj;

    invoke-virtual {v3}, Lezj;->a()J

    move-result-wide v6

    .line 194
    iget-object v5, v0, Lhsw;->a:[Lhsy;

    array-length v8, v5

    move v3, v2

    :goto_2
    if-ge v3, v8, :cond_3

    aget-object v9, v5, v3

    .line 195
    iget-object v10, v9, Lhsy;->b:Lhsx;

    if-eqz v10, :cond_1

    .line 196
    iget-object v9, v9, Lhsy;->b:Lhsx;

    invoke-static {v9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, v9, Lhsx;->a:Ljava/lang/String;

    invoke-static {v10}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    new-instance v10, Lgje;

    invoke-direct {v10}, Lgje;-><init>()V

    const-string v11, "preloadId"

    invoke-virtual {v10, v11, v4}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "preloadVideoRendererProto"

    invoke-static {v9}, Lidh;->a(Lidh;)[B

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lgje;->a(Ljava/lang/String;[B)V

    const-string v11, "taskCreationWallClockMillis"

    invoke-virtual {v10, v11, v6, v7}, Lgje;->a(Ljava/lang/String;J)V

    iget-object v11, p0, Ldgo;->f:Ldgp;

    invoke-virtual {v11}, Ldgp;->a()Ldmz;

    move-result-object v11

    iget-object v9, v9, Lhsx;->a:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v11, v9, v12, v10}, Ldmz;->a(Ljava/lang/String;Ljava/lang/String;Lgje;)V

    .line 194
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move v0, v2

    .line 185
    goto :goto_1

    .line 199
    :cond_3
    iget-object v3, p0, Ldgo;->k:Lezj;

    invoke-virtual {v3}, Lezj;->a()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Ldgo;->a(J)V

    .line 200
    iget-object v3, p0, Ldgo;->e:Ldhv;

    iget v0, v0, Lhsw;->b:I

    int-to-long v4, v0

    invoke-interface {v3, v4, v5}, Ldhv;->a(J)V
    :try_end_0
    .catch Lfdv; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 201
    iget-object v0, p0, Ldgo;->j:Levn;

    new-instance v2, Ldgr;

    invoke-direct {v2}, Ldgr;-><init>()V

    invoke-virtual {v0, v2}, Levn;->d(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 203
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "Error requesting for preload videos service"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    iget-object v0, p0, Ldgo;->j:Levn;

    new-instance v1, Ldgr;

    invoke-direct {v1}, Ldgr;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    move v1, v2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldgo;->j:Levn;

    new-instance v2, Ldgr;

    invoke-direct {v2}, Ldgr;-><init>()V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    throw v0
.end method
