.class public final enum Lczc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lczc;

.field public static final enum b:Lczc;

.field public static final enum c:Lczc;

.field public static final enum d:Lczc;

.field public static final enum e:Lczc;

.field public static final enum f:Lczc;

.field public static final enum g:Lczc;

.field public static final enum h:Lczc;

.field public static final enum i:Lczc;

.field public static final enum j:Lczc;

.field public static final enum k:Lczc;

.field private static final synthetic l:[Lczc;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lczc;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->a:Lczc;

    .line 26
    new-instance v0, Lczc;

    const-string v1, "VIDEO_ERROR"

    invoke-direct {v0, v1, v4}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->b:Lczc;

    .line 31
    new-instance v0, Lczc;

    const-string v1, "UNPLAYABLE"

    invoke-direct {v0, v1, v5}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->c:Lczc;

    .line 36
    new-instance v0, Lczc;

    const-string v1, "REQUEST_FAILED"

    invoke-direct {v0, v1, v6}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->d:Lczc;

    .line 42
    new-instance v0, Lczc;

    const-string v1, "USER_CHECK_FAILED"

    invoke-direct {v0, v1, v7}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->e:Lczc;

    .line 48
    new-instance v0, Lczc;

    const-string v1, "LICENSE_SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->f:Lczc;

    .line 54
    new-instance v0, Lczc;

    const-string v1, "LICENSE_SERVER_CONCURRENT_PLAYBACK_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->g:Lczc;

    .line 59
    new-instance v0, Lczc;

    const-string v1, "PLAYER_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->h:Lczc;

    .line 64
    new-instance v0, Lczc;

    const-string v1, "NO_STREAMS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->i:Lczc;

    .line 70
    new-instance v0, Lczc;

    const-string v1, "WATCH_NEXT_ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->j:Lczc;

    .line 76
    new-instance v0, Lczc;

    const-string v1, "UNPLAYABLE_IN_BACKGROUND"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lczc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lczc;->k:Lczc;

    .line 16
    const/16 v0, 0xb

    new-array v0, v0, [Lczc;

    sget-object v1, Lczc;->a:Lczc;

    aput-object v1, v0, v3

    sget-object v1, Lczc;->b:Lczc;

    aput-object v1, v0, v4

    sget-object v1, Lczc;->c:Lczc;

    aput-object v1, v0, v5

    sget-object v1, Lczc;->d:Lczc;

    aput-object v1, v0, v6

    sget-object v1, Lczc;->e:Lczc;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lczc;->f:Lczc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lczc;->g:Lczc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lczc;->h:Lczc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lczc;->i:Lczc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lczc;->j:Lczc;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lczc;->k:Lczc;

    aput-object v2, v0, v1

    sput-object v0, Lczc;->l:[Lczc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lczc;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lczc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lczc;

    return-object v0
.end method

.method public static values()[Lczc;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lczc;->l:[Lczc;

    invoke-virtual {v0}, [Lczc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lczc;

    return-object v0
.end method


# virtual methods
.method public final varargs a([Lczc;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 82
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 83
    if-ne p0, v3, :cond_1

    .line 84
    const/4 v0, 0x1

    .line 87
    :cond_0
    return v0

    .line 82
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
