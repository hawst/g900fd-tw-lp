.class public final Laez;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field a:Z

.field b:J

.field private final c:Lafa;

.field private final d:Laey;

.field private final e:Landroid/graphics/drawable/StateListDrawable;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/graphics/Rect;

.field private final l:Landroid/graphics/Rect;

.field private final m:Landroid/graphics/Paint;

.field private n:Ldbu;

.field private o:Z

.field private p:J

.field private q:J

.field private r:J

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lafa;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    .line 69
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 71
    const-string v0, "listener cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lafa;

    iput-object v0, p0, Laez;->c:Lafa;

    .line 73
    invoke-virtual {p0}, Laez;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 74
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    .line 76
    const/high16 v2, 0x43660000    # 230.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Laez;->f:I

    .line 77
    const/high16 v2, 0x40a00000    # 5.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Laez;->h:I

    .line 78
    const/high16 v2, 0x41400000    # 12.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Laez;->i:I

    .line 79
    const/high16 v2, 0x41000000    # 8.0f

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Laez;->g:I

    .line 81
    new-instance v1, Laey;

    invoke-direct {v1, p1}, Laey;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Laez;->d:Laey;

    .line 82
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iput-object v1, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    .line 83
    iget-object v1, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    const v3, 0x7f02005a

    .line 85
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 83
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 86
    iget-object v1, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->ENABLED_STATE_SET:[I

    const v3, 0x7f020059

    .line 88
    invoke-static {p1, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 86
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 89
    iget-object v1, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->ENABLED_STATE_SET:[I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 91
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Laez;->j:Landroid/graphics/Rect;

    .line 92
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Laez;->m:Landroid/graphics/Paint;

    .line 93
    iget-object v1, p0, Laez;->m:Landroid/graphics/Paint;

    const/4 v2, 0x2

    const/high16 v3, 0x41600000    # 14.0f

    invoke-static {v2, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 94
    iget-object v0, p0, Laez;->m:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 95
    iget-object v0, p0, Laez;->m:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 96
    iget-object v0, p0, Laez;->m:Landroid/graphics/Paint;

    const-string v1, "00:00"

    const/4 v2, 0x5

    iget-object v3, p0, Laez;->j:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laez;->k:Landroid/graphics/Rect;

    .line 99
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Laez;->l:Landroid/graphics/Rect;

    .line 101
    sget-object v0, Ldbu;->a:Ldbu;

    invoke-virtual {p0, v0}, Laez;->a(Ldbu;)V

    .line 102
    invoke-virtual {p0, v4, v4, v4}, Laez;->a(III)V

    .line 103
    return-void
.end method

.method private a(I)J
    .locals 6

    .prologue
    .line 242
    iget-object v0, p0, Laez;->d:Laey;

    invoke-virtual {v0}, Laey;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 243
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Laez;->i:I

    add-int/2addr v1, v2

    .line 244
    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Laez;->i:I

    sub-int/2addr v0, v2

    .line 245
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 246
    iget-wide v4, p0, Laez;->q:J

    sub-int/2addr v2, v1

    int-to-long v2, v2

    mul-long/2addr v2, v4

    sub-int/2addr v0, v1

    int-to-long v0, v0

    div-long v0, v2, v0

    return-wide v0
.end method

.method private a(J)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 298
    long-to-int v0, p1

    div-int/lit16 v1, v0, 0x3e8

    .line 299
    const-string v0, "%02d:%02d"

    new-array v2, v8, [Ljava/lang/Object;

    div-int/lit8 v3, v1, 0x3c

    rem-int/lit8 v3, v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    rem-int/lit8 v3, v1, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 300
    div-int/lit16 v1, v1, 0xe10

    .line 302
    iget-wide v2, p0, Laez;->q:J

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    const-string v2, "%d:%s"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private a(II)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 176
    iget-wide v0, p0, Laez;->q:J

    const-wide/32 v2, 0x2255100

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const-string v0, "10:00:00"

    .line 178
    :goto_0
    iget-object v1, p0, Laez;->m:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Laez;->j:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v7, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 180
    iget-object v0, p0, Laez;->d:Laey;

    invoke-virtual {v0}, Laey;->getIntrinsicHeight()I

    move-result v0

    .line 181
    iget v1, p0, Laez;->f:I

    if-lt p1, v1, :cond_2

    .line 182
    iget-object v1, p0, Laez;->d:Laey;

    iget-object v2, p0, Laez;->j:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Laez;->g:I

    add-int/2addr v2, v3

    div-int/lit8 v3, p2, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Laez;->g:I

    sub-int v4, p1, v4

    iget-object v5, p0, Laez;->j:Landroid/graphics/Rect;

    .line 183
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v5, p2, 0x2

    div-int/lit8 v6, v0, 0x2

    sub-int/2addr v5, v6

    add-int/2addr v5, v0

    .line 182
    invoke-virtual {v1, v2, v3, v4, v5}, Laey;->setBounds(IIII)V

    .line 184
    iget-object v1, p0, Laez;->j:Landroid/graphics/Rect;

    iget-object v2, p0, Laez;->d:Laey;

    invoke-virtual {v2}, Laey;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    iget-object v3, p0, Laez;->j:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v1, v7, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 190
    :goto_1
    iget-object v1, p0, Laez;->d:Laey;

    iget-object v2, p0, Laez;->l:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Laey;->copyBounds(Landroid/graphics/Rect;)V

    .line 191
    iget-object v1, p0, Laez;->l:Landroid/graphics/Rect;

    sub-int v0, p2, v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v7, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 192
    return-void

    .line 176
    :cond_0
    iget-wide v0, p0, Laez;->q:J

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    const-string v0, "0:00:00"

    goto :goto_0

    :cond_1
    const-string v0, "00:00"

    goto :goto_0

    .line 186
    :cond_2
    iget-object v1, p0, Laez;->d:Laey;

    iget v2, p0, Laez;->g:I

    div-int/lit8 v3, p2, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Laez;->g:I

    sub-int v4, p1, v4

    div-int/lit8 v5, p2, 0x2

    div-int/lit8 v6, v0, 0x2

    sub-int/2addr v5, v6

    add-int/2addr v5, v0

    invoke-virtual {v1, v2, v3, v4, v5}, Laey;->setBounds(IIII)V

    goto :goto_1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Laez;->n:Ldbu;

    iget-boolean v0, v0, Ldbu;->j:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Laez;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Laez;->q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Laez;->o:Z

    .line 123
    iget-boolean v0, p0, Laez;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Laez;->o:Z

    if-nez v0, :cond_0

    .line 124
    invoke-direct {p0}, Laez;->c()V

    .line 126
    :cond_0
    invoke-virtual {p0}, Laez;->a()V

    .line 127
    return-void

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 235
    iput-boolean v2, p0, Laez;->a:Z

    .line 236
    iget-object v0, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    sget-object v1, Laez;->ENABLED_STATE_SET:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 237
    invoke-virtual {p0}, Laez;->invalidate()V

    .line 238
    invoke-virtual {p0}, Laez;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 239
    return-void
.end method


# virtual methods
.method a()V
    .locals 14

    .prologue
    .line 250
    iget-object v0, p0, Laez;->d:Laey;

    invoke-virtual {v0}, Laey;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    .line 251
    iget v4, p0, Laez;->i:I

    .line 252
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Laez;->i:I

    sub-int/2addr v0, v1

    .line 253
    sub-int v5, v0, v4

    .line 254
    iget-boolean v0, p0, Laez;->a:Z

    if-eqz v0, :cond_1

    iget-wide v0, p0, Laez;->b:J

    .line 255
    :goto_0
    iget-wide v6, p0, Laez;->q:J

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_2

    int-to-long v6, v4

    int-to-long v8, v5

    mul-long/2addr v8, v0

    iget-wide v10, p0, Laez;->q:J

    div-long/2addr v8, v10

    add-long/2addr v6, v8

    long-to-int v2, v6

    .line 258
    :goto_1
    iget-object v6, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    iget v7, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v8

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v10

    div-int/lit8 v11, v9, 0x2

    sub-int v11, v7, v11

    div-int/lit8 v12, v10, 0x2

    sub-int v12, v8, v12

    div-int/lit8 v13, v9, 0x2

    sub-int/2addr v7, v13

    add-int/2addr v7, v9

    div-int/lit8 v9, v10, 0x2

    sub-int/2addr v8, v9

    add-int/2addr v8, v10

    invoke-virtual {v6, v11, v12, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 260
    iget-object v6, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    iget-object v7, p0, Laez;->k:Landroid/graphics/Rect;

    invoke-virtual {v6, v7}, Landroid/graphics/drawable/StateListDrawable;->copyBounds(Landroid/graphics/Rect;)V

    .line 261
    iget-object v6, p0, Laez;->k:Landroid/graphics/Rect;

    iget v7, p0, Laez;->h:I

    neg-int v7, v7

    iget v8, p0, Laez;->h:I

    neg-int v8, v8

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Rect;->inset(II)V

    .line 264
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-lez v6, :cond_3

    mul-int/lit8 v6, v2, 0x64

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    div-int v3, v6, v3

    .line 265
    :goto_2
    if-lez v5, :cond_4

    sub-int/2addr v2, v4

    mul-int/lit8 v2, v2, 0x64

    div-int/2addr v2, v5

    .line 266
    :goto_3
    iget-boolean v6, p0, Laez;->o:Z

    if-eqz v6, :cond_0

    move v2, v3

    .line 268
    :cond_0
    iget-wide v6, p0, Laez;->q:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_5

    int-to-long v6, v4

    iget-wide v8, p0, Laez;->r:J

    int-to-long v10, v5

    mul-long/2addr v8, v10

    iget-wide v10, p0, Laez;->q:J

    div-long/2addr v8, v10

    add-long/2addr v6, v8

    long-to-int v3, v6

    .line 270
    :goto_4
    if-lez v5, :cond_6

    sub-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x64

    div-int/2addr v3, v5

    .line 273
    :goto_5
    iget-object v4, p0, Laez;->d:Laey;

    mul-int/lit16 v3, v3, 0x3e8

    add-int/2addr v2, v3

    invoke-virtual {v4, v2}, Laey;->setLevel(I)Z

    .line 275
    invoke-direct {p0, v0, v1}, Laez;->a(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laez;->s:Ljava/lang/String;

    .line 277
    invoke-virtual {p0}, Laez;->invalidate()V

    .line 278
    return-void

    .line 254
    :cond_1
    iget-wide v0, p0, Laez;->p:J

    goto/16 :goto_0

    .line 255
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 264
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 265
    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    .line 268
    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    .line 270
    :cond_6
    const/4 v3, 0x0

    goto :goto_5
.end method

.method public final a(III)V
    .locals 6

    .prologue
    const-wide/32 v4, 0x36ee80

    .line 139
    iget-wide v0, p0, Laez;->q:J

    .line 141
    int-to-long v2, p1

    iput-wide v2, p0, Laez;->p:J

    .line 142
    int-to-long v2, p3

    iput-wide v2, p0, Laez;->r:J

    .line 143
    int-to-long v2, p2

    iput-wide v2, p0, Laez;->q:J

    .line 145
    int-to-long v2, p2

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    iget-object v2, p0, Laez;->t:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 146
    :cond_0
    invoke-direct {p0}, Laez;->b()V

    .line 147
    int-to-long v2, p2

    invoke-direct {p0, v2, v3}, Laez;->a(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Laez;->t:Ljava/lang/String;

    .line 148
    int-to-long v2, p2

    div-long/2addr v2, v4

    div-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-eqz v0, :cond_1

    .line 149
    invoke-virtual {p0}, Laez;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Laez;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Laez;->a(II)V

    .line 152
    :cond_1
    invoke-virtual {p0}, Laez;->a()V

    .line 154
    invoke-virtual {p0}, Laez;->invalidate()V

    .line 155
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 2

    .prologue
    .line 106
    iput-object p1, p0, Laez;->n:Ldbu;

    .line 108
    invoke-direct {p0}, Laez;->b()V

    .line 109
    iget-object v1, p0, Laez;->d:Laey;

    sget-object v0, Ldbu;->c:Ldbu;

    if-ne p1, v0, :cond_0

    sget-object v0, Lafb;->b:[I

    :goto_0
    invoke-virtual {v1, v0}, Laey;->setState([I)Z

    .line 112
    invoke-virtual {p0}, Laez;->invalidate()V

    .line 113
    return-void

    .line 109
    :cond_0
    sget-object v0, Lafb;->a:[I

    goto :goto_0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 282
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 283
    iget-object v0, p0, Laez;->d:Laey;

    invoke-virtual {v0, p1}, Laey;->draw(Landroid/graphics/Canvas;)V

    .line 285
    iget-boolean v0, p0, Laez;->o:Z

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 289
    :cond_0
    invoke-virtual {p0}, Laez;->getWidth()I

    move-result v0

    iget v1, p0, Laez;->f:I

    if-lt v0, v1, :cond_1

    .line 290
    iget-object v0, p0, Laez;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 291
    iget-object v0, p0, Laez;->s:Ljava/lang/String;

    iget-object v1, p0, Laez;->j:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget-object v2, p0, Laez;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget-object v3, p0, Laez;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 292
    iget-object v0, p0, Laez;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 293
    iget-object v0, p0, Laez;->t:Ljava/lang/String;

    invoke-virtual {p0}, Laez;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Laez;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    iget-object v3, p0, Laez;->m:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 295
    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Laez;->k:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Laez;->l:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 160
    iget-object v1, p0, Laez;->j:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 162
    iget-object v1, p0, Laez;->d:Laey;

    .line 163
    invoke-virtual {v1}, Laey;->getIntrinsicWidth()I

    move-result v1

    invoke-static {v1, p1}, Laez;->getDefaultSize(II)I

    move-result v1

    .line 164
    invoke-static {v0, p2}, Laez;->getDefaultSize(II)I

    move-result v0

    .line 162
    invoke-virtual {p0, v1, v0}, Laez;->setMeasuredDimension(II)V

    .line 165
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 169
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 171
    invoke-direct {p0, p1, p2}, Laez;->a(II)V

    .line 172
    invoke-virtual {p0}, Laez;->a()V

    .line 173
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 200
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 231
    :goto_0
    return v0

    .line 202
    :pswitch_0
    iget-boolean v2, p0, Laez;->a:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Laez;->o:Z

    if-eqz v2, :cond_0

    .line 203
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Laez;->k:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Laez;->l:Landroid/graphics/Rect;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    .line 204
    iput-boolean v0, p0, Laez;->a:Z

    .line 205
    invoke-virtual {p0}, Laez;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 206
    iget-object v1, p0, Laez;->e:Landroid/graphics/drawable/StateListDrawable;

    sget-object v2, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 207
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v1}, Laez;->a(I)J

    move-result-wide v2

    iput-wide v2, p0, Laez;->b:J

    .line 208
    invoke-virtual {p0}, Laez;->a()V

    .line 209
    iget-object v1, p0, Laez;->c:Lafa;

    invoke-interface {v1}, Lafa;->a()V

    goto :goto_0

    :cond_2
    move v2, v1

    .line 203
    goto :goto_1

    .line 214
    :pswitch_1
    iget-boolean v2, p0, Laez;->a:Z

    if-eqz v2, :cond_0

    .line 215
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v1}, Laez;->a(I)J

    move-result-wide v2

    iput-wide v2, p0, Laez;->b:J

    .line 216
    invoke-virtual {p0}, Laez;->a()V

    goto :goto_0

    .line 221
    :pswitch_2
    invoke-direct {p0}, Laez;->c()V

    goto :goto_0

    .line 224
    :pswitch_3
    iget-boolean v2, p0, Laez;->a:Z

    if-eqz v2, :cond_0

    .line 225
    invoke-direct {p0}, Laez;->c()V

    .line 226
    iget-object v1, p0, Laez;->c:Lafa;

    iget-wide v2, p0, Laez;->b:J

    long-to-int v2, v2

    invoke-interface {v1, v2}, Lafa;->a(I)V

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setEnabled(Z)V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 118
    invoke-direct {p0}, Laez;->b()V

    .line 119
    return-void
.end method
