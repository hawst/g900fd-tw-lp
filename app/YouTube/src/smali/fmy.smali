.class public final Lfmy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public final a:Lhwp;

.field public final b:Ljava/lang/String;

.field public c:Lfiy;

.field public d:Z

.field private final e:Lfqh;

.field private final f:Lhwt;

.field private final g:Lhxu;

.field private h:Lhog;

.field private i:Ljava/lang/CharSequence;

.field private j:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhwp;Lfqh;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhwp;

    iput-object v0, p0, Lfmy;->a:Lhwp;

    .line 40
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqh;

    iput-object v0, p0, Lfmy;->e:Lfqh;

    .line 41
    iget-object v0, p1, Lhwp;->e:Ljava/lang/String;

    iput-object v0, p0, Lfmy;->b:Ljava/lang/String;

    .line 42
    iget-object v0, p1, Lhwp;->c:Lhwt;

    iput-object v0, p0, Lfmy;->f:Lhwt;

    .line 43
    iget-object v0, p1, Lhwp;->f:Lhxu;

    iput-object v0, p0, Lfmy;->g:Lhxu;

    .line 44
    iget-boolean v0, p1, Lhwp;->a:Z

    iput-boolean v0, p0, Lfmy;->d:Z

    .line 45
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lfmy;->i:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->g:Lhgz;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->g:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmy;->i:Ljava/lang/CharSequence;

    .line 56
    :cond_0
    iget-object v0, p0, Lfmy;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lfmy;->j:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->h:Lhgz;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->h:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmy;->j:Ljava/lang/CharSequence;

    .line 64
    :cond_0
    iget-object v0, p0, Lfmy;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 76
    iget-object v1, p0, Lfmy;->a:Lhwp;

    iget v1, v1, Lhwp;->d:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lfmy;->a:Lhwp;

    iget-object v0, v0, Lhwp;->i:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lfmy;->e:Lfqh;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfmy;->f:Lhwt;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lfmy;->g:Lhxu;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmy;->c:Lfiy;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lhfz;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lfmy;->f:Lhwt;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lfmy;->f:Lhwt;

    iget-object v0, v0, Lhwt;->a:Lhfz;

    .line 101
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lfiy;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lfmy;->c:Lfiy;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmy;->g:Lhxu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmy;->g:Lhxu;

    iget-object v0, v0, Lhxu;->a:Lhqc;

    if-eqz v0, :cond_0

    .line 108
    new-instance v0, Lfiy;

    iget-object v1, p0, Lfmy;->g:Lhxu;

    iget-object v1, v1, Lhxu;->a:Lhqc;

    invoke-direct {v0, v1}, Lfiy;-><init>(Lhqc;)V

    iput-object v0, p0, Lfmy;->c:Lfiy;

    .line 111
    :cond_0
    iget-object v0, p0, Lfmy;->c:Lfiy;

    return-object v0
.end method

.method public final j()Lhog;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lfmy;->h:Lhog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfmy;->f:Lhwt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmy;->f:Lhwt;

    iget-object v0, v0, Lhwt;->b:Lhws;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lfmy;->f:Lhwt;

    iget-object v0, v0, Lhwt;->b:Lhws;

    iget-object v0, v0, Lhws;->a:Lhog;

    iput-object v0, p0, Lfmy;->h:Lhog;

    .line 129
    :cond_0
    iget-object v0, p0, Lfmy;->h:Lhog;

    return-object v0
.end method
