.class final Lcqf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgod;


# instance fields
.field a:Lcwx;

.field b:J

.field c:Z

.field private final d:Lexd;

.field private final e:Lezj;

.field private final f:J

.field private final g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lexd;Lcwx;Lezj;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 919
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 920
    iput-object p1, p0, Lcqf;->d:Lexd;

    .line 921
    iput-object p2, p0, Lcqf;->a:Lcwx;

    .line 922
    iput-object p3, p0, Lcqf;->e:Lezj;

    .line 923
    iput-wide p4, p0, Lcqf;->f:J

    .line 924
    iput-object p6, p0, Lcqf;->g:Ljava/lang/String;

    .line 925
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 941
    packed-switch p2, :pswitch_data_0

    .line 955
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 943
    :pswitch_0
    iget-wide v0, p0, Lcqf;->b:J

    invoke-static {v0, v1}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 945
    :pswitch_1
    iget-object v0, p0, Lcqf;->d:Lexd;

    invoke-interface {v0}, Lexd;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 947
    :pswitch_2
    iget-object v0, p0, Lcqf;->g:Ljava/lang/String;

    goto :goto_0

    .line 949
    :pswitch_3
    iget-object v0, p0, Lcqf;->e:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcqf;->f:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 951
    :pswitch_4
    iget-boolean v0, p0, Lcqf;->c:Z

    if-eqz v0, :cond_0

    const-string v0, "playing"

    goto :goto_0

    :cond_0
    const-string v0, "pause"

    goto :goto_0

    .line 953
    :pswitch_5
    iget-object v0, p0, Lcqf;->a:Lcwx;

    iget v0, v0, Lcwx;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 941
    nop

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method
