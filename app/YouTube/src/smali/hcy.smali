.class public final Lhcy;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lhxf;

.field private c:Lhxf;

.field private d:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26400
    invoke-direct {p0}, Lidf;-><init>()V

    .line 26403
    const-string v0, ""

    iput-object v0, p0, Lhcy;->a:Ljava/lang/String;

    .line 26406
    iput-object v1, p0, Lhcy;->b:Lhxf;

    .line 26409
    iput-object v1, p0, Lhcy;->c:Lhxf;

    .line 26412
    iput-object v1, p0, Lhcy;->d:Lhgz;

    .line 26400
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 26467
    const/4 v0, 0x0

    .line 26468
    iget-object v1, p0, Lhcy;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 26469
    const/4 v0, 0x1

    iget-object v1, p0, Lhcy;->a:Ljava/lang/String;

    .line 26470
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26472
    :cond_0
    iget-object v1, p0, Lhcy;->b:Lhxf;

    if-eqz v1, :cond_1

    .line 26473
    const/4 v1, 0x3

    iget-object v2, p0, Lhcy;->b:Lhxf;

    .line 26474
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26476
    :cond_1
    iget-object v1, p0, Lhcy;->c:Lhxf;

    if-eqz v1, :cond_2

    .line 26477
    const/4 v1, 0x4

    iget-object v2, p0, Lhcy;->c:Lhxf;

    .line 26478
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26480
    :cond_2
    iget-object v1, p0, Lhcy;->d:Lhgz;

    if-eqz v1, :cond_3

    .line 26481
    const/4 v1, 0x5

    iget-object v2, p0, Lhcy;->d:Lhgz;

    .line 26482
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26484
    :cond_3
    iget-object v1, p0, Lhcy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26485
    iput v0, p0, Lhcy;->J:I

    .line 26486
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 26396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhcy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhcy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhcy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcy;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhcy;->b:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhcy;->b:Lhxf;

    :cond_2
    iget-object v0, p0, Lhcy;->b:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhcy;->c:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhcy;->c:Lhxf;

    :cond_3
    iget-object v0, p0, Lhcy;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhcy;->d:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcy;->d:Lhgz;

    :cond_4
    iget-object v0, p0, Lhcy;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 26449
    iget-object v0, p0, Lhcy;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26450
    const/4 v0, 0x1

    iget-object v1, p0, Lhcy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 26452
    :cond_0
    iget-object v0, p0, Lhcy;->b:Lhxf;

    if-eqz v0, :cond_1

    .line 26453
    const/4 v0, 0x3

    iget-object v1, p0, Lhcy;->b:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 26455
    :cond_1
    iget-object v0, p0, Lhcy;->c:Lhxf;

    if-eqz v0, :cond_2

    .line 26456
    const/4 v0, 0x4

    iget-object v1, p0, Lhcy;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 26458
    :cond_2
    iget-object v0, p0, Lhcy;->d:Lhgz;

    if-eqz v0, :cond_3

    .line 26459
    const/4 v0, 0x5

    iget-object v1, p0, Lhcy;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 26461
    :cond_3
    iget-object v0, p0, Lhcy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 26463
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 26426
    if-ne p1, p0, :cond_1

    .line 26433
    :cond_0
    :goto_0
    return v0

    .line 26427
    :cond_1
    instance-of v2, p1, Lhcy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 26428
    :cond_2
    check-cast p1, Lhcy;

    .line 26429
    iget-object v2, p0, Lhcy;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcy;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhcy;->b:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhcy;->b:Lhxf;

    if-nez v2, :cond_3

    .line 26430
    :goto_2
    iget-object v2, p0, Lhcy;->c:Lhxf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhcy;->c:Lhxf;

    if-nez v2, :cond_3

    .line 26431
    :goto_3
    iget-object v2, p0, Lhcy;->d:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhcy;->d:Lhgz;

    if-nez v2, :cond_3

    .line 26432
    :goto_4
    iget-object v2, p0, Lhcy;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhcy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 26433
    goto :goto_0

    .line 26429
    :cond_4
    iget-object v2, p0, Lhcy;->a:Ljava/lang/String;

    iget-object v3, p1, Lhcy;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhcy;->b:Lhxf;

    iget-object v3, p1, Lhcy;->b:Lhxf;

    .line 26430
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhcy;->c:Lhxf;

    iget-object v3, p1, Lhcy;->c:Lhxf;

    .line 26431
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhcy;->d:Lhgz;

    iget-object v3, p1, Lhcy;->d:Lhgz;

    .line 26432
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhcy;->I:Ljava/util/List;

    iget-object v3, p1, Lhcy;->I:Ljava/util/List;

    .line 26433
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 26437
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 26439
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcy;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 26440
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcy;->b:Lhxf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 26441
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcy;->c:Lhxf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 26442
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcy;->d:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 26443
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhcy;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 26444
    return v0

    .line 26439
    :cond_0
    iget-object v0, p0, Lhcy;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 26440
    :cond_1
    iget-object v0, p0, Lhcy;->b:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_1

    .line 26441
    :cond_2
    iget-object v0, p0, Lhcy;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_2

    .line 26442
    :cond_3
    iget-object v0, p0, Lhcy;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 26443
    :cond_4
    iget-object v1, p0, Lhcy;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
