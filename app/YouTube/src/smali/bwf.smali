.class public final Lbwf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/widget/EditText;

.field public final b:Landroid/widget/ImageView;

.field public final c:Landroid/widget/ProgressBar;

.field final d:Landroid/view/View;

.field public e:Ljava/lang/String;

.field private final f:Ldwv;

.field private final g:Leub;

.field private final h:Landroid/content/res/Resources;

.field private final i:Leyt;

.field private final j:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ldsn;Ldwv;Ldwq;Lbwn;Leyt;I)V
    .locals 6

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwv;

    iput-object v0, p0, Lbwf;->f:Ldwv;

    .line 78
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbwf;->i:Leyt;

    .line 82
    new-instance v0, Lbwg;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p1

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lbwg;-><init>(Lbwf;Ldsn;Landroid/app/Activity;Lbwn;Leyt;)V

    invoke-static {p1, v0}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    iput-object v0, p0, Lbwf;->g:Leub;

    .line 115
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbwf;->h:Landroid/content/res/Resources;

    .line 117
    invoke-virtual {p1, p7}, Landroid/app/Activity;->setContentView(I)V

    .line 119
    const v0, 0x7f0802c8

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbwf;->a:Landroid/widget/EditText;

    .line 120
    const v0, 0x7f0802c5

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbwf;->d:Landroid/view/View;

    .line 121
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lbwf;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 124
    :cond_0
    const v0, 0x7f0802c6

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbwf;->b:Landroid/widget/ImageView;

    .line 125
    const v0, 0x7f0802c7

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbwf;->c:Landroid/widget/ProgressBar;

    .line 127
    iget-object v0, p0, Lbwf;->a:Landroid/widget/EditText;

    new-instance v1, Lbwi;

    invoke-direct {v1, p0}, Lbwi;-><init>(Lbwf;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 134
    const v0, 0x7f0801d3

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbwf;->j:Landroid/widget/Button;

    .line 135
    iget-object v0, p0, Lbwf;->j:Landroid/widget/Button;

    sget-object v1, Lezd;->a:Lezd;

    invoke-virtual {v1, p1}, Lezd;->a(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 137
    iget-object v0, p0, Lbwf;->a:Landroid/widget/EditText;

    new-instance v1, Lbwm;

    invoke-direct {v1}, Lbwm;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 138
    iget-object v0, p0, Lbwf;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lbwf;->h:Landroid/content/res/Resources;

    const v2, 0x7f0902e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 140
    iget-object v0, p0, Lbwf;->a:Landroid/widget/EditText;

    new-instance v1, Lbwj;

    invoke-direct {v1, p0}, Lbwj;-><init>(Lbwf;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 154
    iget-object v0, p0, Lbwf;->j:Landroid/widget/Button;

    new-instance v1, Lbwk;

    invoke-direct {v1, p0}, Lbwk;-><init>(Lbwf;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    iget-object v0, p0, Lbwf;->b:Landroid/widget/ImageView;

    new-instance v1, Lbwl;

    invoke-direct {v1, p0, p1}, Lbwl;-><init>(Lbwf;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    return-void
.end method

.method static synthetic a(Lbwf;)V
    .locals 5

    .prologue
    .line 48
    iget-object v0, p0, Lbwf;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lbwf;->i:Leyt;

    const v1, 0x7f0902ff

    invoke-interface {v0, v1}, Leyt;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lbwf;->f:Ldwv;

    new-instance v2, Ldtd;

    const-string v3, "\\D"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ldtd;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lbwf;->g:Leub;

    invoke-interface {v1, v2, v0}, Ldwv;->a(Ldtd;Leub;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    if-nez p0, :cond_0

    const-string p0, ""

    .line 220
    :cond_0
    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "pairingCode"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 221
    if-eqz v0, :cond_1

    move-object p0, v0

    .line 226
    :cond_1
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 183
    iput-object p1, p0, Lbwf;->e:Ljava/lang/String;

    .line 184
    iget-object v0, p0, Lbwf;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lbwf;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 185
    return-void
.end method
