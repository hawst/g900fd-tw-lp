.class final Lcsz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcsy;


# direct methods
.method constructor <init>(Lcsy;Lcsx;)V
    .locals 0

    .prologue
    .line 333
    iput-object p1, p0, Lcsz;->a:Lcsy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 336
    iget-object v9, p0, Lcsz;->a:Lcsy;

    iget-object v0, v9, Lcsy;->d:Lcsx;

    invoke-static {}, Lb;->b()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " = ? AND account"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ? AND gaia_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, v9, Lcsy;->a:Lgit;

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, " = ?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, v9, Lcsy;->a:Lgit;

    iget-object v0, v0, Lgit;->c:Ljava/lang/String;

    aput-object v0, v4, v2

    iget-object v0, v9, Lcsy;->a:Lgit;

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    iget-object v0, v9, Lcsy;->a:Lgit;

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    :goto_0
    iget-object v0, v9, Lcsy;->d:Lcsx;

    iget-object v0, v0, Lcsx;->b:Levi;

    invoke-interface {v0}, Levi;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "identity"

    sget-object v2, Lcti;->b:[Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    :try_start_0
    sget-object v1, Lcua;->a:Lcua;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "profile_account_name_proto"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    const-string v1, "profile_account_photo_thumbnails_proto"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    const-string v2, "profile_mobile_banner_thumbnails_proto"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    iget-object v2, v9, Lcsy;->a:Lgit;

    iget-object v2, v2, Lgit;->b:Lgiv;

    invoke-virtual {v2}, Lgiv;->c()Ljava/lang/String;

    move-result-object v6

    new-instance v2, Lhgz;

    invoke-direct {v2}, Lhgz;-><init>()V

    invoke-static {v2, v0}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v0

    check-cast v0, Lhgz;

    if-eqz v1, :cond_6

    new-instance v2, Lhxf;

    invoke-direct {v2}, Lhxf;-><init>()V

    invoke-static {v2, v1}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v1

    check-cast v1, Lhxf;

    new-instance v2, Lfnc;

    invoke-direct {v2, v1}, Lfnc;-><init>(Lhxf;)V

    :goto_1
    if-eqz v4, :cond_0

    new-instance v1, Lhxf;

    invoke-direct {v1}, Lhxf;-><init>()V

    invoke-static {v1, v4}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v1

    check-cast v1, Lhxf;

    new-instance v5, Lfnc;

    invoke-direct {v5, v1}, Lfnc;-><init>(Lhxf;)V

    :cond_0
    new-instance v1, Lcua;

    invoke-direct {v1, v6, v0, v2, v5}, Lcua;-><init>(Ljava/lang/String;Lhgz;Lfnc;Lfnc;)V

    iput-object v1, v9, Lcsy;->c:Lcua;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 337
    :goto_2
    iget-object v0, p0, Lcsz;->a:Lcsy;

    iget-object v0, v0, Lcsy;->b:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 338
    return-void

    .line 336
    :cond_1
    const-string v0, " IS NULL"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-array v4, v7, [Ljava/lang/String;

    iget-object v0, v9, Lcsy;->a:Lgit;

    iget-object v0, v0, Lgit;->c:Ljava/lang/String;

    aput-object v0, v4, v2

    iget-object v0, v9, Lcsy;->a:Lgit;

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    goto/16 :goto_0

    :cond_2
    :try_start_1
    const-string v0, "profile_display_name"

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v3, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "profile_display_email"

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "profile_thumbnail_uri"

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-static {v3, v2}, Levj;->b(Landroid/database/Cursor;I)Landroid/net/Uri;

    move-result-object v2

    new-instance v4, Lcua;

    invoke-direct {v4, v0, v1, v2}, Lcua;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v4, v9, Lcsy;->c:Lcua;
    :try_end_1
    .catch Lidg; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :cond_3
    :try_start_2
    iget-object v0, v9, Lcsy;->d:Lcsx;

    invoke-static {}, Lb;->b()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "id"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " IS NULL AND account"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " = ? AND gaia_id"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v9, Lcsy;->a:Lgit;

    iget-object v0, v0, Lgit;->b:Lgiv;

    invoke-virtual {v0}, Lgiv;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, " = ?"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v9, Lcsy;->a:Lgit;

    iget-object v5, v5, Lgit;->b:Lgiv;

    invoke-virtual {v5}, Lgiv;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    const/4 v4, 0x1

    iget-object v5, v9, Lcsy;->a:Lgit;

    iget-object v5, v5, Lgit;->b:Lgiv;

    invoke-virtual {v5}, Lgiv;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4

    :goto_3
    iget-object v4, v9, Lcsy;->d:Lcsx;

    iget-object v4, v4, Lcsx;->b:Levi;

    invoke-interface {v4}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "identity"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v5, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_4
    iput-object v1, v9, Lcsy;->c:Lcua;
    :try_end_2
    .catch Lidg; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_5
    :try_start_3
    const-string v0, " IS NULL"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v9, Lcsy;->a:Lgit;

    iget-object v5, v5, Lgit;->b:Lgiv;

    invoke-virtual {v5}, Lgiv;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v4
    :try_end_3
    .catch Lidg; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "Error parsing profile data"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_6
    move-object v2, v5

    goto/16 :goto_1
.end method
