.class public Lght;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lght;->a:Landroid/content/ContentResolver;

    .line 24
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lght;->a:Landroid/content/ContentResolver;

    const-string v0, "youtube:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0, p2}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(Ljava/lang/String;J)J
    .locals 4

    .prologue
    .line 86
    iget-object v1, p0, Lght;->a:Landroid/content/ContentResolver;

    const-string v0, "youtube:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v1, v0, p2, p3}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 30
    const-string v0, "offline_http_report_enabled"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method a(Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    .line 74
    iget-object v1, p0, Lght;->a:Landroid/content/ContentResolver;

    const-string v0, "youtube:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 35
    const-string v0, "offline_http_max_queue_size"

    const/16 v1, 0x3e8

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c()J
    .locals 4

    .prologue
    .line 40
    const-string v0, "offline_http_max_age_days"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toHours(J)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lght;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()I
    .locals 2

    .prologue
    .line 45
    const-string v0, "offline_http_batch_size"

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public e()I
    .locals 2

    .prologue
    .line 50
    const-string v0, "offline_http_report_cap_hours_5_7"

    const/16 v1, 0x18

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 2

    .prologue
    .line 55
    const-string v0, "ping_request_timeout_seconds"

    const/16 v1, 0x3c

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public g()I
    .locals 2

    .prologue
    .line 60
    const-string v0, "offline_dispatch_maximum_errors"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public h()I
    .locals 2

    .prologue
    .line 65
    const-string v0, "offline_retry_backoff_factor"

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 2

    .prologue
    .line 70
    const-string v0, "use_offline_http_service_for_pings"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lght;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
