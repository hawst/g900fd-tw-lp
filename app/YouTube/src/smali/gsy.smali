.class public final Lgsy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgsx;


# instance fields
.field a:F

.field public final b:J

.field public final c:J

.field private final d:D


# direct methods
.method public constructor <init>(JJF)V
    .locals 3

    .prologue
    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    cmp-long v0, p1, p3

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 356
    iput p5, p0, Lgsy;->a:F

    .line 357
    iput-wide p1, p0, Lgsy;->b:J

    .line 358
    iput-wide p3, p0, Lgsy;->c:J

    .line 359
    sub-long v0, p3, p1

    long-to-double v0, v0

    iput-wide v0, p0, Lgsy;->d:D

    .line 360
    return-void

    .line 355
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lgsy;)V
    .locals 2

    .prologue
    .line 362
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 363
    iget v0, p1, Lgsy;->a:F

    iput v0, p0, Lgsy;->a:F

    .line 364
    iget-wide v0, p1, Lgsy;->b:J

    iput-wide v0, p0, Lgsy;->b:J

    .line 365
    iget-wide v0, p1, Lgsy;->c:J

    iput-wide v0, p0, Lgsy;->c:J

    .line 366
    iget-wide v0, p1, Lgsy;->d:D

    iput-wide v0, p0, Lgsy;->d:D

    .line 367
    return-void
.end method


# virtual methods
.method public final a(J)F
    .locals 5

    .prologue
    .line 371
    iget-wide v0, p0, Lgsy;->d:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 372
    const/4 v0, 0x0

    .line 374
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lgsy;->a:F

    invoke-virtual {p0, p1, p2}, Lgsy;->b(J)F

    move-result v1

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public final b(J)F
    .locals 5

    .prologue
    .line 397
    iget-wide v0, p0, Lgsy;->d:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 398
    const/4 v0, 0x0

    .line 400
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lgsy;->b:J

    sub-long v0, p1, v0

    long-to-double v0, v0

    iget-wide v2, p0, Lgsy;->d:D

    div-double/2addr v0, v2

    double-to-float v0, v0

    goto :goto_0
.end method

.method public final b(F)J
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 384
    iget v1, p0, Lgsy;->a:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_0

    .line 385
    iget-wide v0, p0, Lgsy;->b:J

    .line 387
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v2, p0, Lgsy;->b:J

    long-to-double v2, v2

    iget-wide v4, p0, Lgsy;->d:D

    iget v1, p0, Lgsy;->a:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_1

    :goto_1
    float-to-double v0, v0

    mul-double/2addr v0, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lgsy;->a:F

    div-float v0, p1, v0

    goto :goto_1
.end method

.method public final c(F)J
    .locals 6

    .prologue
    .line 392
    iget-wide v0, p0, Lgsy;->b:J

    long-to-double v0, v0

    iget-wide v2, p0, Lgsy;->d:D

    float-to-double v4, p1

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    return-wide v0
.end method

.method public final d(F)J
    .locals 4

    .prologue
    .line 413
    iget v0, p0, Lgsy;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 414
    const-wide/16 v0, 0x0

    .line 416
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lgsy;->d:D

    iget v2, p0, Lgsy;->a:F

    div-float v2, p1, v2

    float-to-double v2, v2

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    goto :goto_0
.end method
