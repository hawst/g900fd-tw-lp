.class public final Lawk;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lfvd;


# instance fields
.field private final a:Lcew;

.field private b:Lfvc;


# direct methods
.method public constructor <init>(Lcew;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 27
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcew;

    iput-object v0, p0, Lawk;->a:Lcew;

    .line 28
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfvc;)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lawk;->b:Lfvc;

    if-ne v0, p1, :cond_0

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    iput-object p1, p0, Lawk;->b:Lfvc;

    .line 36
    invoke-virtual {p0}, Lawk;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lawk;->b:Lfvc;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lawk;->b:Lfvc;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 69
    const-wide/16 v0, 0x1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 59
    const/4 v0, -0x1

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lawk;->a:Lcew;

    const/4 v1, 0x0

    iget-object v2, p0, Lawk;->b:Lfvc;

    invoke-virtual {v0, v1, v2}, Lcew;->a(Lfsg;Lfvc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    return v0
.end method
