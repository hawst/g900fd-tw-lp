.class public final enum Ldih;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldih;

.field public static final enum b:Ldih;

.field private static enum c:Ldih;

.field private static final synthetic e:[Ldih;


# instance fields
.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 95
    new-instance v0, Ldih;

    const-string v1, "AD_COMPLETED"

    invoke-direct {v0, v1, v4, v2}, Ldih;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldih;->a:Ldih;

    .line 96
    new-instance v0, Ldih;

    const-string v1, "PLAYBACK_ABANDONED"

    invoke-direct {v0, v1, v2, v3}, Ldih;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldih;->b:Ldih;

    .line 97
    new-instance v0, Ldih;

    const-string v1, "VIDEO_PLAYING"

    invoke-direct {v0, v1, v3, v5}, Ldih;-><init>(Ljava/lang/String;II)V

    sput-object v0, Ldih;->c:Ldih;

    .line 94
    new-array v0, v5, [Ldih;

    sget-object v1, Ldih;->a:Ldih;

    aput-object v1, v0, v4

    sget-object v1, Ldih;->b:Ldih;

    aput-object v1, v0, v2

    sget-object v1, Ldih;->c:Ldih;

    aput-object v1, v0, v3

    sput-object v0, Ldih;->e:[Ldih;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 102
    iput p3, p0, Ldih;->d:I

    .line 103
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldih;
    .locals 1

    .prologue
    .line 94
    const-class v0, Ldih;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldih;

    return-object v0
.end method

.method public static values()[Ldih;
    .locals 1

    .prologue
    .line 94
    sget-object v0, Ldih;->e:[Ldih;

    invoke-virtual {v0}, [Ldih;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldih;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Ldih;->d:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
