.class public final Lhzc;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:Z

.field public e:Lhxf;

.field private f:[Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:I

.field private o:F

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lhzc;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhzc;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhzc;->c:J

    iput-boolean v2, p0, Lhzc;->d:Z

    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lhzc;->f:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhzc;->g:Ljava/lang/String;

    iput-boolean v2, p0, Lhzc;->h:Z

    const-string v0, ""

    iput-object v0, p0, Lhzc;->i:Ljava/lang/String;

    iput-boolean v2, p0, Lhzc;->j:Z

    iput-boolean v2, p0, Lhzc;->k:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lhzc;->e:Lhxf;

    iput-boolean v2, p0, Lhzc;->l:Z

    iput-boolean v2, p0, Lhzc;->m:Z

    iput v2, p0, Lhzc;->n:I

    const/4 v0, 0x0

    iput v0, p0, Lhzc;->o:F

    iput-boolean v2, p0, Lhzc;->p:Z

    const-string v0, ""

    iput-object v0, p0, Lhzc;->q:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhzc;->r:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhzc;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const/4 v0, 0x1

    iget-object v2, p0, Lhzc;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhzc;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0xf

    iget-object v3, p0, Lhzc;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-wide v2, p0, Lhzc;->c:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    const/16 v2, 0x10

    iget-wide v4, p0, Lhzc;->c:J

    invoke-static {v2, v4, v5}, Lidd;->c(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Lhzc;->d:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x11

    iget-boolean v3, p0, Lhzc;->d:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhzc;->f:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lhzc;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    iget-object v3, p0, Lhzc;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    add-int/2addr v0, v2

    iget-object v1, p0, Lhzc;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhzc;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0x13

    iget-object v2, p0, Lhzc;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Lhzc;->h:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x14

    iget-boolean v2, p0, Lhzc;->h:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhzc;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    const/16 v1, 0x15

    iget-object v2, p0, Lhzc;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lhzc;->j:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x16

    iget-boolean v2, p0, Lhzc;->j:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget-boolean v1, p0, Lhzc;->k:Z

    if-eqz v1, :cond_9

    const/16 v1, 0x18

    iget-boolean v2, p0, Lhzc;->k:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lhzc;->e:Lhxf;

    if-eqz v1, :cond_a

    const/16 v1, 0x19

    iget-object v2, p0, Lhzc;->e:Lhxf;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-boolean v1, p0, Lhzc;->l:Z

    if-eqz v1, :cond_b

    const/16 v1, 0x1b

    iget-boolean v2, p0, Lhzc;->l:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_b
    iget-boolean v1, p0, Lhzc;->m:Z

    if-eqz v1, :cond_c

    const/16 v1, 0x1c

    iget-boolean v2, p0, Lhzc;->m:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lhzc;->n:I

    if-eqz v1, :cond_d

    const/16 v1, 0x1d

    iget v2, p0, Lhzc;->n:I

    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lhzc;->o:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_e

    const/16 v1, 0x1e

    iget v2, p0, Lhzc;->o:F

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_e
    iget-boolean v1, p0, Lhzc;->p:Z

    if-eqz v1, :cond_f

    const/16 v1, 0x1f

    iget-boolean v2, p0, Lhzc;->p:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lhzc;->q:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const/16 v1, 0x20

    iget-object v2, p0, Lhzc;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lhzc;->r:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const/16 v1, 0x21

    iget-object v2, p0, Lhzc;->r:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lhzc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhzc;->J:I

    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhzc;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhzc;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhzc;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzc;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzc;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhzc;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzc;->d:Z

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x92

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhzc;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lhzc;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhzc;->f:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lhzc;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lhzc;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lhzc;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzc;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzc;->h:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzc;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzc;->j:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzc;->k:Z

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhzc;->e:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhzc;->e:Lhxf;

    :cond_3
    iget-object v0, p0, Lhzc;->e:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzc;->l:Z

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzc;->m:Z

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhzc;->n:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhzc;->o:F

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzc;->p:Z

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzc;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzc;->r:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x7a -> :sswitch_2
        0x80 -> :sswitch_3
        0x88 -> :sswitch_4
        0x92 -> :sswitch_5
        0x9a -> :sswitch_6
        0xa0 -> :sswitch_7
        0xaa -> :sswitch_8
        0xb0 -> :sswitch_9
        0xc0 -> :sswitch_a
        0xca -> :sswitch_b
        0xd8 -> :sswitch_c
        0xe0 -> :sswitch_d
        0xe8 -> :sswitch_e
        0xf5 -> :sswitch_f
        0xf8 -> :sswitch_10
        0x102 -> :sswitch_11
        0x10a -> :sswitch_12
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhzc;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhzc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lhzc;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0xf

    iget-object v1, p0, Lhzc;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_1
    iget-wide v0, p0, Lhzc;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    const/16 v0, 0x10

    iget-wide v2, p0, Lhzc;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    :cond_2
    iget-boolean v0, p0, Lhzc;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x11

    iget-boolean v1, p0, Lhzc;->d:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_3
    iget-object v0, p0, Lhzc;->f:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lhzc;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    const/16 v4, 0x12

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lhzc;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x13

    iget-object v1, p0, Lhzc;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_5
    iget-boolean v0, p0, Lhzc;->h:Z

    if-eqz v0, :cond_6

    const/16 v0, 0x14

    iget-boolean v1, p0, Lhzc;->h:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_6
    iget-object v0, p0, Lhzc;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x15

    iget-object v1, p0, Lhzc;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_7
    iget-boolean v0, p0, Lhzc;->j:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x16

    iget-boolean v1, p0, Lhzc;->j:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_8
    iget-boolean v0, p0, Lhzc;->k:Z

    if-eqz v0, :cond_9

    const/16 v0, 0x18

    iget-boolean v1, p0, Lhzc;->k:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_9
    iget-object v0, p0, Lhzc;->e:Lhxf;

    if-eqz v0, :cond_a

    const/16 v0, 0x19

    iget-object v1, p0, Lhzc;->e:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_a
    iget-boolean v0, p0, Lhzc;->l:Z

    if-eqz v0, :cond_b

    const/16 v0, 0x1b

    iget-boolean v1, p0, Lhzc;->l:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_b
    iget-boolean v0, p0, Lhzc;->m:Z

    if-eqz v0, :cond_c

    const/16 v0, 0x1c

    iget-boolean v1, p0, Lhzc;->m:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_c
    iget v0, p0, Lhzc;->n:I

    if-eqz v0, :cond_d

    const/16 v0, 0x1d

    iget v1, p0, Lhzc;->n:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    :cond_d
    iget v0, p0, Lhzc;->o:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_e

    const/16 v0, 0x1e

    iget v1, p0, Lhzc;->o:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    :cond_e
    iget-boolean v0, p0, Lhzc;->p:Z

    if-eqz v0, :cond_f

    const/16 v0, 0x1f

    iget-boolean v1, p0, Lhzc;->p:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_f
    iget-object v0, p0, Lhzc;->q:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const/16 v0, 0x20

    iget-object v1, p0, Lhzc;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_10
    iget-object v0, p0, Lhzc;->r:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x21

    iget-object v1, p0, Lhzc;->r:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_11
    iget-object v0, p0, Lhzc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhzc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhzc;

    iget-object v2, p0, Lhzc;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhzc;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhzc;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhzc;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-wide v2, p0, Lhzc;->c:J

    iget-wide v4, p1, Lhzc;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lhzc;->d:Z

    iget-boolean v3, p1, Lhzc;->d:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzc;->f:[Ljava/lang/String;

    iget-object v3, p1, Lhzc;->f:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhzc;->g:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhzc;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    iget-boolean v2, p0, Lhzc;->h:Z

    iget-boolean v3, p1, Lhzc;->h:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzc;->i:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhzc;->i:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-boolean v2, p0, Lhzc;->j:Z

    iget-boolean v3, p1, Lhzc;->j:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhzc;->k:Z

    iget-boolean v3, p1, Lhzc;->k:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzc;->e:Lhxf;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhzc;->e:Lhxf;

    if-nez v2, :cond_3

    :goto_5
    iget-boolean v2, p0, Lhzc;->l:Z

    iget-boolean v3, p1, Lhzc;->l:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhzc;->m:Z

    iget-boolean v3, p1, Lhzc;->m:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhzc;->n:I

    iget v3, p1, Lhzc;->n:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhzc;->o:F

    iget v3, p1, Lhzc;->o:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lhzc;->p:Z

    iget-boolean v3, p1, Lhzc;->p:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzc;->q:Ljava/lang/String;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhzc;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhzc;->r:Ljava/lang/String;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhzc;->r:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhzc;->I:Ljava/util/List;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhzc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhzc;->a:Ljava/lang/String;

    iget-object v3, p1, Lhzc;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhzc;->b:Ljava/lang/String;

    iget-object v3, p1, Lhzc;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhzc;->g:Ljava/lang/String;

    iget-object v3, p1, Lhzc;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhzc;->i:Ljava/lang/String;

    iget-object v3, p1, Lhzc;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhzc;->e:Lhxf;

    iget-object v3, p1, Lhzc;->e:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhzc;->q:Ljava/lang/String;

    iget-object v3, p1, Lhzc;->q:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhzc;->r:Ljava/lang/String;

    iget-object v3, p1, Lhzc;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhzc;->I:Ljava/util/List;

    iget-object v3, p1, Lhzc;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzc;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzc;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lhzc;->c:J

    iget-wide v6, p0, Lhzc;->c:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhzc;->d:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    iget-object v4, p0, Lhzc;->f:[Ljava/lang/String;

    if-nez v4, :cond_4

    mul-int/lit8 v4, v0, 0x1f

    :cond_0
    mul-int/lit8 v4, v4, 0x1f

    iget-object v0, p0, Lhzc;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhzc;->h:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzc;->i:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhzc;->j:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_6
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhzc;->k:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhzc;->e:Lhxf;

    if-nez v0, :cond_b

    move v0, v1

    :goto_8
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhzc;->l:Z

    if-eqz v0, :cond_c

    move v0, v2

    :goto_9
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhzc;->m:Z

    if-eqz v0, :cond_d

    move v0, v2

    :goto_a
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhzc;->n:I

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhzc;->o:F

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v4

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhzc;->p:Z

    if-eqz v4, :cond_e

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzc;->q:Ljava/lang/String;

    if-nez v0, :cond_f

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzc;->r:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhzc;->I:Ljava/util/List;

    if-nez v2, :cond_11

    :goto_e
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhzc;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lhzc;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_3
    move v0, v3

    goto/16 :goto_2

    :cond_4
    move v4, v0

    move v0, v1

    :goto_f
    iget-object v5, p0, Lhzc;->f:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v4, 0x1f

    iget-object v4, p0, Lhzc;->f:[Ljava/lang/String;

    aget-object v4, v4, v0

    if-nez v4, :cond_5

    move v4, v1

    :goto_10
    add-int/2addr v4, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_5
    iget-object v4, p0, Lhzc;->f:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v4

    goto :goto_10

    :cond_6
    iget-object v0, p0, Lhzc;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_7
    move v0, v3

    goto/16 :goto_4

    :cond_8
    iget-object v0, p0, Lhzc;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_9
    move v0, v3

    goto/16 :goto_6

    :cond_a
    move v0, v3

    goto/16 :goto_7

    :cond_b
    iget-object v0, p0, Lhzc;->e:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_c
    move v0, v3

    goto/16 :goto_9

    :cond_d
    move v0, v3

    goto/16 :goto_a

    :cond_e
    move v2, v3

    goto :goto_b

    :cond_f
    iget-object v0, p0, Lhzc;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_c

    :cond_10
    iget-object v0, p0, Lhzc;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_d

    :cond_11
    iget-object v1, p0, Lhzc;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_e
.end method
