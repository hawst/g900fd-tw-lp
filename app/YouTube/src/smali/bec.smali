.class public Lbec;
.super Lbdx;
.source "SourceFile"


# instance fields
.field private X:Ljava/util/concurrent/Executor;

.field private Y:Ldmj;

.field private Z:Landroid/widget/ListView;

.field private a:Lfus;

.field private aa:Lbek;

.field private ab:Landroid/view/View;

.field private ac:Landroid/widget/EditText;

.field private ad:Landroid/view/View;

.field private ae:Ljava/lang/String;

.field private volatile af:Lbei;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lbdx;-><init>()V

    .line 262
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lbec;->af:Lbei;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lbec;->af:Lbei;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lbei;->b:Z

    .line 209
    :cond_0
    new-instance v0, Lbei;

    iget-object v1, p0, Lbec;->ae:Ljava/lang/String;

    invoke-direct {v0, p0, v1}, Lbei;-><init>(Lbec;Ljava/lang/String;)V

    iput-object v0, p0, Lbec;->af:Lbei;

    .line 210
    iget-object v0, p0, Lbec;->X:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lbec;->af:Lbei;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 211
    return-void
.end method

.method static synthetic a(Lbec;)Lbek;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbec;->aa:Lbek;

    return-object v0
.end method

.method static synthetic a(Lbec;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lbec;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lbec;Ljava/lang/String;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lb;->a()V

    iget-object v0, p0, Lbec;->aa:Lbek;

    invoke-virtual {v0}, Lbek;->clear()V

    iget-object v0, p0, Lbec;->aa:Lbek;

    invoke-virtual {v0, p2}, Lbek;->addAll(Ljava/util/Collection;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 189
    iget-object v1, p0, Lbec;->ad:Landroid/view/View;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 190
    return-void

    .line 189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lbec;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lbec;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lbec;->ae:Ljava/lang/String;

    return-object p1
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lbec;->a:Lfus;

    invoke-virtual {v0, p1}, Lfus;->b(Ljava/lang/String;)V

    .line 198
    return-void
.end method

.method static synthetic c(Lbec;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lbec;->A()V

    return-void
.end method

.method static synthetic c(Lbec;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lbec;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lbec;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbec;->ae:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lbec;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbec;->ae:Ljava/lang/String;

    invoke-direct {p0, v0}, Lbec;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lbec;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic g(Lbec;)Ldmj;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lbec;->Y:Ldmj;

    return-object v0
.end method


# virtual methods
.method public final C()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public final D()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    invoke-static {v0}, Leze;->a(Landroid/view/View;)V

    .line 186
    return-void
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 74
    const v0, 0x7f0400f9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbec;->Z:Landroid/widget/ListView;

    .line 75
    new-instance v0, Lbek;

    iget-object v1, p0, Lbec;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, p0, v1}, Lbek;-><init>(Lbec;Landroid/content/Context;)V

    iput-object v0, p0, Lbec;->aa:Lbek;

    .line 76
    iget-object v0, p0, Lbec;->Z:Landroid/widget/ListView;

    iget-object v1, p0, Lbec;->aa:Lbek;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 78
    iget-object v0, p0, Lbec;->Z:Landroid/widget/ListView;

    new-instance v1, Lbed;

    invoke-direct {v1, p0}, Lbed;-><init>(Lbec;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 85
    iget-object v0, p0, Lbec;->Z:Landroid/widget/ListView;

    new-instance v1, Lbee;

    invoke-direct {v1, p0}, Lbee;-><init>(Lbec;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 104
    const v0, 0x7f0400be

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbec;->ab:Landroid/view/View;

    .line 105
    iget-object v0, p0, Lbec;->ab:Landroid/view/View;

    const v1, 0x7f080287

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    .line 106
    iget-object v0, p0, Lbec;->ab:Landroid/view/View;

    const v1, 0x7f0800be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbec;->ad:Landroid/view/View;

    .line 108
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    iget-object v1, p0, Lbec;->ae:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    new-instance v1, Lbef;

    invoke-direct {v1, p0}, Lbef;-><init>(Lbec;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 125
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    new-instance v1, Lbeg;

    invoke-direct {v1, p0}, Lbeg;-><init>(Lbec;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 137
    iget-object v0, p0, Lbec;->ad:Landroid/view/View;

    new-instance v1, Lbeh;

    invoke-direct {v1, p0}, Lbeh;-><init>(Lbec;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lbec;->ae:Ljava/lang/String;

    invoke-direct {p0, v0}, Lbec;->a(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lbec;->Z:Landroid/widget/ListView;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Lbdx;->a(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lbec;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lbec;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, v1, Lbhz;->m:Lfus;

    iput-object v1, p0, Lbec;->a:Lfus;

    .line 65
    iget-object v1, v0, Lckz;->a:Letc;

    invoke-virtual {v1}, Letc;->h()Ljava/util/concurrent/Executor;

    move-result-object v1

    iput-object v1, p0, Lbec;->X:Ljava/util/concurrent/Executor;

    .line 66
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    invoke-virtual {v0}, Lari;->an()Ldmj;

    move-result-object v0

    iput-object v0, p0, Lbec;->Y:Ldmj;

    .line 68
    invoke-virtual {p0}, Lbec;->h()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbec;->ae:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public final b()Lcan;
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lbec;->W:Lcan;

    if-nez v0, :cond_0

    .line 166
    invoke-virtual {p0}, Lbec;->k()Landroid/content/res/Resources;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lbec;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-virtual {v1}, Lcaq;->h()Lcar;

    move-result-object v1

    iget-object v2, p0, Lbec;->ab:Landroid/view/View;

    .line 168
    iput-object v2, v1, Lcar;->b:Landroid/view/View;

    const v2, 0x7f0700d8

    .line 169
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Lcar;->c:I

    const v2, 0x7f0700d9

    .line 170
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, v1, Lcar;->d:I

    const v0, 0x7f0d013b

    .line 171
    iput v0, v1, Lcar;->e:I

    .line 172
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcar;->a(Ljava/util/Collection;)Lcar;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lcar;->a()Lcaq;

    move-result-object v0

    iput-object v0, p0, Lbec;->W:Lcan;

    .line 175
    :cond_0
    iget-object v0, p0, Lbec;->W:Lcan;

    return-object v0
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Lbdx;->t()V

    .line 151
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 152
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    invoke-static {v0}, Leze;->b(Landroid/view/View;)V

    .line 154
    invoke-direct {p0}, Lbec;->A()V

    .line 155
    return-void
.end method

.method public final u()V
    .locals 1

    .prologue
    .line 159
    invoke-super {p0}, Lbdx;->u()V

    .line 160
    iget-object v0, p0, Lbec;->ac:Landroid/widget/EditText;

    invoke-static {v0}, Leze;->a(Landroid/view/View;)V

    .line 161
    return-void
.end method
