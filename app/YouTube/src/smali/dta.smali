.class public final enum Ldta;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field private static enum A:Ldta;

.field private static enum B:Ldta;

.field private static enum C:Ldta;

.field private static final synthetic D:[Ldta;

.field public static final enum a:Ldta;

.field public static final enum b:Ldta;

.field public static final enum c:Ldta;

.field public static final enum d:Ldta;

.field public static final enum e:Ldta;

.field public static final enum f:Ldta;

.field public static final enum g:Ldta;

.field public static final enum h:Ldta;

.field private static enum i:Ldta;

.field private static enum j:Ldta;

.field private static enum k:Ldta;

.field private static enum l:Ldta;

.field private static enum m:Ldta;

.field private static enum n:Ldta;

.field private static enum o:Ldta;

.field private static enum p:Ldta;

.field private static enum q:Ldta;

.field private static enum r:Ldta;

.field private static enum s:Ldta;

.field private static enum t:Ldta;

.field private static enum u:Ldta;

.field private static enum v:Ldta;

.field private static enum w:Ldta;

.field private static enum x:Ldta;

.field private static enum y:Ldta;

.field private static enum z:Ldta;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Ldta;

    const-string v1, "AUTHORIZATION_LIGHTWEIGHT_ACCOUNT"

    invoke-direct {v0, v1, v3}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->i:Ldta;

    .line 20
    new-instance v0, Ldta;

    const-string v1, "BIG_SCREEN_CONNECTED"

    invoke-direct {v0, v1, v4}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->j:Ldta;

    .line 21
    new-instance v0, Ldta;

    const-string v1, "BIG_SCREEN_DISCONNECTED"

    invoke-direct {v0, v1, v5}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->a:Ldta;

    .line 22
    new-instance v0, Ldta;

    const-string v1, "BIG_SCREEN_ON_ERROR"

    invoke-direct {v0, v1, v6}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->k:Ldta;

    .line 23
    new-instance v0, Ldta;

    const-string v1, "BIG_SCREEN_PLAY_STATE_UPDATE"

    invoke-direct {v0, v1, v7}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->l:Ldta;

    .line 24
    new-instance v0, Ldta;

    const-string v1, "BIG_SCREEN_PLAYER_STATE_CHANGED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->m:Ldta;

    .line 25
    new-instance v0, Ldta;

    const-string v1, "BIG_SCREEN_PLAYLIST_CONFIRMED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->n:Ldta;

    .line 26
    new-instance v0, Ldta;

    const-string v1, "BIG_SCREEN_PLAYLIST_UPDATE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->o:Ldta;

    .line 27
    new-instance v0, Ldta;

    const-string v1, "CLOUD_SERVICE_IPV6_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->b:Ldta;

    .line 28
    new-instance v0, Ldta;

    const-string v1, "CLOUD_SERVICE_NO_NETWORK"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->c:Ldta;

    .line 34
    new-instance v0, Ldta;

    const-string v1, "CONNECTION_STATUS_CONNECTED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->d:Ldta;

    .line 35
    new-instance v0, Ldta;

    const-string v1, "CONNECTION_STATUS_DISCONNECTED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->e:Ldta;

    .line 36
    new-instance v0, Ldta;

    const-string v1, "CONNECTION_STATUS_STARTED_CONNECTING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->f:Ldta;

    .line 37
    new-instance v0, Ldta;

    const-string v1, "CONNECTION_STATUS_STOPPED_CONNECTING"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->g:Ldta;

    .line 38
    new-instance v0, Ldta;

    const-string v1, "DECLINED_PARTY"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->p:Ldta;

    .line 39
    new-instance v0, Ldta;

    const-string v1, "END_PARTY_MODE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->q:Ldta;

    .line 40
    new-instance v0, Ldta;

    const-string v1, "GO_HOME"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->r:Ldta;

    .line 41
    new-instance v0, Ldta;

    const-string v1, "LOUNGE_SERVER_CONNECTION_ERROR"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->h:Ldta;

    .line 42
    new-instance v0, Ldta;

    const-string v1, "LOUNGE_STATUS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->s:Ldta;

    .line 43
    new-instance v0, Ldta;

    const-string v1, "NO_ACTION"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->t:Ldta;

    .line 47
    new-instance v0, Ldta;

    const-string v1, "PARTY_VIDEO_FLING"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->u:Ldta;

    .line 48
    new-instance v0, Ldta;

    const-string v1, "PLAYSTATE_CHANGED"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->v:Ldta;

    .line 49
    new-instance v0, Ldta;

    const-string v1, "QUEUE_MODIFIED"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->w:Ldta;

    .line 50
    new-instance v0, Ldta;

    const-string v1, "QUEUE_MODIFIED_VIDEO_ADDED"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->x:Ldta;

    .line 51
    new-instance v0, Ldta;

    const-string v1, "QUEUE_MODIFIED_VIDEO_REMOVED"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->y:Ldta;

    .line 52
    new-instance v0, Ldta;

    const-string v1, "REFRESH"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->z:Ldta;

    .line 53
    new-instance v0, Ldta;

    const-string v1, "REMOTE_CALL_ERROR"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->A:Ldta;

    .line 54
    new-instance v0, Ldta;

    const-string v1, "SHARED_PLAYLIST_MODIFIED"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->B:Ldta;

    .line 55
    new-instance v0, Ldta;

    const-string v1, "SWITCH_USER"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Ldta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldta;->C:Ldta;

    .line 18
    const/16 v0, 0x1d

    new-array v0, v0, [Ldta;

    sget-object v1, Ldta;->i:Ldta;

    aput-object v1, v0, v3

    sget-object v1, Ldta;->j:Ldta;

    aput-object v1, v0, v4

    sget-object v1, Ldta;->a:Ldta;

    aput-object v1, v0, v5

    sget-object v1, Ldta;->k:Ldta;

    aput-object v1, v0, v6

    sget-object v1, Ldta;->l:Ldta;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldta;->m:Ldta;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldta;->n:Ldta;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldta;->o:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldta;->b:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ldta;->c:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ldta;->d:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Ldta;->e:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Ldta;->f:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Ldta;->g:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Ldta;->p:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Ldta;->q:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Ldta;->r:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Ldta;->h:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Ldta;->s:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Ldta;->t:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Ldta;->u:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Ldta;->v:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Ldta;->w:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Ldta;->x:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Ldta;->y:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Ldta;->z:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Ldta;->A:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Ldta;->B:Ldta;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Ldta;->C:Ldta;

    aput-object v2, v0, v1

    sput-object v0, Ldta;->D:[Ldta;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ldta;
    .locals 1

    .prologue
    .line 67
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldta;->valueOf(Ljava/lang/String;)Ldta;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldta;
    .locals 1

    .prologue
    .line 18
    const-class v0, Ldta;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldta;

    return-object v0
.end method

.method public static values()[Ldta;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Ldta;->D:[Ldta;

    invoke-virtual {v0}, [Ldta;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldta;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Ldta;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 76
    const-class v0, Ldta;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ldta;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
