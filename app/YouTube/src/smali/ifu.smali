.class public final Lifu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final m:Ljava/lang/String;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lift;

.field public c:Landroid/hardware/Camera;

.field public d:Landroid/graphics/Rect;

.field public e:Landroid/graphics/Rect;

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:I

.field public j:I

.field public final k:Lifv;

.field public final l:Lifs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lifu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lifu;->m:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lifu;->a:Landroid/content/Context;

    .line 69
    new-instance v0, Lift;

    invoke-direct {v0, p1}, Lift;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lifu;->b:Lift;

    .line 70
    new-instance v0, Lifv;

    iget-object v1, p0, Lifu;->b:Lift;

    invoke-direct {v0, v1}, Lifv;-><init>(Lift;)V

    iput-object v0, p0, Lifu;->k:Lifv;

    .line 71
    new-instance v0, Lifs;

    invoke-direct {v0}, Lifs;-><init>()V

    iput-object v0, p0, Lifu;->l:Lifs;

    .line 72
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/16 v0, 0x258

    const/16 v3, 0x190

    const/16 v1, 0xf0

    .line 180
    iget-object v2, p0, Lifu;->d:Landroid/graphics/Rect;

    if-nez v2, :cond_2

    .line 181
    iget-object v2, p0, Lifu;->c:Landroid/hardware/Camera;

    if-nez v2, :cond_0

    .line 182
    const/4 v0, 0x0

    .line 202
    :goto_0
    return-object v0

    .line 184
    :cond_0
    iget-object v2, p0, Lifu;->b:Lift;

    iget-object v4, v2, Lift;->b:Landroid/graphics/Point;

    .line 185
    iget v2, v4, Landroid/graphics/Point;->x:I

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    .line 186
    if-ge v2, v1, :cond_3

    move v0, v1

    .line 191
    :cond_1
    :goto_1
    iget v2, v4, Landroid/graphics/Point;->y:I

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    .line 192
    if-ge v2, v1, :cond_4

    .line 197
    :goto_2
    iget v2, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    .line 198
    iget v3, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    .line 199
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-direct {v4, v2, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lifu;->d:Landroid/graphics/Rect;

    .line 200
    sget-object v0, Lifu;->m:Ljava/lang/String;

    iget-object v0, p0, Lifu;->d:Landroid/graphics/Rect;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Calculated framing rect: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    :cond_2
    iget-object v0, p0, Lifu;->d:Landroid/graphics/Rect;

    goto :goto_0

    .line 188
    :cond_3
    if-gt v2, v0, :cond_1

    move v0, v2

    goto :goto_1

    .line 194
    :cond_4
    if-le v2, v3, :cond_5

    move v1, v3

    .line 195
    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public final a(II)V
    .locals 5

    .prologue
    .line 238
    iget-boolean v0, p0, Lifu;->f:Z

    if-eqz v0, :cond_2

    .line 239
    iget-object v0, p0, Lifu;->b:Lift;

    iget-object v0, v0, Lift;->b:Landroid/graphics/Point;

    .line 240
    iget v1, v0, Landroid/graphics/Point;->x:I

    if-le p1, v1, :cond_0

    .line 241
    iget p1, v0, Landroid/graphics/Point;->x:I

    .line 243
    :cond_0
    iget v1, v0, Landroid/graphics/Point;->y:I

    if-le p2, v1, :cond_1

    .line 244
    iget p2, v0, Landroid/graphics/Point;->y:I

    .line 246
    :cond_1
    iget v1, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v1, p1

    div-int/lit8 v1, v1, 0x2

    .line 247
    iget v0, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x2

    .line 248
    new-instance v2, Landroid/graphics/Rect;

    add-int v3, v1, p1

    add-int v4, v0, p2

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lifu;->d:Landroid/graphics/Rect;

    .line 249
    sget-object v0, Lifu;->m:Ljava/lang/String;

    iget-object v0, p0, Lifu;->d:Landroid/graphics/Rect;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Calculated manual framing rect: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const/4 v0, 0x0

    iput-object v0, p0, Lifu;->e:Landroid/graphics/Rect;

    .line 255
    :goto_0
    return-void

    .line 252
    :cond_2
    iput p1, p0, Lifu;->i:I

    .line 253
    iput p2, p0, Lifu;->j:I

    goto :goto_0
.end method

.method public final a(Landroid/os/Handler;I)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lifu;->c:Landroid/hardware/Camera;

    .line 153
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lifu;->g:Z

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lifu;->k:Lifv;

    invoke-virtual {v1, p1, p2}, Lifv;->a(Landroid/os/Handler;I)V

    .line 155
    iget-object v1, p0, Lifu;->k:Lifv;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setOneShotPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 157
    :cond_0
    return-void
.end method

.method public final b()Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 210
    iget-object v0, p0, Lifu;->e:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 211
    invoke-virtual {p0}, Lifu;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 212
    if-nez v0, :cond_1

    .line 213
    const/4 v0, 0x0

    .line 227
    :cond_0
    :goto_0
    return-object v0

    .line 215
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 216
    iget-object v2, p0, Lifu;->b:Lift;

    iget-object v2, v2, Lift;->c:Landroid/graphics/Point;

    .line 217
    if-eqz v2, :cond_0

    .line 220
    iget-object v0, p0, Lifu;->b:Lift;

    iget-object v0, v0, Lift;->b:Landroid/graphics/Point;

    .line 221
    iget v3, v1, Landroid/graphics/Rect;->left:I

    iget v4, v2, Landroid/graphics/Point;->x:I

    mul-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Point;->x:I

    div-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 222
    iget v3, v1, Landroid/graphics/Rect;->right:I

    iget v4, v2, Landroid/graphics/Point;->x:I

    mul-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Point;->x:I

    div-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Rect;->right:I

    .line 223
    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v2, Landroid/graphics/Point;->y:I

    mul-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Point;->y:I

    div-int/2addr v3, v4

    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 224
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v3

    iget v0, v0, Landroid/graphics/Point;->y:I

    div-int v0, v2, v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 225
    iput-object v1, p0, Lifu;->e:Landroid/graphics/Rect;

    .line 227
    :cond_2
    iget-object v0, p0, Lifu;->e:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public final b(Landroid/os/Handler;I)V
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lifu;->c:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lifu;->g:Z

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lifu;->l:Lifs;

    invoke-virtual {v0, p1, p2}, Lifs;->a(Landroid/os/Handler;I)V

    .line 168
    iget-object v0, p0, Lifu;->c:Landroid/hardware/Camera;

    iget-object v1, p0, Lifu;->l:Lifs;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 170
    :cond_0
    return-void
.end method
