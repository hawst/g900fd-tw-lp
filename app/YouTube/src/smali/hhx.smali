.class public final Lhhx;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:[Lhhy;

.field public c:Ljava/lang/String;

.field public d:[B

.field private e:Lhgz;

.field private f:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63932
    invoke-direct {p0}, Lidf;-><init>()V

    .line 63935
    iput-object v1, p0, Lhhx;->a:Lhgz;

    .line 63938
    sget-object v0, Lhhy;->a:[Lhhy;

    iput-object v0, p0, Lhhx;->b:[Lhhy;

    .line 63941
    const-string v0, ""

    iput-object v0, p0, Lhhx;->c:Ljava/lang/String;

    .line 63944
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhhx;->d:[B

    .line 63947
    iput-object v1, p0, Lhhx;->e:Lhgz;

    .line 63950
    iput-object v1, p0, Lhhx;->f:Lhgz;

    .line 63932
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 64031
    .line 64032
    iget-object v0, p0, Lhhx;->a:Lhgz;

    if-eqz v0, :cond_6

    .line 64033
    const/4 v0, 0x2

    iget-object v2, p0, Lhhx;->a:Lhgz;

    .line 64034
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 64036
    :goto_0
    iget-object v2, p0, Lhhx;->b:[Lhhy;

    if-eqz v2, :cond_1

    .line 64037
    iget-object v2, p0, Lhhx;->b:[Lhhy;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 64038
    if-eqz v4, :cond_0

    .line 64039
    const/4 v5, 0x4

    .line 64040
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 64037
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 64044
    :cond_1
    iget-object v1, p0, Lhhx;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 64045
    const/4 v1, 0x5

    iget-object v2, p0, Lhhx;->c:Ljava/lang/String;

    .line 64046
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64048
    :cond_2
    iget-object v1, p0, Lhhx;->d:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 64049
    const/4 v1, 0x7

    iget-object v2, p0, Lhhx;->d:[B

    .line 64050
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 64052
    :cond_3
    iget-object v1, p0, Lhhx;->e:Lhgz;

    if-eqz v1, :cond_4

    .line 64053
    const/16 v1, 0x8

    iget-object v2, p0, Lhhx;->e:Lhgz;

    .line 64054
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64056
    :cond_4
    iget-object v1, p0, Lhhx;->f:Lhgz;

    if-eqz v1, :cond_5

    .line 64057
    const/16 v1, 0x9

    iget-object v2, p0, Lhhx;->f:Lhgz;

    .line 64058
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64060
    :cond_5
    iget-object v1, p0, Lhhx;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64061
    iput v0, p0, Lhhx;->J:I

    .line 64062
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63928
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhhx;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhhx;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhhx;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhhx;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhhx;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhhx;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhhx;->b:[Lhhy;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhhy;

    iget-object v3, p0, Lhhx;->b:[Lhhy;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhhx;->b:[Lhhy;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhhx;->b:[Lhhy;

    :goto_2
    iget-object v2, p0, Lhhx;->b:[Lhhy;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhhx;->b:[Lhhy;

    new-instance v3, Lhhy;

    invoke-direct {v3}, Lhhy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhx;->b:[Lhhy;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhhx;->b:[Lhhy;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhhx;->b:[Lhhy;

    new-instance v3, Lhhy;

    invoke-direct {v3}, Lhhy;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhx;->b:[Lhhy;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhx;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhhx;->d:[B

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lhhx;->e:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhhx;->e:Lhgz;

    :cond_6
    iget-object v0, p0, Lhhx;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhhx;->f:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhhx;->f:Lhgz;

    :cond_7
    iget-object v0, p0, Lhhx;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x3a -> :sswitch_4
        0x42 -> :sswitch_5
        0x4a -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 64003
    iget-object v0, p0, Lhhx;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 64004
    const/4 v0, 0x2

    iget-object v1, p0, Lhhx;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64006
    :cond_0
    iget-object v0, p0, Lhhx;->b:[Lhhy;

    if-eqz v0, :cond_2

    .line 64007
    iget-object v1, p0, Lhhx;->b:[Lhhy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 64008
    if-eqz v3, :cond_1

    .line 64009
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 64007
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64013
    :cond_2
    iget-object v0, p0, Lhhx;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 64014
    const/4 v0, 0x5

    iget-object v1, p0, Lhhx;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 64016
    :cond_3
    iget-object v0, p0, Lhhx;->d:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 64017
    const/4 v0, 0x7

    iget-object v1, p0, Lhhx;->d:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 64019
    :cond_4
    iget-object v0, p0, Lhhx;->e:Lhgz;

    if-eqz v0, :cond_5

    .line 64020
    const/16 v0, 0x8

    iget-object v1, p0, Lhhx;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64022
    :cond_5
    iget-object v0, p0, Lhhx;->f:Lhgz;

    if-eqz v0, :cond_6

    .line 64023
    const/16 v0, 0x9

    iget-object v1, p0, Lhhx;->f:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64025
    :cond_6
    iget-object v0, p0, Lhhx;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 64027
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63966
    if-ne p1, p0, :cond_1

    .line 63975
    :cond_0
    :goto_0
    return v0

    .line 63967
    :cond_1
    instance-of v2, p1, Lhhx;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 63968
    :cond_2
    check-cast p1, Lhhx;

    .line 63969
    iget-object v2, p0, Lhhx;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhx;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhhx;->b:[Lhhy;

    iget-object v3, p1, Lhhx;->b:[Lhhy;

    .line 63970
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhx;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhx;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 63971
    :goto_2
    iget-object v2, p0, Lhhx;->d:[B

    iget-object v3, p1, Lhhx;->d:[B

    .line 63972
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhx;->e:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhhx;->e:Lhgz;

    if-nez v2, :cond_3

    .line 63973
    :goto_3
    iget-object v2, p0, Lhhx;->f:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhhx;->f:Lhgz;

    if-nez v2, :cond_3

    .line 63974
    :goto_4
    iget-object v2, p0, Lhhx;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhhx;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 63975
    goto :goto_0

    .line 63969
    :cond_4
    iget-object v2, p0, Lhhx;->a:Lhgz;

    iget-object v3, p1, Lhhx;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 63970
    :cond_5
    iget-object v2, p0, Lhhx;->c:Ljava/lang/String;

    iget-object v3, p1, Lhhx;->c:Ljava/lang/String;

    .line 63971
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 63972
    :cond_6
    iget-object v2, p0, Lhhx;->e:Lhgz;

    iget-object v3, p1, Lhhx;->e:Lhgz;

    .line 63973
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhhx;->f:Lhgz;

    iget-object v3, p1, Lhhx;->f:Lhgz;

    .line 63974
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhhx;->I:Ljava/util/List;

    iget-object v3, p1, Lhhx;->I:Ljava/util/List;

    .line 63975
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63979
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 63981
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhx;->a:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 63982
    iget-object v2, p0, Lhhx;->b:[Lhhy;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 63988
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhhx;->c:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 63989
    iget-object v2, p0, Lhhx;->d:[B

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    .line 63995
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhhx;->e:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 63996
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhx;->f:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 63997
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhhx;->I:Ljava/util/List;

    if-nez v2, :cond_9

    :goto_4
    add-int/2addr v0, v1

    .line 63998
    return v0

    .line 63981
    :cond_2
    iget-object v0, p0, Lhhx;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    move v2, v0

    move v0, v1

    .line 63984
    :goto_5
    iget-object v3, p0, Lhhx;->b:[Lhhy;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 63985
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhhx;->b:[Lhhy;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_6
    add-int/2addr v2, v3

    .line 63984
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 63985
    :cond_4
    iget-object v2, p0, Lhhx;->b:[Lhhy;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhhy;->hashCode()I

    move-result v2

    goto :goto_6

    .line 63988
    :cond_5
    iget-object v0, p0, Lhhx;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_6
    move v2, v0

    move v0, v1

    .line 63991
    :goto_7
    iget-object v3, p0, Lhhx;->d:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 63992
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhhx;->d:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 63991
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 63995
    :cond_7
    iget-object v0, p0, Lhhx;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 63996
    :cond_8
    iget-object v0, p0, Lhhx;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 63997
    :cond_9
    iget-object v1, p0, Lhhx;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
