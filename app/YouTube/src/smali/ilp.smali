.class final Lilp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lilo;


# direct methods
.method constructor <init>(Lilo;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lilp;->a:Lilo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v7, 0x2

    const/4 v3, 0x0

    .line 87
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    iget-object v5, p0, Lilp;->a:Lilo;

    monitor-enter v5

    .line 90
    :try_start_0
    iget-object v0, p0, Lilp;->a:Lilo;

    invoke-static {v0}, Lilo;->a(Lilo;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v1, p0, Lilp;->a:Lilo;

    invoke-static {v1}, Lilo;->a(Lilo;)Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v6

    move v1, v3

    .line 91
    :goto_0
    invoke-interface {v6}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    invoke-interface {v6}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liln;

    .line 93
    invoke-virtual {v0}, Liln;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lilp;->a:Lilo;

    invoke-static {v2}, Lilo;->b(Lilo;)J

    move-result-wide v8

    invoke-virtual {v0}, Liln;->d()J

    move-result-wide v10

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    sub-long v8, v12, v8

    cmp-long v2, v10, v8

    if-gez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_3

    .line 94
    :cond_0
    invoke-interface {v6}, Ljava/util/ListIterator;->remove()V

    .line 95
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v7, :cond_7

    .line 102
    :cond_1
    iget-object v0, p0, Lilp;->a:Lilo;

    invoke-static {v0}, Lilo;->a(Lilo;)Ljava/util/LinkedList;

    move-result-object v0

    iget-object v2, p0, Lilp;->a:Lilo;

    invoke-static {v2}, Lilo;->a(Lilo;)Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v2

    .line 103
    :goto_2
    invoke-interface {v2}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lilp;->a:Lilo;

    invoke-static {v0}, Lilo;->c(Lilo;)I

    move-result v0

    if-le v1, v0, :cond_4

    .line 104
    invoke-interface {v2}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liln;

    .line 105
    invoke-virtual {v0}, Liln;->c()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 106
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-interface {v2}, Ljava/util/ListIterator;->remove()V

    .line 108
    add-int/lit8 v0, v1, -0x1

    :goto_3
    move v1, v0

    .line 110
    goto :goto_2

    :cond_2
    move v2, v3

    .line 93
    goto :goto_1

    .line 97
    :cond_3
    invoke-virtual {v0}, Liln;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 98
    add-int/lit8 v0, v1, 0x1

    :goto_4
    move v1, v0

    .line 100
    goto :goto_0

    .line 111
    :cond_4
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liln;

    .line 113
    iget-object v0, v0, Liln;->c:Ljava/net/Socket;

    invoke-static {v0}, Limo;->a(Ljava/net/Socket;)V

    goto :goto_5

    .line 111
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 115
    :cond_5
    return-void

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_4
.end method
