.class final Lgcz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xd

    .line 29
    new-array v1, v4, [I

    fill-array-data v1, :array_0

    .line 31
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v4}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lgcz;->a:Landroid/util/SparseIntArray;

    .line 32
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 33
    sget-object v2, Lgcz;->a:Landroid/util/SparseIntArray;

    aget v3, v1, v0

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseIntArray;->append(II)V

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-void

    .line 29
    :array_0
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data
.end method

.method public static a(Leby;Lebv;)[B
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 42
    sget-object v0, Lgcz;->a:Landroid/util/SparseIntArray;

    iget v3, p1, Lebv;->g:I

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    .line 43
    if-ltz v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 45
    iget v0, p0, Leby;->c:I

    add-int/lit8 v0, v0, 0x7

    .line 47
    new-array v4, v0, [B

    .line 49
    aput-byte v5, v4, v2

    .line 52
    const/16 v5, -0xf

    aput-byte v5, v4, v1

    .line 53
    const/4 v1, 0x2

    shl-int/lit8 v3, v3, 0x2

    or-int/lit8 v3, v3, 0x40

    iget v5, p1, Lebv;->f:I

    shr-int/lit8 v5, v5, 0x2

    or-int/2addr v3, v5

    int-to-byte v3, v3

    aput-byte v3, v4, v1

    .line 61
    const/4 v1, 0x3

    iget v3, p1, Lebv;->f:I

    and-int/lit8 v3, v3, 0x3

    shl-int/lit8 v3, v3, 0x6

    shr-int/lit8 v5, v0, 0xb

    or-int/2addr v3, v5

    int-to-byte v3, v3

    aput-byte v3, v4, v1

    .line 67
    const/4 v1, 0x4

    shr-int/lit8 v3, v0, 0x3

    int-to-byte v3, v3

    aput-byte v3, v4, v1

    .line 70
    const/4 v1, 0x5

    and-int/lit8 v0, v0, 0x7

    shl-int/lit8 v0, v0, 0x5

    or-int/lit8 v0, v0, 0x1f

    int-to-byte v0, v0

    aput-byte v0, v4, v1

    .line 73
    const/4 v0, 0x6

    const/4 v1, -0x4

    aput-byte v1, v4, v0

    .line 74
    iget-object v0, p0, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x7

    iget v3, p0, Leby;->c:I

    invoke-static {v0, v2, v4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    return-object v4

    :cond_0
    move v0, v2

    .line 43
    goto :goto_0
.end method
