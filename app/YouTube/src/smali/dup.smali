.class final Ldup;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldxy;


# instance fields
.field private synthetic a:Ldtm;

.field private synthetic b:Ldus;

.field private synthetic c:Ldun;


# direct methods
.method constructor <init>(Ldun;Ldtm;Ldus;)V
    .locals 0

    .prologue
    .line 1071
    iput-object p1, p0, Ldup;->c:Ldun;

    iput-object p2, p0, Ldup;->a:Ldtm;

    iput-object p3, p0, Ldup;->b:Ldus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1099
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0, p1}, Ldun;->a(Ldun;I)Ldwh;

    move-result-object v1

    .line 1100
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v2, "Could not find cloud screen corresponding to DIAL device %s, %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Ldup;->a:Ldtm;

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1102
    iget-object v0, p0, Ldup;->c:Ldun;

    iget-object v0, v0, Ldun;->f:Ldwr;

    .line 1103
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ldwr;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    check-cast v0, Ldwf;

    .line 1104
    iget-object v0, v0, Ldwf;->a:Ldtm;

    iget-object v2, p0, Ldup;->a:Ldtm;

    invoke-virtual {v0, v2}, Ldtm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1105
    :cond_0
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    iget-object v1, p0, Ldup;->a:Ldtm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x56

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Connection to DIAL device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was canceled in the meantime. Will not move to error state."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 1112
    :goto_0
    return-void

    .line 1110
    :cond_1
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0, v5}, Ldun;->a(Ldun;Z)Z

    .line 1111
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0}, Ldun;->g(Ldun;)Lduy;

    move-result-object v0

    invoke-virtual {v0, v1}, Lduy;->a(Ldwh;)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1116
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    iget-object v1, p0, Ldup;->a:Ldtm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x30

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Successfully launched YouTube TV on DIAL device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 1117
    if-eqz p1, :cond_0

    .line 1118
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0}, Ldun;->i(Ldun;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Ldup;->a:Ldtm;

    iget-object v1, v1, Ldtm;->e:Ldtj;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1120
    :cond_0
    return-void
.end method

.method public final a(Ldst;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1074
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v1, "Found corresponding cloud screen %s for DIAL device %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    const/4 v3, 0x1

    iget-object v4, p0, Ldup;->a:Ldtm;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1078
    iget-object v0, p0, Ldup;->c:Ldun;

    iget-object v1, p0, Ldup;->a:Ldtm;

    invoke-static {v0, v1, p1}, Ldun;->a(Ldun;Ldtm;Ldst;)V

    .line 1079
    iget-object v0, p0, Ldup;->c:Ldun;

    iget-object v0, v0, Ldun;->f:Ldwr;

    .line 1080
    if-eqz v0, :cond_0

    .line 1081
    invoke-virtual {v0}, Ldwr;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, Ldwf;

    .line 1082
    iget-object v0, v0, Ldwf;->a:Ldtm;

    iget-object v1, p0, Ldup;->a:Ldtm;

    invoke-virtual {v0, v1}, Ldtm;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1083
    :cond_0
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    iget-object v1, p0, Ldup;->a:Ldtm;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x46

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Connection to DIAL device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was canceled. Will not connect to the cloud"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 1095
    :cond_1
    :goto_0
    return-void

    .line 1088
    :cond_2
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-static {v0, v5}, Ldun;->a(Ldun;Z)Z

    .line 1090
    iget-object v0, p0, Ldup;->c:Ldun;

    invoke-virtual {v0}, Ldun;->i()Ldww;

    move-result-object v0

    .line 1091
    iget-object v1, p0, Ldup;->c:Ldun;

    invoke-static {v1}, Ldun;->c(Ldun;)Ldst;

    move-result-object v1

    invoke-virtual {p1, v1}, Ldst;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Ldww;->b:Ldww;

    if-eq v0, v1, :cond_1

    sget-object v1, Ldww;->a:Ldww;

    if-eq v0, v1, :cond_1

    .line 1093
    :cond_3
    iget-object v0, p0, Ldup;->c:Ldun;

    iget-object v1, p0, Ldup;->b:Ldus;

    invoke-static {v0, p1, v1}, Ldun;->a(Ldun;Ldst;Ldus;)V

    goto :goto_0
.end method
