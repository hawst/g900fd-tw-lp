.class public final Lgzk;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Lhgz;

.field private c:[D

.field private d:Lhgz;

.field private e:Lhog;

.field private f:I

.field private g:Lhgz;

.field private h:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8203
    invoke-direct {p0}, Lidf;-><init>()V

    .line 8206
    iput-object v1, p0, Lgzk;->a:Lhgz;

    .line 8209
    iput-object v1, p0, Lgzk;->b:Lhgz;

    .line 8212
    sget-object v0, Lidj;->c:[D

    iput-object v0, p0, Lgzk;->c:[D

    .line 8215
    iput-object v1, p0, Lgzk;->d:Lhgz;

    .line 8218
    iput-object v1, p0, Lgzk;->e:Lhog;

    .line 8221
    const/4 v0, 0x0

    iput v0, p0, Lgzk;->f:I

    .line 8224
    iput-object v1, p0, Lgzk;->g:Lhgz;

    .line 8227
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgzk;->h:D

    .line 8203
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    .line 8313
    const/4 v0, 0x0

    .line 8314
    iget-object v1, p0, Lgzk;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 8315
    const/4 v0, 0x1

    iget-object v1, p0, Lgzk;->a:Lhgz;

    .line 8316
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8318
    :cond_0
    iget-object v1, p0, Lgzk;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 8319
    const/4 v1, 0x2

    iget-object v2, p0, Lgzk;->b:Lhgz;

    .line 8320
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8322
    :cond_1
    iget-object v1, p0, Lgzk;->c:[D

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgzk;->c:[D

    array-length v1, v1

    if-lez v1, :cond_2

    .line 8323
    iget-object v1, p0, Lgzk;->c:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 8324
    add-int/2addr v0, v1

    .line 8325
    iget-object v1, p0, Lgzk;->c:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8327
    :cond_2
    iget-object v1, p0, Lgzk;->d:Lhgz;

    if-eqz v1, :cond_3

    .line 8328
    const/4 v1, 0x4

    iget-object v2, p0, Lgzk;->d:Lhgz;

    .line 8329
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8331
    :cond_3
    iget-object v1, p0, Lgzk;->e:Lhog;

    if-eqz v1, :cond_4

    .line 8332
    const/4 v1, 0x5

    iget-object v2, p0, Lgzk;->e:Lhog;

    .line 8333
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8335
    :cond_4
    iget v1, p0, Lgzk;->f:I

    if-eqz v1, :cond_5

    .line 8336
    const/4 v1, 0x6

    iget v2, p0, Lgzk;->f:I

    .line 8337
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8339
    :cond_5
    iget-object v1, p0, Lgzk;->g:Lhgz;

    if-eqz v1, :cond_6

    .line 8340
    const/4 v1, 0x7

    iget-object v2, p0, Lgzk;->g:Lhgz;

    .line 8341
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8343
    :cond_6
    iget-wide v2, p0, Lgzk;->h:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_7

    .line 8344
    const/16 v1, 0x8

    iget-wide v2, p0, Lgzk;->h:D

    .line 8345
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 8347
    :cond_7
    iget-object v1, p0, Lgzk;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8348
    iput v0, p0, Lgzk;->J:I

    .line 8349
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 8199
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzk;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzk;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzk;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzk;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzk;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgzk;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzk;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzk;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lgzk;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x19

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lgzk;->c:[D

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [D

    iget-object v2, p0, Lgzk;->c:[D

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lgzk;->c:[D

    :goto_1
    iget-object v1, p0, Lgzk;->c:[D

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lgzk;->c:[D

    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    aput-wide v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lgzk;->c:[D

    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    aput-wide v2, v1, v0

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lgzk;->d:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzk;->d:Lhgz;

    :cond_5
    iget-object v0, p0, Lgzk;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lgzk;->e:Lhog;

    if-nez v0, :cond_6

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgzk;->e:Lhog;

    :cond_6
    iget-object v0, p0, Lgzk;->e:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lgzk;->f:I

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lgzk;->g:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzk;->g:Lhgz;

    :cond_7
    iget-object v0, p0, Lgzk;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lgzk;->h:D

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x41 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    .line 8281
    iget-object v0, p0, Lgzk;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 8282
    const/4 v0, 0x1

    iget-object v1, p0, Lgzk;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8284
    :cond_0
    iget-object v0, p0, Lgzk;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 8285
    const/4 v0, 0x2

    iget-object v1, p0, Lgzk;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8287
    :cond_1
    iget-object v0, p0, Lgzk;->c:[D

    if-eqz v0, :cond_2

    .line 8288
    iget-object v1, p0, Lgzk;->c:[D

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-wide v4, v1, v0

    .line 8289
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v4, v5}, Lidd;->a(ID)V

    .line 8288
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8292
    :cond_2
    iget-object v0, p0, Lgzk;->d:Lhgz;

    if-eqz v0, :cond_3

    .line 8293
    const/4 v0, 0x4

    iget-object v1, p0, Lgzk;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8295
    :cond_3
    iget-object v0, p0, Lgzk;->e:Lhog;

    if-eqz v0, :cond_4

    .line 8296
    const/4 v0, 0x5

    iget-object v1, p0, Lgzk;->e:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8298
    :cond_4
    iget v0, p0, Lgzk;->f:I

    if-eqz v0, :cond_5

    .line 8299
    const/4 v0, 0x6

    iget v1, p0, Lgzk;->f:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 8301
    :cond_5
    iget-object v0, p0, Lgzk;->g:Lhgz;

    if-eqz v0, :cond_6

    .line 8302
    const/4 v0, 0x7

    iget-object v1, p0, Lgzk;->g:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8304
    :cond_6
    iget-wide v0, p0, Lgzk;->h:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_7

    .line 8305
    const/16 v0, 0x8

    iget-wide v2, p0, Lgzk;->h:D

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(ID)V

    .line 8307
    :cond_7
    iget-object v0, p0, Lgzk;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 8309
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8245
    if-ne p1, p0, :cond_1

    .line 8256
    :cond_0
    :goto_0
    return v0

    .line 8246
    :cond_1
    instance-of v2, p1, Lgzk;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 8247
    :cond_2
    check-cast p1, Lgzk;

    .line 8248
    iget-object v2, p0, Lgzk;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzk;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzk;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzk;->b:Lhgz;

    if-nez v2, :cond_3

    .line 8249
    :goto_2
    iget-object v2, p0, Lgzk;->c:[D

    iget-object v3, p1, Lgzk;->c:[D

    .line 8250
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([D[D)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzk;->d:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzk;->d:Lhgz;

    if-nez v2, :cond_3

    .line 8251
    :goto_3
    iget-object v2, p0, Lgzk;->e:Lhog;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzk;->e:Lhog;

    if-nez v2, :cond_3

    .line 8252
    :goto_4
    iget v2, p0, Lgzk;->f:I

    iget v3, p1, Lgzk;->f:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzk;->g:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgzk;->g:Lhgz;

    if-nez v2, :cond_3

    .line 8254
    :goto_5
    iget-wide v2, p0, Lgzk;->h:D

    iget-wide v4, p1, Lgzk;->h:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lgzk;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgzk;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 8256
    goto :goto_0

    .line 8248
    :cond_4
    iget-object v2, p0, Lgzk;->a:Lhgz;

    iget-object v3, p1, Lgzk;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzk;->b:Lhgz;

    iget-object v3, p1, Lgzk;->b:Lhgz;

    .line 8249
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 8250
    :cond_6
    iget-object v2, p0, Lgzk;->d:Lhgz;

    iget-object v3, p1, Lgzk;->d:Lhgz;

    .line 8251
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzk;->e:Lhog;

    iget-object v3, p1, Lgzk;->e:Lhog;

    .line 8252
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lgzk;->g:Lhgz;

    iget-object v3, p1, Lgzk;->g:Lhgz;

    .line 8254
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgzk;->I:Ljava/util/List;

    iget-object v3, p1, Lgzk;->I:Ljava/util/List;

    .line 8256
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v1, 0x0

    .line 8260
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 8262
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzk;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8263
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzk;->b:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8264
    iget-object v2, p0, Lgzk;->c:[D

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 8270
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgzk;->d:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 8271
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzk;->e:Lhog;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 8272
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgzk;->f:I

    add-int/2addr v0, v2

    .line 8273
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzk;->g:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 8274
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lgzk;->h:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p0, Lgzk;->h:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    ushr-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 8275
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzk;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_5
    add-int/2addr v0, v1

    .line 8276
    return v0

    .line 8262
    :cond_1
    iget-object v0, p0, Lgzk;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8263
    :cond_2
    iget-object v0, p0, Lgzk;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    .line 8266
    :goto_6
    iget-object v3, p0, Lgzk;->c:[D

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 8267
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lgzk;->c:[D

    aget-wide v4, v3, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-object v3, p0, Lgzk;->c:[D

    aget-wide v6, v3, v0

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v2, v3

    .line 8266
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 8270
    :cond_4
    iget-object v0, p0, Lgzk;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 8271
    :cond_5
    iget-object v0, p0, Lgzk;->e:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_3

    .line 8273
    :cond_6
    iget-object v0, p0, Lgzk;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    .line 8275
    :cond_7
    iget-object v1, p0, Lgzk;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
