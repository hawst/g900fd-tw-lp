.class public final Lfun;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final a:Landroid/widget/ListPopupWindow;

.field public b:Ljava/lang/Object;

.field private final c:Lfsi;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfhz;Lfut;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lfun;->c:Lfsi;

    .line 58
    const-class v0, Lfkq;

    iget-object v1, p0, Lfun;->c:Lfsi;

    invoke-interface {p3, v0, v1}, Lfut;->a(Ljava/lang/Class;Lfsi;)V

    .line 60
    new-instance v0, Landroid/widget/ListPopupWindow;

    invoke-direct {v0, p1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    .line 61
    iget-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 62
    iget-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/ListPopupWindow;->setPromptPosition(I)V

    .line 63
    iget-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, v3}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 64
    iget-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lfun;->c:Lfsi;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 65
    return-void
.end method

.method private static a(Lfkp;)Z
    .locals 1

    .prologue
    .line 107
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lfkp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Landroid/view/View;Lfkp;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const v1, 0x7f08001a

    .line 128
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    invoke-static {p3}, Lfun;->a(Lfkp;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f08001b

    invoke-virtual {p2, v0, p3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v0, 0x7f08001c

    invoke-virtual {p2, v0, p4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 130
    invoke-virtual {p2, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lfuo;

    invoke-direct {v0, p1, p2}, Lfuo;-><init>(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p2, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 135
    :cond_0
    return-void

    .line 129
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 75
    const v0, 0x7f08001b

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkp;

    .line 76
    invoke-static {v0}, Lfun;->a(Lfkp;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    const v1, 0x7f08001c

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lfun;->c:Lfsi;

    invoke-virtual {v2}, Lfsi;->b()V

    iget-object v2, p0, Lfun;->c:Lfsi;

    invoke-virtual {v0}, Lfkp;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lfsi;->a(Ljava/util/Collection;)V

    iput-object v1, p0, Lfun;->b:Ljava/lang/Object;

    iget-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v2}, Landroid/widget/ListPopupWindow;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    iget-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lfun;->a:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 79
    :cond_0
    return-void
.end method
