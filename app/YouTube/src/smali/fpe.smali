.class public final enum Lfpe;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfpe;

.field public static final enum b:Lfpe;

.field public static final enum c:Lfpe;

.field public static final enum d:Lfpe;

.field public static final enum e:Lfpe;

.field private static final synthetic g:[Lfpe;


# instance fields
.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 411
    new-instance v0, Lfpe;

    const-string v1, "REASON_CLIENT_OFFLINE_INSTREAM_FREQUENCY_CAP"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v3, v2}, Lfpe;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfpe;->a:Lfpe;

    .line 413
    new-instance v0, Lfpe;

    const-string v1, "REASON_CLIENT_OFFLINE_AD_ASSET_FREQUENCY_CAP"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v4, v2}, Lfpe;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfpe;->b:Lfpe;

    .line 415
    new-instance v0, Lfpe;

    const-string v1, "REASON_CLIENT_OFFLINE_AD_ASSET_EXPIRED"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v5, v2}, Lfpe;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfpe;->c:Lfpe;

    .line 418
    new-instance v0, Lfpe;

    const-string v1, "REASON_CLIENT_OFFLINE_INACTIVE_USER"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v6, v2}, Lfpe;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfpe;->d:Lfpe;

    .line 420
    new-instance v0, Lfpe;

    const-string v1, "REASON_CLIENT_OFFLINE_AD_ASSET_NOT_READY"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v7, v2}, Lfpe;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfpe;->e:Lfpe;

    .line 409
    const/4 v0, 0x5

    new-array v0, v0, [Lfpe;

    sget-object v1, Lfpe;->a:Lfpe;

    aput-object v1, v0, v3

    sget-object v1, Lfpe;->b:Lfpe;

    aput-object v1, v0, v4

    sget-object v1, Lfpe;->c:Lfpe;

    aput-object v1, v0, v5

    sget-object v1, Lfpe;->d:Lfpe;

    aput-object v1, v0, v6

    sget-object v1, Lfpe;->e:Lfpe;

    aput-object v1, v0, v7

    sput-object v0, Lfpe;->g:[Lfpe;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 425
    iput p3, p0, Lfpe;->f:I

    .line 426
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfpe;
    .locals 1

    .prologue
    .line 409
    const-class v0, Lfpe;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfpe;

    return-object v0
.end method

.method public static values()[Lfpe;
    .locals 1

    .prologue
    .line 409
    sget-object v0, Lfpe;->g:[Lfpe;

    invoke-virtual {v0}, [Lfpe;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfpe;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 430
    iget v0, p0, Lfpe;->f:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
