.class public Lgit;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lgit;


# instance fields
.field public final b:Lgiv;

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lgiu;

    .line 30
    invoke-static {}, Lgiv;->e()Lgiv;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lgiu;-><init>(Lgiv;Ljava/lang/String;)V

    sput-object v0, Lgit;->a:Lgit;

    .line 29
    return-void
.end method

.method constructor <init>(Lgiv;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgiv;

    iput-object v0, p0, Lgit;->b:Lgiv;

    .line 56
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgit;->c:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 51
    invoke-static {p1, p2}, Lgiv;->a(Ljava/lang/String;Ljava/lang/String;)Lgiv;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lgit;-><init>(Lgiv;Ljava/lang/String;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lgit;->a:Lgit;

    if-eq p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 109
    if-ne p0, p1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    instance-of v2, p1, Lgit;

    if-nez v2, :cond_2

    move v0, v1

    .line 113
    goto :goto_0

    .line 115
    :cond_2
    check-cast p1, Lgit;

    .line 116
    iget-object v2, p0, Lgit;->c:Ljava/lang/String;

    iget-object v3, p1, Lgit;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgit;->b:Lgiv;

    iget-object v3, p1, Lgit;->b:Lgiv;

    .line 117
    invoke-virtual {v2, v3}, Lgiv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lgit;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    sget-object v0, Lgit;->a:Lgit;

    if-ne p0, v0, :cond_0

    .line 123
    const-string v0, "Identity{NO_IDENTITY}"

    .line 126
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Identity{managingAccountName=\'%s\' onBehalfOfParameter=\'%s\' id=\'%s\'}"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lgit;->b:Lgiv;

    .line 128
    invoke-virtual {v3}, Lgiv;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lgit;->b:Lgiv;

    .line 129
    invoke-virtual {v3}, Lgiv;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lgit;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 126
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
