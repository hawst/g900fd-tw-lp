.class public final Ldln;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;


# instance fields
.field public final a:Lflr;

.field public final b:Landroid/net/Uri;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:I

.field public f:Z

.field private final g:Lgjp;

.field private final h:Levn;


# direct methods
.method public constructor <init>(Lgjp;Levn;Ldlo;)V
    .locals 8

    .prologue
    .line 195
    iget-object v3, p3, Ldlo;->a:Lflr;

    iget-object v4, p3, Ldlo;->b:Landroid/net/Uri;

    iget v5, p3, Ldlo;->c:I

    iget-object v6, p3, Ldlo;->d:Ljava/lang/String;

    iget v7, p3, Ldlo;->e:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Ldln;-><init>(Lgjp;Levn;Lflr;Landroid/net/Uri;ILjava/lang/String;I)V

    .line 203
    iget-boolean v0, p3, Ldlo;->f:Z

    iput-boolean v0, p0, Ldln;->f:Z

    .line 204
    return-void
.end method

.method constructor <init>(Lgjp;Levn;Lflr;Landroid/net/Uri;ILjava/lang/String;I)V
    .locals 1

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Ldln;->g:Lgjp;

    .line 215
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldln;->h:Levn;

    .line 216
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Ldln;->b:Landroid/net/Uri;

    .line 217
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflr;

    iput-object v0, p0, Ldln;->a:Lflr;

    .line 218
    iput p5, p0, Ldln;->c:I

    .line 219
    iput-object p6, p0, Ldln;->d:Ljava/lang/String;

    .line 220
    iput p7, p0, Ldln;->e:I

    .line 221
    return-void
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 256
    iget-boolean v0, p0, Ldln;->f:Z

    if-eqz v0, :cond_0

    .line 270
    :goto_0
    return-void

    .line 260
    :cond_0
    iput-boolean v1, p0, Ldln;->f:Z

    .line 261
    iget-object v0, p0, Ldln;->b:Landroid/net/Uri;

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    const-string v2, "cpn"

    iget-object v3, p0, Ldln;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 262
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Ldln;->a:Lflr;

    invoke-virtual {v0}, Lflr;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v4

    iget-object v0, p0, Ldln;->a:Lflr;

    const-string v5, "c3a"

    invoke-virtual {v0}, Lflr;->a()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lflr;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    const-string v5, "r3a"

    iget-object v0, p0, Ldln;->a:Lflr;

    const-string v6, "c3a"

    invoke-virtual {v0}, Lflr;->a()Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v0}, Lflr;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iget v6, p0, Ldln;->e:I

    rem-int v0, v6, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_1
    const-string v0, "atr"

    iget-object v4, v4, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x8

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Pinging "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Ldln;->g:Lgjp;

    const-string v0, "atr"

    new-instance v4, Lgjt;

    const v5, 0x323467f

    invoke-direct {v4, v1, v0, v5}, Lgjt;-><init>(ILjava/lang/String;I)V

    .line 266
    invoke-virtual {v4, v2}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    .line 267
    iput-object v3, v4, Lgjt;->g:Ljava/util/Map;

    .line 268
    iput-boolean v1, v4, Lgjt;->d:Z

    .line 269
    iget-object v0, p0, Ldln;->g:Lgjp;

    sget-object v1, Lggu;->b:Lwu;

    invoke-virtual {v0, v4, v1}, Lgjp;->a(Lgjt;Lwu;)V

    goto/16 :goto_0

    .line 262
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Ldln;->h:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 296
    return-void
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 225
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_0

    .line 226
    iget-wide v0, p1, Ldad;->a:J

    .line 227
    iget v2, p0, Ldln;->c:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 228
    invoke-virtual {p0}, Ldln;->a()V

    .line 231
    :cond_0
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Ldln;->h:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 301
    return-void
.end method
