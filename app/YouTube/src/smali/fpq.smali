.class public Lfpq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lghz;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final i:Lfpt;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Landroid/net/Uri;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:F

.field final f:I

.field public final g:Landroid/net/Uri;

.field public final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 456
    new-instance v0, Lfpt;

    invoke-direct {v0}, Lfpt;-><init>()V

    sput-object v0, Lfpq;->i:Lfpt;

    .line 561
    new-instance v0, Lfpr;

    invoke-direct {v0}, Lfpr;-><init>()V

    sput-object v0, Lfpq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;ZFLandroid/net/Uri;I)V
    .locals 0

    .prologue
    .line 420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421
    iput-object p1, p0, Lfpq;->a:Ljava/lang/String;

    .line 422
    iput-object p2, p0, Lfpq;->h:Ljava/lang/String;

    .line 423
    iput-object p3, p0, Lfpq;->b:Landroid/net/Uri;

    .line 424
    iput-object p4, p0, Lfpq;->c:Ljava/lang/String;

    .line 425
    iput-boolean p5, p0, Lfpq;->d:Z

    .line 426
    iput p6, p0, Lfpq;->e:F

    .line 427
    iput-object p7, p0, Lfpq;->g:Landroid/net/Uri;

    .line 428
    iput p8, p0, Lfpq;->f:I

    .line 429
    return-void
.end method


# virtual methods
.method public final synthetic a()Lgia;
    .locals 1

    .prologue
    .line 402
    new-instance v0, Lfpt;

    invoke-direct {v0, p0}, Lfpt;-><init>(Lfpq;)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 514
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 434
    if-nez p1, :cond_1

    .line 447
    :cond_0
    :goto_0
    return v0

    .line 437
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 440
    check-cast p1, Lfpq;

    .line 441
    iget-object v1, p0, Lfpq;->a:Ljava/lang/String;

    iget-object v2, p1, Lfpq;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 442
    iget-object v1, p0, Lfpq;->h:Ljava/lang/String;

    iget-object v2, p1, Lfpq;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    iget-object v1, p0, Lfpq;->b:Landroid/net/Uri;

    iget-object v2, p1, Lfpq;->b:Landroid/net/Uri;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 444
    iget-object v1, p0, Lfpq;->c:Ljava/lang/String;

    iget-object v2, p1, Lfpq;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 445
    iget v1, p0, Lfpq;->e:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iget v2, p1, Lfpq;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 446
    iget-object v1, p0, Lfpq;->g:Landroid/net/Uri;

    iget-object v2, p1, Lfpq;->g:Landroid/net/Uri;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 447
    iget v1, p0, Lfpq;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p1, Lfpq;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 519
    iget-object v0, p0, Lfpq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 520
    iget-object v0, p0, Lfpq;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 521
    iget-object v0, p0, Lfpq;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 522
    iget-object v0, p0, Lfpq;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 523
    iget-boolean v0, p0, Lfpq;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 524
    iget v0, p0, Lfpq;->e:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 525
    iget-object v0, p0, Lfpq;->g:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 526
    iget v0, p0, Lfpq;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 527
    return-void

    :cond_0
    move v0, v1

    .line 523
    goto :goto_0
.end method
