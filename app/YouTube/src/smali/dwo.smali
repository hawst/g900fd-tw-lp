.class public final enum Ldwo;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldwo;

.field public static final enum b:Ldwo;

.field public static final enum c:Ldwo;

.field public static final enum d:Ldwo;

.field public static final enum e:Ldwo;

.field public static final enum f:Ldwo;

.field public static final enum g:Ldwo;

.field public static final enum h:Ldwo;

.field public static final enum i:Ldwo;

.field public static final enum j:Ldwo;

.field public static final enum k:Ldwo;

.field private static final synthetic n:[Ldwo;


# instance fields
.field final l:Z

.field private final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 9
    new-instance v0, Ldwo;

    const-string v1, "UNSTARTED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2, v4}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->a:Ldwo;

    .line 10
    new-instance v0, Ldwo;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v5, v4, v4}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->b:Ldwo;

    .line 11
    new-instance v0, Ldwo;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v6, v5, v5}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->c:Ldwo;

    .line 12
    new-instance v0, Ldwo;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v7, v6, v5}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->d:Ldwo;

    .line 13
    new-instance v0, Ldwo;

    const-string v1, "BUFFERING"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v7, v5}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->e:Ldwo;

    .line 14
    new-instance v0, Ldwo;

    const-string v1, "VIDEO_CUED"

    invoke-direct {v0, v1, v8, v8, v5}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->f:Ldwo;

    .line 15
    new-instance v0, Ldwo;

    const-string v1, "ADVERTISEMENT"

    const/4 v2, 0x6

    const/16 v3, 0x439

    invoke-direct {v0, v1, v2, v3, v5}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->g:Ldwo;

    .line 17
    new-instance v0, Ldwo;

    const-string v1, "SKIPPED"

    const/4 v2, 0x7

    const/16 v3, 0x43a

    invoke-direct {v0, v1, v2, v3, v4}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->h:Ldwo;

    .line 22
    new-instance v0, Ldwo;

    const-string v1, "UNCONFIRMED"

    const/16 v2, 0x8

    const/16 v3, -0x1f42

    invoke-direct {v0, v1, v2, v3, v4}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->i:Ldwo;

    .line 23
    new-instance v0, Ldwo;

    const-string v1, "ERROR"

    const/16 v2, 0x9

    const/16 v3, -0x1f43

    invoke-direct {v0, v1, v2, v3, v4}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->j:Ldwo;

    .line 27
    new-instance v0, Ldwo;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xa

    const/16 v3, -0x1f41

    invoke-direct {v0, v1, v2, v3, v4}, Ldwo;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Ldwo;->k:Ldwo;

    .line 8
    const/16 v0, 0xb

    new-array v0, v0, [Ldwo;

    sget-object v1, Ldwo;->a:Ldwo;

    aput-object v1, v0, v4

    sget-object v1, Ldwo;->b:Ldwo;

    aput-object v1, v0, v5

    sget-object v1, Ldwo;->c:Ldwo;

    aput-object v1, v0, v6

    sget-object v1, Ldwo;->d:Ldwo;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Ldwo;->e:Ldwo;

    aput-object v2, v0, v1

    sget-object v1, Ldwo;->f:Ldwo;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Ldwo;->g:Ldwo;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Ldwo;->h:Ldwo;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Ldwo;->i:Ldwo;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Ldwo;->j:Ldwo;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Ldwo;->k:Ldwo;

    aput-object v2, v0, v1

    sput-object v0, Ldwo;->n:[Ldwo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Ldwo;->m:I

    .line 43
    iput-boolean p4, p0, Ldwo;->l:Z

    .line 44
    return-void
.end method

.method public static a(I)Ldwo;
    .locals 5

    .prologue
    .line 30
    invoke-static {}, Ldwo;->values()[Ldwo;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 31
    iget v4, v0, Ldwo;->m:I

    if-ne v4, p0, :cond_0

    .line 35
    :goto_1
    return-object v0

    .line 30
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 35
    :cond_1
    sget-object v0, Ldwo;->k:Ldwo;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Ldwo;
    .locals 1

    .prologue
    .line 8
    const-class v0, Ldwo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldwo;

    return-object v0
.end method

.method public static values()[Ldwo;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Ldwo;->n:[Ldwo;

    invoke-virtual {v0}, [Ldwo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldwo;

    return-object v0
.end method
