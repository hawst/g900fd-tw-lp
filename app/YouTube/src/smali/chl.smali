.class public final Lchl;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcij;

.field private final c:Lcij;

.field private final d:Lfsj;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfsj;Lffs;Lgix;Lcub;Leyp;Levn;Leyt;Lfhz;Lfdw;Lfrz;)V
    .locals 17

    .prologue
    .line 48
    move-object/from16 v0, p0

    move-object/from16 v1, p9

    move-object/from16 v2, p2

    move-object/from16 v3, p10

    move-object/from16 v4, p11

    invoke-direct {v0, v1, v2, v3, v4}, Lfsb;-><init>(Lfhz;Lfsj;Lfdw;Lfrz;)V

    .line 50
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lchl;->a:Landroid/app/Activity;

    .line 52
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lchl;->d:Lfsj;

    .line 53
    new-instance v5, Lchn;

    const v7, 0x7f0400dc

    move-object/from16 v6, p1

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    move-object/from16 v16, p11

    invoke-direct/range {v5 .. v16}, Lchn;-><init>(Landroid/app/Activity;ILffs;Lgix;Lcub;Leyp;Levn;Leyt;Lfhz;Lfdw;Lfrz;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lchl;->b:Lcij;

    .line 65
    new-instance v5, Lchn;

    const v7, 0x7f0400db

    move-object/from16 v6, p1

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move-object/from16 v12, p7

    move-object/from16 v13, p8

    move-object/from16 v14, p9

    move-object/from16 v15, p10

    move-object/from16 v16, p11

    invoke-direct/range {v5 .. v16}, Lchn;-><init>(Landroid/app/Activity;ILffs;Lgix;Lcub;Leyp;Levn;Leyt;Lfhz;Lfdw;Lfrz;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lchl;->c:Lcij;

    .line 77
    return-void
.end method

.method private a(Lfsg;Lfnl;)Landroid/view/View;
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 82
    iget-object v0, p0, Lchl;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 83
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lchl;->c:Lcij;

    .line 87
    :goto_0
    invoke-virtual {v0, p2}, Lcij;->a(Lfnl;)V

    .line 88
    iget-object v1, p0, Lchl;->d:Lfsj;

    iget-object v0, v0, Lcij;->a:Landroid/view/View;

    invoke-interface {v1, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 89
    iget-object v0, p0, Lchl;->d:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 83
    :cond_0
    iget-object v0, p0, Lchl;->b:Lcij;

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    check-cast p2, Lfnl;

    invoke-direct {p0, p1, p2}, Lchl;->a(Lfsg;Lfnl;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    check-cast p2, Lfnl;

    invoke-direct {p0, p1, p2}, Lchl;->a(Lfsg;Lfnl;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    check-cast p2, Lfnl;

    invoke-direct {p0, p1, p2}, Lchl;->a(Lfsg;Lfnl;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
