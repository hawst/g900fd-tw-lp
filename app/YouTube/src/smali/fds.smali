.class public Lfds;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lfet;

.field public final b:Lcwg;


# direct methods
.method public constructor <init>(Lfet;Lcwg;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfet;

    iput-object v0, p0, Lfds;->a:Lfet;

    .line 27
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    iput-object v0, p0, Lfds;->b:Lcwg;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Lhog;)Z
    .locals 1

    .prologue
    .line 51
    if-eqz p1, :cond_0

    iget-object v0, p1, Lhog;->i:Libf;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhog;->i:Libf;

    iget-object v0, v0, Libf;->k:Libg;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhog;->i:Libf;

    iget-object v0, v0, Libf;->k:Libg;

    iget-object v0, v0, Libg;->a:Lhqs;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhog;->i:Libf;

    iget-object v0, v0, Libf;->k:Libg;

    iget-object v0, v0, Libg;->a:Lhqs;

    iget-object v0, v0, Lhqs;->a:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lhog;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lfds;->a:Lfet;

    invoke-virtual {v0}, Lfet;->a()Lftj;

    move-result-object v0

    .line 33
    iget-object v1, p1, Lhog;->i:Libf;

    iget-object v1, v1, Libf;->a:Ljava/lang/String;

    iput-object v1, v0, Lftj;->a:Ljava/lang/String;

    .line 34
    iget-object v1, p1, Lhog;->i:Libf;

    iget-object v1, v1, Libf;->b:Ljava/lang/String;

    iput-object v1, v0, Lftj;->c:Ljava/lang/String;

    .line 35
    iget-object v1, p1, Lhog;->i:Libf;

    iget v1, v1, Libf;->c:I

    iput v1, v0, Lftj;->j:I

    .line 36
    iget-object v1, p1, Lhog;->i:Libf;

    iget-object v1, v1, Libf;->i:Ljava/lang/String;

    iput-object v1, v0, Lftj;->b:Ljava/lang/String;

    .line 37
    iget-object v1, p1, Lhog;->a:[B

    invoke-virtual {v0, v1}, Lftj;->a([B)V

    .line 38
    iget-object v1, p0, Lfds;->b:Lcwg;

    invoke-virtual {v1, v0}, Lcwg;->a(Lftj;)V

    .line 39
    invoke-virtual {v0}, Lftj;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Lhog;)[B
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lfds;->a(Lhog;)Z

    move-result v0

    invoke-static {v0}, Lb;->c(Z)V

    .line 45
    iget-object v0, p1, Lhog;->i:Libf;

    iget-object v0, v0, Libf;->k:Libg;

    iget-object v0, v0, Libg;->a:Lhqs;

    iget-object v0, v0, Lhqs;->a:[B

    return-object v0
.end method
