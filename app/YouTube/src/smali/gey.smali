.class public final Lgey;
.super Landroid/view/SurfaceView;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:F

.field final synthetic c:Lgeu;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lgeu;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 231
    iput-object p1, p0, Lgey;->c:Lgeu;

    .line 232
    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 233
    new-instance v0, Lgez;

    invoke-direct {v0, p0, p1}, Lgez;-><init>(Lgey;Lgeu;)V

    iput-object v0, p0, Lgey;->d:Ljava/lang/Runnable;

    .line 242
    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 251
    iget-object v0, p0, Lgey;->c:Lgeu;

    invoke-static {v0}, Lgeu;->c(Lgeu;)I

    move-result v0

    invoke-static {v0, p1}, Lgey;->getDefaultSize(II)I

    move-result v1

    .line 252
    iget-object v0, p0, Lgey;->c:Lgeu;

    invoke-static {v0}, Lgeu;->d(Lgeu;)I

    move-result v0

    invoke-static {v0, p2}, Lgey;->getDefaultSize(II)I

    move-result v0

    .line 257
    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2}, Lgeu;->c(Lgeu;)I

    move-result v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2}, Lgeu;->d(Lgeu;)I

    move-result v2

    if-lez v2, :cond_1

    .line 261
    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2}, Lgeu;->c(Lgeu;)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lgey;->c:Lgeu;

    invoke-static {v3}, Lgeu;->d(Lgeu;)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 262
    int-to-float v3, v1

    int-to-float v4, v0

    div-float/2addr v3, v4

    .line 263
    div-float/2addr v2, v3

    sub-float/2addr v2, v5

    .line 265
    const v3, 0x3c23d70a    # 0.01f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_4

    .line 267
    iget-object v0, p0, Lgey;->c:Lgeu;

    invoke-static {v0}, Lgeu;->d(Lgeu;)I

    move-result v0

    mul-int/2addr v0, v1

    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2}, Lgeu;->c(Lgeu;)I

    move-result v2

    div-int/2addr v0, v2

    .line 272
    :cond_0
    :goto_0
    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2, v1}, Lgeu;->a(Lgeu;I)I

    .line 273
    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2, v0}, Lgeu;->b(Lgeu;I)I

    .line 276
    :cond_1
    iget v2, p0, Lgey;->a:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_2

    iget v2, p0, Lgey;->b:F

    cmpl-float v2, v2, v5

    if-eqz v2, :cond_3

    .line 278
    :cond_2
    iput v5, p0, Lgey;->a:F

    .line 279
    iput v5, p0, Lgey;->b:F

    .line 280
    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2}, Lgeu;->b(Lgeu;)Lu;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 281
    iget-object v2, p0, Lgey;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lgey;->post(Ljava/lang/Runnable;)Z

    .line 285
    :cond_3
    invoke-static {v1, p1}, Lgey;->resolveSize(II)I

    move-result v1

    .line 286
    invoke-static {v0, p2}, Lgey;->resolveSize(II)I

    move-result v0

    .line 288
    invoke-virtual {p0, v1, v0}, Lgey;->setMeasuredDimension(II)V

    .line 289
    return-void

    .line 268
    :cond_4
    const v3, -0x43dc28f6    # -0.01f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 270
    iget-object v1, p0, Lgey;->c:Lgeu;

    invoke-static {v1}, Lgeu;->c(Lgeu;)I

    move-result v1

    mul-int/2addr v1, v0

    iget-object v2, p0, Lgey;->c:Lgeu;

    invoke-static {v2}, Lgeu;->d(Lgeu;)I

    move-result v2

    div-int/2addr v1, v2

    goto :goto_0
.end method
