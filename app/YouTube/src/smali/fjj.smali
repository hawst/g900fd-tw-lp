.class public final Lfjj;
.super Lfki;
.source "SourceFile"


# instance fields
.field public final a:Lhei;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field private final e:Lfqh;

.field private f:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhei;Lfqh;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lfki;-><init>()V

    .line 28
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lfjj;->a:Lhei;

    .line 29
    iput-object p2, p0, Lfjj;->e:Lfqh;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 43
    iget-object v0, p0, Lfjj;->f:[Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfjj;->a:Lhei;

    iget-object v0, v0, Lhei;->e:[Lhgz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfjj;->a:Lhei;

    iget-object v0, v0, Lhei;->e:[Lhgz;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, p0, Lfjj;->f:[Ljava/lang/CharSequence;

    move v0, v1

    :goto_0
    iget-object v3, p0, Lfjj;->a:Lhei;

    iget-object v3, v3, Lhei;->e:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lfjj;->f:[Ljava/lang/CharSequence;

    iget-object v4, p0, Lfjj;->a:Lhei;

    iget-object v4, v4, Lhei;->e:[Lhgz;

    aget-object v4, v4, v0

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    aput-object v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lfjj;->f:[Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    move-object v0, v2

    .line 56
    :goto_1
    return-object v0

    .line 47
    :cond_1
    new-array v0, v9, [Ljava/lang/CharSequence;

    const-string v3, "line.separator"

    .line 48
    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    const-string v3, "line.separator"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v8

    .line 47
    invoke-static {v0}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 49
    iget-object v5, p0, Lfjj;->f:[Ljava/lang/CharSequence;

    array-length v6, v5

    move v3, v1

    :goto_2
    if-ge v3, v6, :cond_3

    aget-object v0, v5, v3

    .line 50
    if-nez v2, :cond_2

    .line 49
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_2

    .line 53
    :cond_2
    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/CharSequence;

    aput-object v2, v7, v1

    aput-object v4, v7, v8

    aput-object v0, v7, v9

    invoke-static {v7}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_3

    :cond_3
    move-object v0, v2

    .line 56
    goto :goto_1
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfjj;->a:Lhei;

    iget-object v0, v0, Lhei;->d:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lfjj;->e:Lfqh;

    return-object v0
.end method
