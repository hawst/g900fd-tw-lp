.class public Lcwq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Levn;

.field b:Ljava/lang/Throwable;

.field volatile c:Lcwx;

.field d:Lcvo;


# direct methods
.method public constructor <init>(Levn;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcwq;->a:Levn;

    .line 41
    return-void
.end method

.method private handlePlayerGeometry(Lcwx;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 114
    iput-object p1, p0, Lcwq;->c:Lcwx;

    .line 115
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcwq;->b()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcwq;->b:Ljava/lang/Throwable;

    .line 47
    new-instance v0, Lcvo;

    invoke-direct {v0, p0}, Lcvo;-><init>(Lcwq;)V

    iput-object v0, p0, Lcwq;->d:Lcvo;

    .line 48
    iget-object v0, p0, Lcwq;->a:Levn;

    iget-object v1, p0, Lcwq;->d:Lcvo;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    monitor-exit p0

    return-void

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lgog;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0}, Lcwq;->j()V

    .line 103
    iget-object v0, p0, Lcwq;->d:Lcvo;

    iput-object p1, v0, Lcvo;->a:Lgog;

    .line 104
    return-void
.end method

.method public declared-synchronized b()V
    .locals 2

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcwq;->d:Lcvo;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcwq;->a:Levn;

    iget-object v1, p0, Lcwq;->d:Lcvo;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcwq;->d:Lcvo;

    .line 56
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "currentPlaybackSequenceMonitor became null here"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcwq;->b:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_0
    monitor-exit p0

    return-void

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Z
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcwq;->d:Lcvo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()I
    .locals 1

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcwq;->j()V

    .line 68
    iget-object v0, p0, Lcwq;->d:Lcvo;

    iget v0, v0, Lcvo;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()Z
    .locals 3

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcwq;->j()V

    .line 74
    iget-object v0, p0, Lcwq;->d:Lcvo;

    iget-object v1, v0, Lcvo;->c:Ldan;

    sget-object v2, Ldan;->d:Ldan;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcvo;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized f()Z
    .locals 1

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcwq;->j()V

    .line 80
    iget-object v0, p0, Lcwq;->d:Lcvo;

    invoke-virtual {v0}, Lcvo;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public g()Lcwx;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcwq;->c:Lcwx;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcwq;->j()V

    .line 91
    iget-object v0, p0, Lcwq;->d:Lcvo;

    iget-object v0, v0, Lcvo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized i()Lgog;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcwq;->j()V

    .line 97
    iget-object v0, p0, Lcwq;->d:Lcvo;

    iget-object v0, v0, Lcvo;->a:Lgog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 96
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method j()V
    .locals 3

    .prologue
    .line 107
    invoke-virtual {p0}, Lcwq;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "PlaybackMonitor queried outside playback sequence"

    iget-object v2, p0, Lcwq;->b:Ljava/lang/Throwable;

    invoke-direct {v0, v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 110
    :cond_0
    return-void
.end method
