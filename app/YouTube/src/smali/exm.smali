.class public final Lexm;
.super Lxj;
.source "SourceFile"


# instance fields
.field private final a:Lilw;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x14

    .line 20
    invoke-direct {p0}, Lxj;-><init>()V

    .line 21
    new-instance v0, Lilw;

    invoke-direct {v0}, Lilw;-><init>()V

    iput-object v0, p0, Lexm;->a:Lilw;

    .line 23
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " gzip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lexm;->b:Ljava/lang/String;

    .line 24
    iget-object v0, p0, Lexm;->a:Lilw;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lilw;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 25
    iget-object v0, p0, Lexm;->a:Lilw;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lilw;->b(JLjava/util/concurrent/TimeUnit;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected final a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lily;

    iget-object v1, p0, Lexm;->a:Lilw;

    invoke-direct {v0, v1}, Lily;-><init>(Lilw;)V

    invoke-virtual {v0, p1}, Lily;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v0

    .line 31
    const-string v1, "User-Agent"

    iget-object v2, p0, Lexm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 33
    return-object v0
.end method
