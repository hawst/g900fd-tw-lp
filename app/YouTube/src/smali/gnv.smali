.class public Lgnv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgln;


# instance fields
.field public final a:Lezj;

.field private final b:Lws;

.field private final c:Lght;

.field private final d:Lgll;


# direct methods
.method public constructor <init>(Lws;Lezj;Lght;Lgll;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lgnv;->b:Lws;

    .line 50
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgnv;->a:Lezj;

    .line 51
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lght;

    iput-object v0, p0, Lgnv;->c:Lght;

    .line 52
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgll;

    iput-object v0, p0, Lgnv;->d:Lgll;

    .line 53
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 57
    const v0, 0x323467f

    return v0
.end method

.method public final a(Ldzy;Lwv;)V
    .locals 6

    .prologue
    .line 74
    iget-boolean v0, p1, Ldzy;->d:Z

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lgjj;

    const-string v1, "malformed request proto"

    invoke-direct {v0, v1}, Lgjj;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lwv;->onErrorResponse(Lxa;)V

    .line 79
    :cond_0
    new-instance v0, Lgnw;

    iget-object v3, p0, Lgnv;->a:Lezj;

    iget-object v4, p0, Lgnv;->c:Lght;

    iget-object v5, p0, Lgnv;->d:Lgll;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lgnw;-><init>(Ldzy;Lwv;Lezj;Lght;Lgll;)V

    .line 81
    iget-object v1, p0, Lgnv;->b:Lws;

    invoke-virtual {v1, v0}, Lws;->a(Lwp;)Lwp;

    .line 82
    return-void
.end method

.method public a(Ldzy;)Z
    .locals 6

    .prologue
    .line 86
    iget-object v0, p0, Lgnv;->a:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lgnv;->c:Lght;

    .line 87
    invoke-virtual {v3}, Lght;->c()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 88
    iget-wide v2, p1, Ldzy;->h:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
