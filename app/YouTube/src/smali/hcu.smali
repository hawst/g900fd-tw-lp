.class public final Lhcu;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhcu;


# instance fields
.field public b:J

.field public c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22875
    const/4 v0, 0x0

    new-array v0, v0, [Lhcu;

    sput-object v0, Lhcu;->a:[Lhcu;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22876
    invoke-direct {p0}, Lidf;-><init>()V

    .line 22879
    const-string v0, ""

    iput-object v0, p0, Lhcu;->d:Ljava/lang/String;

    .line 22882
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhcu;->b:J

    .line 22885
    const-string v0, ""

    iput-object v0, p0, Lhcu;->c:Ljava/lang/String;

    .line 22876
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    .line 22934
    const/4 v0, 0x0

    .line 22935
    iget-object v1, p0, Lhcu;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 22936
    const/4 v0, 0x1

    iget-object v1, p0, Lhcu;->d:Ljava/lang/String;

    .line 22937
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22939
    :cond_0
    iget-wide v2, p0, Lhcu;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 22940
    const/4 v1, 0x2

    iget-wide v2, p0, Lhcu;->b:J

    .line 22941
    invoke-static {v1, v2, v3}, Lidd;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 22943
    :cond_1
    iget-object v1, p0, Lhcu;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 22944
    const/4 v1, 0x3

    iget-object v2, p0, Lhcu;->c:Ljava/lang/String;

    .line 22945
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22947
    :cond_2
    iget-object v1, p0, Lhcu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22948
    iput v0, p0, Lhcu;->J:I

    .line 22949
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 22872
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhcu;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhcu;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhcu;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcu;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhcu;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcu;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 4

    .prologue
    .line 22919
    iget-object v0, p0, Lhcu;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22920
    const/4 v0, 0x1

    iget-object v1, p0, Lhcu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 22922
    :cond_0
    iget-wide v0, p0, Lhcu;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 22923
    const/4 v0, 0x2

    iget-wide v2, p0, Lhcu;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    .line 22925
    :cond_1
    iget-object v0, p0, Lhcu;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 22926
    const/4 v0, 0x3

    iget-object v1, p0, Lhcu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 22928
    :cond_2
    iget-object v0, p0, Lhcu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 22930
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22898
    if-ne p1, p0, :cond_1

    .line 22904
    :cond_0
    :goto_0
    return v0

    .line 22899
    :cond_1
    instance-of v2, p1, Lhcu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 22900
    :cond_2
    check-cast p1, Lhcu;

    .line 22901
    iget-object v2, p0, Lhcu;->d:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcu;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-wide v2, p0, Lhcu;->b:J

    iget-wide v4, p1, Lhcu;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhcu;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhcu;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 22903
    :goto_2
    iget-object v2, p0, Lhcu;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhcu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 22904
    goto :goto_0

    .line 22901
    :cond_4
    iget-object v2, p0, Lhcu;->d:Ljava/lang/String;

    iget-object v3, p1, Lhcu;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhcu;->c:Ljava/lang/String;

    iget-object v3, p1, Lhcu;->c:Ljava/lang/String;

    .line 22903
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhcu;->I:Ljava/util/List;

    iget-object v3, p1, Lhcu;->I:Ljava/util/List;

    .line 22904
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 22908
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 22910
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcu;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 22911
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhcu;->b:J

    iget-wide v4, p0, Lhcu;->b:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 22912
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcu;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 22913
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhcu;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 22914
    return v0

    .line 22910
    :cond_0
    iget-object v0, p0, Lhcu;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 22912
    :cond_1
    iget-object v0, p0, Lhcu;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 22913
    :cond_2
    iget-object v1, p0, Lhcu;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
