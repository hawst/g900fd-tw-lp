.class public Ldmz;
.super Landroid/os/Binder;
.source "SourceFile"


# instance fields
.field public final synthetic b:Ldmu;


# direct methods
.method public constructor <init>(Ldmu;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Ldmz;->b:Ldmu;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Map;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Ldmz;->b:Ldmu;

    new-instance v1, Ljava/util/HashMap;

    iget-object v0, v0, Ldmu;->a:Ljava/util/Map;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 264
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Transfer binder: restore transfers for identity ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v2, p0, Ldmz;->b:Ldmu;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmu;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 266
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v1, v1, Ldmu;->d:Ldne;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Ldne;->a(ILjava/lang/Object;)I

    move-result v1

    invoke-static {v0, v1}, Ldmu;->a(Ldmu;I)I

    .line 267
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v2, p0, Ldmz;->b:Ldmu;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmu;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 252
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v1, v1, Ldmu;->d:Ldne;

    invoke-virtual {v1, p1, p2}, Ldne;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Ldmu;->a(Ldmu;I)I

    .line 253
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lgje;)V
    .locals 9

    .prologue
    .line 224
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v2, p0, Ldmz;->b:Ldmu;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmu;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 225
    iget-object v6, p0, Ldmz;->b:Ldmu;

    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v7, v0, Ldmu;->d:Ldne;

    const/4 v8, 0x2

    new-instance v0, Ldnk;

    const/4 v1, 0x0

    const/16 v4, 0x64

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ldnk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILgje;)V

    invoke-virtual {v7, v8, v0}, Ldne;->a(ILjava/lang/Object;)I

    move-result v0

    invoke-static {v6, v0}, Ldmu;->a(Ldmu;I)I

    .line 226
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILgje;)V
    .locals 9

    .prologue
    .line 237
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v2, p0, Ldmz;->b:Ldmu;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmu;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 238
    iget-object v6, p0, Ldmz;->b:Ldmu;

    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v7, v0, Ldmu;->d:Ldne;

    const/4 v3, 0x0

    const/4 v8, 0x2

    new-instance v0, Ldnk;

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Ldnk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILgje;)V

    invoke-virtual {v7, v8, v0}, Ldne;->a(ILjava/lang/Object;)I

    move-result v0

    invoke-static {v6, v0}, Ldmu;->a(Ldmu;I)I

    .line 244
    return-void
.end method

.method public final a(Ldmw;)Z
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, v0, Ldmu;->c:Ljava/util/Set;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v0, v0, Ldmu;->b:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ldmw;->a()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 270
    const-string v0, "Transfer binder: pause all transfers"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v2, p0, Ldmz;->b:Ldmu;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmu;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 272
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v1, v1, Ldmu;->d:Ldne;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Ldne;->a(I)I

    move-result v1

    invoke-static {v0, v1}, Ldmu;->a(Ldmu;I)I

    .line 273
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v2, p0, Ldmz;->b:Ldmu;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v1, v2}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmu;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 260
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v1, p0, Ldmz;->b:Ldmu;

    iget-object v1, v1, Ldmu;->d:Ldne;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Ldne;->a(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v0, v1}, Ldmu;->a(Ldmu;I)I

    .line 261
    return-void
.end method

.method public final b(Ldmw;)Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Ldmz;->b:Ldmu;

    iget-object v0, v0, Ldmu;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
