.class public final Ldwj;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final f:Ldwj;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field final c:Lgpa;

.field public final d:Ljava/lang/String;

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ldwk;

    invoke-direct {v0}, Ldwk;-><init>()V

    invoke-virtual {v0}, Ldwk;->a()Ldwj;

    move-result-object v0

    sput-object v0, Ldwj;->f:Ldwj;

    return-void
.end method

.method constructor <init>(Ldwk;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iget-object v0, p1, Ldwk;->a:Ljava/lang/String;

    iput-object v0, p0, Ldwj;->a:Ljava/lang/String;

    .line 24
    iget v0, p1, Ldwk;->b:I

    iput v0, p0, Ldwj;->b:I

    .line 25
    iget-object v0, p1, Ldwk;->c:Lgpa;

    iput-object v0, p0, Ldwj;->c:Lgpa;

    .line 26
    iget-object v0, p1, Ldwk;->d:Ljava/lang/String;

    iput-object v0, p0, Ldwj;->d:Ljava/lang/String;

    .line 27
    iget v0, p1, Ldwk;->e:I

    iput v0, p0, Ldwj;->e:I

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 69
    const-string v0, ""

    iget-object v1, p0, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 83
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 73
    const-string v0, ""

    iget-object v1, p0, Ldwj;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 87
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 89
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldwj;->d:Ljava/lang/String;

    invoke-static {v0, p1}, Ldyh;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 94
    if-eqz p1, :cond_0

    instance-of v1, p1, Ldwj;

    if-nez v1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    .line 97
    :cond_1
    check-cast p1, Ldwj;

    .line 98
    iget-object v1, p1, Ldwj;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ldwj;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p1, Ldwj;->d:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ldwj;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    iget v1, p0, Ldwj;->e:I

    iget v2, p1, Ldwj;->e:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 107
    iget-object v2, p0, Ldwj;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    .line 108
    iget-object v2, p0, Ldwj;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 109
    iget v2, p0, Ldwj;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 106
    invoke-static {v0}, Lb;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 114
    const-string v0, "MdxPlaybackDescriptor:\n  VideoId:%s\n  PlaylistId:%s\n  Index:%d"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 116
    iget-object v3, p0, Ldwj;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 117
    iget-object v3, p0, Ldwj;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 118
    iget v3, p0, Ldwj;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 114
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
