.class public final Lflw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;
.implements Lfrx;


# instance fields
.field public final a:Lhsd;

.field public final b:Ljava/util/List;

.field public c:Lfkn;

.field public d:Lflh;

.field private final e:I

.field private f:Ljava/lang/CharSequence;

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/util/List;

.field private i:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lhsd;)V
    .locals 6

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsd;

    iput-object v0, p0, Lflw;->a:Lhsd;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflw;->b:Ljava/util/List;

    .line 52
    iget-object v0, p1, Lhsd;->b:[Lhsf;

    array-length v0, v0

    iput v0, p0, Lflw;->e:I

    .line 53
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lflw;->e:I

    if-ge v0, v1, :cond_2

    .line 54
    iget-object v1, p1, Lhsd;->b:[Lhsf;

    aget-object v1, v1, v0

    .line 55
    iget-object v3, v1, Lhsf;->b:Lhsh;

    .line 56
    if-eqz v3, :cond_0

    .line 57
    new-instance v4, Lflx;

    iget-boolean v1, p1, Lhsd;->g:Z

    iget v5, p0, Lflw;->e:I

    .line 59
    if-eqz v1, :cond_1

    sub-int v1, v5, v0

    int-to-float v1, v1

    const/high16 v5, 0x40a00000    # 5.0f

    div-float/2addr v1, v5

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    const/4 v5, 0x0

    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    move-result v1

    :goto_1
    invoke-direct {v4, v3, v1, p0}, Lflx;-><init>(Lhsh;FLfqh;)V

    .line 61
    iget-object v1, p0, Lflw;->b:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 59
    goto :goto_1

    .line 64
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lflw;->i:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 194
    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->n:Lhog;

    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->n:Lhog;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lflw;->i:Ljava/util/Set;

    .line 200
    :cond_0
    :goto_0
    iget-object v0, p0, Lflw;->i:Ljava/util/Set;

    return-object v0

    .line 197
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lflw;->i:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 2

    .prologue
    .line 185
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 186
    iget-object v0, p0, Lflw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflx;

    .line 187
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_0

    .line 189
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lflw;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->f:Lhgz;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->f:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lflw;->g:Ljava/lang/CharSequence;

    .line 96
    :cond_0
    iget-object v0, p0, Lflw;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lflw;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->k:Lhgz;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->k:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lflw;->f:Ljava/lang/CharSequence;

    .line 107
    :cond_0
    iget-object v0, p0, Lflw;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v0, v0, Lhsd;->m:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 6

    .prologue
    .line 161
    iget-object v0, p0, Lflw;->h:Ljava/util/List;

    if-nez v0, :cond_1

    .line 162
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lflw;->a:Lhsd;

    iget-object v1, v1, Lhsd;->j:[Lhsj;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflw;->h:Ljava/util/List;

    .line 163
    iget-object v0, p0, Lflw;->a:Lhsd;

    iget-object v1, v0, Lhsd;->j:[Lhsj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 164
    iget-object v4, v3, Lhsj;->b:Lhoj;

    if-eqz v4, :cond_0

    .line 165
    iget-object v4, p0, Lflw;->h:Ljava/util/List;

    new-instance v5, Lfjk;

    iget-object v3, v3, Lhsj;->b:Lhoj;

    invoke-direct {v5, v3}, Lfjk;-><init>(Lhoj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lflw;->h:Ljava/util/List;

    return-object v0
.end method
