.class public final Lgxu;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field private b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 782
    invoke-direct {p0}, Lidf;-><init>()V

    .line 785
    const/4 v0, 0x0

    iput-object v0, p0, Lgxu;->a:Lhgz;

    .line 788
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lgxu;->b:[B

    .line 782
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 836
    const/4 v0, 0x0

    .line 837
    iget-object v1, p0, Lgxu;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 838
    const/4 v0, 0x1

    iget-object v1, p0, Lgxu;->a:Lhgz;

    .line 839
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 841
    :cond_0
    iget-object v1, p0, Lgxu;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 842
    const/4 v1, 0x2

    iget-object v2, p0, Lgxu;->b:[B

    .line 843
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 845
    :cond_1
    iget-object v1, p0, Lgxu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 846
    iput v0, p0, Lgxu;->J:I

    .line 847
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 778
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgxu;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgxu;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgxu;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgxu;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxu;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgxu;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lgxu;->b:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 824
    iget-object v0, p0, Lgxu;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 825
    const/4 v0, 0x1

    iget-object v1, p0, Lgxu;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 827
    :cond_0
    iget-object v0, p0, Lgxu;->b:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 828
    const/4 v0, 0x2

    iget-object v1, p0, Lgxu;->b:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 830
    :cond_1
    iget-object v0, p0, Lgxu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 832
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 800
    if-ne p1, p0, :cond_1

    .line 805
    :cond_0
    :goto_0
    return v0

    .line 801
    :cond_1
    instance-of v2, p1, Lgxu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 802
    :cond_2
    check-cast p1, Lgxu;

    .line 803
    iget-object v2, p0, Lgxu;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgxu;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgxu;->b:[B

    iget-object v3, p1, Lgxu;->b:[B

    .line 804
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgxu;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgxu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 805
    goto :goto_0

    .line 803
    :cond_4
    iget-object v2, p0, Lgxu;->a:Lhgz;

    iget-object v3, p1, Lgxu;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 804
    :cond_5
    iget-object v2, p0, Lgxu;->I:Ljava/util/List;

    iget-object v3, p1, Lgxu;->I:Ljava/util/List;

    .line 805
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 809
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 811
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxu;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 812
    iget-object v2, p0, Lgxu;->b:[B

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 818
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lgxu;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_1
    add-int/2addr v0, v1

    .line 819
    return v0

    .line 811
    :cond_1
    iget-object v0, p0, Lgxu;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    .line 814
    :goto_2
    iget-object v3, p0, Lgxu;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 815
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lgxu;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 814
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 818
    :cond_3
    iget-object v1, p0, Lgxu;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
