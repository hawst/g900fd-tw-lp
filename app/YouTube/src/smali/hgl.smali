.class public final Lhgl;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:Lhxf;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Lhog;

.field private g:Lhgz;

.field private h:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 50753
    invoke-direct {p0}, Lidf;-><init>()V

    .line 50756
    iput-wide v2, p0, Lhgl;->a:J

    .line 50759
    iput-wide v2, p0, Lhgl;->b:J

    .line 50762
    const-string v0, ""

    iput-object v0, p0, Lhgl;->d:Ljava/lang/String;

    .line 50765
    const-string v0, ""

    iput-object v0, p0, Lhgl;->e:Ljava/lang/String;

    .line 50768
    iput-object v1, p0, Lhgl;->f:Lhog;

    .line 50771
    iput-object v1, p0, Lhgl;->c:Lhxf;

    .line 50774
    iput-object v1, p0, Lhgl;->g:Lhgz;

    .line 50777
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhgl;->h:[B

    .line 50753
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 50861
    const/4 v0, 0x0

    .line 50862
    iget-wide v2, p0, Lhgl;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 50863
    const/4 v0, 0x1

    iget-wide v2, p0, Lhgl;->a:J

    .line 50864
    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 50866
    :cond_0
    iget-wide v2, p0, Lhgl;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 50867
    const/4 v1, 0x2

    iget-wide v2, p0, Lhgl;->b:J

    .line 50868
    invoke-static {v1, v2, v3}, Lidd;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 50870
    :cond_1
    iget-object v1, p0, Lhgl;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 50871
    const/4 v1, 0x3

    iget-object v2, p0, Lhgl;->d:Ljava/lang/String;

    .line 50872
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50874
    :cond_2
    iget-object v1, p0, Lhgl;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 50875
    const/4 v1, 0x4

    iget-object v2, p0, Lhgl;->e:Ljava/lang/String;

    .line 50876
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50878
    :cond_3
    iget-object v1, p0, Lhgl;->f:Lhog;

    if-eqz v1, :cond_4

    .line 50879
    const/4 v1, 0x5

    iget-object v2, p0, Lhgl;->f:Lhog;

    .line 50880
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50882
    :cond_4
    iget-object v1, p0, Lhgl;->c:Lhxf;

    if-eqz v1, :cond_5

    .line 50883
    const/4 v1, 0x6

    iget-object v2, p0, Lhgl;->c:Lhxf;

    .line 50884
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50886
    :cond_5
    iget-object v1, p0, Lhgl;->g:Lhgz;

    if-eqz v1, :cond_6

    .line 50887
    const/4 v1, 0x7

    iget-object v2, p0, Lhgl;->g:Lhgz;

    .line 50888
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50890
    :cond_6
    iget-object v1, p0, Lhgl;->h:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 50891
    const/16 v1, 0x9

    iget-object v2, p0, Lhgl;->h:[B

    .line 50892
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 50894
    :cond_7
    iget-object v1, p0, Lhgl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50895
    iput v0, p0, Lhgl;->J:I

    .line 50896
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 50749
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgl;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgl;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgl;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhgl;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhgl;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgl;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgl;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhgl;->f:Lhog;

    if-nez v0, :cond_2

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhgl;->f:Lhog;

    :cond_2
    iget-object v0, p0, Lhgl;->f:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhgl;->c:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhgl;->c:Lhxf;

    :cond_3
    iget-object v0, p0, Lhgl;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhgl;->g:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhgl;->g:Lhgz;

    :cond_4
    iget-object v0, p0, Lhgl;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhgl;->h:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 50831
    iget-wide v0, p0, Lhgl;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 50832
    const/4 v0, 0x1

    iget-wide v2, p0, Lhgl;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 50834
    :cond_0
    iget-wide v0, p0, Lhgl;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 50835
    const/4 v0, 0x2

    iget-wide v2, p0, Lhgl;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 50837
    :cond_1
    iget-object v0, p0, Lhgl;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 50838
    const/4 v0, 0x3

    iget-object v1, p0, Lhgl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 50840
    :cond_2
    iget-object v0, p0, Lhgl;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 50841
    const/4 v0, 0x4

    iget-object v1, p0, Lhgl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 50843
    :cond_3
    iget-object v0, p0, Lhgl;->f:Lhog;

    if-eqz v0, :cond_4

    .line 50844
    const/4 v0, 0x5

    iget-object v1, p0, Lhgl;->f:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 50846
    :cond_4
    iget-object v0, p0, Lhgl;->c:Lhxf;

    if-eqz v0, :cond_5

    .line 50847
    const/4 v0, 0x6

    iget-object v1, p0, Lhgl;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 50849
    :cond_5
    iget-object v0, p0, Lhgl;->g:Lhgz;

    if-eqz v0, :cond_6

    .line 50850
    const/4 v0, 0x7

    iget-object v1, p0, Lhgl;->g:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 50852
    :cond_6
    iget-object v0, p0, Lhgl;->h:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    .line 50853
    const/16 v0, 0x9

    iget-object v1, p0, Lhgl;->h:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 50855
    :cond_7
    iget-object v0, p0, Lhgl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 50857
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 50795
    if-ne p1, p0, :cond_1

    .line 50806
    :cond_0
    :goto_0
    return v0

    .line 50796
    :cond_1
    instance-of v2, p1, Lhgl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 50797
    :cond_2
    check-cast p1, Lhgl;

    .line 50798
    iget-wide v2, p0, Lhgl;->a:J

    iget-wide v4, p1, Lhgl;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lhgl;->b:J

    iget-wide v4, p1, Lhgl;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhgl;->d:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgl;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 50800
    :goto_1
    iget-object v2, p0, Lhgl;->e:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgl;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 50801
    :goto_2
    iget-object v2, p0, Lhgl;->f:Lhog;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhgl;->f:Lhog;

    if-nez v2, :cond_3

    .line 50802
    :goto_3
    iget-object v2, p0, Lhgl;->c:Lhxf;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhgl;->c:Lhxf;

    if-nez v2, :cond_3

    .line 50803
    :goto_4
    iget-object v2, p0, Lhgl;->g:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhgl;->g:Lhgz;

    if-nez v2, :cond_3

    .line 50804
    :goto_5
    iget-object v2, p0, Lhgl;->h:[B

    iget-object v3, p1, Lhgl;->h:[B

    .line 50805
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgl;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhgl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 50806
    goto :goto_0

    .line 50798
    :cond_4
    iget-object v2, p0, Lhgl;->d:Ljava/lang/String;

    iget-object v3, p1, Lhgl;->d:Ljava/lang/String;

    .line 50800
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgl;->e:Ljava/lang/String;

    iget-object v3, p1, Lhgl;->e:Ljava/lang/String;

    .line 50801
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhgl;->f:Lhog;

    iget-object v3, p1, Lhgl;->f:Lhog;

    .line 50802
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhgl;->c:Lhxf;

    iget-object v3, p1, Lhgl;->c:Lhxf;

    .line 50803
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhgl;->g:Lhgz;

    iget-object v3, p1, Lhgl;->g:Lhgz;

    .line 50804
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    .line 50805
    :cond_9
    iget-object v2, p0, Lhgl;->I:Ljava/util/List;

    iget-object v3, p1, Lhgl;->I:Ljava/util/List;

    .line 50806
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 50810
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 50812
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhgl;->a:J

    iget-wide v4, p0, Lhgl;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 50813
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhgl;->b:J

    iget-wide v4, p0, Lhgl;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 50814
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgl;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 50815
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgl;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 50816
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgl;->f:Lhog;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 50817
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgl;->c:Lhxf;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 50818
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgl;->g:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 50819
    iget-object v2, p0, Lhgl;->h:[B

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    .line 50825
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhgl;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_5
    add-int/2addr v0, v1

    .line 50826
    return v0

    .line 50814
    :cond_1
    iget-object v0, p0, Lhgl;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 50815
    :cond_2
    iget-object v0, p0, Lhgl;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 50816
    :cond_3
    iget-object v0, p0, Lhgl;->f:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_2

    .line 50817
    :cond_4
    iget-object v0, p0, Lhgl;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_3

    .line 50818
    :cond_5
    iget-object v0, p0, Lhgl;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_6
    move v2, v0

    move v0, v1

    .line 50821
    :goto_6
    iget-object v3, p0, Lhgl;->h:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 50822
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhgl;->h:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 50821
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 50825
    :cond_7
    iget-object v1, p0, Lhgl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
