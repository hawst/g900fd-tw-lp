.class public final Lhgw;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53328
    invoke-direct {p0}, Lidf;-><init>()V

    .line 53331
    const-string v0, ""

    iput-object v0, p0, Lhgw;->a:Ljava/lang/String;

    .line 53328
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 53368
    const/4 v0, 0x0

    .line 53369
    iget-object v1, p0, Lhgw;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 53370
    const/4 v0, 0x1

    iget-object v1, p0, Lhgw;->a:Ljava/lang/String;

    .line 53371
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 53373
    :cond_0
    iget-object v1, p0, Lhgw;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53374
    iput v0, p0, Lhgw;->J:I

    .line 53375
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 53324
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgw;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgw;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgw;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhgw;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 53359
    iget-object v0, p0, Lhgw;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53360
    const/4 v0, 0x1

    iget-object v1, p0, Lhgw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 53362
    :cond_0
    iget-object v0, p0, Lhgw;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 53364
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53342
    if-ne p1, p0, :cond_1

    .line 53346
    :cond_0
    :goto_0
    return v0

    .line 53343
    :cond_1
    instance-of v2, p1, Lhgw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 53344
    :cond_2
    check-cast p1, Lhgw;

    .line 53345
    iget-object v2, p0, Lhgw;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgw;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhgw;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgw;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 53346
    goto :goto_0

    .line 53345
    :cond_4
    iget-object v2, p0, Lhgw;->a:Ljava/lang/String;

    iget-object v3, p1, Lhgw;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgw;->I:Ljava/util/List;

    iget-object v3, p1, Lhgw;->I:Ljava/util/List;

    .line 53346
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 53350
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 53352
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgw;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 53353
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhgw;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 53354
    return v0

    .line 53352
    :cond_0
    iget-object v0, p0, Lhgw;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 53353
    :cond_1
    iget-object v1, p0, Lhgw;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
