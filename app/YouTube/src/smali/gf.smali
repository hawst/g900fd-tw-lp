.class Lgf;
.super Lge;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 898
    invoke-direct {p0}, Lge;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lea;)V
    .locals 1

    .prologue
    .line 923
    iget-object v0, p2, Lea;->a:Ljava/lang/Object;

    check-cast v0, Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 924
    return-void
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 903
    invoke-virtual {p1, p2}, Landroid/view/View;->canScrollHorizontally(I)Z

    move-result v0

    return v0
.end method

.method public final j(Landroid/view/View;)Lhj;
    .locals 2

    .prologue
    .line 951
    iget-object v0, p0, Lgf;->a:Ljava/util/WeakHashMap;

    if-nez v0, :cond_0

    .line 952
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lgf;->a:Ljava/util/WeakHashMap;

    .line 955
    :cond_0
    iget-object v0, p0, Lgf;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhj;

    .line 956
    if-nez v0, :cond_1

    .line 957
    new-instance v0, Lhj;

    invoke-direct {v0, p1}, Lhj;-><init>(Landroid/view/View;)V

    .line 958
    iget-object v1, p0, Lgf;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 960
    :cond_1
    return-object v0
.end method
