.class public Lfka;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhhk;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public final e:Lflz;

.field public f:Lfkp;

.field private final g:Lfqh;

.field private h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhhk;Lfqh;)V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0}, Lfic;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhk;

    iput-object v0, p0, Lfka;->a:Lhhk;

    .line 32
    new-instance v0, Lflz;

    iget-object v1, p1, Lhhk;->b:Lhxf;

    iget-object v2, p1, Lhhk;->i:Lhsk;

    invoke-direct {v0, v1, v2}, Lflz;-><init>(Lhxf;Lhsk;)V

    iput-object v0, p0, Lfka;->e:Lflz;

    .line 34
    iput-object p2, p0, Lfka;->g:Lfqh;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 103
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 104
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfka;->a:Lhhk;

    iget-object v0, v0, Lhhk;->f:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lfka;->h:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lfka;->a:Lhhk;

    iget-object v0, v0, Lhhk;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfka;->h:Ljava/lang/CharSequence;

    .line 45
    :cond_0
    iget-object v0, p0, Lfka;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lfka;->a:Lhhk;

    iget-object v0, v0, Lhhk;->h:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lfka;->g:Lfqh;

    return-object v0
.end method
