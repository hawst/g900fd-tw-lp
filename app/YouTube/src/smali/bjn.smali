.class public final Lbjn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/app/NotificationManager;

.field public final b:Ljava/util/HashMap;

.field final c:Ljava/util/Set;

.field private final d:Landroid/content/Context;

.field private final e:Leyp;

.field private final f:Lexd;

.field private final g:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Leyp;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lbjn;->d:Landroid/content/Context;

    .line 59
    iput-object p3, p0, Lbjn;->e:Leyp;

    .line 61
    const-string v0, "notification"

    .line 62
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lbjn;->a:Landroid/app/NotificationManager;

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lckz;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->b()Lexd;

    move-result-object v0

    iput-object v0, p0, Lbjn;->f:Lexd;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbjn;->g:Landroid/content/res/Resources;

    .line 66
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbjn;->c:Ljava/util/Set;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbjn;->b:Ljava/util/HashMap;

    .line 68
    invoke-virtual {p2, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 69
    return-void
.end method

.method static synthetic a(Lbjn;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 45
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v2, v1

    iget-object v3, p0, Lbjn;->g:Landroid/content/res/Resources;

    const/high16 v4, 0x7f0c0000

    invoke-virtual {v3, v4, v5, v5}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v3

    div-float/2addr v2, v3

    float-to-int v3, v2

    iget-object v2, p0, Lbjn;->g:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/16 v4, 0x40

    invoke-static {v2, v4}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v2

    int-to-float v2, v2

    int-to-float v4, v3

    div-float/2addr v2, v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v2, v2, v6, v6}, Landroid/graphics/Matrix;->setScale(FFFF)V

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v3

    div-int/lit8 v2, v0, 0x2

    const/4 v6, 0x0

    move-object v0, p1

    move v4, v3

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static a(J)Ljava/lang/String;
    .locals 8

    .prologue
    .line 324
    const-wide/32 v0, 0x100000

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 325
    const-string v0, "%.1f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    long-to-double v4, p0

    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 327
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0, p1}, Lfaq;->a(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lgbu;Z)V
    .locals 4

    .prologue
    .line 382
    iget-object v0, p1, Lgbu;->a:Ljava/lang/String;

    .line 383
    iget-object v1, p0, Lbjn;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 385
    if-eqz p2, :cond_0

    .line 387
    iget-object v1, p0, Lbjn;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 392
    :cond_1
    iget-object v1, p1, Lgbu;->h:Landroid/net/Uri;

    .line 393
    if-eqz v1, :cond_0

    .line 396
    iget-object v2, p0, Lbjn;->e:Leyp;

    new-instance v3, Lbjp;

    invoke-direct {v3, p0, v0}, Lbjp;-><init>(Lbjn;Ljava/lang/String;)V

    invoke-interface {v2, v1, v3}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lbjn;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    if-eqz p3, :cond_0

    .line 353
    iget-object v0, p0, Lbjn;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 379
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    if-eqz p2, :cond_0

    .line 360
    iget-object v0, p0, Lbjn;->e:Leyp;

    new-instance v1, Lbjo;

    invoke-direct {v1, p0, p1}, Lbjo;-><init>(Lbjn;Ljava/lang/String;)V

    invoke-interface {v0, p2, v1}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 378
    iget-object v0, p0, Lbjn;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private handleOfflinePlaylistDeleteEvent(Lblp;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 485
    iget-object v0, p1, Lblp;->a:Ljava/lang/String;

    .line 486
    iget-object v1, p0, Lbjn;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    iget-object v1, p0, Lbjn;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 488
    iget-object v1, p0, Lbjn;->a:Landroid/app/NotificationManager;

    const/16 v2, 0x3ec

    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 490
    return-void
.end method

.method private handleOfflinePlaylistProgressEvent(Lblr;)V
    .locals 14
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 473
    iget-boolean v0, p1, Lblr;->b:Z

    if-eqz v0, :cond_0

    .line 474
    iget-object v3, p1, Lblr;->a:Lglw;

    .line 475
    invoke-virtual {v3}, Lglw;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 476
    iget-object v0, v3, Lglw;->a:Lgbu;

    iget-object v2, v0, Lgbu;->a:Ljava/lang/String;

    iget-object v4, v3, Lglw;->a:Lgbu;

    const v0, 0x7f02014b

    iget-boolean v1, v3, Lglw;->c:Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lbjn;->d:Landroid/content/Context;

    const v1, 0x7f0901b7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f02014a

    :goto_0
    new-instance v3, Landroid/content/Intent;

    iget-object v5, p0, Lbjn;->d:Landroid/content/Context;

    const-class v6, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v5, 0x4000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v5, "pane"

    invoke-static {}, La;->g()Lbgh;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v2}, Lbjn;->a(Ljava/lang/String;)Lba;

    move-result-object v5

    iget-object v6, v4, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v6

    invoke-virtual {v6, v1}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lba;->c(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    invoke-virtual {v1, v0}, Lba;->a(I)Lba;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v6, v7}, Lba;->a(IIZ)Lba;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lba;->a(Z)Lba;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lba;->b(Z)Lba;

    move-result-object v0

    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v1, v6, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    const/4 v0, 0x1

    invoke-direct {p0, v4, v0}, Lbjn;->a(Lgbu;Z)V

    iget-object v0, p0, Lbjn;->a:Landroid/app/NotificationManager;

    const/16 v1, 0x3ec

    invoke-virtual {v5}, Lba;->a()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 481
    :cond_0
    :goto_1
    return-void

    .line 476
    :cond_1
    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    const v3, 0x7f0901b6

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 478
    :cond_2
    iget-object v0, v3, Lglw;->a:Lgbu;

    iget-object v4, v0, Lgbu;->a:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pane"

    invoke-static {}, La;->g()Lbgh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    iget-object v0, v3, Lglw;->a:Lgbu;

    iget v2, v0, Lgbu;->j:I

    invoke-virtual {v3}, Lglw;->a()I

    move-result v6

    iget v7, v3, Lglw;->b:I

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v8, p0, Lbjn;->f:Lexd;

    invoke-interface {v8}, Lexd;->a()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v0, p0, Lbjn;->d:Landroid/content/Context;

    const v1, 0x7f090193

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0, v4}, Lbjn;->a(Ljava/lang/String;)Lba;

    move-result-object v6

    iget-object v8, v3, Lglw;->a:Lgbu;

    iget-object v8, v8, Lgbu;->b:Ljava/lang/String;

    invoke-virtual {v6, v8}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v8

    iget-object v9, p0, Lbjn;->d:Landroid/content/Context;

    const v10, 0x7f09027b

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lba;->c(Ljava/lang/CharSequence;)Lba;

    move-result-object v8

    invoke-virtual {v8, v2}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v2

    const v8, 0x7f02014c

    invoke-virtual {v2, v8}, Lba;->a(I)Lba;

    move-result-object v2

    const/16 v8, 0x64

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v7, v9}, Lba;->a(IIZ)Lba;

    move-result-object v2

    invoke-virtual {v2, v1}, Lba;->a(Z)Lba;

    move-result-object v1

    invoke-virtual {v1, v0}, Lba;->b(Z)Lba;

    move-result-object v0

    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v7, 0x8000000

    invoke-static {v1, v2, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    iget-object v0, v3, Lglw;->a:Lgbu;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbjn;->a(Lgbu;Z)V

    iget-object v0, p0, Lbjn;->a:Landroid/app/NotificationManager;

    const/16 v1, 0x3ec

    invoke-virtual {v6}, Lba;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_1

    :cond_3
    iget-object v8, p0, Lbjn;->g:Landroid/content/res/Resources;

    const v9, 0x7f10000c

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v11

    const/4 v6, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v6

    invoke-virtual {v8, v9, v2, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method

.method private handleOfflineVideoCompleteEvent(Lbma;)V
    .locals 9
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 457
    iget-object v0, p1, Lbma;->a:Lgmb;

    iget-boolean v0, v0, Lgmb;->b:Z

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p1, Lbma;->a:Lgmb;

    iget-object v1, v0, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lbjn;->d:Landroid/content/Context;

    const-class v4, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x4000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "pane"

    invoke-static {}, La;->f()Lbgh;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lbjn;->d:Landroid/content/Context;

    const v4, 0x7f0901b5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v1}, Lbjn;->a(Ljava/lang/String;)Lba;

    move-result-object v4

    invoke-virtual {v4, v3}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v3

    iget-object v5, v0, Lgmb;->a:Lgcd;

    iget-object v5, v5, Lgcd;->j:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lba;->c(Ljava/lang/CharSequence;)Lba;

    move-result-object v3

    const v5, 0x7f02014b

    invoke-virtual {v3, v5}, Lba;->a(I)Lba;

    move-result-object v3

    invoke-virtual {v3, v6, v6, v6}, Lba;->a(IIZ)Lba;

    move-result-object v3

    invoke-virtual {v3, v6}, Lba;->a(Z)Lba;

    move-result-object v3

    invoke-virtual {v3, v8}, Lba;->b(Z)Lba;

    move-result-object v3

    iget-object v5, p0, Lbjn;->d:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v5, v6, v2, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v3, v2}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    iget-object v2, v0, Lgmb;->a:Lgcd;

    iget-object v2, v2, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lgmb;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v2, v0, v8}, Lbjn;->a(Ljava/lang/String;Landroid/net/Uri;Z)V

    iget-object v0, p0, Lbjn;->a:Landroid/app/NotificationManager;

    const/16 v2, 0x3eb

    invoke-virtual {v4}, Lba;->a()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 461
    :cond_0
    return-void
.end method

.method private handleOfflineVideoDeleteEvent(Lbmb;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 465
    iget-object v0, p1, Lbmb;->a:Ljava/lang/String;

    .line 466
    iget-object v1, p0, Lbjn;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    iget-object v1, p0, Lbjn;->c:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 468
    iget-object v1, p0, Lbjn;->a:Landroid/app/NotificationManager;

    const/16 v2, 0x3eb

    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 469
    return-void
.end method

.method private handleOfflineVideoStatusUpdateEvent(Lbmc;)V
    .locals 13
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 445
    iget-object v0, p1, Lbmc;->a:Lgmb;

    .line 446
    invoke-virtual {v0}, Lgmb;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 448
    iget-object v1, p0, Lbjn;->a:Landroid/app/NotificationManager;

    .line 449
    iget-object v0, v0, Lgmb;->a:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    const/16 v2, 0x3eb

    .line 448
    invoke-virtual {v1, v0, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    iget-boolean v0, p1, Lbmc;->b:Z

    if-eqz v0, :cond_0

    .line 451
    iget-object v3, p1, Lbmc;->a:Lgmb;

    iget-object v0, v3, Lgmb;->a:Lgcd;

    iget-object v4, v0, Lgcd;->b:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pane"

    invoke-static {}, La;->f()Lbgh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    iget-wide v6, v3, Lgmb;->h:J

    iget-wide v8, v3, Lgmb;->i:J

    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-gtz v0, :cond_3

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    const v2, 0x7f09027b

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v1, v2, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbjn;->d:Landroid/content/Context;

    const v10, 0x7f0901b8

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Lbjn;->a(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v11, v12

    const/4 v6, 0x1

    invoke-static {v8, v9}, Lbjn;->a(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v11, v6

    invoke-virtual {v2, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v4}, Lbjn;->a(Ljava/lang/String;)Lba;

    move-result-object v6

    invoke-virtual {v6, v1}, Lba;->c(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    invoke-virtual {v1, v2}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    const/16 v2, 0x64

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v0, v6}, Lba;->a(IIZ)Lba;

    invoke-virtual {p0, v4}, Lbjn;->a(Ljava/lang/String;)Lba;

    move-result-object v6

    const/4 v2, 0x1

    const/4 v1, 0x0

    const v0, 0x7f02014c

    invoke-virtual {v3}, Lgmb;->j()Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v2, 0x0

    const/4 v1, 0x1

    const v0, 0x7f02014a

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lba;->c(Ljava/lang/CharSequence;)Lba;

    move-result-object v7

    iget-object v8, p0, Lbjn;->d:Landroid/content/Context;

    invoke-virtual {v3, v8}, Lgmb;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Lba;->a(IIZ)Lba;

    :cond_2
    :goto_2
    iget-object v7, v3, Lgmb;->a:Lgcd;

    iget-object v7, v7, Lgcd;->j:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v7

    invoke-virtual {v7, v0}, Lba;->a(I)Lba;

    move-result-object v0

    invoke-virtual {v0, v2}, Lba;->a(Z)Lba;

    move-result-object v0

    invoke-virtual {v0, v1}, Lba;->b(Z)Lba;

    move-result-object v0

    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v2

    const/high16 v7, 0x8000000

    invoke-static {v1, v2, v5, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    iget-object v0, v3, Lgmb;->a:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v3}, Lgmb;->a()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lbjn;->a(Ljava/lang/String;Landroid/net/Uri;Z)V

    iget-object v0, p0, Lbjn;->a:Landroid/app/NotificationManager;

    const/16 v1, 0x3eb

    invoke-virtual {v6}, Lba;->a()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v4, v1, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_3
    const-wide/16 v0, 0x64

    mul-long/2addr v0, v6

    div-long/2addr v0, v8

    long-to-int v0, v0

    goto/16 :goto_1

    :cond_4
    iget-object v7, p0, Lbjn;->f:Lexd;

    invoke-interface {v7}, Lexd;->a()Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    const v2, 0x7f090193

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    const/4 v2, 0x0

    const/4 v1, 0x1

    goto :goto_2
.end method


# virtual methods
.method a(Ljava/lang/String;)Lba;
    .locals 4

    .prologue
    .line 430
    iget-object v0, p0, Lbjn;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lbjn;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lba;

    .line 440
    :goto_0
    return-object v0

    .line 434
    :cond_0
    new-instance v0, Lba;

    iget-object v1, p0, Lbjn;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lba;-><init>(Landroid/content/Context;)V

    .line 435
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lba;->a(J)Lba;

    move-result-object v0

    iget-object v1, p0, Lbjn;->g:Landroid/content/res/Resources;

    const v2, 0x7f0700b3

    .line 436
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lba;->b(I)Lba;

    move-result-object v0

    const/4 v1, 0x1

    .line 437
    invoke-virtual {v0, v1}, Lba;->c(I)Lba;

    move-result-object v0

    .line 438
    iget-object v1, p0, Lbjn;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
