.class public final Lgbw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static b:Landroid/util/SparseArray;

.field private static final c:Landroid/util/SparseIntArray;


# instance fields
.field public final a:I

.field private final d:I

.field private e:Landroid/net/Uri;

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:J


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/16 v11, 0x1e0

    const/16 v10, 0x168

    const/16 v9, 0x2d0

    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 171
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    .line 174
    sput-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/4 v7, 0x5

    new-instance v0, Lgbz;

    const/4 v1, 0x7

    const/16 v2, 0x1aa

    const/16 v3, 0xf0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 175
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x11

    new-instance v0, Lgbz;

    const/4 v1, 0x2

    const/16 v2, 0xb0

    const/16 v3, 0x90

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 176
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x12

    new-instance v0, Lgbz;

    const/4 v1, 0x3

    const/16 v2, 0x280

    move v3, v10

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 177
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x16

    new-instance v0, Lgbz;

    const/16 v1, 0x8

    const/16 v2, 0x500

    move v3, v9

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 178
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x24

    new-instance v0, Lgbz;

    const/16 v1, 0x9

    const/16 v2, 0x140

    const/16 v3, 0xf0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 179
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x25

    new-instance v0, Lgbz;

    const/16 v1, 0x1e

    const/16 v2, 0x780

    const/16 v3, 0x438

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 180
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x3e

    new-instance v2, Lgbz;

    const/16 v3, 0xe

    move v5, v11

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 181
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x40

    new-instance v2, Lgbz;

    const/16 v3, 0x1f

    const/16 v5, 0x438

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 182
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x50

    new-instance v2, Lgbz;

    const/16 v3, 0xb

    const/16 v5, 0x195

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 183
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x51

    new-instance v2, Lgbz;

    const/16 v3, 0xc

    move v5, v10

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 184
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x52

    new-instance v0, Lgbz;

    const/16 v1, 0x15

    const/16 v2, 0x280

    move v3, v10

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 185
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x53

    new-instance v0, Lgbz;

    const/16 v1, 0x16

    const/16 v2, 0x356

    move v3, v11

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 186
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x54

    new-instance v0, Lgbz;

    const/16 v1, 0x17

    const/16 v2, 0x500

    move v3, v9

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 187
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x58

    new-instance v2, Lgbz;

    const/16 v3, 0xd

    move v5, v9

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 188
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x64

    new-instance v0, Lgbz;

    const/16 v1, 0x18

    const/16 v2, 0x280

    move v3, v10

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 189
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x65

    new-instance v0, Lgbz;

    const/16 v1, 0x19

    const/16 v2, 0x356

    move v3, v11

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 190
    sget-object v6, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v7, 0x66

    new-instance v0, Lgbz;

    const/16 v1, 0x1a

    const/16 v2, 0x500

    move v3, v9

    move v5, v8

    invoke-direct/range {v0 .. v5}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v6, v7, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 191
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x71

    new-instance v2, Lgbz;

    const/16 v3, 0x10

    move v5, v9

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 192
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x72

    new-instance v2, Lgbz;

    const/16 v3, 0xf

    move v5, v11

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 193
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x77

    new-instance v2, Lgbz;

    const/16 v3, 0x14

    const/16 v5, 0xf0

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 194
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x9f

    new-instance v2, Lgbz;

    const/16 v3, 0x20

    const/16 v5, 0x438

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 195
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0xb4

    new-instance v2, Lgbz;

    const/16 v3, 0x23

    const/16 v5, 0x438

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0xba

    new-instance v2, Lgbz;

    const/16 v3, 0x22

    move v5, v9

    move v6, v8

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 197
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0xc1

    new-instance v2, Lgbz;

    const/16 v3, 0x26

    const/16 v5, 0x195

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 198
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    const/16 v1, 0x5d

    new-instance v2, Lgbz;

    const/16 v3, 0x1c

    move v5, v4

    move v6, v4

    move v7, v4

    invoke-direct/range {v2 .. v7}, Lgbz;-><init>(IIIZZ)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 201
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lgbw;->c:Landroid/util/SparseIntArray;

    .line 203
    :goto_0
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v4, v0, :cond_0

    .line 204
    sget-object v1, Lgbw;->c:Landroid/util/SparseIntArray;

    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbz;

    iget v0, v0, Lgbz;->b:I

    sget-object v2, Lgbw;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 203
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 454
    :cond_0
    new-instance v0, Lgbx;

    invoke-direct {v0}, Lgbx;-><init>()V

    sput-object v0, Lgbw;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput p1, p0, Lgbw;->a:I

    .line 231
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    if-eqz p3, :cond_0

    .line 235
    invoke-static {p2}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    const-string v3, "dnc"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p2

    .line 237
    :cond_0
    iput-object p2, p0, Lgbw;->e:Landroid/net/Uri;

    .line 238
    iput-object p3, p0, Lgbw;->f:Ljava/lang/String;

    .line 239
    iput-object p5, p0, Lgbw;->i:Ljava/lang/String;

    .line 240
    const-wide/16 v4, 0x0

    cmp-long v0, p6, v4

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 241
    iput-wide p6, p0, Lgbw;->j:J

    .line 242
    const-string v0, "video/wvm"

    invoke-virtual {v0, p5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lgbw;->h:Z

    .line 244
    sget-object v0, Lgbw;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbz;

    .line 245
    if-ltz p4, :cond_3

    move v3, v1

    :goto_1
    invoke-static {v3}, Lb;->b(Z)V

    .line 248
    if-eqz v0, :cond_1

    if-nez p4, :cond_1

    iget p4, v0, Lgbz;->a:I

    :cond_1
    iput p4, p0, Lgbw;->d:I

    .line 249
    if-eqz v0, :cond_4

    iget-boolean v0, v0, Lgbz;->c:Z

    if-eqz v0, :cond_4

    :goto_2
    iput-boolean v1, p0, Lgbw;->g:Z

    .line 250
    return-void

    :cond_2
    move v0, v2

    .line 240
    goto :goto_0

    :cond_3
    move v3, v2

    .line 245
    goto :goto_1

    :cond_4
    move v1, v2

    .line 249
    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 8

    .prologue
    .line 430
    .line 431
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const-class v0, Landroid/net/Uri;

    .line 432
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 433
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 434
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 435
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 436
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    move-object v0, p0

    .line 430
    invoke-direct/range {v0 .. v7}, Lgbw;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    .line 437
    return-void
.end method

.method public static synthetic a()Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lgbw;->c:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 333
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 329
    new-instance v0, Lgby;

    invoke-direct {v0}, Lgby;-><init>()V

    iget-object v1, p0, Lgbw;->e:Landroid/net/Uri;

    iput-object v1, v0, Lgby;->b:Landroid/net/Uri;

    iget-object v1, p0, Lgbw;->f:Ljava/lang/String;

    iput-object v1, v0, Lgby;->f:Ljava/lang/String;

    iget v1, p0, Lgbw;->d:I

    iput v1, v0, Lgby;->c:I

    iget-wide v2, p0, Lgbw;->j:J

    iput-wide v2, v0, Lgby;->e:J

    iget-object v1, p0, Lgbw;->i:Ljava/lang/String;

    iput-object v1, v0, Lgby;->d:Ljava/lang/String;

    iget v1, p0, Lgbw;->a:I

    iput v1, v0, Lgby;->a:I

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 441
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 290
    if-ne p0, p1, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v0

    .line 293
    :cond_1
    instance-of v2, p1, Lgbw;

    if-nez v2, :cond_2

    move v0, v1

    .line 294
    goto :goto_0

    .line 296
    :cond_2
    check-cast p1, Lgbw;

    .line 297
    iget v2, p0, Lgbw;->a:I

    iget v3, p1, Lgbw;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgbw;->d:I

    iget v3, p1, Lgbw;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgbw;->e:Landroid/net/Uri;

    iget-object v3, p1, Lgbw;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgbw;->f:Ljava/lang/String;

    iget-object v3, p1, Lgbw;->f:Ljava/lang/String;

    .line 298
    invoke-static {v2, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lgbw;->g:Z

    iget-boolean v3, p1, Lgbw;->g:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgbw;->h:Z

    iget-boolean v3, p1, Lgbw;->h:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgbw;->i:Ljava/lang/String;

    iget-object v3, p1, Lgbw;->i:Ljava/lang/String;

    .line 299
    invoke-static {v2, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lgbw;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 309
    iget-object v0, p0, Lgbw;->e:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lgbw;->f:Ljava/lang/String;

    iget v2, p0, Lgbw;->d:I

    iget-object v3, p0, Lgbw;->i:Ljava/lang/String;

    iget v4, p0, Lgbw;->a:I

    iget-wide v6, p0, Lgbw;->j:J

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x36

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "["

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 446
    iget v0, p0, Lgbw;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 447
    iget-object v0, p0, Lgbw;->e:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 448
    iget-object v0, p0, Lgbw;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 449
    iget v0, p0, Lgbw;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 450
    iget-object v0, p0, Lgbw;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 451
    iget-wide v0, p0, Lgbw;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 452
    return-void
.end method
