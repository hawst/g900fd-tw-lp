.class public final Lcjk;
.super Lcjh;
.source "SourceFile"


# instance fields
.field private final c:Levn;

.field private final d:Lfiu;

.field private final e:Lcaz;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Levn;Lfiu;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcjh;-><init>()V

    .line 37
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcjk;->c:Levn;

    .line 38
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfiu;

    iput-object v0, p0, Lcjk;->d:Lfiu;

    .line 39
    iget-object v0, p0, Lcjk;->d:Lfiu;

    iget-object v1, v0, Lfiu;->a:Lhbu;

    iget-object v1, v1, Lhbu;->c:Lhbv;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lfiu;->a:Lhbu;

    iget-object v0, v0, Lhbu;->c:Lhbv;

    iget-object v0, v0, Lhbv;->c:Lhyc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcaz;

    invoke-direct {v0, p1}, Lcaz;-><init>(Landroid/app/Activity;)V

    :goto_1
    iput-object v0, p0, Lcjk;->e:Lcaz;

    .line 40
    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcjk;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 45
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcjk;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 50
    return-void
.end method

.method public final c()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcjk;->e:Lcaz;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcjk;->e:Lcaz;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final handleChannelSubscribedEvent(Lbnz;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcjk;->d:Lfiu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjk;->d:Lfiu;

    .line 63
    invoke-virtual {v0}, Lfiu;->b()Lfmy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcjh;->a:Lcjj;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p1, Lbnz;->a:Ljava/lang/String;

    iget-object v1, p0, Lcjk;->d:Lfiu;

    .line 67
    invoke-virtual {v1}, Lfiu;->b()Lfmy;

    move-result-object v1

    iget-object v1, v1, Lfmy;->b:Ljava/lang/String;

    .line 65
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcjh;->a:Lcjj;

    invoke-interface {v0}, Lcjj;->n_()V

    .line 70
    :cond_0
    return-void
.end method

.method public final handleChannelUnsubscribedEvent(Lboa;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcjk;->d:Lfiu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjk;->d:Lfiu;

    .line 75
    invoke-virtual {v0}, Lfiu;->b()Lfmy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcjh;->a:Lcjj;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p1, Lboa;->a:Ljava/lang/String;

    iget-object v1, p0, Lcjk;->d:Lfiu;

    .line 79
    invoke-virtual {v1}, Lfiu;->b()Lfmy;

    move-result-object v1

    iget-object v1, v1, Lfmy;->b:Ljava/lang/String;

    .line 77
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcjh;->a:Lcjj;

    invoke-interface {v0}, Lcjj;->n_()V

    .line 82
    :cond_0
    return-void
.end method
