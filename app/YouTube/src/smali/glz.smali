.class public final Lglz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:J


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lflg;

.field public final c:J

.field public final d:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 22
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lglz;->e:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lflg;JJ)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lglz;->a:Ljava/lang/String;

    .line 35
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflg;

    iput-object v0, p0, Lglz;->b:Lflg;

    .line 36
    iput-wide p3, p0, Lglz;->c:J

    .line 37
    iput-wide p5, p0, Lglz;->d:J

    .line 38
    return-void
.end method


# virtual methods
.method a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    iget-object v2, p0, Lglz;->b:Lflg;

    iget v2, v2, Lflg;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    iget-object v2, p0, Lglz;->b:Lflg;

    iget v2, v2, Lflg;->b:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    :goto_2
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final b()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    invoke-virtual {p0}, Lglz;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v2

    .line 73
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 74
    invoke-virtual {p0}, Lglz;->c()J

    move-result-wide v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-gtz v0, :cond_3

    move v0, v1

    .line 76
    :goto_1
    iget-wide v6, p0, Lglz;->d:J

    sget-wide v8, Lglz;->e:J

    sub-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gez v3, :cond_4

    move v3, v1

    .line 78
    :goto_2
    if-nez v0, :cond_2

    if-eqz v3, :cond_0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 74
    goto :goto_1

    :cond_4
    move v3, v2

    .line 76
    goto :goto_2
.end method

.method public final c()J
    .locals 6

    .prologue
    .line 90
    iget-wide v0, p0, Lglz;->d:J

    iget-object v2, p0, Lglz;->b:Lflg;

    iget v2, v2, Lflg;->e:I

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final d()Lgma;
    .locals 4

    .prologue
    .line 94
    new-instance v0, Lgma;

    invoke-direct {v0}, Lgma;-><init>()V

    iget-object v1, p0, Lglz;->a:Ljava/lang/String;

    .line 95
    iput-object v1, v0, Lgma;->a:Ljava/lang/String;

    iget-object v1, p0, Lglz;->b:Lflg;

    .line 96
    iput-object v1, v0, Lgma;->b:Lflg;

    iget-wide v2, p0, Lglz;->c:J

    .line 97
    iput-wide v2, v0, Lgma;->c:J

    iget-wide v2, p0, Lglz;->d:J

    .line 98
    iput-wide v2, v0, Lgma;->d:J

    return-object v0
.end method
