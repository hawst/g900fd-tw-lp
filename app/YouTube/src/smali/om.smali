.class public final Lom;
.super Lok;
.source "SourceFile"

# interfaces
.implements Landroid/view/SubMenu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcv;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lok;-><init>(Landroid/content/Context;Lct;)V

    .line 34
    return-void
.end method


# virtual methods
.method public final clearHeader()V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0}, Lcv;->clearHeader()V

    .line 74
    return-void
.end method

.method public final getItem()Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0}, Lcv;->getItem()Landroid/view/MenuItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lom;->a(Landroid/view/MenuItem;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final setHeaderIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0, p1}, Lcv;->setHeaderIcon(I)Landroid/view/SubMenu;

    .line 56
    return-object p0
.end method

.method public final setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0, p1}, Lcv;->setHeaderIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 62
    return-object p0
.end method

.method public final setHeaderTitle(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0, p1}, Lcv;->setHeaderTitle(I)Landroid/view/SubMenu;

    .line 44
    return-object p0
.end method

.method public final setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0, p1}, Lcv;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/SubMenu;

    .line 50
    return-object p0
.end method

.method public final setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0, p1}, Lcv;->setHeaderView(Landroid/view/View;)Landroid/view/SubMenu;

    .line 68
    return-object p0
.end method

.method public final setIcon(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0, p1}, Lcv;->setIcon(I)Landroid/view/SubMenu;

    .line 79
    return-object p0
.end method

.method public final setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lom;->d:Ljava/lang/Object;

    check-cast v0, Lcv;

    invoke-interface {v0, p1}, Lcv;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/SubMenu;

    .line 85
    return-object p0
.end method
