.class public final Lcez;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lfsh;


# instance fields
.field private final a:Lfuq;

.field private final b:Lfhz;

.field private final c:Lfur;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/TextView;

.field private f:Lfkq;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfhz;Lfur;Lfuq;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcez;->b:Lfhz;

    .line 42
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfur;

    iput-object v0, p0, Lcez;->c:Lfur;

    .line 43
    iput-object p4, p0, Lcez;->a:Lfuq;

    .line 44
    const v0, 0x7f040047

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcez;->d:Landroid/view/View;

    .line 45
    iget-object v0, p0, Lcez;->d:Landroid/view/View;

    const v1, 0x7f0800c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcez;->e:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lcez;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    .prologue
    .line 25
    check-cast p2, Lfkq;

    iget-object v0, p0, Lcez;->e:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfkq;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-object p2, p0, Lcez;->f:Lfkq;

    iget-object v0, p0, Lcez;->d:Landroid/view/View;

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcez;->a:Lfuq;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcez;->a:Lfuq;

    invoke-interface {v0}, Lfuq;->a()V

    .line 54
    :cond_0
    iget-object v0, p0, Lcez;->f:Lfkq;

    iget-object v0, v0, Lfkq;->a:Lhut;

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p0, Lcez;->b:Lfhz;

    iget-object v1, p0, Lcez;->f:Lfkq;

    iget-object v1, v1, Lfkq;->a:Lhut;

    iget-object v2, p0, Lcez;->c:Lfur;

    invoke-interface {v2}, Lfur;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhut;Ljava/lang/Object;)V

    .line 59
    :cond_1
    :goto_0
    return-void

    .line 56
    :cond_2
    iget-object v0, p0, Lcez;->f:Lfkq;

    iget-object v0, v0, Lfkq;->b:Lhog;

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcez;->b:Lfhz;

    iget-object v1, p0, Lcez;->f:Lfkq;

    iget-object v1, v1, Lfkq;->b:Lhog;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    goto :goto_0
.end method
