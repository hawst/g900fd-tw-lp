.class public final Lbuu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbnn;


# instance fields
.field final a:Lfvi;

.field final b:Lfvi;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Lbuw;

.field private final h:Lbux;

.field private final i:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Leyp;Lbno;)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const v0, 0x7f0800a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbuu;->c:Landroid/view/View;

    .line 40
    new-instance v1, Lfvi;

    iget-object v0, p0, Lbuu;->c:Landroid/view/View;

    const v2, 0x7f0800aa

    .line 42
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lbuu;->a:Lfvi;

    .line 43
    new-instance v1, Lfvi;

    iget-object v0, p0, Lbuu;->c:Landroid/view/View;

    const v2, 0x7f0800ab

    .line 45
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {v1, p2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lbuu;->b:Lfvi;

    .line 46
    iget-object v0, p0, Lbuu;->c:Landroid/view/View;

    const v1, 0x7f0800ad

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuu;->d:Landroid/widget/TextView;

    .line 47
    iget-object v0, p0, Lbuu;->c:Landroid/view/View;

    const v1, 0x7f0800ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbuu;->e:Landroid/widget/TextView;

    .line 48
    iget-object v0, p0, Lbuu;->c:Landroid/view/View;

    const v1, 0x7f0800ac

    .line 49
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbuu;->f:Landroid/widget/ImageView;

    .line 50
    new-instance v0, Lbuw;

    invoke-direct {v0, p0}, Lbuw;-><init>(Lbuu;)V

    iput-object v0, p0, Lbuu;->g:Lbuw;

    .line 51
    new-instance v0, Lbux;

    invoke-direct {v0, p0}, Lbux;-><init>(Lbuu;)V

    iput-object v0, p0, Lbuu;->h:Lbux;

    .line 53
    const v0, 0x7f0800af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbuu;->i:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lbuu;->i:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020245

    .line 55
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 56
    iget-object v0, p0, Lbuu;->i:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0901fa

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 57
    iget-object v0, p0, Lbuu;->i:Landroid/view/View;

    const v1, 0x7f08011d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 59
    new-instance v0, Lbuv;

    invoke-direct {v0, p0, p3}, Lbuv;-><init>(Lbuu;Lbno;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    invoke-virtual {p0}, Lbuu;->c()V

    .line 67
    return-void
.end method


# virtual methods
.method a()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lbuu;->a:Lfvi;

    invoke-virtual {v0}, Lfvi;->b()V

    iget-object v0, v0, Lfvi;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    iget-object v0, p0, Lbuu;->a:Lfvi;

    const v1, 0x7f020223

    invoke-virtual {v0, v1}, Lfvi;->b(I)V

    .line 112
    return-void
.end method

.method public final a(Lcua;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    iget-object v2, p0, Lbuu;->c:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 72
    iget-object v2, p0, Lbuu;->i:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 74
    invoke-virtual {p0}, Lbuu;->a()V

    .line 75
    iget-object v2, p1, Lcua;->f:Lfnc;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcua;->f:Lfnc;

    invoke-virtual {v2}, Lfnc;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 76
    iget-object v2, p0, Lbuu;->a:Lfvi;

    .line 77
    iget-object v3, p1, Lcua;->f:Lfnc;

    iget-object v4, p0, Lbuu;->g:Lbuw;

    .line 76
    invoke-virtual {v2, v3, v4}, Lfvi;->a(Lfnc;Leyo;)V

    .line 81
    :cond_0
    iget-object v2, p0, Lbuu;->b:Lfvi;

    invoke-virtual {v2, v1}, Lfvi;->a(I)V

    .line 82
    iget-object v2, p1, Lcua;->e:Lfnc;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcua;->e:Lfnc;

    invoke-virtual {v2}, Lfnc;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    if-eqz v0, :cond_3

    .line 83
    iget-object v0, p0, Lbuu;->b:Lfvi;

    const v2, 0x7f0700b7

    invoke-virtual {v0, v2}, Lfvi;->b(I)V

    .line 85
    iget-object v0, p0, Lbuu;->b:Lfvi;

    .line 86
    iget-object v2, p1, Lcua;->e:Lfnc;

    iget-object v3, p0, Lbuu;->h:Lbux;

    .line 85
    invoke-virtual {v0, v2, v3}, Lfvi;->a(Lfnc;Leyo;)V

    .line 92
    :goto_2
    iget-object v0, p0, Lbuu;->d:Landroid/widget/TextView;

    iget-object v2, p1, Lcua;->d:Landroid/text/Spanned;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lbuu;->e:Landroid/widget/TextView;

    iget-object v2, p1, Lcua;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lbuu;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 95
    return-void

    :cond_1
    move v2, v1

    .line 75
    goto :goto_0

    :cond_2
    move v0, v1

    .line 82
    goto :goto_1

    .line 89
    :cond_3
    invoke-virtual {p0}, Lbuu;->b()V

    goto :goto_2
.end method

.method b()V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lbuu;->b:Lfvi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfvi;->a(Landroid/graphics/drawable/Drawable;)V

    .line 116
    iget-object v0, p0, Lbuu;->b:Lfvi;

    const v1, 0x7f0201e9

    invoke-virtual {v0, v1}, Lfvi;->c(I)V

    .line 117
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lbuu;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lbuu;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 101
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 105
    iget-object v0, p0, Lbuu;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lbuu;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 107
    return-void
.end method
