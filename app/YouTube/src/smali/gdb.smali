.class public final Lgdb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lgdc;

.field final b:Lebv;

.field private c:B


# direct methods
.method public constructor <init>(Lgdc;Lebv;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lgdb;->a:Lgdc;

    .line 43
    iput-object p2, p0, Lgdb;->b:Lebv;

    .line 44
    const/4 v0, 0x0

    iput-byte v0, p0, Lgdb;->c:B

    .line 45
    iget-object v0, p0, Lgdb;->a:Lgdc;

    const-string v1, "474000100000b00d0001c100000001f0002ab104b2"

    invoke-static {v1}, Lgdb;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lgdc;->a([B)V

    iget-object v0, p0, Lgdb;->a:Lgdc;

    const-string v1, "475000100002b0180001c10000e100f0000fe100f0060a04756e6400152c6928"

    invoke-static {v1}, Lgdb;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-interface {v0, v1}, Lgdc;->a([B)V

    .line 46
    return-void
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 3

    .prologue
    const/16 v2, 0xbc

    .line 207
    invoke-static {p0}, La;->x(Ljava/lang/String;)[B

    move-result-object v1

    .line 208
    array-length v0, v1

    if-gt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 209
    const/4 v0, -0x1

    invoke-static {v1, v2, v0}, La;->a([BIB)[B

    move-result-object v0

    return-object v0

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a([BJZ)[B
    .locals 10

    .prologue
    .line 129
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    if-eqz p4, :cond_0

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    or-int/lit8 v2, v2, 0x40

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    :cond_0
    iget-byte v1, p0, Lgdb;->c:B

    or-int/lit8 v1, v1, 0x30

    int-to-byte v1, v1

    iget-byte v2, p0, Lgdb;->c:B

    add-int/lit8 v2, v2, 0x1

    and-int/lit8 v2, v2, 0xf

    int-to-byte v2, v2

    iput-byte v2, p0, Lgdb;->c:B

    const/4 v2, 0x4

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/16 v4, 0x47

    aput-byte v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aget-byte v4, v0, v4

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x1

    aget-byte v0, v0, v4

    aput-byte v0, v2, v3

    const/4 v0, 0x3

    aput-byte v1, v2, v0

    .line 130
    array-length v1, p1

    const-wide/16 v4, -0x1

    cmp-long v0, p2, v4

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    new-array v0, v0, [B

    const/4 v3, 0x0

    const/16 v4, 0x50

    aput-byte v4, v0, v3

    const/4 v3, 0x1

    const/16 v4, 0x19

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x2

    const/16 v4, 0x11

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x3

    const/16 v4, 0x9

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x4

    const/4 v4, 0x1

    shr-long v4, p2, v4

    const-wide/16 v6, 0xff

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x5

    const/4 v4, 0x7

    shl-long v4, p2, v4

    const-wide/16 v6, 0x80

    and-long/2addr v4, v6

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    const/4 v3, 0x6

    const/4 v4, 0x0

    aput-byte v4, v0, v3

    :goto_0
    array-length v3, v0

    add-int/lit8 v3, v3, 0x1

    rsub-int v4, v3, 0xb8

    sub-int/2addr v4, v1

    if-ltz v4, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lb;->c(Z)V

    new-array v1, v4, [B

    const/4 v5, -0x1

    invoke-static {v1, v5}, Ljava/util/Arrays;->fill([BB)V

    const/4 v5, 0x3

    new-array v5, v5, [[B

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [B

    const/4 v8, 0x0

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    int-to-byte v3, v3

    aput-byte v3, v7, v8

    aput-object v7, v5, v6

    const/4 v3, 0x1

    aput-object v0, v5, v3

    const/4 v0, 0x2

    aput-object v1, v5, v0

    invoke-static {v5}, La;->a([[B)[B

    move-result-object v0

    .line 131
    const/4 v1, 0x3

    new-array v1, v1, [[B

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v0, v1, v2

    const/4 v0, 0x2

    aput-object p1, v1, v0

    invoke-static {v1}, La;->a([[B)[B

    move-result-object v1

    .line 132
    array-length v0, v1

    const/16 v2, 0xbc

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Lb;->c(Z)V

    .line 133
    return-object v1

    .line 130
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-byte v4, v0, v3

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 132
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 129
    :array_0
    .array-data 1
        0x1t
        0x0t
    .end array-data
.end method
