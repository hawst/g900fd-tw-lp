.class public final Lbms;
.super Ldfl;
.source "SourceFile"


# instance fields
.field private final a:Ldeg;

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    invoke-direct {p0, p1}, Ldfl;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Ldeg;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Ldeg;-><init>(II)V

    iput-object v0, p0, Lbms;->a:Ldeg;

    .line 28
    iget-object v0, p0, Lbms;->a:Ldeg;

    invoke-virtual {v0, v3, p2, v3, v3}, Ldeg;->setMargins(IIII)V

    .line 31
    invoke-virtual {p0, v3}, Lbms;->setBackgroundColor(I)V

    .line 32
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lbms;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-super {p0, v0}, Ldfl;->setVisibility(I)V

    .line 72
    return-void

    .line 71
    :cond_0
    iget v0, p0, Lbms;->c:I

    goto :goto_0
.end method


# virtual methods
.method a(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 66
    invoke-virtual {p0}, Lbms;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0902fc

    invoke-virtual {p0, v0, p1}, Lbms;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbms;->setText(Ljava/lang/CharSequence;)V

    .line 63
    return-void
.end method

.method public final c_(Z)V
    .locals 1

    .prologue
    .line 41
    iget-boolean v0, p0, Lbms;->b:Z

    if-ne v0, p1, :cond_0

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    iput-boolean p1, p0, Lbms;->b:Z

    .line 45
    invoke-direct {p0}, Lbms;->c()V

    goto :goto_0
.end method

.method public final r_()Ldeg;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lbms;->a:Ldeg;

    return-object v0
.end method

.method public final setVisibility(I)V
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lbms;->c:I

    if-ne v0, p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    iput p1, p0, Lbms;->c:I

    .line 54
    invoke-direct {p0}, Lbms;->c()V

    goto :goto_0
.end method
