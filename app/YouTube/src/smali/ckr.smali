.class final Lckr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private synthetic a:Lfwf;

.field private synthetic b:Lckq;


# direct methods
.method constructor <init>(Lckq;Lfwf;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lckr;->b:Lckq;

    iput-object p2, p0, Lckr;->a:Lfwf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lckr;->a:Lfwf;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lfwf;->q:Z

    .line 507
    iget-object v0, p0, Lckr;->b:Lckq;

    iget-object v0, v0, Lckq;->a:Lcjs;

    iget-object v0, v0, Lcjs;->h:Lfsi;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    .line 508
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 487
    check-cast p1, Lorg/json/JSONObject;

    :try_start_0
    iget-object v1, p0, Lckr;->a:Lfwf;

    const-string v0, "items"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    new-instance v4, Lfwd;

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lfwd;-><init>(Lorg/json/JSONObject;Lfwf;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lfwf;->m:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lfwf;->k:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iput-object v3, v1, Lfwf;->k:Ljava/util/List;

    const-string v0, "nextPageToken"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "nextPageToken"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lfwf;->m:Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lckr;->b:Lckq;

    iget-object v0, v0, Lckq;->a:Lcjs;

    invoke-static {v0}, Lcjs;->a(Lcjs;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v0, p0, Lckr;->a:Lfwf;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lfwf;->q:Z

    iget-object v0, p0, Lckr;->b:Lckq;

    iget-object v0, v0, Lckq;->a:Lcjs;

    iget-object v0, v0, Lcjs;->h:Lfsi;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    return-void

    :cond_2
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, v1, Lfwf;->m:Ljava/lang/String;

    iget-object v0, v1, Lfwf;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Lfwf;->l:I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lwo;

    invoke-direct {v1, v0}, Lwo;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {p0, v1}, Lckr;->onErrorResponse(Lxa;)V

    goto :goto_2
.end method
