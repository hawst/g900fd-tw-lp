.class public final Ldoj;
.super Landroid/view/OrientationEventListener;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field public a:Z

.field private final b:Ldok;

.field private final c:Landroid/os/Handler;

.field private final d:Z

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ldok;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Ldoj;-><init>(Landroid/content/Context;Landroid/view/WindowManager;Ldok;)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/WindowManager;Ldok;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 104
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;I)V

    .line 105
    const-string v0, "listener cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldok;

    iput-object v0, p0, Ldoj;->b:Ldok;

    .line 107
    invoke-interface {p2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    .line 109
    if-eqz v0, :cond_0

    if-ne v0, v4, :cond_2

    .line 110
    :cond_0
    if-ne v3, v4, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ldoj;->d:Z

    .line 115
    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Ldoj;->c:Landroid/os/Handler;

    .line 116
    return-void

    :cond_1
    move v0, v2

    .line 110
    goto :goto_0

    .line 112
    :cond_2
    if-ne v3, v1, :cond_3

    :goto_2
    iput-boolean v1, p0, Ldoj;->d:Z

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final disable()V
    .locals 2

    .prologue
    .line 166
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldoj;->a:Z

    .line 167
    iget-object v0, p0, Ldoj;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 168
    invoke-super {p0}, Landroid/view/OrientationEventListener;->disable()V

    .line 169
    return-void
.end method

.method public final enable()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 158
    iput v1, p0, Ldoj;->e:I

    .line 159
    const/4 v0, -0x1

    iput v0, p0, Ldoj;->f:I

    .line 160
    iput-boolean v1, p0, Ldoj;->a:Z

    .line 161
    invoke-super {p0}, Landroid/view/OrientationEventListener;->enable()V

    .line 162
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 145
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 147
    :goto_0
    iget v2, p0, Ldoj;->f:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 148
    iget-object v2, p0, Ldoj;->b:Ldok;

    invoke-interface {v2, v0}, Ldok;->b_(Z)V

    .line 152
    :goto_1
    iget v0, p1, Landroid/os/Message;->what:I

    iput v0, p0, Ldoj;->f:I

    .line 153
    return v1

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 150
    :cond_1
    iget-object v2, p0, Ldoj;->b:Ldok;

    invoke-interface {v2, v0}, Ldok;->b(Z)V

    goto :goto_1
.end method

.method public final onOrientationChanged(I)V
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 120
    if-ltz p1, :cond_0

    const/16 v0, 0x1e

    if-le p1, v0, :cond_1

    :cond_0
    const/16 v0, 0x14a

    if-lt p1, v0, :cond_4

    const/16 v0, 0x168

    if-ge p1, v0, :cond_4

    :cond_1
    move v0, v1

    .line 122
    :goto_0
    iget v5, p0, Ldoj;->e:I

    if-eq v0, v5, :cond_3

    .line 123
    iget-object v5, p0, Ldoj;->c:Landroid/os/Handler;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 125
    if-eq v0, v3, :cond_3

    .line 126
    if-eq v0, v1, :cond_2

    if-ne v0, v2, :cond_8

    :cond_2
    move v1, v3

    .line 129
    :goto_1
    iget-boolean v2, p0, Ldoj;->d:Z

    if-eqz v2, :cond_9

    .line 131
    :goto_2
    if-eqz v1, :cond_b

    .line 132
    :goto_3
    iget v1, p0, Ldoj;->f:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_c

    .line 133
    iget-object v1, p0, Ldoj;->c:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 140
    :cond_3
    :goto_4
    iput v0, p0, Ldoj;->e:I

    .line 141
    return-void

    .line 120
    :cond_4
    const/16 v0, 0x3c

    if-lt p1, v0, :cond_5

    const/16 v0, 0x78

    if-gt p1, v0, :cond_5

    const/4 v0, 0x3

    goto :goto_0

    :cond_5
    const/16 v0, 0x96

    if-lt p1, v0, :cond_6

    const/16 v0, 0xd2

    if-gt p1, v0, :cond_6

    move v0, v2

    goto :goto_0

    :cond_6
    const/16 v0, 0xf0

    if-lt p1, v0, :cond_7

    const/16 v0, 0x12c

    if-gt p1, v0, :cond_7

    const/4 v0, 0x5

    goto :goto_0

    :cond_7
    move v0, v3

    goto :goto_0

    :cond_8
    move v1, v4

    .line 126
    goto :goto_1

    .line 129
    :cond_9
    if-nez v1, :cond_a

    move v1, v3

    goto :goto_2

    :cond_a
    move v1, v4

    goto :goto_2

    :cond_b
    move v3, v4

    .line 131
    goto :goto_3

    .line 134
    :cond_c
    iget v1, p0, Ldoj;->f:I

    if-eq v1, v3, :cond_3

    .line 135
    iget-object v1, p0, Ldoj;->c:Landroid/os/Handler;

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_4
.end method
