.class public final Lcxo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcxl;


# instance fields
.field final a:Lcxs;

.field final b:Ljava/util/concurrent/Executor;

.field final c:Lezj;

.field volatile d:Z

.field volatile e:Lcxx;

.field volatile f:Lcxm;

.field g:Z

.field final h:Ljava/util/concurrent/LinkedBlockingQueue;

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcxs;Lezj;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcxo;->h:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 32
    new-instance v0, Lcxp;

    invoke-direct {v0, p0}, Lcxp;-><init>(Lcxo;)V

    iput-object v0, p0, Lcxo;->i:Ljava/lang/Runnable;

    .line 71
    new-instance v0, Lcxt;

    invoke-direct {v0, p0, p2}, Lcxt;-><init>(Lcxo;Lcxs;)V

    iput-object v0, p0, Lcxo;->a:Lcxs;

    .line 72
    iput-object p1, p0, Lcxo;->b:Ljava/util/concurrent/Executor;

    .line 73
    iput-object p3, p0, Lcxo;->c:Lezj;

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcxo;->d:Z

    .line 75
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcxo;->i:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcxo;->a(Ljava/lang/Runnable;)V

    .line 125
    return-void
.end method

.method public final a(Lcxm;)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcxo;->h:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-virtual {p0}, Lcxo;->a()V

    .line 109
    return-void
.end method

.method a(Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 177
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 178
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 182
    :goto_1
    return-void

    .line 177
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 180
    :cond_1
    iget-object v0, p0, Lcxo;->b:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 85
    iput-boolean p1, p0, Lcxo;->d:Z

    .line 86
    invoke-virtual {p0}, Lcxo;->a()V

    .line 87
    return-void
.end method
