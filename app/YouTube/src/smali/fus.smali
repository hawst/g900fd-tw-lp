.class public Lfus;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Landroid/app/Activity;

.field public b:Ljava/lang/Class;

.field public final c:Lglm;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lglm;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lfus;->a:Landroid/app/Activity;

    .line 45
    invoke-virtual {p0}, Lfus;->h()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lfus;->b:Ljava/lang/Class;

    .line 46
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglm;

    iput-object v0, p0, Lfus;->c:Lglm;

    .line 47
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lfus;->b:Ljava/lang/Class;

    .line 52
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lfus;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "ancestor_classname"

    iget-object v1, p0, Lfus;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    :cond_0
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 78
    return-void
.end method

.method public a(Lbgh;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 242
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-eqz v0, :cond_3

    .line 245
    iget-object v0, p0, Lfus;->c:Lglm;

    invoke-interface {v0}, Lglm;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    iget-object v0, p1, Lbgh;->a:Ljava/lang/Class;

    const-class v2, Lbdp;

    if-eq v0, v2, :cond_0

    iget-object v0, p1, Lbgh;->a:Ljava/lang/Class;

    const-class v2, Lbdw;

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 247
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    const v2, 0x7f0901d9

    invoke-static {v0, v2, v1}, Leze;->a(Landroid/content/Context;II)V

    .line 258
    :goto_1
    return-void

    .line 246
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 251
    :cond_2
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lbgh;)V

    goto :goto_1

    .line 253
    :cond_3
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfus;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 254
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 255
    const-string v1, "pane"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 256
    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public a(Lgcd;)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    invoke-static {v0, p1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->a(Landroid/content/Context;Lgcd;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    .line 219
    return-void
.end method

.method public a(Lgom;)V
    .locals 3

    .prologue
    .line 261
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-eqz v0, :cond_1

    .line 264
    iget-object v0, p0, Lfus;->c:Lglm;

    invoke-interface {v0}, Lglm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p1, Lgom;->a:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-boolean v0, v0, Leaa;->g:Z

    if-nez v0, :cond_0

    .line 266
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    const v1, 0x7f0901d9

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    .line 277
    :goto_0
    return-void

    .line 270
    :cond_0
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Lgom;)V

    goto :goto_0

    .line 272
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfus;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 273
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 274
    const-string v1, "watch"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 275
    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Lhuo;[B)V
    .locals 1

    .prologue
    .line 101
    .line 102
    invoke-static {p1, p2}, La;->a(Lhuo;[B)Lbgh;

    move-result-object v0

    .line 101
    invoke-virtual {p0, v0}, Lfus;->a(Lbgh;)V

    .line 103
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    invoke-static {p1}, La;->e(Ljava/lang/String;)Lbgh;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfus;->a(Lbgh;)V

    .line 83
    return-void
.end method

.method public a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenPairingActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    .line 229
    return-void
.end method

.method public a(Ljava/lang/String;Lgog;)V
    .locals 6

    .prologue
    .line 120
    new-instance v0, Lgoh;

    const-string v2, ""

    const/4 v3, -0x1

    const/4 v4, 0x0

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V

    .line 126
    new-instance v1, Lgom;

    invoke-direct {v1, v0}, Lgom;-><init>(Lgoh;)V

    .line 127
    invoke-virtual {p0, v1}, Lfus;->a(Lgom;)V

    .line 128
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 206
    new-instance v0, Lgoh;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p1, p3, v1}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 211
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->a(Z)V

    .line 212
    new-instance v1, Lgom;

    invoke-direct {v1, v0}, Lgom;-><init>(Lgoh;)V

    .line 213
    invoke-virtual {p0, v1}, Lfus;->a(Lgom;)V

    .line 214
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 6

    .prologue
    .line 158
    new-instance v0, Lgoh;

    const/4 v4, 0x0

    sget-object v5, Lgog;->d:Lgog;

    move-object v1, p2

    move-object v2, p1

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V

    .line 164
    new-instance v1, Lgom;

    invoke-direct {v1, v0}, Lgom;-><init>(Lgoh;)V

    .line 165
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lgom;->c(Z)V

    .line 166
    invoke-virtual {v1, p4}, Lgom;->b(Z)V

    .line 167
    invoke-virtual {p0, v1}, Lfus;->a(Lgom;)V

    .line 168
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lfus;->b:Ljava/lang/Class;

    .line 57
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lbnf;->a:Lbnf;

    .line 108
    invoke-static {p1, v0}, La;->a(Ljava/lang/String;Lbnf;)Lbgh;

    move-result-object v0

    .line 107
    invoke-virtual {p0, v0}, Lfus;->a(Lbgh;)V

    .line 111
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lfus;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    .line 90
    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 194
    new-instance v0, Lgoh;

    const-string v1, ""

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 199
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgoh;->a(Z)V

    .line 200
    new-instance v1, Lgom;

    invoke-direct {v1, v0}, Lgom;-><init>(Lgoh;)V

    .line 201
    invoke-virtual {p0, v1}, Lfus;->a(Lgom;)V

    .line 202
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 94
    invoke-static {}, La;->d()Lbgh;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfus;->a(Lbgh;)V

    .line 95
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    .line 223
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfus;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/honeycomb/SettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    .line 224
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 233
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfus;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    .line 234
    return-void
.end method

.method public g()V
    .locals 3

    .prologue
    .line 238
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lfus;->a:Landroid/app/Activity;

    const-class v2, Lcom/google/android/apps/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lfus;->a(Landroid/content/Intent;)V

    .line 239
    return-void
.end method

.method public h()Ljava/lang/Class;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 61
    iget-object v1, p0, Lfus;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 62
    const-string v2, "ancestor_classname"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    :try_start_0
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 70
    :cond_0
    :goto_0
    return-object v0

    .line 67
    :catch_0
    move-exception v1

    goto :goto_0
.end method
