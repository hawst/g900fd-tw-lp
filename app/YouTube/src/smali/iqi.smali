.class final Liqi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[B

.field b:I

.field c:I

.field d:Liqi;

.field e:Liqi;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/16 v0, 0x800

    new-array v0, v0, [B

    iput-object v0, p0, Liqi;->a:[B

    return-void
.end method


# virtual methods
.method public final a()Liqi;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Liqi;->d:Liqi;

    if-eq v0, p0, :cond_0

    iget-object v0, p0, Liqi;->d:Liqi;

    .line 54
    :goto_0
    iget-object v2, p0, Liqi;->e:Liqi;

    iget-object v3, p0, Liqi;->d:Liqi;

    iput-object v3, v2, Liqi;->d:Liqi;

    .line 55
    iget-object v2, p0, Liqi;->d:Liqi;

    iget-object v3, p0, Liqi;->e:Liqi;

    iput-object v3, v2, Liqi;->e:Liqi;

    .line 56
    iput-object v1, p0, Liqi;->d:Liqi;

    .line 57
    iput-object v1, p0, Liqi;->e:Liqi;

    .line 58
    return-object v0

    :cond_0
    move-object v0, v1

    .line 53
    goto :goto_0
.end method

.method public final a(Liqi;)Liqi;
    .locals 1

    .prologue
    .line 66
    iput-object p0, p1, Liqi;->e:Liqi;

    .line 67
    iget-object v0, p0, Liqi;->d:Liqi;

    iput-object v0, p1, Liqi;->d:Liqi;

    .line 68
    iget-object v0, p0, Liqi;->d:Liqi;

    iput-object p1, v0, Liqi;->e:Liqi;

    .line 69
    iput-object p1, p0, Liqi;->d:Liqi;

    .line 70
    return-object p1
.end method

.method public final a(Liqi;I)V
    .locals 6

    .prologue
    const/16 v2, 0x800

    const/4 v5, 0x0

    .line 122
    iget v0, p1, Liqi;->c:I

    iget v1, p1, Liqi;->b:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    if-le v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 124
    :cond_0
    iget v0, p1, Liqi;->c:I

    add-int/2addr v0, p2

    if-le v0, v2, :cond_1

    .line 126
    iget-object v0, p1, Liqi;->a:[B

    iget v1, p1, Liqi;->b:I

    iget-object v2, p1, Liqi;->a:[B

    iget v3, p1, Liqi;->c:I

    iget v4, p1, Liqi;->b:I

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 127
    iget v0, p1, Liqi;->c:I

    iget v1, p1, Liqi;->b:I

    sub-int/2addr v0, v1

    iput v0, p1, Liqi;->c:I

    .line 128
    iput v5, p1, Liqi;->b:I

    .line 131
    :cond_1
    iget-object v0, p0, Liqi;->a:[B

    iget v1, p0, Liqi;->b:I

    iget-object v2, p1, Liqi;->a:[B

    iget v3, p1, Liqi;->c:I

    invoke-static {v0, v1, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    iget v0, p1, Liqi;->c:I

    add-int/2addr v0, p2

    iput v0, p1, Liqi;->c:I

    .line 133
    iget v0, p0, Liqi;->b:I

    add-int/2addr v0, p2

    iput v0, p0, Liqi;->b:I

    .line 134
    return-void
.end method
