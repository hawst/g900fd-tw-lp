.class public final Lffn;
.super Lfsp;
.source "SourceFile"


# instance fields
.field public a:Lhgt;

.field private final b:Lfth;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method constructor <init>(Lfsz;Lgit;Lfth;)V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p2, Lgit;->b:Lgiv;

    sget-object v1, Lfsq;->c:Lfsq;

    invoke-direct {p0, p1, v0, v1}, Lfsp;-><init>(Lfsz;Lgiv;Lfsq;)V

    .line 188
    const-string v0, ""

    iput-object v0, p0, Lffn;->c:Ljava/lang/String;

    .line 189
    const-string v0, ""

    iput-object v0, p0, Lffn;->d:Ljava/lang/String;

    .line 191
    const-string v0, ""

    iput-object v0, p0, Lffn;->j:Ljava/lang/String;

    .line 198
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfth;

    iput-object v0, p0, Lffn;->b:Lfth;

    .line 199
    new-instance v0, Lhgt;

    invoke-direct {v0}, Lhgt;-><init>()V

    iput-object v0, p0, Lffn;->a:Lhgt;

    .line 201
    sget-object v0, Lfhy;->a:[B

    invoke-virtual {p0, v0}, Lffn;->a([B)V

    .line 202
    return-void
.end method


# virtual methods
.method public final a(Lfjk;)Lffn;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p1, Lfjk;->a:Ljava/lang/String;

    invoke-static {v0}, Lffn;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffn;->d:Ljava/lang/String;

    .line 299
    iget-object v0, p1, Lfjk;->b:[B

    invoke-virtual {p0, v0}, Lffn;->a([B)V

    .line 300
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lffn;
    .locals 1

    .prologue
    .line 205
    invoke-static {p1}, Lffn;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffn;->c:Ljava/lang/String;

    .line 206
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    const-string v0, "search"

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lffn;
    .locals 1

    .prologue
    .line 218
    invoke-static {p1}, Lffn;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lffn;->j:Ljava/lang/String;

    .line 219
    return-object p0
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 312
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lffn;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lffn;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lffn;->a([Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method public final synthetic c()Lidh;
    .locals 2

    .prologue
    .line 182
    new-instance v0, Lhui;

    invoke-direct {v0}, Lhui;-><init>()V

    invoke-virtual {p0}, Lffn;->i()Lhjx;

    move-result-object v1

    iput-object v1, v0, Lhui;->a:Lhjx;

    iget-object v1, p0, Lffn;->c:Ljava/lang/String;

    iput-object v1, v0, Lhui;->b:Ljava/lang/String;

    iget-object v1, p0, Lffn;->j:Ljava/lang/String;

    iput-object v1, v0, Lhui;->c:Ljava/lang/String;

    iget-object v1, p0, Lffn;->a:Lhgt;

    iput-object v1, v0, Lhui;->e:Lhgt;

    iget-object v1, p0, Lffn;->d:Ljava/lang/String;

    iput-object v1, v0, Lhui;->d:Ljava/lang/String;

    iget-object v1, p0, Lffn;->b:Lfth;

    invoke-interface {v1}, Lfth;->a()Lhrn;

    move-result-object v1

    iput-object v1, v0, Lhui;->f:Lhrn;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 335
    invoke-virtual {p0}, Lffn;->j()Lggq;

    move-result-object v0

    .line 336
    const-string v1, "query"

    iget-object v2, p0, Lffn;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 337
    const-string v1, "params"

    iget-object v2, p0, Lffn;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 338
    const-string v1, "continuation"

    iget-object v2, p0, Lffn;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 339
    const-string v1, "filterOptions"

    iget-object v2, p0, Lffn;->a:Lhgt;

    invoke-static {v2}, Lidh;->a(Lidh;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;[B)Lggq;

    .line 340
    invoke-virtual {v0}, Lggq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
