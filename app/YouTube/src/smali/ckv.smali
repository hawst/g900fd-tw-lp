.class public Lckv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/io/File;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lezj;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lezj;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lckv;->b:Ljava/util/concurrent/Executor;

    .line 59
    iput-object p2, p0, Lckv;->c:Lezj;

    .line 60
    iput-object p3, p0, Lckv;->a:Ljava/io/File;

    .line 61
    return-void
.end method

.method static synthetic a(Lckv;Ljava/io/Closeable;)V
    .locals 1

    .prologue
    .line 28
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a(Leuc;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lckv;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lckw;

    invoke-direct {v1, p0, p1}, Lckw;-><init>(Lckv;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 94
    return-void
.end method

.method public varargs a([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lckv;->c:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    .line 112
    iget-object v2, p0, Lckv;->b:Ljava/util/concurrent/Executor;

    new-instance v3, Lckx;

    invoke-direct {v3, p0, p1, v0, v1}, Lckx;-><init>(Lckv;[Ljava/lang/String;J)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 137
    return-void
.end method
