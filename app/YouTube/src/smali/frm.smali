.class final Lfrm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lfrl;
    .locals 6

    .prologue
    .line 670
    :try_start_0
    new-instance v0, Lhro;

    invoke-direct {v0}, Lhro;-><init>()V

    .line 671
    invoke-static {p0, v0}, La;->b(Landroid/os/Parcel;Lidh;)Lidh;

    move-result-object v1

    check-cast v1, Lhro;

    .line 672
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 673
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v4, v0, [B

    .line 674
    invoke-virtual {p0, v4}, Landroid/os/Parcel;->readByteArray([B)V

    .line 675
    new-instance v0, Lfrl;

    const-class v5, Lfrf;

    .line 679
    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p0, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lfrf;

    invoke-direct/range {v0 .. v5}, Lfrl;-><init>(Lhro;J[BLfrf;)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 682
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 666
    invoke-static {p1}, Lfrm;->a(Landroid/os/Parcel;)Lfrl;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 666
    new-array v0, p1, [Lfrl;

    return-object v0
.end method
