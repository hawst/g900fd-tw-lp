.class public final Ledq;
.super Ledo;
.source "SourceFile"


# instance fields
.field final g:Ledt;

.field final h:Ledt;

.field private final i:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ledj;JJJIJLjava/util/List;Ledt;Ledt;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 236
    invoke-direct/range {p0 .. p11}, Ledo;-><init>(Ledj;JJJIJLjava/util/List;)V

    .line 238
    iput-object p12, p0, Ledq;->g:Ledt;

    .line 239
    iput-object p13, p0, Ledq;->h:Ledt;

    .line 240
    iput-object p14, p0, Ledq;->i:Landroid/net/Uri;

    .line 241
    return-void
.end method


# virtual methods
.method public final a(Ledk;)Ledj;
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 245
    iget-object v0, p0, Ledq;->g:Ledt;

    if-eqz v0, :cond_5

    .line 246
    iget-object v2, p0, Ledq;->g:Ledt;

    iget-object v0, p1, Ledk;->b:Lecw;

    iget-object v3, v0, Lecw;->a:Ljava/lang/String;

    iget-object v0, p1, Ledk;->b:Lecw;

    iget v6, v0, Lecw;->e:I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    :goto_0
    iget v8, v2, Ledt;->d:I

    if-ge v0, v8, :cond_4

    iget-object v8, v2, Ledt;->a:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v2, Ledt;->b:[I

    aget v8, v8, v0

    if-ne v8, v11, :cond_1

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v8, v2, Ledt;->b:[I

    aget v8, v8, v0

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    iget-object v8, v2, Ledt;->c:[Ljava/lang/String;

    aget-object v8, v8, v0

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    iget-object v8, v2, Ledt;->b:[I

    aget v8, v8, v0

    const/4 v9, 0x3

    if-ne v8, v9, :cond_3

    iget-object v8, v2, Ledt;->c:[Ljava/lang/String;

    aget-object v8, v8, v0

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    iget-object v8, v2, Ledt;->b:[I

    aget v8, v8, v0

    const/4 v9, 0x4

    if-ne v8, v9, :cond_0

    iget-object v8, v2, Ledt;->c:[Ljava/lang/String;

    aget-object v8, v8, v0

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v1

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    iget-object v0, v2, Ledt;->a:[Ljava/lang/String;

    iget v1, v2, Ledt;->d:I

    aget-object v0, v0, v1

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 248
    new-instance v1, Ledj;

    iget-object v2, p0, Ledq;->i:Landroid/net/Uri;

    const-wide/16 v6, -0x1

    invoke-direct/range {v1 .. v7}, Ledj;-><init>(Landroid/net/Uri;Ljava/lang/String;JJ)V

    .line 250
    :goto_2
    return-object v1

    :cond_5
    invoke-super {p0, p1}, Ledo;->a(Ledk;)Ledj;

    move-result-object v1

    goto :goto_2
.end method
