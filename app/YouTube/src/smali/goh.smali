.class public final Lgoh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Leaa;

.field public final b:Lgoj;

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 321
    new-instance v0, Lgoi;

    invoke-direct {v0}, Lgoi;-><init>()V

    sput-object v0, Lgoh;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Leaa;)V
    .locals 1

    .prologue
    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    iput-object p1, p0, Lgoh;->a:Leaa;

    .line 347
    invoke-direct {p0}, Lgoh;->g()Lgoj;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoj;

    iput-object v0, p0, Lgoh;->b:Lgoj;

    .line 349
    const/4 v0, 0x0

    iput v0, p0, Lgoh;->c:I

    .line 350
    return-void
.end method

.method public constructor <init>(Lhog;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    new-instance v0, Leaa;

    invoke-direct {v0}, Leaa;-><init>()V

    iput-object v0, p0, Lgoh;->a:Leaa;

    .line 64
    iget-object v0, p1, Lhog;->i:Libf;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p1, Lhog;->i:Libf;

    .line 66
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v2, v0, Libf;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Leaa;->a(Ljava/lang/String;)Leaa;

    .line 67
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v2, v0, Libf;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Leaa;->c(Ljava/lang/String;)Leaa;

    .line 68
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget v2, v0, Libf;->c:I

    iget-object v3, v0, Libf;->b:Ljava/lang/String;

    .line 69
    invoke-static {v2, v3}, Lgoh;->a(ILjava/lang/String;)I

    move-result v2

    .line 68
    invoke-virtual {v1, v2}, Leaa;->a(I)Leaa;

    .line 70
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v2, v0, Libf;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Leaa;->d(Ljava/lang/String;)Leaa;

    .line 71
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v2, v0, Libf;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Leaa;->e(Ljava/lang/String;)Leaa;

    .line 72
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-boolean v2, v0, Libf;->f:Z

    invoke-virtual {v1, v2}, Leaa;->b(Z)Leaa;

    .line 73
    iget-object v1, p0, Lgoh;->a:Leaa;

    invoke-virtual {v1, v4}, Leaa;->a(Z)Leaa;

    .line 75
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget v2, v0, Libf;->g:F

    const/high16 v3, 0x447a0000    # 1000.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Leaa;->c(I)Leaa;

    .line 76
    iget v0, v0, Libf;->j:I

    iput v0, p0, Lgoh;->c:I

    .line 107
    :goto_0
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v0, p1, Lhog;->a:[B

    if-eqz v0, :cond_3

    iget-object v0, p1, Lhog;->a:[B

    .line 108
    invoke-static {v0}, Licw;->a([B)Licw;

    move-result-object v0

    .line 107
    :goto_1
    invoke-virtual {v1, v0}, Leaa;->a(Licw;)Leaa;

    .line 111
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v5}, Leaa;->c(Z)Leaa;

    .line 112
    iget-object v0, p0, Lgoh;->a:Leaa;

    sget-object v1, Lgog;->a:Lgog;

    invoke-virtual {v1}, Lgog;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Leaa;->b(I)Leaa;

    .line 113
    invoke-direct {p0}, Lgoh;->g()Lgoj;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoj;

    iput-object v0, p0, Lgoh;->b:Lgoj;

    .line 114
    return-void

    .line 77
    :cond_0
    iget-object v0, p1, Lhog;->u:Libk;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p1, Lhog;->u:Libk;

    .line 80
    iget-object v1, p0, Lgoh;->a:Leaa;

    const-string v2, ""

    invoke-virtual {v1, v2}, Leaa;->a(Ljava/lang/String;)Leaa;

    .line 81
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v2, v0, Libk;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Leaa;->c(Ljava/lang/String;)Leaa;

    .line 82
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget v2, v0, Libk;->b:I

    invoke-virtual {v1, v2}, Leaa;->a(I)Leaa;

    .line 83
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Libk;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Leaa;->d(Ljava/lang/String;)Leaa;

    .line 84
    iget-object v0, p0, Lgoh;->a:Leaa;

    const-string v1, ""

    invoke-virtual {v0, v1}, Leaa;->e(Ljava/lang/String;)Leaa;

    .line 85
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v4}, Leaa;->b(Z)Leaa;

    .line 86
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v4}, Leaa;->a(Z)Leaa;

    .line 87
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v4}, Leaa;->c(I)Leaa;

    .line 89
    iput v4, p0, Lgoh;->c:I

    goto :goto_0

    .line 90
    :cond_1
    iget-object v0, p1, Lhog;->w:Lhpm;

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p1, Lhog;->w:Lhpm;

    .line 92
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v2, v0, Lhpm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Leaa;->a(Ljava/lang/String;)Leaa;

    .line 93
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v2, v0, Lhpm;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Leaa;->c(Ljava/lang/String;)Leaa;

    .line 94
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget v2, v0, Lhpm;->c:I

    iget-object v3, v0, Lhpm;->b:Ljava/lang/String;

    .line 95
    invoke-static {v2, v3}, Lgoh;->a(ILjava/lang/String;)I

    move-result v2

    .line 94
    invoke-virtual {v1, v2}, Leaa;->a(I)Leaa;

    .line 96
    iget-object v1, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Lhpm;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Leaa;->d(Ljava/lang/String;)Leaa;

    .line 97
    iget-object v0, p0, Lgoh;->a:Leaa;

    const-string v1, ""

    invoke-virtual {v0, v1}, Leaa;->e(Ljava/lang/String;)Leaa;

    .line 98
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v4}, Leaa;->b(Z)Leaa;

    .line 99
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v5}, Leaa;->a(Z)Leaa;

    .line 100
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v4}, Leaa;->c(I)Leaa;

    .line 102
    iput v4, p0, Lgoh;->c:I

    goto/16 :goto_0

    .line 104
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Navigation endpoint does not contain watch data"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_3
    sget-object v0, Licw;->b:Licw;

    goto/16 :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 6

    .prologue
    .line 121
    sget-object v5, Lgog;->a:Lgog;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    new-instance v0, Leaa;

    invoke-direct {v0}, Leaa;-><init>()V

    iput-object v0, p0, Lgoh;->a:Leaa;

    .line 133
    iget-object v0, p0, Lgoh;->a:Leaa;

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, p1}, Leaa;->a(Ljava/lang/String;)Leaa;

    .line 134
    iget-object v0, p0, Lgoh;->a:Leaa;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, p2}, Leaa;->c(Ljava/lang/String;)Leaa;

    .line 135
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, p3}, Leaa;->a(I)Leaa;

    .line 136
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, p4}, Leaa;->c(I)Leaa;

    .line 137
    iget-object v0, p0, Lgoh;->a:Leaa;

    const-string v1, ""

    invoke-virtual {v0, v1}, Leaa;->d(Ljava/lang/String;)Leaa;

    .line 138
    iget-object v0, p0, Lgoh;->a:Leaa;

    const-string v1, ""

    invoke-virtual {v0, v1}, Leaa;->e(Ljava/lang/String;)Leaa;

    .line 139
    iget-object v0, p0, Lgoh;->a:Leaa;

    sget-object v1, Licw;->b:Licw;

    invoke-virtual {v0, v1}, Leaa;->a(Licw;)Leaa;

    .line 141
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v2}, Leaa;->b(Z)Leaa;

    .line 142
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v2}, Leaa;->a(Z)Leaa;

    .line 143
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {p5}, Lgog;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Leaa;->b(I)Leaa;

    .line 144
    invoke-direct {p0}, Lgoh;->g()Lgoj;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoj;

    iput-object v0, p0, Lgoh;->b:Lgoj;

    .line 146
    iput v2, p0, Lgoh;->c:I

    .line 147
    return-void

    .line 133
    :cond_0
    const-string p1, ""

    goto :goto_0

    .line 134
    :cond_1
    const-string p2, ""

    goto :goto_1
.end method

.method public constructor <init>(Ljava/util/List;II)V
    .locals 1

    .prologue
    .line 153
    sget-object v0, Lgog;->a:Lgog;

    invoke-direct {p0, p1, p2, p3, v0}, Lgoh;-><init>(Ljava/util/List;IILgog;)V

    .line 154
    return-void
.end method

.method public constructor <init>(Ljava/util/List;IILgog;)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    new-instance v0, Leaa;

    invoke-direct {v0}, Leaa;-><init>()V

    iput-object v0, p0, Lgoh;->a:Leaa;

    .line 164
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 165
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 166
    iget-object v3, p0, Lgoh;->a:Leaa;

    invoke-virtual {v3, v0}, Leaa;->b(Ljava/lang/String;)Leaa;

    goto :goto_0

    .line 169
    :cond_1
    iget-object v0, p0, Lgoh;->a:Leaa;

    const-string v2, ""

    invoke-virtual {v0, v2}, Leaa;->c(Ljava/lang/String;)Leaa;

    .line 170
    if-gez p2, :cond_2

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    .line 172
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_3

    const/4 v0, 0x1

    .line 170
    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 173
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, p2}, Leaa;->a(I)Leaa;

    .line 174
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, p3}, Leaa;->c(I)Leaa;

    .line 175
    iget-object v0, p0, Lgoh;->a:Leaa;

    const-string v2, ""

    invoke-virtual {v0, v2}, Leaa;->d(Ljava/lang/String;)Leaa;

    .line 176
    iget-object v0, p0, Lgoh;->a:Leaa;

    const-string v2, ""

    invoke-virtual {v0, v2}, Leaa;->e(Ljava/lang/String;)Leaa;

    .line 177
    iget-object v0, p0, Lgoh;->a:Leaa;

    sget-object v2, Licw;->b:Licw;

    invoke-virtual {v0, v2}, Leaa;->a(Licw;)Leaa;

    .line 179
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v1}, Leaa;->b(Z)Leaa;

    .line 180
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, v1}, Leaa;->a(Z)Leaa;

    .line 181
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {p4}, Lgog;->ordinal()I

    move-result v2

    invoke-virtual {v0, v2}, Leaa;->b(I)Leaa;

    .line 182
    invoke-direct {p0}, Lgoh;->g()Lgoj;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoj;

    iput-object v0, p0, Lgoh;->b:Lgoj;

    .line 184
    iput v1, p0, Lgoh;->c:I

    .line 185
    return-void

    :cond_3
    move v0, v1

    .line 172
    goto :goto_1
.end method

.method private static a(ILjava/lang/String;)I
    .locals 1

    .prologue
    .line 370
    if-nez p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p0, -0x1

    :cond_0
    return p0
.end method

.method private g()Lgoj;
    .locals 3

    .prologue
    .line 194
    invoke-virtual {p0}, Lgoh;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    sget-object v0, Lgoj;->c:Lgoj;

    .line 208
    :goto_0
    return-object v0

    .line 199
    :cond_0
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 200
    sget-object v0, Lgoj;->b:Lgoj;

    goto :goto_0

    .line 201
    :cond_1
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 203
    sget-object v0, Lgoj;->a:Lgoj;

    goto :goto_0

    .line 207
    :cond_2
    const-string v1, "Invalid PlaybackStartDescriptor\n"

    invoke-virtual {p0}, Lgoh;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 208
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lgoh;->a:Leaa;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Leaa;->a(Z)Leaa;

    .line 279
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, p1}, Leaa;->d(Z)Leaa;

    .line 295
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->d:I

    return v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-virtual {v0, p1}, Leaa;->f(Z)Leaa;

    .line 307
    return-void
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 239
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->b:Ljava/util/List;

    .line 241
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    return v0
.end method

.method public final e()[B
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->f:Licw;

    invoke-virtual {v0}, Licw;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final f()Lgog;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->j:I

    invoke-static {v0}, Lgog;->a(I)Lgog;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 311
    const-string v1, "PlaybackStartDescriptor:\n  VideoId:%s\n  PlaylistId:%s\n  Index:%d\n  VideoIds:%s"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 313
    invoke-virtual {p0}, Lgoh;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    .line 314
    invoke-virtual {p0}, Lgoh;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    .line 315
    invoke-virtual {p0}, Lgoh;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x3

    .line 316
    invoke-virtual {p0}, Lgoh;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lgoh;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v2, v3

    .line 311
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 316
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lgoh;->a:Leaa;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lida;)V

    .line 367
    return-void
.end method
