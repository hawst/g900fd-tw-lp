.class public Lfkq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public final a:Lhut;

.field public final b:Lhog;

.field private final c:Lfqh;

.field private d:Lhgz;

.field private e:[B

.field private f:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhnd;Lfqh;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lfkq;->c:Lfqh;

    .line 43
    iget-object v0, p1, Lhnd;->a:Lhgz;

    iput-object v0, p0, Lfkq;->d:Lhgz;

    .line 44
    iget-object v0, p1, Lhnd;->b:Lhiq;

    .line 45
    iget-object v0, p1, Lhnd;->d:[B

    iput-object v0, p0, Lfkq;->e:[B

    .line 46
    iget-object v0, p1, Lhnd;->c:Lhog;

    iput-object v0, p0, Lfkq;->b:Lhog;

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lfkq;->a:Lhut;

    .line 48
    return-void
.end method

.method public constructor <init>(Lhnf;Lfqh;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lfkq;->c:Lfqh;

    .line 32
    iget-object v0, p1, Lhnf;->a:Lhgz;

    iput-object v0, p0, Lfkq;->d:Lhgz;

    .line 33
    iget-object v0, p1, Lhnf;->b:Lhiq;

    .line 34
    iget-object v0, p1, Lhnf;->d:[B

    iput-object v0, p0, Lfkq;->e:[B

    .line 35
    iget-object v0, p1, Lhnf;->c:Lhut;

    iput-object v0, p0, Lfkq;->a:Lhut;

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lfkq;->b:Lhog;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lfkq;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lfkq;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfkq;->f:Ljava/lang/CharSequence;

    .line 62
    :cond_0
    iget-object v0, p0, Lfkq;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lfkq;->e:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lfkq;->c:Lfqh;

    return-object v0
.end method
