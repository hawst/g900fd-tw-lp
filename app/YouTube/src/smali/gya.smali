.class public final Lgya;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lgyc;

.field private b:[Lgyb;

.field private c:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1476
    invoke-direct {p0}, Lidf;-><init>()V

    .line 1653
    sget-object v0, Lgyc;->a:[Lgyc;

    iput-object v0, p0, Lgya;->a:[Lgyc;

    .line 1656
    sget-object v0, Lgyb;->a:[Lgyb;

    iput-object v0, p0, Lgya;->b:[Lgyb;

    .line 1659
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lgya;->c:[B

    .line 1476
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1731
    .line 1732
    iget-object v0, p0, Lgya;->a:[Lgyc;

    if-eqz v0, :cond_1

    .line 1733
    iget-object v3, p0, Lgya;->a:[Lgyc;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 1734
    if-eqz v5, :cond_0

    .line 1735
    const/4 v6, 0x1

    .line 1736
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 1733
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1740
    :cond_2
    iget-object v2, p0, Lgya;->b:[Lgyb;

    if-eqz v2, :cond_4

    .line 1741
    iget-object v2, p0, Lgya;->b:[Lgyb;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 1742
    if-eqz v4, :cond_3

    .line 1743
    const/4 v5, 0x2

    .line 1744
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1741
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1748
    :cond_4
    iget-object v1, p0, Lgya;->c:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1749
    const/4 v1, 0x3

    iget-object v2, p0, Lgya;->c:[B

    .line 1750
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1752
    :cond_5
    iget-object v1, p0, Lgya;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1753
    iput v0, p0, Lgya;->J:I

    .line 1754
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1472
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgya;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgya;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgya;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgya;->a:[Lgyc;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgyc;

    iget-object v3, p0, Lgya;->a:[Lgyc;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lgya;->a:[Lgyc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lgya;->a:[Lgyc;

    :goto_2
    iget-object v2, p0, Lgya;->a:[Lgyc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lgya;->a:[Lgyc;

    new-instance v3, Lgyc;

    invoke-direct {v3}, Lgyc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgya;->a:[Lgyc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lgya;->a:[Lgyc;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lgya;->a:[Lgyc;

    new-instance v3, Lgyc;

    invoke-direct {v3}, Lgyc;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgya;->a:[Lgyc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgya;->b:[Lgyb;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgyb;

    iget-object v3, p0, Lgya;->b:[Lgyb;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lgya;->b:[Lgyb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lgya;->b:[Lgyb;

    :goto_4
    iget-object v2, p0, Lgya;->b:[Lgyb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lgya;->b:[Lgyb;

    new-instance v3, Lgyb;

    invoke-direct {v3}, Lgyb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgya;->b:[Lgyb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lgya;->b:[Lgyb;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgya;->b:[Lgyb;

    new-instance v3, Lgyb;

    invoke-direct {v3}, Lgyb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgya;->b:[Lgyb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lgya;->c:[B

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1708
    iget-object v1, p0, Lgya;->a:[Lgyc;

    if-eqz v1, :cond_1

    .line 1709
    iget-object v2, p0, Lgya;->a:[Lgyc;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1710
    if-eqz v4, :cond_0

    .line 1711
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 1709
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1715
    :cond_1
    iget-object v1, p0, Lgya;->b:[Lgyb;

    if-eqz v1, :cond_3

    .line 1716
    iget-object v1, p0, Lgya;->b:[Lgyb;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1717
    if-eqz v3, :cond_2

    .line 1718
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 1716
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1722
    :cond_3
    iget-object v0, p0, Lgya;->c:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1723
    const/4 v0, 0x3

    iget-object v1, p0, Lgya;->c:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 1725
    :cond_4
    iget-object v0, p0, Lgya;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 1727
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1672
    if-ne p1, p0, :cond_1

    .line 1678
    :cond_0
    :goto_0
    return v0

    .line 1673
    :cond_1
    instance-of v2, p1, Lgya;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 1674
    :cond_2
    check-cast p1, Lgya;

    .line 1675
    iget-object v2, p0, Lgya;->a:[Lgyc;

    iget-object v3, p1, Lgya;->a:[Lgyc;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgya;->b:[Lgyb;

    iget-object v3, p1, Lgya;->b:[Lgyb;

    .line 1676
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgya;->c:[B

    iget-object v3, p1, Lgya;->c:[B

    .line 1677
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgya;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgya;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 1678
    goto :goto_0

    .line 1677
    :cond_4
    iget-object v2, p0, Lgya;->I:Ljava/util/List;

    iget-object v3, p1, Lgya;->I:Ljava/util/List;

    .line 1678
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1682
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1684
    iget-object v2, p0, Lgya;->a:[Lgyc;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 1690
    :cond_0
    iget-object v0, p0, Lgya;->b:[Lgyb;

    if-nez v0, :cond_5

    mul-int/lit8 v2, v2, 0x1f

    .line 1696
    :cond_1
    iget-object v0, p0, Lgya;->c:[B

    if-nez v0, :cond_7

    mul-int/lit8 v2, v2, 0x1f

    .line 1702
    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lgya;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_0
    add-int/2addr v0, v1

    .line 1703
    return v0

    :cond_3
    move v2, v0

    move v0, v1

    .line 1686
    :goto_1
    iget-object v3, p0, Lgya;->a:[Lgyc;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 1687
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgya;->a:[Lgyc;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_2
    add-int/2addr v2, v3

    .line 1686
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1687
    :cond_4
    iget-object v2, p0, Lgya;->a:[Lgyc;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgyc;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_5
    move v0, v1

    .line 1692
    :goto_3
    iget-object v3, p0, Lgya;->b:[Lgyb;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1693
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgya;->b:[Lgyb;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 1692
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1693
    :cond_6
    iget-object v2, p0, Lgya;->b:[Lgyb;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgyb;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_7
    move v0, v1

    .line 1698
    :goto_5
    iget-object v3, p0, Lgya;->c:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1699
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lgya;->c:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 1698
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1702
    :cond_8
    iget-object v1, p0, Lgya;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
