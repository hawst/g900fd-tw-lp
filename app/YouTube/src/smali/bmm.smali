.class public final Lbmm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbic;


# instance fields
.field public final a:Lfgp;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lbmp;

.field private final e:Lgix;

.field private final f:Lfcd;

.field private final g:Lbhz;

.field private final h:Landroid/content/SharedPreferences;

.field private final i:Leyt;


# direct methods
.method public constructor <init>(Lbhz;Lfgp;Lgix;Lfcd;Landroid/content/SharedPreferences;Leyt;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhz;

    iput-object v0, p0, Lbmm;->g:Lbhz;

    .line 115
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgp;

    iput-object v0, p0, Lbmm;->a:Lfgp;

    .line 116
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbmm;->e:Lgix;

    .line 117
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcd;

    iput-object v0, p0, Lbmm;->f:Lfcd;

    .line 118
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lbmm;->h:Landroid/content/SharedPreferences;

    .line 119
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbmm;->i:Leyt;

    .line 120
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 198
    iput-object v0, p0, Lbmm;->b:Ljava/lang/String;

    .line 199
    iput-object v0, p0, Lbmm;->c:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public a(Lfgs;)V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lbmm;->a:Lfgp;

    new-instance v1, Lbmn;

    invoke-direct {v1, p0}, Lbmn;-><init>(Lbmm;)V

    iget-object v0, v0, Lfgp;->e:Lfgt;

    invoke-virtual {v0, p1, v1}, Lfgt;->b(Lfsp;Lwv;)V

    .line 182
    return-void
.end method

.method a(Lfon;)V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lbmm;->d:Lbmp;

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lbmm;->d:Lbmp;

    invoke-interface {v0, p1}, Lbmp;->a(Lfon;)V

    .line 306
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v5, 0x38a

    const/4 v3, 0x1

    .line 229
    iget-object v0, p0, Lbmm;->f:Lfcd;

    iget-object v1, p0, Lbmm;->e:Lgix;

    invoke-virtual {v0, v1}, Lfcd;->a(Lgix;)Landroid/accounts/Account;

    move-result-object v0

    new-instance v1, Leqy;

    iget-object v4, p0, Lbmm;->g:Lbhz;

    invoke-direct {v1, v4}, Leqy;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Leqy;->a(Landroid/accounts/Account;)Leqx;

    move-result-object v0

    check-cast v0, Leqy;

    iget-object v1, p0, Lbmm;->g:Lbhz;

    invoke-static {v1}, La;->n(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbmm;->h:Landroid/content/SharedPreferences;

    invoke-static {v1}, Lghq;->a(Landroid/content/SharedPreferences;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lbmm;->h:Landroid/content/SharedPreferences;

    invoke-static {v1}, Lghn;->a(Landroid/content/SharedPreferences;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Leqy;->a(I)Leqx;

    move-result-object v0

    check-cast v0, Leqy;

    invoke-virtual {v0, p1}, Leqy;->a(Ljava/lang/String;)Leqy;

    move-result-object v0

    invoke-virtual {v0, v3}, Leqy;->b(I)Leqx;

    move-result-object v0

    check-cast v0, Leqy;

    invoke-virtual {v0}, Leqy;->a()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    iget-object v1, p0, Lbmm;->g:Lbhz;

    iget-object v4, v1, Lbhz;->n:Landroid/util/SparseArray;

    if-eqz v4, :cond_3

    iget-object v4, v1, Lbhz;->n:Landroid/util/SparseArray;

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    :goto_1
    if-nez v3, :cond_1

    iget-object v2, v1, Lbhz;->n:Landroid/util/SparseArray;

    if-nez v2, :cond_0

    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, v1, Lbhz;->n:Landroid/util/SparseArray;

    :cond_0
    iget-object v2, v1, Lbhz;->n:Landroid/util/SparseArray;

    invoke-virtual {v2, v5, p0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {v1, v0, v5}, Lbhz;->startActivityForResult(Landroid/content/Intent;I)V

    .line 231
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 229
    goto :goto_0

    :cond_3
    move v3, v2

    .line 230
    goto :goto_1
.end method

.method a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 309
    new-instance v0, Lfon;

    iget-object v1, p0, Lbmm;->i:Leyt;

    invoke-interface {v1, p1}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lfon;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v0}, Lbmm;->a(Lfon;)V

    .line 310
    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 236
    const/16 v0, 0x38a

    if-eq p1, v0, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 258
    :goto_0
    return v0

    .line 240
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 256
    invoke-virtual {p0, v2}, Lbmm;->a(Ljava/lang/Throwable;)V

    .line 258
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 242
    :pswitch_0
    const-string v0, "com.google.android.libraries.inapp.EXTRA_RESPONSE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "orderId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 243
    iget-object v1, p0, Lbmm;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbmm;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lbmm;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lbmm;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    :cond_3
    const-string v0, "Offer and tip conflation occurred. Complete transaction request aborted"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lbmm;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lbmm;->a:Lfgp;

    new-instance v2, Lfgq;

    iget-object v3, v1, Lfgp;->b:Lfsz;

    iget-object v1, v1, Lfgp;->c:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lfgq;-><init>(Lfsz;Lgit;)V

    invoke-virtual {v2, v0}, Lfgq;->b(Ljava/lang/String;)Lfgq;

    move-result-object v0

    iget-object v1, p0, Lbmm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfgq;->a(Ljava/lang/String;)Lfgq;

    move-result-object v0

    iget-object v1, p0, Lbmm;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfgq;->c(Ljava/lang/String;)Lfgq;

    move-result-object v0

    iget-object v1, p0, Lbmm;->d:Lbmp;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lbmm;->d:Lbmp;

    invoke-interface {v1, v0}, Lbmp;->a(Lfgq;)V

    :cond_5
    sget-object v1, Lfhy;->a:[B

    invoke-virtual {v0, v1}, Lfgq;->a([B)V

    iget-object v1, p0, Lbmm;->a:Lfgp;

    new-instance v2, Lbmo;

    invoke-direct {v2, p0}, Lbmo;-><init>(Lbmm;)V

    iget-object v1, v1, Lfgp;->f:Lfgr;

    invoke-virtual {v1, v0, v2}, Lfgr;->b(Lfsp;Lwv;)V

    goto :goto_1

    .line 247
    :pswitch_1
    iget-object v0, p0, Lbmm;->g:Lbhz;

    invoke-virtual {v0}, Lbhz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 248
    new-instance v1, Ljava/lang/Error;

    const v2, 0x7f090329

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lbmm;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 252
    :pswitch_2
    iget-object v0, p0, Lbmm;->d:Lbmp;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbmm;->d:Lbmp;

    invoke-interface {v0}, Lbmp;->z()V

    goto/16 :goto_1

    .line 240
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
