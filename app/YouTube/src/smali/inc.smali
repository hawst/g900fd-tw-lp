.class public final Linc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Limf;


# instance fields
.field public final b:Lilw;

.field public c:Liln;

.field public d:Linj;

.field public e:Limg;

.field public final f:Limd;

.field public g:Linn;

.field public h:J

.field public i:Z

.field public final j:Z

.field public final k:Limb;

.field public l:Limb;

.field public m:Limd;

.field public n:Limd;

.field public o:Limd;

.field public p:Liqk;

.field public q:Lipt;

.field public r:Liql;

.field public s:Lipu;

.field public t:Lu;

.field public u:Limr;

.field private v:Ljava/io/InputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lind;

    invoke-direct {v0}, Lind;-><init>()V

    sput-object v0, Linc;->a:Limf;

    return-void
.end method

.method public constructor <init>(Lilw;Limb;ZLiln;Linj;Lini;Limd;)V
    .locals 2

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Linc;->h:J

    .line 188
    iput-object p1, p0, Linc;->b:Lilw;

    .line 189
    iput-object p2, p0, Linc;->k:Limb;

    .line 190
    iput-boolean p3, p0, Linc;->j:Z

    .line 191
    iput-object p4, p0, Linc;->c:Liln;

    .line 192
    iput-object p5, p0, Linc;->d:Linj;

    .line 193
    iput-object p6, p0, Linc;->p:Liqk;

    .line 194
    iput-object p7, p0, Linc;->f:Limd;

    .line 196
    if-eqz p4, :cond_0

    .line 197
    sget-object v0, Limh;->a:Limh;

    invoke-virtual {v0, p4, p0}, Limh;->b(Liln;Linc;)V

    .line 198
    iget-object v0, p4, Liln;->b:Limg;

    iput-object v0, p0, Linc;->e:Limg;

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Linc;->e:Limg;

    goto :goto_0
.end method

.method public static a(Lils;Lils;)Lils;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 729
    new-instance v2, Lilt;

    invoke-direct {v2}, Lilt;-><init>()V

    move v0, v1

    .line 731
    :goto_0
    iget-object v3, p0, Lils;->a:[Ljava/lang/String;

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    if-ge v0, v3, :cond_3

    .line 732
    invoke-virtual {p0, v0}, Lils;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 733
    invoke-virtual {p0, v0}, Lils;->b(I)Ljava/lang/String;

    move-result-object v4

    .line 734
    const-string v5, "Warning"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 735
    :cond_0
    invoke-static {v3}, Ling;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p1, v3}, Lils;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_2

    .line 738
    :cond_1
    invoke-virtual {v2, v3, v4}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 731
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 742
    :cond_3
    :goto_1
    iget-object v0, p1, Lils;->a:[Ljava/lang/String;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    if-ge v1, v0, :cond_5

    .line 743
    invoke-virtual {p1, v1}, Lils;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 744
    invoke-static {v0}, Ling;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 745
    invoke-virtual {p1, v1}, Lils;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 742
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 749
    :cond_5
    invoke-virtual {v2}, Lilt;->a()Lils;

    move-result-object v0

    return-object v0
.end method

.method public static a(Limd;)Limd;
    .locals 2

    .prologue
    .line 280
    if-eqz p0, :cond_0

    iget-object v0, p0, Limd;->g:Limf;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Limd;->a()Lime;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, v0, Lime;->g:Limf;

    invoke-virtual {v0}, Lime;->a()Limd;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static a(Ljava/net/URL;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 594
    invoke-static {p0}, Limo;->a(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Limo;->a(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/IOException;)Linc;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 384
    iget-object v6, p0, Linc;->p:Liqk;

    iget-object v0, p0, Linc;->d:Linj;

    if-eqz v0, :cond_1

    iget-object v0, p0, Linc;->c:Liln;

    if-eqz v0, :cond_1

    iget-object v0, p0, Linc;->d:Linj;

    iget-object v3, p0, Linc;->c:Liln;

    sget-object v4, Limh;->a:Limh;

    invoke-virtual {v4, v3}, Limh;->b(Liln;)I

    move-result v4

    if-gtz v4, :cond_1

    iget-object v3, v3, Liln;->b:Limg;

    iget-object v4, v3, Limg;->b:Ljava/net/Proxy;

    invoke-virtual {v4}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    sget-object v5, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v4, v5, :cond_0

    iget-object v4, v0, Linj;->d:Ljava/net/ProxySelector;

    if-eqz v4, :cond_0

    iget-object v4, v0, Linj;->d:Ljava/net/ProxySelector;

    iget-object v5, v0, Linj;->b:Ljava/net/URI;

    iget-object v7, v3, Limg;->b:Ljava/net/Proxy;

    invoke-virtual {v7}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v7

    invoke-virtual {v4, v5, v7, p1}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    :cond_0
    iget-object v4, v0, Linj;->e:Limn;

    invoke-virtual {v4, v3}, Limn;->a(Limg;)V

    instance-of v3, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-nez v3, :cond_1

    instance-of v3, p1, Ljavax/net/ssl/SSLProtocolException;

    if-nez v3, :cond_1

    :goto_0
    invoke-virtual {v0}, Linj;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Limg;

    iget-object v4, v0, Linj;->a:Lilj;

    iget-object v5, v0, Linj;->g:Ljava/net/Proxy;

    iget-object v7, v0, Linj;->h:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Linj;->d()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v4, v5, v7, v8}, Limg;-><init>(Lilj;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Ljava/lang/String;)V

    iget-object v4, v0, Linj;->e:Limn;

    invoke-virtual {v4, v3}, Limn;->a(Limg;)V

    goto :goto_0

    :cond_1
    if-eqz v6, :cond_2

    instance-of v0, v6, Lini;

    if-eqz v0, :cond_7

    :cond_2
    move v0, v2

    :goto_1
    iget-object v3, p0, Linc;->d:Linj;

    if-nez v3, :cond_3

    iget-object v3, p0, Linc;->c:Liln;

    if-eqz v3, :cond_6

    :cond_3
    iget-object v3, p0, Linc;->d:Linj;

    if-eqz v3, :cond_5

    iget-object v3, p0, Linc;->d:Linj;

    invoke-virtual {v3}, Linj;->c()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v3}, Linj;->b()Z

    move-result v4

    if-nez v4, :cond_4

    iget-boolean v4, v3, Linj;->i:Z

    if-nez v4, :cond_4

    invoke-virtual {v3}, Linj;->e()Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_4
    move v3, v2

    :goto_2
    if-eqz v3, :cond_6

    :cond_5
    instance-of v3, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v3, :cond_9

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, Ljava/security/cert/CertificateException;

    if-eqz v3, :cond_9

    move v3, v2

    :goto_3
    instance-of v4, p1, Ljava/net/ProtocolException;

    if-nez v3, :cond_a

    if-nez v4, :cond_a

    :goto_4
    if-eqz v2, :cond_6

    if-nez v0, :cond_b

    :cond_6
    const/4 v0, 0x0

    :goto_5
    return-object v0

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    move v3, v1

    goto :goto_2

    :cond_9
    move v3, v1

    goto :goto_3

    :cond_a
    move v2, v1

    goto :goto_4

    :cond_b
    invoke-virtual {p0}, Linc;->i()Liln;

    move-result-object v4

    new-instance v0, Linc;

    iget-object v1, p0, Linc;->b:Lilw;

    iget-object v2, p0, Linc;->k:Limb;

    iget-boolean v3, p0, Linc;->j:Z

    iget-object v5, p0, Linc;->d:Linj;

    check-cast v6, Lini;

    iget-object v7, p0, Linc;->f:Limd;

    invoke-direct/range {v0 .. v7}, Linc;-><init>(Lilw;Limb;ZLiln;Linj;Lini;Limd;)V

    goto :goto_5
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 302
    iget-wide v0, p0, Linc;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 303
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Linc;->h:J

    .line 304
    return-void
.end method

.method public final a(Lils;)V
    .locals 3

    .prologue
    .line 753
    iget-object v0, p0, Linc;->b:Lilw;

    iget-object v0, v0, Lilw;->e:Ljava/net/CookieHandler;

    .line 754
    if-eqz v0, :cond_0

    .line 755
    iget-object v1, p0, Linc;->k:Limb;

    invoke-virtual {v1}, Limb;->b()Ljava/net/URI;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v2}, Ling;->a(Lils;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    .line 757
    :cond_0
    return-void
.end method

.method public a(Liql;)V
    .locals 3

    .prologue
    .line 510
    iput-object p1, p0, Linc;->r:Liql;

    .line 511
    iget-boolean v0, p0, Linc;->i:Z

    if-eqz v0, :cond_0

    const-string v0, "gzip"

    iget-object v1, p0, Linc;->o:Limd;

    const-string v2, "Content-Encoding"

    invoke-virtual {v1, v2}, Limd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Linc;->o:Limd;

    invoke-virtual {v0}, Limd;->a()Lime;

    move-result-object v0

    const-string v1, "Content-Encoding"

    invoke-virtual {v0, v1}, Lime;->a(Ljava/lang/String;)Lime;

    move-result-object v0

    const-string v1, "Content-Length"

    invoke-virtual {v0, v1}, Lime;->a(Ljava/lang/String;)Lime;

    move-result-object v0

    invoke-virtual {v0}, Lime;->a()Limd;

    move-result-object v0

    iput-object v0, p0, Linc;->o:Limd;

    .line 516
    new-instance v0, Lipy;

    invoke-direct {v0, p1}, Lipy;-><init>(Liql;)V

    invoke-static {v0}, Liqa;->a(Liql;)Lipu;

    move-result-object v0

    iput-object v0, p0, Linc;->s:Lipu;

    .line 520
    :goto_0
    return-void

    .line 518
    :cond_0
    invoke-static {p1}, Liqa;->a(Liql;)Lipu;

    move-result-object v0

    iput-object v0, p0, Linc;->s:Lipu;

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Linc;->k:Limb;

    iget-object v0, v0, Limb;->b:Ljava/lang/String;

    invoke-static {v0}, Line;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Limo;->a()Lini;

    move-result-object v0

    iget-object v1, p0, Linc;->p:Liqk;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/net/URL;)Z
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Linc;->k:Limb;

    invoke-virtual {v0}, Limb;->a()Ljava/net/URL;

    move-result-object v0

    .line 834
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Limo;->a(Ljava/net/URL;)I

    move-result v1

    invoke-static {p1}, Limo;->a(Ljava/net/URL;)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Liqk;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Linc;->u:Limr;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 314
    :cond_0
    iget-object v0, p0, Linc;->p:Liqk;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Linc;->o:Limd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Limd;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Linc;->o:Limd;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 338
    :cond_0
    iget-object v0, p0, Linc;->o:Limd;

    return-object v0
.end method

.method public final f()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Linc;->v:Ljava/io/InputStream;

    .line 348
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Linc;->o:Limd;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Linc;->s:Lipu;

    invoke-static {v0}, Liqa;->a(Liql;)Lipu;

    move-result-object v0

    invoke-interface {v0}, Lipu;->f()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Linc;->v:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 405
    sget-object v0, Limh;->a:Limh;

    iget-object v1, p0, Linc;->b:Lilw;

    invoke-virtual {v0, v1}, Limh;->a(Lilw;)Lu;

    move-result-object v0

    .line 406
    if-nez v0, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v1, p0, Linc;->o:Limd;

    iget-object v2, p0, Linc;->l:Limb;

    invoke-static {v1, v2}, Limr;->a(Limd;Limb;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 410
    iget-object v0, p0, Linc;->l:Limb;

    iget-object v0, v0, Limb;->b:Ljava/lang/String;

    invoke-static {v0}, Line;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    :try_start_0
    iget-object v0, p0, Linc;->l:Limb;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 421
    :cond_2
    iget-object v1, p0, Linc;->o:Limd;

    invoke-static {v1}, Linc;->a(Limd;)Limd;

    invoke-interface {v0}, Lu;->i()Lu;

    move-result-object v0

    iput-object v0, p0, Linc;->t:Lu;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Linc;->g:Linn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Linc;->c:Liln;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Linc;->g:Linn;

    invoke-interface {v0}, Linn;->c()V

    .line 433
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Linc;->c:Liln;

    .line 434
    return-void
.end method

.method public final i()Liln;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 456
    iget-object v1, p0, Linc;->q:Lipt;

    if-eqz v1, :cond_2

    .line 458
    iget-object v1, p0, Linc;->q:Lipt;

    invoke-static {v1}, Limo;->a(Ljava/io/Closeable;)V

    .line 464
    :cond_0
    :goto_0
    iget-object v1, p0, Linc;->s:Lipu;

    if-nez v1, :cond_3

    .line 465
    iget-object v1, p0, Linc;->c:Liln;

    if-eqz v1, :cond_1

    iget-object v1, p0, Linc;->c:Liln;

    iget-object v1, v1, Liln;->c:Ljava/net/Socket;

    invoke-static {v1}, Limo;->a(Ljava/net/Socket;)V

    .line 466
    :cond_1
    iput-object v0, p0, Linc;->c:Liln;

    .line 490
    :goto_1
    return-object v0

    .line 459
    :cond_2
    iget-object v1, p0, Linc;->p:Liqk;

    if-eqz v1, :cond_0

    .line 460
    iget-object v1, p0, Linc;->p:Liqk;

    invoke-static {v1}, Limo;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 471
    :cond_3
    iget-object v1, p0, Linc;->s:Lipu;

    invoke-static {v1}, Limo;->a(Ljava/io/Closeable;)V

    .line 474
    iget-object v1, p0, Linc;->v:Ljava/io/InputStream;

    invoke-static {v1}, Limo;->a(Ljava/io/Closeable;)V

    .line 477
    iget-object v1, p0, Linc;->g:Linn;

    if-eqz v1, :cond_4

    iget-object v1, p0, Linc;->c:Liln;

    if-eqz v1, :cond_4

    iget-object v1, p0, Linc;->g:Linn;

    invoke-interface {v1}, Linn;->d()Z

    move-result v1

    if-nez v1, :cond_4

    .line 478
    iget-object v1, p0, Linc;->c:Liln;

    iget-object v1, v1, Liln;->c:Ljava/net/Socket;

    invoke-static {v1}, Limo;->a(Ljava/net/Socket;)V

    .line 479
    iput-object v0, p0, Linc;->c:Liln;

    goto :goto_1

    .line 484
    :cond_4
    iget-object v1, p0, Linc;->c:Liln;

    if-eqz v1, :cond_5

    sget-object v1, Limh;->a:Limh;

    iget-object v2, p0, Linc;->c:Liln;

    invoke-virtual {v1, v2}, Limh;->a(Liln;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 485
    iput-object v0, p0, Linc;->c:Liln;

    .line 488
    :cond_5
    iget-object v1, p0, Linc;->c:Liln;

    .line 489
    iput-object v0, p0, Linc;->c:Liln;

    move-object v0, v1

    .line 490
    goto :goto_1
.end method

.method public final j()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 528
    iget-object v2, p0, Linc;->k:Limb;

    iget-object v2, v2, Limb;->b:Ljava/lang/String;

    const-string v3, "HEAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 547
    :cond_0
    :goto_0
    return v0

    .line 532
    :cond_1
    iget-object v2, p0, Linc;->o:Limd;

    iget v2, v2, Limd;->c:I

    .line 533
    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 536
    goto :goto_0

    .line 542
    :cond_3
    iget-object v2, p0, Linc;->n:Limd;

    invoke-static {v2}, Ling;->a(Limd;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    const-string v2, "chunked"

    iget-object v3, p0, Linc;->n:Limd;

    const-string v4, "Transfer-Encoding"

    invoke-virtual {v3, v4}, Limd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 544
    goto :goto_0
.end method
