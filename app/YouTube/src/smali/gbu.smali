.class public final Lgbu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/util/Date;

.field public final f:Landroid/net/Uri;

.field public final g:Landroid/net/Uri;

.field public final h:Landroid/net/Uri;

.field public final i:Landroid/net/Uri;

.field public final j:I

.field public final k:Z

.field public final l:Z

.field private m:Landroid/net/Uri;

.field private n:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;IZZ)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgbu;->a:Ljava/lang/String;

    .line 81
    iput-object p2, p0, Lgbu;->b:Ljava/lang/String;

    .line 82
    iput-object p3, p0, Lgbu;->c:Ljava/lang/String;

    .line 83
    iput-object p4, p0, Lgbu;->d:Ljava/lang/String;

    .line 84
    iput-object p5, p0, Lgbu;->e:Ljava/util/Date;

    .line 85
    iput-object p6, p0, Lgbu;->f:Landroid/net/Uri;

    .line 86
    iput-object p7, p0, Lgbu;->m:Landroid/net/Uri;

    .line 87
    iput-object p8, p0, Lgbu;->n:Landroid/net/Uri;

    .line 88
    iput-object p9, p0, Lgbu;->g:Landroid/net/Uri;

    .line 89
    iput-object p10, p0, Lgbu;->h:Landroid/net/Uri;

    .line 90
    iput-object p11, p0, Lgbu;->i:Landroid/net/Uri;

    .line 91
    iput p12, p0, Lgbu;->j:I

    .line 92
    iput-boolean p13, p0, Lgbu;->k:Z

    .line 93
    iput-boolean p14, p0, Lgbu;->l:Z

    .line 94
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 138
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Lgbu;->a()Lgbv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lgbv;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lgbv;

    invoke-direct {v0}, Lgbv;-><init>()V

    iget-object v1, p0, Lgbu;->a:Ljava/lang/String;

    .line 98
    iput-object v1, v0, Lgbv;->a:Ljava/lang/String;

    iget-object v1, p0, Lgbu;->b:Ljava/lang/String;

    .line 99
    iput-object v1, v0, Lgbv;->b:Ljava/lang/String;

    iget-object v1, p0, Lgbu;->c:Ljava/lang/String;

    .line 100
    iput-object v1, v0, Lgbv;->c:Ljava/lang/String;

    iget-object v1, p0, Lgbu;->d:Ljava/lang/String;

    .line 101
    iput-object v1, v0, Lgbv;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbu;->e:Ljava/util/Date;

    .line 102
    iput-object v1, v0, Lgbv;->e:Ljava/util/Date;

    iget-object v1, p0, Lgbu;->f:Landroid/net/Uri;

    .line 103
    iput-object v1, v0, Lgbv;->f:Landroid/net/Uri;

    iget-object v1, p0, Lgbu;->m:Landroid/net/Uri;

    .line 104
    iput-object v1, v0, Lgbv;->g:Landroid/net/Uri;

    iget-object v1, p0, Lgbu;->n:Landroid/net/Uri;

    .line 105
    iput-object v1, v0, Lgbv;->h:Landroid/net/Uri;

    iget-object v1, p0, Lgbu;->g:Landroid/net/Uri;

    .line 106
    iput-object v1, v0, Lgbv;->i:Landroid/net/Uri;

    iget-object v1, p0, Lgbu;->h:Landroid/net/Uri;

    .line 107
    iput-object v1, v0, Lgbv;->j:Landroid/net/Uri;

    iget-object v1, p0, Lgbu;->i:Landroid/net/Uri;

    .line 108
    iput-object v1, v0, Lgbv;->k:Landroid/net/Uri;

    iget v1, p0, Lgbu;->j:I

    .line 109
    iput v1, v0, Lgbv;->l:I

    iget-boolean v1, p0, Lgbu;->k:Z

    .line 110
    iput-boolean v1, v0, Lgbv;->m:Z

    iget-boolean v1, p0, Lgbu;->l:Z

    .line 111
    iput-boolean v1, v0, Lgbv;->n:Z

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 116
    instance-of v0, p1, Lgbu;

    if-nez v0, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 120
    :goto_0
    return v0

    .line 119
    :cond_0
    check-cast p1, Lgbu;

    .line 120
    iget-object v0, p0, Lgbu;->a:Ljava/lang/String;

    iget-object v1, p1, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lgbu;->b:Ljava/lang/String;

    return-object v0
.end method
