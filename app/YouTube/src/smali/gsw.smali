.class public final Lgsw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgsx;


# instance fields
.field private final a:Lgsy;

.field private final b:Lgsy;

.field private final c:Lgsy;

.field private final d:F

.field private final e:F

.field private f:F

.field private g:F

.field private h:F

.field private synthetic i:Lgsv;


# direct methods
.method public constructor <init>(Lgsv;Lgsy;Lgsy;Lgsy;)V
    .locals 2

    .prologue
    .line 459
    iput-object p1, p0, Lgsw;->i:Lgsv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    const/4 v0, 0x0

    iput v0, p0, Lgsw;->h:F

    .line 460
    iput-object p2, p0, Lgsw;->a:Lgsy;

    .line 461
    iput-object p3, p0, Lgsw;->b:Lgsy;

    .line 462
    new-instance v0, Lgsy;

    invoke-direct {v0, p4}, Lgsy;-><init>(Lgsy;)V

    iput-object v0, p0, Lgsw;->c:Lgsy;

    .line 463
    iget-wide v0, p3, Lgsy;->b:J

    invoke-virtual {p2, v0, v1}, Lgsy;->a(J)F

    move-result v0

    iput v0, p0, Lgsw;->d:F

    .line 464
    iget-wide v0, p3, Lgsy;->c:J

    invoke-virtual {p2, v0, v1}, Lgsy;->a(J)F

    move-result v0

    iput v0, p0, Lgsw;->e:F

    .line 468
    iget v0, p0, Lgsw;->h:F

    invoke-virtual {p0, v0}, Lgsw;->a(F)V

    .line 469
    return-void
.end method


# virtual methods
.method public final a(J)F
    .locals 3

    .prologue
    .line 495
    iget-object v0, p0, Lgsw;->i:Lgsv;

    invoke-static {v0}, Lgsv;->c(Lgsv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lgsw;->c:Lgsy;

    invoke-virtual {v0, p1, p2}, Lgsy;->a(J)F

    move-result v0

    .line 504
    :goto_0
    return v0

    .line 499
    :cond_0
    iget-object v0, p0, Lgsw;->b:Lgsy;

    iget-wide v0, v0, Lgsy;->b:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 500
    iget-object v0, p0, Lgsw;->a:Lgsy;

    invoke-virtual {v0, p1, p2}, Lgsy;->a(J)F

    move-result v0

    iget v1, p0, Lgsw;->d:F

    iget v2, p0, Lgsw;->f:F

    sub-float/2addr v1, v2

    sub-float/2addr v0, v1

    goto :goto_0

    .line 501
    :cond_1
    iget-object v0, p0, Lgsw;->b:Lgsy;

    iget-wide v0, v0, Lgsy;->c:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    .line 502
    iget-object v0, p0, Lgsw;->a:Lgsy;

    invoke-virtual {v0, p1, p2}, Lgsy;->a(J)F

    move-result v0

    iget v1, p0, Lgsw;->g:F

    iget v2, p0, Lgsw;->e:F

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0

    .line 504
    :cond_2
    iget v0, p0, Lgsw;->f:F

    iget-object v1, p0, Lgsw;->b:Lgsy;

    invoke-virtual {v1, p1, p2}, Lgsy;->a(J)F

    move-result v1

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public final a(F)V
    .locals 4

    .prologue
    .line 478
    iput p1, p0, Lgsw;->h:F

    .line 479
    iget v0, p0, Lgsw;->d:F

    float-to-double v0, v0

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Lgsv;->a(DDF)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lgsw;->f:F

    .line 480
    iget v0, p0, Lgsw;->e:F

    float-to-double v0, v0

    iget-object v2, p0, Lgsw;->i:Lgsv;

    invoke-static {v2}, Lgsv;->a(Lgsv;)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3, p1}, Lgsv;->a(DDF)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lgsw;->g:F

    .line 481
    iget-object v0, p0, Lgsw;->b:Lgsy;

    iget v1, p0, Lgsw;->g:F

    iget v2, p0, Lgsw;->f:F

    sub-float/2addr v1, v2

    iput v1, v0, Lgsy;->a:F

    .line 482
    iget-object v0, p0, Lgsw;->i:Lgsv;

    invoke-static {v0}, Lgsv;->b(Lgsv;)V

    .line 483
    return-void
.end method

.method public final b(J)F
    .locals 1

    .prologue
    .line 535
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(F)J
    .locals 3

    .prologue
    .line 510
    iget-object v0, p0, Lgsw;->i:Lgsv;

    invoke-static {v0}, Lgsv;->c(Lgsv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lgsw;->c:Lgsy;

    invoke-virtual {v0, p1}, Lgsy;->b(F)J

    move-result-wide v0

    .line 519
    :goto_0
    return-wide v0

    .line 514
    :cond_0
    iget v0, p0, Lgsw;->f:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 515
    iget-object v0, p0, Lgsw;->a:Lgsy;

    iget v1, p0, Lgsw;->d:F

    iget v2, p0, Lgsw;->f:F

    sub-float/2addr v1, v2

    add-float/2addr v1, p1

    invoke-virtual {v0, v1}, Lgsy;->b(F)J

    move-result-wide v0

    goto :goto_0

    .line 516
    :cond_1
    iget v0, p0, Lgsw;->g:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    .line 517
    iget-object v0, p0, Lgsw;->a:Lgsy;

    iget v1, p0, Lgsw;->g:F

    iget v2, p0, Lgsw;->e:F

    sub-float/2addr v1, v2

    sub-float v1, p1, v1

    invoke-virtual {v0, v1}, Lgsy;->b(F)J

    move-result-wide v0

    goto :goto_0

    .line 519
    :cond_2
    iget-object v0, p0, Lgsw;->b:Lgsy;

    iget v1, p0, Lgsw;->f:F

    sub-float v1, p1, v1

    invoke-virtual {v0, v1}, Lgsy;->b(F)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final c(F)J
    .locals 1

    .prologue
    .line 530
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final d(F)J
    .locals 2

    .prologue
    .line 545
    iget-object v0, p0, Lgsw;->c:Lgsy;

    invoke-virtual {v0, p1}, Lgsy;->d(F)J

    move-result-wide v0

    return-wide v0
.end method
