.class public final Lehv;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/google/android/gms/cast/CastDevice;

.field b:Lehw;

.field private c:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/CastDevice;Lehw;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CastDevice parameter cannot be null"

    invoke-static {p1, v0}, Lb;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "CastListener parameter cannot be null"

    invoke-static {p2, v0}, Lb;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lehv;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object p2, p0, Lehv;->b:Lehw;

    const/4 v0, 0x0

    iput v0, p0, Lehv;->c:I

    return-void
.end method


# virtual methods
.method public final a()Lehu;
    .locals 1

    new-instance v0, Lehu;

    invoke-direct {v0, p0}, Lehu;-><init>(Lehv;)V

    return-object v0
.end method
