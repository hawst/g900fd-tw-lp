.class public final Lzx;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v1, Lilg;

    invoke-direct {v1, p1}, Lilg;-><init>(Ljava/nio/ByteBuffer;)V

    .line 50
    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lilg;->a(I)I

    move-result v2

    iput v2, p0, Lzx;->b:I

    .line 51
    invoke-virtual {v1, v3}, Lilg;->a(I)I

    move-result v2

    iput v2, p0, Lzx;->c:I

    .line 52
    invoke-virtual {v1, v3}, Lilg;->a(I)I

    move-result v2

    iput v2, p0, Lzx;->d:I

    .line 53
    invoke-virtual {v1, v3}, Lilg;->a(I)I

    move-result v2

    iput v2, p0, Lzx;->e:I

    .line 54
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lilg;->a(I)I

    move-result v2

    iput v2, p0, Lzx;->f:I

    .line 55
    invoke-virtual {v1, v0}, Lilg;->a(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lzx;->a:Z

    .line 56
    const/16 v0, 0x10

    invoke-virtual {v1, v0}, Lilg;->a(I)I

    move-result v0

    iput v0, p0, Lzx;->g:I

    .line 57
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x2

    .line 61
    new-instance v2, Lilh;

    invoke-direct {v2, p1}, Lilh;-><init>(Ljava/nio/ByteBuffer;)V

    .line 62
    iget v0, p0, Lzx;->b:I

    const/4 v3, 0x6

    invoke-virtual {v2, v0, v3}, Lilh;->a(II)V

    .line 63
    iget v0, p0, Lzx;->c:I

    invoke-virtual {v2, v0, v4}, Lilh;->a(II)V

    .line 64
    iget v0, p0, Lzx;->d:I

    invoke-virtual {v2, v0, v4}, Lilh;->a(II)V

    .line 65
    iget v0, p0, Lzx;->e:I

    invoke-virtual {v2, v0, v4}, Lilh;->a(II)V

    .line 66
    iget v0, p0, Lzx;->f:I

    const/4 v3, 0x3

    invoke-virtual {v2, v0, v3}, Lilh;->a(II)V

    .line 67
    iget-boolean v0, p0, Lzx;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0, v1}, Lilh;->a(II)V

    .line 68
    iget v0, p0, Lzx;->g:I

    const/16 v1, 0x10

    invoke-virtual {v2, v0, v1}, Lilh;->a(II)V

    .line 69
    return-void

    .line 67
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 180
    if-ne p0, p1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 183
    :cond_3
    check-cast p1, Lzx;

    .line 185
    iget v2, p0, Lzx;->b:I

    iget v3, p1, Lzx;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 186
    :cond_4
    iget v2, p0, Lzx;->g:I

    iget v3, p1, Lzx;->g:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 187
    :cond_5
    iget v2, p0, Lzx;->c:I

    iget v3, p1, Lzx;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 188
    :cond_6
    iget v2, p0, Lzx;->e:I

    iget v3, p1, Lzx;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    .line 189
    :cond_7
    iget v2, p0, Lzx;->d:I

    iget v3, p1, Lzx;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    .line 190
    :cond_8
    iget-boolean v2, p0, Lzx;->a:Z

    iget-boolean v3, p1, Lzx;->a:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    .line 191
    :cond_9
    iget v2, p0, Lzx;->f:I

    iget v3, p1, Lzx;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 198
    iget v0, p0, Lzx;->b:I

    .line 199
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lzx;->c:I

    add-int/2addr v0, v1

    .line 200
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lzx;->d:I

    add-int/2addr v0, v1

    .line 201
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lzx;->e:I

    add-int/2addr v0, v1

    .line 202
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lzx;->f:I

    add-int/2addr v0, v1

    .line 203
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lzx;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 204
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lzx;->g:I

    add-int/2addr v0, v1

    .line 205
    return v0

    .line 203
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 168
    const-string v0, "SampleFlags{reserved="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lzx;->b:I

    iget v2, p0, Lzx;->c:I

    iget v3, p0, Lzx;->e:I

    iget v4, p0, Lzx;->f:I

    iget-boolean v5, p0, Lzx;->a:Z

    iget v6, p0, Lzx;->g:I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit16 v8, v8, 0xb1

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sampleDependsOn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sampleHasRedundancy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", samplePaddingValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sampleIsDifferenceSample="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sampleDegradationPriority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
