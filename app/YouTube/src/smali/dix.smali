.class public final Ldix;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Legc;

.field public final b:Ljava/io/File;

.field public c:Ldiy;

.field public volatile d:Z

.field public final e:Lcyc;

.field private final f:Lewi;

.field private final g:Ljava/security/Key;

.field private final h:Lewi;

.field private final i:Ljava/lang/Object;

.field private final j:Ljava/lang/Object;

.field private volatile k:Z

.field private final l:Lezj;

.field private m:J

.field private n:J


# direct methods
.method public constructor <init>(Lewi;Legc;Ljava/io/File;Ljava/security/Key;Lewi;Lezj;Lcyc;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-boolean v0, p0, Ldix;->d:Z

    .line 73
    iput-boolean v0, p0, Ldix;->k:Z

    .line 98
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Ldix;->f:Lewi;

    .line 99
    iput-object p2, p0, Ldix;->a:Legc;

    .line 100
    iput-object p3, p0, Ldix;->b:Ljava/io/File;

    .line 101
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/Key;

    iput-object v0, p0, Ldix;->g:Ljava/security/Key;

    .line 102
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Ldix;->h:Lewi;

    .line 103
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Ldix;->l:Lezj;

    .line 104
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyc;

    iput-object v0, p0, Ldix;->e:Lcyc;

    .line 105
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldix;->i:Ljava/lang/Object;

    .line 106
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldix;->j:Ljava/lang/Object;

    .line 109
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Ldix;->m:J

    .line 110
    return-void
.end method

.method private a(Lefc;Lefg;Lfqj;)V
    .locals 12

    .prologue
    .line 227
    const/16 v0, 0x1000

    new-array v8, v0, [B

    .line 229
    const/4 v7, 0x0

    move-object v6, p2

    .line 230
    :goto_0
    if-nez v7, :cond_4

    .line 231
    sget-object v0, Lefv;->a:Lefv;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lefv;->c(I)V

    .line 234
    :try_start_0
    sget-object v0, Lefv;->a:Lefv;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lefv;->a(I)V

    .line 236
    iget-wide v2, v6, Lefg;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 238
    :try_start_1
    invoke-interface {p1, v6}, Lefc;->a(Lefg;)J

    .line 240
    :cond_0
    :goto_1
    iget-boolean v0, p0, Ldix;->d:Z

    if-nez v0, :cond_2

    const/4 v0, 0x0

    const/16 v1, 0x1000

    invoke-interface {p1, v8, v0, v1}, Lefc;->a([BII)I

    move-result v0

    if-ltz v0, :cond_2

    .line 242
    int-to-long v0, v0

    add-long/2addr v2, v0

    .line 243
    iget-wide v0, p0, Ldix;->m:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ldix;->l:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v10, 0x1e

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v10, v11, v5}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    iget-wide v10, p0, Ldix;->n:J

    sub-long/2addr v0, v10

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    new-instance v0, Ldjo;

    const-string v1, "Transfer timed out."

    invoke-direct {v0, v1}, Ldjo;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lefw; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 251
    :catch_0
    move-exception v0

    :try_start_2
    iget-wide v0, v6, Lefg;->d:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_5

    .line 252
    iget-wide v0, v6, Lefg;->e:J

    iget-wide v4, v6, Lefg;->d:J

    sub-long v4, v2, v4

    sub-long v4, v0, v4

    .line 253
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_3

    .line 254
    new-instance v0, Lefg;

    iget-object v1, v6, Lefg;->a:Landroid/net/Uri;

    iget-object v6, v6, Lefg;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    move v0, v7

    .line 266
    :goto_2
    :try_start_3
    invoke-interface {p1}, Lefc;->a()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 269
    :goto_3
    sget-object v2, Lefv;->a:Lefv;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lefv;->d(I)V

    move v7, v0

    move-object v6, v1

    .line 270
    goto :goto_0

    .line 243
    :cond_1
    :try_start_4
    iput-wide v2, p0, Ldix;->m:J

    iget-object v0, p0, Ldix;->l:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iput-wide v0, p0, Ldix;->n:J

    iget-object v0, p0, Ldix;->c:Ldiy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldix;->c:Ldiy;

    invoke-interface {v0, p3, v2, v3}, Ldiy;->a(Lfqj;J)V
    :try_end_4
    .catch Lefw; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 266
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-interface {p1}, Lefc;->a()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 269
    :catchall_1
    move-exception v0

    sget-object v1, Lefv;->a:Lefv;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lefv;->d(I)V

    throw v0

    .line 246
    :cond_2
    const/4 v0, 0x1

    .line 266
    :try_start_6
    invoke-interface {p1}, Lefc;->a()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-object v1, v6

    .line 267
    goto :goto_3

    .line 262
    :cond_3
    const/4 v0, 0x1

    move-object v1, v6

    goto :goto_2

    .line 272
    :cond_4
    return-void

    :cond_5
    move v0, v7

    move-object v1, v6

    goto :goto_2
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Ldix;->f:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legc;

    .line 310
    if-eqz v0, :cond_0

    .line 311
    invoke-interface {v0, p1}, Legc;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v1

    .line 312
    if-eqz v1, :cond_0

    .line 313
    invoke-interface {v1}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legi;

    .line 314
    invoke-interface {v0, v1}, Legc;->b(Legi;)V

    goto :goto_0

    .line 318
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lfqj;J)V
    .locals 6

    .prologue
    .line 200
    const-wide/16 v2, 0x0

    iget-object v0, p1, Lfqj;->a:Lhgy;

    iget v0, v0, Lhgy;->e:I

    int-to-long v0, v0

    mul-long/2addr v0, p2

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    const-wide/16 v4, 0x8

    div-long v4, v0, v4

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Ldix;->a(Lfqj;JJ)V

    .line 201
    return-void
.end method

.method public final a(Lfqj;JJ)V
    .locals 18

    .prologue
    .line 157
    move-object/from16 v0, p0

    iget-object v10, v0, Ldix;->i:Ljava/lang/Object;

    monitor-enter v10

    .line 158
    :try_start_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lfqj;->b:Ljava/lang/String;

    .line 159
    move-object/from16 v0, p1

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget-wide v4, v3, Lhgy;->j:J

    .line 160
    move-object/from16 v0, p1

    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->b:I

    invoke-static {v2, v3, v4, v5}, La;->a(Ljava/lang/String;IJ)Ljava/lang/String;

    move-result-object v11

    .line 161
    new-instance v4, Lefy;

    const/16 v3, 0xa

    move-object/from16 v0, p0

    iget-object v2, v0, Ldix;->h:Lewi;

    invoke-interface {v2}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lefc;

    invoke-direct {v4, v3, v2}, Lefy;-><init>(ILefc;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldix;->f:Lewi;

    invoke-interface {v2}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Legc;

    if-eqz v3, :cond_c

    new-instance v5, Lego;

    move-object/from16 v0, p0

    iget-object v2, v0, Ldix;->g:Ljava/security/Key;

    invoke-interface {v2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v2

    new-instance v6, Lefk;

    invoke-direct {v6}, Lefk;-><init>()V

    invoke-direct {v5, v2, v6}, Lego;-><init>([BLefc;)V

    new-instance v2, Legg;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Legg;-><init>(Legc;Lefc;Lefc;Lefb;ZZLegh;)V

    :goto_0
    new-instance v3, Legn;

    move-object/from16 v0, p0

    iget-object v4, v0, Ldix;->g:Ljava/security/Key;

    invoke-interface {v4}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    const/16 v5, 0x1000

    new-array v5, v5, [B

    new-instance v6, Lege;

    move-object/from16 v0, p0

    iget-object v7, v0, Ldix;->a:Legc;

    const-wide/32 v8, 0x500000

    invoke-direct {v6, v7, v8, v9}, Lege;-><init>(Legc;J)V

    invoke-direct {v3, v4, v5, v6}, Legn;-><init>([B[BLefb;)V

    new-instance v12, Lefz;

    invoke-direct {v12, v2, v3}, Lefz;-><init>(Lefc;Lefb;)V

    .line 163
    move-object/from16 v0, p1

    iget-object v3, v0, Lfqj;->d:Landroid/net/Uri;

    new-instance v13, Ljava/util/LinkedList;

    invoke-direct {v13}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldix;->a:Legc;

    invoke-interface {v2, v11}, Legc;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Ldix;->a:Legc;

    invoke-interface {v2, v11}, Legc;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/NavigableSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    new-instance v2, Lefg;

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    invoke-direct/range {v2 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :cond_1
    :goto_1
    const/4 v3, 0x0

    .line 166
    :try_start_1
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lefg;

    .line 167
    move-object/from16 v0, p0

    iget-object v5, v0, Ldix;->a:Legc;

    iget-object v6, v2, Lefg;->f:Ljava/lang/String;

    iget-wide v8, v2, Lefg;->d:J

    invoke-interface {v5, v6, v8, v9}, Legc;->a(Ljava/lang/String;J)Legi;

    move-result-object v3

    .line 168
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v12, v2, v1}, Ldix;->a(Lefc;Lefg;Lfqj;)V

    .line 172
    move-object/from16 v0, p0

    iget-object v5, v0, Ldix;->a:Legc;

    invoke-interface {v5, v3}, Legc;->a(Legi;)V

    .line 173
    const/4 v3, 0x0

    .line 175
    move-object/from16 v0, p0

    iget-boolean v5, v0, Ldix;->d:Z

    if-eqz v5, :cond_2

    .line 176
    move-object/from16 v0, p0

    iget-boolean v4, v0, Ldix;->k:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v4, :cond_3

    .line 177
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Ldix;->a:Legc;

    iget-object v5, v2, Lefg;->f:Ljava/lang/String;

    iget-wide v6, v2, Lefg;->d:J

    invoke-interface {v4, v5, v6, v7}, Legc;->a(Ljava/lang/String;J)Legi;

    move-result-object v2

    iget-boolean v4, v2, Legi;->d:Z

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Ldix;->a:Legc;

    invoke-interface {v4, v2}, Legc;->b(Legi;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 184
    :cond_3
    :goto_2
    :try_start_3
    monitor-exit v10

    .line 188
    :goto_3
    return-void

    .line 163
    :cond_4
    new-instance v14, Ljava/util/TreeSet;

    invoke-direct {v14}, Ljava/util/TreeSet;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Ldix;->a:Legc;

    invoke-interface {v2, v11}, Legc;->a(Ljava/lang/String;)Ljava/util/NavigableSet;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    move-wide/from16 v4, p2

    :cond_5
    :goto_4
    invoke-virtual {v14}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v14}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Legi;

    move-object v9, v0

    invoke-virtual {v14, v9}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    iget-wide v6, v9, Legi;->b:J

    cmp-long v2, v6, v4

    if-nez v2, :cond_6

    iget-wide v6, v9, Legi;->c:J

    add-long/2addr v4, v6

    goto :goto_4

    :cond_6
    iget-wide v6, v9, Legi;->b:J

    cmp-long v2, v6, v4

    if-gez v2, :cond_7

    iget-wide v6, v9, Legi;->b:J

    iget-wide v0, v9, Legi;->c:J

    move-wide/from16 v16, v0

    add-long v6, v6, v16

    cmp-long v2, v6, v4

    if-lez v2, :cond_5

    iget-wide v4, v9, Legi;->b:J

    iget-wide v6, v9, Legi;->c:J

    add-long/2addr v4, v6

    goto :goto_4

    :cond_7
    iget-wide v6, v9, Legi;->b:J

    cmp-long v2, v6, v4

    if-lez v2, :cond_5

    new-instance v2, Lefg;

    iget-wide v6, v9, Legi;->b:J

    sub-long/2addr v6, v4

    move-object v8, v11

    invoke-direct/range {v2 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-wide v4, v9, Legi;->b:J

    iget-wide v6, v9, Legi;->c:J

    add-long/2addr v4, v6

    goto :goto_4

    :cond_8
    cmp-long v2, v4, p4

    if-gez v2, :cond_1

    new-instance v2, Lefg;

    sub-long v6, p4, v4

    move-object v8, v11

    invoke-direct/range {v2 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 188
    :catchall_0
    move-exception v2

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 177
    :cond_9
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Ldix;->a:Legc;

    invoke-interface {v4, v2}, Legc;->a(Legi;)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v2

    goto :goto_2

    .line 182
    :cond_a
    :try_start_5
    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ldix;->a(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 188
    :try_start_6
    monitor-exit v10

    goto/16 :goto_3

    .line 185
    :catchall_1
    move-exception v2

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Ldix;->a:Legc;

    invoke-interface {v4, v3}, Legc;->a(Legi;)V

    :cond_b
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_c
    move-object v2, v4

    goto/16 :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 113
    iget-object v1, p0, Ldix;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Ldix;->d:Z

    .line 115
    iput-boolean p1, p0, Ldix;->k:Z

    .line 116
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
