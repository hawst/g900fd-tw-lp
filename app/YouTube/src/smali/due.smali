.class final Ldue;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lejt;


# instance fields
.field private synthetic a:Lejr;

.field private synthetic b:Ldub;


# direct methods
.method constructor <init>(Ldub;Lejr;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Ldue;->b:Ldub;

    iput-object p2, p0, Ldue;->a:Lejr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 360
    iget-object v0, p0, Ldue;->b:Ldub;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onConnectionSuspended: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldub;->a(Ljava/lang/String;)V

    .line 361
    iget-object v0, p0, Ldue;->b:Ldub;

    const/4 v1, 0x0

    iput-object v1, v0, Ldub;->f:Lejr;

    .line 362
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 343
    iget-object v1, p0, Ldue;->b:Ldub;

    iget-object v1, v1, Ldub;->i:Ldum;

    sget-object v2, Ldum;->d:Ldum;

    if-ne v1, v2, :cond_0

    .line 344
    iget-object v0, p0, Ldue;->b:Ldub;

    const-string v1, "connectionCallback: no longer connected"

    invoke-virtual {v0, v1}, Ldub;->a(Ljava/lang/String;)V

    .line 356
    :goto_0
    return-void

    .line 349
    :cond_0
    :try_start_0
    iget-object v1, p0, Ldue;->b:Ldub;

    const-string v2, "onConnected"

    invoke-virtual {v1, v2}, Ldub;->a(Ljava/lang/String;)V

    .line 350
    iget-object v1, p0, Ldue;->b:Ldub;

    iget-object v2, p0, Ldue;->a:Lejr;

    iput-object v2, v1, Ldub;->f:Lejr;

    .line 351
    iget-object v2, p0, Ldue;->b:Ldub;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v1, "setup message listener"

    invoke-virtual {v2, v1}, Ldub;->a(Ljava/lang/String;)V

    iget-object v1, v2, Ldub;->i:Ldum;

    sget-object v3, Ldum;->c:Ldum;

    if-eq v1, v3, :cond_1

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    sget-object v0, Lehm;->c:Lehp;

    iget-object v3, v2, Ldub;->f:Lejr;

    const-string v4, "urn:x-cast:com.google.youtube.mdx"

    new-instance v5, Ldui;

    invoke-direct {v5, v2}, Ldui;-><init>(Ldub;)V

    invoke-virtual {v0, v3, v4, v5}, Lehp;->a(Lejr;Ljava/lang/String;Lehx;)V

    const-string v3, "launch application "

    iget-object v0, v2, Ldub;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Ldub;->a(Ljava/lang/String;)V

    sget-object v0, Lehm;->c:Lehp;

    iget-object v3, v2, Ldub;->f:Lejr;

    iget-object v4, v2, Ldub;->d:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Lehp;->a(Lejr;Ljava/lang/String;Z)Leju;

    move-result-object v0

    new-instance v3, Ldug;

    invoke-direct {v3, v2, v1}, Ldug;-><init>(Ldub;Z)V

    invoke-interface {v0, v3}, Leju;->a(Lejx;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "can\'t set Cast message listener"

    invoke-virtual {v2, v0}, Ldub;->a(Ljava/lang/String;)V

    invoke-virtual {v2}, Ldub;->c()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 352
    :catch_1
    move-exception v0

    .line 353
    iget-object v1, p0, Ldue;->b:Ldub;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to launch application "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ldub;->a(Ljava/lang/String;)V

    .line 354
    iget-object v0, p0, Ldue;->b:Ldub;

    invoke-virtual {v0}, Ldub;->c()V

    goto/16 :goto_0

    :cond_1
    move v1, v0

    .line 351
    goto :goto_1

    :cond_2
    :try_start_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2
.end method
