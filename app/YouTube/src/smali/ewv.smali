.class public abstract enum Lewv;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lewv;

.field public static final enum b:Lewv;

.field public static final enum c:Lewv;

.field public static final enum d:Lewv;

.field public static final enum e:Lewv;

.field public static final enum f:Lewv;

.field private static final synthetic g:[Lewv;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Leww;

    const-string v1, "HEAD"

    invoke-direct {v0, v1, v3}, Leww;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lewv;->a:Lewv;

    .line 26
    new-instance v0, Lewx;

    const-string v1, "GET"

    invoke-direct {v0, v1, v4}, Lewx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lewv;->b:Lewv;

    .line 32
    new-instance v0, Lewy;

    const-string v1, "POST"

    invoke-direct {v0, v1, v5}, Lewy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lewv;->c:Lewv;

    .line 38
    new-instance v0, Lewz;

    const-string v1, "PUT"

    invoke-direct {v0, v1, v6}, Lewz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lewv;->d:Lewv;

    .line 44
    new-instance v0, Lexa;

    const-string v1, "PATCH"

    invoke-direct {v0, v1, v7}, Lexa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lewv;->e:Lewv;

    .line 50
    new-instance v0, Lexb;

    const-string v1, "DELETE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lexb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lewv;->f:Lewv;

    .line 19
    const/4 v0, 0x6

    new-array v0, v0, [Lewv;

    sget-object v1, Lewv;->a:Lewv;

    aput-object v1, v0, v3

    sget-object v1, Lewv;->b:Lewv;

    aput-object v1, v0, v4

    sget-object v1, Lewv;->c:Lewv;

    aput-object v1, v0, v5

    sget-object v1, Lewv;->d:Lewv;

    aput-object v1, v0, v6

    sget-object v1, Lewv;->e:Lewv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lewv;->f:Lewv;

    aput-object v2, v0, v1

    sput-object v0, Lewv;->g:[Lewv;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lewv;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lewv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lewv;

    return-object v0
.end method

.method public static values()[Lewv;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lewv;->g:[Lewv;

    invoke-virtual {v0}, [Lewv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lewv;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;
.end method
