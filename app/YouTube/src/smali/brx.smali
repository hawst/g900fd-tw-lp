.class public final Lbrx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbxy;


# instance fields
.field final a:Lbxu;

.field final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private final d:Landroid/view/ViewGroup;

.field private final e:Landroid/view/View;

.field private f:Lcom/google/android/apps/youtube/app/ui/TutorialView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbxu;Landroid/content/SharedPreferences;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lbrx;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 36
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxu;

    iput-object v0, p0, Lbrx;->a:Lbxu;

    .line 37
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lbrx;->b:Landroid/content/SharedPreferences;

    .line 38
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lbrx;->d:Landroid/view/ViewGroup;

    .line 39
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbrx;->e:Landroid/view/View;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 44
    const/16 v0, 0x1388

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lbrx;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbrx;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 50
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbrx;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 51
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->e:Lbqv;

    invoke-virtual {v0}, Lbqv;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbrx;->e:Landroid/view/View;

    .line 52
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lbrx;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 59
    const v1, 0x7f04011a

    iget-object v2, p0, Lbrx;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 60
    const v1, 0x7f080309

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iput-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    .line 61
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    new-instance v1, Lbry;

    invoke-direct {v1, p0}, Lbry;-><init>(Lbrx;)V

    iput-object v1, v0, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a:Lbxt;

    .line 70
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lbrx;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f090283

    .line 71
    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a(I)V

    .line 75
    :cond_0
    iget-object v0, p0, Lbrx;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-gez v0, :cond_1

    .line 76
    iget-object v0, p0, Lbrx;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 78
    :cond_1
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iget-object v1, p0, Lbrx;->d:Landroid/view/ViewGroup;

    iget-object v2, p0, Lbrx;->e:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a(Landroid/view/View;Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a()V

    .line 80
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lbrx;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lbrx;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->b()V

    .line 88
    :cond_0
    return-void
.end method
