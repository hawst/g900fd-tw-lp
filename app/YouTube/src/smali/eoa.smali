.class public Leoa;
.super Lenk;


# static fields
.field private static final e:Ljava/lang/String;

.field private static final f:J

.field private static final g:J

.field private static final h:J

.field private static final i:J


# instance fields
.field public final d:Ljava/util/List;

.field private j:Leig;

.field private final k:Landroid/os/Handler;

.field private final l:Leoe;

.field private final m:Leoe;

.field private final n:Leoe;

.field private final o:Leoe;

.field private final p:Leoe;

.field private final q:Leoe;

.field private final r:Leoe;

.field private final s:Leoe;

.field private final t:Leoe;

.field private final u:Leoe;

.field private final v:Ljava/lang/Runnable;

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-wide/16 v4, 0x18

    const-string v0, "com.google.cast.media"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "urn:x-cast:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Leoa;->e:Ljava/lang/String;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Leoa;->f:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Leoa;->g:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Leoa;->h:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Leoa;->i:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Leoa;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 4

    sget-object v0, Leoa;->e:Ljava/lang/String;

    const-string v1, "MediaControlChannel"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lenk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Leoa;->k:Landroid/os/Handler;

    new-instance v0, Leob;

    invoke-direct {v0, p0}, Leob;-><init>(Leoa;)V

    iput-object v0, p0, Leoa;->v:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leoa;->d:Ljava/util/List;

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->g:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->l:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->l:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->m:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->m:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->n:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->n:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->o:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->o:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->h:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->p:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->p:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->q:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->q:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->r:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->r:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->s:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->s:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->t:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->t:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Leoe;

    sget-wide v2, Leoa;->f:J

    invoke-direct {v0, v2, v3}, Leoe;-><init>(J)V

    iput-object v0, p0, Leoa;->u:Leoe;

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    iget-object v1, p0, Leoa;->u:Leoe;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Leoa;->f()V

    return-void
.end method

.method static synthetic a(Leoa;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    return-object v0
.end method

.method private a(JLorg/json/JSONObject;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Leoa;->l:Leoe;

    invoke-virtual {v0, p1, p2}, Leoe;->a(J)Z

    move-result v3

    iget-object v0, p0, Leoa;->p:Leoe;

    invoke-virtual {v0}, Leoe;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Leoa;->p:Leoe;

    invoke-virtual {v0, p1, p2}, Leoe;->a(J)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    iget-object v4, p0, Leoa;->q:Leoe;

    invoke-virtual {v4}, Leoe;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Leoa;->q:Leoe;

    invoke-virtual {v4, p1, p2}, Leoe;->a(J)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Leoa;->r:Leoe;

    invoke-virtual {v4}, Leoe;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Leoa;->r:Leoe;

    invoke-virtual {v4, p1, p2}, Leoe;->a(J)Z

    move-result v4

    if-nez v4, :cond_8

    :cond_1
    :goto_1
    if-eqz v0, :cond_b

    const/4 v0, 0x2

    :goto_2
    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x1

    :cond_2
    if-nez v3, :cond_3

    iget-object v1, p0, Leoa;->j:Leig;

    if-nez v1, :cond_9

    :cond_3
    new-instance v0, Leig;

    invoke-direct {v0, p3}, Leig;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Leoa;->j:Leig;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    const/4 v0, 0x7

    :goto_3
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-virtual {p0}, Leoa;->a()V

    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    invoke-virtual {p0}, Leoa;->a()V

    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Leoa;->b()V

    :cond_6
    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leoe;

    const/4 v3, 0x0

    invoke-virtual {v0, p1, p2, v2, v3}, Leoe;->a(JILorg/json/JSONObject;)Z

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v1, v2

    goto :goto_1

    :cond_9
    iget-object v1, p0, Leoa;->j:Leig;

    invoke-virtual {v1, p3, v0}, Leig;->a(Lorg/json/JSONObject;I)I

    move-result v0

    goto :goto_3

    :cond_a
    return-void

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method private a(Z)V
    .locals 4

    iget-boolean v0, p0, Leoa;->w:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Leoa;->w:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Leoa;->k:Landroid/os/Handler;

    iget-object v1, p0, Leoa;->v:Ljava/lang/Runnable;

    sget-wide v2, Leoa;->i:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Leoa;->k:Landroid/os/Handler;

    iget-object v1, p0, Leoa;->v:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Leoa;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Leoa;->w:Z

    return v0
.end method

.method static synthetic b(Leoa;Z)V
    .locals 0

    invoke-direct {p0, p1}, Leoa;->a(Z)V

    return-void
.end method

.method private e()J
    .locals 2

    iget-object v0, p0, Leoa;->j:Leig;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No current media session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Leoa;->j:Leig;

    iget-wide v0, v0, Leig;->a:J

    return-wide v0
.end method

.method private f()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Leoa;->a(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Leoa;->j:Leig;

    iget-object v0, p0, Leoa;->l:Leoe;

    invoke-virtual {v0}, Leoe;->a()V

    iget-object v0, p0, Leoa;->p:Leoe;

    invoke-virtual {v0}, Leoe;->a()V

    iget-object v0, p0, Leoa;->q:Leoe;

    invoke-virtual {v0}, Leoe;->a()V

    return-void
.end method


# virtual methods
.method public final a(Leod;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Leoa;->c()J

    move-result-wide v2

    iget-object v1, p0, Leoa;->s:Leoe;

    invoke-virtual {v1, v2, v3, p1}, Leoe;->a(JLeod;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Leoa;->a(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "GET_STATUS"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v1, p0, Leoa;->j:Leig;

    if-eqz v1, :cond_0

    const-string v1, "mediaSessionId"

    iget-object v4, p0, Leoa;->j:Leig;

    iget-wide v4, v4, Leig;->a:J

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Leoa;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Leod;JILorg/json/JSONObject;)J
    .locals 10

    const/4 v8, 0x1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Leoa;->c()J

    move-result-wide v2

    iget-object v1, p0, Leoa;->p:Leoe;

    invoke-virtual {v1, v2, v3, p1}, Leoe;->a(JLeod;)V

    invoke-direct {p0, v8}, Leoa;->a(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "SEEK"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mediaSessionId"

    invoke-direct {p0}, Leoa;->e()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "currentTime"

    long-to-double v4, p2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    if-ne p4, v8, :cond_2

    const-string v1, "resumeState"

    const-string v4, "PLAYBACK_START"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    :goto_0
    if-eqz p5, :cond_1

    const-string v1, "customData"

    invoke-virtual {v0, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Leoa;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :cond_2
    const/4 v1, 0x2

    if-ne p4, v1, :cond_0

    :try_start_1
    const-string v1, "resumeState"

    const-string v4, "PLAYBACK_PAUSE"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public final a(Leod;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Leoa;->c()J

    move-result-wide v2

    iget-object v1, p0, Leoa;->m:Leoe;

    invoke-virtual {v1, v2, v3, p1}, Leoe;->a(JLeod;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Leoa;->a(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "PAUSE"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mediaSessionId"

    invoke-direct {p0}, Leoa;->e()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Leoa;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Leoa;->a:Lenz;

    const-string v1, "message received: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-virtual {v0, v1, v2}, Lenz;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "requestId"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "MEDIA_STATUS"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v2, v3, v0}, Leoa;->a(JLorg/json/JSONObject;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Leoa;->j:Leig;

    invoke-virtual {p0}, Leoa;->a()V

    invoke-virtual {p0}, Leoa;->b()V

    iget-object v0, p0, Leoa;->s:Leoe;

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, Leoe;->a(JILorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Leoa;->a:Lenz;

    const-string v2, "Message is malformed (%s); ignoring: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    aput-object p1, v3, v7

    invoke-virtual {v1, v2, v3}, Lenz;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v4, "INVALID_PLAYER_STATE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v1, p0, Leoa;->a:Lenz;

    const-string v4, "received unexpected error: Invalid Player State."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lenz;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leoe;

    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Leoe;->a(JILorg/json/JSONObject;)Z

    goto :goto_1

    :cond_3
    const-string v4, "LOAD_FAILED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Leoa;->l:Leoe;

    const/16 v4, 0x834

    invoke-virtual {v1, v2, v3, v4, v0}, Leoe;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    :cond_4
    const-string v4, "LOAD_CANCELLED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Leoa;->l:Leoe;

    const/16 v4, 0x835

    invoke-virtual {v1, v2, v3, v4, v0}, Leoe;->a(JILorg/json/JSONObject;)Z

    goto/16 :goto_0

    :cond_5
    const-string v4, "INVALID_REQUEST"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Leoa;->a:Lenz;

    const-string v4, "received unexpected error: Invalid Request."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lenz;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iget-object v0, p0, Leoa;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leoe;

    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Leoe;->a(JILorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public final b(Leod;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Leoa;->c()J

    move-result-wide v2

    iget-object v1, p0, Leoa;->n:Leoe;

    invoke-virtual {v1, v2, v3, p1}, Leoe;->a(JLeod;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Leoa;->a(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "PLAY"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mediaSessionId"

    invoke-direct {p0}, Leoa;->e()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Leoa;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 0

    invoke-direct {p0}, Leoa;->f()V

    return-void
.end method
