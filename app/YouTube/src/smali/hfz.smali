.class public final Lhfz;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[B

.field public c:Ljava/lang/String;

.field public d:Lhiq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47929
    invoke-direct {p0}, Lidf;-><init>()V

    .line 47932
    const-string v0, ""

    iput-object v0, p0, Lhfz;->a:Ljava/lang/String;

    .line 47935
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhfz;->b:[B

    .line 47938
    const-string v0, ""

    iput-object v0, p0, Lhfz;->c:Ljava/lang/String;

    .line 47941
    const/4 v0, 0x0

    iput-object v0, p0, Lhfz;->d:Lhiq;

    .line 47929
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 48001
    const/4 v0, 0x0

    .line 48002
    iget-object v1, p0, Lhfz;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48003
    const/4 v0, 0x1

    iget-object v1, p0, Lhfz;->a:Ljava/lang/String;

    .line 48004
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 48006
    :cond_0
    iget-object v1, p0, Lhfz;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 48007
    const/4 v1, 0x3

    iget-object v2, p0, Lhfz;->b:[B

    .line 48008
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 48010
    :cond_1
    iget-object v1, p0, Lhfz;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 48011
    const/4 v1, 0x4

    iget-object v2, p0, Lhfz;->c:Ljava/lang/String;

    .line 48012
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48014
    :cond_2
    iget-object v1, p0, Lhfz;->d:Lhiq;

    if-eqz v1, :cond_3

    .line 48015
    const/4 v1, 0x5

    iget-object v2, p0, Lhfz;->d:Lhiq;

    .line 48016
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48018
    :cond_3
    iget-object v1, p0, Lhfz;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 48019
    iput v0, p0, Lhfz;->J:I

    .line 48020
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 47925
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfz;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfz;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfz;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfz;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhfz;->b:[B

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfz;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhfz;->d:Lhiq;

    if-nez v0, :cond_2

    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    iput-object v0, p0, Lhfz;->d:Lhiq;

    :cond_2
    iget-object v0, p0, Lhfz;->d:Lhiq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 47983
    iget-object v0, p0, Lhfz;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47984
    const/4 v0, 0x1

    iget-object v1, p0, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 47986
    :cond_0
    iget-object v0, p0, Lhfz;->b:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47987
    const/4 v0, 0x3

    iget-object v1, p0, Lhfz;->b:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 47989
    :cond_1
    iget-object v0, p0, Lhfz;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 47990
    const/4 v0, 0x4

    iget-object v1, p0, Lhfz;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 47992
    :cond_2
    iget-object v0, p0, Lhfz;->d:Lhiq;

    if-eqz v0, :cond_3

    .line 47993
    const/4 v0, 0x5

    iget-object v1, p0, Lhfz;->d:Lhiq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 47995
    :cond_3
    iget-object v0, p0, Lhfz;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 47997
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 47955
    if-ne p1, p0, :cond_1

    .line 47962
    :cond_0
    :goto_0
    return v0

    .line 47956
    :cond_1
    instance-of v2, p1, Lhfz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 47957
    :cond_2
    check-cast p1, Lhfz;

    .line 47958
    iget-object v2, p0, Lhfz;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfz;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhfz;->b:[B

    iget-object v3, p1, Lhfz;->b:[B

    .line 47959
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhfz;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhfz;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 47960
    :goto_2
    iget-object v2, p0, Lhfz;->d:Lhiq;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhfz;->d:Lhiq;

    if-nez v2, :cond_3

    .line 47961
    :goto_3
    iget-object v2, p0, Lhfz;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhfz;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 47962
    goto :goto_0

    .line 47958
    :cond_4
    iget-object v2, p0, Lhfz;->a:Ljava/lang/String;

    iget-object v3, p1, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 47959
    :cond_5
    iget-object v2, p0, Lhfz;->c:Ljava/lang/String;

    iget-object v3, p1, Lhfz;->c:Ljava/lang/String;

    .line 47960
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhfz;->d:Lhiq;

    iget-object v3, p1, Lhfz;->d:Lhiq;

    .line 47961
    invoke-virtual {v2, v3}, Lhiq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhfz;->I:Ljava/util/List;

    iget-object v3, p1, Lhfz;->I:Ljava/util/List;

    .line 47962
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47966
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 47968
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfz;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 47969
    iget-object v2, p0, Lhfz;->b:[B

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 47975
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhfz;->c:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 47976
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfz;->d:Lhiq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 47977
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhfz;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_3
    add-int/2addr v0, v1

    .line 47978
    return v0

    .line 47968
    :cond_1
    iget-object v0, p0, Lhfz;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    .line 47971
    :goto_4
    iget-object v3, p0, Lhfz;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 47972
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhfz;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 47971
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 47975
    :cond_3
    iget-object v0, p0, Lhfz;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 47976
    :cond_4
    iget-object v0, p0, Lhfz;->d:Lhiq;

    invoke-virtual {v0}, Lhiq;->hashCode()I

    move-result v0

    goto :goto_2

    .line 47977
    :cond_5
    iget-object v1, p0, Lhfz;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
