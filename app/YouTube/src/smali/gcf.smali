.class public final Lgcf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgjg;
.implements Ljava/io/Serializable;


# instance fields
.field public A:Z

.field public B:Ljava/util/Set;

.field public C:Z

.field public D:Z

.field public E:Landroid/net/Uri;

.field public F:Z

.field public G:Ljava/lang/String;

.field public H:Z

.field public I:Ljava/lang/String;

.field private J:Lgcg;

.field private K:Lgch;

.field public a:Ljava/lang/String;

.field public b:Ljava/util/Set;

.field public c:Landroid/net/Uri;

.field public d:Landroid/net/Uri;

.field public e:Landroid/net/Uri;

.field public f:Landroid/net/Uri;

.field public g:Landroid/net/Uri;

.field public h:Landroid/net/Uri;

.field public i:Landroid/net/Uri;

.field public j:Ljava/lang/String;

.field public k:I

.field public l:J

.field public m:J

.field public n:J

.field public o:Ljava/lang/String;

.field public p:Landroid/net/Uri;

.field public q:Ljava/util/Date;

.field public r:Ljava/util/Date;

.field public s:Ljava/lang/String;

.field public t:Ljava/lang/String;

.field public u:Ljava/lang/String;

.field public v:Ljava/lang/String;

.field public w:Ljava/util/Map;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461
    sget-object v0, Lgch;->a:Lgch;

    iput-object v0, p0, Lgcf;->K:Lgch;

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 776
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->a:Ljava/lang/String;

    .line 777
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lgcf;->b:Ljava/util/Set;

    .line 778
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->c:Landroid/net/Uri;

    .line 779
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->d:Landroid/net/Uri;

    .line 780
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->e:Landroid/net/Uri;

    .line 781
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->f:Landroid/net/Uri;

    .line 782
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->g:Landroid/net/Uri;

    .line 783
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->h:Landroid/net/Uri;

    .line 784
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->i:Landroid/net/Uri;

    .line 785
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->j:Ljava/lang/String;

    .line 786
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgcf;->k:I

    .line 787
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgcf;->l:J

    .line 788
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgcf;->m:J

    .line 789
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgcf;->n:J

    .line 790
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->o:Ljava/lang/String;

    .line 791
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->p:Landroid/net/Uri;

    .line 792
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lgcf;->q:Ljava/util/Date;

    .line 793
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lgcf;->r:Ljava/util/Date;

    .line 794
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->s:Ljava/lang/String;

    .line 795
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->t:Ljava/lang/String;

    .line 796
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->u:Ljava/lang/String;

    .line 797
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->v:Ljava/lang/String;

    .line 798
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcg;

    iput-object v0, p0, Lgcf;->J:Lgcg;

    .line 799
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lgcf;->w:Ljava/util/Map;

    .line 800
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->x:Ljava/lang/String;

    .line 801
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->y:Ljava/lang/String;

    .line 802
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcf;->z:Z

    .line 803
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgch;

    iput-object v0, p0, Lgcf;->K:Lgch;

    .line 804
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcf;->A:Z

    .line 805
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, p0, Lgcf;->B:Ljava/util/Set;

    .line 806
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcf;->C:Z

    .line 807
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcf;->D:Z

    .line 809
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgcf;->E:Landroid/net/Uri;

    .line 810
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcf;->F:Z

    .line 811
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->G:Ljava/lang/String;

    .line 813
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lgcf;->H:Z

    .line 814
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgcf;->I:Ljava/lang/String;

    .line 815
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 729
    invoke-virtual {p0}, Lgcf;->a()Lgcd;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 733
    iget-object v0, p0, Lgcf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 734
    iget-object v0, p0, Lgcf;->b:Ljava/util/Set;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 735
    iget-object v0, p0, Lgcf;->c:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 736
    iget-object v0, p0, Lgcf;->d:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 737
    iget-object v0, p0, Lgcf;->e:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 738
    iget-object v0, p0, Lgcf;->f:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 739
    iget-object v0, p0, Lgcf;->g:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 740
    iget-object v0, p0, Lgcf;->h:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 741
    iget-object v0, p0, Lgcf;->i:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 742
    iget-object v0, p0, Lgcf;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 743
    iget v0, p0, Lgcf;->k:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 744
    iget-wide v0, p0, Lgcf;->l:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 745
    iget-wide v0, p0, Lgcf;->m:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 746
    iget-wide v0, p0, Lgcf;->n:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 747
    iget-object v0, p0, Lgcf;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 748
    iget-object v0, p0, Lgcf;->p:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 749
    iget-object v0, p0, Lgcf;->q:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 750
    iget-object v0, p0, Lgcf;->r:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 751
    iget-object v0, p0, Lgcf;->s:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 752
    iget-object v0, p0, Lgcf;->t:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 753
    iget-object v0, p0, Lgcf;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 754
    iget-object v0, p0, Lgcf;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 755
    iget-object v0, p0, Lgcf;->J:Lgcg;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 756
    iget-object v0, p0, Lgcf;->w:Ljava/util/Map;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 757
    iget-object v0, p0, Lgcf;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 758
    iget-object v0, p0, Lgcf;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 759
    iget-boolean v0, p0, Lgcf;->z:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 760
    iget-object v0, p0, Lgcf;->K:Lgch;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 761
    iget-boolean v0, p0, Lgcf;->A:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 762
    iget-object v0, p0, Lgcf;->B:Ljava/util/Set;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 763
    iget-boolean v0, p0, Lgcf;->C:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 764
    iget-boolean v0, p0, Lgcf;->D:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 766
    iget-object v0, p0, Lgcf;->E:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 767
    iget-boolean v0, p0, Lgcf;->F:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 768
    iget-object v0, p0, Lgcf;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 770
    iget-boolean v0, p0, Lgcf;->H:Z

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeBoolean(Z)V

    .line 771
    iget-object v0, p0, Lgcf;->I:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 772
    return-void
.end method


# virtual methods
.method public final a()Lgcd;
    .locals 43

    .prologue
    .line 479
    move-object/from16 v0, p0

    iget-object v2, v0, Lgcf;->b:Ljava/util/Set;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lgcf;->b:Ljava/util/Set;

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lgcf;->b:Ljava/util/Set;

    .line 480
    new-instance v2, Lgcd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lgcf;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lgcf;->b:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v5, v0, Lgcf;->c:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lgcf;->d:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v7, v0, Lgcf;->e:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v8, v0, Lgcf;->f:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v9, v0, Lgcf;->g:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v10, v0, Lgcf;->h:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v11, v0, Lgcf;->i:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v12, v0, Lgcf;->j:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v13, v0, Lgcf;->k:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Lgcf;->l:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lgcf;->m:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lgcf;->n:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->o:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->p:Landroid/net/Uri;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->q:Ljava/util/Date;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->r:Ljava/util/Date;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->s:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->t:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->u:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->v:Ljava/lang/String;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->J:Lgcg;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->w:Ljava/util/Map;

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->x:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->y:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcf;->z:Z

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->K:Lgch;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcf;->A:Z

    move/from16 v34, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->B:Ljava/util/Set;

    move-object/from16 v35, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcf;->C:Z

    move/from16 v36, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcf;->D:Z

    move/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->E:Landroid/net/Uri;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcf;->F:Z

    move/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->G:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lgcf;->H:Z

    move/from16 v41, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lgcf;->I:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-direct/range {v2 .. v42}, Lgcd;-><init>(Ljava/lang/String;Ljava/util/Set;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;IJJJLjava/lang/String;Landroid/net/Uri;Ljava/util/Date;Ljava/util/Date;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgcg;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;ZLgch;ZLjava/util/Set;ZZLandroid/net/Uri;ZLjava/lang/String;ZLjava/lang/String;)V

    return-object v2

    .line 479
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    goto/16 :goto_0
.end method

.method public final a(Lgcg;)Lgcf;
    .locals 2

    .prologue
    .line 644
    iget-object v0, p0, Lgcf;->J:Lgcg;

    sget-object v1, Lgcg;->c:Lgcg;

    if-eq v0, v1, :cond_0

    .line 645
    iput-object p1, p0, Lgcf;->J:Lgcg;

    .line 647
    :cond_0
    return-object p0
.end method

.method public final a(Lgch;)Lgcf;
    .locals 1

    .prologue
    .line 679
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgch;

    iput-object v0, p0, Lgcf;->K:Lgch;

    .line 680
    return-object p0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0}, Lgcf;->a()Lgcd;

    move-result-object v0

    return-object v0
.end method
