.class public final Lexz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Leyg;


# instance fields
.field public final b:Levk;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:Leyg;

.field public final e:Ljava/util/concurrent/Executor;

.field private final f:Ljava/util/Map;

.field private final g:Lezj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Leya;

    invoke-direct {v0}, Leya;-><init>()V

    sput-object v0, Lexz;->a:Leyg;

    return-void
.end method

.method public constructor <init>(Levk;Ljava/util/concurrent/ScheduledExecutorService;Lezj;Leyg;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levk;

    iput-object v0, p0, Lexz;->b:Levk;

    .line 63
    iput-object p2, p0, Lexz;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 64
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lexz;->g:Lezj;

    .line 65
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyg;

    iput-object v0, p0, Lexz;->d:Leyg;

    .line 66
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lexz;->e:Ljava/util/concurrent/Executor;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lexz;->f:Ljava/util/Map;

    .line 69
    return-void
.end method

.method static synthetic a(Lexz;)Levk;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lexz;->b:Levk;

    return-object v0
.end method

.method static synthetic a(Lexz;Lead;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lexz;->d(Lead;)V

    return-void
.end method

.method private a(Ljava/util/List;J)V
    .locals 8

    .prologue
    .line 293
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lead;

    .line 294
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Updating task %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Lead;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    .line 295
    iget-wide v2, v0, Lead;->e:J

    add-long/2addr v2, p2

    invoke-virtual {v0, v2, v3}, Lead;->a(J)Lead;

    .line 296
    iget-object v2, p0, Lexz;->b:Levk;

    iget-object v3, v0, Lead;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Levk;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 298
    :cond_0
    return-void
.end method

.method static synthetic b(Lexz;)V
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 26
    invoke-static {}, Lb;->b()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lexz;->g:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v6

    iget-object v0, p0, Lexz;->b:Levk;

    invoke-virtual {v0}, Levk;->d()Levl;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Levl;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Levl;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lead;

    iget-object v1, p0, Lexz;->f:Ljava/util/Map;

    iget-object v9, v0, Lead;->b:Ljava/lang/String;

    invoke-interface {v1, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexy;

    if-nez v1, :cond_2

    const-string v9, "Missing task factory for task type: "

    iget-object v1, v0, Lead;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v9, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    iget-object v0, v0, Lead;->b:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-interface {v1, v0}, Lexy;->a(Lead;)Lexx;

    move-result-object v9

    iget-object v1, v9, Lexx;->a:Lead;

    iget-wide v10, v1, Lead;->d:J

    cmp-long v1, v6, v10

    if-ltz v1, :cond_0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "Executed scheduled task of type %s"

    new-array v11, v2, [Ljava/lang/Object;

    invoke-virtual {v9}, Lexx;->c()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v3

    invoke-static {v1, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lexz;->e:Ljava/util/concurrent/Executor;

    new-instance v10, Leyf;

    invoke-direct {v10, p0, v9}, Leyf;-><init>(Lexz;Lexx;)V

    invoke-interface {v1, v10}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v1, v9, Lexx;->a:Lead;

    iget-wide v10, v1, Lead;->e:J

    const-wide/16 v12, 0x0

    cmp-long v1, v10, v12

    if-lez v1, :cond_3

    move v1, v2

    :goto_2
    if-nez v1, :cond_4

    invoke-virtual {v9}, Lexx;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    invoke-interface {v8}, Levl;->a()V

    iget-object v0, p0, Lexz;->b:Levk;

    invoke-virtual {v0}, Levk;->a()V

    :try_start_0
    invoke-virtual {p0, v4}, Lexz;->a(Ljava/util/List;)V

    invoke-direct {p0, v5, v6, v7}, Lexz;->a(Ljava/util/List;J)V

    iget-object v0, p0, Lexz;->b:Levk;

    invoke-virtual {v0}, Levk;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lexz;->b:Levk;

    invoke-virtual {v0}, Levk;->b()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lexz;->b:Levk;

    invoke-virtual {v1}, Levk;->b()V

    throw v0
.end method

.method private d(Lead;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 176
    iget-wide v0, p1, Lead;->d:J

    iget-object v2, p0, Lexz;->g:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-static {v0, v1, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 178
    new-instance v1, Leye;

    invoke-direct {v1, p0}, Leye;-><init>(Lexz;)V

    .line 185
    iget-wide v4, p1, Lead;->e:J

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    .line 186
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Scheduling task %s with ScheduledExecutorService for repeating execution."

    new-array v5, v6, [Ljava/lang/Object;

    .line 190
    iget-object v6, p1, Lead;->b:Ljava/lang/String;

    aput-object v6, v5, v7

    .line 187
    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 186
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lexz;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 194
    iget-wide v4, p1, Lead;->e:J

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 191
    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 204
    :goto_0
    return-void

    .line 197
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Scheduling task %s with ScheduledExecutorService for one time execution."

    new-array v5, v6, [Ljava/lang/Object;

    .line 201
    iget-object v6, p1, Lead;->b:Ljava/lang/String;

    aput-object v6, v5, v7

    .line 198
    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 197
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lexz;->c:Ljava/util/concurrent/ScheduledExecutorService;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lead;)V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lexz;->e:Ljava/util/concurrent/Executor;

    new-instance v1, Leyd;

    invoke-direct {v1, p0, p1}, Leyd;-><init>(Lexz;Lead;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 127
    return-void
.end method

.method public final a(Lexy;)V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lexz;->f:Ljava/util/Map;

    invoke-interface {p1}, Lexy;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 281
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 282
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Removing task %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    .line 283
    iget-object v2, p0, Lexz;->b:Levk;

    invoke-virtual {v2, v0}, Levk;->a(Ljava/lang/String;)I

    goto :goto_0

    .line 285
    :cond_0
    return-void
.end method

.method public final declared-synchronized b(Lead;)V
    .locals 2

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lb;->b()V

    .line 135
    iget-object v0, p0, Lexz;->b:Levk;

    iget-object v1, p1, Lead;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Levk;->b(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    :goto_0
    monitor-exit p0

    return-void

    .line 140
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Lexz;->c(Lead;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lead;)V
    .locals 2

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lb;->b()V

    .line 148
    iget-object v0, p0, Lexz;->b:Levk;

    iget-object v1, p1, Lead;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Levk;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 149
    invoke-direct {p0, p1}, Lexz;->d(Lead;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 150
    monitor-exit p0

    return-void

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
