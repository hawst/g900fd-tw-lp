.class public final Liky;
.super Likz;
.source "SourceFile"


# instance fields
.field private e:Ljava/util/List;

.field private f:Lzg;

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;

.field private i:[J

.field private j:Ljava/util/List;

.field private k:Lila;

.field private l:Ljava/lang/String;

.field private m:Lyi;


# direct methods
.method public constructor <init>(Lzr;)V
    .locals 26

    .prologue
    .line 43
    invoke-direct/range {p0 .. p0}, Likz;-><init>()V

    .line 37
    const/4 v4, 0x0

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->i:[J

    .line 39
    new-instance v4, Lila;

    invoke-direct {v4}, Lila;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->k:Lila;

    .line 44
    invoke-virtual/range {p1 .. p1}, Lzr;->g()Lzs;

    move-result-object v4

    iget-wide v12, v4, Lzs;->c:J

    .line 45
    new-instance v4, Laae;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Laae;-><init>(Lzr;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->e:Ljava/util/List;

    .line 46
    invoke-virtual/range {p1 .. p1}, Lzr;->i()Lyz;

    move-result-object v4

    invoke-virtual {v4}, Lyz;->g()Lzb;

    move-result-object v4

    invoke-virtual {v4}, Lzb;->g()Lzi;

    move-result-object v5

    .line 47
    invoke-virtual/range {p1 .. p1}, Lzr;->i()Lyz;

    move-result-object v4

    iget-object v4, v4, Lyz;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lyj;

    instance-of v7, v4, Lyx;

    if-eqz v7, :cond_0

    check-cast v4, Lyx;

    :goto_0
    iget-object v4, v4, Lyx;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->l:Ljava/lang/String;

    .line 49
    invoke-virtual/range {p1 .. p1}, Lzr;->i()Lyz;

    move-result-object v4

    invoke-virtual {v4}, Lyz;->g()Lzb;

    move-result-object v4

    invoke-virtual {v4}, Lzb;->h()Lyi;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->m:Lyi;

    .line 50
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->g:Ljava/util/List;

    .line 51
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->h:Ljava/util/List;

    .line 52
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->j:Ljava/util/List;

    .line 54
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->g:Ljava/util/List;

    invoke-virtual {v5}, Lzi;->k()Lzp;

    move-result-object v6

    iget-object v6, v6, Lzp;->a:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 55
    invoke-virtual {v5}, Lzi;->m()Lyl;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 56
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->h:Ljava/util/List;

    invoke-virtual {v5}, Lzi;->m()Lyl;

    move-result-object v6

    iget-object v6, v6, Lyl;->a:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 58
    :cond_1
    invoke-virtual {v5}, Lzi;->n()Lze;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 59
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->j:Ljava/util/List;

    invoke-virtual {v5}, Lzi;->n()Lze;

    move-result-object v6

    iget-object v6, v6, Lze;->a:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 61
    :cond_2
    invoke-virtual {v5}, Lzi;->l()Lzo;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 62
    invoke-virtual {v5}, Lzi;->l()Lzo;

    move-result-object v4

    iget-object v4, v4, Lzo;->a:[J

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->i:[J

    .line 66
    :cond_3
    invoke-virtual {v5}, Lzi;->g()Lzg;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Liky;->f:Lzg;

    .line 67
    move-object/from16 v0, p1

    iget-object v4, v0, Likt;->m:Lyn;

    const-class v5, Lzv;

    invoke-interface {v4, v5}, Lyn;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 68
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_19

    .line 69
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_19

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lzv;

    .line 70
    const-class v5, Lzy;

    invoke-virtual {v4, v5}, Lzv;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 71
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v7, v4

    check-cast v7, Lzy;

    .line 72
    iget-wide v4, v7, Lzy;->a:J

    cmp-long v4, v4, v12

    if-nez v4, :cond_5

    .line 73
    new-instance v16, Ljava/util/LinkedList;

    invoke-direct/range {v16 .. v16}, Ljava/util/LinkedList;-><init>()V

    .line 75
    const-wide/16 v8, 0x1

    .line 76
    invoke-virtual/range {p1 .. p1}, Lzr;->b()Lye;

    move-result-object v4

    const-class v5, Lzw;

    invoke-virtual {v4, v5}, Lye;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_6
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lzw;

    .line 77
    const-class v5, Lzz;

    invoke-virtual {v4, v5}, Lzw;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 78
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :cond_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lzz;

    .line 79
    invoke-virtual {v4}, Lzz;->g()Laaa;

    move-result-object v5

    iget-wide v10, v5, Laaa;->a:J

    cmp-long v5, v10, v12

    if-nez v5, :cond_7

    .line 80
    const-class v5, Laab;

    invoke-virtual {v4, v5}, Lzz;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v4

    .line 81
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v6, v4

    check-cast v6, Laab;

    .line 82
    iget-object v4, v6, Likt;->m:Lyn;

    check-cast v4, Lzz;

    invoke-virtual {v4}, Lzz;->g()Laaa;

    move-result-object v20

    .line 83
    const/4 v4, 0x1

    .line 84
    iget-object v5, v6, Laab;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    move-wide v10, v8

    move v8, v4

    :goto_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_17

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Laac;

    .line 85
    invoke-virtual {v6}, Laab;->g()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 86
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->g:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Liky;->g:Ljava/util/List;

    .line 87
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lzq;

    iget-wide v0, v4, Lzq;->b:J

    move-wide/from16 v22, v0

    iget-wide v0, v5, Laac;->a:J

    move-wide/from16 v24, v0

    cmp-long v4, v22, v24

    if-eqz v4, :cond_d

    .line 88
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->g:Ljava/util/List;

    new-instance v9, Lzq;

    const-wide/16 v22, 0x1

    iget-wide v0, v5, Laac;->a:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v22

    move-wide/from16 v2, v24

    invoke-direct {v9, v0, v1, v2, v3}, Lzq;-><init>(JJ)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    :goto_3
    invoke-virtual {v6}, Laab;->i()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 102
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->h:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->h:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Liky;->h:Ljava/util/List;

    .line 103
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lym;

    iget v4, v4, Lym;->b:I

    iget v9, v5, Laac;->d:I

    if-eq v4, v9, :cond_11

    .line 104
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->h:Ljava/util/List;

    new-instance v9, Lym;

    const/16 v22, 0x1

    iget v0, v5, Laac;->d:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, La;->d(J)I

    move-result v23

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v9, v0, v1}, Lym;-><init>(II)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_a
    :goto_4
    invoke-virtual {v6}, Laab;->h()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 112
    iget-object v4, v5, Laac;->c:Lzx;

    .line 124
    :goto_5
    if-eqz v4, :cond_b

    iget-boolean v4, v4, Lzx;->a:Z

    if-nez v4, :cond_b

    .line 126
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_b
    const-wide/16 v4, 0x1

    add-long v8, v10, v4

    .line 129
    const/4 v4, 0x0

    move-wide v10, v8

    move v8, v4

    .line 130
    goto/16 :goto_2

    .line 47
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 90
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->g:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Liky;->g:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lzq;

    .line 91
    iget-wide v0, v4, Lzq;->a:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x1

    add-long v22, v22, v24

    move-wide/from16 v0, v22

    iput-wide v0, v4, Lzq;->a:J

    goto/16 :goto_3

    .line 94
    :cond_e
    move-object/from16 v0, v20

    iget v4, v0, Likv;->p:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_f

    const/4 v4, 0x1

    :goto_6
    if-eqz v4, :cond_10

    .line 95
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->g:Ljava/util/List;

    new-instance v9, Lzq;

    const-wide/16 v22, 0x1

    move-object/from16 v0, v20

    iget-wide v0, v0, Laaa;->c:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v22

    move-wide/from16 v2, v24

    invoke-direct {v9, v0, v1, v2, v3}, Lzq;-><init>(JJ)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 94
    :cond_f
    const/4 v4, 0x0

    goto :goto_6

    .line 97
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->g:Ljava/util/List;

    new-instance v9, Lzq;

    const-wide/16 v22, 0x1

    iget-wide v0, v7, Lzy;->b:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v22

    move-wide/from16 v2, v24

    invoke-direct {v9, v0, v1, v2, v3}, Lzq;-><init>(JJ)V

    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 106
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->h:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Liky;->h:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-interface {v4, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lym;

    .line 107
    iget v9, v4, Lym;->a:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v4, Lym;->a:I

    goto/16 :goto_4

    .line 114
    :cond_12
    if-eqz v8, :cond_14

    iget v4, v6, Likv;->p:I

    and-int/lit8 v4, v4, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_13

    const/4 v4, 0x1

    :goto_7
    if-eqz v4, :cond_14

    .line 115
    iget-object v4, v6, Laab;->b:Lzx;

    goto/16 :goto_5

    .line 114
    :cond_13
    const/4 v4, 0x0

    goto :goto_7

    .line 117
    :cond_14
    move-object/from16 v0, v20

    iget v4, v0, Likv;->p:I

    and-int/lit8 v4, v4, 0x20

    if-eqz v4, :cond_15

    const/4 v4, 0x1

    :goto_8
    if-eqz v4, :cond_16

    .line 118
    move-object/from16 v0, v20

    iget-object v4, v0, Laaa;->e:Lzx;

    goto/16 :goto_5

    .line 117
    :cond_15
    const/4 v4, 0x0

    goto :goto_8

    .line 120
    :cond_16
    iget-object v4, v7, Lzy;->d:Lzx;

    goto/16 :goto_5

    :cond_17
    move-wide v8, v10

    .line 131
    goto/16 :goto_1

    .line 136
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->i:[J

    .line 137
    move-object/from16 v0, p0

    iget-object v5, v0, Liky;->i:[J

    array-length v5, v5

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v6

    add-int/2addr v5, v6

    new-array v5, v5, [J

    move-object/from16 v0, p0

    iput-object v5, v0, Liky;->i:[J

    .line 138
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Liky;->i:[J

    const/4 v7, 0x0

    array-length v8, v4

    invoke-static {v4, v5, v6, v7, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 139
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 140
    array-length v4, v4

    move v5, v4

    .line 141
    :goto_9
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 142
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 143
    move-object/from16 v0, p0

    iget-object v8, v0, Liky;->i:[J

    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    aput-wide v10, v8, v5

    move v5, v6

    .line 144
    goto :goto_9

    .line 149
    :cond_19
    invoke-virtual/range {p1 .. p1}, Lzr;->i()Lyz;

    move-result-object v4

    invoke-virtual {v4}, Lyz;->h()Lza;

    move-result-object v5

    .line 150
    invoke-virtual/range {p1 .. p1}, Lzr;->g()Lzs;

    move-result-object v6

    .line 152
    iget v4, v6, Likv;->p:I

    and-int/lit8 v4, v4, 0x1

    if-lez v4, :cond_1a

    const/4 v4, 0x1

    :goto_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Liky;->a(Z)V

    .line 153
    iget v4, v6, Likv;->p:I

    and-int/lit8 v4, v4, 0x2

    if-lez v4, :cond_1b

    const/4 v4, 0x1

    :goto_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Liky;->b(Z)V

    .line 154
    iget v4, v6, Likv;->p:I

    and-int/lit8 v4, v4, 0x8

    if-lez v4, :cond_1c

    const/4 v4, 0x1

    :goto_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Liky;->d(Z)V

    .line 155
    iget v4, v6, Likv;->p:I

    and-int/lit8 v4, v4, 0x4

    if-lez v4, :cond_1d

    const/4 v4, 0x1

    :goto_d
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Liky;->c(Z)V

    .line 157
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-wide v8, v6, Lzs;->c:J

    iput-wide v8, v4, Lila;->g:J

    .line 158
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-wide v8, v5, Lza;->a:J

    invoke-static {v8, v9}, La;->c(J)Ljava/util/Date;

    move-result-object v7

    iput-object v7, v4, Lila;->d:Ljava/util/Date;

    .line 159
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-object v7, v5, Lza;->e:Ljava/lang/String;

    iput-object v7, v4, Lila;->a:Ljava/lang/String;

    .line 165
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-wide v8, v5, Lza;->b:J

    invoke-static {v8, v9}, La;->c(J)Ljava/util/Date;

    move-result-object v7

    iput-object v7, v4, Lila;->c:Ljava/util/Date;

    .line 166
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-wide v8, v5, Lza;->c:J

    iput-wide v8, v4, Lila;->b:J

    .line 167
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-wide v8, v6, Lzs;->j:D

    iput-wide v8, v4, Lila;->f:D

    .line 168
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-wide v8, v6, Lzs;->i:D

    iput-wide v8, v4, Lila;->e:D

    .line 169
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget v5, v6, Lzs;->e:I

    iput v5, v4, Lila;->k:I

    .line 170
    move-object/from16 v0, p0

    iget-object v4, v0, Liky;->k:Lila;

    iget-object v5, v6, Lzs;->h:[J

    iput-object v5, v4, Lila;->h:[J

    .line 171
    return-void

    .line 152
    :cond_1a
    const/4 v4, 0x0

    goto :goto_a

    .line 153
    :cond_1b
    const/4 v4, 0x0

    goto :goto_b

    .line 154
    :cond_1c
    const/4 v4, 0x0

    goto :goto_c

    .line 155
    :cond_1d
    const/4 v4, 0x0

    goto :goto_d
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Liky;->e:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lzg;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Liky;->f:Lzg;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Liky;->g:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Liky;->h:Ljava/util/List;

    return-object v0
.end method

.method public final e()[J
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Liky;->i:[J

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Liky;->j:Ljava/util/List;

    return-object v0
.end method

.method public final g()Lila;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Liky;->k:Lila;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Liky;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final bridge synthetic i()Lyj;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Liky;->m:Lyi;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 216
    const-string v0, "Mp4TrackImpl{handler=\'"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Liky;->l:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
