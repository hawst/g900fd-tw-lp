.class public final enum Lffo;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lffo;

.field public static final enum b:Lffo;

.field public static final enum c:Lffo;

.field public static final enum d:Lffo;

.field private static final synthetic f:[Lffo;


# instance fields
.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 144
    new-instance v0, Lffo;

    const-string v1, "UPLOAD_DATE_ANY"

    invoke-direct {v0, v1, v2, v2}, Lffo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffo;->a:Lffo;

    .line 145
    new-instance v0, Lffo;

    const-string v1, "UPLOAD_DATE_TODAY"

    invoke-direct {v0, v1, v5, v3}, Lffo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffo;->b:Lffo;

    .line 146
    new-instance v0, Lffo;

    const-string v1, "UPLOAD_DATE_THIS_WEEK"

    invoke-direct {v0, v1, v3, v4}, Lffo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffo;->c:Lffo;

    .line 147
    new-instance v0, Lffo;

    const-string v1, "UPLOAD_DATE_THIS_MONTH"

    invoke-direct {v0, v1, v4, v6}, Lffo;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lffo;->d:Lffo;

    .line 143
    new-array v0, v6, [Lffo;

    sget-object v1, Lffo;->a:Lffo;

    aput-object v1, v0, v2

    sget-object v1, Lffo;->b:Lffo;

    aput-object v1, v0, v5

    sget-object v1, Lffo;->c:Lffo;

    aput-object v1, v0, v3

    sget-object v1, Lffo;->d:Lffo;

    aput-object v1, v0, v4

    sput-object v0, Lffo;->f:[Lffo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 151
    iput p3, p0, Lffo;->e:I

    .line 152
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lffo;
    .locals 1

    .prologue
    .line 143
    const-class v0, Lffo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lffo;

    return-object v0
.end method

.method public static values()[Lffo;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lffo;->f:[Lffo;

    invoke-virtual {v0}, [Lffo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lffo;

    return-object v0
.end method
