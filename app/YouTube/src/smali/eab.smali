.class public final Leab;
.super Lida;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Leae;

.field public c:Z

.field public d:Leae;

.field public e:Z

.field public f:Leae;

.field public g:Z

.field public h:Leae;

.field public i:Ljava/util/List;

.field public j:Z

.field public k:Leae;

.field public l:Z

.field private m:Z

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1013
    invoke-direct {p0}, Lida;-><init>()V

    .line 1018
    iput-object v1, p0, Leab;->b:Leae;

    .line 1038
    iput-object v1, p0, Leab;->d:Leae;

    .line 1058
    iput-object v1, p0, Leab;->f:Leae;

    .line 1078
    iput-object v1, p0, Leab;->h:Leae;

    .line 1098
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Leab;->i:Ljava/util/List;

    .line 1131
    iput-object v1, p0, Leab;->k:Leae;

    .line 1151
    const/4 v0, 0x0

    iput-boolean v0, p0, Leab;->l:Z

    .line 1206
    const/4 v0, -0x1

    iput v0, p0, Leab;->n:I

    .line 1013
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1208
    iget v0, p0, Leab;->n:I

    if-gez v0, :cond_0

    .line 1210
    invoke-virtual {p0}, Leab;->b()I

    .line 1212
    :cond_0
    iget v0, p0, Leab;->n:I

    return v0
.end method

.method public final a(Leae;)Leab;
    .locals 1

    .prologue
    .line 1022
    if-nez p1, :cond_0

    .line 1023
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1025
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leab;->a:Z

    .line 1026
    iput-object p1, p0, Leab;->b:Leae;

    .line 1027
    return-object p0
.end method

.method public final a(Z)Leab;
    .locals 1

    .prologue
    .line 1155
    const/4 v0, 0x1

    iput-boolean v0, p0, Leab;->m:Z

    .line 1156
    iput-boolean p1, p0, Leab;->l:Z

    .line 1157
    return-object p0
.end method

.method public final synthetic a(Licx;)Lida;
    .locals 1

    .prologue
    .line 1011
    :cond_0
    :goto_0
    invoke-virtual {p1}, Licx;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Licx;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Leab;->a(Leae;)Leab;

    goto :goto_0

    :sswitch_2
    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Leab;->b(Leae;)Leab;

    goto :goto_0

    :sswitch_3
    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Leab;->c(Leae;)Leab;

    goto :goto_0

    :sswitch_4
    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Leab;->d(Leae;)Leab;

    goto :goto_0

    :sswitch_5
    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Leab;->e(Leae;)Leab;

    goto :goto_0

    :sswitch_6
    new-instance v0, Leae;

    invoke-direct {v0}, Leae;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Leab;->f(Leae;)Leab;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Licx;->c()Z

    move-result v0

    invoke-virtual {p0, v0}, Leab;->a(Z)Leab;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Licy;)V
    .locals 3

    .prologue
    .line 1183
    iget-boolean v0, p0, Leab;->a:Z

    if-eqz v0, :cond_0

    .line 1184
    const/4 v0, 0x1

    iget-object v1, p0, Leab;->b:Leae;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILida;)V

    .line 1186
    :cond_0
    iget-boolean v0, p0, Leab;->c:Z

    if-eqz v0, :cond_1

    .line 1187
    const/4 v0, 0x2

    iget-object v1, p0, Leab;->d:Leae;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILida;)V

    .line 1189
    :cond_1
    iget-boolean v0, p0, Leab;->e:Z

    if-eqz v0, :cond_2

    .line 1190
    const/4 v0, 0x3

    iget-object v1, p0, Leab;->f:Leae;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILida;)V

    .line 1192
    :cond_2
    iget-boolean v0, p0, Leab;->g:Z

    if-eqz v0, :cond_3

    .line 1193
    const/4 v0, 0x4

    iget-object v1, p0, Leab;->h:Leae;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILida;)V

    .line 1195
    :cond_3
    iget-object v0, p0, Leab;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leae;

    .line 1196
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Licy;->a(ILida;)V

    goto :goto_0

    .line 1198
    :cond_4
    iget-boolean v0, p0, Leab;->j:Z

    if-eqz v0, :cond_5

    .line 1199
    const/4 v0, 0x6

    iget-object v1, p0, Leab;->k:Leae;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILida;)V

    .line 1201
    :cond_5
    iget-boolean v0, p0, Leab;->m:Z

    if-eqz v0, :cond_6

    .line 1202
    const/4 v0, 0x7

    iget-boolean v1, p0, Leab;->l:Z

    invoke-virtual {p1, v0, v1}, Licy;->a(IZ)V

    .line 1204
    :cond_6
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1216
    const/4 v0, 0x0

    .line 1217
    iget-boolean v1, p0, Leab;->a:Z

    if-eqz v1, :cond_0

    .line 1218
    const/4 v0, 0x1

    .line 1219
    iget-object v1, p0, Leab;->b:Leae;

    invoke-static {v0, v1}, Licy;->b(ILida;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1221
    :cond_0
    iget-boolean v1, p0, Leab;->c:Z

    if-eqz v1, :cond_1

    .line 1222
    const/4 v1, 0x2

    .line 1223
    iget-object v2, p0, Leab;->d:Leae;

    invoke-static {v1, v2}, Licy;->b(ILida;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1225
    :cond_1
    iget-boolean v1, p0, Leab;->e:Z

    if-eqz v1, :cond_2

    .line 1226
    const/4 v1, 0x3

    .line 1227
    iget-object v2, p0, Leab;->f:Leae;

    invoke-static {v1, v2}, Licy;->b(ILida;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1229
    :cond_2
    iget-boolean v1, p0, Leab;->g:Z

    if-eqz v1, :cond_3

    .line 1230
    const/4 v1, 0x4

    .line 1231
    iget-object v2, p0, Leab;->h:Leae;

    invoke-static {v1, v2}, Licy;->b(ILida;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1233
    :cond_3
    iget-object v1, p0, Leab;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leae;

    .line 1234
    const/4 v3, 0x5

    .line 1235
    invoke-static {v3, v0}, Licy;->b(ILida;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1236
    goto :goto_0

    .line 1237
    :cond_4
    iget-boolean v0, p0, Leab;->j:Z

    if-eqz v0, :cond_5

    .line 1238
    const/4 v0, 0x6

    .line 1239
    iget-object v2, p0, Leab;->k:Leae;

    invoke-static {v0, v2}, Licy;->b(ILida;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1241
    :cond_5
    iget-boolean v0, p0, Leab;->m:Z

    if-eqz v0, :cond_6

    .line 1242
    const/4 v0, 0x7

    .line 1243
    iget-boolean v2, p0, Leab;->l:Z

    invoke-static {v0}, Licy;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1245
    :cond_6
    iput v1, p0, Leab;->n:I

    .line 1246
    return v1
.end method

.method public final b(Leae;)Leab;
    .locals 1

    .prologue
    .line 1042
    if-nez p1, :cond_0

    .line 1043
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1045
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leab;->c:Z

    .line 1046
    iput-object p1, p0, Leab;->d:Leae;

    .line 1047
    return-object p0
.end method

.method public final c(Leae;)Leab;
    .locals 1

    .prologue
    .line 1062
    if-nez p1, :cond_0

    .line 1063
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1065
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leab;->e:Z

    .line 1066
    iput-object p1, p0, Leab;->f:Leae;

    .line 1067
    return-object p0
.end method

.method public final d(Leae;)Leab;
    .locals 1

    .prologue
    .line 1082
    if-nez p1, :cond_0

    .line 1083
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1085
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leab;->g:Z

    .line 1086
    iput-object p1, p0, Leab;->h:Leae;

    .line 1087
    return-object p0
.end method

.method public final e(Leae;)Leab;
    .locals 1

    .prologue
    .line 1114
    if-nez p1, :cond_0

    .line 1115
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1117
    :cond_0
    iget-object v0, p0, Leab;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Leab;->i:Ljava/util/List;

    .line 1120
    :cond_1
    iget-object v0, p0, Leab;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1121
    return-object p0
.end method

.method public final f(Leae;)Leab;
    .locals 1

    .prologue
    .line 1135
    if-nez p1, :cond_0

    .line 1136
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1138
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Leab;->j:Z

    .line 1139
    iput-object p1, p0, Leab;->k:Leae;

    .line 1140
    return-object p0
.end method
