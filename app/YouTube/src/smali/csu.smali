.class public final Lcsu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field private synthetic a:Ljava/lang/String;

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:Levn;

.field private synthetic d:Lcst;


# direct methods
.method public constructor <init>(Lcst;Ljava/lang/String;Ljava/lang/String;Levn;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcsu;->d:Lcst;

    iput-object p2, p0, Lcsu;->a:Ljava/lang/String;

    iput-object p3, p0, Lcsu;->b:Ljava/lang/String;

    iput-object p4, p0, Lcsu;->c:Levn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 1

    .prologue
    .line 141
    const-string v0, "Account list request failed"

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 142
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 123
    check-cast p1, Lfig;

    invoke-virtual {p1}, Lfig;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfid;

    iget-object v2, p0, Lcsu;->a:Ljava/lang/String;

    iget-object v3, v0, Lfid;->a:Lgxt;

    iget-object v3, v3, Lgxt;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Lcua;

    iget-object v2, p0, Lcsu;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcua;-><init>(Ljava/lang/String;Lfid;)V

    iget-object v0, p0, Lcsu;->d:Lcst;

    iget-object v0, v0, Lcst;->a:Lctj;

    invoke-interface {v0, v1}, Lctj;->a(Lcua;)V

    iget-object v0, p0, Lcsu;->c:Levn;

    new-instance v1, Lcsw;

    invoke-direct {v1}, Lcsw;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    const-string v1, "Refresh profile failed; no match for identity "

    iget-object v0, p0, Lcsu;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
