.class public final Lgbm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/net/Uri;

.field private c:Lgbn;

.field private d:Ljava/lang/String;

.field private e:Lgcd;

.field private f:Ljava/lang/String;

.field private g:Ljava/util/Date;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;Lgbn;Ljava/lang/String;Lgcd;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lgbm;->a:Ljava/lang/String;

    .line 109
    iput-object p2, p0, Lgbm;->b:Landroid/net/Uri;

    .line 110
    iput-object p3, p0, Lgbm;->c:Lgbn;

    .line 111
    iput-object p4, p0, Lgbm;->d:Ljava/lang/String;

    .line 112
    iput-object p5, p0, Lgbm;->e:Lgcd;

    .line 113
    iput-object p7, p0, Lgbm;->g:Ljava/util/Date;

    .line 114
    if-eqz p3, :cond_0

    iget-boolean v0, p3, Lgbn;->k:Z

    if-nez v0, :cond_0

    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iput-object p4, p0, Lgbm;->f:Ljava/lang/String;

    .line 120
    :goto_0
    iput-object p8, p0, Lgbm;->h:Ljava/lang/String;

    .line 121
    return-void

    .line 118
    :cond_0
    iput-object p6, p0, Lgbm;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 143
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "builder required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 139
    new-instance v0, Lgbo;

    invoke-direct {v0}, Lgbo;-><init>()V

    iget-object v1, p0, Lgbm;->a:Ljava/lang/String;

    iput-object v1, v0, Lgbo;->a:Ljava/lang/String;

    iget-object v1, p0, Lgbm;->b:Landroid/net/Uri;

    iput-object v1, v0, Lgbo;->b:Landroid/net/Uri;

    iget-object v1, p0, Lgbm;->c:Lgbn;

    iput-object v1, v0, Lgbo;->c:Lgbn;

    iget-object v1, p0, Lgbm;->d:Ljava/lang/String;

    iput-object v1, v0, Lgbo;->d:Ljava/lang/String;

    iget-object v1, p0, Lgbm;->e:Lgcd;

    iput-object v1, v0, Lgbo;->e:Lgcd;

    iget-object v1, p0, Lgbm;->f:Ljava/lang/String;

    iput-object v1, v0, Lgbo;->f:Ljava/lang/String;

    iget-object v1, p0, Lgbm;->g:Ljava/util/Date;

    iput-object v1, v0, Lgbo;->g:Ljava/util/Date;

    iget-object v1, p0, Lgbm;->h:Ljava/lang/String;

    iput-object v1, v0, Lgbo;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 133
    iget-object v1, p0, Lgbm;->a:Ljava/lang/String;

    iget-object v0, p0, Lgbm;->c:Lgbn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgbm;->c:Lgbn;

    invoke-virtual {v0}, Lgbn;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, La;->z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lgbm;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
