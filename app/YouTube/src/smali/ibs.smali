.class public final Libs;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:[Lhym;

.field private b:[Lhym;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lidf;-><init>()V

    sget-object v0, Lhym;->a:[Lhym;

    iput-object v0, p0, Libs;->a:[Lhym;

    sget-object v0, Lhym;->a:[Lhym;

    iput-object v0, p0, Libs;->b:[Lhym;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Libs;->a:[Lhym;

    if-eqz v0, :cond_1

    iget-object v3, p0, Libs;->a:[Lhym;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    const/4 v6, 0x1

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    iget-object v2, p0, Libs;->b:[Lhym;

    if-eqz v2, :cond_4

    iget-object v2, p0, Libs;->b:[Lhym;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    if-eqz v4, :cond_3

    const/4 v5, 0x2

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Libs;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libs;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Libs;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Libs;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Libs;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libs;->a:[Lhym;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhym;

    iget-object v3, p0, Libs;->a:[Lhym;

    if-eqz v3, :cond_2

    iget-object v3, p0, Libs;->a:[Lhym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Libs;->a:[Lhym;

    :goto_2
    iget-object v2, p0, Libs;->a:[Lhym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Libs;->a:[Lhym;

    new-instance v3, Lhym;

    invoke-direct {v3}, Lhym;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libs;->a:[Lhym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Libs;->a:[Lhym;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Libs;->a:[Lhym;

    new-instance v3, Lhym;

    invoke-direct {v3}, Lhym;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libs;->a:[Lhym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libs;->b:[Lhym;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhym;

    iget-object v3, p0, Libs;->b:[Lhym;

    if-eqz v3, :cond_5

    iget-object v3, p0, Libs;->b:[Lhym;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Libs;->b:[Lhym;

    :goto_4
    iget-object v2, p0, Libs;->b:[Lhym;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Libs;->b:[Lhym;

    new-instance v3, Lhym;

    invoke-direct {v3}, Lhym;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libs;->b:[Lhym;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Libs;->b:[Lhym;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libs;->b:[Lhym;

    new-instance v3, Lhym;

    invoke-direct {v3}, Lhym;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libs;->b:[Lhym;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Libs;->a:[Lhym;

    if-eqz v1, :cond_1

    iget-object v2, p0, Libs;->a:[Lhym;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Libs;->b:[Lhym;

    if-eqz v1, :cond_3

    iget-object v1, p0, Libs;->b:[Lhym;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Libs;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libs;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libs;

    iget-object v2, p0, Libs;->a:[Lhym;

    iget-object v3, p1, Libs;->a:[Lhym;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libs;->b:[Lhym;

    iget-object v3, p1, Libs;->b:[Lhym;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libs;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Libs;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Libs;->I:Ljava/util/List;

    iget-object v3, p1, Libs;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    iget-object v2, p0, Libs;->a:[Lhym;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Libs;->b:[Lhym;

    if-nez v0, :cond_4

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Libs;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v2, v0

    move v0, v1

    :goto_1
    iget-object v3, p0, Libs;->a:[Lhym;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libs;->a:[Lhym;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_2
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Libs;->a:[Lhym;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhym;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_4
    move v0, v1

    :goto_3
    iget-object v3, p0, Libs;->b:[Lhym;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libs;->b:[Lhym;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    iget-object v2, p0, Libs;->b:[Lhym;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhym;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_6
    iget-object v1, p0, Libs;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
