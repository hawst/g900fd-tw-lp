.class public final Lcpf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;


# instance fields
.field public final a:Levn;

.field private final b:Lgix;

.field private final c:Lgng;

.field private final d:Lezj;

.field private final e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Levn;Lgix;Lgng;Lezj;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcpf;->a:Levn;

    .line 36
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lcpf;->b:Lgix;

    .line 37
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgng;

    iput-object v0, p0, Lcpf;->c:Lgng;

    .line 38
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcpf;->d:Lezj;

    .line 39
    invoke-static {p5}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpf;->e:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public final handleVideoTimeEvent(Ldad;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 49
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_1

    .line 50
    iget-boolean v0, p0, Lcpf;->f:Z

    if-nez v0, :cond_1

    .line 53
    iget-object v0, p0, Lcpf;->b:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    iget-object v0, p0, Lcpf;->c:Lgng;

    iget-object v1, p0, Lcpf;->b:Lgix;

    .line 55
    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    .line 61
    :goto_0
    iget-object v1, p0, Lcpf;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lcpf;->d:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v2

    .line 63
    iget-object v1, p0, Lcpf;->e:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x40

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Tracking last offlined playback for video "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    .line 64
    iget-object v1, p0, Lcpf;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lgnd;->a(Ljava/lang/String;J)V

    .line 67
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcpf;->f:Z

    .line 70
    :cond_1
    return-void

    .line 57
    :cond_2
    iget-object v0, p0, Lcpf;->c:Lgng;

    invoke-virtual {v0}, Lgng;->c()Lgnd;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcpf;->a:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 133
    return-void
.end method
