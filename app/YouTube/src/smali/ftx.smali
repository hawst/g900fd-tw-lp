.class public abstract Lftx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lezl;


# instance fields
.field private final a:Lfdg;

.field private final b:Leyt;

.field private final c:Levn;

.field private final d:Ljava/lang/Object;

.field private final e:Ljava/util/Map;

.field l:Lfjk;

.field m:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lfdg;Levn;Ljava/lang/Object;Leyt;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdg;

    iput-object v0, p0, Lftx;->a:Lfdg;

    .line 94
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lftx;->c:Levn;

    .line 95
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lftx;->b:Leyt;

    .line 96
    iput-object p3, p0, Lftx;->d:Ljava/lang/Object;

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lftx;->e:Ljava/util/Map;

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lftx;->m:Ljava/util/Map;

    .line 104
    new-instance v0, Lfua;

    invoke-direct {v0}, Lfua;-><init>()V

    invoke-direct {p0, v0}, Lftx;->a(Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method private a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lftx;->d:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lftx;->c:Levn;

    iget-object v1, p0, Lftx;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Levn;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 113
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-object v0, p0, Lftx;->c:Levn;

    invoke-virtual {v0, p1}, Levn;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private b(Lfjk;)V
    .locals 2

    .prologue
    .line 276
    if-nez p1, :cond_0

    .line 308
    :goto_0
    return-void

    .line 280
    :cond_0
    new-instance v0, Lfuc;

    invoke-direct {v0}, Lfuc;-><init>()V

    invoke-direct {p0, v0}, Lftx;->a(Ljava/lang/Object;)V

    .line 282
    iput-object p1, p0, Lftx;->l:Lfjk;

    .line 283
    iget-object v0, p0, Lftx;->a:Lfdg;

    new-instance v1, Lftz;

    invoke-direct {v1, p0, p1}, Lftz;-><init>(Lftx;Lfjk;)V

    invoke-interface {v0, p1, v1}, Lfdg;->a(Lfjk;Lwv;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lfjk;)V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lftx;->l:Lfjk;

    if-ne p1, v0, :cond_0

    .line 273
    :goto_0
    return-void

    .line 272
    :cond_0
    invoke-direct {p0, p1}, Lftx;->b(Lfjk;)V

    goto :goto_0
.end method

.method public a(Lfjl;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lftx;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjk;

    .line 137
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0, v0}, Lftx;->a(Lfjk;)V

    .line 140
    :cond_0
    return-void
.end method

.method public a(Lhel;Lfjl;)V
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lfua;

    invoke-direct {v0}, Lfua;-><init>()V

    invoke-direct {p0, v0}, Lftx;->a(Ljava/lang/Object;)V

    .line 211
    return-void
.end method

.method public final a(Ljava/lang/Object;Lfjk;)V
    .locals 4

    .prologue
    .line 160
    invoke-static {}, Lb;->a()V

    .line 161
    if-nez p2, :cond_0

    .line 182
    :goto_0
    return-void

    .line 164
    :cond_0
    iget v0, p2, Lfjk;->d:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 165
    invoke-virtual {p0, p2}, Lftx;->a(Lfjk;)V

    goto :goto_0

    .line 164
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 168
    :cond_2
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 169
    iget-object v0, p0, Lftx;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 170
    iget-object v0, p0, Lftx;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 172
    :cond_3
    iget-object v0, p0, Lftx;->m:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    new-instance v0, Lfty;

    invoke-direct {v0, p0, p1, p2}, Lfty;-><init>(Lftx;Ljava/lang/Object;Lfjk;)V

    .line 181
    iget v2, p2, Lfjk;->d:I

    int-to-long v2, v2

    .line 173
    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method

.method public a(Lxa;Lfjl;)V
    .locals 3

    .prologue
    .line 219
    new-instance v0, Lfub;

    iget-object v1, p0, Lftx;->b:Leyt;

    invoke-interface {v1, p1}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lfub;-><init>(Ljava/lang/CharSequence;Z)V

    invoke-direct {p0, v0}, Lftx;->a(Ljava/lang/Object;)V

    .line 220
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 226
    invoke-virtual {p0}, Lftx;->e()V

    .line 227
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfjk;

    .line 228
    iget-object v2, p0, Lftx;->e:Ljava/util/Map;

    iget-object v3, v0, Lfjk;->c:Lfjl;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 230
    :cond_0
    return-void
.end method

.method public final b(Lfjl;)Z
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lftx;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lftx;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 312
    invoke-virtual {p0}, Lftx;->f()V

    .line 313
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lftx;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lftx;->l:Lfjk;

    .line 238
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lftx;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Timer;

    .line 245
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    goto :goto_0

    .line 247
    :cond_0
    iget-object v0, p0, Lftx;->m:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 248
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lftx;->l:Lfjk;

    invoke-direct {p0, v0}, Lftx;->b(Lfjk;)V

    .line 259
    return-void
.end method
