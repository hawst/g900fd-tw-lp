.class public final Ldoe;
.super Landroid/app/AlertDialog;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lfxe;

.field private final c:Leyp;

.field private final d:Lgck;

.field private final e:Lctu;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/ImageView;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfxe;Leyp;Lgck;Lctu;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 60
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0d0104

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {p0, v0}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Ldoe;->a:Landroid/app/Activity;

    .line 63
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Ldoe;->b:Lfxe;

    .line 64
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Ldoe;->c:Leyp;

    .line 65
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgck;

    iput-object v0, p0, Ldoe;->d:Lgck;

    .line 66
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctu;

    iput-object v0, p0, Ldoe;->e:Lctu;

    .line 68
    const-string v0, "layout_inflater"

    .line 69
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 70
    const v1, 0x7f04004f

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 72
    const v0, 0x7f090113

    invoke-virtual {p0, v0}, Ldoe;->setTitle(I)V

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    .line 73
    invoke-virtual/range {v0 .. v5}, Ldoe;->setView(Landroid/view/View;IIII)V

    .line 74
    invoke-virtual {p0, v2}, Ldoe;->setIcon(I)V

    .line 76
    const v0, 0x7f08016a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldoe;->f:Landroid/view/View;

    .line 77
    const v0, 0x7f0800a5

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldoe;->g:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0800a4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldoe;->h:Landroid/widget/ImageView;

    .line 80
    const/4 v0, -0x1

    const v1, 0x104000a

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Ldoe;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 81
    const/4 v0, -0x2

    const/high16 v1, 0x1040000

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p0}, Ldoe;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 82
    invoke-virtual {p0, p0}, Ldoe;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 83
    invoke-virtual {p0, p0}, Ldoe;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 84
    return-void
.end method

.method static synthetic a(Ldoe;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldoe;->f:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Ldoe;Z)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldoe;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 166
    iput-boolean p1, p0, Ldoe;->i:Z

    .line 167
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Ldoe;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 172
    :cond_0
    return-void
.end method

.method static synthetic b(Ldoe;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldoe;->g:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Ldoe;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldoe;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic d(Ldoe;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldoe;->h:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Ldoe;)Leyp;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldoe;->c:Leyp;

    return-object v0
.end method

.method static synthetic f(Ldoe;)Lctu;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Ldoe;->e:Lctu;

    return-object v0
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Ldoe;->e:Lctu;

    invoke-interface {v0}, Lctu;->b()V

    .line 89
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 100
    packed-switch p2, :pswitch_data_0

    .line 104
    :goto_0
    return-void

    .line 101
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldoe;->a(Z)V

    iget-object v0, p0, Ldoe;->b:Lfxe;

    iget-object v1, p0, Ldoe;->a:Landroid/app/Activity;

    new-instance v2, Ldoh;

    invoke-direct {v2, p0}, Ldoh;-><init>(Ldoe;)V

    invoke-static {v1, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v1

    invoke-interface {v0, v1}, Lfxe;->b(Leuc;)V

    goto :goto_0

    .line 102
    :pswitch_1
    iget-object v0, p0, Ldoe;->e:Lctu;

    invoke-interface {v0}, Lctu;->b()V

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Ldoe;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->removeDialog(I)V

    .line 96
    return-void
.end method

.method protected final onStart()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-super {p0}, Landroid/app/AlertDialog;->onStart()V

    .line 178
    iget-object v0, p0, Ldoe;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v1}, Ldoe;->a(Z)V

    iget-object v0, p0, Ldoe;->d:Lgck;

    iget-object v1, p0, Ldoe;->a:Landroid/app/Activity;

    new-instance v2, Ldof;

    invoke-direct {v2, p0}, Ldof;-><init>(Ldoe;)V

    invoke-static {v1, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v1

    invoke-interface {v0, v1}, Lgck;->a(Leuc;)V

    .line 179
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Ldoe;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-boolean v1, p0, Ldoe;->i:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 180
    return-void
.end method
