.class public final Lbnm;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:J

.field private static final b:J


# instance fields
.field private final c:Lemg;

.field private final d:Landroid/content/SharedPreferences;

.field private e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 22
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lbnm;->a:J

    .line 23
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lbnm;->b:J

    return-void
.end method

.method public constructor <init>(Lemg;Landroid/content/SharedPreferences;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lemg;

    iput-object v0, p0, Lbnm;->c:Lemg;

    .line 34
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lbnm;->d:Landroid/content/SharedPreferences;

    .line 35
    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbnm;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "settings_fetch_task_scheduled"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 65
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbnm;->e:Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit p0

    return-void

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 7

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lbnm;->c:Lemg;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    const-string v6, "settings_fetch"

    invoke-virtual/range {v1 .. v6}, Lemg;->b(JJLjava/lang/String;)V

    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbnm;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    monitor-exit p0

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 7

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbnm;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 50
    iget-object v0, p0, Lbnm;->d:Landroid/content/SharedPreferences;

    const-string v1, "settings_fetch_task_scheduled"

    const/4 v2, 0x0

    .line 51
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 50
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lbnm;->e:Ljava/lang/Boolean;

    .line 54
    :cond_0
    iget-object v0, p0, Lbnm;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    iget-object v1, p0, Lbnm;->c:Lemg;

    sget-wide v2, Lbnm;->a:J

    sget-wide v4, Lbnm;->b:J

    const-string v6, "settings_fetch"

    invoke-virtual/range {v1 .. v6}, Lemg;->a(JJLjava/lang/String;)V

    .line 59
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbnm;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :cond_1
    monitor-exit p0

    return-void

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final handleSignInEvent(Lfcb;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Lbnm;->b()V

    .line 71
    return-void
.end method

.method public final handleSignOutEvent(Lfcc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 75
    invoke-direct {p0}, Lbnm;->b()V

    .line 76
    return-void
.end method
