.class public final enum Ldxb;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldxb;

.field public static final enum b:Ldxb;

.field public static final enum c:Ldxb;

.field public static final enum d:Ldxb;

.field private static final synthetic e:[Ldxb;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Ldxb;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v2}, Ldxb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldxb;->a:Ldxb;

    .line 9
    new-instance v0, Ldxb;

    const-string v1, "MEDIA_PLAYING_AD"

    invoke-direct {v0, v1, v3}, Ldxb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldxb;->b:Ldxb;

    .line 10
    new-instance v0, Ldxb;

    const-string v1, "MEDIA_PLAYING_VIDEO"

    invoke-direct {v0, v1, v4}, Ldxb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldxb;->c:Ldxb;

    .line 11
    new-instance v0, Ldxb;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v5}, Ldxb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldxb;->d:Ldxb;

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [Ldxb;

    sget-object v1, Ldxb;->a:Ldxb;

    aput-object v1, v0, v2

    sget-object v1, Ldxb;->b:Ldxb;

    aput-object v1, v0, v3

    sget-object v1, Ldxb;->c:Ldxb;

    aput-object v1, v0, v4

    sget-object v1, Ldxb;->d:Ldxb;

    aput-object v1, v0, v5

    sput-object v0, Ldxb;->e:[Ldxb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldxb;
    .locals 1

    .prologue
    .line 7
    const-class v0, Ldxb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldxb;

    return-object v0
.end method

.method public static values()[Ldxb;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Ldxb;->e:[Ldxb;

    invoke-virtual {v0}, [Ldxb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldxb;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 17
    sget-object v0, Ldxb;->b:Ldxb;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
