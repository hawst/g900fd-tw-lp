.class public final Lgzl;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Lhgz;

.field private c:[J

.field private d:[D

.field private e:Lhgz;

.field private f:Lhog;

.field private g:[Lhxi;

.field private h:I

.field private i:Lhgz;

.field private j:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8439
    invoke-direct {p0}, Lidf;-><init>()V

    .line 8442
    iput-object v1, p0, Lgzl;->a:Lhgz;

    .line 8445
    iput-object v1, p0, Lgzl;->b:Lhgz;

    .line 8448
    sget-object v0, Lidj;->b:[J

    iput-object v0, p0, Lgzl;->c:[J

    .line 8451
    sget-object v0, Lidj;->c:[D

    iput-object v0, p0, Lgzl;->d:[D

    .line 8454
    iput-object v1, p0, Lgzl;->e:Lhgz;

    .line 8457
    iput-object v1, p0, Lgzl;->f:Lhog;

    .line 8460
    sget-object v0, Lhxi;->a:[Lhxi;

    iput-object v0, p0, Lgzl;->g:[Lhxi;

    .line 8463
    const/4 v0, 0x0

    iput v0, p0, Lgzl;->h:I

    .line 8466
    iput-object v1, p0, Lgzl;->i:Lhgz;

    .line 8469
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgzl;->j:D

    .line 8439
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 8583
    .line 8584
    iget-object v0, p0, Lgzl;->a:Lhgz;

    if-eqz v0, :cond_b

    .line 8585
    const/4 v0, 0x1

    iget-object v2, p0, Lgzl;->a:Lhgz;

    .line 8586
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8588
    :goto_0
    iget-object v2, p0, Lgzl;->b:Lhgz;

    if-eqz v2, :cond_0

    .line 8589
    const/4 v2, 0x2

    iget-object v3, p0, Lgzl;->b:Lhgz;

    .line 8590
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8592
    :cond_0
    iget-object v2, p0, Lgzl;->c:[J

    if-eqz v2, :cond_2

    iget-object v2, p0, Lgzl;->c:[J

    array-length v2, v2

    if-lez v2, :cond_2

    .line 8594
    iget-object v4, p0, Lgzl;->c:[J

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_1

    aget-wide v6, v4, v2

    .line 8596
    invoke-static {v6, v7}, Lidd;->a(J)I

    move-result v6

    add-int/2addr v3, v6

    .line 8594
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 8598
    :cond_1
    add-int/2addr v0, v3

    .line 8599
    iget-object v2, p0, Lgzl;->c:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 8601
    :cond_2
    iget-object v2, p0, Lgzl;->d:[D

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzl;->d:[D

    array-length v2, v2

    if-lez v2, :cond_3

    .line 8602
    iget-object v2, p0, Lgzl;->d:[D

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x8

    .line 8603
    add-int/2addr v0, v2

    .line 8604
    iget-object v2, p0, Lgzl;->d:[D

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 8606
    :cond_3
    iget-object v2, p0, Lgzl;->e:Lhgz;

    if-eqz v2, :cond_4

    .line 8607
    const/4 v2, 0x6

    iget-object v3, p0, Lgzl;->e:Lhgz;

    .line 8608
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8610
    :cond_4
    iget-object v2, p0, Lgzl;->f:Lhog;

    if-eqz v2, :cond_5

    .line 8611
    const/4 v2, 0x7

    iget-object v3, p0, Lgzl;->f:Lhog;

    .line 8612
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 8614
    :cond_5
    iget-object v2, p0, Lgzl;->g:[Lhxi;

    if-eqz v2, :cond_7

    .line 8615
    iget-object v2, p0, Lgzl;->g:[Lhxi;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 8616
    if-eqz v4, :cond_6

    .line 8617
    const/16 v5, 0x8

    .line 8618
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 8615
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 8622
    :cond_7
    iget v1, p0, Lgzl;->h:I

    if-eqz v1, :cond_8

    .line 8623
    const/16 v1, 0x9

    iget v2, p0, Lgzl;->h:I

    .line 8624
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8626
    :cond_8
    iget-object v1, p0, Lgzl;->i:Lhgz;

    if-eqz v1, :cond_9

    .line 8627
    const/16 v1, 0xa

    iget-object v2, p0, Lgzl;->i:Lhgz;

    .line 8628
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8630
    :cond_9
    iget-wide v2, p0, Lgzl;->j:D

    const-wide/16 v4, 0x0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_a

    .line 8631
    const/16 v1, 0xb

    iget-wide v2, p0, Lgzl;->j:D

    .line 8632
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 8634
    :cond_a
    iget-object v1, p0, Lgzl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8635
    iput v0, p0, Lgzl;->J:I

    .line 8636
    return v0

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8435
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgzl;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgzl;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgzl;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzl;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzl;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgzl;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzl;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzl;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lgzl;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzl;->c:[J

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [J

    iget-object v3, p0, Lgzl;->c:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lgzl;->c:[J

    :goto_1
    iget-object v2, p0, Lgzl;->c:[J

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lgzl;->c:[J

    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lgzl;->c:[J

    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v4

    aput-wide v4, v2, v0

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x29

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzl;->d:[D

    array-length v0, v0

    add-int/2addr v2, v0

    new-array v2, v2, [D

    iget-object v3, p0, Lgzl;->d:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v2, p0, Lgzl;->d:[D

    :goto_2
    iget-object v2, p0, Lgzl;->d:[D

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lgzl;->d:[D

    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lgzl;->d:[D

    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lgzl;->e:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzl;->e:Lhgz;

    :cond_6
    iget-object v0, p0, Lgzl;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lgzl;->f:Lhog;

    if-nez v0, :cond_7

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgzl;->f:Lhog;

    :cond_7
    iget-object v0, p0, Lgzl;->f:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzl;->g:[Lhxi;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhxi;

    iget-object v3, p0, Lgzl;->g:[Lhxi;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lgzl;->g:[Lhxi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lgzl;->g:[Lhxi;

    :goto_4
    iget-object v2, p0, Lgzl;->g:[Lhxi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lgzl;->g:[Lhxi;

    new-instance v3, Lhxi;

    invoke-direct {v3}, Lhxi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzl;->g:[Lhxi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lgzl;->g:[Lhxi;

    array-length v0, v0

    goto :goto_3

    :cond_a
    iget-object v2, p0, Lgzl;->g:[Lhxi;

    new-instance v3, Lhxi;

    invoke-direct {v3}, Lhxi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzl;->g:[Lhxi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lgzl;->h:I

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lgzl;->i:Lhgz;

    if-nez v0, :cond_b

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzl;->i:Lhgz;

    :cond_b
    iget-object v0, p0, Lgzl;->i:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->k()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    iput-wide v2, p0, Lgzl;->j:D

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x20 -> :sswitch_3
        0x29 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x59 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 8539
    iget-object v1, p0, Lgzl;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 8540
    const/4 v1, 0x1

    iget-object v2, p0, Lgzl;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 8542
    :cond_0
    iget-object v1, p0, Lgzl;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 8543
    const/4 v1, 0x2

    iget-object v2, p0, Lgzl;->b:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 8545
    :cond_1
    iget-object v1, p0, Lgzl;->c:[J

    if-eqz v1, :cond_2

    .line 8546
    iget-object v2, p0, Lgzl;->c:[J

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-wide v4, v2, v1

    .line 8547
    const/4 v6, 0x4

    invoke-virtual {p1, v6, v4, v5}, Lidd;->b(IJ)V

    .line 8546
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 8550
    :cond_2
    iget-object v1, p0, Lgzl;->d:[D

    if-eqz v1, :cond_3

    .line 8551
    iget-object v2, p0, Lgzl;->d:[D

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-wide v4, v2, v1

    .line 8552
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v4, v5}, Lidd;->a(ID)V

    .line 8551
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8555
    :cond_3
    iget-object v1, p0, Lgzl;->e:Lhgz;

    if-eqz v1, :cond_4

    .line 8556
    const/4 v1, 0x6

    iget-object v2, p0, Lgzl;->e:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 8558
    :cond_4
    iget-object v1, p0, Lgzl;->f:Lhog;

    if-eqz v1, :cond_5

    .line 8559
    const/4 v1, 0x7

    iget-object v2, p0, Lgzl;->f:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 8561
    :cond_5
    iget-object v1, p0, Lgzl;->g:[Lhxi;

    if-eqz v1, :cond_7

    .line 8562
    iget-object v1, p0, Lgzl;->g:[Lhxi;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 8563
    if-eqz v3, :cond_6

    .line 8564
    const/16 v4, 0x8

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 8562
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 8568
    :cond_7
    iget v0, p0, Lgzl;->h:I

    if-eqz v0, :cond_8

    .line 8569
    const/16 v0, 0x9

    iget v1, p0, Lgzl;->h:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 8571
    :cond_8
    iget-object v0, p0, Lgzl;->i:Lhgz;

    if-eqz v0, :cond_9

    .line 8572
    const/16 v0, 0xa

    iget-object v1, p0, Lgzl;->i:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8574
    :cond_9
    iget-wide v0, p0, Lgzl;->j:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_a

    .line 8575
    const/16 v0, 0xb

    iget-wide v2, p0, Lgzl;->j:D

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(ID)V

    .line 8577
    :cond_a
    iget-object v0, p0, Lgzl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 8579
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8489
    if-ne p1, p0, :cond_1

    .line 8502
    :cond_0
    :goto_0
    return v0

    .line 8490
    :cond_1
    instance-of v2, p1, Lgzl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 8491
    :cond_2
    check-cast p1, Lgzl;

    .line 8492
    iget-object v2, p0, Lgzl;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzl;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzl;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzl;->b:Lhgz;

    if-nez v2, :cond_3

    .line 8493
    :goto_2
    iget-object v2, p0, Lgzl;->c:[J

    iget-object v3, p1, Lgzl;->c:[J

    .line 8494
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzl;->d:[D

    iget-object v3, p1, Lgzl;->d:[D

    .line 8495
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([D[D)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzl;->e:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzl;->e:Lhgz;

    if-nez v2, :cond_3

    .line 8496
    :goto_3
    iget-object v2, p0, Lgzl;->f:Lhog;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzl;->f:Lhog;

    if-nez v2, :cond_3

    .line 8497
    :goto_4
    iget-object v2, p0, Lgzl;->g:[Lhxi;

    iget-object v3, p1, Lgzl;->g:[Lhxi;

    .line 8498
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lgzl;->h:I

    iget v3, p1, Lgzl;->h:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzl;->i:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgzl;->i:Lhgz;

    if-nez v2, :cond_3

    .line 8500
    :goto_5
    iget-wide v2, p0, Lgzl;->j:D

    iget-wide v4, p1, Lgzl;->j:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lgzl;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgzl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 8502
    goto :goto_0

    .line 8492
    :cond_4
    iget-object v2, p0, Lgzl;->a:Lhgz;

    iget-object v3, p1, Lgzl;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzl;->b:Lhgz;

    iget-object v3, p1, Lgzl;->b:Lhgz;

    .line 8493
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 8495
    :cond_6
    iget-object v2, p0, Lgzl;->e:Lhgz;

    iget-object v3, p1, Lgzl;->e:Lhgz;

    .line 8496
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzl;->f:Lhog;

    iget-object v3, p1, Lgzl;->f:Lhog;

    .line 8497
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    .line 8498
    :cond_8
    iget-object v2, p0, Lgzl;->i:Lhgz;

    iget-object v3, p1, Lgzl;->i:Lhgz;

    .line 8500
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgzl;->I:Ljava/util/List;

    iget-object v3, p1, Lgzl;->I:Ljava/util/List;

    .line 8502
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v1, 0x0

    .line 8506
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 8508
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzl;->a:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8509
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzl;->b:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8510
    iget-object v2, p0, Lgzl;->c:[J

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    .line 8516
    :cond_0
    iget-object v0, p0, Lgzl;->d:[D

    if-nez v0, :cond_6

    mul-int/lit8 v2, v2, 0x1f

    .line 8522
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgzl;->e:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 8523
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzl;->f:Lhog;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 8524
    iget-object v2, p0, Lgzl;->g:[Lhxi;

    if-nez v2, :cond_9

    mul-int/lit8 v2, v0, 0x1f

    .line 8530
    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget v2, p0, Lgzl;->h:I

    add-int/2addr v0, v2

    .line 8531
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzl;->i:Lhgz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 8532
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lgzl;->j:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    iget-wide v4, p0, Lgzl;->j:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    ushr-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 8533
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzl;->I:Ljava/util/List;

    if-nez v2, :cond_c

    :goto_5
    add-int/2addr v0, v1

    .line 8534
    return v0

    .line 8508
    :cond_3
    iget-object v0, p0, Lgzl;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8509
    :cond_4
    iget-object v0, p0, Lgzl;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_5
    move v2, v0

    move v0, v1

    .line 8512
    :goto_6
    iget-object v3, p0, Lgzl;->c:[J

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 8513
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lgzl;->c:[J

    aget-wide v4, v3, v0

    iget-object v3, p0, Lgzl;->c:[J

    aget-wide v6, v3, v0

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v2, v3

    .line 8512
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_6
    move v0, v1

    .line 8518
    :goto_7
    iget-object v3, p0, Lgzl;->d:[D

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 8519
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lgzl;->d:[D

    aget-wide v4, v3, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    iget-object v3, p0, Lgzl;->d:[D

    aget-wide v6, v3, v0

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v3, v4

    add-int/2addr v2, v3

    .line 8518
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 8522
    :cond_7
    iget-object v0, p0, Lgzl;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 8523
    :cond_8
    iget-object v0, p0, Lgzl;->f:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_9
    move v2, v0

    move v0, v1

    .line 8526
    :goto_8
    iget-object v3, p0, Lgzl;->g:[Lhxi;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 8527
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgzl;->g:[Lhxi;

    aget-object v2, v2, v0

    if-nez v2, :cond_a

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    .line 8526
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 8527
    :cond_a
    iget-object v2, p0, Lgzl;->g:[Lhxi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhxi;->hashCode()I

    move-result v2

    goto :goto_9

    .line 8531
    :cond_b
    iget-object v0, p0, Lgzl;->i:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 8533
    :cond_c
    iget-object v1, p0, Lgzl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_5
.end method
