.class final Lcdh;
.super Lcde;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;Lboi;Lfdw;Lfrz;)V
    .locals 9

    .prologue
    .line 177
    const v5, 0x7f0400c9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcde;-><init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;ILboi;Lfdw;Lfrz;)V

    .line 186
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    check-cast p2, Lfjf;

    invoke-super {p0, p1, p2}, Lcde;->a(Lfsg;Lfjf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    check-cast p2, Lfjf;

    invoke-super {p0, p1, p2}, Lcde;->a(Lfsg;Lfjf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 167
    check-cast p2, Lfjf;

    invoke-super {p0, p1, p2}, Lcde;->a(Lfsg;Lfjf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lfsg;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 193
    iget-object v0, p0, Lcdh;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 196
    iget-object v1, p0, Lcby;->a:Landroid/content/Context;

    invoke-static {v1, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcdh;->h:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 198
    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 199
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 206
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v1, p0, Lcdh;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 202
    iget-object v1, p0, Lcdh;->f:Landroid/content/res/Resources;

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 203
    iget-object v1, p0, Lcdh;->f:Landroid/content/res/Resources;

    const v2, 0x7f0a00ed

    .line 204
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method
