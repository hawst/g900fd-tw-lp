.class public Lfkc;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhhn;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:Lfnc;

.field public g:Lflh;

.field public h:Lfkp;

.field private final i:Lfqh;

.field private j:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhhn;Lfqh;)V
    .locals 5

    .prologue
    .line 37
    invoke-direct {p0}, Lfic;-><init>()V

    .line 38
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhhn;

    iput-object v0, p0, Lfkc;->a:Lhhn;

    .line 39
    iput-object p2, p0, Lfkc;->i:Lfqh;

    .line 43
    iget-object v1, p1, Lhhn;->j:[Lhxe;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 46
    iget-object v4, v3, Lhxe;->b:Lhnx;

    if-eqz v4, :cond_1

    .line 47
    new-instance v0, Lfkv;

    iget-object v1, v3, Lhxe;->b:Lhnx;

    invoke-direct {v0, v1, p0}, Lfkv;-><init>(Lhnx;Lfqh;)V

    .line 51
    :cond_0
    return-void

    .line 45
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 166
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 167
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lfkc;->a:Lhhn;

    iget-object v0, v0, Lhhn;->g:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lfkc;->j:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lfkc;->a:Lhhn;

    iget-object v0, v0, Lhhn;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfkc;->j:Ljava/lang/CharSequence;

    .line 62
    :cond_0
    iget-object v0, p0, Lfkc;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lfkc;->a:Lhhn;

    iget-object v0, v0, Lhhn;->i:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lfkc;->i:Lfqh;

    return-object v0
.end method
