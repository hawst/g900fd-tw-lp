.class public final enum Lgog;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final enum a:Lgog;

.field public static final enum b:Lgog;

.field public static final enum c:Lgog;

.field public static final enum d:Lgog;

.field private static final synthetic f:[Lgog;


# instance fields
.field public final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lgog;

    const-string v1, "NO_FEATURE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lgog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lgog;->a:Lgog;

    .line 18
    new-instance v0, Lgog;

    const-string v1, "EXTERNAL_URL"

    const-string v2, "external"

    invoke-direct {v0, v1, v4, v2}, Lgog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lgog;->b:Lgog;

    .line 19
    new-instance v0, Lgog;

    const-string v1, "MY_UPLOADS"

    const-string v2, "upl"

    invoke-direct {v0, v1, v5, v2}, Lgog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lgog;->c:Lgog;

    .line 20
    new-instance v0, Lgog;

    const-string v1, "REMOTE_QUEUE"

    const-string v2, "remote_queue"

    invoke-direct {v0, v1, v6, v2}, Lgog;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lgog;->d:Lgog;

    .line 13
    const/4 v0, 0x4

    new-array v0, v0, [Lgog;

    sget-object v1, Lgog;->a:Lgog;

    aput-object v1, v0, v3

    sget-object v1, Lgog;->b:Lgog;

    aput-object v1, v0, v4

    sget-object v1, Lgog;->c:Lgog;

    aput-object v1, v0, v5

    sget-object v1, Lgog;->d:Lgog;

    aput-object v1, v0, v6

    sput-object v0, Lgog;->f:[Lgog;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput-object p3, p0, Lgog;->e:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static a(I)Lgog;
    .locals 1

    .prologue
    .line 29
    if-ltz p0, :cond_0

    invoke-static {}, Lgog;->values()[Lgog;

    move-result-object v0

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 30
    :cond_0
    sget-object v0, Lgog;->a:Lgog;

    .line 32
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lgog;->values()[Lgog;

    move-result-object v0

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lgog;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lgog;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgog;

    return-object v0
.end method

.method public static values()[Lgog;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lgog;->f:[Lgog;

    invoke-virtual {v0}, [Lgog;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgog;

    return-object v0
.end method
