.class public final Lbjx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Lbjy;

.field private static final d:Lbjy;


# instance fields
.field public final a:Landroid/content/SharedPreferences;

.field public final b:Ljava/util/List;

.field private final e:Lbjy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lbjy;->b:Lbjy;

    sput-object v0, Lbjx;->c:Lbjy;

    .line 29
    sget-object v0, Lbjy;->b:Lbjy;

    sput-object v0, Lbjx;->d:Lbjy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcyc;Landroid/content/SharedPreferences;)V
    .locals 3

    .prologue
    .line 41
    .line 42
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    .line 44
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 45
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lezk;

    .line 43
    invoke-static {v1, v2}, La;->a(Landroid/content/Context;Lezk;)Z

    move-result v1

    .line 41
    invoke-direct {p0, v0, v1}, Lbjx;-><init>(Landroid/content/SharedPreferences;Z)V

    .line 46
    return-void
.end method

.method private constructor <init>(Landroid/content/SharedPreferences;Z)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lbjx;->a:Landroid/content/SharedPreferences;

    .line 51
    invoke-static {p2}, Lbjx;->a(Z)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbjx;->b:Ljava/util/List;

    .line 52
    if-eqz p2, :cond_0

    sget-object v0, Lbjx;->d:Lbjy;

    :goto_0
    iput-object v0, p0, Lbjx;->e:Lbjy;

    .line 54
    return-void

    .line 52
    :cond_0
    sget-object v0, Lbjx;->c:Lbjy;

    goto :goto_0
.end method

.method private a(Lbjy;)Lbjy;
    .locals 4

    .prologue
    .line 114
    iget-object v0, p0, Lbjx;->a:Landroid/content/SharedPreferences;

    const-string v1, "offline_quality"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_1

    .line 117
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 118
    iget-object v0, p0, Lbjx;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjy;

    .line 119
    iget-object v3, v0, Lbjy;->c:Lflj;

    iget v3, v3, Lflj;->d:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v3, v1, :cond_0

    .line 128
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method

.method private static a(Z)Ljava/util/List;
    .locals 7

    .prologue
    .line 58
    invoke-static {}, Lbjy;->values()[Lbjy;

    move-result-object v1

    .line 59
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 61
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    .line 64
    iget-object v5, v4, Lbjy;->c:Lflj;

    iget v5, v5, Lflj;->d:I

    if-lez v5, :cond_1

    if-nez p0, :cond_0

    .line 65
    iget-object v5, v4, Lbjy;->c:Lflj;

    iget v5, v5, Lflj;->d:I

    const/16 v6, 0x2d0

    if-ge v5, v6, :cond_1

    .line 66
    :cond_0
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_2
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private b(Lgit;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 179
    const-string v0, "offline_resync_interval_%s"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v2, p1, Lgit;->c:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 184
    iget-object v1, p0, Lbjx;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 185
    const-string v1, "offline_resync_interval_%s"

    new-array v2, v3, [Ljava/lang/Object;

    .line 187
    invoke-static {p1}, Lctv;->a(Lgit;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    .line 185
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 188
    iget-object v2, p0, Lbjx;->a:Landroid/content/SharedPreferences;

    const-wide/16 v4, 0x0

    invoke-static {v2, v1, v0, v4, v5}, Lfaq;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;J)V

    .line 191
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lgit;)J
    .locals 4

    .prologue
    .line 163
    iget-object v0, p0, Lbjx;->a:Landroid/content/SharedPreferences;

    invoke-direct {p0, p1}, Lbjx;->b(Lgit;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lgit;J)V
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lbjx;->a:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 174
    invoke-direct {p0, p1}, Lbjx;->b(Lgit;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 175
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 176
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 83
    iget-object v1, p0, Lbjx;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lflh;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 93
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lbjx;->a(Lbjy;)Lbjy;

    move-result-object v2

    .line 94
    if-eqz p1, :cond_2

    .line 95
    if-eqz v2, :cond_0

    .line 96
    invoke-virtual {p1}, Lflh;->a()Ljava/util/Map;

    move-result-object v3

    iget-object v2, v2, Lbjy;->c:Lflj;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 96
    goto :goto_0

    .line 101
    :cond_2
    if-nez v2, :cond_3

    invoke-virtual {p0}, Lbjx;->a()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b()Lbjy;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lbjx;->e:Lbjy;

    invoke-direct {p0, v0}, Lbjx;->a(Lbjy;)Lbjy;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lbjx;->a:Landroid/content/SharedPreferences;

    const-string v1, "offline_policy"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
