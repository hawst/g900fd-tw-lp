.class public final Lfdw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lfdx;

.field private final b:Lfdz;

.field private final c:Ljava/util/List;

.field private final d:Landroid/os/Handler;

.field private e:Lfdy;


# direct methods
.method public constructor <init>(Lfdz;Landroid/os/Handler;Lfdx;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdz;

    iput-object v0, p0, Lfdw;->b:Lfdz;

    .line 93
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lfdw;->d:Landroid/os/Handler;

    .line 94
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdx;

    iput-object v0, p0, Lfdw;->a:Lfdx;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfdw;->c:Ljava/util/List;

    .line 96
    return-void
.end method

.method public static a()Lhkd;
    .locals 4

    .prologue
    .line 370
    new-instance v0, Lhkd;

    invoke-direct {v0}, Lhkd;-><init>()V

    .line 371
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lhkd;->e:J

    .line 372
    return-object v0
.end method

.method private static a(Lfqh;)Liaj;
    .locals 2

    .prologue
    .line 379
    new-instance v0, Liaj;

    invoke-direct {v0}, Liaj;-><init>()V

    .line 380
    invoke-interface {p0}, Lfqh;->d()[B

    move-result-object v1

    if-eqz v1, :cond_0

    .line 381
    invoke-interface {p0}, Lfqh;->d()[B

    move-result-object v1

    iput-object v1, v0, Liaj;->b:[B

    .line 383
    :cond_0
    return-object v0
.end method

.method public static a(Lfqi;)Liaj;
    .locals 2

    .prologue
    .line 391
    new-instance v0, Liaj;

    invoke-direct {v0}, Liaj;-><init>()V

    .line 392
    iget v1, p0, Lfqi;->l:I

    iput v1, v0, Liaj;->c:I

    .line 393
    return-object v0
.end method

.method public static a(Ljava/lang/String;[BLfqi;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 483
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    .line 484
    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 486
    :goto_0
    invoke-virtual {p2}, Lfqi;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 487
    iget v2, p2, Lfqi;->l:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xe

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 488
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " req: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " elt: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " csn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 490
    return-void

    .line 484
    :cond_0
    const-string v0, "null"

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;[B[BLjava/lang/String;Lfqh;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 466
    if-eqz p1, :cond_0

    .line 467
    invoke-static {p1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 469
    :goto_0
    if-eqz p2, :cond_1

    .line 470
    invoke-static {p2, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 473
    :goto_1
    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " req: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " el: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " csn: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " class: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 472
    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 474
    return-void

    .line 467
    :cond_0
    const-string v0, "null"

    move-object v1, v0

    goto :goto_0

    .line 470
    :cond_1
    const-string v0, "null"

    goto :goto_1
.end method

.method public static a([B)Z
    .locals 1

    .prologue
    .line 515
    if-eqz p0, :cond_0

    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b([B)Z
    .locals 1

    .prologue
    .line 508
    if-eqz p0, :cond_0

    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfqg;Lfqh;Lhcq;)V
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-interface {p2}, Lfqh;->d()[B

    move-result-object v0

    invoke-static {v0}, Lfdw;->b([B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    invoke-static {}, Lfdw;->a()Lhkd;

    move-result-object v0

    .line 247
    new-instance v1, Lhcn;

    invoke-direct {v1}, Lhcn;-><init>()V

    iput-object v1, v0, Lhkd;->d:Lhcn;

    .line 248
    iget-object v1, p1, Lfqg;->a:[B

    invoke-static {v1}, Lfdw;->a([B)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 249
    iget-object v1, v0, Lhkd;->d:Lhcn;

    iget-object v2, p1, Lfqg;->a:[B

    iput-object v2, v1, Lhcn;->a:[B

    .line 251
    :cond_2
    iget-object v1, v0, Lhkd;->d:Lhcn;

    invoke-static {p2}, Lfdw;->a(Lfqh;)Liaj;

    move-result-object v2

    iput-object v2, v1, Lhcn;->c:Liaj;

    .line 252
    iget-object v1, v0, Lhkd;->d:Lhcn;

    iget-object v2, p1, Lfqg;->b:Ljava/lang/String;

    iput-object v2, v1, Lhcn;->b:Ljava/lang/String;

    .line 253
    invoke-virtual {p0, v0}, Lfdw;->a(Lhkd;)V

    .line 258
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    const-string v0, "CLICK:"

    .line 260
    iget-object v1, p1, Lfqg;->a:[B

    .line 261
    invoke-interface {p2}, Lfqh;->d()[B

    move-result-object v2

    .line 262
    iget-object v3, p1, Lfqg;->b:Ljava/lang/String;

    .line 259
    invoke-static {v0, v1, v2, v3, p2}, Lfdw;->a(Ljava/lang/String;[B[BLjava/lang/String;Lfqh;)V

    goto :goto_0
.end method

.method public final a(Lfqg;Lfqi;Lhcq;)V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->S()Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    invoke-static {}, Lfdw;->a()Lhkd;

    move-result-object v0

    .line 116
    new-instance v1, Lhuf;

    invoke-direct {v1}, Lhuf;-><init>()V

    iput-object v1, v0, Lhkd;->c:Lhuf;

    .line 117
    iget-object v1, v0, Lhkd;->c:Lhuf;

    iget v2, p2, Lfqi;->l:I

    iput v2, v1, Lhuf;->a:I

    .line 118
    iget-object v1, p1, Lfqg;->a:[B

    invoke-static {v1}, Lfdw;->a([B)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 119
    iget-object v1, v0, Lhkd;->c:Lhuf;

    .line 120
    iget-object v2, p1, Lfqg;->a:[B

    iput-object v2, v1, Lhuf;->b:[B

    .line 122
    :cond_2
    iget-object v1, v0, Lhkd;->c:Lhuf;

    iget-object v2, p1, Lfqg;->b:Ljava/lang/String;

    iput-object v2, v1, Lhuf;->c:Ljava/lang/String;

    .line 123
    invoke-virtual {p0, v0}, Lfdw;->a(Lhkd;)V

    .line 127
    sget-object v0, Lfqi;->a:Lfqi;

    if-eq p2, v0, :cond_3

    iget-object v0, p1, Lfqg;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 129
    :cond_3
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    const-string v0, "GRAFT:"

    .line 131
    iget-object v1, p1, Lfqg;->a:[B

    .line 133
    iget-object v2, p1, Lfqg;->b:Ljava/lang/String;

    .line 130
    invoke-static {v0, v1, p2, v2}, Lfdw;->a(Ljava/lang/String;[BLfqi;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lhkd;)V
    .locals 4

    .prologue
    .line 403
    iget-object v0, p0, Lfdw;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 404
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->U()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfdw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lfdw;->a:Lfdx;

    invoke-interface {v1}, Lfdx;->U()I

    move-result v1

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lfdw;->b()V

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 404
    :cond_1
    iget-object v0, p0, Lfdw;->e:Lfdy;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfdw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->T()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lfdw;->b()V

    goto :goto_0

    :cond_2
    new-instance v0, Lfdy;

    invoke-direct {v0, p0}, Lfdy;-><init>(Lfdw;)V

    iput-object v0, p0, Lfdw;->e:Lfdy;

    iget-object v0, p0, Lfdw;->d:Landroid/os/Handler;

    iget-object v1, p0, Lfdw;->e:Lfdy;

    iget-object v2, p0, Lfdw;->a:Lfdx;

    invoke-interface {v2}, Lfdx;->T()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method b()V
    .locals 4

    .prologue
    .line 438
    iget-object v0, p0, Lfdw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 456
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v0, p0, Lfdw;->b:Lfdz;

    new-instance v1, Lfea;

    iget-object v2, v0, Lfdz;->b:Lfsz;

    iget-object v0, v0, Lfdz;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lfea;-><init>(Lfsz;Lgit;)V

    .line 442
    iget-object v0, p0, Lfdw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkd;

    .line 443
    iget-object v3, v1, Lfea;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 446
    :cond_2
    iget-object v0, p0, Lfdw;->b:Lfdz;

    const-class v2, Lhke;

    .line 448
    invoke-static {v2}, La;->a(Ljava/lang/Class;)Lwv;

    move-result-object v2

    .line 446
    iget-object v0, v0, Lfdz;->e:Lfco;

    invoke-virtual {v0, v1, v2}, Lfco;->a(Lfsp;Lwv;)V

    .line 451
    iget-object v0, p0, Lfdw;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 452
    iget-object v0, p0, Lfdw;->e:Lfdy;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lfdw;->d:Landroid/os/Handler;

    iget-object v1, p0, Lfdw;->e:Lfdy;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 454
    const/4 v0, 0x0

    iput-object v0, p0, Lfdw;->e:Lfdy;

    goto :goto_0
.end method

.method public final b(Lfqg;Lfqh;Lhcq;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 312
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->S()Z

    move-result v0

    if-nez v0, :cond_1

    .line 319
    :cond_0
    return-void

    .line 315
    :cond_1
    :goto_0
    if-eqz p2, :cond_0

    .line 316
    const/4 v2, 0x0

    invoke-interface {p2}, Lfqh;->d()[B

    move-result-object v0

    invoke-static {v0}, Lfdw;->b([B)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2}, Lfqh;->d()[B

    move-result-object v0

    invoke-static {v0}, Lfqg;->a([B)Z

    move-result v3

    if-nez v3, :cond_6

    move v0, v1

    :goto_1
    if-nez v0, :cond_5

    invoke-static {}, Lfdw;->a()Lhkd;

    move-result-object v0

    new-instance v3, Liae;

    invoke-direct {v3}, Liae;-><init>()V

    iput-object v3, v0, Lhkd;->b:Liae;

    iget-object v3, v0, Lhkd;->b:Liae;

    const/4 v4, 0x1

    new-array v4, v4, [Liaj;

    iput-object v4, v3, Liae;->a:[Liaj;

    iget-object v3, v0, Lhkd;->b:Liae;

    iget-object v3, v3, Liae;->a:[Liaj;

    invoke-static {p2}, Lfdw;->a(Lfqh;)Liaj;

    move-result-object v4

    aput-object v4, v3, v1

    iget-object v3, p1, Lfqg;->a:[B

    invoke-static {v3}, Lfdw;->a([B)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lhkd;->b:Liae;

    iget-object v4, p1, Lfqg;->a:[B

    iput-object v4, v3, Liae;->b:[B

    :cond_2
    iget-object v3, v0, Lhkd;->b:Liae;

    iget-object v4, p1, Lfqg;->b:Ljava/lang/String;

    iput-object v4, v3, Liae;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    iput-object v2, v0, Lhkd;->g:Lhcq;

    :cond_3
    invoke-virtual {p0, v0}, Lfdw;->a(Lhkd;)V

    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->V()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "VISIB:"

    iget-object v2, p1, Lfqg;->a:[B

    invoke-interface {p2}, Lfqh;->d()[B

    move-result-object v3

    iget-object v4, p1, Lfqg;->b:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4, p2}, Lfdw;->a(Ljava/lang/String;[B[BLjava/lang/String;Lfqh;)V

    :cond_4
    invoke-interface {p2}, Lfqh;->d()[B

    move-result-object v0

    invoke-static {v0}, Lfqg;->a([B)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p1, Lfqg;->c:Ljava/util/Set;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 317
    :cond_5
    invoke-interface {p2}, Lfqh;->e()Lfqh;

    move-result-object p2

    goto :goto_0

    .line 316
    :cond_6
    iget-object v3, p1, Lfqg;->c:Ljava/util/Set;

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1
.end method

.method public final b(Lfqg;Lfqi;Lhcq;)V
    .locals 3

    .prologue
    .line 275
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->S()Z

    move-result v0

    if-nez v0, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    invoke-static {}, Lfdw;->a()Lhkd;

    move-result-object v0

    .line 279
    new-instance v1, Lhcn;

    invoke-direct {v1}, Lhcn;-><init>()V

    iput-object v1, v0, Lhkd;->d:Lhcn;

    .line 280
    iget-object v1, p1, Lfqg;->a:[B

    invoke-static {v1}, Lfdw;->a([B)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 281
    iget-object v1, v0, Lhkd;->d:Lhcn;

    iget-object v2, p1, Lfqg;->a:[B

    iput-object v2, v1, Lhcn;->a:[B

    .line 283
    :cond_2
    iget-object v1, v0, Lhkd;->d:Lhcn;

    invoke-static {p2}, Lfdw;->a(Lfqi;)Liaj;

    move-result-object v2

    iput-object v2, v1, Lhcn;->c:Liaj;

    .line 284
    iget-object v1, v0, Lhkd;->d:Lhcn;

    iget-object v2, p1, Lfqg;->b:Ljava/lang/String;

    iput-object v2, v1, Lhcn;->b:Ljava/lang/String;

    .line 285
    if-eqz p3, :cond_3

    .line 286
    iput-object p3, v0, Lhkd;->g:Lhcq;

    .line 288
    :cond_3
    invoke-virtual {p0, v0}, Lfdw;->a(Lhkd;)V

    .line 290
    iget-object v0, p0, Lfdw;->a:Lfdx;

    invoke-interface {v0}, Lfdx;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    const-string v0, "CLICK:"

    .line 292
    iget-object v1, p1, Lfqg;->a:[B

    .line 294
    iget-object v2, p1, Lfqg;->b:Ljava/lang/String;

    .line 291
    invoke-static {v0, v1, p2, v2}, Lfdw;->a(Ljava/lang/String;[BLfqi;Ljava/lang/String;)V

    goto :goto_0
.end method
