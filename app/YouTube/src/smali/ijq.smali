.class public final Lijq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liex;


# static fields
.field private static final a:[Lifb;


# instance fields
.field private final b:Likd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [Lifb;

    sput-object v0, Lijq;->a:[Lifb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Likd;

    invoke-direct {v0}, Likd;-><init>()V

    iput-object v0, p0, Lijq;->b:Likd;

    return-void
.end method


# virtual methods
.method public final a(Lieq;Ljava/util/Map;)Liez;
    .locals 12

    .prologue
    .line 72
    if-eqz p2, :cond_14

    sget-object v0, Lies;->a:Lies;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 73
    invoke-virtual {p1}, Lieq;->b()Lifx;

    move-result-object v5

    invoke-virtual {v5}, Lifx;->a()[I

    move-result-object v6

    invoke-virtual {v5}, Lifx;->b()[I

    move-result-object v7

    if-eqz v6, :cond_0

    if-nez v7, :cond_1

    :cond_0
    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_1
    iget v8, v5, Lifx;->b:I

    iget v9, v5, Lifx;->a:I

    const/4 v0, 0x0

    aget v3, v6, v0

    const/4 v0, 0x1

    aget v1, v6, v0

    const/4 v2, 0x1

    const/4 v0, 0x0

    move v4, v3

    move v3, v1

    :goto_0
    if-ge v4, v9, :cond_3

    if-ge v3, v8, :cond_3

    invoke-virtual {v5, v4, v3}, Lifx;->a(II)Z

    move-result v1

    if-eq v2, v1, :cond_18

    add-int/lit8 v1, v0, 0x1

    const/4 v0, 0x5

    if-eq v1, v0, :cond_3

    if-nez v2, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v11, v1

    move v1, v0

    move v0, v11

    :goto_2
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    if-eq v4, v9, :cond_4

    if-ne v3, v8, :cond_5

    :cond_4
    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_5
    const/4 v0, 0x0

    aget v0, v6, v0

    sub-int v0, v4, v0

    int-to-float v0, v0

    const/high16 v1, 0x40e00000    # 7.0f

    div-float v4, v0, v1

    const/4 v0, 0x1

    aget v1, v6, v0

    const/4 v0, 0x1

    aget v8, v7, v0

    const/4 v0, 0x0

    aget v2, v6, v0

    const/4 v0, 0x0

    aget v0, v7, v0

    if-ge v2, v0, :cond_6

    if-lt v1, v8, :cond_7

    :cond_6
    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_7
    sub-int v3, v8, v1

    sub-int v6, v0, v2

    if-eq v3, v6, :cond_8

    sub-int v0, v8, v1

    add-int/2addr v0, v2

    :cond_8
    sub-int v3, v0, v2

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v6

    sub-int v3, v8, v1

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v7

    if-lez v6, :cond_9

    if-gtz v7, :cond_a

    :cond_9
    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_a
    if-eq v7, v6, :cond_b

    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_b
    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v4, v3

    float-to-int v9, v3

    add-int/2addr v1, v9

    add-int/2addr v2, v9

    add-int/lit8 v3, v6, -0x1

    int-to-float v3, v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    add-int/2addr v3, v2

    add-int/lit8 v0, v0, -0x1

    sub-int v0, v3, v0

    if-lez v0, :cond_17

    if-le v0, v9, :cond_c

    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_c
    sub-int v0, v2, v0

    move v3, v0

    :goto_3
    add-int/lit8 v0, v7, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-int v0, v0

    add-int/2addr v0, v1

    add-int/lit8 v2, v8, -0x1

    sub-int/2addr v0, v2

    if-lez v0, :cond_16

    if-le v0, v9, :cond_d

    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_d
    sub-int v0, v1, v0

    :goto_4
    new-instance v8, Lifx;

    invoke-direct {v8, v6, v7}, Lifx;-><init>(II)V

    const/4 v1, 0x0

    move v2, v1

    :goto_5
    if-ge v2, v7, :cond_10

    int-to-float v1, v2

    mul-float/2addr v1, v4

    float-to-int v1, v1

    add-int v9, v0, v1

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v6, :cond_f

    int-to-float v10, v1

    mul-float/2addr v10, v4

    float-to-int v10, v10

    add-int/2addr v10, v3

    invoke-virtual {v5, v10, v9}, Lifx;->a(II)Z

    move-result v10

    if-eqz v10, :cond_e

    invoke-virtual {v8, v1, v2}, Lifx;->b(II)V

    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_f
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    .line 74
    :cond_10
    iget-object v0, p0, Lijq;->b:Likd;

    invoke-virtual {v0, v8, p2}, Likd;->a(Lifx;Ljava/util/Map;)Liga;

    move-result-object v1

    .line 75
    sget-object v0, Lijq;->a:[Lifb;

    move-object v2, v1

    move-object v1, v0

    .line 83
    :goto_7
    iget-object v0, v2, Liga;->e:Ljava/lang/Object;

    instance-of v0, v0, Likh;

    if-eqz v0, :cond_11

    .line 84
    iget-object v0, v2, Liga;->e:Ljava/lang/Object;

    check-cast v0, Likh;

    iget-boolean v0, v0, Likh;->a:Z

    if-eqz v0, :cond_11

    if-eqz v1, :cond_11

    array-length v0, v1

    const/4 v3, 0x3

    if-ge v0, v3, :cond_15

    .line 87
    :cond_11
    :goto_8
    new-instance v0, Liez;

    iget-object v3, v2, Liga;->b:Ljava/lang/String;

    iget-object v4, v2, Liga;->a:[B

    sget-object v5, Lieo;->l:Lieo;

    invoke-direct {v0, v3, v4, v1, v5}, Liez;-><init>(Ljava/lang/String;[B[Lifb;Lieo;)V

    .line 88
    iget-object v1, v2, Liga;->c:Ljava/util/List;

    .line 89
    if-eqz v1, :cond_12

    .line 90
    sget-object v3, Lifa;->b:Lifa;

    invoke-virtual {v0, v3, v1}, Liez;->a(Lifa;Ljava/lang/Object;)V

    .line 92
    :cond_12
    iget-object v1, v2, Liga;->d:Ljava/lang/String;

    .line 93
    if-eqz v1, :cond_13

    .line 94
    sget-object v2, Lifa;->c:Lifa;

    invoke-virtual {v0, v2, v1}, Liez;->a(Lifa;Ljava/lang/Object;)V

    .line 96
    :cond_13
    return-object v0

    .line 77
    :cond_14
    new-instance v0, Likn;

    invoke-virtual {p1}, Lieq;->b()Lifx;

    move-result-object v1

    invoke-direct {v0, v1}, Likn;-><init>(Lifx;)V

    invoke-virtual {v0, p2}, Likn;->a(Ljava/util/Map;)Ligc;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lijq;->b:Likd;

    iget-object v2, v0, Ligc;->d:Lifx;

    invoke-virtual {v1, v2, p2}, Likd;->a(Lifx;Ljava/util/Map;)Liga;

    move-result-object v1

    .line 79
    iget-object v0, v0, Ligc;->e:[Lifb;

    move-object v2, v1

    move-object v1, v0

    goto :goto_7

    .line 84
    :cond_15
    const/4 v0, 0x0

    aget-object v0, v1, v0

    const/4 v3, 0x0

    const/4 v4, 0x2

    aget-object v4, v1, v4

    aput-object v4, v1, v3

    const/4 v3, 0x2

    aput-object v0, v1, v3

    goto :goto_8

    :cond_16
    move v0, v1

    goto/16 :goto_4

    :cond_17
    move v3, v2

    goto/16 :goto_3

    :cond_18
    move v1, v2

    goto/16 :goto_2
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 102
    return-void
.end method
