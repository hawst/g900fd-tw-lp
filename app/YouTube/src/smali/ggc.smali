.class public final Lggc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lecv;


# instance fields
.field private final a:Lecb;

.field private final b:Lefc;

.field private final c:Lecy;

.field private final d:Leda;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:Ljava/util/HashMap;

.field private final i:Ljava/util/HashMap;

.field private final j:[Lecw;

.field private k:[Lecw;

.field private final l:Ljava/util/List;

.field private final m:Legq;

.field private n:J

.field private o:Z

.field private p:Z

.field private final q:Z


# direct methods
.method public varargs constructor <init>(Lefc;Lecy;ILjava/util/List;Legq;ZZ[Ledm;)V
    .locals 6

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lggc;->b:Lefc;

    .line 94
    iput p3, p0, Lggc;->g:I

    .line 95
    array-length v0, p8

    new-array v0, v0, [Lecw;

    iput-object v0, p0, Lggc;->k:[Lecw;

    .line 96
    iget-object v0, p0, Lggc;->k:[Lecw;

    iput-object v0, p0, Lggc;->j:[Lecw;

    .line 97
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lggc;->h:Ljava/util/HashMap;

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lggc;->i:Ljava/util/HashMap;

    .line 99
    new-instance v0, Leda;

    invoke-direct {v0}, Leda;-><init>()V

    iput-object v0, p0, Lggc;->d:Leda;

    .line 100
    iput-object p2, p0, Lggc;->c:Lecy;

    .line 101
    iput-boolean p7, p0, Lggc;->o:Z

    .line 102
    const/4 v2, 0x0

    .line 103
    const/4 v1, 0x0

    .line 104
    const/4 v0, 0x0

    :goto_0
    array-length v3, p8

    if-ge v0, v3, :cond_0

    .line 105
    iget-object v3, p0, Lggc;->k:[Lecw;

    aget-object v4, p8, v0

    iget-object v4, v4, Ledm;->b:Lecw;

    aput-object v4, v3, v0

    .line 106
    iget-object v3, p0, Lggc;->k:[Lecw;

    aget-object v3, v3, v0

    iget v3, v3, Lecw;->c:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 107
    iget-object v3, p0, Lggc;->k:[Lecw;

    aget-object v3, v3, v0

    iget v3, v3, Lecw;->d:I

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 108
    iget-object v3, p0, Lggc;->h:Ljava/util/HashMap;

    iget-object v4, p0, Lggc;->k:[Lecw;

    aget-object v4, v4, v0

    iget-object v4, v4, Lecw;->a:Ljava/lang/String;

    aget-object v5, p8, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 110
    :cond_0
    iput v2, p0, Lggc;->e:I

    .line 111
    iput v1, p0, Lggc;->f:I

    .line 112
    iget-object v0, p0, Lggc;->k:[Lecw;

    new-instance v1, Lecx;

    invoke-direct {v1}, Lecx;-><init>()V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 113
    new-instance v0, Lecb;

    const/4 v1, 0x0

    aget-object v1, p8, v1

    iget-object v1, v1, Ledm;->b:Lecw;

    iget-object v1, v1, Lecw;->b:Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v2, p8, v2

    iget-wide v2, v2, Ledm;->c:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v1, v2, v3}, Lecb;-><init>(Ljava/lang/String;J)V

    iput-object v0, p0, Lggc;->a:Lecb;

    .line 115
    iput-object p4, p0, Lggc;->l:Ljava/util/List;

    .line 116
    iput-object p5, p0, Lggc;->m:Legq;

    .line 117
    iput-boolean p6, p0, Lggc;->q:Z

    .line 118
    return-void
.end method

.method public varargs constructor <init>(Lefc;Lecy;Ljava/util/List;Legq;ZZ[Ledm;)V
    .locals 9

    .prologue
    .line 80
    const/4 v3, 0x1

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lggc;-><init>(Lefc;Lecy;ILjava/util/List;Legq;ZZ[Ledm;)V

    .line 82
    return-void
.end method

.method static a(Leee;Ledk;I)J
    .locals 6

    .prologue
    .line 404
    invoke-interface {p0}, Leee;->a()Leef;

    move-result-object v0

    iget-object v0, v0, Leef;->d:[J

    aget-wide v0, v0, p2

    .line 405
    invoke-interface {p0}, Leee;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 406
    invoke-virtual {p1}, Ledk;->a()Ledj;

    move-result-object v2

    .line 407
    iget-wide v4, v2, Ledj;->a:J

    iget-wide v2, v2, Ledj;->b:J

    add-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 409
    :cond_0
    return-wide v0
.end method

.method private a(Ledk;)Leee;
    .locals 3

    .prologue
    .line 387
    iget-object v1, p1, Ledk;->b:Lecw;

    .line 388
    iget-object v0, p0, Lggc;->i:Ljava/util/HashMap;

    iget-object v2, v1, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leee;

    .line 389
    if-nez v0, :cond_0

    .line 390
    iget-object v0, v1, Lecw;->b:Ljava/lang/String;

    const-string v2, "video/webm"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 391
    new-instance v0, Leeu;

    invoke-direct {v0}, Leeu;-><init>()V

    .line 395
    :goto_0
    iget-object v2, p0, Lggc;->i:Ljava/util/HashMap;

    iget-object v1, v1, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    :cond_0
    return-object v0

    .line 393
    :cond_1
    new-instance v0, Leek;

    invoke-direct {v0}, Leek;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lecb;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lggc;->a:Lecb;

    return-object v0
.end method

.method public final a(Lebv;)V
    .locals 2

    .prologue
    .line 122
    iget-boolean v0, p0, Lggc;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lggc;->a:Lecb;

    iget-object v0, v0, Lecb;->a:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    iget v0, p0, Lggc;->e:I

    iget v1, p0, Lggc;->f:I

    iput v0, p1, Lebv;->i:I

    iput v1, p1, Lebv;->j:I

    iget-object v0, p1, Lebv;->l:Landroid/media/MediaFormat;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lebv;->l:Landroid/media/MediaFormat;

    invoke-virtual {p1, v0}, Lebv;->a(Landroid/media/MediaFormat;)V

    .line 125
    :cond_0
    return-void
.end method

.method public final a(Lecj;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 255
    instance-of v1, p2, Lefq;

    if-nez v1, :cond_0

    instance-of v1, p2, Lefp;

    if-nez v1, :cond_0

    .line 256
    invoke-virtual {p2}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Legb;

    if-eqz v1, :cond_1

    .line 257
    :cond_0
    iget-boolean v1, p0, Lggc;->q:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lggc;->k:[Lecw;

    array-length v1, v1

    const/4 v2, 0x2

    if-lt v1, v2, :cond_1

    if-nez p1, :cond_2

    .line 259
    :cond_1
    :goto_0
    return-void

    .line 257
    :cond_2
    iget-object v1, p0, Lggc;->k:[Lecw;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    new-array v3, v1, [Lecw;

    move v1, v0

    :goto_1
    iget-object v2, p0, Lggc;->k:[Lecw;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lggc;->k:[Lecw;

    aget-object v2, v2, v0

    iget-object v2, v2, Lecw;->a:Ljava/lang/String;

    iget-object v4, p1, Lecj;->a:Lecw;

    iget-object v4, v4, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lggc;->k:[Lecw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    add-int/lit8 v2, v1, 0x1

    iget-object v4, p0, Lggc;->k:[Lecw;

    aget-object v4, v4, v0

    aput-object v4, v3, v1

    move v1, v2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iput-object v3, p0, Lggc;->k:[Lecw;

    iget-object v0, p0, Lggc;->m:Legq;

    invoke-virtual {v0}, Legq;->a()J

    move-result-wide v0

    const-wide/32 v2, 0xea60

    add-long/2addr v0, v2

    iput-wide v0, p0, Lggc;->n:J

    iget-object v0, p0, Lggc;->d:Leda;

    const/4 v1, 0x0

    iput-object v1, v0, Leda;->c:Lecw;

    goto :goto_0
.end method

.method public final a(Ljava/util/List;JJLeck;)V
    .locals 20

    .prologue
    .line 154
    move-object/from16 v0, p0

    iget-wide v2, v0, Lggc;->n:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->m:Legq;

    invoke-virtual {v2}, Legq;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lggc;->n:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 156
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lggc;->n:J

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->j:[Lecw;

    move-object/from16 v0, p0

    iput-object v2, v0, Lggc;->k:[Lecw;

    .line 161
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->d:Leda;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v2, Leda;->a:I

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->d:Leda;

    iget-object v2, v2, Leda;->c:Lecw;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lggc;->p:Z

    if-nez v2, :cond_2

    .line 163
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->c:Lecy;

    move-object/from16 v0, p0

    iget-object v6, v0, Lggc;->k:[Lecw;

    move-object/from16 v0, p0

    iget-object v7, v0, Lggc;->d:Leda;

    move-object/from16 v3, p1

    move-wide/from16 v4, p4

    invoke-interface/range {v2 .. v7}, Lecy;->a(Ljava/util/List;J[Lecw;Leda;)V

    .line 165
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->d:Leda;

    iget-object v11, v2, Leda;->c:Lecw;

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->d:Leda;

    iget v2, v2, Leda;->a:I

    move-object/from16 v0, p6

    iput v2, v0, Leck;->a:I

    .line 168
    if-nez v11, :cond_4

    .line 169
    const/4 v2, 0x0

    move-object/from16 v0, p6

    iput-object v2, v0, Leck;->b:Lecj;

    .line 251
    :cond_3
    :goto_0
    return-void

    .line 175
    :cond_4
    const/4 v2, 0x0

    move v9, v2

    .line 176
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->k:[Lecw;

    array-length v2, v2

    if-ge v9, v2, :cond_16

    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->k:[Lecw;

    array-length v2, v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_16

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->k:[Lecw;

    aget-object v10, v2, v9

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->h:Ljava/util/HashMap;

    iget-object v3, v10, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Ledm;

    .line 180
    iget-object v2, v11, Lecw;->a:Ljava/lang/String;

    iget-object v3, v10, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 181
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lggc;->a(Ledk;)Leee;

    move-result-object v2

    invoke-interface {v2}, Leee;->a()Leef;

    move-result-object v3

    .line 186
    if-eqz v3, :cond_b

    .line 188
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 189
    iget-object v2, v3, Leef;->f:[J

    move-wide/from16 v0, p2

    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v2

    .line 190
    if-gez v2, :cond_5

    neg-int v2, v2

    add-int/lit8 v2, v2, -0x2

    .line 195
    :cond_5
    :goto_2
    const/4 v4, -0x1

    if-ne v2, v4, :cond_7

    .line 196
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    .line 197
    goto :goto_1

    .line 192
    :cond_6
    move-object/from16 v0, p6

    iget v2, v0, Leck;->a:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ledc;

    iget v2, v2, Ledc;->h:I

    goto :goto_2

    .line 199
    :cond_7
    const/16 v4, 0xa

    iget v5, v3, Leef;->b:I

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v12, v4, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lggc;->a(Ledk;)Leee;

    move-result-object v4

    invoke-static {v4, v8, v2}, Lggc;->a(Leee;Ledk;I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    :goto_3
    if-gt v2, v12, :cond_8

    iget-object v13, v3, Leef;->c:[I

    aget v13, v13, v2

    int-to-long v14, v13

    add-long/2addr v6, v14

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    new-instance v2, Lefg;

    iget-object v3, v8, Ledm;->e:Landroid/net/Uri;

    invoke-virtual {v8}, Ledm;->b()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    move-object v8, v2

    .line 208
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Legc;

    iget-object v3, v8, Lefg;->f:Ljava/lang/String;

    iget-wide v4, v8, Lefg;->d:J

    iget-wide v6, v8, Lefg;->e:J

    invoke-interface/range {v2 .. v7}, Legc;->b(Ljava/lang/String;JJ)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_d

    move-object v2, v10

    .line 215
    :goto_6
    move-object/from16 v0, p6

    iget v3, v0, Leck;->a:I

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    if-ne v3, v4, :cond_a

    move-object/from16 v0, p6

    iget-object v3, v0, Leck;->b:Lecj;

    if-eqz v3, :cond_a

    move-object/from16 v0, p6

    iget-object v3, v0, Leck;->b:Lecj;

    iget-object v3, v3, Lecj;->a:Lecw;

    iget-object v3, v3, Lecw;->a:Ljava/lang/String;

    iget-object v4, v2, Lecw;->a:Ljava/lang/String;

    .line 217
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 223
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lggc;->h:Ljava/util/HashMap;

    iget-object v2, v2, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Ledm;

    .line 224
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lggc;->a(Ledk;)Leee;

    move-result-object v2

    invoke-interface {v2}, Leee;->c()Lebv;

    move-result-object v2

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_7
    if-nez v2, :cond_f

    .line 225
    move-object/from16 v0, p0

    iget-object v10, v0, Lggc;->b:Lefc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lggc;->d:Leda;

    iget v11, v2, Leda;->b:I

    iget-object v2, v9, Ledk;->d:Ledj;

    iget-object v3, v9, Ledm;->g:Ledj;

    invoke-virtual {v2, v3}, Ledj;->a(Ledj;)Ledj;

    move-result-object v6

    new-instance v2, Lefg;

    invoke-virtual {v6}, Ledj;->a()Landroid/net/Uri;

    move-result-object v3

    iget-wide v4, v6, Ledj;->a:J

    iget-wide v6, v6, Ledj;->b:J

    invoke-virtual {v9}, Ledm;->b()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    new-instance v3, Lggd;

    move-object/from16 v0, p0

    iget-object v4, v0, Lggc;->i:Ljava/util/HashMap;

    iget-object v5, v9, Ledm;->b:Lecw;

    iget-object v5, v5, Lecw;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Leee;

    move-object v4, v10

    move-object v5, v2

    move v6, v11

    move-object v8, v9

    invoke-direct/range {v3 .. v8}, Lggd;-><init>(Lefc;Lefg;ILeee;Ledm;)V

    .line 229
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lggc;->p:Z

    .line 230
    move-object/from16 v0, p6

    iput-object v3, v0, Leck;->b:Lecj;

    goto/16 :goto_0

    .line 203
    :cond_b
    iget-object v2, v8, Ledk;->d:Ledj;

    .line 204
    iget-object v3, v8, Ledm;->g:Ledj;

    invoke-virtual {v2, v3}, Ledj;->a(Ledj;)Ledj;

    move-result-object v6

    .line 205
    new-instance v2, Lefg;

    invoke-virtual {v6}, Ledj;->a()Landroid/net/Uri;

    move-result-object v3

    iget-wide v4, v6, Ledj;->a:J

    iget-wide v6, v6, Ledj;->b:J

    .line 206
    invoke-virtual {v8}, Ledm;->b()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    move-object v8, v2

    goto/16 :goto_4

    .line 208
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 212
    :cond_d
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    .line 213
    goto/16 :goto_1

    .line 224
    :cond_e
    const/4 v2, 0x0

    goto :goto_7

    .line 235
    :cond_f
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    .line 236
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lggc;->a(Ledk;)Leee;

    move-result-object v2

    invoke-interface {v2}, Leee;->a()Leef;

    move-result-object v2

    .line 237
    iget-object v2, v2, Leef;->f:[J

    move-wide/from16 v0, p2

    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v2

    .line 238
    if-gez v2, :cond_10

    neg-int v2, v2

    add-int/lit8 v2, v2, -0x2

    .line 243
    :cond_10
    :goto_8
    const/4 v3, -0x1

    if-ne v2, v3, :cond_12

    .line 244
    const/4 v2, 0x0

    move-object/from16 v0, p6

    iput-object v2, v0, Leck;->b:Lecj;

    goto/16 :goto_0

    .line 240
    :cond_11
    move-object/from16 v0, p6

    iget v2, v0, Leck;->a:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ledc;

    iget v2, v2, Ledc;->h:I

    goto :goto_8

    .line 248
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lggc;->d:Leda;

    iget v0, v3, Leda;->b:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lggc;->a(Ledk;)Leee;

    move-result-object v3

    invoke-interface {v3}, Leee;->a()Leef;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lggc;->g:I

    iget v5, v3, Leef;->b:I

    sub-int/2addr v5, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    add-int/2addr v4, v2

    add-int/lit8 v8, v4, -0x1

    iget v4, v3, Leef;->b:I

    add-int/lit8 v4, v4, -0x1

    if-ne v8, v4, :cond_13

    const/4 v12, -0x1

    :goto_9
    iget-object v4, v3, Leef;->f:[J

    aget-wide v18, v4, v2

    const/4 v4, -0x1

    if-ne v12, v4, :cond_14

    iget-object v4, v3, Leef;->f:[J

    aget-wide v4, v4, v8

    iget-object v6, v3, Leef;->e:[J

    aget-wide v6, v6, v8

    add-long v10, v4, v6

    :goto_a
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lggc;->a(Ledk;)Leee;

    move-result-object v4

    invoke-static {v4, v9, v2}, Lggc;->a(Leee;Ledk;I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    :goto_b
    if-gt v2, v8, :cond_15

    iget-object v13, v3, Leef;->c:[I

    aget v13, v13, v2

    int-to-long v14, v13

    add-long/2addr v6, v14

    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    :cond_13
    add-int/lit8 v12, v8, 0x1

    goto :goto_9

    :cond_14
    iget-object v4, v3, Leef;->f:[J

    aget-wide v10, v4, v12

    goto :goto_a

    :cond_15
    new-instance v2, Lefg;

    iget-object v3, v9, Ledm;->e:Landroid/net/Uri;

    invoke-virtual {v9}, Ledm;->b()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    new-instance v3, Ledd;

    move-object/from16 v0, p0

    iget-object v4, v0, Lggc;->b:Lefc;

    iget-object v6, v9, Ledm;->b:Lecw;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lggc;->a(Ledk;)Leee;

    move-result-object v13

    const/4 v14, 0x0

    const-wide/16 v15, 0x0

    move-object v5, v2

    move/from16 v7, v17

    move-wide/from16 v8, v18

    invoke-direct/range {v3 .. v16}, Ledd;-><init>(Lefc;Lefg;Lecw;IJJILeee;ZJ)V

    .line 249
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lggc;->p:Z

    .line 250
    move-object/from16 v0, p6

    iput-object v3, v0, Leck;->b:Lecj;

    goto/16 :goto_0

    :cond_16
    move-object v2, v11

    goto/16 :goto_6
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lggc;->c:Lecy;

    .line 135
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lggc;->c:Lecy;

    .line 140
    return-void
.end method
