.class public final Lcfn;
.super Lfsa;
.source "SourceFile"


# instance fields
.field private final a:Leyp;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/ImageView;

.field private h:Lbxd;


# direct methods
.method public constructor <init>(Lbhz;Lffs;Lgix;Lcub;Leyp;Lfhz;Levn;Leyt;Lfdw;Lfrz;)V
    .locals 13

    .prologue
    .line 58
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-direct {p0, v0, v1}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 59
    invoke-static/range {p5 .. p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Leyp;

    iput-object v2, p0, Lcfn;->a:Leyp;

    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcfn;->b:Landroid/view/LayoutInflater;

    .line 61
    iget-object v2, p0, Lcfn;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f040086

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcfn;->c:Landroid/view/View;

    .line 62
    iget-object v2, p0, Lcfn;->c:Landroid/view/View;

    const v3, 0x7f08020f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcfn;->d:Landroid/widget/TextView;

    .line 63
    iget-object v2, p0, Lcfn;->c:Landroid/view/View;

    const v3, 0x7f080211

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcfn;->e:Landroid/widget/TextView;

    .line 64
    iget-object v2, p0, Lcfn;->c:Landroid/view/View;

    const v3, 0x7f080210

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcfn;->f:Landroid/widget/TextView;

    .line 65
    iget-object v2, p0, Lcfn;->c:Landroid/view/View;

    const v3, 0x7f08020d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcfn;->g:Landroid/widget/ImageView;

    .line 67
    new-instance v2, Lbxd;

    new-instance v3, Lbqq;

    iget-object v4, p0, Lcfn;->c:Landroid/view/View;

    const v5, 0x7f0801ec

    .line 68
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lbqq;-><init>(Landroid/view/View;Z)V

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p8

    move-object/from16 v9, p7

    move-object/from16 v10, p6

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    invoke-direct/range {v2 .. v12}, Lbxd;-><init>(Lbxi;Landroid/app/Activity;Lffs;Lgix;Lcub;Leyt;Levn;Lfhz;Lfdw;Lfrz;)V

    iput-object v2, p0, Lcfn;->h:Lbxd;

    .line 79
    return-void
.end method

.method private a(Lfky;)Landroid/view/View;
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p1}, Lfky;->a()Lfnc;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 84
    new-instance v0, Lfvi;

    iget-object v1, p0, Lcfn;->a:Leyp;

    iget-object v2, p0, Lcfn;->g:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    .line 86
    invoke-virtual {p1}, Lfky;->a()Lfnc;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lfvi;->a(Lfnc;Leyo;)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcfn;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lfky;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p1, Lfky;->a:Lhoa;

    iget-object v1, v1, Lhoa;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfky;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p1, Lfky;->b:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcfn;->e:Landroid/widget/TextView;

    iget-object v1, p1, Lfky;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p1, Lfky;->a:Lhoa;

    iget-object v1, v1, Lhoa;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfky;->c:Ljava/lang/CharSequence;

    :cond_2
    iget-object v1, p1, Lfky;->c:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lcfn;->f:Landroid/widget/TextView;

    iget-object v1, p1, Lfky;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p1, Lfky;->a:Lhoa;

    iget-object v1, v1, Lhoa;->f:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfky;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v1, p1, Lfky;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 91
    iget-object v0, p0, Lcfn;->h:Lbxd;

    iget-object v1, p1, Lfky;->e:Lfmy;

    if-nez v1, :cond_4

    iget-object v1, p1, Lfky;->a:Lhoa;

    iget-object v1, v1, Lhoa;->d:Lhoc;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lfky;->a:Lhoa;

    iget-object v1, v1, Lhoa;->d:Lhoc;

    iget-object v1, v1, Lhoc;->a:Lhwp;

    if-eqz v1, :cond_4

    new-instance v1, Lfmy;

    iget-object v2, p1, Lfky;->a:Lhoa;

    iget-object v2, v2, Lhoa;->d:Lhoc;

    iget-object v2, v2, Lhoc;->a:Lhwp;

    invoke-direct {v1, v2, p1}, Lfmy;-><init>(Lhwp;Lfqh;)V

    iput-object v1, p1, Lfky;->e:Lfmy;

    :cond_4
    iget-object v1, p1, Lfky;->e:Lfmy;

    invoke-virtual {v0, v1}, Lbxd;->a(Lfmy;)V

    .line 93
    iget-object v0, p0, Lcfn;->c:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    check-cast p2, Lfky;

    invoke-direct {p0, p2}, Lcfn;->a(Lfky;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 36
    check-cast p2, Lfky;

    invoke-direct {p0, p2}, Lcfn;->a(Lfky;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
