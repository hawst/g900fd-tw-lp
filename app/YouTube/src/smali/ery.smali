.class public final Lery;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Letb;


# static fields
.field public static final b:J


# instance fields
.field public final a:J

.field public c:J

.field public final d:Ljava/lang/String;

.field private final e:Landroid/content/Context;

.field private final f:Lezs;

.field private final g:Lezs;

.field private final h:Lgeq;

.field private final i:Lezj;

.field private volatile j:Lehb;

.field private volatile k:Landroid/util/Pair;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 55
    const/4 v0, 0x5

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x90

    aput v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0xf0

    aput v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x168

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x2d0

    aput v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x438

    aput v2, v0, v1

    .line 87
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lery;->b:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lgeq;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lezj;J)V
    .locals 3

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lery;->e:Landroid/content/Context;

    .line 133
    invoke-static {p3}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 134
    invoke-static {p4}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 135
    invoke-static {p5}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 136
    iput-object p6, p0, Lery;->i:Lezj;

    .line 137
    const-wide/16 v0, 0x0

    cmp-long v0, p7, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 138
    iput-wide p7, p0, Lery;->a:J

    .line 139
    iput-wide p7, p0, Lery;->c:J

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lery;->k:Landroid/util/Pair;

    .line 142
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeq;

    iput-object v0, p0, Lery;->h:Lgeq;

    .line 143
    const-string v0, "a."

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lery;->d:Ljava/lang/String;

    .line 144
    new-instance v0, Lerz;

    invoke-direct {v0, p0, p4, p5}, Lerz;-><init>(Lery;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lery;->f:Lezs;

    .line 159
    new-instance v0, Lesa;

    invoke-direct {v0, p0}, Lesa;-><init>(Lery;)V

    iput-object v0, p0, Lery;->g:Lezs;

    .line 170
    return-void

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic a(Lery;Lehb;)Lehb;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lery;->j:Lehb;

    return-object p1
.end method

.method static synthetic a(Lery;)V
    .locals 3

    .prologue
    .line 41
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v0, Lehb;

    iget-object v1, p0, Lery;->d:Ljava/lang/String;

    iget-object v2, p0, Lery;->e:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lehb;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, Lery;->j:Lehb;

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lesb;

    invoke-direct {v2, p0, v0}, Lesb;-><init>(Lery;Landroid/os/ConditionVariable;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    goto :goto_0
.end method

.method static synthetic b(Lery;)Lehb;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lery;->j:Lehb;

    return-object v0
.end method

.method static synthetic c(Lery;)Lezs;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lery;->f:Lezs;

    return-object v0
.end method

.method static synthetic d(Lery;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lery;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lery;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lery;->e:Landroid/content/Context;

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 251
    :try_start_0
    iget-object v0, p0, Lery;->f:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehb;

    iget-object v1, p0, Lery;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lehb;->a(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 254
    :goto_0
    return-object v0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    const-string v1, "RemoteException when using AdShieldClient.getAdRequestSignals()"

    invoke-static {v1, v0}, Lezp;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 254
    const-string v0, "13"

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 312
    invoke-static {p1}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    const-string v1, "sdkv"

    iget-object v2, p0, Lery;->d:Ljava/lang/String;

    .line 313
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "video_format"

    .line 314
    invoke-virtual {p0}, Lery;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "output"

    const-string v2, "xml_vast2"

    .line 315
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    .line 316
    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Map;
    .locals 3

    .prologue
    .line 217
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 218
    invoke-virtual {p0}, Lery;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lery;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 8

    .prologue
    .line 231
    iget-object v0, p0, Lery;->i:Lezj;

    if-nez v0, :cond_0

    .line 232
    invoke-direct {p0}, Lery;->e()Ljava/lang/String;

    move-result-object v0

    .line 246
    :goto_0
    return-object v0

    .line 234
    :cond_0
    iget-object v2, p0, Lery;->k:Landroid/util/Pair;

    .line 235
    if-eqz v2, :cond_1

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 236
    :goto_1
    if-eqz v2, :cond_2

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 237
    :goto_2
    if-eqz v1, :cond_3

    .line 238
    iget-object v0, p0, Lery;->i:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v4

    .line 239
    cmp-long v0, v4, v2

    if-lez v0, :cond_3

    iget-wide v6, p0, Lery;->c:J

    add-long/2addr v2, v6

    cmp-long v0, v4, v2

    if-gtz v0, :cond_3

    move-object v0, v1

    .line 240
    goto :goto_0

    .line 235
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 236
    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_2

    .line 243
    :cond_3
    invoke-direct {p0}, Lery;->e()Ljava/lang/String;

    move-result-object v0

    .line 244
    iget-object v1, p0, Lery;->i:Lezj;

    invoke-virtual {v1}, Lezj;->b()J

    move-result-wide v2

    .line 245
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    iput-object v1, p0, Lery;->k:Landroid/util/Pair;

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 331
    :try_start_0
    iget-object v0, p0, Lery;->f:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lehb;

    invoke-virtual {v0, p1}, Lehb;->a(Landroid/net/Uri;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 334
    :goto_0
    return v0

    .line 332
    :catch_0
    move-exception v0

    .line 333
    const-string v1, "RemoteException when using AdShieldClient.isAdRequestAdSense()"

    invoke-static {v1, v0}, Lezp;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 334
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lery;->g:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lery;->h:Lgeq;

    const/4 v1, 0x0

    .line 298
    invoke-virtual {v0, v1}, Lgeq;->a(Z)Lgeo;

    move-result-object v0

    .line 299
    iget-object v0, v0, Lgeo;->a:Lgep;

    iget v0, v0, Lgep;->a:I

    .line 300
    sget-object v1, Lesd;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 301
    if-nez v0, :cond_0

    .line 302
    const-string v0, "Could not select a stream, defaulting to itag 36"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 303
    const-string v0, "36"

    .line 305
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
