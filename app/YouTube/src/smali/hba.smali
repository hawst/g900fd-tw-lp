.class public final Lhba;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lheb;

.field private b:Lhhn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 13709
    invoke-direct {p0}, Lidf;-><init>()V

    .line 13712
    iput-object v0, p0, Lhba;->a:Lheb;

    .line 13715
    iput-object v0, p0, Lhba;->b:Lhhn;

    .line 13709
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 13758
    const/4 v0, 0x0

    .line 13759
    iget-object v1, p0, Lhba;->a:Lheb;

    if-eqz v1, :cond_0

    .line 13760
    const v0, 0x3049143

    iget-object v1, p0, Lhba;->a:Lheb;

    .line 13761
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13763
    :cond_0
    iget-object v1, p0, Lhba;->b:Lhhn;

    if-eqz v1, :cond_1

    .line 13764
    const v1, 0x3993a79

    iget-object v2, p0, Lhba;->b:Lhhn;

    .line 13765
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13767
    :cond_1
    iget-object v1, p0, Lhba;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13768
    iput v0, p0, Lhba;->J:I

    .line 13769
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 13705
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhba;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhba;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhba;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhba;->a:Lheb;

    if-nez v0, :cond_2

    new-instance v0, Lheb;

    invoke-direct {v0}, Lheb;-><init>()V

    iput-object v0, p0, Lhba;->a:Lheb;

    :cond_2
    iget-object v0, p0, Lhba;->a:Lheb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhba;->b:Lhhn;

    if-nez v0, :cond_3

    new-instance v0, Lhhn;

    invoke-direct {v0}, Lhhn;-><init>()V

    iput-object v0, p0, Lhba;->b:Lhhn;

    :cond_3
    iget-object v0, p0, Lhba;->b:Lhhn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18248a1a -> :sswitch_1
        0x1cc9d3ca -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 13746
    iget-object v0, p0, Lhba;->a:Lheb;

    if-eqz v0, :cond_0

    .line 13747
    const v0, 0x3049143

    iget-object v1, p0, Lhba;->a:Lheb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13749
    :cond_0
    iget-object v0, p0, Lhba;->b:Lhhn;

    if-eqz v0, :cond_1

    .line 13750
    const v0, 0x3993a79

    iget-object v1, p0, Lhba;->b:Lhhn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13752
    :cond_1
    iget-object v0, p0, Lhba;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 13754
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 13727
    if-ne p1, p0, :cond_1

    .line 13732
    :cond_0
    :goto_0
    return v0

    .line 13728
    :cond_1
    instance-of v2, p1, Lhba;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 13729
    :cond_2
    check-cast p1, Lhba;

    .line 13730
    iget-object v2, p0, Lhba;->a:Lheb;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhba;->a:Lheb;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhba;->b:Lhhn;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhba;->b:Lhhn;

    if-nez v2, :cond_3

    .line 13731
    :goto_2
    iget-object v2, p0, Lhba;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhba;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 13732
    goto :goto_0

    .line 13730
    :cond_4
    iget-object v2, p0, Lhba;->a:Lheb;

    iget-object v3, p1, Lhba;->a:Lheb;

    invoke-virtual {v2, v3}, Lheb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhba;->b:Lhhn;

    iget-object v3, p1, Lhba;->b:Lhhn;

    .line 13731
    invoke-virtual {v2, v3}, Lhhn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhba;->I:Ljava/util/List;

    iget-object v3, p1, Lhba;->I:Ljava/util/List;

    .line 13732
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 13736
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 13738
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhba;->a:Lheb;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 13739
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhba;->b:Lhhn;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 13740
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhba;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 13741
    return v0

    .line 13738
    :cond_0
    iget-object v0, p0, Lhba;->a:Lheb;

    invoke-virtual {v0}, Lheb;->hashCode()I

    move-result v0

    goto :goto_0

    .line 13739
    :cond_1
    iget-object v0, p0, Lhba;->b:Lhhn;

    invoke-virtual {v0}, Lhhn;->hashCode()I

    move-result v0

    goto :goto_1

    .line 13740
    :cond_2
    iget-object v1, p0, Lhba;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
