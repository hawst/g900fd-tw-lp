.class Lbut;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lbur;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    const v0, 0x7f08008a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbut;->a:Landroid/widget/ImageView;

    .line 93
    const v0, 0x7f080286

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbut;->b:Landroid/widget/TextView;

    .line 94
    return-void
.end method


# virtual methods
.method public a(Lgcg;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lbut;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 98
    sget-object v0, Lgcg;->c:Lgcg;

    if-ne p1, v0, :cond_2

    .line 99
    iget-object v0, p0, Lbut;->a:Landroid/widget/ImageView;

    const v1, 0x7f0201d8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    :cond_0
    :goto_0
    iget-object v0, p0, Lbut;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lbut;->b:Landroid/widget/TextView;

    iget v1, p1, Lgcg;->d:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 107
    :cond_1
    return-void

    .line 101
    :cond_2
    iget-object v0, p0, Lbut;->a:Landroid/widget/ImageView;

    const v1, 0x7f0201d9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
