.class public final Lhdt;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lhxf;

.field public c:Lhgz;

.field public d:Lhgz;

.field public e:Lhgz;

.field public f:Lhgz;

.field public g:Lhgz;

.field public h:Lhog;

.field public i:[Lhbi;

.field public j:[B

.field public k:[Lhut;

.field public l:Lhgz;

.field public m:Lhnh;

.field private n:Lhgz;

.field private o:Lhxf;

.field private p:[Lhbi;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33829
    invoke-direct {p0}, Lidf;-><init>()V

    .line 33832
    const-string v0, ""

    iput-object v0, p0, Lhdt;->a:Ljava/lang/String;

    .line 33835
    iput-object v1, p0, Lhdt;->b:Lhxf;

    .line 33838
    iput-object v1, p0, Lhdt;->c:Lhgz;

    .line 33841
    iput-object v1, p0, Lhdt;->n:Lhgz;

    .line 33844
    iput-object v1, p0, Lhdt;->d:Lhgz;

    .line 33847
    iput-object v1, p0, Lhdt;->o:Lhxf;

    .line 33850
    iput-object v1, p0, Lhdt;->e:Lhgz;

    .line 33853
    iput-object v1, p0, Lhdt;->f:Lhgz;

    .line 33856
    iput-object v1, p0, Lhdt;->g:Lhgz;

    .line 33859
    iput-object v1, p0, Lhdt;->h:Lhog;

    .line 33862
    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhdt;->i:[Lhbi;

    .line 33865
    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhdt;->p:[Lhbi;

    .line 33868
    const-string v0, ""

    iput-object v0, p0, Lhdt;->q:Ljava/lang/String;

    .line 33871
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhdt;->j:[B

    .line 33874
    sget-object v0, Lhut;->a:[Lhut;

    iput-object v0, p0, Lhdt;->k:[Lhut;

    .line 33877
    iput-object v1, p0, Lhdt;->l:Lhgz;

    .line 33880
    iput-object v1, p0, Lhdt;->m:Lhnh;

    .line 33829
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 34045
    .line 34046
    iget-object v0, p0, Lhdt;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 34047
    const/4 v0, 0x1

    iget-object v2, p0, Lhdt;->a:Ljava/lang/String;

    .line 34048
    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 34050
    :goto_0
    iget-object v2, p0, Lhdt;->b:Lhxf;

    if-eqz v2, :cond_0

    .line 34051
    const/4 v2, 0x2

    iget-object v3, p0, Lhdt;->b:Lhxf;

    .line 34052
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34054
    :cond_0
    iget-object v2, p0, Lhdt;->c:Lhgz;

    if-eqz v2, :cond_1

    .line 34055
    const/4 v2, 0x3

    iget-object v3, p0, Lhdt;->c:Lhgz;

    .line 34056
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34058
    :cond_1
    iget-object v2, p0, Lhdt;->n:Lhgz;

    if-eqz v2, :cond_2

    .line 34059
    const/4 v2, 0x4

    iget-object v3, p0, Lhdt;->n:Lhgz;

    .line 34060
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34062
    :cond_2
    iget-object v2, p0, Lhdt;->d:Lhgz;

    if-eqz v2, :cond_3

    .line 34063
    const/4 v2, 0x5

    iget-object v3, p0, Lhdt;->d:Lhgz;

    .line 34064
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34066
    :cond_3
    iget-object v2, p0, Lhdt;->o:Lhxf;

    if-eqz v2, :cond_4

    .line 34067
    const/4 v2, 0x6

    iget-object v3, p0, Lhdt;->o:Lhxf;

    .line 34068
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34070
    :cond_4
    iget-object v2, p0, Lhdt;->e:Lhgz;

    if-eqz v2, :cond_5

    .line 34071
    const/4 v2, 0x7

    iget-object v3, p0, Lhdt;->e:Lhgz;

    .line 34072
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34074
    :cond_5
    iget-object v2, p0, Lhdt;->f:Lhgz;

    if-eqz v2, :cond_6

    .line 34075
    const/16 v2, 0x8

    iget-object v3, p0, Lhdt;->f:Lhgz;

    .line 34076
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34078
    :cond_6
    iget-object v2, p0, Lhdt;->g:Lhgz;

    if-eqz v2, :cond_7

    .line 34079
    const/16 v2, 0x9

    iget-object v3, p0, Lhdt;->g:Lhgz;

    .line 34080
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34082
    :cond_7
    iget-object v2, p0, Lhdt;->h:Lhog;

    if-eqz v2, :cond_8

    .line 34083
    const/16 v2, 0xa

    iget-object v3, p0, Lhdt;->h:Lhog;

    .line 34084
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34086
    :cond_8
    iget-object v2, p0, Lhdt;->i:[Lhbi;

    if-eqz v2, :cond_a

    .line 34087
    iget-object v3, p0, Lhdt;->i:[Lhbi;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_a

    aget-object v5, v3, v2

    .line 34088
    if-eqz v5, :cond_9

    .line 34089
    const/16 v6, 0xb

    .line 34090
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 34087
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 34094
    :cond_a
    iget-object v2, p0, Lhdt;->p:[Lhbi;

    if-eqz v2, :cond_c

    .line 34095
    iget-object v3, p0, Lhdt;->p:[Lhbi;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_c

    aget-object v5, v3, v2

    .line 34096
    if-eqz v5, :cond_b

    .line 34097
    const/16 v6, 0xc

    .line 34098
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 34095
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 34102
    :cond_c
    iget-object v2, p0, Lhdt;->q:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 34103
    const/16 v2, 0xd

    iget-object v3, p0, Lhdt;->q:Ljava/lang/String;

    .line 34104
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 34106
    :cond_d
    iget-object v2, p0, Lhdt;->j:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_e

    .line 34107
    const/16 v2, 0x10

    iget-object v3, p0, Lhdt;->j:[B

    .line 34108
    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 34110
    :cond_e
    iget-object v2, p0, Lhdt;->k:[Lhut;

    if-eqz v2, :cond_10

    .line 34111
    iget-object v2, p0, Lhdt;->k:[Lhut;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 34112
    if-eqz v4, :cond_f

    .line 34113
    const/16 v5, 0x11

    .line 34114
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 34111
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 34118
    :cond_10
    iget-object v1, p0, Lhdt;->l:Lhgz;

    if-eqz v1, :cond_11

    .line 34119
    const/16 v1, 0x12

    iget-object v2, p0, Lhdt;->l:Lhgz;

    .line 34120
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34122
    :cond_11
    iget-object v1, p0, Lhdt;->m:Lhnh;

    if-eqz v1, :cond_12

    .line 34123
    const/16 v1, 0x13

    iget-object v2, p0, Lhdt;->m:Lhnh;

    .line 34124
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34126
    :cond_12
    iget-object v1, p0, Lhdt;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34127
    iput v0, p0, Lhdt;->J:I

    .line 34128
    return v0

    :cond_13
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 33825
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhdt;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhdt;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhdt;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhdt;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhdt;->b:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhdt;->b:Lhxf;

    :cond_2
    iget-object v0, p0, Lhdt;->b:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhdt;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdt;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lhdt;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhdt;->n:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdt;->n:Lhgz;

    :cond_4
    iget-object v0, p0, Lhdt;->n:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhdt;->d:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdt;->d:Lhgz;

    :cond_5
    iget-object v0, p0, Lhdt;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhdt;->o:Lhxf;

    if-nez v0, :cond_6

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhdt;->o:Lhxf;

    :cond_6
    iget-object v0, p0, Lhdt;->o:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhdt;->e:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdt;->e:Lhgz;

    :cond_7
    iget-object v0, p0, Lhdt;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhdt;->f:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdt;->f:Lhgz;

    :cond_8
    iget-object v0, p0, Lhdt;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhdt;->g:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdt;->g:Lhgz;

    :cond_9
    iget-object v0, p0, Lhdt;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhdt;->h:Lhog;

    if-nez v0, :cond_a

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhdt;->h:Lhog;

    :cond_a
    iget-object v0, p0, Lhdt;->h:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhdt;->i:[Lhbi;

    if-nez v0, :cond_c

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhdt;->i:[Lhbi;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lhdt;->i:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    iput-object v2, p0, Lhdt;->i:[Lhbi;

    :goto_2
    iget-object v2, p0, Lhdt;->i:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_d

    iget-object v2, p0, Lhdt;->i:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdt;->i:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lhdt;->i:[Lhbi;

    array-length v0, v0

    goto :goto_1

    :cond_d
    iget-object v2, p0, Lhdt;->i:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdt;->i:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhdt;->p:[Lhbi;

    if-nez v0, :cond_f

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhdt;->p:[Lhbi;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lhdt;->p:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    iput-object v2, p0, Lhdt;->p:[Lhbi;

    :goto_4
    iget-object v2, p0, Lhdt;->p:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    iget-object v2, p0, Lhdt;->p:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdt;->p:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_f
    iget-object v0, p0, Lhdt;->p:[Lhbi;

    array-length v0, v0

    goto :goto_3

    :cond_10
    iget-object v2, p0, Lhdt;->p:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdt;->p:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhdt;->q:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhdt;->j:[B

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x8a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhdt;->k:[Lhut;

    if-nez v0, :cond_12

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhut;

    iget-object v3, p0, Lhdt;->k:[Lhut;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lhdt;->k:[Lhut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    iput-object v2, p0, Lhdt;->k:[Lhut;

    :goto_6
    iget-object v2, p0, Lhdt;->k:[Lhut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    iget-object v2, p0, Lhdt;->k:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdt;->k:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_12
    iget-object v0, p0, Lhdt;->k:[Lhut;

    array-length v0, v0

    goto :goto_5

    :cond_13
    iget-object v2, p0, Lhdt;->k:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdt;->k:[Lhut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lhdt;->l:Lhgz;

    if-nez v0, :cond_14

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdt;->l:Lhgz;

    :cond_14
    iget-object v0, p0, Lhdt;->l:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lhdt;->m:Lhnh;

    if-nez v0, :cond_15

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhdt;->m:Lhnh;

    :cond_15
    iget-object v0, p0, Lhdt;->m:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x82 -> :sswitch_e
        0x8a -> :sswitch_f
        0x92 -> :sswitch_10
        0x9a -> :sswitch_11
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 33976
    iget-object v1, p0, Lhdt;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 33977
    const/4 v1, 0x1

    iget-object v2, p0, Lhdt;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 33979
    :cond_0
    iget-object v1, p0, Lhdt;->b:Lhxf;

    if-eqz v1, :cond_1

    .line 33980
    const/4 v1, 0x2

    iget-object v2, p0, Lhdt;->b:Lhxf;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 33982
    :cond_1
    iget-object v1, p0, Lhdt;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 33983
    const/4 v1, 0x3

    iget-object v2, p0, Lhdt;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 33985
    :cond_2
    iget-object v1, p0, Lhdt;->n:Lhgz;

    if-eqz v1, :cond_3

    .line 33986
    const/4 v1, 0x4

    iget-object v2, p0, Lhdt;->n:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 33988
    :cond_3
    iget-object v1, p0, Lhdt;->d:Lhgz;

    if-eqz v1, :cond_4

    .line 33989
    const/4 v1, 0x5

    iget-object v2, p0, Lhdt;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 33991
    :cond_4
    iget-object v1, p0, Lhdt;->o:Lhxf;

    if-eqz v1, :cond_5

    .line 33992
    const/4 v1, 0x6

    iget-object v2, p0, Lhdt;->o:Lhxf;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 33994
    :cond_5
    iget-object v1, p0, Lhdt;->e:Lhgz;

    if-eqz v1, :cond_6

    .line 33995
    const/4 v1, 0x7

    iget-object v2, p0, Lhdt;->e:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 33997
    :cond_6
    iget-object v1, p0, Lhdt;->f:Lhgz;

    if-eqz v1, :cond_7

    .line 33998
    const/16 v1, 0x8

    iget-object v2, p0, Lhdt;->f:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 34000
    :cond_7
    iget-object v1, p0, Lhdt;->g:Lhgz;

    if-eqz v1, :cond_8

    .line 34001
    const/16 v1, 0x9

    iget-object v2, p0, Lhdt;->g:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 34003
    :cond_8
    iget-object v1, p0, Lhdt;->h:Lhog;

    if-eqz v1, :cond_9

    .line 34004
    const/16 v1, 0xa

    iget-object v2, p0, Lhdt;->h:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 34006
    :cond_9
    iget-object v1, p0, Lhdt;->i:[Lhbi;

    if-eqz v1, :cond_b

    .line 34007
    iget-object v2, p0, Lhdt;->i:[Lhbi;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_b

    aget-object v4, v2, v1

    .line 34008
    if-eqz v4, :cond_a

    .line 34009
    const/16 v5, 0xb

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 34007
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34013
    :cond_b
    iget-object v1, p0, Lhdt;->p:[Lhbi;

    if-eqz v1, :cond_d

    .line 34014
    iget-object v2, p0, Lhdt;->p:[Lhbi;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_d

    aget-object v4, v2, v1

    .line 34015
    if-eqz v4, :cond_c

    .line 34016
    const/16 v5, 0xc

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 34014
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 34020
    :cond_d
    iget-object v1, p0, Lhdt;->q:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 34021
    const/16 v1, 0xd

    iget-object v2, p0, Lhdt;->q:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 34023
    :cond_e
    iget-object v1, p0, Lhdt;->j:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_f

    .line 34024
    const/16 v1, 0x10

    iget-object v2, p0, Lhdt;->j:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    .line 34026
    :cond_f
    iget-object v1, p0, Lhdt;->k:[Lhut;

    if-eqz v1, :cond_11

    .line 34027
    iget-object v1, p0, Lhdt;->k:[Lhut;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_11

    aget-object v3, v1, v0

    .line 34028
    if-eqz v3, :cond_10

    .line 34029
    const/16 v4, 0x11

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 34027
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 34033
    :cond_11
    iget-object v0, p0, Lhdt;->l:Lhgz;

    if-eqz v0, :cond_12

    .line 34034
    const/16 v0, 0x12

    iget-object v1, p0, Lhdt;->l:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 34036
    :cond_12
    iget-object v0, p0, Lhdt;->m:Lhnh;

    if-eqz v0, :cond_13

    .line 34037
    const/16 v0, 0x13

    iget-object v1, p0, Lhdt;->m:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 34039
    :cond_13
    iget-object v0, p0, Lhdt;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 34041
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 33907
    if-ne p1, p0, :cond_1

    .line 33927
    :cond_0
    :goto_0
    return v0

    .line 33908
    :cond_1
    instance-of v2, p1, Lhdt;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 33909
    :cond_2
    check-cast p1, Lhdt;

    .line 33910
    iget-object v2, p0, Lhdt;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdt;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhdt;->b:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdt;->b:Lhxf;

    if-nez v2, :cond_3

    .line 33911
    :goto_2
    iget-object v2, p0, Lhdt;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhdt;->c:Lhgz;

    if-nez v2, :cond_3

    .line 33912
    :goto_3
    iget-object v2, p0, Lhdt;->n:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhdt;->n:Lhgz;

    if-nez v2, :cond_3

    .line 33913
    :goto_4
    iget-object v2, p0, Lhdt;->d:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhdt;->d:Lhgz;

    if-nez v2, :cond_3

    .line 33914
    :goto_5
    iget-object v2, p0, Lhdt;->o:Lhxf;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhdt;->o:Lhxf;

    if-nez v2, :cond_3

    .line 33915
    :goto_6
    iget-object v2, p0, Lhdt;->e:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhdt;->e:Lhgz;

    if-nez v2, :cond_3

    .line 33916
    :goto_7
    iget-object v2, p0, Lhdt;->f:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhdt;->f:Lhgz;

    if-nez v2, :cond_3

    .line 33917
    :goto_8
    iget-object v2, p0, Lhdt;->g:Lhgz;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhdt;->g:Lhgz;

    if-nez v2, :cond_3

    .line 33918
    :goto_9
    iget-object v2, p0, Lhdt;->h:Lhog;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhdt;->h:Lhog;

    if-nez v2, :cond_3

    .line 33919
    :goto_a
    iget-object v2, p0, Lhdt;->i:[Lhbi;

    iget-object v3, p1, Lhdt;->i:[Lhbi;

    .line 33920
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhdt;->p:[Lhbi;

    iget-object v3, p1, Lhdt;->p:[Lhbi;

    .line 33921
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhdt;->q:Ljava/lang/String;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhdt;->q:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 33922
    :goto_b
    iget-object v2, p0, Lhdt;->j:[B

    iget-object v3, p1, Lhdt;->j:[B

    .line 33923
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhdt;->k:[Lhut;

    iget-object v3, p1, Lhdt;->k:[Lhut;

    .line 33924
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhdt;->l:Lhgz;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhdt;->l:Lhgz;

    if-nez v2, :cond_3

    .line 33925
    :goto_c
    iget-object v2, p0, Lhdt;->m:Lhnh;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhdt;->m:Lhnh;

    if-nez v2, :cond_3

    .line 33926
    :goto_d
    iget-object v2, p0, Lhdt;->I:Ljava/util/List;

    if-nez v2, :cond_11

    iget-object v2, p1, Lhdt;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 33927
    goto/16 :goto_0

    .line 33910
    :cond_4
    iget-object v2, p0, Lhdt;->a:Ljava/lang/String;

    iget-object v3, p1, Lhdt;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhdt;->b:Lhxf;

    iget-object v3, p1, Lhdt;->b:Lhxf;

    .line 33911
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhdt;->c:Lhgz;

    iget-object v3, p1, Lhdt;->c:Lhgz;

    .line 33912
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhdt;->n:Lhgz;

    iget-object v3, p1, Lhdt;->n:Lhgz;

    .line 33913
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhdt;->d:Lhgz;

    iget-object v3, p1, Lhdt;->d:Lhgz;

    .line 33914
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhdt;->o:Lhxf;

    iget-object v3, p1, Lhdt;->o:Lhxf;

    .line 33915
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhdt;->e:Lhgz;

    iget-object v3, p1, Lhdt;->e:Lhgz;

    .line 33916
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhdt;->f:Lhgz;

    iget-object v3, p1, Lhdt;->f:Lhgz;

    .line 33917
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhdt;->g:Lhgz;

    iget-object v3, p1, Lhdt;->g:Lhgz;

    .line 33918
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhdt;->h:Lhog;

    iget-object v3, p1, Lhdt;->h:Lhog;

    .line 33919
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    .line 33921
    :cond_e
    iget-object v2, p0, Lhdt;->q:Ljava/lang/String;

    iget-object v3, p1, Lhdt;->q:Ljava/lang/String;

    .line 33922
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    .line 33924
    :cond_f
    iget-object v2, p0, Lhdt;->l:Lhgz;

    iget-object v3, p1, Lhdt;->l:Lhgz;

    .line 33925
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhdt;->m:Lhnh;

    iget-object v3, p1, Lhdt;->m:Lhnh;

    .line 33926
    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v2, p0, Lhdt;->I:Ljava/util/List;

    iget-object v3, p1, Lhdt;->I:Ljava/util/List;

    .line 33927
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 33931
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 33933
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 33934
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->b:Lhxf;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 33935
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->c:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 33936
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->n:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 33937
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->d:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 33938
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->o:Lhxf;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 33939
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->e:Lhgz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 33940
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->f:Lhgz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 33941
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->g:Lhgz;

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 33942
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->h:Lhog;

    if-nez v0, :cond_d

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 33943
    iget-object v2, p0, Lhdt;->i:[Lhbi;

    if-nez v2, :cond_e

    mul-int/lit8 v2, v0, 0x1f

    .line 33949
    :cond_0
    iget-object v0, p0, Lhdt;->p:[Lhbi;

    if-nez v0, :cond_10

    mul-int/lit8 v2, v2, 0x1f

    .line 33955
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhdt;->q:Ljava/lang/String;

    if-nez v0, :cond_12

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 33956
    iget-object v2, p0, Lhdt;->j:[B

    if-nez v2, :cond_13

    mul-int/lit8 v2, v0, 0x1f

    .line 33962
    :cond_2
    iget-object v0, p0, Lhdt;->k:[Lhut;

    if-nez v0, :cond_14

    mul-int/lit8 v2, v2, 0x1f

    .line 33968
    :cond_3
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhdt;->l:Lhgz;

    if-nez v0, :cond_16

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 33969
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdt;->m:Lhnh;

    if-nez v0, :cond_17

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 33970
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhdt;->I:Ljava/util/List;

    if-nez v2, :cond_18

    :goto_d
    add-int/2addr v0, v1

    .line 33971
    return v0

    .line 33933
    :cond_4
    iget-object v0, p0, Lhdt;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 33934
    :cond_5
    iget-object v0, p0, Lhdt;->b:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 33935
    :cond_6
    iget-object v0, p0, Lhdt;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 33936
    :cond_7
    iget-object v0, p0, Lhdt;->n:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 33937
    :cond_8
    iget-object v0, p0, Lhdt;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 33938
    :cond_9
    iget-object v0, p0, Lhdt;->o:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 33939
    :cond_a
    iget-object v0, p0, Lhdt;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 33940
    :cond_b
    iget-object v0, p0, Lhdt;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 33941
    :cond_c
    iget-object v0, p0, Lhdt;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 33942
    :cond_d
    iget-object v0, p0, Lhdt;->h:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_e
    move v2, v0

    move v0, v1

    .line 33945
    :goto_e
    iget-object v3, p0, Lhdt;->i:[Lhbi;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 33946
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhdt;->i:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_f

    move v2, v1

    :goto_f
    add-int/2addr v2, v3

    .line 33945
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 33946
    :cond_f
    iget-object v2, p0, Lhdt;->i:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_f

    :cond_10
    move v0, v1

    .line 33951
    :goto_10
    iget-object v3, p0, Lhdt;->p:[Lhbi;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 33952
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhdt;->p:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_11

    move v2, v1

    :goto_11
    add-int/2addr v2, v3

    .line 33951
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 33952
    :cond_11
    iget-object v2, p0, Lhdt;->p:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_11

    .line 33955
    :cond_12
    iget-object v0, p0, Lhdt;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_13
    move v2, v0

    move v0, v1

    .line 33958
    :goto_12
    iget-object v3, p0, Lhdt;->j:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 33959
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhdt;->j:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 33958
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    :cond_14
    move v0, v1

    .line 33964
    :goto_13
    iget-object v3, p0, Lhdt;->k:[Lhut;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 33965
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhdt;->k:[Lhut;

    aget-object v2, v2, v0

    if-nez v2, :cond_15

    move v2, v1

    :goto_14
    add-int/2addr v2, v3

    .line 33964
    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    .line 33965
    :cond_15
    iget-object v2, p0, Lhdt;->k:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhut;->hashCode()I

    move-result v2

    goto :goto_14

    .line 33968
    :cond_16
    iget-object v0, p0, Lhdt;->l:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 33969
    :cond_17
    iget-object v0, p0, Lhdt;->m:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 33970
    :cond_18
    iget-object v1, p0, Lhdt;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_d
.end method
