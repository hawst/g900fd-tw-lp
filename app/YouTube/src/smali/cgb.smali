.class public final Lcgb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsk;


# instance fields
.field final a:Lbka;

.field final b:Lgnd;

.field final c:Ljava/lang/String;

.field final d:I

.field final e:I

.field final f:I

.field final g:I

.field private final h:Landroid/content/Context;

.field private final i:Lcyc;

.field private final j:Levn;

.field private final k:Lexd;

.field private final l:Lglm;

.field private final m:Lfus;

.field private final n:Lcwg;

.field private final o:Lbyg;

.field private final p:Lbrz;

.field private final q:Leyp;

.field private final r:Lbjx;

.field private final s:Lboi;

.field private final t:Lfrz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyc;Levn;Lexd;Lglm;Lfus;Lcwg;Lbyg;Lbrz;Lbka;Lgnd;Leyp;Lbjx;Lfrz;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 479
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcgb;->h:Landroid/content/Context;

    .line 480
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcyc;

    iput-object v1, p0, Lcgb;->i:Lcyc;

    .line 481
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Levn;

    iput-object v1, p0, Lcgb;->j:Levn;

    .line 482
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexd;

    iput-object v1, p0, Lcgb;->k:Lexd;

    .line 483
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lglm;

    iput-object v1, p0, Lcgb;->l:Lglm;

    .line 484
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfus;

    iput-object v1, p0, Lcgb;->m:Lfus;

    .line 485
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    iput-object v1, p0, Lcgb;->n:Lcwg;

    .line 486
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbyg;

    iput-object v1, p0, Lcgb;->o:Lbyg;

    .line 487
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbrz;

    iput-object v1, p0, Lcgb;->p:Lbrz;

    .line 489
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbka;

    iput-object v1, p0, Lcgb;->a:Lbka;

    .line 490
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgnd;

    iput-object v1, p0, Lcgb;->b:Lgnd;

    .line 491
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyp;

    iput-object v1, p0, Lcgb;->q:Leyp;

    .line 492
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjx;

    iput-object v1, p0, Lcgb;->r:Lbjx;

    .line 494
    invoke-static/range {p14 .. p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfrz;

    iput-object v1, p0, Lcgb;->t:Lfrz;

    .line 495
    move-object/from16 v0, p15

    iput-object v0, p0, Lcgb;->c:Ljava/lang/String;

    .line 497
    new-instance v1, Lcgc;

    invoke-direct {v1, p0}, Lcgc;-><init>(Lcgb;)V

    .line 506
    new-instance v2, Lcgd;

    invoke-direct {v2, p0}, Lcgd;-><init>(Lcgb;)V

    .line 516
    new-instance v3, Lcge;

    invoke-direct {v3, p0}, Lcge;-><init>(Lcgb;)V

    .line 523
    new-instance v4, Lcgf;

    invoke-direct {v4, p0}, Lcgf;-><init>(Lcgb;)V

    .line 530
    new-instance v5, Lboi;

    check-cast p1, Landroid/app/Activity;

    invoke-direct {v5, p1}, Lboi;-><init>(Landroid/app/Activity;)V

    iput-object v5, p0, Lcgb;->s:Lboi;

    .line 531
    iget-object v5, p0, Lcgb;->s:Lboi;

    new-instance v6, Lcgg;

    invoke-direct {v6, p0}, Lcgg;-><init>(Lcgb;)V

    iput-object v6, v5, Lboi;->b:Lboq;

    .line 560
    iget-object v5, p0, Lcgb;->s:Lboi;

    const v6, 0x7f09019c

    invoke-virtual {v5, v6, v3}, Lboi;->a(ILbop;)I

    move-result v3

    iput v3, p0, Lcgb;->d:I

    .line 561
    iget-object v3, p0, Lcgb;->s:Lboi;

    const v5, 0x7f09019d

    invoke-virtual {v3, v5, v4}, Lboi;->a(ILbop;)I

    move-result v3

    iput v3, p0, Lcgb;->e:I

    .line 562
    iget-object v3, p0, Lcgb;->s:Lboi;

    const v4, 0x7f09019b

    invoke-virtual {v3, v4, v2}, Lboi;->a(ILbop;)I

    move-result v2

    iput v2, p0, Lcgb;->f:I

    .line 563
    iget-object v2, p0, Lcgb;->s:Lboi;

    const v3, 0x7f09019a

    invoke-virtual {v2, v3, v1}, Lboi;->a(ILbop;)I

    move-result v1

    iput v1, p0, Lcgb;->g:I

    .line 564
    return-void
.end method

.method static synthetic a(Lcgb;IZ)V
    .locals 3

    .prologue
    .line 440
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcgb;->s:Lboi;

    iget-object v1, v0, Lboi;->a:Lbok;

    iget-object v0, v1, Lbok;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbol;

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lbol;->d:Z

    :cond_0
    invoke-virtual {v1}, Lbok;->notifyDataSetChanged()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcgb;->s:Lboi;

    iget-object v1, v0, Lboi;->a:Lbok;

    iget-object v0, v1, Lbok;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbol;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    iput-boolean v2, v0, Lbol;->d:Z

    :cond_2
    invoke-virtual {v1}, Lbok;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic p()Lfsh;
    .locals 17

    .prologue
    .line 440
    new-instance v1, Lcfz;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcgb;->h:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcgb;->i:Lcyc;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcgb;->h:Landroid/content/Context;

    invoke-static {v3, v4}, La;->a(Lcyc;Landroid/content/Context;)Lfsj;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcgb;->k:Lexd;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcgb;->l:Lglm;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcgb;->m:Lfus;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcgb;->n:Lcwg;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcgb;->o:Lbyg;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcgb;->p:Lbrz;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcgb;->a:Lbka;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcgb;->b:Lgnd;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcgb;->q:Leyp;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcgb;->r:Lbjx;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcgb;->t:Lfrz;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcgb;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcgb;->s:Lboi;

    move-object/from16 v16, v0

    invoke-direct/range {v1 .. v16}, Lcfz;-><init>(Landroid/content/Context;Lfsj;Lexd;Lglm;Lfus;Lcwg;Lbyg;Lbrz;Lbka;Lgnd;Leyp;Lbjx;Lfrz;Ljava/lang/String;Lboi;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcgb;->j:Levn;

    invoke-virtual {v2, v1}, Levn;->a(Ljava/lang/Object;)V

    return-object v1
.end method
