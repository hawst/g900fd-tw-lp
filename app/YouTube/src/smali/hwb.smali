.class public final Lhwb;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhwf;

.field public b:Lhwd;

.field public c:Lhwc;

.field public d:Lhen;

.field private e:Lhwe;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Lhwb;->a:Lhwf;

    iput-object v0, p0, Lhwb;->b:Lhwd;

    iput-object v0, p0, Lhwb;->c:Lhwc;

    iput-object v0, p0, Lhwb;->e:Lhwe;

    iput-object v0, p0, Lhwb;->d:Lhen;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhwb;->a:Lhwf;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhwb;->a:Lhwf;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhwb;->b:Lhwd;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhwb;->b:Lhwd;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhwb;->c:Lhwc;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhwb;->c:Lhwc;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhwb;->e:Lhwe;

    if-eqz v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lhwb;->e:Lhwe;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhwb;->d:Lhen;

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Lhwb;->d:Lhen;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhwb;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhwb;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhwb;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhwb;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhwb;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhwb;->a:Lhwf;

    if-nez v0, :cond_2

    new-instance v0, Lhwf;

    invoke-direct {v0}, Lhwf;-><init>()V

    iput-object v0, p0, Lhwb;->a:Lhwf;

    :cond_2
    iget-object v0, p0, Lhwb;->a:Lhwf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhwb;->b:Lhwd;

    if-nez v0, :cond_3

    new-instance v0, Lhwd;

    invoke-direct {v0}, Lhwd;-><init>()V

    iput-object v0, p0, Lhwb;->b:Lhwd;

    :cond_3
    iget-object v0, p0, Lhwb;->b:Lhwd;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhwb;->c:Lhwc;

    if-nez v0, :cond_4

    new-instance v0, Lhwc;

    invoke-direct {v0}, Lhwc;-><init>()V

    iput-object v0, p0, Lhwb;->c:Lhwc;

    :cond_4
    iget-object v0, p0, Lhwb;->c:Lhwc;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhwb;->e:Lhwe;

    if-nez v0, :cond_5

    new-instance v0, Lhwe;

    invoke-direct {v0}, Lhwe;-><init>()V

    iput-object v0, p0, Lhwb;->e:Lhwe;

    :cond_5
    iget-object v0, p0, Lhwb;->e:Lhwe;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhwb;->d:Lhen;

    if-nez v0, :cond_6

    new-instance v0, Lhen;

    invoke-direct {v0}, Lhen;-><init>()V

    iput-object v0, p0, Lhwb;->d:Lhen;

    :cond_6
    iget-object v0, p0, Lhwb;->d:Lhen;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Lhwb;->a:Lhwf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhwb;->a:Lhwf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhwb;->b:Lhwd;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhwb;->b:Lhwd;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhwb;->c:Lhwc;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhwb;->c:Lhwc;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhwb;->e:Lhwe;

    if-eqz v0, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lhwb;->e:Lhwe;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhwb;->d:Lhen;

    if-eqz v0, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Lhwb;->d:Lhen;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhwb;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhwb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhwb;

    iget-object v2, p0, Lhwb;->a:Lhwf;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhwb;->a:Lhwf;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhwb;->b:Lhwd;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhwb;->b:Lhwd;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhwb;->c:Lhwc;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhwb;->c:Lhwc;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhwb;->e:Lhwe;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhwb;->e:Lhwe;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhwb;->d:Lhen;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhwb;->d:Lhen;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhwb;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhwb;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhwb;->a:Lhwf;

    iget-object v3, p1, Lhwb;->a:Lhwf;

    invoke-virtual {v2, v3}, Lhwf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhwb;->b:Lhwd;

    iget-object v3, p1, Lhwb;->b:Lhwd;

    invoke-virtual {v2, v3}, Lhwd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhwb;->c:Lhwc;

    iget-object v3, p1, Lhwb;->c:Lhwc;

    invoke-virtual {v2, v3}, Lhwc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhwb;->e:Lhwe;

    iget-object v3, p1, Lhwb;->e:Lhwe;

    invoke-virtual {v2, v3}, Lhwe;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhwb;->d:Lhen;

    iget-object v3, p1, Lhwb;->d:Lhen;

    invoke-virtual {v2, v3}, Lhen;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhwb;->I:Ljava/util/List;

    iget-object v3, p1, Lhwb;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwb;->a:Lhwf;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwb;->b:Lhwd;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwb;->c:Lhwc;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwb;->e:Lhwe;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhwb;->d:Lhen;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhwb;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhwb;->a:Lhwf;

    invoke-virtual {v0}, Lhwf;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhwb;->b:Lhwd;

    invoke-virtual {v0}, Lhwd;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhwb;->c:Lhwc;

    invoke-virtual {v0}, Lhwc;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhwb;->e:Lhwe;

    invoke-virtual {v0}, Lhwe;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lhwb;->d:Lhen;

    invoke-virtual {v0}, Lhen;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lhwb;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
