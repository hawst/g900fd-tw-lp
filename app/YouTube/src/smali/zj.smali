.class public final Lzj;
.super Likv;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "stsc"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lzj;->a:Ljava/util/List;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 10

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lzj;->c(Ljava/nio/ByteBuffer;)J

    .line 62
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    invoke-static {v0, v1}, La;->d(J)I

    move-result v8

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lzj;->a:Ljava/util/List;

    .line 64
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v8, :cond_0

    .line 65
    iget-object v9, p0, Lzj;->a:Ljava/util/List;

    new-instance v1, Lzk;

    .line 66
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    .line 67
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    .line 68
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lzk;-><init>(JJJ)V

    .line 65
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_0
    return-void
.end method

.method public final a(I)[J
    .locals 8

    .prologue
    .line 95
    new-array v2, p1, [J

    .line 96
    new-instance v0, Ljava/util/LinkedList;

    iget-object v1, p0, Lzj;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 98
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 99
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 100
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzk;

    .line 102
    array-length v1, v2

    :goto_0
    const/4 v4, 0x1

    if-le v1, v4, :cond_1

    .line 103
    add-int/lit8 v4, v1, -0x1

    iget-wide v6, v0, Lzk;->b:J

    aput-wide v6, v2, v4

    .line 104
    int-to-long v4, v1

    iget-wide v6, v0, Lzk;->a:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 105
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzk;

    .line 102
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 108
    :cond_1
    const/4 v1, 0x0

    iget-wide v4, v0, Lzk;->b:J

    aput-wide v4, v2, v1

    .line 109
    return-object v2
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lzj;->d(Ljava/nio/ByteBuffer;)V

    .line 75
    iget-object v0, p0, Lzj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 76
    iget-object v0, p0, Lzj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzk;

    .line 77
    iget-wide v2, v0, Lzk;->a:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 78
    iget-wide v2, v0, Lzk;->b:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 79
    iget-wide v2, v0, Lzk;->c:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    goto :goto_0

    .line 81
    :cond_0
    return-void
.end method

.method protected final d_()J
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lzj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0xc

    add-int/lit8 v0, v0, 0x8

    int-to-long v0, v0

    return-wide v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lzj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "SampleToChunkBox[entryCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
