.class public Lgeu;
.super Lgee;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lgec;


# instance fields
.field public final a:Lgey;

.field private final b:Landroid/view/View;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;

.field private e:Lged;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lgee;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Lgey;

    invoke-direct {v0, p0, p1}, Lgey;-><init>(Lgeu;Landroid/content/Context;)V

    iput-object v0, p0, Lgeu;->a:Lgey;

    .line 40
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {v0}, Lgey;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 41
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 44
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lgeu;->b:Landroid/view/View;

    .line 45
    iget-object v0, p0, Lgeu;->b:Landroid/view/View;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 47
    new-instance v0, Lgev;

    invoke-direct {v0, p0}, Lgev;-><init>(Lgeu;)V

    iput-object v0, p0, Lgeu;->c:Ljava/lang/Runnable;

    .line 54
    new-instance v0, Lgew;

    invoke-direct {v0, p0}, Lgew;-><init>(Lgeu;)V

    iput-object v0, p0, Lgeu;->d:Ljava/lang/Runnable;

    .line 61
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {p0, v0}, Lgeu;->addView(Landroid/view/View;)V

    .line 62
    iget-object v0, p0, Lgeu;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lgeu;->addView(Landroid/view/View;)V

    .line 63
    return-void
.end method

.method static synthetic a(Lgeu;I)I
    .locals 0

    .prologue
    .line 18
    iput p1, p0, Lgeu;->h:I

    return p1
.end method

.method static synthetic a(Lgeu;)Landroid/view/View;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lgeu;->b:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lgeu;I)I
    .locals 0

    .prologue
    .line 18
    iput p1, p0, Lgeu;->i:I

    return p1
.end method

.method static synthetic b(Lgeu;)Lu;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic c(Lgeu;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lgeu;->f:I

    return v0
.end method

.method static synthetic d(Lgeu;)I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lgeu;->g:I

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lgeu;->h:I

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lgeu;->d:Ljava/lang/Runnable;

    iget-object v1, p0, Lgeu;->c:Ljava/lang/Runnable;

    invoke-virtual {p0, v0, v1, p1}, Lgeu;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 91
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 125
    iput p1, p0, Lgeu;->f:I

    .line 126
    iput p2, p0, Lgeu;->g:I

    .line 127
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {v0}, Lgey;->requestLayout()V

    .line 128
    return-void
.end method

.method public final a(Lged;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lgeu;->e:Lged;

    .line 71
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lgeu;->i:I

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 203
    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    .line 204
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "SurfaceMediaView does not support requested type."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lgeu;->c:Ljava/lang/Runnable;

    iget-object v1, p0, Lgeu;->d:Ljava/lang/Runnable;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lgeu;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;I)V

    .line 96
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lgex;

    invoke-direct {v0, p0}, Lgex;-><init>(Lgeu;)V

    invoke-virtual {p0, v0}, Lgeu;->post(Ljava/lang/Runnable;)Z

    .line 113
    return-void
.end method

.method public final e()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {v0}, Lgey;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    return-object v0
.end method

.method public final f()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {v0}, Lgey;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {v0}, Lgey;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 121
    :cond_0
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lgeu;->j:Z

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x3

    return v0
.end method

.method public final j()Landroid/view/View;
    .locals 0

    .prologue
    .line 80
    return-object p0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 195
    iget-object v0, p0, Lgeu;->a:Lgey;

    iget-object v1, p0, Lgeu;->a:Lgey;

    invoke-virtual {v1}, Lgey;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lgeu;->a:Lgey;

    invoke-virtual {v2}, Lgey;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lgey;->layout(IIII)V

    .line 196
    iget-object v0, p0, Lgeu;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 197
    iget-object v0, p0, Lgeu;->b:Landroid/view/View;

    iget-object v1, p0, Lgeu;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lgeu;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 199
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 181
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {v0, p1, p2}, Lgey;->measure(II)V

    .line 182
    iget-object v0, p0, Lgeu;->a:Lgey;

    invoke-virtual {v0}, Lgey;->getMeasuredWidth()I

    move-result v0

    .line 183
    iget-object v1, p0, Lgeu;->a:Lgey;

    invoke-virtual {v1}, Lgey;->getMeasuredHeight()I

    move-result v1

    .line 185
    iget-object v2, p0, Lgeu;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 186
    iget-object v2, p0, Lgeu;->b:Landroid/view/View;

    .line 187
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 188
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 186
    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 190
    :cond_0
    invoke-virtual {p0, v0, v1}, Lgeu;->setMeasuredDimension(II)V

    .line 191
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lgeu;->e:Lged;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lgeu;->e:Lged;

    invoke-interface {v0}, Lged;->b()V

    .line 160
    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgeu;->j:Z

    .line 165
    iget-object v0, p0, Lgeu;->e:Lged;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lgeu;->e:Lged;

    invoke-interface {v0}, Lged;->a()V

    .line 168
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgeu;->j:Z

    .line 173
    iget-object v0, p0, Lgeu;->e:Lged;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lgeu;->e:Lged;

    invoke-interface {v0}, Lged;->c()V

    .line 176
    :cond_0
    invoke-virtual {p0}, Lgeu;->c()V

    .line 177
    return-void
.end method
