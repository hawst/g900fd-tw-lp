.class public final Lhci;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Lhog;

.field private c:Lhgz;

.field private d:Lhbi;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21561
    invoke-direct {p0}, Lidf;-><init>()V

    .line 21564
    iput-object v0, p0, Lhci;->a:Lhgz;

    .line 21567
    iput-object v0, p0, Lhci;->b:Lhog;

    .line 21570
    iput-object v0, p0, Lhci;->c:Lhgz;

    .line 21573
    iput-object v0, p0, Lhci;->d:Lhbi;

    .line 21576
    const-string v0, ""

    iput-object v0, p0, Lhci;->e:Ljava/lang/String;

    .line 21561
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 21637
    const/4 v0, 0x0

    .line 21638
    iget-object v1, p0, Lhci;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 21639
    const/4 v0, 0x1

    iget-object v1, p0, Lhci;->a:Lhgz;

    .line 21640
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21642
    :cond_0
    iget-object v1, p0, Lhci;->b:Lhog;

    if-eqz v1, :cond_1

    .line 21643
    const/4 v1, 0x2

    iget-object v2, p0, Lhci;->b:Lhog;

    .line 21644
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21646
    :cond_1
    iget-object v1, p0, Lhci;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 21647
    const/4 v1, 0x3

    iget-object v2, p0, Lhci;->c:Lhgz;

    .line 21648
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21650
    :cond_2
    iget-object v1, p0, Lhci;->d:Lhbi;

    if-eqz v1, :cond_3

    .line 21651
    const/4 v1, 0x4

    iget-object v2, p0, Lhci;->d:Lhbi;

    .line 21652
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21654
    :cond_3
    iget-object v1, p0, Lhci;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 21655
    const/4 v1, 0x5

    iget-object v2, p0, Lhci;->e:Ljava/lang/String;

    .line 21656
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21658
    :cond_4
    iget-object v1, p0, Lhci;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21659
    iput v0, p0, Lhci;->J:I

    .line 21660
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 21557
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhci;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhci;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhci;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhci;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhci;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhci;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhci;->b:Lhog;

    if-nez v0, :cond_3

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhci;->b:Lhog;

    :cond_3
    iget-object v0, p0, Lhci;->b:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhci;->c:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhci;->c:Lhgz;

    :cond_4
    iget-object v0, p0, Lhci;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhci;->d:Lhbi;

    if-nez v0, :cond_5

    new-instance v0, Lhbi;

    invoke-direct {v0}, Lhbi;-><init>()V

    iput-object v0, p0, Lhci;->d:Lhbi;

    :cond_5
    iget-object v0, p0, Lhci;->d:Lhbi;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhci;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 21616
    iget-object v0, p0, Lhci;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 21617
    const/4 v0, 0x1

    iget-object v1, p0, Lhci;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 21619
    :cond_0
    iget-object v0, p0, Lhci;->b:Lhog;

    if-eqz v0, :cond_1

    .line 21620
    const/4 v0, 0x2

    iget-object v1, p0, Lhci;->b:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 21622
    :cond_1
    iget-object v0, p0, Lhci;->c:Lhgz;

    if-eqz v0, :cond_2

    .line 21623
    const/4 v0, 0x3

    iget-object v1, p0, Lhci;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 21625
    :cond_2
    iget-object v0, p0, Lhci;->d:Lhbi;

    if-eqz v0, :cond_3

    .line 21626
    const/4 v0, 0x4

    iget-object v1, p0, Lhci;->d:Lhbi;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 21628
    :cond_3
    iget-object v0, p0, Lhci;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 21629
    const/4 v0, 0x5

    iget-object v1, p0, Lhci;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 21631
    :cond_4
    iget-object v0, p0, Lhci;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 21633
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21591
    if-ne p1, p0, :cond_1

    .line 21599
    :cond_0
    :goto_0
    return v0

    .line 21592
    :cond_1
    instance-of v2, p1, Lhci;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 21593
    :cond_2
    check-cast p1, Lhci;

    .line 21594
    iget-object v2, p0, Lhci;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhci;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhci;->b:Lhog;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhci;->b:Lhog;

    if-nez v2, :cond_3

    .line 21595
    :goto_2
    iget-object v2, p0, Lhci;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhci;->c:Lhgz;

    if-nez v2, :cond_3

    .line 21596
    :goto_3
    iget-object v2, p0, Lhci;->d:Lhbi;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhci;->d:Lhbi;

    if-nez v2, :cond_3

    .line 21597
    :goto_4
    iget-object v2, p0, Lhci;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhci;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 21598
    :goto_5
    iget-object v2, p0, Lhci;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhci;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 21599
    goto :goto_0

    .line 21594
    :cond_4
    iget-object v2, p0, Lhci;->a:Lhgz;

    iget-object v3, p1, Lhci;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhci;->b:Lhog;

    iget-object v3, p1, Lhci;->b:Lhog;

    .line 21595
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhci;->c:Lhgz;

    iget-object v3, p1, Lhci;->c:Lhgz;

    .line 21596
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhci;->d:Lhbi;

    iget-object v3, p1, Lhci;->d:Lhbi;

    .line 21597
    invoke-virtual {v2, v3}, Lhbi;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhci;->e:Ljava/lang/String;

    iget-object v3, p1, Lhci;->e:Ljava/lang/String;

    .line 21598
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhci;->I:Ljava/util/List;

    iget-object v3, p1, Lhci;->I:Ljava/util/List;

    .line 21599
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21603
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 21605
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhci;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 21606
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhci;->b:Lhog;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 21607
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhci;->c:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 21608
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhci;->d:Lhbi;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 21609
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhci;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 21610
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhci;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 21611
    return v0

    .line 21605
    :cond_0
    iget-object v0, p0, Lhci;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 21606
    :cond_1
    iget-object v0, p0, Lhci;->b:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_1

    .line 21607
    :cond_2
    iget-object v0, p0, Lhci;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 21608
    :cond_3
    iget-object v0, p0, Lhci;->d:Lhbi;

    invoke-virtual {v0}, Lhbi;->hashCode()I

    move-result v0

    goto :goto_3

    .line 21609
    :cond_4
    iget-object v0, p0, Lhci;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 21610
    :cond_5
    iget-object v1, p0, Lhci;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
