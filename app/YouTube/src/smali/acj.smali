.class public final Lacj;
.super Lghl;
.source "SourceFile"

# interfaces
.implements Lfws;


# instance fields
.field final a:Lgku;

.field final b:Ljava/lang/String;

.field final c:[B

.field final d:Ljava/lang/String;

.field private final j:Lgku;

.field private final k:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;[B[B)V
    .locals 2

    .prologue
    .line 101
    invoke-direct {p0, p2, p3}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;)V

    .line 103
    const-string v0, "developerKey cannot be null or empty"

    invoke-static {p4, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacj;->b:Ljava/lang/String;

    .line 104
    const-string v0, "serial cannot be null or empty"

    invoke-static {p5, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacj;->d:Ljava/lang/String;

    .line 105
    const-string v0, "playerApiKey cannot be null"

    invoke-static {p6, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lacj;->k:[B

    .line 107
    invoke-static {p1}, La;->t(Landroid/content/Context;)[B

    move-result-object v0

    iput-object v0, p0, Lacj;->c:[B

    .line 109
    new-instance v0, Lgik;

    sget-object v1, Lewv;->c:Lewv;

    invoke-direct {v0, v1}, Lgik;-><init>(Lewv;)V

    new-instance v1, Lfwt;

    invoke-direct {v1, p7}, Lfwt;-><init>([B)V

    invoke-virtual {p0, v0, v1}, Lacj;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iput-object v0, p0, Lacj;->j:Lgku;

    .line 113
    new-instance v0, Lacl;

    invoke-direct {v0, p0}, Lacl;-><init>(Lacj;)V

    .line 114
    invoke-virtual {p0, v0, v0}, Lacj;->a(Lgib;Lghv;)Lgko;

    move-result-object v0

    iput-object v0, p0, Lacj;->a:Lgku;

    .line 115
    return-void
.end method

.method private b()Ljavax/crypto/Cipher;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v0, 0x1

    .line 153
    :try_start_0
    iget-object v1, p0, Lacj;->d:Ljava/lang/String;

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 159
    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    .line 160
    invoke-virtual {v2, v1}, Ljava/util/zip/CRC32;->update([B)V

    .line 161
    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 162
    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 163
    array-length v2, v1

    if-ne v2, v4, :cond_0

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 165
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v0, "RC4"

    invoke-direct {v2, v1, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 168
    :try_start_1
    const-string v0, "RC4"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 175
    :goto_1
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {v0, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_2
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_3

    .line 179
    return-object v0

    .line 154
    :catch_0
    move-exception v0

    .line 156
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 163
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 170
    :catch_1
    move-exception v0

    new-instance v0, Ladi;

    invoke-direct {v0}, Ladi;-><init>()V

    goto :goto_1

    .line 172
    :catch_2
    move-exception v0

    new-instance v0, Ladi;

    invoke-direct {v0}, Ladi;-><init>()V

    goto :goto_1

    .line 176
    :catch_3
    move-exception v0

    .line 177
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(Leuc;)V
    .locals 4

    .prologue
    .line 122
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lacj;->k:[B

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    const-string v1, "https://www.google.com/youtube/accounts/registerDevice?type=GDATA&developer=%s&serialNumber=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lacj;->d:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lacj;->j:Lgku;

    new-instance v2, Lacm;

    invoke-direct {v2, p0, p1}, Lacm;-><init>(Lacj;Leuc;)V

    invoke-interface {v1, v0, v2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 133
    return-void

    .line 123
    :catch_0
    move-exception v0

    .line 125
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method a()[B
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Lacj;->b()Ljavax/crypto/Cipher;

    move-result-object v0

    .line 142
    :try_start_0
    iget-object v1, p0, Lacj;->c:[B

    invoke-virtual {v0, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 143
    :catch_0
    move-exception v0

    .line 144
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 145
    :catch_1
    move-exception v0

    .line 146
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
