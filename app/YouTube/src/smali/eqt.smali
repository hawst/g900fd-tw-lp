.class public final Leqt;
.super Ljava/lang/Object;


# instance fields
.field private final a:Leps;

.field private b:Lcom/google/android/gms/internal/qu;


# direct methods
.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lu;)V
    .locals 7

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Leqt;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lu;Z)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lu;Z)V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/android/gms/internal/qu;

    const/4 v6, 0x1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qu;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Leqt;->b:Lcom/google/android/gms/internal/qu;

    new-instance v0, Leps;

    new-instance v1, Lejb;

    invoke-direct {v1, p5}, Lejb;-><init>(Lu;)V

    invoke-direct {v0, p1, v1}, Leps;-><init>(Landroid/content/Context;Lejb;)V

    iput-object v0, p0, Leqt;->a:Leps;

    return-void

    :catch_0
    move-exception v0

    const-string v0, "PlayLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v0, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lu;)V
    .locals 6

    const/4 v3, 0x0

    const/16 v2, 0xb

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Leqt;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lu;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    iget-object v0, p0, Leqt;->a:Leps;

    iget-object v1, v0, Leps;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {v0}, Leps;->g()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Leps;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v2, v0, Leps;->d:Lejb;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lejb;->a(Z)V

    invoke-virtual {v0}, Leps;->a()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final varargs a(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Leqt;->a:Leps;

    iget-object v7, p0, Leqt;->b:Lcom/google/android/gms/internal/qu;

    new-instance v1, Lcom/google/android/gms/internal/qq;

    move-object v4, p1

    move-object v6, v5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/internal/qq;-><init>(JLjava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v0, v7, v1}, Leps;->a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    return-void
.end method

.method public final b()V
    .locals 4

    iget-object v0, p0, Leqt;->a:Leps;

    iget-object v1, v0, Leps;->e:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Leps;->d:Lejb;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lejb;->a(Z)V

    invoke-virtual {v0}, Leps;->b()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
