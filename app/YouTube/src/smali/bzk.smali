.class public final Lbzk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private synthetic a:Z

.field private synthetic b:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;Z)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lbzk;->b:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    iput-boolean p2, p0, Lbzk;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    iget-object v0, p0, Lbzk;->b:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 144
    iget-object v0, p0, Lbzk;->b:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->setVisibility(I)V

    .line 146
    iget-boolean v0, p0, Lbzk;->a:Z

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lbzk;->b:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "watch_while_tutorial_views_remaining"

    .line 148
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 149
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 150
    iget-object v0, p0, Lbzk;->b:Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;

    invoke-static {v0, v2}, Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;->a(Lcom/google/android/apps/youtube/app/ui/WatchMinimizedTutorialView;Z)Z

    .line 152
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 156
    return-void
.end method
