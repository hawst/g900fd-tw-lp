.class public final Lbnp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lboe;


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Lcwn;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field c:Lfpi;

.field d:Lfpm;

.field private final e:Leyp;

.field private final f:Landroid/view/ViewStub;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/ImageView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Leub;

.field private m:Leub;

.field private n:Landroid/widget/RatingBar;

.field private o:Landroid/widget/ImageView;

.field private p:Leue;

.field private q:Z

.field private r:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Leyp;Landroid/view/ViewStub;Lcwn;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lbnp;->a:Landroid/app/Activity;

    .line 69
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbnp;->e:Leyp;

    .line 70
    iput-object p4, p0, Lbnp;->b:Lcwn;

    .line 71
    iput-object p3, p0, Lbnp;->f:Landroid/view/ViewStub;

    .line 72
    return-void
.end method

.method static synthetic a(Lbnp;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lbnp;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lbnp;->o:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 205
    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lbnp;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 207
    iget-object v0, p0, Lbnp;->n:Landroid/widget/RatingBar;

    invoke-virtual {v0, v2}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lbnp;->o:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lbnp;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 210
    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 212
    :cond_0
    return-void
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lbnp;->c:Lfpi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbnp;->c:Lfpi;

    iget-object v0, v0, Lfpi;->d:Lfpq;

    iget-object v0, v0, Lfpq;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbnp;->c:Lfpi;

    .line 247
    iget-object v0, v0, Lfpi;->d:Lfpq;

    iget-boolean v0, v0, Lfpq;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 309
    iget-boolean v0, p0, Lbnp;->q:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbnp;->c:Lfpi;

    if-eqz v0, :cond_2

    .line 310
    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v1

    .line 311
    :goto_0
    if-eqz v0, :cond_0

    iget-object v3, p0, Lbnp;->g:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_1

    .line 312
    :cond_0
    iget-object v3, p0, Lbnp;->b:Lcwn;

    iget-object v4, p0, Lbnp;->c:Lfpi;

    iget-object v5, v3, Lcwn;->c:Lcnh;

    if-eqz v5, :cond_1

    iget-object v3, v3, Lcwn;->c:Lcnh;

    invoke-virtual {v3, v4, v1}, Lcnh;->a(Lfpi;I)V

    .line 315
    :cond_1
    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 319
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 310
    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    return-object v0
.end method

.method a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lbnp;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 236
    invoke-direct {p0}, Lbnp;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lbnp;->m:Leub;

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbnp;->p:Leue;

    .line 238
    iget-object v0, p0, Lbnp;->e:Leyp;

    iget-object v1, p0, Lbnp;->c:Lfpi;

    .line 239
    iget-object v1, v1, Lfpi;->d:Lfpq;

    iget-object v1, v1, Lfpq;->g:Landroid/net/Uri;

    iget-object v2, p0, Lbnp;->p:Leue;

    .line 238
    invoke-interface {v0, v1, v2}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 241
    :cond_0
    return-void
.end method

.method public final a(Lfoy;Lfrl;)Z
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/16 v10, 0x8

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 105
    iget-object v0, p1, Lfoy;->ai:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lfoy;->ai:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpi;

    iget v4, v0, Lfpi;->a:I

    const/4 v6, 0x2

    if-ne v4, v6, :cond_0

    iget-object v4, v0, Lfpi;->d:Lfpq;

    if-eqz v4, :cond_0

    move-object v6, v0

    .line 106
    :goto_0
    if-eqz v6, :cond_1

    iget-object v0, v6, Lfpi;->d:Lfpq;

    if-nez v0, :cond_3

    .line 144
    :cond_1
    :goto_1
    return v3

    :cond_2
    move-object v6, v5

    .line 105
    goto :goto_0

    .line 109
    :cond_3
    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    invoke-static {v0}, Ldol;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 110
    invoke-virtual {p2}, Lfrl;->n()Liak;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-boolean v0, v0, Liak;->c:Z

    :goto_2
    if-eqz v0, :cond_b

    move v1, v2

    .line 111
    :goto_3
    iget-object v0, v6, Lfpi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v4, v5

    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpm;

    iget-object v8, v0, Lfpm;->b:Landroid/net/Uri;

    if-eqz v8, :cond_4

    iget-object v8, v0, Lfpm;->c:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    iget v8, v0, Lfpm;->a:I

    const/4 v9, 0x4

    if-ne v8, v9, :cond_5

    move-object v4, v0

    :cond_5
    if-eqz v1, :cond_4

    iget v8, v0, Lfpm;->a:I

    const/16 v9, 0x13

    if-ne v8, v9, :cond_4

    iget-object v8, v6, Lfpi;->d:Lfpq;

    iget-object v8, v8, Lfpq;->h:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    move-object v4, v0

    .line 112
    :cond_6
    if-eqz v4, :cond_1

    .line 115
    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    if-nez v0, :cond_7

    iget-object v0, p0, Lbnp;->f:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbnp;->g:Landroid/view/View;

    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    new-instance v1, Lbns;

    invoke-direct {v1, p0}, Lbns;-><init>(Lbnp;)V

    invoke-static {v0, v1}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    iput-object v0, p0, Lbnp;->l:Leub;

    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    new-instance v1, Lbnr;

    invoke-direct {v1, p0}, Lbnr;-><init>(Lbnp;)V

    invoke-static {v0, v1}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    iput-object v0, p0, Lbnp;->m:Leub;

    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    const v1, 0x7f0800f1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbnp;->h:Landroid/widget/ImageView;

    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    const v1, 0x7f0800f3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnp;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    const v1, 0x7f0800f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lbnp;->n:Landroid/widget/RatingBar;

    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    const v1, 0x7f0800f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbnp;->o:Landroid/widget/ImageView;

    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    const v1, 0x7f0800f6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnp;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lbnp;->a:Landroid/app/Activity;

    const v1, 0x7f02016b

    invoke-static {v0, v1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbnp;->r:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lbnp;->r:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lbnp;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v7, p0, Lbnp;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    invoke-virtual {v0, v3, v3, v1, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    const v1, 0x7f0800f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnp;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lbnp;->j:Landroid/widget/TextView;

    new-instance v1, Lbnq;

    invoke-direct {v1, p0}, Lbnq;-><init>(Lbnp;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lbnp;->g()V

    .line 116
    :cond_7
    iput-object v6, p0, Lbnp;->c:Lfpi;

    .line 117
    iput-object v4, p0, Lbnp;->d:Lfpm;

    .line 118
    iget-object v0, p0, Lbnp;->j:Landroid/widget/TextView;

    iget-object v1, p0, Lbnp;->d:Lfpm;

    iget-object v1, v1, Lfpm;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lbnp;->i:Landroid/widget/TextView;

    iget-object v1, v6, Lfpi;->d:Lfpq;

    iget-object v1, v1, Lfpq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v1, p0, Lbnp;->k:Landroid/widget/TextView;

    .line 121
    iget-object v0, v6, Lfpi;->d:Lfpq;

    iget-object v0, v0, Lfpq;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, ""

    .line 120
    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v0, v6, Lfpi;->d:Lfpq;

    iget-boolean v0, v0, Lfpq;->d:Z

    if-eqz v0, :cond_d

    .line 127
    iget-object v0, p0, Lbnp;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbnp;->n:Landroid/widget/RatingBar;

    invoke-virtual {v0, v3}, Landroid/widget/RatingBar;->setVisibility(I)V

    iget-object v0, p0, Lbnp;->n:Landroid/widget/RatingBar;

    iget-object v1, p0, Lbnp;->c:Lfpi;

    iget-object v1, v1, Lfpi;->d:Lfpq;

    iget v1, v1, Lfpq;->e:F

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 133
    :cond_8
    :goto_5
    iget-object v0, v6, Lfpi;->d:Lfpq;

    iget-object v0, v0, Lfpq;->b:Landroid/net/Uri;

    if-eqz v0, :cond_e

    .line 134
    iget-object v0, p0, Lbnp;->l:Leub;

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbnp;->p:Leue;

    .line 135
    iget-object v0, p0, Lbnp;->e:Leyp;

    iget-object v1, v6, Lfpi;->d:Lfpq;

    iget-object v1, v1, Lfpq;->b:Landroid/net/Uri;

    iget-object v4, p0, Lbnp;->p:Leue;

    invoke-interface {v0, v1, v4}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 140
    :goto_6
    invoke-virtual {p2}, Lfrl;->n()Liak;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-boolean v3, v0, Liak;->b:Z

    .line 141
    :cond_9
    iget-object v1, p0, Lbnp;->j:Landroid/widget/TextView;

    if-eqz v3, :cond_f

    iget-object v0, p0, Lbnp;->r:Landroid/graphics/drawable/Drawable;

    :goto_7
    invoke-virtual {v1, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move v3, v2

    .line 144
    goto/16 :goto_1

    :cond_a
    move v0, v3

    .line 110
    goto/16 :goto_2

    :cond_b
    move v1, v3

    goto/16 :goto_3

    .line 122
    :cond_c
    iget-object v0, v6, Lfpi;->d:Lfpq;

    iget-object v0, v0, Lfpq;->c:Ljava/lang/String;

    goto :goto_4

    .line 128
    :cond_d
    invoke-direct {p0}, Lbnp;->h()Z

    move-result v0

    if-nez v0, :cond_8

    .line 130
    iget-object v0, p0, Lbnp;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lbnp;->n:Landroid/widget/RatingBar;

    invoke-virtual {v0, v10}, Landroid/widget/RatingBar;->setVisibility(I)V

    goto :goto_5

    .line 137
    :cond_e
    invoke-virtual {p0, v5}, Lbnp;->a(Landroid/graphics/Bitmap;)V

    goto :goto_6

    :cond_f
    move-object v0, v5

    .line 141
    goto :goto_7
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbnp;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbnp;->q:Z

    .line 180
    invoke-direct {p0}, Lbnp;->i()V

    .line 181
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 188
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbnp;->q:Z

    .line 189
    iput-object v2, p0, Lbnp;->c:Lfpi;

    .line 190
    iput-object v2, p0, Lbnp;->d:Lfpm;

    .line 191
    iget-object v0, p0, Lbnp;->p:Leue;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lbnp;->p:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 193
    iput-object v2, p0, Lbnp;->p:Leue;

    .line 195
    :cond_0
    invoke-direct {p0}, Lbnp;->g()V

    .line 196
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x1

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 216
    invoke-direct {p0}, Lbnp;->i()V

    .line 217
    return-void
.end method
