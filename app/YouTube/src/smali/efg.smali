.class public final Lefg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Z

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/net/Uri;JJLjava/lang/String;)V
    .locals 10

    .prologue
    .line 61
    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    move-wide v7, p2

    invoke-direct/range {v0 .. v9}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;JZ)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;JJLjava/lang/String;J)V
    .locals 10

    .prologue
    .line 74
    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    move-wide/from16 v7, p7

    invoke-direct/range {v0 .. v9}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;JZ)V

    .line 75
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;JJLjava/lang/String;JZ)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    cmp-long v0, p2, v4

    if-ltz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->b(Z)V

    .line 90
    cmp-long v0, p7, v4

    if-ltz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v0}, La;->b(Z)V

    .line 91
    cmp-long v0, p4, v4

    if-gtz v0, :cond_0

    const-wide/16 v4, -0x1

    cmp-long v0, p4, v4

    if-nez v0, :cond_5

    :cond_0
    move v0, v1

    :goto_2
    invoke-static {v0}, La;->b(Z)V

    .line 92
    cmp-long v0, p2, p7

    if-eqz v0, :cond_1

    if-nez p9, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    invoke-static {v2}, La;->b(Z)V

    .line 93
    iput-object p1, p0, Lefg;->a:Landroid/net/Uri;

    .line 94
    iput-boolean p9, p0, Lefg;->b:Z

    .line 95
    iput-wide p2, p0, Lefg;->c:J

    .line 96
    iput-wide p7, p0, Lefg;->d:J

    .line 97
    iput-wide p4, p0, Lefg;->e:J

    .line 98
    iput-object p6, p0, Lefg;->f:Ljava/lang/String;

    .line 99
    return-void

    :cond_3
    move v0, v2

    .line 89
    goto :goto_0

    :cond_4
    move v0, v2

    .line 90
    goto :goto_1

    :cond_5
    move v0, v2

    .line 91
    goto :goto_2
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 12

    .prologue
    .line 103
    iget-object v0, p0, Lefg;->a:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lefg;->b:Z

    iget-wide v2, p0, Lefg;->c:J

    iget-wide v4, p0, Lefg;->d:J

    iget-wide v6, p0, Lefg;->e:J

    iget-object v8, p0, Lefg;->f:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x55

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "DataSpec["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, ", "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
