.class public final Lher;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lher;


# instance fields
.field public b:Lhet;

.field public c:Lhez;

.field public d:Lhfa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41105
    const/4 v0, 0x0

    new-array v0, v0, [Lher;

    sput-object v0, Lher;->a:[Lher;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41106
    invoke-direct {p0}, Lidf;-><init>()V

    .line 41109
    iput-object v0, p0, Lher;->b:Lhet;

    .line 41112
    iput-object v0, p0, Lher;->c:Lhez;

    .line 41115
    iput-object v0, p0, Lher;->d:Lhfa;

    .line 41106
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 41164
    const/4 v0, 0x0

    .line 41165
    iget-object v1, p0, Lher;->b:Lhet;

    if-eqz v1, :cond_0

    .line 41166
    const v0, 0x47bbbd0

    iget-object v1, p0, Lher;->b:Lhet;

    .line 41167
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 41169
    :cond_0
    iget-object v1, p0, Lher;->c:Lhez;

    if-eqz v1, :cond_1

    .line 41170
    const v1, 0x480d354

    iget-object v2, p0, Lher;->c:Lhez;

    .line 41171
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41173
    :cond_1
    iget-object v1, p0, Lher;->d:Lhfa;

    if-eqz v1, :cond_2

    .line 41174
    const v1, 0x480d379

    iget-object v2, p0, Lher;->d:Lhfa;

    .line 41175
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41177
    :cond_2
    iget-object v1, p0, Lher;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41178
    iput v0, p0, Lher;->J:I

    .line 41179
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 41102
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lher;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lher;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lher;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lher;->b:Lhet;

    if-nez v0, :cond_2

    new-instance v0, Lhet;

    invoke-direct {v0}, Lhet;-><init>()V

    iput-object v0, p0, Lher;->b:Lhet;

    :cond_2
    iget-object v0, p0, Lher;->b:Lhet;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lher;->c:Lhez;

    if-nez v0, :cond_3

    new-instance v0, Lhez;

    invoke-direct {v0}, Lhez;-><init>()V

    iput-object v0, p0, Lher;->c:Lhez;

    :cond_3
    iget-object v0, p0, Lher;->c:Lhez;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lher;->d:Lhfa;

    if-nez v0, :cond_4

    new-instance v0, Lhfa;

    invoke-direct {v0}, Lhfa;-><init>()V

    iput-object v0, p0, Lher;->d:Lhfa;

    :cond_4
    iget-object v0, p0, Lher;->d:Lhfa;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x23ddde82 -> :sswitch_1
        0x24069aa2 -> :sswitch_2
        0x24069bca -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 41149
    iget-object v0, p0, Lher;->b:Lhet;

    if-eqz v0, :cond_0

    .line 41150
    const v0, 0x47bbbd0

    iget-object v1, p0, Lher;->b:Lhet;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41152
    :cond_0
    iget-object v0, p0, Lher;->c:Lhez;

    if-eqz v0, :cond_1

    .line 41153
    const v0, 0x480d354

    iget-object v1, p0, Lher;->c:Lhez;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41155
    :cond_1
    iget-object v0, p0, Lher;->d:Lhfa;

    if-eqz v0, :cond_2

    .line 41156
    const v0, 0x480d379

    iget-object v1, p0, Lher;->d:Lhfa;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41158
    :cond_2
    iget-object v0, p0, Lher;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 41160
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41128
    if-ne p1, p0, :cond_1

    .line 41134
    :cond_0
    :goto_0
    return v0

    .line 41129
    :cond_1
    instance-of v2, p1, Lher;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 41130
    :cond_2
    check-cast p1, Lher;

    .line 41131
    iget-object v2, p0, Lher;->b:Lhet;

    if-nez v2, :cond_4

    iget-object v2, p1, Lher;->b:Lhet;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lher;->c:Lhez;

    if-nez v2, :cond_5

    iget-object v2, p1, Lher;->c:Lhez;

    if-nez v2, :cond_3

    .line 41132
    :goto_2
    iget-object v2, p0, Lher;->d:Lhfa;

    if-nez v2, :cond_6

    iget-object v2, p1, Lher;->d:Lhfa;

    if-nez v2, :cond_3

    .line 41133
    :goto_3
    iget-object v2, p0, Lher;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lher;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 41134
    goto :goto_0

    .line 41131
    :cond_4
    iget-object v2, p0, Lher;->b:Lhet;

    iget-object v3, p1, Lher;->b:Lhet;

    invoke-virtual {v2, v3}, Lhet;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lher;->c:Lhez;

    iget-object v3, p1, Lher;->c:Lhez;

    .line 41132
    invoke-virtual {v2, v3}, Lhez;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lher;->d:Lhfa;

    iget-object v3, p1, Lher;->d:Lhfa;

    .line 41133
    invoke-virtual {v2, v3}, Lhfa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lher;->I:Ljava/util/List;

    iget-object v3, p1, Lher;->I:Ljava/util/List;

    .line 41134
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41138
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 41140
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lher;->b:Lhet;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 41141
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lher;->c:Lhez;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 41142
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lher;->d:Lhfa;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 41143
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lher;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 41144
    return v0

    .line 41140
    :cond_0
    iget-object v0, p0, Lher;->b:Lhet;

    invoke-virtual {v0}, Lhet;->hashCode()I

    move-result v0

    goto :goto_0

    .line 41141
    :cond_1
    iget-object v0, p0, Lher;->c:Lhez;

    invoke-virtual {v0}, Lhez;->hashCode()I

    move-result v0

    goto :goto_1

    .line 41142
    :cond_2
    iget-object v0, p0, Lher;->d:Lhfa;

    invoke-virtual {v0}, Lhfa;->hashCode()I

    move-result v0

    goto :goto_2

    .line 41143
    :cond_3
    iget-object v1, p0, Lher;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
