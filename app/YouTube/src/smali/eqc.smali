.class public final Leqc;
.super Ljava/lang/Object;


# instance fields
.field private final a:[B

.field private final b:I

.field private c:I


# direct methods
.method private constructor <init>([BII)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Leqc;->a:[B

    iput p2, p0, Leqc;->c:I

    add-int v0, p2, p3

    iput v0, p0, Leqc;->b:I

    return-void
.end method

.method public static a([B)Leqc;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Leqc;->a([BII)Leqc;

    move-result-object v0

    return-object v0
.end method

.method public static a([BII)Leqc;
    .locals 1

    new-instance v0, Leqc;

    invoke-direct {v0, p0, p1, p2}, Leqc;-><init>([BII)V

    return-object v0
.end method

.method public static b(I)I
    .locals 1

    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method

.method private c(I)V
    .locals 4

    int-to-byte v0, p1

    iget v1, p0, Leqc;->c:I

    iget v2, p0, Leqc;->b:I

    if-ne v1, v2, :cond_0

    new-instance v0, Lefe;

    iget v1, p0, Leqc;->c:I

    iget v2, p0, Leqc;->b:I

    invoke-direct {v0, v1, v2}, Lefe;-><init>(II)V

    throw v0

    :cond_0
    iget-object v1, p0, Leqc;->a:[B

    iget v2, p0, Leqc;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Leqc;->c:I

    aput-byte v0, v1, v2

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    iget v0, p0, Leqc;->b:I

    iget v1, p0, Leqc;->c:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(I)V
    .locals 1

    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Leqc;->c(I)V

    return-void

    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Leqc;->c(I)V

    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    invoke-static {p1, p2}, Leqm;->a(II)I

    move-result v0

    invoke-virtual {p0, v0}, Leqc;->a(I)V

    return-void
.end method

.method public final a(IJ)V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Leqc;->a(II)V

    invoke-virtual {p0, p2, p3}, Leqc;->a(J)V

    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Leqc;->a(II)V

    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p0, v1}, Leqc;->a(I)V

    invoke-virtual {p0, v0}, Leqc;->b([B)V

    return-void
.end method

.method public final a(J)V
    .locals 5

    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    long-to-int v0, p1

    invoke-direct {p0, v0}, Leqc;->c(I)V

    return-void

    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Leqc;->c(I)V

    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method public final b([B)V
    .locals 4

    array-length v0, p1

    iget v1, p0, Leqc;->b:I

    iget v2, p0, Leqc;->c:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Leqc;->a:[B

    iget v3, p0, Leqc;->c:I

    invoke-static {p1, v1, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Leqc;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Leqc;->c:I

    return-void

    :cond_0
    new-instance v0, Lefe;

    iget v1, p0, Leqc;->c:I

    iget v2, p0, Leqc;->b:I

    invoke-direct {v0, v1, v2}, Lefe;-><init>(II)V

    throw v0
.end method
