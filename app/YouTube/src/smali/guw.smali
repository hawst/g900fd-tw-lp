.class public final Lguw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgue;


# instance fields
.field private final a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

.field private final b:Lffw;

.field private final c:Lu;

.field private final d:Ljava/util/Map;

.field private final e:Lfgg;

.field private final f:Lbt;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/upload/service/UploadService;Lfgi;Landroid/os/Handler;Lu;)V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lguw;->d:Ljava/util/Map;

    .line 28
    new-instance v0, Lgux;

    invoke-direct {v0, p0}, Lgux;-><init>(Lguw;)V

    iput-object v0, p0, Lguw;->e:Lfgg;

    .line 53
    new-instance v0, Lguy;

    invoke-direct {v0, p0}, Lguy;-><init>(Lguw;)V

    iput-object v0, p0, Lguw;->f:Lbt;

    .line 67
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/upload/service/UploadService;

    iput-object v0, p0, Lguw;->a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

    .line 68
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    new-instance v0, Lffw;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p2, v1, v2, p3}, Lffw;-><init>(Lfgi;ZZLandroid/os/Handler;)V

    iput-object v0, p0, Lguw;->b:Lffw;

    .line 74
    iget-object v0, p0, Lguw;->b:Lffw;

    iget-object v1, p0, Lguw;->e:Lfgg;

    invoke-virtual {v0, v1}, Lffw;->a(Lfgg;)V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lguw;->c:Lu;

    .line 79
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 124
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    iget-object v0, p0, Lguw;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-direct {p0}, Lguw;->b()V

    .line 129
    :cond_0
    iget-object v0, p0, Lguw;->d:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 130
    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 131
    iget-object v0, p0, Lguw;->b:Lffw;

    iget-object v1, v0, Lffw;->d:Landroid/os/Handler;

    new-instance v2, Lfgc;

    const-wide/16 v4, 0x32

    invoke-direct {v2, v0, v4, v5, p2}, Lfgc;-><init>(Lffw;JLjava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 130
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 124
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Lgvd;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 83
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    :try_start_1
    iget-object v0, p0, Lguw;->a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgvd;

    .line 89
    iget-object v0, v0, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v3

    .line 90
    iget-object v0, v3, Lgvi;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 91
    iget-object v0, v3, Lgvi;->j:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 92
    iget v0, v3, Lgvi;->d:I
    :try_end_1
    .catch Lgub; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_2

    move v0, v1

    .line 112
    :goto_2
    monitor-exit p0

    return v0

    :cond_0
    move v0, v1

    .line 90
    goto :goto_0

    :cond_1
    move v0, v1

    .line 91
    goto :goto_1

    .line 95
    :cond_2
    :try_start_2
    iget-object v0, v3, Lgvi;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 96
    goto :goto_2

    .line 98
    :cond_3
    iget-boolean v0, v3, Lgvi;->i:Z

    if-nez v0, :cond_4

    move v0, v1

    .line 99
    goto :goto_2

    .line 101
    :cond_4
    iget-object v0, v3, Lgvi;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    .line 103
    goto :goto_2

    .line 105
    :cond_5
    iget-object v0, p0, Lguw;->d:Ljava/util/Map;

    iget-object v4, v3, Lgvi;->e:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    .line 106
    goto :goto_2

    .line 108
    :cond_6
    iget-object v0, v3, Lgvi;->e:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lguw;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lgub; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v2

    .line 109
    goto :goto_2

    .line 110
    :catch_0
    move-exception v0

    .line 111
    :try_start_3
    iget-object v2, p0, Lguw;->a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

    invoke-static {p1, v0}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v1

    .line 112
    goto :goto_2

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lguw;->c:Lu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 193
    :goto_0
    monitor-exit p0

    return-void

    .line 191
    :cond_0
    :try_start_1
    iget-object v0, p0, Lguw;->c:Lu;

    iget-object v0, p0, Lguw;->f:Lbt;

    .line 192
    iget-object v0, p0, Lguw;->c:Lu;

    invoke-interface {v0}, Lu;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lguw;->b:Lffw;

    invoke-virtual {v0}, Lffw;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 192
    :cond_1
    :try_start_2
    iget-object v0, p0, Lguw;->b:Lffw;

    invoke-virtual {v0}, Lffw;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 196
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lguw;->c:Lu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 201
    :goto_0
    monitor-exit p0

    return-void

    .line 199
    :cond_0
    :try_start_1
    iget-object v0, p0, Lguw;->c:Lu;

    iget-object v0, p0, Lguw;->f:Lbt;

    .line 200
    iget-object v0, p0, Lguw;->b:Lffw;

    invoke-virtual {v0}, Lffw;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lguw;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 119
    invoke-direct {p0}, Lguw;->c()V

    .line 120
    iget-object v0, p0, Lguw;->b:Lffw;

    invoke-virtual {v0}, Lffw;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    iget-object v0, p0, Lguw;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 164
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lb;->c(Z)V

    .line 165
    iget-object v1, p0, Lguw;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    invoke-direct {p0}, Lguw;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    :cond_0
    :try_start_1
    iget-object v1, p0, Lguw;->a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

    new-instance v2, Lgva;

    invoke-direct {v2, p0}, Lgva;-><init>(Lguw;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Ljava/lang/String;Lguc;)V
    :try_end_1
    .catch Lgub; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 185
    :goto_1
    monitor-exit p0

    return-void

    .line 164
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 182
    :catch_0
    move-exception v1

    .line 183
    :try_start_2
    iget-object v2, p0, Lguw;->a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

    invoke-static {v0, v1}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Ljava/lang/String;Lhzd;)V
    .locals 3

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lguw;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 138
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lb;->c(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    :try_start_1
    iget-object v1, p0, Lguw;->a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

    new-instance v2, Lguz;

    invoke-direct {v2, p0, p2}, Lguz;-><init>(Lguw;Lhzd;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Ljava/lang/String;Lguc;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    :goto_1
    monitor-exit p0

    return-void

    .line 138
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 156
    :catch_0
    move-exception v1

    .line 157
    :try_start_2
    iget-object v2, p0, Lguw;->a:Lcom/google/android/libraries/youtube/upload/service/UploadService;

    invoke-static {v0, v1}, Lcom/google/android/libraries/youtube/upload/service/UploadService;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 19
    check-cast p2, Lgvd;

    invoke-direct {p0, p1, p2}, Lguw;->a(Ljava/lang/String;Lgvd;)Z

    move-result v0

    return v0
.end method
