.class public abstract Lghi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgku;


# instance fields
.field final a:Leuk;

.field final b:Lezj;

.field private final c:Lgku;

.field private final d:J


# direct methods
.method protected constructor <init>(Leuk;Lgku;Lezj;J)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lghi;->a:Leuk;

    .line 77
    iput-object p2, p0, Lghi;->c:Lgku;

    .line 78
    iput-object p3, p0, Lghi;->b:Lezj;

    .line 79
    iput-wide p4, p0, Lghi;->d:J

    .line 80
    return-void
.end method

.method public static a(Leuk;Lgku;Lezj;J)Lghi;
    .locals 7

    .prologue
    .line 51
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x9a7ec800L

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "time to live must be >=0 and <= 2592000000"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 56
    new-instance v0, Lghk;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lghk;-><init>(Leuk;Lgku;Lezj;J)V

    return-object v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public final a(Ljava/lang/Object;Leuc;)V
    .locals 8

    .prologue
    .line 93
    iget-wide v0, p0, Lghi;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 94
    iget-object v0, p0, Lghi;->a:Leuk;

    invoke-virtual {p0, p1}, Lghi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Leuk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghh;

    .line 95
    iget-object v1, p0, Lghi;->b:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v2

    .line 96
    if-eqz v0, :cond_0

    iget-wide v4, v0, Lghh;->b:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    iget-wide v4, v0, Lghh;->b:J

    iget-wide v6, p0, Lghi;->d:J

    add-long/2addr v4, v6

    cmp-long v1, v4, v2

    if-ltz v1, :cond_0

    .line 100
    iget-object v0, v0, Lghh;->a:Ljava/lang/Object;

    invoke-interface {p2, p1, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 112
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lghi;->c:Lgku;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lghi;->c:Lgku;

    new-instance v1, Lghj;

    invoke-direct {v1, p0, p2}, Lghj;-><init>(Lghi;Leuc;)V

    invoke-interface {v0, p1, v1}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    goto :goto_0

    .line 110
    :cond_1
    new-instance v0, Lghg;

    invoke-direct {v0}, Lghg;-><init>()V

    invoke-interface {p2, p1, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
