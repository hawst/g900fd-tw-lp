.class public abstract Lcvp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgds;
.implements Lgdt;


# instance fields
.field private a:Lgds;

.field private b:Lgdt;


# direct methods
.method public constructor <init>(Lgds;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcvp;->a:Lgds;

    .line 30
    invoke-interface {p1, p0}, Lgds;->a(Lgdt;)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0}, Lgds;->a()V

    .line 47
    return-void
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0, p1, p2}, Lgds;->a(FF)V

    .line 97
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0, p1}, Lgds;->a(I)V

    .line 87
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0, p1, p2, p3}, Lgds;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 37
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0, p1}, Lgds;->a(Landroid/view/Surface;)V

    .line 112
    return-void
.end method

.method public final a(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0, p1}, Lgds;->a(Landroid/view/SurfaceHolder;)V

    .line 107
    return-void
.end method

.method public final a(Lgds;)V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcvp;->b:Lgdt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvp;->b:Lgdt;

    invoke-interface {v0, p0}, Lgdt;->a(Lgds;)V

    .line 163
    :cond_0
    return-void
.end method

.method public final a(Lgds;II)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcvp;->b:Lgdt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvp;->b:Lgdt;

    invoke-interface {v0, p0, p2, p3}, Lgdt;->a(Lgds;II)V

    .line 158
    :cond_0
    return-void
.end method

.method public a(Lgdt;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcvp;->b:Lgdt;

    .line 122
    return-void
.end method

.method public final a(II)Z
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcvp;->b:Lgdt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvp;->b:Lgdt;

    invoke-interface {v0, p1, p2}, Lgdt;->a(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0}, Lgds;->b()V

    .line 52
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0, p1}, Lgds;->b(I)V

    .line 102
    return-void
.end method

.method public final b(II)Z
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcvp;->b:Lgdt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvp;->b:Lgdt;

    invoke-interface {v0, p1, p2}, Lgdt;->b(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0}, Lgds;->c()V

    .line 57
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 152
    invoke-virtual {p0, p1}, Lcvp;->d(I)V

    .line 153
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0}, Lgds;->d()V

    .line 67
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcvp;->b:Lgdt;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcvp;->b:Lgdt;

    invoke-interface {v0, p1}, Lgdt;->c(I)V

    .line 191
    :cond_0
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0}, Lgds;->e()I

    move-result v0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcvp;->a:Lgds;

    invoke-interface {v0}, Lgds;->f()I

    move-result v0

    return v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcvp;->b:Lgdt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvp;->b:Lgdt;

    invoke-interface {v0}, Lgdt;->g()V

    .line 143
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcvp;->b:Lgdt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvp;->b:Lgdt;

    invoke-interface {v0}, Lgdt;->h()V

    .line 148
    :cond_0
    return-void
.end method
