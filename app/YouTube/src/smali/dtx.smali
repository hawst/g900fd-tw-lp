.class public final enum Ldtx;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldtx;

.field public static final enum b:Ldtx;

.field public static final enum c:Ldtx;

.field private static final synthetic d:[Ldtx;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 8
    new-instance v0, Ldtx;

    const-string v1, "PLAYING_VIDEO"

    invoke-direct {v0, v1, v2}, Ldtx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtx;->a:Ldtx;

    .line 10
    new-instance v0, Ldtx;

    const-string v1, "SHOWING_TV_QUEUE"

    invoke-direct {v0, v1, v3}, Ldtx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtx;->b:Ldtx;

    .line 12
    new-instance v0, Ldtx;

    const-string v1, "CONNECTED_ONLY"

    invoke-direct {v0, v1, v4}, Ldtx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldtx;->c:Ldtx;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Ldtx;

    sget-object v1, Ldtx;->a:Ldtx;

    aput-object v1, v0, v2

    sget-object v1, Ldtx;->b:Ldtx;

    aput-object v1, v0, v3

    sget-object v1, Ldtx;->c:Ldtx;

    aput-object v1, v0, v4

    sput-object v0, Ldtx;->d:[Ldtx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldtx;
    .locals 1

    .prologue
    .line 6
    const-class v0, Ldtx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldtx;

    return-object v0
.end method

.method public static values()[Ldtx;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Ldtx;->d:[Ldtx;

    invoke-virtual {v0}, [Ldtx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldtx;

    return-object v0
.end method
