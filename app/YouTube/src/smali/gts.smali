.class public final Lgts;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgtn;


# instance fields
.field private final a:Lfgi;


# direct methods
.method public constructor <init>(Lfgi;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgi;

    iput-object v0, p0, Lgts;->a:Lfgi;

    .line 24
    return-void
.end method

.method private a(Lgvd;)Lguc;
    .locals 6

    .prologue
    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    iget-object v0, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v1

    .line 58
    iget-object v0, v1, Lgvi;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 59
    iget-boolean v0, v1, Lgvi;->m:Z

    if-eqz v0, :cond_1

    .line 60
    new-instance v0, Lgtt;

    invoke-direct {v0, p0}, Lgtt;-><init>(Lgts;)V

    .line 93
    :goto_1
    return-object v0

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    :cond_1
    new-instance v0, Lhfi;

    invoke-direct {v0}, Lhfi;-><init>()V

    .line 74
    iget-object v2, v1, Lgvi;->e:Ljava/lang/String;

    iput-object v2, v0, Lhfi;->c:Ljava/lang/String;

    .line 75
    new-instance v2, Lhyk;

    invoke-direct {v2}, Lhyk;-><init>()V

    iput-object v2, v0, Lhfi;->b:Lhyk;

    .line 76
    iget-object v2, v0, Lhfi;->b:Lhyk;

    new-instance v3, Lhuc;

    invoke-direct {v3}, Lhuc;-><init>()V

    iput-object v3, v2, Lhyk;->a:Lhuc;

    .line 77
    iget-object v2, v0, Lhfi;->b:Lhyk;

    iget-object v2, v2, Lhyk;->a:Lhuc;

    iget-object v3, v1, Lgvi;->g:Ljava/lang/String;

    iput-object v3, v2, Lhuc;->a:Ljava/lang/String;

    .line 80
    :try_start_0
    iget-object v2, p0, Lgts;->a:Lfgi;

    iget-object v1, v1, Lgvi;->a:Ljava/lang/String;

    iget-object v3, v2, Lfgi;->e:Lfco;

    new-instance v4, Lfst;

    iget-object v5, v2, Lfgi;->b:Lfsz;

    iget-object v2, v2, Lfgi;->c:Lgix;

    invoke-interface {v2, v1}, Lgix;->a(Ljava/lang/String;)Lgit;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lfst;-><init>(Lfsz;Lgit;)V

    invoke-virtual {v4, v0}, Lfst;->a(Lidh;)V

    invoke-virtual {v3, v4}, Lfco;->a(Lfsp;)Lidh;

    move-result-object v0

    check-cast v0, Lhfj;

    .line 81
    new-instance v0, Lgtu;

    invoke-direct {v0, p0}, Lgtu;-><init>(Lgts;)V
    :try_end_0
    .catch Lfdv; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 92
    :catch_0
    move-exception v0

    .line 93
    invoke-static {v0}, La;->a(Lfdv;)Lguc;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)J
    .locals 2

    .prologue
    .line 18
    check-cast p1, Lgvd;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v0

    iget v1, v0, Lgvi;->d:I

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lgvi;->n:Z

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lgvi;->i:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lgvi;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lgvi;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lguc;
    .locals 1

    .prologue
    .line 18
    check-cast p2, Lgvd;

    invoke-direct {p0, p2}, Lgts;->a(Lgvd;)Lguc;

    move-result-object v0

    return-object v0
.end method
