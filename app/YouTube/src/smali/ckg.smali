.class final Lckg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lckf;


# instance fields
.field a:Landroid/app/AlertDialog;

.field b:Landroid/app/AlertDialog;

.field c:Lcjw;

.field d:Lfwe;

.field final synthetic e:Lcjs;

.field private f:Landroid/app/AlertDialog;


# direct methods
.method constructor <init>(Lcjs;)V
    .locals 0

    .prologue
    .line 742
    iput-object p1, p0, Lckg;->e:Lcjs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfwe;)V
    .locals 4

    .prologue
    .line 752
    iget-object v0, p0, Lckg;->f:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 753
    new-instance v0, Lcjw;

    iget-object v1, p0, Lckg;->e:Lcjs;

    invoke-direct {v0, v1}, Lcjw;-><init>(Lcjs;)V

    iput-object v0, p0, Lckg;->c:Lcjw;

    .line 754
    new-instance v0, Leyv;

    iget-object v1, p0, Lckg;->e:Lcjs;

    iget-object v1, v1, Lcjs;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Leyv;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0902ba

    invoke-virtual {v0, v1}, Leyv;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lckg;->c:Lcjw;

    new-instance v2, Lckh;

    invoke-direct {v2, p0}, Lckh;-><init>(Lckg;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lckg;->f:Landroid/app/AlertDialog;

    .line 757
    :cond_0
    iput-object p1, p0, Lckg;->d:Lfwe;

    .line 758
    iget-object v0, p0, Lckg;->c:Lcjw;

    iget-object v0, v0, Lcjw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 759
    iget-object v0, p0, Lckg;->c:Lcjw;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lcjs;->a(Lfwe;)Lfwf;

    move-result-object v2

    iget-boolean v2, v2, Lfwf;->b:Z

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, p0, Lckg;->e:Lcjs;

    iget-object v2, v2, Lcjs;->i:Lccp;

    iget-object v2, v2, Lccp;->b:Lgca;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lckg;->e:Lcjs;

    iget-object v2, v2, Lcjs;->i:Lccp;

    iget-object v2, v2, Lccp;->b:Lgca;

    iget-object v2, v2, Lgca;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lckg;->e:Lcjs;

    iget-object v2, v2, Lcjs;->i:Lccp;

    iget-object v2, v2, Lccp;->b:Lgca;

    iget-object v2, v2, Lgca;->d:Ljava/lang/String;

    iget-object v3, p1, Lfwe;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v0, v0, Lcjw;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 760
    iget-object v0, p0, Lckg;->f:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 761
    return-void

    .line 759
    :cond_2
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
