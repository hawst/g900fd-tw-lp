.class public final Lhid;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhid;


# instance fields
.field public b:Lhhu;

.field public c:Lhhr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64924
    const/4 v0, 0x0

    new-array v0, v0, [Lhid;

    sput-object v0, Lhid;->a:[Lhid;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64925
    invoke-direct {p0}, Lidf;-><init>()V

    .line 64928
    iput-object v0, p0, Lhid;->b:Lhhu;

    .line 64931
    iput-object v0, p0, Lhid;->c:Lhhr;

    .line 64925
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 64974
    const/4 v0, 0x0

    .line 64975
    iget-object v1, p0, Lhid;->b:Lhhu;

    if-eqz v1, :cond_0

    .line 64976
    const v0, 0x309eeb1

    iget-object v1, p0, Lhid;->b:Lhhu;

    .line 64977
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 64979
    :cond_0
    iget-object v1, p0, Lhid;->c:Lhhr;

    if-eqz v1, :cond_1

    .line 64980
    const v1, 0x314520e

    iget-object v2, p0, Lhid;->c:Lhhr;

    .line 64981
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64983
    :cond_1
    iget-object v1, p0, Lhid;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64984
    iput v0, p0, Lhid;->J:I

    .line 64985
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 64921
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhid;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhid;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhid;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhid;->b:Lhhu;

    if-nez v0, :cond_2

    new-instance v0, Lhhu;

    invoke-direct {v0}, Lhhu;-><init>()V

    iput-object v0, p0, Lhid;->b:Lhhu;

    :cond_2
    iget-object v0, p0, Lhid;->b:Lhhu;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhid;->c:Lhhr;

    if-nez v0, :cond_3

    new-instance v0, Lhhr;

    invoke-direct {v0}, Lhhr;-><init>()V

    iput-object v0, p0, Lhid;->c:Lhhr;

    :cond_3
    iget-object v0, p0, Lhid;->c:Lhhr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x184f758a -> :sswitch_1
        0x18a29072 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 64962
    iget-object v0, p0, Lhid;->b:Lhhu;

    if-eqz v0, :cond_0

    .line 64963
    const v0, 0x309eeb1

    iget-object v1, p0, Lhid;->b:Lhhu;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64965
    :cond_0
    iget-object v0, p0, Lhid;->c:Lhhr;

    if-eqz v0, :cond_1

    .line 64966
    const v0, 0x314520e

    iget-object v1, p0, Lhid;->c:Lhhr;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64968
    :cond_1
    iget-object v0, p0, Lhid;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 64970
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64943
    if-ne p1, p0, :cond_1

    .line 64948
    :cond_0
    :goto_0
    return v0

    .line 64944
    :cond_1
    instance-of v2, p1, Lhid;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 64945
    :cond_2
    check-cast p1, Lhid;

    .line 64946
    iget-object v2, p0, Lhid;->b:Lhhu;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhid;->b:Lhhu;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhid;->c:Lhhr;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhid;->c:Lhhr;

    if-nez v2, :cond_3

    .line 64947
    :goto_2
    iget-object v2, p0, Lhid;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhid;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 64948
    goto :goto_0

    .line 64946
    :cond_4
    iget-object v2, p0, Lhid;->b:Lhhu;

    iget-object v3, p1, Lhid;->b:Lhhu;

    invoke-virtual {v2, v3}, Lhhu;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhid;->c:Lhhr;

    iget-object v3, p1, Lhid;->c:Lhhr;

    .line 64947
    invoke-virtual {v2, v3}, Lhhr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhid;->I:Ljava/util/List;

    iget-object v3, p1, Lhid;->I:Ljava/util/List;

    .line 64948
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 64952
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 64954
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhid;->b:Lhhu;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 64955
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhid;->c:Lhhr;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 64956
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhid;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 64957
    return v0

    .line 64954
    :cond_0
    iget-object v0, p0, Lhid;->b:Lhhu;

    invoke-virtual {v0}, Lhhu;->hashCode()I

    move-result v0

    goto :goto_0

    .line 64955
    :cond_1
    iget-object v0, p0, Lhid;->c:Lhhr;

    invoke-virtual {v0}, Lhhr;->hashCode()I

    move-result v0

    goto :goto_1

    .line 64956
    :cond_2
    iget-object v1, p0, Lhid;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
