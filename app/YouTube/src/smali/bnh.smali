.class public final enum Lbnh;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lbne;


# static fields
.field public static final enum a:Lbnh;

.field private static enum c:Lbnh;

.field private static enum d:Lbnh;

.field private static enum e:Lbnh;

.field private static final synthetic g:[Lbnh;


# instance fields
.field public final b:Lffo;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lbnh;

    const-string v1, "ANY"

    sget-object v2, Lffo;->a:Lffo;

    const v3, 0x7f0900d3

    invoke-direct {v0, v1, v4, v2, v3}, Lbnh;-><init>(Ljava/lang/String;ILffo;I)V

    sput-object v0, Lbnh;->a:Lbnh;

    .line 15
    new-instance v0, Lbnh;

    const-string v1, "TODAY"

    sget-object v2, Lffo;->b:Lffo;

    const v3, 0x7f0900d0

    invoke-direct {v0, v1, v5, v2, v3}, Lbnh;-><init>(Ljava/lang/String;ILffo;I)V

    sput-object v0, Lbnh;->c:Lbnh;

    .line 16
    new-instance v0, Lbnh;

    const-string v1, "THIS_WEEK"

    sget-object v2, Lffo;->c:Lffo;

    const v3, 0x7f0900d1

    invoke-direct {v0, v1, v6, v2, v3}, Lbnh;-><init>(Ljava/lang/String;ILffo;I)V

    sput-object v0, Lbnh;->d:Lbnh;

    .line 17
    new-instance v0, Lbnh;

    const-string v1, "THIS_MONTH"

    sget-object v2, Lffo;->d:Lffo;

    const v3, 0x7f0900d2

    invoke-direct {v0, v1, v7, v2, v3}, Lbnh;-><init>(Ljava/lang/String;ILffo;I)V

    sput-object v0, Lbnh;->e:Lbnh;

    .line 13
    const/4 v0, 0x4

    new-array v0, v0, [Lbnh;

    sget-object v1, Lbnh;->a:Lbnh;

    aput-object v1, v0, v4

    sget-object v1, Lbnh;->c:Lbnh;

    aput-object v1, v0, v5

    sget-object v1, Lbnh;->d:Lbnh;

    aput-object v1, v0, v6

    sget-object v1, Lbnh;->e:Lbnh;

    aput-object v1, v0, v7

    sput-object v0, Lbnh;->g:[Lbnh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILffo;I)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput-object p3, p0, Lbnh;->b:Lffo;

    .line 24
    iput p4, p0, Lbnh;->f:I

    .line 25
    return-void
.end method

.method public static a(I)Lbnh;
    .locals 2

    .prologue
    .line 50
    invoke-static {}, Lbnh;->values()[Lbnh;

    move-result-object v0

    .line 51
    if-ltz p0, :cond_0

    array-length v1, v0

    if-ge p0, v1, :cond_0

    .line 52
    aget-object v0, v0, p0

    .line 55
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lbnh;->a:Lbnh;

    goto :goto_0
.end method

.method public static a(Lfxn;)Lbnh;
    .locals 2

    .prologue
    .line 59
    if-nez p0, :cond_0

    .line 60
    sget-object v0, Lbnh;->a:Lbnh;

    .line 70
    :goto_0
    return-object v0

    .line 62
    :cond_0
    sget-object v0, Lbni;->a:[I

    invoke-virtual {p0}, Lfxn;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 64
    sget-object v0, Lbnh;->a:Lbnh;

    goto :goto_0

    .line 66
    :pswitch_0
    sget-object v0, Lbnh;->c:Lbnh;

    goto :goto_0

    .line 68
    :pswitch_1
    sget-object v0, Lbnh;->d:Lbnh;

    goto :goto_0

    .line 70
    :pswitch_2
    sget-object v0, Lbnh;->e:Lbnh;

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Lbnh;
    .locals 3

    .prologue
    .line 37
    if-nez p0, :cond_0

    .line 38
    sget-object v0, Lbnh;->a:Lbnh;

    .line 45
    :goto_0
    return-object v0

    .line 42
    :cond_0
    :try_start_0
    invoke-static {p0}, Lbnh;->valueOf(Ljava/lang/String;)Lbnh;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 44
    :catch_0
    move-exception v0

    const-string v1, "Attempted to search with unsupported upload date: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 45
    sget-object v0, Lbnh;->a:Lbnh;

    goto :goto_0

    .line 44
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lbnh;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lbnh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbnh;

    return-object v0
.end method

.method public static values()[Lbnh;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lbnh;->g:[Lbnh;

    invoke-virtual {v0}, [Lbnh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbnh;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lbnh;->f:I

    return v0
.end method
