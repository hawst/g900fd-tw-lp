.class public final Ldyk;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private a:Ldyi;


# direct methods
.method public constructor <init>(Ldyi;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    .line 52
    iput-object p1, p0, Ldyk;->a:Ldyi;

    .line 53
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Ldyk;->a:Ldyi;

    if-nez v0, :cond_0

    .line 64
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Ldyk;->a:Ldyi;

    invoke-virtual {v0}, Ldyi;->a()V

    .line 62
    iget-object v0, p0, Ldyk;->a:Ldyi;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Ldyk;->a:Ldyi;

    goto :goto_0
.end method

.method public final flush()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Ldyk;->a:Ldyi;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ldyl;

    invoke-direct {v0}, Ldyl;-><init>()V

    throw v0

    .line 70
    :cond_0
    iget-object v0, p0, Ldyk;->a:Ldyi;

    invoke-virtual {v0}, Ldyi;->a()V

    .line 71
    iget-object v0, p0, Ldyk;->a:Ldyi;

    .line 72
    return-void
.end method

.method public final write(I)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Ldyk;->a:Ldyi;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Ldyl;

    invoke-direct {v0}, Ldyl;-><init>()V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Ldyk;->a:Ldyi;

    invoke-virtual {v0}, Ldyi;->c()Ljava/nio/ByteBuffer;

    move-result-object v0

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 93
    return-void
.end method

.method public final write([B)V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Ldyk;->a:Ldyi;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ldyl;

    invoke-direct {v0}, Ldyl;-><init>()V

    throw v0

    .line 78
    :cond_0
    iget-object v0, p0, Ldyk;->a:Ldyi;

    const/4 v1, 0x0

    array-length v2, p1

    invoke-virtual {v0, p1, v1, v2}, Ldyi;->b([BII)V

    .line 79
    return-void
.end method

.method public final write([BII)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Ldyk;->a:Ldyi;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Ldyl;

    invoke-direct {v0}, Ldyl;-><init>()V

    throw v0

    .line 85
    :cond_0
    iget-object v0, p0, Ldyk;->a:Ldyi;

    invoke-virtual {v0, p1, p2, p3}, Ldyi;->b([BII)V

    .line 86
    return-void
.end method
