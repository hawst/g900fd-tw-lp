.class public final Ldjv;
.super Ldkh;
.source "SourceFile"


# instance fields
.field l:Lfxg;

.field m:Leue;

.field private final t:Lgku;

.field private final u:Landroid/os/Handler;

.field private final v:Ldjw;

.field private final w:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Lfxe;Ldlb;)V
    .locals 9

    .prologue
    .line 92
    move-object/from16 v0, p8

    iget-object v8, v0, Ldlb;->c:Ldlh;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v8}, Ldkh;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ldlh;)V

    .line 100
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-interface/range {p7 .. p7}, Lfxe;->e()Lgku;

    move-result-object v1

    iput-object v1, p0, Ldjv;->t:Lgku;

    .line 102
    move-object/from16 v0, p8

    iget-object v1, v0, Ldlb;->b:Lfxg;

    iput-object v1, p0, Ldjv;->l:Lfxg;

    .line 103
    move-object/from16 v0, p8

    iget-object v1, v0, Ldlb;->a:Ljava/lang/String;

    iput-object v1, p0, Ldjv;->w:Ljava/lang/String;

    .line 104
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Ldjv;->u:Landroid/os/Handler;

    .line 105
    new-instance v1, Ldjw;

    invoke-direct {v1, p0}, Ldjw;-><init>(Ldjv;)V

    iput-object v1, p0, Ldjv;->v:Ldjw;

    .line 106
    invoke-virtual {p0}, Ldjv;->h()V

    .line 107
    return-void
.end method

.method public constructor <init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Lfxe;Ljava/lang/String;I)V
    .locals 12

    .prologue
    .line 58
    .line 65
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v8

    sget-object v10, Ldaq;->a:[B

    const-string v11, ""

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v9, p9

    .line 58
    invoke-direct/range {v1 .. v11}, Ldkh;-><init>(Lcvq;Levn;Lcwq;Ldaq;Leyt;Lfac;Ljava/util/List;I[BLjava/lang/String;)V

    .line 69
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-static/range {p8 .. p8}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ldjv;->w:Ljava/lang/String;

    .line 71
    invoke-interface/range {p7 .. p7}, Lfxe;->e()Lgku;

    move-result-object v1

    iput-object v1, p0, Ldjv;->t:Lgku;

    .line 73
    invoke-interface/range {p7 .. p7}, Lfxe;->a()Lfxj;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, Lfxj;->c(Ljava/lang/String;)Lfxg;

    move-result-object v1

    iput-object v1, p0, Ldjv;->l:Lfxg;

    .line 74
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Ldjv;->u:Landroid/os/Handler;

    .line 75
    new-instance v1, Ldjw;

    invoke-direct {v1, p0}, Ldjw;-><init>(Ldjv;)V

    iput-object v1, p0, Ldjv;->v:Ldjw;

    .line 76
    sget-object v1, Lgok;->a:Lgok;

    invoke-virtual {p0, v1}, Ldjv;->a(Lgok;)V

    .line 77
    invoke-virtual {p0}, Ldjv;->h()V

    .line 78
    return-void
.end method

.method static synthetic a(Ldjv;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ldjv;->v()V

    return-void
.end method

.method private v()V
    .locals 8

    .prologue
    .line 189
    iget v0, p0, Ldjv;->s:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 190
    iget v0, p0, Ldjv;->s:I

    iget-object v1, p0, Ldjv;->n:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 191
    new-instance v0, Ldki;

    invoke-direct {v0, p0}, Ldki;-><init>(Ldkh;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldjv;->q:Leue;

    .line 192
    iget-object v0, p0, Ldjv;->a:Ldaq;

    iget-object v1, p0, Ldjv;->n:[Ljava/lang/String;

    iget v2, p0, Ldjv;->s:I

    aget-object v1, v1, v2

    sget-object v2, Ldaq;->a:[B

    const-string v3, ""

    iget-object v4, p0, Ldjv;->w:Ljava/lang/String;

    iget v5, p0, Ldjv;->s:I

    const/4 v6, -0x1

    iget-object v7, p0, Ldjv;->q:Leue;

    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    .line 203
    :goto_1
    return-void

    .line 189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_1
    sget-object v0, Lgok;->f:Lgok;

    invoke-virtual {p0, v0}, Ldjv;->a(Lgok;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0}, Ldkh;->a()V

    .line 120
    iget-object v0, p0, Ldjv;->m:Leue;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Ldjv;->m:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Ldjv;->m:Leue;

    .line 124
    :cond_0
    return-void
.end method

.method protected final b(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 156
    iget-object v0, p0, Ldjv;->t:Lgku;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Ldjv;->l:Lfxg;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldjv;->n:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    .line 158
    :goto_0
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 160
    :goto_1
    return v0

    .line 157
    :cond_0
    const v0, 0x7fffffff

    goto :goto_0

    .line 160
    :cond_1
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method

.method protected final c()I
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Ldjv;->f:Lgok;

    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget v0, p0, Ldjv;->r:I

    .line 136
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Ldjv;->s:I

    goto :goto_0
.end method

.method protected final c(I)V
    .locals 3

    .prologue
    .line 166
    invoke-virtual {p0, p1}, Ldjv;->b(I)I

    move-result v0

    iput v0, p0, Ldjv;->s:I

    .line 167
    if-gez p1, :cond_0

    .line 181
    :goto_0
    return-void

    .line 170
    :cond_0
    sget-object v0, Lgok;->b:Lgok;

    invoke-virtual {p0, v0}, Ldjv;->a(Lgok;)V

    .line 171
    iget-object v0, p0, Ldjv;->n:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    .line 172
    invoke-direct {p0}, Ldjv;->v()V

    goto :goto_0

    .line 173
    :cond_1
    iget-object v0, p0, Ldjv;->l:Lfxg;

    if-eqz v0, :cond_2

    .line 174
    invoke-virtual {p0}, Ldjv;->u()V

    goto :goto_0

    .line 175
    :cond_2
    iget-object v0, p0, Ldjv;->n:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_3

    .line 176
    iget v0, p0, Ldjv;->s:I

    iput v0, p0, Ldjv;->r:I

    .line 177
    sget-object v0, Lgok;->f:Lgok;

    invoke-virtual {p0, v0}, Ldjv;->a(Lgok;)V

    goto :goto_0

    .line 179
    :cond_3
    iget-object v0, p0, Ldjv;->c:Levn;

    new-instance v1, Lczs;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lczs;-><init>(Z)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final d()[B
    .locals 1

    .prologue
    .line 142
    sget-object v0, Ldaq;->a:[B

    return-object v0
.end method

.method public final f()Ldkg;
    .locals 4

    .prologue
    .line 111
    new-instance v1, Ldlb;

    iget-object v2, p0, Ldjv;->l:Lfxg;

    iget-object v3, p0, Ldjv;->w:Ljava/lang/String;

    .line 114
    invoke-super {p0}, Ldkh;->f()Ldkg;

    move-result-object v0

    check-cast v0, Ldlh;

    invoke-direct {v1, v2, v3, v0}, Ldlb;-><init>(Lfxg;Ljava/lang/String;Ldlh;)V

    return-object v1
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ldjv;->w:Ljava/lang/String;

    return-object v0
.end method

.method u()V
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Ldjv;->v:Ldjw;

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldjv;->m:Leue;

    .line 185
    iget-object v0, p0, Ldjv;->t:Lgku;

    iget-object v1, p0, Ldjv;->l:Lfxg;

    iget-object v2, p0, Ldjv;->u:Landroid/os/Handler;

    iget-object v3, p0, Ldjv;->m:Leue;

    invoke-static {v2, v3}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 186
    return-void
.end method

.method protected final w_()Z
    .locals 2

    .prologue
    .line 147
    iget v0, p0, Ldjv;->r:I

    iget-object v1, p0, Ldjv;->n:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Ldjv;->l:Lfxg;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
