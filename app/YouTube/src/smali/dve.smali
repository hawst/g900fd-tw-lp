.class final Ldve;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private synthetic a:Lexd;

.field private synthetic b:Ldxf;

.field private synthetic c:Ldyg;

.field private synthetic d:Ldwq;

.field private synthetic e:Lgjx;

.field private synthetic f:Ldvb;


# direct methods
.method constructor <init>(Ldvb;Lexd;Ldxf;Ldyg;Ldwq;Lgjx;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Ldve;->f:Ldvb;

    iput-object p2, p0, Ldve;->a:Lexd;

    iput-object p3, p0, Ldve;->b:Ldxf;

    iput-object p4, p0, Ldve;->c:Ldyg;

    iput-object p5, p0, Ldve;->d:Ldwq;

    iput-object p6, p0, Ldve;->e:Lgjx;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 164
    iget-object v0, p0, Ldve;->f:Ldvb;

    iget-boolean v0, v0, Ldvb;->m:Z

    if-nez v0, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 169
    :pswitch_0
    iget-object v0, p0, Ldve;->a:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    iget-object v0, p0, Ldve;->b:Ldxf;

    iget-object v1, p0, Ldve;->f:Ldvb;

    iget-object v1, v1, Ldvb;->c:Leuc;

    invoke-interface {v0, v1}, Ldxf;->a(Leuc;)V

    .line 177
    :goto_1
    iget-object v0, p0, Ldve;->f:Ldvb;

    iget-object v0, v0, Ldvb;->j:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 172
    :cond_2
    iget-object v0, p0, Ldve;->f:Ldvb;

    iget-object v0, v0, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 173
    iget-object v0, p0, Ldve;->c:Ldyg;

    const-string v1, "Network unavailable. Removing all screens."

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 175
    :cond_3
    iget-object v1, p0, Ldve;->f:Ldvb;

    iget-object v0, v1, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    iget-object v3, v1, Ldvb;->a:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v1, v0}, Ldvb;->a(Ldwr;)V

    goto :goto_2

    :cond_4
    iget-object v0, v1, Ldvb;->b:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    goto :goto_1

    .line 180
    :pswitch_1
    iget-object v0, p0, Ldve;->f:Ldvb;

    invoke-virtual {v0}, Ldvb;->c()V

    .line 181
    iget-object v0, p0, Ldve;->f:Ldvb;

    iget-object v0, v0, Ldvb;->j:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    iget-object v0, p0, Ldve;->f:Ldvb;

    iget-object v0, v0, Ldvb;->j:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 187
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/Set;

    .line 188
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 189
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_5
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwf;

    .line 190
    iget-object v1, v0, Ldwf;->a:Ldtm;

    iget-boolean v1, v1, Ldtm;->d:Z

    if-eqz v1, :cond_5

    .line 191
    iget-object v1, v0, Ldwf;->a:Ldtm;

    iget-object v3, v1, Ldtm;->e:Ldtj;

    .line 194
    iget-object v1, p0, Ldve;->f:Ldvb;

    iget-object v1, v1, Ldvb;->h:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 195
    if-eqz v1, :cond_6

    .line 196
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x5

    if-ge v4, v5, :cond_6

    iget-object v4, p0, Ldve;->d:Ldwq;

    .line 197
    invoke-interface {v4, v0}, Ldwq;->a(Ldwr;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 198
    iget-object v4, p0, Ldve;->c:Ldyg;

    iget-object v0, v0, Ldwf;->a:Ldtm;

    iget-object v0, v0, Ldtm;->c:Ljava/lang/String;

    .line 200
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit16 v7, v7, 0x9c

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "RemoteControl connected/connecting to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " . Will not remove the screen from the list of available devices even though it timed out. Time out count: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-virtual {v4, v0}, Ldyg;->a(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Ldve;->f:Ldvb;

    iget-object v0, v0, Ldvb;->h:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 205
    :cond_6
    iget-object v1, v0, Ldwf;->a:Ldtm;

    iget-object v1, v1, Ldtm;->b:Landroid/net/Uri;

    if-eqz v1, :cond_7

    .line 206
    iget-object v1, p0, Ldve;->c:Ldyg;

    iget-object v3, v0, Ldwf;->a:Ldtm;

    iget-object v3, v3, Ldtm;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Screen "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " timed out. Will check the app status."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ldyg;->a(Ljava/lang/String;)V

    .line 208
    iget-object v1, p0, Ldve;->e:Lgjx;

    .line 209
    iget-object v3, v0, Ldwf;->a:Ldtm;

    iget-object v3, v3, Ldtm;->b:Landroid/net/Uri;

    new-instance v4, Ldvi;

    iget-object v5, p0, Ldve;->f:Ldvb;

    invoke-direct {v4, v5, v0}, Ldvi;-><init>(Ldvb;Ldwf;)V

    .line 208
    invoke-virtual {v1, v3, v4}, Lgjx;->a(Ljava/lang/Object;Leuc;)V

    goto/16 :goto_3

    .line 211
    :cond_7
    new-instance v1, Ldvi;

    iget-object v3, p0, Ldve;->f:Ldvb;

    invoke-direct {v1, v3, v0}, Ldvi;-><init>(Ldvb;Ldwf;)V

    new-instance v0, Ldsp;

    const/4 v3, -0x2

    invoke-direct {v0, v3}, Ldsp;-><init>(I)V

    invoke-virtual {v1, v0}, Ldvi;->a(Ldsp;)V

    goto/16 :goto_3

    .line 219
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/util/Pair;

    .line 220
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ldtj;

    .line 221
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ldvl;

    .line 222
    iget-object v2, p0, Ldve;->f:Ldvb;

    iget-object v2, v2, Ldvb;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_8

    .line 223
    invoke-interface {v0}, Ldvl;->a()V

    .line 225
    :cond_8
    iget-object v0, p0, Ldve;->f:Ldvb;

    iget-object v0, v0, Ldvb;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
