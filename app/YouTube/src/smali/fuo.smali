.class public final Lfuo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:[I

.field private final d:Landroid/graphics/Rect;

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iput-object p1, p0, Lfuo;->a:Landroid/view/View;

    .line 179
    iput-object p2, p0, Lfuo;->b:Landroid/view/View;

    .line 180
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lfuo;->c:[I

    .line 181
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfuo;->d:Landroid/graphics/Rect;

    .line 183
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lfuo;->e:I

    .line 185
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 186
    invoke-direct {p0}, Lfuo;->a()V

    .line 188
    :cond_0
    invoke-virtual {p1, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 189
    return-void
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 206
    iget-object v0, p0, Lfuo;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lfuo;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 246
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Lfuo;->a:Landroid/view/View;

    iget-object v1, p0, Lfuo;->c:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 213
    iget-object v0, p0, Lfuo;->c:[I

    aget v0, v0, v8

    .line 214
    iget-object v1, p0, Lfuo;->c:[I

    aget v1, v1, v5

    .line 215
    iget-object v2, p0, Lfuo;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 216
    iget-object v3, p0, Lfuo;->b:Landroid/view/View;

    iget-object v4, p0, Lfuo;->c:[I

    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 217
    iget-object v3, p0, Lfuo;->c:[I

    aget v3, v3, v8

    .line 218
    iget-object v4, p0, Lfuo;->c:[I

    aget v4, v4, v5

    .line 219
    iget-object v5, p0, Lfuo;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v3, v5

    .line 220
    iget-object v5, p0, Lfuo;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    .line 227
    iget v5, p0, Lfuo;->e:I

    div-int/lit8 v5, v5, 0x2

    .line 228
    iget-object v6, p0, Lfuo;->d:Landroid/graphics/Rect;

    iget v7, p0, Lfuo;->e:I

    sub-int v7, v2, v7

    sub-int v0, v3, v0

    sub-int/2addr v0, v5

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 229
    iget-object v0, p0, Lfuo;->d:Landroid/graphics/Rect;

    sub-int v1, v4, v1

    sub-int/2addr v1, v5

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 230
    iget-object v0, p0, Lfuo;->d:Landroid/graphics/Rect;

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 231
    iget-object v0, p0, Lfuo;->d:Landroid/graphics/Rect;

    iget-object v1, p0, Lfuo;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lfuo;->e:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 232
    iget-object v0, p0, Lfuo;->a:Landroid/view/View;

    new-instance v1, Lfup;

    iget-object v2, p0, Lfuo;->d:Landroid/graphics/Rect;

    iget-object v3, p0, Lfuo;->b:Landroid/view/View;

    invoke-direct {v1, p0, v2, v3}, Lfup;-><init>(Lfuo;Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    goto :goto_0
.end method


# virtual methods
.method public final onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Lfuo;->a()V

    .line 203
    return-void
.end method
