.class public final Lieq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Liep;

.field private b:Lifx;


# direct methods
.method public constructor <init>(Liep;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-nez p1, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Binarizer must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    iput-object p1, p0, Lieq;->a:Liep;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lieq;->a:Liep;

    iget-object v0, v0, Liep;->a:Lieu;

    iget v0, v0, Lieu;->b:I

    return v0
.end method

.method public final b()Lifx;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lieq;->b:Lifx;

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lieq;->a:Liep;

    invoke-virtual {v0}, Liep;->a()Lifx;

    move-result-object v0

    iput-object v0, p0, Lieq;->b:Lifx;

    .line 85
    :cond_0
    iget-object v0, p0, Lieq;->b:Lifx;

    return-object v0
.end method
