.class public final Lhgo;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhgo;


# instance fields
.field public b:Liab;

.field public c:Lhsi;

.field public d:Lhbb;

.field private e:Lheb;

.field private f:Lhdv;

.field private g:Lhds;

.field private h:Lhdz;

.field private i:Lhdt;

.field private j:Lhax;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51664
    const/4 v0, 0x0

    new-array v0, v0, [Lhgo;

    sput-object v0, Lhgo;->a:[Lhgo;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51665
    invoke-direct {p0}, Lidf;-><init>()V

    .line 51668
    iput-object v0, p0, Lhgo;->b:Liab;

    .line 51671
    iput-object v0, p0, Lhgo;->e:Lheb;

    .line 51674
    iput-object v0, p0, Lhgo;->c:Lhsi;

    .line 51677
    iput-object v0, p0, Lhgo;->f:Lhdv;

    .line 51680
    iput-object v0, p0, Lhgo;->g:Lhds;

    .line 51683
    iput-object v0, p0, Lhgo;->h:Lhdz;

    .line 51686
    iput-object v0, p0, Lhgo;->i:Lhdt;

    .line 51689
    iput-object v0, p0, Lhgo;->d:Lhbb;

    .line 51692
    iput-object v0, p0, Lhgo;->j:Lhax;

    .line 51665
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 51777
    const/4 v0, 0x0

    .line 51778
    iget-object v1, p0, Lhgo;->b:Liab;

    if-eqz v1, :cond_0

    .line 51779
    const v0, 0x303c1d6

    iget-object v1, p0, Lhgo;->b:Liab;

    .line 51780
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 51782
    :cond_0
    iget-object v1, p0, Lhgo;->e:Lheb;

    if-eqz v1, :cond_1

    .line 51783
    const v1, 0x3049143

    iget-object v2, p0, Lhgo;->e:Lheb;

    .line 51784
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51786
    :cond_1
    iget-object v1, p0, Lhgo;->c:Lhsi;

    if-eqz v1, :cond_2

    .line 51787
    const v1, 0x3061cf4

    iget-object v2, p0, Lhgo;->c:Lhsi;

    .line 51788
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51790
    :cond_2
    iget-object v1, p0, Lhgo;->f:Lhdv;

    if-eqz v1, :cond_3

    .line 51791
    const v1, 0x3064567

    iget-object v2, p0, Lhgo;->f:Lhdv;

    .line 51792
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51794
    :cond_3
    iget-object v1, p0, Lhgo;->g:Lhds;

    if-eqz v1, :cond_4

    .line 51795
    const v1, 0x3070f41

    iget-object v2, p0, Lhgo;->g:Lhds;

    .line 51796
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51798
    :cond_4
    iget-object v1, p0, Lhgo;->h:Lhdz;

    if-eqz v1, :cond_5

    .line 51799
    const v1, 0x32b52b9

    iget-object v2, p0, Lhgo;->h:Lhdz;

    .line 51800
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51802
    :cond_5
    iget-object v1, p0, Lhgo;->i:Lhdt;

    if-eqz v1, :cond_6

    .line 51803
    const v1, 0x37c6a1c

    iget-object v2, p0, Lhgo;->i:Lhdt;

    .line 51804
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51806
    :cond_6
    iget-object v1, p0, Lhgo;->d:Lhbb;

    if-eqz v1, :cond_7

    .line 51807
    const v1, 0x3e1ae1d

    iget-object v2, p0, Lhgo;->d:Lhbb;

    .line 51808
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51810
    :cond_7
    iget-object v1, p0, Lhgo;->j:Lhax;

    if-eqz v1, :cond_8

    .line 51811
    const v1, 0x4a7aeb4

    iget-object v2, p0, Lhgo;->j:Lhax;

    .line 51812
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51814
    :cond_8
    iget-object v1, p0, Lhgo;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51815
    iput v0, p0, Lhgo;->J:I

    .line 51816
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 51661
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgo;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgo;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgo;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhgo;->b:Liab;

    if-nez v0, :cond_2

    new-instance v0, Liab;

    invoke-direct {v0}, Liab;-><init>()V

    iput-object v0, p0, Lhgo;->b:Liab;

    :cond_2
    iget-object v0, p0, Lhgo;->b:Liab;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhgo;->e:Lheb;

    if-nez v0, :cond_3

    new-instance v0, Lheb;

    invoke-direct {v0}, Lheb;-><init>()V

    iput-object v0, p0, Lhgo;->e:Lheb;

    :cond_3
    iget-object v0, p0, Lhgo;->e:Lheb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhgo;->c:Lhsi;

    if-nez v0, :cond_4

    new-instance v0, Lhsi;

    invoke-direct {v0}, Lhsi;-><init>()V

    iput-object v0, p0, Lhgo;->c:Lhsi;

    :cond_4
    iget-object v0, p0, Lhgo;->c:Lhsi;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhgo;->f:Lhdv;

    if-nez v0, :cond_5

    new-instance v0, Lhdv;

    invoke-direct {v0}, Lhdv;-><init>()V

    iput-object v0, p0, Lhgo;->f:Lhdv;

    :cond_5
    iget-object v0, p0, Lhgo;->f:Lhdv;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhgo;->g:Lhds;

    if-nez v0, :cond_6

    new-instance v0, Lhds;

    invoke-direct {v0}, Lhds;-><init>()V

    iput-object v0, p0, Lhgo;->g:Lhds;

    :cond_6
    iget-object v0, p0, Lhgo;->g:Lhds;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhgo;->h:Lhdz;

    if-nez v0, :cond_7

    new-instance v0, Lhdz;

    invoke-direct {v0}, Lhdz;-><init>()V

    iput-object v0, p0, Lhgo;->h:Lhdz;

    :cond_7
    iget-object v0, p0, Lhgo;->h:Lhdz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhgo;->i:Lhdt;

    if-nez v0, :cond_8

    new-instance v0, Lhdt;

    invoke-direct {v0}, Lhdt;-><init>()V

    iput-object v0, p0, Lhgo;->i:Lhdt;

    :cond_8
    iget-object v0, p0, Lhgo;->i:Lhdt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhgo;->d:Lhbb;

    if-nez v0, :cond_9

    new-instance v0, Lhbb;

    invoke-direct {v0}, Lhbb;-><init>()V

    iput-object v0, p0, Lhgo;->d:Lhbb;

    :cond_9
    iget-object v0, p0, Lhgo;->d:Lhbb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhgo;->j:Lhax;

    if-nez v0, :cond_a

    new-instance v0, Lhax;

    invoke-direct {v0}, Lhax;-><init>()V

    iput-object v0, p0, Lhgo;->j:Lhax;

    :cond_a
    iget-object v0, p0, Lhgo;->j:Lhax;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x181e0eb2 -> :sswitch_1
        0x18248a1a -> :sswitch_2
        0x1830e7a2 -> :sswitch_3
        0x18322b3a -> :sswitch_4
        0x18387a0a -> :sswitch_5
        0x195a95ca -> :sswitch_6
        0x1be350e2 -> :sswitch_7
        0x1f0d70ea -> :sswitch_8
        0x253d75a2 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 51744
    iget-object v0, p0, Lhgo;->b:Liab;

    if-eqz v0, :cond_0

    .line 51745
    const v0, 0x303c1d6

    iget-object v1, p0, Lhgo;->b:Liab;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51747
    :cond_0
    iget-object v0, p0, Lhgo;->e:Lheb;

    if-eqz v0, :cond_1

    .line 51748
    const v0, 0x3049143

    iget-object v1, p0, Lhgo;->e:Lheb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51750
    :cond_1
    iget-object v0, p0, Lhgo;->c:Lhsi;

    if-eqz v0, :cond_2

    .line 51751
    const v0, 0x3061cf4

    iget-object v1, p0, Lhgo;->c:Lhsi;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51753
    :cond_2
    iget-object v0, p0, Lhgo;->f:Lhdv;

    if-eqz v0, :cond_3

    .line 51754
    const v0, 0x3064567

    iget-object v1, p0, Lhgo;->f:Lhdv;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51756
    :cond_3
    iget-object v0, p0, Lhgo;->g:Lhds;

    if-eqz v0, :cond_4

    .line 51757
    const v0, 0x3070f41

    iget-object v1, p0, Lhgo;->g:Lhds;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51759
    :cond_4
    iget-object v0, p0, Lhgo;->h:Lhdz;

    if-eqz v0, :cond_5

    .line 51760
    const v0, 0x32b52b9

    iget-object v1, p0, Lhgo;->h:Lhdz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51762
    :cond_5
    iget-object v0, p0, Lhgo;->i:Lhdt;

    if-eqz v0, :cond_6

    .line 51763
    const v0, 0x37c6a1c

    iget-object v1, p0, Lhgo;->i:Lhdt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51765
    :cond_6
    iget-object v0, p0, Lhgo;->d:Lhbb;

    if-eqz v0, :cond_7

    .line 51766
    const v0, 0x3e1ae1d

    iget-object v1, p0, Lhgo;->d:Lhbb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51768
    :cond_7
    iget-object v0, p0, Lhgo;->j:Lhax;

    if-eqz v0, :cond_8

    .line 51769
    const v0, 0x4a7aeb4

    iget-object v1, p0, Lhgo;->j:Lhax;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 51771
    :cond_8
    iget-object v0, p0, Lhgo;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 51773
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51711
    if-ne p1, p0, :cond_1

    .line 51723
    :cond_0
    :goto_0
    return v0

    .line 51712
    :cond_1
    instance-of v2, p1, Lhgo;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 51713
    :cond_2
    check-cast p1, Lhgo;

    .line 51714
    iget-object v2, p0, Lhgo;->b:Liab;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgo;->b:Liab;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhgo;->e:Lheb;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgo;->e:Lheb;

    if-nez v2, :cond_3

    .line 51715
    :goto_2
    iget-object v2, p0, Lhgo;->c:Lhsi;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhgo;->c:Lhsi;

    if-nez v2, :cond_3

    .line 51716
    :goto_3
    iget-object v2, p0, Lhgo;->f:Lhdv;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhgo;->f:Lhdv;

    if-nez v2, :cond_3

    .line 51717
    :goto_4
    iget-object v2, p0, Lhgo;->g:Lhds;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhgo;->g:Lhds;

    if-nez v2, :cond_3

    .line 51718
    :goto_5
    iget-object v2, p0, Lhgo;->h:Lhdz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhgo;->h:Lhdz;

    if-nez v2, :cond_3

    .line 51719
    :goto_6
    iget-object v2, p0, Lhgo;->i:Lhdt;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhgo;->i:Lhdt;

    if-nez v2, :cond_3

    .line 51720
    :goto_7
    iget-object v2, p0, Lhgo;->d:Lhbb;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhgo;->d:Lhbb;

    if-nez v2, :cond_3

    .line 51721
    :goto_8
    iget-object v2, p0, Lhgo;->j:Lhax;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhgo;->j:Lhax;

    if-nez v2, :cond_3

    .line 51722
    :goto_9
    iget-object v2, p0, Lhgo;->I:Ljava/util/List;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhgo;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 51723
    goto :goto_0

    .line 51714
    :cond_4
    iget-object v2, p0, Lhgo;->b:Liab;

    iget-object v3, p1, Lhgo;->b:Liab;

    invoke-virtual {v2, v3}, Liab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgo;->e:Lheb;

    iget-object v3, p1, Lhgo;->e:Lheb;

    .line 51715
    invoke-virtual {v2, v3}, Lheb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhgo;->c:Lhsi;

    iget-object v3, p1, Lhgo;->c:Lhsi;

    .line 51716
    invoke-virtual {v2, v3}, Lhsi;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhgo;->f:Lhdv;

    iget-object v3, p1, Lhgo;->f:Lhdv;

    .line 51717
    invoke-virtual {v2, v3}, Lhdv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhgo;->g:Lhds;

    iget-object v3, p1, Lhgo;->g:Lhds;

    .line 51718
    invoke-virtual {v2, v3}, Lhds;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhgo;->h:Lhdz;

    iget-object v3, p1, Lhgo;->h:Lhdz;

    .line 51719
    invoke-virtual {v2, v3}, Lhdz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhgo;->i:Lhdt;

    iget-object v3, p1, Lhgo;->i:Lhdt;

    .line 51720
    invoke-virtual {v2, v3}, Lhdt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhgo;->d:Lhbb;

    iget-object v3, p1, Lhgo;->d:Lhbb;

    .line 51721
    invoke-virtual {v2, v3}, Lhbb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lhgo;->j:Lhax;

    iget-object v3, p1, Lhgo;->j:Lhax;

    .line 51722
    invoke-virtual {v2, v3}, Lhax;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_9

    :cond_d
    iget-object v2, p0, Lhgo;->I:Ljava/util/List;

    iget-object v3, p1, Lhgo;->I:Ljava/util/List;

    .line 51723
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 51727
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 51729
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->b:Liab;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 51730
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->e:Lheb;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 51731
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->c:Lhsi;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 51732
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->f:Lhdv;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 51733
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->g:Lhds;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 51734
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->h:Lhdz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 51735
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->i:Lhdt;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 51736
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->d:Lhbb;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 51737
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgo;->j:Lhax;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 51738
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhgo;->I:Ljava/util/List;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    .line 51739
    return v0

    .line 51729
    :cond_0
    iget-object v0, p0, Lhgo;->b:Liab;

    invoke-virtual {v0}, Liab;->hashCode()I

    move-result v0

    goto :goto_0

    .line 51730
    :cond_1
    iget-object v0, p0, Lhgo;->e:Lheb;

    invoke-virtual {v0}, Lheb;->hashCode()I

    move-result v0

    goto :goto_1

    .line 51731
    :cond_2
    iget-object v0, p0, Lhgo;->c:Lhsi;

    invoke-virtual {v0}, Lhsi;->hashCode()I

    move-result v0

    goto :goto_2

    .line 51732
    :cond_3
    iget-object v0, p0, Lhgo;->f:Lhdv;

    invoke-virtual {v0}, Lhdv;->hashCode()I

    move-result v0

    goto :goto_3

    .line 51733
    :cond_4
    iget-object v0, p0, Lhgo;->g:Lhds;

    invoke-virtual {v0}, Lhds;->hashCode()I

    move-result v0

    goto :goto_4

    .line 51734
    :cond_5
    iget-object v0, p0, Lhgo;->h:Lhdz;

    invoke-virtual {v0}, Lhdz;->hashCode()I

    move-result v0

    goto :goto_5

    .line 51735
    :cond_6
    iget-object v0, p0, Lhgo;->i:Lhdt;

    invoke-virtual {v0}, Lhdt;->hashCode()I

    move-result v0

    goto :goto_6

    .line 51736
    :cond_7
    iget-object v0, p0, Lhgo;->d:Lhbb;

    invoke-virtual {v0}, Lhbb;->hashCode()I

    move-result v0

    goto :goto_7

    .line 51737
    :cond_8
    iget-object v0, p0, Lhgo;->j:Lhax;

    invoke-virtual {v0}, Lhax;->hashCode()I

    move-result v0

    goto :goto_8

    .line 51738
    :cond_9
    iget-object v1, p0, Lhgo;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_9
.end method
