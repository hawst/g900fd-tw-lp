.class public Lzd;
.super Likv;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field private f:D

.field private g:F

.field private h:[J

.field private i:I

.field private j:I

.field private q:I

.field private r:I

.field private s:I

.field private t:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 56
    const-string v0, "mvhd"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 40
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lzd;->f:D

    .line 41
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lzd;->g:F

    .line 42
    const/16 v0, 0x9

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lzd;->h:[J

    .line 57
    return-void

    .line 42
    nop

    :array_0
    .array-data 8
        0x10000
        0x0
        0x0
        0x0
        0x10000
        0x0
        0x0
        0x0
        0x40000000
    .end array-data
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 5

    .prologue
    const/16 v4, 0x9

    .line 104
    invoke-virtual {p0, p1}, Lzd;->c(Ljava/nio/ByteBuffer;)J

    .line 105
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 106
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->a:J

    .line 107
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->b:J

    .line 108
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->c:J

    .line 109
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->d:J

    .line 116
    :goto_0
    invoke-static {p1}, La;->g(Ljava/nio/ByteBuffer;)D

    move-result-wide v0

    iput-wide v0, p0, Lzd;->f:D

    .line 117
    invoke-static {p1}, La;->h(Ljava/nio/ByteBuffer;)F

    move-result v0

    iput v0, p0, Lzd;->g:F

    .line 118
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    .line 119
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    .line 120
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    .line 121
    new-array v0, v4, [J

    iput-object v0, p0, Lzd;->h:[J

    .line 122
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_1

    .line 123
    iget-object v1, p0, Lzd;->h:[J

    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 111
    :cond_0
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->a:J

    .line 112
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->b:J

    .line 113
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->c:J

    .line 114
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->d:J

    goto :goto_0

    .line 126
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lzd;->i:I

    .line 127
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lzd;->j:I

    .line 128
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lzd;->q:I

    .line 129
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lzd;->r:I

    .line 130
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lzd;->s:I

    .line 131
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, p0, Lzd;->t:I

    .line 133
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzd;->e:J

    .line 135
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 164
    invoke-virtual {p0, p1}, Lzd;->d(Ljava/nio/ByteBuffer;)V

    .line 165
    iget v1, p0, Likv;->o:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 166
    iget-wide v2, p0, Lzd;->a:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 167
    iget-wide v2, p0, Lzd;->b:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 168
    iget-wide v2, p0, Lzd;->c:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 169
    iget-wide v2, p0, Lzd;->d:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 176
    :goto_0
    iget-wide v2, p0, Lzd;->f:D

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;D)V

    .line 177
    iget v1, p0, Lzd;->g:F

    float-to-double v2, v1

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;D)V

    .line 178
    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 179
    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 180
    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 183
    :goto_1
    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    .line 184
    iget-object v1, p0, Lzd;->h:[J

    aget-wide v2, v1, v0

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 171
    :cond_0
    iget-wide v2, p0, Lzd;->a:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 172
    iget-wide v2, p0, Lzd;->b:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 173
    iget-wide v2, p0, Lzd;->c:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 174
    iget-wide v2, p0, Lzd;->d:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    goto :goto_0

    .line 188
    :cond_1
    iget v0, p0, Lzd;->i:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 189
    iget v0, p0, Lzd;->j:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 190
    iget v0, p0, Lzd;->q:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 191
    iget v0, p0, Lzd;->r:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 192
    iget v0, p0, Lzd;->s:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 193
    iget v0, p0, Lzd;->t:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 195
    iget-wide v0, p0, Lzd;->e:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 196
    return-void
.end method

.method protected final d_()J
    .locals 4

    .prologue
    .line 92
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 94
    const-wide/16 v0, 0x20

    .line 98
    :goto_0
    const-wide/16 v2, 0x50

    add-long/2addr v0, v2

    .line 99
    return-wide v0

    .line 96
    :cond_0
    const-wide/16 v0, 0x14

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 138
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    const-string v0, "MovieHeaderBox["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string v0, "creationTime="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzd;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 141
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string v0, "modificationTime="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzd;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 143
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    const-string v0, "timescale="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzd;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 145
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string v0, "duration="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzd;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 147
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    const-string v0, "rate="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzd;->f:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 149
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    const-string v0, "volume="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lzd;->g:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 151
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lzd;->h:[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 152
    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    const-string v2, "matrix"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lzd;->h:[J

    aget-wide v4, v3, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_0
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const-string v0, "nextTrackId="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzd;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 157
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
