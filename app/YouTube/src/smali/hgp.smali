.class public final Lhgp;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field private b:[Lhnb;

.field private c:[Lhnb;

.field private d:[Lhut;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51912
    invoke-direct {p0}, Lidf;-><init>()V

    .line 51915
    const/4 v0, 0x0

    iput-object v0, p0, Lhgp;->a:Lhgz;

    .line 51918
    sget-object v0, Lhnb;->a:[Lhnb;

    iput-object v0, p0, Lhgp;->b:[Lhnb;

    .line 51921
    sget-object v0, Lhnb;->a:[Lhnb;

    iput-object v0, p0, Lhgp;->c:[Lhnb;

    .line 51924
    sget-object v0, Lhut;->a:[Lhut;

    iput-object v0, p0, Lhgp;->d:[Lhut;

    .line 51912
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 52006
    .line 52007
    iget-object v0, p0, Lhgp;->a:Lhgz;

    if-eqz v0, :cond_6

    .line 52008
    const/4 v0, 0x1

    iget-object v2, p0, Lhgp;->a:Lhgz;

    .line 52009
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 52011
    :goto_0
    iget-object v2, p0, Lhgp;->b:[Lhnb;

    if-eqz v2, :cond_1

    .line 52012
    iget-object v3, p0, Lhgp;->b:[Lhnb;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 52013
    if-eqz v5, :cond_0

    .line 52014
    const/4 v6, 0x2

    .line 52015
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 52012
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 52019
    :cond_1
    iget-object v2, p0, Lhgp;->c:[Lhnb;

    if-eqz v2, :cond_3

    .line 52020
    iget-object v3, p0, Lhgp;->c:[Lhnb;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 52021
    if-eqz v5, :cond_2

    .line 52022
    const/4 v6, 0x3

    .line 52023
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 52020
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 52027
    :cond_3
    iget-object v2, p0, Lhgp;->d:[Lhut;

    if-eqz v2, :cond_5

    .line 52028
    iget-object v2, p0, Lhgp;->d:[Lhut;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 52029
    if-eqz v4, :cond_4

    .line 52030
    const/4 v5, 0x4

    .line 52031
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 52028
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 52035
    :cond_5
    iget-object v1, p0, Lhgp;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52036
    iput v0, p0, Lhgp;->J:I

    .line 52037
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51908
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhgp;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhgp;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhgp;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhgp;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhgp;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhgp;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhgp;->b:[Lhnb;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhnb;

    iget-object v3, p0, Lhgp;->b:[Lhnb;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhgp;->b:[Lhnb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhgp;->b:[Lhnb;

    :goto_2
    iget-object v2, p0, Lhgp;->b:[Lhnb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhgp;->b:[Lhnb;

    new-instance v3, Lhnb;

    invoke-direct {v3}, Lhnb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgp;->b:[Lhnb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhgp;->b:[Lhnb;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhgp;->b:[Lhnb;

    new-instance v3, Lhnb;

    invoke-direct {v3}, Lhnb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgp;->b:[Lhnb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhgp;->c:[Lhnb;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhnb;

    iget-object v3, p0, Lhgp;->c:[Lhnb;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhgp;->c:[Lhnb;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhgp;->c:[Lhnb;

    :goto_4
    iget-object v2, p0, Lhgp;->c:[Lhnb;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhgp;->c:[Lhnb;

    new-instance v3, Lhnb;

    invoke-direct {v3}, Lhnb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgp;->c:[Lhnb;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhgp;->c:[Lhnb;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lhgp;->c:[Lhnb;

    new-instance v3, Lhnb;

    invoke-direct {v3}, Lhnb;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgp;->c:[Lhnb;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhgp;->d:[Lhut;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhut;

    iget-object v3, p0, Lhgp;->d:[Lhut;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lhgp;->d:[Lhut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    iput-object v2, p0, Lhgp;->d:[Lhut;

    :goto_6
    iget-object v2, p0, Lhgp;->d:[Lhut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Lhgp;->d:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgp;->d:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lhgp;->d:[Lhut;

    array-length v0, v0

    goto :goto_5

    :cond_b
    iget-object v2, p0, Lhgp;->d:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgp;->d:[Lhut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 51976
    iget-object v1, p0, Lhgp;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 51977
    const/4 v1, 0x1

    iget-object v2, p0, Lhgp;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 51979
    :cond_0
    iget-object v1, p0, Lhgp;->b:[Lhnb;

    if-eqz v1, :cond_2

    .line 51980
    iget-object v2, p0, Lhgp;->b:[Lhnb;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 51981
    if-eqz v4, :cond_1

    .line 51982
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 51980
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51986
    :cond_2
    iget-object v1, p0, Lhgp;->c:[Lhnb;

    if-eqz v1, :cond_4

    .line 51987
    iget-object v2, p0, Lhgp;->c:[Lhnb;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 51988
    if-eqz v4, :cond_3

    .line 51989
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 51987
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 51993
    :cond_4
    iget-object v1, p0, Lhgp;->d:[Lhut;

    if-eqz v1, :cond_6

    .line 51994
    iget-object v1, p0, Lhgp;->d:[Lhut;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 51995
    if-eqz v3, :cond_5

    .line 51996
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 51994
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 52000
    :cond_6
    iget-object v0, p0, Lhgp;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 52002
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51938
    if-ne p1, p0, :cond_1

    .line 51945
    :cond_0
    :goto_0
    return v0

    .line 51939
    :cond_1
    instance-of v2, p1, Lhgp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 51940
    :cond_2
    check-cast p1, Lhgp;

    .line 51941
    iget-object v2, p0, Lhgp;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgp;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhgp;->b:[Lhnb;

    iget-object v3, p1, Lhgp;->b:[Lhnb;

    .line 51942
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgp;->c:[Lhnb;

    iget-object v3, p1, Lhgp;->c:[Lhnb;

    .line 51943
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgp;->d:[Lhut;

    iget-object v3, p1, Lhgp;->d:[Lhut;

    .line 51944
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgp;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhgp;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 51945
    goto :goto_0

    .line 51941
    :cond_4
    iget-object v2, p0, Lhgp;->a:Lhgz;

    iget-object v3, p1, Lhgp;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 51944
    :cond_5
    iget-object v2, p0, Lhgp;->I:Ljava/util/List;

    iget-object v3, p1, Lhgp;->I:Ljava/util/List;

    .line 51945
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51949
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 51951
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhgp;->a:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 51952
    iget-object v2, p0, Lhgp;->b:[Lhnb;

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    .line 51958
    :cond_0
    iget-object v0, p0, Lhgp;->c:[Lhnb;

    if-nez v0, :cond_6

    mul-int/lit8 v2, v2, 0x1f

    .line 51964
    :cond_1
    iget-object v0, p0, Lhgp;->d:[Lhut;

    if-nez v0, :cond_8

    mul-int/lit8 v2, v2, 0x1f

    .line 51970
    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhgp;->I:Ljava/util/List;

    if-nez v2, :cond_a

    :goto_1
    add-int/2addr v0, v1

    .line 51971
    return v0

    .line 51951
    :cond_3
    iget-object v0, p0, Lhgp;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_4
    move v2, v0

    move v0, v1

    .line 51954
    :goto_2
    iget-object v3, p0, Lhgp;->b:[Lhnb;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 51955
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgp;->b:[Lhnb;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_3
    add-int/2addr v2, v3

    .line 51954
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 51955
    :cond_5
    iget-object v2, p0, Lhgp;->b:[Lhnb;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhnb;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_6
    move v0, v1

    .line 51960
    :goto_4
    iget-object v3, p0, Lhgp;->c:[Lhnb;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 51961
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgp;->c:[Lhnb;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 51960
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 51961
    :cond_7
    iget-object v2, p0, Lhgp;->c:[Lhnb;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhnb;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_8
    move v0, v1

    .line 51966
    :goto_6
    iget-object v3, p0, Lhgp;->d:[Lhut;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 51967
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgp;->d:[Lhut;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_7
    add-int/2addr v2, v3

    .line 51966
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 51967
    :cond_9
    iget-object v2, p0, Lhgp;->d:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhut;->hashCode()I

    move-result v2

    goto :goto_7

    .line 51970
    :cond_a
    iget-object v1, p0, Lhgp;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
