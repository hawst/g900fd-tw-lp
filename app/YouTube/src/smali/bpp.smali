.class public final Lbpp;
.super Lbpw;
.source "SourceFile"


# static fields
.field private static final a:Lbpp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1312
    new-instance v0, Lbpp;

    invoke-direct {v0}, Lbpp;-><init>()V

    sput-object v0, Lbpp;->a:Lbpp;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1309
    invoke-direct {p0}, Lbpw;-><init>()V

    return-void
.end method

.method public static synthetic a()Lbpp;
    .locals 1

    .prologue
    .line 1309
    sget-object v0, Lbpp;->a:Lbpp;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Lfkq;
    .locals 3

    .prologue
    .line 1309
    check-cast p1, Lfjc;

    iget-object v0, p1, Lfjc;->h:Lfkp;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->k:Lhnh;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->k:Lhnh;

    iget-object v0, v0, Lhnh;->a:Lhne;

    if-eqz v0, :cond_0

    new-instance v0, Lfkp;

    iget-object v1, p1, Lfjc;->a:Lhdv;

    iget-object v1, v1, Lhdv;->k:Lhnh;

    iget-object v1, v1, Lhnh;->a:Lhne;

    invoke-direct {v0, v1, p1}, Lfkp;-><init>(Lhne;Lfqh;)V

    iput-object v0, p1, Lfjc;->h:Lfkp;

    :cond_0
    iget-object v0, p1, Lfjc;->h:Lfkp;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lfkp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkq;

    iget-object v2, v0, Lfkq;->a:Lhut;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lhut;->g:Lhgq;

    if-eqz v2, :cond_1

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)Lfkn;
    .locals 2

    .prologue
    .line 1309
    check-cast p1, Lfjc;

    iget-object v0, p1, Lfjc;->f:Lfkn;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->h:Lhll;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->h:Lhll;

    iget-object v0, v0, Lhll;->a:Lhlk;

    if-eqz v0, :cond_0

    new-instance v0, Lfkn;

    iget-object v1, p1, Lfjc;->a:Lhdv;

    iget-object v1, v1, Lhdv;->h:Lhll;

    iget-object v1, v1, Lhll;->a:Lhlk;

    invoke-direct {v0, v1}, Lfkn;-><init>(Lhlk;)V

    iput-object v0, p1, Lfjc;->f:Lfkn;

    :cond_0
    iget-object v0, p1, Lfjc;->f:Lfkn;

    return-object v0
.end method

.method public final synthetic c(Ljava/lang/Object;)Lflh;
    .locals 2

    .prologue
    .line 1309
    check-cast p1, Lfjc;

    iget-object v0, p1, Lfjc;->e:Lflh;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->j:Lhdw;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->j:Lhdw;

    iget-object v0, v0, Lhdw;->a:Lhpn;

    if-eqz v0, :cond_0

    new-instance v0, Lflh;

    iget-object v1, p1, Lfjc;->a:Lhdv;

    iget-object v1, v1, Lhdv;->j:Lhdw;

    iget-object v1, v1, Lhdw;->a:Lhpn;

    invoke-direct {v0, v1}, Lflh;-><init>(Lhpn;)V

    iput-object v0, p1, Lfjc;->e:Lflh;

    :cond_0
    iget-object v0, p1, Lfjc;->e:Lflh;

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1309
    check-cast p1, Lfjc;

    iget-object v0, p1, Lfjc;->g:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->l:Ljava/lang/String;

    invoke-static {v0}, La;->K(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p1, Lfjc;->g:Landroid/net/Uri;

    :cond_0
    iget-object v0, p1, Lfjc;->g:Landroid/net/Uri;

    return-object v0
.end method

.method public final synthetic e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1309
    check-cast p1, Lfjc;

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic f(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1309
    check-cast p1, Lfjc;

    invoke-virtual {p1}, Lfjc;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1309
    check-cast p1, Lfjc;

    iget-object v0, p1, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->a:Ljava/lang/String;

    return-object v0
.end method
