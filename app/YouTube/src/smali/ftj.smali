.class public final Lftj;
.super Lfsp;
.source "SourceFile"


# instance fields
.field public A:Z

.field public B:I

.field public C:I

.field public D:I

.field public E:Ljava/lang/String;

.field public F:I

.field public G:I

.field public H:I

.field public I:[Ljava/lang/String;

.field private final J:Lfac;

.field private K:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public j:I

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Ljava/lang/String;

.field public p:J

.field public q:I

.field public r:I

.field public s:I

.field public t:Z

.field public u:Ljava/lang/String;

.field public v:Z

.field public w:I

.field public x:I

.field public y:I

.field public z:Z


# direct methods
.method public constructor <init>(Lfsz;Lgit;Lfac;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 85
    iget-object v0, p2, Lgit;->b:Lgiv;

    sget-object v1, Lfsq;->c:Lfsq;

    invoke-direct {p0, p1, v0, v1}, Lfsp;-><init>(Lfsz;Lgiv;Lfsq;)V

    .line 41
    iput v3, p0, Lftj;->j:I

    .line 42
    iput-boolean v2, p0, Lftj;->k:Z

    .line 43
    iput-boolean v2, p0, Lftj;->l:Z

    .line 44
    iput-boolean v2, p0, Lftj;->m:Z

    .line 45
    iput-boolean v2, p0, Lftj;->n:Z

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lftj;->o:Ljava/lang/String;

    .line 49
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lftj;->p:J

    .line 50
    iput v3, p0, Lftj;->q:I

    .line 51
    iput v3, p0, Lftj;->r:I

    .line 52
    iput v3, p0, Lftj;->s:I

    .line 53
    iput-boolean v2, p0, Lftj;->t:Z

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lftj;->u:Ljava/lang/String;

    .line 55
    iput-boolean v2, p0, Lftj;->v:Z

    .line 56
    iput v3, p0, Lftj;->w:I

    .line 57
    iput v3, p0, Lftj;->x:I

    .line 58
    iput v3, p0, Lftj;->y:I

    .line 59
    iput-boolean v2, p0, Lftj;->z:Z

    .line 60
    iput-boolean v2, p0, Lftj;->A:Z

    .line 63
    iput v2, p0, Lftj;->B:I

    .line 64
    iput v2, p0, Lftj;->C:I

    .line 65
    iput v2, p0, Lftj;->D:I

    .line 66
    const-string v0, ""

    iput-object v0, p0, Lftj;->E:Ljava/lang/String;

    .line 69
    iput v3, p0, Lftj;->F:I

    .line 71
    iput v3, p0, Lftj;->G:I

    .line 73
    iput v3, p0, Lftj;->H:I

    .line 86
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfac;

    iput-object v0, p0, Lftj;->J:Lfac;

    .line 87
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 282
    const-string v0, "player"

    return-object v0
.end method

.method protected final b()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 258
    iget-object v0, p0, Lftj;->a:Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    iget-object v0, p0, Lftj;->d:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 261
    iget v0, p0, Lftj;->G:I

    if-eq v0, v1, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    iget-boolean v0, p0, Lftj;->n:Z

    if-nez v0, :cond_3

    .line 268
    iget v0, p0, Lftj;->r:I

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 271
    iget v0, p0, Lftj;->w:I

    if-ne v0, v1, :cond_0

    .line 272
    iget-boolean v0, p0, Lftj;->m:Z

    invoke-static {v0}, Lb;->c(Z)V

    goto :goto_0

    .line 268
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 274
    :cond_3
    iget-boolean v0, p0, Lftj;->m:Z

    if-nez v0, :cond_0

    .line 276
    iget-object v0, p0, Lftj;->E:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method

.method public final synthetic c()Lidh;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lftj;->m()Lhrn;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 422
    invoke-virtual {p0}, Lftj;->j()Lggq;

    move-result-object v0

    .line 423
    const-string v1, "isAdRequest"

    iget-boolean v2, p0, Lftj;->n:Z

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Z)Lggq;

    .line 424
    const-string v1, "videoId"

    iget-object v2, p0, Lftj;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 425
    const-string v1, "playlistId"

    iget-object v2, p0, Lftj;->c:Ljava/lang/String;

    invoke-static {v2}, Lftj;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 426
    const-string v1, "playlistIndex"

    iget v2, p0, Lftj;->j:I

    invoke-static {v2}, Lftj;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;I)Lggq;

    .line 427
    const-string v1, "allowControversialContent"

    iget-boolean v2, p0, Lftj;->k:Z

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Z)Lggq;

    .line 428
    const-string v1, "allowAdultContent"

    iget-boolean v2, p0, Lftj;->l:Z

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Z)Lggq;

    .line 429
    const-string v1, "isOfflineRequest"

    iget-boolean v2, p0, Lftj;->m:Z

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Z)Lggq;

    .line 430
    const-string v1, "dataExpiredForSeconds"

    iget v2, p0, Lftj;->F:I

    invoke-static {v2}, Lftj;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;I)Lggq;

    .line 431
    const-string v1, "onBehalfOfClientName"

    iget v2, p0, Lftj;->G:I

    invoke-static {v2}, Lftj;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;I)Lggq;

    .line 432
    const-string v1, "autoplay"

    iget-boolean v2, p0, Lftj;->t:Z

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Z)Lggq;

    .line 433
    const-string v1, "scriptedPlay"

    iget-boolean v2, p0, Lftj;->v:Z

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Z)Lggq;

    .line 434
    const-string v1, "adSystem"

    iget v2, p0, Lftj;->B:I

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;I)Lggq;

    .line 435
    const-string v1, "adType"

    iget v2, p0, Lftj;->C:I

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;I)Lggq;

    .line 436
    const-string v1, "instreamType"

    iget v2, p0, Lftj;->D:I

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;I)Lggq;

    .line 437
    const-string v1, "hostVideoId"

    iget-object v2, p0, Lftj;->E:Ljava/lang/String;

    invoke-static {v2}, Lftj;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 438
    const-string v1, "playerParams"

    iget-object v2, p0, Lftj;->b:Ljava/lang/String;

    invoke-static {v2}, Lftj;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lggq;->a(Ljava/lang/String;Ljava/lang/String;)Lggq;

    .line 439
    invoke-virtual {v0}, Lggq;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/Map;
    .locals 3

    .prologue
    .line 287
    iget-object v0, p0, Lftj;->e:Ljava/util/Map;

    if-nez v0, :cond_1

    .line 288
    invoke-super {p0}, Lfsp;->g()Ljava/util/Map;

    .line 293
    iget-object v0, p0, Lftj;->K:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 295
    iget-object v0, p0, Lftj;->J:Lfac;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lfac;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lftj;->K:Ljava/lang/String;

    .line 298
    :cond_0
    iget-object v0, p0, Lftj;->e:Ljava/util/Map;

    const-string v1, "id"

    iget-object v2, p0, Lftj;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    iget-object v0, p0, Lftj;->e:Ljava/util/Map;

    const-string v1, "t"

    iget-object v2, p0, Lftj;->K:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    :cond_1
    iget-object v0, p0, Lftj;->e:Ljava/util/Map;

    return-object v0
.end method

.method public final m()Lhrn;
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, -0x1

    .line 307
    new-instance v2, Lhrn;

    invoke-direct {v2}, Lhrn;-><init>()V

    .line 308
    iget-boolean v1, p0, Lftj;->l:Z

    iput-boolean v1, v2, Lhrn;->e:Z

    .line 309
    iget-boolean v1, p0, Lftj;->k:Z

    iput-boolean v1, v2, Lhrn;->c:Z

    .line 310
    iget-object v1, p0, Lftj;->a:Ljava/lang/String;

    iput-object v1, v2, Lhrn;->b:Ljava/lang/String;

    .line 311
    iget-boolean v1, p0, Lftj;->m:Z

    iput-boolean v1, v2, Lhrn;->f:Z

    .line 312
    new-instance v1, Lhqo;

    invoke-direct {v1}, Lhqo;-><init>()V

    iput-object v1, v2, Lhrn;->d:Lhqo;

    .line 313
    invoke-virtual {p0}, Lftj;->i()Lhjx;

    move-result-object v1

    iput-object v1, v2, Lhrn;->a:Lhjx;

    .line 318
    iget-object v1, v2, Lhrn;->a:Lhjx;

    iget-object v1, v1, Lhjx;->h:Lgys;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 319
    iget-object v1, v2, Lhrn;->a:Lhjx;

    iget-object v1, v1, Lhjx;->h:Lgys;

    iget-object v1, v1, Lgys;->a:[Lhld;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 320
    iget-object v1, v2, Lhrn;->a:Lhjx;

    iget-object v1, v1, Lhjx;->h:Lgys;

    iget-object v3, v1, Lgys;->a:[Lhld;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 321
    iget-object v6, v5, Lhld;->b:Ljava/lang/String;

    const-string v7, "ms"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v5, v5, Lhld;->c:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 322
    const/4 v0, 0x1

    .line 320
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 326
    :cond_1
    invoke-static {v0}, Lb;->c(Z)V

    .line 328
    iget-object v0, v2, Lhrn;->a:Lhjx;

    iget-object v0, v0, Lhjx;->a:Lhcv;

    if-nez v0, :cond_2

    .line 329
    iget-object v0, v2, Lhrn;->a:Lhjx;

    new-instance v1, Lhcv;

    invoke-direct {v1}, Lhcv;-><init>()V

    iput-object v1, v0, Lhjx;->a:Lhcv;

    .line 331
    :cond_2
    iget-object v0, v2, Lhrn;->a:Lhjx;

    iget-object v0, v0, Lhjx;->a:Lhcv;

    iget-object v1, p0, Lftj;->d:Ljava/lang/String;

    iput-object v1, v0, Lhcv;->m:Ljava/lang/String;

    .line 333
    iget-object v0, p0, Lftj;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 334
    iget-object v0, p0, Lftj;->b:Ljava/lang/String;

    iput-object v0, v2, Lhrn;->i:Ljava/lang/String;

    .line 339
    :cond_3
    iget-object v0, p0, Lftj;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 340
    iget-object v0, p0, Lftj;->c:Ljava/lang/String;

    iput-object v0, v2, Lhrn;->g:Ljava/lang/String;

    .line 341
    iget v0, p0, Lftj;->j:I

    if-ltz v0, :cond_4

    .line 342
    iget v0, p0, Lftj;->j:I

    iput v0, v2, Lhrn;->h:I

    .line 348
    :cond_4
    iget-boolean v0, p0, Lftj;->n:Z

    if-nez v0, :cond_12

    .line 349
    new-instance v1, Lhej;

    invoke-direct {v1}, Lhej;-><init>()V

    .line 350
    iget-object v0, p0, Lftj;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 351
    iget-object v0, p0, Lftj;->o:Ljava/lang/String;

    iput-object v0, v1, Lhej;->a:Ljava/lang/String;

    .line 353
    :cond_5
    iget-wide v4, p0, Lftj;->p:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_6

    .line 354
    iget-wide v4, p0, Lftj;->p:J

    iput-wide v4, v1, Lhej;->c:J

    .line 356
    :cond_6
    iget v0, p0, Lftj;->q:I

    if-eq v0, v8, :cond_7

    .line 357
    iget v0, p0, Lftj;->q:I

    iput v0, v1, Lhej;->b:I

    .line 359
    :cond_7
    iget v0, p0, Lftj;->s:I

    if-eq v0, v8, :cond_8

    .line 360
    iget v0, p0, Lftj;->s:I

    iput v0, v1, Lhej;->d:I

    .line 362
    :cond_8
    iget v0, p0, Lftj;->w:I

    if-eq v0, v8, :cond_9

    .line 363
    iget v0, p0, Lftj;->w:I

    iput v0, v1, Lhej;->f:I

    .line 365
    :cond_9
    iget-boolean v0, p0, Lftj;->t:Z

    iput-boolean v0, v1, Lhej;->h:Z

    .line 366
    iget-object v0, p0, Lftj;->u:Ljava/lang/String;

    iput-object v0, v1, Lhej;->i:Ljava/lang/String;

    .line 367
    iget-boolean v0, p0, Lftj;->v:Z

    iput-boolean v0, v1, Lhej;->g:Z

    .line 368
    iget v0, p0, Lftj;->r:I

    iput v0, v1, Lhej;->e:I

    .line 370
    iget v0, p0, Lftj;->x:I

    if-ne v0, v8, :cond_a

    iget v0, p0, Lftj;->y:I

    if-eq v0, v8, :cond_d

    .line 371
    :cond_a
    new-instance v0, Lgzq;

    invoke-direct {v0}, Lgzq;-><init>()V

    .line 372
    iget v3, p0, Lftj;->x:I

    if-eq v3, v8, :cond_b

    .line 373
    iget v3, p0, Lftj;->x:I

    iput v3, v0, Lgzq;->a:I

    .line 375
    :cond_b
    iget v3, p0, Lftj;->y:I

    if-eq v3, v8, :cond_c

    .line 376
    iget v3, p0, Lftj;->y:I

    iput v3, v0, Lgzq;->b:I

    .line 378
    :cond_c
    iget-boolean v3, p0, Lftj;->z:Z

    iput-boolean v3, v0, Lgzq;->c:Z

    .line 379
    iget-boolean v3, p0, Lftj;->A:Z

    iput-boolean v3, v0, Lgzq;->d:Z

    .line 381
    iput-object v0, v1, Lhej;->j:Lgzq;

    .line 384
    :cond_d
    iget-object v0, p0, Lftj;->I:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lftj;->I:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    .line 385
    new-instance v0, Lhgx;

    invoke-direct {v0}, Lhgx;-><init>()V

    iput-object v0, v1, Lhej;->k:Lhgx;

    .line 386
    iget-object v3, v1, Lhej;->k:Lhgx;

    iget-object v0, p0, Lftj;->I:[Ljava/lang/String;

    iget-object v4, p0, Lftj;->I:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v0, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Lhgx;->a:[Ljava/lang/String;

    .line 389
    :cond_e
    iget-object v0, v2, Lhrn;->d:Lhqo;

    iput-object v1, v0, Lhqo;->a:Lhej;

    .line 399
    :goto_1
    iget v0, p0, Lftj;->F:I

    if-eq v0, v8, :cond_f

    .line 400
    new-instance v0, Lhty;

    invoke-direct {v0}, Lhty;-><init>()V

    .line 401
    iget v1, p0, Lftj;->F:I

    iput v1, v0, Lhty;->a:I

    .line 402
    iget-object v1, v2, Lhrn;->d:Lhqo;

    iput-object v0, v1, Lhqo;->d:Lhty;

    .line 405
    :cond_f
    iget v0, p0, Lftj;->G:I

    if-eq v0, v8, :cond_10

    .line 406
    new-instance v0, Lhcv;

    invoke-direct {v0}, Lhcv;-><init>()V

    .line 407
    iget v1, p0, Lftj;->G:I

    iput v1, v0, Lhcv;->g:I

    .line 408
    iget-object v1, v2, Lhrn;->a:Lhjx;

    iput-object v0, v1, Lhjx;->g:Lhcv;

    .line 411
    :cond_10
    iget v0, p0, Lftj;->H:I

    if-eq v0, v8, :cond_11

    .line 412
    iget-object v0, v2, Lhrn;->d:Lhqo;

    new-instance v1, Lhsu;

    invoke-direct {v1}, Lhsu;-><init>()V

    iput-object v1, v0, Lhqo;->c:Lhsu;

    .line 413
    iget-object v0, v2, Lhrn;->d:Lhqo;

    iget-object v0, v0, Lhqo;->c:Lhsu;

    iget v1, p0, Lftj;->H:I

    iput v1, v0, Lhsu;->a:I

    .line 417
    :cond_11
    return-object v2

    .line 391
    :cond_12
    new-instance v0, Lgyp;

    invoke-direct {v0}, Lgyp;-><init>()V

    .line 392
    iget v1, p0, Lftj;->B:I

    iput v1, v0, Lgyp;->a:I

    .line 393
    iget v1, p0, Lftj;->C:I

    iput v1, v0, Lgyp;->b:I

    .line 394
    iget v1, p0, Lftj;->D:I

    iput v1, v0, Lgyp;->c:I

    .line 395
    iget-object v1, p0, Lftj;->E:Ljava/lang/String;

    iput-object v1, v0, Lgyp;->d:Ljava/lang/String;

    .line 396
    iget-object v1, v2, Lhrn;->d:Lhqo;

    iput-object v0, v1, Lhqo;->b:Lgyp;

    goto :goto_1
.end method
