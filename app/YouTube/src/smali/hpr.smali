.class public final Lhpr;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhpr;


# instance fields
.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:[Lhpt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhpr;

    sput-object v0, Lhpr;->a:[Lhpr;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lhpr;->b:Ljava/lang/String;

    iput v1, p0, Lhpr;->c:I

    iput v1, p0, Lhpr;->d:I

    sget-object v0, Lhpt;->a:[Lhpt;

    iput-object v0, p0, Lhpr;->e:[Lhpt;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhpr;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iget-object v2, p0, Lhpr;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget v2, p0, Lhpr;->c:I

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget v3, p0, Lhpr;->c:I

    invoke-static {v2, v3}, Lidd;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Lhpr;->d:I

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Lhpr;->d:I

    invoke-static {v2, v3}, Lidd;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhpr;->e:[Lhpt;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhpr;->e:[Lhpt;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    const/4 v5, 0x4

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lhpr;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhpr;->J:I

    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhpr;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhpr;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhpr;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhpr;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhpr;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lhpr;->d:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhpr;->e:[Lhpt;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhpt;

    iget-object v3, p0, Lhpr;->e:[Lhpt;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhpr;->e:[Lhpt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhpr;->e:[Lhpt;

    :goto_2
    iget-object v2, p0, Lhpr;->e:[Lhpt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhpr;->e:[Lhpt;

    new-instance v3, Lhpt;

    invoke-direct {v3}, Lhpt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhpr;->e:[Lhpt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhpr;->e:[Lhpt;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhpr;->e:[Lhpt;

    new-instance v3, Lhpt;

    invoke-direct {v3}, Lhpt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhpr;->e:[Lhpt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhpr;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhpr;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget v0, p0, Lhpr;->c:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Lhpr;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    :cond_1
    iget v0, p0, Lhpr;->d:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lhpr;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    :cond_2
    iget-object v0, p0, Lhpr;->e:[Lhpt;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lhpr;->e:[Lhpt;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    if-eqz v3, :cond_3

    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lhpr;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhpr;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhpr;

    iget-object v2, p0, Lhpr;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhpr;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget v2, p0, Lhpr;->c:I

    iget v3, p1, Lhpr;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhpr;->d:I

    iget v3, p1, Lhpr;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhpr;->e:[Lhpt;

    iget-object v3, p1, Lhpr;->e:[Lhpt;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhpr;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhpr;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhpr;->b:Ljava/lang/String;

    iget-object v3, p1, Lhpr;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhpr;->I:Ljava/util/List;

    iget-object v3, p1, Lhpr;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhpr;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhpr;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhpr;->d:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lhpr;->e:[Lhpt;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhpr;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_1
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhpr;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Lhpr;->e:[Lhpt;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhpr;->e:[Lhpt;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_3
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lhpr;->e:[Lhpt;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhpt;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lhpr;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
