.class public final Lhgx;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field private b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53412
    invoke-direct {p0}, Lidf;-><init>()V

    .line 53415
    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lhgx;->a:[Ljava/lang/String;

    .line 53418
    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lhgx;->b:[Ljava/lang/String;

    .line 53412
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 53475
    .line 53476
    iget-object v0, p0, Lhgx;->a:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhgx;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 53478
    iget-object v3, p0, Lhgx;->a:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 53480
    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 53478
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53482
    :cond_0
    add-int/lit8 v0, v2, 0x0

    .line 53483
    iget-object v2, p0, Lhgx;->a:[Ljava/lang/String;

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 53485
    :goto_1
    iget-object v2, p0, Lhgx;->b:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhgx;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 53487
    iget-object v3, p0, Lhgx;->b:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 53489
    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 53487
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 53491
    :cond_1
    add-int/2addr v0, v2

    .line 53492
    iget-object v1, p0, Lhgx;->b:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 53494
    :cond_2
    iget-object v1, p0, Lhgx;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53495
    iput v0, p0, Lhgx;->J:I

    .line 53496
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 53408
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhgx;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhgx;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhgx;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhgx;->a:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lhgx;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhgx;->a:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lhgx;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lhgx;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lhgx;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhgx;->b:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lhgx;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhgx;->b:[Ljava/lang/String;

    :goto_2
    iget-object v1, p0, Lhgx;->b:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lhgx;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lhgx;->b:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 53459
    iget-object v1, p0, Lhgx;->a:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 53460
    iget-object v2, p0, Lhgx;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 53461
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILjava/lang/String;)V

    .line 53460
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53464
    :cond_0
    iget-object v1, p0, Lhgx;->b:[Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 53465
    iget-object v1, p0, Lhgx;->b:[Ljava/lang/String;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 53466
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    .line 53465
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53469
    :cond_1
    iget-object v0, p0, Lhgx;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 53471
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53430
    if-ne p1, p0, :cond_1

    .line 53435
    :cond_0
    :goto_0
    return v0

    .line 53431
    :cond_1
    instance-of v2, p1, Lhgx;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 53432
    :cond_2
    check-cast p1, Lhgx;

    .line 53433
    iget-object v2, p0, Lhgx;->a:[Ljava/lang/String;

    iget-object v3, p1, Lhgx;->a:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgx;->b:[Ljava/lang/String;

    iget-object v3, p1, Lhgx;->b:[Ljava/lang/String;

    .line 53434
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhgx;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgx;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 53435
    goto :goto_0

    .line 53434
    :cond_4
    iget-object v2, p0, Lhgx;->I:Ljava/util/List;

    iget-object v3, p1, Lhgx;->I:Ljava/util/List;

    .line 53435
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 53439
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 53441
    iget-object v2, p0, Lhgx;->a:[Ljava/lang/String;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 53447
    :cond_0
    iget-object v0, p0, Lhgx;->b:[Ljava/lang/String;

    if-nez v0, :cond_4

    mul-int/lit8 v2, v2, 0x1f

    .line 53453
    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhgx;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_0
    add-int/2addr v0, v1

    .line 53454
    return v0

    :cond_2
    move v2, v0

    move v0, v1

    .line 53443
    :goto_1
    iget-object v3, p0, Lhgx;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 53444
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgx;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_2
    add-int/2addr v2, v3

    .line 53443
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53444
    :cond_3
    iget-object v2, p0, Lhgx;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_4
    move v0, v1

    .line 53449
    :goto_3
    iget-object v3, p0, Lhgx;->b:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 53450
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgx;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 53449
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 53450
    :cond_5
    iget-object v2, p0, Lhgx;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_4

    .line 53453
    :cond_6
    iget-object v1, p0, Lhgx;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
