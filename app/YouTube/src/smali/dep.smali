.class public final Ldep;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcxz;
.implements Ldet;
.implements Leuc;


# instance fields
.field final a:Ldbv;

.field final b:Landroid/content/Context;

.field final c:Lcxd;

.field public d:Lgpa;

.field private final e:Ldeo;

.field private final f:Lgot;

.field private final g:Lcxy;

.field private final h:Landroid/os/Handler;

.field private final i:Ldes;

.field private final j:Levn;

.field private k:Lgpm;

.field private l:Ljava/util/List;

.field private m:I

.field private n:Leue;

.field private o:Z

.field private p:Z

.field private q:I


# direct methods
.method public constructor <init>(Ldeo;Lgot;Ldbv;Landroid/content/SharedPreferences;ZLandroid/content/Context;Levn;)V
    .locals 9

    .prologue
    .line 78
    const/4 v5, 0x0

    sget-object v6, Ldes;->a:Ldfe;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Ldep;-><init>(Ldeo;Lgot;Ldbv;Landroid/content/SharedPreferences;ZLdfe;Landroid/content/Context;Levn;)V

    .line 87
    return-void
.end method

.method private constructor <init>(Ldeo;Lgot;Ldbv;Landroid/content/SharedPreferences;ZLdfe;Landroid/content/Context;Levn;)V
    .locals 6

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Ldep;->e:Ldeo;

    .line 98
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgot;

    iput-object v0, p0, Ldep;->f:Lgot;

    .line 99
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbv;

    iput-object v0, p0, Ldep;->a:Ldbv;

    .line 100
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldep;->j:Levn;

    .line 101
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldep;->b:Landroid/content/Context;

    .line 102
    new-instance v0, Lcxy;

    new-instance v1, Lezj;

    invoke-direct {v1}, Lezj;-><init>()V

    new-instance v2, Landroid/os/Handler;

    .line 103
    invoke-virtual {p7}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1, v2}, Lcxy;-><init>(Lcxz;Lezj;Landroid/os/Handler;)V

    iput-object v0, p0, Ldep;->g:Lcxy;

    .line 104
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p7}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ldep;->h:Landroid/os/Handler;

    .line 105
    new-instance v0, Ldes;

    invoke-direct {v0, p7, p4, p5, p6}, Ldes;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;ZLdfe;)V

    iput-object v0, p0, Ldep;->i:Ldes;

    .line 107
    iget-object v0, p0, Ldep;->i:Ldes;

    iput-object p0, v0, Ldes;->b:Ldet;

    .line 108
    iget-object v0, p0, Ldep;->i:Ldes;

    invoke-virtual {v0}, Ldes;->c()Lgpo;

    move-result-object v0

    invoke-interface {p1, v0}, Ldeo;->a(Lgpo;)V

    .line 109
    iget-object v0, p0, Ldep;->i:Ldes;

    invoke-virtual {v0}, Ldes;->b()F

    move-result v0

    invoke-interface {p1, v0}, Ldeo;->a(F)V

    .line 111
    new-instance v0, Lcxd;

    iget-object v1, p0, Ldep;->h:Landroid/os/Handler;

    new-instance v3, Ldeq;

    invoke-direct {v3, p0}, Ldeq;-><init>(Ldep;)V

    const v2, 0x7f09010e

    .line 116
    invoke-virtual {p7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v2, p4

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcxd;-><init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcxe;Lgot;Ljava/lang/String;)V

    iput-object v0, p0, Ldep;->c:Lcxd;

    .line 118
    new-instance v0, Lder;

    invoke-direct {v0, p0}, Lder;-><init>(Ldep;)V

    iput-object v0, p3, Ldbv;->d:Ldby;

    .line 119
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Ldep;->g:Lcxy;

    invoke-virtual {v0}, Lcxy;->a()V

    .line 195
    iget-object v0, p0, Ldep;->e:Ldeo;

    invoke-interface {v0}, Ldeo;->a()V

    .line 196
    iget-object v0, p0, Ldep;->e:Ldeo;

    invoke-interface {v0}, Ldeo;->q_()V

    .line 197
    return-void
.end method

.method private b(I)V
    .locals 6

    .prologue
    .line 170
    iget-object v0, p0, Ldep;->k:Lgpm;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldep;->o:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldep;->p:Z

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Ldep;->e:Ldeo;

    iget-object v1, p0, Ldep;->k:Lgpm;

    invoke-virtual {v1, p1}, Lgpm;->a(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ldeo;->a(Ljava/util/List;)V

    .line 172
    iget-object v0, p0, Ldep;->l:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v0

    .line 173
    if-ltz v0, :cond_1

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iput v0, p0, Ldep;->m:I

    .line 174
    iget v0, p0, Ldep;->m:I

    iget-object v1, p0, Ldep;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 175
    iget-object v1, p0, Ldep;->g:Lcxy;

    iget-object v0, p0, Ldep;->l:Ljava/util/List;

    iget v2, p0, Ldep;->m:I

    .line 176
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 175
    iget-object v2, v1, Lcxy;->b:Lezj;

    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    int-to-long v4, p1

    sub-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, v1, Lcxy;->c:I

    invoke-virtual {v1, v0}, Lcxy;->a(I)V

    .line 181
    :cond_0
    :goto_1
    return-void

    .line 173
    :cond_1
    xor-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 178
    :cond_2
    invoke-direct {p0}, Ldep;->b()V

    goto :goto_1
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 235
    invoke-direct {p0}, Ldep;->b()V

    .line 236
    iget-object v0, p0, Ldep;->k:Lgpm;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Ldep;->a:Ldbv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldbv;->b(Z)V

    .line 238
    iput-object v2, p0, Ldep;->k:Lgpm;

    .line 240
    :cond_0
    iput-object v2, p0, Ldep;->d:Lgpa;

    .line 241
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Ldep;->e:Ldeo;

    iget-object v1, p0, Ldep;->i:Ldes;

    invoke-virtual {v1}, Ldes;->c()Lgpo;

    move-result-object v1

    invoke-interface {v0, v1}, Ldeo;->a(Lgpo;)V

    .line 306
    iget-object v0, p0, Ldep;->e:Ldeo;

    iget-object v1, p0, Ldep;->i:Ldes;

    invoke-virtual {v1}, Ldes;->b()F

    move-result v1

    invoke-interface {v0, v1}, Ldeo;->a(F)V

    .line 307
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 149
    iget-object v1, p0, Ldep;->k:Lgpm;

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, Ldep;->e:Ldeo;

    iget-object v2, p0, Ldep;->k:Lgpm;

    .line 151
    invoke-virtual {v2, p1}, Lgpm;->a(I)Ljava/util/List;

    move-result-object v2

    .line 150
    invoke-interface {v1, v2}, Ldeo;->a(Ljava/util/List;)V

    .line 154
    :cond_0
    iget-object v1, p0, Ldep;->k:Lgpm;

    if-nez v1, :cond_2

    .line 161
    :cond_1
    :goto_0
    return v0

    .line 157
    :cond_2
    iget v1, p0, Ldep;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ldep;->m:I

    .line 158
    iget v1, p0, Ldep;->m:I

    iget-object v2, p0, Ldep;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 159
    iget-object v0, p0, Ldep;->l:Ljava/util/List;

    iget v1, p0, Ldep;->m:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    iget-object v0, p0, Ldep;->i:Ldes;

    iput-object v1, v0, Ldes;->b:Ldet;

    .line 141
    iget-object v0, p0, Ldep;->i:Ldes;

    invoke-virtual {v0}, Ldes;->a()V

    .line 142
    iget-object v0, p0, Ldep;->a:Ldbv;

    iput-object v1, v0, Ldbv;->d:Ldby;

    .line 143
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Ldep;->e:Ldeo;

    invoke-interface {v0, p1}, Ldeo;->a(F)V

    .line 129
    return-void
.end method

.method public final a(Lgpa;)V
    .locals 3

    .prologue
    .line 200
    iput-object p1, p0, Ldep;->d:Lgpa;

    .line 201
    iget-object v0, p0, Ldep;->j:Levn;

    new-instance v1, Lczw;

    iget-object v2, p1, Lgpa;->h:Ljava/lang/String;

    invoke-direct {v1, v2}, Lczw;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 202
    iget-object v0, p0, Ldep;->n:Leue;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Ldep;->n:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 206
    :cond_0
    iget-object v0, p1, Lgpa;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    invoke-direct {p0}, Ldep;->c()V

    .line 213
    :goto_0
    return-void

    .line 209
    :cond_1
    invoke-static {p0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldep;->n:Leue;

    .line 210
    iget-object v0, p0, Ldep;->f:Lgot;

    iget-object v1, p0, Ldep;->h:Landroid/os/Handler;

    iget-object v2, p0, Ldep;->n:Leue;

    .line 211
    invoke-static {v1, v2}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v1

    .line 210
    invoke-interface {v0, p1, v1}, Lgot;->a(Lgpa;Leuc;)V

    goto :goto_0
.end method

.method public final a(Lgpo;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Ldep;->e:Ldeo;

    invoke-interface {v0, p1}, Ldeo;->a(Lgpo;)V

    .line 124
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 43
    const-string v0, "error retrieving subtitle"

    invoke-static {v0, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Ldep;->c()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 43
    check-cast p2, Lgpm;

    iput-object p2, p0, Ldep;->k:Lgpm;

    iget-object v0, p2, Lgpm;->a:Ljava/util/List;

    iput-object v0, p0, Ldep;->l:Ljava/util/List;

    iget-object v0, p0, Ldep;->a:Ldbv;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldbv;->b(Z)V

    iget v0, p0, Ldep;->q:I

    invoke-direct {p0, v0}, Ldep;->b(I)V

    return-void
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 254
    iget-object v1, p1, Ldac;->a:Lgol;

    invoke-virtual {v1}, Lgol;->d()Z

    move-result v1

    iput-boolean v1, p0, Ldep;->p:Z

    .line 255
    iget-object v1, p1, Ldac;->a:Lgol;

    sget-object v2, Lgol;->a:Lgol;

    if-ne v1, v2, :cond_1

    .line 256
    iget-object v1, p0, Ldep;->c:Lcxd;

    invoke-virtual {v1}, Lcxd;->b()V

    .line 257
    invoke-direct {p0}, Ldep;->c()V

    iget-object v1, p0, Ldep;->n:Leue;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldep;->n:Leue;

    iput-boolean v0, v1, Leue;->a:Z

    const/4 v0, 0x0

    iput-object v0, p0, Ldep;->n:Leue;

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v1, p1, Ldac;->a:Lgol;

    sget-object v2, Lgol;->b:Lgol;

    if-ne v1, v2, :cond_2

    .line 259
    iget-object v0, p0, Ldep;->c:Lcxd;

    iget-object v1, p1, Ldac;->b:Lfrl;

    invoke-virtual {v0, v1}, Lcxd;->a(Lfrl;)V

    .line 260
    invoke-direct {p0}, Ldep;->d()V

    goto :goto_0

    .line 261
    :cond_2
    iget-object v1, p1, Ldac;->a:Lgol;

    sget-object v2, Lgol;->i:Lgol;

    if-ne v1, v2, :cond_0

    .line 262
    iget-object v1, p0, Ldep;->c:Lcxd;

    iget-object v1, v1, Lcxd;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    :goto_1
    if-nez v0, :cond_3

    .line 263
    iget-object v0, p0, Ldep;->c:Lcxd;

    iget-object v1, p1, Ldac;->b:Lfrl;

    invoke-virtual {v0, v1}, Lcxd;->a(Lfrl;)V

    .line 264
    invoke-direct {p0}, Ldep;->d()V

    .line 266
    :cond_3
    iget-object v1, p0, Ldep;->j:Levn;

    new-instance v2, Lczw;

    iget-object v0, p0, Ldep;->d:Lgpa;

    if-eqz v0, :cond_5

    iget-object v0, p0, Ldep;->d:Lgpa;

    iget-object v0, v0, Lgpa;->h:Ljava/lang/String;

    :goto_2
    invoke-direct {v2, v0}, Lczw;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Levn;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 262
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 266
    :cond_5
    const-string v0, "-"

    goto :goto_2
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 300
    iget-wide v0, p1, Ldad;->a:J

    long-to-int v0, v0

    iput v0, p0, Ldep;->q:I

    .line 301
    iget v0, p0, Ldep;->q:I

    invoke-direct {p0, v0}, Ldep;->b(I)V

    .line 302
    return-void
.end method

.method public final handleYouTubePlayerStateEvent(Ldae;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 272
    iget v0, p1, Ldae;->a:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    .line 273
    iget v0, p1, Ldae;->a:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 274
    iget v0, p1, Ldae;->a:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ldep;->o:Z

    .line 276
    iget v0, p1, Ldae;->a:I

    packed-switch v0, :pswitch_data_0

    .line 296
    :goto_1
    :pswitch_0
    return-void

    .line 274
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 278
    :pswitch_1
    invoke-direct {p0}, Ldep;->b()V

    goto :goto_1

    .line 281
    :pswitch_2
    iget-object v0, p0, Ldep;->g:Lcxy;

    invoke-virtual {v0}, Lcxy;->a()V

    goto :goto_1

    .line 284
    :pswitch_3
    iget-object v0, p0, Ldep;->g:Lcxy;

    invoke-virtual {v0}, Lcxy;->a()V

    goto :goto_1

    .line 287
    :pswitch_4
    invoke-direct {p0}, Ldep;->b()V

    goto :goto_1

    .line 290
    :pswitch_5
    invoke-direct {p0}, Ldep;->b()V

    goto :goto_1

    .line 293
    :pswitch_6
    invoke-direct {p0}, Ldep;->b()V

    goto :goto_1

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
