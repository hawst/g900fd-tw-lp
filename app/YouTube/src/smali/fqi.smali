.class public final enum Lfqi;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfqi;

.field public static final enum b:Lfqi;

.field public static final enum c:Lfqi;

.field public static final enum d:Lfqi;

.field public static final enum e:Lfqi;

.field public static final enum f:Lfqi;

.field public static final enum g:Lfqi;

.field public static final enum h:Lfqi;

.field public static final enum i:Lfqi;

.field public static final enum j:Lfqi;

.field public static final enum k:Lfqi;

.field private static enum m:Lfqi;

.field private static enum n:Lfqi;

.field private static final synthetic o:[Lfqi;


# instance fields
.field public final l:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lfqi;

    const-string v1, "WATCH_PAGE"

    const/16 v2, 0xef8

    invoke-direct {v0, v1, v4, v2}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->a:Lfqi;

    .line 14
    new-instance v0, Lfqi;

    const-string v1, "BROWSE_PAGE"

    const/16 v2, 0x1aab

    invoke-direct {v0, v1, v5, v2}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->b:Lfqi;

    .line 15
    new-instance v0, Lfqi;

    const-string v1, "SEARCH_PAGE"

    const/16 v2, 0x1274

    invoke-direct {v0, v1, v6, v2}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->c:Lfqi;

    .line 16
    new-instance v0, Lfqi;

    const-string v1, "GUIDE"

    const/16 v2, 0xf7e

    invoke-direct {v0, v1, v7, v2}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->d:Lfqi;

    .line 17
    new-instance v0, Lfqi;

    const-string v1, "PREVIOUS_VIDEO_BUTTON"

    const/16 v2, 0x1a93

    invoke-direct {v0, v1, v8, v2}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->m:Lfqi;

    .line 18
    new-instance v0, Lfqi;

    const-string v1, "UPLOAD_VIDEO_EDITING_PAGE"

    const/4 v2, 0x5

    const/16 v3, 0x2601

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->e:Lfqi;

    .line 19
    new-instance v0, Lfqi;

    const-string v1, "UPLOAD_VIDEO_EDITING_COMPLETED_BUTTON"

    const/4 v2, 0x6

    const/16 v3, 0x25e5

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->f:Lfqi;

    .line 20
    new-instance v0, Lfqi;

    const-string v1, "UPLOAD_VIDEO_EDITING_CANCEL_BUTTON"

    const/4 v2, 0x7

    const/16 v3, 0x25e6

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->g:Lfqi;

    .line 21
    new-instance v0, Lfqi;

    const-string v1, "UPLOAD_VIDEO_EDITING_VIDEO_PREVIEW"

    const/16 v2, 0x8

    const/16 v3, 0x25e8

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->h:Lfqi;

    .line 22
    new-instance v0, Lfqi;

    const-string v1, "UPLOAD_VIDEO_EDITING_VIDEO_PREVIEW_ERROR"

    const/16 v2, 0x9

    const/16 v3, 0x25e9

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->i:Lfqi;

    .line 23
    new-instance v0, Lfqi;

    const-string v1, "UPLOAD_VIDEO_EDITING_TRIM_VIEW"

    const/16 v2, 0xa

    const/16 v3, 0x25ea

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->j:Lfqi;

    .line 24
    new-instance v0, Lfqi;

    const-string v1, "YPC_OFFERS_PAGE"

    const/16 v2, 0xb

    const/16 v3, 0x2004

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->k:Lfqi;

    .line 25
    new-instance v0, Lfqi;

    const-string v1, "YPC_TIP_JAR_PAGE"

    const/16 v2, 0xc

    const/16 v3, 0x2005

    invoke-direct {v0, v1, v2, v3}, Lfqi;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lfqi;->n:Lfqi;

    .line 12
    const/16 v0, 0xd

    new-array v0, v0, [Lfqi;

    sget-object v1, Lfqi;->a:Lfqi;

    aput-object v1, v0, v4

    sget-object v1, Lfqi;->b:Lfqi;

    aput-object v1, v0, v5

    sget-object v1, Lfqi;->c:Lfqi;

    aput-object v1, v0, v6

    sget-object v1, Lfqi;->d:Lfqi;

    aput-object v1, v0, v7

    sget-object v1, Lfqi;->m:Lfqi;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lfqi;->e:Lfqi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lfqi;->f:Lfqi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lfqi;->g:Lfqi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lfqi;->h:Lfqi;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lfqi;->i:Lfqi;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lfqi;->j:Lfqi;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lfqi;->k:Lfqi;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lfqi;->n:Lfqi;

    aput-object v2, v0, v1

    sput-object v0, Lfqi;->o:[Lfqi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lfqi;->l:I

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfqi;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lfqi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfqi;

    return-object v0
.end method

.method public static values()[Lfqi;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lfqi;->o:[Lfqi;

    invoke-virtual {v0}, [Lfqi;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfqi;

    return-object v0
.end method
