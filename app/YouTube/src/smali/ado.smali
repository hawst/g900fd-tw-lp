.class public final Lado;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Z

.field public final c:Lacw;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Lacw;Lgoh;ZZ)V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/4 v1, -0x2

    const/4 v2, -0x1

    .line 91
    invoke-static {p5, p6}, Lado;->a(ZZ)I

    move-result v0

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 93
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lado;->a:Landroid/app/Activity;

    .line 95
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    iput-object v0, p0, Lado;->c:Lacw;

    .line 97
    iput-boolean p5, p0, Lado;->b:Z

    .line 100
    if-eqz p5, :cond_0

    .line 101
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lado;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 102
    const v3, 0x1080011

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 103
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v2, v1, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v0, p2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v4, 0x7f070089

    invoke-virtual {v3, v4}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    move-object p2, v0

    .line 114
    :goto_0
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lado;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 115
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz p5, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {v4, v2, v0, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v3, p2, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 117
    invoke-virtual {p0, v3}, Lado;->setContentView(Landroid/view/View;)V

    .line 119
    invoke-virtual {p3, p4}, Lacw;->a(Lgoh;)V

    .line 120
    return-void

    .line 110
    :cond_0
    const/high16 v0, -0x1000000

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 115
    goto :goto_1
.end method

.method public static a(ZZ)I
    .locals 1

    .prologue
    .line 75
    if-eqz p0, :cond_1

    .line 76
    if-eqz p1, :cond_0

    const v0, 0x7f0d0103

    .line 80
    :goto_0
    return v0

    .line 76
    :cond_0
    const v0, 0x7f0d0102

    goto :goto_0

    .line 80
    :cond_1
    const v0, 0x7f0d0101

    goto :goto_0
.end method


# virtual methods
.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lado;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x1

    .line 152
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lado;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x1

    .line 143
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final onStop()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lado;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lado;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 127
    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->onStop()V

    .line 128
    return-void
.end method
