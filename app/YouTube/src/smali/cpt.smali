.class public final enum Lcpt;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcpt;

.field public static final enum b:Lcpt;

.field private static enum c:Lcpt;

.field private static final synthetic e:[Lcpt;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 56
    new-instance v0, Lcpt;

    const-string v1, "VOD"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcpt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpt;->a:Lcpt;

    .line 57
    new-instance v0, Lcpt;

    const-string v1, "LIVE"

    const-string v2, "live"

    invoke-direct {v0, v1, v4, v2}, Lcpt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpt;->b:Lcpt;

    .line 58
    new-instance v0, Lcpt;

    const-string v1, "DVR"

    const-string v2, "dvr"

    invoke-direct {v0, v1, v5, v2}, Lcpt;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpt;->c:Lcpt;

    .line 55
    const/4 v0, 0x3

    new-array v0, v0, [Lcpt;

    sget-object v1, Lcpt;->a:Lcpt;

    aput-object v1, v0, v3

    sget-object v1, Lcpt;->b:Lcpt;

    aput-object v1, v0, v4

    sget-object v1, Lcpt;->c:Lcpt;

    aput-object v1, v0, v5

    sput-object v0, Lcpt;->e:[Lcpt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput-object p3, p0, Lcpt;->d:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public static a(I)Lcpt;
    .locals 1

    .prologue
    .line 76
    if-ltz p0, :cond_0

    invoke-static {}, Lcpt;->values()[Lcpt;

    move-result-object v0

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 77
    :cond_0
    sget-object v0, Lcpt;->a:Lcpt;

    .line 79
    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Lcpt;->values()[Lcpt;

    move-result-object v0

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcpt;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcpt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcpt;

    return-object v0
.end method

.method public static values()[Lcpt;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcpt;->e:[Lcpt;

    invoke-virtual {v0}, [Lcpt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcpt;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcpt;->d:Ljava/lang/String;

    return-object v0
.end method
