.class public final Lcfi;
.super Lfsa;
.source "SourceFile"


# instance fields
.field private final a:Leyp;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ImageView;

.field private f:Lbxd;


# direct methods
.method public constructor <init>(Lbhz;Lffs;Lgix;Lcub;Leyp;Lfhz;Levn;Leyt;Lfdw;Lfrz;)V
    .locals 13

    .prologue
    .line 56
    move-object/from16 v0, p9

    move-object/from16 v1, p10

    invoke-direct {p0, v0, v1}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 57
    invoke-static/range {p5 .. p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Leyp;

    iput-object v2, p0, Lcfi;->a:Leyp;

    .line 59
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcfi;->b:Landroid/view/LayoutInflater;

    .line 60
    iget-object v2, p0, Lcfi;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f040087

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcfi;->c:Landroid/view/View;

    .line 61
    iget-object v2, p0, Lcfi;->c:Landroid/view/View;

    const v3, 0x7f0800c1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcfi;->d:Landroid/widget/TextView;

    .line 62
    iget-object v2, p0, Lcfi;->c:Landroid/view/View;

    const v3, 0x7f080087

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcfi;->e:Landroid/widget/ImageView;

    .line 63
    new-instance v2, Lbxd;

    new-instance v3, Lbqq;

    iget-object v4, p0, Lcfi;->c:Landroid/view/View;

    const v5, 0x7f0801ec

    .line 64
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lbqq;-><init>(Landroid/view/View;Z)V

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p8

    move-object/from16 v9, p7

    move-object/from16 v10, p6

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    invoke-direct/range {v2 .. v12}, Lbxd;-><init>(Lbxi;Landroid/app/Activity;Lffs;Lgix;Lcub;Leyt;Levn;Lfhz;Lfdw;Lfrz;)V

    iput-object v2, p0, Lcfi;->f:Lbxd;

    .line 74
    return-void
.end method

.method private a(Lfkw;)Landroid/view/View;
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcfi;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lfkw;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    iget-object v1, p1, Lfkw;->a:Lhny;

    iget-object v1, v1, Lhny;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p1, Lfkw;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, p1, Lfkw;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    invoke-virtual {p1}, Lfkw;->a()Lfnc;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lfkw;->a()Lfnc;

    move-result-object v0

    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcfi;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 84
    invoke-virtual {p1}, Lfkw;->a()Lfnc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfnc;->a(I)Lfnb;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcfi;->a:Leyp;

    iget-object v0, v0, Lfnb;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcfi;->e:Landroid/widget/ImageView;

    invoke-static {v1, v0, v2}, Leyi;->a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;)V

    .line 88
    :cond_1
    iget-object v0, p0, Lcfi;->f:Lbxd;

    iget-object v1, p1, Lfkw;->c:Lfmy;

    if-nez v1, :cond_2

    iget-object v1, p1, Lfkw;->a:Lhny;

    iget-object v1, v1, Lhny;->c:Lhoc;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lfkw;->a:Lhny;

    iget-object v1, v1, Lhny;->c:Lhoc;

    iget-object v1, v1, Lhoc;->a:Lhwp;

    if-eqz v1, :cond_2

    new-instance v1, Lfmy;

    iget-object v2, p1, Lfkw;->a:Lhny;

    iget-object v2, v2, Lhny;->c:Lhoc;

    iget-object v2, v2, Lhoc;->a:Lhwp;

    invoke-direct {v1, v2, p1}, Lfmy;-><init>(Lhwp;Lfqh;)V

    iput-object v1, p1, Lfkw;->c:Lfmy;

    :cond_2
    iget-object v1, p1, Lfkw;->c:Lfmy;

    invoke-virtual {v0, v1}, Lbxd;->a(Lfmy;)V

    .line 90
    iget-object v0, p0, Lcfi;->c:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    check-cast p2, Lfkw;

    invoke-direct {p0, p2}, Lcfi;->a(Lfkw;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    check-cast p2, Lfkw;

    invoke-direct {p0, p2}, Lcfi;->a(Lfkw;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
