.class public final Lfmi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhul;

.field public b:Ljava/util/List;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lhul;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhul;

    iput-object v0, p0, Lfmi;->a:Lhul;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 36
    iget-object v0, p0, Lfmi;->c:Ljava/util/List;

    if-nez v0, :cond_7

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfmi;->a:Lhul;

    iget-object v1, v1, Lhul;->a:[Lhun;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfmi;->c:Ljava/util/List;

    .line 38
    iget-object v0, p0, Lfmi;->a:Lhul;

    iget-object v1, v0, Lhul;->a:[Lhun;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 39
    iget-object v4, v3, Lhun;->b:Lhky;

    if-eqz v4, :cond_1

    .line 40
    iget-object v4, p0, Lfmi;->c:Ljava/util/List;

    new-instance v5, Lfkj;

    iget-object v3, v3, Lhun;->b:Lhky;

    invoke-direct {v5, v3, p0}, Lfkj;-><init>(Lhky;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_1
    iget-object v4, v3, Lhun;->c:Lhkv;

    if-eqz v4, :cond_2

    .line 42
    iget-object v4, p0, Lfmi;->c:Ljava/util/List;

    new-instance v5, Lfkl;

    iget-object v3, v3, Lhun;->c:Lhkv;

    invoke-direct {v5, v3, p0}, Lfkl;-><init>(Lhkv;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 43
    :cond_2
    iget-object v4, v3, Lhun;->f:Lhsl;

    if-eqz v4, :cond_3

    .line 44
    iget-object v4, p0, Lfmi;->c:Ljava/util/List;

    new-instance v5, Lfmc;

    iget-object v3, v3, Lhun;->f:Lhsl;

    invoke-direct {v5, v3, p0}, Lfmc;-><init>(Lhsl;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_3
    iget-object v4, v3, Lhun;->e:Lhdo;

    if-eqz v4, :cond_4

    .line 46
    iget-object v4, p0, Lfmi;->c:Ljava/util/List;

    iget-object v3, v3, Lhun;->e:Lhdo;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 47
    :cond_4
    iget-object v4, v3, Lhun;->g:Lhqg;

    if-eqz v4, :cond_5

    .line 48
    iget-object v4, p0, Lfmi;->c:Ljava/util/List;

    iget-object v3, v3, Lhun;->g:Lhqg;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 49
    :cond_5
    iget-object v4, v3, Lhun;->d:Lhvp;

    if-eqz v4, :cond_0

    .line 50
    iget-object v3, v3, Lhun;->d:Lhvp;

    .line 51
    iget-object v4, v3, Lhvp;->d:Lhvq;

    if-eqz v4, :cond_6

    iget-object v4, v3, Lhvp;->d:Lhvq;

    iget-object v4, v4, Lhvq;->b:Lhyx;

    if-eqz v4, :cond_6

    .line 52
    iget-object v4, p0, Lfmi;->c:Ljava/util/List;

    new-instance v5, Lfnh;

    invoke-direct {v5, v3, p0}, Lfnh;-><init>(Lhvp;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 53
    :cond_6
    iget-object v4, v3, Lhvp;->d:Lhvq;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lhvp;->d:Lhvq;

    iget-object v4, v4, Lhvq;->a:Lhin;

    if-eqz v4, :cond_0

    .line 54
    iget-object v4, p0, Lfmi;->c:Ljava/util/List;

    new-instance v5, Lfkd;

    invoke-direct {v5, v3, p0}, Lfkd;-><init>(Lhvp;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 59
    :cond_7
    iget-object v0, p0, Lfmi;->c:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 3

    .prologue
    .line 91
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 92
    invoke-virtual {p0}, Lfmi;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 93
    instance-of v2, v0, Lfat;

    if-eqz v2, :cond_0

    .line 94
    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    goto :goto_0

    .line 97
    :cond_1
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lfmi;->a:Lhul;

    iget-object v0, v0, Lhul;->c:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method
