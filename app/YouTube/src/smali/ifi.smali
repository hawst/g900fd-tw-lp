.class public final Lifi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final g:[I


# instance fields
.field private final a:Lifx;

.field private b:Z

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lifi;->g:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xee0
        0x1dc
        0x83b
        0x707
    .end array-data
.end method

.method public constructor <init>(Lifx;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lifi;->a:Lifx;

    .line 49
    return-void
.end method

.method private static a(JZ)I
    .locals 8

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x0

    .line 197
    if-eqz p2, :cond_0

    .line 198
    const/4 v2, 0x7

    .line 199
    const/4 v0, 0x2

    .line 205
    :goto_0
    sub-int v4, v2, v0

    .line 206
    new-array v5, v2, [I

    .line 207
    add-int/lit8 v2, v2, -0x1

    :goto_1
    if-ltz v2, :cond_1

    .line 208
    long-to-int v6, p0

    and-int/lit8 v6, v6, 0xf

    aput v6, v5, v2

    .line 209
    shr-long/2addr p0, v1

    .line 207
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 201
    :cond_0
    const/16 v0, 0xa

    move v2, v0

    move v0, v1

    .line 202
    goto :goto_0

    .line 212
    :cond_1
    :try_start_0
    new-instance v1, Ligl;

    sget-object v2, Ligj;->d:Ligj;

    invoke-direct {v1, v2}, Ligl;-><init>(Ligj;)V

    .line 213
    invoke-virtual {v1, v5, v4}, Ligl;->a([II)V
    :try_end_0
    .catch Ligm; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v3

    .line 219
    :goto_2
    if-ge v1, v0, :cond_2

    .line 220
    shl-int/lit8 v2, v3, 0x4

    aget v3, v5, v1

    add-int/2addr v3, v2

    .line 219
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 215
    :catch_0
    move-exception v0

    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    .line 222
    :cond_2
    return v3
.end method

.method private a(Lifb;Lifb;I)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 400
    .line 402
    iget v1, p1, Lifb;->a:F

    iget v2, p1, Lifb;->b:F

    iget v3, p2, Lifb;->a:F

    iget v4, p2, Lifb;->b:F

    invoke-static {v1, v2, v3, v4}, La;->a(FFFF)F

    move-result v1

    .line 403
    int-to-float v2, p3

    div-float v2, v1, v2

    .line 404
    iget v3, p1, Lifb;->a:F

    .line 405
    iget v4, p1, Lifb;->b:F

    .line 406
    iget v5, p2, Lifb;->a:F

    iget v6, p1, Lifb;->a:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v2

    div-float/2addr v5, v1

    .line 407
    iget v6, p2, Lifb;->b:F

    iget v7, p1, Lifb;->b:F

    sub-float/2addr v6, v7

    mul-float/2addr v2, v6

    div-float/2addr v2, v1

    move v1, v0

    .line 408
    :goto_0
    if-ge v1, p3, :cond_1

    .line 409
    iget-object v6, p0, Lifi;->a:Lifx;

    int-to-float v7, v1

    mul-float/2addr v7, v5

    add-float/2addr v7, v3

    invoke-static {v7}, La;->b(F)I

    move-result v7

    int-to-float v8, v1

    mul-float/2addr v8, v2

    add-float/2addr v8, v4

    invoke-static {v8}, La;->b(F)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lifx;->a(II)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 410
    const/4 v6, 0x1

    sub-int v7, p3, v1

    add-int/lit8 v7, v7, -0x1

    shl-int/2addr v6, v7

    or-int/2addr v0, v6

    .line 408
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 413
    :cond_1
    return v0
.end method

.method private a(Lifj;Lifj;)I
    .locals 12

    .prologue
    .line 462
    invoke-static {p1, p2}, Lifi;->b(Lifj;Lifj;)F

    move-result v4

    .line 463
    iget v0, p2, Lifj;->a:I

    iget v1, p1, Lifj;->a:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float v5, v0, v4

    .line 464
    iget v0, p2, Lifj;->b:I

    iget v1, p1, Lifj;->b:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float v6, v0, v4

    .line 465
    const/4 v3, 0x0

    .line 467
    iget v0, p1, Lifj;->a:I

    int-to-float v2, v0

    .line 468
    iget v0, p1, Lifj;->b:I

    int-to-float v1, v0

    .line 470
    iget-object v0, p0, Lifi;->a:Lifx;

    iget v7, p1, Lifj;->a:I

    iget v8, p1, Lifj;->b:I

    invoke-virtual {v0, v7, v8}, Lifx;->a(II)Z

    move-result v7

    .line 472
    const/4 v0, 0x0

    move v11, v0

    move v0, v3

    move v3, v2

    move v2, v1

    move v1, v11

    :goto_0
    int-to-float v8, v1

    cmpg-float v8, v8, v4

    if-gez v8, :cond_1

    .line 473
    add-float/2addr v3, v5

    .line 474
    add-float/2addr v2, v6

    .line 475
    iget-object v8, p0, Lifi;->a:Lifx;

    invoke-static {v3}, La;->b(F)I

    move-result v9

    invoke-static {v2}, La;->b(F)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lifx;->a(II)Z

    move-result v8

    if-eq v8, v7, :cond_0

    .line 476
    add-int/lit8 v0, v0, 0x1

    .line 472
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 480
    :cond_1
    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 482
    const v1, 0x3dcccccd    # 0.1f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    const v1, 0x3f666666    # 0.9f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_2

    .line 483
    const/4 v0, 0x0

    .line 486
    :goto_1
    return v0

    :cond_2
    const v1, 0x3dcccccd    # 0.1f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-ne v0, v7, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private a()Lifj;
    .locals 11

    .prologue
    const/high16 v10, 0x40800000    # 4.0f

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 303
    :try_start_0
    new-instance v0, Ligi;

    iget-object v1, p0, Lifi;->a:Lifx;

    invoke-direct {v0, v1}, Ligi;-><init>(Lifx;)V

    invoke-virtual {v0}, Ligi;->a()[Lifb;

    move-result-object v0

    .line 304
    const/4 v1, 0x0

    aget-object v3, v0, v1

    .line 305
    const/4 v1, 0x1

    aget-object v2, v0, v1

    .line 306
    const/4 v1, 0x2

    aget-object v1, v0, v1

    .line 307
    const/4 v4, 0x3

    aget-object v0, v0, v4
    :try_end_0
    .catch Liew; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    :goto_0
    iget v4, v3, Lifb;->a:F

    iget v5, v0, Lifb;->a:F

    add-float/2addr v4, v5

    iget v5, v2, Lifb;->a:F

    add-float/2addr v4, v5

    iget v5, v1, Lifb;->a:F

    add-float/2addr v4, v5

    div-float/2addr v4, v10

    invoke-static {v4}, La;->b(F)I

    move-result v4

    .line 324
    iget v3, v3, Lifb;->b:F

    iget v0, v0, Lifb;->b:F

    add-float/2addr v0, v3

    iget v2, v2, Lifb;->b:F

    add-float/2addr v0, v2

    iget v1, v1, Lifb;->b:F

    add-float/2addr v0, v1

    div-float/2addr v0, v10

    invoke-static {v0}, La;->b(F)I

    move-result v0

    .line 330
    :try_start_1
    new-instance v1, Ligi;

    iget-object v2, p0, Lifi;->a:Lifx;

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3, v4, v0}, Ligi;-><init>(Lifx;III)V

    invoke-virtual {v1}, Ligi;->a()[Lifb;

    move-result-object v5

    .line 331
    const/4 v1, 0x0

    aget-object v3, v5, v1

    .line 332
    const/4 v1, 0x1

    aget-object v2, v5, v1

    .line 333
    const/4 v1, 0x2

    aget-object v1, v5, v1

    .line 334
    const/4 v6, 0x3

    aget-object v0, v5, v6
    :try_end_1
    .catch Liew; {:try_start_1 .. :try_end_1} :catch_1

    .line 345
    :goto_1
    iget v4, v3, Lifb;->a:F

    iget v5, v0, Lifb;->a:F

    add-float/2addr v4, v5

    iget v5, v2, Lifb;->a:F

    add-float/2addr v4, v5

    iget v5, v1, Lifb;->a:F

    add-float/2addr v4, v5

    div-float/2addr v4, v10

    invoke-static {v4}, La;->b(F)I

    move-result v4

    .line 346
    iget v3, v3, Lifb;->b:F

    iget v0, v0, Lifb;->b:F

    add-float/2addr v0, v3

    iget v2, v2, Lifb;->b:F

    add-float/2addr v0, v2

    iget v1, v1, Lifb;->b:F

    add-float/2addr v0, v1

    div-float/2addr v0, v10

    invoke-static {v0}, La;->b(F)I

    move-result v0

    .line 348
    new-instance v1, Lifj;

    invoke-direct {v1, v4, v0}, Lifj;-><init>(II)V

    return-object v1

    .line 313
    :catch_0
    move-exception v0

    iget-object v0, p0, Lifi;->a:Lifx;

    iget v0, v0, Lifx;->a:I

    div-int/lit8 v0, v0, 0x2

    .line 314
    iget-object v1, p0, Lifi;->a:Lifx;

    iget v1, v1, Lifx;->b:I

    div-int/lit8 v4, v1, 0x2

    .line 315
    new-instance v1, Lifj;

    add-int/lit8 v2, v0, 0x7

    add-int/lit8 v3, v4, -0x7

    invoke-direct {v1, v2, v3}, Lifj;-><init>(II)V

    invoke-direct {p0, v1, v7, v8, v9}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v1

    invoke-virtual {v1}, Lifj;->a()Lifb;

    move-result-object v3

    .line 316
    new-instance v1, Lifj;

    add-int/lit8 v2, v0, 0x7

    add-int/lit8 v5, v4, 0x7

    invoke-direct {v1, v2, v5}, Lifj;-><init>(II)V

    invoke-direct {p0, v1, v7, v8, v8}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v1

    invoke-virtual {v1}, Lifj;->a()Lifb;

    move-result-object v2

    .line 317
    new-instance v1, Lifj;

    add-int/lit8 v5, v0, -0x7

    add-int/lit8 v6, v4, 0x7

    invoke-direct {v1, v5, v6}, Lifj;-><init>(II)V

    invoke-direct {p0, v1, v7, v9, v8}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v1

    invoke-virtual {v1}, Lifj;->a()Lifb;

    move-result-object v1

    .line 318
    new-instance v5, Lifj;

    add-int/lit8 v0, v0, -0x7

    add-int/lit8 v4, v4, -0x7

    invoke-direct {v5, v0, v4}, Lifj;-><init>(II)V

    invoke-direct {p0, v5, v7, v9, v9}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v0

    invoke-virtual {v0}, Lifj;->a()Lifb;

    move-result-object v0

    goto/16 :goto_0

    .line 338
    :catch_1
    move-exception v1

    new-instance v1, Lifj;

    add-int/lit8 v2, v4, 0x7

    add-int/lit8 v3, v0, -0x7

    invoke-direct {v1, v2, v3}, Lifj;-><init>(II)V

    invoke-direct {p0, v1, v7, v8, v9}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v1

    invoke-virtual {v1}, Lifj;->a()Lifb;

    move-result-object v3

    .line 339
    new-instance v1, Lifj;

    add-int/lit8 v2, v4, 0x7

    add-int/lit8 v5, v0, 0x7

    invoke-direct {v1, v2, v5}, Lifj;-><init>(II)V

    invoke-direct {p0, v1, v7, v8, v8}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v1

    invoke-virtual {v1}, Lifj;->a()Lifb;

    move-result-object v2

    .line 340
    new-instance v1, Lifj;

    add-int/lit8 v5, v4, -0x7

    add-int/lit8 v6, v0, 0x7

    invoke-direct {v1, v5, v6}, Lifj;-><init>(II)V

    invoke-direct {p0, v1, v7, v9, v8}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v1

    invoke-virtual {v1}, Lifj;->a()Lifb;

    move-result-object v1

    .line 341
    new-instance v5, Lifj;

    add-int/lit8 v4, v4, -0x7

    add-int/lit8 v0, v0, -0x7

    invoke-direct {v5, v4, v0}, Lifj;-><init>(II)V

    invoke-direct {p0, v5, v7, v9, v9}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v0

    invoke-virtual {v0}, Lifj;->a()Lifb;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private a(Lifj;ZII)Lifj;
    .locals 3

    .prologue
    .line 493
    iget v0, p1, Lifj;->a:I

    add-int v1, v0, p3

    .line 494
    iget v0, p1, Lifj;->b:I

    add-int/2addr v0, p4

    .line 496
    :goto_0
    invoke-direct {p0, v1, v0}, Lifi;->a(II)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lifi;->a:Lifx;

    invoke-virtual {v2, v1, v0}, Lifx;->a(II)Z

    move-result v2

    if-ne v2, p2, :cond_0

    .line 497
    add-int/2addr v1, p3

    .line 498
    add-int/2addr v0, p4

    goto :goto_0

    .line 501
    :cond_0
    sub-int/2addr v1, p3

    .line 502
    sub-int v2, v0, p4

    move v0, v1

    .line 504
    :goto_1
    invoke-direct {p0, v0, v2}, Lifi;->a(II)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lifi;->a:Lifx;

    invoke-virtual {v1, v0, v2}, Lifx;->a(II)Z

    move-result v1

    if-ne v1, p2, :cond_1

    .line 505
    add-int/2addr v0, p3

    goto :goto_1

    .line 507
    :cond_1
    sub-int v1, v0, p3

    move v0, v2

    .line 509
    :goto_2
    invoke-direct {p0, v1, v0}, Lifi;->a(II)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lifi;->a:Lifx;

    invoke-virtual {v2, v1, v0}, Lifx;->a(II)Z

    move-result v2

    if-ne v2, p2, :cond_2

    .line 510
    add-int/2addr v0, p4

    goto :goto_2

    .line 512
    :cond_2
    sub-int/2addr v0, p4

    .line 514
    new-instance v2, Lifj;

    invoke-direct {v2, v1, v0}, Lifj;-><init>(II)V

    return-object v2
.end method

.method private a(II)Z
    .locals 1

    .prologue
    .line 546
    if-ltz p1, :cond_0

    iget-object v0, p0, Lifi;->a:Lifx;

    iget v0, v0, Lifx;->a:I

    if-ge p1, v0, :cond_0

    if-lez p2, :cond_0

    iget-object v0, p0, Lifi;->a:Lifx;

    iget v0, v0, Lifx;->b:I

    if-ge p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lifb;)Z
    .locals 2

    .prologue
    .line 550
    iget v0, p1, Lifb;->a:F

    invoke-static {v0}, La;->b(F)I

    move-result v0

    .line 551
    iget v1, p1, Lifb;->b:F

    invoke-static {v1}, La;->b(F)I

    move-result v1

    .line 552
    invoke-direct {p0, v0, v1}, Lifi;->a(II)Z

    move-result v0

    return v0
.end method

.method private static a([Lifb;FF)[Lifb;
    .locals 10

    .prologue
    .line 526
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, p1

    div-float v0, p2, v0

    .line 527
    const/4 v1, 0x0

    aget-object v1, p0, v1

    iget v1, v1, Lifb;->a:F

    const/4 v2, 0x2

    aget-object v2, p0, v2

    iget v2, v2, Lifb;->a:F

    sub-float/2addr v1, v2

    .line 528
    const/4 v2, 0x0

    aget-object v2, p0, v2

    iget v2, v2, Lifb;->b:F

    const/4 v3, 0x2

    aget-object v3, p0, v3

    iget v3, v3, Lifb;->b:F

    sub-float/2addr v2, v3

    .line 529
    const/4 v3, 0x0

    aget-object v3, p0, v3

    iget v3, v3, Lifb;->a:F

    const/4 v4, 0x2

    aget-object v4, p0, v4

    iget v4, v4, Lifb;->a:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 530
    const/4 v4, 0x0

    aget-object v4, p0, v4

    iget v4, v4, Lifb;->b:F

    const/4 v5, 0x2

    aget-object v5, p0, v5

    iget v5, v5, Lifb;->b:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    .line 532
    new-instance v5, Lifb;

    mul-float v6, v0, v1

    add-float/2addr v6, v3

    mul-float v7, v0, v2

    add-float/2addr v7, v4

    invoke-direct {v5, v6, v7}, Lifb;-><init>(FF)V

    .line 533
    new-instance v6, Lifb;

    mul-float/2addr v1, v0

    sub-float v1, v3, v1

    mul-float/2addr v2, v0

    sub-float v2, v4, v2

    invoke-direct {v6, v1, v2}, Lifb;-><init>(FF)V

    .line 535
    const/4 v1, 0x1

    aget-object v1, p0, v1

    iget v1, v1, Lifb;->a:F

    const/4 v2, 0x3

    aget-object v2, p0, v2

    iget v2, v2, Lifb;->a:F

    sub-float/2addr v1, v2

    .line 536
    const/4 v2, 0x1

    aget-object v2, p0, v2

    iget v2, v2, Lifb;->b:F

    const/4 v3, 0x3

    aget-object v3, p0, v3

    iget v3, v3, Lifb;->b:F

    sub-float/2addr v2, v3

    .line 537
    const/4 v3, 0x1

    aget-object v3, p0, v3

    iget v3, v3, Lifb;->a:F

    const/4 v4, 0x3

    aget-object v4, p0, v4

    iget v4, v4, Lifb;->a:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    .line 538
    const/4 v4, 0x1

    aget-object v4, p0, v4

    iget v4, v4, Lifb;->b:F

    const/4 v7, 0x3

    aget-object v7, p0, v7

    iget v7, v7, Lifb;->b:F

    add-float/2addr v4, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v4, v7

    .line 539
    new-instance v7, Lifb;

    mul-float v8, v0, v1

    add-float/2addr v8, v3

    mul-float v9, v0, v2

    add-float/2addr v9, v4

    invoke-direct {v7, v8, v9}, Lifb;-><init>(FF)V

    .line 540
    new-instance v8, Lifb;

    mul-float/2addr v1, v0

    sub-float v1, v3, v1

    mul-float/2addr v0, v2

    sub-float v0, v4, v0

    invoke-direct {v8, v1, v0}, Lifb;-><init>(FF)V

    .line 542
    const/4 v0, 0x4

    new-array v0, v0, [Lifb;

    const/4 v1, 0x0

    aput-object v5, v0, v1

    const/4 v1, 0x1

    aput-object v7, v0, v1

    const/4 v1, 0x2

    aput-object v6, v0, v1

    const/4 v1, 0x3

    aput-object v8, v0, v1

    return-object v0
.end method

.method private static b(Lifj;Lifj;)F
    .locals 4

    .prologue
    .line 556
    iget v0, p0, Lifj;->a:I

    iget v1, p0, Lifj;->b:I

    iget v2, p1, Lifj;->a:I

    iget v3, p1, Lifj;->b:I

    invoke-static {v0, v1, v2, v3}, La;->a(IIII)F

    move-result v0

    return v0
.end method

.method private b()I
    .locals 2

    .prologue
    .line 564
    iget-boolean v0, p0, Lifi;->b:Z

    if-eqz v0, :cond_0

    .line 565
    iget v0, p0, Lifi;->c:I

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0xb

    .line 570
    :goto_0
    return v0

    .line 567
    :cond_0
    iget v0, p0, Lifi;->c:I

    const/4 v1, 0x4

    if-gt v0, v1, :cond_1

    .line 568
    iget v0, p0, Lifi;->c:I

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0xf

    goto :goto_0

    .line 570
    :cond_1
    iget v0, p0, Lifi;->c:I

    mul-int/lit8 v0, v0, 0x4

    iget v1, p0, Lifi;->c:I

    add-int/lit8 v1, v1, -0x4

    div-int/lit8 v1, v1, 0x8

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0xf

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)Lifd;
    .locals 24

    .prologue
    .line 64
    invoke-direct/range {p0 .. p0}, Lifi;->a()Lifj;

    move-result-object v3

    .line 68
    const/4 v2, 0x1

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lifi;->e:I

    move-object v5, v3

    move-object v7, v3

    move-object v9, v3

    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lifi;->e:I

    const/16 v6, 0x9

    if-ge v4, v6, :cond_3

    const/4 v4, 0x1

    const/4 v6, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v2, v4, v6}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v10

    const/4 v4, 0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2, v4, v6}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v8

    const/4 v4, -0x1

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v2, v4, v6}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v6

    const/4 v4, -0x1

    const/4 v11, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v2, v4, v11}, Lifi;->a(Lifj;ZII)Lifj;

    move-result-object v4

    move-object/from16 v0, p0

    iget v11, v0, Lifi;->e:I

    const/4 v12, 0x2

    if-le v11, v12, :cond_0

    invoke-static {v4, v10}, Lifi;->b(Lifj;Lifj;)F

    move-result v11

    move-object/from16 v0, p0

    iget v12, v0, Lifi;->e:I

    int-to-float v12, v12

    mul-float/2addr v11, v12

    invoke-static {v3, v9}, Lifi;->b(Lifj;Lifj;)F

    move-result v12

    move-object/from16 v0, p0

    iget v13, v0, Lifi;->e:I

    add-int/lit8 v13, v13, 0x2

    int-to-float v13, v13

    mul-float/2addr v12, v13

    div-float/2addr v11, v12

    float-to-double v12, v11

    const-wide/high16 v14, 0x3fe8000000000000L    # 0.75

    cmpg-double v12, v12, v14

    if-ltz v12, :cond_3

    float-to-double v12, v11

    const-wide/high16 v14, 0x3ff4000000000000L    # 1.25

    cmpl-double v11, v12, v14

    if-gtz v11, :cond_3

    new-instance v11, Lifj;

    iget v12, v10, Lifj;->a:I

    add-int/lit8 v12, v12, -0x3

    iget v13, v10, Lifj;->b:I

    add-int/lit8 v13, v13, 0x3

    invoke-direct {v11, v12, v13}, Lifj;-><init>(II)V

    new-instance v12, Lifj;

    iget v13, v8, Lifj;->a:I

    add-int/lit8 v13, v13, -0x3

    iget v14, v8, Lifj;->b:I

    add-int/lit8 v14, v14, -0x3

    invoke-direct {v12, v13, v14}, Lifj;-><init>(II)V

    new-instance v13, Lifj;

    iget v14, v6, Lifj;->a:I

    add-int/lit8 v14, v14, 0x3

    iget v15, v6, Lifj;->b:I

    add-int/lit8 v15, v15, -0x3

    invoke-direct {v13, v14, v15}, Lifj;-><init>(II)V

    new-instance v14, Lifj;

    iget v15, v4, Lifj;->a:I

    add-int/lit8 v15, v15, 0x3

    iget v0, v4, Lifj;->b:I

    move/from16 v16, v0

    add-int/lit8 v16, v16, 0x3

    invoke-direct/range {v14 .. v16}, Lifj;-><init>(II)V

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v11}, Lifi;->a(Lifj;Lifj;)I

    move-result v15

    if-eqz v15, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v12}, Lifi;->a(Lifj;Lifj;)I

    move-result v11

    if-ne v11, v15, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lifi;->a(Lifj;Lifj;)I

    move-result v11

    if-ne v11, v15, :cond_1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lifi;->a(Lifj;Lifj;)I

    move-result v11

    if-ne v11, v15, :cond_1

    const/4 v11, 0x1

    :goto_1
    if-eqz v11, :cond_3

    :cond_0
    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget v3, v0, Lifi;->e:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lifi;->e:I

    move-object v3, v4

    move-object v5, v6

    move-object v7, v8

    move-object v9, v10

    goto/16 :goto_0

    :cond_1
    const/4 v11, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lifi;->e:I

    const/4 v4, 0x5

    if-eq v2, v4, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lifi;->e:I

    const/4 v4, 0x7

    if-eq v2, v4, :cond_4

    invoke-static {}, Liew;->a()Liew;

    move-result-object v2

    throw v2

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lifi;->e:I

    const/4 v4, 0x5

    if-ne v2, v4, :cond_7

    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lifi;->b:Z

    new-instance v2, Lifb;

    iget v4, v9, Lifj;->a:I

    int-to-float v4, v4

    const/high16 v6, 0x3f000000    # 0.5f

    add-float/2addr v4, v6

    iget v6, v9, Lifj;->b:I

    int-to-float v6, v6

    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v6, v8

    invoke-direct {v2, v4, v6}, Lifb;-><init>(FF)V

    new-instance v4, Lifb;

    iget v6, v7, Lifj;->a:I

    int-to-float v6, v6

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v6, v8

    iget v7, v7, Lifj;->b:I

    int-to-float v7, v7

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v7, v8

    invoke-direct {v4, v6, v7}, Lifb;-><init>(FF)V

    new-instance v6, Lifb;

    iget v7, v5, Lifj;->a:I

    int-to-float v7, v7

    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v7, v8

    iget v5, v5, Lifj;->b:I

    int-to-float v5, v5

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v5, v8

    invoke-direct {v6, v7, v5}, Lifb;-><init>(FF)V

    new-instance v5, Lifb;

    iget v7, v3, Lifj;->a:I

    int-to-float v7, v7

    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v7, v8

    iget v3, v3, Lifj;->b:I

    int-to-float v3, v3

    const/high16 v8, 0x3f000000    # 0.5f

    sub-float/2addr v3, v8

    invoke-direct {v5, v7, v3}, Lifb;-><init>(FF)V

    const/4 v3, 0x4

    new-array v3, v3, [Lifb;

    const/4 v7, 0x0

    aput-object v2, v3, v7

    const/4 v2, 0x1

    aput-object v4, v3, v2

    const/4 v2, 0x2

    aput-object v6, v3, v2

    const/4 v2, 0x3

    aput-object v5, v3, v2

    move-object/from16 v0, p0

    iget v2, v0, Lifi;->e:I

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x3

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v4, v0, Lifi;->e:I

    mul-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-static {v3, v2, v4}, Lifi;->a([Lifb;FF)[Lifb;

    move-result-object v22

    .line 70
    if-eqz p1, :cond_5

    .line 71
    const/4 v2, 0x0

    aget-object v2, v22, v2

    .line 72
    const/4 v3, 0x0

    const/4 v4, 0x2

    aget-object v4, v22, v4

    aput-object v4, v22, v3

    .line 73
    const/4 v3, 0x2

    aput-object v2, v22, v3

    .line 77
    :cond_5
    const/4 v2, 0x0

    aget-object v2, v22, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lifi;->a(Lifb;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    aget-object v2, v22, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lifi;->a(Lifb;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x2

    aget-object v2, v22, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lifi;->a(Lifb;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x3

    aget-object v2, v22, v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lifi;->a(Lifb;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    invoke-static {}, Liew;->a()Liew;

    move-result-object v2

    throw v2

    .line 68
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 77
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lifi;->e:I

    mul-int/lit8 v4, v2, 0x2

    const/4 v2, 0x4

    new-array v6, v2, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, v22, v3

    const/4 v5, 0x1

    aget-object v5, v22, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v4}, Lifi;->a(Lifb;Lifb;I)I

    move-result v3

    aput v3, v6, v2

    const/4 v2, 0x1

    const/4 v3, 0x1

    aget-object v3, v22, v3

    const/4 v5, 0x2

    aget-object v5, v22, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v4}, Lifi;->a(Lifb;Lifb;I)I

    move-result v3

    aput v3, v6, v2

    const/4 v2, 0x2

    const/4 v3, 0x2

    aget-object v3, v22, v3

    const/4 v5, 0x3

    aget-object v5, v22, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v4}, Lifi;->a(Lifb;Lifb;I)I

    move-result v3

    aput v3, v6, v2

    const/4 v2, 0x3

    const/4 v3, 0x3

    aget-object v3, v22, v3

    const/4 v5, 0x0

    aget-object v5, v22, v5

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v4}, Lifi;->a(Lifb;Lifb;I)I

    move-result v3

    aput v3, v6, v2

    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_4
    const/4 v5, 0x4

    if-ge v2, v5, :cond_9

    aget v5, v6, v2

    add-int/lit8 v7, v4, -0x2

    shr-int v7, v5, v7

    shl-int/lit8 v7, v7, 0x1

    and-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v7

    shl-int/lit8 v3, v3, 0x3

    add-int/2addr v3, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    and-int/lit8 v2, v3, 0x1

    shl-int/lit8 v2, v2, 0xb

    shr-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v2

    const/4 v2, 0x0

    :goto_5
    const/4 v4, 0x4

    if-ge v2, v4, :cond_b

    sget-object v4, Lifi;->g:[I

    aget v4, v4, v2

    xor-int/2addr v4, v3

    invoke-static {v4}, Ljava/lang/Integer;->bitCount(I)I

    move-result v4

    const/4 v5, 0x2

    if-gt v4, v5, :cond_a

    move-object/from16 v0, p0

    iput v2, v0, Lifi;->f:I

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    move/from16 v23, v2

    move-wide v2, v4

    move/from16 v4, v23

    :goto_6
    const/4 v5, 0x4

    if-ge v4, v5, :cond_d

    move-object/from16 v0, p0

    iget v5, v0, Lifi;->f:I

    add-int/2addr v5, v4

    rem-int/lit8 v5, v5, 0x4

    aget v5, v6, v5

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lifi;->b:Z

    if-eqz v7, :cond_c

    const/4 v7, 0x7

    shl-long/2addr v2, v7

    shr-int/lit8 v5, v5, 0x1

    and-int/lit8 v5, v5, 0x7f

    int-to-long v8, v5

    add-long/2addr v2, v8

    :goto_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    invoke-static {}, Liew;->a()Liew;

    move-result-object v2

    throw v2

    :cond_c
    const/16 v7, 0xa

    shl-long/2addr v2, v7

    shr-int/lit8 v7, v5, 0x2

    and-int/lit16 v7, v7, 0x3e0

    shr-int/lit8 v5, v5, 0x1

    and-int/lit8 v5, v5, 0x1f

    add-int/2addr v5, v7

    int-to-long v8, v5

    add-long/2addr v2, v8

    goto :goto_7

    :cond_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lifi;->b:Z

    invoke-static {v2, v3, v4}, Lifi;->a(JZ)I

    move-result v2

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lifi;->b:Z

    if-eqz v3, :cond_e

    shr-int/lit8 v3, v2, 0x6

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lifi;->c:I

    and-int/lit8 v2, v2, 0x3f

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lifi;->d:I

    .line 80
    :goto_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lifi;->a:Lifx;

    move-object/from16 v0, p0

    iget v2, v0, Lifi;->f:I

    rem-int/lit8 v2, v2, 0x4

    aget-object v5, v22, v2

    move-object/from16 v0, p0

    iget v2, v0, Lifi;->f:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0x4

    aget-object v7, v22, v2

    move-object/from16 v0, p0

    iget v2, v0, Lifi;->f:I

    add-int/lit8 v2, v2, 0x2

    rem-int/lit8 v2, v2, 0x4

    aget-object v9, v22, v2

    move-object/from16 v0, p0

    iget v2, v0, Lifi;->f:I

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v2, v2, 0x4

    aget-object v10, v22, v2

    invoke-static {}, Lige;->a()Lige;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lifi;->b()I

    move-result v4

    int-to-float v6, v4

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v6, v8

    move-object/from16 v0, p0

    iget v8, v0, Lifi;->e:I

    int-to-float v8, v8

    sub-float/2addr v6, v8

    int-to-float v8, v4

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v8, v11

    move-object/from16 v0, p0

    iget v11, v0, Lifi;->e:I

    int-to-float v11, v11

    add-float/2addr v8, v11

    iget v14, v5, Lifb;->a:F

    iget v15, v5, Lifb;->b:F

    iget v0, v7, Lifb;->a:F

    move/from16 v16, v0

    iget v0, v7, Lifb;->b:F

    move/from16 v17, v0

    iget v0, v9, Lifb;->a:F

    move/from16 v18, v0

    iget v0, v9, Lifb;->b:F

    move/from16 v19, v0

    iget v0, v10, Lifb;->a:F

    move/from16 v20, v0

    iget v0, v10, Lifb;->b:F

    move/from16 v21, v0

    move v5, v4

    move v7, v6

    move v9, v6

    move v10, v8

    move v11, v8

    move v12, v6

    move v13, v8

    invoke-virtual/range {v2 .. v21}, Lige;->a(Lifx;IIFFFFFFFFFFFFFFFF)Lifx;

    move-result-object v3

    .line 87
    move-object/from16 v0, p0

    iget v2, v0, Lifi;->e:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-direct/range {p0 .. p0}, Lifi;->b()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, v22

    invoke-static {v0, v2, v4}, Lifi;->a([Lifb;FF)[Lifb;

    move-result-object v4

    .line 89
    new-instance v2, Lifd;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lifi;->b:Z

    move-object/from16 v0, p0

    iget v6, v0, Lifi;->d:I

    move-object/from16 v0, p0

    iget v7, v0, Lifi;->c:I

    invoke-direct/range {v2 .. v7}, Lifd;-><init>(Lifx;[Lifb;ZII)V

    return-object v2

    .line 77
    :cond_e
    shr-int/lit8 v3, v2, 0xb

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lifi;->c:I

    and-int/lit16 v2, v2, 0x7ff

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lifi;->d:I

    goto/16 :goto_8
.end method
