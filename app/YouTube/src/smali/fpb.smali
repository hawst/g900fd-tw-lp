.class public final enum Lfpb;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfpb;

.field public static final enum b:Lfpb;

.field public static final enum c:Lfpb;

.field public static final enum d:Lfpb;

.field private static final synthetic f:[Lfpb;


# instance fields
.field public final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 442
    new-instance v0, Lfpb;

    const-string v1, "ADSENSE"

    const-string v2, "2"

    invoke-direct {v0, v1, v3, v2}, Lfpb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfpb;->a:Lfpb;

    .line 443
    new-instance v0, Lfpb;

    const-string v1, "DOUBLECLICK"

    const-string v2, "1"

    invoke-direct {v0, v1, v4, v2}, Lfpb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfpb;->b:Lfpb;

    .line 444
    new-instance v0, Lfpb;

    const-string v1, "FREEWHEEL"

    const-string v2, "4"

    invoke-direct {v0, v1, v5, v2}, Lfpb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfpb;->c:Lfpb;

    .line 445
    new-instance v0, Lfpb;

    const-string v1, "UNKNOWN"

    const-string v2, "0"

    invoke-direct {v0, v1, v6, v2}, Lfpb;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lfpb;->d:Lfpb;

    .line 441
    const/4 v0, 0x4

    new-array v0, v0, [Lfpb;

    sget-object v1, Lfpb;->a:Lfpb;

    aput-object v1, v0, v3

    sget-object v1, Lfpb;->b:Lfpb;

    aput-object v1, v0, v4

    sget-object v1, Lfpb;->c:Lfpb;

    aput-object v1, v0, v5

    sget-object v1, Lfpb;->d:Lfpb;

    aput-object v1, v0, v6

    sput-object v0, Lfpb;->f:[Lfpb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 450
    iput-object p3, p0, Lfpb;->e:Ljava/lang/String;

    .line 451
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfpb;
    .locals 1

    .prologue
    .line 441
    const-class v0, Lfpb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfpb;

    return-object v0
.end method

.method public static values()[Lfpb;
    .locals 1

    .prologue
    .line 441
    sget-object v0, Lfpb;->f:[Lfpb;

    invoke-virtual {v0}, [Lfpb;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfpb;

    return-object v0
.end method
