.class public final Lbpr;
.super Lbpw;
.source "SourceFile"


# static fields
.field private static final a:Lbpr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1444
    new-instance v0, Lbpr;

    invoke-direct {v0}, Lbpr;-><init>()V

    sput-object v0, Lbpr;->a:Lbpr;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1441
    invoke-direct {p0}, Lbpw;-><init>()V

    return-void
.end method

.method public static synthetic a()Lbpr;
    .locals 1

    .prologue
    .line 1441
    sget-object v0, Lbpr;->a:Lbpr;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Lfkq;
    .locals 3

    .prologue
    .line 1441
    check-cast p1, Lfje;

    iget-object v0, p1, Lfje;->g:Lfkp;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->j:Lhnh;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->j:Lhnh;

    iget-object v0, v0, Lhnh;->a:Lhne;

    if-eqz v0, :cond_0

    new-instance v0, Lfkp;

    iget-object v1, p1, Lfje;->a:Lhdz;

    iget-object v1, v1, Lhdz;->j:Lhnh;

    iget-object v1, v1, Lhnh;->a:Lhne;

    invoke-direct {v0, v1, p1}, Lfkp;-><init>(Lhne;Lfqh;)V

    iput-object v0, p1, Lfje;->g:Lfkp;

    :cond_0
    iget-object v0, p1, Lfje;->g:Lfkp;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lfkp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkq;

    iget-object v2, v0, Lfkq;->a:Lhut;

    if-eqz v2, :cond_1

    iget-object v2, v2, Lhut;->g:Lhgq;

    if-eqz v2, :cond_1

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic b(Ljava/lang/Object;)Lfkn;
    .locals 2

    .prologue
    .line 1441
    check-cast p1, Lfje;

    iget-object v0, p1, Lfje;->e:Lfkn;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->h:Lhll;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->h:Lhll;

    iget-object v0, v0, Lhll;->a:Lhlk;

    if-eqz v0, :cond_0

    new-instance v0, Lfkn;

    iget-object v1, p1, Lfje;->a:Lhdz;

    iget-object v1, v1, Lhdz;->h:Lhll;

    iget-object v1, v1, Lhll;->a:Lhlk;

    invoke-direct {v0, v1}, Lfkn;-><init>(Lhlk;)V

    iput-object v0, p1, Lfje;->e:Lfkn;

    :cond_0
    iget-object v0, p1, Lfje;->e:Lfkn;

    return-object v0
.end method

.method public final synthetic d(Ljava/lang/Object;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1441
    check-cast p1, Lfje;

    iget-object v0, p1, Lfje;->f:Landroid/net/Uri;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->i:Ljava/lang/String;

    invoke-static {v0}, La;->K(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p1, Lfje;->f:Landroid/net/Uri;

    :cond_0
    iget-object v0, p1, Lfje;->f:Landroid/net/Uri;

    return-object v0
.end method

.method public final synthetic e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1441
    check-cast p1, Lfje;

    iget-object v0, p1, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic f(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1441
    check-cast p1, Lfje;

    invoke-virtual {p1}, Lfje;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic g(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1441
    check-cast p1, Lfje;

    iget-object v0, p1, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->a:Ljava/lang/String;

    return-object v0
.end method
