.class public Lfvh;
.super Lfug;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lezl;
.implements Lfve;


# instance fields
.field public final c:Landroid/widget/ListView;

.field public final d:Ljava/util/Set;

.field public e:Lfsf;

.field private final f:Lfvd;

.field private final g:Lfsc;

.field private final h:Ljava/util/List;

.field private final i:Lfuw;

.field private final j:Levn;

.field private final k:Lfsl;

.field private n:Lfvc;

.field private o:Z

.field private p:I


# direct methods
.method public constructor <init>(Landroid/widget/ListView;Lfsc;Lfvd;Lfdg;Levn;Lfuw;Leyt;Lfsl;)V
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Levn;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, p4, p5, v0, p7}, Lfug;-><init>(Lfdg;Levn;Ljava/lang/Object;Leyt;)V

    .line 108
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    .line 109
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lfvh;->j:Levn;

    .line 110
    iput-object p3, p0, Lfvh;->f:Lfvd;

    .line 111
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfuw;

    iput-object v0, p0, Lfvh;->i:Lfuw;

    .line 112
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfvh;->h:Ljava/util/List;

    .line 113
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfvh;->d:Ljava/util/Set;

    .line 114
    iput-object p8, p0, Lfvh;->k:Lfsl;

    .line 116
    iput-object p2, p0, Lfvh;->g:Lfsc;

    .line 117
    if-eqz p8, :cond_0

    .line 118
    invoke-virtual {p1, p8}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 121
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lfvh;->p:I

    .line 122
    const-class v0, Lfvh;

    invoke-virtual {p5, p0, v0}, Levn;->a(Ljava/lang/Object;Ljava/lang/Class;)V

    .line 123
    return-void
.end method

.method public constructor <init>(Landroid/widget/ListView;Lfvd;Lfdg;Levn;Lfuw;Leyt;)V
    .locals 9

    .prologue
    .line 68
    new-instance v2, Lfsc;

    invoke-direct {v2}, Lfsc;-><init>()V

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lfvh;-><init>(Landroid/widget/ListView;Lfsc;Lfvd;Lfdg;Levn;Lfuw;Leyt;Lfsl;)V

    .line 77
    return-void
.end method

.method private c(Lfmi;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 310
    if-nez p1, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    iget-object v0, p0, Lfvh;->f:Lfvd;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0}, Lfsc;->a()I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v2, p0, Lfvh;->g:Lfsc;

    iget-object v0, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0}, Lfsc;->a()I

    move-result v3

    iget-object v0, v2, Lfsc;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfse;

    iget-object v0, v0, Lfse;->a:Lfsf;

    iget-object v4, v2, Lfsc;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v4}, Lfsf;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, v2, Lfsc;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 315
    :cond_2
    iget-object v0, p1, Lfmi;->b:Ljava/util/List;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p1, Lfmi;->a:Lhul;

    iget-object v2, v2, Lhul;->b:[Lhum;

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p1, Lfmi;->b:Ljava/util/List;

    iget-object v0, p1, Lfmi;->a:Lhul;

    iget-object v2, v0, Lhul;->b:[Lhum;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_5

    aget-object v4, v2, v0

    iget-object v5, v4, Lhum;->b:Lhoj;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lfmi;->b:Ljava/util/List;

    new-instance v6, Lfjk;

    iget-object v4, v4, Lhum;->b:Lhoj;

    invoke-direct {v6, v4}, Lfjk;-><init>(Lhoj;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v5, v4, Lhum;->c:Lhtn;

    if-eqz v5, :cond_3

    iget-object v5, p1, Lfmi;->b:Ljava/util/List;

    new-instance v6, Lfjk;

    iget-object v4, v4, Lhum;->c:Lhtn;

    invoke-direct {v6, v4}, Lfjk;-><init>(Lhtn;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    iget-object v0, p1, Lfmi;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lfvh;->b(Ljava/util/List;)V

    .line 316
    iget-object v0, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0}, Lfsc;->getViewTypeCount()I

    move-result v2

    .line 318
    invoke-virtual {p1}, Lfmi;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_6
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 319
    iget-object v0, p0, Lfvh;->i:Lfuw;

    .line 320
    invoke-virtual {v0, v4}, Lfuw;->a(Ljava/lang/Object;)Lfuv;

    move-result-object v0

    .line 321
    if-eqz v0, :cond_7

    .line 324
    iget-object v4, p0, Lfvh;->h:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    iget-object v4, p0, Lfvh;->g:Lfsc;

    invoke-interface {v0}, Lfuv;->b()Lfsi;

    move-result-object v5

    invoke-virtual {v4, v5}, Lfsc;->a(Lfsf;)V

    .line 326
    sget-object v4, Lfjl;->a:Lfjl;

    invoke-virtual {p0, v4}, Lfvh;->b(Lfjl;)Z

    move-result v4

    if-nez v4, :cond_6

    instance-of v4, v0, Lftx;

    if-eqz v4, :cond_6

    .line 330
    check-cast v0, Lftx;

    iput-object v0, p0, Lfug;->a:Lftx;

    goto :goto_3

    .line 334
    :cond_7
    instance-of v0, v4, Lhdo;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfvh;->e:Lfsf;

    if-eqz v0, :cond_6

    .line 335
    iget-object v0, p0, Lfvh;->g:Lfsc;

    iget-object v4, p0, Lfvh;->e:Lfsf;

    invoke-virtual {v0, v4}, Lfsc;->a(Lfsf;)V

    goto :goto_3

    .line 340
    :cond_8
    iget-object v0, p0, Lfvh;->f:Lfvd;

    if-eqz v0, :cond_b

    sget-object v0, Lfjl;->a:Lfjl;

    invoke-virtual {p0, v0}, Lfvh;->b(Lfjl;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Lfjl;->b:Lfjl;

    invoke-virtual {p0, v0}, Lfvh;->b(Lfjl;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lfvh;->n:Lfvc;

    if-eqz v0, :cond_b

    :cond_9
    iget-object v0, p0, Lfvh;->n:Lfvc;

    if-nez v0, :cond_a

    new-instance v0, Lfvc;

    iget-object v3, p0, Lfug;->b:Ljava/lang/Object;

    invoke-direct {v0, v3, p0, p0}, Lfvc;-><init>(Ljava/lang/Object;Landroid/view/View$OnClickListener;Lfve;)V

    iput-object v0, p0, Lfvh;->n:Lfvc;

    :cond_a
    iget-object v0, p0, Lfvh;->g:Lfsc;

    iget-object v3, p0, Lfvh;->f:Lfvd;

    invoke-virtual {v0, v3}, Lfsc;->a(Lfsf;)V

    .line 345
    :cond_b
    iget-object v0, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0}, Lfsc;->getViewTypeCount()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 346
    iget-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v2

    iget-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_4
    iget-object v1, p0, Lfvh;->c:Landroid/widget/ListView;

    iget-object v3, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lfvh;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto :goto_4
.end method


# virtual methods
.method public final a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lfvh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfuv;

    .line 190
    invoke-interface {v0}, Lfuv;->a()V

    goto :goto_0

    .line 192
    :cond_0
    return-void
.end method

.method public final a(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lfvh;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 197
    return-void
.end method

.method public final a(Lfmi;)V
    .locals 2

    .prologue
    .line 237
    invoke-virtual {p0}, Lfvh;->h()V

    .line 238
    invoke-direct {p0, p1}, Lfvh;->c(Lfmi;)V

    .line 240
    iget-object v0, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0}, Lfsc;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 241
    iget-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 243
    :cond_0
    return-void
.end method

.method protected final a(Lhel;Lfjl;)V
    .locals 2

    .prologue
    .line 261
    invoke-super {p0, p1, p2}, Lfug;->a(Lhel;Lfjl;)V

    .line 262
    if-eqz p1, :cond_0

    iget-object v0, p1, Lhel;->b:Lhul;

    if-nez v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    new-instance v0, Lfmi;

    iget-object v1, p1, Lhel;->b:Lhul;

    invoke-direct {v0, v1}, Lfmi;-><init>(Lhul;)V

    .line 266
    sget-object v1, Lfjl;->b:Lfjl;

    if-ne p2, v1, :cond_2

    .line 269
    invoke-virtual {p0, v0}, Lfvh;->a(Lfmi;)V

    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfvh;->o:Z

    .line 274
    iget-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0

    .line 276
    :cond_2
    invoke-direct {p0, v0}, Lfvh;->c(Lfmi;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 176
    iget-boolean v0, p0, Lfvh;->o:Z

    if-eqz v0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfvh;->o:Z

    .line 181
    iget-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 182
    iget-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 183
    iget-object v0, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0}, Lfsc;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lfjl;->b:Lfjl;

    invoke-virtual {p0, v0}, Lfvh;->b(Lfjl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    sget-object v0, Lfjl;->b:Lfjl;

    invoke-virtual {p0, v0}, Lfvh;->a(Lfjl;)V

    goto :goto_0
.end method

.method public final b(Lfmi;)V
    .locals 0

    .prologue
    .line 250
    invoke-virtual {p0, p1}, Lfvh;->a(Lfmi;)V

    .line 251
    invoke-virtual {p0}, Lfvh;->b()V

    .line 252
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 438
    invoke-virtual {p0}, Lfvh;->h()V

    .line 439
    iget-object v0, p0, Lfvh;->j:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 440
    iget-object v0, p0, Lfvh;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 441
    iget-object v0, p0, Lfvh;->k:Lfsl;

    if-eqz v0, :cond_1

    .line 442
    iget-object v0, p0, Lfvh;->k:Lfsl;

    iget-object v1, p0, Lfvh;->c:Landroid/widget/ListView;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lfsl;->b:Ljava/util/LinkedList;

    if-nez v2, :cond_0

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, v0, Lfsl;->b:Ljava/util/LinkedList;

    :cond_0
    iget-object v2, v0, Lfsl;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->reclaimViews(Ljava/util/List;)V

    iget-object v1, v0, Lfsl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Lfsl;->a(Ljava/util/Collection;)V

    iget-object v0, v0, Lfsl;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 444
    :cond_1
    return-void
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 216
    const/4 v0, -0x1

    iput v0, p0, Lfvh;->p:I

    .line 217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfvh;->o:Z

    .line 218
    iget-object v1, p0, Lfvh;->g:Lfsc;

    iget-object v0, v1, Lfsc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfse;

    iget-object v0, v0, Lfse;->a:Lfsf;

    iget-object v3, v1, Lfsc;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v3}, Lfsf;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lfsc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, v1, Lfsc;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->clear()V

    invoke-virtual {v1}, Lfsc;->b()V

    invoke-virtual {v1}, Lfsc;->notifyDataSetChanged()V

    .line 221
    iget-object v0, p0, Lfvh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfuv;

    .line 222
    invoke-interface {v0}, Lfuv;->d()V

    goto :goto_1

    .line 224
    :cond_1
    iget-object v0, p0, Lfvh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 226
    iget-object v0, p0, Lfvh;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 227
    iput-object v4, p0, Lfug;->a:Lftx;

    .line 228
    invoke-virtual {p0}, Lfvh;->e()V

    .line 229
    return-void
.end method

.method public handleContentEvent(Lfua;)V
    .locals 2

    .prologue
    .line 414
    iget-object v0, p0, Lfvh;->f:Lfvd;

    if-nez v0, :cond_0

    .line 419
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v0, p0, Lfvh;->n:Lfvc;

    iput-object p1, v0, Lfvc;->d:Lbt;

    .line 418
    iget-object v0, p0, Lfvh;->f:Lfvd;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lfvd;->a(Lfvc;)V

    goto :goto_0
.end method

.method public handleErrorEvent(Lfub;)V
    .locals 2

    .prologue
    .line 405
    iget-object v0, p0, Lfvh;->f:Lfvd;

    if-nez v0, :cond_0

    .line 410
    :goto_0
    return-void

    .line 408
    :cond_0
    iget-object v0, p0, Lfvh;->n:Lfvc;

    iput-object p1, v0, Lfvc;->d:Lbt;

    .line 409
    iget-object v0, p0, Lfvh;->f:Lfvd;

    iget-object v1, p0, Lfvh;->n:Lfvc;

    invoke-interface {v0, v1}, Lfvd;->a(Lfvc;)V

    goto :goto_0
.end method

.method public handleItemDismissedEvent(Lfux;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 433
    iget-object v0, p0, Lfvh;->g:Lfsc;

    iget-object v1, p1, Lfux;->a:Ljava/lang/Object;

    iget-object v0, v0, Lfsc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfse;

    iget-object v0, v0, Lfse;->a:Lfsf;

    invoke-interface {v0, v1}, Lfsf;->a(Ljava/lang/Object;)Z

    goto :goto_0

    .line 434
    :cond_0
    return-void
.end method

.method public handleLoadingEvent(Lfuc;)V
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lfvh;->f:Lfvd;

    if-nez v0, :cond_0

    .line 401
    :goto_0
    return-void

    .line 399
    :cond_0
    iget-object v0, p0, Lfvh;->n:Lfvc;

    iput-object p1, v0, Lfvc;->d:Lbt;

    .line 400
    iget-object v0, p0, Lfvh;->f:Lfvd;

    iget-object v1, p0, Lfvh;->n:Lfvc;

    invoke-interface {v0, v1}, Lfvd;->a(Lfvc;)V

    goto :goto_0
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 428
    invoke-virtual {p0}, Lfvh;->g()V

    .line 429
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 423
    sget-object v0, Lfjl;->a:Lfjl;

    invoke-virtual {p0, v0}, Lfvh;->a(Lfjl;)V

    .line 424
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 283
    add-int v0, p2, p3

    .line 289
    if-ne v0, p4, :cond_0

    iget v0, p0, Lfvh;->p:I

    iget-object v1, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v1}, Lfsc;->getCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 290
    iget-object v0, p0, Lfvh;->g:Lfsc;

    invoke-virtual {v0}, Lfsc;->getCount()I

    move-result v0

    iput v0, p0, Lfvh;->p:I

    .line 291
    sget-object v0, Lfjl;->a:Lfjl;

    invoke-virtual {p0, v0}, Lfvh;->a(Lfjl;)V

    .line 293
    :cond_0
    iget-object v0, p0, Lfvh;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 294
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0

    .line 296
    :cond_1
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lfvh;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 301
    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 303
    :cond_0
    return-void
.end method
