.class public final Ldry;
.super Ldrr;
.source "SourceFile"


# instance fields
.field private final o:Ljava/util/Map;

.field private final p:Ljava/util/concurrent/Executor;

.field private final q:Landroid/content/Context;

.field private final r:Levn;

.field private final s:Ldrj;

.field private final t:Ldyg;

.field private u:Ldri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Levn;Ldwv;Ldxe;Ldwq;Ljava/util/concurrent/Executor;Ldrj;Ldyg;)V
    .locals 8

    .prologue
    .line 58
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    move-object v6, p5

    move-object/from16 v7, p9

    invoke-direct/range {v0 .. v7}, Ldrr;-><init>(Landroid/content/Context;Levn;Levn;Ldwv;Ldwq;Ldxe;Ldyg;)V

    .line 66
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldry;->q:Landroid/content/Context;

    .line 67
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldry;->p:Ljava/util/concurrent/Executor;

    .line 68
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldry;->r:Levn;

    .line 69
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldrj;

    iput-object v0, p0, Ldry;->s:Ldrj;

    .line 70
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldry;->t:Ldyg;

    .line 71
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ldry;->o:Ljava/util/Map;

    .line 72
    return-void
.end method

.method private c(Ljava/lang/String;)Ldtd;
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Ldry;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Ldry;->o:Ljava/util/Map;

    new-instance v1, Ldtd;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ldtd;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    :cond_0
    iget-object v0, p0, Ldry;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtd;

    return-object v0
.end method


# virtual methods
.method protected final a(Ldwr;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 107
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 108
    const-string v1, "pairingCode"

    invoke-static {p1}, Ldry;->b(Ldwr;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Ldry;->c(Ljava/lang/String;)Ldtd;

    move-result-object v2

    invoke-virtual {v2}, Ldtd;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lrq;
    .locals 11

    .prologue
    .line 76
    new-instance v0, Ldrz;

    invoke-virtual {p0, p1}, Ldry;->b(Ljava/lang/String;)Ldwr;

    move-result-object v1

    .line 77
    invoke-direct {p0, p1}, Ldry;->c(Ljava/lang/String;)Ldtd;

    move-result-object v2

    iget-object v3, p0, Ldry;->q:Landroid/content/Context;

    iget-object v4, p0, Ldry;->k:Ldwq;

    iget-object v5, p0, Ldry;->j:Ldwv;

    iget-object v6, p0, Ldry;->l:Ldxe;

    iget-object v7, p0, Ldry;->p:Ljava/util/concurrent/Executor;

    iget-object v8, p0, Ldry;->r:Levn;

    .line 84
    iget-object v9, p0, Ldry;->u:Ldri;

    if-nez v9, :cond_0

    new-instance v9, Ldrk;

    iget-object v10, p0, Ldry;->s:Ldrj;

    invoke-direct {v9, v10}, Ldrk;-><init>(Ldrj;)V

    iput-object v9, p0, Ldry;->u:Ldri;

    :cond_0
    iget-object v9, p0, Ldry;->u:Ldri;

    iget-object v10, p0, Ldry;->t:Ldyg;

    invoke-direct/range {v0 .. v10}, Ldrz;-><init>(Ldwr;Ldtd;Landroid/content/Context;Ldwq;Ldwv;Ldxe;Ljava/util/concurrent/Executor;Levn;Ldri;Ldyg;)V

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string v0, "EXTERNAL_MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    return-object v0
.end method

.method public final c(Ldwr;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Ldry;->o:Ljava/util/Map;

    invoke-static {p1}, Ldry;->b(Ldwr;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void
.end method

.method protected final c()Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    const-string v0, "YouTube"

    return-object v0
.end method
