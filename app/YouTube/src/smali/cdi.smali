.class public final Lcdi;
.super Lfsa;
.source "SourceFile"


# instance fields
.field private final a:Lfsj;

.field private final b:Landroid/widget/TextView;

.field private final c:Lbnv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 3

    .prologue
    .line 40
    invoke-direct {p0, p4, p5}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 42
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcdi;->a:Lfsj;

    .line 43
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 44
    const v1, 0x7f040043

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 45
    const v0, 0x7f08013e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcdi;->b:Landroid/widget/TextView;

    .line 46
    const v0, 0x7f08013f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    new-instance v2, Lbnv;

    invoke-direct {v2, p3, v0}, Lbnv;-><init>(Lfhz;Landroid/widget/TextView;)V

    iput-object v2, p0, Lcdi;->c:Lbnv;

    .line 49
    invoke-interface {p2, v1}, Lfsj;->a(Landroid/view/View;)V

    .line 50
    return-void
.end method

.method private a(Lfsg;Lfjg;)Landroid/view/View;
    .locals 3

    .prologue
    .line 54
    invoke-super {p0, p1, p2}, Lfsa;->a(Lfsg;Lfqh;)Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcdi;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lfjg;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    iget-object v1, p2, Lfjg;->a:Lhdu;

    iget-object v1, v1, Lhdu;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjg;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, p2, Lfjg;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lcdi;->c:Lbnv;

    iget-object v1, p2, Lfjg;->c:Lfit;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfjg;->a:Lhdu;

    iget-object v1, v1, Lhdu;->b:Lhbt;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lfjg;->a:Lhdu;

    iget-object v1, v1, Lhdu;->b:Lhbt;

    iget-object v1, v1, Lhbt;->a:Lhbs;

    if-eqz v1, :cond_1

    new-instance v1, Lfit;

    iget-object v2, p2, Lfjg;->a:Lhdu;

    iget-object v2, v2, Lhdu;->b:Lhbt;

    iget-object v2, v2, Lhbt;->a:Lhbs;

    invoke-direct {v1, v2}, Lfit;-><init>(Lhbs;)V

    iput-object v1, p2, Lfjg;->c:Lfit;

    :cond_1
    iget-object v1, p2, Lfjg;->c:Lfit;

    invoke-virtual {v0, v1}, Lbnv;->a(Lfit;)V

    .line 57
    iget-object v0, p0, Lcdi;->a:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Lfjg;

    invoke-direct {p0, p1, p2}, Lcdi;->a(Lfsg;Lfjg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Lfjg;

    invoke-direct {p0, p1, p2}, Lcdi;->a(Lfsg;Lfjg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
