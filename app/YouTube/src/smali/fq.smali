.class final Lfq;
.super Landroid/database/DataSetObserver;
.source "SourceFile"

# interfaces
.implements Lhd;
.implements Lhe;


# instance fields
.field private a:I

.field private synthetic b:Lfp;


# direct methods
.method constructor <init>(Lfp;)V
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Lfq;->b:Lfp;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 485
    iget v1, p0, Lfq;->a:I

    if-nez v1, :cond_1

    .line 487
    iget-object v1, p0, Lfq;->b:Lfp;

    iget-object v2, p0, Lfq;->b:Lfp;

    iget-object v2, v2, Lfp;->a:Landroid/support/v4/view/ViewPager;

    iget v2, v2, Landroid/support/v4/view/ViewPager;->b:I

    iget-object v3, p0, Lfq;->b:Lfp;

    iget-object v3, v3, Lfp;->a:Landroid/support/v4/view/ViewPager;

    iget-object v3, v3, Landroid/support/v4/view/ViewPager;->a:Lfm;

    invoke-virtual {v1, v2, v3}, Lfp;->a(ILfm;)V

    .line 489
    iget-object v1, p0, Lfq;->b:Lfp;

    invoke-static {v1}, Lfp;->a(Lfp;)F

    move-result v1

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    iget-object v0, p0, Lfq;->b:Lfp;

    invoke-static {v0}, Lfp;->a(Lfp;)F

    move-result v0

    .line 490
    :cond_0
    iget-object v1, p0, Lfq;->b:Lfp;

    iget-object v2, p0, Lfq;->b:Lfp;

    iget-object v2, v2, Lfp;->a:Landroid/support/v4/view/ViewPager;

    iget v2, v2, Landroid/support/v4/view/ViewPager;->b:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Lfp;->a(IFZ)V

    .line 492
    :cond_1
    return-void
.end method

.method public final a(IF)V
    .locals 2

    .prologue
    .line 476
    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 478
    add-int/lit8 p1, p1, 0x1

    .line 480
    :cond_0
    iget-object v0, p0, Lfq;->b:Lfp;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lfp;->a(IFZ)V

    .line 481
    return-void
.end method

.method public final a(Lfm;Lfm;)V
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lfq;->b:Lfp;

    invoke-virtual {v0, p1, p2}, Lfp;->a(Lfm;Lfm;)V

    .line 502
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 496
    iput p1, p0, Lfq;->a:I

    .line 497
    return-void
.end method

.method public final onChanged()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 506
    iget-object v1, p0, Lfq;->b:Lfp;

    iget-object v2, p0, Lfq;->b:Lfp;

    iget-object v2, v2, Lfp;->a:Landroid/support/v4/view/ViewPager;

    iget v2, v2, Landroid/support/v4/view/ViewPager;->b:I

    iget-object v3, p0, Lfq;->b:Lfp;

    iget-object v3, v3, Lfp;->a:Landroid/support/v4/view/ViewPager;

    iget-object v3, v3, Landroid/support/v4/view/ViewPager;->a:Lfm;

    invoke-virtual {v1, v2, v3}, Lfp;->a(ILfm;)V

    .line 508
    iget-object v1, p0, Lfq;->b:Lfp;

    invoke-static {v1}, Lfp;->a(Lfp;)F

    move-result v1

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_0

    iget-object v0, p0, Lfq;->b:Lfp;

    invoke-static {v0}, Lfp;->a(Lfp;)F

    move-result v0

    .line 509
    :cond_0
    iget-object v1, p0, Lfq;->b:Lfp;

    iget-object v2, p0, Lfq;->b:Lfp;

    iget-object v2, v2, Lfp;->a:Landroid/support/v4/view/ViewPager;

    iget v2, v2, Landroid/support/v4/view/ViewPager;->b:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Lfp;->a(IFZ)V

    .line 510
    return-void
.end method
