.class final Lbvt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbxb;


# instance fields
.field private synthetic a:Lbvn;


# direct methods
.method constructor <init>(Lbvn;)V
    .locals 0

    .prologue
    .line 744
    iput-object p1, p0, Lbvt;->a:Lbvn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 747
    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v2

    if-nez v2, :cond_0

    .line 806
    :goto_0
    return-void

    .line 751
    :cond_0
    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v2

    invoke-interface {v2}, Ldwq;->i()Ldww;

    move-result-object v2

    sget-object v3, Ldww;->f:Ldww;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lbvt;->a:Lbvn;

    .line 752
    invoke-static {v2}, Lbvn;->f(Lbvn;)Ldwo;

    move-result-object v2

    sget-object v3, Ldwo;->j:Ldwo;

    if-ne v2, v3, :cond_3

    :cond_1
    move v2, v0

    .line 753
    :goto_1
    if-eqz v2, :cond_7

    .line 754
    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->f(Lbvn;)Ldwo;

    move-result-object v2

    sget-object v3, Ldwo;->j:Ldwo;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lbvt;->a:Lbvn;

    .line 755
    invoke-static {v2}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v2

    invoke-interface {v2}, Ldwq;->j()Ldwh;

    move-result-object v2

    iget-boolean v2, v2, Ldwh;->k:Z

    if-eqz v2, :cond_4

    .line 756
    :cond_2
    :goto_2
    if-eqz v0, :cond_6

    .line 757
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->k(Lbvn;)Lfrl;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 758
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v0

    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->k(Lbvn;)Lfrl;

    move-result-object v2

    iget-object v2, v2, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4, v1}, Ldwq;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    move v2, v1

    .line 752
    goto :goto_1

    :cond_4
    move v0, v1

    .line 755
    goto :goto_2

    .line 761
    :cond_5
    const-string v0, "Should be flinging a video, but it\'s not loaded yet"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 764
    :cond_6
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->l(Lbvn;)Lbvw;

    move-result-object v0

    invoke-interface {v0}, Lbvw;->v()V

    goto :goto_0

    .line 766
    :cond_7
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->k(Lbvn;)Lfrl;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->m(Lbvn;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 778
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->l(Lbvn;)Lbvw;

    move-result-object v0

    invoke-interface {v0}, Lbvw;->v()V

    goto/16 :goto_0

    .line 780
    :cond_8
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v2, Ldww;->b:Ldww;

    if-eq v0, v2, :cond_9

    .line 781
    iget-object v0, p0, Lbvt;->a:Lbvn;

    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v2

    invoke-interface {v2}, Ldwq;->i()Ldww;

    move-result-object v2

    invoke-static {v0, v2}, Lbvn;->a(Lbvn;Ldww;)V

    .line 783
    :cond_9
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->e(Lbvn;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 784
    iget-object v0, p0, Lbvt;->a:Lbvn;

    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v2

    invoke-interface {v2}, Ldwq;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lbvn;->a(Lbvn;Ljava/lang/String;)V

    .line 787
    :cond_a
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->m(Lbvn;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 788
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v0

    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->k(Lbvn;)Lfrl;

    move-result-object v2

    iget-object v2, v2, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v4, v1}, Ldwq;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 790
    :cond_b
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->f(Lbvn;)Ldwo;

    move-result-object v0

    .line 791
    sget-object v2, Ldwo;->i:Ldwo;

    if-eq v0, v2, :cond_c

    sget-object v2, Ldwo;->a:Ldwo;

    if-eq v0, v2, :cond_c

    sget-object v2, Ldwo;->e:Ldwo;

    if-ne v0, v2, :cond_e

    .line 794
    :cond_c
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->h(Lbvn;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 795
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->c()V

    .line 796
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0, v1}, Lbvn;->a(Lbvn;Z)Z

    goto/16 :goto_0

    .line 798
    :cond_d
    iget-object v0, p0, Lbvt;->a:Lbvn;

    invoke-static {v0}, Lbvn;->l(Lbvn;)Lbvw;

    move-result-object v0

    invoke-interface {v0}, Lbvw;->v()V

    goto/16 :goto_0

    .line 801
    :cond_e
    iget-object v1, p0, Lbvt;->a:Lbvn;

    iget-object v2, p0, Lbvt;->a:Lbvn;

    invoke-static {v2}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v2

    invoke-interface {v2}, Ldwq;->k()Ldxb;

    move-result-object v2

    iget-object v3, p0, Lbvt;->a:Lbvn;

    invoke-static {v3}, Lbvn;->g(Lbvn;)Ldwq;

    move-result-object v3

    invoke-interface {v3}, Ldwq;->t()Lfoy;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lbvn;->a(Lbvn;Ldxb;Lfoy;)V

    .line 802
    iget-object v1, p0, Lbvt;->a:Lbvn;

    invoke-static {v1, v0}, Lbvn;->a(Lbvn;Ldwo;)V

    goto/16 :goto_0
.end method
