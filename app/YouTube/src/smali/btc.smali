.class public final Lbtc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Lcyc;

.field public final c:Lgnd;

.field final d:Lbjq;

.field public final e:Leyp;

.field public final f:Levn;

.field public final g:Lfus;

.field public final h:Lexd;

.field public final i:Lbjx;

.field final j:Lbtf;

.field public k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field public l:Landroid/widget/ListView;

.field public m:Lfsi;

.field public n:Landroid/widget/LinearLayout;

.field public o:Landroid/view/View;

.field final p:Ljava/util/HashSet;

.field private final q:Lfhz;

.field private final r:Lfdw;

.field private final s:Lfrz;

.field private final t:Lfjx;

.field private u:Leue;

.field private v:Lcdu;

.field private w:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcyc;Lgnd;Lbjq;Leyp;Levn;Lfus;Lexd;Lbjx;Lbtf;Lfhz;Lfdw;Lfrz;Lfjx;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbtc;->a:Landroid/app/Activity;

    .line 97
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyc;

    iput-object v0, p0, Lbtc;->b:Lcyc;

    .line 98
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnd;

    iput-object v0, p0, Lbtc;->c:Lgnd;

    .line 100
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjq;

    iput-object v0, p0, Lbtc;->d:Lbjq;

    .line 101
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbtc;->e:Leyp;

    .line 102
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lbtc;->f:Levn;

    .line 103
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfus;

    iput-object v0, p0, Lbtc;->g:Lfus;

    .line 104
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lbtc;->h:Lexd;

    .line 105
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lbtc;->i:Lbjx;

    .line 106
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtf;

    iput-object v0, p0, Lbtc;->j:Lbtf;

    .line 107
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lbtc;->q:Lfhz;

    .line 108
    invoke-static {p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lbtc;->r:Lfdw;

    .line 110
    invoke-static {p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lbtc;->s:Lfrz;

    .line 111
    iput-object p14, p0, Lbtc;->t:Lfjx;

    .line 112
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbtc;->p:Ljava/util/HashSet;

    .line 113
    return-void
.end method

.method private handleOfflinePlaylistAddEvent(Lblq;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 184
    iget-object v0, p1, Lblq;->a:Lglw;

    iget-object v0, v0, Lglw;->a:Lgbu;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    .line 185
    iget-object v1, p0, Lbtc;->p:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lbtc;->a()V

    .line 188
    :cond_0
    return-void
.end method

.method private handleOfflinePlaylistDeleteEvent(Lblp;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lbtc;->m:Lfsi;

    new-instance v1, Lbte;

    invoke-direct {v1, p0, p1}, Lbte;-><init>(Lbtc;Lblp;)V

    invoke-virtual {v0, v1}, Lfsi;->a(Lewh;)V

    .line 200
    invoke-virtual {p0}, Lbtc;->a()V

    .line 201
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lbtc;->u:Leue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbtc;->u:Leue;

    iget-boolean v0, v0, Leue;->a:Z

    if-nez v0, :cond_0

    .line 205
    iget-object v0, p0, Lbtc;->u:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 207
    :cond_0
    iget-object v0, p0, Lbtc;->k:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 208
    new-instance v0, Lbtg;

    invoke-direct {v0, p0}, Lbtg;-><init>(Lbtc;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbtc;->u:Leue;

    .line 209
    iget-object v0, p0, Lbtc;->c:Lgnd;

    iget-object v1, p0, Lbtc;->a:Landroid/app/Activity;

    iget-object v2, p0, Lbtc;->u:Leue;

    invoke-static {v1, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v1

    invoke-interface {v0, v1}, Lgnd;->b(Leuc;)V

    .line 210
    return-void
.end method

.method public b()V
    .locals 7

    .prologue
    .line 217
    iget-object v0, p0, Lbtc;->t:Lfjx;

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lbtc;->v:Lcdu;

    if-nez v0, :cond_0

    .line 219
    new-instance v0, Lcdu;

    iget-object v1, p0, Lbtc;->a:Landroid/app/Activity;

    iget-object v2, p0, Lbtc;->e:Leyp;

    iget-object v3, p0, Lbtc;->f:Levn;

    iget-object v4, p0, Lbtc;->q:Lfhz;

    iget-object v5, p0, Lbtc;->r:Lfdw;

    iget-object v6, p0, Lbtc;->s:Lfrz;

    invoke-direct/range {v0 .. v6}, Lcdu;-><init>(Landroid/content/Context;Leyp;Levn;Lfhz;Lfdw;Lfrz;)V

    iput-object v0, p0, Lbtc;->v:Lcdu;

    .line 227
    :cond_0
    iget-object v0, p0, Lbtc;->w:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lbtc;->n:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbtc;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 230
    :cond_1
    iget-object v0, p0, Lbtc;->v:Lcdu;

    new-instance v1, Lfsg;

    invoke-direct {v1}, Lfsg;-><init>()V

    iget-object v2, p0, Lbtc;->t:Lfjx;

    invoke-virtual {v0, v1, v2}, Lcdu;->a(Lfsg;Lfjx;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbtc;->w:Landroid/view/View;

    .line 231
    iget-object v0, p0, Lbtc;->n:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lbtc;->w:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 233
    :cond_2
    return-void
.end method
