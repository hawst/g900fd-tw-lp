.class public final Lgen;
.super Lgeu;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lgec;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lgeu;-><init>(Landroid/content/Context;)V

    .line 23
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 24
    iget-object v0, p0, Lgen;->a:Lgey;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lgey;->setSecure(Z)V

    return-void

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
