.class public final Lbgm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lfcd;

.field private final b:Lgix;

.field private final c:Lbcc;


# direct methods
.method public constructor <init>(Lfcd;Lgix;Lbcc;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcd;

    iput-object v0, p0, Lbgm;->a:Lfcd;

    .line 39
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbgm;->b:Lgix;

    .line 40
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbcc;

    iput-object v0, p0, Lbgm;->c:Lbcc;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 44
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    if-nez p2, :cond_0

    const-string p2, "yt_android_default"

    .line 47
    :cond_0
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0902de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 48
    const/high16 v1, 0x100000

    .line 49
    invoke-static {p1, v1}, La;->b(Landroid/app/Activity;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 50
    invoke-static {p2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v2

    .line 51
    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    iget-object v2, p0, Lbgm;->c:Lbcc;

    .line 52
    invoke-virtual {v2}, Lbcc;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v0

    .line 54
    iget-object v1, p0, Lbgm;->b:Lgix;

    invoke-interface {v1}, Lgix;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    iget-object v1, p0, Lbgm;->a:Lfcd;

    iget-object v2, p0, Lbgm;->b:Lgix;

    invoke-virtual {v1, v2}, Lfcd;->a(Lgix;)Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    .line 57
    :cond_1
    new-instance v1, Lemn;

    invoke-direct {v1, p1}, Lemn;-><init>(Landroid/app/Activity;)V

    .line 58
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lemn;->a(Landroid/content/Intent;)V

    .line 59
    return-void
.end method
