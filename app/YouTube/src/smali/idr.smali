.class final Lidr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liea;


# static fields
.field private static synthetic g:Z


# instance fields
.field private final a:Ljava/net/HttpURLConnection;

.field private final b:Lidl;

.field private c:J

.field private d:I

.field private e:Liee;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lidp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lidr;->g:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/net/HttpURLConnection;Ljava/lang/String;Lidn;Lidl;)V
    .locals 5

    .prologue
    const v1, 0x493e0

    const/4 v4, 0x1

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lidr;->f:I

    .line 95
    iput-object p1, p0, Lidr;->a:Ljava/net/HttpURLConnection;

    .line 97
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 102
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 103
    invoke-virtual {p1, v4}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 106
    iput-object p4, p0, Lidr;->b:Lidl;

    .line 107
    if-eqz p4, :cond_0

    .line 108
    invoke-virtual {p1, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 109
    invoke-interface {p4}, Lidl;->f()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    .line 110
    invoke-interface {p4}, Lidl;->f()J

    move-result-wide v0

    invoke-interface {p4}, Lidl;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 111
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 114
    long-to-int v0, v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 124
    :cond_0
    :goto_0
    invoke-virtual {p3}, Lidn;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 125
    invoke-virtual {p3, v0}, Lidn;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 126
    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 98
    :catch_0
    move-exception v0

    .line 99
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid http method."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 116
    :cond_2
    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(J)V

    goto :goto_0

    .line 119
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    goto :goto_0

    .line 130
    :cond_4
    iput v4, p0, Lidr;->d:I

    .line 131
    return-void
.end method

.method static synthetic a(Lidr;)Lido;
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lidr;->d()Lido;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lidr;)Liee;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lidr;->e:Liee;

    return-object v0
.end method

.method private d()Lido;
    .locals 11

    .prologue
    const/high16 v10, 0x10000

    const/4 v1, 0x0

    .line 171
    monitor-enter p0

    .line 172
    :try_start_0
    iget-object v0, p0, Lidr;->e:Liee;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lidr;->e:Liee;

    .line 175
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 177
    invoke-direct {p0}, Lidr;->g()V

    .line 180
    :try_start_1
    iget-object v0, p0, Lidr;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 193
    iget-object v0, p0, Lidr;->b:Lidl;

    if-nez v0, :cond_1

    .line 194
    invoke-direct {p0}, Lidr;->e()Lido;

    move-result-object v0

    .line 243
    :goto_0
    return-object v0

    .line 175
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 181
    :catch_0
    move-exception v0

    .line 183
    new-instance v1, Lieb;

    sget-object v2, Liec;->a:Liec;

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/Throwable;)V

    throw v1

    .line 184
    :catch_1
    move-exception v0

    .line 186
    :try_start_3
    invoke-direct {p0}, Lidr;->e()Lido;
    :try_end_3
    .catch Lieb; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    goto :goto_0

    .line 188
    :catch_2
    move-exception v1

    new-instance v1, Lieb;

    sget-object v2, Liec;->d:Liec;

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/Throwable;)V

    throw v1

    .line 197
    :cond_1
    :try_start_4
    iget-object v0, p0, Lidr;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v3

    move v0, v1

    .line 209
    :cond_2
    :goto_1
    invoke-direct {p0}, Lidr;->f()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 210
    invoke-direct {p0}, Lidr;->g()V

    .line 212
    new-array v4, v10, [B

    move v2, v1

    .line 213
    :goto_2
    if-ge v2, v10, :cond_3

    invoke-direct {p0}, Lidr;->f()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 214
    :try_start_5
    iget-object v5, p0, Lidr;->b:Lidl;

    sub-int v6, v10, v2

    invoke-interface {v5, v4, v2, v6}, Lidl;->a([BII)I

    move-result v5

    .line 217
    iget-wide v6, p0, Lidr;->c:J

    int-to-long v8, v5

    add-long/2addr v6, v8

    iput-wide v6, p0, Lidr;->c:J
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    .line 218
    add-int/2addr v2, v5

    .line 223
    sub-int v6, v2, v5

    :try_start_6
    invoke-virtual {v3, v4, v6, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 224
    :catch_3
    move-exception v0

    .line 226
    :try_start_7
    invoke-direct {p0}, Lidr;->e()Lido;
    :try_end_7
    .catch Lieb; {:try_start_7 .. :try_end_7} :catch_7

    move-result-object v0

    goto :goto_0

    .line 200
    :catch_4
    move-exception v0

    .line 202
    :try_start_8
    invoke-direct {p0}, Lidr;->e()Lido;
    :try_end_8
    .catch Lieb; {:try_start_8 .. :try_end_8} :catch_5

    move-result-object v0

    goto :goto_0

    .line 204
    :catch_5
    move-exception v1

    new-instance v1, Lieb;

    sget-object v2, Liec;->d:Liec;

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/Throwable;)V

    throw v1

    .line 219
    :catch_6
    move-exception v0

    .line 220
    new-instance v1, Lieb;

    sget-object v2, Liec;->c:Liec;

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/Throwable;)V

    throw v1

    .line 228
    :catch_7
    move-exception v1

    new-instance v1, Lieb;

    sget-object v2, Liec;->c:Liec;

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/Throwable;)V

    throw v1

    .line 231
    :cond_3
    add-int/2addr v0, v2

    .line 234
    iget v2, p0, Lidr;->f:I

    if-le v0, v2, :cond_2

    .line 235
    monitor-enter p0

    .line 236
    :try_start_9
    iget-object v0, p0, Lidr;->e:Liee;

    if-eqz v0, :cond_4

    .line 237
    iget-object v0, p0, Lidr;->e:Liee;

    invoke-virtual {v0}, Liee;->a()V

    .line 239
    :cond_4
    monitor-exit p0

    move v0, v1

    .line 240
    goto :goto_1

    .line 239
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v0

    .line 243
    :cond_5
    invoke-direct {p0}, Lidr;->e()Lido;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private e()Lido;
    .locals 11

    .prologue
    .line 251
    invoke-direct {p0}, Lidr;->g()V

    .line 253
    :try_start_0
    iget-object v0, p0, Lidr;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 261
    :try_start_1
    iget-object v0, p0, Lidr;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v3, v0

    .line 268
    :goto_0
    const/4 v0, 0x0

    .line 269
    iget-object v1, p0, Lidr;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v6

    .line 270
    if-eqz v6, :cond_3

    .line 271
    new-instance v4, Lidn;

    invoke-direct {v4}, Lidn;-><init>()V

    .line 273
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274
    if-eqz v0, :cond_0

    .line 276
    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 277
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    iget-object v9, v4, Lidn;->a:Ljava/util/Map;

    invoke-interface {v9, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    iget-object v9, v4, Lidn;->a:Ljava/util/Map;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v9, v2, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v9, v4, Lidn;->a:Ljava/util/Map;

    invoke-interface {v9, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 256
    :catch_0
    move-exception v0

    .line 257
    new-instance v1, Lieb;

    sget-object v2, Liec;->d:Liec;

    const-string v3, "Error while reading response code."

    invoke-direct {v1, v2, v3, v0}, Lieb;-><init>(Liec;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 265
    :catch_1
    move-exception v0

    iget-object v0, p0, Lidr;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_2
    move-object v0, v4

    .line 283
    :cond_3
    new-instance v1, Lido;

    invoke-direct {v1, v5, v0, v3}, Lido;-><init>(ILidn;Ljava/io/InputStream;)V

    return-object v1
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 288
    :try_start_0
    iget-object v0, p0, Lidr;->b:Lidl;

    invoke-interface {v0}, Lidl;->g()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 289
    :catch_0
    move-exception v0

    .line 290
    new-instance v1, Lieb;

    sget-object v2, Liec;->c:Liec;

    invoke-direct {v1, v2, v0}, Lieb;-><init>(Liec;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private declared-synchronized g()V
    .locals 3

    .prologue
    .line 295
    monitor-enter p0

    :goto_0
    :try_start_0
    iget v0, p0, Lidr;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 298
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 301
    :catch_0
    move-exception v0

    goto :goto_0

    .line 304
    :cond_0
    :try_start_2
    iget v0, p0, Lidr;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 306
    new-instance v0, Lieb;

    sget-object v1, Liec;->b:Liec;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lieb;-><init>(Liec;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 309
    :cond_1
    :try_start_3
    sget-boolean v0, Lidr;->g:Z

    if-nez v0, :cond_2

    iget v0, p0, Lidr;->d:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 310
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Future;
    .locals 2

    .prologue
    .line 136
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lids;

    invoke-direct {v1, p0}, Lids;-><init>(Lidr;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 161
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 162
    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 163
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 164
    return-object v0
.end method

.method public final declared-synchronized a(Liee;I)V
    .locals 1

    .prologue
    .line 357
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lidr;->e:Liee;

    .line 358
    if-lez p2, :cond_0

    .line 359
    iput p2, p0, Lidr;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    :cond_0
    monitor-exit p0

    return-void

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 325
    monitor-enter p0

    .line 326
    const/4 v0, 0x3

    :try_start_0
    iput v0, p0, Lidr;->d:I

    .line 327
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 328
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
