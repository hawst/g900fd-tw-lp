.class public final Lfpx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;


# instance fields
.field private final a:Lhia;

.field private b:Ljava/util/List;

.field private c:Ljava/util/List;

.field private d:Lfqb;


# direct methods
.method public constructor <init>(Lhia;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lfpx;->a:Lhia;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()Lfqb;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lfpx;->d:Lfqb;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfpx;->a:Lhia;

    iget-object v0, v0, Lhia;->d:Lhhw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpx;->a:Lhia;

    iget-object v0, v0, Lhia;->d:Lhhw;

    iget-object v0, v0, Lhhw;->a:Lhhv;

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Lfqb;

    iget-object v1, p0, Lfpx;->a:Lhia;

    iget-object v1, v1, Lhia;->d:Lhhw;

    iget-object v1, v1, Lhhw;->a:Lhhv;

    invoke-direct {v0, v1}, Lfqb;-><init>(Lhhv;)V

    iput-object v0, p0, Lfpx;->d:Lfqb;

    .line 47
    :cond_0
    iget-object v0, p0, Lfpx;->d:Lfqb;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 2

    .prologue
    .line 91
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 92
    invoke-virtual {p0}, Lfpx;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqe;

    .line 93
    invoke-virtual {v0, p1}, Lfqe;->a(Lfau;)V

    goto :goto_0

    .line 95
    :cond_0
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lfpx;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lfpx;->a:Lhia;

    iget-object v0, v0, Lhia;->a:[Lhib;

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lfpx;->a:Lhia;

    iget-object v0, v0, Lhia;->a:[Lhib;

    invoke-static {v0}, La;->a([Lhib;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfpx;->b:Ljava/util/List;

    .line 59
    :cond_0
    :goto_0
    iget-object v0, p0, Lfpx;->b:Ljava/util/List;

    return-object v0

    .line 55
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfpx;->b:Ljava/util/List;

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lfpx;->c:Ljava/util/List;

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lfpx;->a:Lhia;

    iget-object v0, v0, Lhia;->c:[Lhpc;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lfpx;->a:Lhia;

    iget-object v0, v0, Lhia;->c:[Lhpc;

    invoke-static {v0}, La;->a([Lhpc;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfpx;->c:Ljava/util/List;

    .line 71
    :cond_0
    :goto_0
    iget-object v0, p0, Lfpx;->c:Ljava/util/List;

    return-object v0

    .line 67
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfpx;->c:Ljava/util/List;

    goto :goto_0
.end method
