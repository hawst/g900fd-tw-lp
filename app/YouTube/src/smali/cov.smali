.class public final Lcov;
.super Lcnh;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private final a:Lcnw;

.field private final b:Lfoy;

.field private final c:Lcuy;

.field private final d:Lgoc;

.field private e:Z

.field private f:I

.field private g:I

.field private h:Ljava/util/PriorityQueue;

.field private i:Lcwx;


# direct methods
.method constructor <init>(Levn;Lcnw;Lfoy;Ljava/lang/String;IZILcwx;Lgoc;Lcuy;)V
    .locals 8

    .prologue
    .line 71
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    invoke-direct/range {v0 .. v7}, Lcov;-><init>(Levn;Lcnw;Lfoy;Ljava/lang/String;Lcwx;Lgoc;Lcuy;)V

    .line 78
    iput p5, p0, Lcov;->f:I

    .line 79
    iput-boolean p6, p0, Lcov;->e:Z

    .line 80
    iput p7, p0, Lcov;->g:I

    .line 81
    invoke-direct {p0, p7}, Lcov;->b(I)Ljava/util/PriorityQueue;

    move-result-object v0

    iput-object v0, p0, Lcov;->h:Ljava/util/PriorityQueue;

    .line 82
    iget-object v0, p0, Lcov;->c:Lcuy;

    int-to-long v2, p7

    iput-wide v2, v0, Lcuy;->g:J

    .line 83
    return-void
.end method

.method constructor <init>(Levn;Lcnw;Lfoy;Ljava/lang/String;Lcwx;Lgoc;Lcuy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0, p1}, Lcnh;-><init>(Levn;)V

    .line 57
    iput-object v1, p0, Lcov;->i:Lcwx;

    .line 95
    iput-object p2, p0, Lcov;->a:Lcnw;

    .line 96
    iput-object p3, p0, Lcov;->b:Lfoy;

    .line 97
    const/4 v0, -0x1

    iput v0, p0, Lcov;->g:I

    .line 98
    iget v0, p0, Lcov;->g:I

    invoke-direct {p0, v0}, Lcov;->b(I)Ljava/util/PriorityQueue;

    move-result-object v0

    iput-object v0, p0, Lcov;->h:Ljava/util/PriorityQueue;

    .line 100
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwx;

    iput-object v0, p0, Lcov;->i:Lcwx;

    .line 101
    iput-object p7, p0, Lcov;->c:Lcuy;

    .line 102
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoc;

    iput-object v0, p0, Lcov;->d:Lgoc;

    .line 103
    iget-object v0, p0, Lcov;->c:Lcuy;

    invoke-virtual {v0, v1, p4}, Lcuy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcov;->c:Lcuy;

    iput-object p3, v0, Lcuy;->a:Lfoy;

    .line 105
    iget-object v0, p0, Lcov;->c:Lcuy;

    iget-object v1, p0, Lcov;->i:Lcwx;

    iput-object v1, v0, Lcuy;->d:Lcwx;

    .line 106
    return-void
.end method

.method static synthetic a(Lcov;)Lfoy;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcov;->b:Lfoy;

    return-object v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 228
    iget-boolean v0, p0, Lcov;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcov;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    invoke-direct {p0}, Lcov;->o()V

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcov;->e:Z

    .line 233
    :cond_0
    :goto_0
    iget-object v0, p0, Lcov;->h:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcov;->h:Ljava/util/PriorityQueue;

    .line 235
    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    iget-object v1, p0, Lcov;->b:Lfoy;

    iget v1, v1, Lfoy;->o:I

    invoke-virtual {v0, v1}, Lfpf;->a(I)I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 236
    iget-object v0, p0, Lcov;->h:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    iget-object v0, v0, Lfpf;->c:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcov;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 238
    :cond_1
    iput p1, p0, Lcov;->g:I

    .line 240
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget v0, v0, Lfoy;->o:I

    mul-int/lit16 v0, v0, 0x3e8

    if-lez v0, :cond_2

    mul-int/lit8 v1, p1, 0x4

    div-int v0, v1, v0

    .line 241
    :goto_1
    iget v1, p0, Lcov;->f:I

    if-lt v0, v1, :cond_4

    move v2, v0

    .line 243
    :goto_2
    iget v1, p0, Lcov;->f:I

    if-lt v2, v1, :cond_3

    .line 244
    iget-object v1, p0, Lcov;->b:Lfoy;

    packed-switch v2, :pswitch_data_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_3
    invoke-direct {p0, v1}, Lcov;->a(Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 245
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_2

    .line 240
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 244
    :pswitch_0
    iget-object v1, v1, Lfoy;->v:Ljava/util/List;

    goto :goto_3

    :pswitch_1
    iget-object v1, v1, Lfoy;->w:Ljava/util/List;

    goto :goto_3

    :pswitch_2
    iget-object v1, v1, Lfoy;->x:Ljava/util/List;

    goto :goto_3

    .line 248
    :cond_3
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcov;->f:I

    .line 250
    :cond_4
    return-void

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 356
    sget-object v0, Lgod;->h:Lgod;

    invoke-direct {p0, p1, v0}, Lcov;->a(Landroid/net/Uri;Lgod;)V

    .line 357
    return-void
.end method

.method private a(Landroid/net/Uri;Lgod;)V
    .locals 4

    .prologue
    .line 360
    if-eqz p1, :cond_0

    .line 362
    :try_start_0
    iget-object v0, p0, Lcov;->d:Lgoc;

    invoke-virtual {v0, p1, p2}, Lgoc;->a(Landroid/net/Uri;Lgod;)Landroid/net/Uri;
    :try_end_0
    .catch Lfax; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 366
    :goto_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Pinging "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 367
    iget-object v0, p0, Lcov;->a:Lcnw;

    iget-object v1, p0, Lcov;->a:Lcnw;

    const-string v2, "vastad"

    .line 368
    invoke-virtual {v1, p1, v2}, Lcnw;->a(Landroid/net/Uri;Ljava/lang/String;)Lgjt;

    move-result-object v1

    iget-object v2, p0, Lcov;->b:Lfoy;

    .line 369
    iget-boolean v2, v2, Lfoy;->ag:Z

    iput-boolean v2, v1, Lgjt;->d:Z

    iget-object v2, p0, Lcov;->b:Lfoy;

    .line 370
    iget-wide v2, v2, Lfoy;->R:J

    iput-wide v2, v1, Lgjt;->e:J

    sget-object v2, Lggu;->a:Lwu;

    .line 367
    invoke-virtual {v0, v1, v2}, Lcnw;->a(Lgjt;Lwu;)V

    .line 373
    :cond_0
    return-void

    .line 363
    :catch_0
    move-exception v0

    .line 364
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to substitute URI macros "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)Z
    .locals 1

    .prologue
    .line 341
    sget-object v0, Lcuv;->a:Lcuv;

    invoke-direct {p0, p1, v0}, Lcov;->a(Ljava/util/List;Lcuv;)Z

    move-result v0

    return v0
.end method

.method private a(Ljava/util/List;Lcuv;)Z
    .locals 3

    .prologue
    .line 345
    new-instance v1, Lcux;

    invoke-direct {v1, p2}, Lcux;-><init>(Lcuv;)V

    .line 346
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 347
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 348
    invoke-direct {p0, v0, v1}, Lcov;->a(Landroid/net/Uri;Lgod;)V

    goto :goto_0

    .line 350
    :cond_0
    const/4 v0, 0x1

    .line 352
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(I)Ljava/util/PriorityQueue;
    .locals 4

    .prologue
    .line 379
    new-instance v1, Ljava/util/PriorityQueue;

    iget-object v0, p0, Lcov;->b:Lfoy;

    .line 380
    iget-object v0, v0, Lfoy;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-instance v2, Lcow;

    invoke-direct {v2, p0}, Lcow;-><init>(Lcov;)V

    invoke-direct {v1, v0, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 387
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    .line 388
    iget-object v3, p0, Lcov;->b:Lfoy;

    iget v3, v3, Lfoy;->o:I

    invoke-virtual {v0, v3}, Lfpf;->a(I)I

    move-result v3

    if-le v3, p1, :cond_0

    .line 389
    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 392
    :cond_1
    return-object v1
.end method

.method private o()V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    .line 254
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->u:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    .line 255
    return-void
.end method

.method private p()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->r:Lfqy;

    invoke-virtual {v0}, Lfqy;->p()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 141
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 167
    return-void
.end method

.method public final a(Lcuv;)V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public final a(Lcwx;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    iget-object v0, p0, Lcov;->i:Lcwx;

    iget v0, v0, Lcwx;->a:I

    if-ne v0, v4, :cond_1

    move v0, v1

    .line 122
    :goto_0
    iget v3, p1, Lcwx;->a:I

    if-ne v3, v4, :cond_2

    .line 124
    :goto_1
    iput-object p1, p0, Lcov;->i:Lcwx;

    .line 125
    iget-object v2, p0, Lcov;->c:Lcuy;

    iput-object p1, v2, Lcuy;->d:Lcwx;

    .line 127
    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 128
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->H:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    .line 132
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 121
    goto :goto_0

    :cond_2
    move v1, v2

    .line 122
    goto :goto_1

    .line 129
    :cond_3
    if-eqz v0, :cond_0

    if-nez v1, :cond_0

    .line 130
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->I:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    goto :goto_2
.end method

.method protected final a(Lcyz;)V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p1, Lcyz;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    .line 298
    return-void
.end method

.method protected final a(Lczx;)V
    .locals 2

    .prologue
    .line 224
    iget-wide v0, p1, Lczx;->a:J

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcov;->a(I)V

    .line 225
    return-void
.end method

.method protected final a(Ldad;)V
    .locals 2

    .prologue
    .line 217
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_0

    .line 218
    iget-wide v0, p1, Ldad;->a:J

    long-to-int v0, v0

    invoke-direct {p0, v0}, Lcov;->a(I)V

    .line 220
    :cond_0
    return-void
.end method

.method public final a(Lesm;)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public final a(Lfpi;I)V
    .locals 0

    .prologue
    .line 289
    return-void
.end method

.method public final a(Lfpi;Lfpm;)V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method protected final a(Lggp;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 200
    invoke-static {p1}, Lcuv;->a(Lggp;)Lcuv;

    move-result-object v0

    .line 201
    iget v1, p0, Lcov;->f:I

    if-eq v1, v2, :cond_0

    .line 204
    iget-object v1, p0, Lcov;->b:Lfoy;

    iget-object v1, v1, Lfoy;->D:Ljava/util/List;

    invoke-direct {p0, v1, v0}, Lcov;->a(Ljava/util/List;Lcuv;)Z

    .line 205
    iget-object v1, p0, Lcov;->b:Lfoy;

    iget-object v1, v1, Lfoy;->L:Ljava/util/List;

    invoke-direct {p0, v1, v0}, Lcov;->a(Ljava/util/List;Lcuv;)Z

    .line 206
    iput v2, p0, Lcov;->f:I

    .line 208
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 38
    check-cast p1, Lgkr;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ping failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lezp;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public final b(Lcuv;)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->L:Ljava/util/List;

    invoke-direct {p0, v0, p1}, Lcov;->a(Ljava/util/List;Lcuv;)Z

    .line 158
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 163
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 172
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 181
    iget-boolean v0, p0, Lcov;->e:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcov;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 182
    invoke-direct {p0}, Lcov;->o()V

    .line 183
    iput-boolean v1, p0, Lcov;->e:Z

    .line 186
    :cond_0
    iget v0, p0, Lcov;->f:I

    if-nez v0, :cond_1

    .line 187
    iput v1, p0, Lcov;->f:I

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->F:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->D:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    .line 196
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->E:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    .line 213
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 268
    :goto_0
    iget-object v0, p0, Lcov;->h:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcov;->h:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpf;

    iget-object v0, v0, Lfpf;->c:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcov;->a(Landroid/net/Uri;)V

    goto :goto_0

    .line 271
    :cond_0
    iget-object v0, p0, Lcov;->b:Lfoy;

    iget-object v0, v0, Lfoy;->C:Ljava/util/List;

    invoke-direct {p0, v0}, Lcov;->a(Ljava/util/List;)Z

    .line 272
    const/4 v0, 0x5

    iput v0, p0, Lcov;->f:I

    .line 273
    return-void
.end method

.method public final j()Lcnj;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 277
    new-instance v0, Lcnj;

    iget v1, p0, Lcov;->f:I

    iget-boolean v3, p0, Lcov;->e:Z

    .line 282
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    iget v6, p0, Lcov;->g:I

    sget-object v7, Lcnl;->b:Lcnl;

    move v4, v2

    invoke-direct/range {v0 .. v7}, Lcnj;-><init>(IZZZLjava/util/List;ILcnl;)V

    return-object v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method
