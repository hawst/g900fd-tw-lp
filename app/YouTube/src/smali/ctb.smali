.class public final Lctb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lgku;

.field final b:Lfxj;

.field final c:Ljava/util/concurrent/Executor;

.field final d:Lfbu;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object v0, p0, Lctb;->a:Lgku;

    .line 100
    iput-object v0, p0, Lctb;->b:Lfxj;

    .line 101
    iput-object v0, p0, Lctb;->c:Ljava/util/concurrent/Executor;

    .line 102
    iput-object v0, p0, Lctb;->d:Lfbu;

    .line 103
    return-void
.end method

.method public constructor <init>(Lfxj;Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lgir;Ljava/util/List;Lfbu;Lfap;Lfbc;)V
    .locals 6

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxj;

    iput-object v0, p0, Lctb;->b:Lfxj;

    .line 58
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lctb;->c:Ljava/util/concurrent/Executor;

    .line 60
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbu;

    iput-object v0, p0, Lctb;->d:Lfbu;

    .line 63
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkv;

    .line 67
    instance-of v0, v0, Lctd;

    if-eqz v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "IdentityClient must not have an authentication header decorator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_1
    sget-object v1, Lewv;->b:Lewv;

    sget-object v5, Lfxh;->a:Lfxh;

    .line 76
    new-instance v0, Lfxi;

    move-object v2, p4

    move-object v3, p5

    move-object v4, p7

    invoke-direct/range {v0 .. v5}, Lfxi;-><init>(Lewv;Lgir;Ljava/util/List;Lfap;Lfxh;)V

    .line 83
    new-instance v1, Lgko;

    new-instance v2, Lcqx;

    invoke-direct {v2, p8}, Lcqx;-><init>(Lfbc;)V

    invoke-direct {v1, p2, v0, v2}, Lgko;-><init>(Lorg/apache/http/client/HttpClient;Lgib;Lghv;)V

    .line 88
    new-instance v1, Lgko;

    new-instance v2, Lfzm;

    invoke-direct {v2, p8}, Lfzm;-><init>(Lfbc;)V

    invoke-direct {v1, p2, v0, v2}, Lgko;-><init>(Lorg/apache/http/client/HttpClient;Lgib;Lghv;)V

    iput-object v1, p0, Lctb;->a:Lgku;

    .line 92
    return-void
.end method
