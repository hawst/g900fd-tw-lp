.class public final Lhbp;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhtx;

.field public b:Lhbq;

.field public c:Lhel;

.field public d:Lhbn;

.field public e:[B

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16400
    invoke-direct {p0}, Lidf;-><init>()V

    .line 16403
    iput-object v1, p0, Lhbp;->a:Lhtx;

    .line 16406
    const-string v0, ""

    iput-object v0, p0, Lhbp;->f:Ljava/lang/String;

    .line 16409
    iput-object v1, p0, Lhbp;->b:Lhbq;

    .line 16412
    iput-object v1, p0, Lhbp;->c:Lhel;

    .line 16415
    iput-object v1, p0, Lhbp;->d:Lhbn;

    .line 16418
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhbp;->e:[B

    .line 16400
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 16490
    const/4 v0, 0x0

    .line 16491
    iget-object v1, p0, Lhbp;->a:Lhtx;

    if-eqz v1, :cond_0

    .line 16492
    const/4 v0, 0x1

    iget-object v1, p0, Lhbp;->a:Lhtx;

    .line 16493
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 16495
    :cond_0
    iget-object v1, p0, Lhbp;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 16496
    const/4 v1, 0x5

    iget-object v2, p0, Lhbp;->f:Ljava/lang/String;

    .line 16497
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16499
    :cond_1
    iget-object v1, p0, Lhbp;->b:Lhbq;

    if-eqz v1, :cond_2

    .line 16500
    const/16 v1, 0x9

    iget-object v2, p0, Lhbp;->b:Lhbq;

    .line 16501
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16503
    :cond_2
    iget-object v1, p0, Lhbp;->c:Lhel;

    if-eqz v1, :cond_3

    .line 16504
    const/16 v1, 0xa

    iget-object v2, p0, Lhbp;->c:Lhel;

    .line 16505
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16507
    :cond_3
    iget-object v1, p0, Lhbp;->d:Lhbn;

    if-eqz v1, :cond_4

    .line 16508
    const/16 v1, 0xd

    iget-object v2, p0, Lhbp;->d:Lhbn;

    .line 16509
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16511
    :cond_4
    iget-object v1, p0, Lhbp;->e:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 16512
    const/16 v1, 0x10

    iget-object v2, p0, Lhbp;->e:[B

    .line 16513
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 16515
    :cond_5
    iget-object v1, p0, Lhbp;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 16516
    iput v0, p0, Lhbp;->J:I

    .line 16517
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 16396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbp;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbp;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbp;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbp;->a:Lhtx;

    if-nez v0, :cond_2

    new-instance v0, Lhtx;

    invoke-direct {v0}, Lhtx;-><init>()V

    iput-object v0, p0, Lhbp;->a:Lhtx;

    :cond_2
    iget-object v0, p0, Lhbp;->a:Lhtx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhbp;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhbp;->b:Lhbq;

    if-nez v0, :cond_3

    new-instance v0, Lhbq;

    invoke-direct {v0}, Lhbq;-><init>()V

    iput-object v0, p0, Lhbp;->b:Lhbq;

    :cond_3
    iget-object v0, p0, Lhbp;->b:Lhbq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhbp;->c:Lhel;

    if-nez v0, :cond_4

    new-instance v0, Lhel;

    invoke-direct {v0}, Lhel;-><init>()V

    iput-object v0, p0, Lhbp;->c:Lhel;

    :cond_4
    iget-object v0, p0, Lhbp;->c:Lhel;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhbp;->d:Lhbn;

    if-nez v0, :cond_5

    new-instance v0, Lhbn;

    invoke-direct {v0}, Lhbn;-><init>()V

    iput-object v0, p0, Lhbp;->d:Lhbn;

    :cond_5
    iget-object v0, p0, Lhbp;->d:Lhbn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhbp;->e:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x2a -> :sswitch_2
        0x4a -> :sswitch_3
        0x52 -> :sswitch_4
        0x6a -> :sswitch_5
        0x82 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 16466
    iget-object v0, p0, Lhbp;->a:Lhtx;

    if-eqz v0, :cond_0

    .line 16467
    const/4 v0, 0x1

    iget-object v1, p0, Lhbp;->a:Lhtx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16469
    :cond_0
    iget-object v0, p0, Lhbp;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 16470
    const/4 v0, 0x5

    iget-object v1, p0, Lhbp;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 16472
    :cond_1
    iget-object v0, p0, Lhbp;->b:Lhbq;

    if-eqz v0, :cond_2

    .line 16473
    const/16 v0, 0x9

    iget-object v1, p0, Lhbp;->b:Lhbq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16475
    :cond_2
    iget-object v0, p0, Lhbp;->c:Lhel;

    if-eqz v0, :cond_3

    .line 16476
    const/16 v0, 0xa

    iget-object v1, p0, Lhbp;->c:Lhel;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16478
    :cond_3
    iget-object v0, p0, Lhbp;->d:Lhbn;

    if-eqz v0, :cond_4

    .line 16479
    const/16 v0, 0xd

    iget-object v1, p0, Lhbp;->d:Lhbn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 16481
    :cond_4
    iget-object v0, p0, Lhbp;->e:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    .line 16482
    const/16 v0, 0x10

    iget-object v1, p0, Lhbp;->e:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 16484
    :cond_5
    iget-object v0, p0, Lhbp;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 16486
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 16434
    if-ne p1, p0, :cond_1

    .line 16443
    :cond_0
    :goto_0
    return v0

    .line 16435
    :cond_1
    instance-of v2, p1, Lhbp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 16436
    :cond_2
    check-cast p1, Lhbp;

    .line 16437
    iget-object v2, p0, Lhbp;->a:Lhtx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbp;->a:Lhtx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbp;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbp;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 16438
    :goto_2
    iget-object v2, p0, Lhbp;->b:Lhbq;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhbp;->b:Lhbq;

    if-nez v2, :cond_3

    .line 16439
    :goto_3
    iget-object v2, p0, Lhbp;->c:Lhel;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhbp;->c:Lhel;

    if-nez v2, :cond_3

    .line 16440
    :goto_4
    iget-object v2, p0, Lhbp;->d:Lhbn;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhbp;->d:Lhbn;

    if-nez v2, :cond_3

    .line 16441
    :goto_5
    iget-object v2, p0, Lhbp;->e:[B

    iget-object v3, p1, Lhbp;->e:[B

    .line 16442
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhbp;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhbp;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 16443
    goto :goto_0

    .line 16437
    :cond_4
    iget-object v2, p0, Lhbp;->a:Lhtx;

    iget-object v3, p1, Lhbp;->a:Lhtx;

    invoke-virtual {v2, v3}, Lhtx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbp;->f:Ljava/lang/String;

    iget-object v3, p1, Lhbp;->f:Ljava/lang/String;

    .line 16438
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhbp;->b:Lhbq;

    iget-object v3, p1, Lhbp;->b:Lhbq;

    .line 16439
    invoke-virtual {v2, v3}, Lhbq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhbp;->c:Lhel;

    iget-object v3, p1, Lhbp;->c:Lhel;

    .line 16440
    invoke-virtual {v2, v3}, Lhel;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhbp;->d:Lhbn;

    iget-object v3, p1, Lhbp;->d:Lhbn;

    .line 16441
    invoke-virtual {v2, v3}, Lhbn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    .line 16442
    :cond_9
    iget-object v2, p0, Lhbp;->I:Ljava/util/List;

    iget-object v3, p1, Lhbp;->I:Ljava/util/List;

    .line 16443
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 16447
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 16449
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbp;->a:Lhtx;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 16450
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbp;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 16451
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbp;->b:Lhbq;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 16452
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbp;->c:Lhel;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 16453
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbp;->d:Lhbn;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 16454
    iget-object v2, p0, Lhbp;->e:[B

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    .line 16460
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhbp;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_5
    add-int/2addr v0, v1

    .line 16461
    return v0

    .line 16449
    :cond_1
    iget-object v0, p0, Lhbp;->a:Lhtx;

    invoke-virtual {v0}, Lhtx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 16450
    :cond_2
    iget-object v0, p0, Lhbp;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 16451
    :cond_3
    iget-object v0, p0, Lhbp;->b:Lhbq;

    invoke-virtual {v0}, Lhbq;->hashCode()I

    move-result v0

    goto :goto_2

    .line 16452
    :cond_4
    iget-object v0, p0, Lhbp;->c:Lhel;

    invoke-virtual {v0}, Lhel;->hashCode()I

    move-result v0

    goto :goto_3

    .line 16453
    :cond_5
    iget-object v0, p0, Lhbp;->d:Lhbn;

    invoke-virtual {v0}, Lhbn;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_6
    move v2, v0

    move v0, v1

    .line 16456
    :goto_6
    iget-object v3, p0, Lhbp;->e:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 16457
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhbp;->e:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 16456
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 16460
    :cond_7
    iget-object v1, p0, Lhbp;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
