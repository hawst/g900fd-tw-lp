.class public Lebp;
.super Lebf;
.source "SourceFile"


# instance fields
.field final a:Lebt;

.field private final h:Lebu;

.field private final i:J

.field private final j:I

.field private final k:I

.field private l:Landroid/view/Surface;

.field private m:Z

.field private n:Z

.field private o:J

.field private p:J

.field private q:I

.field private r:I

.field private s:I

.field private t:F

.field private u:I

.field private v:I

.field private w:F


# direct methods
.method public constructor <init>(Lebz;IJLandroid/os/Handler;Lebt;I)V
    .locals 12

    .prologue
    .line 199
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    const-wide/16 v6, 0x64

    const/4 v8, 0x0

    const v11, 0x7fffffff

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    invoke-direct/range {v1 .. v11}, Lebp;-><init>(Lebz;Ledv;ZIJLebu;Landroid/os/Handler;Lebt;I)V

    .line 201
    return-void
.end method

.method public constructor <init>(Lebz;Ledv;ZIJLebu;Landroid/os/Handler;Lebt;I)V
    .locals 9

    .prologue
    .line 228
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-direct/range {v2 .. v7}, Lebf;-><init>(Lebz;Ledv;ZLandroid/os/Handler;Lebj;)V

    .line 229
    iput p4, p0, Lebp;->j:I

    .line 230
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p5

    iput-wide v2, p0, Lebp;->i:J

    .line 231
    move-object/from16 v0, p7

    iput-object v0, p0, Lebp;->h:Lebu;

    .line 232
    move-object/from16 v0, p9

    iput-object v0, p0, Lebp;->a:Lebt;

    .line 233
    move/from16 v0, p10

    iput v0, p0, Lebp;->k:I

    .line 234
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lebp;->o:J

    .line 235
    const/4 v2, -0x1

    iput v2, p0, Lebp;->r:I

    .line 236
    const/4 v2, -0x1

    iput v2, p0, Lebp;->s:I

    .line 237
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lebp;->t:F

    .line 238
    const/4 v2, -0x1

    iput v2, p0, Lebp;->u:I

    .line 239
    const/4 v2, -0x1

    iput v2, p0, Lebp;->v:I

    .line 240
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lebp;->w:F

    .line 241
    return-void
.end method

.method private a(Landroid/media/MediaCodec;I)V
    .locals 2

    .prologue
    .line 472
    invoke-direct {p0}, Lebp;->t()V

    .line 473
    const-string v0, "renderVideoBufferImmediate"

    invoke-static {v0}, La;->s(Ljava/lang/String;)V

    .line 474
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 475
    invoke-static {}, La;->o()V

    .line 476
    iget-object v0, p0, Lebp;->b:Lean;

    iget v1, v0, Lean;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lean;->e:I

    .line 477
    invoke-direct {p0}, Lebp;->u()V

    .line 478
    return-void
.end method

.method private t()V
    .locals 5

    .prologue
    .line 491
    iget-object v0, p0, Lebp;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebp;->a:Lebt;

    if-eqz v0, :cond_0

    iget v0, p0, Lebp;->u:I

    iget v1, p0, Lebp;->r:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lebp;->v:I

    iget v1, p0, Lebp;->s:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lebp;->w:F

    iget v1, p0, Lebp;->t:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 510
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    iget v0, p0, Lebp;->r:I

    .line 498
    iget v1, p0, Lebp;->s:I

    .line 499
    iget v2, p0, Lebp;->t:F

    .line 500
    iget-object v3, p0, Lebp;->d:Landroid/os/Handler;

    new-instance v4, Lebq;

    invoke-direct {v4, p0, v0, v1, v2}, Lebq;-><init>(Lebp;IIF)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 507
    iput v0, p0, Lebp;->u:I

    .line 508
    iput v1, p0, Lebp;->v:I

    .line 509
    iput v2, p0, Lebp;->w:F

    goto :goto_0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 513
    iget-object v0, p0, Lebp;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebp;->a:Lebt;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lebp;->m:Z

    if-eqz v0, :cond_1

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 517
    :cond_1
    iget-object v0, p0, Lebp;->l:Landroid/view/Surface;

    .line 518
    iget-object v1, p0, Lebp;->d:Landroid/os/Handler;

    new-instance v2, Lebr;

    invoke-direct {v2, p0, v0}, Lebr;-><init>(Lebp;Landroid/view/Surface;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 525
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebp;->m:Z

    goto :goto_0
.end method

.method private v()V
    .locals 7

    .prologue
    .line 529
    iget-object v0, p0, Lebp;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebp;->a:Lebt;

    if-eqz v0, :cond_0

    iget v0, p0, Lebp;->q:I

    if-nez v0, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 534
    iget v2, p0, Lebp;->q:I

    .line 535
    iget-wide v4, p0, Lebp;->p:J

    sub-long v4, v0, v4

    .line 536
    iget-object v3, p0, Lebp;->d:Landroid/os/Handler;

    new-instance v6, Lebs;

    invoke-direct {v6, p0, v2, v4, v5}, Lebs;-><init>(Lebp;IJ)V

    invoke-virtual {v3, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 543
    const/4 v2, 0x0

    iput v2, p0, Lebp;->q:I

    .line 544
    iput-wide v0, p0, Lebp;->p:J

    goto :goto_0
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 317
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 318
    check-cast p2, Landroid/view/Surface;

    iget-object v0, p0, Lebp;->l:Landroid/view/Surface;

    if-eq v0, p2, :cond_1

    iput-object p2, p0, Lebp;->l:Landroid/view/Surface;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lebp;->m:Z

    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lebp;->k()V

    invoke-virtual {p0}, Lebp;->i()V

    .line 322
    :cond_1
    :goto_0
    return-void

    .line 320
    :cond_2
    invoke-super {p0, p1, p2}, Lebf;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected final a(J)V
    .locals 3

    .prologue
    .line 262
    invoke-super {p0, p1, p2}, Lebf;->a(J)V

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lebp;->n:Z

    .line 264
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lebp;->o:J

    .line 265
    return-void
.end method

.method protected final a(JZ)V
    .locals 5

    .prologue
    .line 250
    invoke-super {p0, p1, p2, p3}, Lebf;->a(JZ)V

    .line 251
    const/4 v0, 0x0

    iput-boolean v0, p0, Lebp;->n:Z

    .line 252
    if-eqz p3, :cond_0

    iget-wide v0, p0, Lebp;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 253
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lebp;->i:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lebp;->o:J

    .line 255
    :cond_0
    iget-object v0, p0, Lebp;->h:Lebu;

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lebp;->h:Lebu;

    invoke-interface {v0}, Lebu;->a()V

    .line 258
    :cond_1
    return-void
.end method

.method public a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lebp;->l:Landroid/view/Surface;

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 351
    iget v0, p0, Lebp;->j:I

    invoke-virtual {p1, v0}, Landroid/media/MediaCodec;->setVideoScalingMode(I)V

    .line 352
    return-void
.end method

.method protected final a(Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    .line 365
    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "crop-left"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "crop-bottom"

    .line 366
    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "crop-top"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 367
    :goto_0
    if-eqz v1, :cond_1

    const-string v0, "crop-right"

    .line 368
    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v2, "crop-left"

    invoke-virtual {p1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    .line 369
    :goto_1
    iput v0, p0, Lebp;->r:I

    .line 370
    if-eqz v1, :cond_2

    const-string v0, "crop-bottom"

    .line 371
    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v1, "crop-top"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 372
    :goto_2
    iput v0, p0, Lebp;->s:I

    .line 373
    return-void

    .line 366
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 368
    :cond_1
    const-string v0, "width"

    .line 369
    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 371
    :cond_2
    const-string v0, "height"

    .line 372
    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_2
.end method

.method protected final a(Lebw;)V
    .locals 2

    .prologue
    .line 356
    invoke-super {p0, p1}, Lebf;->a(Lebw;)V

    .line 359
    iget-object v0, p1, Lebw;->a:Lebv;

    iget v0, v0, Lebv;->e:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    iput v0, p0, Lebp;->t:F

    .line 361
    return-void

    .line 359
    :cond_0
    iget-object v0, p1, Lebw;->a:Lebv;

    iget v0, v0, Lebv;->e:F

    goto :goto_0
.end method

.method protected final a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 11

    .prologue
    .line 386
    if-eqz p9, :cond_0

    .line 387
    const-string v2, "skipVideoBuffer"

    invoke-static {v2}, La;->s(Ljava/lang/String;)V

    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    invoke-static {}, La;->o()V

    iget-object v2, p0, Lebp;->b:Lean;

    iget v3, v2, Lean;->f:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lean;->f:I

    .line 388
    const/4 v2, 0x1

    .line 450
    :goto_0
    return v2

    .line 392
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long/2addr v2, p3

    .line 393
    move-object/from16 v0, p7

    iget-wide v4, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    sub-long/2addr v4, p1

    sub-long/2addr v4, v2

    .line 396
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 397
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v2, v6

    .line 401
    iget-object v8, p0, Lebp;->h:Lebu;

    if-eqz v8, :cond_1

    .line 402
    iget-object v4, p0, Lebp;->h:Lebu;

    move-object/from16 v0, p7

    iget-wide v8, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-interface {v4, v8, v9, v2, v3}, Lebu;->a(JJ)J

    move-result-wide v2

    .line 404
    sub-long v4, v2, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 409
    :cond_1
    const-wide/16 v6, -0x7530

    cmp-long v6, v4, v6

    if-gez v6, :cond_3

    .line 411
    const-string v2, "dropVideoBuffer"

    invoke-static {v2}, La;->s(Ljava/lang/String;)V

    const/4 v2, 0x0

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    invoke-static {}, La;->o()V

    iget-object v2, p0, Lebp;->b:Lean;

    iget v3, v2, Lean;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lean;->g:I

    iget v2, p0, Lebp;->q:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lebp;->q:I

    iget v2, p0, Lebp;->q:I

    iget v3, p0, Lebp;->k:I

    if-ne v2, v3, :cond_2

    invoke-direct {p0}, Lebp;->v()V

    .line 412
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 415
    :cond_3
    iget-boolean v6, p0, Lebp;->n:Z

    if-nez v6, :cond_4

    .line 416
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1}, Lebp;->a(Landroid/media/MediaCodec;I)V

    .line 417
    const/4 v2, 0x1

    iput-boolean v2, p0, Lebp;->n:Z

    .line 418
    const/4 v2, 0x1

    goto :goto_0

    .line 421
    :cond_4
    iget v6, p0, Lecc;->g:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_5

    .line 422
    const/4 v2, 0x0

    goto :goto_0

    .line 425
    :cond_5
    sget v6, Legz;->a:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_6

    .line 427
    const-wide/32 v6, 0xc350

    cmp-long v4, v4, v6

    if-gez v4, :cond_8

    .line 428
    invoke-direct {p0}, Lebp;->t()V

    const-string v4, "releaseOutputBufferTimed"

    invoke-static {v4}, La;->s(Ljava/lang/String;)V

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->releaseOutputBuffer(IJ)V

    invoke-static {}, La;->o()V

    iget-object v2, p0, Lebp;->b:Lean;

    iget v3, v2, Lean;->e:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lean;->e:I

    invoke-direct {p0}, Lebp;->u()V

    .line 429
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 433
    :cond_6
    const-wide/16 v2, 0x7530

    cmp-long v2, v4, v2

    if-gez v2, :cond_8

    .line 434
    const-wide/16 v2, 0x2af8

    cmp-long v2, v4, v2

    if-lez v2, :cond_7

    .line 439
    const-wide/16 v2, 0x2710

    sub-long v2, v4, v2

    const-wide/16 v4, 0x3e8

    :try_start_0
    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 444
    :cond_7
    :goto_1
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-direct {p0, v0, v1}, Lebp;->a(Landroid/media/MediaCodec;I)V

    .line 445
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 441
    :catch_0
    move-exception v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 450
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method public a(Landroid/media/MediaCodec;ZLebv;Lebv;)Z
    .locals 2

    .prologue
    .line 378
    iget-object v0, p4, Lebv;->a:Ljava/lang/String;

    iget-object v1, p3, Lebv;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    iget v0, p3, Lebv;->c:I

    iget v1, p4, Lebv;->c:I

    if-ne v0, v1, :cond_1

    iget v0, p3, Lebv;->d:I

    iget v1, p4, Lebv;->d:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 245
    invoke-static {p1}, La;->p(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lebf;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 289
    invoke-super {p0}, Lebf;->b()V

    .line 290
    const/4 v0, 0x0

    iput v0, p0, Lebp;->q:I

    .line 291
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lebp;->p:J

    .line 292
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 296
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lebp;->o:J

    .line 297
    invoke-direct {p0}, Lebp;->v()V

    .line 298
    invoke-super {p0}, Lebf;->c()V

    .line 299
    return-void
.end method

.method protected final e()Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 269
    invoke-super {p0}, Lebf;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lebp;->n:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 270
    iget v2, p0, Lebf;->f:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 272
    :cond_0
    iput-wide v6, p0, Lebp;->o:J

    .line 283
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v2, v1

    .line 269
    goto :goto_0

    .line 274
    :cond_3
    iget-wide v2, p0, Lebp;->o:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_4

    move v0, v1

    .line 276
    goto :goto_1

    .line 277
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lebp;->o:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 282
    iput-wide v6, p0, Lebp;->o:J

    move v0, v1

    .line 283
    goto :goto_1
.end method

.method public g()V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    const/4 v0, -0x1

    .line 303
    iput v0, p0, Lebp;->r:I

    .line 304
    iput v0, p0, Lebp;->s:I

    .line 305
    iput v1, p0, Lebp;->t:F

    .line 306
    iput v0, p0, Lebp;->u:I

    .line 307
    iput v0, p0, Lebp;->v:I

    .line 308
    iput v1, p0, Lebp;->w:F

    .line 309
    iget-object v0, p0, Lebp;->h:Lebu;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lebp;->h:Lebu;

    invoke-interface {v0}, Lebu;->b()V

    .line 312
    :cond_0
    invoke-super {p0}, Lebf;->g()V

    .line 313
    return-void
.end method

.method protected final j()Z
    .locals 1

    .prologue
    .line 343
    invoke-super {p0}, Lebf;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebp;->l:Landroid/view/Surface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
