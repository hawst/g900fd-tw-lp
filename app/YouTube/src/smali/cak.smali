.class public final Lcak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbql;


# static fields
.field public static final a:[I


# instance fields
.field public final b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field public final c:Lbqk;

.field public d:Lcan;

.field public e:Lcaj;

.field public f:I

.field private final g:Lcal;

.field private final h:Landroid/support/v7/widget/Toolbar;

.field private final i:Lkm;

.field private final j:I

.field private final k:Landroid/animation/ArgbEvaluator;

.field private l:F

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010057

    aput v2, v0, v1

    sput-object v0, Lcak;->a:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Landroid/support/v7/widget/Toolbar;Lcal;Lcan;)V
    .locals 3

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lcak;->k:Landroid/animation/ArgbEvaluator;

    .line 48
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lcak;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 49
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcal;

    iput-object v0, p0, Lcak;->g:Lcal;

    .line 50
    iget-object v0, p0, Lcak;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/support/v7/widget/Toolbar;)V

    .line 51
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcak;->h:Landroid/support/v7/widget/Toolbar;

    .line 52
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d()Lkm;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkm;

    iput-object v0, p0, Lcak;->i:Lkm;

    .line 53
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcan;

    iput-object v0, p0, Lcak;->d:Lcan;

    .line 55
    invoke-static {p1}, La;->a(Landroid/app/Activity;)I

    move-result v0

    iput v0, p0, Lcak;->j:I

    .line 56
    invoke-virtual {p1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0023

    .line 57
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 58
    invoke-virtual {p0}, Lcak;->e()Lcaj;

    move-result-object v1

    iput-object v1, p0, Lcak;->e:Lcaj;

    .line 59
    new-instance v1, Lbqk;

    iget-object v2, p0, Lcak;->e:Lcaj;

    invoke-direct {v1, v2, v0}, Lbqk;-><init>(Lbqm;I)V

    iput-object v1, p0, Lcak;->c:Lbqk;

    .line 60
    iget-object v0, p0, Lcak;->i:Lkm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lkm;->b(Z)V

    .line 61
    iget-object v0, p0, Lcak;->i:Lkm;

    iget-object v1, p0, Lcak;->c:Lbqk;

    invoke-virtual {v0, v1}, Lkm;->a(Landroid/graphics/drawable/Drawable;)V

    .line 62
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcak;->a(F)V

    .line 64
    invoke-virtual {p0}, Lcak;->c()V

    .line 65
    invoke-virtual {p0}, Lcak;->b()V

    .line 66
    return-void
.end method

.method private a(FII)I
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcak;->k:Landroid/animation/ArgbEvaluator;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private a(Lbqm;)I
    .locals 1

    .prologue
    .line 173
    instance-of v0, p1, Lcaj;

    if-eqz v0, :cond_0

    .line 174
    check-cast p1, Lcaj;

    iget v0, p1, Lcaj;->d:I

    .line 176
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcak;->j:I

    goto :goto_0
.end method

.method private a(FI)V
    .locals 2

    .prologue
    .line 180
    iget v0, p0, Lcak;->f:I

    invoke-direct {p0, p1, p2, v0}, Lcak;->a(FII)I

    move-result v0

    .line 181
    iget-object v1, p0, Lcak;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v1, v0}, La;->a(Landroid/app/Activity;I)V

    .line 182
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcak;->e:Lcaj;

    invoke-direct {p0, v0}, Lcak;->a(Lbqm;)I

    move-result v0

    iput v0, p0, Lcak;->m:I

    .line 140
    iget v0, p0, Lcak;->l:F

    iget v1, p0, Lcak;->m:I

    invoke-direct {p0, v0, v1}, Lcak;->a(FI)V

    .line 141
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 160
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcak;->l:F

    .line 161
    iget v0, p0, Lcak;->l:F

    iget v1, p0, Lcak;->m:I

    invoke-direct {p0, v0, v1}, Lcak;->a(FI)V

    .line 162
    return-void
.end method

.method public final a(FLbqm;Lbqm;)V
    .locals 2

    .prologue
    .line 145
    invoke-direct {p0, p2}, Lcak;->a(Lbqm;)I

    move-result v0

    .line 146
    invoke-direct {p0, p3}, Lcak;->a(Lbqm;)I

    move-result v1

    .line 147
    invoke-direct {p0, p1, v0, v1}, Lcak;->a(FII)I

    move-result v0

    iput v0, p0, Lcak;->m:I

    .line 148
    iget v0, p0, Lcak;->l:F

    iget v1, p0, Lcak;->m:I

    invoke-direct {p0, v0, v1}, Lcak;->a(FI)V

    .line 149
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcak;->h:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lcak;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcak;->d:Lcan;

    invoke-interface {v2}, Lcan;->e()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/content/Context;I)V

    .line 96
    iget-object v0, p0, Lcak;->h:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lcak;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v2, p0, Lcak;->d:Lcan;

    invoke-interface {v2}, Lcan;->f()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/content/Context;I)V

    .line 97
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 100
    iget-object v0, p0, Lcak;->d:Lcan;

    invoke-interface {v0}, Lcan;->b()Landroid/view/View;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    .line 103
    iget-object v1, p0, Lcak;->i:Lkm;

    new-instance v2, Lkn;

    invoke-direct {v2, v3, v3}, Lkn;-><init>(II)V

    invoke-virtual {v1, v0, v2}, Lkm;->a(Landroid/view/View;Lkn;)V

    .line 108
    const/16 v0, 0x10

    .line 113
    :goto_0
    iget-object v1, p0, Lcak;->i:Lkm;

    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, Lkm;->a(II)V

    .line 116
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lcak;->i:Lkm;

    iget-object v1, p0, Lcak;->d:Lcan;

    invoke-interface {v1}, Lcan;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkm;->a(Ljava/lang/CharSequence;)V

    .line 111
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcak;->g:Lcal;

    iget-object v1, p0, Lcak;->d:Lcan;

    invoke-interface {v1}, Lcan;->g()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcal;->a(Ljava/util/Collection;)V

    .line 120
    return-void
.end method

.method public e()Lcaj;
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcak;->d:Lcan;

    invoke-interface {v0}, Lcan;->c()I

    move-result v0

    .line 124
    iget-object v1, p0, Lcak;->d:Lcan;

    invoke-interface {v1}, Lcan;->d()I

    move-result v1

    .line 125
    iget-object v2, p0, Lcak;->e:Lcaj;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcak;->e:Lcaj;

    invoke-virtual {v2, v0, v1}, Lcaj;->b(II)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcak;->e:Lcaj;

    .line 127
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, v1}, Lcaj;->a(II)Lcaj;

    move-result-object v0

    goto :goto_0
.end method
