.class public final Lhcj;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21725
    invoke-direct {p0}, Lidf;-><init>()V

    .line 21728
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhcj;->a:Z

    .line 21725
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 21765
    const/4 v0, 0x0

    .line 21766
    iget-boolean v1, p0, Lhcj;->a:Z

    if-eqz v1, :cond_0

    .line 21767
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhcj;->a:Z

    .line 21768
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 21770
    :cond_0
    iget-object v1, p0, Lhcj;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21771
    iput v0, p0, Lhcj;->J:I

    .line 21772
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 21721
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhcj;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhcj;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhcj;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhcj;->a:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 21756
    iget-boolean v0, p0, Lhcj;->a:Z

    if-eqz v0, :cond_0

    .line 21757
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhcj;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 21759
    :cond_0
    iget-object v0, p0, Lhcj;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 21761
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21739
    if-ne p1, p0, :cond_1

    .line 21743
    :cond_0
    :goto_0
    return v0

    .line 21740
    :cond_1
    instance-of v2, p1, Lhcj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 21741
    :cond_2
    check-cast p1, Lhcj;

    .line 21742
    iget-boolean v2, p0, Lhcj;->a:Z

    iget-boolean v3, p1, Lhcj;->a:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhcj;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcj;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 21743
    goto :goto_0

    .line 21742
    :cond_4
    iget-object v2, p0, Lhcj;->I:Ljava/util/List;

    iget-object v3, p1, Lhcj;->I:Ljava/util/List;

    .line 21743
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 21747
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 21749
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhcj;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 21750
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhcj;->I:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 21751
    return v0

    .line 21749
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 21750
    :cond_1
    iget-object v0, p0, Lhcj;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_1
.end method
