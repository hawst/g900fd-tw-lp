.class public final Lhjc;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhjc;


# instance fields
.field public b:I

.field public c:[Lhje;

.field public d:[Lhjs;

.field public e:Lhjg;

.field private f:Lhjd;

.field private g:I

.field private h:J

.field private i:J

.field private j:Lhjf;

.field private k:Lhjn;

.field private l:I

.field private m:Lhjp;

.field private n:Lhjt;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhjc;

    sput-object v0, Lhjc;->a:[Lhjc;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v2, p0, Lhjc;->b:I

    iput-object v1, p0, Lhjc;->f:Lhjd;

    iput v2, p0, Lhjc;->g:I

    iput-wide v4, p0, Lhjc;->h:J

    iput-wide v4, p0, Lhjc;->i:J

    sget-object v0, Lhje;->a:[Lhje;

    iput-object v0, p0, Lhjc;->c:[Lhje;

    sget-object v0, Lhjs;->a:[Lhjs;

    iput-object v0, p0, Lhjc;->d:[Lhjs;

    iput-object v1, p0, Lhjc;->j:Lhjf;

    iput-object v1, p0, Lhjc;->k:Lhjn;

    iput-object v1, p0, Lhjc;->e:Lhjg;

    iput v2, p0, Lhjc;->l:I

    iput-object v1, p0, Lhjc;->m:Lhjp;

    iput-object v1, p0, Lhjc;->n:Lhjt;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 8

    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    iget v0, p0, Lhjc;->b:I

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    iget v2, p0, Lhjc;->b:I

    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhjc;->f:Lhjd;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhjc;->f:Lhjd;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Lhjc;->g:I

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Lhjc;->g:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-wide v2, p0, Lhjc;->h:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-wide v4, p0, Lhjc;->h:J

    invoke-static {v2, v4, v5}, Lidd;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-wide v2, p0, Lhjc;->i:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-wide v4, p0, Lhjc;->i:J

    invoke-static {v2, v4, v5}, Lidd;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhjc;->c:[Lhje;

    if-eqz v2, :cond_5

    iget-object v3, p0, Lhjc;->c:[Lhje;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    if-eqz v5, :cond_4

    const/4 v6, 0x6

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhjc;->d:[Lhjs;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lhjc;->d:[Lhjs;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    if-eqz v4, :cond_6

    const/4 v5, 0x7

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lhjc;->j:Lhjf;

    if-eqz v1, :cond_8

    const/16 v1, 0x8

    iget-object v2, p0, Lhjc;->j:Lhjf;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhjc;->k:Lhjn;

    if-eqz v1, :cond_9

    const/16 v1, 0x9

    iget-object v2, p0, Lhjc;->k:Lhjn;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lhjc;->e:Lhjg;

    if-eqz v1, :cond_a

    const/16 v1, 0xa

    iget-object v2, p0, Lhjc;->e:Lhjg;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lhjc;->l:I

    if-eqz v1, :cond_b

    const/16 v1, 0xb

    iget v2, p0, Lhjc;->l:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lhjc;->m:Lhjp;

    if-eqz v1, :cond_c

    const/16 v1, 0xc

    iget-object v2, p0, Lhjc;->m:Lhjp;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lhjc;->n:Lhjt;

    if-eqz v1, :cond_d

    const/16 v1, 0xd

    iget-object v2, p0, Lhjc;->n:Lhjt;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Lhjc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhjc;->J:I

    return v0

    :cond_e
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhjc;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhjc;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhjc;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    :cond_2
    iput v0, p0, Lhjc;->b:I

    goto :goto_0

    :cond_3
    iput v1, p0, Lhjc;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhjc;->f:Lhjd;

    if-nez v0, :cond_4

    new-instance v0, Lhjd;

    invoke-direct {v0}, Lhjd;-><init>()V

    iput-object v0, p0, Lhjc;->f:Lhjd;

    :cond_4
    iget-object v0, p0, Lhjc;->f:Lhjd;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhjc;->g:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhjc;->h:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhjc;->i:J

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhjc;->c:[Lhje;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhje;

    iget-object v3, p0, Lhjc;->c:[Lhje;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lhjc;->c:[Lhje;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lhjc;->c:[Lhje;

    :goto_2
    iget-object v2, p0, Lhjc;->c:[Lhje;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lhjc;->c:[Lhje;

    new-instance v3, Lhje;

    invoke-direct {v3}, Lhje;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhjc;->c:[Lhje;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lhjc;->c:[Lhje;

    array-length v0, v0

    goto :goto_1

    :cond_7
    iget-object v2, p0, Lhjc;->c:[Lhje;

    new-instance v3, Lhje;

    invoke-direct {v3}, Lhje;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhjc;->c:[Lhje;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhjc;->d:[Lhjs;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhjs;

    iget-object v3, p0, Lhjc;->d:[Lhjs;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lhjc;->d:[Lhjs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lhjc;->d:[Lhjs;

    :goto_4
    iget-object v2, p0, Lhjc;->d:[Lhjs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lhjc;->d:[Lhjs;

    new-instance v3, Lhjs;

    invoke-direct {v3}, Lhjs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhjc;->d:[Lhjs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lhjc;->d:[Lhjs;

    array-length v0, v0

    goto :goto_3

    :cond_a
    iget-object v2, p0, Lhjc;->d:[Lhjs;

    new-instance v3, Lhjs;

    invoke-direct {v3}, Lhjs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhjc;->d:[Lhjs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhjc;->j:Lhjf;

    if-nez v0, :cond_b

    new-instance v0, Lhjf;

    invoke-direct {v0}, Lhjf;-><init>()V

    iput-object v0, p0, Lhjc;->j:Lhjf;

    :cond_b
    iget-object v0, p0, Lhjc;->j:Lhjf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhjc;->k:Lhjn;

    if-nez v0, :cond_c

    new-instance v0, Lhjn;

    invoke-direct {v0}, Lhjn;-><init>()V

    iput-object v0, p0, Lhjc;->k:Lhjn;

    :cond_c
    iget-object v0, p0, Lhjc;->k:Lhjn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhjc;->e:Lhjg;

    if-nez v0, :cond_d

    new-instance v0, Lhjg;

    invoke-direct {v0}, Lhjg;-><init>()V

    iput-object v0, p0, Lhjc;->e:Lhjg;

    :cond_d
    iget-object v0, p0, Lhjc;->e:Lhjg;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_e

    if-eq v0, v4, :cond_e

    if-ne v0, v5, :cond_f

    :cond_e
    iput v0, p0, Lhjc;->l:I

    goto/16 :goto_0

    :cond_f
    iput v1, p0, Lhjc;->l:I

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lhjc;->m:Lhjp;

    if-nez v0, :cond_10

    new-instance v0, Lhjp;

    invoke-direct {v0}, Lhjp;-><init>()V

    iput-object v0, p0, Lhjc;->m:Lhjp;

    :cond_10
    iget-object v0, p0, Lhjc;->m:Lhjp;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lhjc;->n:Lhjt;

    if-nez v0, :cond_11

    new-instance v0, Lhjt;

    invoke-direct {v0}, Lhjt;-><init>()V

    iput-object v0, p0, Lhjc;->n:Lhjt;

    :cond_11
    iget-object v0, p0, Lhjc;->n:Lhjt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    iget v1, p0, Lhjc;->b:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget v2, p0, Lhjc;->b:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_0
    iget-object v1, p0, Lhjc;->f:Lhjd;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhjc;->f:Lhjd;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget v1, p0, Lhjc;->g:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lhjc;->g:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    :cond_2
    iget-wide v2, p0, Lhjc;->h:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-wide v2, p0, Lhjc;->h:J

    invoke-virtual {p1, v1, v2, v3}, Lidd;->b(IJ)V

    :cond_3
    iget-wide v2, p0, Lhjc;->i:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-wide v2, p0, Lhjc;->i:J

    invoke-virtual {p1, v1, v2, v3}, Lidd;->b(IJ)V

    :cond_4
    iget-object v1, p0, Lhjc;->c:[Lhje;

    if-eqz v1, :cond_6

    iget-object v2, p0, Lhjc;->c:[Lhje;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    if-eqz v4, :cond_5

    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lhjc;->d:[Lhjs;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lhjc;->d:[Lhjs;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    if-eqz v3, :cond_7

    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lhjc;->j:Lhjf;

    if-eqz v0, :cond_9

    const/16 v0, 0x8

    iget-object v1, p0, Lhjc;->j:Lhjf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_9
    iget-object v0, p0, Lhjc;->k:Lhjn;

    if-eqz v0, :cond_a

    const/16 v0, 0x9

    iget-object v1, p0, Lhjc;->k:Lhjn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_a
    iget-object v0, p0, Lhjc;->e:Lhjg;

    if-eqz v0, :cond_b

    const/16 v0, 0xa

    iget-object v1, p0, Lhjc;->e:Lhjg;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_b
    iget v0, p0, Lhjc;->l:I

    if-eqz v0, :cond_c

    const/16 v0, 0xb

    iget v1, p0, Lhjc;->l:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_c
    iget-object v0, p0, Lhjc;->m:Lhjp;

    if-eqz v0, :cond_d

    const/16 v0, 0xc

    iget-object v1, p0, Lhjc;->m:Lhjp;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_d
    iget-object v0, p0, Lhjc;->n:Lhjt;

    if-eqz v0, :cond_e

    const/16 v0, 0xd

    iget-object v1, p0, Lhjc;->n:Lhjt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_e
    iget-object v0, p0, Lhjc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhjc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhjc;

    iget v2, p0, Lhjc;->b:I

    iget v3, p1, Lhjc;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhjc;->f:Lhjd;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhjc;->f:Lhjd;

    if-nez v2, :cond_3

    :goto_1
    iget v2, p0, Lhjc;->g:I

    iget v3, p1, Lhjc;->g:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lhjc;->h:J

    iget-wide v4, p1, Lhjc;->h:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-wide v2, p0, Lhjc;->i:J

    iget-wide v4, p1, Lhjc;->i:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhjc;->c:[Lhje;

    iget-object v3, p1, Lhjc;->c:[Lhje;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhjc;->d:[Lhjs;

    iget-object v3, p1, Lhjc;->d:[Lhjs;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhjc;->j:Lhjf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhjc;->j:Lhjf;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhjc;->k:Lhjn;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhjc;->k:Lhjn;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhjc;->e:Lhjg;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhjc;->e:Lhjg;

    if-nez v2, :cond_3

    :goto_4
    iget v2, p0, Lhjc;->l:I

    iget v3, p1, Lhjc;->l:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhjc;->m:Lhjp;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhjc;->m:Lhjp;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhjc;->n:Lhjt;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhjc;->n:Lhjt;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhjc;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhjc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhjc;->f:Lhjd;

    iget-object v3, p1, Lhjc;->f:Lhjd;

    invoke-virtual {v2, v3}, Lhjd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhjc;->j:Lhjf;

    iget-object v3, p1, Lhjc;->j:Lhjf;

    invoke-virtual {v2, v3}, Lhjf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhjc;->k:Lhjn;

    iget-object v3, p1, Lhjc;->k:Lhjn;

    invoke-virtual {v2, v3}, Lhjn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhjc;->e:Lhjg;

    iget-object v3, p1, Lhjc;->e:Lhjg;

    invoke-virtual {v2, v3}, Lhjg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhjc;->m:Lhjp;

    iget-object v3, p1, Lhjc;->m:Lhjp;

    invoke-virtual {v2, v3}, Lhjp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhjc;->n:Lhjt;

    iget-object v3, p1, Lhjc;->n:Lhjt;

    invoke-virtual {v2, v3}, Lhjt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhjc;->I:Ljava/util/List;

    iget-object v3, p1, Lhjc;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/16 v6, 0x20

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhjc;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhjc;->f:Lhjd;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhjc;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhjc;->h:J

    iget-wide v4, p0, Lhjc;->h:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhjc;->i:J

    iget-wide v4, p0, Lhjc;->i:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lhjc;->c:[Lhje;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhjc;->d:[Lhjs;

    if-nez v0, :cond_5

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhjc;->j:Lhjf;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhjc;->k:Lhjn;

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhjc;->e:Lhjg;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhjc;->l:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhjc;->m:Lhjp;

    if-nez v0, :cond_a

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhjc;->n:Lhjt;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhjc;->I:Ljava/util/List;

    if-nez v2, :cond_c

    :goto_6
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Lhjc;->f:Lhjd;

    invoke-virtual {v0}, Lhjd;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    move v2, v0

    move v0, v1

    :goto_7
    iget-object v3, p0, Lhjc;->c:[Lhje;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhjc;->c:[Lhje;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_8
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_4
    iget-object v2, p0, Lhjc;->c:[Lhje;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhje;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_5
    move v0, v1

    :goto_9
    iget-object v3, p0, Lhjc;->d:[Lhjs;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhjc;->d:[Lhjs;

    aget-object v2, v2, v0

    if-nez v2, :cond_6

    move v2, v1

    :goto_a
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_6
    iget-object v2, p0, Lhjc;->d:[Lhjs;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhjs;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_7
    iget-object v0, p0, Lhjc;->j:Lhjf;

    invoke-virtual {v0}, Lhjf;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lhjc;->k:Lhjn;

    invoke-virtual {v0}, Lhjn;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lhjc;->e:Lhjg;

    invoke-virtual {v0}, Lhjg;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_a
    iget-object v0, p0, Lhjc;->m:Lhjp;

    invoke-virtual {v0}, Lhjp;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lhjc;->n:Lhjt;

    invoke-virtual {v0}, Lhjt;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_c
    iget-object v1, p0, Lhjc;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_6
.end method
