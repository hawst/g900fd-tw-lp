.class public final Lhet;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:J

.field private b:Lhgz;

.field private c:Lhgz;

.field private d:Lhiq;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41360
    invoke-direct {p0}, Lidf;-><init>()V

    .line 41363
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhet;->a:J

    .line 41366
    iput-object v2, p0, Lhet;->b:Lhgz;

    .line 41369
    iput-object v2, p0, Lhet;->c:Lhgz;

    .line 41372
    iput-object v2, p0, Lhet;->d:Lhiq;

    .line 41360
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    .line 41427
    const/4 v0, 0x0

    .line 41428
    iget-wide v2, p0, Lhet;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 41429
    const/4 v0, 0x1

    iget-wide v2, p0, Lhet;->a:J

    .line 41430
    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 41432
    :cond_0
    iget-object v1, p0, Lhet;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 41433
    const/4 v1, 0x2

    iget-object v2, p0, Lhet;->b:Lhgz;

    .line 41434
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41436
    :cond_1
    iget-object v1, p0, Lhet;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 41437
    const/4 v1, 0x3

    iget-object v2, p0, Lhet;->c:Lhgz;

    .line 41438
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41440
    :cond_2
    iget-object v1, p0, Lhet;->d:Lhiq;

    if-eqz v1, :cond_3

    .line 41441
    const/4 v1, 0x4

    iget-object v2, p0, Lhet;->d:Lhiq;

    .line 41442
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41444
    :cond_3
    iget-object v1, p0, Lhet;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41445
    iput v0, p0, Lhet;->J:I

    .line 41446
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 41356
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhet;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhet;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhet;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhet;->a:J

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhet;->b:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhet;->b:Lhgz;

    :cond_2
    iget-object v0, p0, Lhet;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhet;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhet;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lhet;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhet;->d:Lhiq;

    if-nez v0, :cond_4

    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    iput-object v0, p0, Lhet;->d:Lhiq;

    :cond_4
    iget-object v0, p0, Lhet;->d:Lhiq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 4

    .prologue
    .line 41409
    iget-wide v0, p0, Lhet;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 41410
    const/4 v0, 0x1

    iget-wide v2, p0, Lhet;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 41412
    :cond_0
    iget-object v0, p0, Lhet;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 41413
    const/4 v0, 0x2

    iget-object v1, p0, Lhet;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41415
    :cond_1
    iget-object v0, p0, Lhet;->c:Lhgz;

    if-eqz v0, :cond_2

    .line 41416
    const/4 v0, 0x3

    iget-object v1, p0, Lhet;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41418
    :cond_2
    iget-object v0, p0, Lhet;->d:Lhiq;

    if-eqz v0, :cond_3

    .line 41419
    const/4 v0, 0x4

    iget-object v1, p0, Lhet;->d:Lhiq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41421
    :cond_3
    iget-object v0, p0, Lhet;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 41423
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41386
    if-ne p1, p0, :cond_1

    .line 41393
    :cond_0
    :goto_0
    return v0

    .line 41387
    :cond_1
    instance-of v2, p1, Lhet;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 41388
    :cond_2
    check-cast p1, Lhet;

    .line 41389
    iget-wide v2, p0, Lhet;->a:J

    iget-wide v4, p1, Lhet;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhet;->b:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhet;->b:Lhgz;

    if-nez v2, :cond_3

    .line 41390
    :goto_1
    iget-object v2, p0, Lhet;->c:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhet;->c:Lhgz;

    if-nez v2, :cond_3

    .line 41391
    :goto_2
    iget-object v2, p0, Lhet;->d:Lhiq;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhet;->d:Lhiq;

    if-nez v2, :cond_3

    .line 41392
    :goto_3
    iget-object v2, p0, Lhet;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhet;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 41393
    goto :goto_0

    .line 41389
    :cond_4
    iget-object v2, p0, Lhet;->b:Lhgz;

    iget-object v3, p1, Lhet;->b:Lhgz;

    .line 41390
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhet;->c:Lhgz;

    iget-object v3, p1, Lhet;->c:Lhgz;

    .line 41391
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhet;->d:Lhiq;

    iget-object v3, p1, Lhet;->d:Lhiq;

    .line 41392
    invoke-virtual {v2, v3}, Lhiq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhet;->I:Ljava/util/List;

    iget-object v3, p1, Lhet;->I:Ljava/util/List;

    .line 41393
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 41397
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 41399
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhet;->a:J

    iget-wide v4, p0, Lhet;->a:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 41400
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhet;->b:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 41401
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhet;->c:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 41402
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhet;->d:Lhiq;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 41403
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhet;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 41404
    return v0

    .line 41400
    :cond_0
    iget-object v0, p0, Lhet;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 41401
    :cond_1
    iget-object v0, p0, Lhet;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 41402
    :cond_2
    iget-object v0, p0, Lhet;->d:Lhiq;

    invoke-virtual {v0}, Lhiq;->hashCode()I

    move-result v0

    goto :goto_2

    .line 41403
    :cond_3
    iget-object v1, p0, Lhet;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
