.class public Ldef;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcwz;


# instance fields
.field private final a:Ljava/util/List;

.field public b:Ldeh;

.field public c:Z

.field private final d:Ljava/util/List;

.field private e:Lcxa;

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ldef;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldef;->a:Ljava/util/List;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldef;->d:Ljava/util/List;

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldef;->setFocusable(Z)V

    .line 71
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Ldef;->setDescendantFocusability(I)V

    .line 72
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)Ldeg;
    .locals 2

    .prologue
    .line 204
    new-instance v0, Ldeg;

    invoke-virtual {p0}, Ldef;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ldeg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Ldef;->e:Lcxa;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Ldef;->e:Lcxa;

    invoke-interface {v0}, Lcxa;->a()V

    .line 140
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x2

    .line 75
    iget-object v0, p0, Ldef;->f:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "videoView has already been set"

    invoke-static {v0, v2}, Lb;->d(ZLjava/lang/Object;)V

    .line 76
    iput-object p1, p0, Ldef;->f:Landroid/view/View;

    .line 77
    new-instance v0, Ldeg;

    invoke-direct {v0, v3, v3}, Ldeg;-><init>(II)V

    .line 78
    const/16 v2, 0x11

    iput v2, v0, Ldeg;->gravity:I

    .line 79
    invoke-virtual {p0, p1, v1, v0}, Ldef;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 80
    return-void

    :cond_0
    move v0, v1

    .line 75
    goto :goto_0
.end method

.method public final a(Lcxa;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Ldef;->e:Lcxa;

    .line 128
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 259
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1c

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "FSUI enableTouchEvents "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Ldef;->b:Ldeh;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Ldef;->b:Ldeh;

    invoke-interface {v0, p1}, Ldeh;->a(Z)V

    .line 263
    :cond_0
    return-void
.end method

.method public final varargs a([Ldec;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 104
    move v2, v3

    :goto_0
    array-length v0, p1

    if-ge v2, v0, :cond_3

    .line 105
    aget-object v1, p1, v2

    .line 106
    invoke-interface {v1}, Ldec;->f_()Landroid/view/View;

    move-result-object v5

    .line 107
    if-nez v5, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Overlay "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " does not provide a View and LayoutParams"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_0
    invoke-interface {v1}, Ldec;->r_()Ldeg;

    move-result-object v6

    .line 112
    instance-of v0, v1, Lddx;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 113
    check-cast v0, Lddx;

    .line 114
    iget-boolean v7, p0, Ldef;->c:Z

    invoke-interface {v0, v7}, Lddx;->c_(Z)V

    .line 115
    invoke-interface {v1, v4}, Ldec;->a_(Z)V

    .line 116
    iget-object v1, p0, Ldef;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    :goto_1
    invoke-virtual {p0, v5, v6}, Ldef;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 118
    :cond_1
    iget-boolean v0, p0, Ldef;->c:Z

    if-nez v0, :cond_2

    move v0, v4

    :goto_2
    invoke-interface {v1, v0}, Ldec;->a_(Z)V

    .line 119
    iget-object v0, p0, Ldef;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v3

    .line 118
    goto :goto_2

    .line 123
    :cond_3
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 88
    if-nez p1, :cond_0

    iget-boolean v0, p0, Ldef;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Ldef;->c(Z)V

    .line 89
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(Z)V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Ldef;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldec;

    .line 93
    if-nez p1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v0, v1}, Ldec;->a_(Z)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 95
    :cond_1
    iget-object v0, p0, Ldef;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddx;

    .line 96
    invoke-interface {v0, p1}, Lddx;->c_(Z)V

    goto :goto_2

    .line 98
    :cond_2
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 219
    instance-of v0, p1, Ldeg;

    return v0
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Ldef;->b:Ldeh;

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 152
    iget-object v1, p0, Ldef;->b:Ldeh;

    invoke-interface {v1, v0}, Ldeh;->a(Landroid/graphics/Rect;)V

    .line 154
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Ldef;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 214
    new-instance v0, Ldeg;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ldef;->a(Landroid/util/AttributeSet;)Ldeg;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 209
    new-instance v0, Ldeg;

    invoke-direct {v0, p1}, Ldeg;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ldef;->a(Landroid/util/AttributeSet;)Ldeg;

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/high16 v7, -0x80000000

    const v6, 0x3fe374bc    # 1.777f

    const/high16 v5, 0x40000000    # 2.0f

    .line 159
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 160
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 161
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 162
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 166
    if-ne v3, v5, :cond_0

    if-ne v4, v5, :cond_0

    move v1, v2

    .line 193
    :goto_0
    invoke-static {v1, p1}, Ldef;->resolveSize(II)I

    move-result v1

    .line 194
    invoke-static {v0, p2}, Ldef;->resolveSize(II)I

    move-result v0

    .line 196
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 197
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 199
    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 200
    return-void

    .line 170
    :cond_0
    if-eq v3, v5, :cond_1

    if-ne v3, v7, :cond_2

    if-nez v4, :cond_2

    .line 173
    :cond_1
    int-to-float v0, v2

    div-float/2addr v0, v6

    float-to-int v0, v0

    move v1, v2

    goto :goto_0

    .line 174
    :cond_2
    if-eq v4, v5, :cond_3

    if-ne v4, v7, :cond_4

    if-nez v3, :cond_4

    .line 177
    :cond_3
    int-to-float v1, v0

    mul-float/2addr v1, v6

    float-to-int v1, v1

    goto :goto_0

    .line 178
    :cond_4
    if-ne v3, v7, :cond_6

    if-ne v4, v7, :cond_6

    .line 179
    int-to-float v1, v0

    int-to-float v3, v2

    div-float/2addr v3, v6

    cmpg-float v1, v1, v3

    if-gez v1, :cond_5

    .line 180
    int-to-float v1, v0

    mul-float/2addr v1, v6

    float-to-int v1, v1

    .line 181
    goto :goto_0

    .line 184
    :cond_5
    int-to-float v0, v2

    div-float/2addr v0, v6

    float-to-int v0, v0

    move v1, v2

    goto :goto_0

    :cond_6
    move v0, v1

    .line 189
    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 132
    invoke-virtual {p0}, Ldef;->a()V

    .line 133
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
