.class public abstract Laah;
.super Likt;
.source "SourceFile"

# interfaces
.implements Lyn;


# instance fields
.field public a:Ljava/util/List;

.field private b:I

.field private c:Lyb;


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Likt;-><init>(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Laah;->b:I

    .line 48
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Laah;->a:Ljava/util/List;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Ljava/util/List;
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Laah;->a(Ljava/lang/Class;Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Z)Ljava/util/List;
    .locals 4

    .prologue
    .line 88
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    iget-object v0, p0, Laah;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 90
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 91
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_1
    if-eqz p2, :cond_0

    instance-of v3, v0, Lyn;

    if-eqz v3, :cond_0

    .line 95
    check-cast v0, Lyn;

    invoke-interface {v0, p1, p2}, Lyn;->a(Ljava/lang/Class;Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 99
    :cond_2
    return-object v1
.end method

.method public final a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;JLyb;)V
    .locals 1

    .prologue
    .line 110
    iput-object p5, p0, Laah;->c:Lyb;

    .line 111
    invoke-super/range {p0 .. p5}, Likt;->a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;JLyb;)V

    .line 113
    return-void
.end method

.method public final c(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x6

    new-array v0, v0, [B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 118
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laah;->b:I

    .line 119
    return-void
.end method

.method public final d(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 122
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/16 v1, 0x8

    if-le v0, v1, :cond_0

    .line 124
    :try_start_0
    iget-object v0, p0, Laah;->a:Ljava/util/List;

    iget-object v1, p0, Laah;->c:Lyb;

    new-instance v2, Lili;

    invoke-direct {v2, p1}, Lili;-><init>(Ljava/nio/ByteBuffer;)V

    invoke-interface {v1, v2, p0}, Lyb;->a(Ljava/nio/channels/ReadableByteChannel;Lyn;)Lyj;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 130
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Likt;->n:Ljava/nio/ByteBuffer;

    .line 131
    return-void
.end method

.method public final e(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x6

    new-array v0, v0, [B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 135
    iget v0, p0, Laah;->b:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 136
    return-void
.end method

.method public final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Laah;->a:Ljava/util/List;

    return-object v0
.end method

.method public final f(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 139
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 140
    invoke-static {v1}, Ljava/nio/channels/Channels;->newChannel(Ljava/io/OutputStream;)Ljava/nio/channels/WritableByteChannel;

    move-result-object v2

    .line 142
    :try_start_0
    iget-object v0, p0, Laah;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 143
    invoke-interface {v0, v2}, Lyj;->a(Ljava/nio/channels/WritableByteChannel;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot happen. Everything should be in memory and therefore no exceptions."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    :try_start_1
    invoke-interface {v2}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 149
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 150
    return-void
.end method
