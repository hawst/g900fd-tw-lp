.class final Lbkt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final a:Landroid/telephony/TelephonyManager;

.field private final b:Lgmt;

.field private final c:Lfxe;

.field private final d:Lbla;

.field private final e:Lgml;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lgmt;Lfxe;Lbla;)V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lbkt;->a:Landroid/telephony/TelephonyManager;

    .line 69
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmt;

    iput-object v0, p0, Lbkt;->b:Lgmt;

    .line 70
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbkt;->c:Lfxe;

    .line 71
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbla;

    iput-object v0, p0, Lbkt;->d:Lbla;

    .line 72
    iget-object v0, p5, Lbla;->i:Lgml;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgml;

    iput-object v0, p0, Lbkt;->e:Lgml;

    .line 73
    return-void
.end method

.method private a(Lblm;)V
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 312
    iget-object v3, p1, Lblm;->a:Ljava/lang/String;

    .line 317
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0, v3}, Lgml;->e(Ljava/lang/String;)Lgbu;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_1

    .line 321
    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->b(Lgbu;)V

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    :try_start_0
    invoke-direct {p0, v3}, Lbkt;->g(Ljava/lang/String;)Lgbu;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 338
    invoke-virtual {v1}, Lgbu;->a()Lgbv;

    move-result-object v0

    const/4 v2, 0x1

    .line 339
    iput-boolean v2, v0, Lgbv;->n:Z

    .line 340
    invoke-virtual {v0}, Lgbv;->a()Lgbu;

    move-result-object v4

    .line 342
    iget-object v0, p0, Lbkt;->e:Lgml;

    iget-object v2, p1, Lblm;->b:Lbjy;

    .line 343
    invoke-virtual {v2}, Lbjy;->a()I

    move-result v2

    invoke-virtual {v0, v4, v2}, Lgml;->a(Lgbu;I)Z

    move-result v0

    .line 344
    if-nez v0, :cond_2

    .line 345
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed inserting playlist placeholder "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v3}, Lbla;->z(Ljava/lang/String;)V

    goto :goto_0

    .line 330
    :catch_0
    move-exception v0

    .line 331
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed requesting playlist "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for offline"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 332
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v3}, Lbla;->z(Ljava/lang/String;)V

    goto :goto_0

    .line 350
    :cond_2
    :try_start_1
    iget-object v0, p0, Lbkt;->b:Lgmt;

    invoke-virtual {v0, v1}, Lgmt;->a(Lgbu;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    iget-object v0, v1, Lgbu;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lbkt;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 351
    :goto_2
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v4}, Lbla;->b(Lgbu;)V

    .line 357
    :try_start_3
    invoke-direct {p0, v3}, Lbkt;->h(Ljava/lang/String;)Ljava/util/List;
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v2

    .line 368
    iget v0, v1, Lgbu;->j:I

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-eq v0, v4, :cond_8

    .line 369
    invoke-virtual {v1}, Lgbu;->a()Lgbv;

    move-result-object v0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Lgbv;->l:I

    invoke-virtual {v0}, Lgbv;->a()Lgbu;

    move-result-object v0

    .line 373
    :goto_3
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 374
    invoke-direct {p0, v2, v1}, Lbkt;->a(Ljava/util/List;Ljava/util/HashSet;)V

    .line 376
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 377
    iget-object v5, p0, Lbkt;->e:Lgml;

    iget-object v6, p1, Lblm;->b:Lbjy;

    .line 381
    invoke-virtual {v6}, Lbjy;->a()I

    move-result v6

    .line 377
    invoke-virtual {v5, v0, v2, v4, v6}, Lgml;->a(Lgbu;Ljava/util/List;Ljava/util/List;I)Z

    move-result v4

    .line 382
    if-nez v4, :cond_5

    .line 383
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed inserting playlist "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 384
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0, v3, v7}, Lgml;->a(Ljava/lang/String;Ljava/util/List;)Z

    .line 385
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v3}, Lbla;->B(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 350
    :catch_1
    move-exception v0

    move-object v2, v0

    const-string v5, "Failed saving playlist thumbnail for "

    iget-object v0, v1, Lgbu;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v0, v2}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :catch_2
    move-exception v0

    move-object v2, v0

    const-string v5, "Failed saving avatar for "

    iget-object v0, v1, Lgbu;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0, v2}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 358
    :catch_3
    move-exception v0

    .line 359
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed requesting playlist videos "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for offline"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 361
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0, v3, v7}, Lgml;->a(Ljava/lang/String;Ljava/util/List;)Z

    .line 362
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v3}, Lbla;->B(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 390
    :cond_5
    invoke-direct {p0, v2}, Lbkt;->a(Ljava/util/List;)V

    .line 395
    iget-object v4, p0, Lbkt;->d:Lbla;

    iget-object v5, v4, Lbla;->m:Lbmd;

    invoke-virtual {v5, v0, v1}, Lbmd;->a(Lgbu;Ljava/util/Collection;)Lbme;

    move-result-object v5

    iget-object v6, v4, Lbla;->n:Ljava/util/Map;

    iget-object v7, v0, Lgbu;->a:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, v0, Lgbu;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x18

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "pudl event playlist "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " add"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lezp;->d(Ljava/lang/String;)V

    new-instance v6, Lblr;

    invoke-virtual {v5}, Lbme;->b()Lglw;

    move-result-object v7

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Lblr;-><init>(Lglw;Z)V

    invoke-virtual {v4, v6}, Lbla;->a(Ljava/lang/Object;)V

    invoke-virtual {v5}, Lbme;->a()I

    move-result v5

    if-nez v5, :cond_6

    iget-object v4, v4, Lbla;->m:Lbmd;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lbmd;->b(Ljava/lang/String;)V

    .line 398
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 399
    iget-object v4, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 400
    iget-object v4, p1, Lblm;->b:Lbjy;

    iget-object v5, p1, Lblm;->c:[B

    invoke-direct {p0, v3, v0, v4, v5}, Lbkt;->a(Ljava/lang/String;Lgcd;Lbjy;[B)V

    goto :goto_6

    :cond_8
    move-object v0, v1

    goto/16 :goto_3
.end method

.method private a(Lgcd;)V
    .locals 4

    .prologue
    .line 507
    :try_start_0
    iget-object v2, p0, Lbkt;->b:Lgmt;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lb;->b()V

    iget-object v0, p1, Lgcd;->g:Landroid/net/Uri;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lgcd;->g:Landroid/net/Uri;

    move-object v1, v0

    :goto_0
    iget-object v0, p1, Lgcd;->e:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lgcd;->e:Landroid/net/Uri;

    :goto_1
    if-eqz v1, :cond_0

    iget-object v3, p1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lgmt;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-static {v3}, La;->d(Ljava/io/File;)V

    invoke-virtual {v2, v1, v3}, Lgmt;->a(Landroid/net/Uri;Ljava/io/File;)V

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lgmt;->e(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, La;->d(Ljava/io/File;)V

    invoke-virtual {v2, v0, v1}, Lgmt;->a(Landroid/net/Uri;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    :cond_1
    :goto_2
    :try_start_1
    iget-object v0, p1, Lgcd;->o:Ljava/lang/String;

    invoke-direct {p0, v0}, Lbkt;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 520
    :goto_3
    return-void

    .line 507
    :cond_2
    :try_start_2
    iget-object v0, p1, Lgcd;->f:Landroid/net/Uri;

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p1, Lgcd;->d:Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 508
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 510
    const-string v2, "Failed saving video thumbnails for "

    iget-object v0, p1, Lgcd;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v0, v1}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 516
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 518
    const-string v2, "Failed saving avatar for "

    iget-object v0, p1, Lgcd;->o:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0, v1}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5
.end method

.method private a(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 406
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0, p1}, Lgml;->e(Ljava/lang/String;)Lgbu;

    move-result-object v0

    .line 407
    if-nez v0, :cond_1

    .line 409
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, p1}, Lbla;->A(Ljava/lang/String;)V

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, v0, Lgml;->f:Lgnp;

    invoke-virtual {v0, p1}, Lgnp;->g(Ljava/lang/String;)I

    move-result v3

    .line 415
    invoke-static {v3}, Lbjy;->a(I)Lbjy;

    move-result-object v4

    .line 421
    :try_start_0
    invoke-direct {p0, p1}, Lbkt;->g(Ljava/lang/String;)Lgbu;

    move-result-object v0

    .line 422
    invoke-direct {p0, p1}, Lbkt;->h(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 428
    iget v1, v0, Lgbu;->j:I

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_7

    .line 430
    const-string v1, "Playlist size doesn\'t match number of playlist videos"

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    .line 431
    invoke-virtual {v0}, Lgbu;->a()Lgbv;

    move-result-object v0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Lgbv;->l:I

    invoke-virtual {v0}, Lgbv;->a()Lgbu;

    move-result-object v0

    move-object v2, v0

    .line 436
    :goto_1
    :try_start_1
    iget-object v0, p0, Lbkt;->b:Lgmt;

    invoke-virtual {v0, v2}, Lgmt;->a(Lgbu;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 442
    :goto_2
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 443
    invoke-direct {p0, v5, v1}, Lbkt;->a(Ljava/util/List;Ljava/util/HashSet;)V

    .line 446
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 447
    iget-object v6, p0, Lbkt;->e:Lgml;

    invoke-virtual {v6, v2, v5, v0, v3}, Lgml;->a(Lgbu;Ljava/util/List;Ljava/util/List;I)Z

    move-result v3

    .line 452
    if-eqz v3, :cond_3

    .line 454
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 455
    invoke-direct {p0, v0}, Lbkt;->c(Ljava/lang/String;)V

    goto :goto_3

    .line 423
    :catch_0
    move-exception v0

    .line 424
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed requesting playlist "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for offline"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 425
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, p1}, Lbla;->A(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 437
    :catch_1
    move-exception v0

    move-object v1, v0

    .line 439
    const-string v6, "Failed saving playlist thumbnail for "

    iget-object v0, v2, Lgbu;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v0, v1}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 459
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed syncing playlist "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 460
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, p1}, Lbla;->A(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 465
    :cond_4
    invoke-direct {p0, v5}, Lbkt;->a(Ljava/util/List;)V

    .line 470
    iget-object v0, p0, Lbkt;->d:Lbla;

    iget-object v3, v0, Lbla;->m:Lbmd;

    invoke-virtual {v3, v2, v1}, Lbmd;->a(Lgbu;Ljava/util/Collection;)Lbme;

    move-result-object v3

    iget-object v6, v0, Lbla;->n:Ljava/util/Map;

    iget-object v7, v2, Lgbu;->a:Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v6, v2, Lgbu;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x19

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "pudl event playlist "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " sync"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lezp;->d(Ljava/lang/String;)V

    new-instance v6, Lbls;

    invoke-virtual {v3}, Lbme;->b()Lglw;

    move-result-object v7

    invoke-direct {v6, v7}, Lbls;-><init>(Lglw;)V

    invoke-virtual {v0, v6}, Lbla;->a(Ljava/lang/Object;)V

    invoke-virtual {v3}, Lbme;->a()I

    move-result v3

    if-nez v3, :cond_5

    iget-object v0, v0, Lbla;->m:Lbmd;

    iget-object v2, v2, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lbmd;->b(Ljava/lang/String;)V

    .line 473
    :cond_5
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 474
    iget-object v3, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 475
    sget-object v3, Lfhy;->a:[B

    invoke-direct {p0, p1, v0, v4, v3}, Lbkt;->a(Ljava/lang/String;Lgcd;Lbjy;[B)V

    goto :goto_5

    :cond_7
    move-object v2, v0

    goto/16 :goto_1
.end method

.method private a(Ljava/lang/String;Lgcd;Lbjy;[B)V
    .locals 4

    .prologue
    .line 603
    iget-object v0, p0, Lbkt;->d:Lbla;

    iget-object v1, p2, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, p3, p4}, Lbla;->a(Ljava/lang/String;Ljava/lang/String;Lbjy;[B)V

    .line 605
    sget-object v1, Lfhy;->a:[B

    iget-object v0, p0, Lbkt;->a:Landroid/telephony/TelephonyManager;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v0, v2}, Lfaq;->a(Landroid/telephony/TelephonyManager;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p2, Lgcd;->E:Ljava/util/Set;

    if-eqz v2, :cond_2

    iget-boolean v2, p2, Lgcd;->D:Z

    iget-object v3, p2, Lgcd;->E:Ljava/util/Set;

    invoke-static {v0}, La;->z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eq v2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lbkt;->d:Lbla;

    iget-object v2, p2, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, p3, v1}, Lbla;->a(Ljava/lang/String;Lbjy;[B)V

    .line 606
    :cond_0
    return-void

    .line 605
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-boolean v0, p2, Lgcd;->D:Z

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0, p1, p2}, Lgml;->a(Ljava/lang/String;Z)Z

    move-result v0

    .line 304
    if-eqz v0, :cond_0

    .line 305
    invoke-direct {p0, p1}, Lbkt;->c(Ljava/lang/String;)V

    .line 309
    :goto_0
    return-void

    .line 307
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed removing video "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 541
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 542
    invoke-direct {p0, v0}, Lbkt;->a(Lgcd;)V

    goto :goto_0

    .line 544
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/HashSet;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 555
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 565
    iget-object v1, p0, Lbkt;->e:Lgml;

    iget-object v5, v0, Lgcd;->b:Ljava/lang/String;

    invoke-static {v5}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, v1, Lgml;->f:Lgnp;

    iget-object v1, v1, Lgnp;->a:Levi;

    invoke-interface {v1}, Levi;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const-string v6, "videos"

    const-string v7, "id = ? AND media_status = ?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    aput-object v5, v8, v3

    sget-object v5, Lglv;->a:Lglv;

    iget v5, v5, Lglv;->j:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v2

    invoke-static {v1, v6, v7, v8}, Levj;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-lez v1, :cond_3

    move v1, v2

    :goto_1
    if-nez v1, :cond_2

    .line 566
    iget-object v1, p0, Lbkt;->d:Lbla;

    iget-object v5, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v1

    .line 567
    if-eqz v1, :cond_1

    .line 568
    invoke-virtual {v1}, Lgmb;->j()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lgmb;->k()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 569
    :cond_1
    iget-object v1, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 575
    :cond_2
    iget-object v1, p0, Lbkt;->e:Lgml;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgml;->a(Ljava/lang/String;)Lgcd;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_3
    move v1, v3

    .line 565
    goto :goto_1

    .line 580
    :cond_4
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 482
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 483
    iget-object v1, p0, Lbkt;->e:Lgml;

    invoke-virtual {v1, p1, v0}, Lgml;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, p1}, Lbla;->B(Ljava/lang/String;)V

    .line 488
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 489
    invoke-direct {p0, v0}, Lbkt;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 492
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x27

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed removing playlist "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 494
    :cond_1
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 498
    iget-object v0, p0, Lbkt;->d:Lbla;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "pudl event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " delete"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    new-instance v1, Lbmb;

    invoke-direct {v1, p1}, Lbmb;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbla;->a(Ljava/lang/Object;)V

    .line 501
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0, p1}, Lgml;->k(Ljava/lang/String;)V

    .line 502
    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 584
    if-nez p1, :cond_1

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 589
    :cond_1
    iget-object v0, p0, Lbkt;->b:Lgmt;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0, p1}, Lgmt;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    iget-object v1, p0, Lbkt;->c:Lfxe;

    invoke-interface {v1, p1, v0}, Lfxe;->f(Ljava/lang/String;Leuc;)V

    invoke-virtual {v0}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgca;

    .line 591
    iget-object v1, v0, Lgca;->e:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 592
    iget-object v1, p0, Lbkt;->b:Lgmt;

    iget-object v0, v0, Lgca;->e:Landroid/net/Uri;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lb;->b()V

    invoke-virtual {v1, p1}, Lgmt;->c(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, La;->d(Ljava/io/File;)V

    invoke-virtual {v1, v0, v2}, Lgmt;->a(Landroid/net/Uri;Ljava/io/File;)V

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 631
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0, p1}, Lgml;->b(Ljava/lang/String;)I

    move-result v0

    .line 633
    invoke-static {v0}, Lbjy;->a(I)Lbjy;

    move-result-object v0

    .line 634
    iget-object v1, p0, Lbkt;->d:Lbla;

    const/4 v2, 0x0

    sget-object v3, Lfhy;->a:[B

    invoke-virtual {v1, v2, p1, v0, v3}, Lbla;->a(Ljava/lang/String;Ljava/lang/String;Lbjy;[B)V

    .line 636
    return-void
.end method

.method private f(Ljava/lang/String;)Lgcd;
    .locals 2

    .prologue
    .line 643
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    .line 644
    iget-object v1, p0, Lbkt;->c:Lfxe;

    invoke-interface {v1, p1, v0}, Lfxe;->d(Ljava/lang/String;Leuc;)V

    .line 645
    invoke-virtual {v0}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    return-object v0
.end method

.method private g(Ljava/lang/String;)Lgbu;
    .locals 2

    .prologue
    .line 649
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    .line 650
    iget-object v1, p0, Lbkt;->c:Lfxe;

    invoke-interface {v1, p1, v0}, Lfxe;->b(Ljava/lang/String;Leuc;)V

    .line 651
    invoke-virtual {v0}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    return-object v0
.end method

.method private h(Ljava/lang/String;)Ljava/util/List;
    .locals 2

    .prologue
    .line 655
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    .line 656
    iget-object v1, p0, Lbkt;->c:Lfxe;

    invoke-interface {v1, p1, v0}, Lfxe;->c(Ljava/lang/String;Leuc;)V

    .line 657
    invoke-virtual {v0}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 78
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0}, Lbla;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return v2

    .line 82
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 85
    :pswitch_0
    iget-object v3, p0, Lbkt;->d:Lbla;

    invoke-static {}, Lb;->b()V

    iget-object v0, v3, Lbla;->d:Lgit;

    iget-object v1, v0, Lgit;->c:Ljava/lang/String;

    iget-object v0, v3, Lbla;->g:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    iget-object v0, v0, Ldmz;->b:Ldmu;

    iget-object v0, v0, Ldmu;->d:Ldne;

    iget-object v0, v0, Ldne;->z:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v3, Lbla;->g:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0}, Ldmz;->a()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjm;

    invoke-static {v0}, La;->c(Lgjm;)Ljava/lang/String;

    move-result-object v5

    iget-object v1, v3, Lbla;->i:Lgml;

    invoke-virtual {v1, v5, v7}, Lgml;->a(Ljava/lang/String;Lgni;)Lgly;

    move-result-object v1

    invoke-virtual {v1}, Lgly;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v3, Lbla;->i:Lgml;

    invoke-virtual {v1, v5, v0}, Lgml;->a(Ljava/lang/String;Lgjm;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lgjm;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3, v5}, Lbla;->y(Ljava/lang/String;)V

    iget-object v0, v3, Lbla;->i:Lgml;

    invoke-static {v5}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, v5}, Lgnm;->g(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, v3, Lbla;->m:Lbmd;

    invoke-virtual {v1, v0}, Lbmd;->a(Ljava/lang/String;)Lbme;

    move-result-object v1

    if-nez v1, :cond_12

    iget-object v1, v3, Lbla;->i:Lgml;

    invoke-virtual {v1, v0}, Lgml;->e(Ljava/lang/String;)Lgbu;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, v3, Lbla;->m:Lbmd;

    invoke-virtual {v1, v0, v7}, Lbmd;->a(Lgbu;Ljava/util/Collection;)Lbme;

    move-result-object v0

    :goto_2
    invoke-virtual {v0, v5}, Lbme;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v0, "pudl transfer playlist not in database"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v0, v3, Lbla;->m:Lbmd;

    invoke-virtual {v0}, Lbmd;->a()V

    iget-object v0, v3, Lbla;->i:Lgml;

    invoke-virtual {v0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0}, Lgnm;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    invoke-virtual {v0}, Lgmb;->l()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3, v0}, Lbla;->a(Lgmb;)V

    goto :goto_3

    .line 90
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lblm;

    .line 91
    iget-object v3, v0, Lblm;->a:Ljava/lang/String;

    iget-object v4, p0, Lbkt;->d:Lbla;

    invoke-virtual {v4, v3}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v3

    if-eqz v3, :cond_6

    :goto_4
    if-nez v1, :cond_0

    .line 92
    iget-object v3, v0, Lblm;->a:Ljava/lang/String;

    iget-object v1, p0, Lbkt;->e:Lgml;

    invoke-virtual {v1, v3}, Lgml;->d(Ljava/lang/String;)Lgmb;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v3}, Lbla;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move v1, v2

    .line 91
    goto :goto_4

    .line 92
    :cond_7
    iget-object v1, p0, Lbkt;->e:Lgml;

    invoke-virtual {v1, v3}, Lgml;->a(Ljava/lang/String;)Lgcd;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v4, p0, Lbkt;->e:Lgml;

    sget-object v5, Lglv;->c:Lglv;

    iget-object v6, v0, Lblm;->b:Lbjy;

    invoke-virtual {v6}, Lbjy;->a()I

    move-result v6

    invoke-virtual {v4, v3, v5, v6}, Lgml;->a(Ljava/lang/String;Lglv;I)Z

    iget-object v4, p0, Lbkt;->e:Lgml;

    invoke-virtual {v4, v3}, Lgml;->g(Ljava/lang/String;)Z

    :goto_5
    iget-object v4, p0, Lbkt;->d:Lbla;

    invoke-virtual {v4, v3}, Lbla;->w(Ljava/lang/String;)V

    iget-object v3, v0, Lblm;->b:Lbjy;

    iget-object v0, v0, Lblm;->c:[B

    invoke-direct {p0, v7, v1, v3, v0}, Lbkt;->a(Ljava/lang/String;Lgcd;Lbjy;[B)V

    goto/16 :goto_0

    :cond_8
    :try_start_0
    invoke-direct {p0, v3}, Lbkt;->f(Ljava/lang/String;)Lgcd;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v4, p0, Lbkt;->e:Lgml;

    iget-object v5, v0, Lblm;->b:Lbjy;

    invoke-virtual {v5}, Lbjy;->a()I

    move-result v5

    invoke-virtual {v4, v1, v5}, Lgml;->a(Lgcd;I)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x23

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Failed inserting video "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v3}, Lbla;->x(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failed requesting video "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " for offline"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v3}, Lbla;->x(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0, v1}, Lbkt;->a(Lgcd;)V

    goto :goto_5

    .line 98
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 99
    iget-object v1, p0, Lbkt;->e:Lgml;

    invoke-virtual {v1, v0}, Lgml;->g(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 100
    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->w(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 102
    :cond_a
    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->x(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 108
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 109
    iget-object v1, p0, Lbkt;->e:Lgml;

    sget-object v3, Lglv;->i:Lglv;

    invoke-virtual {v1, v0, v3}, Lgml;->a(Ljava/lang/String;Lglv;)Z

    iget-object v3, p0, Lbkt;->d:Lbla;

    iget-object v1, v3, Lbla;->g:Lfad;

    iget-object v1, v1, Lfad;->b:Landroid/os/Binder;

    check-cast v1, Ldmz;

    invoke-virtual {v3, v0}, Lbla;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x80

    invoke-virtual {v1, v3, v4}, Ldmz;->a(Ljava/lang/String;I)V

    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->y(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 114
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lbkt;->e:Lgml;

    sget-object v3, Lglv;->c:Lglv;

    invoke-virtual {v1, v0, v3}, Lgml;->a(Ljava/lang/String;Lglv;)Z

    invoke-direct {p0, v0}, Lbkt;->e(Ljava/lang/String;)V

    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->y(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 120
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 121
    iget-object v1, p0, Lbkt;->e:Lgml;

    invoke-virtual {v1, v0}, Lgml;->a(Ljava/lang/String;)Lgcd;

    move-result-object v1

    .line 125
    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lbkt;->e:Lgml;

    invoke-virtual {v1, v0}, Lgml;->d(Ljava/lang/String;)Lgmb;

    move-result-object v1

    if-nez v1, :cond_b

    .line 129
    iget-object v1, p0, Lbkt;->e:Lgml;

    sget-object v3, Lglv;->c:Lglv;

    iget-object v4, p0, Lbkt;->e:Lgml;

    .line 132
    invoke-virtual {v4, v0}, Lgml;->b(Ljava/lang/String;)I

    move-result v4

    .line 129
    invoke-virtual {v1, v0, v3, v4}, Lgml;->a(Ljava/lang/String;Lglv;I)Z

    .line 133
    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->w(Ljava/lang/String;)V

    .line 140
    :goto_6
    invoke-direct {p0, v0}, Lbkt;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 135
    :cond_b
    iget-object v1, p0, Lbkt;->e:Lgml;

    sget-object v3, Lglv;->c:Lglv;

    invoke-virtual {v1, v0, v3}, Lgml;->a(Ljava/lang/String;Lglv;)Z

    .line 136
    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->y(Ljava/lang/String;)V

    goto :goto_6

    .line 146
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 147
    invoke-direct {p0, v0, v2}, Lbkt;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 152
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 153
    invoke-direct {p0, v0, v1}, Lbkt;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 158
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lbkt;->e:Lgml;

    invoke-virtual {v1, v0}, Lgml;->d(Ljava/lang/String;)Lgmb;

    move-result-object v1

    .line 161
    if-eqz v1, :cond_0

    iget-object v3, v1, Lgmb;->d:Lglz;

    if-eqz v3, :cond_0

    .line 162
    iget-object v3, v1, Lgmb;->d:Lglz;

    .line 163
    invoke-virtual {v3}, Lglz;->b()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 164
    iget-object v1, p0, Lbkt;->d:Lbla;

    invoke-virtual {v1, v0}, Lbla;->y(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 166
    :cond_c
    iget-object v0, p0, Lbkt;->d:Lbla;

    invoke-virtual {v0, v1}, Lbla;->a(Lgmb;)V

    goto/16 :goto_0

    .line 173
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lblm;

    .line 174
    invoke-direct {p0, v0}, Lbkt;->a(Lblm;)V

    goto/16 :goto_0

    .line 179
    :pswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 180
    invoke-direct {p0, v0}, Lbkt;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 185
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 186
    invoke-direct {p0, v0}, Lbkt;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 191
    :pswitch_c
    iget-object v0, p0, Lbkt;->e:Lgml;

    iget-object v0, v0, Lgml;->f:Lgnp;

    invoke-virtual {v0}, Lgnp;->b()Ljava/util/List;

    move-result-object v0

    .line 192
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbu;

    .line 193
    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lbkt;->b(Ljava/lang/String;)V

    goto :goto_7

    .line 195
    :cond_d
    iget-object v0, p0, Lbkt;->e:Lgml;

    invoke-virtual {v0}, Lgml;->g()Ljava/util/List;

    move-result-object v0

    .line 196
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    .line 197
    iget-object v0, v0, Lgmb;->a:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lbkt;->a(Ljava/lang/String;Z)V

    goto :goto_8

    .line 203
    :pswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 204
    const-string v3, "Updating offlined video "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_9
    invoke-static {v1}, Lezp;->e(Ljava/lang/String;)V

    :try_start_1
    invoke-direct {p0, v0}, Lbkt;->f(Ljava/lang/String;)Lgcd;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    iget-object v3, p0, Lbkt;->e:Lgml;

    invoke-virtual {v3, v1}, Lgml;->a(Lgcd;)Z

    move-result v3

    if-nez v3, :cond_f

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x23

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed inserting video "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to database"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_9

    :catch_1
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failed requesting video "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " for offline"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :cond_f
    iget-object v3, p0, Lbkt;->b:Lgmt;

    iget-object v4, v1, Lgcd;->b:Ljava/lang/String;

    invoke-static {v4}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v3, v4}, Lgmt;->d(Ljava/lang/String;)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    :cond_10
    invoke-virtual {v3, v4}, Lgmt;->e(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    :cond_11
    invoke-direct {p0, v1}, Lbkt;->a(Lgcd;)V

    iget-object v3, p0, Lbkt;->e:Lgml;

    invoke-virtual {v3, v0}, Lgml;->b(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lbjy;->a(I)Lbjy;

    move-result-object v0

    sget-object v3, Lfhy;->a:[B

    invoke-direct {p0, v7, v1, v0, v3}, Lbkt;->a(Ljava/lang/String;Lgcd;Lbjy;[B)V

    goto/16 :goto_0

    :cond_12
    move-object v0, v1

    goto/16 :goto_2

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
