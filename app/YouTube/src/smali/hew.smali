.class public final Lhew;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:J

.field public c:[Lher;

.field public d:Lhev;

.field public e:[Lhex;

.field private f:Z

.field private g:Lhbt;

.field private h:Lhbt;

.field private i:Lhbt;

.field private j:Lhbt;

.field private k:Lhnh;

.field private l:Lhog;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 41718
    invoke-direct {p0}, Lidf;-><init>()V

    .line 41721
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhew;->f:Z

    .line 41724
    iput-object v2, p0, Lhew;->g:Lhbt;

    .line 41727
    iput-object v2, p0, Lhew;->h:Lhbt;

    .line 41730
    iput-object v2, p0, Lhew;->i:Lhbt;

    .line 41733
    iput-object v2, p0, Lhew;->a:Lhgz;

    .line 41736
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhew;->b:J

    .line 41739
    sget-object v0, Lher;->a:[Lher;

    iput-object v0, p0, Lhew;->c:[Lher;

    .line 41742
    iput-object v2, p0, Lhew;->j:Lhbt;

    .line 41745
    iput-object v2, p0, Lhew;->k:Lhnh;

    .line 41748
    iput-object v2, p0, Lhew;->l:Lhog;

    .line 41751
    iput-object v2, p0, Lhew;->d:Lhev;

    .line 41754
    sget-object v0, Lhex;->a:[Lhex;

    iput-object v0, p0, Lhew;->e:[Lhex;

    .line 41718
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 41875
    .line 41876
    iget-boolean v0, p0, Lhew;->f:Z

    if-eqz v0, :cond_d

    .line 41877
    const/4 v0, 0x1

    iget-boolean v2, p0, Lhew;->f:Z

    .line 41878
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 41880
    :goto_0
    iget-object v2, p0, Lhew;->g:Lhbt;

    if-eqz v2, :cond_0

    .line 41881
    const/4 v2, 0x2

    iget-object v3, p0, Lhew;->g:Lhbt;

    .line 41882
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41884
    :cond_0
    iget-object v2, p0, Lhew;->h:Lhbt;

    if-eqz v2, :cond_1

    .line 41885
    const/4 v2, 0x3

    iget-object v3, p0, Lhew;->h:Lhbt;

    .line 41886
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41888
    :cond_1
    iget-object v2, p0, Lhew;->i:Lhbt;

    if-eqz v2, :cond_2

    .line 41889
    const/4 v2, 0x4

    iget-object v3, p0, Lhew;->i:Lhbt;

    .line 41890
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41892
    :cond_2
    iget-object v2, p0, Lhew;->a:Lhgz;

    if-eqz v2, :cond_3

    .line 41893
    const/4 v2, 0x5

    iget-object v3, p0, Lhew;->a:Lhgz;

    .line 41894
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41896
    :cond_3
    iget-wide v2, p0, Lhew;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 41897
    const/4 v2, 0x6

    iget-wide v4, p0, Lhew;->b:J

    .line 41898
    invoke-static {v2, v4, v5}, Lidd;->c(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 41900
    :cond_4
    iget-object v2, p0, Lhew;->c:[Lher;

    if-eqz v2, :cond_6

    .line 41901
    iget-object v3, p0, Lhew;->c:[Lher;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 41902
    if-eqz v5, :cond_5

    .line 41903
    const/4 v6, 0x7

    .line 41904
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 41901
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 41908
    :cond_6
    iget-object v2, p0, Lhew;->j:Lhbt;

    if-eqz v2, :cond_7

    .line 41909
    const/16 v2, 0x8

    iget-object v3, p0, Lhew;->j:Lhbt;

    .line 41910
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41912
    :cond_7
    iget-object v2, p0, Lhew;->k:Lhnh;

    if-eqz v2, :cond_8

    .line 41913
    const/16 v2, 0x9

    iget-object v3, p0, Lhew;->k:Lhnh;

    .line 41914
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41916
    :cond_8
    iget-object v2, p0, Lhew;->l:Lhog;

    if-eqz v2, :cond_9

    .line 41917
    const/16 v2, 0xa

    iget-object v3, p0, Lhew;->l:Lhog;

    .line 41918
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41920
    :cond_9
    iget-object v2, p0, Lhew;->d:Lhev;

    if-eqz v2, :cond_a

    .line 41921
    const/16 v2, 0xb

    iget-object v3, p0, Lhew;->d:Lhev;

    .line 41922
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 41924
    :cond_a
    iget-object v2, p0, Lhew;->e:[Lhex;

    if-eqz v2, :cond_c

    .line 41925
    iget-object v2, p0, Lhew;->e:[Lhex;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 41926
    if-eqz v4, :cond_b

    .line 41927
    const/16 v5, 0xc

    .line 41928
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 41925
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 41932
    :cond_c
    iget-object v1, p0, Lhew;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41933
    iput v0, p0, Lhew;->J:I

    .line 41934
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 41714
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhew;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhew;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhew;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhew;->f:Z

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhew;->g:Lhbt;

    if-nez v0, :cond_2

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhew;->g:Lhbt;

    :cond_2
    iget-object v0, p0, Lhew;->g:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhew;->h:Lhbt;

    if-nez v0, :cond_3

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhew;->h:Lhbt;

    :cond_3
    iget-object v0, p0, Lhew;->h:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhew;->i:Lhbt;

    if-nez v0, :cond_4

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhew;->i:Lhbt;

    :cond_4
    iget-object v0, p0, Lhew;->i:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhew;->a:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhew;->a:Lhgz;

    :cond_5
    iget-object v0, p0, Lhew;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    iput-wide v2, p0, Lhew;->b:J

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhew;->c:[Lher;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lher;

    iget-object v3, p0, Lhew;->c:[Lher;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhew;->c:[Lher;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhew;->c:[Lher;

    :goto_2
    iget-object v2, p0, Lhew;->c:[Lher;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhew;->c:[Lher;

    new-instance v3, Lher;

    invoke-direct {v3}, Lher;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhew;->c:[Lher;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lhew;->c:[Lher;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lhew;->c:[Lher;

    new-instance v3, Lher;

    invoke-direct {v3}, Lher;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhew;->c:[Lher;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhew;->j:Lhbt;

    if-nez v0, :cond_9

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhew;->j:Lhbt;

    :cond_9
    iget-object v0, p0, Lhew;->j:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhew;->k:Lhnh;

    if-nez v0, :cond_a

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhew;->k:Lhnh;

    :cond_a
    iget-object v0, p0, Lhew;->k:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhew;->l:Lhog;

    if-nez v0, :cond_b

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhew;->l:Lhog;

    :cond_b
    iget-object v0, p0, Lhew;->l:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhew;->d:Lhev;

    if-nez v0, :cond_c

    new-instance v0, Lhev;

    invoke-direct {v0}, Lhev;-><init>()V

    iput-object v0, p0, Lhew;->d:Lhev;

    :cond_c
    iget-object v0, p0, Lhew;->d:Lhev;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhew;->e:[Lhex;

    if-nez v0, :cond_e

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhex;

    iget-object v3, p0, Lhew;->e:[Lhex;

    if-eqz v3, :cond_d

    iget-object v3, p0, Lhew;->e:[Lhex;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    iput-object v2, p0, Lhew;->e:[Lhex;

    :goto_4
    iget-object v2, p0, Lhew;->e:[Lhex;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_f

    iget-object v2, p0, Lhew;->e:[Lhex;

    new-instance v3, Lhex;

    invoke-direct {v3}, Lhex;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhew;->e:[Lhex;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_e
    iget-object v0, p0, Lhew;->e:[Lhex;

    array-length v0, v0

    goto :goto_3

    :cond_f
    iget-object v2, p0, Lhew;->e:[Lhex;

    new-instance v3, Lhex;

    invoke-direct {v3}, Lhex;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhew;->e:[Lhex;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 41825
    iget-boolean v1, p0, Lhew;->f:Z

    if-eqz v1, :cond_0

    .line 41826
    const/4 v1, 0x1

    iget-boolean v2, p0, Lhew;->f:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    .line 41828
    :cond_0
    iget-object v1, p0, Lhew;->g:Lhbt;

    if-eqz v1, :cond_1

    .line 41829
    const/4 v1, 0x2

    iget-object v2, p0, Lhew;->g:Lhbt;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41831
    :cond_1
    iget-object v1, p0, Lhew;->h:Lhbt;

    if-eqz v1, :cond_2

    .line 41832
    const/4 v1, 0x3

    iget-object v2, p0, Lhew;->h:Lhbt;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41834
    :cond_2
    iget-object v1, p0, Lhew;->i:Lhbt;

    if-eqz v1, :cond_3

    .line 41835
    const/4 v1, 0x4

    iget-object v2, p0, Lhew;->i:Lhbt;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41837
    :cond_3
    iget-object v1, p0, Lhew;->a:Lhgz;

    if-eqz v1, :cond_4

    .line 41838
    const/4 v1, 0x5

    iget-object v2, p0, Lhew;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41840
    :cond_4
    iget-wide v2, p0, Lhew;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 41841
    const/4 v1, 0x6

    iget-wide v2, p0, Lhew;->b:J

    invoke-virtual {p1, v1, v2, v3}, Lidd;->a(IJ)V

    .line 41843
    :cond_5
    iget-object v1, p0, Lhew;->c:[Lher;

    if-eqz v1, :cond_7

    .line 41844
    iget-object v2, p0, Lhew;->c:[Lher;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 41845
    if-eqz v4, :cond_6

    .line 41846
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 41844
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41850
    :cond_7
    iget-object v1, p0, Lhew;->j:Lhbt;

    if-eqz v1, :cond_8

    .line 41851
    const/16 v1, 0x8

    iget-object v2, p0, Lhew;->j:Lhbt;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41853
    :cond_8
    iget-object v1, p0, Lhew;->k:Lhnh;

    if-eqz v1, :cond_9

    .line 41854
    const/16 v1, 0x9

    iget-object v2, p0, Lhew;->k:Lhnh;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41856
    :cond_9
    iget-object v1, p0, Lhew;->l:Lhog;

    if-eqz v1, :cond_a

    .line 41857
    const/16 v1, 0xa

    iget-object v2, p0, Lhew;->l:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41859
    :cond_a
    iget-object v1, p0, Lhew;->d:Lhev;

    if-eqz v1, :cond_b

    .line 41860
    const/16 v1, 0xb

    iget-object v2, p0, Lhew;->d:Lhev;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 41862
    :cond_b
    iget-object v1, p0, Lhew;->e:[Lhex;

    if-eqz v1, :cond_d

    .line 41863
    iget-object v1, p0, Lhew;->e:[Lhex;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_d

    aget-object v3, v1, v0

    .line 41864
    if-eqz v3, :cond_c

    .line 41865
    const/16 v4, 0xc

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 41863
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 41869
    :cond_d
    iget-object v0, p0, Lhew;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 41871
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41776
    if-ne p1, p0, :cond_1

    .line 41791
    :cond_0
    :goto_0
    return v0

    .line 41777
    :cond_1
    instance-of v2, p1, Lhew;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 41778
    :cond_2
    check-cast p1, Lhew;

    .line 41779
    iget-boolean v2, p0, Lhew;->f:Z

    iget-boolean v3, p1, Lhew;->f:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhew;->g:Lhbt;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhew;->g:Lhbt;

    if-nez v2, :cond_3

    .line 41780
    :goto_1
    iget-object v2, p0, Lhew;->h:Lhbt;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhew;->h:Lhbt;

    if-nez v2, :cond_3

    .line 41781
    :goto_2
    iget-object v2, p0, Lhew;->i:Lhbt;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhew;->i:Lhbt;

    if-nez v2, :cond_3

    .line 41782
    :goto_3
    iget-object v2, p0, Lhew;->a:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhew;->a:Lhgz;

    if-nez v2, :cond_3

    .line 41783
    :goto_4
    iget-wide v2, p0, Lhew;->b:J

    iget-wide v4, p1, Lhew;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhew;->c:[Lher;

    iget-object v3, p1, Lhew;->c:[Lher;

    .line 41785
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhew;->j:Lhbt;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhew;->j:Lhbt;

    if-nez v2, :cond_3

    .line 41786
    :goto_5
    iget-object v2, p0, Lhew;->k:Lhnh;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhew;->k:Lhnh;

    if-nez v2, :cond_3

    .line 41787
    :goto_6
    iget-object v2, p0, Lhew;->l:Lhog;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhew;->l:Lhog;

    if-nez v2, :cond_3

    .line 41788
    :goto_7
    iget-object v2, p0, Lhew;->d:Lhev;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhew;->d:Lhev;

    if-nez v2, :cond_3

    .line 41789
    :goto_8
    iget-object v2, p0, Lhew;->e:[Lhex;

    iget-object v3, p1, Lhew;->e:[Lhex;

    .line 41790
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhew;->I:Ljava/util/List;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhew;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 41791
    goto :goto_0

    .line 41779
    :cond_4
    iget-object v2, p0, Lhew;->g:Lhbt;

    iget-object v3, p1, Lhew;->g:Lhbt;

    .line 41780
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhew;->h:Lhbt;

    iget-object v3, p1, Lhew;->h:Lhbt;

    .line 41781
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhew;->i:Lhbt;

    iget-object v3, p1, Lhew;->i:Lhbt;

    .line 41782
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhew;->a:Lhgz;

    iget-object v3, p1, Lhew;->a:Lhgz;

    .line 41783
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    .line 41785
    :cond_8
    iget-object v2, p0, Lhew;->j:Lhbt;

    iget-object v3, p1, Lhew;->j:Lhbt;

    .line 41786
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhew;->k:Lhnh;

    iget-object v3, p1, Lhew;->k:Lhnh;

    .line 41787
    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhew;->l:Lhog;

    iget-object v3, p1, Lhew;->l:Lhog;

    .line 41788
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhew;->d:Lhev;

    iget-object v3, p1, Lhew;->d:Lhev;

    .line 41789
    invoke-virtual {v2, v3}, Lhev;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    .line 41790
    :cond_c
    iget-object v2, p0, Lhew;->I:Ljava/util/List;

    iget-object v3, p1, Lhew;->I:Ljava/util/List;

    .line 41791
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 41795
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 41797
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhew;->f:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    .line 41798
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhew;->g:Lhbt;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 41799
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhew;->h:Lhbt;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 41800
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhew;->i:Lhbt;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 41801
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhew;->a:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 41802
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhew;->b:J

    iget-wide v4, p0, Lhew;->b:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 41803
    iget-object v2, p0, Lhew;->c:[Lher;

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    .line 41809
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhew;->j:Lhbt;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 41810
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhew;->k:Lhnh;

    if-nez v0, :cond_a

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 41811
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhew;->l:Lhog;

    if-nez v0, :cond_b

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 41812
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhew;->d:Lhev;

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 41813
    iget-object v2, p0, Lhew;->e:[Lhex;

    if-nez v2, :cond_d

    mul-int/lit8 v2, v0, 0x1f

    .line 41819
    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhew;->I:Ljava/util/List;

    if-nez v2, :cond_f

    :goto_9
    add-int/2addr v0, v1

    .line 41820
    return v0

    .line 41797
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 41798
    :cond_3
    iget-object v0, p0, Lhew;->g:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_1

    .line 41799
    :cond_4
    iget-object v0, p0, Lhew;->h:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_2

    .line 41800
    :cond_5
    iget-object v0, p0, Lhew;->i:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_3

    .line 41801
    :cond_6
    iget-object v0, p0, Lhew;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_7
    move v2, v0

    move v0, v1

    .line 41805
    :goto_a
    iget-object v3, p0, Lhew;->c:[Lher;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 41806
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhew;->c:[Lher;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v1

    :goto_b
    add-int/2addr v2, v3

    .line 41805
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 41806
    :cond_8
    iget-object v2, p0, Lhew;->c:[Lher;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lher;->hashCode()I

    move-result v2

    goto :goto_b

    .line 41809
    :cond_9
    iget-object v0, p0, Lhew;->j:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_5

    .line 41810
    :cond_a
    iget-object v0, p0, Lhew;->k:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto :goto_6

    .line 41811
    :cond_b
    iget-object v0, p0, Lhew;->l:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_7

    .line 41812
    :cond_c
    iget-object v0, p0, Lhew;->d:Lhev;

    invoke-virtual {v0}, Lhev;->hashCode()I

    move-result v0

    goto :goto_8

    :cond_d
    move v2, v0

    move v0, v1

    .line 41815
    :goto_c
    iget-object v3, p0, Lhew;->e:[Lhex;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 41816
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhew;->e:[Lhex;

    aget-object v2, v2, v0

    if-nez v2, :cond_e

    move v2, v1

    :goto_d
    add-int/2addr v2, v3

    .line 41815
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 41816
    :cond_e
    iget-object v2, p0, Lhew;->e:[Lhex;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhex;->hashCode()I

    move-result v2

    goto :goto_d

    .line 41819
    :cond_f
    iget-object v1, p0, Lhew;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_9
.end method
