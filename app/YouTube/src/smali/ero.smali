.class public final enum Lero;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lero;

.field public static final enum b:Lero;

.field public static final enum c:Lero;

.field public static final enum d:Lero;

.field public static final enum e:Lero;

.field public static final enum f:Lero;

.field public static final enum g:Lero;

.field public static final enum h:Lero;

.field public static final enum i:Lero;

.field public static final enum j:Lero;

.field private static final synthetic l:[Lero;


# instance fields
.field public final k:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 59
    new-instance v0, Lero;

    const-string v1, "START"

    invoke-direct {v0, v1, v5, v5}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->a:Lero;

    .line 60
    new-instance v0, Lero;

    const-string v1, "FIRST_QUARTILE"

    invoke-direct {v0, v1, v6, v6}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->b:Lero;

    .line 61
    new-instance v0, Lero;

    const-string v1, "MIDPOINT"

    invoke-direct {v0, v1, v7, v7}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->c:Lero;

    .line 62
    new-instance v0, Lero;

    const-string v1, "THIRD_QUARTILE"

    invoke-direct {v0, v1, v8, v8}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->d:Lero;

    .line 63
    new-instance v0, Lero;

    const-string v1, "COMPLETE"

    const/4 v2, 0x4

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->e:Lero;

    .line 64
    new-instance v0, Lero;

    const-string v1, "RESUME"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->f:Lero;

    .line 65
    new-instance v0, Lero;

    const-string v1, "PAUSE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->g:Lero;

    .line 66
    new-instance v0, Lero;

    const-string v1, "SKIP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->h:Lero;

    .line 67
    new-instance v0, Lero;

    const-string v1, "MUTE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->i:Lero;

    .line 68
    new-instance v0, Lero;

    const-string v1, "UNMUTE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v4}, Lero;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lero;->j:Lero;

    .line 58
    const/16 v0, 0xa

    new-array v0, v0, [Lero;

    sget-object v1, Lero;->a:Lero;

    aput-object v1, v0, v5

    sget-object v1, Lero;->b:Lero;

    aput-object v1, v0, v6

    sget-object v1, Lero;->c:Lero;

    aput-object v1, v0, v7

    sget-object v1, Lero;->d:Lero;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lero;->e:Lero;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lero;->f:Lero;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lero;->g:Lero;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lero;->h:Lero;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lero;->i:Lero;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lero;->j:Lero;

    aput-object v2, v0, v1

    sput-object v0, Lero;->l:[Lero;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput p3, p0, Lero;->k:I

    .line 79
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lero;
    .locals 1

    .prologue
    .line 58
    const-class v0, Lero;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lero;

    return-object v0
.end method

.method public static values()[Lero;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lero;->l:[Lero;

    invoke-virtual {v0}, [Lero;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lero;

    return-object v0
.end method
