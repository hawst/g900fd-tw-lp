.class final Lcur;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic a:Ljava/util/List;

.field private synthetic b:Ljava/lang/String;

.field private synthetic c:Lcup;


# direct methods
.method constructor <init>(Lcup;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcur;->c:Lcup;

    iput-object p2, p0, Lcur;->a:Ljava/util/List;

    iput-object p3, p0, Lcur;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 174
    move-object v0, p1

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 175
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcur;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 176
    iget-object v1, p0, Lcur;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfid;

    .line 177
    invoke-virtual {v0}, Lfid;->b()Ljava/lang/String;

    move-result-object v1

    .line 178
    iget-object v2, p0, Lcur;->c:Lcup;

    new-instance v3, Lgit;

    iget-object v4, p0, Lcur;->b:Ljava/lang/String;

    .line 179
    iget-object v5, v0, Lfid;->a:Lgxt;

    iget-object v5, v5, Lgxt;->f:Ljava/lang/String;

    invoke-direct {v3, v4, v1, v5}, Lgit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcua;

    iget-object v4, p0, Lcur;->b:Ljava/lang/String;

    invoke-direct {v1, v4, v0}, Lcua;-><init>(Ljava/lang/String;Lfid;)V

    .line 178
    invoke-static {v2, v3, v1}, Lcup;->a(Lcup;Lgit;Lcua;)V

    .line 185
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 186
    return-void

    .line 182
    :cond_0
    iget-object v1, p0, Lcur;->c:Lcup;

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid delegate index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcup;->a(Lcup;Ljava/lang/Exception;)V

    goto :goto_0
.end method
