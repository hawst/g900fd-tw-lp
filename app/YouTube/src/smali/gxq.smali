.class public final Lgxq;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgxq;


# instance fields
.field public b:Lhog;

.field public c:Lhxf;

.field public d:Lhgz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    new-array v0, v0, [Lgxq;

    sput-object v0, Lgxq;->a:[Lgxq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-direct {p0}, Lidf;-><init>()V

    .line 136
    iput-object v0, p0, Lgxq;->b:Lhog;

    .line 139
    iput-object v0, p0, Lgxq;->c:Lhxf;

    .line 142
    iput-object v0, p0, Lgxq;->d:Lhgz;

    .line 133
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 191
    const/4 v0, 0x0

    .line 192
    iget-object v1, p0, Lgxq;->b:Lhog;

    if-eqz v1, :cond_0

    .line 193
    const/4 v0, 0x1

    iget-object v1, p0, Lgxq;->b:Lhog;

    .line 194
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 196
    :cond_0
    iget-object v1, p0, Lgxq;->c:Lhxf;

    if-eqz v1, :cond_1

    .line 197
    const/4 v1, 0x2

    iget-object v2, p0, Lgxq;->c:Lhxf;

    .line 198
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_1
    iget-object v1, p0, Lgxq;->d:Lhgz;

    if-eqz v1, :cond_2

    .line 201
    const/4 v1, 0x3

    iget-object v2, p0, Lgxq;->d:Lhgz;

    .line 202
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_2
    iget-object v1, p0, Lgxq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    iput v0, p0, Lgxq;->J:I

    .line 206
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 129
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgxq;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgxq;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgxq;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgxq;->b:Lhog;

    if-nez v0, :cond_2

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgxq;->b:Lhog;

    :cond_2
    iget-object v0, p0, Lgxq;->b:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgxq;->c:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lgxq;->c:Lhxf;

    :cond_3
    iget-object v0, p0, Lgxq;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgxq;->d:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgxq;->d:Lhgz;

    :cond_4
    iget-object v0, p0, Lgxq;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Lgxq;->b:Lhog;

    if-eqz v0, :cond_0

    .line 177
    const/4 v0, 0x1

    iget-object v1, p0, Lgxq;->b:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 179
    :cond_0
    iget-object v0, p0, Lgxq;->c:Lhxf;

    if-eqz v0, :cond_1

    .line 180
    const/4 v0, 0x2

    iget-object v1, p0, Lgxq;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 182
    :cond_1
    iget-object v0, p0, Lgxq;->d:Lhgz;

    if-eqz v0, :cond_2

    .line 183
    const/4 v0, 0x3

    iget-object v1, p0, Lgxq;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 185
    :cond_2
    iget-object v0, p0, Lgxq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 187
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 155
    if-ne p1, p0, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v0

    .line 156
    :cond_1
    instance-of v2, p1, Lgxq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 157
    :cond_2
    check-cast p1, Lgxq;

    .line 158
    iget-object v2, p0, Lgxq;->b:Lhog;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgxq;->b:Lhog;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgxq;->c:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgxq;->c:Lhxf;

    if-nez v2, :cond_3

    .line 159
    :goto_2
    iget-object v2, p0, Lgxq;->d:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgxq;->d:Lhgz;

    if-nez v2, :cond_3

    .line 160
    :goto_3
    iget-object v2, p0, Lgxq;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgxq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 161
    goto :goto_0

    .line 158
    :cond_4
    iget-object v2, p0, Lgxq;->b:Lhog;

    iget-object v3, p1, Lgxq;->b:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgxq;->c:Lhxf;

    iget-object v3, p1, Lgxq;->c:Lhxf;

    .line 159
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgxq;->d:Lhgz;

    iget-object v3, p1, Lgxq;->d:Lhgz;

    .line 160
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgxq;->I:Ljava/util/List;

    iget-object v3, p1, Lgxq;->I:Ljava/util/List;

    .line 161
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 167
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxq;->b:Lhog;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 168
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxq;->c:Lhxf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 169
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgxq;->d:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 170
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgxq;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 171
    return v0

    .line 167
    :cond_0
    iget-object v0, p0, Lgxq;->b:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Lgxq;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_1

    .line 169
    :cond_2
    iget-object v0, p0, Lgxq;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 170
    :cond_3
    iget-object v1, p0, Lgxq;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
