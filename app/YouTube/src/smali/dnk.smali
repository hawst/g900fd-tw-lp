.class public final Ldnk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lgjn;

.field public d:I

.field public e:J

.field public f:J

.field public g:Lgje;

.field public h:Lgje;

.field public i:Ljava/lang/String;

.field public j:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILgje;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 865
    const-string v0, "filePath may not be empty"

    invoke-static {p2, v0}, Lb;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldnk;->a:Ljava/lang/String;

    .line 866
    iput-object p3, p0, Ldnk;->b:Ljava/lang/String;

    .line 867
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgje;

    iput-object v0, p0, Ldnk;->g:Lgje;

    .line 868
    sget-object v0, Lgjn;->a:Lgjn;

    iput-object v0, p0, Ldnk;->c:Lgjn;

    .line 869
    const/4 v0, 0x1

    iput v0, p0, Ldnk;->d:I

    .line 870
    iput-wide v2, p0, Ldnk;->e:J

    .line 871
    iput-wide v2, p0, Ldnk;->f:J

    .line 872
    new-instance v0, Lgje;

    invoke-direct {v0}, Lgje;-><init>()V

    iput-object v0, p0, Ldnk;->h:Lgje;

    .line 873
    iput-object p1, p0, Ldnk;->i:Ljava/lang/String;

    .line 874
    iput p4, p0, Ldnk;->j:I

    .line 875
    return-void
.end method


# virtual methods
.method public final a()Lgjm;
    .locals 14

    .prologue
    .line 890
    new-instance v1, Lgjm;

    iget-object v2, p0, Ldnk;->a:Ljava/lang/String;

    iget-object v3, p0, Ldnk;->b:Ljava/lang/String;

    iget-object v4, p0, Ldnk;->c:Lgjn;

    iget v5, p0, Ldnk;->d:I

    iget-wide v6, p0, Ldnk;->e:J

    iget-wide v8, p0, Ldnk;->f:J

    iget-object v10, p0, Ldnk;->g:Lgje;

    iget-object v11, p0, Ldnk;->h:Lgje;

    iget-object v12, p0, Ldnk;->i:Ljava/lang/String;

    iget v13, p0, Ldnk;->j:I

    invoke-direct/range {v1 .. v13}, Lgjm;-><init>(Ljava/lang/String;Ljava/lang/String;Lgjn;IJJLgje;Lgje;Ljava/lang/String;I)V

    return-object v1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 904
    iget-object v0, p0, Ldnk;->c:Lgjn;

    sget-object v1, Lgjn;->c:Lgjn;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldnk;->c:Lgjn;

    sget-object v1, Lgjn;->d:Lgjn;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
