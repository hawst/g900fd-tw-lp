.class public final enum Laxg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Laxg;

.field public static final enum b:Laxg;

.field public static final enum c:Laxg;

.field private static final synthetic h:[Laxg;


# instance fields
.field final d:Ljava/lang/String;

.field final e:I

.field final f:I

.field private final g:Laxh;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Laxg;

    const-string v1, "START_SETTINGS"

    sget-object v3, Laxh;->a:Laxh;

    const-string v4, "com.google.android.youtube.action.autocast_notification_settings"

    const v5, 0x7f0200f0

    move v6, v2

    invoke-direct/range {v0 .. v6}, Laxg;-><init>(Ljava/lang/String;ILaxh;Ljava/lang/String;II)V

    sput-object v0, Laxg;->a:Laxg;

    .line 30
    new-instance v3, Laxg;

    const-string v4, "ALWAYS"

    sget-object v6, Laxh;->b:Laxh;

    const-string v7, "com.google.android.youtube.action.autocast_notification_always"

    const v9, 0x7f090071

    move v5, v10

    move v8, v2

    invoke-direct/range {v3 .. v9}, Laxg;-><init>(Ljava/lang/String;ILaxh;Ljava/lang/String;II)V

    sput-object v3, Laxg;->b:Laxg;

    .line 38
    new-instance v3, Laxg;

    const-string v4, "ONCE"

    sget-object v6, Laxh;->b:Laxh;

    const-string v7, "com.google.android.youtube.action.autocast_notification_once"

    const v9, 0x7f090072

    move v5, v11

    move v8, v2

    invoke-direct/range {v3 .. v9}, Laxg;-><init>(Ljava/lang/String;ILaxh;Ljava/lang/String;II)V

    sput-object v3, Laxg;->c:Laxg;

    .line 17
    const/4 v0, 0x3

    new-array v0, v0, [Laxg;

    sget-object v1, Laxg;->a:Laxg;

    aput-object v1, v0, v2

    sget-object v1, Laxg;->b:Laxg;

    aput-object v1, v0, v10

    sget-object v1, Laxg;->c:Laxg;

    aput-object v1, v0, v11

    sput-object v0, Laxg;->h:[Laxg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaxh;Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput-object p3, p0, Laxg;->g:Laxh;

    .line 56
    iput-object p4, p0, Laxg;->d:Ljava/lang/String;

    .line 57
    iput p5, p0, Laxg;->e:I

    .line 58
    iput p6, p0, Laxg;->f:I

    .line 59
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laxg;
    .locals 1

    .prologue
    .line 17
    const-class v0, Laxg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laxg;

    return-object v0
.end method

.method public static values()[Laxg;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Laxg;->h:[Laxg;

    invoke-virtual {v0}, [Laxg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laxg;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Laxg;->g:Laxh;

    invoke-virtual {v0, p1, p0}, Laxh;->a(Landroid/content/Context;Laxg;)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
