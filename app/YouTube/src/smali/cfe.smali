.class public final Lcfe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcfe;->c:Lfsj;

    .line 31
    const v0, 0x7f040094

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcfe;->a:Landroid/view/View;

    .line 32
    iget-object v0, p0, Lcfe;->a:Landroid/view/View;

    const v1, 0x7f0801da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcfe;->b:Landroid/widget/TextView;

    .line 34
    iget-object v0, p0, Lcfe;->a:Landroid/view/View;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 35
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    .prologue
    .line 22
    check-cast p2, Lfkr;

    iget-object v0, p0, Lcfe;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lfkr;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcfe;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
