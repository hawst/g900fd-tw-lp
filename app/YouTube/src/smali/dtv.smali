.class public final Ldtv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcwr;


# instance fields
.field private final a:Lcvq;

.field private final b:Ldtp;

.field private final c:Ldaq;

.field private final d:Ldaq;

.field private final e:Ldwq;

.field private final f:Levn;


# direct methods
.method public constructor <init>(Ldwq;Levn;Lcvq;Ldtp;Ldaq;Ldaq;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldtv;->e:Ldwq;

    .line 37
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldtv;->f:Levn;

    .line 38
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvq;

    iput-object v0, p0, Ldtv;->a:Lcvq;

    .line 39
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldtp;

    iput-object v0, p0, Ldtv;->b:Ldtp;

    .line 40
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    iput-object v0, p0, Ldtv;->c:Ldaq;

    .line 41
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    iput-object v0, p0, Ldtv;->d:Ldaq;

    .line 42
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldtv;->e:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    invoke-virtual {v0}, Ldww;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()Lcvq;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ldtv;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtv;->b:Ldtp;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldtv;->a:Lcvq;

    goto :goto_0
.end method

.method public final b()Ldaq;
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ldtv;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtv;->d:Ldaq;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldtv;->c:Ldaq;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ldtv;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final handleMdxStateChangedEvent(Ldwx;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 68
    iget-object v0, p1, Ldwx;->a:Ldww;

    sget-object v1, Ldtw;->a:[I

    invoke-virtual {v0}, Ldww;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 69
    :goto_0
    return-void

    .line 68
    :pswitch_0
    iget-object v0, p0, Ldtv;->f:Levn;

    iget-object v1, p0, Ldtv;->b:Ldtp;

    invoke-virtual {v0, v1}, Levn;->a(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Ldtv;->f:Levn;

    iget-object v1, p0, Ldtv;->b:Ldtp;

    invoke-virtual {v0, v1}, Levn;->b(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
