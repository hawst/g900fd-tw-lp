.class final Ldvu;
.super Ldvz;
.source "SourceFile"


# instance fields
.field private synthetic a:Leuc;

.field private synthetic b:Ldvs;


# direct methods
.method constructor <init>(Ldvs;Ljava/lang/Void;Leuc;Leuc;)V
    .locals 1

    .prologue
    .line 156
    iput-object p1, p0, Ldvu;->b:Ldvs;

    iput-object p4, p0, Ldvu;->a:Leuc;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3}, Ldvz;-><init>(Ldvs;Ljava/lang/Object;Leuc;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    .line 156
    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldvu;->a:Leuc;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v8, v1}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Ldvu;->b:Ldvs;

    invoke-static {v0}, Ldvs;->d(Ldvs;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v2, Ldww;->b:Ldww;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Ldvu;->b:Ldvs;

    invoke-static {v0}, Ldvs;->d(Ldvs;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v2

    invoke-virtual {v2}, Ldwr;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    invoke-virtual {v0, v2}, Ldst;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Ldvu;->a:Leuc;

    invoke-interface {v2, v8, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    iget-object v4, v0, Ldst;->a:Ldsw;

    sget-object v5, Ldsw;->a:Ldsw;

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Ldvu;->b:Ldvs;

    invoke-static {v4}, Ldvs;->e(Ldvs;)Ljava/util/Map;

    move-result-object v4

    iget-object v5, v0, Ldst;->b:Ldth;

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v0, v0, Ldst;->b:Ldth;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v4, v0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v6

    const-wide/high16 v6, 0x4018000000000000L    # 6.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v0, v4

    add-int/lit8 v3, v0, 0x1

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_4

    mul-int/lit8 v1, v0, 0x6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v5, v1, 0x6

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iget-object v5, p0, Ldvu;->b:Ldvs;

    invoke-static {v5}, Ldvs;->e(Ldvs;)Ljava/util/Map;

    move-result-object v5

    iget-object v6, p0, Ldvu;->b:Ldvs;

    invoke-static {v6}, Ldvs;->f(Ldvs;)Ldrf;

    move-result-object v6

    invoke-interface {v2, v1, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v6, v1}, Ldrf;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v5, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    iget-object v1, v0, Ldst;->a:Ldsw;

    sget-object v4, Ldsw;->a:Ldsw;

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Ldvu;->b:Ldvs;

    invoke-static {v1}, Ldvs;->e(Ldvs;)Ljava/util/Map;

    move-result-object v1

    iget-object v4, v0, Ldst;->b:Ldth;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Ldvu;->b:Ldvs;

    invoke-static {v1}, Ldvs;->e(Ldvs;)Ljava/util/Map;

    move-result-object v1

    iget-object v4, v0, Ldst;->b:Ldth;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldtb;

    invoke-virtual {v0, v1}, Ldst;->a(Ldtb;)Ldst;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_6
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-object v0, p0, Ldvu;->b:Ldvs;

    invoke-static {v0}, Ldvs;->g(Ldvs;)Ldrl;

    move-result-object v0

    invoke-virtual {v0, v2}, Ldrl;->a(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    new-instance v4, Ldwd;

    invoke-direct {v4, v0}, Ldwd;-><init>(Ldst;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_9
    iget-object v0, p0, Ldvu;->b:Ldvs;

    invoke-static {v0}, Ldvs;->d(Ldvs;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->b:Ldww;

    if-ne v0, v1, :cond_a

    iget-object v0, p0, Ldvu;->b:Ldvs;

    invoke-static {v0}, Ldvs;->d(Ldvs;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-virtual {v0}, Ldwr;->g()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Ldvu;->b:Ldvs;

    invoke-static {v0}, Ldvs;->d(Ldvs;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Ldvu;->b:Ldvs;

    invoke-static {v0}, Ldvs;->d(Ldvs;)Ldwq;

    move-result-object v0

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v0, p0, Ldvu;->a:Leuc;

    invoke-interface {v0, v8, v3}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
