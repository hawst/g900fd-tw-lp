.class final Lgcw;
.super Ledh;
.source "SourceFile"


# instance fields
.field private a:Landroid/util/SparseArray;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Ledh;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(IILjava/util/List;Ljava/util/List;)Lede;
    .locals 7

    .prologue
    .line 222
    new-instance v0, Lgcx;

    iget-object v5, p0, Lgcw;->b:Ljava/lang/String;

    iget-object v6, p0, Lgcw;->c:Ljava/lang/String;

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lgcx;-><init>(IILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected final a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;JJLedn;)Lede;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 209
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lgcw;->a:Landroid/util/SparseArray;

    .line 210
    const-string v0, "yt:langName"

    invoke-interface {p1, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgcw;->b:Ljava/lang/String;

    .line 211
    iput-object v1, p0, Lgcw;->c:Ljava/lang/String;

    .line 212
    invoke-super/range {p0 .. p8}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;JJLedn;)Lede;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lorg/xmlpull/v1/XmlPullParser;)Ledf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 229
    const-string v0, "schemeIdUri"

    invoke-interface {p1, v4, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 232
    const-string v1, "yt:SystemURL"

    invoke-static {p1, v1}, Lgcw;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 233
    const-string v1, "type"

    invoke-interface {p1, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 234
    const-string v2, "widevine"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 235
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 236
    iget-object v1, p0, Lgcw;->a:Landroid/util/SparseArray;

    const/4 v2, 0x4

    .line 237
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v3

    .line 236
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 244
    :cond_1
    :goto_0
    const-string v1, "ContentProtection"

    invoke-static {p1, v1}, Lgcw;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    new-instance v1, Lgcy;

    iget-object v2, p0, Lgcw;->a:Landroid/util/SparseArray;

    invoke-direct {v1, v0, v2}, Lgcy;-><init>(Ljava/lang/String;Landroid/util/SparseArray;)V

    return-object v1

    .line 238
    :cond_2
    const-string v2, "playready"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 240
    iget-object v1, p0, Lgcw;->a:Landroid/util/SparseArray;

    const/4 v2, 0x5

    .line 241
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v3

    .line 240
    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected final b(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2

    .prologue
    .line 251
    const-string v0, "Role"

    invoke-static {p1, v0}, Lgcw;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x0

    const-string v1, "value"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgcw;->c:Ljava/lang/String;

    .line 254
    :cond_0
    return-void
.end method
