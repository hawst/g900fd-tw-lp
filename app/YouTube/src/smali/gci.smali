.class public final Lgci;
.super Lghl;
.source "SourceFile"

# interfaces
.implements Lgck;


# static fields
.field private static final a:Landroid/net/Uri;


# instance fields
.field private final b:Lgku;

.field private final c:Lgkv;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "https://www.googleapis.com/plus/v1whitelisted/people"

    .line 30
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lgci;->a:Landroid/net/Uri;

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Ljava/lang/String;Lezj;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lgkv;)V
    .locals 6

    .prologue
    .line 45
    invoke-direct {p0, p1, p4, p2, p3}, Lghl;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Lezj;)V

    .line 46
    iput-object p6, p0, Lgci;->c:Lgkv;

    .line 49
    const/16 v0, 0x1f4

    invoke-static {v0}, Lgci;->a(I)Leul;

    move-result-object v0

    .line 50
    new-instance v1, Lgcj;

    invoke-direct {v1}, Lgcj;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lgci;->c:Lgkv;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lgks;

    sget-object v4, Lewv;->b:Lewv;

    invoke-direct {v3, v4, v2}, Lgks;-><init>(Lewv;Ljava/util/List;)V

    invoke-virtual {p0, v3, v1}, Lgci;->a(Lgib;Lghv;)Lgko;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgci;->a(Lgku;)Lgjx;

    move-result-object v1

    new-instance v2, Lgkh;

    invoke-direct {v2, v1}, Lgkh;-><init>(Lgku;)V

    const-wide/32 v4, 0x6ddd00

    invoke-virtual {p0, v0, v2, v4, v5}, Lgci;->a(Leuk;Lgku;J)Lghi;

    move-result-object v0

    iput-object v0, p0, Lgci;->b:Lgku;

    .line 51
    return-void
.end method


# virtual methods
.method public final a(Leuc;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 86
    sget-object v0, Lgci;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "me"

    .line 87
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "fields"

    const-string v2, "displayName,id,image,url"

    .line 88
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 90
    new-instance v1, Lgkr;

    invoke-direct {v1, v0, v3, v3}, Lgkr;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    .line 91
    iget-object v0, p0, Lgci;->b:Lgku;

    invoke-interface {v0, v1, p1}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 92
    return-void
.end method
