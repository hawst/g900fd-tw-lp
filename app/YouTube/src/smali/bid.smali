.class public final Lbid;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbxy;


# instance fields
.field final a:Lbxu;

.field final b:Landroid/content/SharedPreferences;

.field private final c:Lbhz;

.field private final d:Ldwv;

.field private final e:Lbgt;

.field private f:Lcom/google/android/apps/youtube/app/ui/TutorialView;


# direct methods
.method public constructor <init>(Lbhz;Lbxu;Landroid/content/SharedPreferences;Ldwv;Lbgt;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhz;

    iput-object v0, p0, Lbid;->c:Lbhz;

    .line 43
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxu;

    iput-object v0, p0, Lbid;->a:Lbxu;

    .line 44
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lbid;->b:Landroid/content/SharedPreferences;

    .line 45
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwv;

    iput-object v0, p0, Lbid;->d:Ldwv;

    .line 46
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgt;

    iput-object v0, p0, Lbid;->e:Lbgt;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 51
    const/16 v0, 0x157c

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lbid;->e:Lbgt;

    iget-object v0, v0, Lbgt;->e:Landroid/support/v7/app/MediaRouteButton;

    .line 57
    if-eqz v0, :cond_3

    .line 58
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbid;->d:Ldwv;

    .line 59
    invoke-interface {v0}, Ldwv;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    invoke-virtual {v0}, Ldwr;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    .line 60
    :goto_1
    if-eqz v0, :cond_1

    iget-object v3, p0, Lbid;->c:Lbhz;

    instance-of v3, v3, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    if-eqz v3, :cond_1

    .line 61
    iget-object v0, p0, Lbid;->c:Lbhz;

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->o()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lbid;->c:Lbhz;

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 62
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->m()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    and-int/lit8 v0, v0, 0x1

    .line 64
    :cond_1
    return v0

    :cond_2
    move v0, v2

    .line 59
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    .line 62
    goto :goto_2
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lbid;->c:Lbhz;

    invoke-virtual {v0}, Lbhz;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 70
    iget-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    if-nez v1, :cond_0

    .line 71
    iget-object v1, p0, Lbid;->c:Lbhz;

    .line 72
    invoke-virtual {v1}, Lbhz;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04011a

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 73
    const v2, 0x7f080309

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iput-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    .line 74
    iget-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    new-instance v2, Lbie;

    invoke-direct {v2, p0}, Lbie;-><init>(Lbid;)V

    iput-object v2, v1, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a:Lbxt;

    .line 83
    iget-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    iget-object v2, p0, Lbid;->c:Lbhz;

    const v3, 0x7f090307

    invoke-virtual {v2, v3}, Lbhz;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a(I)V

    .line 87
    :cond_0
    iget-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    if-gez v1, :cond_1

    .line 88
    iget-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 90
    :cond_1
    iget-object v1, p0, Lbid;->e:Lbgt;

    iget-object v1, v1, Lbgt;->e:Landroid/support/v7/app/MediaRouteButton;

    .line 91
    iget-object v2, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a(Landroid/view/View;Landroid/view/View;)V

    .line 92
    iget-object v0, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->a()V

    .line 93
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lbid;->c:Lbhz;

    invoke-virtual {v0}, Lbhz;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 99
    iget-object v1, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 100
    iget-object v0, p0, Lbid;->f:Lcom/google/android/apps/youtube/app/ui/TutorialView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/TutorialView;->b()V

    .line 102
    :cond_0
    return-void
.end method

.method public final onMdxScreenAvailabilityChanged(Ldwt;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lbid;->a:Lbxu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 107
    return-void
.end method
