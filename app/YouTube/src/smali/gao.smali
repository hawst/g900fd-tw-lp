.class final Lgao;
.super Lfbd;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 308
    invoke-direct {p0}, Lfbd;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lfah;Lorg/xml/sax/Attributes;)V
    .locals 4

    .prologue
    .line 311
    const-class v0, Lgcf;

    invoke-virtual {p1, v0}, Lfah;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcf;

    .line 312
    const-string v1, "rel"

    invoke-interface {p2, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 313
    invoke-static {}, Lgae;->b()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 314
    const-string v2, "href"

    invoke-interface {p2, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 315
    const-string v3, "edit"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 316
    iput-object v2, v0, Lgcf;->h:Landroid/net/Uri;

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    const-string v3, "http://gdata.youtube.com/schemas/2007#video.captionTracks"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 318
    const-string v1, "true"

    const-string v3, "yt:hasEntries"

    invoke-interface {p2, v3}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 319
    iput-object v2, v0, Lgcf;->i:Landroid/net/Uri;

    goto :goto_0

    .line 321
    :cond_2
    const-string v3, "http://gdata.youtube.com/schemas/2007#live.event"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    iput-object v2, v0, Lgcf;->E:Landroid/net/Uri;

    goto :goto_0
.end method
