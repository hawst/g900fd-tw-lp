.class final Lc;
.super Laf;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Lb;

.field b:I

.field c:I

.field d:I

.field e:Z

.field f:Ljava/lang/String;

.field g:I

.field h:I

.field i:Ljava/lang/CharSequence;

.field j:I

.field k:Ljava/lang/CharSequence;

.field l:Ljava/util/ArrayList;

.field m:Ljava/util/ArrayList;

.field private n:Lv;

.field private o:Lb;

.field private p:Z


# direct methods
.method public constructor <init>(Lv;)V
    .locals 1

    .prologue
    .line 355
    invoke-direct {p0}, Laf;-><init>()V

    .line 228
    const/4 v0, -0x1

    iput v0, p0, Lc;->g:I

    .line 356
    iput-object p1, p0, Lc;->n:Lv;

    .line 357
    return-void
.end method

.method private a(Z)I
    .locals 2

    .prologue
    .line 623
    iget-boolean v0, p0, Lc;->p:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "commit already called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 624
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lc;->p:Z

    .line 631
    iget-boolean v0, p0, Lc;->e:Z

    if-eqz v0, :cond_1

    .line 632
    iget-object v0, p0, Lc;->n:Lv;

    invoke-virtual {v0, p0}, Lv;->a(Lc;)I

    move-result v0

    iput v0, p0, Lc;->g:I

    .line 636
    :goto_0
    iget-object v0, p0, Lc;->n:Lv;

    invoke-virtual {v0, p0, p1}, Lv;->a(Ljava/lang/Runnable;Z)V

    .line 637
    iget v0, p0, Lc;->g:I

    return v0

    .line 634
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lc;->g:I

    goto :goto_0
.end method

.method static synthetic a(Lc;Lf;ZLj;)Ldm;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 191
    new-instance v0, Ldm;

    invoke-direct {v0}, Ldm;-><init>()V

    iget-object v1, p3, Lj;->K:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lc;->l:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    invoke-static {v0, v1}, La;->a(Ljava/util/Map;Landroid/view/View;)V

    if-eqz p2, :cond_1

    iget-object v1, p0, Lc;->l:Ljava/util/ArrayList;

    iget-object v2, p0, Lc;->m:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Lc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ldm;)Ldm;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz p2, :cond_2

    invoke-direct {p0, p1, v0, v3}, Lc;->a(Lf;Ldm;Z)V

    :goto_1
    return-object v0

    :cond_1
    iget-object v1, p0, Lc;->m:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ldr;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    goto :goto_0

    :cond_2
    invoke-static {p1, v0, v3}, Lc;->b(Lf;Ldm;Z)V

    goto :goto_1
.end method

.method private a(Lf;Lj;Z)Ldm;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1089
    new-instance v0, Ldm;

    invoke-direct {v0}, Ldm;-><init>()V

    .line 1090
    iget-object v1, p0, Lc;->l:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 1091
    iget-object v1, p2, Lj;->K:Landroid/view/View;

    invoke-static {v0, v1}, La;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 1092
    if-eqz p3, :cond_1

    .line 1093
    iget-object v1, p0, Lc;->m:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ldr;->a(Ljava/util/Map;Ljava/util/Collection;)Z

    .line 1100
    :cond_0
    :goto_0
    if-eqz p3, :cond_2

    .line 1101
    invoke-direct {p0, p1, v0, v3}, Lc;->a(Lf;Ldm;Z)V

    .line 1114
    :goto_1
    return-object v0

    .line 1095
    :cond_1
    iget-object v1, p0, Lc;->l:Ljava/util/ArrayList;

    iget-object v2, p0, Lc;->m:Ljava/util/ArrayList;

    invoke-static {v1, v2, v0}, Lc;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ldm;)Ldm;

    move-result-object v0

    goto :goto_0

    .line 1107
    :cond_2
    invoke-static {p1, v0, v3}, Lc;->b(Lf;Ldm;Z)V

    goto :goto_1
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ldm;)Ldm;
    .locals 5

    .prologue
    .line 1321
    invoke-virtual {p2}, Ldm;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1332
    :goto_0
    return-object p2

    .line 1324
    :cond_0
    new-instance v1, Ldm;

    invoke-direct {v1}, Ldm;-><init>()V

    .line 1325
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1326
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    .line 1327
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ldm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1328
    if-eqz v0, :cond_1

    .line 1329
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4, v0}, Ldm;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1326
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object p2, v1

    .line 1332
    goto :goto_0
.end method

.method private a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Lf;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 1018
    new-instance v2, Lf;

    invoke-direct {v2, p0}, Lf;-><init>(Lc;)V

    .line 1023
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lc;->n:Lv;

    iget-object v1, v1, Lv;->h:Lo;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, v2, Lf;->d:Landroid/view/View;

    move v6, v7

    move v8, v7

    .line 1027
    :goto_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 1028
    invoke-virtual {p1, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    move-object v0, p0

    move v3, p3

    move-object v4, p1

    move-object v5, p2

    .line 1029
    invoke-direct/range {v0 .. v5}, Lc;->a(ILf;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v1, v9

    .line 1027
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v8, v1

    goto :goto_0

    .line 1036
    :cond_0
    :goto_2
    invoke-virtual {p2}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 1037
    invoke-virtual {p2, v7}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 1038
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, p0

    move v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lc;->a(ILf;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v8, v9

    .line 1036
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1045
    :cond_2
    if-nez v8, :cond_3

    .line 1046
    const/4 v2, 0x0

    .line 1049
    :cond_3
    return-object v2

    :cond_4
    move v1, v8

    goto :goto_1
.end method

.method private static a(Ljava/lang/Object;Lj;Ljava/util/ArrayList;Ldm;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1080
    if-eqz p0, :cond_1

    .line 1081
    iget-object v0, p1, Lj;->K:Landroid/view/View;

    if-eqz p0, :cond_1

    invoke-static {p2, v0}, La;->a(Ljava/util/ArrayList;Landroid/view/View;)V

    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p0, 0x0

    .line 1084
    :cond_1
    :goto_0
    return-object p0

    :cond_2
    move-object v0, p0

    .line 1081
    check-cast v0, Landroid/transition/Transition;

    invoke-static {v0, p2}, La;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private a(ILj;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 416
    iget-object v0, p0, Lc;->n:Lv;

    iput-object v0, p2, Lj;->v:Lv;

    .line 418
    if-eqz p3, :cond_1

    .line 419
    iget-object v0, p2, Lj;->B:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lj;->B:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 420
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change tag of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lj;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 424
    :cond_0
    iput-object p3, p2, Lj;->B:Ljava/lang/String;

    .line 427
    :cond_1
    if-eqz p1, :cond_3

    .line 428
    iget v0, p2, Lj;->z:I

    if-eqz v0, :cond_2

    iget v0, p2, Lj;->z:I

    if-eq v0, p1, :cond_2

    .line 429
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t change container ID of fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Lj;->z:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " now "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 433
    :cond_2
    iput p1, p2, Lj;->z:I

    iput p1, p2, Lj;->A:I

    .line 436
    :cond_3
    new-instance v0, Lb;

    invoke-direct {v0}, Lb;-><init>()V

    .line 437
    iput p4, v0, Lb;->c:I

    .line 438
    iput-object p2, v0, Lb;->d:Lj;

    .line 439
    invoke-virtual {p0, v0}, Lc;->a(Lb;)V

    .line 440
    return-void
.end method

.method private static a(Landroid/util/SparseArray;Lj;)V
    .locals 2

    .prologue
    .line 747
    if-eqz p1, :cond_0

    .line 748
    iget v0, p1, Lj;->A:I

    .line 749
    if-eqz v0, :cond_0

    iget-boolean v1, p1, Lj;->C:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Lj;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lj;->K:Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 751
    invoke-virtual {p0, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 754
    :cond_0
    return-void
.end method

.method static synthetic a(Lc;Ldm;Lf;)V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lc;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldm;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lc;->m:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p2, Lf;->c:Lak;

    iput-object v0, v1, Lak;->a:Landroid/view/View;

    :cond_0
    return-void
.end method

.method static synthetic a(Lc;Lf;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0, p1, p2, p3}, Lc;->a(Lf;ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lc;Lf;Lj;Lj;ZLdm;)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method

.method private static a(Ldm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1398
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1399
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ldm;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1400
    invoke-virtual {p0, v0}, Ldm;->c(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1401
    invoke-virtual {p0, v0, p2}, Ldm;->a(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1407
    :cond_0
    :goto_1
    return-void

    .line 1399
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1405
    :cond_2
    invoke-virtual {p0, p1, p2}, Ldm;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private a(Lf;ILjava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1375
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    move v1, v2

    .line 1376
    :goto_0
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1377
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 1378
    iget-object v3, v0, Lj;->K:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lj;->J:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    iget v3, v0, Lj;->A:I

    if-ne v3, p2, :cond_0

    .line 1380
    iget-boolean v3, v0, Lj;->C:Z

    if-eqz v3, :cond_1

    .line 1381
    iget-object v3, p1, Lf;->b:Ljava/util/ArrayList;

    iget-object v4, v0, Lj;->K:Landroid/view/View;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1382
    iget-object v3, v0, Lj;->K:Landroid/view/View;

    const/4 v4, 0x1

    invoke-static {p3, v3, v4}, La;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 1384
    iget-object v3, p1, Lf;->b:Ljava/util/ArrayList;

    iget-object v0, v0, Lj;->K:Landroid/view/View;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1376
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1387
    :cond_1
    iget-object v3, v0, Lj;->K:Landroid/view/View;

    invoke-static {p3, v3, v2}, La;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 1389
    iget-object v3, p1, Lf;->b:Ljava/util/ArrayList;

    iget-object v0, v0, Lj;->K:Landroid/view/View;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1394
    :cond_2
    return-void
.end method

.method private a(Lf;Ldm;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1422
    iget-object v1, p0, Lc;->m:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    move v2, v0

    :goto_0
    move v3, v0

    .line 1423
    :goto_1
    if-ge v3, v2, :cond_3

    .line 1424
    iget-object v0, p0, Lc;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1425
    iget-object v1, p0, Lc;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1426
    invoke-virtual {p2, v1}, Ldm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 1427
    if-eqz v1, :cond_0

    .line 1428
    invoke-virtual {v1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    .line 1429
    if-eqz p3, :cond_2

    .line 1430
    iget-object v4, p1, Lf;->a:Ldm;

    invoke-static {v4, v0, v1}, Lc;->a(Ldm;Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1422
    :cond_1
    iget-object v1, p0, Lc;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    goto :goto_0

    .line 1432
    :cond_2
    iget-object v4, p1, Lf;->a:Ldm;

    invoke-static {v4, v1, v0}, Lc;->a(Ldm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1436
    :cond_3
    return-void
.end method

.method private a(ILf;ZLandroid/util/SparseArray;Landroid/util/SparseArray;)Z
    .locals 29

    .prologue
    .line 1131
    move-object/from16 v0, p0

    iget-object v3, v0, Lc;->n:Lv;

    iget-object v3, v3, Lv;->i:Ls;

    move/from16 v0, p1

    invoke-interface {v3, v0}, Ls;->a(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 1132
    if-nez v5, :cond_0

    .line 1133
    const/4 v3, 0x0

    .line 1226
    :goto_0
    return v3

    .line 1135
    :cond_0
    move-object/from16 v0, p5

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lj;

    .line 1136
    move-object/from16 v0, p4

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lj;

    .line 1138
    if-nez v10, :cond_2

    const/16 v18, 0x0

    .line 1139
    :goto_1
    if-eqz v10, :cond_1

    if-nez v11, :cond_5

    :cond_1
    const/4 v6, 0x0

    .line 1141
    :goto_2
    if-nez v11, :cond_8

    const/4 v3, 0x0

    move-object v4, v3

    .line 1142
    :goto_3
    if-nez v18, :cond_b

    if-nez v6, :cond_b

    if-nez v4, :cond_b

    .line 1144
    const/4 v3, 0x0

    goto :goto_0

    .line 1138
    :cond_2
    if-eqz p3, :cond_4

    iget-object v3, v10, Lj;->S:Ljava/lang/Object;

    sget-object v4, Lj;->c:Ljava/lang/Object;

    if-ne v3, v4, :cond_3

    const/4 v3, 0x0

    :goto_4
    invoke-static {v3}, La;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    goto :goto_1

    :cond_3
    iget-object v3, v10, Lj;->S:Ljava/lang/Object;

    goto :goto_4

    :cond_4
    const/4 v3, 0x0

    goto :goto_4

    .line 1139
    :cond_5
    if-eqz p3, :cond_7

    iget-object v3, v11, Lj;->T:Ljava/lang/Object;

    sget-object v4, Lj;->c:Ljava/lang/Object;

    if-ne v3, v4, :cond_6

    const/4 v3, 0x0

    :goto_5
    invoke-static {v3}, La;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    goto :goto_2

    :cond_6
    iget-object v3, v11, Lj;->T:Ljava/lang/Object;

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    goto :goto_5

    .line 1141
    :cond_8
    if-eqz p3, :cond_a

    iget-object v3, v11, Lj;->R:Ljava/lang/Object;

    sget-object v4, Lj;->c:Ljava/lang/Object;

    if-ne v3, v4, :cond_9

    const/4 v3, 0x0

    :goto_6
    invoke-static {v3}, La;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    goto :goto_3

    :cond_9
    iget-object v3, v11, Lj;->R:Ljava/lang/Object;

    goto :goto_6

    :cond_a
    const/4 v3, 0x0

    goto :goto_6

    .line 1146
    :cond_b
    const/4 v3, 0x0

    .line 1147
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1148
    if-eqz v6, :cond_c

    .line 1149
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v11, v2}, Lc;->a(Lf;Lj;Z)Ldm;

    move-result-object v3

    .line 1150
    invoke-virtual {v3}, Ldm;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_15

    .line 1151
    move-object/from16 v0, p2

    iget-object v8, v0, Lf;->d:Landroid/view/View;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1157
    :cond_c
    :goto_7
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 1168
    move-object/from16 v0, v23

    invoke-static {v4, v11, v0, v3}, Lc;->a(Ljava/lang/Object;Lj;Ljava/util/ArrayList;Ldm;)Ljava/lang/Object;

    move-result-object v21

    .line 1172
    move-object/from16 v0, p0

    iget-object v4, v0, Lc;->m:Ljava/util/ArrayList;

    if-eqz v4, :cond_e

    if-eqz v3, :cond_e

    .line 1173
    move-object/from16 v0, p0

    iget-object v4, v0, Lc;->m:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ldm;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1174
    if-eqz v3, :cond_e

    .line 1175
    if-eqz v21, :cond_d

    .line 1176
    move-object/from16 v0, v21

    invoke-static {v0, v3}, La;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 1178
    :cond_d
    if-eqz v6, :cond_e

    .line 1179
    invoke-static {v6, v3}, La;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 1185
    :cond_e
    new-instance v17, La;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v10}, La;-><init>(Lc;Lj;)V

    .line 1193
    if-eqz v6, :cond_f

    .line 1194
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v12

    new-instance v3, Ld;

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    move/from16 v9, p3

    invoke-direct/range {v3 .. v11}, Ld;-><init>(Lc;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Lf;ZLj;Lj;)V

    invoke-virtual {v12, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1198
    :cond_f
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1199
    new-instance v16, Ldm;

    invoke-direct/range {v16 .. v16}, Ldm;-><init>()V

    move-object/from16 v3, v18

    .line 1203
    check-cast v3, Landroid/transition/Transition;

    move-object/from16 v4, v21

    check-cast v4, Landroid/transition/Transition;

    move-object v8, v6

    check-cast v8, Landroid/transition/Transition;

    if-eqz v3, :cond_10

    if-eqz v4, :cond_10

    :cond_10
    new-instance v9, Landroid/transition/TransitionSet;

    invoke-direct {v9}, Landroid/transition/TransitionSet;-><init>()V

    if-eqz v3, :cond_11

    invoke-virtual {v9, v3}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    :cond_11
    if-eqz v4, :cond_12

    invoke-virtual {v9, v4}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    :cond_12
    if-eqz v8, :cond_13

    invoke-virtual {v9, v8}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    :cond_13
    move-object v4, v9

    .line 1206
    if-eqz v4, :cond_14

    .line 1207
    move-object/from16 v0, p2

    iget-object v12, v0, Lf;->d:Landroid/view/View;

    move-object/from16 v0, p2

    iget-object v13, v0, Lf;->c:Lak;

    move-object/from16 v0, p2

    iget-object v14, v0, Lf;->a:Ldm;

    move-object/from16 v8, v18

    move-object v9, v6

    move-object v10, v5

    move-object/from16 v11, v17

    move-object/from16 v17, v7

    invoke-static/range {v8 .. v17}, La;->a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;Lal;Landroid/view/View;Lak;Ljava/util/Map;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;)V

    .line 1211
    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v8, Le;

    move-object/from16 v9, p0

    move-object v10, v5

    move-object/from16 v11, p2

    move/from16 v12, p1

    move-object v13, v4

    invoke-direct/range {v8 .. v13}, Le;-><init>(Lc;Landroid/view/View;Lf;ILjava/lang/Object;)V

    invoke-virtual {v3, v8}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1215
    move-object/from16 v0, p2

    iget-object v3, v0, Lf;->d:Landroid/view/View;

    const/4 v8, 0x1

    invoke-static {v4, v3, v8}, La;->a(Ljava/lang/Object;Landroid/view/View;Z)V

    .line 1217
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p1

    invoke-direct {v0, v1, v2, v4}, Lc;->a(Lf;ILjava/lang/Object;)V

    move-object v3, v4

    .line 1219
    check-cast v3, Landroid/transition/Transition;

    invoke-static {v5, v3}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 1221
    move-object/from16 v0, p2

    iget-object v0, v0, Lf;->d:Landroid/view/View;

    move-object/from16 v20, v0

    move-object/from16 v0, p2

    iget-object v0, v0, Lf;->b:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v19, v18

    check-cast v19, Landroid/transition/Transition;

    move-object/from16 v22, v21

    check-cast v22, Landroid/transition/Transition;

    move-object/from16 v24, v6

    check-cast v24, Landroid/transition/Transition;

    move-object/from16 v28, v4

    check-cast v28, Landroid/transition/Transition;

    if-eqz v28, :cond_14

    invoke-virtual {v5}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    new-instance v17, Laj;

    move-object/from16 v18, v5

    move-object/from16 v21, v15

    move-object/from16 v25, v7

    move-object/from16 v26, v16

    invoke-direct/range {v17 .. v28}, Laj;-><init>(Landroid/view/View;Landroid/transition/Transition;Landroid/view/View;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Landroid/transition/Transition;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;Landroid/transition/Transition;)V

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1226
    :cond_14
    if-eqz v4, :cond_16

    const/4 v3, 0x1

    goto/16 :goto_0

    .line 1153
    :cond_15
    invoke-virtual {v3}, Ldm;->values()Ljava/util/Collection;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    .line 1226
    :cond_16
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method private b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
    .locals 6

    .prologue
    .line 776
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->i:Ls;

    invoke-interface {v0}, Ls;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 821
    :cond_0
    return-void

    .line 779
    :cond_1
    iget-object v0, p0, Lc;->a:Lb;

    move-object v3, v0

    .line 780
    :goto_0
    if-eqz v3, :cond_0

    .line 781
    iget v0, v3, Lb;->c:I

    packed-switch v0, :pswitch_data_0

    .line 819
    :goto_1
    iget-object v0, v3, Lb;->a:Lb;

    move-object v3, v0

    goto :goto_0

    .line 783
    :pswitch_0
    iget-object v0, v3, Lb;->d:Lj;

    invoke-static {p2, v0}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 786
    :pswitch_1
    iget-object v1, v3, Lb;->d:Lj;

    .line 787
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 788
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_2
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 789
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 790
    if-eqz v2, :cond_2

    iget v4, v0, Lj;->A:I

    iget v5, v2, Lj;->A:I

    if-ne v4, v5, :cond_3

    .line 791
    :cond_2
    if-ne v0, v2, :cond_4

    .line 792
    const/4 v2, 0x0

    .line 788
    :cond_3
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 794
    :cond_4
    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_3

    :cond_5
    move-object v2, v1

    .line 799
    :cond_6
    invoke-static {p2, v2}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 803
    :pswitch_2
    iget-object v0, v3, Lb;->d:Lj;

    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 806
    :pswitch_3
    iget-object v0, v3, Lb;->d:Lj;

    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 809
    :pswitch_4
    iget-object v0, v3, Lb;->d:Lj;

    invoke-static {p2, v0}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 812
    :pswitch_5
    iget-object v0, v3, Lb;->d:Lj;

    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 815
    :pswitch_6
    iget-object v0, v3, Lb;->d:Lj;

    invoke-static {p2, v0}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 781
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static b(Landroid/util/SparseArray;Lj;)V
    .locals 1

    .prologue
    .line 757
    if-eqz p1, :cond_0

    .line 758
    iget v0, p1, Lj;->A:I

    .line 759
    if-eqz v0, :cond_0

    .line 760
    invoke-virtual {p0, v0, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 763
    :cond_0
    return-void
.end method

.method private static b(Lf;Ldm;Z)V
    .locals 5

    .prologue
    .line 1440
    invoke-virtual {p1}, Ldm;->size()I

    move-result v3

    .line 1441
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 1442
    invoke-virtual {p1, v2}, Ldm;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1443
    invoke-virtual {p1, v2}, Ldm;->c(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    .line 1444
    if-eqz p2, :cond_0

    .line 1445
    iget-object v4, p0, Lf;->a:Ldm;

    invoke-static {v4, v0, v1}, Lc;->a(Ldm;Ljava/lang/String;Ljava/lang/String;)V

    .line 1441
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1447
    :cond_0
    iget-object v4, p0, Lf;->a:Ldm;

    invoke-static {v4, v1, v0}, Lc;->a(Ldm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1450
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 615
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lc;->a(Z)I

    move-result v0

    return v0
.end method

.method public final a(I)Laf;
    .locals 0

    .prologue
    .line 514
    iput p1, p0, Lc;->c:I

    .line 515
    return-object p0
.end method

.method public final a(ILj;Ljava/lang/String;)Laf;
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lc;->a(ILj;Ljava/lang/String;I)V

    .line 412
    return-object p0
.end method

.method public final a(Lj;)Laf;
    .locals 2

    .prologue
    .line 456
    new-instance v0, Lb;

    invoke-direct {v0}, Lb;-><init>()V

    .line 457
    const/4 v1, 0x3

    iput v1, v0, Lb;->c:I

    .line 458
    iput-object p1, v0, Lb;->d:Lj;

    .line 459
    invoke-virtual {p0, v0}, Lc;->a(Lb;)V

    .line 461
    return-object p0
.end method

.method public final a(Lj;Ljava/lang/String;)Laf;
    .locals 2

    .prologue
    .line 401
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lc;->a(ILj;Ljava/lang/String;I)V

    .line 402
    return-object p0
.end method

.method public final a(ZLf;Landroid/util/SparseArray;Landroid/util/SparseArray;)Lf;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v3, 0x0

    .line 874
    if-nez p2, :cond_2

    .line 882
    invoke-virtual {p3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p4}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 883
    :cond_0
    invoke-direct {p0, p3, p4, v10}, Lc;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Lf;

    move-result-object p2

    .line 889
    :cond_1
    invoke-virtual {p0, v9}, Lc;->b(I)V

    .line 891
    if-eqz p2, :cond_3

    move v7, v3

    .line 892
    :goto_0
    if-eqz p2, :cond_4

    move v1, v3

    .line 893
    :goto_1
    iget-object v0, p0, Lc;->o:Lb;

    move-object v6, v0

    .line 894
    :goto_2
    if-eqz v6, :cond_9

    .line 895
    if-eqz p2, :cond_5

    move v5, v3

    .line 896
    :goto_3
    if-eqz p2, :cond_6

    move v0, v3

    .line 897
    :goto_4
    iget v2, v6, Lb;->c:I

    packed-switch v2, :pswitch_data_0

    .line 949
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cmd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v6, Lb;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_2
    if-nez p1, :cond_1

    .line 886
    iget-object v5, p0, Lc;->m:Ljava/util/ArrayList;

    iget-object v6, p0, Lc;->l:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    move v2, v3

    :goto_5
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v7, p2, Lf;->a:Ldm;

    invoke-static {v7, v0, v1}, Lc;->a(Ldm;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 891
    :cond_3
    iget v0, p0, Lc;->d:I

    move v7, v0

    goto :goto_0

    .line 892
    :cond_4
    iget v0, p0, Lc;->c:I

    move v1, v0

    goto :goto_1

    .line 895
    :cond_5
    iget v0, v6, Lb;->g:I

    move v5, v0

    goto :goto_3

    .line 896
    :cond_6
    iget v0, v6, Lb;->h:I

    goto :goto_4

    .line 899
    :pswitch_0
    iget-object v2, v6, Lb;->d:Lj;

    .line 900
    iput v0, v2, Lj;->I:I

    .line 901
    iget-object v0, p0, Lc;->n:Lv;

    invoke-static {v1}, Lv;->b(I)I

    move-result v5

    invoke-virtual {v0, v2, v5, v7}, Lv;->a(Lj;II)V

    .line 953
    :cond_7
    :goto_6
    iget-object v0, v6, Lb;->b:Lb;

    move-object v6, v0

    .line 954
    goto :goto_2

    .line 905
    :pswitch_1
    iget-object v2, v6, Lb;->d:Lj;

    .line 906
    if-eqz v2, :cond_8

    .line 907
    iput v0, v2, Lj;->I:I

    .line 908
    iget-object v0, p0, Lc;->n:Lv;

    invoke-static {v1}, Lv;->b(I)I

    move-result v8

    invoke-virtual {v0, v2, v8, v7}, Lv;->a(Lj;II)V

    .line 911
    :cond_8
    iget-object v0, v6, Lb;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    move v2, v3

    .line 912
    :goto_7
    iget-object v0, v6, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 913
    iget-object v0, v6, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 914
    iput v5, v0, Lj;->I:I

    .line 915
    iget-object v8, p0, Lc;->n:Lv;

    invoke-virtual {v8, v0, v3}, Lv;->a(Lj;Z)V

    .line 912
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    .line 920
    :pswitch_2
    iget-object v0, v6, Lb;->d:Lj;

    .line 921
    iput v5, v0, Lj;->I:I

    .line 922
    iget-object v2, p0, Lc;->n:Lv;

    invoke-virtual {v2, v0, v3}, Lv;->a(Lj;Z)V

    goto :goto_6

    .line 925
    :pswitch_3
    iget-object v0, v6, Lb;->d:Lj;

    .line 926
    iput v5, v0, Lj;->I:I

    .line 927
    iget-object v2, p0, Lc;->n:Lv;

    invoke-static {v1}, Lv;->b(I)I

    move-result v5

    invoke-virtual {v2, v0, v5, v7}, Lv;->c(Lj;II)V

    goto :goto_6

    .line 931
    :pswitch_4
    iget-object v2, v6, Lb;->d:Lj;

    .line 932
    iput v0, v2, Lj;->I:I

    .line 933
    iget-object v0, p0, Lc;->n:Lv;

    invoke-static {v1}, Lv;->b(I)I

    move-result v5

    invoke-virtual {v0, v2, v5, v7}, Lv;->b(Lj;II)V

    goto :goto_6

    .line 937
    :pswitch_5
    iget-object v0, v6, Lb;->d:Lj;

    .line 938
    iput v5, v0, Lj;->I:I

    .line 939
    iget-object v2, p0, Lc;->n:Lv;

    invoke-static {v1}, Lv;->b(I)I

    move-result v5

    invoke-virtual {v2, v0, v5, v7}, Lv;->e(Lj;II)V

    goto :goto_6

    .line 943
    :pswitch_6
    iget-object v0, v6, Lb;->d:Lj;

    .line 944
    iput v5, v0, Lj;->I:I

    .line 945
    iget-object v2, p0, Lc;->n:Lv;

    invoke-static {v1}, Lv;->b(I)I

    move-result v5

    invoke-virtual {v2, v0, v5, v7}, Lv;->d(Lj;II)V

    goto :goto_6

    .line 956
    :cond_9
    if-eqz p1, :cond_a

    .line 957
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v2, p0, Lc;->n:Lv;

    iget v2, v2, Lv;->g:I

    invoke-static {v1}, Lv;->b(I)I

    move-result v1

    invoke-virtual {v0, v2, v1, v7, v10}, Lv;->a(IIIZ)V

    move-object p2, v4

    .line 962
    :cond_a
    iget v0, p0, Lc;->g:I

    if-ltz v0, :cond_c

    .line 963
    iget-object v1, p0, Lc;->n:Lv;

    iget v0, p0, Lc;->g:I

    monitor-enter v1

    :try_start_0
    iget-object v2, v1, Lv;->e:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v1, Lv;->f:Ljava/util/ArrayList;

    if-nez v2, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Lv;->f:Ljava/util/ArrayList;

    :cond_b
    iget-object v2, v1, Lv;->f:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 964
    iput v9, p0, Lc;->g:I

    .line 966
    :cond_c
    return-object p2

    .line 963
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 897
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Landroid/util/SparseArray;Landroid/util/SparseArray;)V
    .locals 3

    .prologue
    .line 834
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->i:Ls;

    invoke-interface {v0}, Ls;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 870
    :cond_0
    return-void

    .line 837
    :cond_1
    iget-object v0, p0, Lc;->a:Lb;

    move-object v2, v0

    .line 838
    :goto_0
    if-eqz v2, :cond_0

    .line 839
    iget v0, v2, Lb;->c:I

    packed-switch v0, :pswitch_data_0

    .line 868
    :goto_1
    iget-object v0, v2, Lb;->a:Lb;

    move-object v2, v0

    goto :goto_0

    .line 841
    :pswitch_0
    iget-object v0, v2, Lb;->d:Lj;

    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 844
    :pswitch_1
    iget-object v0, v2, Lb;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    .line 845
    iget-object v0, v2, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_2

    .line 846
    iget-object v0, v2, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    invoke-static {p2, v0}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    .line 845
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 849
    :cond_2
    iget-object v0, v2, Lb;->d:Lj;

    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 852
    :pswitch_2
    iget-object v0, v2, Lb;->d:Lj;

    invoke-static {p2, v0}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 855
    :pswitch_3
    iget-object v0, v2, Lb;->d:Lj;

    invoke-static {p2, v0}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 858
    :pswitch_4
    iget-object v0, v2, Lb;->d:Lj;

    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 861
    :pswitch_5
    iget-object v0, v2, Lb;->d:Lj;

    invoke-static {p2, v0}, Lc;->b(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 864
    :pswitch_6
    iget-object v0, v2, Lb;->d:Lj;

    invoke-static {p1, v0}, Lc;->a(Landroid/util/SparseArray;Lj;)V

    goto :goto_1

    .line 839
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method final a(Lb;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 386
    iget-object v0, p0, Lc;->a:Lb;

    if-nez v0, :cond_0

    .line 387
    iput-object p1, p0, Lc;->o:Lb;

    iput-object p1, p0, Lc;->a:Lb;

    .line 393
    :goto_0
    iput v1, p1, Lb;->e:I

    .line 394
    iput v1, p1, Lb;->f:I

    .line 395
    iput v1, p1, Lb;->g:I

    .line 396
    iput v1, p1, Lb;->h:I

    .line 397
    iget v0, p0, Lc;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lc;->b:I

    .line 398
    return-void

    .line 389
    :cond_0
    iget-object v0, p0, Lc;->o:Lb;

    iput-object v0, p1, Lb;->b:Lb;

    .line 390
    iget-object v0, p0, Lc;->o:Lb;

    iput-object p1, v0, Lb;->a:Lb;

    .line 391
    iput-object p1, p0, Lc;->o:Lb;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 259
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lc;->f:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mIndex="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lc;->g:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mCommitted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lc;->p:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    iget v0, p0, Lc;->c:I

    if-eqz v0, :cond_0

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTransition=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lc;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mTransitionStyle=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lc;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lc;->h:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lc;->i:Ljava/lang/CharSequence;

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lc;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mBreadCrumbTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lc;->i:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_2
    iget v0, p0, Lc;->j:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lc;->k:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbShortTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lc;->j:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " mBreadCrumbShortTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lc;->k:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lc;->a:Lb;

    if-eqz v0, :cond_c

    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Operations:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lc;->a:Lb;

    move v2, v1

    move-object v3, v0

    :goto_0
    if-eqz v3, :cond_c

    iget v0, v3, Lb;->c:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "cmd="

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, Lb;->c:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  Op #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, v3, Lb;->d:Lj;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget v0, v3, Lb;->e:I

    if-nez v0, :cond_5

    iget v0, v3, Lb;->f:I

    if-eqz v0, :cond_6

    :cond_5
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "enterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lb;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " exitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lb;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_6
    iget v0, v3, Lb;->g:I

    if-nez v0, :cond_7

    iget v0, v3, Lb;->h:I

    if-eqz v0, :cond_8

    :cond_7
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "popEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lb;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, " popExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, v3, Lb;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_8
    iget-object v0, v3, Lb;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, v3, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    move v0, v1

    :goto_2
    iget-object v5, v3, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_b

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v3, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_9

    const-string v5, "Removed: "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    :goto_3
    iget-object v5, v3, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_0
    const-string v0, "NULL"

    goto/16 :goto_1

    :pswitch_1
    const-string v0, "ADD"

    goto/16 :goto_1

    :pswitch_2
    const-string v0, "REPLACE"

    goto/16 :goto_1

    :pswitch_3
    const-string v0, "REMOVE"

    goto/16 :goto_1

    :pswitch_4
    const-string v0, "HIDE"

    goto/16 :goto_1

    :pswitch_5
    const-string v0, "SHOW"

    goto/16 :goto_1

    :pswitch_6
    const-string v0, "DETACH"

    goto/16 :goto_1

    :pswitch_7
    const-string v0, "ATTACH"

    goto/16 :goto_1

    :cond_9
    if-nez v0, :cond_a

    const-string v5, "Removed:"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_3

    :cond_b
    iget-object v3, v3, Lb;->a:Lb;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 260
    :cond_c
    return-void

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 619
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lc;->a(Z)I

    move-result v0

    return v0
.end method

.method public final b(ILj;Ljava/lang/String;)Laf;
    .locals 2

    .prologue
    .line 447
    if-nez p1, :cond_0

    .line 448
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must use non-zero containerViewId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 451
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, p1, p2, p3, v0}, Lc;->a(ILj;Ljava/lang/String;I)V

    .line 452
    return-object p0
.end method

.method public final b(Lj;)Laf;
    .locals 2

    .prologue
    .line 483
    new-instance v0, Lb;

    invoke-direct {v0}, Lb;-><init>()V

    .line 484
    const/4 v1, 0x6

    iput v1, v0, Lb;->c:I

    .line 485
    iput-object p1, v0, Lb;->d:Lj;

    .line 486
    invoke-virtual {p0, v0}, Lc;->a(Lb;)V

    .line 488
    return-object p0
.end method

.method final b(I)V
    .locals 4

    .prologue
    .line 590
    iget-boolean v0, p0, Lc;->e:Z

    if-nez v0, :cond_1

    .line 612
    :cond_0
    return-void

    .line 593
    :cond_1
    iget-object v0, p0, Lc;->a:Lb;

    move-object v2, v0

    .line 596
    :goto_0
    if-eqz v2, :cond_0

    .line 597
    iget-object v0, v2, Lb;->d:Lj;

    if-eqz v0, :cond_2

    .line 598
    iget-object v0, v2, Lb;->d:Lj;

    iget v1, v0, Lj;->u:I

    add-int/2addr v1, p1

    iput v1, v0, Lj;->u:I

    .line 599
    :cond_2
    iget-object v0, v2, Lb;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 603
    iget-object v0, v2, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    .line 604
    iget-object v0, v2, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 605
    iget v3, v0, Lj;->u:I

    add-int/2addr v3, p1

    iput v3, v0, Lj;->u:I

    .line 606
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 610
    :cond_3
    iget-object v0, v2, Lb;->a:Lb;

    move-object v2, v0

    goto :goto_0
.end method

.method public final c(Lj;)Laf;
    .locals 2

    .prologue
    .line 492
    new-instance v0, Lb;

    invoke-direct {v0}, Lb;-><init>()V

    .line 493
    const/4 v1, 0x7

    iput v1, v0, Lb;->c:I

    .line 494
    iput-object p1, v0, Lb;->d:Lj;

    .line 495
    invoke-virtual {p0, v0}, Lc;->a(Lb;)V

    .line 497
    return-object p0
.end method

.method public final run()V
    .locals 14

    .prologue
    const/4 v6, 0x0

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 641
    iget-boolean v0, p0, Lc;->e:Z

    if-eqz v0, :cond_0

    .line 644
    iget v0, p0, Lc;->g:I

    if-gez v0, :cond_0

    .line 645
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "addToBackStack() called after commit()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 649
    :cond_0
    invoke-virtual {p0, v13}, Lc;->b(I)V

    .line 652
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_10

    .line 655
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 656
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 658
    invoke-direct {p0, v0, v1}, Lc;->b(Landroid/util/SparseArray;Landroid/util/SparseArray;)V

    .line 660
    invoke-direct {p0, v0, v1, v2}, Lc;->a(Landroid/util/SparseArray;Landroid/util/SparseArray;Z)Lf;

    move-result-object v0

    move-object v10, v0

    .line 663
    :goto_0
    if-eqz v10, :cond_1

    move v9, v2

    .line 664
    :goto_1
    if-eqz v10, :cond_2

    move v1, v2

    .line 665
    :goto_2
    iget-object v0, p0, Lc;->a:Lb;

    move-object v8, v0

    .line 666
    :goto_3
    if-eqz v8, :cond_d

    .line 667
    if-eqz v10, :cond_3

    move v7, v2

    .line 668
    :goto_4
    if-eqz v10, :cond_4

    move v3, v2

    .line 669
    :goto_5
    iget v0, v8, Lb;->c:I

    packed-switch v0, :pswitch_data_0

    .line 732
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cmd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v8, Lb;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 663
    :cond_1
    iget v0, p0, Lc;->d:I

    move v9, v0

    goto :goto_1

    .line 664
    :cond_2
    iget v0, p0, Lc;->c:I

    move v1, v0

    goto :goto_2

    .line 667
    :cond_3
    iget v0, v8, Lb;->e:I

    move v7, v0

    goto :goto_4

    .line 668
    :cond_4
    iget v0, v8, Lb;->f:I

    move v3, v0

    goto :goto_5

    .line 671
    :pswitch_0
    iget-object v0, v8, Lb;->d:Lj;

    .line 672
    iput v7, v0, Lj;->I:I

    .line 673
    iget-object v3, p0, Lc;->n:Lv;

    invoke-virtual {v3, v0, v2}, Lv;->a(Lj;Z)V

    .line 736
    :cond_5
    :goto_6
    iget-object v0, v8, Lb;->a:Lb;

    move-object v8, v0

    .line 737
    goto :goto_3

    .line 676
    :pswitch_1
    iget-object v0, v8, Lb;->d:Lj;

    .line 677
    iget-object v4, p0, Lc;->n:Lv;

    iget-object v4, v4, Lv;->c:Ljava/util/ArrayList;

    if-eqz v4, :cond_b

    move v4, v2

    move-object v5, v0

    .line 678
    :goto_7
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_c

    .line 679
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v0, v0, Lv;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lj;

    .line 680
    if-eqz v5, :cond_6

    iget v11, v0, Lj;->A:I

    iget v12, v5, Lj;->A:I

    if-ne v11, v12, :cond_7

    .line 683
    :cond_6
    if-ne v0, v5, :cond_8

    .line 684
    iput-object v6, v8, Lb;->d:Lj;

    move-object v5, v6

    .line 678
    :cond_7
    :goto_8
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_7

    .line 686
    :cond_8
    iget-object v11, v8, Lb;->i:Ljava/util/ArrayList;

    if-nez v11, :cond_9

    .line 687
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, v8, Lb;->i:Ljava/util/ArrayList;

    .line 689
    :cond_9
    iget-object v11, v8, Lb;->i:Ljava/util/ArrayList;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 690
    iput v3, v0, Lj;->I:I

    .line 691
    iget-boolean v11, p0, Lc;->e:Z

    if-eqz v11, :cond_a

    .line 692
    iget v11, v0, Lj;->u:I

    add-int/lit8 v11, v11, 0x1

    iput v11, v0, Lj;->u:I

    .line 693
    :cond_a
    iget-object v11, p0, Lc;->n:Lv;

    invoke-virtual {v11, v0, v1, v9}, Lv;->a(Lj;II)V

    goto :goto_8

    :cond_b
    move-object v5, v0

    .line 701
    :cond_c
    if-eqz v5, :cond_5

    .line 702
    iput v7, v5, Lj;->I:I

    .line 703
    iget-object v0, p0, Lc;->n:Lv;

    invoke-virtual {v0, v5, v2}, Lv;->a(Lj;Z)V

    goto :goto_6

    .line 707
    :pswitch_2
    iget-object v0, v8, Lb;->d:Lj;

    .line 708
    iput v3, v0, Lj;->I:I

    .line 709
    iget-object v3, p0, Lc;->n:Lv;

    invoke-virtual {v3, v0, v1, v9}, Lv;->a(Lj;II)V

    goto :goto_6

    .line 712
    :pswitch_3
    iget-object v0, v8, Lb;->d:Lj;

    .line 713
    iput v3, v0, Lj;->I:I

    .line 714
    iget-object v3, p0, Lc;->n:Lv;

    invoke-virtual {v3, v0, v1, v9}, Lv;->b(Lj;II)V

    goto :goto_6

    .line 717
    :pswitch_4
    iget-object v0, v8, Lb;->d:Lj;

    .line 718
    iput v7, v0, Lj;->I:I

    .line 719
    iget-object v3, p0, Lc;->n:Lv;

    invoke-virtual {v3, v0, v1, v9}, Lv;->c(Lj;II)V

    goto :goto_6

    .line 722
    :pswitch_5
    iget-object v0, v8, Lb;->d:Lj;

    .line 723
    iput v3, v0, Lj;->I:I

    .line 724
    iget-object v3, p0, Lc;->n:Lv;

    invoke-virtual {v3, v0, v1, v9}, Lv;->d(Lj;II)V

    goto/16 :goto_6

    .line 727
    :pswitch_6
    iget-object v0, v8, Lb;->d:Lj;

    .line 728
    iput v7, v0, Lj;->I:I

    .line 729
    iget-object v3, p0, Lc;->n:Lv;

    invoke-virtual {v3, v0, v1, v9}, Lv;->e(Lj;II)V

    goto/16 :goto_6

    .line 739
    :cond_d
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v2, p0, Lc;->n:Lv;

    iget v2, v2, Lv;->g:I

    invoke-virtual {v0, v2, v1, v9, v13}, Lv;->a(IIIZ)V

    .line 741
    iget-boolean v0, p0, Lc;->e:Z

    if-eqz v0, :cond_f

    .line 742
    iget-object v0, p0, Lc;->n:Lv;

    iget-object v1, v0, Lv;->d:Ljava/util/ArrayList;

    if-nez v1, :cond_e

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lv;->d:Ljava/util/ArrayList;

    :cond_e
    iget-object v0, v0, Lv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 744
    :cond_f
    return-void

    :cond_10
    move-object v10, v6

    goto/16 :goto_0

    .line 669
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 244
    const-string v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    iget v1, p0, Lc;->g:I

    if-ltz v1, :cond_0

    .line 247
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    iget v1, p0, Lc;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 250
    :cond_0
    iget-object v1, p0, Lc;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 251
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    iget-object v1, p0, Lc;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
