.class public final Lgpw;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Lfdw;

.field private final b:Lfrz;

.field private final c:Li;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/widget/TextView;

.field private g:Lfmp;


# direct methods
.method public constructor <init>(Lfhz;Lfdw;Lfrz;Li;)V
    .locals 5

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 43
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lgpw;->a:Lfdw;

    .line 45
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lgpw;->b:Lfrz;

    .line 46
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Li;

    iput-object v0, p0, Lgpw;->c:Li;

    .line 48
    invoke-virtual {p4}, Li;->j()Lo;

    move-result-object v0

    const v1, 0x7f040103

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lgpw;->d:Landroid/view/View;

    .line 49
    iget-object v0, p0, Lgpw;->d:Landroid/view/View;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, 0x0

    const/4 v3, -0x2

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    iget-object v0, p0, Lgpw;->d:Landroid/view/View;

    const v1, 0x7f08008a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lgpw;->e:Landroid/widget/ImageView;

    .line 51
    iget-object v0, p0, Lgpw;->d:Landroid/view/View;

    const v1, 0x7f080286

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lgpw;->f:Landroid/widget/TextView;

    .line 53
    iget-object v0, p0, Lgpw;->d:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method

.method private a(Lfsg;Lfmp;)Landroid/view/View;
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 60
    iget-object v0, p0, Lgpw;->e:Landroid/widget/ImageView;

    iget-object v1, p2, Lfmp;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 61
    iget-object v0, p0, Lgpw;->f:Landroid/widget/TextView;

    invoke-virtual {p2}, Lfmp;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iput-object p2, p0, Lgpw;->g:Lfmp;

    .line 64
    iget-object v0, p0, Lgpw;->d:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfmp;

    invoke-direct {p0, p1, p2}, Lgpw;->a(Lfsg;Lfmp;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfmp;

    invoke-direct {p0, p1, p2}, Lgpw;->a(Lfsg;Lfmp;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfmp;

    invoke-direct {p0, p1, p2}, Lgpw;->a(Lfsg;Lfmp;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 69
    invoke-super {p0, p1}, Lfsb;->onClick(Landroid/view/View;)V

    .line 71
    iget-object v0, p0, Lgpw;->g:Lfmp;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lgpw;->a:Lfdw;

    iget-object v1, p0, Lgpw;->b:Lfrz;

    .line 75
    invoke-interface {v1}, Lfrz;->B()Lfqg;

    move-result-object v1

    iget-object v2, p0, Lgpw;->g:Lfmp;

    const/4 v3, 0x0

    .line 74
    invoke-virtual {v0, v1, v2, v3}, Lfdw;->a(Lfqg;Lfqh;Lhcq;)V

    .line 80
    :cond_0
    iget-object v0, p0, Lgpw;->c:Li;

    invoke-virtual {v0}, Li;->a()V

    .line 81
    return-void
.end method
