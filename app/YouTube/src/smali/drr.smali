.class public abstract Ldrr;
.super Lrm;
.source "SourceFile"


# instance fields
.field final i:Ldyg;

.field public final j:Ldwv;

.field public final k:Ldwq;

.field public final l:Ldxe;

.field m:Z

.field n:I

.field private final o:Ljava/util/Map;

.field private p:Z

.field private q:Ldru;

.field private r:Ldrt;


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Levn;Ldwv;Ldwq;Ldxe;Ldyg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1}, Lrm;-><init>(Landroid/content/Context;)V

    .line 63
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwv;

    iput-object v0, p0, Ldrr;->j:Ldwv;

    .line 64
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldrr;->k:Ldwq;

    .line 65
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldxe;

    iput-object v0, p0, Ldrr;->l:Ldxe;

    .line 66
    iput-object p7, p0, Ldrr;->i:Ldyg;

    .line 67
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ldrr;->o:Ljava/util/Map;

    .line 69
    iput-boolean v1, p0, Ldrr;->p:Z

    .line 70
    iput-boolean v1, p0, Ldrr;->m:Z

    .line 72
    new-instance v0, Ldru;

    invoke-direct {v0, p0}, Ldru;-><init>(Ldrr;)V

    iput-object v0, p0, Ldrr;->q:Ldru;

    .line 73
    iget-object v0, p0, Ldrr;->q:Ldru;

    invoke-virtual {p2, v0}, Levn;->a(Ljava/lang/Object;)V

    .line 75
    new-instance v0, Ldrt;

    invoke-direct {v0, p0}, Ldrt;-><init>(Ldrr;)V

    iput-object v0, p0, Ldrr;->r:Ldrt;

    .line 76
    iget-object v0, p0, Ldrr;->r:Ldrt;

    invoke-virtual {p3, v0}, Levn;->a(Ljava/lang/Object;)V

    .line 77
    return-void
.end method

.method static synthetic a(Ldrr;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ldrr;->f()V

    return-void
.end method

.method protected static b(Ldwr;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Ldwr;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 161
    iget-boolean v0, p0, Ldrr;->p:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldrr;->m:Z

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Ldrr;->j:Ldwv;

    invoke-virtual {p0}, Ldrr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ldwv;->b(Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Ldrr;->j:Ldwv;

    invoke-virtual {p0}, Ldrr;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ldwv;->a(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Ldwr;)Landroid/os/Bundle;
.end method

.method protected final b(Ljava/lang/String;)Ldwr;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ldrr;->o:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    return-object v0
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public final b(Lrl;)V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Ldrr;->i:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "discoveryRequestChanged : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0}, Ldrr;->b()Ljava/lang/String;

    move-result-object v0

    .line 97
    if-eqz p1, :cond_0

    .line 98
    invoke-virtual {p1}, Lrl;->a()Lrz;

    move-result-object v1

    .line 99
    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v1}, Lrz;->a()Ljava/util/List;

    move-result-object v1

    .line 101
    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldrr;->p:Z

    .line 103
    invoke-direct {p0}, Ldrr;->f()V

    .line 104
    invoke-virtual {p0}, Ldrr;->e()V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldrr;->p:Z

    .line 110
    invoke-direct {p0}, Ldrr;->f()V

    goto :goto_0
.end method

.method protected c(Ldwr;)V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method protected abstract c()Z
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, ""

    return-object v0
.end method

.method protected final e()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 122
    iget-object v0, p0, Ldrr;->o:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 123
    new-instance v1, Lrs;

    invoke-direct {v1}, Lrs;-><init>()V

    .line 125
    iget-object v0, p0, Ldrr;->j:Ldwv;

    invoke-interface {v0}, Ldwv;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    .line 126
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    invoke-virtual {p0}, Ldrr;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    new-instance v4, Lrk;

    invoke-static {v0}, Ldrr;->b(Ldwr;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lrk;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Lrk;->a(Landroid/content/IntentFilter;)Lrk;

    move-result-object v3

    invoke-virtual {v3, v7}, Lrk;->a(I)Lrk;

    move-result-object v3

    invoke-virtual {v3, v7}, Lrk;->e(I)Lrk;

    move-result-object v3

    invoke-virtual {v3, v7}, Lrk;->a(Z)Lrk;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lrk;->d(I)Lrk;

    move-result-object v3

    invoke-virtual {p0}, Ldrr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p0}, Ldrr;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lrk;->a(Ljava/lang/String;)Lrk;

    :cond_0
    invoke-virtual {p0, v0}, Ldrr;->a(Ldwr;)Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lrk;->a(Landroid/os/Bundle;)Lrk;

    iget-object v4, p0, Ldrr;->k:Ldwq;

    invoke-interface {v4, v0}, Ldwq;->a(Ldwr;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Ldrr;->n:I

    invoke-virtual {v3, v4}, Lrk;->c(I)Lrk;

    :cond_1
    invoke-virtual {v3}, Lrk;->a()Lrj;

    move-result-object v3

    .line 127
    invoke-virtual {v1, v3}, Lrs;->a(Lrj;)Lrs;

    .line 128
    iget-object v4, p0, Ldrr;->o:Ljava/util/Map;

    invoke-virtual {v3}, Lrj;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v4, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {v1}, Lrs;->a()Lrr;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldrr;->a(Lrr;)V

    .line 131
    return-void
.end method
