.class public final Lcdq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field final a:Landroid/view/View;

.field final b:Landroid/widget/TextView;

.field c:Lcdt;

.field private final d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const v0, 0x7f04006d

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcdq;->d:Landroid/view/View;

    .line 51
    iget-object v0, p0, Lcdq;->d:Landroid/view/View;

    const v1, 0x7f0801da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcdq;->b:Landroid/widget/TextView;

    .line 52
    iget-object v0, p0, Lcdq;->d:Landroid/view/View;

    const v1, 0x7f0801db

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcdq;->a:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcdq;->a:Landroid/view/View;

    new-instance v1, Lcdr;

    invoke-direct {v1, p0}, Lcdr;-><init>(Lcdq;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    .prologue
    .line 22
    check-cast p2, Lcdt;

    iput-object p2, p0, Lcdq;->c:Lcdt;

    iget-object v1, p0, Lcdq;->a:Landroid/view/View;

    invoke-static {p2}, Lcdt;->a(Lcdt;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcdq;->b:Landroid/widget/TextView;

    invoke-static {p2}, Lcdt;->a(Lcdt;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcdt;->b(Lcdt;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcdq;->d:Landroid/view/View;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcdt;->c(Lcdt;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1
.end method
