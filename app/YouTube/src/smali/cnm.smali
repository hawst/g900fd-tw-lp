.class public final Lcnm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lgjp;

.field private final b:Lgjp;

.field private final c:Lezj;

.field private final d:Levn;

.field private final e:Lcwq;

.field private final f:Lert;

.field private final g:Lgoc;

.field private h:Lcuz;


# direct methods
.method public constructor <init>(Lgjp;Lgjp;Lezj;Levn;Lcwq;Lert;Lcuz;Lgoc;)V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcnm;->a:Lgjp;

    .line 176
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcnm;->b:Lgjp;

    .line 177
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcnm;->c:Lezj;

    .line 178
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcnm;->d:Levn;

    .line 179
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwq;

    iput-object v0, p0, Lcnm;->e:Lcwq;

    .line 180
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcuz;

    iput-object v0, p0, Lcnm;->h:Lcuz;

    .line 181
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lert;

    iput-object v0, p0, Lcnm;->f:Lert;

    .line 182
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoc;

    iput-object v0, p0, Lcnm;->g:Lgoc;

    .line 183
    return-void
.end method

.method private a(Less;)Lcnw;
    .locals 5

    .prologue
    .line 294
    if-eqz p1, :cond_0

    iget-object v0, p1, Less;->m:Lesn;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p1, Less;->m:Lesn;

    iget-object v0, v0, Lesn;->b:Ljava/util/regex/Pattern;

    .line 296
    :goto_0
    new-instance v1, Lcnw;

    iget-object v2, p0, Lcnm;->a:Lgjp;

    iget-object v3, p0, Lcnm;->b:Lgjp;

    iget-object v4, p0, Lcnm;->c:Lezj;

    invoke-direct {v1, v2, v3, v0, v4}, Lcnw;-><init>(Lgjp;Lgjp;Ljava/util/regex/Pattern;Lezj;)V

    return-object v1

    .line 295
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Less;Lfoy;Ljava/lang/String;)Lcnh;
    .locals 10

    .prologue
    .line 190
    iget-object v0, p2, Lfoy;->r:Lfqy;

    invoke-virtual {v0}, Lfqy;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcnm;->f:Lert;

    .line 191
    invoke-virtual {v0}, Lert;->a()Lerr;

    move-result-object v7

    .line 192
    :goto_0
    new-instance v0, Lcox;

    iget-object v1, p0, Lcnm;->d:Levn;

    .line 194
    invoke-direct {p0, p1}, Lcnm;->a(Less;)Lcnw;

    move-result-object v2

    iget-object v3, p0, Lcnm;->e:Lcwq;

    .line 198
    invoke-virtual {v3}, Lcwq;->g()Lcwx;

    move-result-object v6

    iget-object v8, p0, Lcnm;->g:Lgoc;

    iget-object v3, p0, Lcnm;->h:Lcuz;

    .line 201
    invoke-virtual {v3}, Lcuz;->a()Lcuy;

    move-result-object v9

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v9}, Lcox;-><init>(Levn;Lcnw;Less;Lfoy;Ljava/lang/String;Lcwx;Lerr;Lgoc;Lcuy;)V

    .line 202
    invoke-virtual {v0}, Lcox;->m()V

    .line 203
    return-object v0

    .line 191
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public final a(Less;Lfoy;Ljava/lang/String;Lcnj;)Lcnh;
    .locals 17

    .prologue
    .line 241
    if-nez p4, :cond_0

    .line 242
    const/4 v1, 0x0

    .line 282
    :goto_0
    return-object v1

    .line 245
    :cond_0
    move-object/from16 v0, p4

    iget-object v1, v0, Lcnj;->g:Lcnl;

    sget-object v2, Lcnl;->a:Lcnl;

    if-ne v1, v2, :cond_2

    .line 246
    move-object/from16 v0, p2

    iget-object v1, v0, Lfoy;->r:Lfqy;

    invoke-virtual {v1}, Lfqy;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcnm;->f:Lert;

    .line 247
    invoke-virtual {v1}, Lert;->a()Lerr;

    move-result-object v14

    .line 248
    :goto_1
    new-instance v1, Lcox;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcnm;->d:Levn;

    .line 250
    invoke-direct/range {p0 .. p1}, Lcnm;->a(Less;)Lcnw;

    move-result-object v3

    move-object/from16 v0, p4

    iget v7, v0, Lcnj;->a:I

    move-object/from16 v0, p4

    iget-boolean v8, v0, Lcnj;->b:Z

    move-object/from16 v0, p4

    iget-boolean v9, v0, Lcnj;->c:Z

    move-object/from16 v0, p4

    iget-boolean v10, v0, Lcnj;->d:Z

    move-object/from16 v0, p4

    iget-object v11, v0, Lcnj;->e:Ljava/util/List;

    move-object/from16 v0, p4

    iget v12, v0, Lcnj;->f:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcnm;->e:Lcwq;

    .line 260
    invoke-virtual {v4}, Lcwq;->g()Lcwx;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcnm;->g:Lgoc;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcnm;->h:Lcuz;

    .line 263
    invoke-virtual {v4}, Lcuz;->a()Lcuy;

    move-result-object v16

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    invoke-direct/range {v1 .. v16}, Lcox;-><init>(Levn;Lcnw;Less;Lfoy;Ljava/lang/String;IZZZLjava/util/List;ILcwx;Lerr;Lgoc;Lcuy;)V

    .line 264
    invoke-virtual {v1}, Lcox;->m()V

    goto :goto_0

    .line 247
    :cond_1
    const/4 v14, 0x0

    goto :goto_1

    .line 266
    :cond_2
    move-object/from16 v0, p4

    iget-object v1, v0, Lcnj;->g:Lcnl;

    sget-object v2, Lcnl;->b:Lcnl;

    if-ne v1, v2, :cond_3

    .line 267
    new-instance v1, Lcov;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcnm;->d:Levn;

    const/4 v3, 0x0

    .line 269
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcnm;->a(Less;)Lcnw;

    move-result-object v3

    move-object/from16 v0, p4

    iget v6, v0, Lcnj;->a:I

    move-object/from16 v0, p4

    iget-boolean v7, v0, Lcnj;->c:Z

    move-object/from16 v0, p4

    iget v8, v0, Lcnj;->f:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcnm;->e:Lcwq;

    .line 275
    invoke-virtual {v4}, Lcwq;->g()Lcwx;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcnm;->g:Lgoc;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcnm;->h:Lcuz;

    .line 277
    invoke-virtual {v4}, Lcuz;->a()Lcuy;

    move-result-object v11

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v11}, Lcov;-><init>(Levn;Lcnw;Lfoy;Ljava/lang/String;IZILcwx;Lgoc;Lcuy;)V

    .line 278
    invoke-virtual {v1}, Lcov;->m()V

    goto/16 :goto_0

    .line 282
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public final a(Less;Ljava/lang/String;)Lcnh;
    .locals 4

    .prologue
    .line 210
    sget-object v0, Lfoy;->a:Lfoy;

    .line 211
    invoke-virtual {v0}, Lfoy;->b()Lfpc;

    move-result-object v0

    const-wide v2, 0x7fffffffffffffffL

    .line 212
    iput-wide v2, v0, Lfpc;->R:J

    .line 213
    invoke-virtual {v0}, Lfpc;->a()Lfoy;

    move-result-object v0

    .line 210
    invoke-virtual {p0, p1, v0, p2}, Lcnm;->a(Less;Lfoy;Ljava/lang/String;)Lcnh;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfoy;Ljava/lang/String;)Lcnh;
    .locals 8

    .prologue
    .line 221
    new-instance v0, Lcov;

    iget-object v1, p0, Lcnm;->d:Levn;

    const/4 v2, 0x0

    .line 223
    invoke-direct {p0, v2}, Lcnm;->a(Less;)Lcnw;

    move-result-object v2

    iget-object v3, p0, Lcnm;->e:Lcwq;

    .line 226
    invoke-virtual {v3}, Lcwq;->g()Lcwx;

    move-result-object v5

    iget-object v6, p0, Lcnm;->g:Lgoc;

    iget-object v3, p0, Lcnm;->h:Lcuz;

    .line 228
    invoke-virtual {v3}, Lcuz;->a()Lcuy;

    move-result-object v7

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v7}, Lcov;-><init>(Levn;Lcnw;Lfoy;Ljava/lang/String;Lcwx;Lgoc;Lcuy;)V

    .line 229
    invoke-virtual {v0}, Lcov;->m()V

    .line 230
    return-object v0
.end method
