.class final Lsz;
.super Lrq;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Z

.field private c:I

.field private d:I

.field private e:Lsw;

.field private f:I

.field private synthetic g:Lsv;


# direct methods
.method public constructor <init>(Lsv;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 304
    iput-object p1, p0, Lsz;->g:Lsv;

    invoke-direct {p0}, Lrq;-><init>()V

    .line 298
    const/4 v0, -0x1

    iput v0, p0, Lsz;->c:I

    .line 305
    iput-object p2, p0, Lsz;->a:Ljava/lang/String;

    .line 306
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lsz;->g:Lsv;

    invoke-static {v0, p0}, Lsv;->a(Lsv;Lsz;)V

    .line 335
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lsz;->e:Lsw;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lsz;->e:Lsw;

    iget v1, p0, Lsz;->f:I

    invoke-virtual {v0, v1, p1}, Lsw;->a(II)V

    .line 361
    :goto_0
    return-void

    .line 358
    :cond_0
    iput p1, p0, Lsz;->c:I

    .line 359
    const/4 v0, 0x0

    iput v0, p0, Lsz;->d:I

    goto :goto_0
.end method

.method public final a(Lsw;)V
    .locals 6

    .prologue
    .line 309
    iput-object p1, p0, Lsz;->e:Lsw;

    .line 310
    iget-object v0, p0, Lsz;->a:Ljava/lang/String;

    iget v3, p1, Lsw;->d:I

    add-int/lit8 v1, v3, 0x1

    iput v1, p1, Lsw;->d:I

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    const-string v1, "routeId"

    invoke-virtual {v5, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x3

    iget v2, p1, Lsw;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p1, Lsw;->c:I

    const/4 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    iput v3, p0, Lsz;->f:I

    .line 311
    iget-boolean v0, p0, Lsz;->b:Z

    if-eqz v0, :cond_1

    .line 312
    iget v0, p0, Lsz;->f:I

    invoke-virtual {p1, v0}, Lsw;->a(I)V

    .line 313
    iget v0, p0, Lsz;->c:I

    if-ltz v0, :cond_0

    .line 314
    iget v0, p0, Lsz;->f:I

    iget v1, p0, Lsz;->c:I

    invoke-virtual {p1, v0, v1}, Lsw;->a(II)V

    .line 315
    const/4 v0, -0x1

    iput v0, p0, Lsz;->c:I

    .line 317
    :cond_0
    iget v0, p0, Lsz;->d:I

    if-eqz v0, :cond_1

    .line 318
    iget v0, p0, Lsz;->f:I

    iget v1, p0, Lsz;->d:I

    invoke-virtual {p1, v0, v1}, Lsw;->b(II)V

    .line 319
    const/4 v0, 0x0

    iput v0, p0, Lsz;->d:I

    .line 322
    :cond_1
    return-void
.end method

.method public final a(Landroid/content/Intent;Lse;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 374
    iget-object v0, p0, Lsz;->e:Lsw;

    if-eqz v0, :cond_2

    .line 375
    iget-object v0, p0, Lsz;->e:Lsw;

    iget v3, p0, Lsz;->f:I

    iget v2, v0, Lsw;->c:I

    add-int/lit8 v1, v2, 0x1

    iput v1, v0, Lsw;->c:I

    const/16 v1, 0x9

    const/4 v5, 0x0

    move-object v4, p1

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, v0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    const/4 v0, 0x1

    .line 377
    :goto_0
    return v0

    :cond_1
    move v0, v6

    .line 375
    goto :goto_0

    :cond_2
    move v0, v6

    .line 377
    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsz;->b:Z

    .line 340
    iget-object v0, p0, Lsz;->e:Lsw;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lsz;->e:Lsw;

    iget v1, p0, Lsz;->f:I

    invoke-virtual {v0, v1}, Lsw;->a(I)V

    .line 343
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lsz;->e:Lsw;

    if-eqz v0, :cond_0

    .line 366
    iget-object v0, p0, Lsz;->e:Lsw;

    iget v1, p0, Lsz;->f:I

    invoke-virtual {v0, v1, p1}, Lsw;->b(II)V

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    iget v0, p0, Lsz;->d:I

    add-int/2addr v0, p1

    iput v0, p0, Lsz;->d:I

    goto :goto_0
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Lsz;->b:Z

    .line 348
    iget-object v0, p0, Lsz;->e:Lsw;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lsz;->e:Lsw;

    iget v3, p0, Lsz;->f:I

    const/4 v1, 0x6

    iget v2, v0, Lsw;->c:I

    add-int/lit8 v5, v2, 0x1

    iput v5, v0, Lsw;->c:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 351
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 325
    iget-object v0, p0, Lsz;->e:Lsw;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lsz;->e:Lsw;

    iget v3, p0, Lsz;->f:I

    const/4 v1, 0x4

    iget v2, v0, Lsw;->c:I

    add-int/lit8 v5, v2, 0x1

    iput v5, v0, Lsw;->c:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 327
    iput-object v4, p0, Lsz;->e:Lsw;

    .line 328
    const/4 v0, 0x0

    iput v0, p0, Lsz;->f:I

    .line 330
    :cond_0
    return-void
.end method
