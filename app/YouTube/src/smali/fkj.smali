.class public Lfkj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhky;

.field public final b:Lfqh;

.field public c:Ljava/util/List;

.field private d:Ljava/util/List;

.field private e:Lfkk;


# direct methods
.method public constructor <init>(Lhky;Lfqh;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhky;

    iput-object v0, p0, Lfkj;->a:Lhky;

    .line 32
    iput-object p2, p0, Lfkj;->b:Lfqh;

    .line 33
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 36
    iget-object v0, p0, Lfkj;->d:Ljava/util/List;

    if-nez v0, :cond_20

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfkj;->a:Lhky;

    iget-object v1, v1, Lhky;->a:[Lhla;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfkj;->d:Ljava/util/List;

    .line 38
    iget-object v0, p0, Lfkj;->a:Lhky;

    iget-object v1, v0, Lhky;->a:[Lhla;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_20

    aget-object v3, v1, v0

    .line 39
    iget-object v4, v3, Lhla;->f:Lhds;

    if-eqz v4, :cond_1

    .line 40
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfja;

    iget-object v3, v3, Lhla;->f:Lhds;

    invoke-direct {v5, v3, p0}, Lfja;-><init>(Lhds;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_1
    iget-object v4, v3, Lhla;->d:Lhdv;

    if-eqz v4, :cond_2

    .line 42
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfjc;

    iget-object v3, v3, Lhla;->d:Lhdv;

    invoke-direct {v5, v3, p0}, Lfjc;-><init>(Lhdv;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 43
    :cond_2
    iget-object v4, v3, Lhla;->c:Lheb;

    if-eqz v4, :cond_3

    .line 44
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfjf;

    iget-object v3, v3, Lhla;->c:Lheb;

    invoke-direct {v5, v3, p0}, Lfjf;-><init>(Lheb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_3
    iget-object v4, v3, Lhla;->H:Lhdx;

    if-eqz v4, :cond_4

    .line 46
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfjd;

    iget-object v3, v3, Lhla;->H:Lhdx;

    invoke-direct {v5, v3, p0}, Lfjd;-><init>(Lhdx;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 47
    :cond_4
    iget-object v4, v3, Lhla;->p:Lhdt;

    if-eqz v4, :cond_5

    .line 48
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfjb;

    iget-object v3, v3, Lhla;->p:Lhdt;

    invoke-direct {v5, v3, p0}, Lfjb;-><init>(Lhdt;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 49
    :cond_5
    iget-object v4, v3, Lhla;->b:Lhas;

    if-eqz v4, :cond_6

    .line 50
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfio;

    iget-object v3, v3, Lhla;->b:Lhas;

    invoke-direct {v5, v3, p0}, Lfio;-><init>(Lhas;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 51
    :cond_6
    iget-object v4, v3, Lhla;->x:Liav;

    if-eqz v4, :cond_7

    .line 52
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfnr;

    iget-object v3, v3, Lhla;->x:Liav;

    invoke-direct {v5, v3, p0}, Lfnr;-><init>(Liav;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 53
    :cond_7
    iget-object v4, v3, Lhla;->w:Libb;

    if-eqz v4, :cond_8

    .line 54
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfno;

    iget-object v3, v3, Lhla;->w:Libb;

    invoke-direct {v5, v3, p0}, Lfno;-><init>(Libb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 55
    :cond_8
    iget-object v4, v3, Lhla;->i:Lhnv;

    if-eqz v4, :cond_9

    .line 56
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfku;

    iget-object v3, v3, Lhla;->i:Lhnv;

    invoke-direct {v5, v3, p0}, Lfku;-><init>(Lhnv;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 57
    :cond_9
    iget-object v4, v3, Lhla;->k:Lhdz;

    if-eqz v4, :cond_a

    .line 58
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfje;

    iget-object v3, v3, Lhla;->k:Lhdz;

    invoke-direct {v5, v3, p0}, Lfje;-><init>(Lhdz;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 59
    :cond_a
    iget-object v4, v3, Lhla;->g:Lhzu;

    if-eqz v4, :cond_b

    .line 60
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfnl;

    iget-object v3, v3, Lhla;->g:Lhzu;

    invoke-direct {v5, v3, p0}, Lfnl;-><init>(Lhzu;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 61
    :cond_b
    iget-object v4, v3, Lhla;->t:Lhtd;

    if-eqz v4, :cond_c

    .line 62
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfmd;

    iget-object v3, v3, Lhla;->t:Lhtd;

    invoke-direct {v5, v3, p0}, Lfmd;-><init>(Lhtd;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 63
    :cond_c
    iget-object v4, v3, Lhla;->D:Lgym;

    if-eqz v4, :cond_d

    .line 64
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfih;

    iget-object v3, v3, Lhla;->D:Lgym;

    invoke-direct {v5, v3}, Lfih;-><init>(Lgym;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 65
    :cond_d
    iget-object v4, v3, Lhla;->e:Lhgn;

    if-eqz v4, :cond_e

    .line 66
    iget-object v3, v3, Lhla;->e:Lhgn;

    invoke-static {v3, p0}, La;->a(Lhgn;Lfqh;)Lfju;

    move-result-object v3

    .line 69
    if-eqz v3, :cond_0

    .line 70
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 72
    :cond_e
    iget-object v4, v3, Lhla;->q:Lhni;

    if-eqz v4, :cond_f

    .line 73
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfkr;

    iget-object v3, v3, Lhla;->q:Lhni;

    invoke-direct {v5, v3, p0}, Lfkr;-><init>(Lhni;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 74
    :cond_f
    iget-object v4, v3, Lhla;->r:Lhoa;

    if-eqz v4, :cond_10

    .line 75
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfky;

    iget-object v3, v3, Lhla;->r:Lhoa;

    invoke-direct {v5, v3, p0}, Lfky;-><init>(Lhoa;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 76
    :cond_10
    iget-object v4, v3, Lhla;->v:Lhny;

    if-eqz v4, :cond_11

    .line 77
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfkw;

    iget-object v3, v3, Lhla;->v:Lhny;

    invoke-direct {v5, v3, p0}, Lfkw;-><init>(Lhny;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 78
    :cond_11
    iget-object v4, v3, Lhla;->u:Lhnz;

    if-eqz v4, :cond_12

    .line 79
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfkx;

    iget-object v3, v3, Lhla;->u:Lhnz;

    invoke-direct {v5, v3, p0}, Lfkx;-><init>(Lhnz;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 80
    :cond_12
    iget-object v4, v3, Lhla;->s:Lhob;

    if-eqz v4, :cond_13

    .line 81
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfkz;

    iget-object v3, v3, Lhla;->s:Lhob;

    invoke-direct {v5, v3, p0}, Lfkz;-><init>(Lhob;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 83
    :cond_13
    iget-object v4, v3, Lhla;->h:Lhzs;

    if-eqz v4, :cond_14

    .line 84
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfnk;

    iget-object v3, v3, Lhla;->h:Lhzs;

    invoke-direct {v5, v3, p0}, Lfnk;-><init>(Lhzs;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 85
    :cond_14
    iget-object v4, v3, Lhla;->l:Lhzm;

    if-eqz v4, :cond_15

    .line 86
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfnj;

    iget-object v3, v3, Lhla;->l:Lhzm;

    invoke-direct {v5, v3, p0}, Lfnj;-><init>(Lhzm;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 87
    :cond_15
    iget-object v4, v3, Lhla;->n:Lhjb;

    if-eqz v4, :cond_16

    .line 88
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfke;

    iget-object v3, v3, Lhla;->n:Lhjb;

    invoke-direct {v5, v3, p0}, Lfke;-><init>(Lhjb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 89
    :cond_16
    iget-object v4, v3, Lhla;->m:Lhvs;

    if-eqz v4, :cond_17

    .line 90
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfms;

    iget-object v3, v3, Lhla;->m:Lhvs;

    invoke-direct {v5, v3, p0}, Lfms;-><init>(Lhvs;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 91
    :cond_17
    iget-object v4, v3, Lhla;->o:Lhfw;

    if-eqz v4, :cond_18

    .line 92
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfjs;

    iget-object v3, v3, Lhla;->o:Lhfw;

    invoke-direct {v5, v3, p0}, Lfjs;-><init>(Lhfw;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 93
    :cond_18
    iget-object v4, v3, Lhla;->F:Lhhb;

    if-eqz v4, :cond_19

    .line 94
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfjx;

    iget-object v3, v3, Lhla;->F:Lhhb;

    invoke-direct {v5, v3, p0}, Lfjx;-><init>(Lhhb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 95
    :cond_19
    iget-object v4, v3, Lhla;->B:Lhos;

    if-eqz v4, :cond_1a

    .line 96
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfld;

    iget-object v3, v3, Lhla;->B:Lhos;

    invoke-direct {v5, v3, p0}, Lfld;-><init>(Lhos;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 97
    :cond_1a
    iget-object v4, v3, Lhla;->A:Lhdu;

    if-eqz v4, :cond_1b

    .line 98
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfjg;

    iget-object v3, v3, Lhla;->A:Lhdu;

    invoke-direct {v5, v3, p0}, Lfjg;-><init>(Lhdu;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 99
    :cond_1b
    iget-object v4, v3, Lhla;->E:Lhti;

    if-eqz v4, :cond_1c

    .line 100
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfmf;

    iget-object v3, v3, Lhla;->E:Lhti;

    invoke-direct {v5, v3, p0}, Lfmf;-><init>(Lhti;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 101
    :cond_1c
    iget-object v4, v3, Lhla;->y:Lhcg;

    if-eqz v4, :cond_1d

    .line 102
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfix;

    iget-object v3, v3, Lhla;->y:Lhcg;

    invoke-direct {v5, v3}, Lfix;-><init>(Lhcg;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 103
    :cond_1d
    iget-object v4, v3, Lhla;->j:Lhcb;

    if-eqz v4, :cond_1e

    .line 104
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfiv;

    iget-object v3, v3, Lhla;->j:Lhcb;

    invoke-direct {v5, v3}, Lfiv;-><init>(Lhcb;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 105
    :cond_1e
    iget-object v4, v3, Lhla;->G:Licj;

    if-eqz v4, :cond_1f

    .line 106
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfoh;

    iget-object v3, v3, Lhla;->G:Licj;

    invoke-direct {v5, v3, p0}, Lfoh;-><init>(Licj;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 107
    :cond_1f
    iget-object v4, v3, Lhla;->C:Lhbb;

    if-eqz v4, :cond_0

    .line 108
    iget-object v4, p0, Lfkj;->d:Ljava/util/List;

    new-instance v5, Lfip;

    iget-object v3, v3, Lhla;->C:Lhbb;

    invoke-direct {v5, v3, p0}, Lfip;-><init>(Lhbb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 113
    :cond_20
    iget-object v0, p0, Lfkj;->d:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 3

    .prologue
    .line 149
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 150
    invoke-virtual {p0}, Lfkj;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 151
    instance-of v2, v0, Lfat;

    if-eqz v2, :cond_0

    .line 152
    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    goto :goto_0

    .line 155
    :cond_1
    return-void
.end method

.method public final b()Lfkk;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lfkj;->e:Lfkk;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfkj;->a:Lhky;

    iget-object v0, v0, Lhky;->d:Lhku;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfkj;->a:Lhky;

    iget-object v0, v0, Lhky;->d:Lhku;

    iget-object v0, v0, Lhku;->a:Lhkt;

    if-eqz v0, :cond_0

    .line 132
    new-instance v0, Lfkk;

    iget-object v1, p0, Lfkj;->a:Lhky;

    iget-object v1, v1, Lhky;->d:Lhku;

    iget-object v1, v1, Lhku;->a:Lhkt;

    invoke-direct {v0, v1}, Lfkk;-><init>(Lhkt;)V

    iput-object v0, p0, Lfkj;->e:Lfkk;

    .line 134
    :cond_0
    iget-object v0, p0, Lfkj;->e:Lfkk;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lfkj;->a:Lhky;

    iget-object v0, v0, Lhky;->c:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lfkj;->b:Lfqh;

    return-object v0
.end method
