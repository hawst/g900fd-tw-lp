.class public final Lhcp;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22529
    invoke-direct {p0}, Lidf;-><init>()V

    .line 22532
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhcp;->a:[B

    .line 22529
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 22574
    const/4 v0, 0x0

    .line 22575
    iget-object v1, p0, Lhcp;->a:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 22576
    const/4 v0, 0x2

    iget-object v1, p0, Lhcp;->a:[B

    .line 22577
    invoke-static {v0, v1}, Lidd;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 22579
    :cond_0
    iget-object v1, p0, Lhcp;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22580
    iput v0, p0, Lhcp;->J:I

    .line 22581
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 22525
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhcp;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhcp;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhcp;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhcp;->a:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 22565
    iget-object v0, p0, Lhcp;->a:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 22566
    const/4 v0, 0x2

    iget-object v1, p0, Lhcp;->a:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 22568
    :cond_0
    iget-object v0, p0, Lhcp;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 22570
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22543
    if-ne p1, p0, :cond_1

    .line 22547
    :cond_0
    :goto_0
    return v0

    .line 22544
    :cond_1
    instance-of v2, p1, Lhcp;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 22545
    :cond_2
    check-cast p1, Lhcp;

    .line 22546
    iget-object v2, p0, Lhcp;->a:[B

    iget-object v3, p1, Lhcp;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcp;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcp;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 22547
    goto :goto_0

    .line 22546
    :cond_4
    iget-object v2, p0, Lhcp;->I:Ljava/util/List;

    iget-object v3, p1, Lhcp;->I:Ljava/util/List;

    .line 22547
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 22551
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 22553
    iget-object v2, p0, Lhcp;->a:[B

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 22559
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhcp;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_0
    add-int/2addr v0, v1

    .line 22560
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 22555
    :goto_1
    iget-object v3, p0, Lhcp;->a:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 22556
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhcp;->a:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 22555
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 22559
    :cond_2
    iget-object v1, p0, Lhcp;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
