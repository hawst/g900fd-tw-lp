.class final Lawx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lawm;


# instance fields
.field private final a:Lawm;

.field private final b:Lawm;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ProgressBar;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private final i:Landroid/widget/TextView;

.field private final j:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Laww;Landroid/view/View;Lawm;Lawm;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p3, p0, Lawx;->a:Lawm;

    .line 59
    iput-object p4, p0, Lawx;->b:Lawm;

    .line 60
    const v0, 0x7f0800c2

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lawx;->c:Landroid/view/View;

    .line 61
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lawx;->d:Landroid/widget/ImageView;

    .line 62
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f08031e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lawx;->e:Landroid/widget/ImageView;

    .line 63
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f0800de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lawx;->f:Landroid/widget/ProgressBar;

    .line 64
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f080276

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawx;->g:Landroid/widget/TextView;

    .line 65
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f08031f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawx;->h:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f08012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawx;->i:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f080132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lawx;->j:Landroid/widget/TextView;

    .line 68
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lawx;->e:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 93
    iget-object v0, p0, Lawx;->f:Landroid/widget/ProgressBar;

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 94
    iget-object v0, p0, Lawx;->h:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 95
    iget-object v0, p0, Lawx;->g:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 96
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 0

    .prologue
    .line 106
    if-eqz p0, :cond_0

    .line 107
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 109
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lawx;->d:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 100
    iget-object v0, p0, Lawx;->i:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 101
    iget-object v0, p0, Lawx;->j:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 102
    iget-object v0, p0, Lawx;->c:Landroid/view/View;

    const v1, 0x7f080139

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Lawx;->a(Landroid/view/View;I)V

    .line 103
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 43
    check-cast p2, Lbxz;

    invoke-virtual {p2}, Lbxz;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lawx;->a(I)V

    invoke-direct {p0, v1}, Lawx;->b(I)V

    iget-object v0, p0, Lawx;->a:Lawm;

    iget-object v1, p2, Lbxz;->a:Lgcd;

    invoke-interface {v0, p1, v1}, Lawm;->a(ILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v1}, Lawx;->a(I)V

    invoke-direct {p0, v2}, Lawx;->b(I)V

    iget-object v0, p0, Lawx;->b:Lawm;

    iget-object v1, p2, Lbxz;->b:Lgjm;

    invoke-interface {v0, p1, v1}, Lawm;->a(ILjava/lang/Object;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
