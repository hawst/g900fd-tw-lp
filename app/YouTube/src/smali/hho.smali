.class public final Lhho;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhpn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62225
    invoke-direct {p0}, Lidf;-><init>()V

    .line 62228
    const/4 v0, 0x0

    iput-object v0, p0, Lhho;->a:Lhpn;

    .line 62225
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 62265
    const/4 v0, 0x0

    .line 62266
    iget-object v1, p0, Lhho;->a:Lhpn;

    if-eqz v1, :cond_0

    .line 62267
    const v0, 0x39c4528

    iget-object v1, p0, Lhho;->a:Lhpn;

    .line 62268
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 62270
    :cond_0
    iget-object v1, p0, Lhho;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 62271
    iput v0, p0, Lhho;->J:I

    .line 62272
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 62221
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhho;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhho;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhho;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhho;->a:Lhpn;

    if-nez v0, :cond_2

    new-instance v0, Lhpn;

    invoke-direct {v0}, Lhpn;-><init>()V

    iput-object v0, p0, Lhho;->a:Lhpn;

    :cond_2
    iget-object v0, p0, Lhho;->a:Lhpn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1ce22942 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 62256
    iget-object v0, p0, Lhho;->a:Lhpn;

    if-eqz v0, :cond_0

    .line 62257
    const v0, 0x39c4528

    iget-object v1, p0, Lhho;->a:Lhpn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 62259
    :cond_0
    iget-object v0, p0, Lhho;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 62261
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 62239
    if-ne p1, p0, :cond_1

    .line 62243
    :cond_0
    :goto_0
    return v0

    .line 62240
    :cond_1
    instance-of v2, p1, Lhho;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 62241
    :cond_2
    check-cast p1, Lhho;

    .line 62242
    iget-object v2, p0, Lhho;->a:Lhpn;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhho;->a:Lhpn;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhho;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhho;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 62243
    goto :goto_0

    .line 62242
    :cond_4
    iget-object v2, p0, Lhho;->a:Lhpn;

    iget-object v3, p1, Lhho;->a:Lhpn;

    invoke-virtual {v2, v3}, Lhpn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhho;->I:Ljava/util/List;

    iget-object v3, p1, Lhho;->I:Ljava/util/List;

    .line 62243
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 62247
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 62249
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhho;->a:Lhpn;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 62250
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhho;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 62251
    return v0

    .line 62249
    :cond_0
    iget-object v0, p0, Lhho;->a:Lhpn;

    invoke-virtual {v0}, Lhpn;->hashCode()I

    move-result v0

    goto :goto_0

    .line 62250
    :cond_1
    iget-object v1, p0, Lhho;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
