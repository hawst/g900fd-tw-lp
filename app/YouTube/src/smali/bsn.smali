.class final Lbsn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final a:Lbjy;

.field b:Z

.field private final c:Lflj;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private h:Landroid/graphics/drawable/Drawable;

.field private synthetic i:Lbrz;


# direct methods
.method constructor <init>(Lbrz;Lflj;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 487
    iput-object p1, p0, Lbsn;->i:Lbrz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflj;

    iput-object v0, p0, Lbsn;->c:Lflj;

    .line 489
    invoke-static {p2}, Lbjy;->a(Lflj;)Lbjy;

    move-result-object v0

    iput-object v0, p0, Lbsn;->a:Lbjy;

    .line 490
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbsn;->d:Landroid/view/View;

    .line 491
    const v0, 0x7f080242

    .line 492
    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbsn;->e:Landroid/view/View;

    .line 493
    iget-object v0, p0, Lbsn;->e:Landroid/view/View;

    const v1, 0x7f080243

    .line 494
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbsn;->f:Landroid/widget/TextView;

    .line 495
    iget-object v0, p0, Lbsn;->e:Landroid/view/View;

    const v1, 0x7f080244

    .line 496
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbsn;->g:Landroid/widget/TextView;

    .line 497
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbsn;->a(Z)V

    .line 498
    invoke-virtual {p3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 499
    return-void
.end method

.method static synthetic a(Lbsn;)Lbjy;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lbsn;->a:Lbjy;

    return-object v0
.end method

.method static synthetic a(Lbsn;Lfli;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 473
    iget-object v0, p0, Lbsn;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbsn;->f:Landroid/widget/TextView;

    iget-object v2, p1, Lfli;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p1, Lfli;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lbsn;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lbsn;->g:Landroid/widget/TextView;

    iget-object v1, p1, Lfli;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbsn;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic a(Lbsn;Z)V
    .locals 0

    .prologue
    .line 473
    invoke-direct {p0, p1}, Lbsn;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 523
    if-eqz p1, :cond_1

    .line 524
    iget-object v0, p0, Lbsn;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 525
    iget-object v0, p0, Lbsn;->d:Landroid/view/View;

    .line 526
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0200e0

    invoke-static {v0, v1}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbsn;->h:Landroid/graphics/drawable/Drawable;

    .line 528
    :cond_0
    iget-object v0, p0, Lbsn;->e:Landroid/view/View;

    iget-object v1, p0, Lbsn;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 532
    :goto_0
    iput-boolean p1, p0, Lbsn;->b:Z

    .line 533
    return-void

    .line 530
    :cond_1
    iget-object v0, p0, Lbsn;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic b(Lbsn;)V
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lbsn;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic c(Lbsn;)Lflj;
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lbsn;->c:Lflj;

    return-object v0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 541
    iget-object v0, p0, Lbsn;->i:Lbrz;

    iget-object v1, p0, Lbsn;->a:Lbjy;

    invoke-virtual {v0, v1}, Lbrz;->a(Lbjy;)V

    .line 542
    return-void
.end method
