.class public Lfpy;
.super Lfpz;
.source "SourceFile"

# interfaces
.implements Lfqa;


# instance fields
.field public final a:Ljava/util/List;

.field public b:Z

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:[B

.field private final m:[B


# direct methods
.method public constructor <init>(Lhhr;Lfqh;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p2}, Lfpz;-><init>(Lfqh;)V

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p1, Lhhr;->a:Lhhs;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p1, Lhhr;->c:Lhhs;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lfpy;->d:Lfnc;

    .line 47
    iget-object v0, p1, Lhhr;->a:Lhhs;

    iget-object v0, v0, Lhhs;->b:Lhhu;

    iget-object v0, v0, Lhhu;->b:Ljava/lang/String;

    iput-object v0, p0, Lfpy;->k:Ljava/lang/String;

    .line 48
    iget-object v0, p1, Lhhr;->c:Lhhs;

    iget-object v0, v0, Lhhs;->b:Lhhu;

    iget-object v0, v0, Lhhu;->b:Ljava/lang/String;

    iput-object v0, p0, Lfpy;->j:Ljava/lang/String;

    .line 49
    iget-object v0, p1, Lhhr;->a:Lhhs;

    iget-object v0, v0, Lhhs;->b:Lhhu;

    iget-object v0, v0, Lhhu;->g:[B

    iput-object v0, p0, Lfpy;->m:[B

    .line 50
    iget-object v0, p1, Lhhr;->c:Lhhs;

    iget-object v0, v0, Lhhs;->b:Lhhu;

    iget-object v0, v0, Lhhu;->g:[B

    iput-object v0, p0, Lfpy;->l:[B

    .line 52
    iget-object v0, p1, Lhhr;->b:[Lhhs;

    invoke-static {p0, v0, p0}, La;->a(Lfqh;[Lhhs;Lfqa;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfpy;->a:Ljava/util/List;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 2

    .prologue
    .line 80
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 81
    iget-object v0, p0, Lfpy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpz;

    .line 82
    invoke-virtual {v0, p1}, Lfpz;->a(Lfau;)V

    goto :goto_0

    .line 84
    :cond_0
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lfpy;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpy;->j:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfpy;->k:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lfpy;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfpy;->l:[B

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfpy;->m:[B

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lfpy;->b:Z

    return v0
.end method
