.class public final Lhfq;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46630
    invoke-direct {p0}, Lidf;-><init>()V

    .line 46633
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhfq;->a:J

    .line 46630
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    .line 46670
    const/4 v0, 0x0

    .line 46671
    iget-wide v2, p0, Lhfq;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 46672
    const/4 v0, 0x1

    iget-wide v2, p0, Lhfq;->a:J

    .line 46673
    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 46675
    :cond_0
    iget-object v1, p0, Lhfq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 46676
    iput v0, p0, Lhfq;->J:I

    .line 46677
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 46626
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfq;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfq;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfq;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhfq;->a:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 4

    .prologue
    .line 46661
    iget-wide v0, p0, Lhfq;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 46662
    const/4 v0, 0x1

    iget-wide v2, p0, Lhfq;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 46664
    :cond_0
    iget-object v0, p0, Lhfq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 46666
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 46644
    if-ne p1, p0, :cond_1

    .line 46648
    :cond_0
    :goto_0
    return v0

    .line 46645
    :cond_1
    instance-of v2, p1, Lhfq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 46646
    :cond_2
    check-cast p1, Lhfq;

    .line 46647
    iget-wide v2, p0, Lhfq;->a:J

    iget-wide v4, p1, Lhfq;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhfq;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 46648
    goto :goto_0

    .line 46647
    :cond_4
    iget-object v2, p0, Lhfq;->I:Ljava/util/List;

    iget-object v3, p1, Lhfq;->I:Ljava/util/List;

    .line 46648
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 46652
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 46654
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhfq;->a:J

    iget-wide v4, p0, Lhfq;->a:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 46655
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhfq;->I:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 46656
    return v0

    .line 46655
    :cond_0
    iget-object v0, p0, Lhfq;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0
.end method
