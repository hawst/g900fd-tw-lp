.class public final Lbqv;
.super Lld;
.source "SourceFile"


# instance fields
.field public final c:Landroid/support/v4/widget/DrawerLayout;

.field public final d:Landroid/view/View;

.field public e:Z

.field public f:I

.field private final g:Lkp;

.field private final h:Lbqw;

.field private final i:I


# direct methods
.method public constructor <init>(Lkp;Lbqw;Landroid/support/v4/widget/DrawerLayout;II)V
    .locals 5

    .prologue
    const v4, 0x800003

    .line 52
    invoke-direct {p0, p1, p3, p4, p5}, Lld;-><init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;II)V

    .line 57
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkp;

    iput-object v0, p0, Lbqv;->g:Lkp;

    .line 58
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqw;

    iput-object v0, p0, Lbqv;->h:Lbqw;

    .line 59
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    .line 60
    iget-object v0, p0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, p0}, Landroid/support/v4/widget/DrawerLayout;->a(Ljm;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbqv;->e:Z

    .line 62
    iput v4, p0, Lbqv;->i:I

    .line 63
    iget-object v0, p0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/DrawerLayout;->getChildCount()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ljn;

    iget v0, v0, Ljn;->a:I

    if-ne v0, v4, :cond_0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbqv;->d:Landroid/view/View;

    .line 64
    return-void

    .line 63
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0, p1}, Lld;->a(I)V

    .line 143
    iput p1, p0, Lbqv;->f:I

    .line 144
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0, p1}, Lld;->a(Landroid/view/View;)V

    .line 157
    iget-object v0, p0, Lbqv;->h:Lbqw;

    invoke-interface {v0}, Lbqw;->w()V

    .line 158
    return-void
.end method

.method public final a(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 148
    invoke-super {p0, p1, p2}, Lld;->a(Landroid/view/View;F)V

    .line 149
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 150
    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    .line 151
    iget-object v1, p0, Lbqv;->h:Lbqw;

    invoke-interface {v1, v0}, Lbqw;->b(I)V

    .line 152
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0, p1}, Lld;->b(Landroid/view/View;)V

    .line 163
    iget-object v0, p0, Lbqv;->h:Lbqw;

    invoke-interface {v0}, Lbqw;->x()V

    .line 164
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 101
    iget-boolean v0, p0, Lbqv;->e:Z

    if-ne v0, p1, :cond_0

    .line 110
    :goto_0
    return-void

    .line 104
    :cond_0
    iput-boolean p1, p0, Lbqv;->e:Z

    .line 107
    iget-object v1, p0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    iget-boolean v0, p0, Lbqv;->e:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    const v2, 0x800003

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/widget/DrawerLayout;->a(II)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lbqv;->c:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->d(I)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 178
    invoke-virtual {p0, v2}, Lbqv;->a(Z)V

    .line 179
    iget-object v0, p0, Lbqv;->g:Lkp;

    invoke-virtual {v0}, Lkp;->d()Lkm;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_0

    .line 182
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lkm;->c(Z)V

    .line 183
    invoke-virtual {v0, v2}, Lkm;->c(Z)V

    .line 185
    :cond_0
    return-void
.end method
