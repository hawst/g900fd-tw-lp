.class public Lcby;
.super Lfsb;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Leyp;

.field final c:Landroid/view/View;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/view/View;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/TextView;

.field private final i:Lfvi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Landroid/view/View;Lfhz;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0, p4, p5, p6}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 71
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcby;->a:Landroid/content/Context;

    .line 72
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcby;->b:Leyp;

    .line 74
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcby;->c:Landroid/view/View;

    .line 75
    const v0, 0x7f08008b

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcby;->f:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f08012f

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcby;->g:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f080131

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcby;->h:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f080132

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcby;->d:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0800a4

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 80
    new-instance v1, Lfvi;

    invoke-direct {v1, p2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcby;->i:Lfvi;

    .line 81
    const v0, 0x7f080130

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcby;->e:Landroid/view/View;

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;ILfdw;Lfrz;)V
    .locals 7

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 52
    invoke-static {p1, p5, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p6

    move-object v6, p7

    .line 49
    invoke-direct/range {v0 .. v6}, Lcby;-><init>(Landroid/content/Context;Leyp;Landroid/view/View;Lfhz;Lfdw;Lfrz;)V

    .line 56
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-interface {p4, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v0, p0, Lcby;->c:Landroid/view/View;

    invoke-interface {p4, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 60
    const/4 v0, 0x1

    invoke-interface {p4, v0}, Lfsj;->a(Z)V

    .line 61
    return-void
.end method


# virtual methods
.method protected final a(Lfnc;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 115
    if-nez p2, :cond_0

    .line 116
    iget-object v0, p0, Lcby;->g:Landroid/widget/TextView;

    const v1, 0x7f0902e5

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 121
    :goto_0
    iget-object v0, p0, Lcby;->i:Lfvi;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lfvi;->a(Lfnc;Leyo;)V

    .line 122
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lcby;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcby;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method

.method protected final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcby;->h:Landroid/widget/TextView;

    invoke-static {v0, p1}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcby;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    return-void
.end method
