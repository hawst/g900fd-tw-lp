.class public final Lgfl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lggr;

.field private final b:Lws;

.field private final c:[Lgkl;


# direct methods
.method public varargs constructor <init>(Lggr;Lws;[Lgkl;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggr;

    iput-object v0, p0, Lgfl;->a:Lggr;

    .line 53
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lgfl;->b:Lws;

    .line 54
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgkl;

    iput-object v0, p0, Lgfl;->c:[Lgkl;

    .line 55
    return-void
.end method

.method static synthetic a(Ljava/io/InputStream;)[B
    .locals 4

    .prologue
    .line 42
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x400

    new-array v1, v1, [B

    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;[BLjava/lang/String;)Lgfp;
    .locals 4

    .prologue
    .line 62
    invoke-static {}, Lb;->b()V

    .line 64
    invoke-static {p1}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lgfl;->a:Lggr;

    invoke-virtual {v1, v0}, Lggr;->a(Lfao;)Lfao;

    .line 66
    const-string v1, "cpn"

    invoke-virtual {v0, v1, p3}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 67
    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 69
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v1

    .line 70
    new-instance v2, Lgfo;

    .line 71
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lgfl;->c:[Lgkl;

    invoke-direct {v2, v0, p2, v3, v1}, Lgfo;-><init>(Ljava/lang/String;[B[Lgkl;Lwv;)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, v2, Lwp;->e:Z

    .line 73
    new-instance v0, Lgfm;

    invoke-direct {v0}, Lgfm;-><init>()V

    iput-object v0, v2, Lwp;->h:Lwx;

    .line 74
    iget-object v0, p0, Lgfl;->b:Lws;

    invoke-virtual {v0, v2}, Lws;->a(Lwp;)Lwp;

    .line 76
    :try_start_0
    invoke-virtual {v1}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfp;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    new-instance v1, Lgfq;

    invoke-direct {v1, v0}, Lgfq;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 79
    :catch_1
    move-exception v0

    .line 81
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Lgfq;

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lgfq;

    throw v0

    .line 84
    :cond_0
    new-instance v1, Lgfq;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Lgfq;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Landroid/net/Uri;)[B
    .locals 4

    .prologue
    .line 95
    invoke-static {}, Lb;->b()V

    .line 97
    invoke-static {p1}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lgfl;->a:Lggr;

    invoke-virtual {v1, v0}, Lggr;->a(Lfao;)Lfao;

    .line 99
    iget-object v0, v0, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 101
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v1

    .line 102
    iget-object v2, p0, Lgfl;->b:Lws;

    new-instance v3, Lgfn;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0, v1}, Lgfn;-><init>(Ljava/lang/String;Lwv;)V

    invoke-virtual {v2, v3}, Lws;->a(Lwp;)Lwp;

    .line 104
    :try_start_0
    invoke-virtual {v1}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    new-instance v1, Lgfq;

    invoke-direct {v1, v0}, Lgfq;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 107
    :catch_1
    move-exception v0

    .line 109
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Lgfq;

    if-eqz v1, :cond_0

    .line 110
    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Lgfq;

    throw v0

    .line 112
    :cond_0
    new-instance v1, Lgfq;

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    invoke-direct {v1, v0}, Lgfq;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
