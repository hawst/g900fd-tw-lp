.class public final Ldqu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldpo;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private final e:Ldyd;

.field private final f:Ldyg;

.field private final g:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ldyd;Ldyg;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Ldqu;->d:Landroid/content/Context;

    .line 44
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldqu;->c:Ljava/lang/String;

    .line 45
    const/16 v0, 0x50

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Ldqu;->b:I

    .line 46
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldqu;->a:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Ldqu;->e:Ldyd;

    .line 48
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Ldqu;->g:Ljava/util/Map;

    .line 49
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldqu;->f:Ldyg;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Ldpt;)Ldqq;
    .locals 13

    .prologue
    .line 54
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 55
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 57
    invoke-virtual {p1}, Ldpt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v0, "method"

    iget-object v1, p1, Ldpt;->a:Ldtc;

    invoke-virtual {v1}, Ldtc;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-virtual {p1}, Ldpt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "params"

    .line 61
    iget-object v1, p1, Ldpt;->b:Ldte;

    invoke-static {v1}, Ldqp;->a(Ldte;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    :cond_0
    iget-boolean v0, p1, Ldpt;->d:Z

    if-eqz v0, :cond_1

    .line 66
    const-string v0, "ui"

    const-string v1, ""

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    :cond_1
    invoke-virtual {p1}, Ldpt;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    const-string v0, "X-YouTube-LoungeId-Token"

    .line 71
    iget-object v1, p1, Ldpt;->c:Ldtb;

    invoke-virtual {v1}, Ldtb;->toString()Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    :cond_2
    invoke-static {}, Ldzh;->a()Ldza;

    move-result-object v1

    .line 75
    new-instance v0, Ldpv;

    iget-object v2, p0, Ldqu;->c:Ljava/lang/String;

    iget-object v3, p0, Ldqu;->a:Ljava/lang/String;

    iget-object v4, p0, Ldqu;->e:Ldyd;

    iget-object v6, p0, Ldqu;->g:Ljava/util/Map;

    new-instance v8, Ldpx;

    iget-object v9, p0, Ldqu;->d:Landroid/content/Context;

    iget-object v10, p0, Ldqu;->c:Ljava/lang/String;

    iget v11, p0, Ldqu;->b:I

    invoke-direct {v8, v9, v1, v10, v11}, Ldpx;-><init>(Landroid/content/Context;Ldza;Ljava/lang/String;I)V

    new-instance v9, Ldpx;

    iget-object v10, p0, Ldqu;->d:Landroid/content/Context;

    iget-object v11, p0, Ldqu;->c:Ljava/lang/String;

    iget v12, p0, Ldqu;->b:I

    invoke-direct {v9, v10, v1, v11, v12}, Ldpx;-><init>(Landroid/content/Context;Ldza;Ljava/lang/String;I)V

    iget-object v10, p0, Ldqu;->f:Ldyg;

    invoke-direct/range {v0 .. v10}, Ldpv;-><init>(Ldza;Ljava/lang/String;Ljava/lang/String;Ldyd;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ldpx;Ldpx;Ldyg;)V

    return-object v0
.end method
