.class public final enum Ldww;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldww;

.field public static final enum b:Ldww;

.field public static final enum c:Ldww;

.field public static final enum d:Ldww;

.field public static final enum e:Ldww;

.field public static final enum f:Ldww;

.field private static final synthetic g:[Ldww;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 9
    new-instance v0, Ldww;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v3}, Ldww;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldww;->a:Ldww;

    .line 10
    new-instance v0, Ldww;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v4}, Ldww;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldww;->b:Ldww;

    .line 11
    new-instance v0, Ldww;

    const-string v1, "SLEEP"

    invoke-direct {v0, v1, v5}, Ldww;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldww;->c:Ldww;

    .line 12
    new-instance v0, Ldww;

    const-string v1, "DISCONNECTING"

    invoke-direct {v0, v1, v6}, Ldww;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldww;->d:Ldww;

    .line 13
    new-instance v0, Ldww;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v7}, Ldww;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldww;->e:Ldww;

    .line 14
    new-instance v0, Ldww;

    const-string v1, "ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldww;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldww;->f:Ldww;

    .line 8
    const/4 v0, 0x6

    new-array v0, v0, [Ldww;

    sget-object v1, Ldww;->a:Ldww;

    aput-object v1, v0, v3

    sget-object v1, Ldww;->b:Ldww;

    aput-object v1, v0, v4

    sget-object v1, Ldww;->c:Ldww;

    aput-object v1, v0, v5

    sget-object v1, Ldww;->d:Ldww;

    aput-object v1, v0, v6

    sget-object v1, Ldww;->e:Ldww;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldww;->f:Ldww;

    aput-object v2, v0, v1

    sput-object v0, Ldww;->g:[Ldww;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldww;
    .locals 1

    .prologue
    .line 8
    const-class v0, Ldww;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldww;

    return-object v0
.end method

.method public static values()[Ldww;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Ldww;->g:[Ldww;

    invoke-virtual {v0}, [Ldww;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldww;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 18
    sget-object v0, Ldww;->a:Ldww;

    if-eq p0, v0, :cond_0

    sget-object v0, Ldww;->b:Ldww;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
