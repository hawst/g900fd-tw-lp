.class public final Lbmr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgdy;


# instance fields
.field private final a:Lari;


# direct methods
.method public constructor <init>(Lari;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lbmr;->a:Lari;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lfqj;Z)Lgds;
    .locals 6

    .prologue
    .line 34
    iget-object v0, p0, Lbmr;->a:Lari;

    invoke-virtual {v0}, Lari;->aI()Ldjf;

    move-result-object v1

    .line 35
    iget-object v0, p1, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v0}, La;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36
    if-eqz v1, :cond_0

    .line 37
    new-instance v0, Ldji;

    new-instance v2, Lgdr;

    invoke-direct {v2}, Lgdr;-><init>()V

    invoke-direct {v0, v2, v1}, Ldji;-><init>(Lgds;Ldjf;)V

    .line 53
    :goto_0
    return-object v0

    .line 40
    :cond_0
    new-instance v0, Ljava/lang/InstantiationException;

    const-string v1, "Cannot create ProxyPlayer because MediaServer is null"

    invoke-direct {v0, v1}, Ljava/lang/InstantiationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_1
    iget-object v0, p0, Lbmr;->a:Lari;

    invoke-virtual {v0}, Lari;->Z()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p2, :cond_2

    iget-object v0, p1, Lfqj;->a:Lhgy;

    iget-wide v2, v0, Lhgy;->k:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 48
    iget-object v0, p1, Lfqj;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 49
    const-string v0, "Using Regular Player with ExoProxy."

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 50
    new-instance v0, Ldja;

    new-instance v2, Lgdr;

    invoke-direct {v2}, Lgdr;-><init>()V

    invoke-direct {v0, v2, v1, p1}, Ldja;-><init>(Lgds;Ldjf;Lfqj;)V

    goto :goto_0

    .line 52
    :cond_2
    const-string v0, "Using Regular Player."

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 53
    new-instance v0, Lgdr;

    invoke-direct {v0}, Lgdr;-><init>()V

    goto :goto_0
.end method
