.class public final Lhhr;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhhs;

.field public b:[Lhhs;

.field public c:Lhhs;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63050
    invoke-direct {p0}, Lidf;-><init>()V

    .line 63053
    iput-object v1, p0, Lhhr;->a:Lhhs;

    .line 63056
    sget-object v0, Lhhs;->a:[Lhhs;

    iput-object v0, p0, Lhhr;->b:[Lhhs;

    .line 63059
    iput-object v1, p0, Lhhr;->c:Lhhs;

    .line 63050
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 63117
    .line 63118
    iget-object v0, p0, Lhhr;->a:Lhhs;

    if-eqz v0, :cond_3

    .line 63119
    const/4 v0, 0x1

    iget-object v2, p0, Lhhr;->a:Lhhs;

    .line 63120
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 63122
    :goto_0
    iget-object v2, p0, Lhhr;->b:[Lhhs;

    if-eqz v2, :cond_1

    .line 63123
    iget-object v2, p0, Lhhr;->b:[Lhhs;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 63124
    if-eqz v4, :cond_0

    .line 63125
    const/4 v5, 0x2

    .line 63126
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 63123
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 63130
    :cond_1
    iget-object v1, p0, Lhhr;->c:Lhhs;

    if-eqz v1, :cond_2

    .line 63131
    const/4 v1, 0x3

    iget-object v2, p0, Lhhr;->c:Lhhs;

    .line 63132
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63134
    :cond_2
    iget-object v1, p0, Lhhr;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63135
    iput v0, p0, Lhhr;->J:I

    .line 63136
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63046
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhhr;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhhr;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhhr;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhhr;->a:Lhhs;

    if-nez v0, :cond_2

    new-instance v0, Lhhs;

    invoke-direct {v0}, Lhhs;-><init>()V

    iput-object v0, p0, Lhhr;->a:Lhhs;

    :cond_2
    iget-object v0, p0, Lhhr;->a:Lhhs;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhhr;->b:[Lhhs;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhhs;

    iget-object v3, p0, Lhhr;->b:[Lhhs;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhhr;->b:[Lhhs;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhhr;->b:[Lhhs;

    :goto_2
    iget-object v2, p0, Lhhr;->b:[Lhhs;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhhr;->b:[Lhhs;

    new-instance v3, Lhhs;

    invoke-direct {v3}, Lhhs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhr;->b:[Lhhs;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhhr;->b:[Lhhs;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhhr;->b:[Lhhs;

    new-instance v3, Lhhs;

    invoke-direct {v3}, Lhhs;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhhr;->b:[Lhhs;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhhr;->c:Lhhs;

    if-nez v0, :cond_6

    new-instance v0, Lhhs;

    invoke-direct {v0}, Lhhs;-><init>()V

    iput-object v0, p0, Lhhr;->c:Lhhs;

    :cond_6
    iget-object v0, p0, Lhhr;->c:Lhhs;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 63098
    iget-object v0, p0, Lhhr;->a:Lhhs;

    if-eqz v0, :cond_0

    .line 63099
    const/4 v0, 0x1

    iget-object v1, p0, Lhhr;->a:Lhhs;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63101
    :cond_0
    iget-object v0, p0, Lhhr;->b:[Lhhs;

    if-eqz v0, :cond_2

    .line 63102
    iget-object v1, p0, Lhhr;->b:[Lhhs;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 63103
    if-eqz v3, :cond_1

    .line 63104
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 63102
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63108
    :cond_2
    iget-object v0, p0, Lhhr;->c:Lhhs;

    if-eqz v0, :cond_3

    .line 63109
    const/4 v0, 0x3

    iget-object v1, p0, Lhhr;->c:Lhhs;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63111
    :cond_3
    iget-object v0, p0, Lhhr;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 63113
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63072
    if-ne p1, p0, :cond_1

    .line 63078
    :cond_0
    :goto_0
    return v0

    .line 63073
    :cond_1
    instance-of v2, p1, Lhhr;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 63074
    :cond_2
    check-cast p1, Lhhr;

    .line 63075
    iget-object v2, p0, Lhhr;->a:Lhhs;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhr;->a:Lhhs;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhhr;->b:[Lhhs;

    iget-object v3, p1, Lhhr;->b:[Lhhs;

    .line 63076
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhr;->c:Lhhs;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhr;->c:Lhhs;

    if-nez v2, :cond_3

    .line 63077
    :goto_2
    iget-object v2, p0, Lhhr;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhhr;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 63078
    goto :goto_0

    .line 63075
    :cond_4
    iget-object v2, p0, Lhhr;->a:Lhhs;

    iget-object v3, p1, Lhhr;->a:Lhhs;

    invoke-virtual {v2, v3}, Lhhs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 63076
    :cond_5
    iget-object v2, p0, Lhhr;->c:Lhhs;

    iget-object v3, p1, Lhhr;->c:Lhhs;

    .line 63077
    invoke-virtual {v2, v3}, Lhhs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhhr;->I:Ljava/util/List;

    iget-object v3, p1, Lhhr;->I:Ljava/util/List;

    .line 63078
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63082
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 63084
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhr;->a:Lhhs;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 63085
    iget-object v2, p0, Lhhr;->b:[Lhhs;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 63091
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhhr;->c:Lhhs;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 63092
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhhr;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_2
    add-int/2addr v0, v1

    .line 63093
    return v0

    .line 63084
    :cond_1
    iget-object v0, p0, Lhhr;->a:Lhhs;

    invoke-virtual {v0}, Lhhs;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    .line 63087
    :goto_3
    iget-object v3, p0, Lhhr;->b:[Lhhs;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 63088
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhhr;->b:[Lhhs;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 63087
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 63088
    :cond_3
    iget-object v2, p0, Lhhr;->b:[Lhhs;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhhs;->hashCode()I

    move-result v2

    goto :goto_4

    .line 63091
    :cond_4
    iget-object v0, p0, Lhhr;->c:Lhhs;

    invoke-virtual {v0}, Lhhs;->hashCode()I

    move-result v0

    goto :goto_1

    .line 63092
    :cond_5
    iget-object v1, p0, Lhhr;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
