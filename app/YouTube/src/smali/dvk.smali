.class Ldvk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldxw;


# instance fields
.field private final a:Ljava/util/Set;

.field private synthetic b:Ldvb;


# direct methods
.method public constructor <init>(Ldvb;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 720
    iput-object p1, p0, Ldvk;->b:Ldvb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 721
    iput-object p2, p0, Ldvk;->a:Ljava/util/Set;

    .line 722
    return-void
.end method


# virtual methods
.method public a(Ldtm;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 726
    new-instance v2, Ldwf;

    invoke-direct {v2, p1}, Ldwf;-><init>(Ldtm;)V

    .line 727
    iget-object v0, p0, Ldvk;->b:Ldvb;

    iget-object v0, v0, Ldvb;->e:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v3, Ldww;->b:Ldww;

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Ldvk;->b:Ldvb;

    .line 728
    iget-object v0, v0, Ldvb;->e:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-virtual {v2, v0}, Ldwf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ldtm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    iget-object v0, p1, Ldtm;->a:Ldsp;

    iget v0, v0, Ldsp;->c:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 733
    iget-object v0, p0, Ldvk;->b:Ldvb;

    iget-object v0, v0, Ldvb;->l:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x36

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "The app status for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " is STOPPED. Will remove the route!"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 734
    iget-object v0, p0, Ldvk;->b:Ldvb;

    invoke-virtual {v0, v2}, Ldvb;->a(Ldwf;)V

    .line 735
    iget-object v0, p0, Ldvk;->a:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 759
    :goto_0
    return-void

    .line 739
    :cond_0
    iget-object v0, p0, Ldvk;->b:Ldvb;

    iget-object v0, v0, Ldvb;->h:Ljava/util/Map;

    iget-object v3, p1, Ldtm;->e:Ldtj;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 741
    iget-object v0, p0, Ldvk;->b:Ldvb;

    iget-object v3, p1, Ldtm;->e:Ldtj;

    invoke-static {v0, v3}, Ldvb;->a(Ldvb;Ldtj;)Ldwf;

    move-result-object v0

    .line 742
    if-eqz v0, :cond_3

    .line 743
    iget-object v0, v0, Ldwf;->a:Ldtm;

    .line 746
    invoke-virtual {p1}, Ldtm;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Ldtm;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 747
    iget-object v3, p1, Ldtm;->a:Ldsp;

    iget-object v0, v0, Ldtm;->a:Ldsp;

    if-eq v3, v0, :cond_8

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    .line 750
    :cond_1
    iget-object v0, p0, Ldvk;->b:Ldvb;

    iget-object v0, v0, Ldvb;->e:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->b:Ldww;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Ldvk;->b:Ldvb;

    .line 751
    iget-object v0, v0, Ldvb;->e:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->a:Ldww;

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Ldvk;->b:Ldvb;

    .line 752
    iget-object v0, v0, Ldvb;->e:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-virtual {v0}, Ldwr;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 753
    iget-object v0, p1, Ldtm;->e:Ldtj;

    iget-object v1, p0, Ldvk;->b:Ldvb;

    iget-object v1, v1, Ldvb;->e:Ldwq;

    invoke-interface {v1}, Ldwq;->n()Ldwr;

    move-result-object v1

    invoke-virtual {v1}, Ldwr;->f()Ldwf;

    move-result-object v1

    iget-object v1, v1, Ldwf;->a:Ldtm;

    iget-object v1, v1, Ldtm;->e:Ldtj;

    invoke-virtual {v0, v1}, Ldtj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 754
    :cond_3
    iget-object v0, p0, Ldvk;->b:Ldvb;

    invoke-static {v0, v2}, Ldvb;->a(Ldvb;Ldwf;)V

    .line 758
    :cond_4
    iget-object v0, p0, Ldvk;->a:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 747
    :cond_5
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-eq v4, v5, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    check-cast v0, Ldsp;

    iget-object v4, v3, Ldsp;->b:Ldth;

    if-nez v4, :cond_7

    iget-object v0, v0, Ldsp;->b:Ldth;

    if-eqz v0, :cond_8

    move v0, v1

    goto :goto_1

    :cond_7
    iget-object v3, v3, Ldsp;->b:Ldth;

    iget-object v0, v0, Ldsp;->b:Ldth;

    invoke-virtual {v3, v0}, Ldth;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_1

    :cond_8
    const/4 v0, 0x1

    goto :goto_1
.end method
