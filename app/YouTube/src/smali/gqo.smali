.class public final Lgqo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:Ljava/util/TreeSet;

.field public f:J

.field public g:J

.field public final h:Ljava/util/List;

.field private final i:I

.field private final j:I

.field private final k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 625
    new-instance v0, Lgqp;

    invoke-direct {v0}, Lgqp;-><init>()V

    sput-object v0, Lgqo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/net/Uri;IIIIJLjava/util/Collection;J)V
    .locals 10

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v2, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v2, p0, Lgqo;->h:Ljava/util/List;

    .line 113
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lgqo;->a:Landroid/net/Uri;

    .line 114
    iput p2, p0, Lgqo;->b:I

    .line 115
    iput p3, p0, Lgqo;->i:I

    .line 116
    iput p4, p0, Lgqo;->j:I

    .line 120
    if-eqz p5, :cond_0

    const/16 v2, 0x5a

    if-eq p5, v2, :cond_0

    const/16 v2, 0xb4

    if-eq p5, v2, :cond_0

    const/16 v2, 0x10e

    if-eq p5, v2, :cond_0

    .line 124
    new-instance v2, Lgqs;

    const-string v3, "Unsupported rotation specification"

    invoke-direct {v2, v3}, Lgqs;-><init>(Ljava/lang/String;)V

    throw v2

    .line 126
    :cond_0
    iput p5, p0, Lgqo;->c:I

    .line 128
    cmp-long v2, p6, p9

    if-gtz v2, :cond_1

    .line 129
    new-instance v2, Lgqu;

    const-string v3, "Video too short to edit"

    invoke-direct {v2, v3}, Lgqu;-><init>(Ljava/lang/String;)V

    throw v2

    .line 131
    :cond_1
    move-wide/from16 v0, p6

    iput-wide v0, p0, Lgqo;->d:J

    .line 132
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lgqo;->k:J

    .line 134
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    invoke-interface/range {p8 .. p8}, Ljava/util/Collection;->size()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lb;->b(Z)V

    .line 136
    new-instance v2, Ljava/util/TreeSet;

    move-object/from16 v0, p8

    invoke-direct {v2, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lgqo;->e:Ljava/util/TreeSet;

    .line 137
    iget-object v2, p0, Lgqo;->e:Ljava/util/TreeSet;

    invoke-virtual {v2}, Ljava/util/TreeSet;->first()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 138
    iget-object v4, p0, Lgqo;->e:Ljava/util/TreeSet;

    invoke-virtual {v4}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v4, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 139
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v4, v8, v4

    const-wide/32 v8, 0x2dc6c0

    cmp-long v3, v4, v8

    if-lez v3, :cond_3

    .line 140
    new-instance v2, Lgqs;

    invoke-direct {v2}, Lgqs;-><init>()V

    throw v2

    .line 135
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 142
    :cond_3
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-wide v4, v2

    .line 143
    goto :goto_1

    .line 145
    :cond_4
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lgqo;->f:J

    .line 146
    move-wide/from16 v0, p6

    iput-wide v0, p0, Lgqo;->g:J

    .line 147
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lgqo;->h:Ljava/util/List;

    .line 594
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lgqo;->a:Landroid/net/Uri;

    .line 595
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgqo;->b:I

    .line 596
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgqo;->i:I

    .line 597
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgqo;->j:I

    .line 598
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lgqo;->c:I

    .line 599
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgqo;->d:J

    .line 600
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgqo;->k:J

    .line 601
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    iput-object v0, p0, Lgqo;->e:Ljava/util/TreeSet;

    .line 602
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgqo;->f:J

    .line 603
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgqo;->g:J

    .line 604
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Lgqo;
    .locals 4

    .prologue
    .line 162
    sget-object v0, Lgql;->a:Lgql;

    const-wide/32 v2, 0xf4240

    invoke-static {p0, p1, v0, v2, v3}, Lgqo;->a(Landroid/content/Context;Landroid/net/Uri;Lgql;J)Lgqo;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Lgql;J)Lgqo;
    .locals 14

    .prologue
    .line 483
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 484
    new-instance v0, Lgqt;

    invoke-direct {v0}, Lgqt;-><init>()V

    throw v0

    .line 487
    :cond_0
    const/4 v2, -0x1

    .line 488
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 494
    invoke-interface/range {p2 .. p2}, Lgql;->a()Lgqn;

    move-result-object v1

    .line 496
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, p0, p1, v0}, Lgqn;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 498
    invoke-virtual {v1}, Lgqn;->f()J

    move-result-wide v4

    const-wide/32 v6, 0x77359400

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    .line 499
    new-instance v0, Lgqu;

    const-string v2, "Video editing of files >2GB is not supported"

    invoke-direct {v0, v2}, Lgqu;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lgqn;->a()V

    throw v0

    .line 502
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Lgqn;->b()I

    move-result v3

    .line 503
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_4

    .line 504
    invoke-virtual {v1, v0}, Lgqn;->a(I)Landroid/media/MediaFormat;

    move-result-object v4

    .line 505
    const-string v5, "mime"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 506
    const-string v5, "video/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 507
    const/4 v4, -0x1

    if-eq v2, v4, :cond_2

    .line 508
    new-instance v0, Lgqs;

    const-string v2, "Multiple video tracks are not supported"

    invoke-direct {v0, v2}, Lgqs;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v2, v0

    .line 503
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 515
    :cond_4
    const/4 v0, -0x1

    if-ne v2, v0, :cond_5

    .line 516
    new-instance v0, Lgqs;

    const-string v2, "No video tracks found"

    invoke-direct {v0, v2}, Lgqs;-><init>(Ljava/lang/String;)V

    throw v0

    .line 519
    :cond_5
    invoke-virtual {v1, v2}, Lgqn;->b(I)V

    .line 520
    invoke-virtual {v1, v2}, Lgqn;->a(I)Landroid/media/MediaFormat;

    move-result-object v0

    .line 522
    const-string v3, "width"

    invoke-virtual {v0, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v3

    .line 523
    const-string v4, "height"

    invoke-virtual {v0, v4}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v4

    .line 524
    const-string v5, "durationUs"

    invoke-virtual {v0, v5}, Landroid/media/MediaFormat;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 527
    const-wide/16 v10, 0x0

    const/4 v0, 0x2

    invoke-virtual {v1, v10, v11, v0}, Lgqn;->a(JI)V

    .line 529
    invoke-virtual {v1}, Lgqn;->d()J

    move-result-wide v10

    .line 533
    const/4 v0, 0x1

    .line 534
    :goto_1
    const-wide/16 v12, -0x1

    cmp-long v5, v10, v12

    if-eqz v5, :cond_7

    .line 535
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 537
    :goto_2
    int-to-long v12, v0

    add-long/2addr v12, v10

    const/4 v5, 0x1

    invoke-virtual {v1, v12, v13, v5}, Lgqn;->a(JI)V

    .line 538
    invoke-virtual {v1}, Lgqn;->d()J

    move-result-wide v12

    cmp-long v5, v10, v12

    if-nez v5, :cond_6

    .line 539
    mul-int/lit8 v0, v0, 0xa

    goto :goto_2

    .line 544
    :cond_6
    invoke-virtual {v1}, Lgqn;->d()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v10

    goto :goto_1

    .line 548
    :cond_7
    invoke-virtual {v1}, Lgqn;->a()V

    .line 551
    invoke-static {p0, p1}, Lgqo;->b(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v5

    .line 553
    new-instance v0, Lgqo;

    const-wide/32 v9, 0xf4240

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lgqo;-><init>(Landroid/net/Uri;IIIIJLjava/util/Collection;J)V

    return-object v0
.end method

.method public static a(Lgqo;)Lgqo;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 172
    if-nez p0, :cond_0

    move-object v0, v11

    .line 186
    :goto_0
    return-object v0

    .line 176
    :cond_0
    :try_start_0
    new-instance v0, Lgqo;

    iget-object v1, p0, Lgqo;->a:Landroid/net/Uri;

    iget v2, p0, Lgqo;->b:I

    iget v3, p0, Lgqo;->i:I

    iget v4, p0, Lgqo;->j:I

    iget v5, p0, Lgqo;->c:I

    iget-wide v6, p0, Lgqo;->d:J

    iget-object v8, p0, Lgqo;->e:Ljava/util/TreeSet;

    iget-wide v9, p0, Lgqo;->k:J

    invoke-direct/range {v0 .. v10}, Lgqo;-><init>(Landroid/net/Uri;IIIIJLjava/util/Collection;J)V
    :try_end_0
    .catch Lgqu; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    move-object v0, v11

    goto :goto_0
.end method

.method private a(Lgqq;)V
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lgqo;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqr;

    .line 587
    invoke-interface {v0, p0, p1}, Lgqr;->a(Lgqo;Lgqq;)V

    goto :goto_0

    .line 589
    :cond_0
    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 3

    .prologue
    .line 566
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 567
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 569
    :try_start_0
    invoke-virtual {v1, p0, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 570
    const/16 v0, 0x18

    .line 571
    invoke-virtual {v1, v0}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_0

    .line 573
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 579
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 582
    :goto_0
    return v0

    .line 579
    :cond_0
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 582
    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 576
    :catch_0
    move-exception v0

    .line 577
    :try_start_1
    const-string v2, "Unable to determine video rotation"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 579
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v0
.end method


# virtual methods
.method public final a()F
    .locals 3

    .prologue
    const/16 v2, 0xb4

    .line 260
    iget v0, p0, Lgqo;->c:I

    if-eqz v0, :cond_0

    iget v0, p0, Lgqo;->c:I

    if-ne v0, v2, :cond_2

    :cond_0
    iget v0, p0, Lgqo;->i:I

    :goto_0
    int-to-float v1, v0

    iget v0, p0, Lgqo;->c:I

    if-eqz v0, :cond_1

    iget v0, p0, Lgqo;->c:I

    if-ne v0, v2, :cond_3

    :cond_1
    iget v0, p0, Lgqo;->j:I

    :goto_1
    int-to-float v0, v0

    div-float v0, v1, v0

    return v0

    :cond_2
    iget v0, p0, Lgqo;->j:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lgqo;->i:I

    goto :goto_1
.end method

.method public final a(J)J
    .locals 7

    .prologue
    .line 270
    const/4 v2, 0x0

    .line 272
    iget-object v0, p0, Lgqo;->e:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 273
    iget-object v1, p0, Lgqo;->e:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 275
    if-eqz v0, :cond_1

    .line 277
    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sub-long/2addr v2, p1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, p1, v4

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 284
    :cond_0
    :goto_0
    if-nez v1, :cond_2

    .line 285
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 280
    :cond_1
    if-nez v1, :cond_0

    move-object v1, v2

    goto :goto_0

    .line 288
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(Lgqr;)V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lgqo;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    return-void
.end method

.method public final b()J
    .locals 4

    .prologue
    .line 332
    iget-wide v0, p0, Lgqo;->f:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final b(J)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lgqo;->e:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->floor(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public final b(Lgqr;)V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lgqo;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 441
    return-void
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 346
    iget-wide v0, p0, Lgqo;->g:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final c(J)V
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 356
    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    move-wide p1, v0

    .line 363
    :cond_0
    iget-wide v0, p0, Lgqo;->g:J

    iget-wide v2, p0, Lgqo;->k:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 364
    iget-wide v0, p0, Lgqo;->g:J

    iget-wide v2, p0, Lgqo;->k:J

    sub-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 365
    iget-wide v0, p0, Lgqo;->g:J

    iget-wide v2, p0, Lgqo;->k:J

    sub-long p1, v0, v2

    .line 368
    :cond_1
    iget-wide v0, p0, Lgqo;->f:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_2

    .line 369
    iput-wide p1, p0, Lgqo;->f:J

    .line 370
    sget-object v0, Lgqq;->a:Lgqq;

    invoke-direct {p0, v0}, Lgqo;->a(Lgqq;)V

    .line 372
    :cond_2
    return-void

    .line 363
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(J)V
    .locals 7

    .prologue
    .line 381
    iget-wide v0, p0, Lgqo;->d:J

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 382
    iget-wide p1, p0, Lgqo;->d:J

    .line 388
    :cond_0
    iget-wide v0, p0, Lgqo;->f:J

    iget-wide v2, p0, Lgqo;->d:J

    iget-wide v4, p0, Lgqo;->k:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 389
    iget-wide v0, p0, Lgqo;->f:J

    iget-wide v2, p0, Lgqo;->k:J

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_1

    .line 390
    iget-wide v0, p0, Lgqo;->f:J

    iget-wide v2, p0, Lgqo;->k:J

    add-long p1, v0, v2

    .line 393
    :cond_1
    iget-wide v0, p0, Lgqo;->g:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_2

    .line 394
    iput-wide p1, p0, Lgqo;->g:J

    .line 395
    sget-object v0, Lgqq;->b:Lgqq;

    invoke-direct {p0, v0}, Lgqo;->a(Lgqq;)V

    .line 397
    :cond_2
    return-void

    .line 388
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 608
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 452
    if-ne p0, p1, :cond_0

    .line 453
    const/4 v0, 0x1

    .line 460
    :goto_0
    return v0

    .line 455
    :cond_0
    instance-of v0, p1, Lgqo;

    if-nez v0, :cond_1

    .line 456
    const/4 v0, 0x0

    goto :goto_0

    .line 459
    :cond_1
    check-cast p1, Lgqo;

    .line 460
    iget-object v0, p0, Lgqo;->a:Landroid/net/Uri;

    iget-object v1, p1, Lgqo;->a:Landroid/net/Uri;

    invoke-static {v0, v1}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 447
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lgqo;->a:Landroid/net/Uri;

    aput-object v2, v0, v1

    invoke-static {v0}, Lb;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 465
    invoke-static {p0}, Lb;->b(Ljava/lang/Object;)Lewf;

    move-result-object v1

    const-string v0, "source"

    iget-object v2, p0, Lgqo;->a:Landroid/net/Uri;

    .line 466
    new-instance v3, Lewg;

    invoke-direct {v3}, Lewg;-><init>()V

    iget-object v4, v1, Lewf;->a:Lewg;

    iput-object v3, v4, Lewg;->c:Lewg;

    iput-object v3, v1, Lewf;->a:Lewg;

    iput-object v2, v3, Lewg;->b:Ljava/lang/Object;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lewg;->a:Ljava/lang/String;

    .line 467
    invoke-virtual {v1}, Lewf;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lgqo;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 614
    iget v0, p0, Lgqo;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 615
    iget v0, p0, Lgqo;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 616
    iget v0, p0, Lgqo;->j:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 617
    iget v0, p0, Lgqo;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 618
    iget-wide v0, p0, Lgqo;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 619
    iget-wide v0, p0, Lgqo;->k:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 620
    iget-object v0, p0, Lgqo;->e:Ljava/util/TreeSet;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 621
    iget-wide v0, p0, Lgqo;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 622
    iget-wide v0, p0, Lgqo;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 623
    return-void
.end method
