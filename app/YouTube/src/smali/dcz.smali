.class public final Ldcz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ldcx;


# direct methods
.method public constructor <init>(Ldcx;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldcx;

    iput-object v0, p0, Ldcz;->a:Ldcx;

    .line 23
    iget-object v0, p0, Ldcz;->a:Ldcx;

    invoke-virtual {v0}, Ldcx;->c()V

    .line 24
    return-void
.end method


# virtual methods
.method public final handleFadeEvent(Lcyx;)V
    .locals 5
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 28
    sget-object v0, Ldda;->a:[I

    iget v1, p1, Lcyx;->c:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 30
    :pswitch_0
    iget-object v0, p0, Ldcz;->a:Ldcx;

    invoke-virtual {v0}, Ldcx;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcz;->a:Ldcx;

    iget-wide v2, p1, Lcyx;->b:J

    invoke-virtual {v0, v4}, Ldcx;->setVisibility(I)V

    invoke-virtual {v0}, Ldcx;->clearAnimation()V

    iget-object v1, v0, Ldcx;->a:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, v0, Ldcx;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Ldcx;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 33
    :pswitch_1
    iget-object v0, p0, Ldcz;->a:Ldcx;

    invoke-virtual {v0}, Ldcx;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldcz;->a:Ldcx;

    iget-wide v2, p1, Lcyx;->b:J

    invoke-virtual {v0, v4}, Ldcx;->setVisibility(I)V

    invoke-virtual {v0}, Ldcx;->clearAnimation()V

    iget-object v1, v0, Ldcx;->b:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, v0, Ldcx;->b:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Ldcx;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 36
    :pswitch_2
    iget-object v0, p0, Ldcz;->a:Ldcx;

    invoke-virtual {v0}, Ldcx;->c()V

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
