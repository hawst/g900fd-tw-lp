.class public final Lbwt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/graphics/Bitmap;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:F

.field private final f:I

.field private final g:I

.field private final h:[I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:Landroid/graphics/Bitmap;


# direct methods
.method private constructor <init>(IIII[IIIIIIIIIII)V
    .locals 4

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    if-lez p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "stackSize must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 82
    if-lez p2, :cond_1

    const/4 v1, 0x1

    :goto_1
    const-string v2, "width must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 83
    if-lez p3, :cond_2

    const/4 v1, 0x1

    :goto_2
    const-string v2, "height must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 84
    if-lez p4, :cond_3

    const/4 v1, 0x1

    :goto_3
    const-string v2, "scale must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 85
    array-length v1, p5

    if-lt v1, p1, :cond_4

    const/4 v1, 0x1

    :goto_4
    const-string v2, "rotationAngles count must be greater or equal than stackSize"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 88
    if-lez p9, :cond_5

    const/4 v1, 0x1

    :goto_5
    const-string v2, "outerBorder must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 89
    if-lez p11, :cond_6

    const/4 v1, 0x1

    :goto_6
    const-string v2, "innerBorder must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 90
    if-lez p13, :cond_7

    const/4 v1, 0x1

    :goto_7
    const-string v2, "defaultBitmapWidth must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 91
    if-lez p14, :cond_8

    const/4 v1, 0x1

    :goto_8
    const-string v2, "defaultBitmapHeight must be > 0"

    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 93
    iput p1, p0, Lbwt;->b:I

    .line 94
    iput p2, p0, Lbwt;->c:I

    .line 95
    iput p3, p0, Lbwt;->d:I

    .line 96
    int-to-float v1, p4

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    iput v1, p0, Lbwt;->e:F

    .line 97
    iput-object p5, p0, Lbwt;->h:[I

    .line 98
    iput p6, p0, Lbwt;->i:I

    .line 99
    iput p7, p0, Lbwt;->f:I

    .line 100
    iput p8, p0, Lbwt;->g:I

    .line 101
    iput p9, p0, Lbwt;->j:I

    .line 102
    iput p10, p0, Lbwt;->k:I

    .line 103
    iput p11, p0, Lbwt;->l:I

    .line 104
    move/from16 v0, p12

    iput v0, p0, Lbwt;->m:I

    .line 105
    move/from16 v0, p13

    iput v0, p0, Lbwt;->n:I

    .line 106
    move/from16 v0, p14

    iput v0, p0, Lbwt;->o:I

    .line 107
    move/from16 v0, p15

    iput v0, p0, Lbwt;->p:I

    .line 109
    iget v1, p0, Lbwt;->n:I

    iget v2, p0, Lbwt;->o:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget v3, p0, Lbwt;->p:I

    invoke-virtual {v2, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    iput-object v1, p0, Lbwt;->q:Landroid/graphics/Bitmap;

    .line 110
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lbwt;->a(Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lbwt;->a:Landroid/graphics/Bitmap;

    .line 111
    return-void

    .line 81
    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 82
    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 83
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 84
    :cond_3
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 85
    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    .line 88
    :cond_5
    const/4 v1, 0x0

    goto :goto_5

    .line 89
    :cond_6
    const/4 v1, 0x0

    goto :goto_6

    .line 90
    :cond_7
    const/4 v1, 0x0

    goto :goto_7

    .line 91
    :cond_8
    const/4 v1, 0x0

    goto :goto_8
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 17

    .prologue
    .line 47
    const v1, 0x7f0b0026

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const v1, 0x7f0a00f9

    .line 48
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v1, 0x7f0a00fa

    .line 49
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v1, 0x7f0b0027

    .line 50
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    const v1, 0x7f0f0003

    .line 51
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v6

    const v1, 0x7f0a00f8

    .line 52
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    const v1, 0x7f0a00fb

    .line 53
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    const v1, 0x7f0a00fc

    .line 54
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    const v1, 0x7f0a00fd

    .line 55
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    const v1, 0x7f0700c2

    .line 56
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    const v1, 0x7f0a00fe

    .line 57
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    const v1, 0x7f0700c3

    .line 58
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    const v1, 0x7f0a00ff

    .line 59
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    const v1, 0x7f0a0100

    .line 60
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v15

    const v1, 0x7f0700c4

    .line 61
    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v16

    move-object/from16 v1, p0

    .line 47
    invoke-direct/range {v1 .. v16}, Lbwt;-><init>(IIII[IIIIIIIIIII)V

    .line 62
    return-void
.end method

.method private a(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 126
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbwt;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lbwt;->a:Landroid/graphics/Bitmap;

    .line 141
    :goto_0
    return-object v0

    .line 130
    :cond_0
    iget v0, p0, Lbwt;->c:I

    iget v1, p0, Lbwt;->d:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 132
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 133
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 134
    iget-object v1, p0, Lbwt;->q:Landroid/graphics/Bitmap;

    iget v2, p0, Lbwt;->n:I

    iget v3, p0, Lbwt;->o:I

    const/4 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbwt;->a(Landroid/graphics/Bitmap;IIILandroid/graphics/Canvas;)V

    :cond_1
    move-object v0, v6

    .line 141
    goto :goto_0

    .line 136
    :cond_2
    iget v0, p0, Lbwt;->b:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    :goto_1
    if-ltz v4, :cond_1

    .line 137
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 138
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbwt;->a(Landroid/graphics/Bitmap;IIILandroid/graphics/Canvas;)V

    .line 136
    add-int/lit8 v4, v4, -0x1

    goto :goto_1
.end method

.method private a(Landroid/graphics/Bitmap;IIILandroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 145
    invoke-virtual {p5}, Landroid/graphics/Canvas;->save()I

    .line 147
    iget v0, p0, Lbwt;->e:F

    iget v2, p0, Lbwt;->i:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 148
    iget v2, p0, Lbwt;->e:F

    iget v3, p0, Lbwt;->j:I

    iget v4, p0, Lbwt;->l:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 150
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 152
    neg-int v4, p2

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    neg-int v5, p3

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 153
    iget v4, p0, Lbwt;->n:I

    int-to-float v4, v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    iget v5, p0, Lbwt;->e:F

    mul-float/2addr v4, v5

    iget v5, p0, Lbwt;->o:I

    int-to-float v5, v5

    int-to-float v6, p3

    div-float/2addr v5, v6

    iget v6, p0, Lbwt;->e:F

    mul-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 157
    iget-object v4, p0, Lbwt;->h:[I

    aget v4, v4, p4

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 160
    iget v4, p0, Lbwt;->c:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    shl-int/lit8 v5, p4, 0x1

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    iget v5, p0, Lbwt;->d:I

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    shl-int/lit8 v6, p4, 0x1

    int-to-float v6, v6

    mul-float/2addr v0, v6

    sub-float v0, v5, v0

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 165
    iget v0, p0, Lbwt;->c:I

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v4, p0, Lbwt;->e:F

    iget v5, p0, Lbwt;->n:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    div-float/2addr v4, v7

    add-float/2addr v0, v4

    div-int/lit8 v4, v2, 0x2

    int-to-float v4, v4

    add-float/2addr v0, v4

    iget v4, p0, Lbwt;->f:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    iget v4, p0, Lbwt;->d:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    iget v5, p0, Lbwt;->e:F

    iget v6, p0, Lbwt;->o:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    div-float/2addr v5, v7

    sub-float/2addr v4, v5

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float v2, v4, v2

    iget v4, p0, Lbwt;->g:I

    int-to-float v4, v4

    sub-float/2addr v2, v4

    invoke-virtual {v3, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 169
    invoke-virtual {p5, v3}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 172
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 173
    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 174
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 177
    iget v0, p0, Lbwt;->j:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 178
    iget v0, p0, Lbwt;->k:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 179
    int-to-float v3, p2

    int-to-float v4, p3

    move-object v0, p5

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 182
    iget v0, p0, Lbwt;->l:I

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 183
    iget v0, p0, Lbwt;->m:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 184
    int-to-float v3, p2

    int-to-float v4, p3

    move-object v0, p5

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 187
    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 188
    invoke-virtual {p5, p1, v1, v1, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 190
    invoke-virtual {p5}, Landroid/graphics/Canvas;->restore()V

    .line 191
    return-void
.end method
