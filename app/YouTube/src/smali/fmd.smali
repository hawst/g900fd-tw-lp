.class public Lfmd;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhtd;

.field public b:Lfnc;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Lfnc;

.field public f:Ljava/util/List;

.field public g:Ljava/util/List;

.field public h:Liag;

.field public i:Lham;

.field private final j:Lfqh;

.field private k:Ljava/lang/CharSequence;

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/CharSequence;

.field private n:Lhog;

.field private o:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhtd;Lfqh;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lfic;-><init>()V

    .line 80
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtd;

    iput-object v0, p0, Lfmd;->a:Lhtd;

    .line 81
    iput-object p2, p0, Lfmd;->j:Lfqh;

    .line 83
    iget-object v0, p1, Lhtd;->j:Lhtc;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p1, Lhtd;->j:Lhtc;

    iget-object v0, v0, Lhtc;->a:Liag;

    iput-object v0, p0, Lfmd;->h:Liag;

    .line 85
    iget-object v0, p1, Lhtd;->j:Lhtc;

    iget-object v0, v0, Lhtc;->b:Lham;

    iput-object v0, p0, Lfmd;->i:Lham;

    .line 87
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 213
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 214
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lfmd;->a:Lhtd;

    iget-object v0, v0, Lhtd;->i:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lfmd;->k:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lfmd;->a:Lhtd;

    iget-object v0, v0, Lhtd;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmd;->k:Ljava/lang/CharSequence;

    .line 104
    :cond_0
    iget-object v0, p0, Lfmd;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lfmd;->a:Lhtd;

    iget-object v0, v0, Lhtd;->m:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lfmd;->j:Lfqh;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lfmd;->l:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 109
    iget-object v0, p0, Lfmd;->a:Lhtd;

    iget-object v0, v0, Lhtd;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmd;->l:Ljava/lang/CharSequence;

    .line 111
    :cond_0
    iget-object v0, p0, Lfmd;->l:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lfmd;->m:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lfmd;->a:Lhtd;

    iget-object v0, v0, Lhtd;->h:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmd;->m:Ljava/lang/CharSequence;

    .line 139
    :cond_0
    iget-object v0, p0, Lfmd;->m:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Lhog;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lfmd;->n:Lhog;

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lfmd;->h:Liag;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lfmd;->h:Liag;

    iget-object v0, v0, Liag;->b:Lhog;

    iput-object v0, p0, Lfmd;->n:Lhog;

    .line 159
    :cond_0
    :goto_0
    iget-object v0, p0, Lfmd;->n:Lhog;

    return-object v0

    .line 152
    :cond_1
    iget-object v0, p0, Lfmd;->i:Lham;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmd;->i:Lham;

    iget-object v0, v0, Lham;->a:Lhgz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfmd;->i:Lham;

    iget-object v0, v0, Lham;->a:Lhgz;

    iget-object v0, v0, Lhgz;->b:[Lhwo;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lfmd;->i:Lham;

    iget-object v0, v0, Lham;->a:Lhgz;

    iget-object v0, v0, Lhgz;->b:[Lhwo;

    aget-object v0, v0, v1

    iget-object v0, v0, Lhwo;->e:Lhog;

    iput-object v0, p0, Lfmd;->n:Lhog;

    goto :goto_0
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lfmd;->o:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lfmd;->h:Liag;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lfmd;->h:Liag;

    iget-object v0, v0, Liag;->a:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmd;->o:Ljava/lang/CharSequence;

    .line 171
    :cond_0
    :goto_0
    iget-object v0, p0, Lfmd;->o:Ljava/lang/CharSequence;

    return-object v0

    .line 166
    :cond_1
    iget-object v0, p0, Lfmd;->i:Lham;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lfmd;->i:Lham;

    iget-object v0, v0, Lham;->a:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmd;->o:Ljava/lang/CharSequence;

    goto :goto_0
.end method
