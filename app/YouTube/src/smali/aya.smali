.class public final Laya;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Laxk;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/content/SharedPreferences;

.field final c:Ldsn;

.field final d:Landroid/content/res/Resources;

.field final e:Ldbb;

.field f:Z

.field g:Ldwq;

.field private final h:Levn;

.field private final i:Layk;

.field private final j:Ldwv;

.field private final k:Laxw;

.field private final l:Landroid/app/NotificationManager;

.field private m:Landroid/content/BroadcastReceiver;

.field private n:Z

.field private o:Ljava/lang/ref/WeakReference;

.field private final p:Ldso;


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Landroid/content/SharedPreferences;Ldsn;Ldwv;Ldbb;Laxw;)V
    .locals 3

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Laya;->a:Landroid/content/Context;

    .line 86
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Laya;->h:Levn;

    .line 87
    iget-object v0, p0, Laya;->a:Landroid/content/Context;

    const-string v1, "notification"

    .line 88
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Laya;->l:Landroid/app/NotificationManager;

    .line 89
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Laya;->b:Landroid/content/SharedPreferences;

    .line 90
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbb;

    iput-object v0, p0, Laya;->e:Ldbb;

    .line 91
    iget-object v0, p0, Laya;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Laya;->d:Landroid/content/res/Resources;

    .line 92
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldsn;

    iput-object v0, p0, Laya;->c:Ldsn;

    .line 93
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwv;

    iput-object v0, p0, Laya;->j:Ldwv;

    .line 94
    new-instance v0, Layk;

    iget-object v1, p0, Laya;->a:Landroid/content/Context;

    new-instance v2, Laye;

    invoke-direct {v2, p0}, Laye;-><init>(Laya;)V

    invoke-direct {v0, v1, v2}, Layk;-><init>(Landroid/content/Context;Laym;)V

    iput-object v0, p0, Laya;->i:Layk;

    .line 96
    iput-object p7, p0, Laya;->k:Laxw;

    .line 97
    new-instance v0, Layf;

    invoke-direct {v0, p0}, Layf;-><init>(Laya;)V

    iput-object v0, p0, Laya;->p:Ldso;

    .line 98
    return-void
.end method

.method private a(Lba;Laxg;)V
    .locals 3

    .prologue
    .line 261
    .line 262
    iget v1, p2, Laxg;->e:I

    iget-object v0, p0, Laya;->d:Landroid/content/res/Resources;

    .line 263
    iget v2, p2, Laxg;->f:I

    if-eqz v2, :cond_0

    iget v2, p2, Laxg;->f:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Laya;->a:Landroid/content/Context;

    .line 264
    invoke-virtual {p2, v2}, Laxg;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 261
    invoke-virtual {p1, v1, v0, v2}, Lba;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Lba;

    .line 265
    return-void

    .line 263
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Laya;->h:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Laya;->i:Layk;

    invoke-virtual {v0}, Layk;->a()V

    .line 116
    iget-object v0, p0, Laya;->c:Ldsn;

    iget-object v1, p0, Laya;->p:Ldso;

    invoke-virtual {v0, v1}, Ldsn;->a(Ldso;)V

    .line 117
    iget-object v0, p0, Laya;->c:Ldsn;

    invoke-virtual {v0}, Ldsn;->a()V

    .line 118
    return-void
.end method

.method a(Ldwr;)V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Laya;->c:Ldsn;

    new-instance v1, Layb;

    invoke-direct {v1, p0}, Layb;-><init>(Laya;)V

    invoke-virtual {v0, p1, v1}, Ldsn;->a(Ldwr;Leuc;)V

    .line 211
    return-void
.end method

.method public final a(Lo;)V
    .locals 2

    .prologue
    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Laya;->n:Z

    .line 150
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 151
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Laya;->o:Ljava/lang/ref/WeakReference;

    .line 152
    iget-object v0, p0, Laya;->k:Laxw;

    invoke-interface {v0}, Laxw;->b()V

    .line 153
    invoke-virtual {p0}, Laya;->c()V

    .line 154
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Laya;->n:Z

    .line 138
    iget-object v0, p0, Laya;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 139
    iget-boolean v0, p0, Laya;->f:Z

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Laya;->k:Laxw;

    invoke-interface {v0}, Laxw;->a()V

    .line 142
    :cond_0
    return-void
.end method

.method c()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Laya;->m:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Laya;->a:Landroid/content/Context;

    iget-object v1, p0, Laya;->m:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Laya;->m:Landroid/content/BroadcastReceiver;

    .line 161
    :cond_0
    iget-object v0, p0, Laya;->l:Landroid/app/NotificationManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 162
    return-void
.end method

.method public final handlePlaybackPauseOnAudioBecomingNoisyEvent(Lcza;)V
    .locals 8
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 306
    iget-object v0, p0, Laya;->b:Landroid/content/SharedPreferences;

    const-string v1, "enable_autocast"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 307
    if-eqz v0, :cond_2

    .line 308
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Laya;->j:Ldwv;

    invoke-interface {v1}, Ldwv;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-boolean v1, p0, Laya;->n:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Laya;->k:Laxw;

    invoke-interface {v1}, Laxw;->a()V

    :cond_0
    iget-object v1, p0, Laya;->b:Landroid/content/SharedPreferences;

    const-string v2, "autocast_device_id"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Laya;->b:Landroid/content/SharedPreferences;

    const-string v2, "autocast_device_id"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    invoke-virtual {v0}, Ldwr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v0}, Laya;->a(Ldwr;)V

    .line 310
    :cond_2
    :goto_0
    return-void

    .line 308
    :cond_3
    iget-boolean v1, p0, Laya;->n:Z

    if-eqz v1, :cond_5

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    new-instance v1, Layd;

    invoke-direct {v1, p0, v0}, Layd;-><init>(Laya;Ldwr;)V

    iput-object v1, p0, Laya;->m:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Laya;->a:Landroid/content/Context;

    iget-object v2, p0, Laya;->m:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    sget-object v5, Laxg;->b:Laxg;

    iget-object v5, v5, Laxg;->d:Ljava/lang/String;

    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Laya;->a:Landroid/content/Context;

    iget-object v2, p0, Laya;->m:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    sget-object v5, Laxg;->c:Laxg;

    iget-object v5, v5, Laxg;->d:Ljava/lang/String;

    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v1, p0, Laya;->d:Landroid/content/res/Resources;

    const v2, 0x7f09006f

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lba;

    iget-object v2, p0, Laya;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lba;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Laya;->d:Landroid/content/res/Resources;

    const v3, 0x7f090070

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    invoke-virtual {v1, v0}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v1

    sget-object v2, Laxg;->c:Laxg;

    iget-object v3, p0, Laya;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Laxg;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    move-result-object v1

    invoke-virtual {v1, v0}, Lba;->d(Ljava/lang/CharSequence;)Lba;

    move-result-object v0

    const v1, 0x7f020148

    invoke-virtual {v0, v1}, Lba;->a(I)Lba;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lba;->a(J)Lba;

    move-result-object v0

    sget-object v1, Laxg;->c:Laxg;

    invoke-direct {p0, v0, v1}, Laya;->a(Lba;Laxg;)V

    sget-object v1, Laxg;->b:Laxg;

    invoke-direct {p0, v0, v1}, Laya;->a(Lba;Laxg;)V

    sget-object v1, Laxg;->a:Laxg;

    invoke-direct {p0, v0, v1}, Laya;->a(Lba;Laxg;)V

    invoke-virtual {v0}, Lba;->a()Landroid/app/Notification;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_4

    iput v6, v0, Landroid/app/Notification;->priority:I

    :cond_4
    iget-object v1, p0, Laya;->l:Landroid/app/NotificationManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Laya;->o:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lo;->c()Lt;

    move-result-object v6

    new-instance v0, Laxo;

    iget-object v1, p0, Laya;->b:Landroid/content/SharedPreferences;

    iget-object v2, p0, Laya;->d:Landroid/content/res/Resources;

    iget-object v3, p0, Laya;->j:Ldwv;

    new-instance v5, Layc;

    invoke-direct {v5, p0}, Layc;-><init>(Laya;)V

    invoke-direct/range {v0 .. v5}, Laxo;-><init>(Landroid/content/SharedPreferences;Landroid/content/res/Resources;Ldwv;ZLaxq;)V

    new-instance v1, Laxl;

    invoke-direct {v1, v0}, Laxl;-><init>(Laxo;)V

    iget-object v0, v1, Laxl;->a:Laxo;

    invoke-static {v0}, Laxn;->a(Laxo;)Laxn;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Li;->a(Lt;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
