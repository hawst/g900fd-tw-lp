.class public Lfke;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;
.implements Lfrx;


# instance fields
.field public final a:Lhjb;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field private final f:Lfqh;

.field private g:Lhog;

.field private h:Lhog;

.field private i:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lhjb;Lfqh;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjb;

    iput-object v0, p0, Lfke;->a:Lhjb;

    .line 38
    iput-object p2, p0, Lfke;->f:Lfqh;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lfke;->i:Ljava/util/Set;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lfke;->i:Ljava/util/Set;

    .line 132
    :goto_0
    return-object v0

    .line 124
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfke;->i:Ljava/util/Set;

    .line 125
    invoke-virtual {p0}, Lfke;->c()Lhog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 126
    iget-object v0, p0, Lfke;->i:Ljava/util/Set;

    invoke-virtual {p0}, Lfke;->c()Lhog;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 128
    :cond_1
    invoke-virtual {p0}, Lfke;->b()Lhog;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, Lfke;->i:Ljava/util/Set;

    invoke-virtual {p0}, Lfke;->b()Lhog;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 131
    :cond_2
    iget-object v0, p0, Lfke;->i:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfke;->i:Ljava/util/Set;

    .line 132
    iget-object v0, p0, Lfke;->i:Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 137
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 138
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lfke;->g:Lhog;

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lfke;->a:Lhjb;

    iget-object v0, v0, Lhjb;->c:Lhog;

    iput-object v0, p0, Lfke;->g:Lhog;

    .line 94
    :cond_0
    iget-object v0, p0, Lfke;->g:Lhog;

    return-object v0
.end method

.method public final c()Lhog;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lfke;->h:Lhog;

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lfke;->a:Lhjb;

    iget-object v0, v0, Lhjb;->f:Lhog;

    iput-object v0, p0, Lfke;->h:Lhog;

    .line 105
    :cond_0
    iget-object v0, p0, Lfke;->h:Lhog;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lfke;->a:Lhjb;

    iget-object v0, v0, Lhjb;->g:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lfke;->f:Lfqh;

    return-object v0
.end method
