.class public final Ldmf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lfrn;

.field public b:J

.field public c:J

.field public d:J

.field public e:J

.field public f:J

.field public g:J

.field public h:J

.field public i:I

.field final synthetic j:Ldmc;

.field private k:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Ldmc;Liaa;Lfrn;)V
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 169
    iput-object p1, p0, Ldmf;->j:Ldmc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput-wide v0, p0, Ldmf;->c:J

    .line 149
    iput-wide v0, p0, Ldmf;->d:J

    .line 150
    iput-wide v0, p0, Ldmf;->e:J

    .line 153
    iput-wide v0, p0, Ldmf;->f:J

    .line 154
    iput-wide v0, p0, Ldmf;->g:J

    .line 155
    iput-wide v0, p0, Ldmf;->h:J

    .line 157
    const/16 v0, 0x8

    iput v0, p0, Ldmf;->i:I

    .line 160
    new-instance v0, Ldmg;

    invoke-direct {v0, p0}, Ldmg;-><init>(Ldmf;)V

    iput-object v0, p0, Ldmf;->k:Ljava/lang/Runnable;

    .line 170
    iput-object p3, p0, Ldmf;->a:Lfrn;

    .line 172
    if-eqz p2, :cond_0

    .line 173
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v1, p2, Liaa;->b:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Ldmf;->c:J

    .line 174
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v1, p2, Liaa;->c:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Ldmf;->d:J

    .line 175
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v1, p2, Liaa;->d:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Ldmf;->e:J

    .line 177
    :cond_0
    return-void
.end method

.method private b(I)J
    .locals 4

    .prologue
    .line 252
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 253
    iget-wide v2, p0, Ldmf;->b:J

    .line 255
    iput p1, p0, Ldmf;->i:I

    .line 256
    iput-wide v0, p0, Ldmf;->b:J

    .line 258
    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Ldmf;->b()V

    .line 224
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Ldmf;->a(I)V

    .line 225
    return-void
.end method

.method a(I)V
    .locals 4

    .prologue
    .line 228
    sget-object v0, Ldmd;->b:[I

    iget v1, p0, Ldmf;->i:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 244
    :goto_0
    return-void

    .line 231
    :pswitch_0
    iget-wide v0, p0, Ldmf;->f:J

    invoke-direct {p0, p1}, Ldmf;->b(I)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldmf;->f:J

    goto :goto_0

    .line 234
    :pswitch_1
    iget-wide v0, p0, Ldmf;->h:J

    invoke-direct {p0, p1}, Ldmf;->b(I)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldmf;->h:J

    goto :goto_0

    .line 237
    :pswitch_2
    iget-wide v0, p0, Ldmf;->g:J

    invoke-direct {p0, p1}, Ldmf;->b(I)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldmf;->g:J

    goto :goto_0

    .line 243
    :pswitch_3
    invoke-direct {p0, p1}, Ldmf;->b(I)J

    goto :goto_0

    .line 228
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method a(J)V
    .locals 5

    .prologue
    .line 297
    iget-object v0, p0, Ldmf;->j:Ldmc;

    invoke-static {v0}, Ldmc;->b(Ldmc;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Ldmf;->j:Ldmc;

    invoke-static {v0}, Ldmc;->b(Ldmc;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 300
    :cond_0
    iget-object v0, p0, Ldmf;->j:Ldmc;

    iget-object v1, p0, Ldmf;->j:Ldmc;

    invoke-static {v1}, Ldmc;->c(Ldmc;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v1

    iget-object v2, p0, Ldmf;->k:Ljava/lang/Runnable;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, p1, p2, v3}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v1

    invoke-static {v0, v1}, Ldmc;->a(Ldmc;Ljava/util/concurrent/ScheduledFuture;)Ljava/util/concurrent/ScheduledFuture;

    .line 304
    return-void
.end method

.method b()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Ldmf;->j:Ldmc;

    invoke-static {v0}, Ldmc;->b(Ldmc;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Ldmf;->j:Ldmc;

    invoke-static {v0}, Ldmc;->b(Ldmc;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 294
    :cond_0
    return-void
.end method
