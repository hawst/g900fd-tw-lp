.class public final Lcso;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfhz;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcso;->a:Landroid/content/Context;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Lhog;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 27
    :try_start_0
    iget-object v0, p1, Lhog;->n:Lhap;

    if-eqz v0, :cond_1

    new-instance v0, Lcsp;

    const-string v1, "Settings not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcsp; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :catch_0
    move-exception v0

    .line 34
    iget-object v1, p0, Lcso;->a:Landroid/content/Context;

    invoke-static {v1}, La;->n(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35
    iget-object v1, p0, Lcso;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcsp;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Leze;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    :try_start_1
    iget-object v0, p1, Lhog;->r:Lhar;

    if-eqz v0, :cond_2

    new-instance v0, Lcsp;

    const-string v1, "Artist not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p1, Lhog;->c:Lhbm;

    if-eqz v0, :cond_3

    new-instance v0, Lcsp;

    const-string v1, "Browse not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p1, Lhog;->q:Lhbz;

    if-eqz v0, :cond_4

    new-instance v0, Lcsp;

    const-string v1, "Capture not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p1, Lhog;->m:Lhca;

    if-eqz v0, :cond_5

    new-instance v0, Lcsp;

    const-string v1, "Category not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, p1, Lhog;->d:Lhfd;

    if-eqz v0, :cond_6

    new-instance v0, Lcsp;

    const-string v1, "Create Channel not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget-object v0, p1, Lhog;->e:Lhhp;

    if-eqz v0, :cond_7

    new-instance v0, Lcsp;

    const-string v1, "Channel Store not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    iget-object v0, p1, Lhog;->f:Lhja;

    if-eqz v0, :cond_8

    new-instance v0, Lcsp;

    const-string v1, "Inbox not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    iget-object v0, p1, Lhog;->o:Lhns;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lhog;->o:Lhns;

    iget-object v0, v0, Lhns;->a:Ljava/lang/String;

    iget-object v1, p0, Lcso;->a:Landroid/content/Context;

    const-string v2, "http://www.youtube.com/user/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v1, v0}, Ldol;->a(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :cond_9
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    iget-object v0, p1, Lhog;->t:Lhnt;

    if-eqz v0, :cond_b

    new-instance v0, Lcsp;

    const-string v1, "Playlist not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, p1, Lhog;->p:Lhnu;

    if-eqz v0, :cond_c

    new-instance v0, Lcsp;

    const-string v1, "Feed not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    iget-object v0, p1, Lhog;->s:Lhpa;

    if-eqz v0, :cond_d

    new-instance v0, Lcsp;

    const-string v1, "Offline not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget-object v0, p1, Lhog;->w:Lhpm;

    if-eqz v0, :cond_e

    new-instance v0, Lcsp;

    const-string v1, "Offline Watch not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    iget-object v0, p1, Lhog;->j:Lhtf;

    if-eqz v0, :cond_f

    new-instance v0, Lcsp;

    const-string v1, "Purchases not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-object v0, p1, Lhog;->g:Lhuh;

    if-eqz v0, :cond_10

    new-instance v0, Lcsp;

    const-string v1, "Search not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    iget-object v0, p1, Lhog;->l:Lhvu;

    if-eqz v0, :cond_11

    new-instance v0, Lcsp;

    const-string v1, "Sign in not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iget-object v0, p1, Lhog;->h:Lhwx;

    if-eqz v0, :cond_12

    new-instance v0, Lcsp;

    const-string v1, "Subscription Manager not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    iget-object v0, p1, Lhog;->k:Lhyn;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcso;->a:Landroid/content/Context;

    iget-object v1, p1, Lhog;->k:Lhyn;

    iget-object v1, v1, Lhyn;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Ldol;->b(Landroid/content/Context;Landroid/net/Uri;)V

    goto/16 :goto_0

    :cond_13
    iget-object v0, p1, Lhog;->i:Libf;

    if-eqz v0, :cond_14

    iget-object v0, p1, Lhog;->i:Libf;

    iget-object v0, v0, Libf;->a:Ljava/lang/String;

    iget-object v1, p0, Lcso;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Ldol;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_14
    iget-object v0, p1, Lhog;->u:Libk;

    if-eqz v0, :cond_15

    new-instance v0, Lcsp;

    const-string v1, "Watch Playlist not supported"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    new-instance v0, Lcsp;

    const-string v1, "Unknown Navigation"

    invoke-direct {v0, v1}, Lcsp;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcsp; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public final a(Lhut;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 43
    return-void
.end method
