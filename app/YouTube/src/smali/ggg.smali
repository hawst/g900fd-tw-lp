.class public final Lggg;
.super Lggh;
.source "SourceFile"


# instance fields
.field private final e:Lexd;

.field private final f:Leey;

.field private final g:I

.field private final h:F

.field private final i:I

.field private final j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lexd;Leey;IIIIFIIIZ)V
    .locals 10

    .prologue
    .line 46
    const/16 v8, 0x90

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v9, p9

    invoke-direct/range {v1 .. v9}, Lggh;-><init>(Leey;IIIIFII)V

    .line 32
    const/4 v1, 0x0

    iput-boolean v1, p0, Lggg;->k:Z

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexd;

    iput-object v1, p0, Lggg;->e:Lexd;

    .line 56
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leey;

    iput-object v1, p0, Lggg;->f:Leey;

    .line 57
    iput p3, p0, Lggg;->g:I

    .line 58
    move/from16 v0, p7

    iput v0, p0, Lggg;->h:F

    .line 59
    move/from16 v0, p10

    iput v0, p0, Lggg;->i:I

    .line 60
    move/from16 v0, p11

    iput-boolean v0, p0, Lggg;->j:Z

    .line 61
    return-void
.end method


# virtual methods
.method protected final a(J)J
    .locals 5

    .prologue
    const-wide/16 v2, -0x1

    .line 158
    cmp-long v0, p1, v2

    if-nez v0, :cond_2

    .line 159
    iget-object v0, p0, Lggg;->e:Lexd;

    invoke-interface {v0}, Lexd;->j()J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lggg;->j:Z

    if-nez v2, :cond_1

    :cond_0
    iget v0, p0, Lggg;->g:I

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_1
    iget v2, p0, Lggg;->i:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_2
    long-to-float v0, p1

    iget v1, p0, Lggg;->h:F

    mul-float/2addr v0, v1

    iget v1, p0, Lggg;->i:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    float-to-long v0, v0

    goto :goto_0
.end method

.method protected final a([Lecw;J)Lecw;
    .locals 12

    .prologue
    .line 92
    array-length v0, p1

    add-int/lit8 v1, v0, -0x1

    .line 93
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_b

    .line 94
    aget-object v2, p1, v0

    iget v2, v2, Lecw;->d:I

    iget v3, p0, Lggh;->d:I

    if-gt v2, v3, :cond_2

    .line 100
    :goto_1
    const/4 v2, 0x0

    .line 101
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-ltz v1, :cond_a

    .line 102
    aget-object v3, p1, v1

    iget v3, v3, Lecw;->d:I

    iget v4, p0, Lggh;->c:I

    if-lt v3, v4, :cond_3

    .line 108
    :goto_3
    if-le v0, v1, :cond_0

    move v1, v0

    .line 112
    :cond_0
    invoke-virtual {p0, p2, p3}, Lggg;->a(J)J

    move-result-wide v4

    move v3, v0

    .line 113
    :goto_4
    if-gt v3, v1, :cond_9

    .line 114
    aget-object v0, p1, v3

    .line 115
    iget v2, p0, Lggh;->a:I

    iget v6, p0, Lggh;->b:I

    const/4 v7, -0x1

    if-eq v2, v7, :cond_1

    const/4 v7, -0x1

    if-ne v6, v7, :cond_4

    :cond_1
    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_7

    iget v2, v0, Lecw;->e:I

    int-to-long v6, v2

    cmp-long v2, v6, v4

    if-gtz v2, :cond_7

    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_8

    .line 120
    :goto_7
    return-object v0

    .line 93
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 115
    :cond_4
    iget v7, v0, Lecw;->c:I

    int-to-double v8, v7

    const-wide v10, 0x3feb333333333333L    # 0.85

    mul-double/2addr v8, v10

    int-to-double v10, v2

    cmpl-double v2, v8, v10

    if-lez v2, :cond_5

    iget v2, v0, Lecw;->d:I

    int-to-double v8, v2

    const-wide v10, 0x3feb333333333333L    # 0.85

    mul-double/2addr v8, v10

    int-to-double v6, v6

    cmpl-double v2, v8, v6

    if-gtz v2, :cond_6

    :cond_5
    const/4 v2, 0x1

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    goto :goto_6

    .line 113
    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 120
    :cond_9
    aget-object v0, p1, v1

    goto :goto_7

    :cond_a
    move v1, v2

    goto :goto_3

    :cond_b
    move v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/util/List;J[Lecw;Leda;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 69
    iget v0, p0, Lggh;->c:I

    iget v2, p0, Lggh;->d:I

    if-ne v0, v2, :cond_4

    .line 71
    iget-object v0, p0, Lggg;->f:Leey;

    invoke-interface {v0}, Leey;->a()J

    move-result-wide v2

    invoke-virtual {p0, p4, v2, v3}, Lggg;->a([Lecw;J)Lecw;

    move-result-object v0

    iput-object v0, p5, Leda;->c:Lecw;

    .line 72
    iget-boolean v0, p0, Lggg;->k:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput v0, p5, Leda;->b:I

    move v2, v1

    .line 73
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 74
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    .line 75
    iget-object v0, v0, Ledc;->a:Lecw;

    iget v0, v0, Lecw;->d:I

    iget-object v3, p5, Leda;->c:Lecw;

    iget v3, v3, Lecw;->d:I

    if-eq v0, v3, :cond_3

    .line 77
    iput v2, p5, Leda;->a:I

    .line 84
    :cond_0
    :goto_2
    iget-boolean v0, p0, Lggg;->k:Z

    if-nez v0, :cond_1

    .line 85
    iput-boolean v1, p0, Lggg;->k:Z

    .line 87
    :cond_1
    return-void

    .line 72
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 73
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 82
    :cond_4
    invoke-super/range {p0 .. p5}, Lggh;->a(Ljava/util/List;J[Lecw;Leda;)V

    goto :goto_2
.end method
