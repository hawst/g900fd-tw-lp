.class public Lmu;
.super Lkm;
.source "SourceFile"

# interfaces
.implements Lpa;


# static fields
.field private static final e:Z


# instance fields
.field private A:Lhv;

.field private B:Lhx;

.field a:Lmy;

.field b:Lty;

.field c:Ltz;

.field d:Z

.field private f:Landroid/content/Context;

.field private g:Landroid/content/Context;

.field private h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

.field private i:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private j:Lpv;

.field private k:Landroid/support/v7/internal/widget/ActionBarContextView;

.field private l:Landroid/support/v7/internal/widget/ActionBarContainer;

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Z

.field private p:Ljava/util/ArrayList;

.field private q:I

.field private r:Z

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Lnh;

.field private y:Z

.field private z:Lhv;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    const-class v0, Lmu;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    .line 82
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lmu;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lkp;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    invoke-direct {p0}, Lkm;-><init>()V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmu;->p:Ljava/util/ArrayList;

    .line 120
    iput v2, p0, Lmu;->s:I

    .line 122
    iput-boolean v1, p0, Lmu;->t:Z

    .line 127
    iput-boolean v1, p0, Lmu;->w:Z

    .line 135
    new-instance v0, Lmv;

    invoke-direct {v0, p0}, Lmv;-><init>(Lmu;)V

    iput-object v0, p0, Lmu;->z:Lhv;

    .line 155
    new-instance v0, Lmw;

    invoke-direct {v0, p0}, Lmw;-><init>(Lmu;)V

    iput-object v0, p0, Lmu;->A:Lhv;

    .line 163
    new-instance v0, Lmx;

    invoke-direct {v0, p0}, Lmx;-><init>(Lmu;)V

    iput-object v0, p0, Lmu;->B:Lhx;

    .line 173
    invoke-virtual {p1}, Lkp;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 175
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    .line 176
    const v0, 0x7f080093

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iput-object p0, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e:Lpa;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->e:Lpa;

    iget v5, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a:I

    invoke-interface {v3, v5}, Lpa;->d(I)V

    iget v3, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->d:I

    if-eqz v3, :cond_0

    iget v3, v0, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->d:I

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->onWindowSystemUiVisibilityChanged(I)V

    invoke-static {v0}, Lfz;->k(Landroid/view/View;)V

    :cond_0
    const v0, 0x7f080095

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v3, v0, Lpv;

    if-eqz v3, :cond_2

    check-cast v0, Lpv;

    :goto_0
    iput-object v0, p0, Lmu;->j:Lpv;

    const v0, 0x7f080096

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContextView;

    iput-object v0, p0, Lmu;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    const v0, 0x7f080094

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    const v0, 0x7f08000f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ActionBarContainer;

    iput-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v0, p0, Lmu;->j:Lpv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmu;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-nez v0, :cond_4

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " can only be used with a compatible window decor layout"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    instance-of v3, v0, Landroid/support/v7/widget/Toolbar;

    if-eqz v3, :cond_3

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->j()Lpv;

    move-result-object v0

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t make a decor toolbar out of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0}, Lpv;->b()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lmu;->f:Landroid/content/Context;

    iget-object v0, p0, Lmu;->j:Lpv;

    iput v2, p0, Lmu;->q:I

    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0}, Lpv;->l()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    move v3, v1

    :goto_1
    if-eqz v3, :cond_5

    iput-boolean v1, p0, Lmu;->n:Z

    :cond_5
    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    invoke-static {v0}, Lna;->a(Landroid/content/Context;)Lna;

    move-result-object v5

    iget-object v0, v5, Lna;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    const/16 v6, 0xe

    if-ge v0, v6, :cond_8

    move v0, v1

    :goto_2
    if-nez v0, :cond_6

    if-eqz v3, :cond_6

    :cond_6
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-virtual {v5}, Lna;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lmu;->k(Z)V

    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    const/4 v3, 0x0

    sget-object v5, Lmd;->a:[I

    const v6, 0x7f010053

    invoke-virtual {v0, v3, v5, v6, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/16 v3, 0x14

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-boolean v3, v3, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->b:Z

    if-nez v3, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v3, v2

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    iput-boolean v1, p0, Lmu;->d:Z

    iget-object v3, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v3, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Z)V

    :cond_a
    const/16 v1, 0x19

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    if-eqz v1, :cond_b

    int-to-float v1, v1

    iget-object v2, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2, v1}, Lfz;->e(Landroid/view/View;F)V

    iget-object v2, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2, v1}, Lfz;->e(Landroid/view/View;F)V

    :cond_b
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 177
    if-nez p2, :cond_c

    .line 178
    const v0, 0x1020002

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lmu;->m:Landroid/view/View;

    .line 180
    :cond_c
    return-void
.end method

.method static synthetic a(Lmu;Lnh;)Lnh;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lmu;->x:Lnh;

    return-object v0
.end method

.method static synthetic a(Lmu;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lmu;->t:Z

    return v0
.end method

.method static synthetic a(ZZZ)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lmu;->b(ZZZ)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lmu;)Landroid/view/View;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lmu;->m:Landroid/view/View;

    return-object v0
.end method

.method private static b(ZZZ)Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 738
    if-eqz p2, :cond_1

    .line 743
    :cond_0
    :goto_0
    return v0

    .line 740
    :cond_1
    if-nez p0, :cond_2

    if-eqz p1, :cond_0

    .line 741
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lmu;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic d(Lmu;)Landroid/support/v7/internal/widget/ActionBarContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    return-object v0
.end method

.method static synthetic e(Lmu;)I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lmu;->q:I

    return v0
.end method

.method static synthetic f(Lmu;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    return-object v0
.end method

.method static synthetic g(Lmu;)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic h(Lmu;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lmu;->u:Z

    return v0
.end method

.method static synthetic i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lmu;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    return-object v0
.end method

.method static synthetic j(Lmu;)Lpv;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lmu;->j:Lpv;

    return-object v0
.end method

.method static synthetic k(Lmu;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    return-object v0
.end method

.method private k(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 271
    iput-boolean p1, p0, Lmu;->r:Z

    .line 273
    iget-boolean v0, p0, Lmu;->r:Z

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, v3}, Lpv;->a(Lqd;)V

    .line 275
    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Lqd;)V

    .line 280
    :goto_0
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0}, Lpv;->m()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 281
    :goto_1
    iget-object v4, p0, Lmu;->j:Lpv;

    iget-boolean v3, p0, Lmu;->r:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_2

    move v3, v1

    :goto_2
    invoke-interface {v4, v3}, Lpv;->a(Z)V

    .line 292
    iget-object v3, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    iget-boolean v4, p0, Lmu;->r:Z

    if-nez v4, :cond_3

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, v3, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->c:Z

    .line 293
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Lqd;)V

    .line 278
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, v3}, Lpv;->a(Lqd;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 280
    goto :goto_1

    :cond_2
    move v3, v2

    .line 281
    goto :goto_2

    :cond_3
    move v1, v2

    .line 292
    goto :goto_3
.end method

.method private l(Z)V
    .locals 8

    .prologue
    const/4 v2, 0x2

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 749
    iget-boolean v0, p0, Lmu;->u:Z

    iget-boolean v1, p0, Lmu;->v:Z

    invoke-static {v6, v0, v1}, Lmu;->b(ZZZ)Z

    move-result v0

    .line 752
    if-eqz v0, :cond_9

    .line 753
    iget-boolean v0, p0, Lmu;->w:Z

    if-nez v0, :cond_5

    .line 754
    iput-boolean v5, p0, Lmu;->w:Z

    .line 755
    iget-object v0, p0, Lmu;->x:Lnh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmu;->x:Lnh;

    invoke-virtual {v0}, Lnh;->b()V

    :cond_0
    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    iget v0, p0, Lmu;->s:I

    if-nez v0, :cond_6

    sget-boolean v0, Lmu;->e:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lmu;->y:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_6

    :cond_1
    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lfz;->b(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    if-eqz p1, :cond_2

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    iget-object v2, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    aget v1, v1, v5

    int-to-float v1, v1

    sub-float/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v1, v0}, Lfz;->b(Landroid/view/View;F)V

    new-instance v1, Lnh;

    invoke-direct {v1}, Lnh;-><init>()V

    iget-object v2, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v2

    invoke-virtual {v2, v4}, Lhj;->c(F)Lhj;

    move-result-object v2

    iget-object v3, p0, Lmu;->B:Lhx;

    invoke-virtual {v2, v3}, Lhj;->a(Lhx;)Lhj;

    invoke-virtual {v1, v2}, Lnh;->a(Lhj;)Lnh;

    iget-boolean v2, p0, Lmu;->t:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmu;->m:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmu;->m:Landroid/view/View;

    invoke-static {v2, v0}, Lfz;->b(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->m:Landroid/view/View;

    invoke-static {v0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v0

    invoke-virtual {v0, v4}, Lhj;->c(F)Lhj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lnh;->a(Lhj;)Lnh;

    :cond_3
    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_4

    iget v0, p0, Lmu;->q:I

    if-ne v0, v5, :cond_4

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v2, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v0, v2}, Lfz;->b(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v0

    invoke-virtual {v0, v4}, Lhj;->c(F)Lhj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lnh;->a(Lhj;)Lnh;

    :cond_4
    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    const v2, 0x10a0006

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, Lnh;->a(Landroid/view/animation/Interpolator;)Lnh;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Lnh;->a(J)Lnh;

    iget-object v0, p0, Lmu;->A:Lhv;

    invoke-virtual {v1, v0}, Lnh;->a(Lhv;)Lnh;

    iput-object v1, p0, Lmu;->x:Lnh;

    invoke-virtual {v1}, Lnh;->a()V

    :goto_0
    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v0}, Lfz;->k(Landroid/view/View;)V

    .line 763
    :cond_5
    :goto_1
    return-void

    .line 755
    :cond_6
    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Lfz;->c(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lfz;->b(Landroid/view/View;F)V

    iget-boolean v0, p0, Lmu;->t:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmu;->m:Landroid/view/View;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lmu;->m:Landroid/view/View;

    invoke-static {v0, v4}, Lfz;->b(Landroid/view/View;F)V

    :cond_7
    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_8

    iget v0, p0, Lmu;->q:I

    if-ne v0, v5, :cond_8

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Lfz;->c(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v4}, Lfz;->b(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v6}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    :cond_8
    iget-object v0, p0, Lmu;->A:Lhv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lhv;->b(Landroid/view/View;)V

    goto :goto_0

    .line 758
    :cond_9
    iget-boolean v0, p0, Lmu;->w:Z

    if-eqz v0, :cond_5

    .line 759
    iput-boolean v6, p0, Lmu;->w:Z

    .line 760
    iget-object v0, p0, Lmu;->x:Lnh;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lmu;->x:Lnh;

    invoke-virtual {v0}, Lnh;->b()V

    :cond_a
    iget v0, p0, Lmu;->s:I

    if-nez v0, :cond_f

    sget-boolean v0, Lmu;->e:Z

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lmu;->y:Z

    if-nez v0, :cond_b

    if-eqz p1, :cond_f

    :cond_b
    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Lfz;->c(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0, v5}, Landroid/support/v7/internal/widget/ActionBarContainer;->a(Z)V

    new-instance v1, Lnh;

    invoke-direct {v1}, Lnh;-><init>()V

    iget-object v0, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    if-eqz p1, :cond_c

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    iget-object v3, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v3, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getLocationInWindow([I)V

    aget v2, v2, v5

    int-to-float v2, v2

    sub-float/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v2}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v2

    invoke-virtual {v2, v0}, Lhj;->c(F)Lhj;

    move-result-object v2

    iget-object v3, p0, Lmu;->B:Lhx;

    invoke-virtual {v2, v3}, Lhj;->a(Lhx;)Lhj;

    invoke-virtual {v1, v2}, Lnh;->a(Lhj;)Lnh;

    iget-boolean v2, p0, Lmu;->t:Z

    if-eqz v2, :cond_d

    iget-object v2, p0, Lmu;->m:Landroid/view/View;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lmu;->m:Landroid/view/View;

    invoke-static {v2}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v2

    invoke-virtual {v2, v0}, Lhj;->c(F)Lhj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lnh;->a(Lhj;)Lnh;

    :cond_d
    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0, v7}, Lfz;->c(Landroid/view/View;F)V

    iget-object v0, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-static {v0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v0

    iget-object v2, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Lhj;->c(F)Lhj;

    move-result-object v0

    invoke-virtual {v1, v0}, Lnh;->a(Lhj;)Lnh;

    :cond_e
    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    invoke-virtual {v1, v0}, Lnh;->a(Landroid/view/animation/Interpolator;)Lnh;

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Lnh;->a(J)Lnh;

    iget-object v0, p0, Lmu;->z:Lhv;

    invoke-virtual {v1, v0}, Lnh;->a(Lhv;)Lnh;

    iput-object v1, p0, Lmu;->x:Lnh;

    invoke-virtual {v1}, Lnh;->a()V

    goto/16 :goto_1

    :cond_f
    iget-object v0, p0, Lmu;->z:Lhv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lhv;->b(Landroid/view/View;)V

    goto/16 :goto_1

    .line 755
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 760
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0}, Lpv;->l()I

    move-result v0

    return v0
.end method

.method public final a(Ltz;)Lty;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 503
    iget-object v0, p0, Lmu;->a:Lmy;

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lmu;->a:Lmy;

    invoke-virtual {v0}, Lmy;->c()V

    .line 507
    :cond_0
    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Z)V

    .line 508
    iget-object v0, p0, Lmu;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()V

    .line 509
    new-instance v0, Lmy;

    invoke-direct {v0, p0, p1}, Lmy;-><init>(Lmu;Ltz;)V

    .line 510
    invoke-virtual {v0}, Lmy;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 511
    invoke-virtual {v0}, Lmy;->d()V

    .line 512
    iget-object v1, p0, Lmu;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v1, v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Lty;)V

    .line 513
    invoke-virtual {p0, v3}, Lmu;->j(Z)V

    .line 514
    iget-object v1, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    if-eqz v1, :cond_1

    iget v1, p0, Lmu;->q:I

    if-ne v1, v3, :cond_1

    .line 516
    iget-object v1, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContainer;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 517
    iget-object v1, p0, Lmu;->l:Landroid/support/v7/internal/widget/ActionBarContainer;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->setVisibility(I)V

    .line 518
    iget-object v1, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v1, :cond_1

    .line 519
    iget-object v1, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {v1}, Lfz;->k(Landroid/view/View;)V

    .line 523
    :cond_1
    iget-object v1, p0, Lmu;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->sendAccessibilityEvent(I)V

    .line 524
    iput-object v0, p0, Lmu;->a:Lmy;

    .line 527
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1314
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, p1}, Lpv;->a(I)V

    .line 1315
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 461
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0}, Lpv;->l()I

    move-result v0

    .line 462
    and-int/lit8 v1, p2, 0x4

    if-eqz v1, :cond_0

    .line 463
    const/4 v1, 0x1

    iput-boolean v1, p0, Lmu;->n:Z

    .line 465
    :cond_0
    iget-object v1, p0, Lmu;->j:Lpv;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Lpv;->b(I)V

    .line 466
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    invoke-static {v0}, Lna;->a(Landroid/content/Context;)Lna;

    move-result-object v0

    invoke-virtual {v0}, Lna;->a()Z

    move-result v0

    invoke-direct {p0, v0}, Lmu;->k(Z)V

    .line 268
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 469
    iget-object v2, p0, Lmu;->i:Landroid/support/v7/internal/widget/ActionBarContainer;

    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/support/v7/internal/widget/ActionBarContainer;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-object p1, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_1

    invoke-virtual {p1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->b:Landroid/view/View;

    if-eqz v3, :cond_1

    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    iget-object v4, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget-object v5, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->b:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    iget-object v6, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->b:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v6

    iget-object v7, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->b:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_1
    iget-boolean v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->f:Z

    if-eqz v3, :cond_4

    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->e:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_3

    :cond_2
    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v7/internal/widget/ActionBarContainer;->setWillNotDraw(Z)V

    invoke-virtual {v2}, Landroid/support/v7/internal/widget/ActionBarContainer;->invalidate()V

    .line 470
    return-void

    :cond_3
    move v0, v1

    .line 469
    goto :goto_0

    :cond_4
    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->c:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_5

    iget-object v3, v2, Landroid/support/v7/internal/widget/ActionBarContainer;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Lkn;)V
    .locals 1

    .prologue
    .line 1238
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1239
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, p1}, Lpv;->a(Landroid/view/View;)V

    .line 1240
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, p1}, Lpv;->b(Ljava/lang/CharSequence;)V

    .line 442
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 373
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lmu;->a(II)V

    .line 374
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 4

    .prologue
    .line 882
    iget-object v0, p0, Lmu;->g:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 883
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 884
    iget-object v1, p0, Lmu;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    .line 885
    const v2, 0x7f010056

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 886
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    .line 888
    if-eqz v0, :cond_1

    .line 889
    new-instance v1, Landroid/view/ContextThemeWrapper;

    iget-object v2, p0, Lmu;->f:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lmu;->g:Landroid/content/Context;

    .line 894
    :cond_0
    :goto_0
    iget-object v0, p0, Lmu;->g:Landroid/content/Context;

    return-object v0

    .line 891
    :cond_1
    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    iput-object v0, p0, Lmu;->g:Landroid/content/Context;

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lmu;->f:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmu;->a(Ljava/lang/CharSequence;)V

    .line 404
    return-void
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, p1}, Lpv;->a(Landroid/graphics/drawable/Drawable;)V

    .line 905
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, p1}, Lpv;->a(Ljava/lang/CharSequence;)V

    .line 447
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 378
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lmu;->a(II)V

    .line 379
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0, p1}, Lpv;->d(I)V

    .line 920
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 383
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lmu;->a(II)V

    .line 384
    return-void

    .line 383
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 328
    iput p1, p0, Lmu;->s:I

    .line 329
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 388
    const/4 v0, 0x0

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lmu;->a(II)V

    .line 389
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lmu;->j:Lpv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0}, Lpv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, Lmu;->j:Lpv;

    invoke-interface {v0}, Lpv;->d()V

    .line 938
    const/4 v0, 0x1

    .line 940
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 676
    iget-boolean v0, p0, Lmu;->u:Z

    if-eqz v0, :cond_0

    .line 677
    const/4 v0, 0x0

    iput-boolean v0, p0, Lmu;->u:Z

    .line 678
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lmu;->l(Z)V

    .line 680
    :cond_0
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lmu;->j:Lpv;

    .line 399
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 701
    iget-boolean v0, p0, Lmu;->u:Z

    if-nez v0, :cond_0

    .line 702
    iput-boolean v1, p0, Lmu;->u:Z

    .line 703
    invoke-direct {p0, v1}, Lmu;->l(Z)V

    .line 705
    :cond_0
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 1341
    iget-boolean v0, p0, Lmu;->n:Z

    if-nez v0, :cond_0

    .line 1342
    invoke-virtual {p0, p1}, Lmu;->c(Z)V

    .line 1344
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lmu;->x:Lnh;

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lmu;->x:Lnh;

    invoke-virtual {v0}, Lnh;->b()V

    .line 926
    const/4 v0, 0x0

    iput-object v0, p0, Lmu;->x:Lnh;

    .line 928
    :cond_0
    return-void
.end method

.method public final g(Z)V
    .locals 1

    .prologue
    .line 339
    iput-boolean p1, p0, Lmu;->y:Z

    .line 340
    if-nez p1, :cond_0

    iget-object v0, p0, Lmu;->x:Lnh;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lmu;->x:Lnh;

    invoke-virtual {v0}, Lnh;->b()V

    .line 343
    :cond_0
    return-void
.end method

.method public final h(Z)V
    .locals 3

    .prologue
    .line 354
    iget-boolean v0, p0, Lmu;->o:Z

    if-ne p1, v0, :cond_1

    .line 363
    :cond_0
    return-void

    .line 357
    :cond_1
    iput-boolean p1, p0, Lmu;->o:Z

    .line 359
    iget-object v0, p0, Lmu;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 360
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 361
    iget-object v2, p0, Lmu;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 360
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 654
    iput-boolean p1, p0, Lmu;->t:Z

    .line 655
    return-void
.end method

.method public final j(Z)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 870
    if-eqz p1, :cond_2

    .line 871
    iget-boolean v0, p0, Lmu;->v:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lmu;->v:Z

    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a()V

    :cond_0
    invoke-direct {p0, v2}, Lmu;->l(Z)V

    .line 876
    :cond_1
    :goto_0
    iget-object v3, p0, Lmu;->j:Lpv;

    if-eqz p1, :cond_4

    move v0, v1

    :goto_1
    invoke-interface {v3, v0}, Lpv;->c(I)V

    .line 877
    iget-object v0, p0, Lmu;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz p1, :cond_5

    :goto_2
    invoke-virtual {v0, v2}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(I)V

    .line 879
    return-void

    .line 873
    :cond_2
    iget-boolean v0, p0, Lmu;->v:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lmu;->v:Z

    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmu;->h:Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    invoke-static {}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a()V

    :cond_3
    invoke-direct {p0, v2}, Lmu;->l(Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 876
    goto :goto_1

    :cond_5
    move v2, v1

    .line 877
    goto :goto_2
.end method
