.class public final Ldbe;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lexd;

.field public final b:Ljava/util/List;

.field public final c:Ldbk;

.field public d:Ljava/lang/String;

.field private final e:Landroid/os/Handler;

.field private final f:Lfxe;

.field private final g:Leyp;

.field private final h:Lgix;

.field private final i:Lgng;

.field private final j:Ldbj;

.field private k:Leue;

.field private l:Leue;

.field private m:Z

.field private n:Ldbi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lfxe;Leyp;Lgix;Lgng;Lexd;Ljava/lang/String;Ljava/lang/Class;Ldbj;ILdba;Ldbb;)V
    .locals 15

    .prologue
    .line 140
    const/4 v14, 0x0

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Ldbe;-><init>(Landroid/content/Context;Landroid/os/Handler;Lfxe;Leyp;Lgix;Lgng;Lexd;Ljava/lang/String;Ljava/lang/Class;Ldbj;ILdba;Ldbb;B)V

    .line 154
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lfxe;Leyp;Lgix;Lgng;Lexd;Ljava/lang/String;Ljava/lang/Class;Ldbj;ILdba;Ldbb;B)V
    .locals 10

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    iput-object v2, p0, Ldbe;->e:Landroid/os/Handler;

    .line 180
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfxe;

    iput-object v2, p0, Ldbe;->f:Lfxe;

    .line 181
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Leyp;

    iput-object v2, p0, Ldbe;->g:Leyp;

    .line 182
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgix;

    iput-object v2, p0, Ldbe;->h:Lgix;

    .line 183
    invoke-static/range {p6 .. p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgng;

    iput-object v2, p0, Ldbe;->i:Lgng;

    .line 184
    move-object/from16 v0, p7

    iput-object v0, p0, Ldbe;->a:Lexd;

    .line 185
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldbj;

    iput-object v2, p0, Ldbe;->j:Ldbj;

    .line 186
    new-instance v2, Ldbk;

    invoke-direct {v2}, Ldbk;-><init>()V

    iput-object v2, p0, Ldbe;->c:Ldbk;

    .line 187
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 188
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Ldbe;->b:Ljava/util/List;

    .line 189
    iget-object v9, p0, Ldbe;->b:Ljava/util/List;

    new-instance v2, Ldbc;

    move-object v3, p1

    move-object/from16 v4, p8

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move-object/from16 v7, p13

    move/from16 v8, p11

    invoke-direct/range {v2 .. v8}, Ldbc;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ldbj;Ldbb;I)V

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    new-instance v2, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;

    move-object/from16 v0, p10

    move-object/from16 v1, p12

    invoke-direct {v2, p1, v0, v1}, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;-><init>(Landroid/content/Context;Ldbj;Ldba;)V

    iput-object v2, p0, Ldbe;->n:Ldbi;

    .line 201
    iget-object v2, p0, Ldbe;->b:Ljava/util/List;

    iget-object v3, p0, Ldbe;->n:Ldbi;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    return-void
.end method

.method static synthetic a(Ldbe;Lgcd;)V
    .locals 7

    .prologue
    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lgcd;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v6, p1, Lgcd;->g:Landroid/net/Uri;

    :goto_0
    iget-object v2, p1, Lgcd;->b:Ljava/lang/String;

    iget-object v3, p1, Lgcd;->j:Ljava/lang/String;

    iget v0, p1, Lgcd;->k:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Ldbe;->a(Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;)V

    return-void

    :cond_0
    iget-object v6, p1, Lgcd;->f:Landroid/net/Uri;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbe;->m:Z

    .line 206
    invoke-virtual {p0}, Ldbe;->c()V

    .line 207
    return-void
.end method

.method public final a(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Ldbe;->n:Ldbi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldbe;->n:Ldbi;

    instance-of v0, v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Ldbe;->n:Ldbi;

    check-cast v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;

    iput-object p1, v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->d:Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;

    iget-object v0, v0, Lcom/google/android/apps/youtube/core/player/notification/ExternalPlaybackControllerV14;->c:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, p1}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 314
    :cond_0
    return-void
.end method

.method public final a(Ldbl;)V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Ldbe;->c:Ldbk;

    iput-object p1, v0, Ldbk;->b:Ldbl;

    .line 362
    invoke-virtual {p0}, Ldbe;->c()V

    .line 363
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 231
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    iget-object v0, p0, Ldbe;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    iput-object p1, p0, Ldbe;->d:Ljava/lang/String;

    .line 234
    invoke-virtual {p0}, Ldbe;->d()V

    .line 235
    iget-object v0, p0, Ldbe;->a:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    new-instance v0, Ldbg;

    invoke-direct {v0, p0}, Ldbg;-><init>(Ldbe;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldbe;->k:Leue;

    .line 237
    iget-object v0, p0, Ldbe;->f:Lfxe;

    iget-object v1, p0, Ldbe;->e:Landroid/os/Handler;

    iget-object v2, p0, Ldbe;->k:Leue;

    .line 239
    invoke-static {v1, v2}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v1

    .line 237
    invoke-interface {v0, p1, v1}, Lfxe;->d(Ljava/lang/String;Leuc;)V

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    invoke-virtual {p0, p1}, Ldbe;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;)V
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Ldbe;->c:Ldbk;

    iput-object p2, v0, Ldbk;->a:Ljava/lang/String;

    .line 277
    iget-object v0, p0, Ldbe;->c:Ldbk;

    .line 278
    iget-object v0, p0, Ldbe;->c:Ldbk;

    iput-wide p3, v0, Ldbk;->f:J

    .line 279
    invoke-virtual {p0}, Ldbe;->c()V

    .line 280
    iget-object v0, p0, Ldbe;->j:Ldbj;

    invoke-interface {v0}, Ldbj;->a()V

    .line 281
    new-instance v0, Ldbh;

    invoke-direct {v0, p0}, Ldbh;-><init>(Ldbe;)V

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldbe;->l:Leue;

    .line 282
    if-eqz p5, :cond_0

    .line 283
    iget-object v0, p0, Ldbe;->g:Leyp;

    iget-object v1, p0, Ldbe;->e:Landroid/os/Handler;

    iget-object v2, p0, Ldbe;->l:Leue;

    .line 285
    invoke-static {v1, v2}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v1

    .line 283
    invoke-interface {v0, p5, v1}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 287
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Ldbe;->n:Ldbi;

    if-eqz v0, :cond_0

    .line 296
    if-eqz p1, :cond_1

    .line 297
    iget-object v0, p0, Ldbe;->b:Ljava/util/List;

    iget-object v1, p0, Ldbe;->n:Ldbi;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    iget-object v0, p0, Ldbe;->b:Ljava/util/List;

    iget-object v1, p0, Ldbe;->n:Ldbi;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    iget-object v0, p0, Ldbe;->b:Ljava/util/List;

    iget-object v1, p0, Ldbe;->n:Ldbi;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Ldbe;->c:Ldbk;

    iput-boolean p1, v0, Ldbk;->c:Z

    .line 367
    iget-object v0, p0, Ldbe;->c:Ldbk;

    iput-boolean p2, v0, Ldbk;->d:Z

    .line 368
    invoke-virtual {p0}, Ldbe;->c()V

    .line 369
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbe;->m:Z

    .line 211
    iget-object v0, p0, Ldbe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbi;

    .line 212
    invoke-interface {v0}, Ldbi;->a()V

    goto :goto_0

    .line 214
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 326
    iget-object v0, p0, Ldbe;->h:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    iget-object v0, p0, Ldbe;->i:Lgng;

    iget-object v1, p0, Ldbe;->h:Lgix;

    .line 332
    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    .line 334
    invoke-interface {v0, p1}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v1

    .line 335
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lgmb;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336
    iget-object v1, p0, Ldbe;->e:Landroid/os/Handler;

    new-instance v2, Ldbf;

    invoke-direct {v2, p0, p1}, Ldbf;-><init>(Ldbe;Ljava/lang/String;)V

    .line 337
    invoke-static {v1, v2}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v1

    .line 336
    invoke-interface {v0, p1, v1}, Lgnd;->b(Ljava/lang/String;Leuc;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Ldbe;->c:Ldbk;

    iput-boolean p1, v0, Ldbk;->g:Z

    .line 373
    invoke-virtual {p0}, Ldbe;->c()V

    .line 374
    return-void
.end method

.method c()V
    .locals 3

    .prologue
    .line 377
    iget-boolean v0, p0, Ldbe;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldbe;->c:Ldbk;

    iget-object v0, v0, Ldbk;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 378
    iget-object v0, p0, Ldbe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbi;

    .line 379
    iget-object v2, p0, Ldbe;->c:Ldbk;

    invoke-interface {v0, v2}, Ldbi;->a(Ldbk;)V

    goto :goto_1

    .line 377
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 382
    :cond_1
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 385
    iget-object v0, p0, Ldbe;->c:Ldbk;

    invoke-virtual {v0}, Ldbk;->a()V

    .line 386
    iget-object v0, p0, Ldbe;->l:Leue;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Ldbe;->l:Leue;

    iput-boolean v1, v0, Leue;->a:Z

    .line 389
    :cond_0
    iget-object v0, p0, Ldbe;->k:Leue;

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Ldbe;->k:Leue;

    iput-boolean v1, v0, Leue;->a:Z

    .line 392
    :cond_1
    return-void
.end method
