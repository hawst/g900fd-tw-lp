.class public final Lfig;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lgyh;

.field private b:Lfif;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lgyh;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lfig;->a:Lgyh;

    .line 29
    return-void
.end method

.method private static a(Lgxw;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 100
    iget-object v1, p0, Lgxw;->a:[Lgxy;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 101
    iget-object v4, v3, Lgxy;->b:Lgxt;

    if-eqz v4, :cond_0

    .line 102
    new-instance v4, Lfid;

    iget-object v3, v3, Lgxy;->b:Lgxt;

    const/4 v5, 0x0

    invoke-direct {v4, v3, v5}, Lfid;-><init>(Lgxt;Lfqh;)V

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_1
    return-void
.end method

.method private a(Lgya;Ljava/util/List;)V
    .locals 5

    .prologue
    .line 89
    iget-object v1, p1, Lgya;->a:[Lgyc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 90
    iget-object v4, v3, Lgyc;->b:Lgxw;

    if-eqz v4, :cond_0

    .line 91
    iget-object v3, v3, Lgyc;->b:Lgxw;

    invoke-static {v3, p2}, Lfig;->a(Lgxw;Ljava/util/List;)V

    .line 89
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_1
    return-void
.end method

.method private a(Lhul;Ljava/util/List;)V
    .locals 5

    .prologue
    .line 76
    iget-object v1, p1, Lhul;->a:[Lhun;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 77
    iget-object v4, v3, Lhun;->h:Lgxw;

    if-eqz v4, :cond_0

    .line 78
    iget-object v3, v3, Lhun;->h:Lgxw;

    invoke-static {v3, p2}, Lfig;->a(Lgxw;Ljava/util/List;)V

    .line 76
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 35
    iget-object v0, p0, Lfig;->c:Ljava/util/List;

    if-nez v0, :cond_3

    .line 36
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 37
    iget-object v0, p0, Lfig;->a:Lgyh;

    iget-object v0, v0, Lgyh;->a:[Lgyi;

    if-eqz v0, :cond_2

    .line 39
    iget-object v0, p0, Lfig;->a:Lgyh;

    iget-object v2, v0, Lgyh;->a:[Lgyi;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 40
    iget-object v5, v4, Lgyi;->c:Lgya;

    if-eqz v5, :cond_1

    .line 41
    iget-object v4, v4, Lgyi;->c:Lgya;

    invoke-direct {p0, v4, v1}, Lfig;->a(Lgya;Ljava/util/List;)V

    .line 39
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_1
    iget-object v5, v4, Lgyi;->b:Lhul;

    if-eqz v5, :cond_0

    .line 44
    iget-object v4, v4, Lgyi;->b:Lhul;

    invoke-direct {p0, v4, v1}, Lfig;->a(Lhul;Ljava/util/List;)V

    goto :goto_1

    .line 48
    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfig;->c:Ljava/util/List;

    .line 51
    :cond_3
    iget-object v0, p0, Lfig;->c:Ljava/util/List;

    return-object v0
.end method

.method public final b()Lfif;
    .locals 5

    .prologue
    .line 55
    iget-object v0, p0, Lfig;->b:Lfif;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfig;->a:Lgyh;

    iget-object v0, v0, Lgyh;->a:[Lgyi;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lfig;->a:Lgyh;

    iget-object v1, v0, Lgyh;->a:[Lgyi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 58
    iget-object v4, v3, Lgyi;->c:Lgya;

    if-eqz v4, :cond_1

    .line 59
    new-instance v0, Lfif;

    iget-object v1, v3, Lgyi;->c:Lgya;

    invoke-direct {v0, v1}, Lfif;-><init>(Lgya;)V

    iput-object v0, p0, Lfig;->b:Lfif;

    .line 69
    :cond_0
    :goto_1
    iget-object v0, p0, Lfig;->b:Lfif;

    return-object v0

    .line 62
    :cond_1
    iget-object v4, v3, Lgyi;->b:Lhul;

    if-eqz v4, :cond_2

    .line 63
    new-instance v0, Lfif;

    iget-object v1, v3, Lgyi;->b:Lhul;

    invoke-direct {v0, v1}, Lfif;-><init>(Lhul;)V

    iput-object v0, p0, Lfig;->b:Lfif;

    goto :goto_1

    .line 57
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
