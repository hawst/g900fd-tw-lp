.class public Lfno;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Libb;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Lfnv;

.field public e:Ljava/lang/CharSequence;

.field private final f:Lfqh;

.field private g:Lfnq;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/util/List;

.field private j:Ljava/lang/CharSequence;

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;


# direct methods
.method public constructor <init>(Libb;Lfqh;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libb;

    iput-object v0, p0, Lfno;->a:Libb;

    .line 42
    iput-object p2, p0, Lfno;->f:Lfqh;

    .line 43
    return-void
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    iget-object v0, v0, Liaw;->c:Libe;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    iget-object v0, v0, Liaw;->b:Liar;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lfnq;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lfno;->g:Lfnq;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->c:Lias;

    iget-object v0, v0, Lias;->a:Liat;

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Lfnq;

    iget-object v1, p0, Lfno;->a:Libb;

    iget-object v1, v1, Libb;->c:Lias;

    iget-object v1, v1, Lias;->a:Liat;

    invoke-direct {v0, v1}, Lfnq;-><init>(Liat;)V

    iput-object v0, p0, Lfno;->g:Lfnq;

    .line 75
    :cond_0
    iget-object v0, p0, Lfno;->g:Lfnq;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 2

    .prologue
    .line 177
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 178
    invoke-virtual {p0}, Lfno;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnu;

    .line 179
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_0

    .line 181
    :cond_0
    invoke-virtual {p0}, Lfno;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnw;

    .line 182
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_1

    .line 184
    :cond_1
    invoke-virtual {p0}, Lfno;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnp;

    .line 185
    invoke-interface {p1, v0}, Lfau;->a(Lfat;)V

    goto :goto_2

    .line 187
    :cond_2
    return-void
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lfno;->h:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lfno;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    iget-object v0, v0, Liaw;->c:Libe;

    iget-object v0, v0, Libe;->a:Lhgz;

    .line 97
    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfno;->h:Ljava/lang/CharSequence;

    .line 100
    :cond_0
    iget-object v0, p0, Lfno;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lfno;->j:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lfno;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    iget-object v0, v0, Liaw;->b:Liar;

    iget-object v0, v0, Liar;->a:Lhgz;

    .line 106
    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfno;->j:Ljava/lang/CharSequence;

    .line 109
    :cond_0
    iget-object v0, p0, Lfno;->j:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->g:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lfno;->f:Lfqh;

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 6

    .prologue
    .line 113
    iget-object v0, p0, Lfno;->i:Ljava/util/List;

    if-nez v0, :cond_1

    .line 114
    invoke-direct {p0}, Lfno;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    iget-object v0, v0, Liaw;->c:Libe;

    iget-object v1, v0, Libe;->b:[Libd;

    .line 116
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfno;->i:Ljava/util/List;

    .line 117
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 118
    iget-object v4, p0, Lfno;->i:Ljava/util/List;

    new-instance v5, Lfnw;

    invoke-direct {v5, v3}, Lfnw;-><init>(Libd;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfno;->i:Ljava/util/List;

    .line 124
    :cond_1
    iget-object v0, p0, Lfno;->i:Ljava/util/List;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 6

    .prologue
    .line 128
    iget-object v0, p0, Lfno;->k:Ljava/util/List;

    if-nez v0, :cond_1

    .line 129
    invoke-direct {p0}, Lfno;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->e:Liaw;

    iget-object v0, v0, Liaw;->b:Liar;

    iget-object v1, v0, Liar;->b:[Liaq;

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfno;->k:Ljava/util/List;

    .line 132
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 133
    iget-object v4, p0, Lfno;->k:Ljava/util/List;

    new-instance v5, Lfnp;

    invoke-direct {v5, v3}, Lfnp;-><init>(Liaq;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 136
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfno;->k:Ljava/util/List;

    .line 139
    :cond_1
    iget-object v0, p0, Lfno;->k:Ljava/util/List;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 6

    .prologue
    .line 151
    iget-object v0, p0, Lfno;->l:Ljava/util/List;

    if-nez v0, :cond_1

    .line 152
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->d:Liaz;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lfno;->a:Libb;

    iget-object v0, v0, Libb;->d:Liaz;

    iget-object v1, v0, Liaz;->b:[Liba;

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfno;->l:Ljava/util/List;

    .line 155
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 156
    iget-object v4, p0, Lfno;->l:Ljava/util/List;

    new-instance v5, Lfnu;

    invoke-direct {v5, v3}, Lfnu;-><init>(Liba;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 159
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfno;->l:Ljava/util/List;

    .line 162
    :cond_1
    iget-object v0, p0, Lfno;->l:Ljava/util/List;

    return-object v0
.end method
