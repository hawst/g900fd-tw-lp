.class public final Lcfz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lfsh;


# instance fields
.field private A:Lgcd;

.field private B:I

.field private C:Lboi;

.field public final a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/ImageView;

.field private i:Lcga;

.field private j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

.field private k:Landroid/view/View;

.field private final l:Landroid/content/Context;

.field private final m:Landroid/content/res/Resources;

.field private final n:Lexd;

.field private final o:Lglm;

.field private final p:Lfus;

.field private final q:Lcwg;

.field private final r:Lbyg;

.field private final s:Lbrz;

.field private final t:Lbka;

.field private final u:Lgnd;

.field private final v:Leyp;

.field private final w:Lbjx;

.field private final x:Lfrz;

.field private final y:Lfsj;

.field private final z:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lexd;Lglm;Lfus;Lcwg;Lbyg;Lbrz;Lbka;Lgnd;Leyp;Lbjx;Lfrz;Ljava/lang/String;Lboi;)V
    .locals 4

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcfz;->l:Landroid/content/Context;

    .line 116
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfsj;

    iput-object v1, p0, Lcfz;->y:Lfsj;

    .line 117
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcfz;->m:Landroid/content/res/Resources;

    .line 118
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lexd;

    iput-object v1, p0, Lcfz;->n:Lexd;

    .line 119
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lglm;

    iput-object v1, p0, Lcfz;->o:Lglm;

    .line 120
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfus;

    iput-object v1, p0, Lcfz;->p:Lfus;

    .line 121
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcwg;

    iput-object v1, p0, Lcfz;->q:Lcwg;

    .line 122
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbyg;

    iput-object v1, p0, Lcfz;->r:Lbyg;

    .line 123
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbrz;

    iput-object v1, p0, Lcfz;->s:Lbrz;

    .line 124
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbka;

    iput-object v1, p0, Lcfz;->t:Lbka;

    .line 125
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgnd;

    iput-object v1, p0, Lcfz;->u:Lgnd;

    .line 126
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyp;

    iput-object v1, p0, Lcfz;->v:Leyp;

    .line 127
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjx;

    iput-object v1, p0, Lcfz;->w:Lbjx;

    .line 129
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfrz;

    iput-object v1, p0, Lcfz;->x:Lfrz;

    .line 130
    move-object/from16 v0, p14

    iput-object v0, p0, Lcfz;->z:Ljava/lang/String;

    .line 131
    invoke-static/range {p15 .. p15}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lboi;

    iput-object v1, p0, Lcfz;->C:Lboi;

    .line 133
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 134
    const v2, 0x7f0400a4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcfz;->b:Landroid/view/View;

    .line 136
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    const v2, 0x7f08008b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcfz;->c:Landroid/widget/TextView;

    .line 137
    iget-object v1, p0, Lcfz;->c:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 138
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    const v2, 0x7f08012f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcfz;->d:Landroid/widget/TextView;

    .line 139
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    const v2, 0x7f080131

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcfz;->e:Landroid/widget/TextView;

    .line 140
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    const v2, 0x7f080132

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    .line 141
    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 143
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    const v2, 0x7f080249

    .line 144
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcfz;->g:Landroid/view/View;

    .line 145
    iget-object v1, p0, Lcfz;->g:Landroid/view/View;

    const v2, 0x7f08012c

    .line 146
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lcfz;->a:Landroid/view/View;

    .line 147
    iget-object v1, p0, Lcfz;->a:Landroid/view/View;

    const v2, 0x7f0800a4

    .line 148
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcfz;->h:Landroid/widget/ImageView;

    .line 149
    new-instance v1, Lcga;

    invoke-direct {v1, p0}, Lcga;-><init>(Lcfz;)V

    iput-object v1, p0, Lcfz;->i:Lcga;

    .line 151
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    const v2, 0x7f080218

    .line 152
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    iput-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    .line 154
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    const v2, 0x7f080130

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcfz;->k:Landroid/view/View;

    .line 155
    iget-object v1, p0, Lcfz;->b:Landroid/view/View;

    invoke-interface {p2, v1}, Lfsj;->a(Landroid/view/View;)V

    .line 156
    invoke-interface {p2, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 157
    return-void
.end method

.method static synthetic a(Lcfz;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcfz;->z:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lgmb;)V
    .locals 10

    .prologue
    const v7, 0x7f07009c

    const v6, 0x7f07009b

    const/16 v5, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 297
    if-eqz p1, :cond_0

    .line 298
    invoke-virtual {p1}, Lgmb;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 300
    :cond_0
    iget-object v0, p0, Lcfz;->A:Lgcd;

    iget-object v0, p0, Lcfz;->h:Landroid/widget/ImageView;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcfz;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcfz;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v4, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    iget-object v0, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->c()V

    if-nez p1, :cond_1

    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    const v1, 0x7f090195

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    if-eqz v2, :cond_2

    iget-object v0, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const v1, 0x7f020166

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(I)V

    .line 308
    :goto_1
    return-void

    .line 300
    :cond_1
    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcfz;->l:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lgmb;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lgmb;->k()Z

    move-result v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(I)V

    goto :goto_1

    .line 301
    :cond_3
    invoke-virtual {p1}, Lgmb;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 303
    iget-object v1, p0, Lcfz;->A:Lgcd;

    iget-object v0, p0, Lcfz;->h:Landroid/widget/ImageView;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcfz;->c:Landroid/widget/TextView;

    iget-object v4, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcfz;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcfz;->f:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v4, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcfz;->o:Lglm;

    invoke-interface {v0}, Lglm;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v1, p1, Lgmb;->d:Lglz;

    invoke-virtual {v1}, Lglz;->c()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v1, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-static {v2, v3, v4, v5, v1}, La;->a(JJLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lgcd;->r:Ljava/util/Date;

    iget-object v4, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-static {v0, v4}, La;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v5, p0, Lcfz;->m:Landroid/content/res/Resources;

    const v6, 0x7f100014

    iget-wide v8, v1, Lgcd;->l:J

    long-to-int v7, v8

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    if-eqz v0, :cond_5

    :goto_2
    aput-object v0, v8, v3

    iget-wide v0, v1, Lgcd;->l:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v8, v2

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_5
    const-string v0, ""

    goto :goto_2

    .line 306
    :cond_6
    iget-object v0, p0, Lcfz;->A:Lgcd;

    iget-object v0, p0, Lcfz;->h:Landroid/widget/ImageView;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    iget-object v0, p0, Lcfz;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcfz;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    iget-object v0, p0, Lcfz;->m:Landroid/content/res/Resources;

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1}, Lgmb;->e()I

    move-result v4

    iget-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const/16 v5, 0x64

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(II)V

    invoke-virtual {p1}, Lgmb;->c()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v5, p0, Lcfz;->l:Landroid/content/Context;

    const v6, 0x7f090196

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v5, v6, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const v2, 0x7f02015b

    invoke-virtual {v1, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(I)V

    iget-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->c()V

    :goto_3
    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v2, p0, Lcfz;->f:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1

    :cond_7
    iget-object v1, p0, Lcfz;->n:Lexd;

    invoke-interface {v1}, Lexd;->a()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    const v2, 0x7f090193

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lcfz;->w:Lbjx;

    invoke-virtual {v1}, Lbjx;->c()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcfz;->n:Lexd;

    invoke-interface {v1}, Lexd;->c()Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    const v2, 0x7f090194

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    goto :goto_3

    :cond_9
    invoke-virtual {p1}, Lgmb;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p1, Lgmb;->g:Lgjn;

    sget-object v5, Lgjn;->b:Lgjn;

    if-ne v1, v5, :cond_a

    move v1, v2

    :goto_4
    if-eqz v1, :cond_b

    iget-object v0, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v1, p0, Lcfz;->l:Landroid/content/Context;

    const v5, 0x7f090191

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v5, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcfz;->m:Landroid/content/res/Resources;

    const v1, 0x7f0700b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a()V

    goto :goto_3

    :cond_a
    move v1, v3

    goto :goto_4

    :cond_b
    iget-object v1, p0, Lcfz;->f:Landroid/widget/TextView;

    iget-object v5, p0, Lcfz;->l:Landroid/content/Context;

    const v6, 0x7f090192

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v5, v6, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcfz;->j:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    goto/16 :goto_3
.end method

.method static synthetic b(Lcfz;)Lbka;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcfz;->t:Lbka;

    return-object v0
.end method

.method private handleConnectivityChangedEvent(Lewk;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcfz;->u:Lgnd;

    iget-object v1, p0, Lcfz;->A:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v1

    .line 279
    if-eqz v1, :cond_1

    .line 280
    invoke-virtual {v1}, Lgmb;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Lgmb;->g:Lgjn;

    sget-object v2, Lgjn;->a:Lgjn;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {v1}, Lgmb;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 281
    :cond_0
    invoke-direct {p0, v1}, Lcfz;->a(Lgmb;)V

    .line 283
    :cond_1
    return-void

    .line 280
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleOfflineDataCacheUpdatedEvent(Lbln;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcfz;->u:Lgnd;

    iget-object v1, p0, Lcfz;->A:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 293
    invoke-direct {p0, v0}, Lcfz;->a(Lgmb;)V

    .line 294
    return-void
.end method

.method private handleOfflineVideoCompleteEvent(Lbma;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 263
    iget-object v0, p0, Lcfz;->A:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v1, p1, Lbma;->a:Lgmb;

    iget-object v1, v1, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p1, Lbma;->a:Lgmb;

    invoke-direct {p0, v0}, Lcfz;->a(Lgmb;)V

    .line 266
    :cond_0
    return-void
.end method

.method private handleOfflineVideoStatusUpdateEvent(Lbmc;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 253
    iget-object v0, p0, Lcfz;->A:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v1, p1, Lbmc;->a:Lgmb;

    iget-object v1, v1, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p1, Lbmc;->a:Lgmb;

    invoke-direct {p0, v0}, Lcfz;->a(Lgmb;)V

    .line 256
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 5

    .prologue
    .line 59
    check-cast p2, Lgcd;

    iget-object v0, p0, Lcfz;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcfz;->m:Landroid/content/res/Resources;

    const v2, 0x7f0b0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iput-object p2, p0, Lcfz;->A:Lgcd;

    iget v0, p1, Lfsg;->a:I

    iput v0, p0, Lcfz;->B:I

    iget-object v0, p0, Lcfz;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lgcd;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcfz;->d:Landroid/widget/TextView;

    iget v1, p2, Lgcd;->k:I

    invoke-static {v1}, La;->c(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcfz;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lgcd;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lgcd;->b:Ljava/lang/String;

    iget-object v1, p0, Lcfz;->u:Lgnd;

    invoke-interface {v1, v0}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v0

    iget-object v1, p2, Lgcd;->f:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcfz;->v:Leyp;

    iget-object v2, p2, Lgcd;->f:Landroid/net/Uri;

    iget-object v3, p0, Lcfz;->h:Landroid/widget/ImageView;

    iget-object v4, p0, Lcfz;->i:Lcga;

    invoke-static {v1, v2, v3, v4}, Leyi;->a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;Leyo;)V

    :goto_0
    invoke-direct {p0, v0}, Lcfz;->a(Lgmb;)V

    iget-object v0, p0, Lcfz;->C:Lboi;

    iget-object v1, p0, Lcfz;->k:Landroid/view/View;

    invoke-static {v0, v1, p2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    iget-object v0, p0, Lcfz;->y:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v1, p0, Lcfz;->h:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 195
    iget-object v0, p0, Lcfz;->A:Lgcd;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcfz;->A:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    .line 197
    iget-object v1, p0, Lcfz;->u:Lgnd;

    invoke-interface {v1, v0}, Lgnd;->b(Ljava/lang/String;)Lgmb;

    move-result-object v1

    .line 198
    if-eqz v1, :cond_6

    .line 199
    invoke-virtual {v1}, Lgmb;->j()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 200
    iget-object v0, v1, Lgmb;->a:Lgcd;

    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lgmb;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, v1, Lgmb;->e:Lflo;

    iget-object v2, p0, Lcfz;->q:Lcwg;

    new-instance v3, Lcgh;

    invoke-direct {v3, p0, v0}, Lcgh;-><init>(Lcfz;Ljava/lang/String;)V

    new-instance v0, Lcwx;

    const/4 v4, 0x0

    invoke-direct {v0, v4}, Lcwx;-><init>(I)V

    invoke-virtual {v2, v1, v3, v0}, Lcwg;->a(Lflo;Lcwi;Lcwx;)V

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    invoke-virtual {v1}, Lgmb;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, p0, Lcfz;->t:Lbka;

    iget-object v2, p0, Lcfz;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lbka;->a(Ljava/lang/String;Ljava/lang/String;Lbkg;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lgmb;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lgmb;->d:Lglz;

    invoke-virtual {v0}, Lglz;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcfz;->s:Lbrz;

    invoke-virtual {v0}, Lbrz;->a()V

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lglz;->b:Lflg;

    invoke-virtual {v0}, Lflg;->b()Lfki;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcfz;->r:Lbyg;

    iget-object v2, p0, Lcfz;->x:Lfrz;

    invoke-virtual {v1, v0, v2}, Lbyg;->a(Lfki;Lfrz;)V

    goto :goto_0

    .line 203
    :cond_4
    invoke-virtual {v1}, Lgmb;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 204
    iget-object v1, p0, Lcfz;->z:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 205
    iget-object v1, p0, Lcfz;->p:Lfus;

    invoke-virtual {v1, v0}, Lfus;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :cond_5
    iget-object v1, p0, Lcfz;->p:Lfus;

    iget-object v2, p0, Lcfz;->z:Ljava/lang/String;

    iget v3, p0, Lcfz;->B:I

    invoke-virtual {v1, v2, v0, v3}, Lfus;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 213
    :cond_6
    iget-object v1, p0, Lcfz;->t:Lbka;

    iget-object v2, p0, Lcfz;->z:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Lbka;->a(Ljava/lang/String;Ljava/lang/String;Lbkg;)V

    goto :goto_0
.end method
