.class public final Lflc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfrx;


# instance fields
.field private a:Lhaz;

.field private b:Lhaz;

.field private c:Lhaz;

.field private d:Lhaz;

.field private e:Lhaz;

.field private f:Lhaz;

.field private g:Lhaz;

.field private h:Lhaz;

.field private i:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lhay;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lflc;->i:Ljava/util/Set;

    .line 37
    iget-object v2, p1, Lhay;->a:[Lhaz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 38
    invoke-direct {p0, v4}, Lflc;->a(Lhaz;)V

    .line 39
    iget v5, v4, Lhaz;->b:I

    packed-switch v5, :pswitch_data_0

    .line 51
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 41
    :pswitch_0
    iput-object v4, p0, Lflc;->a:Lhaz;

    goto :goto_1

    .line 44
    :pswitch_1
    iput-object v4, p0, Lflc;->b:Lhaz;

    goto :goto_1

    .line 47
    :pswitch_2
    iput-object v4, p0, Lflc;->c:Lhaz;

    goto :goto_1

    .line 50
    :pswitch_3
    iput-object v4, p0, Lflc;->d:Lhaz;

    goto :goto_1

    .line 56
    :cond_0
    iget-object v1, p1, Lhay;->b:[Lhaz;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 57
    invoke-direct {p0, v3}, Lflc;->a(Lhaz;)V

    .line 58
    iget v4, v3, Lhaz;->b:I

    packed-switch v4, :pswitch_data_1

    .line 70
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 60
    :pswitch_4
    iput-object v3, p0, Lflc;->e:Lhaz;

    goto :goto_3

    .line 63
    :pswitch_5
    iput-object v3, p0, Lflc;->f:Lhaz;

    goto :goto_3

    .line 66
    :pswitch_6
    iput-object v3, p0, Lflc;->g:Lhaz;

    goto :goto_3

    .line 69
    :pswitch_7
    iput-object v3, p0, Lflc;->h:Lhaz;

    goto :goto_3

    .line 75
    :cond_1
    return-void

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch

    .line 58
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
    .end packed-switch
.end method

.method private a(Lhaz;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p1, Lhaz;->c:Lhog;

    invoke-direct {p0, v0}, Lflc;->a(Lhog;)V

    .line 79
    iget-object v0, p1, Lhaz;->d:Lhog;

    invoke-direct {p0, v0}, Lflc;->a(Lhog;)V

    .line 80
    iget-object v0, p1, Lhaz;->e:Lhog;

    invoke-direct {p0, v0}, Lflc;->a(Lhog;)V

    .line 81
    return-void
.end method

.method private a(Lhog;)V
    .locals 1

    .prologue
    .line 84
    if-eqz p1, :cond_0

    .line 85
    iget-object v0, p0, Lflc;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ZZZ)Lhaz;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lflc;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    .line 109
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lflc;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 110
    :goto_1
    if-nez v2, :cond_3

    if-nez v0, :cond_3

    .line 111
    if-eqz p3, :cond_2

    iget-object v0, p0, Lflc;->e:Lhaz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflc;->e:Lhaz;

    .line 119
    :goto_2
    return-object v0

    :cond_0
    move v2, v1

    .line 108
    goto :goto_0

    :cond_1
    move v0, v1

    .line 109
    goto :goto_1

    .line 111
    :cond_2
    iget-object v0, p0, Lflc;->a:Lhaz;

    goto :goto_2

    .line 113
    :cond_3
    if-nez v2, :cond_5

    .line 114
    if-eqz p3, :cond_4

    iget-object v0, p0, Lflc;->g:Lhaz;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflc;->g:Lhaz;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lflc;->c:Lhaz;

    goto :goto_2

    .line 116
    :cond_5
    if-nez v0, :cond_7

    .line 117
    if-eqz p3, :cond_6

    iget-object v0, p0, Lflc;->f:Lhaz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflc;->f:Lhaz;

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lflc;->b:Lhaz;

    goto :goto_2

    .line 119
    :cond_7
    if-eqz p3, :cond_8

    iget-object v0, p0, Lflc;->h:Lhaz;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflc;->h:Lhaz;

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lflc;->d:Lhaz;

    goto :goto_2
.end method

.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lflc;->i:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 128
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 129
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lflc;->c:Lhaz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lflc;->b:Lhaz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
