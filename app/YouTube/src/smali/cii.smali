.class final Lcii;
.super Lcby;
.source "SourceFile"


# instance fields
.field private synthetic f:Lcif;


# direct methods
.method public constructor <init>(Lcif;Landroid/content/Context;Leyp;Landroid/view/View;Lfhz;Lfdw;Lfrz;)V
    .locals 7

    .prologue
    .line 182
    iput-object p1, p0, Lcii;->f:Lcif;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 183
    invoke-direct/range {v0 .. v6}, Lcby;-><init>(Landroid/content/Context;Leyp;Landroid/view/View;Lfhz;Lfdw;Lfrz;)V

    .line 190
    return-void
.end method


# virtual methods
.method public final a(Lfnm;)V
    .locals 7

    .prologue
    const/4 v1, 0x2

    .line 193
    iget-object v4, p0, Lcby;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcii;->f:Lcif;

    iget-object v0, v0, Lcif;->d:Landroid/content/Context;

    invoke-static {v0}, La;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_0
    iget-object v2, p1, Lfnm;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    iget-object v2, p1, Lfnm;->a:Liab;

    iget-object v2, v2, Liab;->g:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p1, Lfnm;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v3, p1, Lfnm;->c:Ljava/lang/CharSequence;

    iget-object v2, p1, Lfnm;->f:Ljava/lang/CharSequence;

    if-nez v2, :cond_1

    iget-object v2, p1, Lfnm;->a:Liab;

    iget-object v2, v2, Liab;->j:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p1, Lfnm;->f:Ljava/lang/CharSequence;

    :cond_1
    iget-object v2, p1, Lfnm;->f:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v2, p1, Lfnm;->e:Ljava/lang/CharSequence;

    if-nez v2, :cond_2

    iget-object v2, p1, Lfnm;->a:Liab;

    iget-object v2, v2, Liab;->e:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p1, Lfnm;->e:Ljava/lang/CharSequence;

    :cond_2
    iget-object v2, p1, Lfnm;->e:Ljava/lang/CharSequence;

    :cond_3
    if-nez v3, :cond_5

    move-object v0, v2

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    return-void

    .line 193
    :cond_4
    iget-object v0, p0, Lcii;->f:Lcif;

    iget-object v0, v0, Lcif;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    goto :goto_0

    :cond_5
    if-nez v2, :cond_6

    move-object v0, v3

    goto :goto_1

    :cond_6
    if-ne v0, v1, :cond_7

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v0, v5, v3

    aput-object v2, v5, v1

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    :cond_7
    const-string v0, " \u00b7 "

    goto :goto_2
.end method
