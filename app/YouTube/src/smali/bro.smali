.class public final Lbro;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lezl;
.implements Lfbn;


# instance fields
.field public final a:Lfbh;

.field final b:Landroid/app/Activity;

.field final c:Leyp;

.field final d:Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

.field final e:Landroid/widget/ImageView;

.field final f:Landroid/widget/EditText;

.field public g:Ljava/lang/String;

.field public h:Z

.field private final i:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Leyp;Lfcz;Levn;Leyt;)V
    .locals 8

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbro;->b:Landroid/app/Activity;

    .line 60
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbro;->c:Leyp;

    .line 62
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const v0, 0x7f08021b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

    iput-object v0, p0, Lbro;->d:Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

    .line 65
    const v0, 0x7f08021c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbro;->i:Landroid/view/View;

    .line 66
    const v0, 0x7f080146

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 67
    const v0, 0x7f080221

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 68
    const v0, 0x7f080220

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbro;->e:Landroid/widget/ImageView;

    .line 69
    const v0, 0x7f080157

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbro;->f:Landroid/widget/EditText;

    .line 70
    iget-object v0, p0, Lbro;->f:Landroid/widget/EditText;

    new-instance v1, Lbrs;

    invoke-direct {v1, p0}, Lbrs;-><init>(Lbro;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 72
    new-instance v0, Lfbh;

    iget-object v3, p0, Lbro;->d:Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

    new-instance v7, Lbrp;

    invoke-direct {v7, p0}, Lbrp;-><init>(Lbro;)V

    move-object v1, p4

    move-object v2, p0

    move-object v4, p5

    move-object v5, p6

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lfbh;-><init>(Lfcz;Lfbn;Landroid/view/View;Levn;Leyt;Leyp;Lfut;)V

    iput-object v0, p0, Lbro;->a:Lfbh;

    .line 76
    iget-object v0, p0, Lbro;->i:Landroid/view/View;

    new-instance v1, Lbrr;

    invoke-direct {v1, p0}, Lbrr;-><init>(Lbro;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v0, p0, Lbro;->d:Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

    new-instance v1, Lbrq;

    invoke-direct {v1, p0}, Lbrq;-><init>(Lbro;)V

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbro;->h:Z

    .line 79
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbro;->a:Lfbh;

    invoke-virtual {v0}, Lfbh;->a()V

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbro;->h:Z

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lbro;->g:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lbro;->d:Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->a()V

    .line 98
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 86
    if-eqz p1, :cond_0

    .line 87
    iget-object v0, p0, Lbro;->d:Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->setVisibility(I)V

    .line 91
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Lbro;->d:Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/VerticalDrawerLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lbro;->b:Landroid/app/Activity;

    const v1, 0x7f090331

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    .line 130
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 138
    iget-object v0, p0, Lbro;->b:Landroid/app/Activity;

    const v1, 0x7f090332

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    .line 139
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 192
    invoke-virtual {p0}, Lbro;->a()V

    .line 193
    return-void
.end method
