.class final Lacv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field private d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1226
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacv;->a:Ljava/lang/String;

    .line 1227
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacv;->b:Ljava/lang/String;

    .line 1228
    invoke-static {p3}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lacv;->c:Ljava/lang/String;

    .line 1229
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1233
    if-ne p0, p1, :cond_1

    .line 1242
    :cond_0
    :goto_0
    return v0

    .line 1236
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Lacv;

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    .line 1237
    goto :goto_0

    .line 1239
    :cond_3
    check-cast p1, Lacv;

    .line 1240
    iget-object v2, p0, Lacv;->a:Ljava/lang/String;

    iget-object v3, p1, Lacv;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lacv;->b:Ljava/lang/String;

    iget-object v3, p1, Lacv;->b:Ljava/lang/String;

    .line 1241
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lacv;->c:Ljava/lang/String;

    iget-object v3, p1, Lacv;->c:Ljava/lang/String;

    .line 1242
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1247
    iget v0, p0, Lacv;->d:I

    .line 1248
    if-nez v0, :cond_0

    .line 1249
    iget-object v0, p0, Lacv;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1251
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lacv;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1252
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lacv;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1253
    iput v0, p0, Lacv;->d:I

    .line 1255
    :cond_0
    return v0
.end method
