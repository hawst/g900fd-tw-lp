.class final Lbtp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lbto;


# direct methods
.method constructor <init>(Lbto;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lbtp;->a:Lbto;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 67
    iget-object v0, p0, Lbtp;->a:Lbto;

    iget-object v0, v0, Lbto;->e:Landroid/view/View;

    const v1, 0x7f08024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 68
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    .line 70
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 71
    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbt;

    .line 74
    instance-of v1, v0, Lfln;

    if-eqz v1, :cond_2

    .line 75
    check-cast v0, Lfln;

    .line 76
    new-instance v1, Lbto;

    iget-object v2, p0, Lbtp;->a:Lbto;

    iget-object v2, v2, Lbto;->a:Landroid/content/Context;

    iget-object v3, p0, Lbtp;->a:Lbto;

    iget-object v3, v3, Lbto;->b:Lfhz;

    iget-object v4, p0, Lbtp;->a:Lbto;

    iget-object v4, v4, Lbto;->c:Leyt;

    invoke-direct {v1, v2, v3, v4, v0}, Lbto;-><init>(Landroid/content/Context;Lfhz;Leyt;Lfln;)V

    .line 77
    invoke-virtual {v1}, Lbto;->a()V

    .line 86
    :cond_0
    :goto_0
    iget-object v0, p0, Lbtp;->a:Lbto;

    iget-object v0, v0, Lbto;->d:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 89
    :cond_1
    return-void

    .line 78
    :cond_2
    instance-of v1, v0, Lflm;

    if-eqz v1, :cond_3

    .line 79
    check-cast v0, Lflm;

    .line 80
    iget-object v1, p0, Lbtp;->a:Lbto;

    iget-object v1, v1, Lbto;->b:Lfhz;

    iget-object v0, v0, Lflm;->c:Lhqa;

    iget-object v0, v0, Lhqa;->c:Lhut;

    invoke-interface {v1, v0, v3}, Lfhz;->a(Lhut;Ljava/lang/Object;)V

    goto :goto_0

    .line 81
    :cond_3
    instance-of v1, v0, Lfll;

    if-eqz v1, :cond_0

    .line 82
    check-cast v0, Lfll;

    .line 83
    iget-object v1, p0, Lbtp;->a:Lbto;

    iget-object v1, v1, Lbto;->b:Lfhz;

    iget-object v0, v0, Lfll;->c:Lhpz;

    iget-object v0, v0, Lhpz;->c:Lhog;

    invoke-interface {v1, v0, v3}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    goto :goto_0
.end method
