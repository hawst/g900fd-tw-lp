.class public final Leri;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/View;

.field public b:Z

.field c:Lerp;

.field private final d:Lern;

.field private e:Landroid/os/Handler;

.field private f:Z

.field private g:Lerl;


# direct methods
.method public constructor <init>(Landroid/view/View;Lern;)V
    .locals 2

    .prologue
    .line 160
    new-instance v0, Lerl;

    invoke-direct {v0}, Lerl;-><init>()V

    new-instance v1, Lerp;

    invoke-direct {v1}, Lerp;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Leri;-><init>(Landroid/view/View;Lern;Lerl;Lerp;)V

    .line 161
    return-void
.end method

.method private constructor <init>(Landroid/view/View;Lern;Lerl;Lerp;)V
    .locals 3

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    iput-object p1, p0, Leri;->a:Landroid/view/View;

    .line 165
    iput-object p2, p0, Leri;->d:Lern;

    .line 166
    iput-object p3, p0, Leri;->g:Lerl;

    .line 167
    iput-object p4, p0, Leri;->c:Lerp;

    .line 169
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lerm;

    invoke-direct {v2, p0}, Lerm;-><init>(Leri;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Leri;->e:Landroid/os/Handler;

    .line 172
    iget-object v0, p0, Leri;->a:Landroid/view/View;

    new-instance v1, Lerj;

    invoke-direct {v1, p0}, Lerj;-><init>(Leri;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 181
    return-void
.end method

.method private c()D
    .locals 4

    .prologue
    .line 355
    iget-object v0, p0, Leri;->a:Landroid/view/View;

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_3

    .line 356
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 357
    iget-object v1, p0, Leri;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    .line 358
    if-eqz v1, :cond_3

    .line 359
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    mul-int/2addr v0, v1

    int-to-double v0, v0

    .line 360
    iget-object v2, p0, Leri;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, Leri;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-double v2, v2

    div-double/2addr v0, v2

    .line 364
    :goto_1
    return-wide v0

    .line 355
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v1, v1, Landroid/view/View;

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_2
    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 364
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 197
    iget-object v0, p0, Leri;->c:Lerp;

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v2, "sdk"

    const-string v3, "a"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "a"

    sget-object v3, Lerp;->a:Ljava/text/DecimalFormat;

    iget-wide v4, v0, Lerp;->m:D

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "c"

    sget-object v3, Lerp;->a:Ljava/text/DecimalFormat;

    iget-wide v4, v0, Lerp;->c:D

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "tos"

    const-string v3, ","

    iget-object v4, v0, Lerp;->f:[Ljava/lang/Long;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "ctos"

    const-string v3, ","

    iget-object v4, v0, Lerp;->g:[Ljava/lang/Long;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "mtos"

    const-string v3, ","

    iget-object v4, v0, Lerp;->h:[Ljava/lang/Long;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "pt"

    const-string v3, ","

    iget-object v4, v0, Lerp;->i:Ljava/util/List;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lerp;->j:Landroid/graphics/Rect;

    if-eqz v2, :cond_0

    const-string v2, "p"

    const-string v3, "%d,%d,%d,%d"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lerp;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget-object v5, v0, Lerp;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    iget-object v5, v0, Lerp;->j:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    const/4 v5, 0x3

    iget-object v6, v0, Lerp;->j:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "ps"

    const-string v3, "%d,%d"

    new-array v4, v9, [Ljava/lang/Object;

    iget v5, v0, Lerp;->l:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    iget v0, v0, Lerp;->k:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 199
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 200
    const-string v3, "%s=%s"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 202
    :cond_1
    const-string v0, "&"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lero;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 209
    sget-object v0, Lerk;->a:[I

    invoke-virtual {p1}, Lero;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 234
    :goto_0
    return-void

    .line 211
    :pswitch_0
    iget-object v0, p0, Leri;->c:Lerp;

    iget-object v1, p0, Leri;->g:Lerl;

    invoke-virtual {v1}, Lerl;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lerp;->b:J

    iget-object v0, p0, Leri;->c:Lerp;

    invoke-direct {p0}, Leri;->c()D

    move-result-wide v2

    sget-object v1, Lero;->a:Lero;

    invoke-virtual {v0, v2, v3, v1}, Lerp;->a(DLero;)V

    invoke-virtual {p0, v4}, Leri;->a(Z)V

    goto :goto_0

    .line 216
    :pswitch_1
    iget-object v0, p0, Leri;->c:Lerp;

    invoke-direct {p0}, Leri;->c()D

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p1}, Lerp;->a(DLero;)V

    invoke-virtual {p0, v4}, Leri;->a(Z)V

    goto :goto_0

    .line 219
    :pswitch_2
    iget-object v0, p0, Leri;->c:Lerp;

    invoke-direct {p0}, Leri;->c()D

    move-result-wide v2

    sget-object v1, Lero;->e:Lero;

    invoke-virtual {v0, v2, v3, v1}, Lerp;->a(DLero;)V

    invoke-virtual {p0, v5}, Leri;->a(Z)V

    goto :goto_0

    .line 222
    :pswitch_3
    invoke-virtual {p0, v5}, Leri;->a(Z)V

    iget-object v0, p0, Leri;->c:Lerp;

    iput-boolean v5, v0, Lerp;->d:Z

    goto :goto_0

    .line 225
    :pswitch_4
    invoke-virtual {p0, v4}, Leri;->a(Z)V

    iget-object v0, p0, Leri;->c:Lerp;

    iput-boolean v4, v0, Lerp;->d:Z

    goto :goto_0

    .line 228
    :pswitch_5
    invoke-virtual {p0, v5}, Leri;->a(Z)V

    iget-object v0, p0, Leri;->c:Lerp;

    iput-boolean v5, v0, Lerp;->e:Z

    goto :goto_0

    .line 232
    :pswitch_6
    invoke-virtual {p0, v4}, Leri;->a(Z)V

    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method final a(Z)V
    .locals 14

    .prologue
    .line 277
    invoke-virtual {p0}, Leri;->b()V

    .line 278
    iget-boolean v0, p0, Leri;->b:Z

    if-eqz v0, :cond_1

    .line 293
    :cond_0
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v2, p0, Leri;->c:Lerp;

    iget-object v0, p0, Leri;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    if-gtz v1, :cond_4

    const-wide/16 v0, 0x0

    :goto_1
    iput-wide v0, v2, Lerp;->m:D

    .line 283
    iget-object v1, p0, Leri;->c:Lerp;

    iget-object v0, p0, Leri;->g:Lerl;

    invoke-virtual {v0}, Lerl;->a()J

    move-result-wide v2

    invoke-direct {p0}, Leri;->c()D

    move-result-wide v4

    iget-wide v6, v1, Lerp;->b:J

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    iget-boolean v0, v1, Lerp;->e:Z

    if-eqz v0, :cond_5

    .line 285
    :cond_2
    :goto_2
    iget-object v0, p0, Leri;->c:Lerp;

    sget-object v1, Lerq;->c:Lerq;

    invoke-virtual {v1}, Lerq;->ordinal()I

    move-result v1

    iget-object v2, v0, Lerp;->g:[Ljava/lang/Long;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, v0, Lerp;->h:[Ljava/lang/Long;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_b

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_3

    iget-boolean v0, p0, Leri;->f:Z

    if-nez v0, :cond_3

    .line 286
    iget-object v0, p0, Leri;->d:Lern;

    invoke-virtual {p0}, Leri;->a()Ljava/lang/String;

    invoke-interface {v0}, Lern;->a()V

    .line 287
    const/4 v0, 0x1

    iput-boolean v0, p0, Leri;->f:Z

    .line 290
    :cond_3
    if-nez p1, :cond_0

    .line 291
    iget-object v0, p0, Leri;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 282
    :cond_4
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    int-to-double v4, v0

    int-to-double v0, v1

    div-double v0, v4, v0

    goto :goto_1

    .line 283
    :cond_5
    iget-wide v6, v1, Lerp;->b:J

    sub-long v6, v2, v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    iget-boolean v0, v1, Lerp;->d:Z

    if-nez v0, :cond_a

    invoke-static {v4, v5}, Lerp;->a(D)Lerq;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-virtual {v8}, Lerq;->ordinal()I

    move-result v0

    iget-object v9, v1, Lerp;->f:[Ljava/lang/Long;

    aget-object v10, v9, v0

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v10, v6

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v0

    :goto_4
    iget-object v9, v1, Lerp;->g:[Ljava/lang/Long;

    const/4 v9, 0x5

    if-ge v0, v9, :cond_7

    iget-object v9, v1, Lerp;->g:[Ljava/lang/Long;

    aget-object v10, v9, v0

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    add-long/2addr v10, v6

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v0

    iget-object v9, v1, Lerp;->g:[Ljava/lang/Long;

    aget-object v9, v9, v0

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v9, v1, Lerp;->h:[Ljava/lang/Long;

    aget-object v9, v9, v0

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-lez v9, :cond_6

    iget-object v9, v1, Lerp;->h:[Ljava/lang/Long;

    iget-object v10, v1, Lerp;->g:[Ljava/lang/Long;

    aget-object v10, v10, v0

    aput-object v10, v9, v0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    :goto_5
    iget-object v6, v1, Lerp;->g:[Ljava/lang/Long;

    const/4 v6, 0x5

    if-ge v0, v6, :cond_a

    if-eqz v8, :cond_8

    invoke-virtual {v8}, Lerq;->ordinal()I

    move-result v6

    if-lt v0, v6, :cond_8

    if-eqz p1, :cond_9

    :cond_8
    iget-object v6, v1, Lerp;->g:[Ljava/lang/Long;

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v0

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_a
    iput-wide v2, v1, Lerp;->b:J

    iput-wide v4, v1, Lerp;->c:D

    goto/16 :goto_2

    .line 285
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method public b()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Leri;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 308
    return-void
.end method
