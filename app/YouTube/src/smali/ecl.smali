.class public final Lecl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lebz;
.implements Lefs;


# instance fields
.field final a:I

.field final b:Lecu;

.field private final c:Leba;

.field private final d:Lecv;

.field private final e:Leck;

.field private final f:Ljava/util/LinkedList;

.field private final g:Ljava/util/List;

.field private final h:I

.field private final i:Z

.field private final j:Landroid/os/Handler;

.field private final k:I

.field private l:I

.field private m:J

.field private n:J

.field private o:J

.field private p:J

.field private q:Z

.field private r:Lefr;

.field private s:Ljava/io/IOException;

.field private t:Z

.field private u:I

.field private v:J

.field private w:Lebv;

.field private volatile x:Lecw;


# direct methods
.method public constructor <init>(Lecv;Leba;IZ)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 162
    const/high16 v3, 0x500000

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, v5

    move v7, v4

    invoke-direct/range {v0 .. v7}, Lecl;-><init>(Lecv;Leba;IZLandroid/os/Handler;Lecu;I)V

    .line 163
    return-void
.end method

.method private constructor <init>(Lecv;Leba;IZLandroid/os/Handler;Lecu;I)V
    .locals 9

    .prologue
    .line 168
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v8}, Lecl;-><init>(Lecv;Leba;IZLandroid/os/Handler;Lecu;II)V

    .line 170
    return-void
.end method

.method public constructor <init>(Lecv;Leba;IZLandroid/os/Handler;Lecu;II)V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p1, p0, Lecl;->d:Lecv;

    .line 176
    iput-object p2, p0, Lecl;->c:Leba;

    .line 177
    iput p3, p0, Lecl;->h:I

    .line 178
    iput-boolean p4, p0, Lecl;->i:Z

    .line 179
    iput-object p5, p0, Lecl;->j:Landroid/os/Handler;

    .line 180
    iput-object p6, p0, Lecl;->b:Lecu;

    .line 181
    iput p7, p0, Lecl;->a:I

    .line 182
    iput p8, p0, Lecl;->k:I

    .line 183
    new-instance v0, Leck;

    invoke-direct {v0}, Leck;-><init>()V

    iput-object v0, p0, Lecl;->e:Leck;

    .line 184
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    .line 185
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lecl;->g:Ljava/util/List;

    .line 186
    const/4 v0, 0x0

    iput v0, p0, Lecl;->l:I

    .line 187
    return-void
.end method

.method private a(JJJ)V
    .locals 9

    .prologue
    .line 797
    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecl;->b:Lecu;

    if-eqz v0, :cond_0

    .line 798
    iget-object v8, p0, Lecl;->j:Landroid/os/Handler;

    new-instance v0, Lect;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-direct/range {v0 .. v7}, Lect;-><init>(Lecl;JJJ)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 806
    :cond_0
    return-void
.end method

.method private a(Ledc;)V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 647
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 660
    :cond_0
    :goto_0
    return-void

    .line 651
    :cond_1
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    iget-wide v2, v0, Ledc;->f:J

    move-wide v6, v4

    .line 653
    :goto_1
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    if-eq p1, v0, :cond_2

    .line 654
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    .line 655
    iget-object v1, v0, Lecj;->e:Lefx;

    invoke-virtual {v1}, Lefx;->c()J

    move-result-wide v4

    add-long/2addr v6, v4

    .line 656
    iget-wide v4, v0, Ledc;->g:J

    .line 657
    invoke-virtual {v0}, Ledc;->a()V

    goto :goto_1

    :cond_2
    move-object v1, p0

    .line 659
    invoke-direct/range {v1 .. v7}, Lecl;->a(JJJ)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;IZJJJ)V
    .locals 12

    .prologue
    .line 714
    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecl;->b:Lecu;

    if-eqz v0, :cond_0

    .line 715
    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    new-instance v1, Lecm;

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    invoke-direct/range {v1 .. v11}, Lecm;-><init>(Lecl;Ljava/lang/String;IZJJJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 723
    :cond_0
    return-void
.end method

.method private c(J)V
    .locals 1

    .prologue
    .line 480
    iput-wide p1, p0, Lecl;->o:J

    .line 481
    iget-object v0, p0, Lecl;->r:Lefr;

    iget-boolean v0, v0, Lefr;->c:Z

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lecl;->r:Lefr;

    invoke-virtual {v0}, Lefr;->a()V

    .line 488
    :goto_0
    return-void

    .line 484
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lecl;->a(Ledc;)V

    .line 485
    invoke-direct {p0}, Lecl;->h()V

    .line 486
    invoke-direct {p0}, Lecl;->i()V

    goto :goto_0
.end method

.method private c(I)Z
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 679
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-gt v0, p1, :cond_0

    .line 680
    const/4 v0, 0x0

    .line 692
    :goto_0
    return v0

    .line 684
    :cond_0
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    iget-wide v4, v0, Ledc;->g:J

    move-wide v6, v2

    .line 685
    :goto_1
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, p1, :cond_1

    .line 686
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    .line 687
    iget-object v1, v0, Lecj;->e:Lefx;

    invoke-virtual {v1}, Lefx;->c()J

    move-result-wide v2

    add-long/2addr v6, v2

    .line 688
    iget-wide v2, v0, Ledc;->f:J

    .line 689
    invoke-virtual {v0}, Ledc;->a()V

    goto :goto_1

    .line 691
    :cond_1
    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lecl;->b:Lecu;

    if-eqz v0, :cond_2

    iget-object v8, p0, Lecl;->j:Landroid/os/Handler;

    new-instance v0, Lecr;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lecr;-><init>(Lecl;JJJ)V

    invoke-virtual {v8, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 692
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Lecl;->s:Ljava/io/IOException;

    if-eqz v0, :cond_0

    iget v0, p0, Lecl;->u:I

    iget v1, p0, Lecl;->k:I

    if-le v0, v1, :cond_0

    .line 368
    iget-object v0, p0, Lecl;->s:Ljava/io/IOException;

    throw v0

    .line 370
    :cond_0
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 495
    iget-object v0, p0, Lecl;->e:Leck;

    iput-object v2, v0, Leck;->b:Lecj;

    .line 496
    iput-object v2, p0, Lecl;->s:Ljava/io/IOException;

    .line 497
    iput v1, p0, Lecl;->u:I

    .line 498
    iput-boolean v1, p0, Lecl;->t:Z

    .line 499
    return-void
.end method

.method private i()V
    .locals 14

    .prologue
    .line 502
    iget-boolean v0, p0, Lecl;->t:Z

    if-eqz v0, :cond_1

    .line 504
    iget-object v0, p0, Lecl;->c:Leba;

    iget-wide v2, p0, Lecl;->m:J

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, p0

    invoke-interface/range {v0 .. v7}, Leba;->a(Ljava/lang/Object;JJZZ)Z

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 508
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 509
    invoke-direct {p0}, Lecl;->j()J

    move-result-wide v10

    .line 510
    iget-object v0, p0, Lecl;->s:Ljava/io/IOException;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    move v8, v0

    .line 511
    :goto_1
    iget-object v0, p0, Lecl;->r:Lefr;

    iget-boolean v0, v0, Lefr;->c:Z

    if-nez v0, :cond_2

    if-eqz v8, :cond_6

    :cond_2
    const/4 v0, 0x1

    move v9, v0

    .line 515
    :goto_2
    if-nez v9, :cond_e

    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v0, v0, Leck;->b:Lecj;

    if-nez v0, :cond_3

    const-wide/16 v0, -0x1

    cmp-long v0, v10, v0

    if-nez v0, :cond_4

    :cond_3
    iget-wide v0, p0, Lecl;->p:J

    sub-long v0, v12, v0

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-lez v0, :cond_e

    .line 518
    :cond_4
    iput-wide v12, p0, Lecl;->p:J

    .line 519
    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v1, p0, Lecl;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Leck;->a:I

    .line 520
    iget-object v0, p0, Lecl;->d:Lecv;

    iget-object v1, p0, Lecl;->g:Ljava/util/List;

    iget-wide v2, p0, Lecl;->o:J

    iget-wide v4, p0, Lecl;->m:J

    iget-object v6, p0, Lecl;->e:Leck;

    invoke-interface/range {v0 .. v6}, Lecv;->a(Ljava/util/List;JJLeck;)V

    .line 522
    iget-object v0, p0, Lecl;->e:Leck;

    iget v0, v0, Leck;->a:I

    invoke-direct {p0, v0}, Lecl;->c(I)Z

    move-result v0

    .line 524
    iget-object v1, p0, Lecl;->e:Leck;

    iget-object v1, v1, Leck;->b:Lecj;

    if-nez v1, :cond_7

    .line 526
    const-wide/16 v4, -0x1

    .line 534
    :goto_3
    iget-object v0, p0, Lecl;->c:Leba;

    iget-wide v2, p0, Lecl;->m:J

    const/4 v7, 0x0

    move-object v1, p0

    move v6, v9

    invoke-interface/range {v0 .. v7}, Leba;->a(Ljava/lang/Object;JJZZ)Z

    move-result v0

    .line 537
    if-eqz v8, :cond_d

    .line 538
    iget-wide v0, p0, Lecl;->v:J

    sub-long v0, v12, v0

    .line 539
    iget v2, p0, Lecl;->u:I

    int-to-long v2, v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x1388

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 540
    const/4 v0, 0x0

    iput-object v0, p0, Lecl;->s:Ljava/io/IOException;

    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v8, v0, Leck;->b:Lecj;

    instance-of v0, v8, Ledc;

    if-nez v0, :cond_9

    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v1, p0, Lecl;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Leck;->a:I

    iget-object v0, p0, Lecl;->d:Lecv;

    iget-object v1, p0, Lecl;->g:Ljava/util/List;

    iget-wide v2, p0, Lecl;->o:J

    iget-wide v4, p0, Lecl;->m:J

    iget-object v6, p0, Lecl;->e:Leck;

    invoke-interface/range {v0 .. v6}, Lecv;->a(Ljava/util/List;JJLeck;)V

    iget-object v0, p0, Lecl;->e:Leck;

    iget v0, v0, Leck;->a:I

    invoke-direct {p0, v0}, Lecl;->c(I)Z

    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v0, v0, Leck;->b:Lecj;

    if-ne v0, v8, :cond_8

    iget-object v0, p0, Lecl;->r:Lefr;

    invoke-virtual {v0, v8, p0}, Lefr;->a(Lefu;Lefs;)V

    goto/16 :goto_0

    .line 510
    :cond_5
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_1

    .line 511
    :cond_6
    const/4 v0, 0x0

    move v9, v0

    goto/16 :goto_2

    .line 527
    :cond_7
    if-eqz v0, :cond_e

    .line 529
    invoke-direct {p0}, Lecl;->j()J

    move-result-wide v4

    goto :goto_3

    .line 540
    :cond_8
    invoke-virtual {v8}, Lecj;->a()V

    invoke-direct {p0}, Lecl;->k()V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    if-ne v8, v0, :cond_a

    iget-object v0, p0, Lecl;->r:Lefr;

    invoke-virtual {v0, v8, p0}, Lefr;->a(Lefu;Lefs;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ledc;

    if-ne v8, v7, :cond_b

    const/4 v0, 0x1

    :goto_4
    invoke-static {v0}, La;->c(Z)V

    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v1, p0, Lecl;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Leck;->a:I

    iget-object v0, p0, Lecl;->d:Lecv;

    iget-object v1, p0, Lecl;->g:Ljava/util/List;

    iget-wide v2, p0, Lecl;->o:J

    iget-wide v4, p0, Lecl;->m:J

    iget-object v6, p0, Lecl;->e:Leck;

    invoke-interface/range {v0 .. v6}, Lecv;->a(Ljava/util/List;JJLeck;)V

    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, v7}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v0, v0, Leck;->b:Lecj;

    if-ne v0, v8, :cond_c

    iget-object v0, p0, Lecl;->r:Lefr;

    invoke-virtual {v0, v8, p0}, Lefr;->a(Lefu;Lefs;)V

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    :cond_c
    iget-object v0, p0, Lecl;->e:Leck;

    iget v0, v0, Leck;->a:I

    invoke-direct {p0, v0}, Lecl;->c(I)Z

    invoke-direct {p0}, Lecl;->h()V

    invoke-direct {p0}, Lecl;->k()V

    goto/16 :goto_0

    .line 545
    :cond_d
    iget-object v1, p0, Lecl;->r:Lefr;

    iget-boolean v1, v1, Lefr;->c:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 546
    invoke-direct {p0}, Lecl;->k()V

    goto/16 :goto_0

    :cond_e
    move-wide v4, v10

    goto/16 :goto_3
.end method

.method private j()J
    .locals 3

    .prologue
    .line 555
    invoke-direct {p0}, Lecl;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    iget-wide v0, p0, Lecl;->o:J

    .line 559
    :goto_0
    return-wide v0

    .line 558
    :cond_0
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    .line 559
    iget v1, v0, Ledc;->h:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_1
    iget-wide v0, v0, Ledc;->g:J

    goto :goto_0
.end method

.method private k()V
    .locals 11

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 618
    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v10, v0, Leck;->b:Lecj;

    .line 619
    if-nez v10, :cond_0

    .line 638
    :goto_0
    return-void

    .line 623
    :cond_0
    iget-object v0, p0, Lecl;->c:Leba;

    invoke-interface {v0}, Leba;->b()Leex;

    move-result-object v1

    iget-object v0, v10, Lecj;->e:Lefx;

    if-nez v0, :cond_2

    move v0, v6

    :goto_1
    invoke-static {v0}, La;->c(Z)V

    new-instance v0, Lefx;

    iget-object v2, v10, Lecj;->c:Lefc;

    iget-object v7, v10, Lecj;->d:Lefg;

    invoke-direct {v0, v2, v7, v1}, Lefx;-><init>(Lefc;Lefg;Leex;)V

    iput-object v0, v10, Lecj;->e:Lefx;

    .line 624
    instance-of v0, v10, Ledc;

    if-eqz v0, :cond_3

    move-object v0, v10

    .line 625
    check-cast v0, Ledc;

    .line 626
    invoke-direct {p0}, Lecl;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 627
    iget-wide v6, p0, Lecl;->o:J

    invoke-virtual {v0, v6, v7, v3}, Ledc;->a(JZ)Z

    .line 628
    iput-wide v4, p0, Lecl;->o:J

    .line 630
    :cond_1
    iget-object v1, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 631
    iget-object v1, v0, Ledc;->a:Lecw;

    iget-object v1, v1, Lecw;->a:Ljava/lang/String;

    iget v2, v0, Ledc;->b:I

    iget-wide v4, v0, Ledc;->f:J

    iget-wide v6, v0, Ledc;->g:J

    .line 632
    iget-object v0, v0, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->g()J

    move-result-wide v8

    move-object v0, p0

    .line 631
    invoke-direct/range {v0 .. v9}, Lecl;->a(Ljava/lang/String;IZJJJ)V

    .line 637
    :goto_2
    iget-object v0, p0, Lecl;->r:Lefr;

    invoke-virtual {v0, v10, p0}, Lefr;->a(Lefu;Lefs;)V

    goto :goto_0

    :cond_2
    move v0, v3

    .line 623
    goto :goto_1

    .line 634
    :cond_3
    iget-object v0, v10, Lecj;->a:Lecw;

    iget-object v1, v0, Lecw;->a:Ljava/lang/String;

    iget v2, v10, Lecj;->b:I

    .line 635
    iget-object v0, v10, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->g()J

    move-result-wide v8

    move-object v0, p0

    move v3, v6

    move-wide v6, v4

    .line 634
    invoke-direct/range {v0 .. v9}, Lecl;->a(Ljava/lang/String;IZJJJ)V

    goto :goto_2
.end method

.method private l()Z
    .locals 4

    .prologue
    .line 700
    iget-wide v0, p0, Lecl;->o:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(IJLebw;Leby;Z)I
    .locals 8

    .prologue
    .line 274
    move-object v1, p0

    :goto_0
    iget v0, v1, Lecl;->l:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, La;->c(Z)V

    .line 275
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, La;->c(Z)V

    .line 277
    iget-boolean v0, v1, Lecl;->q:Z

    if-eqz v0, :cond_2

    .line 278
    const/4 v0, 0x0

    iput-boolean v0, v1, Lecl;->q:Z

    .line 279
    const/4 v0, -0x5

    .line 342
    :goto_3
    return v0

    .line 274
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 275
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 282
    :cond_2
    if-eqz p6, :cond_3

    .line 283
    const/4 v0, -0x2

    goto :goto_3

    .line 286
    :cond_3
    iput-wide p2, v1, Lecl;->m:J

    .line 287
    invoke-direct {v1}, Lecl;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 288
    invoke-direct {v1}, Lecl;->g()V

    .line 289
    iget-object v0, v1, Lecl;->d:Lecv;

    .line 290
    const/4 v0, -0x2

    goto :goto_3

    .line 296
    :cond_4
    iget-object v0, v1, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ledc;

    .line 297
    iget-object v0, v6, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->i()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 299
    iget-object v0, v1, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_5

    .line 300
    iget-object v0, v1, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    iget-object v2, v0, Lecj;->e:Lefx;

    invoke-virtual {v2}, Lefx;->c()J

    move-result-wide v6

    invoke-virtual {v0}, Ledc;->a()V

    iget-wide v2, v0, Ledc;->f:J

    iget-wide v4, v0, Ledc;->g:J

    invoke-direct/range {v1 .. v7}, Lecl;->a(JJJ)V

    .line 301
    iget-object v0, v1, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    .line 302
    invoke-virtual {v0}, Ledc;->h()V

    .line 303
    const/4 p6, 0x0

    goto :goto_0

    .line 304
    :cond_5
    invoke-virtual {v6}, Ledc;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 305
    const/4 v0, -0x1

    goto :goto_3

    .line 307
    :cond_6
    iget-object v0, v1, Lecl;->d:Lecv;

    .line 308
    const/4 v0, -0x2

    goto :goto_3

    .line 314
    :cond_7
    iget-object v0, v1, Lecl;->x:Lecw;

    if-eqz v0, :cond_8

    iget-object v0, v1, Lecl;->x:Lecw;

    iget-object v2, v6, Ledc;->a:Lecw;

    invoke-virtual {v0, v2}, Lecw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 315
    :cond_8
    iget-object v0, v6, Ledc;->a:Lecw;

    iget-object v2, v0, Lecw;->a:Ljava/lang/String;

    iget v3, v6, Ledc;->b:I

    iget-wide v4, v6, Ledc;->f:J

    iget-object v0, v1, Lecl;->j:Landroid/os/Handler;

    if-eqz v0, :cond_9

    iget-object v0, v1, Lecl;->b:Lecu;

    if-eqz v0, :cond_9

    iget-object v7, v1, Lecl;->j:Landroid/os/Handler;

    new-instance v0, Lecs;

    invoke-direct/range {v0 .. v5}, Lecs;-><init>(Lecl;Ljava/lang/String;IJ)V

    invoke-virtual {v7, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 317
    :cond_9
    iget-object v0, v6, Ledc;->a:Lecw;

    iput-object v0, v1, Lecl;->x:Lecw;

    .line 320
    :cond_a
    invoke-virtual {v6}, Ledc;->i()Z

    move-result v0

    if-nez v0, :cond_c

    .line 321
    iget-object v0, v1, Lecl;->s:Ljava/io/IOException;

    if-eqz v0, :cond_b

    .line 322
    iget-object v0, v1, Lecl;->s:Ljava/io/IOException;

    throw v0

    .line 324
    :cond_b
    const/4 v0, -0x2

    goto/16 :goto_3

    .line 327
    :cond_c
    invoke-virtual {v6}, Ledc;->k()Lebv;

    move-result-object v2

    .line 328
    if-eqz v2, :cond_f

    iget-object v0, v1, Lecl;->w:Lebv;

    if-ne v2, v0, :cond_d

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_f

    .line 329
    iget-object v0, v1, Lecl;->d:Lecv;

    invoke-interface {v0, v2}, Lecv;->a(Lebv;)V

    .line 330
    iput-object v2, p4, Lebw;->a:Lebv;

    .line 331
    invoke-virtual {v6}, Ledc;->l()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p4, Lebw;->b:Ljava/util/Map;

    .line 332
    iput-object v2, v1, Lecl;->w:Lebv;

    .line 333
    const/4 v0, -0x4

    goto/16 :goto_3

    .line 328
    :cond_d
    if-nez v0, :cond_e

    const/4 v0, 0x0

    goto :goto_4

    :cond_e
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lebv;->a(Lebv;Z)Z

    move-result v0

    goto :goto_4

    .line 336
    :cond_f
    invoke-virtual {v6, p5}, Ledc;->a(Leby;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 337
    iget-boolean v0, v1, Lecl;->i:Z

    if-eqz v0, :cond_10

    iget-wide v2, p5, Leby;->e:J

    iget-wide v0, v1, Lecl;->n:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_10

    const/4 v0, 0x1

    :goto_5
    iput-boolean v0, p5, Leby;->f:Z

    .line 338
    const/4 v0, -0x3

    goto/16 :goto_3

    .line 337
    :cond_10
    const/4 v0, 0x0

    goto :goto_5

    .line 341
    :cond_11
    invoke-direct {v1}, Lecl;->g()V

    .line 342
    const/4 v0, -0x2

    goto/16 :goto_3
.end method

.method public final a(I)Lecb;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 214
    iget v0, p0, Lecl;->l:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 215
    if-nez p1, :cond_1

    :goto_1
    invoke-static {v1}, La;->c(Z)V

    .line 216
    iget-object v0, p0, Lecl;->d:Lecv;

    invoke-interface {v0}, Lecv;->a()Lecb;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 214
    goto :goto_0

    :cond_1
    move v1, v2

    .line 215
    goto :goto_1
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 221
    iget v0, p0, Lecl;->l:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 222
    if-nez p1, :cond_1

    :goto_1
    invoke-static {v1}, La;->c(Z)V

    .line 223
    const/4 v0, 0x2

    iput v0, p0, Lecl;->l:I

    .line 224
    iget-object v0, p0, Lecl;->d:Lecv;

    invoke-interface {v0}, Lecv;->b()V

    .line 225
    iget-object v0, p0, Lecl;->c:Leba;

    iget v1, p0, Lecl;->h:I

    invoke-interface {v0, p0, v1}, Leba;->a(Ljava/lang/Object;I)V

    .line 226
    iput-object v3, p0, Lecl;->x:Lecw;

    .line 227
    iput-object v3, p0, Lecl;->w:Lebv;

    .line 228
    iput-wide p2, p0, Lecl;->m:J

    .line 229
    iput-wide p2, p0, Lecl;->n:J

    .line 230
    invoke-direct {p0, p2, p3}, Lecl;->c(J)V

    .line 231
    return-void

    :cond_0
    move v0, v2

    .line 221
    goto :goto_0

    :cond_1
    move v1, v2

    .line 222
    goto :goto_1
.end method

.method public final a(Ljava/io/IOException;)V
    .locals 2

    .prologue
    .line 460
    iput-object p1, p0, Lecl;->s:Ljava/io/IOException;

    .line 461
    iget v0, p0, Lecl;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lecl;->u:I

    .line 462
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lecl;->v:J

    .line 463
    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecl;->b:Lecu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    new-instance v1, Lecp;

    invoke-direct {v1, p0, p1}, Lecp;-><init>(Lecl;Ljava/io/IOException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 464
    :cond_0
    iget-object v0, p0, Lecl;->d:Lecv;

    iget-object v1, p0, Lecl;->e:Leck;

    iget-object v1, v1, Leck;->b:Lecj;

    invoke-interface {v0, v1, p1}, Lecv;->a(Lecj;Ljava/lang/Exception;)V

    .line 465
    invoke-direct {p0}, Lecl;->i()V

    .line 466
    return-void
.end method

.method public final a()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 200
    iget v0, p0, Lecl;->l:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 201
    new-instance v2, Lefr;

    const-string v3, "Loader:"

    iget-object v0, p0, Lecl;->d:Lecv;

    invoke-interface {v0}, Lecv;->a()Lecb;

    move-result-object v0

    iget-object v0, v0, Lecb;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v0}, Lefr;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lecl;->r:Lefr;

    .line 202
    iput v1, p0, Lecl;->l:I

    .line 203
    return v1

    .line 200
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(J)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 255
    iget v0, p0, Lecl;->l:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 256
    iput-wide p1, p0, Lecl;->m:J

    .line 257
    iget-object v0, p0, Lecl;->d:Lecv;

    .line 258
    invoke-direct {p0}, Lecl;->i()V

    .line 259
    invoke-direct {p0}, Lecl;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v1, v2

    .line 267
    :cond_1
    :goto_1
    return v1

    :cond_2
    move v0, v2

    .line 255
    goto :goto_0

    .line 261
    :cond_3
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    invoke-virtual {v0}, Ledc;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    invoke-virtual {v0}, Ledc;->j()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public final b()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 208
    iget v0, p0, Lecl;->l:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 209
    return v1

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235
    iget v0, p0, Lecl;->l:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 236
    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, La;->c(Z)V

    .line 237
    iput-boolean v2, p0, Lecl;->q:Z

    .line 238
    iput v1, p0, Lecl;->l:I

    .line 240
    :try_start_0
    iget-object v0, p0, Lecl;->d:Lecv;

    iget-object v1, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-interface {v0}, Lecv;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    iget-object v0, p0, Lecl;->c:Leba;

    invoke-interface {v0, p0}, Leba;->a(Ljava/lang/Object;)V

    .line 243
    iget-object v0, p0, Lecl;->r:Lefr;

    iget-boolean v0, v0, Lefr;->c:Z

    if-eqz v0, :cond_2

    .line 244
    iget-object v0, p0, Lecl;->r:Lefr;

    invoke-virtual {v0}, Lefr;->a()V

    .line 250
    :goto_2
    return-void

    :cond_0
    move v0, v2

    .line 235
    goto :goto_0

    :cond_1
    move v0, v2

    .line 236
    goto :goto_1

    .line 246
    :cond_2
    invoke-direct {p0, v4}, Lecl;->a(Ledc;)V

    .line 247
    invoke-direct {p0}, Lecl;->h()V

    .line 248
    iget-object v0, p0, Lecl;->c:Leba;

    invoke-interface {v0}, Leba;->a()V

    goto :goto_2

    .line 242
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lecl;->c:Leba;

    invoke-interface {v1, p0}, Leba;->a(Ljava/lang/Object;)V

    .line 243
    iget-object v1, p0, Lecl;->r:Lefr;

    iget-boolean v1, v1, Lefr;->c:Z

    if-eqz v1, :cond_3

    .line 244
    iget-object v1, p0, Lecl;->r:Lefr;

    invoke-virtual {v1}, Lefr;->a()V

    .line 248
    :goto_3
    throw v0

    .line 246
    :cond_3
    invoke-direct {p0, v4}, Lecl;->a(Ledc;)V

    .line 247
    invoke-direct {p0}, Lecl;->h()V

    .line 248
    iget-object v1, p0, Lecl;->c:Leba;

    invoke-interface {v1}, Leba;->a()V

    goto :goto_3
.end method

.method public final b(J)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 348
    iget v0, p0, Lecl;->l:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 349
    iput-wide p1, p0, Lecl;->m:J

    .line 350
    iput-wide p1, p0, Lecl;->n:J

    .line 351
    iget-wide v4, p0, Lecl;->o:J

    cmp-long v0, v4, p1

    if-nez v0, :cond_1

    .line 364
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 348
    goto :goto_0

    .line 355
    :cond_1
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    iget-wide v4, v0, Ledc;->f:J

    cmp-long v4, p1, v4

    if-ltz v4, :cond_4

    invoke-virtual {v0}, Ledc;->g()Z

    move-result v4

    if-nez v4, :cond_3

    iget-wide v4, v0, Ledc;->g:J

    cmp-long v4, p1, v4

    if-gez v4, :cond_2

    .line 356
    :cond_3
    :goto_2
    if-nez v0, :cond_5

    .line 357
    invoke-direct {p0, p1, p2}, Lecl;->c(J)V

    .line 358
    iput-boolean v1, p0, Lecl;->q:Z

    goto :goto_1

    .line 355
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 360
    :cond_5
    iget-boolean v3, p0, Lecl;->q:Z

    iget-object v4, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v4

    if-ne v0, v4, :cond_6

    :goto_3
    invoke-virtual {v0, p1, p2, v1}, Ledc;->a(JZ)Z

    move-result v1

    or-int/2addr v1, v3

    iput-boolean v1, p0, Lecl;->q:Z

    .line 361
    invoke-direct {p0, v0}, Lecl;->a(Ledc;)V

    .line 362
    invoke-direct {p0}, Lecl;->i()V

    goto :goto_1

    :cond_6
    move v1, v2

    .line 360
    goto :goto_3
.end method

.method public final c()J
    .locals 10

    .prologue
    .line 387
    iget v0, p0, Lecl;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 388
    invoke-direct {p0}, Lecl;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389
    iget-wide v0, p0, Lecl;->o:J

    .line 405
    :goto_1
    return-wide v0

    .line 387
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 391
    :cond_1
    iget-object v0, p0, Lecl;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    .line 392
    iget-object v1, p0, Lecl;->e:Leck;

    iget-object v1, v1, Leck;->b:Lecj;

    .line 393
    if-eqz v1, :cond_3

    if-ne v0, v1, :cond_3

    .line 395
    iget-object v1, v0, Lecj;->e:Lefx;

    invoke-virtual {v1}, Lefx;->g()J

    move-result-wide v2

    .line 396
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 397
    iget-wide v4, v0, Ledc;->f:J

    iget-wide v6, v0, Ledc;->g:J

    iget-wide v8, v0, Ledc;->f:J

    sub-long/2addr v6, v8

    .line 398
    iget-object v0, v0, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->c()J

    move-result-wide v0

    mul-long/2addr v0, v6

    div-long/2addr v0, v2

    add-long/2addr v0, v4

    goto :goto_1

    .line 400
    :cond_2
    iget-wide v0, v0, Ledc;->f:J

    goto :goto_1

    .line 402
    :cond_3
    invoke-virtual {v0}, Ledc;->g()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 403
    const-wide/16 v0, -0x3

    goto :goto_1

    .line 405
    :cond_4
    iget-wide v0, v0, Ledc;->g:J

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 411
    iget v0, p0, Lecl;->l:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 412
    iget-object v0, p0, Lecl;->r:Lefr;

    if-eqz v0, :cond_1

    .line 413
    iget-object v0, p0, Lecl;->r:Lefr;

    iget-boolean v2, v0, Lefr;->c:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lefr;->a()V

    :cond_0
    iget-object v0, v0, Lefr;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 414
    const/4 v0, 0x0

    iput-object v0, p0, Lecl;->r:Lefr;

    .line 416
    :cond_1
    iput v1, p0, Lecl;->l:I

    .line 417
    return-void

    :cond_2
    move v0, v1

    .line 411
    goto :goto_0
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 421
    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v1, v0, Leck;->b:Lecj;

    .line 422
    iget-object v0, v1, Lecj;->e:Lefx;

    invoke-virtual {v0}, Lefx;->c()J

    move-result-wide v2

    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecl;->b:Lecu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lecl;->j:Landroid/os/Handler;

    new-instance v4, Lecn;

    invoke-direct {v4, p0, v2, v3}, Lecn;-><init>(Lecl;J)V

    invoke-virtual {v0, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 424
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lecj;->b()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    instance-of v0, v1, Ledc;

    if-nez v0, :cond_1

    .line 433
    invoke-virtual {v1}, Lecj;->a()V

    .line 435
    :cond_1
    iget-boolean v0, p0, Lecl;->t:Z

    if-nez v0, :cond_2

    .line 436
    invoke-direct {p0}, Lecl;->h()V

    .line 438
    :cond_2
    invoke-direct {p0}, Lecl;->i()V

    .line 439
    :goto_0
    return-void

    .line 425
    :catch_0
    move-exception v0

    .line 426
    :try_start_1
    iput-object v0, p0, Lecl;->s:Ljava/io/IOException;

    .line 427
    iget v2, p0, Lecl;->u:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lecl;->u:I

    .line 428
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lecl;->v:J

    .line 429
    const/4 v2, 0x1

    iput-boolean v2, p0, Lecl;->t:Z

    .line 430
    iget-object v2, p0, Lecl;->j:Landroid/os/Handler;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lecl;->b:Lecu;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lecl;->j:Landroid/os/Handler;

    new-instance v3, Lecq;

    invoke-direct {v3, p0, v0}, Lecq;-><init>(Lecl;Ljava/io/IOException;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    :cond_3
    instance-of v0, v1, Ledc;

    if-nez v0, :cond_4

    .line 433
    invoke-virtual {v1}, Lecj;->a()V

    .line 435
    :cond_4
    iget-boolean v0, p0, Lecl;->t:Z

    if-nez v0, :cond_5

    .line 436
    invoke-direct {p0}, Lecl;->h()V

    .line 438
    :cond_5
    invoke-direct {p0}, Lecl;->i()V

    goto :goto_0

    .line 432
    :catchall_0
    move-exception v0

    instance-of v2, v1, Ledc;

    if-nez v2, :cond_6

    .line 433
    invoke-virtual {v1}, Lecj;->a()V

    .line 435
    :cond_6
    iget-boolean v1, p0, Lecl;->t:Z

    if-nez v1, :cond_7

    .line 436
    invoke-direct {p0}, Lecl;->h()V

    .line 438
    :cond_7
    invoke-direct {p0}, Lecl;->i()V

    throw v0
.end method

.method public final f()V
    .locals 5

    .prologue
    .line 444
    iget-object v0, p0, Lecl;->e:Leck;

    iget-object v0, v0, Leck;->b:Lecj;

    .line 445
    iget-object v1, v0, Lecj;->e:Lefx;

    invoke-virtual {v1}, Lefx;->c()J

    move-result-wide v2

    iget-object v1, p0, Lecl;->j:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lecl;->b:Lecu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lecl;->j:Landroid/os/Handler;

    new-instance v4, Leco;

    invoke-direct {v4, p0, v2, v3}, Leco;-><init>(Lecl;J)V

    invoke-virtual {v1, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 446
    :cond_0
    instance-of v1, v0, Ledc;

    if-nez v1, :cond_1

    .line 447
    invoke-virtual {v0}, Lecj;->a()V

    .line 449
    :cond_1
    invoke-direct {p0}, Lecl;->h()V

    .line 450
    iget v0, p0, Lecl;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 451
    iget-wide v0, p0, Lecl;->o:J

    invoke-direct {p0, v0, v1}, Lecl;->c(J)V

    .line 456
    :goto_0
    return-void

    .line 453
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lecl;->a(Ledc;)V

    .line 454
    iget-object v0, p0, Lecl;->c:Leba;

    invoke-interface {v0}, Leba;->a()V

    goto :goto_0
.end method
