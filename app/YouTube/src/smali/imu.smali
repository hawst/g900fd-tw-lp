.class Limu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field private final b:Lu;

.field private c:Liqk;

.field private synthetic d:Limt;


# direct methods
.method constructor <init>(Limt;Lu;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 377
    iput-object p1, p0, Limu;->d:Limt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 379
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lu;->j()Liqk;

    move-result-object v1

    .line 380
    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    .line 384
    :cond_0
    iput-object v1, p0, Limu;->c:Liqk;

    .line 385
    iput-object p2, p0, Limu;->b:Lu;

    .line 386
    return-void

    :cond_1
    move-object v1, v0

    .line 379
    goto :goto_0
.end method


# virtual methods
.method protected final A_()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Limu;->b:Lu;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Limu;->b:Lu;

    .line 433
    :cond_0
    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0}, Limt;->e(Limt;)Liln;

    move-result-object v0

    iget-object v0, v0, Liln;->c:Ljava/net/Socket;

    invoke-static {v0}, Limo;->a(Ljava/net/Socket;)V

    .line 434
    iget-object v0, p0, Limu;->d:Limt;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Limt;->a(Limt;I)I

    .line 435
    return-void
.end method

.method protected final a(Lipq;J)V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Limu;->c:Liqk;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Limu;->c:Liqk;

    invoke-virtual {p1}, Lipq;->o()Lipq;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Liqk;->a(Lipq;J)V

    .line 394
    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 401
    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0}, Limt;->b(Limt;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Limu;->d:Limt;

    invoke-static {v1}, Limt;->b(Limt;)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_0
    iget-object v0, p0, Limu;->b:Lu;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Limu;->c:Liqk;

    invoke-interface {v0}, Liqk;->close()V

    .line 407
    :cond_1
    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0, v2}, Limt;->a(Limt;I)I

    .line 408
    if-eqz p1, :cond_3

    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0}, Limt;->c(Limt;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 409
    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0, v2}, Limt;->b(Limt;I)I

    .line 410
    sget-object v0, Limh;->a:Limh;

    iget-object v1, p0, Limu;->d:Limt;

    invoke-static {v1}, Limt;->d(Limt;)Lilo;

    move-result-object v1

    iget-object v2, p0, Limu;->d:Limt;

    invoke-static {v2}, Limt;->e(Limt;)Liln;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Limh;->a(Lilo;Liln;)V

    .line 415
    :cond_2
    :goto_0
    return-void

    .line 411
    :cond_3
    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0}, Limt;->c(Limt;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 412
    iget-object v0, p0, Limu;->d:Limt;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Limt;->a(Limt;I)I

    .line 413
    iget-object v0, p0, Limu;->d:Limt;

    invoke-static {v0}, Limt;->e(Limt;)Liln;

    move-result-object v0

    iget-object v0, v0, Liln;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0
.end method
