.class final Leem;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:[B

.field b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-array v0, p1, [B

    iput-object v0, p0, Leem;->a:[B

    .line 19
    return-void
.end method

.method static b([BII)I
    .locals 3

    .prologue
    .line 121
    aget-byte v0, p0, p1

    and-int/lit16 v1, v0, 0xff

    .line 122
    add-int/lit8 v0, p1, 0x1

    :goto_0
    add-int v2, p1, p2

    if-ge v0, v2, :cond_0

    .line 123
    shl-int/lit8 v1, v1, 0x8

    .line 124
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_0
    return v1
.end method

.method private static c([BII)J
    .locals 6

    .prologue
    .line 130
    aget-byte v0, p0, p1

    and-int/lit16 v0, v0, 0xff

    int-to-long v2, v0

    .line 131
    add-int/lit8 v0, p1, 0x1

    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 132
    const/16 v1, 0x8

    shl-long/2addr v2, v1

    .line 133
    aget-byte v1, p0, v0

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_0
    return-wide v2
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leem;->b([BII)I

    move-result v0

    .line 53
    iget v1, p0, Leem;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Leem;->b:I

    .line 54
    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Leem;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Leem;->b:I

    .line 35
    return-void
.end method

.method public final a([BII)V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 43
    iget v0, p0, Leem;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Leem;->b:I

    .line 44
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 58
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Leem;->b([BII)I

    move-result v0

    .line 59
    iget v1, p0, Leem;->b:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Leem;->b:I

    .line 60
    return v0
.end method

.method public final c()J
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Leem;->c([BII)J

    move-result-wide v0

    .line 65
    iget v2, p0, Leem;->b:I

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Leem;->b:I

    .line 66
    return-wide v0
.end method

.method public final d()I
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Leem;->b([BII)I

    move-result v0

    .line 71
    iget v1, p0, Leem;->b:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Leem;->b:I

    .line 72
    return v0
.end method

.method public final e()J
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Leem;->c([BII)J

    move-result-wide v0

    .line 77
    iget v2, p0, Leem;->b:I

    add-int/lit8 v2, v2, 0x8

    iput v2, p0, Leem;->b:I

    .line 78
    return-wide v0
.end method

.method public final f()I
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Leem;->b([BII)I

    move-result v0

    .line 98
    iget v1, p0, Leem;->b:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Leem;->b:I

    .line 99
    if-gez v0, :cond_0

    .line 100
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Top bit not zero: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :cond_0
    return v0
.end method

.method public final g()J
    .locals 5

    .prologue
    .line 112
    iget-object v0, p0, Leem;->a:[B

    iget v1, p0, Leem;->b:I

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Leem;->c([BII)J

    move-result-wide v0

    .line 113
    iget v2, p0, Leem;->b:I

    add-int/lit8 v2, v2, 0x8

    iput v2, p0, Leem;->b:I

    .line 114
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 115
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x26

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Top bit not zero: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 117
    :cond_0
    return-wide v0
.end method
