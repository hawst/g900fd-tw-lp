.class public final Libo;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Libo;


# instance fields
.field private b:I

.field private c:Libp;

.field private d:Libn;

.field private e:Libr;

.field private f:Ljava/lang/String;

.field private g:I

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Libo;

    sput-object v0, Libo;->a:[Libo;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v1, p0, Libo;->b:I

    iput-object v0, p0, Libo;->c:Libp;

    iput-object v0, p0, Libo;->d:Libn;

    iput-object v0, p0, Libo;->e:Libr;

    const-string v0, ""

    iput-object v0, p0, Libo;->f:Ljava/lang/String;

    iput v1, p0, Libo;->g:I

    const-string v0, ""

    iput-object v0, p0, Libo;->h:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Libo;->b:I

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Libo;->b:I

    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Libo;->c:Libp;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Libo;->c:Libp;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Libo;->d:Libn;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Libo;->d:Libn;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Libo;->e:Libr;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Libo;->e:Libr;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Libo;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Libo;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Libo;->g:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Libo;->g:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Libo;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Libo;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Libo;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libo;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Libo;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Libo;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Libo;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Libo;->b:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Libo;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Libo;->c:Libp;

    if-nez v0, :cond_4

    new-instance v0, Libp;

    invoke-direct {v0}, Libp;-><init>()V

    iput-object v0, p0, Libo;->c:Libp;

    :cond_4
    iget-object v0, p0, Libo;->c:Libp;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Libo;->d:Libn;

    if-nez v0, :cond_5

    new-instance v0, Libn;

    invoke-direct {v0}, Libn;-><init>()V

    iput-object v0, p0, Libo;->d:Libn;

    :cond_5
    iget-object v0, p0, Libo;->d:Libn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Libo;->e:Libr;

    if-nez v0, :cond_6

    new-instance v0, Libr;

    invoke-direct {v0}, Libr;-><init>()V

    iput-object v0, p0, Libo;->e:Libr;

    :cond_6
    iget-object v0, p0, Libo;->e:Libr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libo;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Libo;->g:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libo;->h:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget v0, p0, Libo;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Libo;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_0
    iget-object v0, p0, Libo;->c:Libp;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Libo;->c:Libp;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Libo;->d:Libn;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Libo;->d:Libn;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Libo;->e:Libr;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Libo;->e:Libr;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Libo;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Libo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_4
    iget v0, p0, Libo;->g:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Libo;->g:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_5
    iget-object v0, p0, Libo;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Libo;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_6
    iget-object v0, p0, Libo;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libo;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libo;

    iget v2, p0, Libo;->b:I

    iget v3, p1, Libo;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Libo;->c:Libp;

    if-nez v2, :cond_4

    iget-object v2, p1, Libo;->c:Libp;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Libo;->d:Libn;

    if-nez v2, :cond_5

    iget-object v2, p1, Libo;->d:Libn;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Libo;->e:Libr;

    if-nez v2, :cond_6

    iget-object v2, p1, Libo;->e:Libr;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Libo;->f:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Libo;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget v2, p0, Libo;->g:I

    iget v3, p1, Libo;->g:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Libo;->h:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Libo;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Libo;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Libo;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Libo;->c:Libp;

    iget-object v3, p1, Libo;->c:Libp;

    invoke-virtual {v2, v3}, Libp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Libo;->d:Libn;

    iget-object v3, p1, Libo;->d:Libn;

    invoke-virtual {v2, v3}, Libn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Libo;->e:Libr;

    iget-object v3, p1, Libo;->e:Libr;

    invoke-virtual {v2, v3}, Libr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libo;->f:Ljava/lang/String;

    iget-object v3, p1, Libo;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Libo;->h:Ljava/lang/String;

    iget-object v3, p1, Libo;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Libo;->I:Ljava/util/List;

    iget-object v3, p1, Libo;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libo;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libo;->c:Libp;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libo;->d:Libn;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libo;->e:Libr;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libo;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libo;->g:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libo;->h:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Libo;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Libo;->c:Libp;

    invoke-virtual {v0}, Libp;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Libo;->d:Libn;

    invoke-virtual {v0}, Libn;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Libo;->e:Libr;

    invoke-virtual {v0}, Libr;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Libo;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v0, p0, Libo;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_5
    iget-object v1, p0, Libo;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
