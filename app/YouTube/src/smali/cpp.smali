.class public final Lcpp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lggl;


# instance fields
.field public a:J

.field private b:J

.field private c:I

.field private synthetic d:Lcpo;


# direct methods
.method constructor <init>(Lcpo;)V
    .locals 0

    .prologue
    .line 806
    iput-object p1, p0, Lcpp;->d:Lcpo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IJJ)V
    .locals 2

    .prologue
    .line 814
    iget-wide v0, p0, Lcpp;->b:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lcpp;->b:J

    .line 815
    iget v0, p0, Lcpp;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcpp;->c:I

    .line 816
    iput-wide p4, p0, Lcpp;->a:J

    .line 817
    iget-object v0, p0, Lcpp;->d:Lcpo;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcpo;->a(Lcpo;Z)V

    .line 818
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 821
    iget-wide v0, p0, Lcpp;->b:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    .line 822
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 823
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    .line 824
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcpp;->b:J

    .line 825
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    .line 826
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcpp;->c:I

    int-to-float v1, v1

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    .line 827
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 828
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 822
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 829
    iput-wide v4, p0, Lcpp;->b:J

    .line 830
    const/4 v0, 0x0

    iput v0, p0, Lcpp;->c:I

    .line 832
    :cond_0
    return-void
.end method
