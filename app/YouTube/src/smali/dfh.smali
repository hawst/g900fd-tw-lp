.class public final Ldfh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcxm;


# instance fields
.field final a:Lcwn;

.field public b:Lfoo;

.field c:Lesm;

.field private final d:Ldff;

.field private final e:Lezj;

.field private final f:Lcxl;

.field private final g:Lcwz;

.field private h:Landroid/os/CountDownTimer;

.field private i:Lcxk;

.field private j:Z

.field private k:Z

.field private l:Levn;


# direct methods
.method public constructor <init>(Lcwn;Levn;Ldff;Lezj;Lcxl;Lcwz;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldfh;->l:Levn;

    .line 61
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldff;

    iput-object v0, p0, Ldfh;->d:Ldff;

    .line 62
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Ldfh;->e:Lezj;

    .line 63
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxl;

    iput-object v0, p0, Ldfh;->f:Lcxl;

    .line 64
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwn;

    iput-object v0, p0, Ldfh;->a:Lcwn;

    .line 65
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwz;

    iput-object v0, p0, Ldfh;->g:Lcwz;

    .line 66
    new-instance v0, Ldfi;

    invoke-direct {v0, p0}, Ldfi;-><init>(Ldfh;)V

    invoke-interface {p3, v0}, Ldff;->a(Ldfg;)V

    .line 77
    invoke-direct {p0}, Ldfh;->e()V

    .line 78
    return-void
.end method

.method static synthetic a(Ldfh;J)V
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Ldfh;->b:Lfoo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfh;->b:Lfoo;

    invoke-virtual {v0}, Lfoo;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Ldfh;->b:Lfoo;

    invoke-virtual {v0}, Lfoo;->b()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfos;

    iget-object v0, v0, Lfos;->a:Leah;

    iget v0, v0, Leah;->h:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    sub-long/2addr v0, p1

    iget-object v2, p0, Ldfh;->l:Levn;

    new-instance v3, Lczx;

    invoke-direct {v3, v0, v1}, Lczx;-><init>(J)V

    invoke-virtual {v2, v3}, Levn;->d(Ljava/lang/Object;)V

    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-lez v2, :cond_2

    iget-object v2, p0, Ldfh;->d:Ldff;

    long-to-int v3, p1

    invoke-interface {v2, v3}, Ldff;->a(I)V

    iget-boolean v2, p0, Ldfh;->k:Z

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-boolean v0, p0, Ldfh;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfh;->j:Z

    iget-object v0, p0, Ldfh;->d:Ldff;

    invoke-interface {v0}, Ldff;->b()V

    iget-object v0, p0, Ldfh;->a:Lcwn;

    invoke-virtual {v0}, Lcwn;->e()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Ldfh;->c()V

    goto :goto_0
.end method

.method private static a(Lfoy;)Z
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lfoy;->aj:Lfoo;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ldfh;->f()V

    .line 82
    iget-object v0, p0, Ldfh;->d:Ldff;

    invoke-interface {v0}, Ldff;->a()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldfh;->j:Z

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Ldfh;->b:Lfoo;

    .line 85
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Ldfh;->h:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Ldfh;->h:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Ldfh;->h:Landroid/os/CountDownTimer;

    .line 210
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Ldfh;->i:Lcxk;

    .line 137
    invoke-direct {p0}, Ldfh;->e()V

    .line 138
    return-void
.end method

.method public final a(Lcxk;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 119
    iput-object p1, p0, Ldfh;->i:Lcxk;

    .line 121
    iget-object v0, p0, Ldfh;->b:Lfoo;

    invoke-virtual {v0}, Lfoo;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfos;

    .line 122
    iput-boolean v1, p0, Ldfh;->j:Z

    .line 123
    iget-object v3, p0, Ldfh;->d:Ldff;

    .line 124
    iget-object v4, v0, Lfos;->a:Leah;

    iget-object v4, v4, Leah;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lfos;->b()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v0}, Lfos;->a()Lfox;

    move-result-object v6

    sget-object v7, Lfox;->b:Lfox;

    if-ne v6, v7, :cond_0

    move v1, v2

    .line 123
    :cond_0
    invoke-interface {v3, v4, v5, v1}, Ldff;->a(Ljava/lang/String;Ljava/util/List;Z)V

    .line 125
    iget-object v1, p0, Ldfh;->d:Ldff;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 126
    iget-object v4, v0, Lfos;->a:Leah;

    iget v4, v4, Leah;->h:I

    int-to-long v4, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    long-to-int v3, v4

    .line 125
    invoke-interface {v1, v3}, Ldff;->a(I)V

    .line 127
    iget-object v1, p0, Ldfh;->a:Lcwn;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcwn;->a(J)V

    .line 128
    new-instance v1, Lesm;

    iget-object v3, p0, Ldfh;->e:Lezj;

    invoke-direct {v1, v3}, Lesm;-><init>(Lezj;)V

    iput-object v1, p0, Ldfh;->c:Lesm;

    .line 129
    iget-object v0, v0, Lfos;->a:Leah;

    iget v0, v0, Leah;->h:I

    new-instance v1, Ldfj;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v1, p0, v0}, Ldfj;-><init>(Ldfh;I)V

    iput-object v1, p0, Ldfh;->h:Landroid/os/CountDownTimer;

    iget-object v0, p0, Ldfh;->h:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    iget-object v0, p0, Ldfh;->c:Lesm;

    iget-object v1, v0, Lesm;->a:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v4

    iput-wide v4, v0, Lesm;->b:J

    .line 130
    iget-object v0, p0, Ldfh;->d:Ldff;

    invoke-interface {v0, v2}, Ldff;->a(Z)V

    .line 131
    iget-object v0, p0, Ldfh;->g:Lcwz;

    invoke-interface {v0, v2}, Lcwz;->a(Z)V

    .line 132
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Ldfh;->c:Lesm;

    invoke-virtual {v0}, Lesm;->a()V

    .line 155
    iget-object v0, p0, Ldfh;->a:Lcwn;

    iget-object v1, p0, Ldfh;->c:Lesm;

    invoke-virtual {v0, v1}, Lcwn;->a(Lesm;)V

    .line 156
    iget-object v0, p0, Ldfh;->a:Lcwn;

    invoke-virtual {v0}, Lcwn;->c()V

    .line 157
    invoke-virtual {p0}, Ldfh;->d()V

    .line 158
    return-void
.end method

.method d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 188
    iget-object v0, p0, Ldfh;->g:Lcwz;

    invoke-interface {v0, v1}, Lcwz;->a(Z)V

    .line 189
    invoke-direct {p0}, Ldfh;->f()V

    .line 190
    iget-object v0, p0, Ldfh;->d:Ldff;

    invoke-interface {v0, v1}, Ldff;->a(Z)V

    .line 191
    iget-object v0, p0, Ldfh;->i:Lcxk;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Ldfh;->i:Lcxk;

    invoke-interface {v0}, Lcxk;->a()V

    .line 193
    iput-object v2, p0, Ldfh;->i:Lcxk;

    .line 195
    :cond_0
    iput-object v2, p0, Ldfh;->b:Lfoo;

    .line 196
    return-void
.end method

.method public final handleShowSurveyEvent(Lczv;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 114
    iget-object v0, p1, Lczv;->a:Lesf;

    iget-object v0, v0, Lesf;->b:Lfoy;

    invoke-static {v0}, Ldfh;->a(Lfoy;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldfh;->b:Lfoo;

    iget-object v2, v0, Lfoy;->aj:Lfoo;

    if-eq v1, v2, :cond_1

    :cond_0
    invoke-direct {p0}, Ldfh;->e()V

    invoke-static {v0}, Ldfh;->a(Lfoy;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lfoy;->f()Z

    move-result v1

    iput-boolean v1, p0, Ldfh;->k:Z

    iget-object v0, v0, Lfoy;->aj:Lfoo;

    iput-object v0, p0, Ldfh;->b:Lfoo;

    iget-object v0, p0, Ldfh;->f:Lcxl;

    invoke-interface {v0, p0}, Lcxl;->a(Lcxm;)V

    .line 115
    :cond_1
    return-void
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 107
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->a:Lgol;

    if-ne v0, v1, :cond_0

    .line 108
    invoke-direct {p0}, Ldfh;->e()V

    .line 110
    :cond_0
    return-void
.end method
