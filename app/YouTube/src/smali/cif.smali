.class public Lcif;
.super Lfsb;
.source "SourceFile"


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/View;

.field public final c:Landroid/widget/LinearLayout;

.field public final d:Landroid/content/Context;

.field e:Lfni;

.field private f:Landroid/view/View;

.field private final g:Lcii;

.field private final h:Landroid/view/View;

.field private final i:Lccs;

.field private final j:Landroid/view/View;

.field private final m:Lboi;

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyc;Leyp;Lfhz;Lboi;ILfdw;Lfrz;)V
    .locals 10

    .prologue
    .line 62
    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {p0, p4, v0, v1}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 63
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcif;->d:Landroid/content/Context;

    .line 64
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lboi;

    iput-object v2, p0, Lcif;->m:Lboi;

    .line 67
    const/4 v2, 0x0

    move/from16 v0, p6

    invoke-static {p1, v0, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcif;->h:Landroid/view/View;

    .line 68
    new-instance v2, Lcii;

    iget-object v6, p0, Lcif;->h:Landroid/view/View;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p3

    move-object v7, p4

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v2 .. v9}, Lcii;-><init>(Lcif;Landroid/content/Context;Leyp;Landroid/view/View;Lfhz;Lfdw;Lfrz;)V

    iput-object v2, p0, Lcif;->g:Lcii;

    .line 76
    iget-object v2, p0, Lcif;->h:Landroid/view/View;

    const v3, 0x7f08012c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcif;->a:Landroid/view/View;

    .line 77
    iget-object v2, p0, Lcif;->h:Landroid/view/View;

    const v3, 0x7f080106

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcif;->f:Landroid/view/View;

    .line 78
    iget-object v2, p0, Lcif;->h:Landroid/view/View;

    const v3, 0x7f080269

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcif;->b:Landroid/view/View;

    .line 79
    iget-object v2, p0, Lcif;->h:Landroid/view/View;

    const v3, 0x7f080268

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcif;->c:Landroid/widget/LinearLayout;

    .line 80
    iget-object v2, p0, Lcif;->h:Landroid/view/View;

    const v3, 0x7f080267

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcif;->j:Landroid/view/View;

    .line 82
    new-instance v2, Lccs;

    iget-object v3, p0, Lcif;->h:Landroid/view/View;

    invoke-direct {v2, p3, v3}, Lccs;-><init>(Leyp;Landroid/view/View;)V

    iput-object v2, p0, Lcif;->i:Lccs;

    .line 83
    iget-object v2, p0, Lcif;->h:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v2, p0, Lcif;->j:Landroid/view/View;

    new-instance v3, Lcig;

    invoke-direct {v3, p0, p4}, Lcig;-><init>(Lcif;Lfhz;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    return-void
.end method

.method private a(Lfsg;Lfni;)Landroid/view/View;
    .locals 4

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 100
    iput-object p2, p0, Lcif;->e:Lfni;

    .line 103
    iget-object v0, p0, Lcif;->i:Lccs;

    invoke-virtual {v0, p2}, Lccs;->a(Lfju;)Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcif;->d:Landroid/content/Context;

    invoke-static {v0}, La;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    .line 105
    :goto_0
    iget v1, p0, Lcif;->n:I

    if-eq v0, v1, :cond_0

    .line 106
    iput v0, p0, Lcif;->n:I

    .line 107
    invoke-virtual {p0, v0}, Lcif;->a(I)V

    .line 111
    :cond_0
    invoke-virtual {p2}, Lfni;->g()Lfnm;

    move-result-object v0

    .line 113
    iget-object v1, p0, Lcif;->g:Lcii;

    invoke-virtual {v0}, Lfnm;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcii;->a(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v1, p0, Lcif;->g:Lcii;

    invoke-virtual {v1, v0}, Lcii;->a(Lfnm;)V

    .line 115
    iget-object v1, p0, Lcif;->g:Lcii;

    .line 116
    iget-object v2, v0, Lfnm;->b:Lfnc;

    if-nez v2, :cond_1

    new-instance v2, Lfnc;

    iget-object v3, v0, Lfnm;->a:Liab;

    iget-object v3, v3, Liab;->b:Lhxf;

    invoke-direct {v2, v3}, Lfnc;-><init>(Lhxf;)V

    iput-object v2, v0, Lfnm;->b:Lfnc;

    :cond_1
    iget-object v2, v0, Lfnm;->b:Lfnc;

    .line 117
    iget-object v3, v0, Lfnm;->d:Ljava/lang/CharSequence;

    if-nez v3, :cond_2

    iget-object v3, v0, Lfnm;->a:Liab;

    iget-object v3, v3, Liab;->d:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v0, Lfnm;->d:Ljava/lang/CharSequence;

    :cond_2
    iget-object v0, v0, Lfnm;->d:Ljava/lang/CharSequence;

    .line 115
    invoke-virtual {v1, v2, v0}, Lcii;->a(Lfnc;Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcif;->m:Lboi;

    iget-object v1, p0, Lcif;->g:Lcii;

    .line 121
    iget-object v1, v1, Lcby;->e:Landroid/view/View;

    .line 119
    invoke-static {v0, v1, p2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 124
    iget-object v0, p0, Lcif;->h:Landroid/view/View;

    return-object v0

    .line 104
    :cond_3
    iget-object v0, p0, Lcif;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    check-cast p2, Lfni;

    invoke-direct {p0, p1, p2}, Lcif;->a(Lfsg;Lfni;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    check-cast p2, Lfni;

    invoke-direct {p0, p1, p2}, Lcif;->a(Lfsg;Lfni;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    check-cast p2, Lfni;

    invoke-direct {p0, p1, p2}, Lcif;->a(Lfsg;Lfni;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(I)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 136
    iget-object v0, p0, Lcif;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 137
    iget-object v0, p0, Lcif;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 138
    iget-object v1, p0, Lcif;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 140
    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    .line 141
    iget-object v3, p0, Lcif;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 142
    iget-object v3, p0, Lcif;->a:Landroid/view/View;

    const v4, 0x7f0a00dc

    .line 143
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, p0, Lcif;->a:Landroid/view/View;

    .line 144
    invoke-virtual {v5}, Landroid/view/View;->getPaddingTop()I

    move-result v5

    const v6, 0x7f0a00dd

    .line 145
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    const v7, 0x7f0a00db

    .line 146
    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 142
    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 147
    iget-object v3, p0, Lcif;->b:Landroid/view/View;

    iget-object v4, p0, Lcif;->b:Landroid/view/View;

    .line 148
    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcif;->b:Landroid/view/View;

    .line 150
    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcif;->b:Landroid/view/View;

    .line 151
    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 147
    invoke-virtual {v3, v4, v8, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 152
    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 153
    const v3, 0x7f0b0020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 154
    iput v8, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 155
    const v0, 0x7f0b0021

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 156
    iget-object v0, p0, Lcif;->f:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v3, p0, Lcif;->c:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 159
    iget-object v3, p0, Lcif;->a:Landroid/view/View;

    iget-object v4, p0, Lcif;->a:Landroid/view/View;

    .line 160
    invoke-virtual {v4}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 159
    invoke-virtual {v3, v8, v4, v8, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 161
    iget-object v3, p0, Lcif;->b:Landroid/view/View;

    iget-object v4, p0, Lcif;->b:Landroid/view/View;

    .line 162
    invoke-virtual {v4}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    const v5, 0x7f0a00da

    .line 163
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v5, p0, Lcif;->b:Landroid/view/View;

    .line 164
    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcif;->b:Landroid/view/View;

    .line 165
    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 161
    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 166
    iput v9, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 167
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 168
    iput v9, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 169
    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 170
    iget-object v0, p0, Lcif;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
