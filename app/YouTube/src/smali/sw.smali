.class final Lsw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final a:Landroid/os/Messenger;

.field final b:Ltb;

.field c:I

.field d:I

.field e:I

.field f:I

.field final g:Landroid/util/SparseArray;

.field final synthetic h:Lsv;

.field private final i:Landroid/os/Messenger;


# direct methods
.method public constructor <init>(Lsv;Landroid/os/Messenger;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 394
    iput-object p1, p0, Lsw;->h:Lsv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 386
    iput v0, p0, Lsw;->c:I

    .line 387
    iput v0, p0, Lsw;->d:I

    .line 391
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lsw;->g:Landroid/util/SparseArray;

    .line 395
    iput-object p2, p0, Lsw;->a:Landroid/os/Messenger;

    .line 396
    new-instance v0, Ltb;

    invoke-direct {v0, p0}, Ltb;-><init>(Lsw;)V

    iput-object v0, p0, Lsw;->b:Ltb;

    .line 397
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lsw;->b:Ltb;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lsw;->i:Landroid/os/Messenger;

    .line 398
    return-void
.end method

.method static synthetic a(Lsw;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 381
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lse;

    invoke-virtual {v0, v2, v2}, Lse;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lsw;->g:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 524
    const/4 v1, 0x5

    iget v2, p0, Lsw;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lsw;->c:I

    move-object v0, p0

    move v3, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 526
    return-void
.end method

.method public final a(II)V
    .locals 6

    .prologue
    .line 534
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 535
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 536
    const/4 v1, 0x7

    iget v2, p0, Lsw;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lsw;->c:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 538
    return-void
.end method

.method public final a(Lrl;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 561
    const/16 v1, 0xa

    iget v2, p0, Lsw;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lsw;->c:I

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v4, p1, Lrl;->a:Landroid/os/Bundle;

    :goto_0
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 563
    return-void

    :cond_0
    move-object v4, v5

    .line 561
    goto :goto_0
.end method

.method public final a()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 401
    iget v0, p0, Lsw;->c:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lsw;->c:I

    iput v0, p0, Lsw;->f:I

    .line 402
    iget v2, p0, Lsw;->f:I

    move-object v0, p0

    move v3, v1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_0

    move v1, v6

    .line 414
    :goto_0
    return v1

    .line 409
    :cond_0
    :try_start_0
    iget-object v0, p0, Lsw;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lsw;->binderDied()V

    move v1, v6

    .line 414
    goto :goto_0
.end method

.method a(IIILjava/lang/Object;Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    .line 566
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 567
    iput p1, v0, Landroid/os/Message;->what:I

    .line 568
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 569
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 570
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 571
    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 572
    iget-object v1, p0, Lsw;->i:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 574
    :try_start_0
    iget-object v1, p0, Lsw;->a:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 575
    const/4 v0, 0x1

    .line 583
    :goto_0
    return v0

    .line 578
    :catch_0
    move-exception v0

    .line 579
    const/4 v1, 0x2

    if-eq p1, v1, :cond_0

    .line 580
    const-string v1, "MediaRouteProviderProxy"

    const-string v2, "Could not send message to service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 583
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 582
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final b(II)V
    .locals 6

    .prologue
    .line 541
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 542
    const-string v0, "volume"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 543
    const/16 v1, 0x8

    iget v2, p0, Lsw;->c:I

    add-int/lit8 v0, v2, 0x1

    iput v0, p0, Lsw;->c:I

    const/4 v4, 0x0

    move-object v0, p0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lsw;->a(IIILjava/lang/Object;Landroid/os/Bundle;)Z

    .line 545
    return-void
.end method

.method public final binderDied()V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lsw;->h:Lsv;

    invoke-static {v0}, Lsv;->a(Lsv;)Lta;

    move-result-object v0

    new-instance v1, Lsy;

    invoke-direct {v1, p0}, Lsy;-><init>(Lsw;)V

    invoke-virtual {v0, v1}, Lta;->post(Ljava/lang/Runnable;)Z

    .line 507
    return-void
.end method
