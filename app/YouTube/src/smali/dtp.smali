.class public final Ldtp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcvq;


# instance fields
.field final a:Landroid/os/Handler;

.field private final b:Landroid/content/Context;

.field private final c:Levn;

.field private final d:Ldwq;

.field private final e:Lcwq;

.field private final f:Ldyg;

.field private g:Lfrl;

.field private h:Lgol;

.field private i:I

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Ldwq;Lcwq;Ldyg;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldtp;->b:Landroid/content/Context;

    .line 72
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldtp;->c:Levn;

    .line 73
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldtp;->d:Ldwq;

    .line 74
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwq;

    iput-object v0, p0, Ldtp;->e:Lcwq;

    .line 75
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldtp;->f:Ldyg;

    .line 77
    new-instance v0, Ldtq;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1, p3}, Ldtq;-><init>(Ldtp;Landroid/os/Looper;Ldwq;)V

    iput-object v0, p0, Ldtp;->a:Landroid/os/Handler;

    .line 94
    invoke-direct {p0}, Ldtp;->A()V

    .line 95
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 482
    const/4 v0, 0x0

    iput-object v0, p0, Ldtp;->g:Lfrl;

    .line 483
    sget-object v0, Lgol;->a:Lgol;

    invoke-direct {p0, v0}, Ldtp;->c(Lgol;)V

    .line 484
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Ldtp;->a(IZ)V

    .line 485
    invoke-virtual {p0}, Ldtp;->a()V

    .line 486
    iget-object v0, p0, Ldtp;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 487
    return-void
.end method

.method private B()V
    .locals 4

    .prologue
    .line 517
    iget-object v0, p0, Ldtp;->c:Levn;

    new-instance v1, Ldab;

    iget-boolean v2, p0, Ldtp;->j:Z

    iget-boolean v3, p0, Ldtp;->k:Z

    invoke-direct {v1, v2, v3}, Ldab;-><init>(ZZ)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 518
    return-void
.end method

.method private a(IZ)V
    .locals 3

    .prologue
    .line 462
    iget v0, p0, Ldtp;->i:I

    if-ne v0, p1, :cond_0

    .line 468
    :goto_0
    return-void

    .line 465
    :cond_0
    iput p1, p0, Ldtp;->i:I

    .line 466
    iget-object v0, p0, Ldtp;->f:Ldyg;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "MdxDirector PlayerState move to : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 467
    iget-object v0, p0, Ldtp;->c:Levn;

    new-instance v1, Ldae;

    invoke-direct {v1, p1, p2}, Ldae;-><init>(IZ)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Lcyv;)V
    .locals 3

    .prologue
    .line 491
    iget-object v0, p0, Ldtp;->c:Levn;

    new-instance v1, Lcyu;

    iget-object v2, p0, Ldtp;->d:Ldwq;

    invoke-interface {v2}, Ldwq;->t()Lfoy;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcyu;-><init>(Lfoy;Lcyv;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 492
    return-void
.end method

.method private a(Ldwo;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 412
    iget-object v0, p0, Ldtp;->f:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MdxDirector: handle MDx player state "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 413
    sget-object v0, Ldtr;->b:[I

    invoke-virtual {p1}, Ldwo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 440
    :goto_0
    sget-object v0, Ldwo;->c:Ldwo;

    if-ne p1, v0, :cond_2

    .line 449
    invoke-virtual {p0}, Ldtp;->a()V

    .line 450
    iget-object v0, p0, Ldtp;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 451
    iget-object v0, p0, Ldtp;->a:Landroid/os/Handler;

    iget-object v1, p0, Ldtp;->a:Landroid/os/Handler;

    .line 452
    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    .line 451
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 459
    :cond_0
    :goto_1
    return-void

    .line 416
    :pswitch_0
    const/4 v0, 0x5

    invoke-direct {p0, v0, v4}, Ldtp;->a(IZ)V

    goto :goto_0

    .line 419
    :pswitch_1
    iget-object v0, p0, Ldtp;->h:Lgol;

    invoke-virtual {v0}, Lgol;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 420
    sget-object v0, Lcyv;->a:Lcyv;

    invoke-direct {p0, v0}, Ldtp;->a(Lcyv;)V

    .line 422
    :cond_1
    const/4 v0, 0x7

    invoke-direct {p0, v0, v5}, Ldtp;->a(IZ)V

    goto :goto_0

    .line 425
    :pswitch_2
    const/4 v0, 0x3

    invoke-direct {p0, v0, v4}, Ldtp;->a(IZ)V

    goto :goto_0

    .line 428
    :pswitch_3
    const/4 v0, 0x2

    invoke-direct {p0, v0, v4}, Ldtp;->a(IZ)V

    goto :goto_0

    .line 431
    :pswitch_4
    sget-object v0, Lcyv;->c:Lcyv;

    invoke-direct {p0, v0}, Ldtp;->a(Lcyv;)V

    goto :goto_0

    .line 434
    :pswitch_5
    invoke-direct {p0}, Ldtp;->z()V

    .line 435
    const/16 v0, 0x8

    invoke-direct {p0, v0, v5}, Ldtp;->a(IZ)V

    goto :goto_0

    .line 439
    :pswitch_6
    const/4 v0, 0x4

    invoke-direct {p0, v0, v5}, Ldtp;->a(IZ)V

    goto :goto_0

    .line 455
    :cond_2
    iget-object v0, p0, Ldtp;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 456
    iget-object v0, p0, Ldtp;->a:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    .line 413
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private a(Ldxb;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 365
    iget-object v0, p0, Ldtp;->f:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MdxDirector: handle MDx video stage "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 366
    sget-object v0, Ldtr;->a:[I

    invoke-virtual {p1}, Ldxb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 389
    :goto_0
    return-void

    .line 368
    :pswitch_0
    iget-object v0, p0, Ldtp;->g:Lfrl;

    if-eqz v0, :cond_0

    .line 369
    sget-object v0, Lgol;->b:Lgol;

    invoke-direct {p0, v0}, Ldtp;->c(Lgol;)V

    .line 373
    :goto_1
    iput v4, p0, Ldtp;->i:I

    goto :goto_0

    .line 371
    :cond_0
    sget-object v0, Lgol;->a:Lgol;

    invoke-direct {p0, v0}, Ldtp;->c(Lgol;)V

    goto :goto_1

    .line 376
    :pswitch_1
    sget-object v0, Lgol;->f:Lgol;

    invoke-direct {p0, v0}, Ldtp;->c(Lgol;)V

    .line 377
    iput v4, p0, Ldtp;->i:I

    .line 378
    invoke-virtual {p0}, Ldtp;->a()V

    goto :goto_0

    .line 381
    :pswitch_2
    sget-object v0, Lgol;->i:Lgol;

    invoke-direct {p0, v0}, Ldtp;->c(Lgol;)V

    .line 382
    iput v4, p0, Ldtp;->i:I

    .line 383
    invoke-virtual {p0}, Ldtp;->a()V

    goto :goto_0

    .line 386
    :pswitch_3
    sget-object v0, Lgol;->j:Lgol;

    invoke-direct {p0, v0}, Ldtp;->c(Lgol;)V

    goto :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->b:Ldww;

    if-eq v0, v1, :cond_0

    .line 133
    :goto_0
    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ldtp;->h:Lgol;

    .line 127
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    invoke-direct {p0, v0}, Ldtp;->a(Ldxb;)V

    .line 128
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->m()Ldwo;

    move-result-object v0

    invoke-direct {p0, v0}, Ldtp;->a(Ldwo;)V

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->l()Ldwo;

    move-result-object v0

    invoke-direct {p0, v0}, Ldtp;->a(Ldwo;)V

    goto :goto_0
.end method

.method private c(Lgol;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 392
    iget-object v0, p0, Ldtp;->h:Lgol;

    if-ne v0, p1, :cond_0

    .line 405
    :goto_0
    return-void

    .line 395
    :cond_0
    iput-object p1, p0, Ldtp;->h:Lgol;

    .line 396
    iget-object v0, p0, Ldtp;->f:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MdxDirector VideoStage move to : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 398
    iget-object v7, p0, Ldtp;->c:Levn;

    new-instance v0, Ldac;

    iget-object v2, p0, Ldtp;->g:Lfrl;

    const/4 v3, 0x0

    iget-object v1, p0, Ldtp;->d:Ldwq;

    .line 402
    invoke-interface {v1}, Ldwq;->t()Lfoy;

    move-result-object v4

    move-object v1, p1

    move v6, v5

    invoke-direct/range {v0 .. v6}, Ldac;-><init>(Lgol;Lfrl;Ljava/lang/String;Lfoy;ZZ)V

    .line 398
    invoke-virtual {v7, v0}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private y()V
    .locals 3

    .prologue
    .line 177
    iget-object v0, p0, Ldtp;->g:Lfrl;

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Ldtp;->f:Ldyg;

    const-string v1, "MdxDirector: can not fling video, missing playerResponse"

    invoke-virtual {v0, v1}, Ldyg;->c(Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 182
    :cond_0
    iget-object v0, p0, Ldtp;->d:Ldwq;

    new-instance v1, Ldwk;

    invoke-direct {v1}, Ldwk;-><init>()V

    iget-object v2, p0, Ldtp;->g:Lfrl;

    .line 184
    iget-object v2, v2, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldwk;->a(Ljava/lang/String;)Ldwk;

    move-result-object v1

    iget-object v2, p0, Ldtp;->e:Lcwq;

    .line 185
    invoke-virtual {v2}, Lcwq;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldwk;->b(Ljava/lang/String;)Ldwk;

    move-result-object v1

    const/4 v2, 0x1

    .line 186
    invoke-virtual {v1, v2}, Ldwk;->a(I)Ldwk;

    move-result-object v1

    .line 187
    invoke-virtual {v1}, Ldwk;->a()Ldwj;

    move-result-object v1

    .line 182
    invoke-interface {v0, v1}, Ldwq;->a(Ldwj;)V

    goto :goto_0
.end method

.method private z()V
    .locals 6

    .prologue
    .line 475
    iget-object v0, p0, Ldtp;->c:Levn;

    new-instance v1, Lczb;

    sget-object v2, Lczc;->c:Lczc;

    sget-object v3, Ldwh;->h:Ldwh;

    .line 477
    iget-boolean v3, v3, Ldwh;->k:Z

    iget-object v4, p0, Ldtp;->b:Landroid/content/Context;

    sget-object v5, Ldwh;->h:Ldwh;

    .line 478
    iget v5, v5, Ldwh;->j:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lczb;-><init>(Lczc;ZLjava/lang/String;)V

    .line 475
    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 479
    return-void
.end method


# virtual methods
.method a()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 318
    sget-object v0, Ldtr;->a:[I

    iget-object v1, p0, Ldtp;->d:Ldwq;

    invoke-interface {v1}, Ldwq;->k()Ldxb;

    move-result-object v1

    invoke-virtual {v1}, Ldxb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 336
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 324
    :pswitch_0
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->s()J

    move-result-wide v0

    long-to-int v1, v0

    .line 325
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->t()Lfoy;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->t()Lfoy;

    move-result-object v0

    iget v0, v0, Lfoy;->o:I

    mul-int/lit16 v0, v0, 0x3e8

    :goto_0
    move v2, v1

    .line 338
    :goto_1
    iget-object v9, p0, Ldtp;->c:Levn;

    new-instance v1, Ldad;

    int-to-long v2, v2

    int-to-long v4, v0

    const-wide/16 v6, 0x0

    invoke-direct/range {v1 .. v8}, Ldad;-><init>(JJJZ)V

    invoke-virtual {v9, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 339
    :pswitch_1
    return-void

    :cond_0
    move v0, v8

    .line 325
    goto :goto_0

    .line 328
    :pswitch_2
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->r()J

    move-result-wide v0

    long-to-int v1, v0

    .line 329
    iget-object v0, p0, Ldtp;->g:Lfrl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldtp;->g:Lfrl;

    invoke-virtual {v0}, Lfrl;->c()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    :goto_2
    move v2, v1

    .line 330
    goto :goto_1

    :cond_1
    move v0, v8

    .line 329
    goto :goto_2

    .line 332
    :pswitch_3
    iget-object v0, p0, Ldtp;->g:Lfrl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldtp;->g:Lfrl;

    invoke-virtual {v0}, Lfrl;->c()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    :goto_3
    move v2, v0

    .line 334
    goto :goto_1

    :cond_2
    move v0, v8

    .line 332
    goto :goto_3

    .line 318
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(F)V
    .locals 3

    .prologue
    const/16 v0, 0x64

    .line 237
    float-to-int v1, p1

    mul-int/lit8 v1, v1, 0x64

    .line 238
    iget-object v2, p0, Ldtp;->d:Ldwq;

    if-le v1, v0, :cond_0

    :goto_0
    invoke-interface {v2, v0}, Ldwq;->b(I)V

    .line 239
    return-void

    :cond_0
    move v0, v1

    .line 238
    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Ldtp;->d:Ldwq;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-interface {v0, v1}, Ldwq;->a(I)V

    .line 249
    return-void
.end method

.method public final a(Lcvr;)V
    .locals 0

    .prologue
    .line 525
    return-void
.end method

.method public final a(Lfrl;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 137
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v3, Ldww;->b:Ldww;

    if-eq v0, v3, :cond_0

    .line 174
    :goto_0
    return-void

    .line 140
    :cond_0
    iput-object p1, p0, Ldtp;->g:Lfrl;

    .line 141
    iget-object v0, p0, Ldtp;->f:Ldyg;

    const-string v3, "MdxDirector load videoId: %s, playlistId: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 143
    iget-object v5, p1, Lfrl;->a:Lhro;

    invoke-static {v5}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    iget-object v5, p0, Ldtp;->e:Lcwq;

    .line 144
    invoke-virtual {v5}, Lcwq;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 141
    invoke-virtual {v0, v3, v4}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    sget-object v0, Lgol;->b:Lgol;

    invoke-direct {p0, v0}, Ldtp;->c(Lgol;)V

    .line 147
    invoke-virtual {p1}, Lfrl;->g()Lflo;

    move-result-object v0

    invoke-virtual {v0}, Lflo;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    invoke-direct {p0}, Ldtp;->z()V

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p1, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v3

    .line 155
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 156
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 166
    :goto_1
    if-eqz v0, :cond_3

    .line 167
    iget-object v0, p0, Ldtp;->f:Ldyg;

    const-string v4, "MdxDirector: remote screen already play videoId %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-virtual {v0, v4, v1}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    invoke-direct {p0}, Ldtp;->b()V

    goto :goto_0

    .line 159
    :cond_2
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 161
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 162
    goto :goto_1

    .line 173
    :cond_3
    invoke-direct {p0}, Ldtp;->y()V

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final a(Lgec;)V
    .locals 0

    .prologue
    .line 566
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 556
    return-void
.end method

.method public final a(ZI)V
    .locals 0

    .prologue
    .line 545
    return-void
.end method

.method public final a(Lgol;)Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Ldtp;->h:Lgol;

    invoke-virtual {v0, p1}, Lgol;->a(Lgol;)Z

    move-result v0

    return v0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 253
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->r()J

    move-result-wide v0

    int-to-long v2, p1

    add-long/2addr v0, v2

    .line 254
    long-to-int v0, v0

    invoke-virtual {p0, v0}, Ldtp;->a(I)V

    .line 255
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 264
    .line 265
    invoke-direct {p0}, Ldtp;->A()V

    .line 266
    return-void
.end method

.method public final b(ZI)V
    .locals 0

    .prologue
    .line 530
    return-void
.end method

.method public final b(Lgol;)Z
    .locals 3

    .prologue
    .line 303
    iget-object v0, p0, Ldtp;->h:Lgol;

    const/4 v1, 0x1

    new-array v1, v1, [Lgol;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lgol;->a([Lgol;)Z

    move-result v0

    return v0
.end method

.method public final c()Lcwn;
    .locals 1

    .prologue
    .line 588
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 551
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 540
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ldtp;->b()V

    .line 119
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 502
    iget-boolean v0, p0, Ldtp;->j:Z

    if-eq p1, v0, :cond_0

    .line 503
    iput-boolean p1, p0, Ldtp;->j:Z

    .line 504
    invoke-direct {p0}, Ldtp;->B()V

    .line 506
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    sget-object v1, Ldxb;->a:Ldxb;

    if-eq v0, v1, :cond_1

    .line 193
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->a()V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    sget-object v0, Lgol;->b:Lgol;

    invoke-virtual {p0, v0}, Ldtp;->b(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-direct {p0}, Ldtp;->y()V

    goto :goto_0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 510
    iget-boolean v0, p0, Ldtp;->k:Z

    if-eq p1, v0, :cond_0

    .line 511
    iput-boolean p1, p0, Ldtp;->k:Z

    .line 512
    invoke-direct {p0}, Ldtp;->B()V

    .line 514
    :cond_0
    return-void
.end method

.method public final f(Z)Lcvr;
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    sget-object v1, Ldxb;->a:Ldxb;

    if-eq v0, v1, :cond_0

    .line 204
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->a()V

    .line 206
    :cond_0
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    sget-object v1, Ldxb;->c:Ldxb;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final handleMdxAdPlayerStateChangedEvent(Ldwa;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 359
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lgol;->b:Lgol;

    invoke-virtual {p0, v0}, Ldtp;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p1, Ldwa;->a:Ldwo;

    invoke-direct {p0, v0}, Ldtp;->a(Ldwo;)V

    .line 362
    :cond_0
    return-void
.end method

.method public final handleMdxVideoPlayerStateChangedEvent(Ldxa;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 352
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    sget-object v1, Ldxb;->c:Ldxb;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    sget-object v0, Lgol;->b:Lgol;

    invoke-virtual {p0, v0}, Ldtp;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p1, Ldxa;->a:Ldwo;

    invoke-direct {p0, v0}, Ldtp;->a(Ldwo;)V

    .line 355
    :cond_0
    return-void

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final handleMdxVideoStageEvent(Ldxc;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 345
    sget-object v0, Lgol;->b:Lgol;

    invoke-virtual {p0, v0}, Ldtp;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p1, Ldxc;->a:Ldxb;

    invoke-direct {p0, v0}, Ldtp;->a(Ldxb;)V

    .line 348
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->b()V

    .line 221
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 227
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 243
    invoke-direct {p0}, Ldtp;->A()V

    .line 244
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 561
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->u()V

    .line 260
    return-void
.end method

.method public final o()V
    .locals 0

    .prologue
    .line 571
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final r()I
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->r()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 280
    sget-object v0, Lgol;->b:Lgol;

    invoke-virtual {p0, v0}, Ldtp;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldtp;->g:Lfrl;

    .line 281
    invoke-virtual {v0}, Lfrl;->c()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Ldtp;->d:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    sget-object v1, Ldxb;->d:Ldxb;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Lcxl;
    .locals 1

    .prologue
    .line 593
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final v()Lcvk;
    .locals 1

    .prologue
    .line 598
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final w()V
    .locals 0

    .prologue
    .line 535
    return-void
.end method

.method public final x()V
    .locals 0

    .prologue
    .line 313
    invoke-direct {p0}, Ldtp;->A()V

    .line 315
    return-void
.end method
