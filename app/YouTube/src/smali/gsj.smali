.class public final Lgsj;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field private static final c:Lgsk;

.field private static final d:Landroid/util/SparseArray;


# instance fields
.field public a:Lgrs;

.field public b:J

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/animation/ObjectAnimator;

.field private g:F

.field private h:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Lgsk;

    invoke-direct {v0}, Lgsk;-><init>()V

    sput-object v0, Lgsj;->c:Lgsk;

    .line 58
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lgsj;->d:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 69
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    .line 66
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lgsj;->g:F

    .line 67
    iget v0, p0, Lgsj;->g:F

    iput v0, p0, Lgsj;->h:F

    .line 70
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 71
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 72
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 73
    sget-object v0, Lgsj;->c:Lgsk;

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lgsj;->g:F

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    .line 74
    return-void
.end method

.method private static a(FFFFF)F
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 233
    cmpg-float v0, p1, p2

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 234
    cmpg-float v0, p3, p4

    if-gez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lb;->b(Z)V

    .line 235
    cmpg-float v0, p0, p1

    if-gtz v0, :cond_2

    .line 243
    :goto_2
    return p3

    :cond_0
    move v0, v2

    .line 233
    goto :goto_0

    :cond_1
    move v1, v2

    .line 234
    goto :goto_1

    .line 238
    :cond_2
    cmpl-float v0, p0, p2

    if-ltz v0, :cond_3

    move p3, p4

    .line 239
    goto :goto_2

    .line 242
    :cond_3
    sub-float v0, p0, p1

    sub-float v1, p2, p1

    div-float/2addr v0, v1

    .line 243
    sub-float v1, p4, p3

    mul-float/2addr v0, v1

    add-float/2addr p3, v0

    goto :goto_2
.end method

.method static synthetic a(Lgsj;)F
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lgsj;->h:F

    return v0
.end method

.method static synthetic a(Lgsj;F)F
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lgsj;->h:F

    return p1
.end method

.method private a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lgsj;->a:Lgrs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgsj;->a:Lgrs;

    invoke-virtual {v0}, Lgrs;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(I)Landroid/graphics/ColorMatrixColorFilter;
    .locals 5

    .prologue
    .line 247
    sget-object v1, Lgsj;->d:Landroid/util/SparseArray;

    monitor-enter v1

    .line 248
    const/4 v0, 0x0

    const/16 v2, 0xff

    :try_start_0
    invoke-static {v2, p0}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 249
    sget-object v0, Lgsj;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/ColorMatrixColorFilter;

    .line 250
    if-nez v0, :cond_0

    .line 251
    new-instance v3, Landroid/graphics/ColorMatrix;

    invoke-direct {v3}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 252
    int-to-float v0, v2

    const/high16 v4, 0x437f0000    # 255.0f

    div-float/2addr v0, v4

    invoke-virtual {v3, v0}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 253
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v0, v3}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    .line 254
    sget-object v3, Lgsj;->d:Landroid/util/SparseArray;

    invoke-virtual {v3, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 256
    :cond_0
    monitor-exit v1

    return-object v0

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 125
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 126
    iget-object v1, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 128
    iget v1, p0, Lgsj;->h:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 129
    iput v0, p0, Lgsj;->h:F

    .line 130
    invoke-virtual {p0}, Lgsj;->invalidateSelf()V

    .line 132
    :cond_0
    iput v0, p0, Lgsj;->g:F

    .line 133
    return-void
.end method

.method public final a(FI)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 143
    invoke-static {p1, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 144
    iget v1, p0, Lgsj;->g:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_0

    .line 157
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v1, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 149
    iget-object v1, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 150
    iget-object v1, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    int-to-long v2, p2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 151
    iget-object v1, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 152
    iget v1, p0, Lgsj;->h:F

    const v2, 0x3f333333    # 0.7f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    cmpl-float v1, v0, v4

    if-nez v1, :cond_1

    .line 153
    iget-object v1, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 155
    :cond_1
    iget-object v1, p0, Lgsj;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 156
    iput v0, p0, Lgsj;->g:F

    goto :goto_0
.end method

.method public final a(Lgrs;)V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lgsj;->a:Lgrs;

    if-eq p1, v0, :cond_1

    .line 93
    iget-object v0, p0, Lgsj;->a:Lgrs;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lgsj;->a:Lgrs;

    invoke-virtual {v0}, Lgrs;->c()V

    .line 96
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lgrs;->b()Lgrs;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lgsj;->a:Lgrs;

    .line 97
    invoke-virtual {p0}, Lgsj;->invalidateSelf()V

    .line 99
    :cond_1
    return-void

    .line 96
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x3f333333    # 0.7f

    const/4 v6, 0x0

    .line 163
    invoke-virtual {p0}, Lgsj;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v1, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    .line 169
    iget v2, p0, Lgsj;->h:F

    invoke-static {v2, v6, v7, v6, v8}, Lgsj;->a(FFFFF)F

    move-result v2

    .line 170
    iget v3, p0, Lgsj;->h:F

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-static {v3, v6, v7, v4, v8}, Lgsj;->a(FFFFF)F

    move-result v3

    .line 171
    iget v4, p0, Lgsj;->h:F

    const/high16 v5, 0x437f0000    # 255.0f

    invoke-static {v4, v7, v8, v6, v5}, Lgsj;->a(FFFFF)F

    move-result v4

    float-to-int v4, v4

    .line 173
    iget-object v5, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-static {v4}, Lgsj;->a(I)Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 174
    iget-object v4, p0, Lgsj;->e:Landroid/graphics/Paint;

    int-to-float v5, v1

    mul-float/2addr v2, v5

    float-to-int v2, v2

    invoke-virtual {v4, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 175
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 176
    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v4

    invoke-virtual {p1, v3, v3, v2, v4}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 177
    invoke-direct {p0}, Lgsj;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 178
    if-eqz v2, :cond_1

    .line 179
    const/4 v3, 0x0

    iget-object v4, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 183
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 184
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_0

    .line 181
    :cond_1
    iget-object v2, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 3

    .prologue
    .line 209
    invoke-direct {p0}, Lgsj;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    const/16 v2, 0xff

    if-lt v1, v2, :cond_0

    iget v1, p0, Lgsj;->h:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    :cond_0
    const/4 v0, -0x3

    .line 213
    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    .line 190
    if-eq p1, v0, :cond_0

    .line 191
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 192
    invoke-virtual {p0}, Lgsj;->invalidateSelf()V

    .line 194
    :cond_0
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lgsj;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 204
    invoke-virtual {p0}, Lgsj;->invalidateSelf()V

    .line 205
    return-void
.end method
