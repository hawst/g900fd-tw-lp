.class public final Lgvj;
.super Lidh;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 313
    invoke-direct {p0}, Lidh;-><init>()V

    .line 322
    const-string v0, ""

    iput-object v0, p0, Lgvj;->a:Ljava/lang/String;

    .line 325
    const-string v0, ""

    iput-object v0, p0, Lgvj;->b:Ljava/lang/String;

    .line 328
    const/4 v0, 0x0

    iput v0, p0, Lgvj;->c:I

    .line 331
    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lgvj;->d:[Ljava/lang/String;

    .line 313
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 377
    .line 378
    iget-object v0, p0, Lgvj;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 379
    const/4 v0, 0x1

    iget-object v2, p0, Lgvj;->a:Ljava/lang/String;

    .line 380
    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 382
    :goto_0
    iget-object v2, p0, Lgvj;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 383
    const/4 v2, 0x2

    iget-object v3, p0, Lgvj;->b:Ljava/lang/String;

    .line 384
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 386
    :cond_0
    iget v2, p0, Lgvj;->c:I

    if-eqz v2, :cond_1

    .line 387
    const/4 v2, 0x3

    iget v3, p0, Lgvj;->c:I

    .line 388
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 390
    :cond_1
    iget-object v2, p0, Lgvj;->d:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgvj;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 392
    iget-object v3, p0, Lgvj;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 394
    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 392
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 396
    :cond_2
    add-int/2addr v0, v2

    .line 397
    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 399
    :cond_3
    iput v0, p0, Lgvj;->J:I

    .line 400
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 309
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lidj;->a(Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvj;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgvj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->i()I

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    :cond_1
    iput v0, p0, Lgvj;->c:I

    goto :goto_0

    :cond_2
    iput v3, p0, Lgvj;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lgvj;->d:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lgvj;->d:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 359
    iget-object v0, p0, Lgvj;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    const/4 v0, 0x1

    iget-object v1, p0, Lgvj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 362
    :cond_0
    iget-object v0, p0, Lgvj;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 363
    const/4 v0, 0x2

    iget-object v1, p0, Lgvj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 365
    :cond_1
    iget v0, p0, Lgvj;->c:I

    if-eqz v0, :cond_2

    .line 366
    const/4 v0, 0x3

    iget v1, p0, Lgvj;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 368
    :cond_2
    iget-object v0, p0, Lgvj;->d:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 369
    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 370
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 373
    :cond_3
    return-void
.end method

.method public final b()Lgvj;
    .locals 2

    .prologue
    .line 346
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgvj;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 351
    iget-object v1, p0, Lgvj;->d:[Ljava/lang/String;

    invoke-virtual {v1}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    iput-object v1, v0, Lgvj;->d:[Ljava/lang/String;

    .line 353
    :cond_0
    return-object v0

    .line 347
    :catch_0
    move-exception v0

    .line 348
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lgvj;->b()Lgvj;

    move-result-object v0

    return-object v0
.end method
