.class public final Lhfc;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhfc;


# instance fields
.field public b:I

.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43409
    const/4 v0, 0x0

    new-array v0, v0, [Lhfc;

    sput-object v0, Lhfc;->a:[Lhfc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43410
    invoke-direct {p0}, Lidf;-><init>()V

    .line 43413
    iput v0, p0, Lhfc;->b:I

    .line 43416
    iput v0, p0, Lhfc;->c:I

    .line 43410
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 43459
    const/4 v0, 0x0

    .line 43460
    iget v1, p0, Lhfc;->b:I

    if-eqz v1, :cond_0

    .line 43461
    const/4 v0, 0x1

    iget v1, p0, Lhfc;->b:I

    .line 43462
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 43464
    :cond_0
    iget v1, p0, Lhfc;->c:I

    if-eqz v1, :cond_1

    .line 43465
    const/4 v1, 0x2

    iget v2, p0, Lhfc;->c:I

    .line 43466
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 43468
    :cond_1
    iget-object v1, p0, Lhfc;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43469
    iput v0, p0, Lhfc;->J:I

    .line 43470
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 43406
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfc;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfc;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfc;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhfc;->b:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhfc;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 43447
    iget v0, p0, Lhfc;->b:I

    if-eqz v0, :cond_0

    .line 43448
    const/4 v0, 0x1

    iget v1, p0, Lhfc;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 43450
    :cond_0
    iget v0, p0, Lhfc;->c:I

    if-eqz v0, :cond_1

    .line 43451
    const/4 v0, 0x2

    iget v1, p0, Lhfc;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 43453
    :cond_1
    iget-object v0, p0, Lhfc;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 43455
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43428
    if-ne p1, p0, :cond_1

    .line 43433
    :cond_0
    :goto_0
    return v0

    .line 43429
    :cond_1
    instance-of v2, p1, Lhfc;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 43430
    :cond_2
    check-cast p1, Lhfc;

    .line 43431
    iget v2, p0, Lhfc;->b:I

    iget v3, p1, Lhfc;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhfc;->c:I

    iget v3, p1, Lhfc;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhfc;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfc;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 43433
    goto :goto_0

    .line 43431
    :cond_4
    iget-object v2, p0, Lhfc;->I:Ljava/util/List;

    iget-object v3, p1, Lhfc;->I:Ljava/util/List;

    .line 43433
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 43437
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 43439
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhfc;->b:I

    add-int/2addr v0, v1

    .line 43440
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhfc;->c:I

    add-int/2addr v0, v1

    .line 43441
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhfc;->I:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 43442
    return v0

    .line 43441
    :cond_0
    iget-object v0, p0, Lhfc;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_0
.end method
