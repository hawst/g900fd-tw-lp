.class final Lckw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Leuc;

.field private synthetic b:Lckv;


# direct methods
.method constructor <init>(Lckv;Leuc;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lckw;->b:Lckv;

    iput-object p2, p0, Lckw;->a:Leuc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 70
    .line 71
    :try_start_0
    new-instance v1, Ljava/io/FileReader;

    iget-object v0, p0, Lckw;->b:Lckv;

    iget-object v0, v0, Lckv;->a:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :try_start_1
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 76
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 79
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    :goto_1
    :try_start_2
    const-string v2, "Unable to read from file"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 85
    iget-object v0, p0, Lckw;->a:Leuc;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 90
    iget-object v0, p0, Lckw;->b:Lckv;

    invoke-static {v0, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    .line 91
    :goto_2
    return-void

    .line 82
    :cond_0
    :try_start_3
    iget-object v0, p0, Lckw;->a:Leuc;

    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v2}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 90
    iget-object v0, p0, Lckw;->b:Lckv;

    invoke-static {v0, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    goto :goto_2

    .line 86
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 87
    :goto_3
    :try_start_4
    const-string v2, "Unable to read from file"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 88
    iget-object v0, p0, Lckw;->a:Leuc;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 90
    iget-object v0, p0, Lckw;->b:Lckv;

    invoke-static {v0, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    iget-object v2, p0, Lckw;->b:Lckv;

    invoke-static {v2, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 86
    :catch_2
    move-exception v0

    goto :goto_3

    .line 83
    :catch_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method
