.class final Lgdo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lebt;


# instance fields
.field private synthetic a:Lgdj;


# direct methods
.method constructor <init>(Lgdj;)V
    .locals 0

    .prologue
    .line 1468
    iput-object p1, p0, Lgdo;->a:Lgdj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 1481
    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0, p1}, Lgdj;->a(Lgdj;I)I

    .line 1482
    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0, p2}, Lgdj;->b(Lgdj;I)I

    .line 1483
    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0}, Lgdj;->i(Lgdj;)Lgec;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0}, Lgdj;->i(Lgdj;)Lgec;

    move-result-object v0

    invoke-interface {v0}, Lgec;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1485
    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0}, Lgdj;->i(Lgdj;)Lgec;

    move-result-object v0

    iget-object v1, p0, Lgdo;->a:Lgdj;

    invoke-static {v1}, Lgdj;->g(Lgdj;)I

    move-result v1

    iget-object v2, p0, Lgdo;->a:Lgdj;

    invoke-static {v2}, Lgdj;->h(Lgdj;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lgec;->a(II)V

    .line 1487
    :cond_0
    return-void
.end method

.method public final a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 1

    .prologue
    .line 1498
    const-string v0, "Video CryptoError with ExoPlayer."

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1499
    return-void
.end method

.method public final a(Lebi;)V
    .locals 1

    .prologue
    .line 1492
    const-string v0, "Error with ExoPlayer video decoder initialization."

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1493
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1473
    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0}, Lgdj;->i(Lgdj;)Lgec;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1474
    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0}, Lgdj;->i(Lgdj;)Lgec;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgec;->a(I)V

    .line 1476
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1503
    iget-object v0, p0, Lgdo;->a:Lgdj;

    invoke-static {v0}, Lgdj;->c(Lgdj;)Lgei;

    move-result-object v0

    invoke-interface {v0, p1}, Lgei;->e(I)V

    .line 1504
    return-void
.end method
