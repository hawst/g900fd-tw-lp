.class public final Leyi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 32
    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Leyi;->a:Ljava/util/Set;

    .line 31
    return-void
.end method

.method private static a(Ljava/util/Iterator;)Leyo;
    .locals 1

    .prologue
    .line 159
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfag;

    invoke-virtual {v0}, Lfag;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyo;

    .line 161
    if-eqz v0, :cond_0

    .line 167
    :goto_1
    return-object v0

    .line 165
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 167
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 29
    sget-object v0, Leyi;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Leyi;->a(Ljava/util/Iterator;)Leyo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p0}, Leyo;->c(Landroid/widget/ImageView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 29
    sget-object v0, Leyi;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Leyi;->a(Ljava/util/Iterator;)Leyo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p0, p1}, Leyo;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static a(Leyo;)V
    .locals 2

    .prologue
    .line 114
    sget-object v0, Leyi;->a:Ljava/util/Set;

    new-instance v1, Lfag;

    invoke-direct {v1, p0}, Lfag;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    return-void
.end method

.method public static a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Leyi;->a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;Leyo;)V

    .line 186
    return-void
.end method

.method public static a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;Leyo;)V
    .locals 2

    .prologue
    .line 202
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    new-instance v0, Leyn;

    invoke-virtual {p2}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Leyn;-><init>(Landroid/content/Context;)V

    invoke-static {p0, v0, p1, p2, p3}, Leyi;->a(Leyp;Leym;Landroid/net/Uri;Landroid/widget/ImageView;Leyo;)V

    .line 204
    return-void
.end method

.method public static a(Leyp;Leym;Landroid/net/Uri;Landroid/widget/ImageView;Leyo;)V
    .locals 3

    .prologue
    .line 212
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 214
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    new-instance v1, Leyk;

    invoke-direct {v1, p3, p1, p4}, Leyk;-><init>(Landroid/widget/ImageView;Leym;Leyo;)V

    .line 218
    invoke-virtual {p3}, Landroid/widget/ImageView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    .line 219
    if-nez v0, :cond_0

    .line 220
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 222
    :cond_0
    invoke-static {v0, v1}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v0

    .line 223
    invoke-interface {p0, p2, v0}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 225
    if-eqz p4, :cond_1

    .line 226
    invoke-interface {p4, p3}, Leyo;->a(Landroid/widget/ImageView;)V

    .line 228
    :cond_1
    invoke-static {p3}, Leyi;->c(Landroid/widget/ImageView;)V

    .line 229
    return-void
.end method

.method static synthetic b(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 29
    sget-object v0, Leyi;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Leyi;->a(Ljava/util/Iterator;)Leyo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p0}, Leyo;->b(Landroid/widget/ImageView;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static b(Leyo;)V
    .locals 2

    .prologue
    .line 118
    sget-object v0, Leyi;->a:Ljava/util/Set;

    new-instance v1, Lfag;

    invoke-direct {v1, p0}, Lfag;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 119
    return-void
.end method

.method private static c(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 122
    sget-object v0, Leyi;->a:Ljava/util/Set;

    .line 123
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 125
    :goto_0
    invoke-static {v0}, Leyi;->a(Ljava/util/Iterator;)Leyo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 126
    invoke-interface {v1, p0}, Leyo;->a(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 128
    :cond_0
    return-void
.end method
