.class public final Lbzq;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:Lbzs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1459
    new-instance v0, Lbzr;

    invoke-direct {v0}, Lbzr;-><init>()V

    sput-object v0, Lbzq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1477
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1478
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lbzq;->a(I)Lbzs;

    move-result-object v0

    iput-object v0, p0, Lbzq;->a:Lbzs;

    .line 1479
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1488
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1489
    return-void
.end method

.method private static a(I)Lbzs;
    .locals 5

    .prologue
    .line 1492
    invoke-static {}, Lbzs;->values()[Lbzs;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1493
    iget v4, v0, Lbzs;->d:I

    if-ne v4, p0, :cond_0

    .line 1498
    :goto_1
    return-object v0

    .line 1492
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1498
    :cond_1
    sget-object v0, Lbzs;->a:Lbzs;

    goto :goto_1
.end method

.method public static synthetic a(Lbzq;)Lbzs;
    .locals 1

    .prologue
    .line 1457
    iget-object v0, p0, Lbzq;->a:Lbzs;

    return-object v0
.end method

.method public static synthetic a(Lbzq;Lbzs;)Lbzs;
    .locals 0

    .prologue
    .line 1457
    iput-object p1, p0, Lbzq;->a:Lbzs;

    return-object p1
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1483
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1484
    iget-object v0, p0, Lbzq;->a:Lbzs;

    iget v0, v0, Lbzs;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1485
    return-void
.end method
