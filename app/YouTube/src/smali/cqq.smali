.class final Lcqq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcql;


# instance fields
.field private final a:Lcqi;

.field private final b:Lewh;

.field private c:Ljava/util/Set;

.field private d:Ljava/util/Set;

.field private synthetic e:Lcqn;


# direct methods
.method constructor <init>(Lcqn;Lcqi;Lewh;)V
    .locals 1

    .prologue
    .line 260
    iput-object p1, p0, Lcqq;->e:Lcqn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcqq;->c:Ljava/util/Set;

    .line 255
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcqq;->d:Ljava/util/Set;

    .line 261
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcqi;

    iput-object v0, p0, Lcqq;->a:Lcqi;

    .line 263
    iput-object p3, p0, Lcqq;->b:Lewh;

    .line 264
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Lcql;
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcqq;->c:Ljava/util/Set;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 284
    return-object p0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 248
    check-cast p1, Levd;

    iget-object v0, p0, Lcqq;->b:Lewh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqq;->b:Lewh;

    invoke-interface {v0, p1}, Lewh;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcqq;->a:Lcqi;

    iget-object v1, p0, Lcqq;->e:Lcqn;

    invoke-interface {v0, v1}, Lcqi;->a(Lcqj;)Lcqh;

    move-result-object v1

    iget-object v0, p0, Lcqq;->c:Ljava/util/Set;

    iget-object v2, p0, Lcqq;->d:Ljava/util/Set;

    invoke-virtual {v1}, Lcqh;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "CsiAction ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] already started. Ignored."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcqq;->e:Lcqn;

    iget-object v0, v0, Lcqn;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, v1, Lcqh;->e:Ljava/util/Set;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    iput-object v0, v1, Lcqh;->f:Ljava/util/Set;

    new-instance v0, Laca;

    iget-object v2, v1, Lcqh;->a:Ljava/lang/String;

    iget-object v3, v1, Lcqh;->d:Lacb;

    invoke-direct {v0, v2, v3}, Laca;-><init>(Ljava/lang/String;Lacb;)V

    iput-object v0, v1, Lcqh;->b:Laca;

    iget-object v0, v1, Lcqh;->b:Laca;

    invoke-virtual {p1}, Levd;->a()J

    move-result-wide v2

    new-instance v0, Labz;

    invoke-direct {v0, v2, v3, v4, v4}, Labz;-><init>(JLjava/lang/String;Labz;)V

    iput-object v0, v1, Lcqh;->c:Labz;

    goto :goto_0
.end method

.method public final b(Ljava/lang/Class;)Lcql;
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcqq;->d:Ljava/util/Set;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 290
    return-object p0
.end method
