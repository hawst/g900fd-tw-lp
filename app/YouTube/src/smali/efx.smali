.class public Lefx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefu;


# instance fields
.field final a:Lefc;

.field final b:Lefg;

.field final c:Leex;

.field final d:Leff;

.field e:Leew;

.field volatile f:Z

.field volatile g:J

.field volatile h:J

.field i:I

.field j:I

.field k:I


# direct methods
.method public constructor <init>(Lefc;Lefg;Leex;)V
    .locals 4

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iget-wide v0, p2, Lefg;->e:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 57
    iput-object p1, p0, Lefx;->a:Lefc;

    .line 58
    iput-object p2, p0, Lefx;->b:Lefg;

    .line 59
    iput-object p3, p0, Lefx;->c:Leex;

    .line 60
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lefx;->h:J

    .line 61
    new-instance v0, Leff;

    invoke-direct {v0}, Leff;-><init>()V

    iput-object v0, p0, Lefx;->d:Leff;

    .line 62
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 131
    const/4 v3, 0x0

    iget-object v4, p0, Lefx;->d:Leff;

    move-object v0, p0

    move-object v2, v1

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Lefx;->a(Ljava/nio/ByteBuffer;[BILeff;I)I

    move-result v0

    return v0
.end method

.method public a(Ljava/nio/ByteBuffer;I)I
    .locals 6

    .prologue
    .line 136
    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lefx;->d:Leff;

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lefx;->a(Ljava/nio/ByteBuffer;[BILeff;I)I

    move-result v0

    return v0
.end method

.method a(Ljava/nio/ByteBuffer;[BILeff;I)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-virtual {p0}, Lefx;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const/4 v0, -0x1

    .line 184
    :goto_0
    return v0

    .line 153
    :cond_0
    iget-wide v2, p0, Lefx;->g:J

    iget v0, p4, Leff;->a:I

    int-to-long v4, v0

    sub-long/2addr v2, v4

    int-to-long v4, p5

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v3, v2

    .line 154
    if-nez v3, :cond_1

    move v0, v1

    .line 155
    goto :goto_0

    .line 157
    :cond_1
    iget v0, p4, Leff;->a:I

    if-nez v0, :cond_2

    .line 158
    iput v1, p4, Leff;->b:I

    .line 159
    iget-object v0, p0, Lefx;->e:Leew;

    iput v1, p4, Leff;->c:I

    .line 160
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->c()I

    move-result v0

    iput v0, p4, Leff;->d:I

    .line 163
    :cond_2
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->b()[[B

    move-result-object v4

    move v2, v1

    move v0, p3

    .line 164
    :goto_1
    if-ge v2, v3, :cond_6

    .line 165
    iget v5, p4, Leff;->d:I

    if-nez v5, :cond_3

    .line 166
    iget v5, p4, Leff;->b:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p4, Leff;->b:I

    .line 167
    iget-object v5, p0, Lefx;->e:Leew;

    iget v5, p4, Leff;->b:I

    iput v1, p4, Leff;->c:I

    .line 168
    iget-object v5, p0, Lefx;->e:Leew;

    iget v6, p4, Leff;->b:I

    invoke-interface {v5}, Leew;->c()I

    move-result v5

    iput v5, p4, Leff;->d:I

    .line 170
    :cond_3
    iget v5, p4, Leff;->d:I

    sub-int v6, v3, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 171
    if-eqz p1, :cond_5

    .line 172
    iget v6, p4, Leff;->b:I

    aget-object v6, v4, v6

    iget v7, p4, Leff;->c:I

    invoke-virtual {p1, v6, v7, v5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 178
    :cond_4
    :goto_2
    iget v6, p4, Leff;->a:I

    add-int/2addr v6, v5

    iput v6, p4, Leff;->a:I

    .line 179
    add-int/2addr v2, v5

    .line 180
    iget v6, p4, Leff;->c:I

    add-int/2addr v6, v5

    iput v6, p4, Leff;->c:I

    .line 181
    iget v6, p4, Leff;->d:I

    sub-int v5, v6, v5

    iput v5, p4, Leff;->d:I

    goto :goto_1

    .line 173
    :cond_5
    if-eqz p2, :cond_4

    .line 174
    iget v6, p4, Leff;->b:I

    aget-object v6, v4, v6

    iget v7, p4, Leff;->c:I

    invoke-static {v6, v7, p2, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 176
    add-int/2addr v0, v5

    goto :goto_2

    :cond_6
    move v0, v2

    .line 184
    goto :goto_0
.end method

.method public a([BII)I
    .locals 6

    .prologue
    .line 141
    const/4 v1, 0x0

    iget-object v4, p0, Lefx;->d:Leff;

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lefx;->a(Ljava/nio/ByteBuffer;[BILeff;I)I

    move-result v0

    return v0
.end method

.method public a()J
    .locals 4

    .prologue
    .line 113
    iget-wide v0, p0, Lefx;->g:J

    iget-object v2, p0, Lefx;->d:Leff;

    iget v2, v2, Leff;->a:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-object v0, p0, Lefx;->d:Leff;

    iput v1, v0, Leff;->a:I

    iput v1, v0, Leff;->b:I

    iput v1, v0, Leff;->c:I

    iput v1, v0, Leff;->d:I

    .line 69
    return-void
.end method

.method public c()J
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lefx;->g:J

    return-wide v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefx;->f:Z

    .line 192
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lefx;->f:Z

    return v0
.end method

.method public f()V
    .locals 15

    .prologue
    const-wide/16 v12, 0x0

    const/high16 v7, 0x40000

    const-wide/16 v8, -0x1

    .line 201
    iget-boolean v0, p0, Lefx;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lefx;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    :try_start_0
    iget-wide v0, p0, Lefx;->g:J

    cmp-long v0, v0, v12

    if-nez v0, :cond_6

    iget-wide v0, p0, Lefx;->h:J

    cmp-long v0, v0, v8

    if-nez v0, :cond_6

    .line 209
    iget-object v0, p0, Lefx;->b:Lefg;

    .line 210
    iget-object v1, p0, Lefx;->a:Lefc;

    invoke-interface {v1, v0}, Lefc;->a(Lefg;)J

    move-result-wide v0

    .line 211
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 212
    new-instance v2, Lefe;

    new-instance v3, Legb;

    iget-object v4, p0, Lefx;->b:Lefg;

    iget-wide v4, v4, Lefg;->e:J

    invoke-direct {v3, v4, v5, v0, v1}, Legb;-><init>(JJ)V

    invoke-direct {v2, v3}, Lefe;-><init>(Ljava/io/IOException;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lefx;->a:Lefc;

    invoke-static {v1}, Legz;->a(Lefc;)V

    throw v0

    .line 215
    :cond_2
    :try_start_1
    iput-wide v0, p0, Lefx;->h:J

    .line 224
    :goto_1
    iget-object v0, p0, Lefx;->e:Leew;

    if-nez v0, :cond_3

    .line 225
    iget-wide v0, p0, Lefx;->h:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_8

    iget-wide v0, p0, Lefx;->h:J

    long-to-int v0, v0

    .line 227
    :goto_2
    iget-object v1, p0, Lefx;->c:Leex;

    invoke-interface {v1, v0}, Leex;->a(I)Leew;

    move-result-object v0

    iput-object v0, p0, Lefx;->e:Leew;

    .line 229
    :cond_3
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->a()I

    move-result v2

    .line 231
    iget-wide v0, p0, Lefx;->g:J

    cmp-long v0, v0, v12

    if-nez v0, :cond_4

    .line 232
    const/4 v0, 0x0

    iput v0, p0, Lefx;->i:I

    .line 233
    iget-object v0, p0, Lefx;->e:Leew;

    const/4 v0, 0x0

    iput v0, p0, Lefx;->j:I

    .line 234
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->c()I

    move-result v0

    iput v0, p0, Lefx;->k:I

    .line 237
    :cond_4
    const v1, 0x7fffffff

    .line 238
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->b()[[B

    move-result-object v0

    move v14, v1

    move v1, v2

    move v2, v14

    .line 239
    :cond_5
    :goto_3
    iget-boolean v3, p0, Lefx;->f:Z

    if-nez v3, :cond_d

    if-lez v2, :cond_d

    invoke-virtual {p0}, Lefx;->k()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 240
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 241
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 217
    :cond_6
    iget-wide v0, p0, Lefx;->h:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lefx;->h:J

    iget-wide v2, p0, Lefx;->g:J

    sub-long v4, v0, v2

    .line 219
    :goto_4
    new-instance v0, Lefg;

    iget-object v1, p0, Lefx;->b:Lefg;

    iget-object v1, v1, Lefg;->a:Landroid/net/Uri;

    iget-object v2, p0, Lefx;->b:Lefg;

    iget-wide v2, v2, Lefg;->d:J

    iget-wide v10, p0, Lefx;->g:J

    add-long/2addr v2, v10

    iget-object v6, p0, Lefx;->b:Lefg;

    iget-object v6, v6, Lefg;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 221
    iget-object v1, p0, Lefx;->a:Lefc;

    invoke-interface {v1, v0}, Lefc;->a(Lefg;)J

    goto :goto_1

    :cond_7
    move-wide v4, v8

    .line 217
    goto :goto_4

    :cond_8
    move v0, v7

    .line 225
    goto :goto_2

    .line 243
    :cond_9
    iget-object v2, p0, Lefx;->a:Lefc;

    iget v3, p0, Lefx;->i:I

    aget-object v3, v0, v3

    iget v4, p0, Lefx;->j:I

    iget v5, p0, Lefx;->k:I

    invoke-interface {v2, v3, v4, v5}, Lefc;->a([BII)I

    move-result v2

    .line 245
    if-lez v2, :cond_b

    .line 246
    iget-wide v4, p0, Lefx;->g:J

    int-to-long v10, v2

    add-long/2addr v4, v10

    iput-wide v4, p0, Lefx;->g:J

    .line 247
    iget v3, p0, Lefx;->j:I

    add-int/2addr v3, v2

    iput v3, p0, Lefx;->j:I

    .line 248
    iget v3, p0, Lefx;->k:I

    sub-int/2addr v3, v2

    iput v3, p0, Lefx;->k:I

    .line 249
    iget v3, p0, Lefx;->k:I

    if-nez v3, :cond_5

    invoke-virtual {p0}, Lefx;->k()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 250
    iget v3, p0, Lefx;->i:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lefx;->i:I

    .line 251
    iget-wide v4, p0, Lefx;->g:J

    int-to-long v10, v1

    cmp-long v3, v4, v10

    if-nez v3, :cond_a

    .line 252
    iget-object v0, p0, Lefx;->e:Leew;

    add-int/2addr v1, v7

    invoke-interface {v0, v1}, Leew;->a(I)V

    .line 253
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->a()I

    move-result v1

    .line 254
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->b()[[B

    move-result-object v0

    .line 256
    :cond_a
    iget-object v3, p0, Lefx;->e:Leew;

    iget v3, p0, Lefx;->i:I

    const/4 v3, 0x0

    iput v3, p0, Lefx;->j:I

    .line 257
    iget-object v3, p0, Lefx;->e:Leew;

    iget v4, p0, Lefx;->i:I

    invoke-interface {v3}, Leew;->c()I

    move-result v3

    iput v3, p0, Lefx;->k:I

    goto/16 :goto_3

    .line 259
    :cond_b
    iget-wide v4, p0, Lefx;->h:J

    cmp-long v3, v4, v8

    if-nez v3, :cond_c

    .line 260
    iget-wide v4, p0, Lefx;->g:J

    iput-wide v4, p0, Lefx;->h:J

    goto/16 :goto_3

    .line 261
    :cond_c
    iget-wide v4, p0, Lefx;->h:J

    iget-wide v10, p0, Lefx;->g:J

    cmp-long v3, v4, v10

    if-eqz v3, :cond_5

    .line 262
    new-instance v0, Lefe;

    new-instance v1, Legb;

    iget-wide v2, p0, Lefx;->h:J

    iget-wide v4, p0, Lefx;->g:J

    invoke-direct {v1, v2, v3, v4, v5}, Legb;-><init>(JJ)V

    invoke-direct {v0, v1}, Lefe;-><init>(Ljava/io/IOException;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267
    :cond_d
    iget-object v0, p0, Lefx;->a:Lefc;

    invoke-static {v0}, Legz;->a(Lefc;)V

    goto/16 :goto_0
.end method

.method public g()J
    .locals 4

    .prologue
    .line 97
    iget-wide v0, p0, Lefx;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lefx;->h:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lefx;->b:Lefg;

    iget-wide v0, v0, Lefg;->e:J

    goto :goto_0
.end method

.method public h()Z
    .locals 4

    .prologue
    .line 106
    iget-wide v0, p0, Lefx;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lefx;->g:J

    iget-wide v2, p0, Lefx;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 4

    .prologue
    .line 118
    iget-wide v0, p0, Lefx;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefx;->d:Leff;

    iget v0, v0, Leff;->a:I

    int-to-long v0, v0

    iget-wide v2, p0, Lefx;->h:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lefx;->e:Leew;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lefx;->e:Leew;

    invoke-interface {v0}, Leew;->d()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lefx;->e:Leew;

    .line 127
    :cond_0
    return-void
.end method

.method k()Z
    .locals 4

    .prologue
    .line 272
    iget-wide v0, p0, Lefx;->h:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lefx;->g:J

    iget-wide v2, p0, Lefx;->h:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
