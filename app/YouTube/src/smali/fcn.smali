.class public abstract Lfcn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lftb;

.field public final b:Lfsz;

.field public final c:Lgix;

.field public final d:Lws;


# direct methods
.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v2, p0, Lfcn;->a:Lftb;

    .line 39
    new-instance v0, Lfsz;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v1}, Lfsz;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lfcn;->b:Lfsz;

    .line 41
    new-instance v0, Lgiy;

    invoke-direct {v0}, Lgiy;-><init>()V

    iput-object v0, p0, Lfcn;->c:Lgix;

    .line 42
    iput-object v2, p0, Lfcn;->d:Lws;

    .line 43
    return-void
.end method

.method public constructor <init>(Lftb;Lfsz;Lgix;Lws;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftb;

    iput-object v0, p0, Lfcn;->a:Lftb;

    .line 51
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsz;

    iput-object v0, p0, Lfcn;->b:Lfsz;

    .line 52
    iput-object p3, p0, Lfcn;->c:Lgix;

    .line 53
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lfcn;->d:Lws;

    .line 54
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/Class;)Lfco;
    .locals 3

    .prologue
    .line 58
    new-instance v0, Lfco;

    iget-object v1, p0, Lfcn;->a:Lftb;

    iget-object v2, p0, Lfcn;->d:Lws;

    invoke-direct {v0, v1, v2, p1}, Lfco;-><init>(Lftb;Lws;Ljava/lang/Class;)V

    return-object v0
.end method
