.class public final Lqd;
.super Landroid/widget/HorizontalScrollView;
.source "SourceFile"

# interfaces
.implements Lpq;


# instance fields
.field a:Ljava/lang/Runnable;

.field public b:Z

.field c:I

.field private d:Lqg;

.field private e:Lqi;

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    return-void
.end method

.method static synthetic a(Lqd;)Landroid/support/v7/widget/LinearLayoutCompat;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic a(Lqd;Lko;Z)Lqh;
    .locals 4

    .prologue
    .line 55
    const/4 v0, 0x1

    new-instance v1, Lqh;

    invoke-virtual {p0}, Lqd;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, v0}, Lqh;-><init>(Lqd;Landroid/content/Context;Lko;Z)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lqh;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lqd;->g:I

    invoke-direct {v0, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Lqh;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v1
.end method

.method private a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 180
    iput p1, p0, Lqd;->h:I

    .line 181
    invoke-virtual {v5}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildCount()I

    move-result v3

    move v2, v1

    .line 182
    :goto_0
    if-ge v2, v3, :cond_3

    .line 183
    invoke-virtual {v5, v2}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 184
    if-ne v2, p1, :cond_2

    const/4 v0, 0x1

    .line 185
    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setSelected(Z)V

    .line 186
    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {v5, p1}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v4, p0, Lqd;->a:Ljava/lang/Runnable;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lqd;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v4}, Lqd;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    new-instance v4, Lqe;

    invoke-direct {v4, p0, v0}, Lqe;-><init>(Lqd;Landroid/view/View;)V

    iput-object v4, p0, Lqd;->a:Ljava/lang/Runnable;

    iget-object v0, p0, Lqd;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lqd;->post(Ljava/lang/Runnable;)Z

    .line 182
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 184
    goto :goto_1

    .line 190
    :cond_3
    iget-object v0, p0, Lqd;->e:Lqi;

    if-eqz v0, :cond_4

    if-ltz p1, :cond_4

    .line 191
    iget-object v0, p0, Lqd;->e:Lqi;

    invoke-virtual {v0, p1}, Lqi;->a(I)V

    .line 193
    :cond_4
    return-void
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lqd;->e:Lqi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lqd;->e:Lqi;

    invoke-virtual {v0}, Lqi;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 170
    invoke-direct {p0}, Lqd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return v4

    .line 172
    :cond_0
    iget-object v0, p0, Lqd;->e:Lqi;

    invoke-virtual {p0, v0}, Lqd;->removeView(Landroid/view/View;)V

    .line 173
    const/4 v0, 0x0

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lqd;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v0, p0, Lqd;->e:Lqi;

    iget v0, v0, Lpo;->o:I

    invoke-direct {p0, v0}, Lqd;->a(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 367
    check-cast p1, Lqh;

    .line 368
    iget-object v0, p1, Lqh;->a:Lko;

    .line 369
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 273
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    .line 274
    iget-object v0, p0, Lqd;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lqd;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lqd;->post(Ljava/lang/Runnable;)Z

    .line 278
    :cond_0
    return-void
.end method

.method protected final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 220
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 221
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 224
    :cond_0
    invoke-virtual {p0}, Lqd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lna;->a(Landroid/content/Context;)Lna;

    move-result-object v1

    .line 227
    iget-object v0, v1, Lna;->a:Landroid/content/Context;

    const/4 v2, 0x0

    sget-object v3, Lmd;->a:[I

    const v4, 0x7f010053

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    const/4 v0, 0x1

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getLayoutDimension(II)I

    move-result v0

    iget-object v3, v1, Lna;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v1}, Lna;->a()Z

    move-result v4

    if-nez v4, :cond_1

    const v4, 0x7f0a001d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_1
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    iput v0, p0, Lqd;->g:I

    invoke-virtual {p0}, Lqd;->requestLayout()V

    .line 228
    iget-object v0, v1, Lna;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lqd;->f:I

    .line 229
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 282
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onDetachedFromWindow()V

    .line 283
    iget-object v0, p0, Lqd;->a:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lqd;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lqd;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 286
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 94
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 95
    if-ne v3, v8, :cond_6

    move v0, v1

    .line 96
    :goto_0
    invoke-virtual {p0, v0}, Lqd;->setFillViewport(Z)V

    .line 98
    invoke-virtual {v6}, Landroid/support/v7/widget/LinearLayoutCompat;->getChildCount()I

    move-result v4

    .line 99
    if-le v4, v1, :cond_8

    if-eq v3, v8, :cond_0

    const/high16 v5, -0x80000000

    if-ne v3, v5, :cond_8

    .line 101
    :cond_0
    const/4 v3, 0x2

    if-le v4, v3, :cond_7

    .line 102
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lqd;->c:I

    .line 106
    :goto_1
    iget v3, p0, Lqd;->c:I

    iget v4, p0, Lqd;->f:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lqd;->c:I

    .line 111
    :goto_2
    iget v3, p0, Lqd;->g:I

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 113
    if-nez v0, :cond_9

    iget-boolean v4, p0, Lqd;->b:Z

    if-eqz v4, :cond_9

    .line 115
    :goto_3
    if-eqz v1, :cond_b

    .line 117
    invoke-virtual {v6, v2, v3}, Landroid/support/v7/widget/LinearLayoutCompat;->measure(II)V

    .line 118
    invoke-virtual {v6}, Landroid/support/v7/widget/LinearLayoutCompat;->getMeasuredWidth()I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    if-le v1, v2, :cond_a

    .line 119
    invoke-direct {p0}, Lqd;->a()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lqd;->e:Lqi;

    if-nez v1, :cond_1

    new-instance v1, Lqi;

    invoke-virtual {p0}, Lqd;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f01006b

    invoke-direct {v1, v2, v6, v4}, Lqi;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v2, Luo;

    const/4 v4, -0x2

    invoke-direct {v2, v4, v7}, Luo;-><init>(II)V

    invoke-virtual {v1, v2}, Lqi;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, p0}, Lqi;->b(Lpq;)V

    iput-object v1, p0, Lqd;->e:Lqi;

    :cond_1
    invoke-virtual {p0, v6}, Lqd;->removeView(Landroid/view/View;)V

    iget-object v1, p0, Lqd;->e:Lqi;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v2, v4, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1, v2}, Lqd;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lqd;->e:Lqi;

    iget-object v1, v1, Lop;->a:Landroid/widget/SpinnerAdapter;

    if-nez v1, :cond_2

    iget-object v1, p0, Lqd;->e:Lqi;

    new-instance v2, Lqf;

    invoke-direct {v2, p0}, Lqf;-><init>(Lqd;)V

    invoke-virtual {v1, v2}, Lqi;->a(Landroid/widget/SpinnerAdapter;)V

    :cond_2
    iget-object v1, p0, Lqd;->a:Ljava/lang/Runnable;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lqd;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lqd;->removeCallbacks(Ljava/lang/Runnable;)Z

    iput-object v6, p0, Lqd;->a:Ljava/lang/Runnable;

    :cond_3
    iget-object v1, p0, Lqd;->e:Lqi;

    iget v2, p0, Lqd;->h:I

    invoke-virtual {v1, v2}, Lqi;->a(I)V

    .line 127
    :cond_4
    :goto_4
    invoke-virtual {p0}, Lqd;->getMeasuredWidth()I

    move-result v1

    .line 128
    invoke-super {p0, p1, v3}, Landroid/widget/HorizontalScrollView;->onMeasure(II)V

    .line 129
    invoke-virtual {p0}, Lqd;->getMeasuredWidth()I

    move-result v2

    .line 131
    if-eqz v0, :cond_5

    if-eq v1, v2, :cond_5

    .line 133
    iget v0, p0, Lqd;->h:I

    invoke-direct {p0, v0}, Lqd;->a(I)V

    .line 135
    :cond_5
    return-void

    :cond_6
    move v0, v2

    .line 95
    goto/16 :goto_0

    .line 104
    :cond_7
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iput v3, p0, Lqd;->c:I

    goto/16 :goto_1

    .line 108
    :cond_8
    iput v7, p0, Lqd;->c:I

    goto/16 :goto_2

    :cond_9
    move v1, v2

    .line 113
    goto/16 :goto_3

    .line 121
    :cond_a
    invoke-direct {p0}, Lqd;->b()Z

    goto :goto_4

    .line 124
    :cond_b
    invoke-direct {p0}, Lqd;->b()Z

    goto :goto_4
.end method
