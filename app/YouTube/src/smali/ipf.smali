.class final Lipf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liqk;


# static fields
.field private static synthetic c:Z


# instance fields
.field private a:Z

.field private b:Z

.field private synthetic d:Lipe;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 470
    const-class v0, Lipe;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lipf;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lipe;)V
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Lipf;->d:Lipe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lipf;)Z
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Lipf;->b:Z

    return v0
.end method

.method static synthetic a(Lipf;Z)Z
    .locals 0

    .prologue
    .line 470
    iput-boolean p1, p0, Lipf;->b:Z

    return p1
.end method

.method static synthetic b(Lipf;)Z
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Lipf;->a:Z

    return v0
.end method


# virtual methods
.method public final a()Liqm;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->g(Lipe;)Liph;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lipq;J)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 480
    sget-boolean v0, Lipf;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 490
    :cond_0
    :try_start_0
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->g(Lipe;)Liph;

    move-result-object v0

    invoke-virtual {v0}, Liph;->b()V

    .line 493
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->h(Lipe;)V

    .line 494
    iget-object v0, p0, Lipf;->d:Lipe;

    iget-wide v2, v0, Lipe;->b:J

    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 495
    iget-object v0, p0, Lipf;->d:Lipe;

    iget-wide v2, v0, Lipe;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lipe;->b:J

    .line 496
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498
    sub-long/2addr p2, v4

    .line 499
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->a(Lipe;)Lios;

    move-result-object v0

    iget-object v1, p0, Lipf;->d:Lipe;

    invoke-static {v1}, Lipe;->b(Lipe;)I

    move-result v1

    const/4 v2, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lios;->a(IZLipq;J)V

    .line 481
    :cond_1
    cmp-long v0, p2, v6

    if-lez v0, :cond_2

    .line 483
    iget-object v1, p0, Lipf;->d:Lipe;

    monitor-enter v1

    .line 484
    :try_start_1
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->g(Lipe;)Liph;

    move-result-object v0

    invoke-virtual {v0}, Liph;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 486
    :goto_0
    :try_start_2
    iget-object v0, p0, Lipf;->d:Lipe;

    iget-wide v2, v0, Lipe;->b:J

    cmp-long v0, v2, v6

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lipf;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lipf;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->d(Lipe;)Linr;

    move-result-object v0

    if-nez v0, :cond_0

    .line 487
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->e(Lipe;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 490
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v2, p0, Lipf;->d:Lipe;

    invoke-static {v2}, Lipe;->g(Lipe;)Liph;

    move-result-object v2

    invoke-virtual {v2}, Liph;->b()V

    throw v0

    .line 496
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 501
    :cond_2
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 504
    sget-boolean v0, Lipf;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 505
    :cond_0
    iget-object v1, p0, Lipf;->d:Lipe;

    monitor-enter v1

    .line 506
    :try_start_0
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->h(Lipe;)V

    .line 507
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 508
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->a(Lipe;)Lios;

    move-result-object v0

    invoke-virtual {v0}, Lios;->c()V

    .line 509
    return-void

    .line 507
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final close()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 516
    sget-boolean v0, Lipf;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 517
    :cond_0
    iget-object v1, p0, Lipf;->d:Lipe;

    monitor-enter v1

    .line 518
    :try_start_0
    iget-boolean v0, p0, Lipf;->a:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    .line 528
    :goto_0
    return-void

    .line 519
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    iget-object v0, p0, Lipf;->d:Lipe;

    iget-object v0, v0, Lipe;->g:Lipf;

    iget-boolean v0, v0, Lipf;->b:Z

    if-nez v0, :cond_2

    .line 521
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->a(Lipe;)Lios;

    move-result-object v0

    iget-object v1, p0, Lipf;->d:Lipe;

    invoke-static {v1}, Lipe;->b(Lipe;)I

    move-result v1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lios;->a(IZLipq;J)V

    .line 523
    :cond_2
    iget-object v1, p0, Lipf;->d:Lipe;

    monitor-enter v1

    .line 524
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lipf;->a:Z

    .line 525
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 526
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->a(Lipe;)Lios;

    move-result-object v0

    invoke-virtual {v0}, Lios;->c()V

    .line 527
    iget-object v0, p0, Lipf;->d:Lipe;

    invoke-static {v0}, Lipe;->f(Lipe;)V

    goto :goto_0

    .line 519
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 525
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
