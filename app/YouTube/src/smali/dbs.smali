.class public final enum Ldbs;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldbs;

.field public static final enum b:Ldbs;

.field public static final enum c:Ldbs;

.field public static final enum d:Ldbs;

.field public static final enum e:Ldbs;

.field public static final enum f:Ldbs;

.field public static final enum g:Ldbs;

.field private static final synthetic h:[Ldbs;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 96
    new-instance v0, Ldbs;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v3}, Ldbs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbs;->a:Ldbs;

    .line 97
    new-instance v0, Ldbs;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v4}, Ldbs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbs;->b:Ldbs;

    .line 98
    new-instance v0, Ldbs;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v5}, Ldbs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbs;->c:Ldbs;

    .line 99
    new-instance v0, Ldbs;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v6}, Ldbs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbs;->d:Ldbs;

    .line 100
    new-instance v0, Ldbs;

    const-string v1, "RECOVERABLE_ERROR"

    invoke-direct {v0, v1, v7}, Ldbs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbs;->e:Ldbs;

    .line 101
    new-instance v0, Ldbs;

    const-string v1, "UNRECOVERABLE_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldbs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbs;->f:Ldbs;

    .line 102
    new-instance v0, Ldbs;

    const-string v1, "ENDED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Ldbs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbs;->g:Ldbs;

    .line 95
    const/4 v0, 0x7

    new-array v0, v0, [Ldbs;

    sget-object v1, Ldbs;->a:Ldbs;

    aput-object v1, v0, v3

    sget-object v1, Ldbs;->b:Ldbs;

    aput-object v1, v0, v4

    sget-object v1, Ldbs;->c:Ldbs;

    aput-object v1, v0, v5

    sget-object v1, Ldbs;->d:Ldbs;

    aput-object v1, v0, v6

    sget-object v1, Ldbs;->e:Ldbs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldbs;->f:Ldbs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Ldbs;->g:Ldbs;

    aput-object v2, v0, v1

    sput-object v0, Ldbs;->h:[Ldbs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbs;
    .locals 1

    .prologue
    .line 95
    const-class v0, Ldbs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbs;

    return-object v0
.end method

.method public static values()[Ldbs;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Ldbs;->h:[Ldbs;

    invoke-virtual {v0}, [Ldbs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbs;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 105
    sget-object v0, Ldbs;->e:Ldbs;

    if-eq p0, v0, :cond_0

    sget-object v0, Ldbs;->f:Ldbs;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 112
    sget-object v0, Ldbs;->b:Ldbs;

    if-eq p0, v0, :cond_0

    sget-object v0, Ldbs;->c:Ldbs;

    if-eq p0, v0, :cond_0

    sget-object v0, Ldbs;->g:Ldbs;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Ldbs;->b()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Ldbs;->d:Ldbs;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
