.class public abstract Ledc;
.super Lecj;
.source "SourceFile"


# instance fields
.field public final f:J

.field public final g:J

.field public final h:I


# direct methods
.method public constructor <init>(Lefc;Lefg;Lecw;IJJI)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3, p4}, Lecj;-><init>(Lefc;Lefg;Lecw;I)V

    .line 46
    iput-wide p5, p0, Ledc;->f:J

    .line 47
    iput-wide p7, p0, Ledc;->g:J

    .line 48
    iput p9, p0, Ledc;->h:I

    .line 49
    return-void
.end method


# virtual methods
.method public abstract a(JZ)Z
.end method

.method public abstract a(Leby;)Z
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Ledc;->h:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract h()V
.end method

.method public abstract i()Z
.end method

.method public abstract j()Z
.end method

.method public abstract k()Lebv;
.end method

.method public abstract l()Ljava/util/Map;
.end method
