.class public final Lbzv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbog;


# instance fields
.field private final a:Leyp;

.field private final b:Lfhz;

.field private final c:Landroid/view/ViewStub;

.field private final d:Lfdw;

.field private final e:Lfrz;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Lbnv;

.field private j:Z

.field private k:Lfoe;

.field private l:Lfvi;

.field private m:Lfnc;

.field private n:Z

.field private o:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Leyp;Lfhz;Landroid/view/ViewStub;Lfdw;Lfrz;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lbzv;->f:Landroid/view/View;

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbzv;->a:Leyp;

    .line 56
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lbzv;->b:Lfhz;

    .line 57
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lbzv;->c:Landroid/view/ViewStub;

    .line 58
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdw;

    iput-object v0, p0, Lbzv;->d:Lfdw;

    .line 59
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lbzv;->e:Lfrz;

    .line 60
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 159
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    return-object v0
.end method

.method a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 163
    iget-boolean v0, p0, Lbzv;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbzv;->k:Lfoe;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 164
    if-eqz p1, :cond_0

    .line 165
    iget-object v0, p0, Lbzv;->e:Lfrz;

    .line 166
    invoke-interface {v0}, Lfrz;->B()Lfqg;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lbzv;->d:Lfdw;

    iget-object v2, p0, Lbzv;->k:Lfoe;

    invoke-virtual {v1, v0, v2, v4}, Lfdw;->b(Lfqg;Lfqh;Lhcq;)V

    .line 170
    :cond_0
    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :cond_1
    iget-boolean v0, p0, Lbzv;->n:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbzv;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbzv;->o:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lbzv;->o:Landroid/widget/ImageView;

    .line 173
    invoke-virtual {v0}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lbzv;->m:Lfnc;

    if-eqz v0, :cond_2

    .line 174
    iput-boolean v3, p0, Lbzv;->n:Z

    .line 179
    iget-object v0, p0, Lbzv;->l:Lfvi;

    iget-object v1, p0, Lbzv;->m:Lfnc;

    invoke-virtual {v0, v1, v4}, Lfvi;->a(Lfnc;Leyo;)V

    .line 181
    :cond_2
    return-void
.end method

.method public final a(Lfoy;Lfnx;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 107
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 110
    :cond_1
    iget-object v2, p2, Lfnx;->k:Lfoe;

    if-nez v2, :cond_2

    iget-object v2, p2, Lfnx;->a:Libi;

    iget-object v2, v2, Libi;->f:Lhee;

    if-eqz v2, :cond_2

    iget-object v2, p2, Lfnx;->a:Libi;

    iget-object v2, v2, Libi;->f:Lhee;

    iget-object v2, v2, Lhee;->c:Lhox;

    if-eqz v2, :cond_2

    new-instance v2, Lfoe;

    iget-object v3, p2, Lfnx;->a:Libi;

    iget-object v3, v3, Libi;->f:Lhee;

    iget-object v3, v3, Lhee;->c:Lhox;

    invoke-direct {v2, v3}, Lfoe;-><init>(Lhox;)V

    iput-object v2, p2, Lfnx;->k:Lfoe;

    :cond_2
    iget-object v2, p2, Lfnx;->k:Lfoe;

    iput-object v2, p0, Lbzv;->k:Lfoe;

    .line 111
    iget-object v2, p0, Lbzv;->k:Lfoe;

    if-eqz v2, :cond_0

    .line 114
    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    if-nez v0, :cond_3

    iget-object v0, p0, Lbzv;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbzv;->f:Landroid/view/View;

    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    const v2, 0x7f0800c0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbzv;->o:Landroid/widget/ImageView;

    iget-object v0, p0, Lbzv;->o:Landroid/widget/ImageView;

    new-instance v2, Lbzw;

    invoke-direct {v2, p0}, Lbzw;-><init>(Lbzv;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    new-instance v2, Lfvi;

    iget-object v0, p0, Lbzv;->a:Leyp;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iget-object v3, p0, Lbzv;->o:Landroid/widget/ImageView;

    invoke-direct {v2, v0, v3, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v2, p0, Lbzv;->l:Lfvi;

    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    const v2, 0x7f08008b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzv;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    const v2, 0x7f08013e

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbzv;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    const v2, 0x7f08013f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Lbnv;

    iget-object v3, p0, Lbzv;->b:Lfhz;

    invoke-direct {v2, v3, v0}, Lbnv;-><init>(Lfhz;Landroid/widget/TextView;)V

    iput-object v2, p0, Lbzv;->i:Lbnv;

    invoke-direct {p0}, Lbzv;->g()V

    .line 116
    :cond_3
    iget-object v0, p0, Lbzv;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lbzv;->k:Lfoe;

    iget-object v3, v2, Lfoe;->b:Ljava/lang/String;

    if-nez v3, :cond_4

    iget-object v3, v2, Lfoe;->a:Lhox;

    iget-object v3, v3, Lhox;->a:Lhgz;

    if-eqz v3, :cond_4

    iget-object v3, v2, Lfoe;->a:Lhox;

    iget-object v3, v3, Lhox;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lfoe;->b:Ljava/lang/String;

    :cond_4
    iget-object v2, v2, Lfoe;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lbzv;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lbzv;->k:Lfoe;

    iget-object v3, v2, Lfoe;->c:Ljava/lang/String;

    if-nez v3, :cond_5

    iget-object v3, v2, Lfoe;->a:Lhox;

    iget-object v3, v3, Lhox;->b:Lhgz;

    if-eqz v3, :cond_5

    iget-object v3, v2, Lfoe;->a:Lhox;

    iget-object v3, v3, Lhox;->b:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lfoe;->c:Ljava/lang/String;

    :cond_5
    iget-object v2, v2, Lfoe;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lbzv;->i:Lbnv;

    iget-object v2, p0, Lbzv;->k:Lfoe;

    iget-object v3, v2, Lfoe;->d:Lfit;

    if-nez v3, :cond_6

    iget-object v3, v2, Lfoe;->a:Lhox;

    iget-object v3, v3, Lhox;->c:Lhbt;

    if-eqz v3, :cond_6

    iget-object v3, v2, Lfoe;->a:Lhox;

    iget-object v3, v3, Lhox;->c:Lhbt;

    iget-object v3, v3, Lhbt;->a:Lhbs;

    if-eqz v3, :cond_6

    new-instance v3, Lfit;

    iget-object v4, v2, Lfoe;->a:Lhox;

    iget-object v4, v4, Lhox;->c:Lhbt;

    iget-object v4, v4, Lhbt;->a:Lhbs;

    invoke-direct {v3, v4}, Lfit;-><init>(Lhbs;)V

    iput-object v3, v2, Lfoe;->d:Lfit;

    :cond_6
    iget-object v2, v2, Lfoe;->d:Lfit;

    invoke-virtual {v0, v2}, Lbnv;->a(Lfit;)V

    .line 120
    iget-object v0, p0, Lbzv;->k:Lfoe;

    if-eqz v0, :cond_8

    .line 121
    iget-object v0, p0, Lbzv;->k:Lfoe;

    iget-object v2, v0, Lfoe;->e:Lfnc;

    if-nez v2, :cond_7

    new-instance v2, Lfnc;

    iget-object v3, v0, Lfoe;->a:Lhox;

    iget-object v3, v3, Lhox;->d:Lhxf;

    invoke-direct {v2, v3}, Lfnc;-><init>(Lhxf;)V

    iput-object v2, v0, Lfoe;->e:Lfnc;

    :cond_7
    iget-object v0, v0, Lfoe;->e:Lfnc;

    iput-object v0, p0, Lbzv;->m:Lfnc;

    .line 122
    iput-boolean v1, p0, Lbzv;->n:Z

    :cond_8
    move v0, v1

    .line 124
    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbzv;->f:Landroid/view/View;

    .line 102
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 129
    iget-boolean v1, p0, Lbzv;->j:Z

    .line 130
    iput-boolean v0, p0, Lbzv;->j:Z

    .line 131
    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lbzv;->a(Z)V

    .line 132
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 141
    iput-boolean v0, p0, Lbzv;->j:Z

    .line 142
    iput-object v1, p0, Lbzv;->k:Lfoe;

    .line 143
    iput-object v1, p0, Lbzv;->m:Lfnc;

    .line 144
    iput-boolean v0, p0, Lbzv;->n:Z

    .line 145
    invoke-direct {p0}, Lbzv;->g()V

    .line 146
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbzv;->a(Z)V

    .line 137
    return-void
.end method
