.class public final Lgwe;
.super Lgxk;
.source "SourceFile"

# interfaces
.implements Lacu;
.implements Landroid/os/IBinder$DeathRecipient;
.implements Lgwb;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lgwd;

.field private final d:Ljava/lang/String;

.field private volatile e:Lgwl;

.field private volatile f:Laco;

.field private volatile g:Lalz;

.field private h:Lewj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgwd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lgwl;)V
    .locals 9

    .prologue
    .line 165
    invoke-direct {p0}, Lgxk;-><init>()V

    .line 166
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lgwe;->a:Landroid/content/Context;

    .line 167
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgwl;

    iput-object v0, p0, Lgwe;->e:Lgwl;

    .line 168
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lgwe;->b:Landroid/os/Handler;

    .line 169
    const-string v0, "serviceDestroyedNotifier"

    .line 170
    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgwd;

    iput-object v0, p0, Lgwe;->c:Lgwd;

    .line 171
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgwe;->d:Ljava/lang/String;

    .line 172
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const/4 v0, 0x0

    iget-object v2, p0, Lgwe;->b:Landroid/os/Handler;

    const/4 v8, 0x0

    move-object v1, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-static/range {v0 .. v8}, Laco;->a(ZLacu;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 185
    return-void
.end method

.method static synthetic a(Lgwe;Z)V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lgwe;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 260
    iget-object v0, p0, Lgwe;->h:Lewj;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lgwe;->h:Lewj;

    iget-object v2, v0, Lewj;->a:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 262
    iput-object v3, p0, Lgwe;->h:Lewj;

    .line 264
    :cond_0
    iget-object v0, p0, Lgwe;->f:Laco;

    if-eqz v0, :cond_1

    .line 265
    iget-object v2, p0, Lgwe;->f:Laco;

    if-nez p1, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Laco;->a(Z)V

    .line 266
    iput-object v3, p0, Lgwe;->f:Laco;

    .line 268
    :cond_1
    iput-object v3, p0, Lgwe;->g:Lalz;

    .line 269
    iget-object v0, p0, Lgwe;->e:Lgwl;

    if-eqz v0, :cond_2

    .line 270
    iget-object v0, p0, Lgwe;->e:Lgwl;

    invoke-interface {v0}, Lgwl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 271
    iput-object v3, p0, Lgwe;->e:Lgwl;

    .line 273
    :cond_2
    iget-object v0, p0, Lgwe;->c:Lgwd;

    invoke-virtual {v0, p0}, Lgwd;->b(Lgwb;)V

    .line 277
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 278
    return-void

    :cond_3
    move v0, v1

    .line 265
    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lgwe;->f:Laco;

    if-nez v0, :cond_0

    .line 216
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "YouTubeServiceEntity not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 218
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lgxe;)Lgxh;
    .locals 3

    .prologue
    .line 228
    invoke-direct {p0}, Lgwe;->c()V

    .line 229
    new-instance v0, Lalq;

    iget-object v1, p0, Lgwe;->b:Landroid/os/Handler;

    iget-object v2, p0, Lgwe;->f:Laco;

    invoke-direct {v0, v1, v2, p1}, Lalq;-><init>(Landroid/os/Handler;Laco;Lgxe;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgwe;->b(Z)V

    .line 253
    return-void
.end method

.method public final a(Laco;)V
    .locals 4

    .prologue
    .line 189
    iput-object p1, p0, Lgwe;->f:Laco;

    .line 190
    new-instance v0, Lewj;

    iget-object v1, p0, Lgwe;->a:Landroid/content/Context;

    iget-object v2, p1, Laco;->I:Levn;

    invoke-direct {v0, v1, v2}, Lewj;-><init>(Landroid/content/Context;Levn;)V

    iput-object v0, p0, Lgwe;->h:Lewj;

    .line 191
    new-instance v0, Lalz;

    iget-object v1, p0, Lgwe;->a:Landroid/content/Context;

    iget-object v2, p0, Lgwe;->b:Landroid/os/Handler;

    iget-object v3, p0, Lgwe;->c:Lgwd;

    invoke-direct {v0, v1, v2, v3, p1}, Lalz;-><init>(Landroid/content/Context;Landroid/os/Handler;Lgwd;Laco;)V

    iput-object v0, p0, Lgwe;->g:Lalz;

    .line 194
    iget-object v0, p0, Lgwe;->e:Lgwl;

    if-eqz v0, :cond_0

    .line 196
    :try_start_0
    iget-object v0, p0, Lgwe;->e:Lgwl;

    invoke-interface {v0}, Lgwl;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 197
    iget-object v0, p0, Lgwe;->e:Lgwl;

    sget-object v1, Lgwg;->a:Lgwg;

    invoke-virtual {v1}, Lgwg;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lgwe;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lgwl;->a(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :cond_0
    :goto_0
    iget-object v0, p0, Lgwe;->c:Lgwd;

    invoke-virtual {v0, p0}, Lgwd;->a(Lgwb;)V

    .line 203
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lgwe;->f:Laco;

    .line 208
    const-string v0, "Error creating ApiEnvironment"

    invoke-static {v0, p1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 209
    iget-object v0, p0, Lgwe;->e:Lgwl;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lgwe;->e:Lgwl;

    invoke-static {p1}, Laco;->a(Ljava/lang/Exception;)Lgwg;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/api/service/YouTubeService;->a(Lgwl;Lgwg;)V

    .line 212
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lgwe;->b:Landroid/os/Handler;

    new-instance v1, Lgwf;

    invoke-direct {v1, p0, p1}, Lgwf;-><init>(Lgwe;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 240
    return-void
.end method

.method public final b()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 222
    invoke-direct {p0}, Lgwe;->c()V

    .line 223
    iget-object v0, p0, Lgwe;->g:Lalz;

    invoke-virtual {v0}, Lalz;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final binderDied()V
    .locals 1

    .prologue
    .line 244
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgwe;->a(Z)V

    .line 247
    iget-object v0, p0, Lgwe;->d:Ljava/lang/String;

    invoke-static {v0}, Laco;->a(Ljava/lang/String;)V

    .line 248
    return-void
.end method
