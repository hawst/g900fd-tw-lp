.class public final Lcbb;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/WrapperListAdapter;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:Lcba;

.field private final b:Landroid/view/View;

.field private final c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Lcba;ZLandroid/view/View;Z)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 37
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcba;

    iput-object v0, p0, Lcbb;->a:Lcba;

    .line 38
    const-string v0, "paddingView cannot be null"

    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcbb;->b:Landroid/view/View;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcbb;->c:Z

    .line 40
    new-instance v0, Lcbc;

    invoke-direct {v0, p0}, Lcbc;-><init>(Lcbb;)V

    invoke-virtual {p1, v0}, Lcba;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 41
    if-eqz p4, :cond_0

    .line 42
    invoke-virtual {p0}, Lcbb;->a()V

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcbb;->b()V

    goto :goto_0
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcbb;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcbb;->c:Z

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    :cond_0
    return p1
.end method

.method public static a(Lcba;Landroid/view/View;Z)Lcbb;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    new-instance v0, Lcbb;

    invoke-direct {v0, p0, v1, p1, v1}, Lcbb;-><init>(Lcba;ZLandroid/view/View;Z)V

    return-object v0
.end method

.method private b(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 171
    iget-boolean v1, p0, Lcbb;->d:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcbb;->c:Z

    if-eqz v1, :cond_1

    move v1, v0

    :goto_0
    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Lcbb;->a:Lcba;

    invoke-virtual {v1}, Lcba;->getCount()I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 142
    iget-boolean v0, p0, Lcbb;->d:Z

    .line 143
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcbb;->d:Z

    .line 144
    if-nez v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcbb;->notifyDataSetChanged()V

    .line 147
    :cond_0
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcbb;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-virtual {v0}, Lcba;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 150
    iget-boolean v0, p0, Lcbb;->d:Z

    .line 151
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcbb;->d:Z

    .line 152
    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p0}, Lcbb;->notifyDataSetChanged()V

    .line 155
    :cond_0
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-virtual {v0}, Lcba;->getCount()I

    move-result v1

    iget-boolean v0, p0, Lcbb;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcbb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-direct {p0, p1}, Lcbb;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcba;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcbb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    const-wide/16 v0, -0x1

    .line 92
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-direct {p0, p1}, Lcbb;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcba;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcbb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-virtual {v0}, Lcba;->getViewTypeCount()I

    move-result v0

    .line 68
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-direct {p0, p1}, Lcbb;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcba;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcbb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcbb;->b:Landroid/view/View;

    .line 100
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-direct {p0, p1}, Lcbb;->a(I)I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Lcba;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-virtual {v0}, Lcba;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getWrappedAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcbb;->a:Lcba;

    return-object v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-virtual {v0}, Lcba;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcbb;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 133
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcbb;->a:Lcba;

    invoke-direct {p0, p1}, Lcbb;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcba;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method
