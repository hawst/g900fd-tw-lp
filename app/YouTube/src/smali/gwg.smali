.class public final enum Lgwg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgwg;

.field public static final enum b:Lgwg;

.field public static final enum c:Lgwg;

.field public static final enum d:Lgwg;

.field public static final enum e:Lgwg;

.field public static final enum f:Lgwg;

.field private static enum g:Lgwg;

.field private static enum h:Lgwg;

.field private static enum i:Lgwg;

.field private static enum j:Lgwg;

.field private static enum k:Lgwg;

.field private static enum l:Lgwg;

.field private static final synthetic m:[Lgwg;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lgwg;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->a:Lgwg;

    .line 34
    new-instance v0, Lgwg;

    const-string v1, "INTERNAL_ERROR"

    invoke-direct {v0, v1, v4}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->b:Lgwg;

    .line 41
    new-instance v0, Lgwg;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v5}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->g:Lgwg;

    .line 48
    new-instance v0, Lgwg;

    const-string v1, "SERVICE_MISSING"

    invoke-direct {v0, v1, v6}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->h:Lgwg;

    .line 55
    new-instance v0, Lgwg;

    const-string v1, "SERVICE_VERSION_UPDATE_REQUIRED"

    invoke-direct {v0, v1, v7}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->i:Lgwg;

    .line 62
    new-instance v0, Lgwg;

    const-string v1, "SERVICE_DISABLED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->j:Lgwg;

    .line 69
    new-instance v0, Lgwg;

    const-string v1, "SERVICE_INVALID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->k:Lgwg;

    .line 74
    new-instance v0, Lgwg;

    const-string v1, "ERROR_CONNECTING_TO_SERVICE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->l:Lgwg;

    .line 81
    new-instance v0, Lgwg;

    const-string v1, "CLIENT_LIBRARY_UPDATE_REQUIRED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->c:Lgwg;

    .line 87
    new-instance v0, Lgwg;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->d:Lgwg;

    .line 95
    new-instance v0, Lgwg;

    const-string v1, "DEVELOPER_KEY_INVALID"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->e:Lgwg;

    .line 105
    new-instance v0, Lgwg;

    const-string v1, "INVALID_APPLICATION_SIGNATURE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lgwg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgwg;->f:Lgwg;

    .line 24
    const/16 v0, 0xc

    new-array v0, v0, [Lgwg;

    sget-object v1, Lgwg;->a:Lgwg;

    aput-object v1, v0, v3

    sget-object v1, Lgwg;->b:Lgwg;

    aput-object v1, v0, v4

    sget-object v1, Lgwg;->g:Lgwg;

    aput-object v1, v0, v5

    sget-object v1, Lgwg;->h:Lgwg;

    aput-object v1, v0, v6

    sget-object v1, Lgwg;->i:Lgwg;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgwg;->j:Lgwg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgwg;->k:Lgwg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgwg;->l:Lgwg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lgwg;->c:Lgwg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lgwg;->d:Lgwg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lgwg;->e:Lgwg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lgwg;->f:Lgwg;

    aput-object v2, v0, v1

    sput-object v0, Lgwg;->m:[Lgwg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 213
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgwg;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lgwg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgwg;

    return-object v0
.end method

.method public static values()[Lgwg;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lgwg;->m:[Lgwg;

    invoke-virtual {v0}, [Lgwg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgwg;

    return-object v0
.end method
