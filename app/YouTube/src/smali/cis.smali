.class public Lcis;
.super Lfsa;
.source "SourceFile"


# instance fields
.field final a:Lfhz;

.field b:Z

.field private final c:Landroid/content/Context;

.field private final d:Leyp;

.field private final e:Lfsj;

.field private final f:Landroid/content/res/Resources;

.field private final g:Landroid/view/LayoutInflater;

.field private final h:Landroid/widget/LinearLayout;

.field private final i:Landroid/widget/LinearLayout;

.field private j:Landroid/widget/FrameLayout;

.field private m:Landroid/widget/ImageView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/LinearLayout;

.field private p:Z

.field private q:Z

.field private r:I

.field private s:Lfno;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 87
    invoke-direct {p0, p5, p6}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 88
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcis;->c:Landroid/content/Context;

    .line 89
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcis;->d:Leyp;

    .line 90
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcis;->e:Lfsj;

    .line 91
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcis;->a:Lfhz;

    .line 93
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcis;->f:Landroid/content/res/Resources;

    .line 94
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    .line 96
    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->a()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 97
    const v0, 0x7f080290

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    .line 98
    const v0, 0x7f080291

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcis;->i:Landroid/widget/LinearLayout;

    .line 100
    iput-boolean v3, p0, Lcis;->b:Z

    .line 101
    iput-boolean v3, p0, Lcis;->p:Z

    .line 102
    iput-boolean v3, p0, Lcis;->q:Z

    .line 104
    invoke-interface {p3, v1}, Lfsj;->a(Landroid/view/View;)V

    .line 105
    return-void
.end method

.method private a(Lfsg;Lfno;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 109
    iget-boolean v0, p0, Lcis;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcis;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iget v2, p0, Lcis;->r:I

    if-ne v0, v2, :cond_0

    .line 110
    iget-object v0, p0, Lcis;->e:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    .line 113
    :cond_0
    iget-boolean v0, p0, Lcis;->p:Z

    if-nez v0, :cond_1

    .line 114
    iput-object p2, p0, Lcis;->s:Lfno;

    .line 115
    iget-object v0, p0, Lcis;->s:Lfno;

    iget-object v0, v0, Lfno;->a:Libb;

    iget-boolean v0, v0, Libb;->f:Z

    if-nez v0, :cond_8

    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lcis;->b:Z

    .line 118
    :cond_1
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f080299

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 121
    :cond_2
    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->b()I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 122
    iget-object v2, p0, Lcis;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 124
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f08029d

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 125
    iget-object v2, p0, Lcis;->s:Lfno;

    iget-object v4, v2, Lfno;->b:Ljava/lang/CharSequence;

    if-nez v4, :cond_3

    iget-object v4, v2, Lfno;->a:Libb;

    iget-object v4, v4, Libb;->a:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, v2, Lfno;->b:Ljava/lang/CharSequence;

    :cond_3
    iget-object v2, v2, Lfno;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f08029e

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcis;->n:Landroid/widget/TextView;

    .line 128
    iget-object v0, p0, Lcis;->n:Landroid/widget/TextView;

    iget-object v2, p0, Lcis;->f:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    const/4 v4, 0x4

    invoke-static {v2, v4}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v2

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 129
    iget-object v0, p0, Lcis;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 130
    iget-object v0, p0, Lcis;->n:Landroid/widget/TextView;

    iget-object v2, p0, Lcis;->s:Lfno;

    iget-object v4, v2, Lfno;->c:Ljava/lang/CharSequence;

    if-nez v4, :cond_4

    iget-object v4, v2, Lfno;->a:Libb;

    iget-object v4, v4, Libb;->b:[Lhgz;

    if-eqz v4, :cond_4

    iget-object v4, v2, Lfno;->a:Libb;

    iget-object v4, v4, Libb;->b:[Lhgz;

    aget-object v1, v4, v1

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, v2, Lfno;->c:Ljava/lang/CharSequence;

    :cond_4
    iget-object v1, v2, Lfno;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f08029c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcis;->m:Landroid/widget/ImageView;

    .line 133
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f08029a

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcis;->j:Landroid/widget/FrameLayout;

    .line 135
    iget-object v0, p0, Lcis;->j:Landroid/widget/FrameLayout;

    new-instance v1, Lcit;

    invoke-direct {v1, p0}, Lcit;-><init>(Lcis;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f080293

    .line 144
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 145
    iget-object v1, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v2, 0x7f080294

    .line 146
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/youtube/common/ui/FixedAspectRatioRelativeLayout;

    .line 147
    const v2, 0x7f080298

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 149
    iget-object v4, p0, Lcis;->s:Lfno;

    invoke-virtual {v4}, Lfno;->a()Lfnq;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 150
    iget-object v4, p0, Lcis;->f:Landroid/content/res/Resources;

    const v5, 0x7f0c0005

    invoke-virtual {v4, v5, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v4

    iput v4, v1, Lcom/google/android/libraries/youtube/common/ui/FixedAspectRatioRelativeLayout;->a:F

    .line 151
    const v1, 0x7f080296

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_5

    .line 153
    const v1, 0x7f080295

    .line 154
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 155
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 157
    :cond_5
    iget-object v1, p0, Lcis;->s:Lfno;

    invoke-virtual {v1}, Lfno;->a()Lfnq;

    move-result-object v4

    .line 158
    iget-object v1, v4, Lfnq;->a:Liat;

    iget-object v1, v1, Liat;->d:Lhog;

    .line 159
    const v5, 0x7f080337

    .line 162
    invoke-virtual {v4}, Lfnq;->c()Lfnc;

    move-result-object v6

    .line 159
    invoke-direct {p0, v0, v5, v6}, Lcis;->a(Landroid/view/View;ILfnc;)V

    .line 163
    const v5, 0x7f080338

    .line 166
    invoke-virtual {v4}, Lfnq;->d()Lfnc;

    move-result-object v6

    .line 163
    invoke-direct {p0, v0, v5, v6}, Lcis;->a(Landroid/view/View;ILfnc;)V

    .line 167
    const v5, 0x7f080339

    .line 170
    invoke-virtual {v4}, Lfnq;->e()Lfnc;

    move-result-object v6

    .line 167
    invoke-direct {p0, v0, v5, v6}, Lcis;->a(Landroid/view/View;ILfnc;)V

    .line 171
    invoke-virtual {v4}, Lfnq;->f()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    :goto_2
    new-instance v2, Lciu;

    invoke-direct {v2, p0, v1}, Lciu;-><init>(Lcis;Lhog;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-boolean v1, p0, Lcis;->p:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcis;->q:Z

    if-eqz v1, :cond_6

    .line 193
    invoke-direct {p0}, Lcis;->e()V

    .line 195
    :cond_6
    invoke-direct {p0}, Lcis;->f()V

    .line 196
    iput-boolean v3, p0, Lcis;->p:Z

    .line 197
    iget-object v1, p0, Lcis;->f:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcis;->r:I

    .line 198
    iget v1, p0, Lcis;->r:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_7

    .line 200
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 201
    iget-object v2, p0, Lcis;->s:Lfno;

    invoke-virtual {v2}, Lfno;->a()Lfnq;

    move-result-object v2

    if-nez v2, :cond_d

    .line 202
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 206
    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    iget-object v0, p0, Lcis;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 208
    const/high16 v1, 0x40000000    # 2.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 209
    iget-object v1, p0, Lcis;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 211
    :cond_7
    iget-object v0, p0, Lcis;->e:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    move v0, v1

    .line 115
    goto/16 :goto_1

    .line 173
    :cond_9
    iget-object v1, p0, Lcis;->s:Lfno;

    iget-object v4, v1, Lfno;->d:Lfnv;

    if-nez v4, :cond_a

    iget-object v4, v1, Lfno;->a:Libb;

    iget-object v4, v4, Libb;->c:Lias;

    iget-object v4, v4, Lias;->b:Libc;

    if-eqz v4, :cond_a

    new-instance v4, Lfnv;

    iget-object v5, v1, Lfno;->a:Libb;

    iget-object v5, v5, Libb;->c:Lias;

    iget-object v5, v5, Lias;->b:Libc;

    invoke-direct {v4, v5}, Lfnv;-><init>(Libc;)V

    iput-object v4, v1, Lfno;->d:Lfnv;

    :cond_a
    iget-object v4, v1, Lfno;->d:Lfnv;

    .line 174
    iget-object v1, v4, Lfnv;->a:Libc;

    iget-object v1, v1, Libc;->b:Lhog;

    .line 175
    const v5, 0x7f080297

    .line 178
    iget-object v6, v4, Lfnv;->b:Lfnc;

    if-nez v6, :cond_b

    new-instance v6, Lfnc;

    iget-object v7, v4, Lfnv;->a:Libc;

    iget-object v7, v7, Libc;->a:Lhxf;

    invoke-direct {v6, v7}, Lfnc;-><init>(Lhxf;)V

    iput-object v6, v4, Lfnv;->b:Lfnc;

    :cond_b
    iget-object v6, v4, Lfnv;->b:Lfnc;

    .line 175
    invoke-direct {p0, v0, v5, v6}, Lcis;->a(Landroid/view/View;ILfnc;)V

    .line 179
    iget-object v5, v4, Lfnv;->c:Ljava/lang/CharSequence;

    if-nez v5, :cond_c

    iget-object v5, v4, Lfnv;->a:Libc;

    iget-object v5, v5, Libc;->c:Lhgz;

    invoke-static {v5}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v5

    iput-object v5, v4, Lfnv;->c:Ljava/lang/CharSequence;

    :cond_c
    iget-object v4, v4, Lfnv;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 204
    :cond_d
    const/high16 v2, 0x40800000    # 4.0f

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_3
.end method

.method private a(Landroid/view/View;ILfnc;)V
    .locals 3

    .prologue
    .line 408
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 410
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfvi;

    .line 411
    if-nez v1, :cond_0

    .line 412
    new-instance v1, Lfvi;

    iget-object v2, p0, Lcis;->d:Leyp;

    invoke-direct {v1, v2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    .line 413
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 416
    :cond_0
    invoke-virtual {p3}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lfvi;->a(I)V

    .line 417
    const/4 v0, 0x0

    invoke-virtual {v1, p3, v0}, Lfvi;->a(Lfnc;Leyo;)V

    .line 418
    return-void

    .line 416
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic a(Lcis;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcis;->f()V

    return-void
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v8, -0x1

    const/4 v3, 0x0

    .line 255
    iget-object v0, p0, Lcis;->s:Lfno;

    invoke-virtual {v0}, Lfno;->h()Ljava/util/List;

    move-result-object v4

    .line 257
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    :cond_0
    return-void

    .line 261
    :cond_1
    iget-boolean v0, p0, Lcis;->q:Z

    if-nez v0, :cond_3

    .line 262
    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->c()I

    move-result v1

    iget-object v2, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 263
    iget-object v0, p0, Lcis;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 264
    iget-object v1, p0, Lcis;->s:Lfno;

    iget-object v2, v1, Lfno;->e:Ljava/lang/CharSequence;

    if-nez v2, :cond_2

    iget-object v2, v1, Lfno;->a:Libb;

    iget-object v2, v2, Libb;->d:Liaz;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lfno;->a:Libb;

    iget-object v2, v2, Libb;->d:Liaz;

    iget-object v2, v2, Liaz;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfno;->e:Ljava/lang/CharSequence;

    :cond_2
    iget-object v1, v1, Lfno;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->d()I

    move-result v1

    iget-object v2, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 268
    :cond_3
    iget-object v0, p0, Lcis;->o:Landroid/widget/LinearLayout;

    if-nez v0, :cond_4

    .line 269
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcis;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcis;->o:Landroid/widget/LinearLayout;

    .line 270
    iget-object v0, p0, Lcis;->o:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v8, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 272
    iget-object v0, p0, Lcis;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 273
    iget-object v0, p0, Lcis;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x7

    invoke-static {v0, v1}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    .line 274
    iget-object v1, p0, Lcis;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 275
    iget-object v0, p0, Lcis;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcis;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 280
    :goto_0
    iget-object v0, p0, Lcis;->f:Landroid/content/res/Resources;

    const v1, 0x7f0b001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 281
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 282
    iget-object v1, p0, Lcis;->o:Landroid/widget/LinearLayout;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move v2, v3

    .line 284
    :goto_1
    if-ge v2, v5, :cond_0

    .line 285
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnu;

    iget-object v1, p0, Lcis;->g:Landroid/view/LayoutInflater;

    const v6, 0x7f040136

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    const v1, 0x7f08008b

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lfnu;->c()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0800a4

    invoke-virtual {v0}, Lfnu;->d()Lfnc;

    move-result-object v7

    invoke-direct {p0, v6, v1, v7}, Lcis;->a(Landroid/view/View;ILfnc;)V

    iget-object v0, v0, Lfnu;->a:Liba;

    iget-object v0, v0, Liba;->d:Lhog;

    new-instance v1, Lcix;

    invoke-direct {v1, p0, v0}, Lcix;-><init>(Lcis;Lhog;)V

    invoke-virtual {v6, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    iget-object v0, p0, Lcis;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 287
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v8, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 289
    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 277
    :cond_4
    iget-object v0, p0, Lcis;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0
.end method

.method private f()V
    .locals 11

    .prologue
    const v10, 0x7f0800a4

    const v9, 0x7f08008b

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v8, 0x1

    .line 294
    iget-boolean v0, p0, Lcis;->b:Z

    if-eqz v0, :cond_b

    .line 295
    iget-boolean v0, p0, Lcis;->q:Z

    if-nez v0, :cond_9

    .line 296
    iget-object v0, p0, Lcis;->s:Lfno;

    invoke-virtual {v0}, Lfno;->f()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcis;->s:Lfno;

    invoke-virtual {v0}, Lfno;->b()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->c()I

    move-result v1

    iget-object v2, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcis;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcis;->s:Lfno;

    invoke-virtual {v1}, Lfno;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->d()I

    move-result v1

    iget-object v2, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move v2, v3

    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->d()I

    move-result v1

    iget-object v5, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_1
    iget-object v5, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnw;

    iget-object v1, p0, Lcis;->g:Landroid/view/LayoutInflater;

    const v6, 0x7f040139

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lfnw;->d()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f08012f

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lfnw;->e()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v1, v7}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lfnw;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    const v1, 0x7f08033d

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0}, Lfnw;->c()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {v0}, Lfnw;->f()Lfnc;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lfnw;->f()Lfnc;

    move-result-object v1

    invoke-direct {p0, v6, v10, v1}, Lcis;->a(Landroid/view/View;ILfnc;)V

    :cond_3
    iget-object v0, v0, Lfnw;->a:Libd;

    iget-object v0, v0, Libd;->f:Lhog;

    const v1, 0x7f08033c

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v7, Lciv;

    invoke-direct {v7, p0, v0}, Lciv;-><init>(Lcis;Lhog;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 297
    :cond_4
    iget-object v0, p0, Lcis;->s:Lfno;

    invoke-virtual {v0}, Lfno;->g()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcis;->s:Lfno;

    invoke-virtual {v0}, Lfno;->c()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->c()I

    move-result v1

    iget-object v2, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcis;->i:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcis;->s:Lfno;

    invoke-virtual {v1}, Lfno;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->d()I

    move-result v1

    iget-object v2, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move v2, v3

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcis;->g:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcis;->d()I

    move-result v1

    iget-object v5, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v5, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    :cond_6
    iget-object v5, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnp;

    iget-object v1, p0, Lcis;->g:Landroid/view/LayoutInflater;

    const v6, 0x7f04012e

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lfnp;->c()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f080335

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lfnp;->d()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v1, v7}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lfnp;->e()Lfnc;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lfnp;->e()Lfnc;

    move-result-object v1

    invoke-direct {p0, v6, v10, v1}, Lcis;->a(Landroid/view/View;ILfnc;)V

    :cond_7
    iget-object v0, v0, Lfnp;->a:Liaq;

    iget-object v0, v0, Liaq;->e:Lhog;

    const v1, 0x7f080334

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v7, Lciw;

    invoke-direct {v7, p0, v0}, Lciw;-><init>(Lcis;Lhog;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 298
    :cond_8
    invoke-direct {p0}, Lcis;->e()V

    .line 299
    iput-boolean v8, p0, Lcis;->q:Z

    .line 301
    :cond_9
    iget-object v0, p0, Lcis;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_a

    .line 302
    iget-object v0, p0, Lcis;->j:Landroid/widget/FrameLayout;

    const v1, 0x7f08029b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 304
    invoke-virtual {v0, v1, v1, v1, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 308
    :goto_2
    iget-object v0, p0, Lcis;->m:Landroid/widget/ImageView;

    const v1, 0x7f0200fa

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 309
    iget-object v0, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 321
    :goto_3
    return-void

    .line 306
    :cond_a
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f080292

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 311
    :cond_b
    iget-object v0, p0, Lcis;->f:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v8, :cond_c

    .line 312
    iget-object v0, p0, Lcis;->j:Landroid/widget/FrameLayout;

    const v1, 0x7f08029b

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 313
    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 314
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 318
    :goto_4
    iget-object v0, p0, Lcis;->m:Landroid/widget/ImageView;

    const v1, 0x7f02011d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 319
    iget-object v0, p0, Lcis;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_3

    .line 316
    :cond_c
    iget-object v0, p0, Lcis;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f080292

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 421
    const v0, 0x7f04012d

    return v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lfno;

    invoke-direct {p0, p1, p2}, Lcis;->a(Lfsg;Lfno;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 51
    check-cast p2, Lfno;

    invoke-direct {p0, p1, p2}, Lcis;->a(Lfsg;Lfno;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 425
    const v0, 0x7f040131

    return v0
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 441
    const v0, 0x7f040138

    return v0
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 445
    const v0, 0x7f040133

    return v0
.end method
