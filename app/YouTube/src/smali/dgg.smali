.class public final Ldgg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldgn;
.implements Lfrw;


# instance fields
.field public a:Ldgm;

.field private b:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldgg;->b:Ljava/util/List;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Ldgk;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldgg;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public final a(Lhog;)V
    .locals 7

    .prologue
    .line 36
    iget-object v0, p0, Ldgg;->a:Ldgm;

    const-string v1, "must call init()"

    invoke-static {v0, v1}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-static {p1}, La;->b(Lhog;)Lhqt;

    move-result-object v0

    .line 39
    if-nez v0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    iget-boolean v1, v0, Lhqt;->b:Z

    if-nez v1, :cond_0

    iget v0, v0, Lhqt;->a:I

    if-nez v0, :cond_0

    .line 50
    iget-object v4, p0, Ldgg;->a:Ldgm;

    .line 51
    new-instance v0, Ldgk;

    iget-object v1, v4, Ldgm;->a:Levn;

    iget-object v2, v4, Ldgm;->b:Ljava/util/concurrent/Executor;

    iget-object v3, v4, Ldgm;->c:Ldaq;

    iget-object v4, v4, Ldgm;->d:Lcws;

    move-object v5, p1

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Ldgk;-><init>(Levn;Ljava/util/concurrent/Executor;Ldaq;Lcws;Lhog;Ldgn;)V

    .line 52
    iget-object v1, p0, Ldgg;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    invoke-virtual {v0}, Ldgk;->b()V

    goto :goto_0
.end method
