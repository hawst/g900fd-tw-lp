.class final Lgry;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgsa;
.implements Lgsb;


# instance fields
.field a:Lgsa;

.field private final b:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 456
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lgry;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Lgsa;)Lgsa;
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lgry;->a:Lgsa;

    .line 460
    iget-object v1, p0, Lgry;->a:Lgsa;

    if-eqz v1, :cond_0

    .line 461
    iget-object v1, p0, Lgry;->a:Lgsa;

    invoke-interface {v1, p0}, Lgsa;->b(Lgsb;)V

    .line 463
    :cond_0
    iput-object p1, p0, Lgry;->a:Lgsa;

    .line 464
    iget-object v1, p0, Lgry;->a:Lgsa;

    if-eqz v1, :cond_1

    .line 465
    iget-object v1, p0, Lgry;->a:Lgsa;

    invoke-interface {v1, p0}, Lgsa;->a(Lgsb;)V

    .line 467
    :cond_1
    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 514
    return-void
.end method

.method public final a(Lgsa;Lgrs;)V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lgry;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    .line 519
    invoke-interface {v0, p0, p2}, Lgsb;->a(Lgsa;Lgrs;)V

    goto :goto_0

    .line 521
    :cond_0
    return-void
.end method

.method public final a(Lgsb;)V
    .locals 2

    .prologue
    .line 497
    iget-object v1, p0, Lgry;->b:Ljava/util/List;

    monitor-enter v1

    .line 498
    :try_start_0
    iget-object v0, p0, Lgry;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 499
    invoke-virtual {p0}, Lgry;->d()Z

    move-result v0

    .line 500
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    if-eqz v0, :cond_0

    .line 502
    invoke-interface {p1, p0}, Lgsb;->b(Lgsa;)V

    .line 504
    :cond_0
    return-void

    .line 500
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 537
    iget-object v1, p0, Lgry;->b:Ljava/util/List;

    monitor-enter v1

    .line 538
    :try_start_0
    iget-object v0, p0, Lgry;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 539
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    invoke-interface {v0, p1}, Lgsb;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 539
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 543
    :cond_0
    return-void
.end method

.method public final a(JJ)Z
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lgry;->a:Lgsa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgry;->a:Lgsa;

    invoke-interface {v0, p1, p2, p3, p4}, Lgsa;->a(JJ)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(JZ)Lgrs;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lgry;->a:Lgsa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgry;->a:Lgsa;

    invoke-interface {v0, p1, p2, p3}, Lgsa;->b(JZ)Lgrs;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lgsa;)V
    .locals 3

    .prologue
    .line 526
    iget-object v1, p0, Lgry;->b:Ljava/util/List;

    monitor-enter v1

    .line 527
    :try_start_0
    iget-object v0, p0, Lgry;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 528
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    invoke-interface {v0, p0}, Lgsb;->b(Lgsa;)V

    goto :goto_0

    .line 528
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 532
    :cond_0
    return-void
.end method

.method public final b(Lgsb;)V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lgry;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 509
    return-void
.end method

.method public final c(J)Lgrs;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lgry;->a:Lgsa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgry;->a:Lgsa;

    invoke-interface {v0, p1, p2}, Lgsa;->c(J)Lgrs;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lgry;->a:Lgsa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgry;->a:Lgsa;

    invoke-interface {v0}, Lgsa;->d()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
