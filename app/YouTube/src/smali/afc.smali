.class public final Lafc;
.super Ladp;
.source "SourceFile"

# interfaces
.implements Ladt;
.implements Lakw;


# instance fields
.field private final a:Ladu;

.field private b:Landroid/view/SurfaceView;

.field private final c:Landroid/view/View;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ladu;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0, p1}, Ladp;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladu;

    iput-object v0, p0, Lafc;->a:Ladu;

    .line 39
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lafc;->c:Landroid/view/View;

    .line 40
    iget-object v0, p0, Lafc;->c:Landroid/view/View;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 42
    iget-object v0, p0, Lafc;->c:Landroid/view/View;

    invoke-virtual {p0, v0}, Lafc;->addView(Landroid/view/View;)V

    .line 44
    new-instance v0, Lafd;

    invoke-direct {v0, p0}, Lafd;-><init>(Lafc;)V

    iput-object v0, p0, Lafc;->d:Ljava/lang/Runnable;

    .line 50
    return-void
.end method

.method static synthetic a(Lafc;)Landroid/view/View;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lafc;->c:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 108
    return-object p0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lafc;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lafc;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 124
    return-void
.end method

.method public final a(Landroid/view/Surface;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lafc;->a:Ladu;

    invoke-interface {v0}, Ladu;->a()V

    .line 97
    return-void
.end method

.method public final a(Landroid/view/SurfaceView;)V
    .locals 1

    .prologue
    .line 81
    iput-object p1, p0, Lafc;->b:Landroid/view/SurfaceView;

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lafc;->addView(Landroid/view/View;I)V

    .line 84
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lafc;->removeView(Landroid/view/View;)V

    .line 115
    iget-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lafc;->addView(Landroid/view/View;I)V

    .line 117
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lafc;->d:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lafc;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 129
    iget-object v0, p0, Lafc;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    invoke-virtual {p0, v0}, Lafc;->removeView(Landroid/view/View;)V

    .line 91
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    .line 92
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lafc;->a:Ladu;

    invoke-interface {v0}, Ladu;->b()V

    .line 102
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 69
    iget-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lafc;->b:Landroid/view/SurfaceView;

    iget-object v1, p0, Lafc;->b:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lafc;->b:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/SurfaceView;->layout(IIII)V

    .line 72
    :cond_0
    iget-object v0, p0, Lafc;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    .line 73
    iget-object v0, p0, Lafc;->c:Landroid/view/View;

    iget-object v1, p0, Lafc;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lafc;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 75
    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 54
    invoke-super {p0, p1, p2}, Ladp;->onMeasure(II)V

    .line 56
    invoke-virtual {p0}, Lafc;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 57
    invoke-virtual {p0}, Lafc;->getMeasuredHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 59
    iget-object v2, p0, Lafc;->b:Landroid/view/SurfaceView;

    if-eqz v2, :cond_0

    .line 60
    iget-object v2, p0, Lafc;->b:Landroid/view/SurfaceView;

    invoke-virtual {v2, v0, v1}, Landroid/view/SurfaceView;->measure(II)V

    .line 62
    :cond_0
    iget-object v2, p0, Lafc;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_1

    .line 63
    iget-object v2, p0, Lafc;->c:Landroid/view/View;

    invoke-virtual {v2, v0, v1}, Landroid/view/View;->measure(II)V

    .line 65
    :cond_1
    return-void
.end method
