.class public final Lewf;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lewg;

.field private final b:Ljava/lang/String;

.field private c:Lewg;

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 280
    new-instance v0, Lewg;

    invoke-direct {v0}, Lewg;-><init>()V

    iput-object v0, p0, Lewf;->c:Lewg;

    .line 281
    iget-object v0, p0, Lewf;->c:Lewg;

    iput-object v0, p0, Lewf;->a:Lewg;

    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lewf;->d:Z

    .line 288
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lewf;->b:Ljava/lang/String;

    .line 289
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 467
    const-string v1, ""

    .line 469
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x20

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v2, p0, Lewf;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x7b

    .line 470
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 471
    iget-object v0, p0, Lewf;->c:Lewg;

    iget-object v0, v0, Lewg;->c:Lewg;

    :goto_0
    if-eqz v0, :cond_1

    .line 473
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    const-string v1, ", "

    .line 477
    iget-object v3, v0, Lewg;->a:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 478
    iget-object v3, v0, Lewg;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x3d

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 480
    :cond_0
    iget-object v3, v0, Lewg;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 472
    iget-object v0, v0, Lewg;->c:Lewg;

    goto :goto_0

    .line 483
    :cond_1
    const/16 v0, 0x7d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
