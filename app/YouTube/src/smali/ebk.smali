.class public final Lebk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lebk;->a:Ljava/util/HashMap;

    return-void
.end method

.method public static a(Ljava/lang/String;Z)Leap;
    .locals 5

    .prologue
    .line 37
    invoke-static {p0, p1}, Lebk;->b(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v1

    .line 38
    if-nez v1, :cond_0

    .line 39
    const/4 v0, 0x0

    .line 41
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Leap;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/media/MediaCodecInfo$CodecCapabilities;

    sget v3, Legz;->a:I

    const/16 v4, 0x13

    if-lt v3, v4, :cond_1

    const-string v3, "adaptive-playback"

    invoke-virtual {v1, v3}, Landroid/media/MediaCodecInfo$CodecCapabilities;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v1

    :goto_1
    invoke-direct {v2, v0, v1}, Leap;-><init>(Ljava/lang/String;Z)V

    move-object v0, v2

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(II)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 124
    const-string v0, "video/avc"

    invoke-static {v0, v2}, Lebk;->b(Ljava/lang/String;Z)Landroid/util/Pair;

    move-result-object v0

    .line 125
    if-nez v0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v2

    .line 129
    :cond_1
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaCodecInfo$CodecCapabilities;

    move v1, v2

    .line 130
    :goto_1
    iget-object v3, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    array-length v3, v3

    if-ge v1, v3, :cond_0

    .line 131
    iget-object v3, v0, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    aget-object v3, v3, v1

    .line 132
    iget v4, v3, Landroid/media/MediaCodecInfo$CodecProfileLevel;->profile:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    iget v3, v3, Landroid/media/MediaCodecInfo$CodecProfileLevel;->level:I

    const/16 v4, 0x40

    if-lt v3, v4, :cond_2

    .line 133
    const/4 v2, 0x1

    goto :goto_0

    .line 130
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static declared-synchronized b(Ljava/lang/String;Z)Landroid/util/Pair;
    .locals 17

    .prologue
    .line 62
    const-class v7, Lebk;

    monitor-enter v7

    :try_start_0
    new-instance v3, Lebl;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lebl;-><init>(Ljava/lang/String;Z)V

    .line 63
    sget-object v2, Lebk;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    sget-object v2, Lebk;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :goto_0
    monitor-exit v7

    return-object v2

    .line 66
    :cond_0
    :try_start_1
    sget v2, Legz;->a:I

    const/16 v4, 0x15

    if-lt v2, v4, :cond_2

    new-instance v2, Lebo;

    move/from16 v0, p1

    invoke-direct {v2, v0}, Lebo;-><init>(Z)V

    move-object v6, v2

    .line 68
    :goto_1
    invoke-interface {v6}, Lebm;->a()I

    move-result v8

    .line 69
    invoke-interface {v6}, Lebm;->b()Z

    move-result v9

    .line 71
    const/4 v2, 0x0

    move v5, v2

    :goto_2
    if-ge v5, v8, :cond_9

    .line 72
    invoke-interface {v6, v5}, Lebm;->a(I)Landroid/media/MediaCodecInfo;

    move-result-object v10

    .line 73
    invoke-virtual {v10}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v11

    .line 74
    invoke-virtual {v10}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "OMX."

    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    if-nez v9, :cond_1

    const-string v2, ".secure"

    .line 75
    invoke-virtual {v11, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 76
    :cond_1
    invoke-virtual {v10}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v12

    .line 77
    const/4 v2, 0x0

    move v4, v2

    :goto_3
    array-length v2, v12

    if-ge v4, v2, :cond_8

    .line 78
    aget-object v2, v12, v4

    .line 79
    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 80
    invoke-virtual {v10, v2}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v13

    .line 81
    if-nez v9, :cond_5

    .line 84
    sget-object v14, Lebk;->a:Ljava/util/HashMap;

    iget-boolean v2, v3, Lebl;->a:Z

    if-eqz v2, :cond_3

    new-instance v2, Lebl;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v15}, Lebl;-><init>(Ljava/lang/String;Z)V

    .line 85
    :goto_4
    invoke-static {v11, v13}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v15

    .line 84
    invoke-virtual {v14, v2, v15}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v14, Lebk;->a:Ljava/util/HashMap;

    iget-boolean v2, v3, Lebl;->a:Z

    if-eqz v2, :cond_4

    move-object v2, v3

    :goto_5
    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    const-string v16, ".secure"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 87
    invoke-static {v15, v13}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v13

    .line 86
    invoke-virtual {v14, v2, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :goto_6
    sget-object v2, Lebk;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 96
    sget-object v2, Lebk;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    goto/16 :goto_0

    .line 66
    :cond_2
    new-instance v2, Lebn;

    invoke-direct {v2}, Lebn;-><init>()V

    move-object v6, v2

    goto/16 :goto_1

    :cond_3
    move-object v2, v3

    .line 84
    goto :goto_4

    .line 86
    :cond_4
    new-instance v2, Lebl;

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v15}, Lebl;-><init>(Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 62
    :catchall_0
    move-exception v2

    monitor-exit v7

    throw v2

    .line 91
    :cond_5
    :try_start_2
    invoke-virtual {v10, v2}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v2

    .line 90
    invoke-interface {v6, v2}, Lebm;->a(Landroid/media/MediaCodecInfo$CodecCapabilities;)Z

    move-result v14

    .line 92
    sget-object v15, Lebk;->a:Ljava/util/HashMap;

    iget-boolean v2, v3, Lebl;->a:Z

    if-ne v2, v14, :cond_6

    move-object v2, v3

    .line 93
    :goto_7
    invoke-static {v11, v13}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v13

    .line 92
    invoke-virtual {v15, v2, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_6
    new-instance v2, Lebl;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v14}, Lebl;-><init>(Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_7

    .line 77
    :cond_7
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_3

    .line 71
    :cond_8
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_2

    .line 102
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_0
.end method
