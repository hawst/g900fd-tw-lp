.class public final Lhsi;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lhgz;

.field public c:[Lhxf;

.field public d:J

.field public e:Lhog;

.field public f:Lhgz;

.field public g:Lhgz;

.field public h:Lhgz;

.field public i:[B

.field public j:Lhsk;

.field private k:Lhgz;

.field private l:[Lhrt;

.field private m:Lgyy;

.field private n:Ljava/lang/String;

.field private o:Lhll;

.field private p:Lhgz;

.field private q:Lhgz;

.field private r:[Lhbi;

.field private s:Lhnh;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lhsi;->a:Ljava/lang/String;

    iput-object v2, p0, Lhsi;->b:Lhgz;

    sget-object v0, Lhxf;->a:[Lhxf;

    iput-object v0, p0, Lhsi;->c:[Lhxf;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhsi;->d:J

    iput-object v2, p0, Lhsi;->e:Lhog;

    iput-object v2, p0, Lhsi;->k:Lhgz;

    iput-object v2, p0, Lhsi;->f:Lhgz;

    iput-object v2, p0, Lhsi;->g:Lhgz;

    sget-object v0, Lhrt;->a:[Lhrt;

    iput-object v0, p0, Lhsi;->l:[Lhrt;

    iput-object v2, p0, Lhsi;->m:Lgyy;

    const-string v0, ""

    iput-object v0, p0, Lhsi;->n:Ljava/lang/String;

    iput-object v2, p0, Lhsi;->h:Lhgz;

    iput-object v2, p0, Lhsi;->o:Lhll;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhsi;->i:[B

    iput-object v2, p0, Lhsi;->p:Lhgz;

    iput-object v2, p0, Lhsi;->q:Lhgz;

    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhsi;->r:[Lhbi;

    iput-object v2, p0, Lhsi;->s:Lhnh;

    iput-object v2, p0, Lhsi;->j:Lhsk;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhsi;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    iget-object v2, p0, Lhsi;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhsi;->b:Lhgz;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhsi;->b:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhsi;->c:[Lhxf;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lhsi;->c:[Lhxf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    if-eqz v5, :cond_1

    const/4 v6, 0x3

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-wide v2, p0, Lhsi;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    const/4 v2, 0x4

    iget-wide v4, p0, Lhsi;->d:J

    invoke-static {v2, v4, v5}, Lidd;->d(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhsi;->e:Lhog;

    if-eqz v2, :cond_4

    const/4 v2, 0x5

    iget-object v3, p0, Lhsi;->e:Lhog;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhsi;->k:Lhgz;

    if-eqz v2, :cond_5

    const/4 v2, 0x6

    iget-object v3, p0, Lhsi;->k:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lhsi;->f:Lhgz;

    if-eqz v2, :cond_6

    const/4 v2, 0x7

    iget-object v3, p0, Lhsi;->f:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lhsi;->g:Lhgz;

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    iget-object v3, p0, Lhsi;->g:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lhsi;->l:[Lhrt;

    if-eqz v2, :cond_9

    iget-object v3, p0, Lhsi;->l:[Lhrt;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    if-eqz v5, :cond_8

    const/16 v6, 0x9

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_9
    iget-object v2, p0, Lhsi;->m:Lgyy;

    if-eqz v2, :cond_a

    const/16 v2, 0xa

    iget-object v3, p0, Lhsi;->m:Lgyy;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Lhsi;->n:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const/16 v2, 0xb

    iget-object v3, p0, Lhsi;->n:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget-object v2, p0, Lhsi;->h:Lhgz;

    if-eqz v2, :cond_c

    const/16 v2, 0xd

    iget-object v3, p0, Lhsi;->h:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lhsi;->o:Lhll;

    if-eqz v2, :cond_d

    const/16 v2, 0xe

    iget-object v3, p0, Lhsi;->o:Lhll;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget-object v2, p0, Lhsi;->i:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_e

    const/16 v2, 0xf

    iget-object v3, p0, Lhsi;->i:[B

    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget-object v2, p0, Lhsi;->p:Lhgz;

    if-eqz v2, :cond_f

    const/16 v2, 0x10

    iget-object v3, p0, Lhsi;->p:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_f
    iget-object v2, p0, Lhsi;->q:Lhgz;

    if-eqz v2, :cond_10

    const/16 v2, 0x11

    iget-object v3, p0, Lhsi;->q:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_10
    iget-object v2, p0, Lhsi;->r:[Lhbi;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lhsi;->r:[Lhbi;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_12

    aget-object v4, v2, v1

    if-eqz v4, :cond_11

    const/16 v5, 0x12

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_11
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_12
    iget-object v1, p0, Lhsi;->s:Lhnh;

    if-eqz v1, :cond_13

    const/16 v1, 0x13

    iget-object v2, p0, Lhsi;->s:Lhnh;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-object v1, p0, Lhsi;->j:Lhsk;

    if-eqz v1, :cond_14

    const/16 v1, 0x14

    iget-object v2, p0, Lhsi;->j:Lhsk;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-object v1, p0, Lhsi;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhsi;->J:I

    return v0

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhsi;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhsi;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhsi;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsi;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhsi;->b:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsi;->b:Lhgz;

    :cond_2
    iget-object v0, p0, Lhsi;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsi;->c:[Lhxf;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhxf;

    iget-object v3, p0, Lhsi;->c:[Lhxf;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhsi;->c:[Lhxf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhsi;->c:[Lhxf;

    :goto_2
    iget-object v2, p0, Lhsi;->c:[Lhxf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhsi;->c:[Lhxf;

    new-instance v3, Lhxf;

    invoke-direct {v3}, Lhxf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsi;->c:[Lhxf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhsi;->c:[Lhxf;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhsi;->c:[Lhxf;

    new-instance v3, Lhxf;

    invoke-direct {v3}, Lhxf;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsi;->c:[Lhxf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v2

    iput-wide v2, p0, Lhsi;->d:J

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lhsi;->e:Lhog;

    if-nez v0, :cond_6

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhsi;->e:Lhog;

    :cond_6
    iget-object v0, p0, Lhsi;->e:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhsi;->k:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsi;->k:Lhgz;

    :cond_7
    iget-object v0, p0, Lhsi;->k:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lhsi;->f:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsi;->f:Lhgz;

    :cond_8
    iget-object v0, p0, Lhsi;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhsi;->g:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsi;->g:Lhgz;

    :cond_9
    iget-object v0, p0, Lhsi;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsi;->l:[Lhrt;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhrt;

    iget-object v3, p0, Lhsi;->l:[Lhrt;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lhsi;->l:[Lhrt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lhsi;->l:[Lhrt;

    :goto_4
    iget-object v2, p0, Lhsi;->l:[Lhrt;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lhsi;->l:[Lhrt;

    new-instance v3, Lhrt;

    invoke-direct {v3}, Lhrt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsi;->l:[Lhrt;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lhsi;->l:[Lhrt;

    array-length v0, v0

    goto :goto_3

    :cond_c
    iget-object v2, p0, Lhsi;->l:[Lhrt;

    new-instance v3, Lhrt;

    invoke-direct {v3}, Lhrt;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsi;->l:[Lhrt;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhsi;->m:Lgyy;

    if-nez v0, :cond_d

    new-instance v0, Lgyy;

    invoke-direct {v0}, Lgyy;-><init>()V

    iput-object v0, p0, Lhsi;->m:Lgyy;

    :cond_d
    iget-object v0, p0, Lhsi;->m:Lgyy;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsi;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lhsi;->h:Lhgz;

    if-nez v0, :cond_e

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsi;->h:Lhgz;

    :cond_e
    iget-object v0, p0, Lhsi;->h:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lhsi;->o:Lhll;

    if-nez v0, :cond_f

    new-instance v0, Lhll;

    invoke-direct {v0}, Lhll;-><init>()V

    iput-object v0, p0, Lhsi;->o:Lhll;

    :cond_f
    iget-object v0, p0, Lhsi;->o:Lhll;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhsi;->i:[B

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lhsi;->p:Lhgz;

    if-nez v0, :cond_10

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsi;->p:Lhgz;

    :cond_10
    iget-object v0, p0, Lhsi;->p:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lhsi;->q:Lhgz;

    if-nez v0, :cond_11

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsi;->q:Lhgz;

    :cond_11
    iget-object v0, p0, Lhsi;->q:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_11
    const/16 v0, 0x92

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsi;->r:[Lhbi;

    if-nez v0, :cond_13

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhsi;->r:[Lhbi;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lhsi;->r:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    iput-object v2, p0, Lhsi;->r:[Lhbi;

    :goto_6
    iget-object v2, p0, Lhsi;->r:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_14

    iget-object v2, p0, Lhsi;->r:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsi;->r:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_13
    iget-object v0, p0, Lhsi;->r:[Lhbi;

    array-length v0, v0

    goto :goto_5

    :cond_14
    iget-object v2, p0, Lhsi;->r:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsi;->r:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lhsi;->s:Lhnh;

    if-nez v0, :cond_15

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhsi;->s:Lhnh;

    :cond_15
    iget-object v0, p0, Lhsi;->s:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lhsi;->j:Lhsk;

    if-nez v0, :cond_16

    new-instance v0, Lhsk;

    invoke-direct {v0}, Lhsk;-><init>()V

    iput-object v0, p0, Lhsi;->j:Lhsk;

    :cond_16
    iget-object v0, p0, Lhsi;->j:Lhsk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhsi;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lhsi;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lhsi;->b:Lhgz;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhsi;->b:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Lhsi;->c:[Lhxf;

    if-eqz v1, :cond_3

    iget-object v2, p0, Lhsi;->c:[Lhxf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    if-eqz v4, :cond_2

    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-wide v2, p0, Lhsi;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    const/4 v1, 0x4

    iget-wide v2, p0, Lhsi;->d:J

    invoke-virtual {p1, v1, v2, v3}, Lidd;->b(IJ)V

    :cond_4
    iget-object v1, p0, Lhsi;->e:Lhog;

    if-eqz v1, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lhsi;->e:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v1, p0, Lhsi;->k:Lhgz;

    if-eqz v1, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Lhsi;->k:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_6
    iget-object v1, p0, Lhsi;->f:Lhgz;

    if-eqz v1, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lhsi;->f:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_7
    iget-object v1, p0, Lhsi;->g:Lhgz;

    if-eqz v1, :cond_8

    const/16 v1, 0x8

    iget-object v2, p0, Lhsi;->g:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v1, p0, Lhsi;->l:[Lhrt;

    if-eqz v1, :cond_a

    iget-object v2, p0, Lhsi;->l:[Lhrt;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    if-eqz v4, :cond_9

    const/16 v5, 0x9

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_a
    iget-object v1, p0, Lhsi;->m:Lgyy;

    if-eqz v1, :cond_b

    const/16 v1, 0xa

    iget-object v2, p0, Lhsi;->m:Lgyy;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_b
    iget-object v1, p0, Lhsi;->n:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const/16 v1, 0xb

    iget-object v2, p0, Lhsi;->n:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_c
    iget-object v1, p0, Lhsi;->h:Lhgz;

    if-eqz v1, :cond_d

    const/16 v1, 0xd

    iget-object v2, p0, Lhsi;->h:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_d
    iget-object v1, p0, Lhsi;->o:Lhll;

    if-eqz v1, :cond_e

    const/16 v1, 0xe

    iget-object v2, p0, Lhsi;->o:Lhll;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_e
    iget-object v1, p0, Lhsi;->i:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_f

    const/16 v1, 0xf

    iget-object v2, p0, Lhsi;->i:[B

    invoke-virtual {p1, v1, v2}, Lidd;->a(I[B)V

    :cond_f
    iget-object v1, p0, Lhsi;->p:Lhgz;

    if-eqz v1, :cond_10

    const/16 v1, 0x10

    iget-object v2, p0, Lhsi;->p:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_10
    iget-object v1, p0, Lhsi;->q:Lhgz;

    if-eqz v1, :cond_11

    const/16 v1, 0x11

    iget-object v2, p0, Lhsi;->q:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_11
    iget-object v1, p0, Lhsi;->r:[Lhbi;

    if-eqz v1, :cond_13

    iget-object v1, p0, Lhsi;->r:[Lhbi;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_13

    aget-object v3, v1, v0

    if-eqz v3, :cond_12

    const/16 v4, 0x12

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_12
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_13
    iget-object v0, p0, Lhsi;->s:Lhnh;

    if-eqz v0, :cond_14

    const/16 v0, 0x13

    iget-object v1, p0, Lhsi;->s:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_14
    iget-object v0, p0, Lhsi;->j:Lhsk;

    if-eqz v0, :cond_15

    const/16 v0, 0x14

    iget-object v1, p0, Lhsi;->j:Lhsk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_15
    iget-object v0, p0, Lhsi;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhsi;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhsi;

    iget-object v2, p0, Lhsi;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhsi;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhsi;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhsi;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhsi;->c:[Lhxf;

    iget-object v3, p1, Lhsi;->c:[Lhxf;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lhsi;->d:J

    iget-wide v4, p1, Lhsi;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhsi;->e:Lhog;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhsi;->e:Lhog;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhsi;->k:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhsi;->k:Lhgz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhsi;->f:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhsi;->f:Lhgz;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhsi;->g:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhsi;->g:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhsi;->l:[Lhrt;

    iget-object v3, p1, Lhsi;->l:[Lhrt;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsi;->m:Lgyy;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhsi;->m:Lgyy;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhsi;->n:Ljava/lang/String;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhsi;->n:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhsi;->h:Lhgz;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhsi;->h:Lhgz;

    if-nez v2, :cond_3

    :goto_9
    iget-object v2, p0, Lhsi;->o:Lhll;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhsi;->o:Lhll;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhsi;->i:[B

    iget-object v3, p1, Lhsi;->i:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsi;->p:Lhgz;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhsi;->p:Lhgz;

    if-nez v2, :cond_3

    :goto_b
    iget-object v2, p0, Lhsi;->q:Lhgz;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhsi;->q:Lhgz;

    if-nez v2, :cond_3

    :goto_c
    iget-object v2, p0, Lhsi;->r:[Lhbi;

    iget-object v3, p1, Lhsi;->r:[Lhbi;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsi;->s:Lhnh;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhsi;->s:Lhnh;

    if-nez v2, :cond_3

    :goto_d
    iget-object v2, p0, Lhsi;->j:Lhsk;

    if-nez v2, :cond_11

    iget-object v2, p1, Lhsi;->j:Lhsk;

    if-nez v2, :cond_3

    :goto_e
    iget-object v2, p0, Lhsi;->I:Ljava/util/List;

    if-nez v2, :cond_12

    iget-object v2, p1, Lhsi;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhsi;->a:Ljava/lang/String;

    iget-object v3, p1, Lhsi;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhsi;->b:Lhgz;

    iget-object v3, p1, Lhsi;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhsi;->e:Lhog;

    iget-object v3, p1, Lhsi;->e:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhsi;->k:Lhgz;

    iget-object v3, p1, Lhsi;->k:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhsi;->f:Lhgz;

    iget-object v3, p1, Lhsi;->f:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhsi;->g:Lhgz;

    iget-object v3, p1, Lhsi;->g:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhsi;->m:Lgyy;

    iget-object v3, p1, Lhsi;->m:Lgyy;

    invoke-virtual {v2, v3}, Lgyy;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhsi;->n:Ljava/lang/String;

    iget-object v3, p1, Lhsi;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhsi;->h:Lhgz;

    iget-object v3, p1, Lhsi;->h:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhsi;->o:Lhll;

    iget-object v3, p1, Lhsi;->o:Lhll;

    invoke-virtual {v2, v3}, Lhll;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhsi;->p:Lhgz;

    iget-object v3, p1, Lhsi;->p:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhsi;->q:Lhgz;

    iget-object v3, p1, Lhsi;->q:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhsi;->s:Lhnh;

    iget-object v3, p1, Lhsi;->s:Lhnh;

    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v2, p0, Lhsi;->j:Lhsk;

    iget-object v3, p1, Lhsi;->j:Lhsk;

    invoke-virtual {v2, v3}, Lhsk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v2, p0, Lhsi;->I:Ljava/util/List;

    iget-object v3, p1, Lhsi;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->b:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsi;->c:[Lhxf;

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-wide v2, p0, Lhsi;->d:J

    iget-wide v4, p0, Lhsi;->d:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->e:Lhog;

    if-nez v0, :cond_8

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->k:Lhgz;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->f:Lhgz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->g:Lhgz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsi;->l:[Lhrt;

    if-nez v2, :cond_c

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsi;->m:Lgyy;

    if-nez v0, :cond_e

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->n:Ljava/lang/String;

    if-nez v0, :cond_f

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->h:Lhgz;

    if-nez v0, :cond_10

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->o:Lhll;

    if-nez v0, :cond_11

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsi;->i:[B

    if-nez v2, :cond_12

    mul-int/lit8 v2, v0, 0x1f

    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsi;->p:Lhgz;

    if-nez v0, :cond_13

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->q:Lhgz;

    if-nez v0, :cond_14

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsi;->r:[Lhbi;

    if-nez v2, :cond_15

    mul-int/lit8 v2, v0, 0x1f

    :cond_3
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsi;->s:Lhnh;

    if-nez v0, :cond_17

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsi;->j:Lhsk;

    if-nez v0, :cond_18

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhsi;->I:Ljava/util/List;

    if-nez v2, :cond_19

    :goto_e
    add-int/2addr v0, v1

    return v0

    :cond_4
    iget-object v0, p0, Lhsi;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lhsi;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_6
    move v2, v0

    move v0, v1

    :goto_f
    iget-object v3, p0, Lhsi;->c:[Lhxf;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhsi;->c:[Lhxf;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_10
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_f

    :cond_7
    iget-object v2, p0, Lhsi;->c:[Lhxf;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhxf;->hashCode()I

    move-result v2

    goto :goto_10

    :cond_8
    iget-object v0, p0, Lhsi;->e:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lhsi;->k:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lhsi;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_b
    iget-object v0, p0, Lhsi;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_c
    move v2, v0

    move v0, v1

    :goto_11
    iget-object v3, p0, Lhsi;->l:[Lhrt;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhsi;->l:[Lhrt;

    aget-object v2, v2, v0

    if-nez v2, :cond_d

    move v2, v1

    :goto_12
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_11

    :cond_d
    iget-object v2, p0, Lhsi;->l:[Lhrt;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhrt;->hashCode()I

    move-result v2

    goto :goto_12

    :cond_e
    iget-object v0, p0, Lhsi;->m:Lgyy;

    invoke-virtual {v0}, Lgyy;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_f
    iget-object v0, p0, Lhsi;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_7

    :cond_10
    iget-object v0, p0, Lhsi;->h:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_11
    iget-object v0, p0, Lhsi;->o:Lhll;

    invoke-virtual {v0}, Lhll;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_12
    move v2, v0

    move v0, v1

    :goto_13
    iget-object v3, p0, Lhsi;->i:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhsi;->i:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_13

    :cond_13
    iget-object v0, p0, Lhsi;->p:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_14
    iget-object v0, p0, Lhsi;->q:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_b

    :cond_15
    move v2, v0

    move v0, v1

    :goto_14
    iget-object v3, p0, Lhsi;->r:[Lhbi;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhsi;->r:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_16

    move v2, v1

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    :cond_16
    iget-object v2, p0, Lhsi;->r:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_15

    :cond_17
    iget-object v0, p0, Lhsi;->s:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_18
    iget-object v0, p0, Lhsi;->j:Lhsk;

    invoke-virtual {v0}, Lhsk;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_19
    iget-object v1, p0, Lhsi;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_e
.end method
