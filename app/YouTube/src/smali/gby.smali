.class public final Lgby;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:I

.field public b:Landroid/net/Uri;

.field c:I

.field public d:Ljava/lang/String;

.field e:J

.field f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348
    const/4 v0, 0x0

    iput v0, p0, Lgby;->a:I

    .line 349
    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 405
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgby;->a:I

    .line 406
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgby;->b:Landroid/net/Uri;

    .line 407
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgby;->f:Ljava/lang/String;

    .line 408
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lgby;->c:I

    .line 409
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgby;->d:Ljava/lang/String;

    .line 410
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lgby;->e:J

    .line 411
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 391
    invoke-virtual {p0}, Lgby;->a()Lgbw;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 2

    .prologue
    .line 395
    iget v0, p0, Lgby;->a:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 396
    iget-object v0, p0, Lgby;->b:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 397
    iget-object v0, p0, Lgby;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 398
    iget v0, p0, Lgby;->c:I

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 399
    iget-object v0, p0, Lgby;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 400
    iget-wide v0, p0, Lgby;->e:J

    invoke-virtual {p1, v0, v1}, Ljava/io/ObjectOutputStream;->writeLong(J)V

    .line 401
    return-void
.end method


# virtual methods
.method public final a()Lgbw;
    .locals 8

    .prologue
    .line 387
    new-instance v0, Lgbw;

    iget v1, p0, Lgby;->a:I

    iget-object v2, p0, Lgby;->b:Landroid/net/Uri;

    iget-object v3, p0, Lgby;->f:Ljava/lang/String;

    iget v4, p0, Lgby;->c:I

    iget-object v5, p0, Lgby;->d:Ljava/lang/String;

    iget-wide v6, p0, Lgby;->e:J

    invoke-direct/range {v0 .. v7}, Lgbw;-><init>(ILandroid/net/Uri;Ljava/lang/String;ILjava/lang/String;J)V

    return-object v0
.end method
