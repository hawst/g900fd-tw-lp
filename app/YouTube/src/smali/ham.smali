.class public final Lham;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field private b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11517
    invoke-direct {p0}, Lidf;-><init>()V

    .line 11520
    const/4 v0, 0x0

    iput-object v0, p0, Lham;->a:Lhgz;

    .line 11523
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lham;->b:[B

    .line 11517
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 11571
    const/4 v0, 0x0

    .line 11572
    iget-object v1, p0, Lham;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 11573
    const/4 v0, 0x1

    iget-object v1, p0, Lham;->a:Lhgz;

    .line 11574
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11576
    :cond_0
    iget-object v1, p0, Lham;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 11577
    const/4 v1, 0x3

    iget-object v2, p0, Lham;->b:[B

    .line 11578
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 11580
    :cond_1
    iget-object v1, p0, Lham;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11581
    iput v0, p0, Lham;->J:I

    .line 11582
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 11513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lham;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lham;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lham;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lham;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lham;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lham;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lham;->b:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 11559
    iget-object v0, p0, Lham;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 11560
    const/4 v0, 0x1

    iget-object v1, p0, Lham;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11562
    :cond_0
    iget-object v0, p0, Lham;->b:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 11563
    const/4 v0, 0x3

    iget-object v1, p0, Lham;->b:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 11565
    :cond_1
    iget-object v0, p0, Lham;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 11567
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11535
    if-ne p1, p0, :cond_1

    .line 11540
    :cond_0
    :goto_0
    return v0

    .line 11536
    :cond_1
    instance-of v2, p1, Lham;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 11537
    :cond_2
    check-cast p1, Lham;

    .line 11538
    iget-object v2, p0, Lham;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lham;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lham;->b:[B

    iget-object v3, p1, Lham;->b:[B

    .line 11539
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lham;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lham;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 11540
    goto :goto_0

    .line 11538
    :cond_4
    iget-object v2, p0, Lham;->a:Lhgz;

    iget-object v3, p1, Lham;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 11539
    :cond_5
    iget-object v2, p0, Lham;->I:Ljava/util/List;

    iget-object v3, p1, Lham;->I:Ljava/util/List;

    .line 11540
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11544
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 11546
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lham;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 11547
    iget-object v2, p0, Lham;->b:[B

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 11553
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lham;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_1
    add-int/2addr v0, v1

    .line 11554
    return v0

    .line 11546
    :cond_1
    iget-object v0, p0, Lham;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    .line 11549
    :goto_2
    iget-object v3, p0, Lham;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 11550
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lham;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 11549
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 11553
    :cond_3
    iget-object v1, p0, Lham;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
