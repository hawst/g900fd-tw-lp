.class public Ledx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ledv;


# instance fields
.field final a:Ledz;

.field final b:Landroid/media/MediaDrm;

.field final c:Leeb;

.field final d:Ledw;

.field final e:Leed;

.field final f:Ljava/util/UUID;

.field g:I

.field h:Z

.field i:I

.field j:[B

.field private final k:Landroid/os/Handler;

.field private l:Landroid/os/HandlerThread;

.field private m:Landroid/os/Handler;

.field private n:Landroid/media/MediaCrypto;

.field private o:Ljava/lang/Exception;

.field private p:Ljava/lang/String;

.field private q:[B


# direct methods
.method public constructor <init>(Ljava/util/UUID;Landroid/os/Looper;Ledw;Landroid/os/Handler;Ledz;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput-object p1, p0, Ledx;->f:Ljava/util/UUID;

    .line 82
    iput-object p3, p0, Ledx;->d:Ledw;

    .line 83
    iput-object p4, p0, Ledx;->k:Landroid/os/Handler;

    .line 84
    iput-object p5, p0, Ledx;->a:Ledz;

    .line 85
    new-instance v0, Landroid/media/MediaDrm;

    invoke-direct {v0, p1}, Landroid/media/MediaDrm;-><init>(Ljava/util/UUID;)V

    iput-object v0, p0, Ledx;->b:Landroid/media/MediaDrm;

    .line 86
    iget-object v0, p0, Ledx;->b:Landroid/media/MediaDrm;

    new-instance v1, Leea;

    invoke-direct {v1, p0}, Leea;-><init>(Ledx;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaDrm;->setOnEventListener(Landroid/media/MediaDrm$OnEventListener;)V

    .line 87
    new-instance v0, Leeb;

    invoke-direct {v0, p0, p2}, Leeb;-><init>(Ledx;Landroid/os/Looper;)V

    iput-object v0, p0, Ledx;->c:Leeb;

    .line 88
    new-instance v0, Leed;

    invoke-direct {v0, p0, p2}, Leed;-><init>(Ledx;Landroid/os/Looper;)V

    iput-object v0, p0, Ledx;->e:Leed;

    .line 89
    const/4 v0, 0x1

    iput v0, p0, Ledx;->i:I

    .line 90
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 166
    iget v0, p0, Ledx;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Ledx;->g:I

    if-eqz v0, :cond_1

    .line 184
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Ledx;->i:I

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Ledx;->h:Z

    .line 171
    iget-object v0, p0, Ledx;->c:Leeb;

    invoke-virtual {v0, v2}, Leeb;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 172
    iget-object v0, p0, Ledx;->e:Leed;

    invoke-virtual {v0, v2}, Leed;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 173
    iget-object v0, p0, Ledx;->m:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 174
    iput-object v2, p0, Ledx;->m:Landroid/os/Handler;

    .line 175
    iget-object v0, p0, Ledx;->l:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 176
    iput-object v2, p0, Ledx;->l:Landroid/os/HandlerThread;

    .line 177
    iput-object v2, p0, Ledx;->q:[B

    .line 178
    iput-object v2, p0, Ledx;->n:Landroid/media/MediaCrypto;

    .line 179
    iput-object v2, p0, Ledx;->o:Ljava/lang/Exception;

    .line 180
    iget-object v0, p0, Ledx;->j:[B

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Ledx;->b:Landroid/media/MediaDrm;

    iget-object v1, p0, Ledx;->j:[B

    invoke-virtual {v0, v1}, Landroid/media/MediaDrm;->closeSession([B)V

    .line 182
    iput-object v2, p0, Ledx;->j:[B

    goto :goto_0
.end method

.method a(Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 267
    instance-of v0, p1, Landroid/media/NotProvisionedException;

    if-eqz v0, :cond_0

    .line 268
    invoke-virtual {p0}, Ledx;->e()V

    .line 272
    :goto_0
    return-void

    .line 270
    :cond_0
    invoke-virtual {p0, p1}, Ledx;->b(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 144
    iget v0, p0, Ledx;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ledx;->g:I

    if-eq v0, v2, :cond_0

    .line 162
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Ledx;->m:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 148
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DrmRequestHandler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ledx;->l:Landroid/os/HandlerThread;

    .line 149
    iget-object v0, p0, Ledx;->l:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 150
    new-instance v0, Leec;

    iget-object v1, p0, Ledx;->l:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Leec;-><init>(Ledx;Landroid/os/Looper;)V

    iput-object v0, p0, Ledx;->m:Landroid/os/Handler;

    .line 152
    :cond_1
    iget-object v0, p0, Ledx;->q:[B

    if-nez v0, :cond_2

    .line 153
    iput-object p2, p0, Ledx;->p:Ljava/lang/String;

    .line 154
    iget-object v0, p0, Ledx;->f:Ljava/util/UUID;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Ledx;->q:[B

    .line 155
    iget-object v0, p0, Ledx;->q:[B

    if-nez v0, :cond_2

    .line 156
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Ledx;->f:Ljava/util/UUID;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Media does not support uuid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ledx;->b(Ljava/lang/Exception;)V

    goto :goto_0

    .line 160
    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Ledx;->i:I

    .line 161
    invoke-virtual {p0, v2}, Ledx;->a(Z)V

    goto :goto_0
.end method

.method a(Z)V
    .locals 3

    .prologue
    .line 188
    :try_start_0
    iget-object v0, p0, Ledx;->b:Landroid/media/MediaDrm;

    invoke-virtual {v0}, Landroid/media/MediaDrm;->openSession()[B

    move-result-object v0

    iput-object v0, p0, Ledx;->j:[B

    .line 189
    new-instance v0, Landroid/media/MediaCrypto;

    iget-object v1, p0, Ledx;->f:Ljava/util/UUID;

    iget-object v2, p0, Ledx;->j:[B

    invoke-direct {v0, v1, v2}, Landroid/media/MediaCrypto;-><init>(Ljava/util/UUID;[B)V

    iput-object v0, p0, Ledx;->n:Landroid/media/MediaCrypto;

    .line 190
    const/4 v0, 0x3

    iput v0, p0, Ledx;->i:I

    .line 191
    invoke-virtual {p0}, Ledx;->f()V
    :try_end_0
    .catch Landroid/media/NotProvisionedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 201
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 193
    if-eqz p1, :cond_0

    .line 194
    invoke-virtual {p0}, Ledx;->e()V

    goto :goto_0

    .line 196
    :cond_0
    invoke-virtual {p0, v0}, Ledx;->b(Ljava/lang/Exception;)V

    goto :goto_0

    .line 198
    :catch_1
    move-exception v0

    .line 199
    invoke-virtual {p0, v0}, Ledx;->b(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 107
    iget v0, p0, Ledx;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Ledx;->i:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Ledx;->n:Landroid/media/MediaCrypto;

    invoke-virtual {v0, p1}, Landroid/media/MediaCrypto;->requiresSecureDecoderComponent(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Ledx;->i:I

    return v0
.end method

.method b(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 275
    iput-object p1, p0, Ledx;->o:Ljava/lang/Exception;

    .line 276
    iget-object v0, p0, Ledx;->k:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ledx;->a:Ledz;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Ledx;->k:Landroid/os/Handler;

    new-instance v1, Ledy;

    invoke-direct {v1, p0, p1}, Ledy;-><init>(Ledx;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 284
    :cond_0
    iget v0, p0, Ledx;->i:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 285
    const/4 v0, 0x0

    iput v0, p0, Ledx;->i:I

    .line 287
    :cond_1
    return-void
.end method

.method public final c()Landroid/media/MediaCrypto;
    .locals 2

    .prologue
    .line 99
    iget v0, p0, Ledx;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Ledx;->i:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 102
    :cond_0
    iget-object v0, p0, Ledx;->n:Landroid/media/MediaCrypto;

    return-object v0
.end method

.method public final d()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Ledx;->i:I

    if-nez v0, :cond_0

    iget-object v0, p0, Ledx;->o:Ljava/lang/Exception;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()V
    .locals 3

    .prologue
    .line 204
    iget-boolean v0, p0, Ledx;->h:Z

    if-eqz v0, :cond_0

    .line 210
    :goto_0
    return-void

    .line 207
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ledx;->h:Z

    .line 208
    iget-object v0, p0, Ledx;->b:Landroid/media/MediaDrm;

    invoke-virtual {v0}, Landroid/media/MediaDrm;->getProvisionRequest()Landroid/media/MediaDrm$ProvisionRequest;

    move-result-object v0

    .line 209
    iget-object v1, p0, Ledx;->m:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method f()V
    .locals 6

    .prologue
    .line 239
    :try_start_0
    iget-object v0, p0, Ledx;->b:Landroid/media/MediaDrm;

    iget-object v1, p0, Ledx;->j:[B

    iget-object v2, p0, Ledx;->q:[B

    iget-object v3, p0, Ledx;->p:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaDrm;->getKeyRequest([B[BLjava/lang/String;ILjava/util/HashMap;)Landroid/media/MediaDrm$KeyRequest;

    move-result-object v0

    .line 241
    iget-object v1, p0, Ledx;->m:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catch Landroid/media/NotProvisionedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    return-void

    .line 242
    :catch_0
    move-exception v0

    .line 243
    invoke-virtual {p0, v0}, Ledx;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method
