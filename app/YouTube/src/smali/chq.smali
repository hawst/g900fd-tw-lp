.class public final Lchq;
.super Lfsa;
.source "SourceFile"


# instance fields
.field final a:Lfhz;

.field private final b:Landroid/content/Context;

.field private final c:Lfsj;

.field private final d:Landroid/content/res/Resources;

.field private final e:Landroid/view/ViewGroup;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/view/View$OnClickListener;

.field private final h:Landroid/view/ViewGroup;

.field private final i:Landroid/view/ViewGroup;

.field private j:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p4, p5}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 55
    iput-object p1, p0, Lchq;->b:Landroid/content/Context;

    .line 56
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lchq;->c:Lfsj;

    .line 57
    iput-object p3, p0, Lchq;->a:Lfhz;

    .line 59
    const v0, 0x7f0400ea

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lchq;->e:Landroid/view/ViewGroup;

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lchq;->d:Landroid/content/res/Resources;

    .line 61
    iget-object v0, p0, Lchq;->e:Landroid/view/ViewGroup;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lchq;->f:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lchq;->e:Landroid/view/ViewGroup;

    const v1, 0x7f0802b2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lchq;->h:Landroid/view/ViewGroup;

    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lchq;->a(I)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lchq;->i:Landroid/view/ViewGroup;

    .line 65
    new-instance v0, Lchr;

    invoke-direct {v0, p0}, Lchr;-><init>(Lchq;)V

    iput-object v0, p0, Lchq;->g:Landroid/view/View$OnClickListener;

    .line 76
    iget-object v0, p0, Lchq;->e:Landroid/view/ViewGroup;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 77
    return-void
.end method

.method private a(Lfsg;Lfmf;)Landroid/view/View;
    .locals 8

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lfsa;->a(Lfsg;Lfqh;)Landroid/view/View;

    .line 83
    iget-object v0, p0, Lchq;->d:Landroid/content/res/Resources;

    const v1, 0x7f0e0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 84
    invoke-virtual {p2}, Lfmf;->a()Ljava/util/List;

    move-result-object v0

    .line 85
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 87
    iget-object v3, p0, Lchq;->f:Landroid/widget/TextView;

    iget-object v4, p2, Lfmf;->b:Landroid/text/Spanned;

    if-nez v4, :cond_0

    iget-object v4, p2, Lfmf;->a:Lhti;

    iget-object v4, v4, Lhti;->a:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p2, Lfmf;->b:Landroid/text/Spanned;

    :cond_0
    iget-object v4, p2, Lfmf;->b:Landroid/text/Spanned;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v1, :cond_3

    .line 90
    :goto_0
    iget-object v3, p0, Lchq;->i:Landroid/view/ViewGroup;

    invoke-direct {p0, v3, v2, v0}, Lchq;->a(Landroid/view/ViewGroup;Ljava/util/Iterator;I)V

    .line 92
    if-eqz v1, :cond_4

    .line 93
    iget-object v1, p0, Lchq;->j:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 94
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lchq;->a(I)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lchq;->j:Landroid/view/ViewGroup;

    .line 96
    :cond_1
    iget-object v1, p0, Lchq;->j:Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v2, v0}, Lchq;->a(Landroid/view/ViewGroup;Ljava/util/Iterator;I)V

    .line 97
    iget-object v0, p0, Lchq;->j:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 102
    :cond_2
    :goto_1
    iget-object v0, p0, Lchq;->c:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 88
    :cond_3
    int-to-double v4, v0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    goto :goto_0

    .line 98
    :cond_4
    iget-object v0, p0, Lchq;->j:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lchq;->j:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private a(I)Landroid/view/ViewGroup;
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lchq;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 117
    iget-object v0, p0, Lchq;->b:Landroid/content/Context;

    const v1, 0x7f0400e9

    iget-object v2, p0, Lchq;->h:Landroid/view/ViewGroup;

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 119
    :cond_0
    iget-object v0, p0, Lchq;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;Ljava/util/Iterator;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 126
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    move v4, v0

    .line 128
    :goto_0
    if-ge v4, p3, :cond_0

    .line 129
    iget-object v0, p0, Lchq;->b:Landroid/content/Context;

    const v1, 0x7f0400eb

    invoke-static {v0, v1, p1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 130
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_0
    move v2, v3

    .line 134
    :goto_1
    if-ge v2, v4, :cond_3

    .line 135
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 137
    if-ge v2, p3, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 138
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfme;

    .line 139
    iget-object v5, v1, Lfme;->a:Landroid/text/Spanned;

    if-nez v5, :cond_1

    iget-object v5, v1, Lfme;->b:Lhth;

    iget-object v5, v5, Lhth;->a:Lhgz;

    invoke-static {v5}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v5

    iput-object v5, v1, Lfme;->a:Landroid/text/Spanned;

    :cond_1
    iget-object v5, v1, Lfme;->a:Landroid/text/Spanned;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    const v5, 0x7f08001d

    iget-object v1, v1, Lfme;->b:Lhth;

    iget-object v1, v1, Lhth;->b:Lhog;

    invoke-virtual {v0, v5, v1}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 141
    iget-object v1, p0, Lchq;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 144
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 147
    :cond_3
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    check-cast p2, Lfmf;

    invoke-direct {p0, p1, p2}, Lchq;->a(Lfsg;Lfmf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 35
    check-cast p2, Lfmf;

    invoke-direct {p0, p1, p2}, Lchq;->a(Lfsg;Lfmf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
