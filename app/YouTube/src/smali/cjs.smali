.class public final Lcjs;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Lfvt;

.field final c:Lfxe;

.field final d:Lgix;

.field final e:Leyt;

.field final f:Lffg;

.field final g:Lfhz;

.field public final h:Lfsi;

.field final i:Lccp;

.field final j:Lcck;

.field final k:Lckd;

.field l:Lfwc;

.field private final m:Lcuo;

.field private final n:Lcub;

.field private final o:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcyc;Landroid/widget/ListView;Lfvt;Leyp;Lfxe;Lcuo;Lgix;Lcub;Leyt;Lffg;Lfhz;)V
    .locals 18

    .prologue
    .line 143
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 144
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->a:Landroid/app/Activity;

    .line 145
    invoke-static/range {p3 .. p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    invoke-static/range {p4 .. p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfvt;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->b:Lfvt;

    .line 147
    invoke-static/range {p6 .. p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfxe;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->c:Lfxe;

    .line 148
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcuo;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->m:Lcuo;

    .line 149
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lgix;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->d:Lgix;

    .line 150
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcub;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->n:Lcub;

    .line 151
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Leyt;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->e:Leyt;

    .line 152
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lffg;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->f:Lffg;

    .line 153
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lfhz;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->g:Lfhz;

    .line 154
    invoke-static/range {p3 .. p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    invoke-static/range {p5 .. p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    new-instance v3, Lccp;

    invoke-direct {v3}, Lccp;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->i:Lccp;

    .line 158
    new-instance v3, Lcck;

    invoke-direct {v3}, Lcck;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->j:Lcck;

    .line 159
    new-instance v3, Lckd;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lckd;-><init>(Lcjs;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->k:Lckd;

    .line 163
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0x19

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->o:Ljava/util/List;

    .line 165
    new-instance v7, Lcjx;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcjx;-><init>(Lcjs;)V

    .line 166
    new-instance v9, Lcka;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcka;-><init>(Lcjs;)V

    .line 167
    new-instance v10, Lckg;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lckg;-><init>(Lcjs;)V

    .line 169
    new-instance v3, Lfsi;

    invoke-direct {v3}, Lfsi;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcjs;->h:Lfsi;

    .line 170
    move-object/from16 v0, p0

    iget-object v11, v0, Lcjs;->h:Lfsi;

    const-class v12, Lfwf;

    new-instance v3, Lcic;

    new-instance v8, Lckq;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lckq;-><init>(Lcjs;)V

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p5

    invoke-direct/range {v3 .. v10}, Lcic;-><init>(Landroid/app/Activity;Lcyc;Leyp;Lcbk;Lcid;Lcbl;Lckf;)V

    invoke-virtual {v11, v12, v3}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 180
    move-object/from16 v0, p0

    iget-object v3, v0, Lcjs;->h:Lfsi;

    const-class v4, Lfwd;

    new-instance v11, Lccr;

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p5

    move-object v15, v7

    move-object/from16 v16, v9

    move-object/from16 v17, v10

    invoke-direct/range {v11 .. v17}, Lccr;-><init>(Landroid/app/Activity;Lcyc;Leyp;Lcbk;Lcbl;Lckf;)V

    invoke-virtual {v3, v4, v11}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 189
    move-object/from16 v0, p0

    iget-object v3, v0, Lcjs;->h:Lfsi;

    const-class v4, Lccp;

    new-instance v5, Lcco;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcjs;->k:Lckd;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p5

    invoke-direct {v5, v0, v1, v2, v6}, Lcco;-><init>(Landroid/app/Activity;Lcyc;Leyp;Lckc;)V

    invoke-virtual {v3, v4, v5}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v3, v0, Lcjs;->h:Lfsi;

    const-class v4, Lcck;

    new-instance v5, Lccj;

    new-instance v6, Lcjy;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcjy;-><init>(Lcjs;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v5, v0, v1, v6}, Lccj;-><init>(Landroid/app/Activity;Lcyc;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v4, v5}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 199
    move-object/from16 v0, p0

    iget-object v3, v0, Lcjs;->h:Lfsi;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 200
    return-void
.end method

.method static synthetic a(Lfwe;)Lfwf;
    .locals 1

    .prologue
    .line 82
    instance-of v0, p0, Lfwd;

    if-eqz v0, :cond_0

    check-cast p0, Lfwd;

    iget-object p0, p0, Lfwd;->b:Lfwf;

    :goto_0
    return-object p0

    :cond_0
    check-cast p0, Lfwf;

    goto :goto_0
.end method

.method static synthetic a(Lcjs;)V
    .locals 0

    .prologue
    .line 82
    invoke-virtual {p0}, Lcjs;->b()V

    return-void
.end method

.method static synthetic a(Lcjs;I)V
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcjs;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Leze;->a(Landroid/content/Context;II)V

    return-void
.end method

.method static synthetic a(Lcjs;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcjs;->i:Lccp;

    iget-object v0, v0, Lccp;->b:Lgca;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcjs;->n:Lcub;

    iget-object v1, p0, Lcjs;->a:Landroid/app/Activity;

    new-instance v2, Lcju;

    invoke-direct {v2, p0, p1}, Lcju;-><init>(Lcjs;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v0}, Lcjs;->a(Lgca;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcjs;->d:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    iget-object v0, p0, Lcjs;->i:Lccp;

    const/4 v1, 0x0

    iput-object v1, v0, Lccp;->b:Lgca;

    .line 233
    iget-object v0, p0, Lcjs;->h:Lfsi;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    .line 251
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcjs;->c:Lfxe;

    iget-object v1, p0, Lcjs;->a:Landroid/app/Activity;

    new-instance v2, Lcjt;

    invoke-direct {v2, p0}, Lcjt;-><init>(Lcjs;)V

    .line 238
    invoke-static {v1, v2}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v1

    .line 237
    invoke-interface {v0, v1}, Lfxe;->a(Leuc;)V

    goto :goto_0
.end method

.method public final a(Lfwc;)V
    .locals 2

    .prologue
    .line 265
    iput-object p1, p0, Lcjs;->l:Lfwc;

    .line 266
    iget-object v1, p0, Lcjs;->i:Lccp;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, v1, Lccp;->a:Z

    .line 267
    invoke-virtual {p0}, Lcjs;->b()V

    .line 268
    return-void

    .line 266
    :cond_0
    iget-boolean v0, p1, Lfwc;->d:Z

    goto :goto_0
.end method

.method a(Lgca;)Z
    .locals 4

    .prologue
    .line 357
    iget-object v0, p0, Lcjs;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcjs;->m:Lcuo;

    iget-object v2, p0, Lcjs;->d:Lgix;

    iget-object v3, p0, Lcjs;->c:Lfxe;

    invoke-static {v0, v1, v2, p1, v3}, Lbra;->a(Landroid/app/Activity;Lcuo;Lgix;Lgca;Lfxe;)Landroid/app/Dialog;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 361
    const/4 v0, 0x1

    .line 363
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 4

    .prologue
    .line 275
    iget-object v0, p0, Lcjs;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 277
    iget-object v0, p0, Lcjs;->l:Lfwc;

    if-eqz v0, :cond_4

    .line 278
    iget-object v0, p0, Lcjs;->l:Lfwc;

    invoke-virtual {v0}, Lfwc;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 279
    iget-object v0, p0, Lcjs;->j:Lcck;

    const/4 v1, 0x3

    iput v1, v0, Lcck;->a:I

    .line 286
    :goto_0
    iget-object v0, p0, Lcjs;->o:Ljava/util/List;

    iget-object v1, p0, Lcjs;->i:Lccp;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    iget-object v0, p0, Lcjs;->l:Lfwc;

    invoke-virtual {v0}, Lfwc;->a()I

    move-result v0

    if-lez v0, :cond_3

    .line 289
    iget-object v0, p0, Lcjs;->l:Lfwc;

    iget-object v0, v0, Lfwc;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwf;

    .line 290
    iget-object v2, p0, Lcjs;->o:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    iget-object v0, v0, Lfwf;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfwd;

    .line 292
    iget-object v3, p0, Lcjs;->o:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 280
    :cond_1
    iget-object v0, p0, Lcjs;->l:Lfwc;

    invoke-virtual {v0}, Lfwc;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 281
    iget-object v0, p0, Lcjs;->j:Lcck;

    const/4 v1, 0x2

    iput v1, v0, Lcck;->a:I

    goto :goto_0

    .line 283
    :cond_2
    iget-object v0, p0, Lcjs;->j:Lcck;

    const/4 v1, 0x1

    iput v1, v0, Lcck;->a:I

    goto :goto_0

    .line 296
    :cond_3
    iget-object v0, p0, Lcjs;->o:Ljava/util/List;

    iget-object v1, p0, Lcjs;->j:Lcck;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_4
    iget-object v0, p0, Lcjs;->h:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 300
    iget-object v0, p0, Lcjs;->h:Lfsi;

    iget-object v1, p0, Lcjs;->o:Ljava/util/List;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/util/Collection;)V

    .line 301
    return-void
.end method

.method public final handleSignInEvent(Lfcb;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 212
    invoke-virtual {p0}, Lcjs;->a()V

    .line 213
    return-void
.end method

.method public final handleSignOutEvent(Lfcc;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 223
    iget-object v0, p0, Lcjs;->i:Lccp;

    const/4 v1, 0x0

    iput-object v1, v0, Lccp;->b:Lgca;

    .line 224
    iget-object v0, p0, Lcjs;->h:Lfsi;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    .line 225
    return-void
.end method
