.class public final enum Lgol;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgol;

.field public static final enum b:Lgol;

.field public static final enum c:Lgol;

.field public static final enum d:Lgol;

.field public static final enum e:Lgol;

.field public static final enum f:Lgol;

.field public static final enum g:Lgol;

.field public static final enum h:Lgol;

.field public static final enum i:Lgol;

.field public static final enum j:Lgol;

.field private static final synthetic k:[Lgol;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lgol;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v3}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->a:Lgol;

    .line 20
    new-instance v0, Lgol;

    const-string v1, "PLAYBACK_LOADED"

    invoke-direct {v0, v1, v4}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->b:Lgol;

    .line 25
    new-instance v0, Lgol;

    const-string v1, "AD_LOADING"

    invoke-direct {v0, v1, v5}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->c:Lgol;

    .line 30
    new-instance v0, Lgol;

    const-string v1, "AD_LOADED"

    invoke-direct {v0, v1, v6}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->d:Lgol;

    .line 35
    new-instance v0, Lgol;

    const-string v1, "MEDIA_AD_PLAY_REQUESTED"

    invoke-direct {v0, v1, v7}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->e:Lgol;

    .line 40
    new-instance v0, Lgol;

    const-string v1, "MEDIA_PLAYING_AD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->f:Lgol;

    .line 45
    new-instance v0, Lgol;

    const-string v1, "READY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->g:Lgol;

    .line 50
    new-instance v0, Lgol;

    const-string v1, "MEDIA_VIDEO_PLAY_REQUESTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->h:Lgol;

    .line 55
    new-instance v0, Lgol;

    const-string v1, "MEDIA_PLAYING_VIDEO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->i:Lgol;

    .line 60
    new-instance v0, Lgol;

    const-string v1, "ENDED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lgol;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgol;->j:Lgol;

    .line 8
    const/16 v0, 0xa

    new-array v0, v0, [Lgol;

    sget-object v1, Lgol;->a:Lgol;

    aput-object v1, v0, v3

    sget-object v1, Lgol;->b:Lgol;

    aput-object v1, v0, v4

    sget-object v1, Lgol;->c:Lgol;

    aput-object v1, v0, v5

    sget-object v1, Lgol;->d:Lgol;

    aput-object v1, v0, v6

    sget-object v1, Lgol;->e:Lgol;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgol;->f:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgol;->g:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgol;->h:Lgol;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lgol;->i:Lgol;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lgol;->j:Lgol;

    aput-object v2, v0, v1

    sput-object v0, Lgol;->k:[Lgol;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgol;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lgol;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgol;

    return-object v0
.end method

.method public static values()[Lgol;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lgol;->k:[Lgol;

    invoke-virtual {v0}, [Lgol;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgol;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 85
    const/4 v0, 0x3

    new-array v0, v0, [Lgol;

    const/4 v1, 0x0

    sget-object v2, Lgol;->d:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lgol;->e:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lgol;->f:Lgol;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgol;->a([Lgol;)Z

    move-result v0

    return v0
.end method

.method public final a(Lgol;)Z
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lgol;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lgol;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs a([Lgol;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 73
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 74
    if-ne p0, v3, :cond_1

    .line 75
    const/4 v0, 0x1

    .line 78
    :cond_0
    return v0

    .line 73
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x4

    new-array v0, v0, [Lgol;

    const/4 v1, 0x0

    sget-object v2, Lgol;->e:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lgol;->f:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lgol;->h:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lgol;->i:Lgol;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgol;->a([Lgol;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 113
    const/4 v0, 0x2

    new-array v0, v0, [Lgol;

    const/4 v1, 0x0

    sget-object v2, Lgol;->e:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lgol;->f:Lgol;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgol;->a([Lgol;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 120
    const/4 v0, 0x2

    new-array v0, v0, [Lgol;

    const/4 v1, 0x0

    sget-object v2, Lgol;->h:Lgol;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lgol;->i:Lgol;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lgol;->a([Lgol;)Z

    move-result v0

    return v0
.end method
