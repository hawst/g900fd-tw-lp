.class public final Lead;
.super Lida;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:J

.field public e:J

.field public f:Ljava/util/List;

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 530
    invoke-direct {p0}, Lida;-><init>()V

    .line 535
    const-string v0, ""

    iput-object v0, p0, Lead;->b:Ljava/lang/String;

    .line 552
    iput-wide v2, p0, Lead;->d:J

    .line 569
    iput-wide v2, p0, Lead;->e:J

    .line 586
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lead;->f:Ljava/util/List;

    .line 645
    const/4 v0, -0x1

    iput v0, p0, Lead;->h:I

    .line 530
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 647
    iget v0, p0, Lead;->h:I

    if-gez v0, :cond_0

    .line 649
    invoke-virtual {p0}, Lead;->b()I

    .line 651
    :cond_0
    iget v0, p0, Lead;->h:I

    return v0
.end method

.method public final a(J)Lead;
    .locals 1

    .prologue
    .line 556
    const/4 v0, 0x1

    iput-boolean v0, p0, Lead;->c:Z

    .line 557
    iput-wide p1, p0, Lead;->d:J

    .line 558
    return-object p0
.end method

.method public final a(Leac;)Lead;
    .locals 1

    .prologue
    .line 602
    if-nez p1, :cond_0

    .line 603
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 605
    :cond_0
    iget-object v0, p0, Lead;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 606
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lead;->f:Ljava/util/List;

    .line 608
    :cond_1
    iget-object v0, p0, Lead;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lead;
    .locals 1

    .prologue
    .line 539
    const/4 v0, 0x1

    iput-boolean v0, p0, Lead;->a:Z

    .line 540
    iput-object p1, p0, Lead;->b:Ljava/lang/String;

    .line 541
    return-object p0
.end method

.method public final synthetic a(Licx;)Lida;
    .locals 2

    .prologue
    .line 528
    :cond_0
    :goto_0
    invoke-virtual {p1}, Licx;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Licx;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Licx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lead;->a(Ljava/lang/String;)Lead;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Licx;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lead;->a(J)Lead;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Licx;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lead;->b(J)Lead;

    goto :goto_0

    :sswitch_4
    new-instance v0, Leac;

    invoke-direct {v0}, Leac;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Lead;->a(Leac;)Lead;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x3a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Licy;)V
    .locals 4

    .prologue
    .line 631
    iget-boolean v0, p0, Lead;->a:Z

    if-eqz v0, :cond_0

    .line 632
    const/4 v0, 0x2

    iget-object v1, p0, Lead;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILjava/lang/String;)V

    .line 634
    :cond_0
    iget-boolean v0, p0, Lead;->c:Z

    if-eqz v0, :cond_1

    .line 635
    const/4 v0, 0x3

    iget-wide v2, p0, Lead;->d:J

    invoke-virtual {p1, v0, v2, v3}, Licy;->a(IJ)V

    .line 637
    :cond_1
    iget-boolean v0, p0, Lead;->g:Z

    if-eqz v0, :cond_2

    .line 638
    const/4 v0, 0x4

    iget-wide v2, p0, Lead;->e:J

    invoke-virtual {p1, v0, v2, v3}, Licy;->a(IJ)V

    .line 640
    :cond_2
    iget-object v0, p0, Lead;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leac;

    .line 641
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Licy;->a(ILida;)V

    goto :goto_0

    .line 643
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 655
    const/4 v0, 0x0

    .line 656
    iget-boolean v1, p0, Lead;->a:Z

    if-eqz v1, :cond_0

    .line 657
    const/4 v0, 0x2

    .line 658
    iget-object v1, p0, Lead;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Licy;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 660
    :cond_0
    iget-boolean v1, p0, Lead;->c:Z

    if-eqz v1, :cond_1

    .line 661
    const/4 v1, 0x3

    .line 662
    iget-wide v2, p0, Lead;->d:J

    invoke-static {v1, v2, v3}, Licy;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 664
    :cond_1
    iget-boolean v1, p0, Lead;->g:Z

    if-eqz v1, :cond_2

    .line 665
    const/4 v1, 0x4

    .line 666
    iget-wide v2, p0, Lead;->e:J

    invoke-static {v1, v2, v3}, Licy;->b(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 668
    :cond_2
    iget-object v1, p0, Lead;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leac;

    .line 669
    const/4 v3, 0x7

    .line 670
    invoke-static {v3, v0}, Licy;->b(ILida;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 671
    goto :goto_0

    .line 672
    :cond_3
    iput v1, p0, Lead;->h:I

    .line 673
    return v1
.end method

.method public final b(J)Lead;
    .locals 1

    .prologue
    .line 573
    const/4 v0, 0x1

    iput-boolean v0, p0, Lead;->g:Z

    .line 574
    iput-wide p1, p0, Lead;->e:J

    .line 575
    return-object p0
.end method
