.class public Lld;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljm;


# instance fields
.field final a:Landroid/support/v4/widget/DrawerLayout;

.field b:Z

.field private final c:Lle;

.field private d:Llh;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Z

.field private final g:I

.field private final h:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 150
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lld;-><init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;Landroid/graphics/drawable/Drawable;II)V

    .line 152
    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Landroid/support/v7/widget/Toolbar;Landroid/support/v4/widget/DrawerLayout;Landroid/graphics/drawable/Drawable;II)V
    .locals 2

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/4 v0, 0x1

    iput-boolean v0, p0, Lld;->b:Z

    .line 194
    instance-of v0, p1, Llf;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 207
    check-cast v0, Llf;

    invoke-interface {v0}, Llf;->a()Lle;

    move-result-object v0

    iput-object v0, p0, Lld;->c:Lle;

    .line 218
    :goto_0
    iput-object p3, p0, Lld;->a:Landroid/support/v4/widget/DrawerLayout;

    .line 219
    iput p5, p0, Lld;->g:I

    .line 220
    iput p6, p0, Lld;->h:I

    .line 221
    new-instance v0, Llg;

    iget-object v1, p0, Lld;->c:Lle;

    invoke-interface {v1}, Lle;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Llg;-><init>(Landroid/app/Activity;Landroid/content/Context;)V

    iput-object v0, p0, Lld;->d:Llh;

    .line 225
    invoke-direct {p0}, Lld;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lld;->e:Landroid/graphics/drawable/Drawable;

    .line 229
    return-void

    .line 208
    :cond_0
    instance-of v0, p1, Lll;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 209
    check-cast v0, Lll;

    invoke-interface {v0}, Lll;->e()Lle;

    move-result-object v0

    iput-object v0, p0, Lld;->c:Lle;

    goto :goto_0

    .line 210
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_2

    .line 211
    new-instance v0, Llk;

    invoke-direct {v0, p1}, Llk;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lld;->c:Lle;

    goto :goto_0

    .line 212
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_3

    .line 213
    new-instance v0, Llj;

    invoke-direct {v0, p1}, Llj;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lld;->c:Lle;

    goto :goto_0

    .line 215
    :cond_3
    new-instance v0, Lli;

    invoke-direct {v0, p1}, Lli;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lld;->c:Lle;

    goto :goto_0
.end method

.method private a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lld;->c:Lle;

    invoke-interface {v0, p1, p2}, Lle;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 453
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lld;->c:Lle;

    invoke-interface {v0, p1}, Lle;->a(I)V

    .line 457
    return-void
.end method

.method private c()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lld;->c:Lle;

    invoke-interface {v0}, Lle;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const v2, 0x800003

    .line 241
    iget-object v0, p0, Lld;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayout;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 242
    iget-object v0, p0, Lld;->d:Llh;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Llh;->a(F)V

    .line 246
    :goto_0
    iget-boolean v0, p0, Lld;->b:Z

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lld;->d:Llh;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lld;->a:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayout;->d(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lld;->h:I

    :goto_1
    invoke-direct {p0, v0, v1}, Lld;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 251
    :cond_0
    return-void

    .line 244
    :cond_1
    iget-object v0, p0, Lld;->d:Llh;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Llh;->a(F)V

    goto :goto_0

    .line 247
    :cond_2
    iget v1, p0, Lld;->g:I

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 422
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 306
    invoke-direct {p0}, Lld;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lld;->e:Landroid/graphics/drawable/Drawable;

    .line 308
    iput-boolean v1, p0, Lld;->f:Z

    .line 310
    iget-boolean v0, p0, Lld;->b:Z

    if-nez v0, :cond_0

    .line 315
    iget-object v0, p0, Lld;->e:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0, v1}, Lld;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 317
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lld;->d:Llh;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, Llh;->a(F)V

    .line 393
    iget-boolean v0, p0, Lld;->b:Z

    if-eqz v0, :cond_0

    .line 394
    iget v0, p0, Lld;->h:I

    invoke-direct {p0, v0}, Lld;->b(I)V

    .line 396
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;F)V
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, Lld;->d:Llh;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-static {v2, p2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-interface {v0, v1}, Llh;->a(F)V

    .line 381
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 357
    iget-boolean v0, p0, Lld;->b:Z

    if-eq p1, v0, :cond_0

    .line 358
    if-eqz p1, :cond_2

    .line 359
    iget-object v0, p0, Lld;->d:Llh;

    check-cast v0, Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lld;->a:Landroid/support/v4/widget/DrawerLayout;

    const v2, 0x800003

    invoke-virtual {v1, v2}, Landroid/support/v4/widget/DrawerLayout;->d(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lld;->h:I

    :goto_0
    invoke-direct {p0, v0, v1}, Lld;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 365
    :goto_1
    iput-boolean p1, p0, Lld;->b:Z

    .line 367
    :cond_0
    return-void

    .line 359
    :cond_1
    iget v1, p0, Lld;->g:I

    goto :goto_0

    .line 363
    :cond_2
    iget-object v0, p0, Lld;->e:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lld;->a(Landroid/graphics/drawable/Drawable;I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lld;->f:Z

    if-nez v0, :cond_0

    .line 264
    invoke-direct {p0}, Lld;->c()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lld;->e:Landroid/graphics/drawable/Drawable;

    .line 266
    :cond_0
    invoke-virtual {p0}, Lld;->a()V

    .line 267
    return-void
.end method

.method public b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lld;->d:Llh;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Llh;->a(F)V

    .line 408
    iget-boolean v0, p0, Lld;->b:Z

    if-eqz v0, :cond_0

    .line 409
    iget v0, p0, Lld;->g:I

    invoke-direct {p0, v0}, Lld;->b(I)V

    .line 411
    :cond_0
    return-void
.end method
