.class public Landroid/support/v7/widget/ShareActionProvider;
.super Lek;
.source "SourceFile"


# instance fields
.field private c:I

.field private final d:Lvq;

.field private final e:Landroid/content/Context;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lek;-><init>(Landroid/content/Context;)V

    .line 124
    const/4 v0, 0x4

    iput v0, p0, Landroid/support/v7/widget/ShareActionProvider;->c:I

    .line 129
    new-instance v0, Lvq;

    invoke-direct {v0, p0}, Lvq;-><init>(Landroid/support/v7/widget/ShareActionProvider;)V

    iput-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->d:Lvq;

    .line 145
    const-string v0, "share_history.xml"

    iput-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->f:Ljava/lang/String;

    .line 158
    iput-object p1, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    .line 159
    return-void
.end method

.method public static synthetic a(Landroid/support/v7/widget/ShareActionProvider;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    return-object v0
.end method

.method public static synthetic b(Landroid/support/v7/widget/ShareActionProvider;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 5

    .prologue
    .line 182
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    iget-object v1, p0, Landroid/support/v7/widget/ShareActionProvider;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lpc;->a(Landroid/content/Context;Ljava/lang/String;)Lpc;

    move-result-object v0

    .line 183
    new-instance v1, Lpi;

    iget-object v2, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Lpi;-><init>(Landroid/content/Context;)V

    .line 184
    iget-object v2, v1, Lpi;->a:Lpm;

    iget-object v3, v2, Lpm;->c:Lpi;

    invoke-static {v3}, Lpi;->a(Lpi;)Lpm;

    move-result-object v3

    iget-object v3, v3, Lpm;->a:Lpc;

    if-eqz v3, :cond_0

    iget-object v4, v2, Lpm;->c:Lpi;

    invoke-virtual {v4}, Lpi;->isShown()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v2, Lpm;->c:Lpi;

    invoke-static {v4}, Lpi;->i(Lpi;)Landroid/database/DataSetObserver;

    move-result-object v4

    invoke-virtual {v3, v4}, Lpc;->unregisterObserver(Ljava/lang/Object;)V

    :cond_0
    iput-object v0, v2, Lpm;->a:Lpc;

    if-eqz v0, :cond_1

    iget-object v3, v2, Lpm;->c:Lpi;

    invoke-virtual {v3}, Lpi;->isShown()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v2, Lpm;->c:Lpi;

    invoke-static {v3}, Lpi;->i(Lpi;)Landroid/database/DataSetObserver;

    move-result-object v3

    invoke-virtual {v0, v3}, Lpc;->registerObserver(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {v2}, Lpm;->notifyDataSetChanged()V

    invoke-virtual {v1}, Lpi;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lpi;->a()Z

    invoke-virtual {v1}, Lpi;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, v1, Lpi;->f:Z

    if-nez v0, :cond_3

    .line 187
    :cond_2
    :goto_0
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 188
    iget-object v2, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f010065

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 189
    iget-object v2, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v2, v0}, Lqw;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 190
    iget-object v2, v1, Lpi;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 191
    iput-object p0, v1, Lpi;->c:Lek;

    .line 194
    const v0, 0x7f090088

    iput v0, v1, Lpi;->g:I

    .line 196
    const v0, 0x7f090089

    invoke-virtual {v1}, Lpi;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lpi;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 199
    return-object v1

    .line 184
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, v1, Lpi;->d:Z

    iget v0, v1, Lpi;->e:I

    invoke-virtual {v1, v0}, Lpi;->a(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/SubMenu;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 216
    invoke-interface {p1}, Landroid/view/SubMenu;->clear()V

    .line 218
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/widget/ShareActionProvider;->f:Ljava/lang/String;

    invoke-static {v0, v2}, Lpc;->a(Landroid/content/Context;Ljava/lang/String;)Lpc;

    move-result-object v2

    .line 219
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 221
    invoke-virtual {v2}, Lpc;->a()I

    move-result v4

    .line 222
    iget v0, p0, Landroid/support/v7/widget/ShareActionProvider;->c:I

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v0, v1

    .line 225
    :goto_0
    if-ge v0, v5, :cond_0

    .line 226
    invoke-virtual {v2, v0}, Lpc;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 227
    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {p1, v1, v0, v0, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v7, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/widget/ShareActionProvider;->d:Lvq;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    :cond_0
    if-ge v5, v4, :cond_1

    .line 234
    iget-object v0, p0, Landroid/support/v7/widget/ShareActionProvider;->e:Landroid/content/Context;

    const v6, 0x7f090087

    invoke-virtual {v0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v5, v5, v0}, Landroid/view/SubMenu;->addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;

    move-result-object v5

    move v0, v1

    .line 237
    :goto_1
    if-ge v0, v4, :cond_1

    .line 238
    invoke-virtual {v2, v0}, Lpc;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 239
    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v5, v1, v0, v0, v7}, Landroid/view/SubMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v7

    invoke-virtual {v6, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-interface {v7, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v6

    iget-object v7, p0, Landroid/support/v7/widget/ShareActionProvider;->d:Lvq;

    invoke-interface {v6, v7}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 244
    :cond_1
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 207
    const/4 v0, 0x1

    return v0
.end method
