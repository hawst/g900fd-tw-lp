.class public final Lhea;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhlc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35985
    invoke-direct {p0}, Lidf;-><init>()V

    .line 35988
    const/4 v0, 0x0

    iput-object v0, p0, Lhea;->a:Lhlc;

    .line 35985
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 36025
    const/4 v0, 0x0

    .line 36026
    iget-object v1, p0, Lhea;->a:Lhlc;

    if-eqz v1, :cond_0

    .line 36027
    const v0, 0x413fd1c

    iget-object v1, p0, Lhea;->a:Lhlc;

    .line 36028
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 36030
    :cond_0
    iget-object v1, p0, Lhea;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36031
    iput v0, p0, Lhea;->J:I

    .line 36032
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 35981
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhea;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhea;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhea;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhea;->a:Lhlc;

    if-nez v0, :cond_2

    new-instance v0, Lhlc;

    invoke-direct {v0}, Lhlc;-><init>()V

    iput-object v0, p0, Lhea;->a:Lhlc;

    :cond_2
    iget-object v0, p0, Lhea;->a:Lhlc;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x209fe8e2 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 36016
    iget-object v0, p0, Lhea;->a:Lhlc;

    if-eqz v0, :cond_0

    .line 36017
    const v0, 0x413fd1c

    iget-object v1, p0, Lhea;->a:Lhlc;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 36019
    :cond_0
    iget-object v0, p0, Lhea;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 36021
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 35999
    if-ne p1, p0, :cond_1

    .line 36003
    :cond_0
    :goto_0
    return v0

    .line 36000
    :cond_1
    instance-of v2, p1, Lhea;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 36001
    :cond_2
    check-cast p1, Lhea;

    .line 36002
    iget-object v2, p0, Lhea;->a:Lhlc;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhea;->a:Lhlc;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhea;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhea;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 36003
    goto :goto_0

    .line 36002
    :cond_4
    iget-object v2, p0, Lhea;->a:Lhlc;

    iget-object v3, p1, Lhea;->a:Lhlc;

    invoke-virtual {v2, v3}, Lhlc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhea;->I:Ljava/util/List;

    iget-object v3, p1, Lhea;->I:Ljava/util/List;

    .line 36003
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 36007
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 36009
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhea;->a:Lhlc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 36010
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhea;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 36011
    return v0

    .line 36009
    :cond_0
    iget-object v0, p0, Lhea;->a:Lhlc;

    invoke-virtual {v0}, Lhlc;->hashCode()I

    move-result v0

    goto :goto_0

    .line 36010
    :cond_1
    iget-object v1, p0, Lhea;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
