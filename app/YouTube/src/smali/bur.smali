.class public final Lbur;
.super Lcba;
.source "SourceFile"


# instance fields
.field private synthetic a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)V
    .locals 1

    .prologue
    .line 55
    iput-object p1, p0, Lbur;->a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-direct {p0}, Lcba;-><init>()V

    .line 56
    invoke-static {}, Lgcg;->values()[Lgcg;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbur;->b(Ljava/lang/Iterable;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 76
    if-nez p2, :cond_0

    .line 77
    iget-object v0, p0, Lbur;->a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 78
    new-instance v0, Lbus;

    invoke-direct {v0, p0, p2}, Lbus;-><init>(Lbur;Landroid/view/View;)V

    .line 79
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 83
    :goto_0
    invoke-virtual {p0, p1}, Lbur;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcg;

    invoke-virtual {v1, v0}, Lbus;->a(Lgcg;)V

    .line 84
    return-object p2

    .line 81
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbus;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 62
    if-nez p2, :cond_0

    .line 63
    iget-object v0, p0, Lbur;->a:Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;->a(Lcom/google/android/apps/youtube/app/ui/PrivacySpinner;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400bc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 64
    new-instance v0, Lbut;

    invoke-direct {v0, p0, p2}, Lbut;-><init>(Lbur;Landroid/view/View;)V

    .line 65
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    .line 69
    :goto_0
    invoke-virtual {p0, p1}, Lbur;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcg;

    invoke-virtual {v1, v0}, Lbut;->a(Lgcg;)V

    .line 70
    return-object p2

    .line 67
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbut;

    move-object v1, v0

    goto :goto_0
.end method
