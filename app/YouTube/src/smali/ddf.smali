.class public final Lddf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/util/Map;


# instance fields
.field public final a:Lddu;

.field public b:D

.field public c:Z

.field public d:Lddk;

.field private final f:Ljava/util/Map;

.field private final g:I

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/view/animation/Animation;

.field private final j:Landroid/view/animation/Animation;

.field private final k:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 163
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 164
    sget-object v1, Lddk;->a:Lddk;

    const v2, 0x7f020297

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    sget-object v1, Lddk;->b:Lddk;

    const v2, 0x7f02029b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    sget-object v1, Lddk;->c:Lddk;

    const v2, 0x7f020299

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    sget-object v1, Lddk;->d:Lddk;

    const v2, 0x7f02029a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    sget-object v1, Lddk;->e:Lddk;

    const v2, 0x7f020298

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    sget-object v1, Lddk;->f:Lddk;

    const v2, 0x7f020295

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lddf;->e:Ljava/util/Map;

    .line 171
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lddu;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 198
    iput-object p2, p0, Lddf;->a:Lddu;

    .line 199
    sget-object v1, Lddf;->e:Ljava/util/Map;

    invoke-static {p1, v1}, Lddf;->a(Landroid/content/Context;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, p0, Lddf;->f:Ljava/util/Map;

    .line 200
    iput-object p3, p0, Lddf;->h:Landroid/widget/ImageView;

    .line 201
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lddi;

    invoke-direct {v2, p0}, Lddi;-><init>(Lddf;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lddf;->k:Landroid/os/Handler;

    .line 202
    const v1, 0x7f0b000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lddf;->g:I

    .line 204
    const v0, 0x7f050011

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lddf;->i:Landroid/view/animation/Animation;

    .line 205
    const v0, 0x7f050012

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lddf;->j:Landroid/view/animation/Animation;

    .line 206
    return-void
.end method

.method static synthetic a(Lddf;)D
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lddf;->b:D

    return-wide v0
.end method

.method private static a(Landroid/content/Context;Ljava/util/Map;)Ljava/util/Map;
    .locals 8

    .prologue
    .line 340
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 341
    const v0, 0x7f020296

    invoke-static {p0, v0}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 342
    instance-of v0, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 344
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 346
    :cond_0
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 347
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {p0, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 348
    instance-of v2, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v2, :cond_1

    move-object v2, v3

    .line 350
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 353
    :cond_1
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    const/4 v7, 0x1

    aput-object v3, v6, v7

    invoke-direct {v2, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 355
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 357
    :cond_2
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lddf;->j:Landroid/view/animation/Animation;

    new-instance v1, Lddh;

    invoke-direct {v1, p0}, Lddh;-><init>(Lddf;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 286
    iget-object v0, p0, Lddf;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 287
    iget-object v0, p0, Lddf;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lddf;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 288
    return-void
.end method

.method static synthetic a(Lddf;Lddk;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lddf;->a()V

    return-void
.end method

.method static synthetic b(Lddf;)Lddu;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lddf;->a:Lddu;

    return-object v0
.end method

.method static synthetic b(Lddf;Lddk;)V
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p1}, Lddk;->a()Z

    move-result v0

    invoke-static {v0}, Lb;->b(Z)V

    invoke-virtual {p0, p1}, Lddf;->b(Lddk;)V

    invoke-virtual {p0, p1}, Lddf;->c(Lddk;)V

    return-void
.end method

.method static synthetic c(Lddf;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lddf;->h:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public final a(Lddk;Ldbu;)I
    .locals 4

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x1

    .line 136
    sget-object v2, Lddj;->a:[I

    invoke-virtual {p1}, Lddk;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 152
    :cond_0
    :goto_0
    return v0

    .line 139
    :pswitch_0
    iget-boolean v2, p2, Ldbu;->l:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 143
    :pswitch_1
    iget-boolean v2, p0, Lddf;->c:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p2, Ldbu;->j:Z

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 146
    :pswitch_2
    iget-boolean v2, p2, Ldbu;->j:Z

    if-nez v2, :cond_0

    .line 149
    iget-boolean v0, p2, Ldbu;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lddk;)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lddf;->d:Lddk;

    if-ne v0, p1, :cond_1

    .line 230
    invoke-virtual {p1}, Lddk;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    invoke-direct {p0}, Lddf;->a()V

    .line 232
    invoke-virtual {p1}, Lddk;->a()Z

    move-result v0

    invoke-static {v0}, Lb;->b(Z)V

    iget-object v0, p0, Lddf;->k:Landroid/os/Handler;

    const/16 v1, 0xea

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 234
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lddf;->d:Lddk;

    .line 236
    :cond_1
    return-void
.end method

.method public a(Lddk;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 243
    iget-object v0, p0, Lddf;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lddf;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 248
    iget-object v0, p0, Lddf;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lddf;->i:Landroid/view/animation/Animation;

    new-instance v1, Lddg;

    invoke-direct {v1, p0, p2, p1}, Lddg;-><init>(Lddf;ZLddk;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 266
    iget-object v0, p0, Lddf;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 267
    iget-object v1, p0, Lddf;->h:Landroid/widget/ImageView;

    iget-object v0, p0, Lddf;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 268
    iget-object v0, p0, Lddf;->h:Landroid/widget/ImageView;

    iget-object v1, p0, Lddf;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public b(Lddk;)V
    .locals 4

    .prologue
    .line 327
    invoke-virtual {p1}, Lddk;->a()Z

    move-result v0

    invoke-static {v0}, Lb;->b(Z)V

    .line 328
    invoke-virtual {p1, p0}, Lddk;->a(Lddf;)V

    .line 329
    iget-wide v0, p0, Lddf;->b:D

    const-wide v2, 0x3ff199999999999aL    # 1.1

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    iput-wide v0, p0, Lddf;->b:D

    .line 330
    return-void
.end method

.method public c(Lddk;)V
    .locals 4

    .prologue
    .line 333
    invoke-virtual {p1}, Lddk;->a()Z

    move-result v0

    invoke-static {v0}, Lb;->b(Z)V

    .line 334
    iget-object v0, p0, Lddf;->k:Landroid/os/Handler;

    const/16 v1, 0xea

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 335
    iget-object v1, p0, Lddf;->k:Landroid/os/Handler;

    iget v2, p0, Lddf;->g:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 336
    return-void
.end method
