.class public final Lgym;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3709
    invoke-direct {p0}, Lidf;-><init>()V

    .line 3712
    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lgym;->a:[Ljava/lang/String;

    .line 3709
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3756
    .line 3757
    iget-object v1, p0, Lgym;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgym;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 3759
    iget-object v2, p0, Lgym;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 3761
    invoke-static {v4}, Lidd;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 3759
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3763
    :cond_0
    add-int/lit8 v0, v1, 0x0

    .line 3764
    iget-object v1, p0, Lgym;->a:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3766
    :cond_1
    iget-object v1, p0, Lgym;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3767
    iput v0, p0, Lgym;->J:I

    .line 3768
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3705
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgym;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgym;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgym;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lgym;->a:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lgym;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lgym;->a:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lgym;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lgym;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lgym;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 3745
    iget-object v0, p0, Lgym;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3746
    iget-object v1, p0, Lgym;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 3747
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    .line 3746
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3750
    :cond_0
    iget-object v0, p0, Lgym;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 3752
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3723
    if-ne p1, p0, :cond_1

    .line 3727
    :cond_0
    :goto_0
    return v0

    .line 3724
    :cond_1
    instance-of v2, p1, Lgym;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 3725
    :cond_2
    check-cast p1, Lgym;

    .line 3726
    iget-object v2, p0, Lgym;->a:[Ljava/lang/String;

    iget-object v3, p1, Lgym;->a:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgym;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgym;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 3727
    goto :goto_0

    .line 3726
    :cond_4
    iget-object v2, p0, Lgym;->I:Ljava/util/List;

    iget-object v3, p1, Lgym;->I:Ljava/util/List;

    .line 3727
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3731
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3733
    iget-object v2, p0, Lgym;->a:[Ljava/lang/String;

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 3739
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lgym;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_0
    add-int/2addr v0, v1

    .line 3740
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 3735
    :goto_1
    iget-object v3, p0, Lgym;->a:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 3736
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgym;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    move v2, v1

    :goto_2
    add-int/2addr v2, v3

    .line 3735
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3736
    :cond_2
    iget-object v2, p0, Lgym;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 3739
    :cond_3
    iget-object v1, p0, Lgym;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
