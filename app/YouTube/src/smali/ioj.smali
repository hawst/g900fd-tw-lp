.class final Lioj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field final b:Lipu;

.field private final c:Lipz;


# direct methods
.method public constructor <init>(Lipu;)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Liok;

    invoke-direct {v0, p0, p1}, Liok;-><init>(Lioj;Liql;)V

    .line 65
    new-instance v1, Liol;

    invoke-direct {v1, p0}, Liol;-><init>(Lioj;)V

    .line 77
    new-instance v2, Lipz;

    invoke-direct {v2, v0, v1}, Lipz;-><init>(Liql;Ljava/util/zip/Inflater;)V

    iput-object v2, p0, Lioj;->c:Lipz;

    .line 78
    iget-object v0, p0, Lioj;->c:Lipz;

    invoke-static {v0}, Liqa;->a(Liql;)Lipu;

    move-result-object v0

    iput-object v0, p0, Lioj;->b:Lipu;

    .line 79
    return-void
.end method

.method private a()Lipv;
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Lioj;->b:Lipu;

    invoke-interface {v0}, Lipu;->i()I

    move-result v0

    .line 102
    iget-object v1, p0, Lioj;->b:Lipu;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, Lipu;->c(J)Lipv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/util/List;
    .locals 6

    .prologue
    .line 82
    iget v0, p0, Lioj;->a:I

    add-int/2addr v0, p1

    iput v0, p0, Lioj;->a:I

    .line 84
    iget-object v0, p0, Lioj;->b:Lipu;

    invoke-interface {v0}, Lipu;->i()I

    move-result v1

    .line 85
    if-gez v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "numberOfPairs < 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    const/16 v0, 0x400

    if-le v1, v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "numberOfPairs > 1024: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 90
    invoke-direct {p0}, Lioj;->a()Lipv;

    move-result-object v3

    invoke-virtual {v3}, Lipv;->c()Lipv;

    move-result-object v3

    .line 91
    invoke-direct {p0}, Lioj;->a()Lipv;

    move-result-object v4

    .line 92
    iget-object v5, v3, Lipv;->b:[B

    array-length v5, v5

    if-nez v5, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "name.size == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_2
    new-instance v5, Linv;

    invoke-direct {v5, v3, v4}, Linv;-><init>(Lipv;Lipv;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_3
    iget v0, p0, Lioj;->a:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lioj;->c:Lipz;

    invoke-virtual {v0}, Lipz;->b()Z

    iget v0, p0, Lioj;->a:I

    if-eqz v0, :cond_4

    new-instance v0, Ljava/io/IOException;

    iget v1, p0, Lioj;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "compressedLimit > 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_4
    return-object v2
.end method
