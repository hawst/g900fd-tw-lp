.class public final Liav;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhog;

.field public c:[Liau;

.field public d:Liaz;

.field public e:[B

.field private f:I

.field private g:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Liav;->a:Lhgz;

    iput-object v1, p0, Liav;->b:Lhog;

    sget-object v0, Liau;->a:[Liau;

    iput-object v0, p0, Liav;->c:[Liau;

    const/4 v0, 0x0

    iput v0, p0, Liav;->f:I

    iput-object v1, p0, Liav;->d:Liaz;

    iput-object v1, p0, Liav;->g:Lhgz;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Liav;->e:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Liav;->a:Lhgz;

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    iget-object v2, p0, Liav;->a:Lhgz;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Liav;->b:Lhog;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Liav;->b:Lhog;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Liav;->c:[Liau;

    if-eqz v2, :cond_2

    iget-object v2, p0, Liav;->c:[Liau;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    if-eqz v4, :cond_1

    const/4 v5, 0x3

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget v1, p0, Liav;->f:I

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Liav;->f:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Liav;->d:Liaz;

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Liav;->d:Liaz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Liav;->g:Lhgz;

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    iget-object v2, p0, Liav;->g:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Liav;->e:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v1, 0x9

    iget-object v2, p0, Liav;->e:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Liav;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Liav;->J:I

    return v0

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Liav;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Liav;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Liav;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Liav;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Liav;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Liav;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Liav;->b:Lhog;

    if-nez v0, :cond_3

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Liav;->b:Lhog;

    :cond_3
    iget-object v0, p0, Liav;->b:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Liav;->c:[Liau;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Liau;

    iget-object v3, p0, Liav;->c:[Liau;

    if-eqz v3, :cond_4

    iget-object v3, p0, Liav;->c:[Liau;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Liav;->c:[Liau;

    :goto_2
    iget-object v2, p0, Liav;->c:[Liau;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Liav;->c:[Liau;

    new-instance v3, Liau;

    invoke-direct {v3}, Liau;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Liav;->c:[Liau;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Liav;->c:[Liau;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Liav;->c:[Liau;

    new-instance v3, Liau;

    invoke-direct {v3}, Liau;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Liav;->c:[Liau;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Liav;->f:I

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Liav;->d:Liaz;

    if-nez v0, :cond_7

    new-instance v0, Liaz;

    invoke-direct {v0}, Liaz;-><init>()V

    iput-object v0, p0, Liav;->d:Liaz;

    :cond_7
    iget-object v0, p0, Liav;->d:Liaz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Liav;->g:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Liav;->g:Lhgz;

    :cond_8
    iget-object v0, p0, Liav;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Liav;->e:[B

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Liav;->a:Lhgz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liav;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Liav;->b:Lhog;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Liav;->b:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Liav;->c:[Liau;

    if-eqz v0, :cond_3

    iget-object v1, p0, Liav;->c:[Liau;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget v0, p0, Liav;->f:I

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    iget v1, p0, Liav;->f:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_4
    iget-object v0, p0, Liav;->d:Liaz;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Liav;->d:Liaz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v0, p0, Liav;->g:Lhgz;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Liav;->g:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_6
    iget-object v0, p0, Liav;->e:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Liav;->e:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_7
    iget-object v0, p0, Liav;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liav;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liav;

    iget-object v2, p0, Liav;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Liav;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Liav;->b:Lhog;

    if-nez v2, :cond_5

    iget-object v2, p1, Liav;->b:Lhog;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Liav;->c:[Liau;

    iget-object v3, p1, Liav;->c:[Liau;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Liav;->f:I

    iget v3, p1, Liav;->f:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Liav;->d:Liaz;

    if-nez v2, :cond_6

    iget-object v2, p1, Liav;->d:Liaz;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Liav;->g:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Liav;->g:Lhgz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Liav;->e:[B

    iget-object v3, p1, Liav;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Liav;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Liav;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liav;->a:Lhgz;

    iget-object v3, p1, Liav;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Liav;->b:Lhog;

    iget-object v3, p1, Liav;->b:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Liav;->d:Liaz;

    iget-object v3, p1, Liav;->d:Liaz;

    invoke-virtual {v2, v3}, Liaz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Liav;->g:Lhgz;

    iget-object v3, p1, Liav;->g:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Liav;->I:Ljava/util/List;

    iget-object v3, p1, Liav;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liav;->a:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liav;->b:Lhog;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Liav;->c:[Liau;

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget v2, p0, Liav;->f:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liav;->d:Liaz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Liav;->g:Lhgz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    iget-object v2, p0, Liav;->e:[B

    if-nez v2, :cond_8

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Liav;->I:Ljava/util/List;

    if-nez v2, :cond_9

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Liav;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Liav;->b:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    move v2, v0

    move v0, v1

    :goto_5
    iget-object v3, p0, Liav;->c:[Liau;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Liav;->c:[Liau;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_6
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    iget-object v2, p0, Liav;->c:[Liau;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Liau;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    iget-object v0, p0, Liav;->d:Liaz;

    invoke-virtual {v0}, Liaz;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_7
    iget-object v0, p0, Liav;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_8
    move v2, v0

    move v0, v1

    :goto_7
    iget-object v3, p0, Liav;->e:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Liav;->e:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v1, p0, Liav;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
