.class public final Lgfb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgeh;


# instance fields
.field final a:Lgeh;

.field final b:Lgeh;

.field final c:Lgei;

.field d:Z

.field private final e:Landroid/os/Handler;

.field private final f:Landroid/os/Handler;

.field private g:Lgeh;

.field private h:Lfrf;

.field private i:Ljava/lang/String;

.field private j:Lfqy;


# direct methods
.method public constructor <init>(Lgeh;Lgeh;Lezj;)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeh;

    iput-object v0, p0, Lgfb;->a:Lgeh;

    .line 71
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeh;

    iput-object v0, p0, Lgfb;->b:Lgeh;

    .line 72
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lgfe;

    invoke-direct {v1, p0}, Lgfe;-><init>(Lgfb;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lgfb;->e:Landroid/os/Handler;

    .line 73
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lgfc;

    invoke-direct {v1, p0}, Lgfc;-><init>(Lgfb;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lgfb;->f:Landroid/os/Handler;

    .line 74
    iget-object v0, p0, Lgfb;->e:Landroid/os/Handler;

    invoke-interface {p1, v0}, Lgeh;->a(Landroid/os/Handler;)V

    .line 75
    iget-object v0, p0, Lgfb;->f:Landroid/os/Handler;

    invoke-interface {p2, v0}, Lgeh;->a(Landroid/os/Handler;)V

    .line 76
    new-instance v1, Lgdf;

    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    invoke-direct {v1, v0}, Lgdf;-><init>(Lezj;)V

    iput-object v1, p0, Lgfb;->c:Lgei;

    .line 78
    iput-object p2, p0, Lgfb;->g:Lgeh;

    .line 79
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(F)V

    .line 212
    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(F)V

    .line 213
    return-void
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lgfb;->c:Lgei;

    invoke-interface {v0, p1}, Lgei;->a(Landroid/os/Handler;)V

    .line 84
    return-void
.end method

.method public final a(Lfrf;ILjava/lang/String;Lfqy;)V
    .locals 4

    .prologue
    .line 100
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrf;

    iput-object v0, p0, Lgfb;->h:Lfrf;

    .line 101
    invoke-static {p3}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgfb;->i:Ljava/lang/String;

    .line 102
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqy;

    iput-object v0, p0, Lgfb;->j:Lfqy;

    .line 103
    invoke-virtual {p0}, Lgfb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0, p1, p2, p3, p4}, Lgeh;->a(Lfrf;ILjava/lang/String;Lfqy;)V

    .line 112
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-boolean v0, p1, Lfrf;->h:Z

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lgfb;->c:Lgei;

    new-instance v1, Lggp;

    const-string v2, "fmt.unplayable"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lggp;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Lgei;->a(Lggp;)V

    goto :goto_0

    .line 110
    :cond_1
    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0, p1, p2, p3, p4}, Lgeh;->a(Lfrf;ILjava/lang/String;Lfqy;)V

    goto :goto_0
.end method

.method public final a(Lgec;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 238
    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lgfb;->c:Lgei;

    invoke-interface {v0, v1}, Lgei;->a(Z)V

    .line 242
    :cond_0
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(Lgec;)V

    .line 243
    invoke-virtual {p0}, Lgfb;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->l()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    iget-object v0, p0, Lgfb;->h:Lfrf;

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lgfb;->d:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lgfb;->h:Lfrf;

    iget-object v0, v0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v0, v0, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v0}, La;->c(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_5

    const/16 v0, 0x7d0

    :goto_1
    iget-object v2, p0, Lgfb;->a:Lgeh;

    iget-object v3, p0, Lgfb;->h:Lfrf;

    iget-object v4, p0, Lgfb;->b:Lgeh;

    invoke-interface {v4}, Lgeh;->i()I

    move-result v4

    add-int/2addr v0, v4

    iget-object v4, p0, Lgfb;->i:Ljava/lang/String;

    iget-object v5, p0, Lgfb;->j:Lfqy;

    invoke-interface {v2, v3, v0, v4, v5}, Lgeh;->a(Lfrf;ILjava/lang/String;Lfqy;)V

    iput-boolean v1, p0, Lgfb;->d:Z

    .line 244
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v1

    .line 243
    goto :goto_0

    :cond_5
    const/16 v0, 0xfa0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lgfb;->a:Lgeh;

    iput-object v0, p0, Lgfb;->g:Lgeh;

    iget-object v0, p0, Lgfb;->h:Lfrf;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lgfb;->a:Lgeh;

    iget-object v1, p0, Lgfb;->h:Lfrf;

    iget-object v2, p0, Lgfb;->b:Lgeh;

    invoke-interface {v2}, Lgeh;->i()I

    move-result v2

    iget-object v3, p0, Lgfb;->i:Ljava/lang/String;

    iget-object v4, p0, Lgfb;->j:Lfqy;

    invoke-interface {v0, v1, v2, v3, v4}, Lgeh;->a(Lfrf;ILjava/lang/String;Lfqy;)V

    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->f()V

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lgfb;->a:Lgeh;

    iget-object v1, p0, Lgfb;->e:Landroid/os/Handler;

    invoke-interface {v0, v1}, Lgeh;->b(Landroid/os/Handler;)V

    .line 227
    iget-object v0, p0, Lgfb;->b:Lgeh;

    iget-object v1, p0, Lgfb;->f:Landroid/os/Handler;

    invoke-interface {v0, v1}, Lgeh;->b(Landroid/os/Handler;)V

    .line 228
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(Z)V

    .line 229
    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(Z)V

    .line 230
    return-void
.end method

.method a()Z
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lgfb;->g:Lgeh;

    iget-object v1, p0, Lgfb;->a:Lgeh;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lfrf;Lfqy;Z)[Lfqj;
    .locals 1

    .prologue
    .line 122
    if-eqz p3, :cond_0

    iget-object v0, p0, Lgfb;->b:Lgeh;

    .line 123
    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lgeh;->a(Lfrf;Lfqy;Z)[Lfqj;

    move-result-object v0

    return-object v0

    .line 122
    :cond_0
    iget-object v0, p0, Lgfb;->a:Lgeh;

    goto :goto_0
.end method

.method public final b(Lfrf;Lfqy;Z)Lger;
    .locals 1

    .prologue
    .line 130
    if-eqz p3, :cond_0

    iget-object v0, p0, Lgfb;->b:Lgeh;

    .line 131
    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lgeh;->b(Lfrf;Lfqy;Z)Lger;

    move-result-object v0

    return-object v0

    .line 130
    :cond_0
    iget-object v0, p0, Lgfb;->a:Lgeh;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->b()V

    .line 117
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lgfb;->d:Z

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {p0}, Lgfb;->p()V

    .line 186
    :cond_0
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->b(I)V

    .line 187
    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lgfb;->c:Lgei;

    invoke-interface {v0, p1}, Lgei;->b(Landroid/os/Handler;)V

    .line 89
    return-void
.end method

.method public final c()Lfqj;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->c()Lfqj;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lfqj;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->d()Lfqj;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lgfb;->d:Z

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p0}, Lgfb;->p()V

    .line 173
    :cond_0
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->e()V

    .line 174
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->f()V

    .line 179
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->g()V

    .line 192
    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->g()V

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lgfb;->h:Lfrf;

    .line 194
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->h()V

    .line 199
    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->h()V

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lgfb;->h:Lfrf;

    .line 201
    return-void
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->i()I

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->j()I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lgfb;->d:Z

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->k()I

    move-result v0

    .line 164
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->k()I

    move-result v0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->l()Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lgfb;->g:Lgeh;

    invoke-interface {v0}, Lgeh;->m()Z

    move-result v0

    return v0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->n()V

    .line 206
    iget-object v0, p0, Lgfb;->c:Lgei;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgei;->a(Z)V

    .line 207
    return-void
.end method

.method public final o()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 249
    iget-object v0, p0, Lgfb;->h:Lfrf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgfb;->h:Lfrf;

    iget-boolean v0, v0, Lfrf;->h:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lgfb;->a:Lgeh;

    .line 250
    invoke-interface {v0}, Lgeh;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 251
    :goto_0
    iput-boolean v1, p0, Lgfb;->d:Z

    invoke-virtual {p0}, Lgfb;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgfb;->b:Lgeh;

    iget-object v1, p0, Lgfb;->h:Lfrf;

    iget-object v2, p0, Lgfb;->a:Lgeh;

    invoke-interface {v2}, Lgeh;->i()I

    move-result v2

    add-int/lit16 v2, v2, 0xc8

    iget-object v3, p0, Lgfb;->i:Ljava/lang/String;

    iget-object v4, p0, Lgfb;->j:Lfqy;

    invoke-interface {v0, v1, v2, v3, v4}, Lgeh;->a(Lfrf;ILjava/lang/String;Lfqy;)V

    :cond_1
    iget-object v0, p0, Lgfb;->b:Lgeh;

    iput-object v0, p0, Lgfb;->g:Lgeh;

    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->h()V

    .line 252
    iget-object v0, p0, Lgfb;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->o()V

    .line 253
    return-void

    :cond_2
    move v0, v1

    .line 250
    goto :goto_0
.end method

.method p()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 314
    iput-boolean v1, p0, Lgfb;->d:Z

    .line 315
    iget-object v0, p0, Lgfb;->a:Lgeh;

    iput-object v0, p0, Lgfb;->g:Lgeh;

    .line 316
    iget-object v0, p0, Lgfb;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->f()V

    .line 317
    iget-object v0, p0, Lgfb;->c:Lgei;

    invoke-interface {v0, v1}, Lgei;->a(Z)V

    .line 318
    return-void
.end method
