.class final Lbcl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwv;


# instance fields
.field a:Z

.field private synthetic b:Lbci;


# direct methods
.method constructor <init>(Lbci;)V
    .locals 0

    .prologue
    .line 614
    iput-object p1, p0, Lbcl;->b:Lbci;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onErrorResponse(Lxa;)V
    .locals 3

    .prologue
    .line 654
    iget-boolean v0, p0, Lbcl;->a:Z

    if-nez v0, :cond_0

    .line 655
    iget-object v0, p0, Lbcl;->b:Lbci;

    invoke-static {v0}, Lbci;->h(Lbci;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    iget-object v1, p0, Lbcl;->b:Lbci;

    invoke-static {v1}, Lbci;->k(Lbci;)Leyt;

    move-result-object v1

    invoke-interface {v1, p1}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;Z)V

    .line 657
    :cond_0
    return-void
.end method

.method public final synthetic onResponse(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 614
    check-cast p1, Lfis;

    iget-boolean v2, p0, Lbcl;->a:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lbcl;->b:Lbci;

    invoke-static {v2, v0}, Lbci;->a(Lbci;Z)Z

    iget-object v2, p0, Lbcl;->b:Lbci;

    new-instance v3, Lfqg;

    iget-object v4, p0, Lbcl;->b:Lbci;

    invoke-static {v4}, Lbci;->c(Lbci;)Letc;

    move-result-object v4

    invoke-virtual {v4}, Letc;->k()Lfac;

    move-result-object v4

    iget-object v5, p0, Lbcl;->b:Lbci;

    invoke-static {v5}, Lbci;->d(Lbci;)Lhog;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lfqg;-><init>(Lfac;Lhog;)V

    invoke-static {v2, v3}, Lbci;->a(Lbci;Lfqg;)Lfqg;

    iget-object v2, p0, Lbcl;->b:Lbci;

    invoke-static {v2}, Lbci;->f(Lbci;)Lfdw;

    move-result-object v2

    iget-object v3, p0, Lbcl;->b:Lbci;

    invoke-static {v3}, Lbci;->e(Lbci;)Lfqg;

    move-result-object v3

    sget-object v4, Lfqi;->b:Lfqi;

    invoke-virtual {v2, v3, v4, v1}, Lfdw;->a(Lfqg;Lfqi;Lhcq;)V

    iget-object v2, p0, Lbcl;->b:Lbci;

    invoke-static {v2}, Lbci;->g(Lbci;)Levn;

    move-result-object v2

    new-instance v3, Lazp;

    invoke-direct {v3}, Lazp;-><init>()V

    invoke-virtual {v2, v3}, Levn;->d(Ljava/lang/Object;)V

    iget-object v2, p1, Lfis;->a:Lhbp;

    iget-object v2, v2, Lhbp;->b:Lhbq;

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lbcl;->b:Lbci;

    invoke-static {v0}, Lbci;->h(Lbci;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    const v1, 0x7f090208

    iget-object v2, v0, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lbcl;->b:Lbci;

    iget-object v1, p1, Lfis;->a:Lhbp;

    iget-object v1, v1, Lhbp;->a:Lhtx;

    invoke-static {v1}, La;->a(Lhtx;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbci;->a(Lbci;Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lbcl;->b:Lbci;

    invoke-virtual {p1}, Lfis;->b()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lflv;

    if-eqz v0, :cond_3

    const-string v0, ""

    :goto_1
    invoke-static {v2, v0}, Lbci;->a(Lbci;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    iget-object v0, p0, Lbcl;->b:Lbci;

    iget-object v1, p0, Lbcl;->b:Lbci;

    invoke-static {v1, p1}, Lbci;->a(Lbci;Lfis;)Lcjh;

    move-result-object v1

    invoke-static {v0, v1}, Lbci;->a(Lbci;Lcjh;)V

    iget-object v0, p0, Lbcl;->b:Lbci;

    invoke-virtual {p1}, Lfis;->a()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lbcl;->b:Lbci;

    invoke-virtual {p1}, Lfis;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lbci;->a(Lbci;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lbci;->a(Lbci;Ljava/util/List;Landroid/view/View;)V

    iget-object v0, p0, Lbcl;->b:Lbci;

    invoke-static {v0}, Lbci;->b(Lbci;)V

    iget-object v0, p0, Lbcl;->b:Lbci;

    invoke-static {v0}, Lbci;->h(Lbci;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    iget-object v0, p0, Lbcl;->b:Lbci;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p1, Lfis;->a:Lhbp;

    iget-object v2, v2, Lhbp;->a:Lhtx;

    iget v2, v2, Lhtx;->c:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    iget-object v1, p0, Lbcl;->b:Lbci;

    invoke-static {v1}, Lbci;->i(Lbci;)Lezj;

    move-result-object v1

    invoke-virtual {v1}, Lezj;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    iput-wide v2, v0, Lbci;->a:J

    iget-object v0, p0, Lbcl;->b:Lbci;

    invoke-static {v0}, Lbci;->j(Lbci;)V

    iget-object v0, p0, Lbcl;->b:Lbci;

    invoke-static {v0}, Lbci;->g(Lbci;)Levn;

    move-result-object v0

    new-instance v1, Lazo;

    invoke-direct {v1}, Lazo;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lfis;->b()Ljava/lang/Object;

    move-result-object v0

    instance-of v3, v0, Lfiu;

    if-eqz v3, :cond_4

    check-cast v0, Lfiu;

    invoke-virtual {v0}, Lfiu;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    instance-of v3, v0, Lfjw;

    if-eqz v3, :cond_7

    check-cast v0, Lfjw;

    iget-object v1, v0, Lfjw;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_5

    iget-object v1, v0, Lfjw;->a:Lhgp;

    iget-object v1, v1, Lhgp;->a:Lhgz;

    if-eqz v1, :cond_6

    iget-object v1, v0, Lfjw;->a:Lhgp;

    iget-object v1, v1, Lhgp;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, v0, Lfjw;->b:Ljava/lang/CharSequence;

    :cond_5
    :goto_2
    iget-object v0, v0, Lfjw;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    const-string v1, ""

    iput-object v1, v0, Lfjw;->b:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_7
    instance-of v3, v0, Lflv;

    if-eqz v3, :cond_8

    check-cast v0, Lflv;

    invoke-virtual {v0}, Lflv;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    move-object v0, v1

    goto/16 :goto_1
.end method
