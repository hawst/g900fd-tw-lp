.class public final Lbnl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldhv;


# static fields
.field private static final a:J


# instance fields
.field private final b:Lemg;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    sput-wide v0, Lbnl;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lemg;->a(Landroid/content/Context;)Lemg;

    move-result-object v0

    iput-object v0, p0, Lbnl;->b:Lemg;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 38
    iget-object v1, p0, Lbnl;->b:Lemg;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    const-string v6, "preload_videos"

    invoke-virtual/range {v1 .. v6}, Lemg;->b(JJLjava/lang/String;)V

    .line 42
    return-void
.end method

.method public final a(J)V
    .locals 7

    .prologue
    .line 48
    iget-object v1, p0, Lbnl;->b:Lemg;

    sget-wide v4, Lbnl;->a:J

    const-string v6, "preload_videos"

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lemg;->a(JJLjava/lang/String;)V

    .line 52
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lbnl;->b:Lemg;

    const-string v1, "preload_videos"

    invoke-virtual {v0, v1}, Lemg;->a(Ljava/lang/String;)V

    .line 57
    return-void
.end method
