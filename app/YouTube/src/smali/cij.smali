.class public Lcij;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/view/View;

.field private final b:Landroid/app/Activity;

.field private final c:Lfvi;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private final g:Lbxd;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILffs;Lgix;Lcub;Leyp;Levn;Leyt;Lfhz;Lfdw;Lfrz;)V
    .locals 12

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lcij;->b:Landroid/app/Activity;

    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 59
    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcij;->a:Landroid/view/View;

    .line 60
    iget-object v1, p0, Lcij;->a:Landroid/view/View;

    const v2, 0x7f080104

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcij;->d:Landroid/widget/TextView;

    .line 61
    iget-object v1, p0, Lcij;->a:Landroid/view/View;

    const v2, 0x7f0801eb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcij;->e:Landroid/widget/TextView;

    .line 62
    iget-object v1, p0, Lcij;->a:Landroid/view/View;

    const v2, 0x7f0801ec

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcij;->f:Landroid/view/View;

    .line 63
    new-instance v2, Lfvi;

    iget-object v1, p0, Lcij;->a:Landroid/view/View;

    const v3, 0x7f08028f

    .line 65
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    move-object/from16 v0, p6

    invoke-direct {v2, v0, v1}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcij;->c:Lfvi;

    .line 66
    new-instance v1, Lbxd;

    iget-object v2, p0, Lcij;->f:Landroid/view/View;

    .line 67
    invoke-virtual {p0, v2}, Lcij;->a(Landroid/view/View;)Lbxi;

    move-result-object v2

    move-object v3, p1

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p8

    move-object/from16 v8, p7

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v1 .. v11}, Lbxd;-><init>(Lbxi;Landroid/app/Activity;Lffs;Lgix;Lcub;Leyt;Levn;Lfhz;Lfdw;Lfrz;)V

    iput-object v1, p0, Lcij;->g:Lbxd;

    .line 77
    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;)Lbxi;
    .locals 2

    .prologue
    .line 117
    new-instance v0, Lbqq;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lbqq;-><init>(Landroid/view/View;Z)V

    return-object v0
.end method

.method public final a(Lfnl;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Lcij;->d:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfnl;->c()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    invoke-virtual {p1}, Lfnl;->g()Lfnc;

    move-result-object v0

    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcij;->c:Lfvi;

    invoke-virtual {p1}, Lfnl;->g()Lfnc;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lfvi;->a(Lfnc;Leyo;)V

    .line 88
    :goto_0
    iget-object v3, p0, Lcij;->a:Landroid/view/View;

    iget-object v0, p1, Lfnl;->c:Lhog;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 90
    invoke-virtual {p1}, Lfnl;->f()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 91
    iget-object v0, p0, Lcij;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcij;->e:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfnl;->f()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    :goto_2
    iget-object v0, p1, Lfnl;->d:Lfmy;

    if-nez v0, :cond_5

    iget-object v0, p1, Lfnl;->a:Lhzu;

    iget-object v0, v0, Lhzu;->f:Lhzv;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lfnl;->a:Lhzu;

    iget-object v0, v0, Lhzu;->f:Lhzv;

    iget-object v0, v0, Lhzv;->a:Lhwp;

    if-eqz v0, :cond_5

    new-instance v0, Lfmy;

    iget-object v3, p1, Lfnl;->a:Lhzu;

    iget-object v3, v3, Lhzu;->f:Lhzv;

    iget-object v3, v3, Lhzv;->a:Lhwp;

    invoke-direct {v0, v3, p1}, Lfmy;-><init>(Lhwp;Lfqh;)V

    iput-object v0, p1, Lfnl;->d:Lfmy;

    :cond_0
    :goto_3
    iget-object v0, p1, Lfnl;->d:Lfmy;

    .line 101
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lfmy;->i()Lfiy;

    move-result-object v3

    if-nez v3, :cond_1

    .line 102
    new-instance v3, Lfiy;

    iget-object v4, p0, Lcij;->b:Landroid/app/Activity;

    const v5, 0x7f090271

    new-array v1, v1, [Ljava/lang/Object;

    .line 105
    invoke-virtual {p1}, Lfnl;->c()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v1, v2

    .line 103
    invoke-virtual {v4, v5, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    iget-object v2, p0, Lcij;->b:Landroid/app/Activity;

    const v4, 0x104000a

    .line 106
    invoke-virtual {v2, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcij;->b:Landroid/app/Activity;

    const/high16 v5, 0x1040000

    .line 107
    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v1, v2, v4}, Lfiy;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 102
    iput-object v3, v0, Lfmy;->c:Lfiy;

    .line 109
    :cond_1
    iget-object v1, p0, Lcij;->g:Lbxd;

    invoke-virtual {v1, v0}, Lbxd;->a(Lfmy;)V

    .line 110
    return-void

    .line 85
    :cond_2
    iget-object v0, p0, Lcij;->c:Lfvi;

    const v3, 0x7f0201e9

    invoke-virtual {v0, v3}, Lfvi;->c(I)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 88
    goto :goto_1

    .line 94
    :cond_4
    iget-object v0, p0, Lcij;->e:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 97
    :cond_5
    iget-object v0, p1, Lfnl;->d:Lfmy;

    if-nez v0, :cond_0

    iget-object v0, p1, Lfnl;->a:Lhzu;

    iget-object v0, v0, Lhzu;->c:Lhww;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfnl;->a:Lhzu;

    iget-object v0, v0, Lhzu;->c:Lhww;

    iget v3, v0, Lhww;->a:I

    const/4 v0, 0x2

    if-eq v3, v0, :cond_6

    move v0, v1

    :goto_4
    new-instance v4, Lhwp;

    invoke-direct {v4}, Lhwp;-><init>()V

    iput-boolean v0, v4, Lhwp;->b:Z

    iget-object v0, p1, Lfnl;->b:Ljava/lang/String;

    iput-object v0, v4, Lhwp;->e:Ljava/lang/String;

    iput v3, v4, Lhwp;->d:I

    new-instance v0, Lfmy;

    invoke-direct {v0, v4, p1}, Lfmy;-><init>(Lhwp;Lfqh;)V

    iput-object v0, p1, Lfnl;->d:Lfmy;

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_4
.end method
