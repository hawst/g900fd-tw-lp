.class final Lcta;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcua;

.field private synthetic b:Lcsy;


# direct methods
.method constructor <init>(Lcsy;Lcua;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcta;->b:Lcsy;

    iput-object p2, p0, Lcta;->a:Lcua;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 368
    iget-object v0, p0, Lcta;->b:Lcsy;

    iget-object v1, p0, Lcta;->a:Lcua;

    iget-object v2, v0, Lcsy;->d:Lcsx;

    invoke-static {}, Lb;->b()V

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "account"

    iget-object v4, v0, Lcsy;->a:Lgit;

    iget-object v4, v4, Lgit;->b:Lgiv;

    invoke-virtual {v4}, Lgiv;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Lcsy;->a:Lgit;

    iget-object v3, v3, Lgit;->b:Lgiv;

    invoke-virtual {v3}, Lgiv;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "gaia_id"

    iget-object v4, v0, Lcsy;->a:Lgit;

    iget-object v4, v4, Lgit;->b:Lgiv;

    invoke-virtual {v4}, Lgiv;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v3, "id"

    iget-object v4, v0, Lcsy;->a:Lgit;

    iget-object v4, v4, Lgit;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_1

    const-string v3, "profile_account_name_proto"

    iget-object v4, v1, Lcua;->c:Lhgz;

    invoke-static {v4}, Lidh;->a(Lidh;)[B

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "profile_account_photo_thumbnails_proto"

    iget-object v4, v1, Lcua;->e:Lfnc;

    invoke-static {v2, v3, v4}, Lcsy;->a(Landroid/content/ContentValues;Ljava/lang/String;Lfnc;)V

    const-string v3, "profile_mobile_banner_thumbnails_proto"

    iget-object v1, v1, Lcua;->f:Lfnc;

    invoke-static {v2, v3, v1}, Lcsy;->a(Landroid/content/ContentValues;Ljava/lang/String;Lfnc;)V

    :goto_1
    const-string v1, "profile_display_name"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "profile_display_email"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "profile_thumbnail_uri"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    iget-object v0, v0, Lcsy;->d:Lcsx;

    iget-object v0, v0, Lcsx;->b:Levi;

    invoke-interface {v0}, Levi;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "identity"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 369
    return-void

    .line 368
    :cond_0
    const-string v3, "gaia_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "profile_account_name_proto"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "profile_account_photo_thumbnails_proto"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "profile_mobile_banner_thumbnails_proto"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method
