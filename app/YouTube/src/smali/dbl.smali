.class public final enum Ldbl;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldbl;

.field public static final enum b:Ldbl;

.field public static final enum c:Ldbl;

.field public static final enum d:Ldbl;

.field public static final enum e:Ldbl;

.field public static final enum f:Ldbl;

.field private static final synthetic g:[Ldbl;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75
    new-instance v0, Ldbl;

    const-string v1, "BUFFERING"

    invoke-direct {v0, v1, v3}, Ldbl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbl;->a:Ldbl;

    .line 76
    new-instance v0, Ldbl;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v4}, Ldbl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbl;->b:Ldbl;

    .line 77
    new-instance v0, Ldbl;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v5}, Ldbl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbl;->c:Ldbl;

    .line 78
    new-instance v0, Ldbl;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v6}, Ldbl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbl;->d:Ldbl;

    .line 79
    new-instance v0, Ldbl;

    const-string v1, "ENDED"

    invoke-direct {v0, v1, v7}, Ldbl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbl;->e:Ldbl;

    .line 80
    new-instance v0, Ldbl;

    const-string v1, "ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Ldbl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Ldbl;->f:Ldbl;

    .line 74
    const/4 v0, 0x6

    new-array v0, v0, [Ldbl;

    sget-object v1, Ldbl;->a:Ldbl;

    aput-object v1, v0, v3

    sget-object v1, Ldbl;->b:Ldbl;

    aput-object v1, v0, v4

    sget-object v1, Ldbl;->c:Ldbl;

    aput-object v1, v0, v5

    sget-object v1, Ldbl;->d:Ldbl;

    aput-object v1, v0, v6

    sget-object v1, Ldbl;->e:Ldbl;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Ldbl;->f:Ldbl;

    aput-object v2, v0, v1

    sput-object v0, Ldbl;->g:[Ldbl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldbl;
    .locals 1

    .prologue
    .line 74
    const-class v0, Ldbl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldbl;

    return-object v0
.end method

.method public static values()[Ldbl;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Ldbl;->g:[Ldbl;

    invoke-virtual {v0}, [Ldbl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldbl;

    return-object v0
.end method
