.class final Lme;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final e:Ljava/util/Comparator;


# instance fields
.field final a:Ljava/util/List;

.field private final b:[F

.field private final c:[I

.field private final d:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lme;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 439
    new-instance v0, Lmf;

    invoke-direct {v0}, Lmf;-><init>()V

    sput-object v0, Lme;->e:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>(Lmh;I)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lme;->b:[F

    .line 85
    iget v2, p1, Lmh;->c:I

    .line 86
    iget-object v4, p1, Lmh;->a:[I

    .line 87
    iget-object v3, p1, Lmh;->b:[I

    .line 91
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v2}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lme;->d:Landroid/util/SparseIntArray;

    move v0, v1

    .line 92
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    .line 93
    iget-object v5, p0, Lme;->d:Landroid/util/SparseIntArray;

    aget v6, v4, v0

    aget v7, v3, v0

    invoke-virtual {v5, v6, v7}, Landroid/util/SparseIntArray;->append(II)V

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_0
    new-array v0, v2, [I

    iput-object v0, p0, Lme;->c:[I

    .line 99
    array-length v5, v4

    move v3, v1

    move v2, v1

    :goto_1
    if-ge v3, v5, :cond_1

    aget v6, v4, v3

    .line 100
    invoke-static {v6}, Landroid/graphics/Color;->red(I)I

    move-result v0

    invoke-static {v6}, Landroid/graphics/Color;->green(I)I

    move-result v7

    invoke-static {v6}, Landroid/graphics/Color;->blue(I)I

    move-result v8

    iget-object v9, p0, Lme;->b:[F

    invoke-static {v0, v7, v8, v9}, La;->a(III[F)V

    iget-object v0, p0, Lme;->b:[F

    invoke-static {v0}, Lme;->a([F)Z

    move-result v0

    if-nez v0, :cond_4

    .line 101
    iget-object v7, p0, Lme;->c:[I

    add-int/lit8 v0, v2, 0x1

    aput v6, v7, v2

    .line 99
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    .line 105
    :cond_1
    if-gt v2, p2, :cond_2

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lme;->a:Ljava/util/List;

    .line 108
    iget-object v0, p0, Lme;->c:[I

    array-length v2, v0

    :goto_3
    if-ge v1, v2, :cond_3

    aget v3, v0, v1

    .line 109
    iget-object v4, p0, Lme;->a:Ljava/util/List;

    new-instance v5, Lml;

    iget-object v6, p0, Lme;->d:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    invoke-direct {v5, v3, v6}, Lml;-><init>(II)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 113
    :cond_2
    add-int/lit8 v0, v2, -0x1

    new-instance v2, Ljava/util/PriorityQueue;

    sget-object v3, Lme;->e:Ljava/util/Comparator;

    invoke-direct {v2, p2, v3}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    new-instance v3, Lmg;

    invoke-direct {v3, p0, v1, v0}, Lmg;-><init>(Lme;II)V

    invoke-virtual {v2, v3}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    invoke-static {v2, p2}, Lme;->a(Ljava/util/PriorityQueue;I)V

    invoke-static {v2}, Lme;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lme;->a:Ljava/util/List;

    .line 115
    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method private static a(Ljava/util/Collection;)Ljava/util/List;
    .locals 4

    .prologue
    .line 166
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 167
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmg;

    .line 168
    invoke-virtual {v0}, Lmg;->e()Lml;

    move-result-object v0

    .line 169
    invoke-virtual {v0}, Lml;->a()[F

    move-result-object v3

    invoke-static {v3}, Lme;->a([F)Z

    move-result v3

    if-nez v3, :cond_0

    .line 172
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 175
    :cond_1
    return-object v1
.end method

.method static a(Landroid/graphics/Bitmap;I)Lme;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 69
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 70
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 72
    mul-int v0, v3, v7

    new-array v1, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 73
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 75
    new-instance v0, Lme;

    new-instance v2, Lmh;

    invoke-direct {v2, v1}, Lmh;-><init>([I)V

    invoke-direct {v0, v2, p1}, Lme;-><init>(Lmh;I)V

    return-object v0
.end method

.method private static a(Ljava/util/PriorityQueue;I)V
    .locals 6

    .prologue
    .line 150
    :goto_0
    invoke-virtual {p0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 151
    invoke-virtual {p0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmg;

    .line 153
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lmg;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 155
    invoke-virtual {v0}, Lmg;->b()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not split a box with only 1 color"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lmg;->d()I

    move-result v1

    new-instance v2, Lmg;

    iget-object v3, v0, Lmg;->b:Lme;

    add-int/lit8 v4, v1, 0x1

    iget v5, v0, Lmg;->a:I

    invoke-direct {v2, v3, v4, v5}, Lmg;-><init>(Lme;II)V

    iput v1, v0, Lmg;->a:I

    invoke-virtual {v0}, Lmg;->c()V

    invoke-virtual {p0, v2}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    .line 157
    invoke-virtual {p0, v0}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    :cond_1
    return-void
.end method

.method static synthetic a(Lme;III)V
    .locals 4

    .prologue
    .line 44
    packed-switch p1, :pswitch_data_0

    :cond_0
    :pswitch_0
    return-void

    :goto_0
    :pswitch_1
    if-gt p2, p3, :cond_0

    iget-object v0, p0, Lme;->c:[I

    aget v0, v0, p2

    iget-object v1, p0, Lme;->c:[I

    shr-int/lit8 v2, v0, 0x8

    and-int/lit16 v2, v2, 0xff

    shr-int/lit8 v3, v0, 0x10

    and-int/lit16 v3, v3, 0xff

    and-int/lit16 v0, v0, 0xff

    invoke-static {v2, v3, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    aput v0, v1, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :goto_1
    :pswitch_2
    if-gt p2, p3, :cond_0

    iget-object v0, p0, Lme;->c:[I

    aget v0, v0, p2

    iget-object v1, p0, Lme;->c:[I

    and-int/lit16 v2, v0, 0xff

    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v0, v0, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-static {v2, v3, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    aput v0, v1, p2

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a([F)Z
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 412
    aget v2, p0, v4

    const v3, 0x3f733333    # 0.95f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    aget v2, p0, v4

    const v3, 0x3d4ccccd    # 0.05f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_3

    move v2, v1

    :goto_1
    if-nez v2, :cond_0

    aget v2, p0, v0

    const/high16 v3, 0x41200000    # 10.0f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_4

    aget v2, p0, v0

    const/high16 v3, 0x42140000    # 37.0f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_4

    aget v2, p0, v1

    const v3, 0x3f51eb85    # 0.82f

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_4

    move v2, v1

    :goto_2
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_2
.end method

.method static synthetic a(Lme;)[I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lme;->c:[I

    return-object v0
.end method

.method static synthetic b(Lme;)Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lme;->d:Landroid/util/SparseIntArray;

    return-object v0
.end method
