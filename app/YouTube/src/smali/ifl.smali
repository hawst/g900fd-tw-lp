.class final Lifl;
.super Landroid/os/Handler;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/zxing/client/android/CaptureActivity;

.field private final c:Liev;

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lifl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lifl;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/google/zxing/client/android/CaptureActivity;Ljava/util/Hashtable;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lifl;->d:Z

    .line 43
    new-instance v0, Liev;

    invoke-direct {v0}, Liev;-><init>()V

    iput-object v0, p0, Lifl;->c:Liev;

    .line 44
    iget-object v0, p0, Lifl;->c:Liev;

    invoke-virtual {v0, p2}, Liev;->a(Ljava/util/Map;)V

    .line 45
    iput-object p1, p0, Lifl;->b:Lcom/google/zxing/client/android/CaptureActivity;

    .line 46
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 50
    iget-boolean v0, p0, Lifl;->d:Z

    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080001

    if-ne v0, v1, :cond_5

    .line 54
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, [B

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iget-object v0, p0, Lifl;->b:Lcom/google/zxing/client/android/CaptureActivity;

    iget-object v0, v0, Lcom/google/zxing/client/android/CaptureActivity;->b:Lifu;

    invoke-virtual {v0}, Lifu;->b()Landroid/graphics/Rect;

    move-result-object v7

    if-nez v7, :cond_3

    move-object v1, v9

    :goto_1
    if-eqz v1, :cond_6

    new-instance v0, Lieq;

    new-instance v2, Ligf;

    invoke-direct {v2, v1}, Ligf;-><init>(Lieu;)V

    invoke-direct {v0, v2}, Lieq;-><init>(Liep;)V

    :try_start_0
    iget-object v2, p0, Lifl;->c:Liev;

    iget-object v3, v2, Liev;->a:[Liex;

    if-nez v3, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Liev;->a(Ljava/util/Map;)V

    :cond_2
    invoke-virtual {v2, v0}, Liev;->a(Lieq;)Liez;
    :try_end_0
    .catch Liey; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v2, p0, Lifl;->c:Liev;

    invoke-virtual {v2}, Liev;->a()V

    :goto_2
    iget-object v2, p0, Lifl;->b:Lcom/google/zxing/client/android/CaptureActivity;

    iget-object v2, v2, Lcom/google/zxing/client/android/CaptureActivity;->c:Lifk;

    if-eqz v0, :cond_4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v3, Lifl;->a:Ljava/lang/String;

    sub-long/2addr v4, v10

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v6, 0x28

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Found barcode in "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v2, :cond_0

    const v3, 0x7f080003

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "barcode_bitmap"

    invoke-virtual {v1}, Lifr;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :cond_3
    new-instance v0, Lifr;

    iget v4, v7, Landroid/graphics/Rect;->left:I

    iget v5, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-direct/range {v0 .. v8}, Lifr;-><init>([BIIIIIIZ)V

    move-object v1, v0

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v0, p0, Lifl;->c:Liev;

    invoke-virtual {v0}, Liev;->a()V

    move-object v0, v9

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lifl;->c:Liev;

    invoke-virtual {v1}, Liev;->a()V

    throw v0

    :cond_4
    if-eqz v2, :cond_0

    const v0, 0x7f080002

    invoke-static {v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 55
    :cond_5
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x7f080005

    if-ne v0, v1, :cond_0

    .line 56
    iput-boolean v8, p0, Lifl;->d:Z

    .line 57
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    goto/16 :goto_0

    :cond_6
    move-object v0, v9

    goto/16 :goto_2
.end method
