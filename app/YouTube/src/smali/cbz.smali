.class public final Lcbz;
.super Lfsa;
.source "SourceFile"

# interfaces
.implements Leyo;


# instance fields
.field private final a:Lbhz;

.field private final b:Leyp;

.field private final c:Lfhz;

.field private final d:Lcyc;

.field private final e:Landroid/view/View;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/widget/ImageView;

.field private final i:Landroid/widget/ImageView;

.field private final j:Landroid/view/View;

.field private final m:Lfvi;

.field private final n:Lfvi;

.field private final o:Lesy;

.field private final p:Lbxd;

.field private final q:Lcca;

.field private r:Lbnw;


# direct methods
.method public constructor <init>(Lbhz;Lffs;Lgix;Lcub;Leyp;Lfhz;Levn;Lesy;Leyt;Lcyc;Lfdw;Lfrz;Lcca;)V
    .locals 13

    .prologue
    .line 92
    move-object/from16 v0, p11

    move-object/from16 v1, p12

    invoke-direct {p0, v0, v1}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 93
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbhz;

    iput-object v2, p0, Lcbz;->a:Lbhz;

    .line 94
    invoke-static/range {p5 .. p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Leyp;

    iput-object v2, p0, Lcbz;->b:Leyp;

    .line 95
    invoke-static/range {p6 .. p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfhz;

    iput-object v2, p0, Lcbz;->c:Lfhz;

    .line 96
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lesy;

    iput-object v2, p0, Lcbz;->o:Lesy;

    .line 97
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcyc;

    iput-object v2, p0, Lcbz;->d:Lcyc;

    .line 98
    move-object/from16 v0, p13

    iput-object v0, p0, Lcbz;->q:Lcca;

    .line 100
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 104
    const v3, 0x7f0400c2

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcbz;->e:Landroid/view/View;

    .line 105
    iget-object v2, p0, Lcbz;->e:Landroid/view/View;

    const v3, 0x7f0800c0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcbz;->h:Landroid/widget/ImageView;

    .line 106
    new-instance v2, Lfvi;

    iget-object v3, p0, Lcbz;->h:Landroid/widget/ImageView;

    move-object/from16 v0, p5

    invoke-direct {v2, v0, v3}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcbz;->m:Lfvi;

    .line 107
    iget-object v2, p0, Lcbz;->e:Landroid/view/View;

    const v3, 0x7f080102

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcbz;->i:Landroid/widget/ImageView;

    .line 108
    new-instance v2, Lfvi;

    iget-object v3, p0, Lcbz;->i:Landroid/widget/ImageView;

    move-object/from16 v0, p5

    invoke-direct {v2, v0, v3}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcbz;->n:Lfvi;

    .line 111
    iget-object v2, p0, Lcbz;->e:Landroid/view/View;

    const v3, 0x7f080104

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcbz;->f:Landroid/widget/TextView;

    .line 112
    iget-object v2, p0, Lcbz;->e:Landroid/view/View;

    const v3, 0x7f080105

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcbz;->g:Landroid/widget/TextView;

    .line 113
    iget-object v2, p0, Lcbz;->e:Landroid/view/View;

    const v3, 0x7f080106

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcbz;->j:Landroid/view/View;

    .line 115
    iget-object v2, p0, Lcbz;->e:Landroid/view/View;

    const v3, 0x7f0801ec

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 116
    new-instance v2, Lbxd;

    .line 117
    new-instance v3, Lbva;

    invoke-direct {v3, v4}, Lbva;-><init>(Landroid/view/View;)V

    move-object v4, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p9

    move-object/from16 v9, p7

    move-object/from16 v10, p6

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v2 .. v12}, Lbxd;-><init>(Lbxi;Landroid/app/Activity;Lffs;Lgix;Lcub;Leyt;Levn;Lfhz;Lfdw;Lfrz;)V

    iput-object v2, p0, Lcbz;->p:Lbxd;

    .line 129
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcbz;->m:Lfvi;

    const v1, 0x7f0201e9

    invoke-virtual {v0, v1}, Lfvi;->c(I)V

    .line 205
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcbz;->n:Lfvi;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lfvi;->a(Landroid/widget/ImageView$ScaleType;)V

    .line 209
    iget-object v0, p0, Lcbz;->n:Lfvi;

    const v1, 0x7f0200bc

    invoke-virtual {v0, v1}, Lfvi;->c(I)V

    .line 210
    iget-object v0, p0, Lcbz;->q:Lcca;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcbz;->q:Lcca;

    invoke-interface {v0}, Lcca;->a()V

    .line 213
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lfiu;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 133
    iget-object v0, p0, Lcbz;->f:Landroid/widget/TextView;

    invoke-virtual {p1}, Lfiu;->c()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v0, p0, Lcbz;->g:Landroid/widget/TextView;

    iget-object v4, p1, Lfiu;->d:Ljava/lang/CharSequence;

    if-nez v4, :cond_0

    iget-object v4, p1, Lfiu;->a:Lhbu;

    iget-object v4, v4, Lhbu;->e:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p1, Lfiu;->d:Ljava/lang/CharSequence;

    :cond_0
    iget-object v4, p1, Lfiu;->d:Ljava/lang/CharSequence;

    invoke-static {v0, v4}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p1, Lfiu;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 136
    iget-object v4, p0, Lcbz;->o:Lesy;

    iget-object v0, p1, Lfiu;->e:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lfiu;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_1
    invoke-virtual {v4, v0}, Lesy;->a(Landroid/net/Uri;)V

    .line 137
    iput-object v1, p1, Lfiu;->e:Ljava/lang/String;

    .line 140
    :cond_1
    iget-object v0, p1, Lfiu;->b:Lfnc;

    if-nez v0, :cond_2

    new-instance v0, Lfnc;

    iget-object v4, p1, Lfiu;->a:Lhbu;

    iget-object v4, v4, Lhbu;->b:Lhxf;

    invoke-direct {v0, v4}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p1, Lfiu;->b:Lfnc;

    :cond_2
    iget-object v0, p1, Lfiu;->b:Lfnc;

    .line 141
    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 142
    iget-object v4, p0, Lcbz;->m:Lfvi;

    invoke-virtual {v4, v0, p0}, Lfvi;->a(Lfnc;Leyo;)V

    .line 147
    :goto_2
    invoke-virtual {p1}, Lfiu;->a()Lfnc;

    move-result-object v0

    .line 148
    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 149
    iget-object v4, p0, Lcbz;->n:Lfvi;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Lfvi;->a(Landroid/widget/ImageView$ScaleType;)V

    .line 150
    iget-object v4, p0, Lcbz;->n:Lfvi;

    invoke-virtual {v4, v0, p0}, Lfvi;->a(Lfnc;Leyo;)V

    .line 155
    :goto_3
    iget-object v0, p1, Lfiu;->c:Lfiw;

    if-nez v0, :cond_3

    iget-object v0, p1, Lfiu;->a:Lhbu;

    iget-object v0, v0, Lhbu;->c:Lhbv;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lfiu;->a:Lhbu;

    iget-object v0, v0, Lhbu;->c:Lhbv;

    iget-object v0, v0, Lhbv;->a:Lhcd;

    if-eqz v0, :cond_3

    new-instance v0, Lfiw;

    iget-object v4, p1, Lfiu;->a:Lhbu;

    iget-object v4, v4, Lhbu;->c:Lhbv;

    iget-object v4, v4, Lhbv;->a:Lhcd;

    invoke-direct {v0, v4, p1}, Lfiw;-><init>(Lhcd;Lfqh;)V

    iput-object v0, p1, Lfiu;->c:Lfiw;

    :cond_3
    iget-object v4, p1, Lfiu;->c:Lfiw;

    .line 156
    invoke-virtual {p1}, Lfiu;->b()Lfmy;

    move-result-object v0

    .line 157
    if-eqz v4, :cond_b

    .line 158
    iget-object v0, p0, Lcbz;->r:Lbnw;

    if-nez v0, :cond_4

    new-instance v1, Lbnw;

    iget-object v5, p0, Lcbz;->a:Lbhz;

    iget-object v6, p0, Lcbz;->b:Leyp;

    iget-object v7, p0, Lcbz;->c:Lfhz;

    iget-object v0, p0, Lcbz;->e:Landroid/view/View;

    const v8, 0x7f080107

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v5, v6, v7, v0}, Lbnw;-><init>(Landroid/app/Activity;Leyp;Lfhz;Landroid/view/View;)V

    iput-object v1, p0, Lcbz;->r:Lbnw;

    :cond_4
    iget-object v0, p0, Lcbz;->r:Lbnw;

    invoke-virtual {v0, v4}, Lbnw;->a(Lfiw;)V

    iget-object v0, p0, Lcbz;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcbz;->d:Lcyc;

    iget-object v0, p0, Lcbz;->a:Lbhz;

    iget-object v0, p0, Lcbz;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 159
    iget-object v0, v4, Lfiw;->c:Lfmy;

    if-nez v0, :cond_5

    iget-object v0, v4, Lfiw;->a:Lhcd;

    iget-object v0, v0, Lhcd;->d:Lhce;

    if-eqz v0, :cond_5

    iget-object v0, v4, Lfiw;->a:Lhcd;

    iget-object v0, v0, Lhcd;->d:Lhce;

    iget-object v0, v0, Lhce;->a:Lhwp;

    if-eqz v0, :cond_5

    new-instance v0, Lfmy;

    iget-object v1, v4, Lfiw;->a:Lhcd;

    iget-object v1, v1, Lhcd;->d:Lhce;

    iget-object v1, v1, Lhce;->a:Lhwp;

    invoke-direct {v0, v1, v4}, Lfmy;-><init>(Lhwp;Lfqh;)V

    iput-object v0, v4, Lfiw;->c:Lfmy;

    :cond_5
    iget-object v0, v4, Lfiw;->c:Lfmy;

    .line 166
    :goto_4
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lfmy;->i()Lfiy;

    move-result-object v1

    if-nez v1, :cond_6

    .line 167
    new-instance v1, Lfiy;

    iget-object v4, p0, Lcbz;->a:Lbhz;

    const v5, 0x7f090271

    new-array v2, v2, [Ljava/lang/Object;

    .line 170
    invoke-virtual {p1}, Lfiu;->c()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v2, v3

    .line 168
    invoke-virtual {v4, v5, v2}, Lbhz;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    iget-object v3, p0, Lcbz;->a:Lbhz;

    const v4, 0x104000a

    .line 171
    invoke-virtual {v3, v4}, Lbhz;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcbz;->a:Lbhz;

    const/high16 v5, 0x1040000

    .line 172
    invoke-virtual {v4, v5}, Lbhz;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lfiy;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 167
    iput-object v1, v0, Lfmy;->c:Lfiy;

    .line 174
    :cond_6
    iget-object v1, p0, Lcbz;->p:Lbxd;

    invoke-virtual {v1, v0}, Lbxd;->a(Lfmy;)V

    .line 175
    iget-object v0, p0, Lcbz;->e:Landroid/view/View;

    return-object v0

    :cond_7
    move v0, v3

    .line 135
    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    .line 136
    goto/16 :goto_1

    .line 144
    :cond_9
    invoke-direct {p0}, Lcbz;->a()V

    goto/16 :goto_2

    .line 152
    :cond_a
    invoke-direct {p0}, Lcbz;->b()V

    goto/16 :goto_3

    .line 161
    :cond_b
    iget-object v4, p0, Lcbz;->r:Lbnw;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcbz;->r:Lbnw;

    invoke-virtual {v4, v1}, Lbnw;->a(Lfiw;)V

    :cond_c
    iget-object v1, p0, Lcbz;->j:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    check-cast p2, Lfiu;

    invoke-virtual {p0, p2}, Lcbz;->a(Lfiu;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 48
    check-cast p2, Lfiu;

    invoke-virtual {p0, p2}, Lcbz;->a(Lfiu;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 237
    return-void
.end method

.method public final a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcbz;->i:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 227
    iget-object v0, p0, Lcbz;->q:Lcca;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcbz;->q:Lcca;

    invoke-interface {v0, p2}, Lcca;->a(Landroid/graphics/Bitmap;)V

    .line 231
    :cond_0
    return-void
.end method

.method public final b(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public final c(Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcbz;->h:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    .line 218
    invoke-direct {p0}, Lcbz;->a()V

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget-object v0, p0, Lcbz;->i:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    .line 220
    invoke-direct {p0}, Lcbz;->b()V

    goto :goto_0
.end method
