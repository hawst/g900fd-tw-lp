.class public final Lgco;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Legq;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lefj;

.field public final d:Lefj;

.field public final e:Lefh;

.field public final f:Ljava/lang/String;

.field public final g:Lexd;

.field public final h:Lezi;

.field public final i:Lggn;

.field public final j:Lezj;

.field public final k:Lges;

.field public final l:Lggm;

.field public final m:Lgfx;

.field private final n:Lefh;

.field private final o:Lewi;

.field private p:Lorg/chromium/net/HttpUrlRequestFactory;

.field private q:Z

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Legq;

    invoke-direct {v0}, Legq;-><init>()V

    sput-object v0, Lgco;->a:Legq;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Lexd;Lezi;Lggn;Lewi;Lezj;Lgeq;ZZZZ)V
    .locals 6

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lgco;->b:Landroid/content/Context;

    .line 99
    new-instance v0, Lefj;

    invoke-direct {v0}, Lefj;-><init>()V

    iput-object v0, p0, Lgco;->c:Lefj;

    .line 100
    new-instance v0, Lefj;

    invoke-direct {v0}, Lefj;-><init>()V

    iput-object v0, p0, Lgco;->d:Lefj;

    .line 101
    new-instance v1, Lefh;

    .line 102
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iget-object v2, p0, Lgco;->c:Lefj;

    invoke-direct {v1, v0, v2}, Lefh;-><init>(Landroid/os/Handler;Lefj;)V

    iput-object v1, p0, Lgco;->e:Lefh;

    .line 103
    new-instance v0, Lefh;

    iget-object v1, p0, Lgco;->d:Lefj;

    invoke-direct {v0, p2, v1}, Lefh;-><init>(Landroid/os/Handler;Lefj;)V

    iput-object v0, p0, Lgco;->n:Lefh;

    .line 105
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgco;->f:Ljava/lang/String;

    .line 106
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lgco;->g:Lexd;

    .line 107
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezi;

    iput-object v0, p0, Lgco;->h:Lezi;

    .line 108
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggn;

    iput-object v0, p0, Lgco;->i:Lggn;

    .line 109
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lgco;->o:Lewi;

    .line 110
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lgco;->j:Lezj;

    .line 111
    if-nez p10, :cond_0

    const/16 v5, 0x1e0

    .line 120
    :goto_0
    new-instance v0, Lgdg;

    .line 121
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgeq;

    iget-object v3, p0, Lgco;->e:Lefh;

    move-object v2, p4

    move-object v4, p6

    invoke-direct/range {v0 .. v5}, Lgdg;-><init>(Lgeq;Lexd;Leey;Lggn;I)V

    iput-object v0, p0, Lgco;->k:Lges;

    .line 126
    new-instance v0, Lggm;

    invoke-direct {v0}, Lggm;-><init>()V

    iput-object v0, p0, Lgco;->l:Lggm;

    .line 127
    new-instance v0, Lgfx;

    invoke-direct {v0}, Lgfx;-><init>()V

    iput-object v0, p0, Lgco;->m:Lgfx;

    .line 128
    return-void

    .line 111
    :cond_0
    if-nez p11, :cond_1

    const/16 v5, 0x2d0

    goto :goto_0

    :cond_1
    if-nez p12, :cond_2

    const/16 v5, 0x438

    goto :goto_0

    :cond_2
    if-nez p13, :cond_3

    const/16 v5, 0x5a0

    goto :goto_0

    :cond_3
    const/16 v5, 0x870

    goto :goto_0
.end method

.method private a(Lefh;Z)Lewi;
    .locals 1

    .prologue
    .line 250
    new-instance v0, Lgcq;

    invoke-direct {v0, p0, p1, p2}, Lgcq;-><init>(Lgco;Lefh;Z)V

    return-object v0
.end method

.method static synthetic a(Lgco;)Lewi;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgco;->o:Lewi;

    return-object v0
.end method

.method static synthetic a(Lgco;Lorg/chromium/net/HttpUrlRequestFactory;)Lorg/chromium/net/HttpUrlRequestFactory;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lgco;->p:Lorg/chromium/net/HttpUrlRequestFactory;

    return-object p1
.end method

.method static synthetic a(Lgco;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lgco;->q:Z

    return p1
.end method

.method static synthetic b(Lgco;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgco;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lgco;Z)Z
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lgco;->r:Z

    return p1
.end method

.method static synthetic c(Lgco;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lgco;->q:Z

    return v0
.end method

.method static synthetic d(Lgco;)Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lgco;->r:Z

    return v0
.end method

.method static synthetic e(Lgco;)Lorg/chromium/net/HttpUrlRequestFactory;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgco;->p:Lorg/chromium/net/HttpUrlRequestFactory;

    return-object v0
.end method

.method static synthetic f(Lgco;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgco;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lgco;)Lezj;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgco;->j:Lezj;

    return-object v0
.end method

.method static synthetic h(Lgco;)Lggn;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lgco;->i:Lggn;

    return-object v0
.end method


# virtual methods
.method public final a()Lewi;
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lgco;->e:Lefh;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lgco;->a(Lefh;Z)Lewi;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lewi;
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lgco;->n:Lefh;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lgco;->a(Lefh;Z)Lewi;

    move-result-object v0

    return-object v0
.end method
