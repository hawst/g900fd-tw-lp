.class final Lcvo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lgog;

.field b:Ljava/lang/String;

.field volatile c:Ldan;

.field volatile d:I

.field private volatile e:Z

.field private volatile f:I


# direct methods
.method constructor <init>(Lcwq;)V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    const/4 v0, 0x3

    iput v0, p0, Lcvo;->f:I

    return-void
.end method

.method private handlePlaybackScriptedOperationEvent(Ldak;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 217
    sget-object v0, Lcvn;->c:[I

    invoke-virtual {p1}, Ldak;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 225
    :goto_0
    return-void

    .line 219
    :pswitch_0
    const/4 v0, 0x1

    iput v0, p0, Lcvo;->f:I

    goto :goto_0

    .line 222
    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lcvo;->f:I

    goto :goto_0

    .line 217
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private handleSequenceChangedEvent(Ldal;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 229
    iget-object v0, p1, Ldal;->a:Ljava/lang/String;

    iput-object v0, p0, Lcvo;->b:Ljava/lang/String;

    .line 230
    return-void
.end method

.method private handleSequencerNavigationRequestEvent(Ldam;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 172
    sget-object v0, Lcvn;->a:[I

    iget-object v1, p1, Ldam;->a:Ldan;

    invoke-virtual {v1}, Ldan;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 184
    :goto_0
    iget v0, p0, Lcvo;->f:I

    if-ne v0, v3, :cond_0

    .line 191
    const/4 v0, 0x2

    iput v0, p0, Lcvo;->f:I

    .line 195
    :goto_1
    return-void

    .line 174
    :pswitch_0
    iput-boolean v3, p0, Lcvo;->e:Z

    .line 175
    iget-object v0, p1, Ldam;->a:Ldan;

    iput-object v0, p0, Lcvo;->c:Ldan;

    goto :goto_0

    .line 181
    :pswitch_1
    iput v2, p0, Lcvo;->d:I

    .line 182
    iput-boolean v2, p0, Lcvo;->e:Z

    .line 183
    iget-object v0, p1, Ldam;->a:Ldan;

    iput-object v0, p0, Lcvo;->c:Ldan;

    goto :goto_0

    .line 193
    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcvo;->f:I

    goto :goto_1

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 199
    sget-object v0, Lcvn;->b:[I

    iget-object v1, p1, Ldac;->a:Lgol;

    invoke-virtual {v1}, Lgol;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 201
    :pswitch_0
    iget-boolean v0, p0, Lcvo;->e:Z

    if-eqz v0, :cond_0

    .line 202
    iget v0, p0, Lcvo;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcvo;->d:I

    .line 203
    iput-boolean v2, p0, Lcvo;->e:Z

    goto :goto_0

    .line 207
    :pswitch_1
    iput v2, p0, Lcvo;->d:I

    .line 208
    iput-boolean v2, p0, Lcvo;->e:Z

    goto :goto_0

    .line 199
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method a()Z
    .locals 2

    .prologue
    .line 155
    iget v0, p0, Lcvo;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
