.class public final Lgun;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgue;


# instance fields
.field final a:Lguf;

.field final b:Lgtn;

.field private final c:Lbt;

.field private final d:Lgum;

.field private final e:Lu;

.field private final f:Ljava/util/LinkedHashMap;

.field private final g:Ljava/util/LinkedHashMap;

.field private h:Z


# direct methods
.method public constructor <init>(Lguf;Lgtn;Lgum;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lgun;-><init>(Lguf;Lgtn;Lgum;Lu;)V

    .line 39
    return-void
.end method

.method private constructor <init>(Lguf;Lgtn;Lgum;Lu;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lguo;

    invoke-direct {v0, p0}, Lguo;-><init>(Lgun;)V

    iput-object v0, p0, Lgun;->c:Lbt;

    .line 30
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    .line 31
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lgun;->g:Ljava/util/LinkedHashMap;

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgun;->h:Z

    .line 46
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguf;

    iput-object v0, p0, Lgun;->a:Lguf;

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtn;

    iput-object v0, p0, Lgun;->b:Lgtn;

    .line 48
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgum;

    iput-object v0, p0, Lgun;->d:Lgum;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lgun;->e:Lu;

    .line 50
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 203
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-boolean v0, p0, Lgun;->h:Z

    invoke-static {v0}, Lb;->c(Z)V

    .line 206
    iget-object v0, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 207
    if-nez v0, :cond_0

    move v0, v1

    .line 223
    :goto_0
    monitor-exit p0

    return v0

    .line 211
    :cond_0
    :try_start_1
    iget-object v3, p0, Lgun;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :cond_1
    iget-object v1, p0, Lgun;->d:Lgum;

    iget-object v3, v0, Lguq;->a:Ljava/lang/Runnable;

    iget-wide v4, v0, Lguq;->b:J

    invoke-virtual {v1, v3, v4, v5}, Lgum;->a(Ljava/lang/Runnable;J)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 216
    iget-object v1, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 218
    goto :goto_0

    .line 220
    :cond_2
    iget-object v1, p0, Lgun;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 221
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Id collision in active task map"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v2

    .line 223
    goto :goto_0
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/Object;J)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 151
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v1, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    const/4 v0, 0x0

    .line 190
    :cond_0
    monitor-exit p0

    return v0

    .line 158
    :cond_1
    :try_start_1
    new-instance v1, Lgup;

    invoke-direct {v1, p0, p1, p2}, Lgup;-><init>(Lgun;Ljava/lang/String;Ljava/lang/Object;)V

    .line 177
    iget-object v2, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 178
    invoke-direct {p0}, Lgun;->b()V

    .line 180
    :cond_2
    iget-object v2, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    new-instance v3, Lguq;

    invoke-direct {v3, v1, p3, p4}, Lguq;-><init>(Ljava/lang/Runnable;J)V

    invoke-virtual {v2, p1, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 181
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Id collision in task map"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 183
    :cond_3
    :try_start_2
    iget-boolean v1, p0, Lgun;->h:Z

    if-eqz v1, :cond_0

    .line 187
    invoke-direct {p0, p1}, Lgun;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 188
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Could not start a task which was just created"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgun;->e:Lu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 58
    :goto_0
    monitor-exit p0

    return-void

    .line 56
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgun;->e:Lu;

    iget-object v0, p0, Lgun;->c:Lbt;

    .line 57
    iget-object v0, p0, Lgun;->e:Lu;

    invoke-interface {v0}, Lu;->g()Z

    move-result v0

    iput-boolean v0, p0, Lgun;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 233
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    iget-object v0, p0, Lgun;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    if-nez v0, :cond_0

    .line 237
    const/4 v0, 0x0

    .line 240
    :goto_0
    monitor-exit p0

    return v0

    .line 239
    :cond_0
    const/4 v2, 0x1

    :try_start_1
    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 240
    goto :goto_0

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgun;->e:Lu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 65
    :goto_0
    monitor-exit p0

    return-void

    .line 64
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgun;->e:Lu;

    iget-object v0, p0, Lgun;->c:Lbt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 3

    .prologue
    .line 286
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lgun;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 287
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 288
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lgun;->b(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 290
    :cond_0
    monitor-exit p0

    return-void

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lgun;->d()V

    .line 271
    iget-object v0, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    :goto_0
    monitor-exit p0

    return-void

    .line 274
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 275
    invoke-direct {p0}, Lgun;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 99
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :try_start_1
    iget-object v0, p0, Lgun;->a:Lguf;

    invoke-virtual {v0, p1}, Lguf;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 105
    iget-object v0, p0, Lgun;->b:Lgtn;

    invoke-interface {v0, v3}, Lgtn;->a(Ljava/lang/Object;)J

    move-result-wide v4

    .line 106
    iget-object v0, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguq;

    .line 107
    if-eqz v0, :cond_2

    .line 108
    iget-wide v6, v0, Lguq;->b:J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    cmp-long v0, v6, v4

    if-nez v0, :cond_0

    move v0, v1

    .line 135
    :goto_0
    monitor-exit p0

    return v0

    .line 113
    :cond_0
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 114
    cmp-long v0, v6, v8

    if-gtz v0, :cond_1

    cmp-long v0, v4, v8

    if-gtz v0, :cond_1

    move v0, v1

    .line 117
    goto :goto_0

    .line 120
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lgun;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 121
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Could not remove an existing task"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 132
    :catch_0
    move-exception v0

    .line 133
    :try_start_3
    iget-object v1, p0, Lgun;->a:Lguf;

    invoke-static {p1, v0}, Lguf;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move v0, v2

    .line 135
    goto :goto_0

    .line 124
    :cond_2
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    move v0, v2

    .line 126
    goto :goto_0

    .line 128
    :cond_3
    :try_start_4
    invoke-direct {p0, p1, v3, v4, v5}, Lgun;->a(Ljava/lang/String;Ljava/lang/Object;J)Z

    move-result v0

    if-nez v0, :cond_4

    .line 129
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Could not add a missing task"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v0, v1

    .line 131
    goto :goto_0
.end method

.method declared-synchronized a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 251
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    if-eqz p2, :cond_0

    .line 254
    invoke-direct {p0, p1}, Lgun;->b(Ljava/lang/String;)Z

    .line 258
    :goto_0
    iget-object v0, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    .line 260
    const/4 v0, 0x0

    .line 265
    :goto_1
    monitor-exit p0

    return v0

    .line 256
    :cond_0
    :try_start_1
    iget-object v0, p0, Lgun;->g:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 251
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 262
    :cond_1
    :try_start_2
    iget-object v0, p0, Lgun;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 263
    invoke-direct {p0}, Lgun;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 265
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
