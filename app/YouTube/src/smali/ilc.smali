.class final Lilc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lyj;


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/util/List;

.field private c:Lyn;

.field private d:J


# direct methods
.method constructor <init>(Lilb;Likx;)V
    .locals 16

    .prologue
    .line 404
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 388
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lilc;->b:Ljava/util/List;

    .line 391
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iput-wide v2, v0, Lilc;->d:J

    .line 406
    move-object/from16 v0, p2

    iget-object v2, v0, Likx;->a:Ljava/util/List;

    move-object/from16 v0, p0

    iput-object v2, v0, Lilc;->a:Ljava/util/List;

    .line 407
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 408
    move-object/from16 v0, p2

    iget-object v2, v0, Likx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Likz;

    .line 409
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1}, Lilb;->a(Likz;Likx;)[I

    move-result-object v4

    invoke-interface {v10, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 412
    :cond_0
    const/4 v2, 0x0

    move v5, v2

    :goto_1
    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    array-length v2, v2

    if-ge v5, v2, :cond_4

    .line 413
    move-object/from16 v0, p0

    iget-object v2, v0, Lilc;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Likz;

    .line 415
    invoke-interface {v10, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    .line 416
    const-wide/16 v6, 0x0

    .line 417
    const/4 v4, 0x0

    move-wide v8, v6

    :goto_2
    if-ge v4, v5, :cond_2

    .line 418
    aget v6, v3, v4

    int-to-long v6, v6

    add-long/2addr v6, v8

    .line 417
    add-int/lit8 v4, v4, 0x1

    move-wide v8, v6

    goto :goto_2

    .line 421
    :cond_2
    invoke-static {v8, v9}, La;->d(J)I

    move-result v4

    move v6, v4

    :goto_3
    int-to-long v12, v6

    aget v4, v3, v5

    int-to-long v14, v4

    add-long/2addr v14, v8

    cmp-long v4, v12, v14

    if-gez v4, :cond_1

    .line 423
    move-object/from16 v0, p1

    iget-object v4, v0, Lilb;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/nio/ByteBuffer;

    .line 424
    move-object/from16 v0, p0

    iget-wide v12, v0, Lilc;->d:J

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v7

    int-to-long v14, v7

    add-long/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Lilc;->d:J

    .line 425
    move-object/from16 v0, p0

    iget-object v7, v0, Lilc;->b:Ljava/util/List;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-result-object v4

    check-cast v4, Ljava/nio/ByteBuffer;

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 421
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_3

    .line 412
    :cond_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_1

    .line 432
    :cond_4
    return-void
.end method

.method private static a(J)Z
    .locals 4

    .prologue
    .line 459
    const-wide/16 v0, 0x8

    add-long/2addr v0, p0

    const-wide v2, 0x100000000L

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/nio/channels/ReadableByteChannel;Ljava/nio/ByteBuffer;JLyb;)V
    .locals 0

    .prologue
    .line 402
    return-void
.end method

.method public final a(Ljava/nio/channels/WritableByteChannel;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 464
    iget-object v0, p0, Lilc;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 465
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t call getBox() multiple times."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    :cond_0
    const/16 v0, 0x10

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 469
    invoke-virtual {p0}, Lilc;->l_()J

    move-result-wide v2

    .line 470
    invoke-static {v2, v3}, Lilc;->a(J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 471
    invoke-static {v0, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 475
    :goto_0
    const-string v1, "mdat"

    invoke-static {v1}, Lye;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 476
    invoke-static {v2, v3}, Lilc;->a(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 477
    const/16 v1, 0x8

    new-array v1, v1, [B

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 481
    :goto_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 482
    invoke-interface {p1, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 484
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lilc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 485
    iget-object v0, p0, Lilc;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 486
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 487
    invoke-interface {p1, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 489
    iget-object v0, p0, Lilc;->b:Ljava/util/List;

    invoke-interface {v0, v1, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 484
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 473
    :cond_1
    const-wide/16 v4, 0x1

    invoke-static {v0, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    goto :goto_0

    .line 479
    :cond_2
    invoke-static {v0, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    goto :goto_1

    .line 491
    :cond_3
    iput-object v6, p0, Lilc;->b:Ljava/util/List;

    .line 492
    return-void
.end method

.method public final a(Lyn;)V
    .locals 0

    .prologue
    .line 398
    iput-object p1, p0, Lilc;->c:Lyn;

    .line 399
    return-void
.end method

.method public final b()J
    .locals 5

    .prologue
    .line 435
    .line 436
    const-wide/16 v0, 0x10

    .line 437
    :goto_0
    invoke-interface {p0}, Lyj;->k_()Lyn;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 438
    invoke-interface {p0}, Lyj;->k_()Lyn;

    move-result-object v2

    invoke-interface {v2}, Lyn;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 439
    if-eq p0, v0, :cond_0

    .line 440
    invoke-interface {v0}, Lyj;->l_()J

    move-result-wide v0

    add-long/2addr v2, v0

    .line 443
    goto :goto_1

    .line 444
    :cond_0
    invoke-interface {p0}, Lyj;->k_()Lyn;

    move-result-object p0

    move-wide v0, v2

    goto :goto_0

    .line 446
    :cond_1
    return-wide v0
.end method

.method public final k_()Lyn;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lilc;->c:Lyn;

    return-object v0
.end method

.method public final l_()J
    .locals 4

    .prologue
    .line 455
    const-wide/16 v0, 0x10

    iget-wide v2, p0, Lilc;->d:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public final m_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    const-string v0, "mdat"

    return-object v0
.end method
