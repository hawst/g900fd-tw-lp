.class final Lcwc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private synthetic a:Lcvz;


# direct methods
.method constructor <init>(Lcvz;)V
    .locals 0

    .prologue
    .line 1306
    iput-object p1, p0, Lcwc;->a:Lcvz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 11

    .prologue
    const v5, 0x7f09011b

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1310
    iget v0, p1, Landroid/os/Message;->what:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "PlayerEvent: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 1311
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1313
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->m:Lgol;

    invoke-virtual {v0}, Lgol;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1322
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_1

    :cond_1
    :goto_1
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_2

    :goto_2
    if-eqz v2, :cond_2

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget v2, p1, Landroid/os/Message;->what:I

    iput v2, v0, Lcvz;->p:I

    iget-object v0, p0, Lcwc;->a:Lcvz;

    invoke-virtual {v0}, Lcvz;->y()V

    .line 1324
    :cond_2
    :goto_3
    return v1

    .line 1311
    :pswitch_2
    iget-object v0, p0, Lcwc;->a:Lcvz;

    const/4 v3, 0x2

    iput v3, v0, Lcvz;->l:I

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcwc;->a:Lcvz;

    const/4 v3, 0x3

    iput v3, v0, Lcvz;->l:I

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iput v1, v0, Lcvz;->l:I

    goto :goto_0

    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lgdq;

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget-object v3, v3, Lcvz;->f:Lcwn;

    invoke-virtual {v3, v0}, Lcwn;->a(Lgdq;)V

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget-object v3, v3, Lcvz;->b:Levn;

    invoke-virtual {v3, v0}, Levn;->c(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v4, v0, Lcwn;->o:Lcpo;

    if-eqz v4, :cond_0

    iget-object v0, v0, Lcwn;->o:Lcpo;

    iget v4, v0, Lcpo;->k:I

    if-eq v3, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcpo;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ":"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v0, Lcpo;->i:Lcpx;

    const-string v7, "sur"

    invoke-virtual {v6, v7}, Lcpx;->a(Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput v3, v0, Lcpo;->k:I

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v3, v0, Lcvz;->b:Levn;

    new-instance v4, Ldap;

    iget v0, p1, Landroid/os/Message;->arg1:I

    if-eqz v0, :cond_3

    move v0, v1

    :goto_4
    invoke-direct {v4, v0}, Ldap;-><init>(Z)V

    invoke-virtual {v3, v4}, Levn;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_4

    :pswitch_8
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->b:Levn;

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Levn;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1322
    :pswitch_9
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget v3, v3, Lcvz;->o:F

    iget-object v4, p0, Lcwc;->a:Lcvz;

    iget-object v4, v4, Lcvz;->j:Lfrd;

    iget-object v4, v4, Lfrd;->a:Lfqy;

    invoke-virtual {v0, v3, v4}, Lcvz;->a(FLfqy;)V

    goto/16 :goto_1

    :pswitch_a
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget-object v3, v3, Lcvz;->m:Lgol;

    invoke-virtual {v3}, Lgol;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget v3, v3, Lcvz;->h:I

    int-to-long v4, v3

    :goto_5
    invoke-virtual {v0, v4, v5}, Lcwn;->a(J)V

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->n:Lcwd;

    iget-object v3, v0, Lcwd;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcwd;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v3, v0, Lcwd;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v3}, Lcwd;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcwc;->a:Lcvz;

    sget-object v3, Lgol;->e:Lgol;

    invoke-virtual {v0, v3}, Lcvz;->b(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcwc;->a:Lcvz;

    sget-object v3, Lgol;->f:Lgol;

    invoke-virtual {v0, v3}, Lcvz;->c(Lgol;)V

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->b:Levn;

    new-instance v3, Lczk;

    invoke-direct {v3}, Lczk;-><init>()V

    invoke-virtual {v0, v3}, Levn;->d(Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcwc;->a:Lcvz;

    sget-object v3, Lgol;->h:Lgol;

    invoke-virtual {v0, v3}, Lcvz;->b(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcwc;->a:Lcvz;

    sget-object v3, Lgol;->i:Lgol;

    invoke-virtual {v0, v3}, Lcvz;->c(Lgol;)V

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->b:Levn;

    new-instance v3, Lczk;

    invoke-direct {v3}, Lczk;-><init>()V

    invoke-virtual {v0, v3}, Levn;->d(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget v3, v3, Lcvz;->g:I

    int-to-long v4, v3

    goto :goto_5

    :pswitch_b
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    iget-object v3, v0, Lcwn;->q:Lcqa;

    if-eqz v3, :cond_6

    iget-object v3, v0, Lcwn;->q:Lcqa;

    invoke-virtual {v3, v2}, Lcqa;->a(Z)V

    :cond_6
    iget-object v3, v0, Lcwn;->c:Lcnh;

    if-eqz v3, :cond_7

    iget-object v3, v0, Lcwn;->c:Lcnh;

    invoke-virtual {v3}, Lcnh;->g()V

    :cond_7
    iget-object v3, v0, Lcwn;->o:Lcpo;

    if-eqz v3, :cond_8

    iget-object v0, v0, Lcwn;->o:Lcpo;

    invoke-virtual {v0}, Lcpo;->a()V

    :cond_8
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->n:Lcwd;

    invoke-virtual {v0}, Lcwd;->a()V

    goto/16 :goto_1

    :pswitch_c
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    iget-object v3, v0, Lcwn;->q:Lcqa;

    if-eqz v3, :cond_9

    iget-object v3, v0, Lcwn;->q:Lcqa;

    invoke-virtual {v3, v2}, Lcqa;->a(Z)V

    :cond_9
    iget-object v3, v0, Lcwn;->c:Lcnh;

    if-eqz v3, :cond_a

    iget-object v3, v0, Lcwn;->c:Lcnh;

    invoke-virtual {v3}, Lcnh;->h()V

    :cond_a
    iget-object v3, v0, Lcwn;->o:Lcpo;

    if-eqz v3, :cond_b

    iget-object v0, v0, Lcwn;->o:Lcpo;

    sget-object v3, Lcpu;->e:Lcpu;

    invoke-virtual {v0, v3}, Lcpo;->a(Lcpu;)V

    :cond_b
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->a:Lcvk;

    invoke-virtual {v0}, Lcvk;->b()V

    goto/16 :goto_1

    :pswitch_d
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    invoke-virtual {v0, v1}, Lcwn;->a(Z)V

    goto/16 :goto_1

    :pswitch_e
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    invoke-virtual {v0, v2}, Lcwn;->a(Z)V

    goto/16 :goto_1

    :pswitch_f
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->n:Lcwd;

    invoke-virtual {v0}, Lcwd;->a()V

    iget-object v3, v0, Lcwd;->c:Lcvz;

    iget-object v3, v3, Lcvz;->c:Lgeh;

    invoke-interface {v3}, Lgeh;->j()I

    move-result v3

    iget-object v0, v0, Lcwd;->c:Lcvz;

    const/16 v4, 0x64

    invoke-static {v0, v3, v3, v4}, Lcvz;->a(Lcvz;III)V

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    invoke-virtual {v0}, Lcwn;->c()V

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->m:Lgol;

    invoke-virtual {v0}, Lgol;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcwc;->a:Lcvz;

    sget-object v3, Lcyv;->a:Lcyv;

    invoke-static {v0, v3}, Lcvz;->a(Lcvz;Lcyv;)V

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lcwc;->a:Lcvz;

    sget-object v3, Lgol;->j:Lgol;

    invoke-virtual {v0, v3}, Lcvz;->c(Lgol;)V

    goto/16 :goto_1

    :pswitch_10
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lggp;

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget-object v3, v3, Lcvz;->b:Levn;

    invoke-virtual {v3, v0}, Levn;->d(Ljava/lang/Object;)V

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget-object v4, v0, Lggp;->a:Ljava/lang/String;

    const-string v6, "staleconfig"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_d

    const-string v6, "net.timeout"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    :cond_d
    iget-object v4, v3, Lcvz;->i:Lfrl;

    if-eqz v4, :cond_e

    iget-object v4, v3, Lcvz;->i:Lfrl;

    iget-object v4, v4, Lfrl;->d:Lfrf;

    if-eqz v4, :cond_e

    iget-object v4, v3, Lcvz;->i:Lfrl;

    iget-object v4, v4, Lfrl;->d:Lfrf;

    iget-object v3, v3, Lcvz;->e:Lezj;

    invoke-virtual {v3}, Lezj;->b()J

    move-result-wide v6

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0xa

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v8, v9, v10}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Lfrf;->a(J)Z

    move-result v3

    if-eqz v3, :cond_e

    move v3, v1

    :goto_6
    if-eqz v3, :cond_10

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->i:Lfrl;

    iget-object v0, v0, Lfrl;->d:Lfrf;

    iget-object v2, p0, Lcwc;->a:Lcvz;

    iget-object v2, v2, Lcvz;->e:Lezj;

    invoke-virtual {v2}, Lezj;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lfrf;->a(J)Z

    move-result v4

    if-nez v4, :cond_f

    const/4 v0, -0x1

    :goto_7
    iget-object v2, p0, Lcwc;->a:Lcvz;

    iget-object v2, v2, Lcvz;->b:Levn;

    new-instance v3, Ldao;

    invoke-direct {v3, v0}, Ldao;-><init>(I)V

    invoke-virtual {v2, v3}, Levn;->c(Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_e
    move v3, v2

    goto :goto_6

    :cond_f
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v6, v0, Lfrf;->i:J

    sub-long/2addr v2, v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3, v0}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    long-to-int v0, v2

    goto :goto_7

    :cond_10
    invoke-virtual {v0}, Lggp;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcwc;->a:Lcvz;

    iget-object v3, v3, Lcvz;->m:Lgol;

    invoke-virtual {v3}, Lgol;->c()Z

    move-result v3

    if-eqz v3, :cond_11

    iget-object v0, p0, Lcwc;->a:Lcvz;

    sget-object v3, Lcyv;->b:Lcyv;

    invoke-static {v0, v3}, Lcvz;->a(Lcvz;Lcyv;)V

    :goto_8
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->n:Lcwd;

    invoke-virtual {v0}, Lcwd;->a()V

    goto/16 :goto_1

    :cond_11
    iget-object v6, v0, Lggp;->a:Ljava/lang/String;

    const v3, 0x7f09011a

    sget-object v4, Lczc;->h:Lczc;

    const-string v7, "net.nomobiledata"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_13

    const v3, 0x7f0900eb

    move-object v0, v4

    move v4, v3

    move v3, v1

    :goto_9
    if-eqz v3, :cond_12

    iget-object v5, p0, Lcwc;->a:Lcvz;

    iget-object v5, v5, Lcvz;->m:Lgol;

    invoke-virtual {v5}, Lgol;->d()Z

    move-result v5

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcwc;->a:Lcvz;

    iget-object v6, p0, Lcwc;->a:Lcvz;

    invoke-virtual {v6}, Lcvz;->B()I

    move-result v6

    iput v6, v5, Lcvz;->g:I

    :cond_12
    iget-object v5, p0, Lcwc;->a:Lcvz;

    new-instance v6, Lczb;

    iget-object v7, p0, Lcwc;->a:Lcvz;

    iget-object v7, v7, Lcvz;->d:Landroid/content/Context;

    invoke-virtual {v7, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v0, v3, v4}, Lczb;-><init>(Lczc;ZLjava/lang/String;)V

    invoke-virtual {v5, v6}, Lcvz;->c(Lczb;)V

    goto :goto_8

    :cond_13
    const-string v7, "net.dns"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_14

    move-object v0, v4

    move v3, v1

    move v4, v5

    goto :goto_9

    :cond_14
    const-string v7, "net.connect"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_15

    move-object v0, v4

    move v3, v1

    move v4, v5

    goto :goto_9

    :cond_15
    const-string v5, "net.timeout"

    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_16

    const-string v5, "net.closed"

    invoke-virtual {v6, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_17

    :cond_16
    const v3, 0x7f09011c

    move-object v0, v4

    move v4, v3

    move v3, v1

    goto :goto_9

    :cond_17
    const-string v5, "fmt.unplayable"

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_18

    const v3, 0x7f09011d

    move-object v0, v4

    move v4, v3

    move v3, v2

    goto :goto_9

    :cond_18
    const-string v5, "drm.missingapi"

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_19

    const v3, 0x7f090108

    move-object v0, v4

    move v4, v3

    move v3, v2

    goto :goto_9

    :cond_19
    const-string v5, "drm.auth"

    invoke-virtual {v6, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1a

    iget-object v5, v0, Lggp;->c:Ljava/lang/Object;

    instance-of v5, v5, Ljava/lang/Integer;

    if-eqz v5, :cond_1a

    iget-object v0, v0, Lggp;->c:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sget-object v0, Lczc;->f:Lczc;

    sparse-switch v4, :sswitch_data_0

    :goto_a
    move v4, v3

    move v3, v1

    goto/16 :goto_9

    :sswitch_0
    const v3, 0x7f090104

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_1
    const v3, 0x7f0900fc

    sget-object v0, Lczc;->g:Lczc;

    goto :goto_a

    :sswitch_2
    const v3, 0x7f090101

    sget-object v0, Lczc;->g:Lczc;

    goto :goto_a

    :sswitch_3
    const v3, 0x7f090102

    sget-object v0, Lczc;->g:Lczc;

    goto :goto_a

    :sswitch_4
    const v3, 0x7f090103

    sget-object v0, Lczc;->g:Lczc;

    goto :goto_a

    :sswitch_5
    const v3, 0x7f0900fb

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_6
    const v3, 0x7f090105

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_7
    const v3, 0x7f0900fd

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_8
    const v3, 0x7f090106

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_9
    const v3, 0x7f090107

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_a
    const v3, 0x7f0900fe

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_b
    const v3, 0x7f090100

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :sswitch_c
    const v3, 0x7f0900ff

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :cond_1a
    const-string v0, "drm"

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    sget-object v0, Lczc;->f:Lczc;

    move v4, v3

    move v3, v2

    goto/16 :goto_9

    :pswitch_11
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v4, v0, Lcwn;->q:Lcqa;

    if-eqz v4, :cond_1b

    iget-object v4, v0, Lcwn;->q:Lcqa;

    int-to-long v6, v3

    invoke-virtual {v4, v6, v7}, Lcqa;->a(J)V

    :cond_1b
    iget-object v3, v0, Lcwn;->o:Lcpo;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcwn;->o:Lcpo;

    sget-object v3, Lcpu;->g:Lcpu;

    invoke-virtual {v0, v3}, Lcpo;->a(Lcpu;)V

    goto/16 :goto_1

    :pswitch_12
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->a:Lcvk;

    iget v3, p1, Landroid/os/Message;->arg1:I

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Lcvk;->b(J)J

    move-result-wide v4

    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->n:Lcwd;

    iput-wide v4, v0, Lcwd;->b:J

    goto/16 :goto_1

    :pswitch_13
    iget-object v0, p0, Lcwc;->a:Lcvz;

    iget-object v0, v0, Lcvz;->f:Lcwn;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v4, v0, Lcwn;->o:Lcpo;

    if-eqz v4, :cond_1

    iget-object v0, v0, Lcwn;->o:Lcpo;

    iget v4, v0, Lcpo;->p:I

    add-int/2addr v3, v4

    iput v3, v0, Lcpo;->p:I

    invoke-virtual {v0, v2}, Lcpo;->a(Z)V

    goto/16 :goto_1

    :pswitch_14
    move v2, v1

    goto/16 :goto_2

    :cond_1c
    move-object v0, v4

    move v4, v3

    move v3, v1

    goto/16 :goto_9

    .line 1311
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_6
    .end packed-switch

    .line 1322
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_1
        :pswitch_13
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_0
        0x12d -> :sswitch_1
        0x12f -> :sswitch_2
        0x130 -> :sswitch_3
        0x131 -> :sswitch_4
        0x190 -> :sswitch_5
        0x191 -> :sswitch_6
        0x192 -> :sswitch_7
        0x193 -> :sswitch_8
        0x195 -> :sswitch_9
        0x1f4 -> :sswitch_a
        0x1f5 -> :sswitch_b
        0x1f6 -> :sswitch_c
    .end sparse-switch
.end method
