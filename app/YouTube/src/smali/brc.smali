.class final Lbrc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic a:Lfxe;

.field private synthetic b:Landroid/app/Activity;

.field private synthetic c:Lcuo;


# direct methods
.method constructor <init>(Lfxe;Landroid/app/Activity;Lcuo;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lbrc;->a:Lfxe;

    iput-object p2, p0, Lbrc;->b:Landroid/app/Activity;

    iput-object p3, p0, Lbrc;->c:Lcuo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 105
    iget-object v0, p0, Lbrc;->a:Lfxe;

    invoke-interface {v0}, Lfxe;->c()V

    .line 106
    invoke-static {}, Lbra;->a()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "next"

    const-string v2, "/merge_identity_done"

    .line 107
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "feature"

    const-string v2, "android"

    .line 108
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "app_version"

    iget-object v2, p0, Lbrc;->b:Landroid/app/Activity;

    .line 111
    invoke-static {v2}, La;->s(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 109
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "username"

    iget-object v2, p0, Lbrc;->c:Lcuo;

    .line 112
    invoke-virtual {v2}, Lcuo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 114
    invoke-static {v0}, Ldol;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lbrc;->b:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 116
    return-void
.end method
