.class public final Laek;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final a:I

.field final b:Lael;

.field final c:Laez;

.field final d:Landroid/widget/ImageButton;

.field e:Ldbt;

.field f:Landroid/widget/TextView;

.field private final g:I

.field private final h:I

.field private final i:Landroid/view/View;

.field private final j:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Laen;Lafa;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 47
    const-string v0, "context cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 48
    const-string v0, "optionsViewListener cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-virtual {p0}, Laek;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 52
    const/high16 v1, 0x42480000    # 50.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Laek;->a:I

    .line 53
    const/high16 v1, 0x42340000    # 45.0f

    mul-float/2addr v1, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Laek;->g:I

    .line 54
    const/high16 v1, 0x40e00000    # 7.0f

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Laek;->h:I

    .line 56
    new-instance v0, Lael;

    invoke-direct {v0, p1, p2}, Lael;-><init>(Landroid/content/Context;Laen;)V

    iput-object v0, p0, Laek;->b:Lael;

    .line 57
    iget-object v0, p0, Laek;->b:Lael;

    invoke-virtual {p0, v0}, Laek;->addView(Landroid/view/View;)V

    .line 59
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laek;->i:Landroid/view/View;

    .line 60
    iget-object v0, p0, Laek;->i:Landroid/view/View;

    const v1, 0x7f020055

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 61
    iget-object v0, p0, Laek;->i:Landroid/view/View;

    invoke-virtual {p0, v0}, Laek;->addView(Landroid/view/View;)V

    .line 63
    new-instance v0, Laez;

    invoke-direct {v0, p1, p3}, Laez;-><init>(Landroid/content/Context;Lafa;)V

    iput-object v0, p0, Laek;->c:Laez;

    .line 64
    iget-object v0, p0, Laek;->c:Laez;

    invoke-virtual {p0, v0}, Laek;->addView(Landroid/view/View;)V

    .line 66
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Laek;->f:Landroid/widget/TextView;

    .line 67
    iget-object v0, p0, Laek;->f:Landroid/widget/TextView;

    const v1, 0x7f02004f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 68
    iget-object v0, p0, Laek;->f:Landroid/widget/TextView;

    const-string v1, "LIVE"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Laek;->f:Landroid/widget/TextView;

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 70
    iget-object v0, p0, Laek;->f:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 71
    iget-object v0, p0, Laek;->f:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 72
    iget-object v0, p0, Laek;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Laek;->addView(Landroid/view/View;)V

    .line 74
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 75
    sget-object v1, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    const v2, 0x7f020051

    .line 76
    invoke-static {p1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 75
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 77
    sget-object v1, Landroid/view/View;->ENABLED_STATE_SET:[I

    const v2, 0x7f020050

    .line 78
    invoke-static {p1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 77
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 79
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Laek;->j:Landroid/widget/ImageButton;

    .line 80
    iget-object v1, p0, Laek;->j:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 81
    iget-object v1, p0, Laek;->j:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    iget-object v0, p0, Laek;->j:Landroid/widget/ImageButton;

    iget v1, p0, Laek;->h:I

    iget v2, p0, Laek;->h:I

    iget v3, p0, Laek;->h:I

    iget v4, p0, Laek;->h:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 83
    iget-object v0, p0, Laek;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Laek;->j:Landroid/widget/ImageButton;

    const v1, 0x7f09013b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Laek;->j:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Laek;->addView(Landroid/view/View;)V

    .line 87
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 88
    sget-object v1, Landroid/view/View;->PRESSED_ENABLED_SELECTED_STATE_SET:[I

    const v2, 0x7f020053

    .line 89
    invoke-static {p1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 88
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 90
    sget-object v1, Landroid/view/View;->ENABLED_SELECTED_STATE_SET:[I

    const v2, 0x7f020052

    .line 91
    invoke-static {p1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 90
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 92
    sget-object v1, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    const v2, 0x7f02004e

    .line 93
    invoke-static {p1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 94
    sget-object v1, Landroid/view/View;->ENABLED_STATE_SET:[I

    const v2, 0x7f02004d

    .line 95
    invoke-static {p1, v2}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 94
    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 96
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    .line 97
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 98
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 99
    iget-object v0, p0, Laek;->j:Landroid/widget/ImageButton;

    iget v1, p0, Laek;->h:I

    iget v2, p0, Laek;->h:I

    iget v3, p0, Laek;->h:I

    iget v4, p0, Laek;->h:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/ImageButton;->setPadding(IIII)V

    .line 100
    iget-object v0, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iget-object v0, p0, Laek;->d:Landroid/widget/ImageButton;

    const v1, 0x7f09012f

    .line 102
    invoke-virtual {p1, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {p0, v0}, Laek;->addView(Landroid/view/View;)V

    .line 104
    return-void
.end method

.method private a(Landroid/view/View;I)I
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Laek;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 217
    iget-object v1, p0, Laek;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    div-int/lit8 v2, v0, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p2

    iget-object v3, p0, Laek;->i:Landroid/view/View;

    .line 218
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v3, v0

    .line 217
    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/view/View;->layout(IIII)V

    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 161
    iget v0, p0, Laek;->a:I

    iget v1, p0, Laek;->g:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x2

    return v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Laek;->c:Laez;

    int-to-long v2, p1

    iput-wide v2, v0, Laez;->b:J

    invoke-virtual {v0}, Laez;->a()V

    .line 136
    return-void
.end method

.method public final a(III)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Laek;->c:Laez;

    invoke-virtual {v0, p1, p2, p3}, Laez;->a(III)V

    .line 140
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Laek;->c:Laez;

    iput-boolean p1, v0, Laez;->a:Z

    .line 132
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 144
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Laek;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_0

    const v0, 0x7f090130

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 146
    return-void

    .line 144
    :cond_0
    const v0, 0x7f09012f

    goto :goto_0
.end method

.method public final gatherTransparentRegion(Landroid/graphics/Region;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 236
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 237
    invoke-virtual {p0, v0}, Laek;->getLocationInWindow([I)V

    .line 238
    aget v1, v0, v3

    aget v2, v0, v6

    aget v3, v0, v3

    invoke-virtual {p0}, Laek;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Laek;->getLeft()I

    move-result v4

    sub-int/2addr v3, v4

    aget v0, v0, v6

    .line 239
    invoke-virtual {p0}, Laek;->getBottom()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {p0}, Laek;->getTop()I

    move-result v4

    sub-int v4, v0, v4

    sget-object v5, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    move-object v0, p1

    .line 238
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Region;->op(IIIILandroid/graphics/Region$Op;)Z

    .line 240
    return v6
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 224
    iget-object v0, p0, Laek;->e:Ldbt;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "listener not set for ControlsOverlay"

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    .line 226
    iget-object v0, p0, Laek;->d:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_3

    .line 227
    iget-object v0, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isSelected()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {p0, v1}, Laek;->b(Z)V

    .line 228
    iget-object v0, p0, Laek;->e:Ldbt;

    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    invoke-interface {v0, v1}, Ldbt;->a(Z)V

    .line 232
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 224
    goto :goto_0

    :cond_2
    move v1, v2

    .line 227
    goto :goto_1

    .line 229
    :cond_3
    iget-object v0, p0, Laek;->j:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 230
    iget-object v0, p0, Laek;->b:Lael;

    invoke-virtual {v0}, Lael;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lael;->d:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1}, Landroid/view/animation/AnimationSet;->cancel()V

    iget-object v1, v0, Lael;->e:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1}, Landroid/view/animation/AnimationSet;->reset()V

    iget-object v1, v0, Lael;->e:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Lael;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    :cond_4
    iget-object v1, v0, Lael;->e:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1}, Landroid/view/animation/AnimationSet;->cancel()V

    iget-object v1, v0, Lael;->d:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1}, Landroid/view/animation/AnimationSet;->reset()V

    iget-object v1, v0, Lael;->d:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0, v1}, Lael;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v0, v2}, Lael;->setVisibility(I)V

    goto :goto_2
.end method

.method protected final onLayout(ZIIII)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 194
    sub-int v0, p4, p2

    .line 195
    sub-int v1, p5, p3

    .line 197
    iget-object v2, p0, Laek;->i:Landroid/view/View;

    iget-object v3, p0, Laek;->i:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v4, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 198
    iget-object v1, p0, Laek;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    .line 199
    iget-object v2, p0, Laek;->b:Lael;

    iget-object v3, p0, Laek;->b:Lael;

    invoke-virtual {v3}, Lael;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {v2, v4, v3, v0, v1}, Lael;->layout(IIII)V

    .line 201
    iget v0, p0, Laek;->h:I

    .line 202
    iget-object v1, p0, Laek;->c:Laez;

    invoke-virtual {v1}, Laez;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 203
    iget-object v1, p0, Laek;->c:Laez;

    invoke-direct {p0, v1, v0}, Laek;->a(Landroid/view/View;I)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :goto_0
    iget v1, p0, Laek;->h:I

    add-int/2addr v0, v1

    .line 209
    iget-object v1, p0, Laek;->j:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v0}, Laek;->a(Landroid/view/View;I)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    if-eq v1, v5, :cond_0

    .line 211
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v0}, Laek;->a(Landroid/view/View;I)I

    .line 213
    :cond_0
    return-void

    .line 205
    :cond_1
    iget-object v1, p0, Laek;->f:Landroid/widget/TextView;

    invoke-direct {p0, v1, v0}, Laek;->a(Landroid/view/View;I)I

    .line 206
    iget-object v1, p0, Laek;->c:Laez;

    invoke-virtual {v1}, Laez;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 166
    const/4 v0, 0x0

    invoke-static {v0, p1}, Laek;->getDefaultSize(II)I

    move-result v0

    .line 167
    invoke-virtual {p0}, Laek;->a()I

    move-result v1

    invoke-static {v1, p2}, Laek;->resolveSize(II)I

    move-result v1

    .line 168
    invoke-virtual {p0, v0, v1}, Laek;->setMeasuredDimension(II)V

    .line 170
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 171
    iget-object v2, p0, Laek;->b:Lael;

    iget v3, p0, Laek;->g:I

    .line 172
    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 171
    invoke-virtual {v2, v1, v3}, Lael;->measure(II)V

    .line 175
    iget v2, p0, Laek;->a:I

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 176
    iget v3, p0, Laek;->a:I

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 178
    iget-object v4, p0, Laek;->i:Landroid/view/View;

    invoke-virtual {v4, v1, v2}, Landroid/view/View;->measure(II)V

    .line 179
    iget-object v1, p0, Laek;->j:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageButton;->measure(II)V

    .line 181
    iget v1, p0, Laek;->h:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 182
    iget-object v1, p0, Laek;->j:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 183
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 184
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageButton;->measure(II)V

    .line 185
    iget-object v1, p0, Laek;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    .line 187
    :cond_0
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 188
    iget-object v1, p0, Laek;->c:Laez;

    invoke-virtual {v1, v0, v3}, Laez;->measure(II)V

    .line 189
    iget-object v1, p0, Laek;->f:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3}, Landroid/widget/TextView;->measure(II)V

    .line 190
    return-void
.end method
