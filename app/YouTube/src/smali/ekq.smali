.class public final Lekq;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lj;

.field private final c:Landroid/content/Intent;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Intent;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lekq;->a:Landroid/app/Activity;

    const/4 v0, 0x0

    iput-object v0, p0, Lekq;->b:Lj;

    iput-object p2, p0, Lekq;->c:Landroid/content/Intent;

    iput p3, p0, Lekq;->d:I

    return-void
.end method

.method public constructor <init>(Lj;Landroid/content/Intent;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lekq;->a:Landroid/app/Activity;

    iput-object p1, p0, Lekq;->b:Lj;

    iput-object p2, p0, Lekq;->c:Landroid/content/Intent;

    iput p3, p0, Lekq;->d:I

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lekq;->c:Landroid/content/Intent;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lekq;->b:Lj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lekq;->b:Lj;

    iget-object v1, p0, Lekq;->c:Landroid/content/Intent;

    iget v2, p0, Lekq;->d:I

    iget-object v3, v0, Lj;->w:Lo;

    if-nez v3, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " not attached to Activity"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v0, "SettingsRedirect"

    const-string v1, "Can\'t redirect to app settings for Google Play services"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v3, v0, Lj;->w:Lo;

    invoke-virtual {v3, v0, v1, v2}, Lo;->a(Lj;Landroid/content/Intent;I)V

    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lekq;->c:Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lekq;->a:Landroid/app/Activity;

    iget-object v1, p0, Lekq;->c:Landroid/content/Intent;

    iget v2, p0, Lekq;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
