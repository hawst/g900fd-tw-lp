.class public final Lbzc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Leyt;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Leyt;)V
    .locals 1

    .prologue
    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 508
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbzc;->a:Landroid/app/Activity;

    .line 509
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbzc;->b:Leyt;

    .line 510
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 502
    const-string v0, "GData"

    const-string v1, "InvalidEntryException"

    const/4 v2, 0x0

    const-string v3, "Video already in playlist"

    invoke-static {p2, v0, v1, v2, v3}, Lcmr;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbzc;->a:Landroid/app/Activity;

    const v1, 0x7f0902ae

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Error adding to watch later"

    invoke-static {v0, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lbzc;->b:Leyt;

    invoke-interface {v0, p2}, Leyt;->c(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 502
    iget-object v0, p0, Lbzc;->a:Landroid/app/Activity;

    const v1, 0x7f0902ad

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    return-void
.end method
