.class public final enum Lcye;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcye;

.field public static final enum b:Lcye;

.field public static final enum c:Lcye;

.field public static final enum d:Lcye;

.field private static final synthetic e:[Lcye;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcye;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcye;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcye;->a:Lcye;

    .line 26
    new-instance v0, Lcye;

    const-string v1, "SIMULATE_AD_LOAD"

    invoke-direct {v0, v1, v3}, Lcye;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcye;->b:Lcye;

    .line 27
    new-instance v0, Lcye;

    const-string v1, "PRECACHE_AD_METADATA"

    invoke-direct {v0, v1, v4}, Lcye;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcye;->c:Lcye;

    .line 28
    new-instance v0, Lcye;

    const-string v1, "PRECACHE_AD_STREAM"

    invoke-direct {v0, v1, v5}, Lcye;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcye;->d:Lcye;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcye;

    sget-object v1, Lcye;->a:Lcye;

    aput-object v1, v0, v2

    sget-object v1, Lcye;->b:Lcye;

    aput-object v1, v0, v3

    sget-object v1, Lcye;->c:Lcye;

    aput-object v1, v0, v4

    sget-object v1, Lcye;->d:Lcye;

    aput-object v1, v0, v5

    sput-object v0, Lcye;->e:[Lcye;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcye;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcye;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcye;

    return-object v0
.end method

.method public static values()[Lcye;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcye;->e:[Lcye;

    invoke-virtual {v0}, [Lcye;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcye;

    return-object v0
.end method
