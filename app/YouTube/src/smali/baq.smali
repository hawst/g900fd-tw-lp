.class final Lbaq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ListView;

.field private final d:Landroid/widget/TextView;

.field private final e:Lfsi;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 458
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 459
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 460
    const v1, 0x7f040054

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lbaq;->a:Landroid/view/ViewGroup;

    .line 462
    iget-object v0, p0, Lbaq;->a:Landroid/view/ViewGroup;

    const v1, 0x7f080180

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbaq;->b:Landroid/widget/TextView;

    .line 463
    iget-object v0, p0, Lbaq;->a:Landroid/view/ViewGroup;

    const v1, 0x7f080181

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbaq;->c:Landroid/widget/ListView;

    .line 464
    iget-object v0, p0, Lbaq;->a:Landroid/view/ViewGroup;

    const v1, 0x7f080182

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbaq;->d:Landroid/widget/TextView;

    .line 466
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lbaq;->e:Lfsi;

    .line 467
    iget-object v0, p0, Lbaq;->e:Lfsi;

    const-class v1, Lbam;

    new-instance v2, Lbao;

    invoke-direct {v2, p1}, Lbao;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    .line 468
    iget-object v0, p0, Lbaq;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lbaq;->e:Lfsi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 469
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 2

    .prologue
    .line 450
    check-cast p2, Lbap;

    iget-object v0, p0, Lbaq;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lbap;->a:Lgmb;

    iget-object v1, v1, Lgmb;->a:Lgcd;

    iget-object v1, v1, Lgcd;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lbap;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lbap;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbaq;->a:Landroid/view/ViewGroup;

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lbaq;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lbaq;->e:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    iget-object v0, p0, Lbaq;->e:Lfsi;

    iget-object v1, p2, Lbap;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lfsi;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lbaq;->e:Lfsi;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    iget-object v0, p0, Lbaq;->a:Landroid/view/ViewGroup;

    goto :goto_0
.end method
