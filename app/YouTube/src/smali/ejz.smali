.class final Lejz;
.super Ljava/lang/Object;

# interfaces
.implements Lejr;


# instance fields
.field final a:I

.field final b:Ljava/util/concurrent/locks/Lock;

.field c:Leiz;

.field d:I

.field volatile e:I

.field volatile f:I

.field g:J

.field final h:Landroid/os/Handler;

.field final i:Landroid/os/Bundle;

.field j:Z

.field final k:Ljava/util/Set;

.field private final l:Ljava/util/concurrent/locks/Condition;

.field private final m:Lekz;

.field private final n:Landroid/os/Looper;

.field private o:Ljava/util/Queue;

.field private p:Z

.field private q:I

.field private final r:Ljava/util/Map;

.field private final s:Ljava/util/Set;

.field private final t:Leke;

.field private final u:Lejt;

.field private final v:Lelb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;II)V
    .locals 12

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v2, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    iget-object v2, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v2

    iput-object v2, p0, Lejz;->l:Ljava/util/concurrent/locks/Condition;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lejz;->o:Ljava/util/Queue;

    const/4 v2, 0x4

    iput v2, p0, Lejz;->e:I

    const/4 v2, 0x0

    iput-boolean v2, p0, Lejz;->p:Z

    const-wide/16 v2, 0x1388

    iput-wide v2, p0, Lejz;->g:J

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lejz;->i:Landroid/os/Bundle;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lejz;->r:Ljava/util/Map;

    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lejz;->s:Ljava/util/Set;

    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lejz;->k:Ljava/util/Set;

    new-instance v2, Leka;

    invoke-direct {v2, p0}, Leka;-><init>(Lejz;)V

    iput-object v2, p0, Lejz;->t:Leke;

    new-instance v2, Lekb;

    invoke-direct {v2, p0}, Lekb;-><init>(Lejz;)V

    iput-object v2, p0, Lejz;->u:Lejt;

    new-instance v2, Lekc;

    invoke-direct {v2, p0}, Lekc;-><init>(Lejz;)V

    iput-object v2, p0, Lejz;->v:Lelb;

    new-instance v2, Lekz;

    iget-object v3, p0, Lejz;->v:Lelb;

    invoke-direct {v2, p2, v3}, Lekz;-><init>(Landroid/os/Looper;Lelb;)V

    iput-object v2, p0, Lejz;->m:Lekz;

    iput-object p2, p0, Lejz;->n:Landroid/os/Looper;

    new-instance v2, Lekf;

    invoke-direct {v2, p0, p2}, Lekf;-><init>(Lejz;Landroid/os/Looper;)V

    iput-object v2, p0, Lejz;->h:Landroid/os/Handler;

    move/from16 v0, p8

    iput v0, p0, Lejz;->a:I

    invoke-interface/range {p5 .. p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lejt;

    iget-object v4, p0, Lejz;->m:Lekz;

    invoke-virtual {v4, v2}, Lekz;->a(Lejt;)V

    goto :goto_0

    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lejc;

    iget-object v4, p0, Lejz;->m:Lekz;

    invoke-virtual {v4, v2}, Lekz;->a(Lejc;)V

    goto :goto_1

    :cond_1
    invoke-interface/range {p4 .. p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lejj;

    iget-object v2, v3, Lejj;->a:Lejl;

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    iget-object v10, p0, Lejz;->r:Ljava/util/Map;

    iget-object v11, v3, Lejj;->b:Lejm;

    iget-object v7, p0, Lejz;->u:Lejt;

    new-instance v8, Lekd;

    invoke-direct {v8, p0, v2}, Lekd;-><init>(Lejz;Lejl;)V

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v2 .. v8}, Lejl;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lejt;Lejc;)Lejk;

    move-result-object v2

    invoke-interface {v10, v11, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_2
    iget-object v2, p3, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lejz;)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lejz;->q:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lejz;->q:I

    iget v0, p0, Lejz;->q:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lejz;->c:Leiz;

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lejz;->p:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lejz;->a(I)V

    invoke-virtual {p0}, Lejz;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lejz;->h:Landroid/os/Handler;

    iget-object v1, p0, Lejz;->h:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lejz;->g:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    iput-boolean v4, p0, Lejz;->j:Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lejz;->m:Lekz;

    iget-object v1, p0, Lejz;->c:Leiz;

    invoke-virtual {v0, v1}, Lekz;->a(Leiz;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    iput v0, p0, Lejz;->e:I

    invoke-direct {p0}, Lejz;->g()V

    iget-object v0, p0, Lejz;->l:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, Lejz;->f()V

    iget-boolean v0, p0, Lejz;->p:Z

    if-eqz v0, :cond_3

    iput-boolean v4, p0, Lejz;->p:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lejz;->a(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lejz;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lejz;->m:Lekz;

    invoke-virtual {v1, v0}, Lekz;->a(Landroid/os/Bundle;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lejz;->i:Landroid/os/Bundle;

    goto :goto_2
.end method

.method private a(Lekg;)V
    .locals 2

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-interface {p1}, Lekg;->c()Lejm;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lb;->b(ZLjava/lang/Object;)V

    iget-object v0, p0, Lejz;->k:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lejz;->t:Leke;

    invoke-interface {p1, v0}, Lekg;->a(Leke;)V

    invoke-virtual {p0}, Lejz;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lekg;->c(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-interface {p1}, Lekg;->c()Lejm;

    move-result-object v0

    invoke-virtual {p0, v0}, Lejz;->a(Lejm;)Lejk;

    move-result-object v0

    invoke-interface {p1, v0}, Lekg;->b(Lejk;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    invoke-virtual {p0}, Lejz;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lejz;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lb;->a(ZLjava/lang/Object;)V

    :goto_1
    iget-object v0, p0, Lejz;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :try_start_1
    iget-object v0, p0, Lejz;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-direct {p0, v0}, Lejz;->a(Lekg;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "GoogleApiClientImpl"

    const-string v2, "Service died while flushing queue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lejz;->f:I

    iget-object v0, p0, Lejz;->h:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(Lejm;)Lejk;
    .locals 2

    iget-object v0, p0, Lejz;->r:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejk;

    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Lb;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public final a(Lejp;)Lejp;
    .locals 2

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    new-instance v0, Lejo;

    iget-object v1, p0, Lejz;->n:Landroid/os/Looper;

    invoke-direct {v0, v1}, Lejo;-><init>(Landroid/os/Looper;)V

    iput-object v0, p1, Lejn;->b:Lejo;

    invoke-virtual {p0}, Lejz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lejz;->b(Lejp;)Lejp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lejz;->o:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a()V
    .locals 2

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lejz;->p:Z

    invoke-virtual {p0}, Lejz;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lejz;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lejz;->j:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lejz;->c:Leiz;

    const/4 v0, 0x1

    iput v0, p0, Lejz;->e:I

    iget-object v0, p0, Lejz;->i:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    iget-object v0, p0, Lejz;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lejz;->q:I

    iget-object v0, p0, Lejz;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejk;

    invoke-interface {v0}, Lejk;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method a(I)V
    .locals 5

    const/4 v1, 0x3

    const/4 v4, -0x1

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget v0, p0, Lejz;->e:I

    if-eq v0, v1, :cond_a

    if-ne p1, v4, :cond_4

    invoke-virtual {p0}, Lejz;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lejz;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0}, Lekg;->a()V

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lejz;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    :cond_1
    iget-object v0, p0, Lejz;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lekg;

    invoke-interface {v0}, Lekg;->a()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lejz;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lejz;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcw;

    invoke-virtual {v0}, Lcw;->r()V

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lejz;->s:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lejz;->c:Leiz;

    if-nez v0, :cond_4

    iget-object v0, p0, Lejz;->o:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lejz;->p:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_3
    return-void

    :cond_4
    :try_start_2
    invoke-virtual {p0}, Lejz;->d()Z

    move-result v0

    invoke-virtual {p0}, Lejz;->c()Z

    move-result v1

    const/4 v2, 0x3

    iput v2, p0, Lejz;->e:I

    if-eqz v0, :cond_6

    if-ne p1, v4, :cond_5

    const/4 v0, 0x0

    iput-object v0, p0, Lejz;->c:Leiz;

    :cond_5
    iget-object v0, p0, Lejz;->l:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    :cond_6
    const/4 v0, 0x0

    iput-boolean v0, p0, Lejz;->j:Z

    iget-object v0, p0, Lejz;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lejk;

    invoke-interface {v0}, Lejk;->c()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v0}, Lejk;->b()V

    goto :goto_4

    :cond_8
    const/4 v0, 0x1

    iput-boolean v0, p0, Lejz;->j:Z

    const/4 v0, 0x4

    iput v0, p0, Lejz;->e:I

    if-eqz v1, :cond_a

    if-eq p1, v4, :cond_9

    iget-object v0, p0, Lejz;->m:Lekz;

    invoke-virtual {v0, p1}, Lekz;->a(I)V

    :cond_9
    const/4 v0, 0x0

    iput-boolean v0, p0, Lejz;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_a
    iget-object v0, p0, Lejz;->b:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_3
.end method

.method public final a(Lejc;)V
    .locals 1

    iget-object v0, p0, Lejz;->m:Lekz;

    invoke-virtual {v0, p1}, Lekz;->a(Lejc;)V

    return-void
.end method

.method public final a(Lejt;)V
    .locals 1

    iget-object v0, p0, Lejz;->m:Lekz;

    invoke-virtual {v0, p1}, Lekz;->a(Lejt;)V

    return-void
.end method

.method public final b(Lejp;)Lejp;
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lejz;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lejz;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, Lb;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lejz;->f()V

    :try_start_0
    invoke-direct {p0, p1}, Lejz;->a(Lekg;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-object p1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v1}, Lejz;->a(I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    invoke-direct {p0}, Lejz;->g()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lejz;->a(I)V

    return-void
.end method

.method public final b(Lejc;)V
    .locals 4

    iget-object v0, p0, Lejz;->m:Lekz;

    invoke-static {p1}, Lb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lekz;->e:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lekz;->e:Ljava/util/ArrayList;

    iget-object v0, v0, Lekz;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionFailedListener(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lejt;)V
    .locals 4

    iget-object v0, p0, Lejz;->m:Lekz;

    invoke-static {p1}, Lb;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lekz;->b:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lekz;->b:Ljava/util/ArrayList;

    iget-object v2, v0, Lekz;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionCallbacks(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    iget-boolean v2, v0, Lekz;->d:Z

    if-eqz v2, :cond_0

    iget-object v0, v0, Lekz;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()Z
    .locals 2

    iget v0, p0, Lejz;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lejz;->e:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()Z
    .locals 1

    iget v0, p0, Lejz;->f:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
