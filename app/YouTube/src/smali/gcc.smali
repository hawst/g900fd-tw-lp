.class public final enum Lgcc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgcc;

.field public static final enum b:Lgcc;

.field public static final enum c:Lgcc;

.field private static final synthetic d:[Lgcc;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lgcc;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v2}, Lgcc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcc;->a:Lgcc;

    new-instance v0, Lgcc;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v3}, Lgcc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcc;->b:Lgcc;

    new-instance v0, Lgcc;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lgcc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgcc;->c:Lgcc;

    const/4 v0, 0x3

    new-array v0, v0, [Lgcc;

    sget-object v1, Lgcc;->a:Lgcc;

    aput-object v1, v0, v2

    sget-object v1, Lgcc;->b:Lgcc;

    aput-object v1, v0, v3

    sget-object v1, Lgcc;->c:Lgcc;

    aput-object v1, v0, v4

    sput-object v0, Lgcc;->d:[Lgcc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgcc;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lgcc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgcc;

    return-object v0
.end method

.method public static values()[Lgcc;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lgcc;->d:[Lgcc;

    invoke-virtual {v0}, [Lgcc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgcc;

    return-object v0
.end method
