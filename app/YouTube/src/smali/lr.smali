.class public Llr;
.super Landroid/app/Dialog;
.source "SourceFile"


# instance fields
.field private final a:Lsb;

.field private final b:Lls;

.field private c:Lrz;

.field private d:Ljava/util/ArrayList;

.field private e:Llt;

.field private f:Landroid/widget/ListView;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Llr;-><init>(Landroid/content/Context;I)V

    .line 62
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-static {p1, v0}, La;->a(Landroid/content/Context;Z)Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 54
    sget-object v0, Lrz;->c:Lrz;

    iput-object v0, p0, Llr;->c:Lrz;

    .line 66
    invoke-virtual {p0}, Llr;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 68
    invoke-static {v0}, Lsb;->a(Landroid/content/Context;)Lsb;

    move-result-object v0

    iput-object v0, p0, Llr;->a:Lsb;

    .line 69
    new-instance v0, Lls;

    invoke-direct {v0, p0}, Lls;-><init>(Llr;)V

    iput-object v0, p0, Llr;->b:Lls;

    .line 70
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 180
    iget-boolean v0, p0, Llr;->g:Z

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Llr;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 182
    iget-object v0, p0, Llr;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Llr;->a:Lsb;

    invoke-static {}, Lsb;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 183
    iget-object v2, p0, Llr;->d:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsk;

    invoke-virtual {p0, v0}, Llr;->a(Lsk;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move v0, v1

    goto :goto_0

    .line 184
    :cond_0
    iget-object v0, p0, Llr;->d:Ljava/util/ArrayList;

    sget-object v1, Llu;->a:Llu;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 185
    iget-object v0, p0, Llr;->e:Llt;

    invoke-virtual {v0}, Llt;->notifyDataSetChanged()V

    .line 187
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lrz;)V
    .locals 3

    .prologue
    .line 88
    if-nez p1, :cond_0

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Llr;->c:Lrz;

    invoke-virtual {v0, p1}, Lrz;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 93
    iput-object p1, p0, Llr;->c:Lrz;

    .line 95
    iget-boolean v0, p0, Llr;->g:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Llr;->a:Lsb;

    iget-object v1, p0, Llr;->b:Lls;

    invoke-virtual {v0, v1}, Lsb;->a(Lsc;)V

    .line 97
    iget-object v0, p0, Llr;->a:Lsb;

    iget-object v1, p0, Llr;->b:Lls;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Lsb;->a(Lrz;Lsc;I)V

    .line 101
    :cond_1
    invoke-virtual {p0}, Llr;->a()V

    .line 103
    :cond_2
    return-void
.end method

.method public a(Lsk;)Z
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p1}, Lsk;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lsk;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Llr;->c:Lrz;

    invoke-virtual {p1, v0}, Lsk;->a(Lrz;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 161
    invoke-super {p0}, Landroid/app/Dialog;->onAttachedToWindow()V

    .line 163
    iput-boolean v3, p0, Llr;->g:Z

    .line 164
    iget-object v0, p0, Llr;->a:Lsb;

    iget-object v1, p0, Llr;->c:Lrz;

    iget-object v2, p0, Llr;->b:Lls;

    invoke-virtual {v0, v1, v2, v3}, Lsb;->a(Lrz;Lsc;I)V

    .line 165
    invoke-virtual {p0}, Llr;->a()V

    .line 166
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 139
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 141
    invoke-virtual {p0}, Llr;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 143
    const v0, 0x7f040095

    invoke-virtual {p0, v0}, Llr;->setContentView(I)V

    .line 144
    const v0, 0x7f090077

    invoke-virtual {p0, v0}, Llr;->setTitle(I)V

    .line 147
    invoke-virtual {p0}, Llr;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0}, Llr;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010040

    invoke-static {v1, v2}, La;->a(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/view/Window;->setFeatureDrawableResource(II)V

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Llr;->d:Ljava/util/ArrayList;

    .line 152
    new-instance v0, Llt;

    invoke-virtual {p0}, Llr;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Llr;->d:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2}, Llt;-><init>(Llr;Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Llr;->e:Llt;

    .line 153
    const v0, 0x7f08022a

    invoke-virtual {p0, v0}, Llr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Llr;->f:Landroid/widget/ListView;

    .line 154
    iget-object v0, p0, Llr;->f:Landroid/widget/ListView;

    iget-object v1, p0, Llr;->e:Llt;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    iget-object v0, p0, Llr;->f:Landroid/widget/ListView;

    iget-object v1, p0, Llr;->e:Llt;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 156
    iget-object v0, p0, Llr;->f:Landroid/widget/ListView;

    const v1, 0x1020004

    invoke-virtual {p0, v1}, Llr;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 157
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Llr;->g:Z

    .line 171
    iget-object v0, p0, Llr;->a:Lsb;

    iget-object v1, p0, Llr;->b:Lls;

    invoke-virtual {v0, v1}, Lsb;->a(Lsc;)V

    .line 173
    invoke-super {p0}, Landroid/app/Dialog;->onDetachedFromWindow()V

    .line 174
    return-void
.end method
