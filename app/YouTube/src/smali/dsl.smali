.class public final Ldsl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ldwq;

.field final b:Z

.field c:Z

.field private final d:Ldwl;


# direct methods
.method public constructor <init>(Ldwq;Ldwl;Z)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldsl;->c:Z

    .line 27
    iput-object p2, p0, Ldsl;->d:Ldwl;

    .line 28
    iput-object p1, p0, Ldsl;->a:Ldwq;

    .line 29
    iput-boolean p3, p0, Ldsl;->b:Z

    .line 30
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 78
    iget-boolean v0, p0, Ldsl;->c:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unselect route, is user initiated: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-boolean v0, p0, Ldsl;->b:Z

    .line 79
    iget-object v0, p0, Ldsl;->a:Ldwq;

    iget-boolean v1, p0, Ldsl;->c:Z

    invoke-interface {v0, v1}, Ldwq;->a(Z)V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldsl;->c:Z

    .line 81
    return-void
.end method

.method public final a(Ldwr;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, -0x1

    .line 38
    const-string v0, "select route "

    invoke-virtual {p1}, Ldwr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_0
    iget-boolean v0, p0, Ldsl;->b:Z

    .line 39
    iget-object v6, p0, Ldsl;->d:Ldwl;

    new-instance v0, Ldsm;

    invoke-direct {v0, p0, p1}, Ldsm;-><init>(Ldsl;Ldwr;)V

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v6, Ldwl;->d:Leue;

    if-eqz v1, :cond_0

    iget-object v1, v6, Ldwl;->d:Leue;

    const/4 v2, 0x1

    iput-boolean v2, v1, Leue;->a:Z

    iput-object v4, v6, Ldwl;->d:Leue;

    :cond_0
    iget-object v1, v6, Ldwl;->c:Ldwn;

    if-eqz v1, :cond_2

    iget-object v1, v6, Ldwl;->c:Ldwn;

    invoke-interface {v1}, Ldwn;->a()Lcws;

    move-result-object v2

    invoke-virtual {v2}, Lcws;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v3, Ldwk;

    invoke-direct {v3}, Ldwk;-><init>()V

    invoke-virtual {v3, v1}, Ldwk;->a(Ljava/lang/String;)Ldwk;

    move-result-object v3

    invoke-virtual {v2}, Lcws;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ldwk;->b(Ljava/lang/String;)Ldwk;

    move-result-object v3

    invoke-virtual {v2}, Lcws;->f()I

    move-result v2

    invoke-virtual {v3, v2}, Ldwk;->a(I)Ldwk;

    move-result-object v2

    iget-object v3, v6, Ldwl;->c:Ldwn;

    invoke-interface {v3}, Ldwn;->b()I

    move-result v3

    iput v3, v2, Ldwk;->b:I

    iget-object v3, v6, Ldwl;->c:Ldwn;

    invoke-interface {v3}, Ldwn;->c()Lgpa;

    move-result-object v3

    iput-object v3, v2, Ldwk;->c:Lgpa;

    invoke-virtual {v2}, Ldwk;->a()Ldwj;

    move-result-object v2

    new-instance v3, Ldwm;

    invoke-direct {v3, v6, v2, v0}, Ldwm;-><init>(Ldwl;Ldwj;Leuc;)V

    invoke-static {v3}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, v6, Ldwl;->d:Leue;

    iget-object v0, v6, Ldwl;->a:Ldaq;

    sget-object v2, Ldaq;->a:[B

    const-string v3, ""

    const-string v4, ""

    iget-object v7, v6, Ldwl;->d:Leue;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    .line 54
    :goto_1
    return-void

    .line 38
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_2
    const-string v1, "error: no available player source, will connect to TV without videoId"

    iget-boolean v1, v6, Ldwl;->b:Z

    sget-object v1, Ldwj;->f:Ldwj;

    invoke-interface {v0, v4, v1}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method
