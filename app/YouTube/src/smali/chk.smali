.class public final Lchk;
.super Lcif;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcyc;Leyp;Lfhz;Lboi;ILfdw;Lfrz;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct/range {p0 .. p8}, Lcif;-><init>(Landroid/content/Context;Lcyc;Leyp;Lfhz;Lboi;ILfdw;Lfrz;)V

    .line 43
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 48
    iget-object v0, p0, Lchk;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 49
    iget-object v0, p0, Lchk;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 50
    iget-object v1, p0, Lchk;->b:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 52
    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    .line 53
    iget-object v3, p0, Lchk;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 54
    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 55
    const v3, 0x7f0b0020

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v3, v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 56
    iput v5, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 57
    const v0, 0x7f0b0021

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 58
    iget-object v0, p0, Lchk;->b:Landroid/view/View;

    const v1, 0x7f0a00eb

    .line 59
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 58
    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 77
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v3, p0, Lchk;->c:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 66
    iput v7, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 67
    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 68
    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 69
    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 70
    iget-object v0, p0, Lchk;->b:Landroid/view/View;

    const v1, 0x7f0a00da

    .line 72
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 70
    invoke-virtual {v0, v5, v1, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method
