.class public final Lcaj;
.super Lbqm;
.source "SourceFile"


# instance fields
.field public final c:I

.field public final d:I


# direct methods
.method private constructor <init>(II)V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbqm;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 23
    iput p1, p0, Lcaj;->c:I

    .line 24
    iput p2, p0, Lcaj;->d:I

    .line 25
    return-void
.end method

.method public static a(II)Lcaj;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcaj;

    invoke-direct {v0, p0, p1}, Lcaj;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public final a(Lbqm;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 37
    instance-of v0, p1, Lcaj;

    if-nez v0, :cond_0

    move v0, v1

    .line 41
    :goto_0
    return v0

    :cond_0
    move-object v0, p1

    .line 40
    check-cast v0, Lcaj;

    .line 41
    iget v2, v0, Lcaj;->c:I

    iget v0, v0, Lcaj;->d:I

    invoke-virtual {p0, v2, v0}, Lcaj;->b(II)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcaj;->a:Landroid/graphics/drawable/Drawable;

    iget-object v2, p1, Lbqm;->a:Landroid/graphics/drawable/Drawable;

    if-ne v0, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(II)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 45
    if-eq p1, v4, :cond_0

    iget v2, p0, Lcaj;->c:I

    if-ne p1, v2, :cond_0

    move v3, v0

    .line 47
    :goto_0
    if-eq p2, v4, :cond_1

    iget v2, p0, Lcaj;->d:I

    if-ne p2, v2, :cond_1

    move v2, v0

    .line 50
    :goto_1
    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    :goto_2
    return v0

    :cond_0
    move v3, v1

    .line 45
    goto :goto_0

    :cond_1
    move v2, v1

    .line 47
    goto :goto_1

    :cond_2
    move v0, v1

    .line 50
    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 55
    iget v0, p0, Lcaj;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcaj;->c:I

    .line 56
    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcaj;->d:I

    .line 57
    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcaj;->a:Landroid/graphics/drawable/Drawable;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x28

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "resId 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " abColor 0x"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sbColor 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " drawable "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
