.class public Ldst;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ldsw;

.field public final b:Ldth;

.field public final c:Ljava/lang/String;

.field public final d:Ldss;

.field public final e:Ldtb;

.field private final f:Ldtl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ldsu;

    invoke-direct {v0}, Ldsu;-><init>()V

    sput-object v0, Ldst;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ldth;Ljava/lang/String;Ldss;Ldtb;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    sget-object v0, Ldsw;->a:Ldsw;

    iput-object v0, p0, Ldst;->a:Ldsw;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Ldst;->f:Ldtl;

    .line 57
    iput-object p1, p0, Ldst;->b:Ldth;

    .line 58
    iput-object p2, p0, Ldst;->c:Ljava/lang/String;

    .line 59
    iput-object p3, p0, Ldst;->d:Ldss;

    .line 60
    iput-object p4, p0, Ldst;->e:Ldtb;

    .line 61
    return-void
.end method

.method public constructor <init>(Ldtl;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    sget-object v0, Ldsw;->b:Ldsw;

    iput-object v0, p0, Ldst;->a:Ldsw;

    .line 65
    iput-object p1, p0, Ldst;->f:Ldtl;

    .line 66
    iput-object v1, p0, Ldst;->b:Ldth;

    .line 67
    iput-object p2, p0, Ldst;->c:Ljava/lang/String;

    .line 68
    iput-object v1, p0, Ldst;->d:Ldss;

    .line 69
    new-instance v0, Ldtb;

    invoke-virtual {p1}, Ldtl;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldtb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ldst;->e:Ldtb;

    .line 70
    return-void
.end method


# virtual methods
.method public final a(Ldtb;)Ldst;
    .locals 4

    .prologue
    .line 169
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v0, p0, Ldst;->a:Ldsw;

    sget-object v1, Ldsw;->a:Ldsw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 171
    new-instance v0, Ldst;

    iget-object v1, p0, Ldst;->b:Ldth;

    iget-object v2, p0, Ldst;->c:Ljava/lang/String;

    iget-object v3, p0, Ldst;->d:Ldss;

    invoke-direct {v0, v1, v2, v3, p1}, Ldst;-><init>(Ldth;Ljava/lang/String;Ldss;Ldtb;)V

    return-object v0

    .line 170
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ldst;
    .locals 4

    .prologue
    .line 175
    sget-object v0, Ldsv;->a:[I

    iget-object v1, p0, Ldst;->a:Ldsw;

    invoke-virtual {v1}, Ldsw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 181
    :goto_0
    return-object p0

    .line 177
    :pswitch_0
    new-instance v0, Ldst;

    iget-object v1, p0, Ldst;->b:Ldth;

    iget-object v2, p0, Ldst;->d:Ldss;

    iget-object v3, p0, Ldst;->e:Ldtb;

    invoke-direct {v0, v1, p1, v2, v3}, Ldst;-><init>(Ldth;Ljava/lang/String;Ldss;Ldtb;)V

    move-object p0, v0

    goto :goto_0

    .line 179
    :pswitch_1
    new-instance v0, Ldst;

    iget-object v1, p0, Ldst;->f:Ldtl;

    invoke-direct {v0, v1, p1}, Ldst;-><init>(Ldtl;Ljava/lang/String;)V

    move-object p0, v0

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    if-ne p0, p1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_3
    check-cast p1, Ldst;

    .line 84
    iget-object v2, p0, Ldst;->a:Ldsw;

    iget-object v3, p1, Ldst;->a:Ldsw;

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 85
    goto :goto_0

    .line 87
    :cond_4
    iget-object v2, p0, Ldst;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 88
    iget-object v2, p1, Ldst;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_5
    iget-object v2, p0, Ldst;->c:Ljava/lang/String;

    iget-object v3, p1, Ldst;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 92
    goto :goto_0

    .line 94
    :cond_6
    iget-object v2, p0, Ldst;->d:Ldss;

    if-nez v2, :cond_7

    .line 95
    iget-object v2, p1, Ldst;->d:Ldss;

    if-eqz v2, :cond_8

    move v0, v1

    .line 96
    goto :goto_0

    .line 98
    :cond_7
    iget-object v2, p0, Ldst;->d:Ldss;

    iget-object v3, p1, Ldst;->d:Ldss;

    invoke-virtual {v2, v3}, Ldss;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 99
    goto :goto_0

    .line 101
    :cond_8
    iget-object v2, p0, Ldst;->b:Ldth;

    if-nez v2, :cond_9

    .line 102
    iget-object v2, p1, Ldst;->b:Ldth;

    if-eqz v2, :cond_a

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_9
    iget-object v2, p0, Ldst;->b:Ldth;

    iget-object v3, p1, Ldst;->b:Ldth;

    invoke-virtual {v2, v3}, Ldth;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_a
    iget-object v2, p0, Ldst;->f:Ldtl;

    if-nez v2, :cond_b

    .line 109
    iget-object v2, p1, Ldst;->f:Ldtl;

    if-eqz v2, :cond_0

    move v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_b
    iget-object v2, p0, Ldst;->f:Ldtl;

    iget-object v3, p1, Ldst;->f:Ldtl;

    invoke-virtual {v2, v3}, Ldtl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 113
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 143
    iget-object v0, p0, Ldst;->a:Ldsw;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 146
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldst;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 147
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldst;->d:Ldss;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 148
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ldst;->b:Ldth;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ldst;->f:Ldtl;

    if-nez v2, :cond_4

    .line 150
    :goto_4
    add-int/2addr v0, v1

    .line 151
    return v0

    .line 143
    :cond_0
    iget-object v0, p0, Ldst;->a:Ldsw;

    invoke-virtual {v0}, Ldsw;->hashCode()I

    move-result v0

    goto :goto_0

    .line 146
    :cond_1
    iget-object v0, p0, Ldst;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 147
    :cond_2
    iget-object v0, p0, Ldst;->d:Ldss;

    invoke-virtual {v0}, Ldss;->hashCode()I

    move-result v0

    goto :goto_2

    .line 148
    :cond_3
    iget-object v0, p0, Ldst;->b:Ldth;

    invoke-virtual {v0}, Ldth;->hashCode()I

    move-result v0

    goto :goto_3

    .line 149
    :cond_4
    iget-object v1, p0, Ldst;->f:Ldtl;

    .line 150
    invoke-virtual {v1}, Ldtl;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 156
    iget-object v0, p0, Ldst;->a:Ldsw;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldst;->b:Ldth;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldst;->c:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2b

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "CloudScreen [accessType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", screenId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 206
    iget-object v0, p0, Ldst;->a:Ldsw;

    invoke-virtual {v0}, Ldsw;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 207
    iget-object v0, p0, Ldst;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Ldst;->a:Ldsw;

    sget-object v1, Ldsw;->a:Ldsw;

    if-ne v0, v1, :cond_1

    .line 209
    iget-object v0, p0, Ldst;->b:Ldth;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 210
    iget-object v0, p0, Ldst;->e:Ldtb;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 211
    iget-object v0, p0, Ldst;->d:Ldss;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Ldst;->d:Ldss;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v0, p0, Ldst;->f:Ldtl;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    goto :goto_0
.end method
