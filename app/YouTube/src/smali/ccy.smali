.class public final Lccy;
.super Lcbx;
.source "SourceFile"


# instance fields
.field private final e:Lboi;

.field private final f:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;ILboi;Lfdw;Lfrz;)V
    .locals 7

    .prologue
    .line 42
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move v4, p5

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcbx;-><init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V

    .line 49
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lccy;->f:Lfsj;

    .line 50
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lccy;->e:Lboi;

    .line 52
    invoke-interface {p3, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcbx;->b:Landroid/view/View;

    invoke-interface {p3, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 54
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Lfsj;->a(Z)V

    .line 55
    return-void
.end method

.method private a(Lfsg;Lfjc;)Landroid/view/View;
    .locals 3

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcbx;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 61
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcbx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 62
    :cond_0
    invoke-virtual {p2}, Lfjc;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lccy;->a(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p2, Lfjc;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    iget-object v0, p2, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfjc;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v0, p2, Lfjc;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lccy;->b(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p2, Lfjc;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    iget-object v0, p2, Lfjc;->a:Lhdv;

    iget-object v0, v0, Lhdv;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfjc;->c:Ljava/lang/CharSequence;

    :cond_2
    iget-object v0, p2, Lfjc;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lccy;->c(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p2, Lfjc;->i:Lflz;

    invoke-virtual {v0}, Lflz;->b()Lfma;

    move-result-object v0

    .line 67
    iget-object v1, p2, Lfjc;->i:Lflz;

    invoke-virtual {v1}, Lflz;->a()Lfnc;

    move-result-object v1

    .line 68
    iget-object v2, p2, Lfjc;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_3

    iget-object v2, p2, Lfjc;->a:Lhdv;

    iget-object v2, v2, Lhdv;->g:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p2, Lfjc;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v2, p2, Lfjc;->d:Ljava/lang/CharSequence;

    .line 65
    invoke-virtual {p0, v0, v1, v2}, Lccy;->a(Lfma;Lfnc;Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcbx;->d:Landroid/view/View;

    iget-object v1, p0, Lccy;->f:Lfsj;

    invoke-interface {v1}, Lfsj;->a()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lccy;->e:Lboi;

    invoke-static {v1, v2, v0, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lccy;->f:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfjc;

    invoke-direct {p0, p1, p2}, Lccy;->a(Lfsg;Lfjc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfjc;

    invoke-direct {p0, p1, p2}, Lccy;->a(Lfsg;Lfjc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfjc;

    invoke-direct {p0, p1, p2}, Lccy;->a(Lfsg;Lfjc;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
