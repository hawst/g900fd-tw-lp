.class public final Lhaz;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhaz;


# instance fields
.field public b:I

.field public c:Lhog;

.field public d:Lhog;

.field public e:Lhog;

.field private f:Lhba;

.field private g:Lhba;

.field private h:Lhba;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13694
    const/4 v0, 0x0

    new-array v0, v0, [Lhaz;

    sput-object v0, Lhaz;->a:[Lhaz;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13695
    invoke-direct {p0}, Lidf;-><init>()V

    .line 13813
    const/4 v0, 0x0

    iput v0, p0, Lhaz;->b:I

    .line 13816
    iput-object v1, p0, Lhaz;->c:Lhog;

    .line 13819
    iput-object v1, p0, Lhaz;->d:Lhog;

    .line 13822
    iput-object v1, p0, Lhaz;->e:Lhog;

    .line 13825
    iput-object v1, p0, Lhaz;->f:Lhba;

    .line 13828
    iput-object v1, p0, Lhaz;->g:Lhba;

    .line 13831
    iput-object v1, p0, Lhaz;->h:Lhba;

    .line 13695
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 13904
    const/4 v0, 0x0

    .line 13905
    iget v1, p0, Lhaz;->b:I

    if-eqz v1, :cond_0

    .line 13906
    const/4 v0, 0x1

    iget v1, p0, Lhaz;->b:I

    .line 13907
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 13909
    :cond_0
    iget-object v1, p0, Lhaz;->c:Lhog;

    if-eqz v1, :cond_1

    .line 13910
    const/4 v1, 0x2

    iget-object v2, p0, Lhaz;->c:Lhog;

    .line 13911
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13913
    :cond_1
    iget-object v1, p0, Lhaz;->d:Lhog;

    if-eqz v1, :cond_2

    .line 13914
    const/4 v1, 0x3

    iget-object v2, p0, Lhaz;->d:Lhog;

    .line 13915
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13917
    :cond_2
    iget-object v1, p0, Lhaz;->e:Lhog;

    if-eqz v1, :cond_3

    .line 13918
    const/4 v1, 0x4

    iget-object v2, p0, Lhaz;->e:Lhog;

    .line 13919
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13921
    :cond_3
    iget-object v1, p0, Lhaz;->f:Lhba;

    if-eqz v1, :cond_4

    .line 13922
    const/4 v1, 0x5

    iget-object v2, p0, Lhaz;->f:Lhba;

    .line 13923
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13925
    :cond_4
    iget-object v1, p0, Lhaz;->g:Lhba;

    if-eqz v1, :cond_5

    .line 13926
    const/4 v1, 0x6

    iget-object v2, p0, Lhaz;->g:Lhba;

    .line 13927
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13929
    :cond_5
    iget-object v1, p0, Lhaz;->h:Lhba;

    if-eqz v1, :cond_6

    .line 13930
    const/4 v1, 0x7

    iget-object v2, p0, Lhaz;->h:Lhba;

    .line 13931
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13933
    :cond_6
    iget-object v1, p0, Lhaz;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13934
    iput v0, p0, Lhaz;->J:I

    .line 13935
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 13691
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhaz;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhaz;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhaz;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhaz;->b:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lhaz;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhaz;->c:Lhog;

    if-nez v0, :cond_4

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhaz;->c:Lhog;

    :cond_4
    iget-object v0, p0, Lhaz;->c:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhaz;->d:Lhog;

    if-nez v0, :cond_5

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhaz;->d:Lhog;

    :cond_5
    iget-object v0, p0, Lhaz;->d:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhaz;->e:Lhog;

    if-nez v0, :cond_6

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhaz;->e:Lhog;

    :cond_6
    iget-object v0, p0, Lhaz;->e:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhaz;->f:Lhba;

    if-nez v0, :cond_7

    new-instance v0, Lhba;

    invoke-direct {v0}, Lhba;-><init>()V

    iput-object v0, p0, Lhaz;->f:Lhba;

    :cond_7
    iget-object v0, p0, Lhaz;->f:Lhba;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhaz;->g:Lhba;

    if-nez v0, :cond_8

    new-instance v0, Lhba;

    invoke-direct {v0}, Lhba;-><init>()V

    iput-object v0, p0, Lhaz;->g:Lhba;

    :cond_8
    iget-object v0, p0, Lhaz;->g:Lhba;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lhaz;->h:Lhba;

    if-nez v0, :cond_9

    new-instance v0, Lhba;

    invoke-direct {v0}, Lhba;-><init>()V

    iput-object v0, p0, Lhaz;->h:Lhba;

    :cond_9
    iget-object v0, p0, Lhaz;->h:Lhba;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 13877
    iget v0, p0, Lhaz;->b:I

    if-eqz v0, :cond_0

    .line 13878
    const/4 v0, 0x1

    iget v1, p0, Lhaz;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 13880
    :cond_0
    iget-object v0, p0, Lhaz;->c:Lhog;

    if-eqz v0, :cond_1

    .line 13881
    const/4 v0, 0x2

    iget-object v1, p0, Lhaz;->c:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13883
    :cond_1
    iget-object v0, p0, Lhaz;->d:Lhog;

    if-eqz v0, :cond_2

    .line 13884
    const/4 v0, 0x3

    iget-object v1, p0, Lhaz;->d:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13886
    :cond_2
    iget-object v0, p0, Lhaz;->e:Lhog;

    if-eqz v0, :cond_3

    .line 13887
    const/4 v0, 0x4

    iget-object v1, p0, Lhaz;->e:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13889
    :cond_3
    iget-object v0, p0, Lhaz;->f:Lhba;

    if-eqz v0, :cond_4

    .line 13890
    const/4 v0, 0x5

    iget-object v1, p0, Lhaz;->f:Lhba;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13892
    :cond_4
    iget-object v0, p0, Lhaz;->g:Lhba;

    if-eqz v0, :cond_5

    .line 13893
    const/4 v0, 0x6

    iget-object v1, p0, Lhaz;->g:Lhba;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13895
    :cond_5
    iget-object v0, p0, Lhaz;->h:Lhba;

    if-eqz v0, :cond_6

    .line 13896
    const/4 v0, 0x7

    iget-object v1, p0, Lhaz;->h:Lhba;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 13898
    :cond_6
    iget-object v0, p0, Lhaz;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 13900
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 13848
    if-ne p1, p0, :cond_1

    .line 13858
    :cond_0
    :goto_0
    return v0

    .line 13849
    :cond_1
    instance-of v2, p1, Lhaz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 13850
    :cond_2
    check-cast p1, Lhaz;

    .line 13851
    iget v2, p0, Lhaz;->b:I

    iget v3, p1, Lhaz;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhaz;->c:Lhog;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhaz;->c:Lhog;

    if-nez v2, :cond_3

    .line 13852
    :goto_1
    iget-object v2, p0, Lhaz;->d:Lhog;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhaz;->d:Lhog;

    if-nez v2, :cond_3

    .line 13853
    :goto_2
    iget-object v2, p0, Lhaz;->e:Lhog;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhaz;->e:Lhog;

    if-nez v2, :cond_3

    .line 13854
    :goto_3
    iget-object v2, p0, Lhaz;->f:Lhba;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhaz;->f:Lhba;

    if-nez v2, :cond_3

    .line 13855
    :goto_4
    iget-object v2, p0, Lhaz;->g:Lhba;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhaz;->g:Lhba;

    if-nez v2, :cond_3

    .line 13856
    :goto_5
    iget-object v2, p0, Lhaz;->h:Lhba;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhaz;->h:Lhba;

    if-nez v2, :cond_3

    .line 13857
    :goto_6
    iget-object v2, p0, Lhaz;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhaz;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 13858
    goto :goto_0

    .line 13851
    :cond_4
    iget-object v2, p0, Lhaz;->c:Lhog;

    iget-object v3, p1, Lhaz;->c:Lhog;

    .line 13852
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhaz;->d:Lhog;

    iget-object v3, p1, Lhaz;->d:Lhog;

    .line 13853
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhaz;->e:Lhog;

    iget-object v3, p1, Lhaz;->e:Lhog;

    .line 13854
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhaz;->f:Lhba;

    iget-object v3, p1, Lhaz;->f:Lhba;

    .line 13855
    invoke-virtual {v2, v3}, Lhba;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhaz;->g:Lhba;

    iget-object v3, p1, Lhaz;->g:Lhba;

    .line 13856
    invoke-virtual {v2, v3}, Lhba;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhaz;->h:Lhba;

    iget-object v3, p1, Lhaz;->h:Lhba;

    .line 13857
    invoke-virtual {v2, v3}, Lhba;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhaz;->I:Ljava/util/List;

    iget-object v3, p1, Lhaz;->I:Ljava/util/List;

    .line 13858
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 13862
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 13864
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhaz;->b:I

    add-int/2addr v0, v2

    .line 13865
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhaz;->c:Lhog;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 13866
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhaz;->d:Lhog;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 13867
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhaz;->e:Lhog;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 13868
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhaz;->f:Lhba;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 13869
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhaz;->g:Lhba;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 13870
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhaz;->h:Lhba;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 13871
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhaz;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 13872
    return v0

    .line 13865
    :cond_0
    iget-object v0, p0, Lhaz;->c:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_0

    .line 13866
    :cond_1
    iget-object v0, p0, Lhaz;->d:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_1

    .line 13867
    :cond_2
    iget-object v0, p0, Lhaz;->e:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_2

    .line 13868
    :cond_3
    iget-object v0, p0, Lhaz;->f:Lhba;

    invoke-virtual {v0}, Lhba;->hashCode()I

    move-result v0

    goto :goto_3

    .line 13869
    :cond_4
    iget-object v0, p0, Lhaz;->g:Lhba;

    invoke-virtual {v0}, Lhba;->hashCode()I

    move-result v0

    goto :goto_4

    .line 13870
    :cond_5
    iget-object v0, p0, Lhaz;->h:Lhba;

    invoke-virtual {v0}, Lhba;->hashCode()I

    move-result v0

    goto :goto_5

    .line 13871
    :cond_6
    iget-object v1, p0, Lhaz;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_6
.end method
