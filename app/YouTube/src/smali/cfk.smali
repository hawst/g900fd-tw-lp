.class public final Lcfk;
.super Lfsa;
.source "SourceFile"


# instance fields
.field private final a:Leyp;

.field private final b:Lfhz;

.field private final c:Landroid/content/res/Resources;

.field private final d:Landroid/view/LayoutInflater;

.field private e:Lfkx;

.field private f:Landroid/view/ViewGroup;

.field private g:Lcfm;

.field private h:Lcfm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lfdw;Lfrz;)V
    .locals 4

    .prologue
    .line 59
    invoke-direct {p0, p4, p5}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 60
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcfk;->a:Leyp;

    .line 61
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcfk;->b:Lfhz;

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcfk;->c:Landroid/content/res/Resources;

    .line 63
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcfk;->d:Landroid/view/LayoutInflater;

    .line 64
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcfk;->f:Landroid/view/ViewGroup;

    .line 65
    iget-object v0, p0, Lcfk;->f:Landroid/view/ViewGroup;

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    return-void
.end method

.method private a(Lcfm;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p1, Lcfm;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcfk;->e:Lfkx;

    iget-object v3, v2, Lfkx;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    iget-object v3, v2, Lfkx;->a:Lhnz;

    iget-object v3, v3, Lhnz;->b:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfkx;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v2, v2, Lfkx;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v0, p1, Lcfm;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcfk;->e:Lfkx;

    .line 97
    iget-object v3, v2, Lfkx;->c:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    iget-object v3, v2, Lfkx;->a:Lhnz;

    iget-object v3, v3, Lhnz;->c:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfkx;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v2, v2, Lfkx;->c:Ljava/lang/CharSequence;

    .line 95
    invoke-static {v0, v2}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p1, Lcfm;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcfk;->e:Lfkx;

    .line 100
    iget-object v3, v2, Lfkx;->d:Ljava/lang/CharSequence;

    if-nez v3, :cond_2

    iget-object v3, v2, Lfkx;->a:Lhnz;

    iget-object v3, v3, Lhnz;->d:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfkx;->d:Ljava/lang/CharSequence;

    :cond_2
    iget-object v2, v2, Lfkx;->d:Ljava/lang/CharSequence;

    .line 98
    invoke-static {v0, v2}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 101
    iget-object v4, p1, Lcfm;->e:Landroid/widget/TextView;

    iget-object v2, p0, Lcfk;->e:Lfkx;

    .line 103
    iget-object v0, v2, Lfkx;->e:[Ljava/lang/CharSequence;

    if-nez v0, :cond_3

    iget-object v0, v2, Lfkx;->a:Lhnz;

    iget-object v0, v0, Lhnz;->f:[Lhgz;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lfkx;->a:Lhnz;

    iget-object v0, v0, Lhnz;->f:[Lhgz;

    array-length v0, v0

    if-lez v0, :cond_3

    iget-object v0, v2, Lfkx;->a:Lhnz;

    iget-object v0, v0, Lhnz;->f:[Lhgz;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    iput-object v0, v2, Lfkx;->e:[Ljava/lang/CharSequence;

    move v0, v1

    :goto_0
    iget-object v3, v2, Lfkx;->a:Lhnz;

    iget-object v3, v3, Lhnz;->f:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    iget-object v3, v2, Lfkx;->e:[Ljava/lang/CharSequence;

    iget-object v5, v2, Lfkx;->a:Lhnz;

    iget-object v5, v5, Lhnz;->f:[Lhgz;

    aget-object v5, v5, v0

    invoke-static {v5}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v5

    aput-object v5, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v5, v2, Lfkx;->e:[Ljava/lang/CharSequence;

    const/4 v2, 0x0

    if-eqz v5, :cond_5

    array-length v0, v5

    if-lez v0, :cond_5

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    array-length v7, v5

    move v3, v1

    :goto_1
    if-ge v3, v7, :cond_5

    aget-object v8, v5, v3

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v9, Landroid/text/style/BulletSpan;

    const/16 v10, 0x14

    invoke-direct {v9, v10}, Landroid/text/style/BulletSpan;-><init>(I)V

    invoke-interface {v8}, Ljava/lang/CharSequence;->length()I

    move-result v8

    invoke-virtual {v0, v9, v1, v8, v1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    if-nez v2, :cond_4

    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v0

    goto :goto_1

    :cond_4
    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/CharSequence;

    aput-object v2, v8, v1

    const/4 v2, 0x1

    aput-object v6, v8, v2

    const/4 v2, 0x2

    aput-object v0, v8, v2

    invoke-static {v8}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_2

    .line 101
    :cond_5
    invoke-static {v4, v2}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 104
    iget-object v0, p1, Lcfm;->f:Landroid/widget/TextView;

    const-string v2, "line.separator"

    .line 107
    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcfk;->e:Lfkx;

    iget-object v4, p0, Lcfk;->b:Lfhz;

    .line 108
    invoke-virtual {v3, v4}, Lfkx;->a(Lfhz;)[Ljava/lang/CharSequence;

    move-result-object v3

    .line 106
    invoke-static {v2, v3}, Lfvo;->a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 104
    invoke-static {v0, v2}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcfk;->e:Lfkx;

    invoke-virtual {v0}, Lfkx;->a()Lfnc;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcfk;->e:Lfkx;

    invoke-virtual {v0}, Lfkx;->a()Lfnc;

    move-result-object v0

    invoke-virtual {v0}, Lfnc;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 112
    iget-object v0, p0, Lcfk;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    .line 113
    iget-object v2, p0, Lcfk;->e:Lfkx;

    .line 114
    invoke-virtual {v2}, Lfkx;->a()Lfnc;

    move-result-object v2

    invoke-virtual {v2, v0}, Lfnc;->a(I)Lfnb;

    move-result-object v0

    .line 115
    iget-object v2, p0, Lcfk;->a:Leyp;

    .line 117
    iget-object v0, v0, Lfnb;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcfm;->g:Landroid/widget/ImageView;

    .line 115
    invoke-static {v2, v0, v3}, Leyi;->a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;)V

    .line 119
    iget-object v0, p1, Lcfm;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 124
    :goto_3
    iget-object v0, p0, Lcfk;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 125
    iget-object v0, p0, Lcfk;->f:Landroid/view/ViewGroup;

    iget-object v1, p1, Lcfm;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 126
    iget-object v0, p0, Lcfk;->f:Landroid/view/ViewGroup;

    return-object v0

    .line 121
    :cond_6
    iget-object v0, p1, Lcfm;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    :cond_7
    move-object v0, v2

    goto :goto_2
.end method

.method private a(Lfkx;)Landroid/view/View;
    .locals 5

    .prologue
    const v4, 0x7f040088

    const/4 v3, 0x0

    .line 71
    iput-object p1, p0, Lcfk;->e:Lfkx;

    .line 72
    iget-object v0, p0, Lcfk;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 73
    iget-object v0, p0, Lcfk;->g:Lcfm;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcfm;

    iget-object v1, p0, Lcfk;->d:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcfk;->f:Landroid/view/ViewGroup;

    .line 75
    invoke-virtual {v1, v4, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcfm;-><init>(Lcfk;Landroid/view/View;)V

    iput-object v0, p0, Lcfk;->g:Lcfm;

    .line 80
    :cond_0
    iget-object v0, p0, Lcfk;->g:Lcfm;

    invoke-direct {p0, v0}, Lcfk;->a(Lcfm;)Landroid/view/View;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    .line 82
    :cond_1
    iget-object v0, p0, Lcfk;->h:Lcfm;

    if-nez v0, :cond_2

    .line 83
    new-instance v0, Lcfm;

    iget-object v1, p0, Lcfk;->d:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcfk;->f:Landroid/view/ViewGroup;

    .line 84
    invoke-virtual {v1, v4, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcfm;-><init>(Lcfk;Landroid/view/View;)V

    iput-object v0, p0, Lcfk;->h:Lcfm;

    .line 89
    :cond_2
    iget-object v0, p0, Lcfk;->h:Lcfm;

    invoke-direct {p0, v0}, Lcfk;->a(Lcfm;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lfkx;

    invoke-direct {p0, p2}, Lcfk;->a(Lfkx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lfkx;

    invoke-direct {p0, p2}, Lcfk;->a(Lfkx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
