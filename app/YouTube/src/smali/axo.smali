.class public final Laxo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/SharedPreferences;

.field final b:Landroid/content/res/Resources;

.field final c:Laxq;

.field private final d:Ldwv;

.field private final e:Z


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;Landroid/content/res/Resources;Ldwv;ZLaxq;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Laxo;->a:Landroid/content/SharedPreferences;

    .line 54
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Laxo;->b:Landroid/content/res/Resources;

    .line 55
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwv;

    iput-object v0, p0, Laxo;->d:Ldwv;

    .line 56
    iput-boolean p4, p0, Laxo;->e:Z

    .line 57
    iput-object p5, p0, Laxo;->c:Laxq;

    .line 58
    return-void
.end method

.method private a([Ljava/lang/CharSequence;)I
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Laxo;->a:Landroid/content/SharedPreferences;

    const-string v1, "autocast_setting_message"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Laxo;->a:Landroid/content/SharedPreferences;

    const-string v1, "autocast_setting_message"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 113
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 114
    aget-object v2, p1, v0

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    :goto_1
    return v0

    .line 113
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/app/Dialog;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 61
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Laxo;->d:Ldwv;

    invoke-interface {v0}, Ldwv;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 64
    invoke-static {}, Laxr;->values()[Laxr;

    move-result-object v0

    array-length v0, v0

    .line 65
    iget-boolean v1, p0, Laxo;->e:Z

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    move v1, v0

    .line 68
    :goto_0
    new-array v5, v1, [Ljava/lang/CharSequence;

    move v2, v3

    .line 69
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 70
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    .line 69
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 65
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 73
    :cond_1
    iget-boolean v0, p0, Laxo;->e:Z

    if-eqz v0, :cond_2

    .line 74
    invoke-static {}, Laxr;->values()[Laxr;

    move-result-object v0

    .line 75
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v3

    if-ge v2, v1, :cond_2

    .line 76
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v3

    aget-object v6, v0, v3

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v6, v7}, Laxr;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    .line 75
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 80
    :cond_2
    invoke-direct {p0, v5}, Laxo;->a([Ljava/lang/CharSequence;)I

    move-result v0

    .line 82
    new-instance v1, Leyv;

    invoke-direct {v1, p1}, Leyv;-><init>(Landroid/content/Context;)V

    new-instance v2, Laxp;

    invoke-direct {v2, p0, v5, v4}, Laxp;-><init>(Laxo;[Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 83
    invoke-virtual {v1, v5, v0, v2}, Leyv;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Laxo;->b:Landroid/content/res/Resources;

    const v2, 0x7f090073

    .line 97
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900a2

    const/4 v2, 0x0

    .line 98
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 100
    return-object v0
.end method
