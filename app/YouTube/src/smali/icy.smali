.class public final Licy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field b:I

.field private final c:[B

.field private final d:Ljava/io/OutputStream;


# direct methods
.method constructor <init>([BII)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Licy;->d:Ljava/io/OutputStream;

    .line 66
    iput-object p1, p0, Licy;->c:[B

    .line 67
    iput p2, p0, Licy;->b:I

    .line 68
    add-int v0, p2, p3

    iput v0, p0, Licy;->a:I

    .line 69
    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 626
    if-ltz p0, :cond_0

    .line 627
    invoke-static {p0}, Licy;->e(I)I

    move-result v0

    .line 630
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 664
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 665
    array-length v1, v0

    invoke-static {v1}, Licy;->e(I)I

    move-result v1

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v0, v1

    return v0

    .line 668
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "UTF-8 not supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 916
    :goto_0
    const-wide/16 v0, -0x80

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 917
    long-to-int v0, p1

    invoke-direct {p0, v0}, Licy;->c(I)V

    .line 918
    return-void

    .line 920
    :cond_0
    long-to-int v0, p1

    and-int/lit8 v0, v0, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Licy;->c(I)V

    .line 921
    const/4 v0, 0x7

    ushr-long/2addr p1, v0

    goto :goto_0
.end method

.method private a([B)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 839
    array-length v0, p1

    iget v1, p0, Licy;->a:I

    iget v2, p0, Licy;->b:I

    sub-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    iget-object v1, p0, Licy;->c:[B

    iget v2, p0, Licy;->b:I

    invoke-static {p1, v4, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Licy;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Licy;->b:I

    return-void

    :cond_0
    iget v1, p0, Licy;->a:I

    iget v2, p0, Licy;->b:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Licy;->c:[B

    iget v3, p0, Licy;->b:I

    invoke-static {p1, v4, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v2, v1, 0x0

    sub-int/2addr v0, v1

    iget v0, p0, Licy;->a:I

    iput v0, p0, Licy;->b:I

    new-instance v0, Lefe;

    invoke-direct {v0}, Lefe;-><init>()V

    throw v0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 881
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lidb;->a(II)I

    move-result v0

    invoke-static {v0}, Licy;->e(I)I

    move-result v0

    return v0
.end method

.method public static b(II)I
    .locals 2

    .prologue
    .line 437
    invoke-static {p0}, Licy;->b(I)I

    move-result v0

    invoke-static {p1}, Licy;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(IJ)I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 429
    invoke-static {p0}, Licy;->b(I)I

    move-result v1

    const-wide/16 v2, -0x80

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const-wide/16 v2, -0x4000

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-wide/32 v2, -0x200000

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const-wide/32 v2, -0x10000000

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const-wide v2, -0x800000000L

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    const/4 v0, 0x5

    goto :goto_0

    :cond_4
    const-wide v2, -0x40000000000L

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_5

    const/4 v0, 0x6

    goto :goto_0

    :cond_5
    const-wide/high16 v2, -0x2000000000000L

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_6

    const/4 v0, 0x7

    goto :goto_0

    :cond_6
    const-wide/high16 v2, -0x100000000000000L

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_7

    const/16 v0, 0x8

    goto :goto_0

    :cond_7
    const-wide/high16 v2, -0x8000000000000000L

    and-long/2addr v2, p1

    cmp-long v0, v2, v4

    if-nez v0, :cond_8

    const/16 v0, 0x9

    goto :goto_0

    :cond_8
    const/16 v0, 0xa

    goto :goto_0
.end method

.method public static b(ILicw;)I
    .locals 3

    .prologue
    .line 500
    invoke-static {p0}, Licy;->b(I)I

    move-result v0

    iget-object v1, p1, Licw;->a:[B

    array-length v1, v1

    invoke-static {v1}, Licy;->e(I)I

    move-result v1

    iget-object v2, p1, Licw;->a:[B

    array-length v2, v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILida;)I
    .locals 3

    .prologue
    .line 491
    invoke-static {p0}, Licy;->b(I)I

    move-result v0

    invoke-virtual {p1}, Lida;->b()I

    move-result v1

    invoke-static {v1}, Licy;->e(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public static b(ILjava/lang/String;)I
    .locals 2

    .prologue
    .line 473
    invoke-static {p0}, Licy;->b(I)I

    move-result v0

    invoke-static {p1}, Licy;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 834
    int-to-byte v0, p1

    iget v1, p0, Licy;->b:I

    iget v2, p0, Licy;->a:I

    if-ne v1, v2, :cond_0

    new-instance v0, Lefe;

    invoke-direct {v0}, Lefe;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Licy;->c:[B

    iget v2, p0, Licy;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Licy;->b:I

    aput-byte v0, v1, v2

    .line 835
    return-void
.end method

.method private c(II)V
    .locals 1

    .prologue
    .line 876
    invoke-static {p1, p2}, Lidb;->a(II)I

    move-result v0

    invoke-direct {p0, v0}, Licy;->d(I)V

    .line 877
    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 890
    :goto_0
    and-int/lit8 v0, p1, -0x80

    if-nez v0, :cond_0

    .line 891
    invoke-direct {p0, p1}, Licy;->c(I)V

    .line 892
    return-void

    .line 894
    :cond_0
    and-int/lit8 v0, p1, 0x7f

    or-int/lit16 v0, v0, 0x80

    invoke-direct {p0, v0}, Licy;->c(I)V

    .line 895
    ushr-int/lit8 p1, p1, 0x7

    goto :goto_0
.end method

.method private static e(I)I
    .locals 1

    .prologue
    .line 906
    and-int/lit8 v0, p0, -0x80

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 910
    :goto_0
    return v0

    .line 907
    :cond_0
    and-int/lit16 v0, p0, -0x4000

    if-nez v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    .line 908
    :cond_1
    const/high16 v0, -0x200000

    and-int/2addr v0, p0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    .line 909
    :cond_2
    const/high16 v0, -0x10000000

    and-int/2addr v0, p0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    .line 910
    :cond_3
    const/4 v0, 0x5

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Licy;->c(II)V

    .line 151
    if-ltz p2, :cond_0

    invoke-direct {p0, p2}, Licy;->d(I)V

    .line 152
    :goto_0
    return-void

    .line 151
    :cond_0
    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Licy;->a(J)V

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Licy;->c(II)V

    .line 144
    invoke-direct {p0, p2, p3}, Licy;->a(J)V

    .line 145
    return-void
.end method

.method public final a(ILicw;)V
    .locals 2

    .prologue
    .line 200
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Licy;->c(II)V

    .line 201
    invoke-virtual {p2}, Licw;->a()[B

    move-result-object v0

    array-length v1, v0

    invoke-direct {p0, v1}, Licy;->d(I)V

    invoke-direct {p0, v0}, Licy;->a([B)V

    .line 202
    return-void
.end method

.method public final a(ILida;)V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Licy;->c(II)V

    .line 194
    invoke-virtual {p2}, Lida;->a()I

    move-result v0

    invoke-direct {p0, v0}, Licy;->d(I)V

    invoke-virtual {p2, p0}, Lida;->a(Licy;)V

    .line 195
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 178
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Licy;->c(II)V

    .line 179
    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v1, v0

    invoke-direct {p0, v1}, Licy;->d(I)V

    invoke-direct {p0, v0}, Licy;->a([B)V

    .line 180
    return-void
.end method

.method public final a(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 171
    invoke-direct {p0, p1, v0}, Licy;->c(II)V

    .line 172
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v0}, Licy;->c(I)V

    .line 173
    return-void
.end method
