.class public final Lbkh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgot;


# instance fields
.field final a:Lgix;

.field final b:Lgng;

.field private final c:Lgot;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Lglm;


# direct methods
.method public constructor <init>(Lgot;Ljava/util/concurrent/Executor;Levn;Lgix;Lglm;Lgng;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgot;

    iput-object v0, p0, Lbkh;->c:Lgot;

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbkh;->d:Ljava/util/concurrent/Executor;

    .line 42
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbkh;->a:Lgix;

    .line 44
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglm;

    iput-object v0, p0, Lbkh;->e:Lglm;

    .line 45
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgng;

    iput-object v0, p0, Lbkh;->b:Lgng;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Lgpa;Leuc;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lbkh;->c:Lgot;

    invoke-interface {v0, p1, p2}, Lgot;->a(Lgpa;Leuc;)V

    .line 89
    return-void
.end method

.method public final a(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lbkh;->e:Lglm;

    invoke-interface {v0}, Lglm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lbkh;->d:Ljava/util/concurrent/Executor;

    new-instance v1, Lbki;

    invoke-direct {v1, p0, p1, p2}, Lbki;-><init>(Lbkh;Ljava/lang/String;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lbkh;->c:Lgot;

    invoke-interface {v0, p1, p2}, Lgot;->a(Ljava/lang/String;Leuc;)V

    goto :goto_0
.end method

.method public final b(Lgpa;Leuc;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbkh;->c:Lgot;

    invoke-interface {v0, p1, p2}, Lgot;->b(Lgpa;Leuc;)V

    .line 95
    return-void
.end method
