.class public final Lfvo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, " \u00b7 "

    sput-object v0, Lfvo;->a:Ljava/lang/CharSequence;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 209
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    .line 210
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 211
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 212
    if-nez v0, :cond_2

    .line 213
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 214
    const/high16 v2, 0x10000

    .line 215
    invoke-virtual {v0, p1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 216
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 217
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    const-string v0, "keep_history"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 221
    invoke-virtual {p1, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    :cond_1
    :goto_0
    return-object p1

    .line 225
    :cond_2
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    const-string v0, "keep_history"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public static a(Lhgz;)Landroid/text/Spanned;
    .locals 2

    .prologue
    .line 46
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lfvo;->a(Lhgz;Lfhz;Z)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lhgz;I)Landroid/text/Spanned;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    const/4 v0, 0x1

    invoke-static {p0, v0, v1, v1}, Lfvo;->a(Lhgz;ILfhz;Lfvs;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lhgz;ILfhz;Lfvs;)Landroid/text/Spanned;
    .locals 11

    .prologue
    const/16 v10, 0x21

    const/4 v1, 0x0

    .line 88
    if-nez p0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 140
    :goto_0
    return-object v0

    .line 92
    :cond_0
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 96
    iget-object v7, p0, Lhgz;->b:[Lhwo;

    array-length v8, v7

    move v6, v1

    move v0, v1

    move v3, v1

    :goto_1
    if-ge v6, v8, :cond_5

    aget-object v9, v7, v6

    .line 97
    iget-object v2, v9, Lhwo;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 98
    iget-object v2, v9, Lhwo;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    .line 102
    iget-object v0, v9, Lhwo;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 104
    iget-boolean v0, v9, Lhwo;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iget-boolean v5, v9, Lhwo;->d:Z

    if-eqz v5, :cond_4

    const/4 v5, 0x2

    :goto_3
    or-int/2addr v0, v5

    .line 106
    if-eqz v0, :cond_1

    .line 108
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v4, v5, v3, v2, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 111
    :cond_1
    if-eqz p3, :cond_2

    if-eqz p2, :cond_2

    iget-object v0, v9, Lhwo;->e:Lhog;

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, v9, Lhwo;->e:Lhog;

    .line 115
    invoke-interface {p3, p2, v0}, Lfvs;->a(Lfhz;Lhog;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    .line 114
    invoke-virtual {v4, v0, v3, v2, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_2
    move v0, v2

    .line 96
    :goto_4
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v2

    goto :goto_1

    :cond_3
    move v0, v1

    .line 104
    goto :goto_2

    :cond_4
    move v5, v1

    goto :goto_3

    .line 124
    :cond_5
    if-eqz p1, :cond_6

    .line 125
    invoke-static {v4, p1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 127
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v2, Landroid/text/style/URLSpan;

    invoke-virtual {v4, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 128
    array-length v2, v0

    :goto_5
    if-ge v1, v2, :cond_6

    aget-object v3, v0, v1

    .line 129
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 130
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 131
    invoke-virtual {v4, v3}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 132
    new-instance v7, Lfvp;

    .line 133
    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v7, v3}, Lfvp;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v4, v7, v5, v6, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 128
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    move-object v0, v4

    .line 140
    goto/16 :goto_0

    :cond_7
    move v2, v3

    goto :goto_4
.end method

.method public static a(Lhgz;Lfhz;Z)Landroid/text/Spanned;
    .locals 2

    .prologue
    .line 66
    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v1, p1, v0}, Lfvo;->a(Lhgz;ILfhz;Lfvs;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0

    .line 70
    :cond_0
    invoke-static {p2}, Lfvq;->a(Z)Lfvs;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(J)Lhgz;
    .locals 4

    .prologue
    .line 154
    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    .line 155
    new-instance v1, Lhwo;

    invoke-direct {v1}, Lhwo;-><init>()V

    .line 156
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lhwo;->b:Ljava/lang/String;

    .line 157
    const/4 v2, 0x1

    new-array v2, v2, [Lhwo;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    iput-object v2, v0, Lhgz;->b:[Lhwo;

    .line 158
    return-object v0
.end method

.method public static varargs a([Ljava/lang/String;)Lhgz;
    .locals 6

    .prologue
    .line 162
    new-instance v1, Lhgz;

    invoke-direct {v1}, Lhgz;-><init>()V

    .line 163
    array-length v2, p0

    .line 164
    new-array v3, v2, [Lhwo;

    .line 165
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 166
    new-instance v4, Lhwo;

    invoke-direct {v4}, Lhwo;-><init>()V

    .line 167
    aget-object v5, p0, v0

    iput-object v5, v4, Lhwo;->b:Ljava/lang/String;

    .line 168
    aput-object v4, v3, v0

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_0
    iput-object v3, v1, Lhgz;->b:[Lhwo;

    .line 171
    return-object v1
.end method

.method public static a()Lhwo;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lhwo;

    invoke-direct {v0}, Lhwo;-><init>()V

    .line 149
    const/4 v1, 0x1

    iput-boolean v1, v0, Lhwo;->c:Z

    .line 150
    return-object v0
.end method

.method public static varargs a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 182
    const-string v1, ""

    .line 183
    if-eqz p1, :cond_2

    array-length v0, p1

    if-lez v0, :cond_2

    .line 184
    if-nez p0, :cond_0

    sget-object p0, Lfvo;->a:Ljava/lang/CharSequence;

    .line 186
    :cond_0
    array-length v4, p1

    move v2, v3

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v0, p1, v2

    .line 187
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 188
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 186
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 193
    :cond_1
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/CharSequence;

    aput-object v1, v5, v3

    const/4 v1, 0x1

    aput-object p0, v5, v1

    const/4 v1, 0x2

    aput-object v0, v5, v1

    invoke-static {v5}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 197
    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static varargs a([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lfvo;->a(Ljava/lang/CharSequence;[Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method
