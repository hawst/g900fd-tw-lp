.class public Lbqr;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;

.field private final b:Landroid/view/animation/Animation;

.field private c:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lbqr;->b:Landroid/view/animation/Animation;

    .line 26
    iget-object v0, p0, Lbqr;->b:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbqr;->a:Ljava/util/List;

    .line 42
    return-void
.end method

.method static synthetic a(Lbqr;)Ljava/util/List;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lbqr;->a:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lbqr;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lbqr;->b:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lbqr;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbqr;->setAnimation(Landroid/view/animation/Animation;)V

    .line 83
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbqr;->setVisibility(I)V

    .line 84
    return-void
.end method

.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 51
    const v0, 0x7f08020a

    invoke-virtual {p0, v0}, Lbqr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbqr;->c:Landroid/widget/Button;

    .line 52
    iget-object v0, p0, Lbqr;->c:Landroid/widget/Button;

    new-instance v1, Lbqs;

    invoke-direct {v1, p0}, Lbqs;-><init>(Lbqr;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    new-instance v0, Lbqt;

    invoke-direct {v0, p0}, Lbqt;-><init>(Lbqr;)V

    invoke-virtual {p0, v0}, Lbqr;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    return-void
.end method
