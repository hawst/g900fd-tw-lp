.class public final Lgsd;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# instance fields
.field public final a:Landroid/graphics/Paint;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field private final e:I

.field private final f:I

.field private final g:F

.field private final h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint$FontMetricsInt;

.field private final j:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;F)V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lgsd;->b:Ljava/lang/String;

    .line 50
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lgsd;->j:Landroid/graphics/RectF;

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 62
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lgsd;->h:Landroid/graphics/Paint;

    .line 63
    iget-object v1, p0, Lgsd;->h:Landroid/graphics/Paint;

    const v2, 0x7f070075

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 64
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lgsd;->a:Landroid/graphics/Paint;

    .line 65
    iget-object v1, p0, Lgsd;->a:Landroid/graphics/Paint;

    const v2, 0x7f070076

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 66
    iget-object v1, p0, Lgsd;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 67
    iget-object v1, p0, Lgsd;->a:Landroid/graphics/Paint;

    const v2, 0x7f0a0045

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v2, p2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 68
    iget-object v1, p0, Lgsd;->a:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    iput-object v1, p0, Lgsd;->i:Landroid/graphics/Paint$FontMetricsInt;

    .line 69
    const v1, 0x7f0a0047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, p0, Lgsd;->e:I

    .line 70
    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    iput v1, p0, Lgsd;->f:I

    .line 71
    const v1, 0x7f0a0046

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    mul-float/2addr v0, p2

    iput v0, p0, Lgsd;->g:F

    .line 72
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 148
    invoke-virtual {p0}, Lgsd;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 150
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    .line 152
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v6

    .line 154
    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 156
    iget-object v0, p0, Lgsd;->j:Landroid/graphics/RectF;

    int-to-float v3, v1

    iget v4, p0, Lgsd;->f:I

    sub-int v4, v2, v4

    int-to-float v4, v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 157
    iget-object v0, p0, Lgsd;->j:Landroid/graphics/RectF;

    iget v3, p0, Lgsd;->g:F

    iget v4, p0, Lgsd;->g:F

    iget-object v5, p0, Lgsd;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 159
    iget-object v0, p0, Lgsd;->b:Ljava/lang/String;

    iget v3, p0, Lgsd;->e:I

    int-to-float v3, v3

    iget v4, p0, Lgsd;->e:I

    iget-object v5, p0, Lgsd;->i:Landroid/graphics/Paint$FontMetricsInt;

    iget v5, v5, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lgsd;->i:Landroid/graphics/Paint$FontMetricsInt;

    iget v5, v5, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lgsd;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 166
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 167
    const/4 v0, 0x0

    iget v3, p0, Lgsd;->f:I

    sub-int v3, v2, v3

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 168
    div-int/lit8 v0, v1, 0x2

    iget v1, p0, Lgsd;->d:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lgsd;->f:I

    sub-int v1, v2, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 169
    const/high16 v0, 0x42340000    # 45.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    .line 170
    iget v0, p0, Lgsd;->f:I

    iget v1, p0, Lgsd;->f:I

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v3, v0, v1

    .line 171
    neg-float v1, v3

    neg-float v2, v3

    iget-object v5, p0, Lgsd;->h:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 172
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 173
    return-void
.end method

.method public final getAlpha()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lgsd;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 139
    iget v0, p0, Lgsd;->e:I

    mul-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lgsd;->i:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->leading:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lgsd;->i:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lgsd;->i:Landroid/graphics/Paint$FontMetricsInt;

    iget v1, v1, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    add-int/2addr v0, v1

    iget v1, p0, Lgsd;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 134
    iget v0, p0, Lgsd;->e:I

    mul-int/lit8 v0, v0, 0x2

    iget v1, p0, Lgsd;->c:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 194
    const/4 v0, -0x3

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lgsd;->a:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 178
    iget-object v0, p0, Lgsd;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 179
    invoke-virtual {p0}, Lgsd;->invalidateSelf()V

    .line 180
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 190
    return-void
.end method
