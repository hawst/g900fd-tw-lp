.class public final Lcwo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcnj;

.field private final c:Lcor;

.field private final d:Lcpl;

.field private final e:Lcpv;

.field private final f:Lcqd;

.field private final g:Ldlo;

.field private final h:Lfoy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcwp;

    invoke-direct {v0}, Lcwp;-><init>()V

    sput-object v0, Lcwo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcwo;->a:Ljava/lang/String;

    .line 154
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcnj;

    iput-object v0, p0, Lcwo;->b:Lcnj;

    .line 155
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcor;

    iput-object v0, p0, Lcwo;->c:Lcor;

    .line 156
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcpl;

    iput-object v0, p0, Lcwo;->d:Lcpl;

    .line 157
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcpv;

    iput-object v0, p0, Lcwo;->e:Lcpv;

    .line 158
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcqd;

    iput-object v0, p0, Lcwo;->f:Lcqd;

    .line 159
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ldlo;

    iput-object v0, p0, Lcwo;->g:Ldlo;

    .line 160
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lfoy;

    iput-object v0, p0, Lcwo;->h:Lfoy;

    .line 161
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcnj;Lcor;Lcpl;Lcpv;Lcqd;Ldlo;Lfoy;)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p1, p0, Lcwo;->a:Ljava/lang/String;

    .line 142
    iput-object p2, p0, Lcwo;->b:Lcnj;

    .line 143
    iput-object p3, p0, Lcwo;->c:Lcor;

    .line 144
    iput-object p4, p0, Lcwo;->d:Lcpl;

    .line 145
    iput-object p5, p0, Lcwo;->e:Lcpv;

    .line 146
    iput-object p6, p0, Lcwo;->f:Lcqd;

    .line 147
    iput-object p7, p0, Lcwo;->g:Ldlo;

    .line 148
    iput-object p8, p0, Lcwo;->h:Lfoy;

    .line 149
    return-void
.end method

.method static synthetic a(Lcwo;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcwo;)Lfoy;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->h:Lfoy;

    return-object v0
.end method

.method static synthetic c(Lcwo;)Lcnj;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->b:Lcnj;

    return-object v0
.end method

.method static synthetic d(Lcwo;)Lcor;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->c:Lcor;

    return-object v0
.end method

.method static synthetic e(Lcwo;)Lcpl;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->d:Lcpl;

    return-object v0
.end method

.method static synthetic f(Lcwo;)Lcpv;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->e:Lcpv;

    return-object v0
.end method

.method static synthetic g(Lcwo;)Lcqd;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->f:Lcqd;

    return-object v0
.end method

.method static synthetic h(Lcwo;)Ldlo;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcwo;->g:Ldlo;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 177
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "PlaybackClientManagerState { videoId=%s }"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcwo;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 165
    iget-object v0, p0, Lcwo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcwo;->b:Lcnj;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 167
    iget-object v0, p0, Lcwo;->c:Lcor;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 168
    iget-object v0, p0, Lcwo;->d:Lcpl;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 169
    iget-object v0, p0, Lcwo;->e:Lcpv;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 170
    iget-object v0, p0, Lcwo;->f:Lcqd;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 171
    iget-object v0, p0, Lcwo;->g:Ldlo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 172
    iget-object v0, p0, Lcwo;->h:Lfoy;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 173
    return-void
.end method
