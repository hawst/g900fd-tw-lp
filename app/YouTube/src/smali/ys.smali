.class public final Lys;
.super Likv;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "elst"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 57
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lys;->a:Ljava/util/List;

    .line 62
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lys;->c(Ljava/nio/ByteBuffer;)J

    .line 87
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    invoke-static {v0, v1}, La;->d(J)I

    move-result v1

    .line 88
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lys;->a:Ljava/util/List;

    .line 89
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 90
    iget-object v2, p0, Lys;->a:Ljava/util/List;

    new-instance v3, Lyt;

    invoke-direct {v3, p0, p1}, Lyt;-><init>(Lys;Ljava/nio/ByteBuffer;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lys;->d(Ljava/nio/ByteBuffer;)V

    .line 98
    iget-object v0, p0, Lys;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 99
    iget-object v0, p0, Lys;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyt;

    .line 100
    iget-object v2, v0, Lyt;->d:Lys;

    iget v2, v2, Likv;->o:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget-wide v2, v0, Lyt;->a:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    iget-wide v2, v0, Lyt;->b:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    :goto_1
    iget-wide v2, v0, Lyt;->c:D

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;D)V

    goto :goto_0

    :cond_0
    iget-wide v2, v0, Lyt;->a:J

    invoke-static {v2, v3}, La;->d(J)I

    move-result v2

    int-to-long v2, v2

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    iget-wide v2, v0, Lyt;->b:J

    invoke-static {v2, v3}, La;->d(J)I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 102
    :cond_1
    return-void
.end method

.method protected final d_()J
    .locals 4

    .prologue
    const-wide/16 v2, 0x8

    .line 74
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 76
    iget-object v0, p0, Lys;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x14

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 81
    :goto_0
    return-wide v0

    .line 78
    :cond_0
    iget-object v0, p0, Lys;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0xc

    int-to-long v0, v0

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 106
    const-string v0, "EditListBox{entries="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lys;->a:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
