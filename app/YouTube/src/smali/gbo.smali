.class public final Lgbo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgjg;
.implements Ljava/io/Serializable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public c:Lgbn;

.field public d:Ljava/lang/String;

.field public e:Lgcd;

.field public f:Ljava/lang/String;

.field public g:Ljava/util/Date;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgbo;->a:Ljava/lang/String;

    .line 242
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, La;->D(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lgbo;->b:Landroid/net/Uri;

    .line 243
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgbn;

    iput-object v0, p0, Lgbo;->c:Lgbn;

    .line 244
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgbo;->d:Ljava/lang/String;

    .line 245
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iput-object v0, p0, Lgbo;->e:Lgcd;

    .line 246
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgbo;->f:Ljava/lang/String;

    .line 247
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lgbo;->g:Ljava/util/Date;

    .line 248
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgbo;->h:Ljava/lang/String;

    .line 249
    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lgbo;->a()Lgbm;

    move-result-object v0

    return-object v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lgbo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lgbo;->b:Landroid/net/Uri;

    invoke-static {v0}, La;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 232
    iget-object v0, p0, Lgbo;->c:Lgbn;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lgbo;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 234
    iget-object v0, p0, Lgbo;->e:Lgcd;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 235
    iget-object v0, p0, Lgbo;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 236
    iget-object v0, p0, Lgbo;->g:Ljava/util/Date;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lgbo;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 238
    return-void
.end method


# virtual methods
.method public final a()Lgbm;
    .locals 9

    .prologue
    .line 214
    new-instance v0, Lgbm;

    iget-object v1, p0, Lgbo;->a:Ljava/lang/String;

    iget-object v2, p0, Lgbo;->b:Landroid/net/Uri;

    iget-object v3, p0, Lgbo;->c:Lgbn;

    iget-object v4, p0, Lgbo;->d:Ljava/lang/String;

    iget-object v5, p0, Lgbo;->e:Lgcd;

    iget-object v6, p0, Lgbo;->f:Ljava/lang/String;

    iget-object v7, p0, Lgbo;->g:Ljava/util/Date;

    iget-object v8, p0, Lgbo;->h:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lgbm;-><init>(Ljava/lang/String;Landroid/net/Uri;Lgbn;Ljava/lang/String;Lgcd;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;)V

    return-object v0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lgbo;->a()Lgbm;

    move-result-object v0

    return-object v0
.end method
