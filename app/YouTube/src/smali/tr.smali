.class Ltr;
.super Ltn;
.source "SourceFile"

# interfaces
.implements Lss;


# instance fields
.field private o:Lsr;

.field private p:Lsu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltw;)V
    .locals 0

    .prologue
    .line 716
    invoke-direct {p0, p1, p2}, Ltn;-><init>(Landroid/content/Context;Ltw;)V

    .line 717
    return-void
.end method


# virtual methods
.method protected a(Ltp;Lrk;)V
    .locals 3

    .prologue
    .line 742
    invoke-super {p0, p1, p2}, Ltn;->a(Ltp;Lrk;)V

    .line 744
    iget-object v0, p1, Ltp;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 745
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lrk;->a(Z)Lrk;

    .line 748
    :cond_0
    invoke-virtual {p0, p1}, Ltr;->a(Ltp;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    iget-object v0, p2, Lrk;->a:Landroid/os/Bundle;

    const-string v1, "connecting"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 752
    :cond_1
    iget-object v0, p1, Ltp;->a:Ljava/lang/Object;

    check-cast v0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {v0}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_2

    .line 755
    invoke-virtual {v0}, Landroid/view/Display;->getDisplayId()I

    move-result v0

    invoke-virtual {p2, v0}, Lrk;->f(I)Lrk;

    .line 757
    :cond_2
    return-void
.end method

.method protected a(Ltp;)Z
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Ltr;->p:Lsu;

    if-nez v0, :cond_0

    .line 777
    new-instance v0, Lsu;

    invoke-direct {v0}, Lsu;-><init>()V

    iput-object v0, p0, Ltr;->p:Lsu;

    .line 779
    :cond_0
    iget-object v0, p0, Ltr;->p:Lsu;

    iget-object v1, p1, Ltp;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lsu;->a(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 761
    invoke-super {p0}, Ltn;->c()V

    .line 763
    iget-object v0, p0, Ltr;->o:Lsr;

    if-nez v0, :cond_0

    .line 764
    new-instance v0, Lsr;

    iget-object v2, p0, Lrm;->a:Landroid/content/Context;

    iget-object v3, p0, Lrm;->c:Lro;

    invoke-direct {v0, v2, v3}, Lsr;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Ltr;->o:Lsr;

    .line 767
    :cond_0
    iget-object v2, p0, Ltr;->o:Lsr;

    iget-boolean v0, p0, Ltr;->l:Z

    if-eqz v0, :cond_2

    iget v0, p0, Ltr;->k:I

    :goto_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_4

    iget-boolean v0, v2, Lsr;->c:Z

    if-nez v0, :cond_1

    iget-object v0, v2, Lsr;->b:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, v2, Lsr;->c:Z

    iget-object v0, v2, Lsr;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 768
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 767
    goto :goto_0

    :cond_3
    const-string v0, "MediaRouterJellybeanMr1"

    const-string v1, "Cannot scan for wifi displays because the DisplayManager.scanWifiDisplays() method is not available on this device."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    iget-boolean v0, v2, Lsr;->c:Z

    if-eqz v0, :cond_1

    iput-boolean v1, v2, Lsr;->c:Z

    iget-object v0, v2, Lsr;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method protected final d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 772
    new-instance v0, Lst;

    invoke-direct {v0, p0}, Lst;-><init>(Lss;)V

    return-object v0
.end method

.method public final f(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 721
    invoke-virtual {p0, p1}, Ltr;->g(Ljava/lang/Object;)I

    move-result v0

    .line 722
    if-ltz v0, :cond_0

    .line 723
    iget-object v1, p0, Ltr;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ltp;

    .line 724
    check-cast p1, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p1}, Landroid/media/MediaRouter$RouteInfo;->getPresentationDisplay()Landroid/view/Display;

    move-result-object v1

    .line 726
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/Display;->getDisplayId()I

    move-result v1

    .line 728
    :goto_0
    iget-object v2, v0, Ltp;->c:Lrj;

    invoke-virtual {v2}, Lrj;->m()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 730
    new-instance v2, Lrk;

    iget-object v3, v0, Ltp;->c:Lrj;

    invoke-direct {v2, v3}, Lrk;-><init>(Lrj;)V

    invoke-virtual {v2, v1}, Lrk;->f(I)Lrk;

    move-result-object v1

    invoke-virtual {v1}, Lrk;->a()Lrj;

    move-result-object v1

    iput-object v1, v0, Ltp;->c:Lrj;

    .line 734
    invoke-virtual {p0}, Ltr;->b()V

    .line 737
    :cond_0
    return-void

    .line 726
    :cond_1
    const/4 v1, -0x1

    goto :goto_0
.end method
