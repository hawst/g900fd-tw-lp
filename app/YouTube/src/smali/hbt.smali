.class public final Lhbt;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhbs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17058
    invoke-direct {p0}, Lidf;-><init>()V

    .line 17061
    const/4 v0, 0x0

    iput-object v0, p0, Lhbt;->a:Lhbs;

    .line 17058
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 17098
    const/4 v0, 0x0

    .line 17099
    iget-object v1, p0, Lhbt;->a:Lhbs;

    if-eqz v1, :cond_0

    .line 17100
    const v0, 0x3e22b11

    iget-object v1, p0, Lhbt;->a:Lhbs;

    .line 17101
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 17103
    :cond_0
    iget-object v1, p0, Lhbt;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17104
    iput v0, p0, Lhbt;->J:I

    .line 17105
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 17054
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbt;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbt;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbt;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbt;->a:Lhbs;

    if-nez v0, :cond_2

    new-instance v0, Lhbs;

    invoke-direct {v0}, Lhbs;-><init>()V

    iput-object v0, p0, Lhbt;->a:Lhbs;

    :cond_2
    iget-object v0, p0, Lhbt;->a:Lhbs;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1f11588a -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 17089
    iget-object v0, p0, Lhbt;->a:Lhbs;

    if-eqz v0, :cond_0

    .line 17090
    const v0, 0x3e22b11

    iget-object v1, p0, Lhbt;->a:Lhbs;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 17092
    :cond_0
    iget-object v0, p0, Lhbt;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 17094
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 17072
    if-ne p1, p0, :cond_1

    .line 17076
    :cond_0
    :goto_0
    return v0

    .line 17073
    :cond_1
    instance-of v2, p1, Lhbt;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 17074
    :cond_2
    check-cast p1, Lhbt;

    .line 17075
    iget-object v2, p0, Lhbt;->a:Lhbs;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbt;->a:Lhbs;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbt;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbt;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 17076
    goto :goto_0

    .line 17075
    :cond_4
    iget-object v2, p0, Lhbt;->a:Lhbs;

    iget-object v3, p1, Lhbt;->a:Lhbs;

    invoke-virtual {v2, v3}, Lhbs;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbt;->I:Ljava/util/List;

    iget-object v3, p1, Lhbt;->I:Ljava/util/List;

    .line 17076
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 17080
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 17082
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbt;->a:Lhbs;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 17083
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbt;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 17084
    return v0

    .line 17082
    :cond_0
    iget-object v0, p0, Lhbt;->a:Lhbs;

    invoke-virtual {v0}, Lhbs;->hashCode()I

    move-result v0

    goto :goto_0

    .line 17083
    :cond_1
    iget-object v1, p0, Lhbt;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
