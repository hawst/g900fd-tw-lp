.class final Ldkc;
.super Ldjy;
.source "SourceFile"


# instance fields
.field private synthetic b:Ldjx;


# direct methods
.method public constructor <init>(Ldjx;I)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Ldkc;->b:Ldjx;

    .line 638
    invoke-direct {p0, p1, p2}, Ldjy;-><init>(Ldjx;I)V

    .line 639
    return-void
.end method

.method static synthetic a(Ldkc;Lfnx;)V
    .locals 2

    .prologue
    .line 635
    iget-boolean v0, p0, Ldkc;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldkc;->b:Ldjx;

    iput-object p1, v0, Ldjx;->h:Lfnx;

    iget-object v0, p0, Ldkc;->b:Ldjx;

    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {v0, v1}, Ldjx;->a(Lgok;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 7

    .prologue
    .line 643
    invoke-virtual {p0, p1}, Ldkc;->b(I)Z

    move-result v0

    .line 644
    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Ldkc;->b:Ldjx;

    iget-object v0, v0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    iget-object v1, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v0, p0, Ldkc;->b:Ldjx;

    iget-object v0, v0, Ldjx;->c:Levn;

    new-instance v2, Lczr;

    invoke-direct {v2}, Lczr;-><init>()V

    invoke-virtual {v0, v2}, Levn;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Ldkc;->b:Ldjx;

    iget-object v0, v0, Ldjx;->m:Lglm;

    invoke-interface {v0}, Lglm;->a()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v6

    iget-object v0, p0, Ldkc;->b:Ldjx;

    iget-object v0, v0, Ldjx;->l:Ldaw;

    iget-object v2, p0, Ldkc;->b:Ldjx;

    invoke-virtual {v2}, Ldjx;->u()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Ldkc;->b:Ldjx;

    iget-object v2, v2, Ldjx;->r:Ljava/lang/String;

    :goto_0
    iget-object v3, p0, Ldkc;->b:Ldjx;

    invoke-virtual {v3}, Ldjx;->u()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, p1

    :goto_1
    const-string v4, ""

    iget-object v5, p0, Ldkc;->b:Ldjx;

    iget-object v5, v5, Ldjx;->o:[B

    invoke-virtual/range {v0 .. v6}, Ldaw;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLwv;)V

    const-wide/16 v0, 0x3

    :try_start_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, v0, v1, v2}, Lgky;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnx;

    iget-object v1, p0, Ldkc;->b:Ldjx;

    iget-object v1, v1, Ldjx;->a:Landroid/content/Context;

    iget-object v2, p0, Ldkc;->b:Ldjx;

    iget-object v2, v2, Ldjx;->s:Lgbu;

    iget-object v3, p0, Ldkc;->b:Ldjx;

    iget-object v3, v3, Ldjx;->t:Ljava/util/List;

    invoke-static {v0, v1, v2, v3, p1}, La;->a(Lfnx;Landroid/content/Context;Lgbu;Ljava/util/List;I)Lfnx;

    move-result-object v0

    iget-object v1, p0, Ldkc;->b:Ldjx;

    iget-object v1, v1, Ldjx;->p:Ljava/util/concurrent/Executor;

    new-instance v2, Ldkd;

    invoke-direct {v2, p0, v0}, Ldkd;-><init>(Ldkc;Lfnx;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 647
    :cond_0
    :goto_2
    return-void

    .line 645
    :cond_1
    const-string v2, ""

    goto :goto_0

    :cond_2
    const/4 v3, -0x1

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_3
    :goto_3
    iget-object v0, p0, Ldkc;->b:Ldjx;

    invoke-virtual {v0}, Ldjx;->u()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldkc;->b:Ldjx;

    iget-object v0, v0, Ldjx;->a:Landroid/content/Context;

    iget-object v1, p0, Ldkc;->b:Ldjx;

    iget-object v1, v1, Ldjx;->s:Lgbu;

    iget-object v2, p0, Ldkc;->b:Ldjx;

    iget-object v2, v2, Ldjx;->t:Ljava/util/List;

    invoke-static {v0, v1, v2, p1}, La;->a(Landroid/content/Context;Lgbu;Ljava/util/List;I)Lfnx;

    move-result-object v0

    :goto_4
    iget-object v1, p0, Ldkc;->b:Ldjx;

    iget-object v1, v1, Ldjx;->p:Ljava/util/concurrent/Executor;

    new-instance v2, Ldke;

    invoke-direct {v2, p0, v0}, Ldke;-><init>(Ldkc;Lfnx;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Ldkc;->b:Ldjx;

    iget-object v1, v0, Ldjx;->a:Landroid/content/Context;

    iget-object v0, p0, Ldkc;->b:Ldjx;

    iget-object v0, v0, Ldjx;->t:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    invoke-static {v1, v0}, La;->a(Landroid/content/Context;Lgcd;)Lfnx;

    move-result-object v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_3
.end method
