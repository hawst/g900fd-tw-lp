.class public final Lmy;
.super Lty;
.source "SourceFile"

# interfaces
.implements Lns;


# instance fields
.field private c:Ltz;

.field private d:Lnr;

.field private e:Ljava/lang/ref/WeakReference;

.field private synthetic f:Lmu;


# direct methods
.method public constructor <init>(Lmu;Ltz;)V
    .locals 2

    .prologue
    .line 951
    iput-object p1, p0, Lmy;->f:Lmu;

    invoke-direct {p0}, Lty;-><init>()V

    .line 952
    iput-object p2, p0, Lmy;->c:Ltz;

    .line 953
    new-instance v0, Lnr;

    invoke-virtual {p1}, Lmu;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lnr;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    iput v1, v0, Lnr;->e:I

    iput-object v0, p0, Lmy;->d:Lnr;

    .line 955
    iget-object v0, p0, Lmy;->d:Lnr;

    invoke-virtual {v0, p0}, Lnr;->a(Lns;)V

    .line 956
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 960
    new-instance v0, Lne;

    iget-object v1, p0, Lmy;->f:Lmu;

    invoke-virtual {v1}, Lmu;->b()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lne;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1036
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->k(Lmu;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmy;->b(Ljava/lang/CharSequence;)V

    .line 1037
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1020
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->d(Landroid/view/View;)V

    .line 1021
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lmy;->e:Ljava/lang/ref/WeakReference;

    .line 1022
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1026
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->b(Ljava/lang/CharSequence;)V

    .line 1027
    return-void
.end method

.method public final a(Lnr;)V
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lmy;->c:Ltz;

    if-nez v0, :cond_0

    .line 1103
    :goto_0
    return-void

    .line 1101
    :cond_0
    invoke-virtual {p0}, Lmy;->d()V

    .line 1102
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->a()Z

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1056
    invoke-super {p0, p1}, Lty;->a(Z)V

    .line 1057
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Z)V

    .line 1058
    return-void
.end method

.method public final a(Lnr;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1071
    iget-object v0, p0, Lmy;->c:Ltz;

    if-eqz v0, :cond_0

    .line 1072
    iget-object v0, p0, Lmy;->c:Ltz;

    invoke-interface {v0, p0, p2}, Ltz;->a(Lty;Landroid/view/MenuItem;)Z

    move-result v0

    .line 1074
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lmy;->d:Lnr;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1041
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->k(Lmu;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lmy;->a(Ljava/lang/CharSequence;)V

    .line 1042
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1031
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/internal/widget/ActionBarContextView;->a(Ljava/lang/CharSequence;)V

    .line 1032
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 970
    iget-object v0, p0, Lmy;->f:Lmu;

    iget-object v0, v0, Lmu;->a:Lmy;

    if-eq v0, p0, :cond_0

    .line 997
    :goto_0
    return-void

    .line 979
    :cond_0
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->g(Lmu;)Z

    move-result v0

    iget-object v1, p0, Lmy;->f:Lmu;

    invoke-static {v1}, Lmu;->h(Lmu;)Z

    move-result v1

    invoke-static {v0, v1, v2}, Lmu;->a(ZZZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 982
    iget-object v0, p0, Lmy;->f:Lmu;

    iput-object p0, v0, Lmu;->b:Lty;

    .line 983
    iget-object v0, p0, Lmy;->f:Lmu;

    iget-object v1, p0, Lmy;->c:Ltz;

    iput-object v1, v0, Lmu;->c:Ltz;

    .line 987
    :goto_1
    iput-object v4, p0, Lmy;->c:Ltz;

    .line 988
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-virtual {v0, v2}, Lmu;->j(Z)V

    .line 991
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v1

    iget v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->k:I

    if-eq v0, v3, :cond_1

    iget-object v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->h:Landroid/view/View;

    if-nez v0, :cond_3

    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->c()V

    .line 992
    :cond_1
    :goto_2
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->j(Lmu;)Lpv;

    move-result-object v0

    invoke-interface {v0}, Lpv;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->sendAccessibilityEvent(I)V

    .line 994
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->f(Lmu;)Landroid/support/v7/internal/widget/ActionBarOverlayLayout;

    move-result-object v0

    iget-object v1, p0, Lmy;->f:Lmu;

    iget-boolean v1, v1, Lmu;->d:Z

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarOverlayLayout;->a(Z)V

    .line 996
    iget-object v0, p0, Lmy;->f:Lmu;

    iput-object v4, v0, Lmu;->a:Lmy;

    goto :goto_0

    .line 985
    :cond_2
    iget-object v0, p0, Lmy;->c:Ltz;

    invoke-interface {v0, p0}, Ltz;->a(Lty;)V

    goto :goto_1

    .line 991
    :cond_3
    invoke-virtual {v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->b()V

    iput v3, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->k:I

    iget-object v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->h:Landroid/view/View;

    invoke-static {v0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v2

    iget-object v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v3, v0

    iget-object v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v3, v0

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Lhj;->b(F)Lhj;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Lhj;->a(J)Lhj;

    invoke-virtual {v0, v1}, Lhj;->a(Lhv;)Lhj;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Lhj;->a(Landroid/view/animation/Interpolator;)Lhj;

    new-instance v2, Lnh;

    invoke-direct {v2}, Lnh;-><init>()V

    invoke-virtual {v2, v0}, Lnh;->a(Lhj;)Lnh;

    iget-object v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->b:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v0, :cond_4

    iget-object v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->b:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->getChildCount()I

    :cond_4
    iput-object v2, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Lnh;

    iget-object v0, v1, Landroid/support/v7/internal/widget/ActionBarContextView;->j:Lnh;

    invoke-virtual {v0}, Lnh;->a()V

    goto :goto_2
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1001
    iget-object v0, p0, Lmy;->d:Lnr;

    invoke-virtual {v0}, Lnr;->d()V

    .line 1003
    :try_start_0
    iget-object v0, p0, Lmy;->c:Ltz;

    iget-object v1, p0, Lmy;->d:Lnr;

    invoke-interface {v0, p0, v1}, Ltz;->b(Lty;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1005
    iget-object v0, p0, Lmy;->d:Lnr;

    invoke-virtual {v0}, Lnr;->e()V

    .line 1006
    return-void

    .line 1005
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmy;->d:Lnr;

    invoke-virtual {v1}, Lnr;->e()V

    throw v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1010
    iget-object v0, p0, Lmy;->d:Lnr;

    invoke-virtual {v0}, Lnr;->d()V

    .line 1012
    :try_start_0
    iget-object v0, p0, Lmy;->c:Ltz;

    iget-object v1, p0, Lmy;->d:Lnr;

    invoke-interface {v0, p0, v1}, Ltz;->a(Lty;Landroid/view/Menu;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1014
    iget-object v1, p0, Lmy;->d:Lnr;

    invoke-virtual {v1}, Lnr;->e()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lmy;->d:Lnr;

    invoke-virtual {v1}, Lnr;->e()V

    throw v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1046
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1051
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1062
    iget-object v0, p0, Lmy;->f:Lmu;

    invoke-static {v0}, Lmu;->i(Lmu;)Landroid/support/v7/internal/widget/ActionBarContextView;

    move-result-object v0

    iget-boolean v0, v0, Landroid/support/v7/internal/widget/ActionBarContextView;->i:Z

    return v0
.end method

.method public final i()Landroid/view/View;
    .locals 1

    .prologue
    .line 1067
    iget-object v0, p0, Lmy;->e:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmy;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
