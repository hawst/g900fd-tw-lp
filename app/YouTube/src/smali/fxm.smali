.class public final enum Lfxm;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lfxm;

.field private static enum b:Lfxm;

.field private static enum c:Lfxm;

.field private static enum d:Lfxm;

.field private static enum e:Lfxm;

.field private static enum f:Lfxm;

.field private static enum g:Lfxm;

.field private static enum h:Lfxm;

.field private static enum i:Lfxm;

.field private static enum j:Lfxm;

.field private static final synthetic k:[Lfxm;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 188
    new-instance v0, Lfxm;

    const-string v1, "MOST_VIEWED"

    invoke-direct {v0, v1, v3}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->b:Lfxm;

    .line 189
    new-instance v0, Lfxm;

    const-string v1, "TOP_RATED"

    invoke-direct {v0, v1, v4}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->c:Lfxm;

    .line 190
    new-instance v0, Lfxm;

    const-string v1, "MOST_DISCUSSED"

    invoke-direct {v0, v1, v5}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->d:Lfxm;

    .line 191
    new-instance v0, Lfxm;

    const-string v1, "TOP_FAVORITES"

    invoke-direct {v0, v1, v6}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->e:Lfxm;

    .line 192
    new-instance v0, Lfxm;

    const-string v1, "MOST_RESPONDED"

    invoke-direct {v0, v1, v7}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->f:Lfxm;

    .line 193
    new-instance v0, Lfxm;

    const-string v1, "MOST_POPULAR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->a:Lfxm;

    .line 194
    new-instance v0, Lfxm;

    const-string v1, "MOST_LINKED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->g:Lfxm;

    .line 195
    new-instance v0, Lfxm;

    const-string v1, "RECENTLY_FEATURED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->h:Lfxm;

    .line 196
    new-instance v0, Lfxm;

    const-string v1, "WATCH_ON_MOBILE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->i:Lfxm;

    .line 197
    new-instance v0, Lfxm;

    const-string v1, "ON_THE_WEB"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lfxm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lfxm;->j:Lfxm;

    .line 187
    const/16 v0, 0xa

    new-array v0, v0, [Lfxm;

    sget-object v1, Lfxm;->b:Lfxm;

    aput-object v1, v0, v3

    sget-object v1, Lfxm;->c:Lfxm;

    aput-object v1, v0, v4

    sget-object v1, Lfxm;->d:Lfxm;

    aput-object v1, v0, v5

    sget-object v1, Lfxm;->e:Lfxm;

    aput-object v1, v0, v6

    sget-object v1, Lfxm;->f:Lfxm;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lfxm;->a:Lfxm;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lfxm;->g:Lfxm;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lfxm;->h:Lfxm;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lfxm;->i:Lfxm;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lfxm;->j:Lfxm;

    aput-object v2, v0, v1

    sput-object v0, Lfxm;->k:[Lfxm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lfxm;
    .locals 1

    .prologue
    .line 187
    const-class v0, Lfxm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lfxm;

    return-object v0
.end method

.method public static values()[Lfxm;
    .locals 1

    .prologue
    .line 187
    sget-object v0, Lfxm;->k:[Lfxm;

    invoke-virtual {v0}, [Lfxm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lfxm;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    invoke-super {p0}, Ljava/lang/Enum;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
