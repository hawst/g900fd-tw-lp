.class public final Ldek;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldea;


# instance fields
.field a:Ldbu;

.field b:Ldeb;

.field c:Z

.field d:Z

.field e:Z

.field f:[Ljava/lang/String;

.field g:I

.field h:Z

.field i:Z

.field j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot show StubOverflowOverlay"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 50
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Ldek;->a:Ldbu;

    .line 32
    return-void
.end method

.method public final a(Ldeb;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Ldek;->b:Ldeb;

    .line 37
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 69
    iput-boolean p1, p0, Ldek;->c:Z

    .line 70
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Ldek;->f:[Ljava/lang/String;

    .line 92
    iput p2, p0, Ldek;->g:I

    .line 93
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Ldek;->e:Z

    .line 87
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Ldek;->d:Z

    .line 82
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 74
    iget-boolean v0, p0, Ldek;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldek;->a:Ldbu;

    iget-boolean v0, v0, Ldbu;->k:Z

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Ldek;->b:Ldeb;

    invoke-interface {v0}, Ldeb;->a()V

    .line 77
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 102
    iput-boolean p1, p0, Ldek;->i:Z

    .line 103
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 58
    iput-boolean v0, p0, Ldek;->c:Z

    .line 59
    iput-boolean v0, p0, Ldek;->e:Z

    .line 60
    iput-boolean v0, p0, Ldek;->h:Z

    .line 61
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 97
    iput-boolean p1, p0, Ldek;->h:Z

    .line 98
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 107
    iput-boolean p1, p0, Ldek;->j:Z

    .line 108
    return-void
.end method
