.class final Ldgd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldfv;


# instance fields
.field private synthetic a:Ldfw;


# direct methods
.method constructor <init>(Ldfw;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Ldgd;->a:Ldfw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 596
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget v0, v0, Ldfw;->h:I

    if-ltz v0, :cond_0

    .line 597
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->f:[Z

    iget-object v1, p0, Ldgd;->a:Ldfw;

    iget v1, v1, Ldfw;->h:I

    aput-boolean v2, v0, v1

    .line 599
    :cond_0
    iget-object v0, p0, Ldgd;->a:Ldfw;

    invoke-virtual {v0, v2}, Ldfw;->a(Z)V

    .line 600
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 582
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->i:Lhbw;

    if-eqz v0, :cond_0

    .line 583
    if-eqz p1, :cond_1

    .line 584
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->i:Lhbw;

    iget-object v0, v0, Lhbw;->g:Lhog;

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->c:Lfhz;

    iget-object v1, p0, Ldgd;->a:Ldfw;

    iget-object v1, v1, Ldfw;->i:Lhbw;

    iget-object v1, v1, Lhbw;->g:Lhog;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    .line 592
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->g:[Z

    iget-object v1, p0, Ldgd;->a:Ldfw;

    iget v1, v1, Ldfw;->h:I

    aput-boolean v2, v0, v1

    .line 589
    iget-object v0, p0, Ldgd;->a:Ldfw;

    invoke-virtual {v0, v2, v2}, Ldfw;->a(ZZ)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 567
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->d:Lhqz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgd;->a:Ldfw;

    .line 568
    iget-object v0, v0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->a:Lhgm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgd;->a:Ldfw;

    .line 569
    iget-object v0, v0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->a:Lhgm;

    iget-object v0, v0, Lhgm;->e:Lhog;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->c:Lfhz;

    iget-object v1, p0, Ldgd;->a:Ldfw;

    iget-object v1, v1, Ldfw;->d:Lhqz;

    iget-object v1, v1, Lhqz;->a:Lhgm;

    iget-object v1, v1, Lhgm;->e:Lhog;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lfhz;->a(Lhog;Ljava/lang/Object;)V

    .line 572
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Ldgd;->a:Ldfw;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldfw;->e:Z

    .line 577
    iget-object v0, p0, Ldgd;->a:Ldfw;

    invoke-virtual {v0}, Ldfw;->a()V

    .line 578
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 604
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->k:Lfkg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget v0, v0, Ldfw;->j:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Ldgd;->a:Ldfw;

    .line 605
    iget v0, v0, Ldfw;->j:I

    iget-object v1, p0, Ldgd;->a:Ldfw;

    iget-object v1, v1, Ldfw;->k:Lfkg;

    invoke-virtual {v1}, Lfkg;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 606
    iget-object v0, p0, Ldgd;->a:Ldfw;

    iget-object v0, v0, Ldfw;->k:Lfkg;

    invoke-virtual {v0}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Ldgd;->a:Ldfw;

    iget v1, v1, Ldfw;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkf;

    .line 607
    iget-object v1, p0, Ldgd;->a:Ldfw;

    iget-object v1, v1, Ldfw;->a:Levn;

    new-instance v2, Ldah;

    iget-object v3, p0, Ldgd;->a:Ldfw;

    .line 609
    iget-object v3, v3, Ldfw;->l:Ldgc;

    invoke-virtual {v3}, Ldgc;->a()Lfqg;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ldah;-><init>(Lfkf;Lfqg;)V

    .line 607
    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 611
    :cond_0
    return-void
.end method
