.class public Lfmb;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhsp;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Lfnc;

.field public e:Lflh;

.field public final f:Ljava/lang/String;

.field public final g:Z

.field private final h:Lfqh;

.field private i:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhsp;Ljava/lang/String;ZLfqh;)V
    .locals 5

    .prologue
    .line 48
    invoke-direct {p0}, Lfic;-><init>()V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsp;

    iput-object v0, p0, Lfmb;->a:Lhsp;

    .line 50
    iput-object p4, p0, Lfmb;->h:Lfqh;

    .line 51
    iput-object p2, p0, Lfmb;->f:Ljava/lang/String;

    .line 52
    iput-boolean p3, p0, Lfmb;->g:Z

    .line 56
    iget-object v1, p1, Lhsp;->g:[Lhbi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 58
    iget-object v4, v3, Lhbi;->d:Lhnx;

    if-eqz v4, :cond_1

    .line 59
    iget-object v0, v3, Lhbi;->d:Lhnx;

    .line 63
    :cond_0
    return-void

    .line 57
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 174
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 175
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lfmb;->a:Lhsp;

    iget-object v0, v0, Lhsp;->f:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lfmb;->i:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lfmb;->a:Lhsp;

    iget-object v0, v0, Lhsp;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmb;->i:Ljava/lang/CharSequence;

    .line 98
    :cond_0
    iget-object v0, p0, Lfmb;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lfmb;->a:Lhsp;

    iget-object v0, v0, Lhsp;->j:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lfmb;->h:Lfqh;

    return-object v0
.end method
