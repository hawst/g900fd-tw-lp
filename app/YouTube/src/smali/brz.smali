.class public final Lbrz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field final b:Lbjx;

.field c:Landroid/app/AlertDialog;

.field d:Ljava/util/LinkedList;

.field e:Landroid/widget/CheckBox;

.field f:Lbsm;

.field public g:Landroid/app/AlertDialog;

.field public h:Lbsj;

.field public i:Landroid/app/AlertDialog;

.field public j:Lbsl;

.field public k:Landroid/app/AlertDialog;

.field public l:Lbsi;

.field public m:Landroid/app/AlertDialog;

.field public n:Lbsk;

.field private final o:Landroid/content/SharedPreferences;

.field private p:Landroid/view/View$OnClickListener;

.field private q:Landroid/app/AlertDialog;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Landroid/content/SharedPreferences;Lbjx;)V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 107
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lbrz;->o:Landroid/content/SharedPreferences;

    .line 108
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lbrz;->b:Lbjx;

    .line 109
    return-void
.end method

.method private a(Lbsn;)V
    .locals 1

    .prologue
    .line 112
    invoke-static {p1}, Lbsn;->a(Lbsn;)Lbjy;

    move-result-object v0

    if-nez v0, :cond_0

    .line 115
    invoke-static {p1}, Lbsn;->b(Lbsn;)V

    .line 119
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-object v0, p0, Lbrz;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public a([Lbsh;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;
    .locals 7

    .prologue
    .line 418
    new-instance v0, Lbsf;

    iget-object v2, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v3, 0x7f04005e

    const v4, 0x7f08008b

    move-object v1, p0

    move-object v5, p1

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lbsf;-><init>(Lbrz;Landroid/content/Context;II[Lbsh;[Lbsh;)V

    .line 435
    new-instance v1, Leyv;

    iget-object v2, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v1, v2}, Leyv;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090183

    .line 436
    invoke-virtual {v1, v2}, Leyv;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 437
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 438
    invoke-virtual {v1, v0, p2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900a2

    const/4 v2, 0x0

    .line 439
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 440
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 284
    iget-object v0, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v1, 0x7f0901aa

    .line 285
    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    const v2, 0x7f0901ab

    .line 286
    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 284
    iget-object v0, p0, Lbrz;->q:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v3, 0x7f040060

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0801c0

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbrz;->r:Landroid/widget/TextView;

    const v0, 0x7f080142

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbrz;->s:Landroid/widget/TextView;

    new-instance v0, Leyv;

    iget-object v4, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v4}, Leyv;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Leyv;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0900a1

    invoke-virtual {v0, v3, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbrz;->q:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lbrz;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbrz;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbrz;->q:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 287
    return-void
.end method

.method a(Lbjy;)V
    .locals 3

    .prologue
    .line 274
    iget-object v0, p0, Lbrz;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsn;

    .line 275
    invoke-static {v0}, Lbsn;->a(Lbsn;)Lbjy;

    move-result-object v1

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v0, v1}, Lbsn;->a(Lbsn;Z)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 277
    :cond_1
    return-void
.end method

.method public a(Lflh;Lbsm;I)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 194
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    invoke-virtual {p1}, Lflh;->a()Ljava/util/Map;

    move-result-object v4

    .line 195
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsm;

    iput-object v0, p0, Lbrz;->f:Lbsm;

    iget-object v0, p0, Lbrz;->c:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a2

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbrz;->d:Ljava/util/LinkedList;

    new-instance v0, Lbsn;

    sget-object v2, Lflj;->a:Lflj;

    const v5, 0x7f080245

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {v0, p0, v2, v5}, Lbsn;-><init>(Lbrz;Lflj;Landroid/view/View;)V

    invoke-direct {p0, v0}, Lbrz;->a(Lbsn;)V

    new-instance v0, Lbsn;

    sget-object v2, Lflj;->b:Lflj;

    const v5, 0x7f080246

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {v0, p0, v2, v5}, Lbsn;-><init>(Lbrz;Lflj;Landroid/view/View;)V

    invoke-direct {p0, v0}, Lbrz;->a(Lbsn;)V

    new-instance v0, Lbsn;

    sget-object v2, Lflj;->c:Lflj;

    const v5, 0x7f080247

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {v0, p0, v2, v5}, Lbsn;-><init>(Lbrz;Lflj;Landroid/view/View;)V

    invoke-direct {p0, v0}, Lbrz;->a(Lbsn;)V

    const v0, 0x7f080248

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbrz;->e:Landroid/widget/CheckBox;

    new-instance v0, Leyv;

    iget-object v2, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v0, v2}, Leyv;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0900a1

    invoke-virtual {v0, v2, v6}, Leyv;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0900a2

    invoke-virtual {v0, v2, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbrz;->c:Landroid/app/AlertDialog;

    :cond_0
    iget-object v0, p0, Lbrz;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0, p3}, Landroid/app/AlertDialog;->setTitle(I)V

    iget-object v0, p0, Lbrz;->b:Lbjx;

    invoke-virtual {v0}, Lbjx;->b()Lbjy;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbrz;->a(Lbjy;)V

    iget-object v0, p0, Lbrz;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lbrz;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsn;

    invoke-static {v0}, Lbsn;->c(Lbsn;)Lflj;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfli;

    if-eqz v1, :cond_1

    invoke-static {v0, v1}, Lbsn;->a(Lbsn;Lfli;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lbsn;->b(Lbsn;)V

    goto :goto_0

    :cond_2
    if-lez v2, :cond_4

    iget-object v0, p0, Lbrz;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    iget-object v0, p0, Lbrz;->p:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_3

    new-instance v0, Lbsa;

    invoke-direct {v0, p0}, Lbsa;-><init>(Lbrz;)V

    iput-object v0, p0, Lbrz;->p:Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lbrz;->c:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lbrz;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const/4 v3, 0x1

    :cond_4
    return v3
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 450
    iget-object v2, p0, Lbrz;->o:Landroid/content/SharedPreferences;

    const-string v3, "show_offline_first_add_dialog"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 451
    new-instance v2, Leyv;

    iget-object v3, p0, Lbrz;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-direct {v2, v3}, Leyv;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0901a2

    .line 452
    invoke-virtual {v2, v3}, Leyv;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0901a3

    .line 453
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 454
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0901d8

    new-instance v4, Lbsg;

    invoke-direct {v4, p0}, Lbsg;-><init>(Lbrz;)V

    .line 455
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0901d7

    const/4 v4, 0x0

    .line 461
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 462
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 463
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 464
    iget-object v2, p0, Lbrz;->o:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "show_offline_first_add_dialog"

    .line 465
    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 466
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 469
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
