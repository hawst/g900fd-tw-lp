.class public Ldyi;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/logging/Logger;


# instance fields
.field private final b:Ljava/util/List;

.field private c:Ljava/nio/ByteBuffer;

.field private d:Ljava/nio/CharBuffer;

.field private e:Ljava/nio/ByteBuffer;

.field private f:Ljava/nio/CharBuffer;

.field private g:Ljava/lang/String;

.field private h:Ljava/nio/charset/CharsetEncoder;

.field private i:I

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:Lu;

.field private o:Ljava/nio/CharBuffer;

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const-class v0, Ldyi;

    .line 95
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Ldyi;->a:Ljava/util/logging/Logger;

    .line 94
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldyi;->b:Ljava/util/List;

    .line 113
    iput-object v1, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    .line 114
    iput-object v1, p0, Ldyi;->d:Ljava/nio/CharBuffer;

    .line 115
    iput-object v1, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    .line 116
    iput-object v1, p0, Ldyi;->f:Ljava/nio/CharBuffer;

    .line 117
    iput-object v1, p0, Ldyi;->g:Ljava/lang/String;

    .line 118
    iput-object v1, p0, Ldyi;->h:Ljava/nio/charset/CharsetEncoder;

    .line 119
    const/16 v0, 0x1000

    iput v0, p0, Ldyi;->i:I

    .line 121
    iput-boolean v2, p0, Ldyi;->j:Z

    .line 122
    iput v2, p0, Ldyi;->k:I

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Ldyi;->l:I

    .line 124
    iput v2, p0, Ldyi;->m:I

    .line 125
    iput-object v1, p0, Ldyi;->n:Lu;

    .line 126
    iput-object v1, p0, Ldyi;->o:Ljava/nio/CharBuffer;

    .line 128
    iput v2, p0, Ldyi;->p:I

    .line 135
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1373
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    iput v0, p0, Ldyi;->k:I

    .line 1374
    iget-object v1, p0, Ldyi;->b:Ljava/util/List;

    monitor-enter v1

    .line 1375
    :try_start_0
    iget-object v0, p0, Ldyi;->b:Ljava/util/List;

    iget-object v2, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1376
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1377
    const/4 v0, 0x0

    iput-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    .line 1380
    iget v0, p0, Ldyi;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ldyi;->b()I

    move-result v0

    iget v1, p0, Ldyi;->l:I

    if-lt v0, v1, :cond_0

    .line 1381
    :cond_0
    return-void

    .line 1376
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)I
    .locals 6

    .prologue
    .line 866
    const/4 v1, 0x0

    .line 872
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 873
    :goto_0
    if-lez v0, :cond_1

    .line 874
    invoke-virtual {p0}, Ldyi;->d()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 875
    if-eqz v2, :cond_1

    .line 876
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    .line 879
    if-gt v3, v0, :cond_0

    .line 880
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 881
    add-int v0, v1, v3

    .line 889
    :goto_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    .line 890
    invoke-virtual {p0}, Ldyi;->e()V

    move v5, v1

    move v1, v0

    move v0, v5

    .line 891
    goto :goto_0

    .line 883
    :cond_0
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    .line 884
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 885
    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 886
    add-int/2addr v0, v1

    .line 887
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_1

    .line 892
    :cond_1
    return v1
.end method

.method public final a([BII)I
    .locals 3

    .prologue
    .line 830
    move v1, p3

    .line 836
    :goto_0
    if-lez v1, :cond_2

    .line 837
    invoke-virtual {p0}, Ldyi;->d()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 838
    if-nez v2, :cond_0

    .line 839
    if-ne p3, v1, :cond_2

    .line 840
    const/4 v0, -0x1

    .line 854
    :goto_1
    return v0

    .line 844
    :cond_0
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 845
    if-le v0, v1, :cond_1

    move v0, v1

    .line 848
    :cond_1
    invoke-virtual {v2, p1, p2, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 849
    sub-int/2addr v1, v0

    .line 850
    add-int/2addr p2, v0

    .line 852
    invoke-virtual {p0}, Ldyi;->e()V

    goto :goto_0

    .line 854
    :cond_2
    sub-int v0, p3, v1

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    if-lez v0, :cond_0

    .line 401
    invoke-direct {p0}, Ldyi;->f()V

    .line 403
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 940
    const/4 v0, 0x0

    .line 943
    iget-object v1, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 944
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 948
    :cond_0
    iget-object v2, p0, Ldyi;->b:Ljava/util/List;

    monitor-enter v2

    .line 949
    :try_start_0
    iget-object v1, p0, Ldyi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 950
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 951
    goto :goto_0

    .line 952
    :cond_1
    monitor-exit v2

    .line 953
    return v1

    .line 952
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b([BII)V
    .locals 3

    .prologue
    .line 1102
    move v1, p3

    :goto_0
    if-lez v1, :cond_1

    .line 1109
    invoke-virtual {p0}, Ldyi;->c()Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 1110
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 1111
    if-le v0, v1, :cond_0

    move v0, v1

    .line 1114
    :cond_0
    invoke-virtual {v2, p1, p2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 1115
    sub-int/2addr v1, v0

    .line 1116
    add-int/2addr p2, v0

    .line 1117
    goto :goto_0

    .line 1118
    :cond_1
    return-void
.end method

.method public final c()Ljava/nio/ByteBuffer;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1185
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1186
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    .line 1193
    :goto_0
    return-object v0

    .line 1187
    :cond_0
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    .line 1188
    invoke-direct {p0}, Ldyi;->f()V

    .line 1191
    :cond_1
    iget v0, p0, Ldyi;->k:I

    if-lez v0, :cond_2

    iget-boolean v0, p0, Ldyi;->j:Z

    if-nez v0, :cond_5

    iget v0, p0, Ldyi;->k:I

    iget v1, p0, Ldyi;->i:I

    div-int/lit8 v1, v1, 0x2

    if-gt v0, v1, :cond_5

    iget v0, p0, Ldyi;->i:I

    shr-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldyi;->i:I

    :goto_1
    iget v0, p0, Ldyi;->i:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ldyi;->i:I

    iget v0, p0, Ldyi;->i:I

    const/high16 v1, 0x400000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Ldyi;->i:I

    :cond_2
    iget v0, p0, Ldyi;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    iget v0, p0, Ldyi;->l:I

    iget v1, p0, Ldyi;->i:I

    if-ge v0, v1, :cond_3

    iget v0, p0, Ldyi;->l:I

    iput v0, p0, Ldyi;->i:I

    :cond_3
    iget v0, p0, Ldyi;->i:I

    add-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ldyi;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Ldyi;->a:Ljava/util/logging/Logger;

    iget v2, p0, Ldyi;->i:I

    add-int/lit8 v2, v2, 0x2

    iget v3, p0, Ldyi;->k:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x48

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "allocating "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " byte input buffer last byte buf size: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    :cond_4
    iput-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    .line 1192
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1193
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    goto/16 :goto_0

    .line 1191
    :cond_5
    iget v0, p0, Ldyi;->k:I

    iget v1, p0, Ldyi;->i:I

    add-int/lit8 v1, v1, 0x2

    if-ne v0, v1, :cond_6

    iget v0, p0, Ldyi;->i:I

    shl-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldyi;->i:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyi;->j:Z

    goto :goto_1

    :cond_6
    iput-boolean v2, p0, Ldyi;->j:Z

    goto :goto_1
.end method

.method public final d()Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 1209
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1211
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    .line 1224
    :goto_0
    return-object v0

    .line 1212
    :cond_0
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_1

    .line 1213
    const/4 v0, 0x0

    iput-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    .line 1216
    :cond_1
    iget-object v1, p0, Ldyi;->b:Ljava/util/List;

    monitor-enter v1

    .line 1217
    :try_start_0
    iget-object v0, p0, Ldyi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1218
    iget-object v0, p0, Ldyi;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    .line 1219
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1221
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1224
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 1221
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1235
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1236
    const/4 v0, 0x0

    iput-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    .line 1238
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1129
    const/4 v0, 0x0

    .line 1130
    if-nez v0, :cond_4

    .line 1131
    const-string v0, "US-ASCII"

    move-object v1, v0

    .line 1136
    :goto_0
    :try_start_0
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_0

    .line 1143
    iget-object v0, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    iget-object v3, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    add-int/2addr v0, v3

    .line 1144
    iget-object v3, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    iget-object v4, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1145
    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Ldyi;->c:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v4, v5, v0, v3, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1149
    :cond_0
    iget-object v3, p0, Ldyi;->b:Ljava/util/List;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150
    :try_start_1
    iget-object v0, p0, Ldyi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 1151
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v5

    .line 1152
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    .line 1153
    new-instance v7, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-direct {v7, v0, v5, v6, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1155
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1169
    :catch_0
    move-exception v0

    .line 1170
    const-string v1, "unable to convert IOBuffer to string: "

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1172
    :goto_2
    return-object v0

    .line 1155
    :cond_1
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1158
    :try_start_4
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_2

    .line 1165
    iget-object v0, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v0

    .line 1166
    iget-object v3, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 1167
    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Ldyi;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v4, v5, v0, v3, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_4} :catch_0

    .line 1172
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1170
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto/16 :goto_0
.end method
