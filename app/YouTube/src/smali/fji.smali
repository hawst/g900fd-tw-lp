.class public final Lfji;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lheg;

.field public b:Lhgh;

.field public c:Lgzw;

.field public d:Lfil;

.field public e:Lfik;

.field public f:Lfng;

.field public g:Lfko;

.field private h:Lhgf;

.field private i:Lgzy;

.field private j:Lgzx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lfji;->a:Lheg;

    .line 34
    return-void
.end method

.method public constructor <init>(Lheg;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lheg;

    iput-object v0, p0, Lfji;->a:Lheg;

    .line 46
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lfji;->a:Lheg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lfji;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->e:Lgzv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lfji;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->d:Lhoh;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lhgf;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0}, Lfji;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->g:Lhgf;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->g:Lhgf;

    .line 167
    :goto_0
    return-object v0

    .line 164
    :cond_0
    iget-object v0, p0, Lfji;->h:Lhgf;

    if-nez v0, :cond_1

    .line 165
    new-instance v0, Lhgf;

    invoke-direct {v0}, Lhgf;-><init>()V

    iput-object v0, p0, Lfji;->h:Lhgf;

    .line 167
    :cond_1
    iget-object v0, p0, Lfji;->h:Lhgf;

    goto :goto_0
.end method

.method public final e()Lgzy;
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lfji;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->j:Lgzy;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->j:Lgzy;

    .line 199
    :goto_0
    return-object v0

    .line 195
    :cond_0
    iget-object v0, p0, Lfji;->i:Lgzy;

    if-nez v0, :cond_1

    .line 196
    new-instance v0, Lgzy;

    invoke-direct {v0}, Lgzy;-><init>()V

    iput-object v0, p0, Lfji;->i:Lgzy;

    .line 199
    :cond_1
    iget-object v0, p0, Lfji;->i:Lgzy;

    goto :goto_0
.end method

.method public final f()Lgzx;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lfji;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->l:Lgzx;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->l:Lgzx;

    .line 211
    :goto_0
    return-object v0

    .line 207
    :cond_0
    iget-object v0, p0, Lfji;->j:Lgzx;

    if-nez v0, :cond_1

    .line 208
    new-instance v0, Lgzx;

    invoke-direct {v0}, Lgzx;-><init>()V

    iput-object v0, p0, Lfji;->j:Lgzx;

    .line 211
    :cond_1
    iget-object v0, p0, Lfji;->j:Lgzx;

    goto :goto_0
.end method
