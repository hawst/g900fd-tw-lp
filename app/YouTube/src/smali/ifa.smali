.class public final enum Lifa;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lifa;

.field public static final enum b:Lifa;

.field public static final enum c:Lifa;

.field public static final enum d:Lifa;

.field public static final enum e:Lifa;

.field public static final enum f:Lifa;

.field public static final enum g:Lifa;

.field public static final enum h:Lifa;

.field private static enum i:Lifa;

.field private static final synthetic j:[Lifa;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Lifa;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v3}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->i:Lifa;

    .line 39
    new-instance v0, Lifa;

    const-string v1, "ORIENTATION"

    invoke-direct {v0, v1, v4}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->a:Lifa;

    .line 50
    new-instance v0, Lifa;

    const-string v1, "BYTE_SEGMENTS"

    invoke-direct {v0, v1, v5}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->b:Lifa;

    .line 56
    new-instance v0, Lifa;

    const-string v1, "ERROR_CORRECTION_LEVEL"

    invoke-direct {v0, v1, v6}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->c:Lifa;

    .line 61
    new-instance v0, Lifa;

    const-string v1, "ISSUE_NUMBER"

    invoke-direct {v0, v1, v7}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->d:Lifa;

    .line 67
    new-instance v0, Lifa;

    const-string v1, "SUGGESTED_PRICE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->e:Lifa;

    .line 73
    new-instance v0, Lifa;

    const-string v1, "POSSIBLE_COUNTRY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->f:Lifa;

    .line 78
    new-instance v0, Lifa;

    const-string v1, "UPC_EAN_EXTENSION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->g:Lifa;

    .line 83
    new-instance v0, Lifa;

    const-string v1, "PDF417_EXTRA_METADATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lifa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lifa;->h:Lifa;

    .line 25
    const/16 v0, 0x9

    new-array v0, v0, [Lifa;

    sget-object v1, Lifa;->i:Lifa;

    aput-object v1, v0, v3

    sget-object v1, Lifa;->a:Lifa;

    aput-object v1, v0, v4

    sget-object v1, Lifa;->b:Lifa;

    aput-object v1, v0, v5

    sget-object v1, Lifa;->c:Lifa;

    aput-object v1, v0, v6

    sget-object v1, Lifa;->d:Lifa;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lifa;->e:Lifa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lifa;->f:Lifa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lifa;->g:Lifa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lifa;->h:Lifa;

    aput-object v2, v0, v1

    sput-object v0, Lifa;->j:[Lifa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lifa;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lifa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lifa;

    return-object v0
.end method

.method public static values()[Lifa;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lifa;->j:[Lifa;

    invoke-virtual {v0}, [Lifa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lifa;

    return-object v0
.end method
