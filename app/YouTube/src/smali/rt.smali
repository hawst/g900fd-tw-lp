.class public abstract Lrt;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Lry;

.field private final d:Landroid/os/Messenger;

.field private final e:Lrw;

.field private final f:Lrx;

.field private g:Lrm;

.field private h:Lrl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "MediaRouteProviderSrv"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lrt;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lrt;->b:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Lry;

    invoke-direct {v0, p0}, Lry;-><init>(Lrt;)V

    iput-object v0, p0, Lrt;->c:Lry;

    .line 86
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lrt;->c:Lry;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lrt;->d:Landroid/os/Messenger;

    .line 87
    new-instance v0, Lrw;

    invoke-direct {v0, p0}, Lrw;-><init>(Lrt;)V

    iput-object v0, p0, Lrt;->e:Lrw;

    .line 88
    new-instance v0, Lrx;

    invoke-direct {v0, p0}, Lrx;-><init>(Lrt;)V

    iput-object v0, p0, Lrt;->f:Lrx;

    .line 89
    return-void
.end method

.method static synthetic a(Lrt;Landroid/os/Messenger;)I
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->c(Landroid/os/Messenger;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    invoke-static {p0}, Lrt;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/os/Messenger;I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 56
    if-eqz p1, :cond_0

    move-object v0, p0

    move v2, p1

    move v3, v1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lrt;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 56
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lrt;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Lrt;Lrr;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 56
    if-eqz p1, :cond_1

    iget-object v4, p1, Lrr;->a:Landroid/os/Bundle;

    :goto_0
    iget-object v0, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v2

    :goto_1
    if-ge v7, v8, :cond_2

    iget-object v0, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lrv;

    iget-object v0, v6, Lrv;->a:Landroid/os/Messenger;

    const/4 v1, 0x5

    move v3, v2

    invoke-static/range {v0 .. v5}, Lrt;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    sget-boolean v0, Lrt;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Sent descriptor change event, descriptor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_1
    move-object v4, v5

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Landroid/os/Messenger;II)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 138
    if-lez p3, :cond_3

    .line 139
    invoke-direct {p0, p1}, Lrt;->c(Landroid/os/Messenger;)I

    move-result v0

    .line 140
    if-gez v0, :cond_3

    .line 141
    new-instance v0, Lrv;

    invoke-direct {v0, p0, p1, p3}, Lrv;-><init>(Lrt;Landroid/os/Messenger;I)V

    .line 142
    invoke-virtual {v0}, Lrv;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 143
    iget-object v1, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    .line 145
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Registered, version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    if-eqz p2, :cond_1

    .line 148
    iget-object v0, p0, Lrt;->g:Lrm;

    iget-object v0, v0, Lrm;->g:Lrr;

    .line 149
    const/4 v1, 0x2

    if-eqz v0, :cond_2

    iget-object v4, v0, Lrr;->a:Landroid/os/Bundle;

    :goto_0
    move-object v0, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lrt;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    .line 157
    :cond_1
    :goto_1
    return v3

    :cond_2
    move-object v4, v5

    .line 149
    goto :goto_0

    .line 157
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lrt;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 56
    iget-object v0, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v5

    move v1, v5

    move-object v3, v2

    :goto_0
    if-ge v6, v7, :cond_2

    iget-object v0, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    iget-object v4, v0, Lrv;->b:Lrl;

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Lrl;->a()Lrz;

    move-result-object v0

    invoke-virtual {v0}, Lrz;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lrl;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    invoke-virtual {v4}, Lrl;->b()Z

    move-result v0

    or-int/2addr v0, v1

    if-nez v3, :cond_1

    move-object v1, v2

    move-object v2, v4

    :goto_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-object v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v2, :cond_6

    new-instance v1, Lsa;

    invoke-virtual {v3}, Lrl;->a()Lrz;

    move-result-object v2

    invoke-direct {v1, v2}, Lsa;-><init>(Lrz;)V

    :goto_2
    invoke-virtual {v4}, Lrl;->a()Lrz;

    move-result-object v2

    invoke-virtual {v1, v2}, Lsa;->a(Lrz;)Lsa;

    move-object v2, v3

    goto :goto_1

    :cond_2
    if-eqz v2, :cond_3

    new-instance v3, Lrl;

    invoke-virtual {v2}, Lsa;->a()Lrz;

    move-result-object v0

    invoke-direct {v3, v0, v1}, Lrl;-><init>(Lrz;Z)V

    :cond_3
    iget-object v0, p0, Lrt;->h:Lrl;

    if-eq v0, v3, :cond_5

    iget-object v0, p0, Lrt;->h:Lrl;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lrt;->h:Lrl;

    invoke-virtual {v0, v3}, Lrl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iput-object v3, p0, Lrt;->h:Lrl;

    iget-object v0, p0, Lrt;->g:Lrm;

    invoke-virtual {v0, v3}, Lrm;->a(Lrl;)V

    const/4 v0, 0x1

    :goto_3
    return v0

    :cond_5
    move v0, v5

    goto :goto_3

    :cond_6
    move-object v1, v2

    goto :goto_2

    :cond_7
    move v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method static synthetic a(Lrt;Landroid/os/Messenger;I)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Unregistered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Lrv;->b()V

    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lrt;Landroid/os/Messenger;II)Z
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lrt;->a(Landroid/os/Messenger;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lrt;Landroid/os/Messenger;III)Z
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Lrv;->a(I)Lrq;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Lrq;->a(I)V

    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Route volume changed, controllerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", volume="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lrt;Landroid/os/Messenger;IILandroid/content/Intent;)Z
    .locals 8

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, p3}, Lrv;->a(I)Lrq;

    move-result-object v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Lru;

    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lru;-><init>(Lrt;Lrv;ILandroid/content/Intent;Landroid/os/Messenger;I)V

    :cond_0
    invoke-virtual {v7, p4, v0}, Lrq;->a(Landroid/content/Intent;Lse;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lrt;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Route control request delivered, controllerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", intent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lrt;Landroid/os/Messenger;IILjava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v2, v3, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p3}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v2

    if-gez v2, :cond_1

    iget-object v2, v3, Lrv;->d:Lrt;

    iget-object v2, v2, Lrt;->g:Lrm;

    invoke-virtual {v2, p4}, Lrm;->a(Ljava/lang/String;)Lrq;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v4, v3, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v4, p3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Route controller created, controllerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", routeId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lrt;Landroid/os/Messenger;ILrl;)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Lrv;->a(Lrl;)Z

    move-result v1

    sget-boolean v2, Lrt;->a:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Set discovery request, request="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", actuallyChanged="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compositeDiscoveryRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lrt;->h:Lrl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/os/Messenger;)Lrv;
    .locals 2

    .prologue
    .line 414
    invoke-direct {p0, p1}, Lrt;->c(Landroid/os/Messenger;)I

    move-result v0

    .line 415
    if-ltz v0, :cond_0

    iget-object v1, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lrt;)Lrw;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lrt;->e:Lrw;

    return-object v0
.end method

.method private static b(Landroid/os/Messenger;I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 436
    if-eqz p1, :cond_0

    .line 437
    const/4 v1, 0x1

    const/4 v3, 0x0

    move-object v0, p0

    move v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lrt;->b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    .line 439
    :cond_0
    return-void
.end method

.method private static b(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 443
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 444
    iput p1, v0, Landroid/os/Message;->what:I

    .line 445
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 446
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 447
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 448
    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 450
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 456
    :goto_0
    return-void

    .line 453
    :catch_0
    move-exception v0

    .line 454
    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not send message to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lrt;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 455
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lrt;Landroid/os/Messenger;)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Binder died"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Lrv;->b()V

    :cond_1
    return-void
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 56
    sget-boolean v0, Lrt;->a:Z

    return v0
.end method

.method static synthetic b(Lrt;Landroid/os/Messenger;II)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v0, v3, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrq;

    if-eqz v0, :cond_1

    iget-object v4, v3, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v4, p3}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {v0}, Lrq;->a()V

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    sget-boolean v0, Lrt;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route controller released, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method static synthetic b(Lrt;Landroid/os/Messenger;III)Z
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Lrv;->a(I)Lrq;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Lrq;->b(I)V

    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Route volume updated, controllerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", delta="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/os/Messenger;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 419
    iget-object v0, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    .line 420
    :goto_0
    if-ge v1, v3, :cond_2

    .line 421
    iget-object v0, p0, Lrt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrv;

    .line 422
    iget-object v0, v0, Lrv;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v4

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    move v0, v1

    .line 426
    :goto_2
    return v0

    :cond_0
    move v0, v2

    .line 422
    goto :goto_1

    .line 420
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 426
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method static synthetic c(Lrt;Landroid/os/Messenger;II)Z
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Lrv;->a(I)Lrq;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lrq;->b()V

    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Route selected, controllerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Client connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lrt;Landroid/os/Messenger;II)Z
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lrt;->b(Landroid/os/Messenger;)Lrv;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Lrv;->a(I)Lrq;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lrq;->c()V

    sget-boolean v1, Lrt;->a:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": Route unselected, controllerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, p2}, Lrt;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Lrm;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Lrt;->g:Lrm;

    if-nez v0, :cond_1

    .line 115
    invoke-virtual {p0}, Lrt;->a()Lrm;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_1

    .line 117
    iget-object v1, v0, Lrm;->b:Lrp;

    iget-object v1, v1, Lrp;->a:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-virtual {p0}, Lrt;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCreateMediaRouteProvider() returned a provider whose package name does not match the package name of the service.  A media route provider service can only export its own media route providers.  Provider package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".  Service package name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lrt;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_0
    iput-object v0, p0, Lrt;->g:Lrm;

    .line 127
    iget-object v0, p0, Lrt;->g:Lrm;

    iget-object v1, p0, Lrt;->f:Lrx;

    invoke-virtual {v0, v1}, Lrm;->a(Lrn;)V

    .line 130
    :cond_1
    iget-object v0, p0, Lrt;->g:Lrm;

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Lrt;->d:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
