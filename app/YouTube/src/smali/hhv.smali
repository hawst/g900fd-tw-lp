.class public final Lhhv;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field private b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63739
    invoke-direct {p0}, Lidf;-><init>()V

    .line 63742
    const-string v0, ""

    iput-object v0, p0, Lhhv;->a:Ljava/lang/String;

    .line 63745
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhhv;->b:[B

    .line 63739
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 63793
    const/4 v0, 0x0

    .line 63794
    iget-object v1, p0, Lhhv;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 63795
    const/4 v0, 0x1

    iget-object v1, p0, Lhhv;->a:Ljava/lang/String;

    .line 63796
    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 63798
    :cond_0
    iget-object v1, p0, Lhhv;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 63799
    const/4 v1, 0x3

    iget-object v2, p0, Lhhv;->b:[B

    .line 63800
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 63802
    :cond_1
    iget-object v1, p0, Lhhv;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63803
    iput v0, p0, Lhhv;->J:I

    .line 63804
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 63735
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhhv;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhhv;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhhv;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhv;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhhv;->b:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 63781
    iget-object v0, p0, Lhhv;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63782
    const/4 v0, 0x1

    iget-object v1, p0, Lhhv;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 63784
    :cond_0
    iget-object v0, p0, Lhhv;->b:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63785
    const/4 v0, 0x3

    iget-object v1, p0, Lhhv;->b:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 63787
    :cond_1
    iget-object v0, p0, Lhhv;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 63789
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63757
    if-ne p1, p0, :cond_1

    .line 63762
    :cond_0
    :goto_0
    return v0

    .line 63758
    :cond_1
    instance-of v2, p1, Lhhv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 63759
    :cond_2
    check-cast p1, Lhhv;

    .line 63760
    iget-object v2, p0, Lhhv;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhv;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhhv;->b:[B

    iget-object v3, p1, Lhhv;->b:[B

    .line 63761
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhv;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhv;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 63762
    goto :goto_0

    .line 63760
    :cond_4
    iget-object v2, p0, Lhhv;->a:Ljava/lang/String;

    iget-object v3, p1, Lhhv;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 63761
    :cond_5
    iget-object v2, p0, Lhhv;->I:Ljava/util/List;

    iget-object v3, p1, Lhhv;->I:Ljava/util/List;

    .line 63762
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63766
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 63768
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhv;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 63769
    iget-object v2, p0, Lhhv;->b:[B

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 63775
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhhv;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_1
    add-int/2addr v0, v1

    .line 63776
    return v0

    .line 63768
    :cond_1
    iget-object v0, p0, Lhhv;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    .line 63771
    :goto_2
    iget-object v3, p0, Lhhv;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 63772
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhhv;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 63771
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 63775
    :cond_3
    iget-object v1, p0, Lhhv;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
