.class final Liob;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liql;


# instance fields
.field a:I

.field b:B

.field c:I

.field d:I

.field e:S

.field private final f:Lipu;


# direct methods
.method public constructor <init>(Lipu;)V
    .locals 0

    .prologue
    .line 604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 605
    iput-object p1, p0, Liob;->f:Lipu;

    .line 606
    return-void
.end method


# virtual methods
.method public final a()Liqm;
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Liob;->f:Lipu;

    invoke-interface {v0}, Lipu;->a()Liqm;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lipq;J)J
    .locals 10

    .prologue
    const/4 v9, 0x1

    const-wide/16 v0, -0x1

    const/4 v8, 0x0

    .line 609
    :cond_0
    iget v2, p0, Liob;->d:I

    if-nez v2, :cond_5

    .line 610
    iget-object v2, p0, Liob;->f:Lipu;

    iget-short v3, p0, Liob;->e:S

    int-to-long v4, v3

    invoke-interface {v2, v4, v5}, Lipu;->f(J)V

    .line 611
    iput-short v8, p0, Liob;->e:S

    .line 612
    iget-byte v2, p0, Liob;->b:B

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_2

    .line 620
    :cond_1
    :goto_0
    return-wide v0

    .line 613
    :cond_2
    iget v2, p0, Liob;->c:I

    iget-object v3, p0, Liob;->f:Lipu;

    invoke-static {v3}, Lioa;->a(Lipu;)I

    move-result v3

    iput v3, p0, Liob;->d:I

    iput v3, p0, Liob;->a:I

    iget-object v3, p0, Liob;->f:Lipu;

    invoke-interface {v3}, Lipu;->g()B

    move-result v3

    int-to-byte v3, v3

    iget-object v4, p0, Liob;->f:Lipu;

    invoke-interface {v4}, Lipu;->g()B

    move-result v4

    int-to-byte v4, v4

    iput-byte v4, p0, Liob;->b:B

    invoke-static {}, Lioa;->b()Ljava/util/logging/Logger;

    move-result-object v4

    sget-object v5, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {}, Lioa;->b()Ljava/util/logging/Logger;

    move-result-object v4

    iget v5, p0, Liob;->c:I

    iget v6, p0, Liob;->a:I

    iget-byte v7, p0, Liob;->b:B

    invoke-static {v9, v5, v6, v3, v7}, Lioc;->a(ZIIBB)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :cond_3
    iget-object v4, p0, Liob;->f:Lipu;

    invoke-interface {v4}, Lipu;->i()I

    move-result v4

    const v5, 0x7fffffff

    and-int/2addr v4, v5

    iput v4, p0, Liob;->c:I

    const/16 v4, 0x9

    if-eq v3, v4, :cond_4

    const-string v0, "%s != TYPE_CONTINUATION"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    iget v3, p0, Liob;->c:I

    if-eq v3, v2, :cond_0

    const-string v0, "TYPE_CONTINUATION streamId changed"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lioa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 617
    :cond_5
    iget-object v2, p0, Liob;->f:Lipu;

    iget v3, p0, Liob;->d:I

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Lipu;->b(Lipq;J)J

    move-result-wide v2

    .line 618
    cmp-long v4, v2, v0

    if-eqz v4, :cond_1

    .line 619
    iget v0, p0, Liob;->d:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Liob;->d:I

    move-wide v0, v2

    .line 620
    goto :goto_0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 628
    return-void
.end method
