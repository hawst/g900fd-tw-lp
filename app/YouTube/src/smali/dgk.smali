.class public final Ldgk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Levc;


# instance fields
.field final a:Ldaq;

.field final b:Lhog;

.field final c:I

.field final d:Z

.field volatile e:Z

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Ldgn;

.field private final h:Ljava/util/Set;


# direct methods
.method public constructor <init>(Levn;Ljava/util/concurrent/Executor;Ldaq;Lcws;Lhog;Ldgn;)V
    .locals 3

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Ldgk;->f:Ljava/util/concurrent/Executor;

    .line 88
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    iput-object v0, p0, Ldgk;->a:Ldaq;

    .line 89
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhog;

    iput-object v0, p0, Ldgk;->b:Lhog;

    .line 90
    iput-object p6, p0, Ldgk;->g:Ldgn;

    .line 93
    invoke-static {p5}, La;->b(Lhog;)Lhqt;

    move-result-object v0

    .line 92
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqt;

    .line 94
    invoke-static {v0}, La;->a(Lhqt;)Z

    move-result v1

    iput-boolean v1, p0, Ldgk;->d:Z

    .line 97
    invoke-static {v0}, La;->b(Lhqt;)I

    move-result v1

    iput v1, p0, Ldgk;->c:I

    .line 98
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Ldgk;->h:Ljava/util/Set;

    .line 99
    iget v1, v0, Lhqt;->c:I

    if-lez v1, :cond_1

    .line 100
    new-instance v0, Ldgh;

    invoke-direct {v0, p1, p4, p5, p0}, Ldgh;-><init>(Levn;Lcws;Lhog;Levc;)V

    .line 103
    invoke-virtual {v0}, Ldgh;->a()V

    .line 104
    iget-object v1, p0, Ldgk;->h:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget v0, v0, Lhqt;->c:I

    if-gez v0, :cond_0

    .line 106
    new-instance v0, Ldgf;

    invoke-direct {v0, p1, p5, p0}, Ldgf;-><init>(Levn;Lhog;Levc;)V

    .line 108
    iget-object v1, v0, Ldgf;->a:Levn;

    invoke-virtual {v1, v0}, Levn;->a(Ljava/lang/Object;)V

    .line 109
    new-instance v1, Ldgh;

    invoke-direct {v1, p1, p4, p5, p0}, Ldgh;-><init>(Levn;Lcws;Lhog;Levc;)V

    .line 112
    invoke-virtual {v1}, Ldgh;->a()V

    .line 113
    iget-object v2, p0, Ldgk;->h:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v0, p0, Ldgk;->h:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 143
    invoke-virtual {p0}, Ldgk;->b()V

    .line 144
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Ldgk;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levb;

    invoke-virtual {v0}, Levb;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ldgk;->e:Z

    if-nez v0, :cond_1

    .line 151
    iget-object v0, p0, Ldgk;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Ldgl;

    invoke-direct {v1, p0}, Ldgl;-><init>(Ldgk;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Ldgk;->g:Ldgn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldgk;->g:Ldgn;

    invoke-interface {v0, p0}, Ldgn;->a(Ldgk;)V

    .line 153
    :cond_1
    return-void

    .line 150
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
