.class public Lcmp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcyc;


# instance fields
.field private a:Landroid/content/ContentResolver;

.field public final b:Lfhw;

.field private c:Ljava/lang/String;

.field private final d:Lezs;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcmq;

    invoke-direct {v0, p0}, Lcmq;-><init>(Lcmp;)V

    iput-object v0, p0, Lcmp;->d:Lezs;

    .line 47
    iput-object p1, p0, Lcmp;->a:Landroid/content/ContentResolver;

    .line 48
    iput-object p2, p0, Lcmp;->c:Ljava/lang/String;

    .line 49
    new-instance v0, Lfhw;

    invoke-direct {v0}, Lfhw;-><init>()V

    iput-object v0, p0, Lcmp;->b:Lfhw;

    .line 50
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 126
    const-string v0, "device_supports_1080p_playback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final B()Z
    .locals 2

    .prologue
    .line 131
    const-string v0, "device_supports_1440p_playback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final C()Z
    .locals 2

    .prologue
    .line 136
    const-string v0, "device_supports_2160p_playback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final D()Lfhw;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcmp;->b:Lfhw;

    return-object v0
.end method

.method public final E()Z
    .locals 2

    .prologue
    .line 177
    const-string v0, "can_use_texture_surface"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final F()I
    .locals 2

    .prologue
    .line 187
    const-string v0, "pudl_ad_frequency_cap"

    const/16 v1, 0x1a4

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final G()I
    .locals 2

    .prologue
    .line 192
    const-string v0, "pudl_ad_asset_frequency_cap"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final H()I
    .locals 2

    .prologue
    .line 197
    const-string v0, "pudl_ad_asset_time_to_live"

    const v1, 0x3f480

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final I()I
    .locals 2

    .prologue
    .line 202
    const-string v0, "pudl_ad_lact_skippable"

    const/16 v1, 0x708

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final J()I
    .locals 2

    .prologue
    .line 208
    const-string v0, "pudl_ad_lact_nonskippable"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final K()J
    .locals 4

    .prologue
    .line 213
    const-string v0, "task_master_delay_before_startup_millis"

    const-wide/16 v2, 0x2710

    invoke-virtual {p0, v0, v2, v3}, Lcmp;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final L()I
    .locals 2

    .prologue
    .line 220
    const-string v0, "offline_resync_continuation_deferred_service_threshold_seconds"

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final M()Z
    .locals 2

    .prologue
    .line 225
    const-string v0, "attempt_offline_resync_on_expired_continuation"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final N()I
    .locals 2

    .prologue
    .line 240
    const-string v0, "maximum_consecutive_skipped_unplayable_videos"

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final O()J
    .locals 4

    .prologue
    .line 255
    const-string v0, "ads_timeout_millis"

    const-wide/16 v2, 0x3a98

    invoke-virtual {p0, v0, v2, v3}, Lcmp;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final P()J
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcmp;->d:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final Q()Z
    .locals 2

    .prologue
    .line 265
    const-string v0, "block_watch_next_on_player"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final R()J
    .locals 4

    .prologue
    .line 270
    const-string v0, "minimum_free_disk_space_bytes"

    const-wide/32 v2, 0x3200000

    invoke-virtual {p0, v0, v2, v3}, Lcmp;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final S()Z
    .locals 2

    .prologue
    .line 292
    const-string v0, "interaction_logging_enabled"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final T()I
    .locals 2

    .prologue
    .line 297
    const-string v0, "interaction_logging_queue_flush_time_ms"

    const v1, 0xea60

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final U()I
    .locals 2

    .prologue
    .line 304
    const-string v0, "interaction_logging_max_queue_size"

    const/16 v1, 0x1e

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final V()Z
    .locals 2

    .prologue
    .line 311
    const-string v0, "interaction_logging_dev_logging_enabled"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final W()Z
    .locals 2

    .prologue
    .line 316
    const-string v0, "enable_mdx_logs"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final X()Ljava/lang/String;
    .locals 2

    .prologue
    .line 321
    const-string v0, "cast_route_id"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final Y()J
    .locals 4

    .prologue
    .line 326
    const-string v0, "cache_ms_param_millis"

    sget-object v1, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcmp;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final Z()Z
    .locals 2

    .prologue
    .line 331
    const-string v0, "enable_mdx_https_browser_channel"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;I)I
    .locals 5

    .prologue
    .line 153
    iget-object v0, p0, Lcmp;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcmp;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;J)J
    .locals 6

    .prologue
    .line 157
    iget-object v0, p0, Lcmp;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcmp;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2, p3}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 149
    iget-object v0, p0, Lcmp;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcmp;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/SharedPreferences;)V
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcmp;->b:Lfhw;

    const-string v1, "com.google.android.libraries.youtube.innertube.pref.inner_tube_config"

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lfji;

    invoke-direct {v1}, Lfji;-><init>()V

    iput-object v1, v0, Lfhw;->a:Lfji;

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    new-instance v2, Lheg;

    invoke-direct {v2}, Lheg;-><init>()V

    invoke-static {v2, v1}, Lidh;->a(Lidh;[B)Lidh;

    new-instance v1, Lfji;

    invoke-direct {v1, v2}, Lfji;-><init>(Lheg;)V

    iput-object v1, v0, Lfhw;->a:Lfji;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v1, Lfji;

    invoke-direct {v1}, Lfji;-><init>()V

    iput-object v1, v0, Lfhw;->a:Lfji;

    goto :goto_0

    :catch_1
    move-exception v1

    new-instance v1, Lfji;

    invoke-direct {v1}, Lfji;-><init>()V

    iput-object v1, v0, Lfhw;->a:Lfji;

    goto :goto_0

    :catch_2
    move-exception v1

    new-instance v1, Lfji;

    invoke-direct {v1}, Lfji;-><init>()V

    iput-object v1, v0, Lfhw;->a:Lfji;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 5

    .prologue
    .line 145
    iget-object v0, p0, Lcmp;->a:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcmp;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final aa()Z
    .locals 2

    .prologue
    .line 361
    const-string v0, "enable_account_switcher"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ab()Z
    .locals 2

    .prologue
    .line 371
    const-string v0, "enable_mdx_wake_up_screen"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final ac()Lfko;
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    iget-object v1, v0, Lfji;->g:Lfko;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lfji;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lfji;->a:Lheg;

    iget-object v1, v1, Lheg;->b:Lhhi;

    iget-object v1, v1, Lhhi;->c:Lhmy;

    if-eqz v1, :cond_1

    new-instance v1, Lfko;

    iget-object v2, v0, Lfji;->a:Lheg;

    iget-object v2, v2, Lheg;->b:Lhhi;

    iget-object v2, v2, Lhhi;->c:Lhmy;

    invoke-direct {v1, v2}, Lfko;-><init>(Lhmy;)V

    iput-object v1, v0, Lfji;->g:Lfko;

    :cond_0
    :goto_0
    iget-object v0, v0, Lfji;->g:Lfko;

    return-object v0

    :cond_1
    new-instance v1, Lfko;

    new-instance v2, Lhmy;

    invoke-direct {v2}, Lhmy;-><init>()V

    invoke-direct {v1, v2}, Lfko;-><init>(Lhmy;)V

    iput-object v1, v0, Lfji;->g:Lfko;

    goto :goto_0
.end method

.method public final ad()Z
    .locals 2

    .prologue
    .line 380
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lfji;->a:Lheg;

    iget-object v1, v1, Lheg;->b:Lhhi;

    iget-object v1, v1, Lhhi;->n:Lgzu;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->n:Lgzu;

    iget-boolean v0, v0, Lgzu;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ae()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 385
    iget-object v2, p0, Lcmp;->b:Lfhw;

    iget-object v2, v2, Lfhw;->a:Lfji;

    invoke-virtual {v2}, Lfji;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v2, Lfji;->a:Lheg;

    iget-object v3, v3, Lheg;->b:Lhhi;

    iget-object v3, v3, Lhhi;->m:Lhht;

    if-eqz v3, :cond_0

    iget-object v2, v2, Lfji;->a:Lheg;

    iget-object v2, v2, Lheg;->b:Lhhi;

    iget-object v2, v2, Lhhi;->m:Lhht;

    iget-boolean v2, v2, Lhht;->a:Z

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final af()Z
    .locals 3

    .prologue
    .line 395
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    iget-object v1, v0, Lfji;->e:Lfik;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lfji;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lfik;

    iget-object v2, v0, Lfji;->a:Lheg;

    iget-object v2, v2, Lheg;->b:Lhhi;

    iget-object v2, v2, Lhhi;->o:Lgzz;

    invoke-direct {v1, v2}, Lfik;-><init>(Lgzz;)V

    iput-object v1, v0, Lfji;->e:Lfik;

    :cond_0
    :goto_0
    iget-object v0, v0, Lfji;->e:Lfik;

    iget-boolean v0, v0, Lfik;->a:Z

    return v0

    :cond_1
    new-instance v1, Lfik;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lfik;-><init>(Lgzz;)V

    iput-object v1, v0, Lfji;->e:Lfik;

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 167
    invoke-static {p1}, La;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 170
    :goto_0
    return v0

    :cond_0
    const-string v0, "csi_enabled"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    return v0
.end method

.method public m()I
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    return v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    return v0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    return v0
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    return v0
.end method

.method public q()Z
    .locals 1

    .prologue
    .line 346
    const/4 v0, 0x0

    return v0
.end method

.method public final r()Lfxh;
    .locals 2

    .prologue
    .line 59
    const-string v0, "gdata_version"

    const-string v1, "2.1"

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfxh;->a(Ljava/lang/String;)Lfxh;

    move-result-object v0

    return-object v0
.end method

.method public final s()Z
    .locals 2

    .prologue
    .line 64
    const-string v0, "enable_device_retention"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2

    .prologue
    .line 69
    const-string v0, "experiment_id"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 74
    const-string v0, "use_innertube_playlist_service"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final v()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    const-string v0, "adsense_query_domain"

    const-string v1, "googleads.g.doubleclick.net"

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    const-string v0, "adsense_query_domain"

    const-string v1, "/pagead/ads"

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    const-string v0, "device_country"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 116
    const-string v0, "is_low_end_mobile_network"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 121
    const-string v0, "device_supports_720p_playback"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcmp;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
