.class public final Laaf;
.super Laah;
.source "SourceFile"

# interfaces
.implements Lyn;


# instance fields
.field private b:I

.field private c:I

.field private d:J

.field private e:I

.field private f:I

.field private g:I

.field private h:J

.field private i:J

.field private j:J

.field private o:J

.field private p:I

.field private q:J

.field private r:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0, p1}, Laah;-><init>(Ljava/lang/String;)V

    .line 71
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    const/16 v3, 0x14

    .line 183
    invoke-virtual {p0, p1}, Laaf;->c(Ljava/nio/ByteBuffer;)V

    .line 186
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaf;->e:I

    .line 189
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaf;->p:I

    .line 190
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaf;->q:J

    .line 192
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaf;->b:I

    .line 193
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaf;->c:I

    .line 195
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaf;->f:I

    .line 197
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaf;->g:I

    .line 199
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaf;->d:J

    .line 200
    iget-object v0, p0, Laaf;->l:Ljava/lang/String;

    const-string v1, "mlpa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    iget-wide v0, p0, Laaf;->d:J

    const/16 v2, 0x10

    ushr-long/2addr v0, v2

    iput-wide v0, p0, Laaf;->d:J

    .line 205
    :cond_0
    iget v0, p0, Laaf;->e:I

    if-lez v0, :cond_1

    .line 206
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaf;->h:J

    .line 207
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaf;->i:J

    .line 208
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaf;->j:J

    .line 209
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaf;->o:J

    .line 211
    :cond_1
    iget v0, p0, Laaf;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 213
    new-array v0, v3, [B

    iput-object v0, p0, Laaf;->r:[B

    .line 214
    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->get(I)B

    .line 216
    :cond_2
    invoke-virtual {p0, p1}, Laaf;->d(Ljava/nio/ByteBuffer;)V

    .line 218
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 251
    invoke-virtual {p0, p1}, Laaf;->e(Ljava/nio/ByteBuffer;)V

    .line 252
    iget v0, p0, Laaf;->e:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 253
    iget v0, p0, Laaf;->p:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 254
    iget-wide v0, p0, Laaf;->q:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 255
    iget v0, p0, Laaf;->b:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 256
    iget v0, p0, Laaf;->c:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 257
    iget v0, p0, Laaf;->f:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 258
    iget v0, p0, Laaf;->g:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 260
    iget-object v0, p0, Laaf;->l:Ljava/lang/String;

    const-string v1, "mlpa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    iget-wide v0, p0, Laaf;->d:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 266
    :goto_0
    iget v0, p0, Laaf;->e:I

    if-lez v0, :cond_0

    .line 267
    iget-wide v0, p0, Laaf;->h:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 268
    iget-wide v0, p0, Laaf;->i:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 269
    iget-wide v0, p0, Laaf;->j:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 270
    iget-wide v0, p0, Laaf;->o:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 273
    :cond_0
    iget v0, p0, Laaf;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 274
    iget-object v0, p0, Laaf;->r:[B

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 276
    :cond_1
    invoke-virtual {p0, p1}, Laaf;->f(Ljava/nio/ByteBuffer;)V

    .line 277
    return-void

    .line 263
    :cond_2
    iget-wide v0, p0, Laaf;->d:J

    const/16 v2, 0x10

    shl-long/2addr v0, v2

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    goto :goto_0
.end method

.method protected final d_()J
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 223
    const-wide/16 v4, 0x1c

    iget v0, p0, Laaf;->e:I

    if-lez v0, :cond_1

    const-wide/16 v0, 0x10

    :goto_0
    add-long/2addr v0, v4

    .line 225
    iget v4, p0, Laaf;->e:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    const-wide/16 v2, 0x14

    :cond_0
    add-long/2addr v0, v2

    .line 226
    iget-object v2, p0, Laaf;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 227
    invoke-interface {v0}, Lyj;->l_()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 228
    goto :goto_1

    :cond_1
    move-wide v0, v2

    .line 223
    goto :goto_0

    .line 229
    :cond_2
    return-wide v2
.end method

.method public final toString()Ljava/lang/String;
    .locals 22

    .prologue
    .line 234
    const-string v2, "AudioSampleEntry{bytesPerSample="

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Laaf;->o:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Laaf;->j:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Laaf;->i:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Laaf;->h:J

    move-object/from16 v0, p0

    iget v3, v0, Laaf;->g:I

    move-object/from16 v0, p0

    iget v12, v0, Laaf;->f:I

    move-object/from16 v0, p0

    iget v13, v0, Laaf;->e:I

    move-object/from16 v0, p0

    iget-wide v14, v0, Laaf;->d:J

    move-object/from16 v0, p0

    iget v0, v0, Laaf;->c:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Laaf;->b:I

    move/from16 v17, v0

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Laah;->a:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    move/from16 v0, v20

    add-int/lit16 v0, v0, 0x12d

    move/from16 v20, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v21

    add-int v20, v20, v21

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", bytesPerFrame="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", bytesPerPacket="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", samplesPerPacket="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", packetSize="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", compressionId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", soundVersion="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sampleRate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sampleSize="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", channelCount="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", boxes="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
