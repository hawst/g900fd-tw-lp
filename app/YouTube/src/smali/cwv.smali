.class final Lcwv;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcws;

.field private c:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcws;)V
    .locals 0

    .prologue
    .line 992
    iput-object p1, p0, Lcwv;->b:Lcws;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 999
    iget-object v0, p0, Lcwv;->c:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 1000
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcwv;->c:Landroid/os/Handler;

    .line 1002
    :cond_0
    iget-boolean v0, p0, Lcwv;->a:Z

    if-nez v0, :cond_1

    .line 1003
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1004
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1005
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1006
    iget-object v1, p0, Lcwv;->b:Lcws;

    iget-object v1, v1, Lcws;->a:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1007
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcwv;->a:Z

    .line 1009
    :cond_1
    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1020
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1021
    iget-object v0, p0, Lcwv;->b:Lcws;

    iget-object v0, v0, Lcws;->c:Lgeh;

    invoke-interface {v0}, Lgeh;->m()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcwv;->b:Lcws;

    iget-object v0, v0, Lcws;->c:Lgeh;

    invoke-interface {v0}, Lgeh;->i()I

    move-result v0

    if-lez v0, :cond_0

    .line 1025
    iget-object v0, p0, Lcwv;->c:Landroid/os/Handler;

    new-instance v1, Lcww;

    invoke-direct {v1, p0}, Lcww;-><init>(Lcwv;)V

    const-wide/32 v2, 0x2bf20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1036
    :cond_0
    :goto_0
    return-void

    .line 1034
    :cond_1
    iget-object v0, p0, Lcwv;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method
