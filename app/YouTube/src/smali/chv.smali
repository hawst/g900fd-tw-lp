.class public final Lchv;
.super Lcbd;
.source "SourceFile"


# instance fields
.field private final b:Landroid/view/View;

.field private final c:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private final d:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private final e:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1, p3, p4, p5}, Lcbd;-><init>(Landroid/content/Context;Lfhz;Lfdw;Lfrz;)V

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lchv;->e:Lfsj;

    .line 43
    const v0, 0x7f040106

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lchv;->b:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lchv;->b:Landroid/view/View;

    const v1, 0x7f0802ed

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lchv;->c:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 46
    iget-object v0, p0, Lchv;->b:Landroid/view/View;

    const v1, 0x7f0802ee

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lchv;->d:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 49
    iget-object v0, p0, Lchv;->b:Landroid/view/View;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 50
    return-void
.end method

.method private a(Lfsg;Lfms;)Landroid/view/View;
    .locals 5

    .prologue
    .line 54
    invoke-virtual {p2}, Lfms;->b()Lhog;

    move-result-object v0

    .line 55
    invoke-virtual {p2}, Lfms;->c()Lhog;

    move-result-object v1

    .line 57
    iget-object v2, p0, Lchv;->c:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v3, p2, Lfms;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_0

    iget-object v3, p2, Lfms;->a:Lhvs;

    iget-object v3, v3, Lhvs;->a:Lhgz;

    if-eqz v3, :cond_0

    iget-object v3, p2, Lfms;->a:Lhvs;

    iget-object v3, v3, Lhvs;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p2, Lfms;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v3, p2, Lfms;->b:Ljava/lang/CharSequence;

    .line 58
    iget-object v4, p2, Lfms;->c:Ljava/lang/CharSequence;

    if-nez v4, :cond_1

    iget-object v4, p2, Lfms;->a:Lhvs;

    iget-object v4, v4, Lhvs;->b:Lhgz;

    if-eqz v4, :cond_1

    iget-object v4, p2, Lfms;->a:Lhvs;

    iget-object v4, v4, Lhvs;->b:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p2, Lfms;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v4, p2, Lfms;->c:Ljava/lang/CharSequence;

    .line 57
    invoke-virtual {p0, v3, v4, v0}, Lchv;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lhog;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lchv;->d:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v2, p2, Lfms;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_2

    iget-object v2, p2, Lfms;->a:Lhvs;

    iget-object v2, v2, Lhvs;->d:Lhgz;

    if-eqz v2, :cond_2

    iget-object v2, p2, Lfms;->a:Lhvs;

    iget-object v2, v2, Lhvs;->d:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, p2, Lfms;->d:Ljava/lang/CharSequence;

    :cond_2
    iget-object v2, p2, Lfms;->d:Ljava/lang/CharSequence;

    .line 60
    iget-object v3, p2, Lfms;->e:Ljava/lang/CharSequence;

    if-nez v3, :cond_3

    iget-object v3, p2, Lfms;->a:Lhvs;

    iget-object v3, v3, Lhvs;->e:Lhgz;

    if-eqz v3, :cond_3

    iget-object v3, p2, Lfms;->a:Lhvs;

    iget-object v3, v3, Lhvs;->e:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p2, Lfms;->e:Ljava/lang/CharSequence;

    :cond_3
    iget-object v3, p2, Lfms;->e:Ljava/lang/CharSequence;

    .line 59
    invoke-virtual {p0, v2, v3, v1}, Lchv;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lhog;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v0, p0, Lchv;->e:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfms;

    invoke-direct {p0, p1, p2}, Lchv;->a(Lfsg;Lfms;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfms;

    invoke-direct {p0, p1, p2}, Lchv;->a(Lfsg;Lfms;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
