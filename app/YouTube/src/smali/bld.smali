.class final Lbld;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lbla;


# direct methods
.method constructor <init>(Lbla;)V
    .locals 0

    .prologue
    .line 1075
    iput-object p1, p0, Lbld;->a:Lbla;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1080
    iget-object v0, p0, Lbld;->a:Lbla;

    iget-object v0, v0, Lbla;->f:Lbjx;

    iget-object v1, p0, Lbld;->a:Lbla;

    iget-object v1, v1, Lbla;->d:Lgit;

    invoke-virtual {v0, v1}, Lbjx;->a(Lgit;)J

    move-result-wide v0

    .line 1081
    cmp-long v2, v0, v6

    if-lez v2, :cond_0

    .line 1082
    iget-object v2, p0, Lbld;->a:Lbla;

    .line 1083
    iget-object v2, v2, Lbla;->i:Lgml;

    iget-object v2, v2, Lgml;->f:Lgnp;

    invoke-virtual {v2}, Lgnp;->c()J

    move-result-wide v2

    .line 1084
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1085
    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    add-long/2addr v0, v2

    .line 1086
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x35

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "leastRecentLastRefreshTimestamp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    .line 1087
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Expected refresh time: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    .line 1088
    iget-object v2, p0, Lbld;->a:Lbla;

    iget-object v2, v2, Lbla;->e:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    .line 1089
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x23

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Current clock: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lezp;->e(Ljava/lang/String;)V

    .line 1090
    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    .line 1091
    const-string v0, "Stale videos found; attempting a resync"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 1092
    iget-object v0, p0, Lbld;->a:Lbla;

    iget-object v0, v0, Lbla;->o:Lgmw;

    invoke-interface {v0, v6, v7}, Lgmw;->a(J)V

    .line 1095
    :cond_0
    return-void
.end method
