.class public final Lgur;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgtn;


# static fields
.field private static final a:Ljava/nio/charset/Charset;


# instance fields
.field private final b:Lguf;

.field private final c:Lfhw;

.field private final d:Lgvh;

.field private final e:Lgix;

.field private final f:Lfbu;

.field private final g:Lieh;

.field private final h:Lief;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lgur;->a:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Lguf;Lfhw;Lgvh;Lgix;Lfbu;)V
    .locals 7

    .prologue
    .line 67
    new-instance v0, Lidp;

    invoke-direct {v0}, Lidp;-><init>()V

    .line 73
    new-instance v1, Liej;

    invoke-direct {v1, v0}, Liej;-><init>(Lidm;)V

    new-instance v6, Liei;

    iget-object v0, v1, Liej;->a:Lidm;

    invoke-direct {v6, v0}, Liei;-><init>(Lidm;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 67
    invoke-direct/range {v0 .. v6}, Lgur;-><init>(Lguf;Lfhw;Lgvh;Lgix;Lfbu;Lieh;)V

    .line 74
    return-void
.end method

.method private constructor <init>(Lguf;Lfhw;Lgvh;Lgix;Lfbu;Lieh;)V
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lguf;

    iput-object v0, p0, Lgur;->b:Lguf;

    .line 84
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhw;

    iput-object v0, p0, Lgur;->c:Lfhw;

    .line 85
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgvh;

    iput-object v0, p0, Lgur;->d:Lgvh;

    .line 86
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lgur;->e:Lgix;

    .line 87
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbu;

    iput-object v0, p0, Lgur;->f:Lfbu;

    .line 88
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    iput-object v0, p0, Lgur;->g:Lieh;

    .line 89
    new-instance v0, Lieg;

    invoke-direct {v0}, Lieg;-><init>()V

    const-wide/16 v2, 0x258

    iput-wide v2, v0, Lieg;->a:J

    new-instance v1, Lief;

    invoke-direct {v1, v0}, Lief;-><init>(Lieg;)V

    iput-object v1, p0, Lgur;->h:Lief;

    .line 90
    return-void
.end method

.method private a(Ljava/lang/String;Lgvd;)Lguc;
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 117
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    :try_start_0
    invoke-direct {p0, p2}, Lgur;->a(Lgvd;)Liea;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lwb; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 129
    new-instance v1, Lgut;

    invoke-direct {v1, p0, p1}, Lgut;-><init>(Lgur;Ljava/lang/String;)V

    const/16 v2, 0x400

    invoke-interface {v0, v1, v2}, Liea;->a(Liee;I)V

    .line 130
    invoke-static {v0}, Lgur;->a(Liea;)Ljava/lang/String;

    move-result-object v1

    .line 131
    if-nez v1, :cond_0

    .line 132
    invoke-static {v3}, La;->e(I)Lguc;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    .line 124
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    invoke-static {v0}, La;->e(I)Lguc;

    move-result-object v0

    goto :goto_0

    .line 127
    :catch_1
    move-exception v0

    invoke-static {v3}, La;->e(I)Lguc;

    move-result-object v0

    goto :goto_0

    .line 134
    :cond_0
    new-instance v0, Lgus;

    invoke-direct {v0, p0, v1}, Lgus;-><init>(Lgur;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lgur;)Lguf;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lgur;->b:Lguf;

    return-object v0
.end method

.method private a(Lgvd;)Liea;
    .locals 10

    .prologue
    .line 202
    iget-object v0, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v0}, Lgvi;->b()Lgvi;

    move-result-object v2

    .line 203
    iget-object v0, v2, Lgvi;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 204
    iget-object v1, p0, Lgur;->d:Lgvh;

    invoke-virtual {v1, v0}, Lgvh;->a(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    .line 205
    new-instance v4, Lidt;

    const/high16 v3, 0x100000

    invoke-direct {v4, v1, v3}, Lidt;-><init>(Ljava/io/InputStream;I)V

    .line 207
    iget-object v1, v2, Lgvi;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    iget-object v0, p0, Lgur;->g:Lieh;

    iget-object v1, v2, Lgvi;->f:Ljava/lang/String;

    iget-object v2, p0, Lgur;->h:Lief;

    invoke-interface {v0, v1, v4, v2}, Lieh;->a(Ljava/lang/String;Lidl;Lief;)Liea;

    move-result-object v0

    .line 254
    :goto_0
    return-object v0

    .line 214
    :cond_0
    new-instance v3, Lidn;

    invoke-direct {v3}, Lidn;-><init>()V

    .line 216
    :try_start_0
    iget-object v1, p0, Lgur;->d:Lgvh;

    invoke-virtual {v1, v0}, Lgvh;->b(Landroid/net/Uri;)J

    move-result-wide v0

    .line 217
    const-wide/16 v6, -0x1

    cmp-long v5, v0, v6

    if-eqz v5, :cond_1

    .line 218
    const-string v5, "X-Goog-Upload-Header-Content-Length"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Lidn;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 224
    :cond_1
    :goto_1
    iget-object v0, p0, Lgur;->e:Lgix;

    iget-object v1, v2, Lgvi;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lgix;->a(Ljava/lang/String;)Lgit;

    move-result-object v0

    .line 225
    if-nez v0, :cond_2

    .line 226
    new-instance v0, Lwb;

    invoke-direct {v0}, Lwb;-><init>()V

    throw v0

    .line 230
    :cond_2
    :try_start_1
    iget-object v1, p0, Lgur;->f:Lfbu;

    .line 231
    iget-object v0, v0, Lgit;->b:Lgiv;

    .line 230
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5}, Lfbu;->a(Lgiv;Z)Ljava/util/concurrent/Future;

    move-result-object v0

    .line 231
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbw;

    .line 232
    invoke-virtual {v0}, Lfbw;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 233
    invoke-virtual {v0}, Lfbw;->c()Landroid/util/Pair;

    move-result-object v1

    .line 234
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Lidn;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0

    .line 242
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 246
    :try_start_2
    const-string v0, "frontendUploadId"

    iget-object v1, v2, Lgvi;->e:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 247
    const-string v0, "deviceDisplayName"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, " "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 248
    const-string v0, "fileId"

    iget-object v1, v2, Lgvi;->b:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 254
    iget-object v0, p0, Lgur;->g:Lieh;

    iget-object v1, p0, Lgur;->c:Lfhw;

    .line 255
    invoke-virtual {v1}, Lfhw;->d()Lfng;

    move-result-object v1

    iget-object v1, v1, Lfng;->d:Ljava/lang/String;

    const-string v2, "POST"

    .line 259
    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lgur;->h:Lief;

    .line 254
    invoke-interface/range {v0 .. v6}, Lieh;->a(Ljava/lang/String;Ljava/lang/String;Lidn;Lidl;Ljava/lang/String;Lief;)Liea;

    move-result-object v0

    goto/16 :goto_0

    .line 236
    :cond_3
    :try_start_3
    new-instance v0, Lwb;

    invoke-direct {v0}, Lwb;-><init>()V

    throw v0
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_0

    .line 239
    :catch_0
    move-exception v0

    new-instance v0, Lwb;

    invoke-direct {v0}, Lwb;-><init>()V

    throw v0

    .line 249
    :catch_1
    move-exception v0

    .line 251
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    goto/16 :goto_1
.end method

.method private static a(Liea;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 266
    :try_start_0
    invoke-interface {p0}, Liea;->a()Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lied;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 273
    invoke-virtual {v0}, Lied;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 303
    :cond_0
    :goto_0
    return-object v0

    .line 268
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 269
    :catch_1
    move-exception v0

    .line 270
    invoke-interface {p0}, Liea;->c()V

    .line 271
    throw v0

    .line 276
    :cond_1
    invoke-virtual {v0}, Lied;->b()Z

    move-result v2

    if-nez v2, :cond_2

    move-object v0, v1

    .line 277
    goto :goto_0

    .line 279
    :cond_2
    iget-object v0, v0, Lied;->b:Lido;

    .line 280
    iget v2, v0, Lido;->a:I

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_3

    move-object v0, v1

    .line 281
    goto :goto_0

    .line 285
    :cond_3
    :try_start_1
    iget-object v0, v0, Lido;->c:Ljava/io/InputStream;

    invoke-static {v0}, La;->b(Ljava/io/InputStream;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 294
    :try_start_2
    new-instance v2, Lorg/json/JSONObject;

    new-instance v3, Ljava/lang/String;

    sget-object v4, Lgur;->a:Ljava/nio/charset/Charset;

    invoke-direct {v3, v0, v4}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 295
    const-string v0, "status"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 296
    const-string v0, "scottyResourceId"

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v0

    .line 300
    const-string v2, "STATUS_SUCCESS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 301
    goto :goto_0

    .line 287
    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 298
    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)J
    .locals 4

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 43
    check-cast p1, Lgvd;

    if-eqz p1, :cond_0

    iget-object v2, p1, Lgvd;->a:Lgvi;

    invoke-virtual {v2}, Lgvi;->b()Lgvi;

    move-result-object v2

    iget v3, v2, Lgvi;->d:I

    if-nez v3, :cond_0

    iget-boolean v3, v2, Lgvi;->m:Z

    if-nez v3, :cond_0

    iget-boolean v3, v2, Lgvi;->n:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v3, v2, Lgvi;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, Lgvi;->e:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v2, v2, Lgvi;->h:Z

    if-nez v2, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;)Lguc;
    .locals 1

    .prologue
    .line 43
    check-cast p2, Lgvd;

    invoke-direct {p0, p1, p2}, Lgur;->a(Ljava/lang/String;Lgvd;)Lguc;

    move-result-object v0

    return-object v0
.end method
