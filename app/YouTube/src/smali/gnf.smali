.class public final enum Lgnf;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgnf;

.field public static final enum b:Lgnf;

.field public static final enum c:Lgnf;

.field private static e:Landroid/util/SparseArray;

.field private static final synthetic f:[Lgnf;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 40
    new-instance v1, Lgnf;

    const-string v2, "EMPTY"

    invoke-direct {v1, v2, v0, v0}, Lgnf;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lgnf;->a:Lgnf;

    .line 43
    new-instance v1, Lgnf;

    const-string v2, "FORECASTING"

    invoke-direct {v1, v2, v3, v3}, Lgnf;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lgnf;->b:Lgnf;

    .line 46
    new-instance v1, Lgnf;

    const-string v2, "FULL"

    invoke-direct {v1, v2, v4, v4}, Lgnf;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lgnf;->c:Lgnf;

    .line 38
    const/4 v1, 0x3

    new-array v1, v1, [Lgnf;

    sget-object v2, Lgnf;->a:Lgnf;

    aput-object v2, v1, v0

    sget-object v2, Lgnf;->b:Lgnf;

    aput-object v2, v1, v3

    sget-object v2, Lgnf;->c:Lgnf;

    aput-object v2, v1, v4

    sput-object v1, Lgnf;->f:[Lgnf;

    .line 60
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    sput-object v1, Lgnf;->e:Landroid/util/SparseArray;

    .line 61
    invoke-static {}, Lgnf;->values()[Lgnf;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 62
    sget-object v4, Lgnf;->e:Landroid/util/SparseArray;

    iget v5, v3, Lgnf;->d:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput p3, p0, Lgnf;->d:I

    .line 52
    return-void
.end method

.method public static a(I)Lgnf;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lgnf;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnf;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lgnf;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lgnf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgnf;

    return-object v0
.end method

.method public static values()[Lgnf;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lgnf;->f:[Lgnf;

    invoke-virtual {v0}, [Lgnf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgnf;

    return-object v0
.end method
