.class public final Ldkj;
.super Ldjp;
.source "SourceFile"


# instance fields
.field final l:Ldaw;

.field final m:Landroid/os/Handler;

.field final n:Lcyc;

.field o:Lgoh;

.field private final p:Ljava/util/concurrent/Executor;

.field private final q:Z

.field private r:Lhaz;

.field private s:Lgoh;

.field private volatile t:Ldkn;


# direct methods
.method public constructor <init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Lcyc;Ldlj;)V
    .locals 9

    .prologue
    .line 110
    move-object/from16 v0, p10

    iget-object v8, v0, Ldlj;->g:Ldkz;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object v7, p5

    invoke-direct/range {v1 .. v8}, Ldjp;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldaq;Ldkz;)V

    .line 117
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Ldkj;->p:Ljava/util/concurrent/Executor;

    .line 119
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcyc;

    iput-object v1, p0, Ldkj;->n:Lcyc;

    .line 120
    move-object/from16 v0, p10

    iget-object v1, v0, Ldlj;->a:Lfrl;

    iput-object v1, p0, Ldkj;->g:Lfrl;

    .line 121
    move-object/from16 v0, p10

    iget-object v1, v0, Ldlj;->b:Lfnx;

    iput-object v1, p0, Ldkj;->h:Lfnx;

    .line 122
    move-object/from16 v0, p10

    iget-object v1, v0, Ldlj;->c:Lgoh;

    iput-object v1, p0, Ldkj;->o:Lgoh;

    .line 123
    move-object/from16 v0, p10

    iget-object v1, v0, Ldlj;->d:Lgoh;

    iput-object v1, p0, Ldkj;->s:Lgoh;

    .line 124
    move-object/from16 v0, p10

    iget-boolean v1, v0, Ldlj;->e:Z

    iput-boolean v1, p0, Ldkj;->k:Z

    .line 125
    move-object/from16 v0, p10

    iget-boolean v1, v0, Ldlj;->f:Z

    iput-boolean v1, p0, Ldkj;->j:Z

    .line 126
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldaw;

    iput-object v1, p0, Ldkj;->l:Ldaw;

    .line 127
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Ldkj;->m:Landroid/os/Handler;

    .line 129
    const/4 v1, 0x1

    iput-boolean v1, p0, Ldkj;->q:Z

    .line 130
    sget-object v1, Lgok;->a:Lgok;

    invoke-virtual {p0, v1}, Ldkj;->a(Lgok;)V

    .line 131
    iget-object v1, p0, Ldkj;->g:Lfrl;

    if-eqz v1, :cond_0

    .line 132
    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {p0, v1}, Ldkj;->a(Lgok;)V

    .line 133
    iget-object v1, p0, Ldkj;->h:Lfnx;

    if-eqz v1, :cond_0

    .line 134
    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {p0, v1}, Ldkj;->a(Lgok;)V

    .line 137
    :cond_0
    invoke-virtual {p0}, Ldkj;->r()V

    .line 138
    invoke-virtual {p0}, Ldkj;->h()V

    .line 139
    return-void
.end method

.method public constructor <init>(Lcvq;Levn;Lcwq;Ljava/util/concurrent/Executor;Ldaq;Ldaw;Leyt;Lfac;Lcyc;ZLgoh;)V
    .locals 8

    .prologue
    .line 84
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p7

    move-object/from16 v6, p8

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Ldjp;-><init>(Lcvq;Levn;Lcwq;Leyt;Lfac;Ldaq;)V

    .line 85
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Ldkj;->p:Ljava/util/concurrent/Executor;

    .line 86
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldaw;

    iput-object v1, p0, Ldkj;->l:Ldaw;

    .line 87
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcyc;

    iput-object v1, p0, Ldkj;->n:Lcyc;

    .line 88
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Ldkj;->m:Landroid/os/Handler;

    .line 89
    move/from16 v0, p10

    iput-boolean v0, p0, Ldkj;->q:Z

    .line 90
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgoh;

    iput-object v1, p0, Ldkj;->s:Lgoh;

    .line 91
    sget-object v1, Lgok;->a:Lgok;

    invoke-virtual {p0, v1}, Ldkj;->a(Lgok;)V

    .line 92
    invoke-virtual {p0}, Ldkj;->h()V

    .line 93
    return-void
.end method

.method private a(Lgoh;ZZ)V
    .locals 6

    .prologue
    .line 480
    iget-object v0, p0, Ldkj;->i:Lfqg;

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    .line 481
    :cond_0
    new-instance v0, Lfqg;

    iget-object v1, p0, Ldkj;->e:Lfac;

    .line 482
    invoke-virtual {p1}, Lgoh;->e()[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lfqg;-><init>(Lfac;[B)V

    iput-object v0, p0, Ldkj;->i:Lfqg;

    .line 487
    :cond_1
    if-eqz p2, :cond_2

    .line 488
    invoke-virtual {p0}, Ldkj;->a()V

    .line 489
    sget-object v0, Lgok;->b:Lgok;

    invoke-virtual {p0, v0}, Ldkj;->a(Lgok;)V

    .line 492
    :cond_2
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgoh;

    iput-object v0, p0, Ldkj;->s:Lgoh;

    .line 493
    iget-object v0, p0, Ldkj;->s:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldkj;->s:Lgoh;

    .line 494
    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 495
    iget-object v0, p0, Ldkj;->s:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldkj;->s:Lgoh;

    .line 496
    iget-object v1, v1, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldkj;->s:Lgoh;

    .line 497
    iget-object v2, v2, Lgoh;->a:Leaa;

    iget v2, v2, Leaa;->d:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x48

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Malformed WatchEndpoint [videoId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ",playlistId="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",playlistIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 495
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 504
    :goto_0
    return-void

    .line 499
    :cond_3
    new-instance v0, Ldkn;

    iget-object v1, p0, Ldkj;->s:Lgoh;

    invoke-direct {v0, p0, v1, p2}, Ldkn;-><init>(Ldkj;Lgoh;Z)V

    iput-object v0, p0, Ldkj;->t:Ldkn;

    .line 502
    iget-object v0, p0, Ldkj;->p:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Ldkj;->t:Ldkn;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 313
    invoke-super {p0}, Ldjp;->a()V

    .line 314
    iget-object v1, p0, Ldkj;->t:Ldkn;

    if-eqz v1, :cond_0

    .line 315
    iget-object v1, p0, Ldkj;->t:Ldkn;

    iget-boolean v2, v1, Ldkn;->b:Z

    if-eqz v2, :cond_2

    iput-boolean v0, v1, Ldkn;->a:Z

    :goto_0
    if-eqz v0, :cond_1

    .line 316
    const/4 v0, 0x0

    iput-object v0, p0, Ldkj;->t:Ldkn;

    .line 323
    :cond_0
    iget-object v0, p0, Ldkj;->g:Lfrl;

    if-eqz v0, :cond_4

    .line 324
    iget-object v0, p0, Ldkj;->h:Lfnx;

    if-eqz v0, :cond_3

    .line 325
    sget-object v0, Lgok;->e:Lgok;

    iput-object v0, p0, Ldkj;->f:Lgok;

    .line 334
    :cond_1
    :goto_1
    return-void

    .line 315
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 327
    :cond_3
    sget-object v0, Lgok;->d:Lgok;

    iput-object v0, p0, Ldkj;->f:Lgok;

    goto :goto_1

    .line 331
    :cond_4
    iget-object v0, p0, Ldkj;->f:Lgok;

    sget-object v1, Lgok;->b:Lgok;

    if-ne v0, v1, :cond_1

    .line 332
    sget-object v0, Lgok;->a:Lgok;

    invoke-virtual {p0, v0}, Ldkj;->a(Lgok;)V

    goto :goto_1
.end method

.method public final a(Lgoh;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 380
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    iget-object v0, p1, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ldkj;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    sget-object v0, Ldan;->f:Ldan;

    invoke-virtual {p0, v0}, Ldkj;->a(Ldan;)V

    .line 385
    iget-object v0, p0, Ldkj;->b:Lcvq;

    invoke-interface {v0, v1, v1}, Lcvq;->a(ZI)V

    .line 386
    const/4 v0, 0x1

    .line 390
    :goto_0
    iget-object v2, p0, Ldkj;->c:Levn;

    new-instance v3, Ldal;

    iget-object v4, p1, Lgoh;->a:Leaa;

    iget-object v4, v4, Leaa;->c:Ljava/lang/String;

    invoke-direct {v3, v4}, Ldal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Levn;->c(Ljava/lang/Object;)V

    .line 391
    invoke-direct {p0, p1, v0, v1}, Ldkj;->a(Lgoh;ZZ)V

    .line 392
    return-void

    .line 388
    :cond_0
    sget-object v0, Lgok;->a:Lgok;

    invoke-virtual {p0, v0}, Ldkj;->a(Lgok;)V

    move v0, v1

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 338
    iput-boolean p1, p0, Ldkj;->k:Z

    .line 339
    invoke-virtual {p0}, Ldkj;->r()V

    .line 340
    return-void
.end method

.method protected final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 423
    iget-object v0, p0, Ldkj;->f:Lgok;

    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Ldkj;->o:Lgoh;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    iget-object v0, p0, Ldkj;->o:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    .line 430
    :goto_0
    return-object v0

    .line 426
    :cond_0
    iget-object v0, p0, Ldkj;->f:Lgok;

    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    iget-object v0, p0, Ldkj;->g:Lfrl;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    iget-object v0, p0, Ldkj;->g:Lfrl;

    iget-object v0, v0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 430
    :cond_1
    iget-object v0, p0, Ldkj;->s:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 344
    iput-boolean p1, p0, Ldkj;->j:Z

    .line 345
    invoke-virtual {p0}, Ldkj;->r()V

    .line 346
    return-void
.end method

.method protected final c()I
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Ldkj;->f:Lgok;

    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Ldkj;->o:Lgoh;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    iget-object v0, p0, Ldkj;->o:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->d:I

    .line 450
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Ldkj;->s:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->d:I

    goto :goto_0
.end method

.method protected final d()[B
    .locals 2

    .prologue
    .line 456
    iget-object v0, p0, Ldkj;->f:Lgok;

    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Ldkj;->o:Lgoh;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    iget-object v0, p0, Ldkj;->o:Lgoh;

    invoke-virtual {v0}, Lgoh;->e()[B

    move-result-object v0

    .line 460
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldkj;->s:Lgoh;

    invoke-virtual {v0}, Lgoh;->e()[B

    move-result-object v0

    goto :goto_0
.end method

.method protected final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Ldkj;->f:Lgok;

    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Ldkj;->o:Lgoh;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    iget-object v0, p0, Ldkj;->o:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->l:Ljava/lang/String;

    .line 470
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldkj;->s:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->l:Ljava/lang/String;

    goto :goto_0
.end method

.method public final synthetic f()Ldkg;
    .locals 8

    .prologue
    .line 54
    new-instance v0, Ldlj;

    iget-object v1, p0, Ldkj;->g:Lfrl;

    iget-object v2, p0, Ldkj;->h:Lfnx;

    iget-object v3, p0, Ldkj;->o:Lgoh;

    iget-object v4, p0, Ldkj;->s:Lgoh;

    iget-boolean v5, p0, Ldkj;->k:Z

    iget-boolean v6, p0, Ldkj;->j:Z

    invoke-super {p0}, Ldjp;->f()Ldkg;

    move-result-object v7

    check-cast v7, Ldkz;

    invoke-direct/range {v0 .. v7}, Ldlj;-><init>(Lfrl;Lfnx;Lgoh;Lgoh;ZZLdkz;)V

    return-object v0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Ldkj;->s:Lgoh;

    if-eqz v0, :cond_0

    .line 156
    invoke-super {p0}, Ldjp;->k()V

    .line 157
    iget-object v0, p0, Ldkj;->s:Lgoh;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ldkj;->a(Lgoh;ZZ)V

    .line 159
    :cond_0
    return-void
.end method

.method protected final m()Z
    .locals 1

    .prologue
    .line 418
    const/4 v0, 0x1

    return v0
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 167
    iget-object v0, p0, Ldkj;->t:Ldkn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkj;->t:Ldkn;

    new-instance v1, Ldkl;

    invoke-direct {v1, p0}, Ldkl;-><init>(Ldkj;)V

    invoke-virtual {v0, v1}, Ldkn;->a(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    :cond_0
    invoke-virtual {p0}, Ldkj;->t_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    invoke-super {p0}, Ldjp;->n()V

    .line 170
    new-instance v0, Lgoh;

    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->d:Lhog;

    invoke-direct {v0, v1}, Lgoh;-><init>(Lhog;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ldkj;->a(Lgoh;ZZ)V

    .line 176
    :cond_1
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 191
    iget-object v0, p0, Ldkj;->t:Ldkn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkj;->t:Ldkn;

    new-instance v1, Ldkm;

    invoke-direct {v1, p0}, Ldkm;-><init>(Ldkj;)V

    invoke-virtual {v0, v1}, Ldkn;->a(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    :cond_0
    invoke-virtual {p0}, Ldkj;->u_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    invoke-super {p0}, Ldjp;->o()V

    .line 194
    new-instance v0, Lgoh;

    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->e:Lhog;

    invoke-direct {v0, v1}, Lgoh;-><init>(Lhog;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ldkj;->a(Lgoh;ZZ)V

    .line 201
    :cond_1
    return-void
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 216
    iget-object v0, p0, Ldkj;->t:Ldkn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkj;->t:Ldkn;

    new-instance v1, Ldkk;

    invoke-direct {v1, p0}, Ldkk;-><init>(Ldkj;)V

    invoke-virtual {v0, v1}, Ldkn;->a(Ljava/lang/Runnable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 217
    :cond_0
    invoke-virtual {p0}, Ldkj;->v_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    invoke-super {p0}, Ldjp;->p()V

    .line 219
    new-instance v0, Lgoh;

    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->c:Lhog;

    invoke-direct {v0, v1}, Lgoh;-><init>(Lhog;)V

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Ldkj;->a(Lgoh;ZZ)V

    .line 225
    :cond_1
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 239
    iget-object v0, p0, Ldkj;->s:Lgoh;

    if-eqz v0, :cond_0

    .line 240
    invoke-super {p0}, Ldjp;->q()V

    .line 241
    iget-object v0, p0, Ldkj;->s:Lgoh;

    invoke-direct {p0, v0, v1, v1}, Ldkj;->a(Lgoh;ZZ)V

    .line 243
    :cond_0
    return-void
.end method

.method public final r()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 364
    iget-object v0, p0, Ldkj;->h:Lfnx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkj;->h:Lfnx;

    .line 365
    iget-object v0, v0, Lfnx;->f:Lflc;

    if-eqz v0, :cond_0

    .line 366
    iget-boolean v0, p0, Ldkj;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldkj;->h:Lfnx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkj;->h:Lfnx;

    iget-object v0, v0, Lfnx;->f:Lflc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldkj;->h:Lfnx;

    iget-object v0, v0, Lfnx;->f:Lflc;

    invoke-virtual {v0}, Lflc;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Ldkj;->j:Z

    .line 367
    iget-boolean v0, p0, Ldkj;->k:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldkj;->h:Lfnx;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldkj;->h:Lfnx;

    iget-object v0, v0, Lfnx;->f:Lflc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldkj;->h:Lfnx;

    iget-object v0, v0, Lfnx;->f:Lflc;

    invoke-virtual {v0}, Lflc;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    :goto_3
    iput-boolean v1, p0, Ldkj;->k:Z

    .line 368
    iget-object v0, p0, Ldkj;->h:Lfnx;

    iget-object v0, v0, Lfnx;->f:Lflc;

    iget-boolean v1, p0, Ldkj;->j:Z

    iget-boolean v2, p0, Ldkj;->k:Z

    iget-object v3, p0, Ldkj;->b:Lcvq;

    .line 369
    invoke-interface {v3}, Lcvq;->p()Z

    move-result v3

    .line 368
    invoke-virtual {v0, v1, v2, v3}, Lflc;->a(ZZZ)Lhaz;

    move-result-object v0

    iput-object v0, p0, Ldkj;->r:Lhaz;

    .line 370
    invoke-virtual {p0}, Ldkj;->g()V

    .line 372
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 366
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    .line 367
    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method public final s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Ldkj;->f:Lgok;

    sget-object v1, Lgok;->e:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    iget-object v0, p0, Ldkj;->o:Lgoh;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    iget-object v0, p0, Ldkj;->o:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    .line 440
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Ldkj;->s:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v0, v0, Leaa;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final s_()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 250
    iget-object v2, p0, Ldkj;->s:Lgoh;

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldkj;->f:Lgok;

    const/4 v3, 0x2

    new-array v3, v3, [Lgok;

    sget-object v4, Lgok;->d:Lgok;

    aput-object v4, v3, v1

    sget-object v4, Lgok;->e:Lgok;

    aput-object v4, v3, v0

    .line 251
    invoke-virtual {v2, v3}, Lgok;->a([Lgok;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 258
    :goto_0
    return v0

    .line 257
    :cond_1
    iget-object v2, p0, Ldkj;->s:Lgoh;

    invoke-direct {p0, v2, v1, v0}, Ldkj;->a(Lgoh;ZZ)V

    goto :goto_0
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Ldkj;->o:Lgoh;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Ldkj;->o:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget v0, v0, Leaa;->d:I

    .line 402
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final t_()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 266
    iget-object v1, p0, Ldkj;->r:Lhaz;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->d:Lhog;

    if-nez v1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v0

    .line 270
    :cond_1
    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->d:Lhog;

    iget-object v1, v1, Lhog;->i:Libf;

    if-nez v1, :cond_2

    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->d:Lhog;

    iget-object v1, v1, Lhog;->u:Libk;

    if-eqz v1, :cond_0

    .line 272
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final u_()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 282
    iget-object v1, p0, Ldkj;->r:Lhaz;

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->e:Lhog;

    if-nez v1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->e:Lhog;

    iget-object v1, v1, Lhog;->i:Libf;

    if-nez v1, :cond_2

    iget-object v1, p0, Ldkj;->r:Lhaz;

    iget-object v1, v1, Lhaz;->e:Lhog;

    iget-object v1, v1, Lhog;->u:Libk;

    if-eqz v1, :cond_0

    .line 288
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final v_()Z
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Ldkj;->r:Lhaz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkj;->r:Lhaz;

    iget-object v0, v0, Lhaz;->c:Lhog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldkj;->r:Lhaz;

    iget-object v0, v0, Lhaz;->c:Lhog;

    iget-object v0, v0, Lhog;->i:Libf;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldkj;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
