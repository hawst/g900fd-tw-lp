.class public final Lrc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lpv;


# instance fields
.field a:Landroid/support/v7/widget/Toolbar;

.field b:Ljava/lang/CharSequence;

.field c:Lmt;

.field d:Z

.field private e:I

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Z

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/CharSequence;

.field private n:Lub;

.field private o:I

.field private final p:Lqw;

.field private q:I

.field private r:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Z)V
    .locals 2

    .prologue
    .line 88
    const v0, 0x7f09007c

    const v1, 0x7f02000e

    invoke-direct {p0, p1, p2, v0, v1}, Lrc;-><init>(Landroid/support/v7/widget/Toolbar;ZII)V

    .line 90
    return-void
.end method

.method private constructor <init>(Landroid/support/v7/widget/Toolbar;ZII)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    iput v2, p0, Lrc;->o:I

    .line 84
    iput v2, p0, Lrc;->q:I

    .line 94
    iput-object p1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    .line 95
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->h:Ljava/lang/CharSequence;

    iput-object v0, p0, Lrc;->b:Ljava/lang/CharSequence;

    .line 96
    iget-object v0, p1, Landroid/support/v7/widget/Toolbar;->i:Ljava/lang/CharSequence;

    iput-object v0, p0, Lrc;->l:Ljava/lang/CharSequence;

    .line 97
    iget-object v0, p0, Lrc;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_e

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lrc;->k:Z

    .line 99
    if-eqz p2, :cond_f

    .line 100
    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v4, Lmd;->a:[I

    const v5, 0x7f010053

    invoke-static {v0, v3, v4, v5, v2}, Lrb;->a(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lrb;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v2}, Lrb;->b(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 104
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 105
    invoke-virtual {p0, v3}, Lrc;->b(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Lrb;->b(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 109
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 110
    iput-object v3, p0, Lrc;->l:Ljava/lang/CharSequence;

    iget v4, p0, Lrc;->e:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_1

    iget-object v4, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    .line 113
    :cond_1
    const/16 v3, 0x9

    invoke-virtual {v0, v3}, Lrb;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 114
    if-eqz v3, :cond_2

    .line 115
    iput-object v3, p0, Lrc;->i:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lrc;->n()V

    .line 118
    :cond_2
    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lrb;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 119
    if-eqz v3, :cond_3

    .line 120
    invoke-direct {p0, v3}, Lrc;->b(Landroid/graphics/drawable/Drawable;)V

    .line 123
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lrb;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 124
    if-eqz v3, :cond_4

    .line 125
    invoke-virtual {p0, v3}, Lrc;->a(Landroid/graphics/drawable/Drawable;)V

    .line 128
    :cond_4
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v2}, Lrb;->a(II)I

    move-result v3

    invoke-virtual {p0, v3}, Lrc;->b(I)V

    .line 130
    const/16 v3, 0xe

    invoke-virtual {v0, v3, v2}, Lrb;->e(II)I

    move-result v3

    .line 132
    if-eqz v3, :cond_5

    .line 133
    iget-object v4, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    iget-object v5, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4, v3, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lrc;->a(Landroid/view/View;)V

    .line 135
    iget v3, p0, Lrc;->e:I

    or-int/lit8 v3, v3, 0x10

    invoke-virtual {p0, v3}, Lrc;->b(I)V

    .line 138
    :cond_5
    invoke-virtual {v0, v1, v2}, Lrb;->d(II)I

    move-result v1

    .line 139
    if-lez v1, :cond_6

    .line 140
    iget-object v3, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v3}, Landroid/support/v7/widget/Toolbar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 141
    iput v1, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 142
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/Toolbar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    :cond_6
    const/16 v1, 0x15

    invoke-virtual {v0, v1, v6}, Lrb;->b(II)I

    move-result v1

    .line 147
    const/16 v3, 0x16

    invoke-virtual {v0, v3, v6}, Lrb;->b(II)I

    move-result v3

    .line 149
    if-gez v1, :cond_7

    if-ltz v3, :cond_8

    .line 150
    :cond_7
    iget-object v4, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget-object v4, v4, Landroid/support/v7/widget/Toolbar;->g:Lqc;

    invoke-virtual {v4, v1, v3}, Lqc;->a(II)V

    .line 154
    :cond_8
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Lrb;->e(II)I

    move-result v1

    .line 155
    if-eqz v1, :cond_9

    .line 156
    iget-object v3, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/content/Context;I)V

    .line 159
    :cond_9
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Lrb;->e(II)I

    move-result v1

    .line 161
    if-eqz v1, :cond_a

    .line 162
    iget-object v3, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v4, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v4}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Landroid/support/v7/widget/Toolbar;->b(Landroid/content/Context;I)V

    .line 165
    :cond_a
    const/16 v1, 0x1a

    invoke-virtual {v0, v1, v2}, Lrb;->e(II)I

    move-result v1

    .line 166
    if-eqz v1, :cond_b

    .line 167
    iget-object v2, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/Toolbar;->a(I)V

    .line 170
    :cond_b
    iget-object v1, v0, Lrb;->a:Landroid/content/res/TypedArray;

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 172
    invoke-virtual {v0}, Lrb;->a()Lqw;

    move-result-object v0

    iput-object v0, p0, Lrc;->p:Lqw;

    .line 179
    :goto_1
    iget v0, p0, Lrc;->q:I

    if-eq p3, v0, :cond_c

    iput p3, p0, Lrc;->q:I

    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->f()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget v0, p0, Lrc;->q:I

    invoke-virtual {p0, v0}, Lrc;->d(I)V

    .line 180
    :cond_c
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->f()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lrc;->m:Ljava/lang/CharSequence;

    .line 182
    iget-object v0, p0, Lrc;->p:Lqw;

    invoke-virtual {v0, p4}, Lqw;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, Lrc;->r:Landroid/graphics/drawable/Drawable;

    if-eq v1, v0, :cond_d

    iput-object v0, p0, Lrc;->r:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lrc;->p()V

    .line 184
    :cond_d
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lrd;

    invoke-direct {v1, p0}, Lrd;-><init>(Lrc;)V

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->h()V

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    return-void

    :cond_e
    move v0, v2

    .line 97
    goto/16 :goto_0

    .line 174
    :cond_f
    const/16 v0, 0xb

    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/ImageButton;

    if-eqz v2, :cond_11

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_10

    const/16 v0, 0xf

    :cond_10
    iput v0, p0, Lrc;->e:I

    .line 176
    new-instance v0, Lqw;

    invoke-virtual {p1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lqw;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lrc;->p:Lqw;

    goto :goto_1

    :cond_11
    move-object v1, v3

    .line 174
    goto :goto_2
.end method

.method private b(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lrc;->h:Landroid/graphics/drawable/Drawable;

    .line 351
    invoke-direct {p0}, Lrc;->n()V

    .line 352
    return-void
.end method

.method private c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 283
    iput-object p1, p0, Lrc;->b:Ljava/lang/CharSequence;

    .line 284
    iget v0, p0, Lrc;->e:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 287
    :cond_0
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 366
    const/4 v0, 0x0

    .line 367
    iget v1, p0, Lrc;->e:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    .line 368
    iget v0, p0, Lrc;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_5

    .line 369
    iget-object v0, p0, Lrc;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lrc;->i:Landroid/graphics/drawable/Drawable;

    .line 374
    :cond_0
    :goto_0
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    if-eqz v0, :cond_6

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    if-nez v2, :cond_1

    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    :cond_1
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/view/View;)V

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Landroid/view/View;)V

    :cond_2
    :goto_1
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    if-eqz v2, :cond_3

    iget-object v1, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 375
    :cond_3
    return-void

    .line 369
    :cond_4
    iget-object v0, p0, Lrc;->h:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 371
    :cond_5
    iget-object v0, p0, Lrc;->h:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 374
    :cond_6
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_1
.end method

.method private o()V
    .locals 2

    .prologue
    .line 654
    iget v0, p0, Lrc;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lrc;->m:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 656
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget v1, p0, Lrc;->q:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->b(I)V

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 658
    :cond_1
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lrc;->m:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->c(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 664
    iget v0, p0, Lrc;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    .line 665
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v0, p0, Lrc;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lrc;->j:Landroid/graphics/drawable/Drawable;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/Toolbar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 667
    :cond_0
    return-void

    .line 665
    :cond_1
    iget-object v0, p0, Lrc;->r:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 345
    if-eqz p1, :cond_0

    iget-object v0, p0, Lrc;->p:Lqw;

    invoke-virtual {v0, p1}, Lqw;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lrc;->b(Landroid/graphics/drawable/Drawable;)V

    .line 346
    return-void

    .line 345
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 631
    iput-object p1, p0, Lrc;->j:Landroid/graphics/drawable/Drawable;

    .line 632
    invoke-direct {p0}, Lrc;->p()V

    .line 633
    return-void
.end method

.method public final a(Landroid/view/Menu;Loh;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 409
    iget-object v0, p0, Lrc;->n:Lub;

    if-nez v0, :cond_0

    .line 410
    new-instance v0, Lub;

    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lub;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lrc;->n:Lub;

    .line 411
    iget-object v0, p0, Lrc;->n:Lub;

    .line 413
    :cond_0
    iget-object v0, p0, Lrc;->n:Lub;

    iput-object p2, v0, Lnm;->d:Loh;

    .line 414
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    check-cast p1, Lnr;

    iget-object v1, p0, Lrc;->n:Lub;

    if-nez p1, :cond_1

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v2, :cond_4

    :cond_1
    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->g()V

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->a:Lnr;

    if-eq v2, p1, :cond_4

    if-eqz v2, :cond_2

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->k:Lub;

    invoke-virtual {v2, v3}, Lnr;->b(Log;)V

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    invoke-virtual {v2, v3}, Lnr;->b(Log;)V

    :cond_2
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    if-nez v2, :cond_3

    new-instance v2, Lvw;

    invoke-direct {v2, v0}, Lvw;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    :cond_3
    iput-boolean v4, v1, Lub;->i:Z

    if-eqz p1, :cond_5

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {p1, v1, v2}, Lnr;->a(Log;Landroid/content/Context;)V

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {p1, v2, v3}, Lnr;->a(Log;Landroid/content/Context;)V

    :goto_0
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget v3, v0, Landroid/support/v7/widget/Toolbar;->f:I

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/ActionMenuView;->a(I)V

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ActionMenuView;->a(Lub;)V

    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->k:Lub;

    .line 415
    :cond_4
    return-void

    .line 414
    :cond_5
    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {v1, v2, v5}, Lub;->a(Landroid/content/Context;Lnr;)V

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    iget-object v3, v0, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {v2, v3, v5}, Lvw;->a(Landroid/content/Context;Lnr;)V

    invoke-virtual {v1, v4}, Lub;->b(Z)V

    iget-object v2, v0, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    invoke-virtual {v2, v4}, Lvw;->b(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lrc;->g:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lrc;->e:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lrc;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 589
    :cond_0
    iput-object p1, p0, Lrc;->g:Landroid/view/View;

    .line 590
    if-eqz p1, :cond_1

    iget v0, p0, Lrc;->e:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_1

    .line 591
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lrc;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 593
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 266
    iget-boolean v0, p0, Lrc;->k:Z

    if-nez v0, :cond_0

    .line 267
    invoke-direct {p0, p1}, Lrc;->c(Ljava/lang/CharSequence;)V

    .line 269
    :cond_0
    return-void
.end method

.method public final a(Lmt;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lrc;->c:Lmt;

    .line 261
    return-void
.end method

.method public final a(Lqd;)V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Lrc;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrc;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    if-ne v0, v1, :cond_0

    .line 469
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lrc;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    .line 471
    :cond_0
    iput-object p1, p0, Lrc;->f:Landroid/view/View;

    .line 472
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iput-boolean p1, v0, Landroid/support/v7/widget/Toolbar;->o:Z

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->requestLayout()V

    .line 495
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 429
    iget v0, p0, Lrc;->e:I

    .line 430
    xor-int/2addr v0, p1

    .line 431
    iput p1, p0, Lrc;->e:I

    .line 432
    if-eqz v0, :cond_3

    .line 433
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_0

    .line 434
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_4

    .line 435
    invoke-direct {p0}, Lrc;->p()V

    .line 436
    invoke-direct {p0}, Lrc;->o()V

    .line 442
    :cond_0
    :goto_0
    and-int/lit8 v1, v0, 0x3

    if-eqz v1, :cond_1

    .line 443
    invoke-direct {p0}, Lrc;->n()V

    .line 446
    :cond_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 447
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_5

    .line 448
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Lrc;->b:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v2, p0, Lrc;->l:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    .line 456
    :cond_2
    :goto_1
    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_3

    iget-object v0, p0, Lrc;->g:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 457
    and-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_6

    .line 458
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lrc;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->addView(Landroid/view/View;)V

    .line 464
    :cond_3
    :goto_2
    return-void

    .line 438
    :cond_4
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 451
    :cond_5
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->a(Ljava/lang/CharSequence;)V

    .line 452
    iget-object v1, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->b(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 460
    :cond_6
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lrc;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrc;->k:Z

    .line 279
    invoke-direct {p0, p1}, Lrc;->c(Ljava/lang/CharSequence;)V

    .line 280
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 602
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 603
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhj;->a(F)Lhj;

    move-result-object v0

    new-instance v1, Lre;

    invoke-direct {v1, p0}, Lre;-><init>(Lrc;)V

    invoke-virtual {v0, v1}, Lhj;->a(Lhv;)Lhj;

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    if-nez p1, :cond_0

    .line 619
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0}, Lfz;->i(Landroid/view/View;)Lhj;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lhj;->a(F)Lhj;

    move-result-object v0

    new-instance v1, Lrf;

    invoke-direct {v1, p0}, Lrf;-><init>(Lrc;)V

    invoke-virtual {v0, v1}, Lhj;->a(Lhv;)Lhj;

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->e()V

    .line 256
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 650
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lrc;->m:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lrc;->o()V

    .line 651
    return-void

    .line 650
    :cond_0
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget-boolean v0, v0, Landroid/support/v7/widget/ActionMenuView;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->a()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    iget-object v2, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v3, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v3, :cond_3

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget-object v3, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lub;

    if-eqz v3, :cond_2

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lub;

    iget-object v3, v2, Lub;->l:Lue;

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lub;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    :goto_2
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->b()Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 399
    iget-object v2, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v3, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    if-eqz v3, :cond_1

    iget-object v2, v2, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget-object v3, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lub;

    if-eqz v3, :cond_0

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lub;

    invoke-virtual {v2}, Lub;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lrc;->d:Z

    .line 405
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lrc;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->c()V

    .line 420
    return-void
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 424
    iget v0, p0, Lrc;->e:I

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 504
    const/4 v0, 0x0

    return v0
.end method
