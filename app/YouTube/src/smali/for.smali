.class public final Lfor;
.super Lgia;
.source "SourceFile"


# instance fields
.field private a:Lfoo;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Lgia;-><init>()V

    return-void
.end method

.method public constructor <init>(Lfoo;)V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Lgia;-><init>()V

    .line 176
    iput-object p1, p0, Lfor;->a:Lfoo;

    .line 177
    return-void
.end method

.method private static b(Lorg/json/JSONObject;I)Lfoo;
    .locals 4

    .prologue
    .line 193
    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 194
    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Unsupported version"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_0
    :try_start_0
    new-instance v1, Lfoo;

    new-instance v0, Leag;

    invoke-direct {v0}, Leag;-><init>()V

    const-string v2, "data_pb"

    .line 199
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 198
    invoke-static {v0, v2}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v0

    check-cast v0, Leag;

    invoke-direct {v1, v0}, Lfoo;-><init>(Leag;)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 200
    :catch_0
    move-exception v0

    .line 201
    new-instance v1, Lorg/json/JSONException;

    const-string v2, "Unable to parse proto: "

    invoke-virtual {v0}, Lidg;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x2

    return v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 167
    invoke-static {p1, p2}, Lfor;->b(Lorg/json/JSONObject;I)Lfoo;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 186
    const-string v0, "data_pb"

    iget-object v1, p0, Lfor;->a:Lfoo;

    .line 188
    invoke-static {v1}, Lfoo;->a(Lfoo;)Leag;

    move-result-object v1

    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 189
    return-void
.end method
