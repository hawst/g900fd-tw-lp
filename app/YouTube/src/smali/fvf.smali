.class public final Lfvf;
.super Lfud;
.source "SourceFile"


# instance fields
.field private final c:Lfsi;

.field private d:Lfqh;


# direct methods
.method public constructor <init>(Lfdg;Lfuu;Levn;Leyt;)V
    .locals 1

    .prologue
    .line 49
    .line 51
    invoke-virtual {p2}, Lfuu;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsl;

    .line 49
    invoke-direct {p0, p1, v0, p3, p4}, Lfud;-><init>(Lfdg;Lfsl;Levn;Leyt;)V

    .line 54
    const-class v0, Lfmc;

    invoke-virtual {p2, v0}, Lfuu;->a(Ljava/lang/Class;)V

    .line 55
    iget-object v0, p0, Lfud;->a:Lfsi;

    iput-object v0, p0, Lfvf;->c:Lfsi;

    .line 56
    invoke-virtual {p3, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method private b(Lfmc;)V
    .locals 6

    .prologue
    .line 70
    if-nez p1, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p1, Lfmc;->c:Ljava/util/List;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lfmc;->a:Lhsl;

    iget-object v1, v1, Lhsl;->d:[Lhsm;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p1, Lfmc;->c:Ljava/util/List;

    iget-object v0, p1, Lfmc;->a:Lhsl;

    iget-object v1, v0, Lhsl;->d:[Lhsm;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    iget-object v4, v3, Lhsm;->b:Lhoj;

    if-eqz v4, :cond_2

    iget-object v4, p1, Lfmc;->c:Ljava/util/List;

    new-instance v5, Lfjk;

    iget-object v3, v3, Lhsm;->b:Lhoj;

    invoke-direct {v5, v3}, Lfjk;-><init>(Lhoj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p1, Lfmc;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lfvf;->b(Ljava/util/List;)V

    .line 75
    invoke-virtual {p1}, Lfmc;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfvf;->a(Ljava/util/Collection;)V

    .line 76
    iget-object v0, p0, Lfvf;->d:Lfqh;

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p1, Lfmc;->b:Lfqh;

    iput-object v0, p0, Lfvf;->d:Lfqh;

    goto :goto_0
.end method

.method private handleVideoRemovedFromPlaylistEvent(Lffa;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lfvf;->c:Lfsi;

    new-instance v1, Lfvg;

    invoke-direct {v1, p0, p1}, Lfvg;-><init>(Lfvf;Lffa;)V

    invoke-virtual {v0, v1}, Lfsi;->a(Lewh;)V

    .line 107
    return-void
.end method


# virtual methods
.method public final a(Lfmc;)V
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lfvf;->h()V

    .line 61
    invoke-direct {p0, p1}, Lfvf;->b(Lfmc;)V

    .line 62
    iget-object v0, p1, Lfmc;->b:Lfqh;

    iput-object v0, p0, Lfvf;->d:Lfqh;

    .line 63
    return-void
.end method

.method protected final a(Lhel;Lfjl;)V
    .locals 3

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lfud;->a(Lhel;Lfjl;)V

    .line 84
    if-eqz p1, :cond_0

    iget-object v0, p1, Lhel;->e:Lhsl;

    if-nez v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    new-instance v0, Lfmc;

    iget-object v1, p1, Lhel;->e:Lhsl;

    iget-object v2, p0, Lfvf;->d:Lfqh;

    invoke-direct {v0, v1, v2}, Lfmc;-><init>(Lhsl;Lfqh;)V

    .line 90
    sget-object v1, Lfjl;->b:Lfjl;

    if-ne p2, v1, :cond_2

    .line 91
    invoke-virtual {p0, v0}, Lfvf;->a(Lfmc;)V

    goto :goto_0

    .line 93
    :cond_2
    invoke-direct {p0, v0}, Lfvf;->b(Lfmc;)V

    goto :goto_0
.end method
