.class public final Lffw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lfgi;

.field final b:Ljava/util/PriorityQueue;

.field public final c:Ljava/util/concurrent/CopyOnWriteArrayList;

.field public final d:Landroid/os/Handler;

.field final e:Z

.field final f:Z

.field g:Z

.field private final h:Ljava/lang/Runnable;

.field private final i:Ljava/lang/Runnable;

.field private final j:Ljava/lang/Runnable;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lfgi;ZZ)V
    .locals 4

    .prologue
    .line 233
    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Landroid/os/Handler;

    .line 234
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 233
    invoke-direct {p0, p1, v0, v1, v2}, Lffw;-><init>(Lfgi;ZZLandroid/os/Handler;)V

    .line 235
    return-void
.end method

.method public constructor <init>(Lfgi;ZZLandroid/os/Handler;)V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lffw;->g:Z

    .line 184
    new-instance v0, Lffx;

    invoke-direct {v0, p0}, Lffx;-><init>(Lffw;)V

    iput-object v0, p0, Lffw;->h:Ljava/lang/Runnable;

    .line 191
    new-instance v0, Lffy;

    invoke-direct {v0, p0}, Lffy;-><init>(Lffw;)V

    iput-object v0, p0, Lffw;->i:Ljava/lang/Runnable;

    .line 202
    new-instance v0, Lffz;

    invoke-direct {v0, p0}, Lffz;-><init>(Lffw;)V

    iput-object v0, p0, Lffw;->j:Ljava/lang/Runnable;

    .line 213
    new-instance v0, Lfga;

    invoke-direct {v0, p0}, Lfga;-><init>(Lffw;)V

    iput-object v0, p0, Lffw;->k:Ljava/lang/Runnable;

    .line 252
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgi;

    iput-object v0, p0, Lffw;->a:Lfgi;

    .line 253
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lffw;->b:Ljava/util/PriorityQueue;

    .line 254
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 255
    iput-boolean p2, p0, Lffw;->e:Z

    .line 256
    iput-boolean p3, p0, Lffw;->f:Z

    .line 257
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lffw;->d:Landroid/os/Handler;

    .line 258
    return-void
.end method

.method static a(Ljava/util/List;)Lhhg;
    .locals 6

    .prologue
    .line 375
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 377
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 378
    new-instance v3, Lhhg;

    invoke-direct {v3}, Lhhg;-><init>()V

    .line 379
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgh;

    .line 380
    iget-object v5, v0, Lfgh;->d:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 381
    iget-object v0, v0, Lfgh;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 382
    :cond_1
    iget-object v5, v0, Lfgh;->b:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 383
    new-instance v5, Lhye;

    invoke-direct {v5}, Lhye;-><init>()V

    .line 384
    iget-object v0, v0, Lfgh;->b:Ljava/lang/String;

    iput-object v0, v5, Lhye;->b:Ljava/lang/String;

    .line 385
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 386
    :cond_2
    iget-object v5, v0, Lfgh;->c:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 387
    new-instance v5, Lhye;

    invoke-direct {v5}, Lhye;-><init>()V

    .line 388
    iget-object v0, v0, Lfgh;->c:Ljava/lang/String;

    iput-object v0, v5, Lhye;->c:Ljava/lang/String;

    .line 389
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 392
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lhye;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhye;

    iput-object v0, v3, Lhhg;->b:[Lhye;

    .line 393
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Lhhg;->c:[Ljava/lang/String;

    .line 394
    return-object v3
.end method

.method static synthetic a(Lffw;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lffw;->d:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lffw;->d:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lffw;->d:Landroid/os/Handler;

    iget-object v1, p0, Lffw;->k:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 328
    return-void
.end method

.method public final a(Lfgg;)V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    return-void
.end method

.method a(Lhyf;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 398
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    invoke-virtual {p0}, Lffw;->e()V

    .line 401
    iget-object v0, p1, Lhyf;->a:Lhye;

    iget-object v0, v0, Lhye;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 402
    iget-object v0, p1, Lhyf;->a:Lhye;

    iget-object v0, v0, Lhye;->b:Ljava/lang/String;

    move-object v1, v0

    .line 405
    :goto_0
    iget-object v0, p1, Lhyf;->a:Lhye;

    iget-object v0, v0, Lhye;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 406
    iget-object v0, p1, Lhyf;->a:Lhye;

    iget-object v2, v0, Lhye;->c:Ljava/lang/String;

    .line 409
    :cond_0
    iget-object v5, p1, Lhyf;->b:[Lhyh;

    array-length v6, v5

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_5

    aget-object v7, v5, v4

    .line 411
    iget-object v0, v7, Lhyh;->e:Lhzd;

    if-eqz v0, :cond_1

    .line 412
    iget-object v0, p0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgg;

    .line 413
    iget-object v9, v7, Lhyh;->e:Lhzd;

    invoke-virtual {v0, v1, v9}, Lfgg;->a(Ljava/lang/String;Lhzd;)V

    goto :goto_2

    .line 415
    :cond_1
    iget-object v0, v7, Lhyh;->b:Lhtb;

    if-eqz v0, :cond_2

    .line 416
    iget-object v0, p0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgg;

    .line 417
    iget-object v9, v7, Lhyh;->b:Lhtb;

    invoke-virtual {v0, v2, v9}, Lfgg;->a(Ljava/lang/String;Lhtb;)V

    goto :goto_3

    .line 420
    :cond_2
    iget-object v0, v7, Lhyh;->c:Lhyl;

    if-eqz v0, :cond_3

    .line 421
    iget-object v0, p0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgg;

    .line 422
    iget-object v9, v7, Lhyh;->c:Lhyl;

    invoke-virtual {v0, v2, v9}, Lfgg;->a(Ljava/lang/String;Lhyl;)V

    goto :goto_4

    .line 424
    :cond_3
    iget-object v0, v7, Lhyh;->d:Lhxh;

    if-eqz v0, :cond_4

    .line 425
    iget-object v0, p0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 426
    iget-object v8, v7, Lhyh;->d:Lhxh;

    goto :goto_5

    .line 409
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 432
    :cond_5
    iget-object v4, p1, Lhyf;->c:[Lhyg;

    array-length v5, v4

    move v0, v3

    :goto_6
    if-ge v3, v5, :cond_7

    aget-object v6, v4, v3

    .line 433
    iget-object v7, v6, Lhyg;->b:Lhyi;

    if-eqz v7, :cond_6

    .line 434
    iget-object v0, v6, Lhyg;->b:Lhyi;

    .line 436
    new-instance v6, Lfgh;

    invoke-direct {v6}, Lfgh;-><init>()V

    .line 438
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iget v7, v0, Lhyi;->b:I

    int-to-long v10, v7

    add-long/2addr v8, v10

    iput-wide v8, v6, Lfgh;->a:J

    .line 439
    iget-object v0, v0, Lhyi;->a:Ljava/lang/String;

    iput-object v0, v6, Lfgh;->d:Ljava/lang/String;

    .line 440
    iput-object v1, v6, Lfgh;->b:Ljava/lang/String;

    .line 441
    iput-object v2, v6, Lfgh;->c:Ljava/lang/String;

    .line 442
    iget-object v0, p0, Lffw;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0, v6}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 443
    const/4 v0, 0x1

    .line 432
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 447
    :cond_7
    if-nez v0, :cond_8

    .line 448
    iget-object v0, p0, Lffw;->c:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgg;

    .line 449
    invoke-virtual {v0, v1}, Lfgg;->b(Ljava/lang/String;)V

    goto :goto_7

    .line 452
    :cond_8
    return-void

    :cond_9
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lffw;->d:Landroid/os/Handler;

    iget-object v1, p0, Lffw;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 349
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 355
    iget-object v0, p0, Lffw;->d:Landroid/os/Handler;

    iget-object v1, p0, Lffw;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 356
    return-void
.end method

.method d()V
    .locals 6

    .prologue
    .line 518
    invoke-virtual {p0}, Lffw;->e()V

    .line 519
    iget-object v0, p0, Lffw;->d:Landroid/os/Handler;

    iget-object v1, p0, Lffw;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 520
    iget-boolean v0, p0, Lffw;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lffw;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    iget-object v0, p0, Lffw;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgh;

    iget-wide v0, v0, Lfgh;->a:J

    .line 524
    iget-object v2, p0, Lffw;->d:Landroid/os/Handler;

    iget-object v3, p0, Lffw;->h:Ljava/lang/Runnable;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v0, v4

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method e()V
    .locals 2

    .prologue
    .line 556
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lffw;->d:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 557
    return-void

    .line 556
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
