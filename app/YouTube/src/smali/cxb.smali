.class public Lcxb;
.super Ldef;
.source "SourceFile"


# instance fields
.field public a:Lgfa;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcxb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ldef;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object v0, p1

    .line 31
    check-cast v0, Landroid/app/Activity;

    .line 32
    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcwy;

    .line 34
    new-instance v1, Lgfa;

    .line 36
    invoke-interface {v0}, Lcwy;->o()Lezg;

    move-result-object v0

    invoke-interface {v0}, Lezg;->ar()Lcyc;

    move-result-object v0

    invoke-static {v0}, La;->a(Lezk;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-direct {v1, p1, v0}, Lgfa;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcxb;->a:Lgfa;

    .line 38
    iget-object v0, p0, Lcxb;->a:Lgfa;

    invoke-virtual {p0, v0}, Lcxb;->a(Landroid/view/View;)V

    .line 39
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method


# virtual methods
.method public final b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47
    iget-object v0, p0, Lcxb;->a:Lgfa;

    invoke-virtual {v0}, Lgfa;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, Lgfa;->a:Lgec;

    invoke-interface {v2}, Lgec;->i()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget-object v0, v0, Lgfa;->a:Lgec;

    invoke-interface {v0}, Lgec;->i()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 48
    if-eqz p1, :cond_3

    .line 49
    iget-object v0, p0, Lcxb;->a:Lgfa;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lgfa;->setVisibility(I)V

    .line 54
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Ldef;->b(Z)V

    .line 55
    return-void

    :cond_2
    move v0, v1

    .line 47
    goto :goto_0

    .line 51
    :cond_3
    iget-object v0, p0, Lcxb;->a:Lgfa;

    invoke-virtual {v0, v1}, Lgfa;->setVisibility(I)V

    goto :goto_1
.end method
