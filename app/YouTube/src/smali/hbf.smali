.class public final Lhbf;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lhbg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14945
    invoke-direct {p0}, Lidf;-><init>()V

    .line 15055
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhbf;->a:Z

    .line 15058
    const/4 v0, 0x0

    iput-object v0, p0, Lhbf;->b:Lhbg;

    .line 14945
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 15101
    const/4 v0, 0x0

    .line 15102
    iget-boolean v1, p0, Lhbf;->a:Z

    if-eqz v1, :cond_0

    .line 15103
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhbf;->a:Z

    .line 15104
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 15106
    :cond_0
    iget-object v1, p0, Lhbf;->b:Lhbg;

    if-eqz v1, :cond_1

    .line 15107
    const/4 v1, 0x2

    iget-object v2, p0, Lhbf;->b:Lhbg;

    .line 15108
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15110
    :cond_1
    iget-object v1, p0, Lhbf;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15111
    iput v0, p0, Lhbf;->J:I

    .line 15112
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 14941
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbf;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbf;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbf;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhbf;->a:Z

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhbf;->b:Lhbg;

    if-nez v0, :cond_2

    new-instance v0, Lhbg;

    invoke-direct {v0}, Lhbg;-><init>()V

    iput-object v0, p0, Lhbf;->b:Lhbg;

    :cond_2
    iget-object v0, p0, Lhbf;->b:Lhbg;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 15089
    iget-boolean v0, p0, Lhbf;->a:Z

    if-eqz v0, :cond_0

    .line 15090
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhbf;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 15092
    :cond_0
    iget-object v0, p0, Lhbf;->b:Lhbg;

    if-eqz v0, :cond_1

    .line 15093
    const/4 v0, 0x2

    iget-object v1, p0, Lhbf;->b:Lhbg;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15095
    :cond_1
    iget-object v0, p0, Lhbf;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 15097
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 15070
    if-ne p1, p0, :cond_1

    .line 15075
    :cond_0
    :goto_0
    return v0

    .line 15071
    :cond_1
    instance-of v2, p1, Lhbf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 15072
    :cond_2
    check-cast p1, Lhbf;

    .line 15073
    iget-boolean v2, p0, Lhbf;->a:Z

    iget-boolean v3, p1, Lhbf;->a:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhbf;->b:Lhbg;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbf;->b:Lhbg;

    if-nez v2, :cond_3

    .line 15074
    :goto_1
    iget-object v2, p0, Lhbf;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbf;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 15075
    goto :goto_0

    .line 15073
    :cond_4
    iget-object v2, p0, Lhbf;->b:Lhbg;

    iget-object v3, p1, Lhbf;->b:Lhbg;

    .line 15074
    invoke-virtual {v2, v3}, Lhbg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbf;->I:Ljava/util/List;

    iget-object v3, p1, Lhbf;->I:Ljava/util/List;

    .line 15075
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 15079
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 15081
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhbf;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    .line 15082
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbf;->b:Lhbg;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 15083
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbf;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 15084
    return v0

    .line 15081
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 15082
    :cond_1
    iget-object v0, p0, Lhbf;->b:Lhbg;

    invoke-virtual {v0}, Lhbg;->hashCode()I

    move-result v0

    goto :goto_1

    .line 15083
    :cond_2
    iget-object v1, p0, Lhbf;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
