.class public final Lfli;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/CharSequence;

.field public final b:Ljava/lang/CharSequence;

.field final c:Lflj;


# direct methods
.method constructor <init>(Lhpo;)V
    .locals 3

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    iget-object v0, p1, Lhpo;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfli;->a:Ljava/lang/CharSequence;

    .line 99
    iget-object v0, p1, Lhpo;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfli;->b:Ljava/lang/CharSequence;

    .line 100
    iget v0, p1, Lhpo;->d:I

    packed-switch v0, :pswitch_data_0

    .line 111
    iget v0, p1, Lhpo;->d:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x36

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid offlineability.format.format_type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lfli;->c:Lflj;

    .line 115
    :goto_0
    return-void

    .line 102
    :pswitch_0
    sget-object v0, Lflj;->a:Lflj;

    iput-object v0, p0, Lfli;->c:Lflj;

    goto :goto_0

    .line 105
    :pswitch_1
    sget-object v0, Lflj;->b:Lflj;

    iput-object v0, p0, Lfli;->c:Lflj;

    goto :goto_0

    .line 108
    :pswitch_2
    sget-object v0, Lflj;->c:Lflj;

    iput-object v0, p0, Lfli;->c:Lflj;

    goto :goto_0

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
