.class public final Leeu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leee;


# instance fields
.field final a:Leet;

.field final b:[B

.field c:Leby;

.field d:I

.field e:J

.field f:J

.field g:J

.field h:J

.field i:I

.field j:I

.field k:I

.field l:I

.field m:[B

.field n:Z

.field o:J

.field p:J

.field q:J

.field r:Lebv;

.field s:Leef;

.field t:Legs;

.field u:Legs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Leeq;

    invoke-direct {v0}, Leeq;-><init>()V

    invoke-direct {p0, v0}, Leeu;-><init>(Leet;)V

    .line 108
    return-void
.end method

.method private constructor <init>(Leet;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const-wide/16 v2, -0x1

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const/4 v0, 0x3

    new-array v0, v0, [B

    iput-object v0, p0, Leeu;->b:[B

    .line 88
    iput-wide v2, p0, Leeu;->e:J

    .line 89
    iput-wide v2, p0, Leeu;->f:J

    .line 90
    const-wide/32 v0, 0xf4240

    iput-wide v0, p0, Leeu;->g:J

    .line 91
    iput-wide v2, p0, Leeu;->h:J

    .line 92
    iput v4, p0, Leeu;->i:I

    .line 93
    iput v4, p0, Leeu;->j:I

    .line 94
    iput v4, p0, Leeu;->k:I

    .line 95
    iput v4, p0, Leeu;->l:I

    .line 98
    iput-wide v2, p0, Leeu;->o:J

    .line 99
    iput-wide v2, p0, Leeu;->p:J

    .line 100
    iput-wide v2, p0, Leeu;->q:J

    .line 111
    iput-object p1, p0, Leeu;->a:Leet;

    .line 112
    iget-object v0, p0, Leeu;->a:Leet;

    new-instance v1, Leev;

    invoke-direct {v1, p0}, Leev;-><init>(Leeu;)V

    invoke-interface {v0, v1}, Leet;->a(Lees;)V

    .line 113
    return-void
.end method


# virtual methods
.method public final a(Lefx;Leby;)I
    .locals 2

    .prologue
    .line 118
    iput-object p2, p0, Leeu;->c:Leby;

    .line 119
    const/4 v0, 0x0

    iput v0, p0, Leeu;->d:I

    .line 120
    :cond_0
    :goto_0
    iget v0, p0, Leeu;->d:I

    and-int/lit8 v0, v0, 0x27

    if-nez v0, :cond_2

    .line 121
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-interface {v0, p1}, Leet;->a(Lefx;)I

    move-result v0

    .line 122
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 123
    iget v0, p0, Leeu;->d:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Leeu;->d:I

    goto :goto_0

    .line 124
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 125
    iget v0, p0, Leeu;->d:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Leeu;->d:I

    goto :goto_0

    .line 128
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Leeu;->c:Leby;

    .line 129
    iget v0, p0, Leeu;->d:I

    return v0
.end method

.method a(J)J
    .locals 5

    .prologue
    .line 400
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v2, p0, Leeu;->g:J

    mul-long/2addr v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a()Leef;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Leeu;->s:Leef;

    return-object v0
.end method

.method public final a(JZ)Z
    .locals 7

    .prologue
    const-wide/16 v4, -0x1

    .line 134
    if-eqz p3, :cond_0

    iget-object v0, p0, Leeu;->s:Leef;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Leeu;->p:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, Leeu;->q:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, Leeu;->q:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 139
    iget-object v0, p0, Leeu;->s:Leef;

    iget-object v0, v0, Leef;->f:[J

    iget-wide v2, p0, Leeu;->p:J

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 140
    if-ltz v0, :cond_0

    iget-wide v2, p0, Leeu;->p:J

    iget-object v1, p0, Leeu;->s:Leef;

    iget-object v1, v1, Leef;->e:[J

    aget-wide v0, v1, v0

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    .line 147
    :goto_0
    return v0

    .line 144
    :cond_0
    iput-wide v4, p0, Leeu;->p:J

    .line 145
    iput-wide v4, p0, Leeu;->q:J

    .line 146
    iget-object v0, p0, Leeu;->a:Leet;

    invoke-interface {v0}, Leet;->b()V

    .line 147
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 157
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Lebv;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Leeu;->r:Lebv;

    return-object v0
.end method

.method public final d()Ljava/util/Map;
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return-object v0
.end method

.method e()Ljava/util/ArrayList;
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v0, 0x0

    .line 491
    :try_start_0
    iget-object v1, p0, Leeu;->m:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    if-eq v1, v3, :cond_0

    .line 492
    new-instance v0, Lebx;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 529
    :catch_0
    move-exception v0

    new-instance v0, Lebx;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v0

    move v3, v4

    .line 496
    :goto_0
    :try_start_1
    iget-object v1, p0, Leeu;->m:[B

    aget-byte v1, v1, v3

    if-ne v1, v6, :cond_1

    .line 497
    add-int/lit16 v1, v2, 0xff

    .line 498
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 500
    :cond_1
    iget-object v5, p0, Leeu;->m:[B

    add-int/lit8 v1, v3, 0x1

    aget-byte v3, v5, v3

    add-int/2addr v2, v3

    .line 503
    :goto_1
    iget-object v3, p0, Leeu;->m:[B

    aget-byte v3, v3, v1

    if-ne v3, v6, :cond_2

    .line 504
    add-int/lit16 v0, v0, 0xff

    .line 505
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 507
    :cond_2
    iget-object v3, p0, Leeu;->m:[B

    add-int/lit8 v5, v1, 0x1

    aget-byte v1, v3, v1

    add-int/2addr v0, v1

    .line 509
    iget-object v1, p0, Leeu;->m:[B

    aget-byte v1, v1, v5

    if-eq v1, v4, :cond_3

    .line 510
    new-instance v0, Lebx;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_3
    new-array v1, v2, [B

    .line 513
    iget-object v3, p0, Leeu;->m:[B

    const/4 v4, 0x0

    invoke-static {v3, v5, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 514
    add-int/2addr v2, v5

    .line 515
    iget-object v3, p0, Leeu;->m:[B

    aget-byte v3, v3, v2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_4

    .line 516
    new-instance v0, Lebx;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    .line 518
    :cond_4
    add-int/2addr v0, v2

    .line 519
    iget-object v2, p0, Leeu;->m:[B

    aget-byte v2, v2, v0

    const/4 v3, 0x5

    if-eq v2, v3, :cond_5

    .line 520
    new-instance v0, Lebx;

    const-string v1, "Error parsing vorbis codec private"

    invoke-direct {v0, v1}, Lebx;-><init>(Ljava/lang/String;)V

    throw v0

    .line 522
    :cond_5
    iget-object v2, p0, Leeu;->m:[B

    array-length v2, v2

    sub-int/2addr v2, v0

    new-array v2, v2, [B

    .line 523
    iget-object v3, p0, Leeu;->m:[B

    const/4 v4, 0x0

    iget-object v5, p0, Leeu;->m:[B

    array-length v5, v5

    sub-int/2addr v5, v0

    invoke-static {v3, v0, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 524
    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 525
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 526
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 527
    return-object v0
.end method
