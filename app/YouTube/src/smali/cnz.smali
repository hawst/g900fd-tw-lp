.class public final Lcnz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lftg;


# instance fields
.field private final a:Lcnu;

.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:Z


# direct methods
.method public constructor <init>(Lcnu;Landroid/telephony/TelephonyManager;Landroid/content/pm/PackageManager;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnu;

    iput-object v0, p0, Lcnz;->a:Lcnu;

    .line 33
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v3, Landroid/content/Intent;

    const-string v0, "android.intent.action.DIAL"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p2}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    iput v0, p0, Lcnz;->b:I

    .line 38
    invoke-virtual {p2}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v0

    iput v0, p0, Lcnz;->c:I

    .line 39
    invoke-virtual {p2}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcnz;->d:Z

    .line 40
    invoke-virtual {p3, v3, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcnz;->e:Z

    .line 41
    return-void

    :cond_0
    move v0, v2

    .line 39
    goto :goto_0

    :cond_1
    move v1, v2

    .line 40
    goto :goto_1
.end method


# virtual methods
.method public final a(Lftj;)V
    .locals 7

    .prologue
    .line 46
    invoke-static {}, Lb;->b()V

    .line 47
    iget-object v0, p0, Lcnz;->a:Lcnu;

    iget-object v1, v0, Lcnu;->g:Lcwq;

    .line 48
    iget-object v0, p0, Lcnz;->a:Lcnu;

    .line 49
    iget-object v0, v0, Lcnu;->a:Lery;

    iget-object v2, v0, Lery;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lery;->d()Ljava/lang/String;

    move-result-object v0

    const-string v3, "xml_vast2"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1b

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "sdkv="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "&video_format="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "&output="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lftj;->u:Ljava/lang/String;

    iget-object v0, p0, Lcnz;->a:Lcnu;

    .line 50
    iget-object v0, v0, Lcnu;->e:Ljava/lang/String;

    iput-object v0, p1, Lftj;->o:Ljava/lang/String;

    iget-object v0, p0, Lcnz;->a:Lcnu;

    .line 51
    iget-object v0, v0, Lcnu;->d:Lexd;

    invoke-interface {v0}, Lexd;->i()I

    move-result v0

    iput v0, p1, Lftj;->r:I

    iget-object v0, p0, Lcnz;->a:Lcnu;

    .line 52
    invoke-virtual {v0}, Lcnu;->c()I

    move-result v0

    iput v0, p1, Lftj;->q:I

    iget v0, p0, Lcnz;->b:I

    .line 53
    iput v0, p1, Lftj;->x:I

    iget v0, p0, Lcnz;->c:I

    .line 54
    iput v0, p1, Lftj;->y:I

    iget-boolean v0, p0, Lcnz;->d:Z

    .line 55
    iput-boolean v0, p1, Lftj;->z:Z

    iget-boolean v0, p0, Lcnz;->e:Z

    .line 56
    iput-boolean v0, p1, Lftj;->A:Z

    .line 60
    monitor-enter v1

    .line 61
    :try_start_0
    invoke-virtual {v1}, Lcwq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v1}, Lcwq;->e()Z

    move-result v0

    iput-boolean v0, p1, Lftj;->t:Z

    .line 64
    invoke-virtual {v1}, Lcwq;->d()I

    move-result v0

    iput v0, p1, Lftj;->s:I

    .line 65
    invoke-virtual {v1}, Lcwq;->f()Z

    move-result v0

    iput-boolean v0, p1, Lftj;->v:Z

    .line 67
    invoke-virtual {v1}, Lcwq;->g()Lcwx;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {v1}, Lcwq;->g()Lcwx;

    move-result-object v0

    iget v0, v0, Lcwx;->a:I

    iput v0, p1, Lftj;->w:I

    .line 71
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    iget-object v0, p0, Lcnz;->a:Lcnu;

    iget-object v0, v0, Lcnu;->f:Lezf;

    .line 75
    if-eqz v0, :cond_1

    .line 76
    invoke-virtual {v0}, Lezf;->b()J

    move-result-wide v0

    iput-wide v0, p1, Lftj;->p:J

    .line 78
    :cond_1
    return-void

    .line 71
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
