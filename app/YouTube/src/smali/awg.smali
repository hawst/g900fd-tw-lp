.class public final Lawg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lawo;


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lawg;->a:Ljava/util/List;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lawo;)Lawg;
    .locals 1

    .prologue
    .line 28
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iget-object v0, p0, Lawg;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    return-object p0
.end method

.method public final a(Landroid/view/View;Landroid/view/ViewGroup;)Lawm;
    .locals 3

    .prologue
    .line 42
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 43
    iget-object v0, p0, Lawg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawo;

    .line 44
    invoke-interface {v0, p1, p2}, Lawo;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lawm;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 46
    :cond_0
    new-instance v0, Lawh;

    invoke-direct {v0, p1, v1}, Lawh;-><init>(Landroid/view/View;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lawg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawo;

    .line 36
    invoke-interface {v0, p1}, Lawo;->a(Ljava/lang/Iterable;)V

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method
