.class public Lbau;
.super Lbhz;
.source "SourceFile"


# instance fields
.field private e:Landroid/view/View;

.field private f:Landroid/widget/TextView;

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/Button;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Lgit;

.field private l:Lgnd;

.field private o:Lckv;

.field private p:Landroid/os/AsyncTask;

.field private q:Landroid/os/Handler;

.field private r:Lbay;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lbhz;-><init>()V

    .line 195
    return-void
.end method

.method static synthetic a(Lbau;)Lgnd;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbau;->l:Lgnd;

    return-object v0
.end method

.method static synthetic b(Lbau;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbau;->p:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic c(Lbau;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lbau;->l:Lgnd;

    invoke-interface {v0}, Lgnd;->g()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmb;

    iget-object v2, v0, Lgmb;->d:Lglz;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lgmb;->d:Lglz;

    invoke-virtual {v0}, Lglz;->d()Lgma;

    move-result-object v0

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lgma;->d:J

    invoke-virtual {v0}, Lgma;->a()Lglz;

    move-result-object v0

    iget-object v2, p0, Lbau;->l:Lgnd;

    invoke-interface {v2, v0}, Lgnd;->a(Lglz;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic d(Lbau;)Lbay;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbau;->r:Lbay;

    return-object v0
.end method

.method static synthetic e(Lbau;)Lckv;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbau;->o:Lckv;

    return-object v0
.end method

.method static synthetic f(Lbau;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbau;->j:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lbau;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbau;->q:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 64
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-static {p0}, La;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lbau;->finish()V

    .line 71
    :cond_0
    const v0, 0x7f040052

    invoke-virtual {p0, v0}, Lbau;->setContentView(I)V

    .line 72
    const v0, 0x7f080177

    invoke-virtual {p0, v0}, Lbau;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbau;->e:Landroid/view/View;

    .line 73
    const v0, 0x7f08016d

    invoke-virtual {p0, v0}, Lbau;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbau;->f:Landroid/widget/TextView;

    .line 75
    const v0, 0x7f080178

    invoke-virtual {p0, v0}, Lbau;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbau;->g:Landroid/widget/Button;

    .line 76
    iget-object v0, p0, Lbau;->g:Landroid/widget/Button;

    new-instance v1, Lbav;

    invoke-direct {v1, p0}, Lbav;-><init>(Lbau;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v0, 0x7f080179

    invoke-virtual {p0, v0}, Lbau;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbau;->h:Landroid/widget/Button;

    .line 88
    iget-object v0, p0, Lbau;->h:Landroid/widget/Button;

    new-instance v1, Lbaw;

    invoke-direct {v1, p0}, Lbaw;-><init>(Lbau;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v0, 0x7f080176

    invoke-virtual {p0, v0}, Lbau;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbau;->i:Landroid/widget/TextView;

    .line 99
    invoke-virtual {p0}, Lbau;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lari;->am()Lckv;

    move-result-object v0

    iput-object v0, p0, Lbau;->o:Lckv;

    .line 101
    invoke-virtual {v1}, Lari;->aD()Lcst;

    move-result-object v0

    .line 102
    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    invoke-virtual {v1}, Lari;->aD()Lcst;

    move-result-object v0

    invoke-virtual {v0}, Lcst;->d()Lgit;

    move-result-object v0

    iput-object v0, p0, Lbau;->k:Lgit;

    .line 104
    iget-object v2, p0, Lbau;->f:Landroid/widget/TextView;

    const-string v3, "Signed in as "

    iget-object v0, p0, Lbau;->k:Lgit;

    invoke-virtual {v0}, Lgit;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    invoke-virtual {v1}, Lari;->O()Lgng;

    move-result-object v0

    iget-object v1, p0, Lbau;->k:Lgit;

    invoke-virtual {v0, v1}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    iput-object v0, p0, Lbau;->l:Lgnd;

    .line 108
    new-instance v0, Lbax;

    invoke-direct {v0, p0}, Lbax;-><init>(Lbau;)V

    iput-object v0, p0, Lbau;->p:Landroid/os/AsyncTask;

    .line 128
    :goto_1
    const v0, 0x7f08017b

    invoke-virtual {p0, v0}, Lbau;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbau;->j:Landroid/widget/TextView;

    .line 130
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "debugOfflineLogs"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 132
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v2, Lbaz;

    invoke-direct {v2, p0}, Lbaz;-><init>(Lbau;)V

    invoke-direct {v1, v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lbau;->q:Landroid/os/Handler;

    .line 133
    new-instance v0, Lbay;

    invoke-direct {v0, p0}, Lbay;-><init>(Lbau;)V

    iput-object v0, p0, Lbau;->r:Lbay;

    .line 134
    return-void

    .line 104
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_2
    iget-object v0, p0, Lbau;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lbau;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Lbhz;->onPause()V

    .line 152
    iget-object v0, p0, Lbau;->q:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 153
    iget-object v0, p0, Lbau;->q:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 154
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0}, Lbhz;->onResume()V

    .line 143
    invoke-virtual {p0}, Lbau;->d()Lkm;

    move-result-object v0

    const-string v1, "Offline Refresh"

    invoke-virtual {v0, v1}, Lkm;->a(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lbau;->q:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 146
    return-void
.end method
