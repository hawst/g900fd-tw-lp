.class public final Lfko;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/Set;

.field public final b:Z

.field public final c:Z


# direct methods
.method public constructor <init>(Lhmy;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 23
    iget-object v1, p1, Lhmy;->a:[Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lhmy;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 24
    iget-object v1, p1, Lhmy;->a:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 26
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lfko;->a:Ljava/util/Set;

    .line 27
    iget-object v0, p1, Lhmy;->b:Lhmz;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lhmy;->b:Lhmz;

    iget-object v0, v0, Lhmz;->a:Lgzs;

    if-nez v0, :cond_2

    .line 28
    :cond_1
    iput-boolean v2, p0, Lfko;->b:Z

    .line 29
    iput-boolean v2, p0, Lfko;->c:Z

    .line 35
    :goto_0
    return-void

    .line 31
    :cond_2
    iget-object v0, p1, Lhmy;->b:Lhmz;

    iget-object v0, v0, Lhmz;->a:Lgzs;

    iget-boolean v0, v0, Lgzs;->a:Z

    iput-boolean v0, p0, Lfko;->b:Z

    .line 33
    iget-object v0, p1, Lhmy;->b:Lhmz;

    iget-object v0, v0, Lhmz;->a:Lgzs;

    iget-boolean v0, v0, Lgzs;->b:Z

    iput-boolean v0, p0, Lfko;->c:Z

    goto :goto_0
.end method
