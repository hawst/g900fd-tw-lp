.class public final Lgzi;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:[Lgzj;

.field private c:Lhog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 7945
    invoke-direct {p0}, Lidf;-><init>()V

    .line 8055
    iput-object v1, p0, Lgzi;->a:Lhgz;

    .line 8058
    sget-object v0, Lgzj;->a:[Lgzj;

    iput-object v0, p0, Lgzi;->b:[Lgzj;

    .line 8061
    iput-object v1, p0, Lgzi;->c:Lhog;

    .line 7945
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 8119
    .line 8120
    iget-object v0, p0, Lgzi;->a:Lhgz;

    if-eqz v0, :cond_3

    .line 8121
    const/4 v0, 0x1

    iget-object v2, p0, Lgzi;->a:Lhgz;

    .line 8122
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8124
    :goto_0
    iget-object v2, p0, Lgzi;->b:[Lgzj;

    if-eqz v2, :cond_1

    .line 8125
    iget-object v2, p0, Lgzi;->b:[Lgzj;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 8126
    if-eqz v4, :cond_0

    .line 8127
    const/4 v5, 0x2

    .line 8128
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 8125
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 8132
    :cond_1
    iget-object v1, p0, Lgzi;->c:Lhog;

    if-eqz v1, :cond_2

    .line 8133
    const/4 v1, 0x3

    iget-object v2, p0, Lgzi;->c:Lhog;

    .line 8134
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8136
    :cond_2
    iget-object v1, p0, Lgzi;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8137
    iput v0, p0, Lgzi;->J:I

    .line 8138
    return v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7941
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lgzi;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lgzi;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lgzi;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzi;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzi;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgzi;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lgzi;->b:[Lgzj;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgzj;

    iget-object v3, p0, Lgzi;->b:[Lgzj;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lgzi;->b:[Lgzj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lgzi;->b:[Lgzj;

    :goto_2
    iget-object v2, p0, Lgzi;->b:[Lgzj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lgzi;->b:[Lgzj;

    new-instance v3, Lgzj;

    invoke-direct {v3}, Lgzj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzi;->b:[Lgzj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lgzi;->b:[Lgzj;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzi;->b:[Lgzj;

    new-instance v3, Lgzj;

    invoke-direct {v3}, Lgzj;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lgzi;->b:[Lgzj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgzi;->c:Lhog;

    if-nez v0, :cond_6

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgzi;->c:Lhog;

    :cond_6
    iget-object v0, p0, Lgzi;->c:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 8100
    iget-object v0, p0, Lgzi;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 8101
    const/4 v0, 0x1

    iget-object v1, p0, Lgzi;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8103
    :cond_0
    iget-object v0, p0, Lgzi;->b:[Lgzj;

    if-eqz v0, :cond_2

    .line 8104
    iget-object v1, p0, Lgzi;->b:[Lgzj;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 8105
    if-eqz v3, :cond_1

    .line 8106
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 8104
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8110
    :cond_2
    iget-object v0, p0, Lgzi;->c:Lhog;

    if-eqz v0, :cond_3

    .line 8111
    const/4 v0, 0x3

    iget-object v1, p0, Lgzi;->c:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 8113
    :cond_3
    iget-object v0, p0, Lgzi;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 8115
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8074
    if-ne p1, p0, :cond_1

    .line 8080
    :cond_0
    :goto_0
    return v0

    .line 8075
    :cond_1
    instance-of v2, p1, Lgzi;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 8076
    :cond_2
    check-cast p1, Lgzi;

    .line 8077
    iget-object v2, p0, Lgzi;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzi;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzi;->b:[Lgzj;

    iget-object v3, p1, Lgzi;->b:[Lgzj;

    .line 8078
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lgzi;->c:Lhog;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzi;->c:Lhog;

    if-nez v2, :cond_3

    .line 8079
    :goto_2
    iget-object v2, p0, Lgzi;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzi;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 8080
    goto :goto_0

    .line 8077
    :cond_4
    iget-object v2, p0, Lgzi;->a:Lhgz;

    iget-object v3, p1, Lgzi;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 8078
    :cond_5
    iget-object v2, p0, Lgzi;->c:Lhog;

    iget-object v3, p1, Lgzi;->c:Lhog;

    .line 8079
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgzi;->I:Ljava/util/List;

    iget-object v3, p1, Lgzi;->I:Ljava/util/List;

    .line 8080
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8084
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 8086
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzi;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8087
    iget-object v2, p0, Lgzi;->b:[Lgzj;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 8093
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lgzi;->c:Lhog;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8094
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzi;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_2
    add-int/2addr v0, v1

    .line 8095
    return v0

    .line 8086
    :cond_1
    iget-object v0, p0, Lgzi;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    .line 8089
    :goto_3
    iget-object v3, p0, Lgzi;->b:[Lgzj;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 8090
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lgzi;->b:[Lgzj;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 8089
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 8090
    :cond_3
    iget-object v2, p0, Lgzi;->b:[Lgzj;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgzj;->hashCode()I

    move-result v2

    goto :goto_4

    .line 8093
    :cond_4
    iget-object v0, p0, Lgzi;->c:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_1

    .line 8094
    :cond_5
    iget-object v1, p0, Lgzi;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
