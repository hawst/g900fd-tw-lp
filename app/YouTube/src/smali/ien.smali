.class public final Lien;
.super Leqd;


# static fields
.field private static volatile c:[Lien;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Leqd;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lien;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lien;->e:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lien;->a:Leqh;

    const/4 v0, -0x1

    iput v0, p0, Lien;->b:I

    return-void
.end method

.method public static a()[Lien;
    .locals 2

    sget-object v0, Lien;->c:[Lien;

    if-nez v0, :cond_1

    sget-object v1, Leqj;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lien;->c:[Lien;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Lien;

    sput-object v0, Lien;->c:[Lien;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lien;->c:[Lien;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lien;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lien;

    iget-object v2, p0, Lien;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    iget-object v2, p1, Lien;->d:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lien;->d:Ljava/lang/String;

    iget-object v3, p1, Lien;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lien;->e:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lien;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lien;->e:Ljava/lang/String;

    iget-object v3, p1, Lien;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lien;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lien;->e:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    return v0

    :cond_0
    iget-object v0, p0, Lien;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lien;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method
