.class public final Lfyx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgku;


# instance fields
.field private a:Lfxj;

.field private final b:Lgku;


# direct methods
.method public constructor <init>(Lfxj;Lgku;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxj;

    iput-object v0, p0, Lfyx;->a:Lfxj;

    .line 32
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgku;

    iput-object v0, p0, Lfyx;->b:Lgku;

    .line 33
    return-void
.end method

.method private a(Landroid/net/Uri;)Ljava/util/List;
    .locals 6

    .prologue
    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 63
    :cond_0
    invoke-static {}, Leud;->a()Leud;

    move-result-object v0

    .line 64
    iget-object v2, p0, Lfyx;->b:Lgku;

    iget-object v3, p0, Lfyx;->a:Lfxj;

    iget v3, v3, Lfxj;->c:I

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "start-index"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    const-string v4, "max-results"

    invoke-virtual {p1, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v4, v5, v3}, Lfxj;->a(Landroid/net/Uri$Builder;II)V

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p1

    :cond_2
    invoke-static {p1}, Lfxg;->a(Landroid/net/Uri;)Lfxg;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 65
    invoke-virtual {v0}, Leud;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjh;

    .line 66
    iget-object v2, v0, Lgjh;->f:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 67
    iget-object p1, v0, Lgjh;->e:Landroid/net/Uri;

    .line 68
    iget v0, v0, Lgjh;->a:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_3

    if-nez p1, :cond_0

    .line 70
    :cond_3
    return-object v1
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Leuc;)V
    .locals 3

    .prologue
    .line 21
    check-cast p1, Ljava/lang/String;

    invoke-static {}, Lb;->b()V

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "https://gdata.youtube.com/feeds/api/playlists/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :try_start_0
    invoke-direct {p0, v0}, Lfyx;->a(Landroid/net/Uri;)Ljava/util/List;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-interface {p2, p1, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
