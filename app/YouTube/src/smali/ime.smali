.class public final Lime;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Limb;

.field public b:Lima;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Lilr;

.field f:Lilt;

.field public g:Limf;

.field h:Limd;

.field i:Limd;

.field j:Limd;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const/4 v0, -0x1

    iput v0, p0, Lime;->c:I

    .line 232
    new-instance v0, Lilt;

    invoke-direct {v0}, Lilt;-><init>()V

    iput-object v0, p0, Lime;->f:Lilt;

    .line 233
    return-void
.end method

.method constructor <init>(Limd;)V
    .locals 1

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const/4 v0, -0x1

    iput v0, p0, Lime;->c:I

    .line 236
    iget-object v0, p1, Limd;->a:Limb;

    iput-object v0, p0, Lime;->a:Limb;

    .line 237
    iget-object v0, p1, Limd;->b:Lima;

    iput-object v0, p0, Lime;->b:Lima;

    .line 238
    iget v0, p1, Limd;->c:I

    iput v0, p0, Lime;->c:I

    .line 239
    iget-object v0, p1, Limd;->d:Ljava/lang/String;

    iput-object v0, p0, Lime;->d:Ljava/lang/String;

    .line 240
    iget-object v0, p1, Limd;->e:Lilr;

    iput-object v0, p0, Lime;->e:Lilr;

    .line 241
    iget-object v0, p1, Limd;->f:Lils;

    invoke-virtual {v0}, Lils;->a()Lilt;

    move-result-object v0

    iput-object v0, p0, Lime;->f:Lilt;

    .line 242
    iget-object v0, p1, Limd;->g:Limf;

    iput-object v0, p0, Lime;->g:Limf;

    .line 243
    iget-object v0, p1, Limd;->h:Limd;

    iput-object v0, p0, Lime;->h:Limd;

    .line 244
    iget-object v0, p1, Limd;->i:Limd;

    iput-object v0, p0, Lime;->i:Limd;

    .line 245
    iget-object v0, p1, Limd;->j:Limd;

    iput-object v0, p0, Lime;->j:Limd;

    .line 246
    return-void
.end method

.method private static a(Ljava/lang/String;Limd;)V
    .locals 3

    .prologue
    .line 320
    iget-object v0, p1, Limd;->g:Limf;

    if-eqz v0, :cond_0

    .line 321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".body != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_0
    iget-object v0, p1, Limd;->h:Limd;

    if-eqz v0, :cond_1

    .line 323
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".networkResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 324
    :cond_1
    iget-object v0, p1, Limd;->i:Limd;

    if-eqz v0, :cond_2

    .line 325
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".cacheResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_2
    iget-object v0, p1, Limd;->j:Limd;

    if-eqz v0, :cond_3

    .line 327
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".priorResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()Limd;
    .locals 4

    .prologue
    .line 344
    iget-object v0, p0, Lime;->a:Limb;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "request == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_0
    iget-object v0, p0, Lime;->b:Lima;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "protocol == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_1
    iget v0, p0, Lime;->c:I

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lime;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "code < 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347
    :cond_2
    new-instance v0, Limd;

    invoke-direct {v0, p0}, Limd;-><init>(Lime;)V

    return-object v0
.end method

.method public final a(Lils;)Lime;
    .locals 1

    .prologue
    .line 298
    invoke-virtual {p1}, Lils;->a()Lilt;

    move-result-object v0

    iput-object v0, p0, Lime;->f:Lilt;

    .line 299
    return-object p0
.end method

.method public final a(Limd;)Lime;
    .locals 1

    .prologue
    .line 308
    if-eqz p1, :cond_0

    const-string v0, "networkResponse"

    invoke-static {v0, p1}, Lime;->a(Ljava/lang/String;Limd;)V

    .line 309
    :cond_0
    iput-object p1, p0, Lime;->h:Limd;

    .line 310
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lime;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lime;->f:Lilt;

    invoke-virtual {v0, p1}, Lilt;->a(Ljava/lang/String;)Lilt;

    .line 293
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lime;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lime;->f:Lilt;

    invoke-virtual {v0, p1, p2}, Lilt;->c(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 279
    return-object p0
.end method

.method public final b(Limd;)Lime;
    .locals 1

    .prologue
    .line 314
    if-eqz p1, :cond_0

    const-string v0, "cacheResponse"

    invoke-static {v0, p1}, Lime;->a(Ljava/lang/String;Limd;)V

    .line 315
    :cond_0
    iput-object p1, p0, Lime;->i:Limd;

    .line 316
    return-object p0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lime;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lime;->f:Lilt;

    invoke-virtual {v0, p1, p2}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 288
    return-object p0
.end method

.method public final c(Limd;)Lime;
    .locals 2

    .prologue
    .line 332
    if-eqz p1, :cond_0

    iget-object v0, p1, Limd;->g:Limf;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "priorResponse.body != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :cond_0
    iput-object p1, p0, Lime;->j:Limd;

    .line 334
    return-object p0
.end method
