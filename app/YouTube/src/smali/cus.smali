.class final Lcus;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/content/res/Resources;

.field private final c:Ljava/util/List;

.field private final d:Leyp;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;Leyp;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 211
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 212
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcus;->a:Landroid/app/Activity;

    .line 213
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcus;->b:Landroid/content/res/Resources;

    .line 214
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcus;->c:Ljava/util/List;

    .line 215
    iput-object p3, p0, Lcus;->d:Leyp;

    .line 216
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    const-string v1, "Must have at least two profiles."

    invoke-static {v0, v1}, Lb;->d(ZLjava/lang/Object;)V

    .line 217
    return-void

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcus;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcus;->b:Landroid/content/res/Resources;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcus;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcus;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 265
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 221
    .line 222
    if-nez p2, :cond_0

    .line 223
    iget-object v0, p0, Lcus;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040018

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 226
    check-cast v0, Landroid/widget/CheckedTextView;

    .line 227
    iget-object v2, p0, Lcus;->c:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfid;

    .line 229
    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setTag(Ljava/lang/Object;)V

    .line 230
    invoke-virtual {v2}, Lfid;->a()Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    iget-object v3, p0, Lcus;->b:Landroid/content/res/Resources;

    const v4, 0x7f0a0059

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 234
    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 235
    const v5, 0x7f070083

    invoke-virtual {v4, v5}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 236
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcus;->b:Landroid/content/res/Resources;

    invoke-direct {v5, v6, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 237
    invoke-virtual {v0, v5, v7, v7, v7}, Landroid/widget/CheckedTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 239
    iget-object v4, p0, Lcus;->d:Leyp;

    .line 240
    invoke-virtual {v2}, Lfid;->c()Lfnc;

    move-result-object v5

    invoke-virtual {v5, v3, v3}, Lfnc;->a(II)Lfnb;

    move-result-object v3

    iget-object v3, v3, Lfnb;->a:Landroid/net/Uri;

    iget-object v5, p0, Lcus;->a:Landroid/app/Activity;

    new-instance v6, Lcut;

    invoke-direct {v6, p0, v0, v2}, Lcut;-><init>(Lcus;Landroid/widget/CheckedTextView;Lfid;)V

    .line 241
    invoke-static {v5, v6}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    .line 239
    invoke-interface {v4, v3, v0}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    .line 260
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method
