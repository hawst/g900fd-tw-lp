.class public final Lfvw;
.super Lfwb;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Lfwb;-><init>()V

    .line 204
    const/4 v0, -0x1

    iput v0, p0, Lfvw;->a:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 237
    iget-object v0, p0, Lfvw;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "activities/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/comments"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/Map;
    .locals 5

    .prologue
    .line 242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 243
    iget v1, p0, Lfvw;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 244
    const-string v1, "maxResults"

    iget v2, p0, Lfvw;->a:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0xb

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    :cond_0
    iget-object v1, p0, Lfvw;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 247
    const-string v1, "pageToken"

    iget-object v2, p0, Lfvw;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :cond_1
    const-string v1, "sortOrder"

    const-string v2, "descending"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    return-object v0
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lfvw;->b:Ljava/lang/String;

    invoke-static {v0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 257
    iget v0, p0, Lfvw;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 258
    iget v0, p0, Lfvw;->a:I

    if-lez v0, :cond_1

    iget v0, p0, Lfvw;->a:I

    const/16 v1, 0x1f4

    if-gt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 260
    :cond_0
    return-void

    .line 258
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
