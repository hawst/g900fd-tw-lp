.class public abstract Lfju;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lflb;
.implements Lfqh;
.implements Lfrx;


# instance fields
.field public final a:Lhgn;

.field final b:Lfqh;

.field final c:Lfqh;

.field public d:Lfnc;

.field public e:Ljava/lang/CharSequence;

.field private f:Lhog;


# direct methods
.method constructor <init>(Lhgn;Lfqh;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhgn;

    iput-object v0, p0, Lfju;->a:Lhgn;

    .line 85
    iput-object p2, p0, Lfju;->b:Lfqh;

    .line 86
    new-instance v0, Lfjv;

    invoke-direct {v0, p0}, Lfjv;-><init>(Lfju;)V

    iput-object v0, p0, Lfju;->c:Lfqh;

    .line 100
    iget-object v0, p1, Lhgn;->d:Lhgo;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lfju;->c()Lflb;

    move-result-object v0

    check-cast v0, Lfrx;

    invoke-interface {v0}, Lfrx;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lfju;->c()Lflb;

    move-result-object v0

    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    .line 187
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 118
    invoke-virtual {p0}, Lfju;->c()Lflb;

    move-result-object v0

    invoke-interface {v0}, Lflb;->b()Lhog;

    move-result-object v0

    return-object v0
.end method

.method public abstract c()Lflb;
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p0}, Lfju;->c()Lflb;

    move-result-object v0

    check-cast v0, Lfqh;

    invoke-interface {v0}, Lfqh;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Lfju;->c()Lflb;

    move-result-object v0

    check-cast v0, Lfqh;

    invoke-interface {v0}, Lfqh;->e()Lfqh;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lhog;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lfju;->f:Lhog;

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lfju;->a:Lhgn;

    iget-object v0, v0, Lhgn;->b:Lhog;

    iput-object v0, p0, Lfju;->f:Lhog;

    .line 130
    :cond_0
    iget-object v0, p0, Lfju;->f:Lhog;

    return-object v0
.end method
