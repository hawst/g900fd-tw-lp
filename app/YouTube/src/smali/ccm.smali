.class public final Lccm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/EditText;

.field private final d:Leyp;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILeyp;Lckc;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lccm;->d:Leyp;

    .line 51
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lccm;->a:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lccm;->a:Landroid/view/View;

    const v1, 0x7f0801c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lccm;->b:Landroid/widget/ImageView;

    .line 53
    iget-object v0, p0, Lccm;->a:Landroid/view/View;

    const v1, 0x7f0801c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lccm;->c:Landroid/widget/EditText;

    .line 54
    iget-object v0, p0, Lccm;->c:Landroid/widget/EditText;

    new-instance v1, Lccn;

    invoke-direct {v1, p0, p4}, Lccn;-><init>(Lccm;Lckc;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 3

    .prologue
    .line 27
    check-cast p2, Lccp;

    iget-boolean v0, p2, Lccp;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccm;->c:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lccm;->c:Landroid/widget/EditText;

    const v1, 0x7f0902b5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    :goto_0
    iget-object v0, p2, Lccp;->b:Lgca;

    if-nez v0, :cond_1

    iget-object v0, p0, Lccm;->b:Landroid/widget/ImageView;

    const v1, 0x7f0201e9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    iget-object v0, p0, Lccm;->a:Landroid/view/View;

    return-object v0

    :cond_0
    iget-object v0, p0, Lccm;->c:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lccm;->c:Landroid/widget/EditText;

    const v1, 0x7f09020c

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lccm;->d:Leyp;

    iget-object v1, p2, Lccp;->b:Lgca;

    iget-object v1, v1, Lgca;->e:Landroid/net/Uri;

    iget-object v2, p0, Lccm;->b:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, Leyi;->a(Leyp;Landroid/net/Uri;Landroid/widget/ImageView;)V

    goto :goto_1
.end method
