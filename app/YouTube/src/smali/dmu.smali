.class public abstract Ldmu;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Ldni;
.implements Ldnn;


# instance fields
.field a:Ljava/util/Map;

.field b:Z

.field c:Ljava/util/Set;

.field public d:Ldne;

.field public e:Ldmq;

.field private f:Ldmx;

.field private g:Ldmz;

.field private h:I

.field private i:Landroid/content/SharedPreferences;

.field private j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 513
    return-void
.end method

.method static synthetic a(Ldmu;I)I
    .locals 0

    .prologue
    .line 37
    iput p1, p0, Ldmu;->h:I

    return p1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;Ldmw;)Lfad;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 94
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    new-instance v0, Ldmv;

    invoke-direct {v0, p1, p2}, Ldmv;-><init>(Ljava/lang/Class;Ldmw;)V

    .line 107
    iget-boolean v1, v0, Lfad;->a:Z

    if-nez v1, :cond_0

    iput-boolean v3, v0, Lfad;->a:Z

    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, Lfad;->c:Ljava/lang/Class;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, v0, Lfad;->d:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not bind to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    return-object v0
.end method

.method static synthetic a(Ldmu;Ljava/util/Map;)Ljava/util/Map;
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Ldmu;->a:Ljava/util/Map;

    return-object p1
.end method

.method static synthetic a(Ldmu;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldmu;->c:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic a(Ldmu;Z)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldmu;->b:Z

    return v0
.end method

.method static synthetic b(Ldmu;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Ldmu;->a:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c(Ldmu;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Ldmu;->h:I

    return v0
.end method

.method static synthetic d(Ldmu;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ldmu;->k()V

    return-void
.end method

.method static synthetic e(Ldmu;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ldmu;->l()V

    return-void
.end method

.method static synthetic f(Ldmu;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ldmu;->m()V

    return-void
.end method

.method static synthetic g(Ldmu;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ldmu;->n()V

    return-void
.end method

.method private k()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 477
    .line 478
    const v0, 0x7f09010b

    invoke-virtual {p0, v0}, Ldmu;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 481
    iget-object v2, p0, Ldmu;->i:Landroid/content/SharedPreferences;

    .line 482
    invoke-virtual {p0}, Ldmu;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 483
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 485
    iget-object v2, p0, Ldmu;->d:Ldne;

    const/4 v3, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, Ldne;->a(IIILjava/lang/Object;)I

    move-result v0

    iput v0, p0, Ldmu;->h:I

    .line 486
    return-void

    :cond_0
    move v0, v1

    .line 485
    goto :goto_0
.end method

.method private l()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 489
    invoke-virtual {p0}, Ldmu;->g()Ljava/lang/String;

    move-result-object v0

    .line 490
    if-eqz v0, :cond_0

    iget-object v2, p0, Ldmu;->i:Landroid/content/SharedPreferences;

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 491
    :goto_0
    iget-object v2, p0, Ldmu;->d:Ldne;

    const/4 v3, 0x5

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, Ldne;->a(IIILjava/lang/Object;)I

    move-result v0

    iput v0, p0, Ldmu;->h:I

    .line 492
    return-void

    :cond_0
    move v0, v1

    .line 490
    goto :goto_0

    :cond_1
    move v0, v1

    .line 491
    goto :goto_1
.end method

.method private m()V
    .locals 5

    .prologue
    .line 495
    invoke-virtual {p0}, Ldmu;->h()Ljava/lang/String;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_0

    iget-object v1, p0, Ldmu;->i:Landroid/content/SharedPreferences;

    .line 497
    invoke-virtual {p0}, Ldmu;->i()I

    move-result v2

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 498
    :goto_0
    iget-object v1, p0, Ldmu;->d:Ldne;

    const/4 v2, 0x6

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Ldne;->a(IIILjava/lang/Object;)I

    move-result v0

    iput v0, p0, Ldmu;->h:I

    .line 499
    return-void

    .line 497
    :cond_0
    invoke-virtual {p0}, Ldmu;->i()I

    move-result v0

    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 506
    invoke-virtual {p0}, Ldmu;->j()Ljava/lang/String;

    move-result-object v0

    .line 507
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 508
    iget-object v1, p0, Ldmu;->i:Landroid/content/SharedPreferences;

    const v2, 0x7fffffff

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 509
    iget-object v1, p0, Ldmu;->e:Ldmq;

    invoke-virtual {v1, v0}, Ldmq;->a(I)V

    .line 511
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Ldmu;->d:Ldne;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ldne;->a(I)I

    move-result v0

    return v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 386
    iget-object v0, p0, Ldmu;->f:Ldmx;

    iget-object v1, p0, Ldmu;->f:Ldmx;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Ldmx;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmx;->sendMessage(Landroid/os/Message;)Z

    .line 387
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 3

    .prologue
    .line 356
    iget-object v0, p0, Ldmu;->f:Ldmx;

    iget-object v1, p0, Ldmu;->f:Ldmx;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Ldmx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmx;->sendMessage(Landroid/os/Message;)Z

    .line 357
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 300
    const/16 v0, 0x14

    return v0
.end method

.method public b(Lgjm;)V
    .locals 3

    .prologue
    .line 366
    iget-object v0, p0, Ldmu;->f:Ldmx;

    iget-object v1, p0, Ldmu;->f:Ldmx;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, p1}, Ldmx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmx;->sendMessage(Landroid/os/Message;)Z

    .line 367
    return-void
.end method

.method public c()Ldmz;
    .locals 1

    .prologue
    .line 304
    new-instance v0, Ldmz;

    invoke-direct {v0, p0}, Ldmz;-><init>(Ldmu;)V

    return-object v0
.end method

.method public c(Lgjm;)V
    .locals 3

    .prologue
    .line 381
    iget-object v0, p0, Ldmu;->f:Ldmx;

    iget-object v1, p0, Ldmu;->f:Ldmx;

    const/4 v2, 0x7

    invoke-virtual {v1, v2, p1}, Ldmx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmx;->sendMessage(Landroid/os/Message;)Z

    .line 382
    return-void
.end method

.method public final d(Lgjm;)V
    .locals 3

    .prologue
    .line 361
    iget-object v0, p0, Ldmu;->f:Ldmx;

    iget-object v1, p0, Ldmu;->f:Ldmx;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p1}, Ldmx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmx;->sendMessage(Landroid/os/Message;)Z

    .line 362
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x1

    return v0
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public final e(Lgjm;)V
    .locals 3

    .prologue
    .line 371
    iget-object v0, p0, Ldmu;->f:Ldmx;

    iget-object v1, p0, Ldmu;->f:Ldmx;

    const/4 v2, 0x5

    invoke-virtual {v1, v2, p1}, Ldmx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmx;->sendMessage(Landroid/os/Message;)Z

    .line 372
    return-void
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public final f(Lgjm;)V
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Ldmu;->f:Ldmx;

    iget-object v1, p0, Ldmu;->f:Ldmx;

    const/4 v2, 0x6

    invoke-virtual {v1, v2, p1}, Ldmx;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldmx;->sendMessage(Landroid/os/Message;)Z

    .line 377
    return-void
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public i()I
    .locals 1

    .prologue
    .line 502
    const v0, 0x7fffffff

    return v0
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Ldmu;->g:Ldmz;

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    .line 115
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 116
    const-string v0, "creating transfer service"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 118
    new-instance v0, Ldmx;

    invoke-direct {v0, p0}, Ldmx;-><init>(Ldmu;)V

    iput-object v0, p0, Ldmu;->f:Ldmx;

    .line 119
    new-instance v0, Ldne;

    .line 123
    invoke-virtual {p0}, Ldmu;->e()Ljava/lang/String;

    move-result-object v4

    .line 124
    invoke-virtual {p0}, Ldmu;->b()I

    move-result v5

    .line 125
    invoke-virtual {p0}, Ldmu;->d()Z

    move-result v6

    move-object v1, p0

    move-object v2, p0

    move-object v3, p0

    invoke-direct/range {v0 .. v6}, Ldne;-><init>(Landroid/content/Context;Ldni;Ldnn;Ljava/lang/String;IZ)V

    iput-object v0, p0, Ldmu;->d:Ldne;

    .line 126
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Ldmu;->c:Ljava/util/Set;

    .line 127
    invoke-virtual {p0}, Ldmu;->c()Ldmz;

    move-result-object v0

    iput-object v0, p0, Ldmu;->g:Ldmz;

    .line 129
    invoke-virtual {p0}, Ldmu;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Ldmq;

    invoke-direct {v0}, Ldmq;-><init>()V

    iput-object v0, p0, Ldmu;->e:Ldmq;

    .line 133
    :cond_0
    invoke-virtual {p0}, Ldmu;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    invoke-virtual {p0}, Ldmu;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 135
    invoke-virtual {p0}, Ldmu;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p0}, Ldmu;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    :cond_1
    invoke-virtual {p0}, Ldmu;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lckz;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ldmu;->i:Landroid/content/SharedPreferences;

    .line 138
    new-instance v0, Ldmy;

    invoke-direct {v0, p0}, Ldmy;-><init>(Ldmu;)V

    iput-object v0, p0, Ldmu;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 139
    iget-object v0, p0, Ldmu;->i:Landroid/content/SharedPreferences;

    iget-object v1, p0, Ldmu;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 141
    :cond_2
    invoke-direct {p0}, Ldmu;->k()V

    .line 142
    invoke-direct {p0}, Ldmu;->l()V

    .line 143
    invoke-direct {p0}, Ldmu;->m()V

    .line 144
    invoke-direct {p0}, Ldmu;->n()V

    .line 148
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {p0, v0}, Ldmu;->a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldmu;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 149
    invoke-virtual {p0}, Ldmu;->a()I

    move-result v0

    iput v0, p0, Ldmu;->h:I

    .line 150
    return-void
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    .line 169
    iget-object v0, p0, Ldmu;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Ldmu;->i:Landroid/content/SharedPreferences;

    iget-object v1, p0, Ldmu;->j:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 172
    :cond_0
    iget-object v1, p0, Ldmu;->d:Ldne;

    :goto_0
    iget-object v0, v1, Ldne;->p:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "wifiLock held in quit"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    iget-object v0, v1, Ldne;->p:Landroid/net/wifi/WifiManager$WifiLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$WifiLock;->release()V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Ldne;->s:Ldnj;

    iget-object v2, v0, Ldnj;->b:Ldne;

    iget-object v2, v2, Ldne;->a:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, v1, Ldne;->r:Ldnl;

    iget-object v2, v0, Ldnl;->c:Ldne;

    iget-object v2, v2, Ldne;->a:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, v1, Ldne;->t:Ldnh;

    iget-object v2, v0, Ldnh;->b:Ldne;

    iget-object v2, v2, Ldne;->a:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v2, v1, Ldne;->f:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget v0, v1, Ldne;->h:I

    iget v3, v1, Ldne;->g:I

    sub-int v3, v0, v3

    if-nez v3, :cond_2

    const/4 v0, 0x1

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x1d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "pendingMessages = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lb;->d(ZLjava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, v1, Ldne;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    iget-object v0, v1, Ldne;->j:Ldnc;

    iget-object v1, v0, Ldnc;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    const/4 v1, 0x0

    iput-object v1, v0, Ldnc;->a:Landroid/database/sqlite/SQLiteDatabase;

    .line 173
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 174
    return-void

    .line 172
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    return v0
.end method
