.class public Lcqh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Laca;

.field c:Labz;

.field final d:Lacb;

.field e:Ljava/util/Set;

.field f:Ljava/util/Set;

.field g:Z

.field private h:Ljava/util/Set;

.field private i:Z

.field private final j:Ljava/lang/String;

.field private final k:I

.field private final l:Lcwx;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILcwx;ZLjava/lang/String;)V
    .locals 7

    .prologue
    .line 53
    .line 54
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v6, Lacb;

    invoke-direct {v6}, Lacb;-><init>()V

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    .line 53
    invoke-direct/range {v0 .. v6}, Lcqh;-><init>(Ljava/lang/String;ILcwx;ZLjava/lang/String;Lacb;)V

    .line 60
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcwx;ZLjava/lang/String;Lacb;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcqh;->a:Ljava/lang/String;

    .line 71
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacb;

    iput-object v0, p0, Lcqh;->d:Lacb;

    .line 73
    iput-boolean p4, p0, Lcqh;->i:Z

    .line 74
    iput-object p5, p0, Lcqh;->j:Ljava/lang/String;

    .line 75
    iput p2, p0, Lcqh;->k:I

    .line 76
    iput-object p3, p0, Lcqh;->l:Lcwx;

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcqh;->h:Ljava/util/Set;

    .line 79
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcqh;->b:Laca;

    iput-object p1, v0, Laca;->a:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcqh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const-string v0, "CsiAction not yet started."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 90
    :cond_0
    iget-object v0, p0, Lcqh;->b:Laca;

    iget-object v0, v0, Laca;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcqh;->b:Laca;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcqh;->c:Labz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Levd;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    invoke-virtual {p0}, Lcqh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "CsiAction ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] not yet started."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 138
    :cond_0
    :goto_0
    return v2

    .line 119
    :cond_1
    iget-object v0, p0, Lcqh;->h:Ljava/util/Set;

    iget-object v3, p1, Levd;->g:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    const-string v0, "CsiAction [%s] already ticked %s. Ignored."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 122
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 123
    iget-object v4, p1, Levd;->g:Ljava/lang/String;

    aput-object v4, v3, v1

    .line 120
    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 136
    :goto_1
    iget-boolean v3, p0, Lcqh;->g:Z

    iget-object v0, p0, Lcqh;->f:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcqh;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-le v0, v1, :cond_5

    move v0, v1

    :goto_2
    or-int/2addr v0, v3

    iput-boolean v0, p0, Lcqh;->g:Z

    .line 138
    iget-object v0, p0, Lcqh;->e:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcqh;->g:Z

    if-eqz v0, :cond_0

    :cond_2
    move v2, v1

    goto :goto_0

    .line 125
    :cond_3
    iget-object v0, p1, Levd;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 126
    iget-object v0, p0, Lcqh;->b:Laca;

    iget-object v3, p0, Lcqh;->c:Labz;

    invoke-virtual {p1}, Levd;->a()J

    move-result-wide v4

    new-array v6, v1, [Ljava/lang/String;

    iget-object v7, p1, Levd;->g:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-virtual {v0, v3, v4, v5, v6}, Laca;->a(Labz;J[Ljava/lang/String;)Z

    .line 127
    iget-object v0, p0, Lcqh;->h:Ljava/util/Set;

    iget-object v3, p1, Levd;->g:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 129
    :cond_4
    const-string v0, "CsiAction [%s] triggered with no registered label"

    new-array v3, v1, [Ljava/lang/Object;

    .line 131
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 129
    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move v0, v2

    .line 136
    goto :goto_2
.end method

.method public final b()Laca;
    .locals 2

    .prologue
    .line 151
    invoke-virtual {p0}, Lcqh;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    const-string v0, "CsiAction.start() should be called before report. Ignored."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 153
    const/4 v0, 0x0

    .line 164
    :goto_0
    return-object v0

    .line 155
    :cond_0
    const-string v1, "mod_li"

    iget-boolean v0, p0, Lcqh;->i:Z

    if-eqz v0, :cond_3

    const-string v0, "1"

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcqh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    const-string v0, ""

    iget-object v1, p0, Lcqh;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 157
    const-string v0, "egs"

    iget-object v1, p0, Lcqh;->j:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcqh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_1
    const-string v0, "conn"

    iget v1, p0, Lcqh;->k:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcqh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcqh;->l:Lcwx;

    if-eqz v0, :cond_2

    .line 161
    const-string v0, "vis"

    iget-object v1, p0, Lcqh;->l:Lcwx;

    iget v1, v1, Lcwx;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcqh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    :cond_2
    iget-object v0, p0, Lcqh;->b:Laca;

    goto :goto_0

    .line 155
    :cond_3
    const-string v0, "0"

    goto :goto_1
.end method
