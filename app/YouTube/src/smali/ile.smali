.class public final Lile;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lild;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Likz;Likx;)[J
    .locals 16

    .prologue
    .line 43
    invoke-virtual/range {p1 .. p1}, Likz;->c()Ljava/util/List;

    move-result-object v8

    .line 45
    const-wide/16 v6, 0x0

    .line 46
    move-object/from16 v0, p2

    iget-object v2, v0, Likx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Likz;

    .line 47
    const-wide/16 v4, 0x0

    invoke-virtual {v3}, Likz;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzq;

    iget-wide v12, v2, Lzq;->a:J

    iget-wide v14, v2, Lzq;->b:J

    mul-long/2addr v12, v14

    add-long/2addr v4, v12

    goto :goto_1

    :cond_0
    invoke-virtual {v3}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v2, v2, Lila;->b:J

    div-long v2, v4, v2

    long-to-double v2, v2

    .line 48
    cmpg-double v4, v6, v2

    if-gez v4, :cond_7

    :goto_2
    move-wide v6, v2

    .line 51
    goto :goto_0

    .line 53
    :cond_1
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double v2, v6, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    add-int/lit8 v2, v2, -0x1

    .line 54
    if-gtz v2, :cond_2

    .line 55
    const/4 v2, 0x1

    .line 58
    :cond_2
    new-array v9, v2, [J

    .line 59
    const-wide/16 v2, -0x1

    invoke-static {v9, v2, v3}, Ljava/util/Arrays;->fill([JJ)V

    .line 60
    const/4 v2, 0x0

    const-wide/16 v4, 0x1

    aput-wide v4, v9, v2

    .line 62
    const-wide/16 v4, 0x0

    .line 63
    const/4 v2, 0x0

    .line 64
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v2

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzq;

    .line 65
    const/4 v6, 0x0

    :goto_3
    int-to-long v10, v6

    iget-wide v12, v2, Lzq;->a:J

    cmp-long v7, v10, v12

    if-gez v7, :cond_3

    .line 66
    invoke-virtual/range {p1 .. p1}, Likz;->g()Lila;

    move-result-object v7

    iget-wide v10, v7, Lila;->b:J

    div-long v10, v4, v10

    const-wide/16 v12, 0x2

    div-long/2addr v10, v12

    long-to-int v7, v10

    add-int/lit8 v10, v7, 0x1

    .line 67
    array-length v7, v9

    if-ge v10, v7, :cond_3

    .line 68
    add-int/lit8 v7, v3, 0x1

    add-int/lit8 v3, v3, 0x1

    int-to-long v12, v3

    aput-wide v12, v9, v10

    .line 71
    iget-wide v10, v2, Lzq;->b:J

    add-long/2addr v4, v10

    .line 65
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v7

    goto :goto_3

    .line 74
    :cond_4
    add-int/lit8 v2, v3, 0x1

    int-to-long v4, v2

    .line 76
    array-length v2, v9

    add-int/lit8 v2, v2, -0x1

    :goto_4
    if-ltz v2, :cond_6

    .line 77
    aget-wide v6, v9, v2

    const-wide/16 v10, -0x1

    cmp-long v3, v6, v10

    if-nez v3, :cond_5

    .line 78
    aput-wide v4, v9, v2

    .line 80
    :cond_5
    aget-wide v4, v9, v2

    .line 76
    add-int/lit8 v2, v2, -0x1

    goto :goto_4

    .line 82
    :cond_6
    return-object v9

    :cond_7
    move-wide v2, v6

    goto :goto_2
.end method
