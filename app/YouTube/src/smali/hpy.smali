.class public final Lhpy;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhpy;


# instance fields
.field public b:Lhqa;

.field public c:Lhpz;

.field public d:Lhqb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lhpy;

    sput-object v0, Lhpy;->a:[Lhpy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Lhpy;->b:Lhqa;

    iput-object v0, p0, Lhpy;->c:Lhpz;

    iput-object v0, p0, Lhpy;->d:Lhqb;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lhpy;->b:Lhqa;

    if-eqz v1, :cond_0

    const v0, 0x4a05067

    iget-object v1, p0, Lhpy;->b:Lhqa;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Lhpy;->c:Lhpz;

    if-eqz v1, :cond_1

    const v1, 0x4a18621

    iget-object v2, p0, Lhpy;->c:Lhpz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhpy;->d:Lhqb;

    if-eqz v1, :cond_2

    const v1, 0x4a44aae

    iget-object v2, p0, Lhpy;->d:Lhqb;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhpy;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhpy;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhpy;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhpy;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhpy;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhpy;->b:Lhqa;

    if-nez v0, :cond_2

    new-instance v0, Lhqa;

    invoke-direct {v0}, Lhqa;-><init>()V

    iput-object v0, p0, Lhpy;->b:Lhqa;

    :cond_2
    iget-object v0, p0, Lhpy;->b:Lhqa;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhpy;->c:Lhpz;

    if-nez v0, :cond_3

    new-instance v0, Lhpz;

    invoke-direct {v0}, Lhpz;-><init>()V

    iput-object v0, p0, Lhpy;->c:Lhpz;

    :cond_3
    iget-object v0, p0, Lhpy;->c:Lhpz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhpy;->d:Lhqb;

    if-nez v0, :cond_4

    new-instance v0, Lhqb;

    invoke-direct {v0}, Lhqb;-><init>()V

    iput-object v0, p0, Lhpy;->d:Lhqb;

    :cond_4
    iget-object v0, p0, Lhpy;->d:Lhqb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2502833a -> :sswitch_1
        0x250c310a -> :sswitch_2
        0x25225572 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Lhpy;->b:Lhqa;

    if-eqz v0, :cond_0

    const v0, 0x4a05067

    iget-object v1, p0, Lhpy;->b:Lhqa;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhpy;->c:Lhpz;

    if-eqz v0, :cond_1

    const v0, 0x4a18621

    iget-object v1, p0, Lhpy;->c:Lhpz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhpy;->d:Lhqb;

    if-eqz v0, :cond_2

    const v0, 0x4a44aae

    iget-object v1, p0, Lhpy;->d:Lhqb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhpy;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhpy;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhpy;

    iget-object v2, p0, Lhpy;->b:Lhqa;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhpy;->b:Lhqa;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhpy;->c:Lhpz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhpy;->c:Lhpz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhpy;->d:Lhqb;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhpy;->d:Lhqb;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhpy;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhpy;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhpy;->b:Lhqa;

    iget-object v3, p1, Lhpy;->b:Lhqa;

    invoke-virtual {v2, v3}, Lhqa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhpy;->c:Lhpz;

    iget-object v3, p1, Lhpy;->c:Lhpz;

    invoke-virtual {v2, v3}, Lhpz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhpy;->d:Lhqb;

    iget-object v3, p1, Lhpy;->d:Lhqb;

    invoke-virtual {v2, v3}, Lhqb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhpy;->I:Ljava/util/List;

    iget-object v3, p1, Lhpy;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhpy;->b:Lhqa;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhpy;->c:Lhpz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhpy;->d:Lhqb;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhpy;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Lhpy;->b:Lhqa;

    invoke-virtual {v0}, Lhqa;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhpy;->c:Lhpz;

    invoke-virtual {v0}, Lhpz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhpy;->d:Lhqb;

    invoke-virtual {v0}, Lhqb;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lhpy;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
