.class public final Lzs;
.super Likv;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:I

.field public f:I

.field public g:F

.field public h:[J

.field public i:D

.field public j:D


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "tkhd"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 46
    const/16 v0, 0x9

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    iput-object v0, p0, Lzs;->h:[J

    .line 54
    return-void

    .line 46
    nop

    :array_0
    .array-data 8
        0x10000
        0x0
        0x0
        0x0
        0x10000
        0x0
        0x0
        0x0
        0x40000000
    .end array-data
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 5

    .prologue
    const/16 v4, 0x9

    .line 109
    invoke-virtual {p0, p1}, Lzs;->c(Ljava/nio/ByteBuffer;)J

    .line 110
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 111
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->a:J

    .line 112
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->b:J

    .line 113
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->c:J

    .line 114
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    .line 115
    invoke-static {p1}, La;->f(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->d:J

    .line 123
    :goto_0
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    .line 124
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    .line 125
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lzs;->e:I

    .line 126
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Lzs;->f:I

    .line 127
    invoke-static {p1}, La;->h(Ljava/nio/ByteBuffer;)F

    move-result v0

    iput v0, p0, Lzs;->g:F

    .line 128
    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    .line 129
    new-array v0, v4, [J

    iput-object v0, p0, Lzs;->h:[J

    .line 130
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_1

    .line 131
    iget-object v1, p0, Lzs;->h:[J

    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 117
    :cond_0
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->a:J

    .line 118
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->b:J

    .line 119
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->c:J

    .line 120
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    .line 121
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Lzs;->d:J

    goto :goto_0

    .line 133
    :cond_1
    invoke-static {p1}, La;->g(Ljava/nio/ByteBuffer;)D

    move-result-wide v0

    iput-wide v0, p0, Lzs;->i:D

    .line 134
    invoke-static {p1}, La;->g(Ljava/nio/ByteBuffer;)D

    move-result-wide v0

    iput-wide v0, p0, Lzs;->j:D

    .line 135
    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 138
    invoke-virtual {p0, p1}, Lzs;->d(Ljava/nio/ByteBuffer;)V

    .line 139
    iget v1, p0, Likv;->o:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 140
    iget-wide v2, p0, Lzs;->a:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 141
    iget-wide v2, p0, Lzs;->b:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 142
    iget-wide v2, p0, Lzs;->c:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 143
    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 144
    iget-wide v2, p0, Lzs;->d:J

    invoke-static {p1, v2, v3}, Lyf;->a(Ljava/nio/ByteBuffer;J)V

    .line 152
    :goto_0
    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 153
    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 154
    iget v1, p0, Lzs;->e:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 155
    iget v1, p0, Lzs;->f:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 156
    iget v1, p0, Lzs;->g:F

    float-to-double v2, v1

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;D)V

    .line 157
    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 158
    :goto_1
    const/16 v1, 0x9

    if-ge v0, v1, :cond_1

    .line 159
    iget-object v1, p0, Lzs;->h:[J

    aget-wide v2, v1, v0

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 146
    :cond_0
    iget-wide v2, p0, Lzs;->a:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 147
    iget-wide v2, p0, Lzs;->b:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 148
    iget-wide v2, p0, Lzs;->c:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 149
    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 150
    iget-wide v2, p0, Lzs;->d:J

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    goto :goto_0

    .line 161
    :cond_1
    iget-wide v0, p0, Lzs;->i:D

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;D)V

    .line 162
    iget-wide v0, p0, Lzs;->j:D

    invoke-static {p1, v0, v1}, Lyf;->a(Ljava/nio/ByteBuffer;D)V

    .line 163
    return-void
.end method

.method protected final d_()J
    .locals 4

    .prologue
    .line 97
    iget v0, p0, Likv;->o:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 99
    const-wide/16 v0, 0x24

    .line 103
    :goto_0
    const-wide/16 v2, 0x3c

    add-long/2addr v0, v2

    .line 104
    return-wide v0

    .line 101
    :cond_0
    const-wide/16 v0, 0x18

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 166
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    const-string v0, "TrackHeaderBox["

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    const-string v0, "creationTime="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzs;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 169
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    const-string v0, "modificationTime="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzs;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 171
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string v0, "trackId="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzs;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 173
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    const-string v0, "duration="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzs;->d:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 175
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const-string v0, "layer="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lzs;->e:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 177
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    const-string v0, "alternateGroup="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lzs;->f:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 179
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    const-string v0, "volume="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lzs;->g:F

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 181
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lzs;->h:[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 182
    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    const-string v2, "matrix"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lzs;->h:[J

    aget-wide v4, v3, v0

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_0
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 186
    const-string v0, "width="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzs;->i:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 187
    const-string v0, ";"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    const-string v0, "height="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lzs;->j:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 189
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
