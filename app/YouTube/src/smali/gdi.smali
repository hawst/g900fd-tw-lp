.class public final Lgdi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgeh;


# instance fields
.field private final a:Lgeh;

.field private final b:Lgeh;

.field private final c:Landroid/content/Context;

.field private final d:Lggn;

.field private e:Lgeh;

.field private f:Z

.field private g:Lgec;


# direct methods
.method public constructor <init>(Lgeh;Lgeh;Landroid/content/Context;Lggn;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeh;

    iput-object v0, p0, Lgdi;->a:Lgeh;

    .line 48
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeh;

    iput-object v0, p0, Lgdi;->b:Lgeh;

    .line 49
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lgdi;->c:Landroid/content/Context;

    .line 50
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggn;

    iput-object v0, p0, Lgdi;->d:Lggn;

    .line 51
    iput-object p1, p0, Lgdi;->e:Lgeh;

    .line 52
    return-void
.end method

.method private static a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 130
    const/4 v1, 0x2

    const/16 v2, 0x40

    :try_start_0
    invoke-static {v1, v2}, Lebk;->a(II)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 135
    :goto_0
    return v0

    .line 133
    :catch_0
    move-exception v1

    goto :goto_0

    .line 135
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private a(Lfrf;Lfqy;)Z
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-static {}, Lgdi;->a()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v2, v4

    .line 110
    :cond_1
    :goto_0
    return v2

    .line 90
    :cond_2
    iget-boolean v0, p1, Lfrf;->h:Z

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lfrf;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v2, v4

    .line 91
    goto :goto_0

    .line 95
    :cond_4
    iget-object v0, p1, Lfrf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lgdi;->d:Lggn;

    invoke-virtual {v0}, Lggn;->a()Lfrb;

    move-result-object v0

    invoke-virtual {p2, v0}, Lfqy;->a(Lfrb;)Z

    move-result v0

    if-nez v0, :cond_5

    move v2, v4

    .line 101
    goto :goto_0

    .line 105
    :cond_5
    invoke-static {}, Lfqm;->h()Ljava/util/Set;

    move-result-object v5

    invoke-static {}, Lfqm;->k()Ljava/util/Set;

    move-result-object v6

    iget-object v0, p1, Lfrf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v1, v2

    move v3, v2

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v0, v0, Lfqj;->a:Lhgy;

    iget v0, v0, Lhgy;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    or-int/2addr v3, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    or-int/2addr v0, v1

    move v1, v0

    goto :goto_1

    :cond_6
    if-eqz v3, :cond_7

    if-eqz v1, :cond_7

    move v0, v4

    :goto_2
    if-nez v0, :cond_1

    move v2, v4

    .line 106
    goto :goto_0

    :cond_7
    move v0, v2

    .line 105
    goto :goto_2
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lgdi;->a:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(F)V

    .line 253
    iget-object v0, p0, Lgdi;->b:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(F)V

    .line 254
    return-void
.end method

.method public final a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lgdi;->a:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(Landroid/os/Handler;)V

    .line 57
    iget-object v0, p0, Lgdi;->b:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(Landroid/os/Handler;)V

    .line 58
    return-void
.end method

.method public final a(Lfrf;ILjava/lang/String;Lfqy;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 72
    invoke-direct {p0, p1, p4}, Lgdi;->a(Lfrf;Lfqy;)Z

    move-result v1

    .line 74
    if-eqz v1, :cond_3

    iget-object v0, p0, Lgdi;->b:Lgeh;

    :goto_0
    iget-object v2, p0, Lgdi;->e:Lgeh;

    if-eq v0, v2, :cond_1

    iget-boolean v2, p0, Lgdi;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgdi;->e:Lgeh;

    invoke-interface {v2}, Lgeh;->o()V

    iget-object v2, p0, Lgdi;->g:Lgec;

    invoke-interface {v0, v2}, Lgeh;->a(Lgec;)V

    :cond_0
    iput-object v0, p0, Lgdi;->e:Lgeh;

    .line 76
    :cond_1
    iget-object v0, p0, Lgdi;->d:Lggn;

    iget-object v0, v0, Lggn;->a:Landroid/content/SharedPreferences;

    const-string v2, "show_exo_player_debug_messages"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 77
    if-eqz v1, :cond_4

    const-string v0, "Using fallback player"

    :goto_1
    iget-object v1, p0, Lgdi;->c:Landroid/content/Context;

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 80
    :cond_2
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0, p1, p2, p3, p4}, Lgeh;->a(Lfrf;ILjava/lang/String;Lfqy;)V

    .line 81
    return-void

    .line 74
    :cond_3
    iget-object v0, p0, Lgdi;->a:Lgeh;

    goto :goto_0

    .line 77
    :cond_4
    const-string v0, "Using ExoPlayer"

    goto :goto_1
.end method

.method public final a(Lgec;)V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgdi;->f:Z

    .line 274
    iput-object p1, p0, Lgdi;->g:Lgec;

    .line 275
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(Lgec;)V

    .line 276
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->a(Z)V

    .line 233
    return-void
.end method

.method public final a(Lfrf;Lfqy;Z)[Lfqj;
    .locals 1

    .prologue
    .line 174
    .line 175
    invoke-direct {p0, p1, p2}, Lgdi;->a(Lfrf;Lfqy;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdi;->b:Lgeh;

    .line 176
    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lgeh;->a(Lfrf;Lfqy;Z)[Lfqj;

    move-result-object v0

    return-object v0

    .line 175
    :cond_0
    iget-object v0, p0, Lgdi;->a:Lgeh;

    goto :goto_0
.end method

.method public final b(Lfrf;Lfqy;Z)Lger;
    .locals 1

    .prologue
    .line 183
    .line 184
    invoke-direct {p0, p1, p2}, Lgdi;->a(Lfrf;Lfqy;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgdi;->b:Lgeh;

    .line 185
    :goto_0
    invoke-interface {v0, p1, p2, p3}, Lgeh;->b(Lfrf;Lfqy;Z)Lger;

    move-result-object v0

    return-object v0

    .line 184
    :cond_0
    iget-object v0, p0, Lgdi;->a:Lgeh;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->b()V

    .line 142
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->b(I)V

    .line 218
    return-void
.end method

.method public final b(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lgdi;->a:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->b(Landroid/os/Handler;)V

    .line 63
    iget-object v0, p0, Lgdi;->b:Lgeh;

    invoke-interface {v0, p1}, Lgeh;->b(Landroid/os/Handler;)V

    .line 64
    return-void
.end method

.method public final c()Lfqj;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->c()Lfqj;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lfqj;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->d()Lfqj;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->e()V

    .line 208
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->f()V

    .line 213
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->g()V

    .line 223
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->h()V

    .line 228
    return-void
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->i()I

    move-result v0

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->j()I

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->k()I

    move-result v0

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->l()Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->m()Z

    move-result v0

    return v0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lgdi;->e:Lgeh;

    invoke-interface {v0}, Lgeh;->n()V

    .line 269
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgdi;->f:Z

    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lgdi;->g:Lgec;

    .line 282
    iget-object v0, p0, Lgdi;->a:Lgeh;

    invoke-interface {v0}, Lgeh;->o()V

    .line 283
    iget-object v0, p0, Lgdi;->b:Lgeh;

    invoke-interface {v0}, Lgeh;->o()V

    .line 284
    return-void
.end method
