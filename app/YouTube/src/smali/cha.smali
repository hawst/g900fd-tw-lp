.class public final Lcha;
.super Lcby;
.source "SourceFile"


# instance fields
.field private final f:Landroid/view/View;

.field private final g:Landroid/content/Context;

.field private final h:Lboi;

.field private final i:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;Lboi;Lfdw;Lfrz;)V
    .locals 8

    .prologue
    .line 43
    const v5, 0x7f040042

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcby;-><init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;ILfdw;Lfrz;)V

    .line 51
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcha;->g:Landroid/content/Context;

    .line 52
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcha;->i:Lfsj;

    .line 53
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcha;->h:Lboi;

    .line 55
    iget-object v0, p0, Lcby;->c:Landroid/view/View;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcha;->f:Landroid/view/View;

    .line 56
    return-void
.end method

.method private a(Lfsg;Lfmb;)Landroid/view/View;
    .locals 3

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lcby;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcha;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 66
    iget-object v0, p0, Lcha;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 67
    const v2, 0x7f0b0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 69
    invoke-virtual {p2}, Lfmb;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcha;->a(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p2, Lfmb;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p2, Lfmb;->a:Lhsp;

    iget-object v0, v0, Lhsp;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfmb;->c:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p2, Lfmb;->c:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcha;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p2, Lfmb;->d:Lfnc;

    if-nez v0, :cond_1

    new-instance v0, Lfnc;

    iget-object v1, p2, Lfmb;->a:Lhsp;

    iget-object v1, v1, Lhsp;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p2, Lfmb;->d:Lfnc;

    :cond_1
    iget-object v0, p2, Lfmb;->d:Lfnc;

    iget-object v1, p2, Lfmb;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfmb;->a:Lhsp;

    iget-object v1, v1, Lhsp;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfmb;->b:Ljava/lang/CharSequence;

    :cond_2
    iget-object v1, p2, Lfmb;->b:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcha;->a(Lfnc;Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcha;->i:Lfsj;

    .line 77
    invoke-interface {v0}, Lfsj;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcha;->h:Lboi;

    .line 79
    iget-object v2, p0, Lcby;->e:Landroid/view/View;

    .line 76
    invoke-static {v0, v1, v2, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 82
    iget-object v0, p0, Lcha;->i:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Lfmb;

    invoke-direct {p0, p1, p2}, Lcha;->a(Lfsg;Lfmb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Lfmb;

    invoke-direct {p0, p1, p2}, Lcha;->a(Lfsg;Lfmb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 27
    check-cast p2, Lfmb;

    invoke-direct {p0, p1, p2}, Lcha;->a(Lfsg;Lfmb;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
