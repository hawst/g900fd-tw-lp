.class public final Libf;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:F

.field public h:F

.field public i:Ljava/lang/String;

.field public j:I

.field public k:Libg;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Libf;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Libf;->b:Ljava/lang/String;

    iput v1, p0, Libf;->c:I

    const-string v0, ""

    iput-object v0, p0, Libf;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Libf;->e:Ljava/lang/String;

    iput-boolean v1, p0, Libf;->f:Z

    iput v2, p0, Libf;->g:F

    iput v2, p0, Libf;->h:F

    const-string v0, ""

    iput-object v0, p0, Libf;->i:Ljava/lang/String;

    iput v1, p0, Libf;->j:I

    const/4 v0, 0x0

    iput-object v0, p0, Libf;->k:Libg;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Libf;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Libf;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Libf;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Libf;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Libf;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Libf;->c:I

    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Libf;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Libf;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Libf;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Libf;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Libf;->f:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-boolean v2, p0, Libf;->f:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Libf;->g:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Libf;->g:F

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Libf;->h:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget v2, p0, Libf;->h:F

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Libf;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const/16 v1, 0x9

    iget-object v2, p0, Libf;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Libf;->j:I

    if-eqz v1, :cond_9

    const/16 v1, 0xc

    iget v2, p0, Libf;->j:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Libf;->k:Libg;

    if-eqz v1, :cond_a

    const v1, 0x40fd70f

    iget-object v2, p0, Libf;->k:Libg;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Libf;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libf;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Libf;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Libf;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Libf;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libf;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libf;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Libf;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libf;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libf;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Libf;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Libf;->g:F

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Libf;->h:F

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libf;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Libf;->j:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Libf;->j:I

    goto :goto_0

    :sswitch_b
    iget-object v0, p0, Libf;->k:Libg;

    if-nez v0, :cond_4

    new-instance v0, Libg;

    invoke-direct {v0}, Libg;-><init>()V

    iput-object v0, p0, Libf;->k:Libg;

    :cond_4
    iget-object v0, p0, Libf;->k:Libg;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
        0x4a -> :sswitch_9
        0x60 -> :sswitch_a
        0x207eb87a -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Libf;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Libf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v0, p0, Libf;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Libf;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_1
    iget v0, p0, Libf;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Libf;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    :cond_2
    iget-object v0, p0, Libf;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Libf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Libf;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Libf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_4
    iget-boolean v0, p0, Libf;->f:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-boolean v1, p0, Libf;->f:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_5
    iget v0, p0, Libf;->g:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Libf;->g:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    :cond_6
    iget v0, p0, Libf;->h:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget v1, p0, Libf;->h:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    :cond_7
    iget-object v0, p0, Libf;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Libf;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_8
    iget v0, p0, Libf;->j:I

    if-eqz v0, :cond_9

    const/16 v0, 0xc

    iget v1, p0, Libf;->j:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_9
    iget-object v0, p0, Libf;->k:Libg;

    if-eqz v0, :cond_a

    const v0, 0x40fd70f

    iget-object v1, p0, Libf;->k:Libg;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_a
    iget-object v0, p0, Libf;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libf;

    iget-object v2, p0, Libf;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Libf;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Libf;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Libf;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget v2, p0, Libf;->c:I

    iget v3, p1, Libf;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Libf;->d:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Libf;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Libf;->e:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Libf;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-boolean v2, p0, Libf;->f:Z

    iget-boolean v3, p1, Libf;->f:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Libf;->g:F

    iget v3, p1, Libf;->g:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Libf;->h:F

    iget v3, p1, Libf;->h:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Libf;->i:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Libf;->i:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_5
    iget v2, p0, Libf;->j:I

    iget v3, p1, Libf;->j:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Libf;->k:Libg;

    if-nez v2, :cond_9

    iget-object v2, p1, Libf;->k:Libg;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Libf;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Libf;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Libf;->a:Ljava/lang/String;

    iget-object v3, p1, Libf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Libf;->b:Ljava/lang/String;

    iget-object v3, p1, Libf;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Libf;->d:Ljava/lang/String;

    iget-object v3, p1, Libf;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libf;->e:Ljava/lang/String;

    iget-object v3, p1, Libf;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Libf;->i:Ljava/lang/String;

    iget-object v3, p1, Libf;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Libf;->k:Libg;

    iget-object v3, p1, Libf;->k:Libg;

    invoke-virtual {v2, v3}, Libg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Libf;->I:Ljava/util/List;

    iget-object v3, p1, Libf;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libf;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libf;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libf;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libf;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libf;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Libf;->f:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libf;->g:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libf;->h:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libf;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Libf;->j:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libf;->k:Libg;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Libf;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Libf;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Libf;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Libf;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Libf;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    const/4 v0, 0x2

    goto :goto_4

    :cond_5
    iget-object v0, p0, Libf;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    iget-object v0, p0, Libf;->k:Libg;

    invoke-virtual {v0}, Libg;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_7
    iget-object v1, p0, Libf;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_7
.end method
