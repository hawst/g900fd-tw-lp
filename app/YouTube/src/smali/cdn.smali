.class public final Lcdn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field a:Lhog;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/ImageView;

.field private final d:Lfsj;

.field private final e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfhz;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcdn;->d:Lfsj;

    .line 40
    const v0, 0x7f04006c

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 41
    const v0, 0x7f0801d8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcdn;->b:Landroid/widget/TextView;

    .line 42
    const v0, 0x7f0801d9

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcdn;->c:Landroid/widget/ImageView;

    .line 43
    new-instance v0, Lcdo;

    invoke-direct {v0, p0, p3}, Lcdo;-><init>(Lcdn;Lfhz;)V

    iput-object v0, p0, Lcdn;->e:Landroid/view/View$OnClickListener;

    .line 51
    invoke-interface {p2, v1}, Lfsj;->a(Landroid/view/View;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 26
    check-cast p2, Lfuh;

    iget-object v0, p2, Lfuh;->d:Lhog;

    iput-object v0, p0, Lcdn;->a:Lhog;

    iget-object v3, p0, Lcdn;->d:Lfsj;

    iget-object v0, p2, Lfuh;->c:Landroid/view/View$OnClickListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcdn;->e:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-interface {v3, v0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p2, Lfuh;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcdn;->b:Landroid/widget/TextView;

    iget-object v3, p2, Lfuh;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcdn;->c:Landroid/widget/ImageView;

    iget-object v3, p0, Lcdn;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v3, p2, Lfuh;->b:Z

    iget-object v4, p0, Lcdn;->b:Landroid/widget/TextView;

    if-eqz v3, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcdn;->c:Landroid/widget/ImageView;

    if-eqz v3, :cond_3

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcdn;->d:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p2, Lfuh;->c:Landroid/view/View$OnClickListener;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcdn;->b:Landroid/widget/TextView;

    const v3, 0x7f0902a9

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_3
.end method
