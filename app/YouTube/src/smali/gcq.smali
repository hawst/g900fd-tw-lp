.class final Lgcq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lewi;


# instance fields
.field private synthetic a:Lefh;

.field private synthetic b:Z

.field private synthetic c:Lgco;


# direct methods
.method constructor <init>(Lgco;Lefh;Z)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lgcq;->c:Lgco;

    iput-object p2, p0, Lgcq;->a:Lefh;

    iput-boolean p3, p0, Lgcq;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Z)Lefc;
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/16 v4, 0x1bb

    const/16 v3, 0x50

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 258
    :goto_0
    iget-object v0, p0, Lgcq;->c:Lgco;

    invoke-static {v0}, Lgco;->a(Lgco;)Lewi;

    move-result-object v0

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lfqy;

    .line 260
    if-nez v8, :cond_1

    .line 261
    new-instance v0, Lefm;

    iget-object v1, p0, Lgcq;->c:Lgco;

    .line 262
    invoke-static {v1}, Lgco;->b(Lgco;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lefm;->a:Legt;

    iget-object v3, p0, Lgcq;->a:Lefh;

    const/16 v4, 0x1f40

    const/16 v5, 0x1f40

    invoke-direct/range {v0 .. v5}, Lefm;-><init>(Ljava/lang/String;Legt;Lega;II)V

    move-object v2, v0

    .line 329
    :goto_1
    iget-boolean v0, p0, Lgcq;->b:Z

    if-eqz v0, :cond_0

    .line 330
    sget-object v1, Lgcr;->a:[I

    iget-object v0, p0, Lgcq;->c:Lgco;

    invoke-static {v0}, Lgco;->h(Lgco;)Lggn;

    move-result-object v3

    const-string v4, "medialib_diagnostic_bandwidth_throttling_policy"

    const-class v5, Lggo;

    sget-object v6, Lggo;->a:Lggo;

    invoke-virtual {v3}, Lggn;->b()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v10

    :goto_2
    invoke-virtual {v3, v4, v5, v6, v0}, Lggn;->a(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;Z)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lggo;

    invoke-virtual {v0}, Lggo;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 340
    :cond_0
    :goto_3
    return-object v2

    .line 267
    :cond_1
    if-eqz p1, :cond_7

    iget-object v0, v8, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->f:Lgzv;

    if-eqz v0, :cond_5

    iget-object v0, v8, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->f:Lgzv;

    iget v0, v0, Lgzv;->a:I

    if-ne v0, v12, :cond_4

    move v0, v10

    :goto_4
    if-eqz v0, :cond_7

    .line 268
    invoke-virtual {v8}, Lfqy;->n()Z

    move-result v0

    iget-object v1, p0, Lgcq;->c:Lgco;

    invoke-static {v1}, Lgco;->c(Lgco;)Z

    move-result v1

    if-ne v0, v1, :cond_2

    invoke-virtual {v8}, Lfqy;->m()Z

    move-result v0

    iget-object v1, p0, Lgcq;->c:Lgco;

    invoke-static {v1}, Lgco;->d(Lgco;)Z

    move-result v1

    if-eq v0, v1, :cond_3

    .line 269
    :cond_2
    iget-object v0, p0, Lgcq;->c:Lgco;

    invoke-virtual {v8}, Lfqy;->n()Z

    move-result v1

    invoke-static {v0, v1}, Lgco;->a(Lgco;Z)Z

    .line 270
    iget-object v0, p0, Lgcq;->c:Lgco;

    invoke-virtual {v8}, Lfqy;->m()Z

    move-result v1

    invoke-static {v0, v1}, Lgco;->b(Lgco;Z)Z

    .line 271
    iget-object v0, p0, Lgcq;->c:Lgco;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgco;->a(Lgco;Lorg/chromium/net/HttpUrlRequestFactory;)Lorg/chromium/net/HttpUrlRequestFactory;

    .line 273
    :cond_3
    iget-object v0, p0, Lgcq;->c:Lgco;

    invoke-static {v0}, Lgco;->e(Lgco;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v0

    if-nez v0, :cond_6

    .line 274
    new-instance v0, Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    invoke-direct {v0}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;-><init>()V

    .line 279
    iget-object v1, p0, Lgcq;->c:Lgco;

    invoke-static {v1}, Lgco;->c(Lgco;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    move-result-object v1

    iget-object v2, p0, Lgcq;->c:Lgco;

    invoke-static {v2}, Lgco;->d(Lgco;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->b(Z)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    move-result-object v1

    const-string v2, "foo.googlevideo.com"

    .line 280
    invoke-virtual {v1, v2, v3, v3}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    move-result-object v1

    const-string v2, "foo.googlevideo.com"

    .line 281
    invoke-virtual {v1, v2, v4, v4}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    move-result-object v1

    const-string v2, "foo.c.youtube.com"

    .line 282
    invoke-virtual {v1, v2, v3, v3}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    move-result-object v1

    const-string v2, "foo.c.youtube.com"

    .line 283
    invoke-virtual {v1, v2, v4, v4}, Lorg/chromium/net/HttpUrlRequestFactoryConfig;->a(Ljava/lang/String;II)Lorg/chromium/net/HttpUrlRequestFactoryConfig;

    .line 285
    :try_start_0
    iget-object v1, p0, Lgcq;->c:Lgco;

    iget-object v2, p0, Lgcq;->c:Lgco;

    invoke-static {v2}, Lgco;->f(Lgco;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lorg/chromium/net/HttpUrlRequestFactory;->a(Landroid/content/Context;Lorg/chromium/net/HttpUrlRequestFactoryConfig;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v0

    invoke-static {v1, v0}, Lgco;->a(Lgco;Lorg/chromium/net/HttpUrlRequestFactory;)Lorg/chromium/net/HttpUrlRequestFactory;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :goto_5
    iget-object v0, p0, Lgcq;->c:Lgco;

    invoke-static {v0}, Lgco;->e(Lgco;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v0

    if-nez v0, :cond_6

    move p1, v9

    .line 291
    goto/16 :goto_0

    :cond_4
    move v0, v9

    .line 267
    goto/16 :goto_4

    :cond_5
    move v0, v9

    goto/16 :goto_4

    .line 287
    :catch_0
    move-exception v0

    iget-object v0, p0, Lgcq;->c:Lgco;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lgco;->a(Lgco;Lorg/chromium/net/HttpUrlRequestFactory;)Lorg/chromium/net/HttpUrlRequestFactory;

    goto :goto_5

    .line 294
    :cond_6
    new-instance v0, Lgfy;

    iget-object v1, p0, Lgcq;->c:Lgco;

    .line 295
    invoke-static {v1}, Lgco;->e(Lgco;)Lorg/chromium/net/HttpUrlRequestFactory;

    move-result-object v1

    sget-object v2, Lefm;->a:Legt;

    iget-object v3, p0, Lgcq;->a:Lefh;

    .line 298
    invoke-virtual {v8}, Lfqy;->h()I

    move-result v4

    .line 299
    invoke-virtual {v8}, Lfqy;->i()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lgfy;-><init>(Lorg/chromium/net/HttpUrlRequestFactory;Legt;Lega;II)V

    move-object v2, v0

    goto/16 :goto_1

    .line 300
    :cond_7
    invoke-static {}, Lfqy;->o()Z

    .line 301
    iget-object v0, v8, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    if-eqz v0, :cond_8

    iget-object v0, v8, Lfqy;->a:Lhre;

    iget-object v0, v0, Lhre;->b:Lhgg;

    iget-boolean v0, v0, Lhgg;->w:Z

    :goto_6
    if-eqz v0, :cond_b

    .line 312
    new-instance v0, Lgfv;

    iget-object v1, p0, Lgcq;->c:Lgco;

    .line 313
    invoke-static {v1}, Lgco;->g(Lgco;)Lezj;

    move-result-object v1

    iget-object v2, p0, Lgcq;->c:Lgco;

    .line 314
    invoke-static {v2}, Lgco;->b(Lgco;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lefm;->a:Legt;

    iget-object v4, p0, Lgcq;->a:Lefh;

    .line 317
    invoke-virtual {v8}, Lfqy;->h()I

    move-result v5

    .line 318
    invoke-virtual {v8}, Lfqy;->i()I

    move-result v6

    .line 319
    iget-object v7, v8, Lfqy;->a:Lhre;

    iget-object v7, v7, Lhre;->b:Lhgg;

    if-eqz v7, :cond_9

    iget-object v7, v8, Lfqy;->a:Lhre;

    iget-object v7, v7, Lhre;->b:Lhgg;

    iget v7, v7, Lhgg;->x:I

    .line 320
    :goto_7
    iget-object v11, v8, Lfqy;->a:Lhre;

    iget-object v11, v11, Lhre;->b:Lhgg;

    if-eqz v11, :cond_a

    iget-object v8, v8, Lfqy;->a:Lhre;

    iget-object v8, v8, Lhre;->b:Lhgg;

    iget-boolean v8, v8, Lhgg;->y:Z

    :goto_8
    invoke-direct/range {v0 .. v8}, Lgfv;-><init>(Lezj;Ljava/lang/String;Legt;Lega;IIIZ)V

    move-object v2, v0

    goto/16 :goto_1

    :cond_8
    move v0, v9

    .line 301
    goto :goto_6

    :cond_9
    move v7, v9

    .line 319
    goto :goto_7

    :cond_a
    move v8, v9

    .line 320
    goto :goto_8

    .line 322
    :cond_b
    new-instance v0, Lefm;

    iget-object v1, p0, Lgcq;->c:Lgco;

    .line 323
    invoke-static {v1}, Lgco;->b(Lgco;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lefm;->a:Legt;

    iget-object v3, p0, Lgcq;->a:Lefh;

    .line 326
    invoke-virtual {v8}, Lfqy;->h()I

    move-result v4

    .line 327
    invoke-virtual {v8}, Lfqy;->i()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lefm;-><init>(Ljava/lang/String;Legt;Lega;II)V

    move-object v2, v0

    goto/16 :goto_1

    :cond_c
    move v0, v9

    .line 330
    goto/16 :goto_2

    .line 332
    :pswitch_0
    iget-object v0, p0, Lgcq;->c:Lgco;

    invoke-static {v0}, Lgco;->h(Lgco;)Lggn;

    move-result-object v0

    invoke-virtual {v0}, Lggn;->c()[I

    move-result-object v6

    .line 333
    new-instance v0, Lgge;

    iget-object v1, p0, Lgcq;->c:Lgco;

    .line 334
    invoke-static {v1}, Lgco;->g(Lgco;)Lezj;

    move-result-object v1

    array-length v3, v6

    if-lez v3, :cond_d

    aget v3, v6, v9

    :goto_9
    array-length v4, v6

    if-le v4, v10, :cond_e

    aget v4, v6, v10

    :goto_a
    array-length v5, v6

    if-le v5, v12, :cond_f

    aget v5, v6, v12

    :goto_b
    array-length v7, v6

    const/4 v8, 0x3

    if-le v7, v8, :cond_10

    const/4 v7, 0x3

    aget v6, v6, v7

    :goto_c
    invoke-direct/range {v0 .. v6}, Lgge;-><init>(Lezj;Lefc;IIII)V

    move-object v2, v0

    goto/16 :goto_3

    :cond_d
    move v3, v9

    goto :goto_9

    :cond_e
    move v4, v9

    goto :goto_a

    :cond_f
    move v5, v9

    goto :goto_b

    :cond_10
    move v6, v9

    goto :goto_c

    .line 330
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final synthetic e_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgcq;->a(Z)Lefc;

    move-result-object v0

    return-object v0
.end method
