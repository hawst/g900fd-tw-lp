.class public final Lfbp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private synthetic a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 123
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-boolean v0, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->b:Z

    if-eqz v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-object v3, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    move v0, v2

    :goto_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->measure(II)V

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->getMeasuredHeight()I

    move-result v4

    iget-object v5, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-gt v3, v5, :cond_3

    iget-object v0, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-le v4, v0, :cond_5

    :cond_3
    move v0, v2

    :goto_2
    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iput-boolean v2, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->b:Z

    .line 128
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-object v2, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v2}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->d(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-object v2, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v2}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)I

    move-result v2

    iget-object v3, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-static {v3}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->e(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->setSelection(II)V

    .line 130
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iput-boolean v1, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->b:Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 126
    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-boolean v0, v0, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->b:Z

    if-nez v0, :cond_0

    .line 137
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->a(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;Ljava/lang/String;)Ljava/lang/String;

    .line 138
    iget-object v0, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    iget-object v1, p0, Lfbp;->a:Lcom/google/android/libraries/youtube/conversation/DraggableEditText;

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->getSelectionStart()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/youtube/conversation/DraggableEditText;->b(Lcom/google/android/libraries/youtube/conversation/DraggableEditText;I)I

    .line 140
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 144
    return-void
.end method
