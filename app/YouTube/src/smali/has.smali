.class public final Lhas;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lias;

.field public c:Liaz;

.field public d:[Liaw;

.field public e:Z

.field public f:Lhgz;

.field public g:[B

.field private h:Lhog;

.field private i:[Lhgz;

.field private j:[Lhbi;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12228
    invoke-direct {p0}, Lidf;-><init>()V

    .line 12231
    iput-object v1, p0, Lhas;->a:Lhgz;

    .line 12234
    iput-object v1, p0, Lhas;->h:Lhog;

    .line 12237
    sget-object v0, Lhgz;->a:[Lhgz;

    iput-object v0, p0, Lhas;->i:[Lhgz;

    .line 12240
    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhas;->j:[Lhbi;

    .line 12243
    iput-object v1, p0, Lhas;->b:Lias;

    .line 12246
    iput-object v1, p0, Lhas;->c:Liaz;

    .line 12249
    sget-object v0, Liaw;->a:[Liaw;

    iput-object v0, p0, Lhas;->d:[Liaw;

    .line 12252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhas;->e:Z

    .line 12255
    iput-object v1, p0, Lhas;->f:Lhgz;

    .line 12258
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhas;->g:[B

    .line 12228
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 12381
    .line 12382
    iget-object v0, p0, Lhas;->a:Lhgz;

    if-eqz v0, :cond_c

    .line 12383
    const/4 v0, 0x1

    iget-object v2, p0, Lhas;->a:Lhgz;

    .line 12384
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 12386
    :goto_0
    iget-object v2, p0, Lhas;->h:Lhog;

    if-eqz v2, :cond_0

    .line 12387
    const/4 v2, 0x2

    iget-object v3, p0, Lhas;->h:Lhog;

    .line 12388
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 12390
    :cond_0
    iget-object v2, p0, Lhas;->i:[Lhgz;

    if-eqz v2, :cond_2

    .line 12391
    iget-object v3, p0, Lhas;->i:[Lhgz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 12392
    if-eqz v5, :cond_1

    .line 12393
    const/4 v6, 0x3

    .line 12394
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 12391
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 12398
    :cond_2
    iget-object v2, p0, Lhas;->j:[Lhbi;

    if-eqz v2, :cond_4

    .line 12399
    iget-object v3, p0, Lhas;->j:[Lhbi;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 12400
    if-eqz v5, :cond_3

    .line 12401
    const/4 v6, 0x4

    .line 12402
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 12399
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 12406
    :cond_4
    iget-object v2, p0, Lhas;->b:Lias;

    if-eqz v2, :cond_5

    .line 12407
    const/4 v2, 0x5

    iget-object v3, p0, Lhas;->b:Lias;

    .line 12408
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 12410
    :cond_5
    iget-object v2, p0, Lhas;->c:Liaz;

    if-eqz v2, :cond_6

    .line 12411
    const/4 v2, 0x6

    iget-object v3, p0, Lhas;->c:Liaz;

    .line 12412
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 12414
    :cond_6
    iget-object v2, p0, Lhas;->d:[Liaw;

    if-eqz v2, :cond_8

    .line 12415
    iget-object v2, p0, Lhas;->d:[Liaw;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 12416
    if-eqz v4, :cond_7

    .line 12417
    const/4 v5, 0x7

    .line 12418
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 12415
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 12422
    :cond_8
    iget-boolean v1, p0, Lhas;->e:Z

    if-eqz v1, :cond_9

    .line 12423
    const/16 v1, 0x8

    iget-boolean v2, p0, Lhas;->e:Z

    .line 12424
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 12426
    :cond_9
    iget-object v1, p0, Lhas;->f:Lhgz;

    if-eqz v1, :cond_a

    .line 12427
    const/16 v1, 0x9

    iget-object v2, p0, Lhas;->f:Lhgz;

    .line 12428
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12430
    :cond_a
    iget-object v1, p0, Lhas;->g:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_b

    .line 12431
    const/16 v1, 0xb

    iget-object v2, p0, Lhas;->g:[B

    .line 12432
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 12434
    :cond_b
    iget-object v1, p0, Lhas;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12435
    iput v0, p0, Lhas;->J:I

    .line 12436
    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12224
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhas;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhas;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhas;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhas;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhas;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhas;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhas;->h:Lhog;

    if-nez v0, :cond_3

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhas;->h:Lhog;

    :cond_3
    iget-object v0, p0, Lhas;->h:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhas;->i:[Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgz;

    iget-object v3, p0, Lhas;->i:[Lhgz;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lhas;->i:[Lhgz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lhas;->i:[Lhgz;

    :goto_2
    iget-object v2, p0, Lhas;->i:[Lhgz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lhas;->i:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhas;->i:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhas;->i:[Lhgz;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lhas;->i:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhas;->i:[Lhgz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhas;->j:[Lhbi;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhas;->j:[Lhbi;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lhas;->j:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lhas;->j:[Lhbi;

    :goto_4
    iget-object v2, p0, Lhas;->j:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lhas;->j:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhas;->j:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lhas;->j:[Lhbi;

    array-length v0, v0

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lhas;->j:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhas;->j:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lhas;->b:Lias;

    if-nez v0, :cond_a

    new-instance v0, Lias;

    invoke-direct {v0}, Lias;-><init>()V

    iput-object v0, p0, Lhas;->b:Lias;

    :cond_a
    iget-object v0, p0, Lhas;->b:Lias;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhas;->c:Liaz;

    if-nez v0, :cond_b

    new-instance v0, Liaz;

    invoke-direct {v0}, Liaz;-><init>()V

    iput-object v0, p0, Lhas;->c:Liaz;

    :cond_b
    iget-object v0, p0, Lhas;->c:Liaz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhas;->d:[Liaw;

    if-nez v0, :cond_d

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Liaw;

    iget-object v3, p0, Lhas;->d:[Liaw;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lhas;->d:[Liaw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    iput-object v2, p0, Lhas;->d:[Liaw;

    :goto_6
    iget-object v2, p0, Lhas;->d:[Liaw;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Lhas;->d:[Liaw;

    new-instance v3, Liaw;

    invoke-direct {v3}, Liaw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhas;->d:[Liaw;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_d
    iget-object v0, p0, Lhas;->d:[Liaw;

    array-length v0, v0

    goto :goto_5

    :cond_e
    iget-object v2, p0, Lhas;->d:[Liaw;

    new-instance v3, Liaw;

    invoke-direct {v3}, Liaw;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhas;->d:[Liaw;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhas;->e:Z

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhas;->f:Lhgz;

    if-nez v0, :cond_f

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhas;->f:Lhgz;

    :cond_f
    iget-object v0, p0, Lhas;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhas;->g:[B

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 12333
    iget-object v1, p0, Lhas;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 12334
    const/4 v1, 0x1

    iget-object v2, p0, Lhas;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 12336
    :cond_0
    iget-object v1, p0, Lhas;->h:Lhog;

    if-eqz v1, :cond_1

    .line 12337
    const/4 v1, 0x2

    iget-object v2, p0, Lhas;->h:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 12339
    :cond_1
    iget-object v1, p0, Lhas;->i:[Lhgz;

    if-eqz v1, :cond_3

    .line 12340
    iget-object v2, p0, Lhas;->i:[Lhgz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 12341
    if-eqz v4, :cond_2

    .line 12342
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 12340
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 12346
    :cond_3
    iget-object v1, p0, Lhas;->j:[Lhbi;

    if-eqz v1, :cond_5

    .line 12347
    iget-object v2, p0, Lhas;->j:[Lhbi;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 12348
    if-eqz v4, :cond_4

    .line 12349
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 12347
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 12353
    :cond_5
    iget-object v1, p0, Lhas;->b:Lias;

    if-eqz v1, :cond_6

    .line 12354
    const/4 v1, 0x5

    iget-object v2, p0, Lhas;->b:Lias;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 12356
    :cond_6
    iget-object v1, p0, Lhas;->c:Liaz;

    if-eqz v1, :cond_7

    .line 12357
    const/4 v1, 0x6

    iget-object v2, p0, Lhas;->c:Liaz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 12359
    :cond_7
    iget-object v1, p0, Lhas;->d:[Liaw;

    if-eqz v1, :cond_9

    .line 12360
    iget-object v1, p0, Lhas;->d:[Liaw;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 12361
    if-eqz v3, :cond_8

    .line 12362
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 12360
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 12366
    :cond_9
    iget-boolean v0, p0, Lhas;->e:Z

    if-eqz v0, :cond_a

    .line 12367
    const/16 v0, 0x8

    iget-boolean v1, p0, Lhas;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 12369
    :cond_a
    iget-object v0, p0, Lhas;->f:Lhgz;

    if-eqz v0, :cond_b

    .line 12370
    const/16 v0, 0x9

    iget-object v1, p0, Lhas;->f:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 12372
    :cond_b
    iget-object v0, p0, Lhas;->g:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_c

    .line 12373
    const/16 v0, 0xb

    iget-object v1, p0, Lhas;->g:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 12375
    :cond_c
    iget-object v0, p0, Lhas;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 12377
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12278
    if-ne p1, p0, :cond_1

    .line 12291
    :cond_0
    :goto_0
    return v0

    .line 12279
    :cond_1
    instance-of v2, p1, Lhas;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 12280
    :cond_2
    check-cast p1, Lhas;

    .line 12281
    iget-object v2, p0, Lhas;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhas;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhas;->h:Lhog;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhas;->h:Lhog;

    if-nez v2, :cond_3

    .line 12282
    :goto_2
    iget-object v2, p0, Lhas;->i:[Lhgz;

    iget-object v3, p1, Lhas;->i:[Lhgz;

    .line 12283
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhas;->j:[Lhbi;

    iget-object v3, p1, Lhas;->j:[Lhbi;

    .line 12284
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhas;->b:Lias;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhas;->b:Lias;

    if-nez v2, :cond_3

    .line 12285
    :goto_3
    iget-object v2, p0, Lhas;->c:Liaz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhas;->c:Liaz;

    if-nez v2, :cond_3

    .line 12286
    :goto_4
    iget-object v2, p0, Lhas;->d:[Liaw;

    iget-object v3, p1, Lhas;->d:[Liaw;

    .line 12287
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhas;->e:Z

    iget-boolean v3, p1, Lhas;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhas;->f:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhas;->f:Lhgz;

    if-nez v2, :cond_3

    .line 12289
    :goto_5
    iget-object v2, p0, Lhas;->g:[B

    iget-object v3, p1, Lhas;->g:[B

    .line 12290
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhas;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhas;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 12291
    goto :goto_0

    .line 12281
    :cond_4
    iget-object v2, p0, Lhas;->a:Lhgz;

    iget-object v3, p1, Lhas;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhas;->h:Lhog;

    iget-object v3, p1, Lhas;->h:Lhog;

    .line 12282
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    .line 12284
    :cond_6
    iget-object v2, p0, Lhas;->b:Lias;

    iget-object v3, p1, Lhas;->b:Lias;

    .line 12285
    invoke-virtual {v2, v3}, Lias;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhas;->c:Liaz;

    iget-object v3, p1, Lhas;->c:Liaz;

    .line 12286
    invoke-virtual {v2, v3}, Liaz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    .line 12287
    :cond_8
    iget-object v2, p0, Lhas;->f:Lhgz;

    iget-object v3, p1, Lhas;->f:Lhgz;

    .line 12289
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    .line 12290
    :cond_9
    iget-object v2, p0, Lhas;->I:Ljava/util/List;

    iget-object v3, p1, Lhas;->I:Ljava/util/List;

    .line 12291
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 12295
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 12297
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhas;->a:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 12298
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhas;->h:Lhog;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 12299
    iget-object v2, p0, Lhas;->i:[Lhgz;

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    .line 12305
    :cond_0
    iget-object v0, p0, Lhas;->j:[Lhbi;

    if-nez v0, :cond_8

    mul-int/lit8 v2, v2, 0x1f

    .line 12311
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhas;->b:Lias;

    if-nez v0, :cond_a

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 12312
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhas;->c:Liaz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 12313
    iget-object v2, p0, Lhas;->d:[Liaw;

    if-nez v2, :cond_c

    mul-int/lit8 v2, v0, 0x1f

    .line 12319
    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-boolean v0, p0, Lhas;->e:Z

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    :goto_4
    add-int/2addr v0, v2

    .line 12320
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhas;->f:Lhgz;

    if-nez v0, :cond_f

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 12321
    iget-object v2, p0, Lhas;->g:[B

    if-nez v2, :cond_10

    mul-int/lit8 v2, v0, 0x1f

    .line 12327
    :cond_3
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhas;->I:Ljava/util/List;

    if-nez v2, :cond_11

    :goto_6
    add-int/2addr v0, v1

    .line 12328
    return v0

    .line 12297
    :cond_4
    iget-object v0, p0, Lhas;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 12298
    :cond_5
    iget-object v0, p0, Lhas;->h:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_6
    move v2, v0

    move v0, v1

    .line 12301
    :goto_7
    iget-object v3, p0, Lhas;->i:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 12302
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhas;->i:[Lhgz;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_8
    add-int/2addr v2, v3

    .line 12301
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 12302
    :cond_7
    iget-object v2, p0, Lhas;->i:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgz;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    move v0, v1

    .line 12307
    :goto_9
    iget-object v3, p0, Lhas;->j:[Lhbi;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 12308
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhas;->j:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_a
    add-int/2addr v2, v3

    .line 12307
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 12308
    :cond_9
    iget-object v2, p0, Lhas;->j:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_a

    .line 12311
    :cond_a
    iget-object v0, p0, Lhas;->b:Lias;

    invoke-virtual {v0}, Lias;->hashCode()I

    move-result v0

    goto :goto_2

    .line 12312
    :cond_b
    iget-object v0, p0, Lhas;->c:Liaz;

    invoke-virtual {v0}, Liaz;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_c
    move v2, v0

    move v0, v1

    .line 12315
    :goto_b
    iget-object v3, p0, Lhas;->d:[Liaw;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 12316
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhas;->d:[Liaw;

    aget-object v2, v2, v0

    if-nez v2, :cond_d

    move v2, v1

    :goto_c
    add-int/2addr v2, v3

    .line 12315
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 12316
    :cond_d
    iget-object v2, p0, Lhas;->d:[Liaw;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Liaw;->hashCode()I

    move-result v2

    goto :goto_c

    .line 12319
    :cond_e
    const/4 v0, 0x2

    goto/16 :goto_4

    .line 12320
    :cond_f
    iget-object v0, p0, Lhas;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_10
    move v2, v0

    move v0, v1

    .line 12323
    :goto_d
    iget-object v3, p0, Lhas;->g:[B

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 12324
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhas;->g:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 12323
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    .line 12327
    :cond_11
    iget-object v1, p0, Lhas;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_6
.end method
