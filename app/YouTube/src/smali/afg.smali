.class public final Lafg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbt;


# instance fields
.field a:Z

.field private final b:Ldbt;

.field private final c:Laet;


# direct methods
.method public constructor <init>(Ldbt;Laet;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbt;

    iput-object v0, p0, Lafg;->b:Ldbt;

    .line 25
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laet;

    iput-object v0, p0, Lafg;->c:Laet;

    .line 26
    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lafg;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Lafg;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lafg;->c:Laet;

    invoke-virtual {v0}, Laet;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 34
    :cond_0
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->n()V

    .line 36
    :cond_1
    return-void
.end method

.method private static p()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 41
    const-class v1, Landroid/view/View;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 42
    const-class v1, Laet;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    .line 43
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 44
    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v6, v4, v1

    .line 47
    :try_start_0
    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 53
    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 54
    invoke-virtual {v6}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 58
    :goto_1
    return v0

    .line 44
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 50
    :catch_0
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lafg;->o()V

    .line 64
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->a()V

    .line 65
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lafg;->o()V

    .line 82
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0, p1}, Ldbt;->a(I)V

    .line 83
    return-void
.end method

.method public final a(Lgpa;)V
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0}, Lafg;->o()V

    .line 154
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0, p1}, Ldbt;->a(Lgpa;)V

    .line 155
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0}, Lafg;->o()V

    .line 136
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0, p1}, Ldbt;->a(Z)V

    .line 137
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lafg;->o()V

    .line 70
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->b()V

    .line 71
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lafg;->o()V

    .line 100
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0, p1}, Ldbt;->b(I)V

    .line 101
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lafg;->o()V

    .line 76
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->c()V

    .line 77
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lafg;->o()V

    .line 88
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->d()V

    .line 89
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lafg;->o()V

    .line 94
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->e()V

    .line 95
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 105
    invoke-direct {p0}, Lafg;->o()V

    .line 106
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->f()V

    .line 107
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0}, Lafg;->o()V

    .line 112
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->g()V

    .line 113
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Lafg;->o()V

    .line 118
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->h()V

    .line 119
    return-void
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0}, Lafg;->o()V

    .line 124
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->i()V

    .line 125
    return-void
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 129
    invoke-direct {p0}, Lafg;->o()V

    .line 130
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->j()V

    .line 131
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lafg;->o()V

    .line 142
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->k()V

    .line 143
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Lafg;->o()V

    .line 148
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->l()V

    .line 149
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 159
    invoke-direct {p0}, Lafg;->o()V

    .line 160
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->m()V

    .line 161
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lafg;->b:Ldbt;

    invoke-interface {v0}, Ldbt;->n()V

    .line 166
    return-void
.end method
