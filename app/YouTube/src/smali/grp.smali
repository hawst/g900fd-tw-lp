.class public final Lgrp;
.super Lgrn;
.source "SourceFile"


# instance fields
.field final d:J

.field final e:J

.field private f:J


# direct methods
.method public constructor <init>(JJLgru;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 31
    const/16 v0, 0xa

    invoke-direct {p0, p5, p6, v0}, Lgrn;-><init>(Lgru;Ljava/lang/String;I)V

    .line 16
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgrp;->f:J

    .line 32
    iput-wide p1, p0, Lgrp;->d:J

    .line 33
    iput-wide p3, p0, Lgrp;->e:J

    .line 34
    return-void
.end method


# virtual methods
.method public final a(JZ)Lgrb;
    .locals 5

    .prologue
    .line 80
    iput-wide p1, p0, Lgrp;->f:J

    .line 82
    sget-object v0, Lgrb;->a:Lgrb;

    .line 83
    if-nez p3, :cond_0

    iget-wide v2, p0, Lgrp;->e:J

    cmp-long v1, p1, v2

    if-ltz v1, :cond_1

    .line 84
    :cond_0
    sget-object v0, Lgrb;->b:Lgrb;

    .line 87
    :cond_1
    return-object v0
.end method

.method public final a(J)Z
    .locals 3

    .prologue
    .line 75
    iget-wide v0, p0, Lgrp;->d:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lgrp;->e:J

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JJ)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 56
    iget-wide v2, p0, Lgrp;->d:J

    cmp-long v1, p3, v2

    if-ltz v1, :cond_0

    iget-wide v2, p0, Lgrp;->e:J

    cmp-long v1, p1, v2

    if-lez v1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    iget-wide v2, p0, Lgrp;->e:J

    invoke-static {p3, p4, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iget-wide v4, p0, Lgrp;->f:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lgrb;
    .locals 3

    .prologue
    .line 66
    invoke-super {p0}, Lgrn;->b()Lgrb;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 68
    iget-wide v0, p0, Lgrp;->d:J

    sget-object v2, Lgrf;->a:Lgrf;

    invoke-static {v0, v1, v2}, Lgrb;->a(JLgrf;)Lgrb;

    move-result-object v0

    .line 70
    :cond_0
    return-object v0
.end method
