.class public Leyk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Leym;

.field private final c:Leyo;

.field private final d:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Leym;Leyo;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    .line 243
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leym;

    iput-object v0, p0, Leyk;->b:Leym;

    .line 244
    iput-object p3, p0, Leyk;->c:Leyo;

    .line 246
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 247
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 248
    invoke-virtual {p2}, Leym;->a()Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Leyk;->d:Landroid/view/animation/Animation;

    .line 249
    invoke-virtual {p2}, Leym;->a()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250
    iget-object v0, p0, Leyk;->d:Landroid/view/animation/Animation;

    new-instance v1, Leyl;

    invoke-direct {v1, p0}, Leyl;-><init>(Leyk;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 255
    :cond_0
    const v0, 0x7f080014

    invoke-virtual {p1, v0, p0}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 256
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    const v1, 0x7f080014

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 299
    iget-object v0, p0, Leyk;->c:Leyo;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Leyk;->c:Leyo;

    iget-object v1, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Leyo;->b(Landroid/widget/ImageView;)V

    .line 302
    :cond_1
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-static {v0}, Leyi;->b(Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const v2, 0x7f080015

    .line 260
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    const v1, 0x7f080014

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 279
    :goto_0
    return-void

    .line 265
    :cond_0
    iget-object v0, p0, Leyk;->b:Leym;

    iget-object v1, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, p2}, Leym;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 266
    iget-object v0, p0, Leyk;->c:Leyo;

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Leyk;->c:Leyo;

    iget-object v1, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-interface {v0, v1, p2}, Leyo;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 269
    :cond_1
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-static {v0, p2}, Leyi;->a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 272
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Leyk;->d:Landroid/view/animation/Animation;

    if-nez v0, :cond_3

    .line 273
    :cond_2
    invoke-virtual {p0}, Leyk;->a()V

    goto :goto_0

    .line 275
    :cond_3
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v2, p1}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 276
    iget-object v0, p0, Leyk;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    .line 277
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Leyk;->d:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    const v1, 0x7f080014

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_1

    iget-object v0, p0, Leyk;->c:Leyo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Leyk;->c:Leyo;

    iget-object v1, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Leyo;->c(Landroid/widget/ImageView;)V

    :cond_0
    iget-object v0, p0, Leyk;->a:Landroid/widget/ImageView;

    invoke-static {v0}, Leyi;->a(Landroid/widget/ImageView;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 234
    check-cast p1, Landroid/net/Uri;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Leyk;->a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    return-void
.end method
