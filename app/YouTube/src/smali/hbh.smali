.class public final Lhbh;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhbf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15156
    invoke-direct {p0}, Lidf;-><init>()V

    .line 15159
    const/4 v0, 0x0

    iput-object v0, p0, Lhbh;->a:Lhbf;

    .line 15156
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 15196
    const/4 v0, 0x0

    .line 15197
    iget-object v1, p0, Lhbh;->a:Lhbf;

    if-eqz v1, :cond_0

    .line 15198
    const v0, 0x3da974e

    iget-object v1, p0, Lhbh;->a:Lhbf;

    .line 15199
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 15201
    :cond_0
    iget-object v1, p0, Lhbh;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15202
    iput v0, p0, Lhbh;->J:I

    .line 15203
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 15152
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhbh;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhbh;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhbh;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhbh;->a:Lhbf;

    if-nez v0, :cond_2

    new-instance v0, Lhbf;

    invoke-direct {v0}, Lhbf;-><init>()V

    iput-object v0, p0, Lhbh;->a:Lhbf;

    :cond_2
    iget-object v0, p0, Lhbh;->a:Lhbf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1ed4ba72 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 15187
    iget-object v0, p0, Lhbh;->a:Lhbf;

    if-eqz v0, :cond_0

    .line 15188
    const v0, 0x3da974e

    iget-object v1, p0, Lhbh;->a:Lhbf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 15190
    :cond_0
    iget-object v0, p0, Lhbh;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 15192
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 15170
    if-ne p1, p0, :cond_1

    .line 15174
    :cond_0
    :goto_0
    return v0

    .line 15171
    :cond_1
    instance-of v2, p1, Lhbh;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 15172
    :cond_2
    check-cast p1, Lhbh;

    .line 15173
    iget-object v2, p0, Lhbh;->a:Lhbf;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhbh;->a:Lhbf;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhbh;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhbh;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 15174
    goto :goto_0

    .line 15173
    :cond_4
    iget-object v2, p0, Lhbh;->a:Lhbf;

    iget-object v3, p1, Lhbh;->a:Lhbf;

    invoke-virtual {v2, v3}, Lhbf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhbh;->I:Ljava/util/List;

    iget-object v3, p1, Lhbh;->I:Ljava/util/List;

    .line 15174
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 15178
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 15180
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhbh;->a:Lhbf;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 15181
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhbh;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 15182
    return v0

    .line 15180
    :cond_0
    iget-object v0, p0, Lhbh;->a:Lhbf;

    invoke-virtual {v0}, Lhbf;->hashCode()I

    move-result v0

    goto :goto_0

    .line 15181
    :cond_1
    iget-object v1, p0, Lhbh;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
