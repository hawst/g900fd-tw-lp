.class public final Lcda;
.super Lcby;
.source "SourceFile"


# instance fields
.field private final f:Landroid/widget/LinearLayout;

.field private final g:Landroid/widget/RelativeLayout;

.field private final h:Landroid/content/res/Resources;

.field private final i:Lboi;

.field private final j:Lfsj;

.field private final m:Lerv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfhz;Lboi;Lerv;Lfdw;Lfrz;)V
    .locals 8

    .prologue
    .line 49
    const v5, 0x7f040040

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcby;-><init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;ILfdw;Lfrz;)V

    .line 57
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcda;->h:Landroid/content/res/Resources;

    .line 58
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcda;->j:Lfsj;

    .line 59
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcda;->i:Lboi;

    .line 60
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lerv;

    iput-object v0, p0, Lcda;->m:Lerv;

    .line 62
    iget-object v0, p0, Lcby;->c:Landroid/view/View;

    const v1, 0x7f080138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcda;->f:Landroid/widget/LinearLayout;

    .line 63
    iget-object v0, p0, Lcda;->f:Landroid/widget/LinearLayout;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcda;->g:Landroid/widget/RelativeLayout;

    .line 64
    return-void
.end method

.method private a(Lfsg;Lfjd;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 68
    invoke-super {p0, p1, p2}, Lcby;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 69
    iget-boolean v0, p2, Lfjd;->h:Z

    if-nez v0, :cond_1

    .line 70
    iput-boolean v4, p2, Lfjd;->h:Z

    .line 71
    iget-object v0, p0, Lcda;->m:Lerv;

    iget-object v1, p2, Lfjd;->a:Lhdx;

    iget-object v1, v1, Lhdx;->a:Ljava/lang/String;

    iget-object v2, p2, Lfjd;->g:Ljava/util/List;

    if-nez v2, :cond_0

    iget-object v2, p2, Lfjd;->a:Lhdx;

    iget-object v2, v2, Lhdx;->j:[Ljava/lang/String;

    invoke-static {v2}, La;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p2, Lfjd;->g:Ljava/util/List;

    :cond_0
    iget-object v2, p2, Lfjd;->g:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lerv;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 74
    :cond_1
    iget-object v0, p0, Lcda;->g:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcda;->h:Landroid/content/res/Resources;

    const v2, 0x7f0b0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v1, p0, Lcby;->a:Landroid/content/Context;

    invoke-static {v1, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcda;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 75
    :goto_0
    invoke-virtual {p2}, Lfjd;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcda;->a(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p2, Lfjd;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    iget-object v0, p2, Lfjd;->a:Lhdx;

    iget-object v0, v0, Lhdx;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfjd;->e:Ljava/lang/CharSequence;

    :cond_2
    iget-object v0, p2, Lfjd;->e:Ljava/lang/CharSequence;

    iget-object v1, p2, Lfjd;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_4

    iget-object v1, p2, Lfjd;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p2, Lfjd;->a:Lhdx;

    iget-object v1, v1, Lhdx;->f:Lhgz;

    if-eqz v1, :cond_8

    iget-object v1, p2, Lfjd;->a:Lhdx;

    iget-object v1, v1, Lhdx;->f:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjd;->b:Ljava/lang/CharSequence;

    :cond_3
    :goto_1
    iget-object v1, p2, Lfjd;->b:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iput-object v1, p2, Lfjd;->c:Ljava/lang/CharSequence;

    :cond_4
    iget-object v1, p2, Lfjd;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcda;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p2, Lfjd;->f:Lfnc;

    if-nez v0, :cond_5

    new-instance v0, Lfnc;

    iget-object v1, p2, Lfjd;->a:Lhdx;

    iget-object v1, v1, Lhdx;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p2, Lfjd;->f:Lfnc;

    :cond_5
    iget-object v0, p2, Lfjd;->f:Lfnc;

    iget-object v1, p2, Lfjd;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_6

    iget-object v1, p2, Lfjd;->a:Lhdx;

    iget-object v1, v1, Lhdx;->g:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjd;->d:Ljava/lang/CharSequence;

    :cond_6
    iget-object v1, p2, Lfjd;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcda;->a(Lfnc;Ljava/lang/CharSequence;)V

    .line 79
    iget-object v0, p0, Lcda;->j:Lfsj;

    .line 80
    invoke-interface {v0}, Lfsj;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcda;->i:Lboi;

    .line 82
    iget-object v2, p0, Lcby;->e:Landroid/view/View;

    .line 79
    invoke-static {v0, v1, v2, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcda;->j:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 74
    :cond_7
    iget-object v1, p0, Lcda;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v1, p0, Lcda;->h:Landroid/content/res/Resources;

    const v2, 0x7f0a008e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_0

    .line 76
    :cond_8
    iget-object v1, p2, Lfjd;->a:Lhdx;

    iget-object v1, v1, Lhdx;->e:Lhgz;

    if-eqz v1, :cond_3

    iget-object v1, p2, Lfjd;->a:Lhdx;

    iget-object v1, v1, Lhdx;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjd;->b:Ljava/lang/CharSequence;

    goto :goto_1
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    check-cast p2, Lfjd;

    invoke-direct {p0, p1, p2}, Lcda;->a(Lfsg;Lfjd;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    check-cast p2, Lfjd;

    invoke-direct {p0, p1, p2}, Lcda;->a(Lfsg;Lfjd;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    check-cast p2, Lfjd;

    invoke-direct {p0, p1, p2}, Lcda;->a(Lfsg;Lfjd;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
