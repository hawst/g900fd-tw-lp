.class final Ladk;
.super Ljavax/crypto/CipherSpi;
.source "SourceFile"


# instance fields
.field private a:[B

.field private b:I

.field private c:I

.field private d:[B


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljavax/crypto/CipherSpi;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Ladk;->d:[B

    invoke-direct {p0, v0}, Ladk;->a([B)V

    .line 161
    return-void
.end method

.method private a([B)V
    .locals 7

    .prologue
    const/16 v6, 0x100

    const/4 v0, 0x0

    .line 164
    iput-object p1, p0, Ladk;->d:[B

    .line 166
    iput v0, p0, Ladk;->b:I

    .line 167
    iput v0, p0, Ladk;->c:I

    .line 169
    iget-object v1, p0, Ladk;->a:[B

    if-nez v1, :cond_0

    .line 170
    new-array v1, v6, [B

    iput-object v1, p0, Ladk;->a:[B

    :cond_0
    move v1, v0

    .line 174
    :goto_0
    if-ge v1, v6, :cond_1

    .line 175
    iget-object v2, p0, Ladk;->a:[B

    int-to-byte v3, v1

    aput-byte v3, v2, v1

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v0

    move v2, v0

    .line 181
    :goto_1
    if-ge v0, v6, :cond_2

    .line 182
    aget-byte v3, p1, v2

    and-int/lit16 v3, v3, 0xff

    iget-object v4, p0, Ladk;->a:[B

    aget-byte v4, v4, v0

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    and-int/lit16 v1, v1, 0xff

    .line 184
    iget-object v3, p0, Ladk;->a:[B

    aget-byte v3, v3, v0

    .line 185
    iget-object v4, p0, Ladk;->a:[B

    iget-object v5, p0, Ladk;->a:[B

    aget-byte v5, v5, v1

    aput-byte v5, v4, v0

    .line 186
    iget-object v4, p0, Ladk;->a:[B

    aput-byte v3, v4, v1

    .line 187
    add-int/lit8 v2, v2, 0x1

    array-length v3, p1

    rem-int/2addr v2, v3

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 189
    :cond_2
    return-void
.end method

.method private a([BII[BI)V
    .locals 7

    .prologue
    .line 136
    add-int v0, p2, p3

    array-length v1, p1

    if-le v0, v1, :cond_0

    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Input buffer too short"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    add-int v0, p5, p3

    array-length v1, p4

    if-le v0, v1, :cond_1

    .line 141
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Output buffer too short"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_2

    .line 145
    iget v1, p0, Ladk;->b:I

    add-int/lit8 v1, v1, 0x1

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Ladk;->b:I

    .line 146
    iget-object v1, p0, Ladk;->a:[B

    iget v2, p0, Ladk;->b:I

    aget-byte v1, v1, v2

    iget v2, p0, Ladk;->c:I

    add-int/2addr v1, v2

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Ladk;->c:I

    .line 149
    iget-object v1, p0, Ladk;->a:[B

    iget v2, p0, Ladk;->b:I

    aget-byte v1, v1, v2

    .line 150
    iget-object v2, p0, Ladk;->a:[B

    iget v3, p0, Ladk;->b:I

    iget-object v4, p0, Ladk;->a:[B

    iget v5, p0, Ladk;->c:I

    aget-byte v4, v4, v5

    aput-byte v4, v2, v3

    .line 151
    iget-object v2, p0, Ladk;->a:[B

    iget v3, p0, Ladk;->c:I

    aput-byte v1, v2, v3

    .line 154
    add-int v1, v0, p5

    add-int v2, v0, p2

    aget-byte v2, p1, v2

    iget-object v3, p0, Ladk;->a:[B

    iget-object v4, p0, Ladk;->a:[B

    iget v5, p0, Ladk;->b:I

    aget-byte v4, v4, v5

    iget-object v5, p0, Ladk;->a:[B

    iget v6, p0, Ladk;->c:I

    aget-byte v5, v5, v6

    add-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    aget-byte v3, v3, v4

    xor-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p4, v1

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    :cond_2
    return-void
.end method


# virtual methods
.method protected final engineDoFinal([BII[BI)I
    .locals 0

    .prologue
    .line 128
    if-eqz p3, :cond_0

    .line 129
    invoke-direct/range {p0 .. p5}, Ladk;->a([BII[BI)V

    .line 131
    :cond_0
    invoke-direct {p0}, Ladk;->a()V

    .line 132
    return p3
.end method

.method protected final engineDoFinal([BII)[B
    .locals 1

    .prologue
    .line 115
    if-eqz p3, :cond_0

    .line 116
    invoke-virtual {p0, p1, p2, p3}, Ladk;->engineUpdate([BII)[B

    move-result-object v0

    .line 117
    invoke-direct {p0}, Ladk;->a()V

    .line 121
    :goto_0
    return-object v0

    .line 120
    :cond_0
    invoke-direct {p0}, Ladk;->a()V

    .line 121
    const/4 v0, 0x0

    new-array v0, v0, [B

    goto :goto_0
.end method

.method protected final engineGetBlockSize()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method protected final engineGetIV()[B
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final engineGetKeySize(Ljava/security/Key;)I
    .locals 1

    .prologue
    .line 50
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x3

    return v0
.end method

.method protected final engineGetOutputSize(I)I
    .locals 0

    .prologue
    .line 55
    return p1
.end method

.method protected final engineGetParameters()Ljava/security/AlgorithmParameters;
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final engineInit(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p4}, Ladk;->engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    .line 92
    return-void
.end method

.method protected final engineInit(ILjava/security/Key;Ljava/security/SecureRandom;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Ladk;->engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    .line 97
    return-void
.end method

.method protected final engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 2

    .prologue
    .line 81
    invoke-interface {p2}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RC4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "Must be provided with an RC4 key."

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    iput-object v0, p0, Ladk;->d:[B

    .line 85
    iget-object v0, p0, Ladk;->d:[B

    invoke-direct {p0, v0}, Ladk;->a([B)V

    .line 86
    return-void
.end method

.method protected final engineSetMode(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 66
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "can\'t support mode "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final engineSetPadding(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 73
    const-string v0, "NoPadding"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljavax/crypto/NoSuchPaddingException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Padding "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " unknown."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/crypto/NoSuchPaddingException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    return-void
.end method

.method protected final engineUpdate([BII[BI)I
    .locals 0

    .prologue
    .line 109
    invoke-direct/range {p0 .. p5}, Ladk;->a([BII[BI)V

    .line 110
    return p3
.end method

.method protected final engineUpdate([BII)[B
    .locals 6

    .prologue
    .line 101
    new-array v4, p3, [B

    .line 102
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Ladk;->a([BII[BI)V

    .line 103
    return-object v4
.end method
