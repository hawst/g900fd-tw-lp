.class public final enum Lgoj;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgoj;

.field public static final enum b:Lgoj;

.field public static final enum c:Lgoj;

.field private static final synthetic d:[Lgoj;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    new-instance v0, Lgoj;

    const-string v1, "SINGLE_VIDEO"

    invoke-direct {v0, v1, v2}, Lgoj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgoj;->a:Lgoj;

    .line 47
    new-instance v0, Lgoj;

    const-string v1, "PLAYLIST"

    invoke-direct {v0, v1, v3}, Lgoj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgoj;->b:Lgoj;

    .line 48
    new-instance v0, Lgoj;

    const-string v1, "VIDEO_LIST"

    invoke-direct {v0, v1, v4}, Lgoj;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgoj;->c:Lgoj;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [Lgoj;

    sget-object v1, Lgoj;->a:Lgoj;

    aput-object v1, v0, v2

    sget-object v1, Lgoj;->b:Lgoj;

    aput-object v1, v0, v3

    sget-object v1, Lgoj;->c:Lgoj;

    aput-object v1, v0, v4

    sput-object v0, Lgoj;->d:[Lgoj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgoj;
    .locals 1

    .prologue
    .line 45
    const-class v0, Lgoj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgoj;

    return-object v0
.end method

.method public static values()[Lgoj;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lgoj;->d:[Lgoj;

    invoke-virtual {v0}, [Lgoj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgoj;

    return-object v0
.end method
