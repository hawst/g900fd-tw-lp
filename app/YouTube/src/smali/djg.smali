.class public final Ldjg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefc;


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Lefc;

.field private c:Lefc;

.field private d:Ljava/lang/String;

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Ljava/util/List;Lefc;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Ldjg;->a:Ljava/util/List;

    .line 40
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefc;

    iput-object v0, p0, Ldjg;->b:Lefc;

    .line 41
    return-void
.end method

.method private b()V
    .locals 9

    .prologue
    .line 83
    :try_start_0
    iget-object v0, p0, Ldjg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    new-instance v0, Ljava/io/FileNotFoundException;

    const-string v1, "Chunk not found: no caches."

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :catch_0
    move-exception v0

    .line 109
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 94
    :cond_0
    const/4 v0, 0x0

    .line 95
    :try_start_1
    iget-object v1, p0, Ldjg;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legc;

    .line 96
    iget-object v2, p0, Ldjg;->d:Ljava/lang/String;

    iget-wide v4, p0, Ldjg;->e:J

    invoke-interface {v0, v2, v4, v5}, Legc;->a(Ljava/lang/String;J)Legi;

    move-result-object v2

    .line 97
    iget-boolean v3, v2, Legi;->d:Z

    if-eqz v3, :cond_1

    .line 98
    iget-object v0, v2, Legi;->e:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iget-wide v4, p0, Ldjg;->e:J

    iget-wide v6, v2, Legi;->b:J

    sub-long v7, v4, v6

    iget-wide v2, v2, Legi;->c:J

    sub-long/2addr v2, v7

    iget-wide v4, p0, Ldjg;->f:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    new-instance v0, Lefg;

    iget-wide v2, p0, Ldjg;->e:J

    iget-object v6, p0, Ldjg;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lefg;-><init>(Landroid/net/Uri;JJLjava/lang/String;J)V

    iget-object v1, p0, Ldjg;->b:Lefc;

    iput-object v1, p0, Ldjg;->c:Lefc;

    iget-object v1, p0, Ldjg;->c:Lefc;

    invoke-interface {v1, v0}, Lefc;->a(Lefg;)J

    .line 99
    return-void

    .line 102
    :cond_1
    invoke-interface {v0, v2}, Legc;->a(Legi;)V

    move-object v0, v2

    .line 103
    goto :goto_0

    .line 105
    :cond_2
    new-instance v1, Ljava/io/FileNotFoundException;

    iget-wide v2, v0, Legi;->b:J

    iget-wide v4, v0, Legi;->b:J

    iget-wide v6, v0, Legi;->c:J

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v8, 0x50

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Chunk not found: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Ldjg;->c:Lefc;

    if-nez v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Ldjg;->c:Lefc;

    invoke-interface {v0}, Lefc;->a()V

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Ldjg;->c:Lefc;

    goto :goto_0
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    .line 58
    :goto_0
    iget-object v0, p0, Ldjg;->c:Lefc;

    invoke-interface {v0, p1, p2, p3}, Lefc;->a([BII)I

    move-result v0

    .line 59
    if-ltz v0, :cond_1

    .line 60
    iget-wide v2, p0, Ldjg;->e:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Ldjg;->e:J

    .line 61
    iget-wide v2, p0, Ldjg;->f:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Ldjg;->f:J

    .line 69
    :cond_0
    return v0

    .line 63
    :cond_1
    invoke-direct {p0}, Ldjg;->c()V

    .line 64
    iget-wide v2, p0, Ldjg;->f:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 65
    invoke-direct {p0}, Ldjg;->b()V

    goto :goto_0
.end method

.method public final a(Lefg;)J
    .locals 4

    .prologue
    .line 45
    iget-boolean v0, p1, Lefg;->b:Z

    invoke-static {v0}, Lb;->c(Z)V

    .line 48
    iget-wide v0, p1, Lefg;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 49
    iget-object v0, p1, Lefg;->f:Ljava/lang/String;

    iput-object v0, p0, Ldjg;->d:Ljava/lang/String;

    .line 50
    iget-wide v0, p1, Lefg;->d:J

    iput-wide v0, p0, Ldjg;->e:J

    .line 51
    iget-wide v0, p1, Lefg;->e:J

    iput-wide v0, p0, Ldjg;->f:J

    .line 52
    invoke-direct {p0}, Ldjg;->b()V

    .line 53
    iget-wide v0, p1, Lefg;->e:J

    return-wide v0

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ldjg;->c()V

    .line 75
    return-void
.end method
