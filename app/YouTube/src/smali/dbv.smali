.class public final Ldbv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcws;

.field final b:Levn;

.field final c:Ldbr;

.field d:Ldby;

.field e:Ldbx;

.field f:Z

.field g:[Lfre;

.field private final h:Landroid/content/res/Resources;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/util/Map;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcws;Levn;Ldbr;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Ldbv;->a:Lcws;

    .line 81
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldbv;->b:Levn;

    .line 82
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbr;

    iput-object v0, p0, Ldbv;->c:Ldbr;

    .line 83
    iput-object p1, p0, Ldbv;->h:Landroid/content/res/Resources;

    .line 85
    new-instance v0, Ldbw;

    invoke-direct {v0, p0}, Ldbw;-><init>(Ldbv;)V

    invoke-interface {p4, v0}, Ldbr;->a(Ldbt;)V

    .line 86
    return-void
.end method

.method private handleTimelineMarkerChangeEvent(Ldft;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Ldbv;->l:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 291
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldbv;->l:Ljava/util/Map;

    .line 293
    :cond_0
    iget-object v0, p0, Ldbv;->l:Ljava/util/Map;

    iget-object v1, p1, Ldft;->a:Ldfs;

    iget-object v2, p1, Ldft;->b:[Ldfq;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 294
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-object v1, p0, Ldbv;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ldbr;->a(Ljava/util/Map;)V

    .line 295
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Ldbv;->c:Ldbr;

    invoke-interface {v0, p1}, Ldbr;->j(Z)V

    .line 98
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Ldbv;->c:Ldbr;

    invoke-interface {v0, p1}, Ldbr;->k(Z)V

    .line 102
    return-void
.end method

.method public final handleAdClickThroughChangedEvent(Ldaf;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 285
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-boolean v1, p1, Ldaf;->a:Z

    invoke-interface {v0, v1}, Ldbr;->i(Z)V

    .line 286
    return-void
.end method

.method public final handleAudioOnlyEvent(Lcyw;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 274
    iget-boolean v0, p1, Lcyw;->a:Z

    iput-boolean v0, p0, Ldbv;->f:Z

    .line 275
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-boolean v1, p0, Ldbv;->f:Z

    invoke-interface {v0, v1}, Ldbr;->l(Z)V

    .line 276
    iget-object v0, p0, Ldbv;->c:Ldbr;

    instance-of v0, v0, Ldcd;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Ldbv;->c:Ldbr;

    check-cast v0, Ldcd;

    .line 278
    iget-boolean v1, p0, Ldbv;->f:Z

    iput-boolean v1, v0, Ldcd;->f:Z

    .line 279
    iget-boolean v1, p0, Ldbv;->f:Z

    invoke-virtual {v0, v1}, Ldcd;->n(Z)V

    .line 281
    :cond_0
    return-void
.end method

.method public final handleFormatStreamChangeEvent(Lgdq;)V
    .locals 9
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 258
    iget-object v0, p0, Ldbv;->c:Ldbr;

    invoke-virtual {p1}, Lgdq;->a()Z

    move-result v2

    invoke-interface {v0, v2}, Ldbr;->c(Z)V

    .line 259
    invoke-virtual {p1}, Lgdq;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 260
    iget-object v0, p1, Lgdq;->d:[Lfre;

    iput-object v0, p0, Ldbv;->g:[Lfre;

    iget-object v0, p1, Lgdq;->a:Lfqj;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lgdq;->a:Lfqj;

    invoke-virtual {v0}, Lfqj;->b()I

    move-result v0

    :goto_0
    iget-object v2, p0, Ldbv;->g:[Lfre;

    array-length v2, v2

    new-array v5, v2, [Ljava/lang/String;

    move v2, v4

    move v3, v1

    :goto_1
    iget-object v1, p0, Ldbv;->g:[Lfre;

    array-length v1, v1

    if-ge v2, v1, :cond_3

    iget-object v1, p0, Ldbv;->g:[Lfre;

    aget-object v6, v1, v2

    iget v1, v6, Lfre;->a:I

    const/4 v7, -0x2

    if-ne v1, v7, :cond_1

    iget-object v1, p0, Ldbv;->h:Landroid/content/res/Resources;

    const v7, 0x7f090121

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_2
    aput-object v1, v5, v2

    iget v1, v6, Lfre;->a:I

    if-ne v1, v0, :cond_5

    move v1, v2

    :goto_3
    add-int/lit8 v2, v2, 0x1

    move v3, v1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v7, p0, Ldbv;->h:Landroid/content/res/Resources;

    iget-boolean v1, v6, Lfre;->b:Z

    if-eqz v1, :cond_2

    const v1, 0x7f090120

    :goto_4
    invoke-virtual {v7, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    iget v8, v6, Lfre;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v4

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_2
    const v1, 0x7f09011f

    goto :goto_4

    :cond_3
    iget-object v0, p0, Ldbv;->c:Ldbr;

    invoke-interface {v0, v5, v3}, Ldbr;->a([Ljava/lang/String;I)V

    .line 262
    :cond_4
    return-void

    :cond_5
    move v1, v3

    goto :goto_3
.end method

.method public final handlePlaybackServiceException(Lczb;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 267
    iget-object v0, p1, Lczb;->a:Lczc;

    const/16 v1, 0xa

    new-array v1, v1, [Lczc;

    const/4 v2, 0x0

    sget-object v3, Lczc;->a:Lczc;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lczc;->b:Lczc;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lczc;->c:Lczc;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, Lczc;->d:Lczc;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, Lczc;->e:Lczc;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    sget-object v3, Lczc;->f:Lczc;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lczc;->g:Lczc;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lczc;->h:Lczc;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lczc;->i:Lczc;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lczc;->k:Lczc;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lczc;->a([Lczc;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-object v1, p1, Lczb;->c:Ljava/lang/String;

    iget-boolean v2, p1, Lczb;->b:Z

    invoke-interface {v0, v1, v2}, Ldbr;->a(Ljava/lang/String;Z)V

    .line 270
    :cond_0
    return-void
.end method

.method public final handleSequencerHasPreviousNextEvent(Lczt;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-boolean v1, p1, Lczt;->a:Z

    invoke-interface {v0, v1}, Ldbr;->e(Z)V

    .line 239
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-boolean v1, p1, Lczt;->b:Z

    invoke-interface {v0, v1}, Ldbr;->d(Z)V

    .line 240
    return-void
.end method

.method public final handleVideoFullscreenEvent(Ldab;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 252
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-boolean v1, p1, Ldab;->a:Z

    invoke-interface {v0, v1}, Ldbr;->f(Z)V

    .line 253
    iget-object v0, p0, Ldbv;->c:Ldbr;

    iget-boolean v1, p1, Ldab;->b:Z

    invoke-interface {v0, v1}, Ldbr;->g(Z)V

    .line 254
    return-void
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 155
    iget-object v3, p1, Ldac;->a:Lgol;

    .line 156
    sget-object v0, Lgol;->b:Lgol;

    invoke-virtual {v3, v0}, Lgol;->a(Lgol;)Z

    move-result v0

    iput-boolean v0, p0, Ldbv;->k:Z

    .line 158
    sget-object v0, Lgol;->a:Lgol;

    if-ne v3, v0, :cond_3

    .line 159
    iput-boolean v2, p0, Ldbv;->i:Z

    iput-boolean v2, p0, Ldbv;->j:Z

    iput-boolean v2, p0, Ldbv;->k:Z

    iget-object v0, p0, Ldbv;->c:Ldbr;

    invoke-interface {v0}, Ldbr;->f()V

    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbu;->a:Ldbu;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbu;)V

    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbs;->a:Ldbs;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbs;)V

    iget-object v0, p0, Ldbv;->c:Ldbr;

    invoke-interface {v0, v2}, Ldbr;->i(Z)V

    const/4 v0, 0x0

    iput-object v0, p0, Ldbv;->g:[Lfre;

    .line 169
    :cond_0
    :goto_0
    invoke-virtual {v3}, Lgol;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 170
    iget-boolean v0, p1, Ldac;->f:Z

    if-nez v0, :cond_7

    iget-object v0, p1, Ldac;->d:Lfoy;

    if-nez v0, :cond_7

    .line 171
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbu;->e:Ldbu;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbu;)V

    .line 190
    :cond_1
    :goto_1
    iget-object v0, p0, Ldbv;->c:Ldbr;

    instance-of v0, v0, Ldcd;

    if-eqz v0, :cond_2

    .line 191
    iget-object v0, p0, Ldbv;->c:Ldbr;

    check-cast v0, Ldcd;

    iget-boolean v4, p1, Ldac;->f:Z

    if-nez v4, :cond_d

    :goto_2
    invoke-virtual {v0, v1}, Ldcd;->n(Z)V

    .line 194
    :cond_2
    sget-object v0, Lgol;->g:Lgol;

    invoke-virtual {v3, v0}, Lgol;->a(Lgol;)Z

    move-result v0

    .line 195
    iget-object v1, p0, Ldbv;->c:Ldbr;

    invoke-interface {v1, v0}, Ldbr;->h(Z)V

    .line 196
    return-void

    .line 160
    :cond_3
    iget-boolean v0, p0, Ldbv;->k:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    new-array v0, v0, [Lgol;

    sget-object v4, Lgol;->c:Lgol;

    aput-object v4, v0, v2

    sget-object v4, Lgol;->e:Lgol;

    aput-object v4, v0, v1

    sget-object v4, Lgol;->h:Lgol;

    aput-object v4, v0, v5

    invoke-virtual {v3, v0}, Lgol;->a([Lgol;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 161
    :cond_4
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbs;->d:Ldbs;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbs;)V

    goto :goto_0

    .line 162
    :cond_5
    new-array v0, v5, [Lgol;

    sget-object v4, Lgol;->d:Lgol;

    aput-object v4, v0, v2

    sget-object v4, Lgol;->g:Lgol;

    aput-object v4, v0, v1

    invoke-virtual {v3, v0}, Lgol;->a([Lgol;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 163
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbs;->c:Ldbs;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbs;)V

    goto :goto_0

    .line 164
    :cond_6
    sget-object v0, Lgol;->j:Lgol;

    if-ne v3, v0, :cond_0

    .line 165
    iput-boolean v1, p0, Ldbv;->j:Z

    .line 166
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbs;->g:Ldbs;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbs;)V

    goto :goto_0

    .line 173
    :cond_7
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbu;->c:Ldbu;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbu;)V

    .line 174
    iget-object v4, p0, Ldbv;->c:Ldbr;

    iget-object v0, p1, Ldac;->d:Lfoy;

    if-eqz v0, :cond_8

    iget-object v0, p1, Ldac;->d:Lfoy;

    iget-object v0, v0, Lfoy;->W:Lfkg;

    if-eqz v0, :cond_8

    move v0, v1

    :goto_3
    invoke-interface {v4, v0}, Ldbr;->m(Z)V

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_3

    .line 176
    :cond_9
    sget-object v0, Lgol;->g:Lgol;

    invoke-virtual {v3, v0}, Lgol;->a(Lgol;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 177
    iget-boolean v0, p1, Ldac;->f:Z

    if-eqz v0, :cond_b

    .line 178
    iget-boolean v0, p1, Ldac;->e:Z

    if-eqz v0, :cond_a

    .line 179
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbu;->d:Ldbu;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbu;)V

    .line 186
    :goto_4
    iget-object v4, p0, Ldbv;->c:Ldbr;

    iget-object v0, p1, Ldac;->b:Lfrl;

    if-eqz v0, :cond_c

    .line 187
    iget-object v0, p1, Ldac;->b:Lfrl;

    invoke-virtual {v0}, Lfrl;->k()Lfkg;

    move-result-object v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 186
    :goto_5
    invoke-interface {v4, v0}, Ldbr;->m(Z)V

    goto/16 :goto_1

    .line 181
    :cond_a
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbu;->a:Ldbu;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbu;)V

    goto :goto_4

    .line 184
    :cond_b
    iget-object v0, p0, Ldbv;->c:Ldbr;

    sget-object v4, Ldbu;->b:Ldbu;

    invoke-interface {v0, v4}, Ldbr;->a(Ldbu;)V

    goto :goto_4

    :cond_c
    move v0, v2

    .line 187
    goto :goto_5

    :cond_d
    move v1, v2

    .line 191
    goto/16 :goto_2
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Ldbv;->c:Ldbr;

    .line 245
    iget-wide v2, p1, Ldad;->a:J

    long-to-int v1, v2

    .line 246
    iget-wide v2, p1, Ldad;->b:J

    long-to-int v2, v2

    .line 247
    iget-wide v4, p1, Ldad;->c:J

    long-to-int v3, v4

    .line 244
    invoke-interface {v0, v1, v2, v3}, Ldbr;->a(III)V

    .line 248
    return-void
.end method

.method public final handleYouTubePlayerStateEvent(Ldae;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 200
    iget-boolean v0, p0, Ldbv;->k:Z

    if-nez v0, :cond_1

    .line 234
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    iget v0, p1, Ldae;->a:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 205
    :pswitch_1
    iput-boolean v1, p0, Ldbv;->j:Z

    .line 206
    iget-object v1, p0, Ldbv;->c:Ldbr;

    sget-object v0, Ldbs;->b:Ldbs;

    .line 229
    :goto_1
    invoke-interface {v1, v0}, Ldbr;->a(Ldbs;)V

    goto :goto_0

    .line 211
    :pswitch_2
    iget-boolean v0, p0, Ldbv;->j:Z

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Ldbv;->c:Ldbr;

    .line 230
    :cond_2
    sget-object v1, Ldbs;->c:Ldbs;

    move-object v2, v1

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    .line 217
    :pswitch_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbv;->i:Z

    .line 218
    iget-object v1, p0, Ldbv;->c:Ldbr;

    sget-object v0, Ldbs;->d:Ldbs;

    goto :goto_1

    .line 222
    :pswitch_4
    iput-boolean v1, p0, Ldbv;->i:Z

    .line 223
    iget-object v1, p0, Ldbv;->c:Ldbr;

    .line 224
    iget-boolean v0, p1, Ldae;->b:Z

    if-eqz v0, :cond_3

    sget-object v0, Ldbs;->b:Ldbs;

    .line 223
    :goto_2
    invoke-interface {v1, v0}, Ldbr;->a(Ldbs;)V

    goto :goto_0

    .line 224
    :cond_3
    sget-object v0, Ldbs;->c:Ldbs;

    goto :goto_2

    .line 228
    :pswitch_5
    iget-boolean v0, p0, Ldbv;->i:Z

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Ldbv;->c:Ldbr;

    .line 230
    iget-boolean v1, p1, Ldae;->b:Z

    if-eqz v1, :cond_2

    sget-object v1, Ldbs;->b:Ldbs;

    move-object v2, v1

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    .line 203
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
