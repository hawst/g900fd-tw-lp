.class public final Ldnz;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Ldeh;


# static fields
.field private static final c:I

.field private static final d:I

.field private static final e:I

.field private static final f:I


# instance fields
.field public a:Z

.field public b:Z

.field private final g:Landroid/view/Window;

.field private final h:Landroid/app/ActionBar;

.field private final i:Ldef;

.field private final j:Ldoa;

.field private k:Landroid/graphics/Rect;

.field private final l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private final r:Z

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 46
    const/4 v1, 0x3

    .line 50
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_0

    .line 51
    const/4 v1, 0x7

    .line 52
    const/16 v2, 0x600

    .line 54
    const/16 v0, 0x100

    .line 56
    :goto_0
    sput v1, Ldnz;->c:I

    .line 57
    sput v2, Ldnz;->e:I

    .line 58
    sput v0, Ldnz;->f:I

    .line 59
    const/4 v0, 0x1

    sput v0, Ldnz;->d:I

    .line 60
    return-void

    :cond_0
    move v2, v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/Window;Landroid/app/ActionBar;Ldef;Ldoa;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 87
    const-string v0, "window cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Window;

    iput-object v0, p0, Ldnz;->g:Landroid/view/Window;

    .line 88
    const-string v0, "playerOverlaysLayout cannot be null"

    .line 89
    invoke-static {p3, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldef;

    iput-object v0, p0, Ldnz;->i:Ldef;

    .line 90
    iput-object p0, p3, Ldef;->b:Ldeh;

    .line 91
    iput-object p2, p0, Ldnz;->h:Landroid/app/ActionBar;

    .line 92
    iput-object p4, p0, Ldnz;->j:Ldoa;

    .line 93
    invoke-virtual {p3, p0}, Ldef;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 94
    invoke-virtual {p3}, Ldef;->getSystemUiVisibility()I

    move-result v0

    iput v0, p0, Ldnz;->m:I

    .line 95
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Ldnz;->a:Z

    .line 99
    if-eqz p2, :cond_1

    .line 100
    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    new-array v1, v1, [I

    const v3, 0x10102eb

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 102
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Ldnz;->l:I

    .line 103
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 108
    :goto_1
    invoke-virtual {p1}, Landroid/view/Window;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Ldol;->a(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Ldnz;->r:Z

    .line 109
    iput-boolean v2, p0, Ldnz;->s:Z

    .line 110
    return-void

    :cond_0
    move v0, v2

    .line 95
    goto :goto_0

    .line 105
    :cond_1
    iput v2, p0, Ldnz;->l:I

    goto :goto_1
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 226
    invoke-virtual {p0, v2}, Ldnz;->removeMessages(I)V

    .line 227
    iget v3, p0, Ldnz;->m:I

    iget-boolean v0, p0, Ldnz;->n:Z

    iget-boolean v4, p0, Ldnz;->o:Z

    and-int/2addr v4, v0

    iget-boolean v0, p0, Ldnz;->s:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    and-int/2addr v4, v0

    sget v0, Ldnz;->c:I

    and-int/2addr v0, v3

    sget v5, Ldnz;->c:I

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_1
    iget-boolean v5, p0, Ldnz;->n:Z

    iget-boolean v6, p0, Ldnz;->o:Z

    and-int/2addr v5, v6

    iget-boolean v6, p0, Ldnz;->s:Z

    and-int/2addr v5, v6

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_4

    if-nez v0, :cond_4

    move v3, v1

    :goto_2
    if-ne v4, v0, :cond_0

    if-eq v5, v3, :cond_5

    :cond_0
    move v0, v1

    :goto_3
    iget-boolean v3, p0, Ldnz;->q:Z

    if-nez v3, :cond_6

    if-eqz v0, :cond_6

    :goto_4
    if-eqz v1, :cond_1

    .line 228
    const-wide/16 v0, 0x9c4

    invoke-virtual {p0, v2, v0, v1}, Ldnz;->sendEmptyMessageDelayed(IJ)Z

    .line 230
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 227
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4
.end method

.method private d()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 279
    iget-boolean v1, p0, Ldnz;->r:Z

    if-eqz v1, :cond_1

    .line 285
    :cond_0
    :goto_0
    return v0

    .line 284
    :cond_1
    iget-boolean v1, p0, Ldnz;->b:Z

    if-eqz v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_2

    iget-object v1, p0, Ldnz;->h:Landroid/app/ActionBar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Ldnz;->g:Landroid/view/Window;

    const/16 v2, 0x9

    .line 285
    invoke-virtual {v1, v2}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v2, 0x0

    .line 289
    iget-object v0, p0, Ldnz;->i:Ldef;

    invoke-virtual {v0}, Ldef;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_5

    .line 290
    iget-object v0, p0, Ldnz;->i:Ldef;

    invoke-virtual {v0, v1}, Ldef;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 292
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Ldeg;

    .line 293
    iget-boolean v0, v0, Ldeg;->a:Z

    if-eqz v0, :cond_1

    .line 294
    iget-boolean v0, p0, Ldnz;->n:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_2

    iget-object v0, p0, Ldnz;->k:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 295
    :cond_0
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 289
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 296
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_3

    .line 297
    iget-object v0, p0, Ldnz;->k:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Ldnz;->k:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Ldnz;->k:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Ldnz;->k:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v0, v5, v6, v7}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 298
    :cond_3
    iget-boolean v0, p0, Ldnz;->a:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldnz;->g:Landroid/view/Window;

    const/16 v5, 0x9

    invoke-virtual {v0, v5}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 299
    iget v0, p0, Ldnz;->l:I

    invoke-virtual {v4, v2, v0, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 301
    :cond_4
    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 305
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldnz;->removeMessages(I)V

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldnz;->q:Z

    .line 219
    return-void
.end method

.method public final a(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 181
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Ldnz;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Ldnz;->g:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v1

    .line 186
    iget-object v0, p0, Ldnz;->h:Landroid/app/ActionBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldnz;->h:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 187
    :goto_0
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 188
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Ldnz;->l:I

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 192
    :cond_0
    iget-object v0, p0, Ldnz;->k:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 193
    iput-object p1, p0, Ldnz;->k:Landroid/graphics/Rect;

    .line 194
    invoke-direct {p0}, Ldnz;->e()V

    .line 196
    :cond_1
    return-void

    .line 186
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 200
    iget-boolean v0, p0, Ldnz;->s:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "FSUI enableFullscreenTouchEvents "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 201
    iget-boolean v0, p0, Ldnz;->s:Z

    if-eq v0, p1, :cond_0

    .line 202
    iput-boolean p1, p0, Ldnz;->s:Z

    .line 203
    invoke-virtual {p0}, Ldnz;->b()V

    .line 205
    :cond_0
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/16 v1, 0x400

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 249
    invoke-direct {p0}, Ldnz;->c()V

    .line 251
    iget-boolean v3, p0, Ldnz;->r:Z

    if-eqz v3, :cond_1

    .line 252
    iget-boolean v2, p0, Ldnz;->n:Z

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "FSUI Window.FLAG_FULLSCREEN "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->d(Ljava/lang/String;)V

    .line 253
    iget-object v2, p0, Ldnz;->g:Landroid/view/Window;

    iget-boolean v3, p0, Ldnz;->n:Z

    if-eqz v3, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {v2, v0, v1}, Landroid/view/Window;->setFlags(II)V

    .line 271
    :goto_0
    return-void

    .line 257
    :cond_1
    iget-boolean v1, p0, Ldnz;->n:Z

    if-eqz v1, :cond_5

    .line 258
    sget v3, Ldnz;->e:I

    .line 259
    iget-boolean v1, p0, Ldnz;->o:Z

    if-eqz v1, :cond_4

    .line 262
    iget-boolean v1, p0, Ldnz;->s:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    or-int/2addr v1, v3

    .line 265
    :goto_2
    invoke-direct {p0}, Ldnz;->d()Z

    move-result v3

    if-eqz v3, :cond_3

    move v3, v0

    :goto_3
    or-int/2addr v1, v3

    .line 267
    :goto_4
    const-string v3, "FSUI setSystemUiVisibility 0x%08x [fullscreen=%s hideSysUi=%s]"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    .line 268
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    iget-boolean v0, p0, Ldnz;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v2

    const/4 v0, 0x2

    iget-boolean v2, p0, Ldnz;->o:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v4, v0

    .line 267
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Ldnz;->i:Ldef;

    invoke-virtual {v0, v1}, Ldef;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 262
    :cond_2
    sget v1, Ldnz;->c:I

    goto :goto_1

    .line 265
    :cond_3
    sget v3, Ldnz;->f:I

    goto :goto_3

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    move v1, v0

    goto :goto_4
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x400

    const/4 v1, 0x0

    .line 118
    iget-boolean v0, p0, Ldnz;->n:Z

    if-eq v0, p1, :cond_4

    .line 119
    if-eqz p1, :cond_0

    .line 120
    iget-object v0, p0, Ldnz;->g:Landroid/view/Window;

    .line 121
    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Ldnz;->p:Z

    .line 124
    :cond_0
    iput-boolean p1, p0, Ldnz;->n:Z

    .line 125
    invoke-virtual {p0}, Ldnz;->b()V

    .line 126
    invoke-direct {p0}, Ldnz;->e()V

    .line 127
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v0, v3, :cond_3

    .line 128
    iget-object v0, p0, Ldnz;->g:Landroid/view/Window;

    if-nez p1, :cond_1

    iget-boolean v3, p0, Ldnz;->p:Z

    if-eqz v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFlags(II)V

    .line 133
    :cond_3
    iget-boolean v0, p0, Ldnz;->a:Z

    if-eqz v0, :cond_4

    .line 134
    if-eqz p1, :cond_6

    iget-object v0, p0, Ldnz;->g:Landroid/view/Window;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 135
    iget-object v0, p0, Ldnz;->h:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 141
    :cond_4
    :goto_1
    return-void

    :cond_5
    move v0, v1

    .line 121
    goto :goto_0

    .line 136
    :cond_6
    if-nez p1, :cond_4

    .line 137
    iget-object v0, p0, Ldnz;->h:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_1
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 147
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1c

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "FSUI setSystemUiHidden "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 148
    iput-boolean p1, p0, Ldnz;->o:Z

    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldnz;->removeMessages(I)V

    .line 150
    invoke-virtual {p0}, Ldnz;->b()V

    .line 151
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-boolean v0, p0, Ldnz;->n:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldnz;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldnz;->g:Landroid/view/Window;

    const/16 v1, 0x9

    .line 152
    invoke-virtual {v0, v1}, Landroid/view/Window;->hasFeature(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    if-eqz p1, :cond_1

    .line 154
    iget-object v0, p0, Ldnz;->h:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Ldnz;->h:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    goto :goto_0
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 209
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 214
    :goto_0
    return-void

    .line 211
    :pswitch_0
    invoke-virtual {p0}, Ldnz;->b()V

    goto :goto_0

    .line 209
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onSystemUiVisibilityChange(I)V
    .locals 2

    .prologue
    .line 163
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Ldnz;->i:Ldef;

    .line 164
    invoke-virtual {v0}, Ldef;->getSystemUiVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 167
    iget-object v0, p0, Ldnz;->i:Ldef;

    invoke-virtual {v0, p1}, Ldef;->setSystemUiVisibility(I)V

    .line 169
    :cond_0
    iget-object v0, p0, Ldnz;->j:Ldoa;

    if-eqz v0, :cond_1

    iget v0, p0, Ldnz;->m:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Ldnz;->s:Z

    if-nez v0, :cond_1

    .line 172
    iget-object v0, p0, Ldnz;->j:Ldoa;

    invoke-interface {v0}, Ldoa;->a()V

    .line 174
    :cond_1
    iput p1, p0, Ldnz;->m:I

    .line 176
    invoke-direct {p0}, Ldnz;->c()V

    .line 177
    return-void
.end method
