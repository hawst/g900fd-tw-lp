.class public final Lduy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Levn;

.field final b:Landroid/os/Handler;

.field final c:Lezj;

.field d:Ldww;

.field e:Ldwh;

.field f:Ldxb;

.field g:Ldwo;

.field h:Ldwo;

.field i:Ldwj;

.field j:Lfoy;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Z

.field n:J

.field o:J

.field p:J

.field q:J

.field r:I

.field private final s:Ldyg;


# direct methods
.method public constructor <init>(Levn;Lezj;Ldyg;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Ldww;->e:Ldww;

    iput-object v0, p0, Lduy;->d:Ldww;

    .line 52
    sget-object v0, Ldwj;->f:Ldwj;

    iput-object v0, p0, Lduy;->i:Ldwj;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lduy;->k:Ljava/lang/String;

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lduy;->l:Ljava/lang/String;

    .line 85
    const/16 v0, 0x1e

    iput v0, p0, Lduy;->r:I

    .line 91
    iput-object p1, p0, Lduy;->a:Levn;

    .line 92
    iput-object p2, p0, Lduy;->c:Lezj;

    .line 93
    iput-object p3, p0, Lduy;->s:Ldyg;

    .line 95
    new-instance v0, Lduz;

    invoke-direct {v0, p0}, Lduz;-><init>(Lduy;)V

    iput-object v0, p0, Lduy;->b:Landroid/os/Handler;

    .line 108
    sget-object v0, Ldxb;->a:Ldxb;

    invoke-virtual {p0, v0}, Lduy;->a(Ldxb;)V

    .line 109
    sget-object v0, Ldwo;->a:Ldwo;

    invoke-virtual {p0, v0}, Lduy;->a(Ldwo;)V

    .line 110
    sget-object v0, Ldwo;->a:Ldwo;

    invoke-virtual {p0, v0}, Lduy;->b(Ldwo;)V

    .line 111
    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lduy;->m:Z

    .line 123
    iget-object v0, p0, Lduy;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 124
    return-void
.end method

.method final a(J)V
    .locals 5

    .prologue
    .line 127
    :goto_0
    iget-boolean v0, p0, Lduy;->m:Z

    if-eqz v0, :cond_0

    .line 128
    iget-wide v0, p0, Lduy;->n:J

    sub-long v0, p1, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 129
    invoke-virtual {p0}, Lduy;->a()V

    goto :goto_0

    .line 136
    :cond_0
    iput-wide p1, p0, Lduy;->n:J

    .line 137
    iget-object v0, p0, Lduy;->c:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lduy;->o:J

    .line 138
    :cond_1
    return-void
.end method

.method final a(Ldwh;)V
    .locals 4

    .prologue
    .line 179
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    iget-object v0, p0, Lduy;->d:Ldww;

    sget-object v1, Ldww;->e:Ldww;

    if-ne v0, v1, :cond_0

    .line 181
    iget-object v0, p0, Lduy;->s:Ldyg;

    const-string v1, "Remote control trying to move to ERROR state while OFFLINE"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 188
    :goto_0
    return-void

    .line 184
    :cond_0
    iput-object p1, p0, Lduy;->e:Ldwh;

    .line 185
    sget-object v0, Ldww;->f:Ldww;

    iput-object v0, p0, Lduy;->d:Ldww;

    .line 186
    iget-object v0, p0, Lduy;->s:Ldyg;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MDX state moved to error state with error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Lduy;->a:Levn;

    new-instance v1, Ldwx;

    iget-object v2, p0, Lduy;->d:Ldww;

    invoke-direct {v1, v2}, Ldwx;-><init>(Ldww;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(Ldwj;)V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lduy;->i:Ldwj;

    invoke-virtual {v0, p1}, Ldwj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iput-object p1, p0, Lduy;->i:Ldwj;

    .line 231
    iget-object v0, p0, Lduy;->a:Levn;

    new-instance v1, Ldwi;

    iget-object v2, p0, Lduy;->i:Ldwj;

    invoke-direct {v1, v2}, Ldwi;-><init>(Ldwj;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 233
    :cond_0
    return-void
.end method

.method final a(Ldwo;)V
    .locals 4

    .prologue
    .line 195
    iget-object v0, p0, Lduy;->g:Ldwo;

    if-ne v0, p1, :cond_0

    .line 213
    :goto_0
    return-void

    .line 198
    :cond_0
    iput-object p1, p0, Lduy;->g:Ldwo;

    .line 199
    iget-object v0, p0, Lduy;->s:Ldyg;

    iget-object v1, p0, Lduy;->g:Ldwo;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MDX video player state moved to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 201
    sget-object v0, Ldva;->a:[I

    iget-object v1, p0, Lduy;->g:Ldwo;

    invoke-virtual {v1}, Ldwo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 207
    :goto_1
    iget-object v0, p0, Lduy;->a:Levn;

    new-instance v1, Ldxa;

    iget-object v2, p0, Lduy;->g:Ldwo;

    invoke-direct {v1, v2}, Ldxa;-><init>(Ldwo;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0

    .line 206
    :pswitch_0
    invoke-virtual {p0}, Lduy;->b()V

    goto :goto_1

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method final a(Ldwr;)V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lduy;->a:Levn;

    new-instance v1, Ldwu;

    invoke-direct {v1, p1}, Ldwu;-><init>(Ldwr;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 310
    return-void
.end method

.method final a(Ldww;)V
    .locals 4

    .prologue
    .line 169
    sget-object v0, Ldww;->f:Ldww;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 170
    iget-object v0, p0, Lduy;->d:Ldww;

    if-ne v0, p1, :cond_1

    .line 176
    :goto_1
    return-void

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :cond_1
    iput-object p1, p0, Lduy;->d:Ldww;

    .line 174
    iget-object v0, p0, Lduy;->s:Ldyg;

    iget-object v1, p0, Lduy;->d:Ldww;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MDX state moved to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lduy;->a:Levn;

    new-instance v1, Ldwx;

    iget-object v2, p0, Lduy;->d:Ldww;

    invoke-direct {v1, v2}, Ldwx;-><init>(Ldww;)V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method final a(Ldxb;)V
    .locals 4

    .prologue
    .line 342
    iget-object v0, p0, Lduy;->f:Ldxb;

    if-ne v0, p1, :cond_0

    .line 348
    :goto_0
    return-void

    .line 345
    :cond_0
    iput-object p1, p0, Lduy;->f:Ldxb;

    .line 346
    iget-object v0, p0, Lduy;->s:Ldyg;

    iget-object v1, p0, Lduy;->f:Ldxb;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MDX video stage moved to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lduy;->a:Levn;

    new-instance v1, Ldxc;

    iget-object v2, p0, Lduy;->f:Ldxb;

    invoke-direct {v1, v2}, Ldxc;-><init>(Ldxb;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 251
    iput-object p1, p0, Lduy;->k:Ljava/lang/String;

    .line 253
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    sget-object v0, Ldwj;->f:Ldwj;

    .line 272
    :goto_0
    iget-object v1, p0, Lduy;->a:Levn;

    new-instance v2, Ldwp;

    invoke-direct {v2, v0}, Ldwp;-><init>(Ldwj;)V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    .line 273
    :goto_1
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lduy;->i:Ldwj;

    invoke-virtual {v0}, Ldwj;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lduy;->i:Ldwj;

    .line 257
    invoke-virtual {v0, p1}, Ldwj;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lduy;->i:Ldwj;

    goto :goto_0

    .line 259
    :cond_1
    const-string v0, ""

    iget-object v1, p0, Lduy;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 262
    new-instance v0, Ldwk;

    invoke-direct {v0}, Ldwk;-><init>()V

    iget-object v1, p0, Lduy;->l:Ljava/lang/String;

    .line 263
    invoke-virtual {v0, v1}, Ldwk;->a(Ljava/lang/String;)Ldwk;

    move-result-object v0

    .line 264
    invoke-virtual {v0, p1}, Ldwk;->b(Ljava/lang/String;)Ldwk;

    move-result-object v0

    const/4 v1, 0x1

    .line 265
    invoke-virtual {v0, v1}, Ldwk;->a(I)Ldwk;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Ldwk;->a()Ldwj;

    move-result-object v0

    goto :goto_0

    .line 268
    :cond_2
    iget-object v0, p0, Lduy;->s:Ldyg;

    const-string v1, "Screen states: first video Id is not set"

    invoke-virtual {v0, v1}, Ldyg;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method final b()V
    .locals 2

    .prologue
    .line 141
    invoke-virtual {p0}, Lduy;->a()V

    .line 142
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lduy;->a(J)V

    .line 143
    return-void
.end method

.method final b(J)V
    .locals 3

    .prologue
    .line 153
    iput-wide p1, p0, Lduy;->p:J

    .line 154
    iget-object v0, p0, Lduy;->c:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lduy;->q:J

    .line 155
    return-void
.end method

.method final b(Ldwo;)V
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Lduy;->h:Ldwo;

    if-ne v0, p1, :cond_0

    .line 226
    :goto_0
    return-void

    .line 219
    :cond_0
    iput-object p1, p0, Lduy;->h:Ldwo;

    .line 220
    iget-object v0, p0, Lduy;->s:Ldyg;

    iget-object v1, p0, Lduy;->h:Ldwo;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "MDX ad player state moved to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 222
    iget-object v0, p0, Lduy;->h:Ldwo;

    iget-boolean v0, v0, Ldwo;->l:Z

    if-nez v0, :cond_1

    .line 223
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lduy;->b(J)V

    .line 225
    :cond_1
    iget-object v0, p0, Lduy;->a:Levn;

    new-instance v1, Ldwa;

    iget-object v2, p0, Lduy;->h:Ldwo;

    invoke-direct {v1, v2}, Ldwa;-><init>(Ldwo;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 285
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lduy;->l:Ljava/lang/String;

    .line 288
    return-void
.end method

.method final c()V
    .locals 2

    .prologue
    .line 246
    sget-object v0, Ldwo;->a:Ldwo;

    invoke-virtual {p0, v0}, Lduy;->b(Ldwo;)V

    .line 247
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lduy;->b(J)V

    .line 248
    return-void
.end method
