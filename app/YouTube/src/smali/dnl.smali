.class final Ldnl;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field volatile a:Z

.field volatile b:Z

.field final synthetic c:Ldne;

.field private final d:Landroid/net/ConnectivityManager;


# direct methods
.method public constructor <init>(Ldne;)V
    .locals 2

    .prologue
    .line 740
    iput-object p1, p0, Ldnl;->c:Ldne;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 741
    iget-object v0, p1, Ldne;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Ldnl;->d:Landroid/net/ConnectivityManager;

    .line 742
    return-void
.end method


# virtual methods
.method a()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 771
    iget-object v0, p0, Ldnl;->d:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 772
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    move v3, v0

    .line 773
    :goto_0
    if-eqz v3, :cond_2

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    .line 774
    :goto_1
    iget-boolean v4, p0, Ldnl;->a:Z

    if-ne v4, v3, :cond_3

    iget-boolean v4, p0, Ldnl;->b:Z

    if-ne v4, v0, :cond_3

    .line 780
    :goto_2
    return v2

    :cond_0
    move v3, v2

    .line 772
    goto :goto_0

    :cond_1
    move v0, v2

    .line 773
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    .line 778
    :cond_3
    iput-boolean v3, p0, Ldnl;->a:Z

    .line 779
    iput-boolean v0, p0, Ldnl;->b:Z

    move v2, v1

    .line 780
    goto :goto_2
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 765
    invoke-virtual {p0}, Ldnl;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Ldnl;->c:Ldne;

    invoke-static {v0}, Ldne;->a(Ldne;)V

    .line 768
    :cond_0
    return-void
.end method
