.class public final Ldbc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbi;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/Class;

.field private final d:I

.field private e:Landroid/app/Service;

.field private f:Landroid/app/NotificationManager;

.field private g:Landroid/content/res/Resources;

.field private h:Ljava/lang/String;

.field private final i:Landroid/content/BroadcastReceiver;

.field private final j:Landroid/content/IntentFilter;

.field private final k:Ldbb;

.field private l:Z

.field private m:Lba;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ldbj;ILdbb;Landroid/app/Service;)V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldbc;->a:Landroid/content/Context;

    .line 89
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Ldbc;->c:Ljava/lang/Class;

    .line 90
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbb;

    iput-object v0, p0, Ldbc;->k:Ldbb;

    .line 91
    iput-object p2, p0, Ldbc;->b:Ljava/lang/String;

    .line 92
    iput p5, p0, Ldbc;->d:I

    .line 94
    const-string v0, "notification"

    .line 98
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Ldbc;->f:Landroid/app/NotificationManager;

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Ldbc;->g:Landroid/content/res/Resources;

    .line 101
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Ldbc;->j:Landroid/content/IntentFilter;

    .line 103
    iget-object v0, p0, Ldbc;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_prev"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Ldbc;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_play_pause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Ldbc;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_next"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Ldbc;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_close"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Ldbc;->j:Landroid/content/IntentFilter;

    const-string v1, "com.google.android.youtube.action.controller_notification_replay"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    new-instance v0, Ldbd;

    invoke-direct {v0, p0, p4}, Ldbd;-><init>(Ldbc;Ldbj;)V

    iput-object v0, p0, Ldbc;->i:Landroid/content/BroadcastReceiver;

    .line 124
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ldbj;Ldbb;I)V
    .locals 8

    .prologue
    .line 63
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p6

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Ldbc;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Class;Ldbj;ILdbb;Landroid/app/Service;)V

    .line 71
    return-void
.end method

.method private a(Ldbk;Z)Landroid/widget/RemoteViews;
    .locals 10

    .prologue
    const v9, 0x7f08025c

    const/4 v3, 0x1

    const v8, 0x7f080261

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 157
    if-eqz p2, :cond_5

    const v0, 0x7f0400ab

    .line 159
    :goto_0
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v4, p0, Ldbc;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v4, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 162
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v0, v4, :cond_0

    .line 163
    const v0, 0x7f080259

    const-string v4, "setBackgroundColor"

    const v5, -0xb9b9ba

    invoke-virtual {v6, v0, v4, v5}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 165
    :cond_0
    const v0, 0x7f08008b

    iget-object v4, p1, Ldbk;->a:Ljava/lang/String;

    invoke-virtual {v6, v0, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 166
    const v0, 0x7f08011d

    iget-object v4, p0, Ldbc;->b:Ljava/lang/String;

    invoke-virtual {v6, v0, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 168
    iget-object v0, p1, Ldbk;->b:Ldbl;

    sget-object v4, Ldbl;->c:Ldbl;

    if-ne v0, v4, :cond_6

    const v0, 0x7f020150

    .line 170
    :goto_1
    const v4, 0x7f08025d

    invoke-virtual {v6, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 173
    iget-object v0, p1, Ldbk;->b:Ldbl;

    sget-object v4, Ldbl;->a:Ldbl;

    if-ne v0, v4, :cond_7

    move v5, v3

    .line 174
    :goto_2
    const v4, 0x7f08025e

    if-eqz v5, :cond_8

    move v0, v1

    :goto_3
    invoke-virtual {v6, v4, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 177
    iget-object v0, p1, Ldbk;->b:Ldbl;

    sget-object v4, Ldbl;->e:Ldbl;

    if-ne v0, v4, :cond_9

    move v4, v3

    .line 178
    :goto_4
    const v7, 0x7f08025f

    if-eqz v4, :cond_a

    move v0, v1

    :goto_5
    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 181
    iget-object v0, p1, Ldbk;->b:Ldbl;

    sget-object v7, Ldbl;->f:Ldbl;

    if-ne v0, v7, :cond_b

    .line 182
    :goto_6
    const v7, 0x7f080260

    if-eqz v3, :cond_c

    move v0, v1

    :goto_7
    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 185
    const v7, 0x7f08025d

    if-nez v5, :cond_d

    if-nez v4, :cond_d

    if-nez v3, :cond_d

    move v0, v1

    :goto_8
    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 189
    iget-object v0, p1, Ldbk;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 190
    iget-object v3, p0, Ldbc;->g:Landroid/content/res/Resources;

    if-eqz p2, :cond_e

    const v0, 0x7f0a006c

    :goto_9
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v3, v0

    .line 192
    iget-object v4, p0, Ldbc;->g:Landroid/content/res/Resources;

    if-eqz p2, :cond_f

    const v0, 0x7f0a006d

    :goto_a
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 194
    const v4, 0x7f0800a4

    iget-object v5, p1, Ldbk;->e:Landroid/graphics/Bitmap;

    .line 196
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v3, v7

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v0, v7

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v0, v7

    float-to-int v0, v0

    invoke-static {v5, v3, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 194
    invoke-virtual {v6, v4, v0}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 199
    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Ldbc;->k:Ldbb;

    invoke-virtual {v0}, Ldbb;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    const v0, 0x7f08011c

    iget-object v3, p0, Ldbc;->k:Ldbb;

    invoke-virtual {v3}, Ldbb;->a()Landroid/widget/RemoteViews;

    move-result-object v3

    invoke-virtual {v6, v0, v3}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 201
    const v0, 0x7f08011c

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 205
    :cond_2
    if-eqz p2, :cond_12

    .line 206
    const-string v0, "setEnabled"

    iget-boolean v1, p1, Ldbk;->c:Z

    invoke-virtual {v6, v9, v0, v1}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 207
    const-string v0, "setEnabled"

    iget-boolean v1, p1, Ldbk;->d:Z

    invoke-virtual {v6, v8, v0, v1}, Landroid/widget/RemoteViews;->setBoolean(ILjava/lang/String;Z)V

    .line 208
    iget-boolean v0, p1, Ldbk;->c:Z

    if-eqz v0, :cond_10

    const v0, 0x7f020152

    .line 211
    :goto_b
    invoke-virtual {v6, v9, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 212
    iget-boolean v0, p1, Ldbk;->d:Z

    if-eqz v0, :cond_11

    const v0, 0x7f02014e

    .line 215
    :goto_c
    invoke-virtual {v6, v8, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 225
    :cond_3
    :goto_d
    if-eqz p2, :cond_4

    .line 227
    const-string v0, "com.google.android.youtube.action.controller_notification_prev"

    invoke-direct {p0, v6, v9, v0}, Ldbc;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 229
    :cond_4
    const v0, 0x7f08025d

    const-string v1, "com.google.android.youtube.action.controller_notification_play_pause"

    invoke-direct {p0, v6, v0, v1}, Ldbc;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 230
    const-string v0, "com.google.android.youtube.action.controller_notification_next"

    invoke-direct {p0, v6, v8, v0}, Ldbc;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 231
    const v0, 0x7f08025f

    const-string v1, "com.google.android.youtube.action.controller_notification_replay"

    invoke-direct {p0, v6, v0, v1}, Ldbc;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 232
    const v0, 0x7f0800c8

    const-string v1, "com.google.android.youtube.action.controller_notification_close"

    invoke-direct {p0, v6, v0, v1}, Ldbc;->a(Landroid/widget/RemoteViews;ILjava/lang/String;)V

    .line 234
    return-object v6

    .line 157
    :cond_5
    const v0, 0x7f0400ad

    goto/16 :goto_0

    .line 168
    :cond_6
    const v0, 0x7f020151

    goto/16 :goto_1

    :cond_7
    move v5, v1

    .line 173
    goto/16 :goto_2

    :cond_8
    move v0, v2

    .line 174
    goto/16 :goto_3

    :cond_9
    move v4, v1

    .line 177
    goto/16 :goto_4

    :cond_a
    move v0, v2

    .line 178
    goto/16 :goto_5

    :cond_b
    move v3, v1

    .line 181
    goto/16 :goto_6

    :cond_c
    move v0, v2

    .line 182
    goto/16 :goto_7

    :cond_d
    move v0, v2

    .line 185
    goto/16 :goto_8

    .line 190
    :cond_e
    const v0, 0x7f0a006a

    goto/16 :goto_9

    .line 192
    :cond_f
    const v0, 0x7f0a006b

    goto/16 :goto_a

    .line 208
    :cond_10
    const v0, 0x7f020153

    goto :goto_b

    .line 212
    :cond_11
    const v0, 0x7f02014f

    goto :goto_c

    .line 217
    :cond_12
    iget-boolean v0, p1, Ldbk;->d:Z

    if-eqz v0, :cond_13

    :goto_e
    invoke-virtual {v6, v8, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 218
    iget-object v0, p0, Ldbc;->h:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 220
    const v0, 0x7f08011d

    iget-object v1, p0, Ldbc;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_d

    :cond_13
    move v1, v2

    .line 217
    goto :goto_e
.end method

.method private a(Landroid/widget/RemoteViews;ILjava/lang/String;)V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Ldbc;->a:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x8000000

    invoke-static {v0, v1, v2, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 284
    invoke-virtual {p1, p2, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 285
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Ldbc;->e:Landroid/app/Service;

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Ldbc;->e:Landroid/app/Service;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Service;->stopForeground(Z)V

    .line 145
    :goto_0
    iget-boolean v0, p0, Ldbc;->l:Z

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Ldbc;->a:Landroid/content/Context;

    iget-object v1, p0, Ldbc;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldbc;->l:Z

    .line 149
    :cond_0
    return-void

    .line 142
    :cond_1
    iget-object v0, p0, Ldbc;->f:Landroid/app/NotificationManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method public final a(Ldbk;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 128
    invoke-direct {p0, p1, v8}, Ldbc;->a(Ldbk;Z)Landroid/widget/RemoteViews;

    move-result-object v0

    .line 129
    invoke-direct {p0, p1, v7}, Ldbc;->a(Ldbk;Z)Landroid/widget/RemoteViews;

    move-result-object v1

    .line 130
    iget-object v2, p1, Ldbk;->a:Ljava/lang/String;

    iget-object v3, p0, Ldbc;->m:Lba;

    if-nez v3, :cond_0

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Ldbc;->a:Landroid/content/Context;

    iget-object v5, p0, Ldbc;->c:Ljava/lang/Class;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x4000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v4, Lba;

    iget-object v5, p0, Ldbc;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, Lba;-><init>(Landroid/content/Context;)V

    iget v5, p0, Ldbc;->d:I

    invoke-virtual {v4, v5}, Lba;->a(I)Lba;

    move-result-object v4

    invoke-virtual {v4, v7}, Lba;->a(Z)Lba;

    move-result-object v4

    iget-object v5, p0, Ldbc;->a:Landroid/content/Context;

    const/high16 v6, 0x8000000

    invoke-static {v5, v8, v3, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v4, v3}, Lba;->a(Landroid/app/PendingIntent;)Lba;

    move-result-object v3

    invoke-virtual {v3, v7}, Lba;->c(I)Lba;

    move-result-object v3

    iput-object v3, p0, Ldbc;->m:Lba;

    :cond_0
    iget-object v3, p0, Ldbc;->m:Lba;

    invoke-virtual {v3, v0}, Lba;->a(Landroid/widget/RemoteViews;)Lba;

    move-result-object v0

    invoke-virtual {v0, v2}, Lba;->d(Ljava/lang/CharSequence;)Lba;

    move-result-object v0

    invoke-virtual {v0}, Lba;->a()Landroid/app/Notification;

    move-result-object v0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    iput-object v1, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    :cond_1
    iget-object v1, p0, Ldbc;->e:Landroid/app/Service;

    if-eqz v1, :cond_3

    iget-object v1, p0, Ldbc;->e:Landroid/app/Service;

    invoke-virtual {v1, v9, v0}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    .line 131
    :goto_0
    iget-boolean v0, p0, Ldbc;->l:Z

    if-nez v0, :cond_2

    .line 132
    iget-object v0, p0, Ldbc;->a:Landroid/content/Context;

    iget-object v1, p0, Ldbc;->i:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Ldbc;->j:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 133
    iput-boolean v7, p0, Ldbc;->l:Z

    .line 135
    :cond_2
    return-void

    .line 130
    :cond_3
    iget-object v1, p0, Ldbc;->f:Landroid/app/NotificationManager;

    invoke-virtual {v1, v9, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Ldbc;->h:Ljava/lang/String;

    .line 154
    return-void
.end method
