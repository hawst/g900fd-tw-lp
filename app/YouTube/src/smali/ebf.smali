.class public abstract Lebf;
.super Lecc;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:J

.field private final a:Ledv;

.field public final b:Lean;

.field final c:Lebj;

.field public final d:Landroid/os/Handler;

.field e:Landroid/media/MediaCodec;

.field f:I

.field private final h:Z

.field private final i:Lebz;

.field private final j:Leby;

.field private final k:Lebw;

.field private final l:Ljava/util/HashSet;

.field private final m:Landroid/media/MediaCodec$BufferInfo;

.field private n:Lebv;

.field private o:Ljava/util/Map;

.field private p:Z

.field private q:[Ljava/nio/ByteBuffer;

.field private r:[Ljava/nio/ByteBuffer;

.field private s:J

.field private t:I

.field private u:I

.field private v:Z

.field private w:Z

.field private x:I

.field private y:I

.field private z:Z


# direct methods
.method public constructor <init>(Lebz;Ledv;ZLandroid/os/Handler;Lebj;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 171
    invoke-direct {p0}, Lecc;-><init>()V

    .line 172
    sget v0, Legz;->a:I

    const/16 v2, 0x10

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 173
    iput-object p1, p0, Lebf;->i:Lebz;

    .line 174
    iput-object p2, p0, Lebf;->a:Ledv;

    .line 175
    iput-boolean p3, p0, Lebf;->h:Z

    .line 176
    iput-object p4, p0, Lebf;->d:Landroid/os/Handler;

    .line 177
    iput-object p5, p0, Lebf;->c:Lebj;

    .line 178
    new-instance v0, Lean;

    invoke-direct {v0}, Lean;-><init>()V

    iput-object v0, p0, Lebf;->b:Lean;

    .line 179
    new-instance v0, Leby;

    invoke-direct {v0, v1}, Leby;-><init>(I)V

    iput-object v0, p0, Lebf;->j:Leby;

    .line 180
    new-instance v0, Lebw;

    invoke-direct {v0}, Lebw;-><init>()V

    iput-object v0, p0, Lebf;->k:Lebw;

    .line 181
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lebf;->l:Ljava/util/HashSet;

    .line 182
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, Lebf;->m:Landroid/media/MediaCodec$BufferInfo;

    .line 183
    return-void

    :cond_0
    move v0, v1

    .line 172
    goto :goto_0
.end method

.method private a(Landroid/media/MediaCodec$CryptoException;)V
    .locals 2

    .prologue
    .line 765
    iget-object v0, p0, Lebf;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebf;->c:Lebj;

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lebf;->d:Landroid/os/Handler;

    new-instance v1, Lebh;

    invoke-direct {v1, p0, p1}, Lebh;-><init>(Lebf;Landroid/media/MediaCodec$CryptoException;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 773
    :cond_0
    return-void
.end method

.method private a(Z)Z
    .locals 7

    .prologue
    .line 479
    iget-boolean v0, p0, Lebf;->z:Z

    if-eqz v0, :cond_0

    .line 480
    const/4 v0, 0x0

    .line 585
    :goto_0
    return v0

    .line 482
    :cond_0
    iget v0, p0, Lebf;->t:I

    if-gez v0, :cond_2

    .line 483
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v0

    iput v0, p0, Lebf;->t:I

    .line 484
    iget v0, p0, Lebf;->t:I

    if-gez v0, :cond_1

    .line 485
    const/4 v0, 0x0

    goto :goto_0

    .line 487
    :cond_1
    iget-object v0, p0, Lebf;->j:Leby;

    iget-object v1, p0, Lebf;->q:[Ljava/nio/ByteBuffer;

    iget v2, p0, Lebf;->t:I

    aget-object v1, v1, v2

    iput-object v1, v0, Leby;->b:Ljava/nio/ByteBuffer;

    .line 488
    iget-object v0, p0, Lebf;->j:Leby;

    iget-object v0, v0, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 492
    :cond_2
    iget-boolean v0, p0, Lebf;->B:Z

    if-eqz v0, :cond_4

    .line 494
    const/4 v0, -0x3

    .line 511
    :cond_3
    :goto_1
    const/4 v1, -0x2

    if-ne v0, v1, :cond_7

    .line 512
    const/4 v0, 0x0

    goto :goto_0

    .line 498
    :cond_4
    iget v0, p0, Lebf;->x:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 499
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lebf;->n:Lebv;

    iget-object v0, v0, Lebv;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 500
    iget-object v0, p0, Lebf;->n:Lebv;

    iget-object v0, v0, Lebv;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 501
    iget-object v2, p0, Lebf;->j:Leby;

    iget-object v2, v2, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 499
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 503
    :cond_5
    const/4 v0, 0x2

    iput v0, p0, Lebf;->x:I

    .line 505
    :cond_6
    iget-object v0, p0, Lebf;->i:Lebz;

    iget v1, p0, Lebf;->y:I

    iget-wide v2, p0, Lebf;->D:J

    iget-object v4, p0, Lebf;->k:Lebw;

    iget-object v5, p0, Lebf;->j:Leby;

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lebz;->a(IJLebw;Leby;Z)I

    move-result v0

    .line 506
    if-eqz p1, :cond_3

    iget v1, p0, Lebf;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    const/4 v1, -0x2

    if-ne v0, v1, :cond_3

    .line 507
    const/4 v1, 0x2

    iput v1, p0, Lebf;->f:I

    goto :goto_1

    .line 514
    :cond_7
    const/4 v1, -0x5

    if-ne v0, v1, :cond_8

    .line 515
    invoke-direct {p0}, Lebf;->t()V

    .line 516
    const/4 v0, 0x1

    goto :goto_0

    .line 518
    :cond_8
    const/4 v1, -0x4

    if-ne v0, v1, :cond_a

    .line 519
    iget v0, p0, Lebf;->x:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    .line 522
    iget-object v0, p0, Lebf;->j:Leby;

    iget-object v0, v0, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 523
    const/4 v0, 0x1

    iput v0, p0, Lebf;->x:I

    .line 525
    :cond_9
    iget-object v0, p0, Lebf;->k:Lebw;

    invoke-virtual {p0, v0}, Lebf;->a(Lebw;)V

    .line 526
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 528
    :cond_a
    const/4 v1, -0x1

    if-ne v0, v1, :cond_c

    .line 529
    iget v0, p0, Lebf;->x:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    .line 533
    iget-object v0, p0, Lebf;->j:Leby;

    iget-object v0, v0, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 534
    const/4 v0, 0x1

    iput v0, p0, Lebf;->x:I

    .line 536
    :cond_b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lebf;->z:Z

    .line 538
    :try_start_0
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    iget v1, p0, Lebf;->t:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 539
    const/4 v0, -0x1

    iput v0, p0, Lebf;->t:I
    :try_end_0
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 540
    :catch_0
    move-exception v0

    .line 541
    invoke-direct {p0, v0}, Lebf;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 542
    new-instance v1, Leat;

    invoke-direct {v1, v0}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 546
    :cond_c
    iget-boolean v0, p0, Lebf;->C:Z

    if-eqz v0, :cond_f

    .line 549
    iget-object v0, p0, Lebf;->j:Leby;

    iget v0, v0, Leby;->d:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_e

    .line 550
    iget-object v0, p0, Lebf;->j:Leby;

    iget-object v0, v0, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 551
    iget v0, p0, Lebf;->x:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    .line 554
    const/4 v0, 0x1

    iput v0, p0, Lebf;->x:I

    .line 556
    :cond_d
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 558
    :cond_e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lebf;->C:Z

    .line 560
    :cond_f
    iget-object v0, p0, Lebf;->j:Leby;

    iget v0, v0, Leby;->d:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    .line 561
    :goto_3
    iget-boolean v1, p0, Lebf;->v:Z

    if-eqz v1, :cond_13

    iget-object v1, p0, Lebf;->a:Ledv;

    invoke-interface {v1}, Ledv;->b()I

    move-result v1

    if-nez v1, :cond_11

    new-instance v0, Leat;

    iget-object v1, p0, Lebf;->a:Ledv;

    invoke-interface {v1}, Ledv;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 560
    :cond_10
    const/4 v0, 0x0

    goto :goto_3

    .line 561
    :cond_11
    const/4 v2, 0x4

    if-eq v1, v2, :cond_13

    if-nez v0, :cond_12

    iget-boolean v1, p0, Lebf;->h:Z

    if-nez v1, :cond_13

    :cond_12
    const/4 v1, 0x1

    :goto_4
    iput-boolean v1, p0, Lebf;->B:Z

    .line 562
    iget-boolean v1, p0, Lebf;->B:Z

    if-eqz v1, :cond_14

    .line 563
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 561
    :cond_13
    const/4 v1, 0x0

    goto :goto_4

    .line 566
    :cond_14
    :try_start_1
    iget-object v1, p0, Lebf;->j:Leby;

    iget-object v1, v1, Leby;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 567
    iget-object v1, p0, Lebf;->j:Leby;

    iget v1, v1, Leby;->c:I

    sub-int v1, v3, v1

    .line 568
    iget-object v2, p0, Lebf;->j:Leby;

    iget-wide v4, v2, Leby;->e:J

    .line 569
    iget-object v2, p0, Lebf;->j:Leby;

    iget-boolean v2, v2, Leby;->f:Z

    if-eqz v2, :cond_15

    .line 570
    iget-object v2, p0, Lebf;->l:Ljava/util/HashSet;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 572
    :cond_15
    if-eqz v0, :cond_18

    .line 573
    iget-object v0, p0, Lebf;->j:Leby;

    iget-object v0, v0, Leby;->a:Leao;

    iget-object v3, v0, Leao;->g:Landroid/media/MediaCodec$CryptoInfo;

    if-nez v1, :cond_16

    .line 575
    :goto_5
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    iget v1, p0, Lebf;->t:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueSecureInputBuffer(IILandroid/media/MediaCodec$CryptoInfo;JI)V

    .line 579
    :goto_6
    const/4 v0, -0x1

    iput v0, p0, Lebf;->t:I

    .line 580
    const/4 v0, 0x0

    iput v0, p0, Lebf;->x:I

    .line 585
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 573
    :cond_16
    iget-object v0, v3, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    if-nez v0, :cond_17

    const/4 v0, 0x1

    new-array v0, v0, [I

    iput-object v0, v3, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    :cond_17
    iget-object v0, v3, Landroid/media/MediaCodec$CryptoInfo;->numBytesOfClearData:[I

    const/4 v2, 0x0

    aget v6, v0, v2

    add-int/2addr v1, v6

    aput v1, v0, v2
    :try_end_1
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_5

    .line 581
    :catch_1
    move-exception v0

    .line 582
    invoke-direct {p0, v0}, Lebf;->a(Landroid/media/MediaCodec$CryptoException;)V

    .line 583
    new-instance v1, Leat;

    invoke-direct {v1, v0}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 577
    :cond_18
    :try_start_2
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    iget v1, p0, Lebf;->t:I

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_2
    .catch Landroid/media/MediaCodec$CryptoException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_6
.end method

.method private t()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 451
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lebf;->s:J

    .line 452
    iput v2, p0, Lebf;->t:I

    .line 453
    iput v2, p0, Lebf;->u:I

    .line 454
    iput-boolean v3, p0, Lebf;->C:Z

    .line 455
    iget-object v0, p0, Lebf;->l:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 458
    sget v0, Legz;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    .line 459
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    .line 464
    :goto_0
    iget-boolean v0, p0, Lebf;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebf;->n:Lebv;

    if-eqz v0, :cond_0

    .line 467
    iput v3, p0, Lebf;->x:I

    .line 469
    :cond_0
    return-void

    .line 461
    :cond_1
    invoke-virtual {p0}, Lebf;->k()V

    .line 462
    invoke-virtual {p0}, Lebf;->i()V

    goto :goto_0
.end method


# virtual methods
.method protected a(J)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 372
    iput-wide p1, p0, Lebf;->D:J

    .line 373
    iget-object v0, p0, Lebf;->i:Lebz;

    invoke-interface {v0, p1, p2}, Lebz;->b(J)V

    .line 374
    iput v1, p0, Lebf;->f:I

    .line 375
    iput-boolean v1, p0, Lebf;->z:Z

    .line 376
    iput-boolean v1, p0, Lebf;->A:Z

    .line 377
    iput-boolean v1, p0, Lebf;->B:Z

    .line 378
    return-void
.end method

.method protected final a(JJ)V
    .locals 11

    .prologue
    .line 393
    :try_start_0
    iget-object v0, p0, Lebf;->i:Lebz;

    invoke-interface {v0, p1, p2}, Lebz;->a(J)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lebf;->f:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lebf;->f:I

    .line 396
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lebf;->i:Lebz;

    iget v1, p0, Lebf;->y:I

    iget-wide v2, p0, Lebf;->D:J

    iget-object v4, p0, Lebf;->k:Lebw;

    iget-object v5, p0, Lebf;->j:Leby;

    const/4 v6, 0x1

    invoke-interface/range {v0 .. v6}, Lebz;->a(IJLebw;Leby;Z)I

    move-result v0

    const/4 v1, -0x5

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lebf;->t()V

    .line 397
    :cond_0
    iget-object v0, p0, Lebf;->n:Lebv;

    if-nez v0, :cond_4

    .line 398
    iget-object v0, p0, Lebf;->i:Lebz;

    iget v1, p0, Lebf;->y:I

    iget-wide v2, p0, Lebf;->D:J

    iget-object v4, p0, Lebf;->k:Lebw;

    iget-object v5, p0, Lebf;->j:Leby;

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lebz;->a(IJLebw;Leby;Z)I

    move-result v0

    const/4 v1, -0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lebf;->k:Lebw;

    invoke-virtual {p0, v0}, Lebf;->a(Lebw;)V

    .line 412
    :cond_1
    :goto_1
    iget-object v0, p0, Lebf;->b:Lean;

    invoke-virtual {v0}, Lean;->a()V

    .line 415
    return-void

    .line 393
    :cond_2
    iget v0, p0, Lebf;->f:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 399
    :cond_4
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lebf;->j()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Lecc;->g:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 400
    iget-object v0, p0, Lebf;->j:Leby;

    const/4 v1, 0x0

    iput-object v1, v0, Leby;->b:Ljava/nio/ByteBuffer;

    const/4 v0, -0x3

    :cond_5
    :goto_2
    const/4 v1, -0x3

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lebf;->D:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_1

    iget-object v0, p0, Lebf;->i:Lebz;

    iget v1, p0, Lebf;->y:I

    iget-wide v2, p0, Lebf;->D:J

    iget-object v4, p0, Lebf;->k:Lebw;

    iget-object v5, p0, Lebf;->j:Leby;

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lebz;->a(IJLebw;Leby;Z)I

    move-result v0

    const/4 v1, -0x3

    if-ne v0, v1, :cond_6

    iget-object v1, p0, Lebf;->j:Leby;

    iget-boolean v1, v1, Leby;->f:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lebf;->j:Leby;

    iget-wide v2, v1, Leby;->e:J

    iput-wide v2, p0, Lebf;->D:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 413
    :catch_0
    move-exception v0

    .line 414
    new-instance v1, Leat;

    invoke-direct {v1, v0}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 400
    :cond_6
    const/4 v1, -0x4

    if-ne v0, v1, :cond_5

    :try_start_1
    iget-object v1, p0, Lebf;->k:Lebw;

    invoke-virtual {p0, v1}, Lebf;->a(Lebw;)V

    goto :goto_2

    .line 402
    :cond_7
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lebf;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 403
    invoke-virtual {p0}, Lebf;->i()V

    .line 405
    :cond_8
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_1

    .line 406
    :cond_9
    iget-boolean v0, p0, Lebf;->A:Z

    if-nez v0, :cond_e

    iget v0, p0, Lebf;->u:I

    if-gez v0, :cond_a

    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    iget-object v1, p0, Lebf;->m:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v0

    iput v0, p0, Lebf;->u:I

    :cond_a
    iget v0, p0, Lebf;->u:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_c

    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    invoke-virtual {p0, v0}, Lebf;->a(Landroid/media/MediaFormat;)V

    iget-object v0, p0, Lebf;->b:Lean;

    iget v1, v0, Lean;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lean;->c:I

    const/4 v0, 0x1

    :goto_3
    if-nez v0, :cond_9

    .line 407
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lebf;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 408
    :cond_b
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lebf;->a(Z)Z

    move-result v0

    if-nez v0, :cond_b

    goto/16 :goto_1

    .line 406
    :cond_c
    iget v0, p0, Lebf;->u:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_d

    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lebf;->r:[Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lebf;->b:Lean;

    iget v1, v0, Lean;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lean;->d:I

    const/4 v0, 0x1

    goto :goto_3

    :cond_d
    iget v0, p0, Lebf;->u:I

    if-ltz v0, :cond_e

    iget-object v0, p0, Lebf;->m:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    iput-boolean v0, p0, Lebf;->A:Z

    :cond_e
    const/4 v0, 0x0

    goto :goto_3

    :cond_f
    iget-object v0, p0, Lebf;->l:Ljava/util/HashSet;

    iget-object v1, p0, Lebf;->m:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v2, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    iget-object v6, p0, Lebf;->e:Landroid/media/MediaCodec;

    iget-object v0, p0, Lebf;->r:[Ljava/nio/ByteBuffer;

    iget v1, p0, Lebf;->u:I

    aget-object v7, v0, v1

    iget-object v8, p0, Lebf;->m:Landroid/media/MediaCodec$BufferInfo;

    iget v9, p0, Lebf;->u:I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v1 .. v10}, Lebf;->a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z

    move-result v0

    if-eqz v0, :cond_e

    if-eqz v10, :cond_10

    iget-object v0, p0, Lebf;->l:Ljava/util/HashSet;

    iget-object v1, p0, Lebf;->m:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v2, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    :goto_4
    const/4 v0, -0x1

    iput v0, p0, Lebf;->u:I

    const/4 v0, 0x1

    goto :goto_3

    :cond_10
    iget-object v0, p0, Lebf;->m:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v0, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v0, p0, Lebf;->D:J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method protected a(JZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 223
    iget-object v0, p0, Lebf;->i:Lebz;

    iget v1, p0, Lebf;->y:I

    invoke-interface {v0, v1, p1, p2}, Lebz;->a(IJ)V

    .line 224
    iput v2, p0, Lebf;->f:I

    .line 225
    iput-boolean v2, p0, Lebf;->z:Z

    .line 226
    iput-boolean v2, p0, Lebf;->A:Z

    .line 227
    iput-boolean v2, p0, Lebf;->B:Z

    .line 228
    iput-wide p1, p0, Lebf;->D:J

    .line 229
    return-void
.end method

.method public a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 2

    .prologue
    .line 237
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 238
    return-void
.end method

.method protected a(Landroid/media/MediaFormat;)V
    .locals 0

    .prologue
    .line 647
    return-void
.end method

.method protected a(Lebw;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 626
    iget-object v0, p0, Lebf;->n:Lebv;

    .line 627
    iget-object v1, p1, Lebw;->a:Lebv;

    iput-object v1, p0, Lebf;->n:Lebv;

    .line 628
    iget-object v1, p1, Lebw;->b:Ljava/util/Map;

    iput-object v1, p0, Lebf;->o:Ljava/util/Map;

    .line 629
    iget-object v1, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lebf;->e:Landroid/media/MediaCodec;

    iget-boolean v2, p0, Lebf;->p:Z

    iget-object v3, p0, Lebf;->n:Lebv;

    invoke-virtual {p0, v1, v2, v0, v3}, Lebf;->a(Landroid/media/MediaCodec;ZLebv;Lebv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    iput-boolean v4, p0, Lebf;->w:Z

    .line 631
    iput v4, p0, Lebf;->x:I

    .line 636
    :goto_0
    return-void

    .line 633
    :cond_0
    invoke-virtual {p0}, Lebf;->k()V

    .line 634
    invoke-virtual {p0}, Lebf;->i()V

    goto :goto_0
.end method

.method protected abstract a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
.end method

.method public a(Landroid/media/MediaCodec;ZLebv;Lebv;)Z
    .locals 1

    .prologue
    .line 666
    const/4 v0, 0x0

    return v0
.end method

.method protected a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 388
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 671
    iget-boolean v0, p0, Lebf;->A:Z

    return v0
.end method

.method protected e()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 676
    iget-object v2, p0, Lebf;->n:Lebv;

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lebf;->B:Z

    if-nez v2, :cond_2

    iget v2, p0, Lebf;->f:I

    if-nez v2, :cond_0

    iget v2, p0, Lebf;->u:I

    if-gez v2, :cond_0

    .line 677
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lebf;->s:J

    const-wide/16 v6, 0x3e8

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected f()J
    .locals 2

    .prologue
    .line 355
    iget-wide v0, p0, Lebf;->D:J

    return-wide v0
.end method

.method public g()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 308
    iput-object v0, p0, Lebf;->n:Lebv;

    .line 309
    iput-object v0, p0, Lebf;->o:Ljava/util/Map;

    .line 311
    :try_start_0
    invoke-virtual {p0}, Lebf;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 314
    :try_start_1
    iget-boolean v0, p0, Lebf;->v:Z

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lebf;->a:Ledv;

    invoke-interface {v0}, Ledv;->a()V

    .line 316
    const/4 v0, 0x0

    iput-boolean v0, p0, Lebf;->v:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 319
    :cond_0
    iget-object v0, p0, Lebf;->i:Lebz;

    iget v1, p0, Lebf;->y:I

    invoke-interface {v0, v1}, Lebz;->b(I)V

    .line 322
    return-void

    .line 319
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lebf;->i:Lebz;

    iget v2, p0, Lebf;->y:I

    invoke-interface {v1, v2}, Lebz;->b(I)V

    throw v0

    .line 321
    :catchall_1
    move-exception v0

    .line 314
    :try_start_2
    iget-boolean v1, p0, Lebf;->v:Z

    if-eqz v1, :cond_1

    .line 315
    iget-object v1, p0, Lebf;->a:Ledv;

    invoke-interface {v1}, Ledv;->a()V

    .line 316
    const/4 v1, 0x0

    iput-boolean v1, p0, Lebf;->v:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 319
    :cond_1
    iget-object v1, p0, Lebf;->i:Lebz;

    iget v2, p0, Lebf;->y:I

    invoke-interface {v1, v2}, Lebz;->b(I)V

    throw v0

    :catchall_2
    move-exception v0

    iget-object v1, p0, Lebf;->i:Lebz;

    iget v2, p0, Lebf;->y:I

    invoke-interface {v1, v2}, Lebz;->b(I)V

    throw v0
.end method

.method protected final h()I
    .locals 2

    .prologue
    .line 188
    :try_start_0
    iget-object v0, p0, Lebf;->i:Lebz;

    invoke-interface {v0}, Lebz;->a()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lebf;->i:Lebz;

    invoke-interface {v1}, Lebz;->b()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 200
    iget-object v1, p0, Lebf;->i:Lebz;

    invoke-interface {v1, v0}, Lebz;->a(I)Lecb;

    move-result-object v1

    iget-object v1, v1, Lecb;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lebf;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    iput v0, p0, Lebf;->y:I

    .line 202
    const/4 v0, 0x1

    .line 206
    :goto_1
    return v0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    new-instance v1, Leat;

    invoke-direct {v1, v0}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 196
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected final i()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v1, 0x0

    const/4 v9, -0x1

    .line 242
    invoke-virtual {p0}, Lebf;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lebf;->n:Lebv;

    iget-object v3, v0, Lebv;->a:Ljava/lang/String;

    .line 247
    const/4 v0, 0x0

    .line 249
    iget-object v2, p0, Lebf;->o:Ljava/util/Map;

    if-eqz v2, :cond_b

    .line 250
    iget-object v0, p0, Lebf;->a:Ledv;

    if-nez v0, :cond_2

    .line 251
    new-instance v0, Leat;

    const-string v1, "Media requires a DrmSessionManager"

    invoke-direct {v0, v1}, Leat;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_2
    iget-boolean v0, p0, Lebf;->v:Z

    if-nez v0, :cond_3

    .line 254
    iget-object v0, p0, Lebf;->a:Ledv;

    iget-object v2, p0, Lebf;->o:Ljava/util/Map;

    invoke-interface {v0, v2, v3}, Ledv;->a(Ljava/util/Map;Ljava/lang/String;)V

    .line 255
    iput-boolean v10, p0, Lebf;->v:Z

    .line 257
    :cond_3
    iget-object v0, p0, Lebf;->a:Ledv;

    invoke-interface {v0}, Ledv;->b()I

    move-result v0

    .line 258
    if-nez v0, :cond_4

    .line 259
    new-instance v0, Leat;

    iget-object v1, p0, Lebf;->a:Ledv;

    invoke-interface {v1}, Ledv;->d()Ljava/lang/Exception;

    move-result-object v1

    invoke-direct {v0, v1}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 260
    :cond_4
    if-eq v0, v11, :cond_5

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    .line 262
    :cond_5
    iget-object v0, p0, Lebf;->a:Ledv;

    invoke-interface {v0}, Ledv;->c()Landroid/media/MediaCrypto;

    move-result-object v2

    .line 263
    iget-object v0, p0, Lebf;->a:Ledv;

    invoke-interface {v0, v3}, Ledv;->a(Ljava/lang/String;)Z

    move-result v0

    .line 270
    :goto_1
    invoke-static {v3, v0}, Lebk;->a(Ljava/lang/String;Z)Leap;

    move-result-object v0

    .line 272
    iget-object v3, v0, Leap;->a:Ljava/lang/String;

    .line 273
    iget-boolean v0, v0, Leap;->b:Z

    iput-boolean v0, p0, Lebf;->p:Z

    .line 275
    :try_start_0
    invoke-static {v3}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    .line 276
    iget-object v4, p0, Lebf;->e:Landroid/media/MediaCodec;

    iget-object v5, p0, Lebf;->n:Lebv;

    iget-object v0, v5, Lebv;->l:Landroid/media/MediaFormat;

    if-nez v0, :cond_8

    new-instance v6, Landroid/media/MediaFormat;

    invoke-direct {v6}, Landroid/media/MediaFormat;-><init>()V

    const-string v0, "mime"

    iget-object v7, v5, Lebv;->a:Ljava/lang/String;

    invoke-virtual {v6, v0, v7}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "max-input-size"

    iget v7, v5, Lebv;->b:I

    invoke-static {v6, v0, v7}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "width"

    iget v7, v5, Lebv;->c:I

    invoke-static {v6, v0, v7}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "height"

    iget v7, v5, Lebv;->d:I

    invoke-static {v6, v0, v7}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "channel-count"

    iget v7, v5, Lebv;->f:I

    invoke-static {v6, v0, v7}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "sample-rate"

    iget v7, v5, Lebv;->g:I

    invoke-static {v6, v0, v7}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "bitrate"

    iget v7, v5, Lebv;->h:I

    invoke-static {v6, v0, v7}, Lebv;->a(Landroid/media/MediaFormat;Ljava/lang/String;I)V

    const-string v0, "com.google.android.videos.pixelWidthHeightRatio"

    iget v7, v5, Lebv;->e:F

    const/high16 v8, -0x40800000    # -1.0f

    cmpl-float v8, v7, v8

    if-eqz v8, :cond_6

    invoke-virtual {v6, v0, v7}, Landroid/media/MediaFormat;->setFloat(Ljava/lang/String;F)V

    :cond_6
    :goto_2
    iget-object v0, v5, Lebv;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v7, 0xf

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "csd-"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v0, v5, Lebv;->k:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/media/MediaFormat;->setByteBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_7
    invoke-virtual {v5, v6}, Lebv;->a(Landroid/media/MediaFormat;)V

    iput-object v6, v5, Lebv;->l:Landroid/media/MediaFormat;

    :cond_8
    iget-object v0, v5, Lebv;->l:Landroid/media/MediaFormat;

    invoke-virtual {p0, v4, v0, v2}, Lebf;->a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;Landroid/media/MediaCrypto;)V

    .line 277
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 278
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lebf;->q:[Ljava/nio/ByteBuffer;

    .line 279
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lebf;->r:[Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    iget v0, p0, Lecc;->g:I

    if-ne v0, v11, :cond_a

    .line 287
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    :goto_3
    iput-wide v0, p0, Lebf;->s:J

    .line 288
    iput v9, p0, Lebf;->t:I

    .line 289
    iput v9, p0, Lebf;->u:I

    .line 290
    iput-boolean v10, p0, Lebf;->C:Z

    .line 291
    iget-object v0, p0, Lebf;->b:Lean;

    iget v1, v0, Lean;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lean;->a:I

    goto/16 :goto_0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    new-instance v1, Lebi;

    iget-object v2, p0, Lebf;->n:Lebv;

    invoke-direct {v1, v3, v2, v0}, Lebi;-><init>(Ljava/lang/String;Lebv;Ljava/lang/Throwable;)V

    .line 283
    iget-object v0, p0, Lebf;->d:Landroid/os/Handler;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lebf;->c:Lebj;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lebf;->d:Landroid/os/Handler;

    new-instance v2, Lebg;

    invoke-direct {v2, p0, v1}, Lebg;-><init>(Lebf;Lebi;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 284
    :cond_9
    new-instance v0, Leat;

    invoke-direct {v0, v1}, Leat;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 287
    :cond_a
    const-wide/16 v0, -0x1

    goto :goto_3

    :cond_b
    move-object v2, v0

    move v0, v1

    goto/16 :goto_1
.end method

.method protected j()Z
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-nez v0, :cond_0

    iget-object v0, p0, Lebf;->n:Lebv;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final k()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 326
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lebf;->s:J

    .line 327
    iput v4, p0, Lebf;->t:I

    .line 328
    iput v4, p0, Lebf;->u:I

    .line 329
    iget-object v0, p0, Lebf;->l:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 330
    iput-object v2, p0, Lebf;->q:[Ljava/nio/ByteBuffer;

    .line 331
    iput-object v2, p0, Lebf;->r:[Ljava/nio/ByteBuffer;

    .line 332
    iput-boolean v3, p0, Lebf;->w:Z

    .line 333
    iput-boolean v3, p0, Lebf;->p:Z

    .line 334
    iput v3, p0, Lebf;->x:I

    .line 335
    iget-object v0, p0, Lebf;->b:Lean;

    iget v1, v0, Lean;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lean;->b:I

    .line 337
    :try_start_0
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 340
    :try_start_1
    iget-object v0, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342
    iput-object v2, p0, Lebf;->e:Landroid/media/MediaCodec;

    .line 346
    :cond_0
    return-void

    .line 342
    :catchall_0
    move-exception v0

    iput-object v2, p0, Lebf;->e:Landroid/media/MediaCodec;

    throw v0

    .line 344
    :catchall_1
    move-exception v0

    .line 340
    :try_start_2
    iget-object v1, p0, Lebf;->e:Landroid/media/MediaCodec;

    invoke-virtual {v1}, Landroid/media/MediaCodec;->release()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 342
    iput-object v2, p0, Lebf;->e:Landroid/media/MediaCodec;

    throw v0

    :catchall_2
    move-exception v0

    iput-object v2, p0, Lebf;->e:Landroid/media/MediaCodec;

    throw v0
.end method

.method protected final l()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lebf;->i:Lebz;

    invoke-interface {v0}, Lebz;->d()V

    .line 351
    return-void
.end method

.method protected final m()J
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lebf;->i:Lebz;

    iget v1, p0, Lebf;->y:I

    invoke-interface {v0, v1}, Lebz;->a(I)Lecb;

    move-result-object v0

    iget-wide v0, v0, Lecb;->b:J

    return-wide v0
.end method

.method protected final n()J
    .locals 4

    .prologue
    .line 365
    iget-object v0, p0, Lebf;->i:Lebz;

    invoke-interface {v0}, Lebz;->c()J

    move-result-wide v0

    .line 366
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x3

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 367
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0}, Lebf;->f()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method
