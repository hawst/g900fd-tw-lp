.class public final Lbso;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Lcyc;

.field public final c:Lfxe;

.field public final d:Lfeb;

.field public final e:Lgnd;

.field public final f:Levn;

.field public final g:Lfus;

.field public final h:Leyp;

.field public final i:Lexd;

.field public final j:Lglm;

.field public final k:Lbjx;

.field public final l:Lcwg;

.field public final m:Ljava/lang/String;

.field public final n:Lbty;

.field public final o:Lbka;

.field public final p:Lbjq;

.field public final q:Lbyg;

.field public final r:Lbrz;

.field public final s:Lfrz;

.field public t:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field public u:Landroid/widget/ListView;

.field public v:Lfsi;

.field public w:Leue;

.field final x:Ljava/util/Set;

.field y:Ljava/lang/String;

.field public z:Lbsr;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcyc;Lfus;Lfxe;Lfeb;Lgnd;Levn;Leyp;Lexd;Lglm;Lbjx;Lcwg;Lbty;Lbka;Lbjq;Lbyg;Lbrz;Lfrz;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbso;->a:Landroid/app/Activity;

    .line 104
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyc;

    iput-object v0, p0, Lbso;->b:Lcyc;

    .line 105
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbso;->c:Lfxe;

    .line 106
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfeb;

    iput-object v0, p0, Lbso;->d:Lfeb;

    .line 107
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfus;

    iput-object v0, p0, Lbso;->g:Lfus;

    .line 108
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgnd;

    iput-object v0, p0, Lbso;->e:Lgnd;

    .line 109
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lbso;->f:Levn;

    .line 110
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbso;->h:Leyp;

    .line 111
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lbso;->i:Lexd;

    .line 112
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglm;

    iput-object v0, p0, Lbso;->j:Lglm;

    .line 113
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjx;

    iput-object v0, p0, Lbso;->k:Lbjx;

    .line 114
    invoke-static {p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwg;

    iput-object v0, p0, Lbso;->l:Lcwg;

    .line 115
    invoke-static {p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbty;

    iput-object v0, p0, Lbso;->n:Lbty;

    .line 116
    invoke-static {p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbka;

    iput-object v0, p0, Lbso;->o:Lbka;

    .line 118
    invoke-static/range {p15 .. p15}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjq;

    iput-object v0, p0, Lbso;->p:Lbjq;

    .line 119
    invoke-static/range {p16 .. p16}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyg;

    iput-object v0, p0, Lbso;->q:Lbyg;

    .line 120
    invoke-static/range {p17 .. p17}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrz;

    iput-object v0, p0, Lbso;->r:Lbrz;

    .line 122
    invoke-static/range {p18 .. p18}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrz;

    iput-object v0, p0, Lbso;->s:Lfrz;

    .line 123
    invoke-static/range {p19 .. p19}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbso;->m:Ljava/lang/String;

    .line 124
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbso;->x:Ljava/util/Set;

    .line 125
    const-string v0, ""

    iput-object v0, p0, Lbso;->y:Ljava/lang/String;

    .line 126
    return-void
.end method

.method private handleOfflinePlaylistSyncEvent(Lbls;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 229
    iget-object v0, p0, Lbso;->m:Ljava/lang/String;

    iget-object v1, p1, Lbls;->a:Lglw;

    iget-object v1, v1, Lglw;->a:Lgbu;

    iget-object v1, v1, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    invoke-virtual {p0}, Lbso;->a()V

    .line 232
    :cond_0
    return-void
.end method

.method private handleOfflineVideoDeleteEvent(Lbmb;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lbso;->x:Ljava/util/Set;

    iget-object v1, p1, Lbmb;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {p0}, Lbso;->a()V

    .line 224
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 208
    iget-object v0, p0, Lbso;->w:Leue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbso;->w:Leue;

    iget-boolean v0, v0, Leue;->a:Z

    if-nez v0, :cond_0

    .line 209
    iget-object v0, p0, Lbso;->w:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 211
    :cond_0
    iget-object v0, p0, Lbso;->t:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 212
    new-instance v0, Lbsq;

    invoke-direct {v0, p0}, Lbsq;-><init>(Lbso;)V

    .line 213
    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbso;->w:Leue;

    .line 214
    iget-object v0, p0, Lbso;->e:Lgnd;

    iget-object v1, p0, Lbso;->m:Ljava/lang/String;

    iget-object v2, p0, Lbso;->a:Landroid/app/Activity;

    iget-object v3, p0, Lbso;->w:Leue;

    .line 215
    invoke-static {v2, v3}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    .line 214
    invoke-interface {v0, v1, v2}, Lgnd;->a(Ljava/lang/String;Leuc;)V

    .line 216
    return-void
.end method
