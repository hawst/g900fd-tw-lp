.class public abstract Lbhz;
.super Lkp;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

.field private f:Lari;

.field private g:Letc;

.field private h:Lcal;

.field private i:Lezf;

.field private j:Lbxu;

.field public m:Lfus;

.field public n:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lkp;-><init>()V

    .line 48
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    const/16 v2, 0x11

    const/4 v1, 0x0

    .line 360
    invoke-static {p0}, Lejf;->a(Landroid/content/Context;)I

    move-result v0

    .line 361
    packed-switch v0, :pswitch_data_0

    .line 382
    invoke-static {v0, p0, v2}, Lejf;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    .line 384
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 385
    new-instance v1, Lbib;

    invoke-direct {v1, p0}, Lbib;-><init>(Lbhz;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 391
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 394
    :goto_0
    :pswitch_0
    return-void

    .line 367
    :pswitch_1
    invoke-static {v0, p0, v2}, Lejf;->a(ILandroid/app/Activity;I)Landroid/app/Dialog;

    move-result-object v0

    .line 369
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 370
    new-instance v1, Lbia;

    invoke-direct {v1, p0}, Lbia;-><init>(Lbhz;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 377
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 361
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final B()Lbxu;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lbhz;->j:Lbxu;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lbxu;

    invoke-direct {v0}, Lbxu;-><init>()V

    iput-object v0, p0, Lbhz;->j:Lbxu;

    .line 138
    :cond_0
    iget-object v0, p0, Lbhz;->j:Lbxu;

    return-object v0
.end method

.method public final C()Lcal;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lbhz;->h:Lcal;

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lcal;

    invoke-direct {v0, p0}, Lcal;-><init>(Lbhz;)V

    iput-object v0, p0, Lbhz;->h:Lcal;

    .line 204
    :cond_0
    iget-object v0, p0, Lbhz;->h:Lcal;

    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    return-object v0
.end method

.method public a_(I)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 234
    packed-switch p1, :pswitch_data_0

    .line 240
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 236
    :pswitch_1
    iget-object v0, p0, Lbhz;->f:Lari;

    invoke-virtual {v0}, Lari;->m()Lctk;

    move-result-object v0

    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Leyv;

    invoke-direct {v1, p0}, Leyv;-><init>(Landroid/content/Context;)V

    const v2, 0x7f090116

    invoke-virtual {v1, v2}, Leyv;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090117

    new-instance v3, Lctq;

    invoke-direct {v3, v0, p0}, Lctq;-><init>(Lctk;Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lctp;

    invoke-direct {v2, v0}, Lctp;-><init>(Lctk;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 238
    :pswitch_2
    iget-object v0, p0, Lbhz;->f:Lari;

    invoke-virtual {v0}, Lari;->m()Lctk;

    move-result-object v1

    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ldoe;

    iget-object v2, v1, Lctk;->a:Lfxe;

    iget-object v3, v1, Lctk;->c:Leyp;

    iget-object v4, v1, Lctk;->b:Lgck;

    iget-object v5, v1, Lctk;->d:Lctu;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ldoe;-><init>(Landroid/app/Activity;Lfxe;Leyp;Lgck;Lctu;)V

    goto :goto_0

    .line 234
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Lkp;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method public j()V
    .locals 0

    .prologue
    .line 170
    return-void
.end method

.method public k()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 173
    invoke-virtual {p0}, Lbhz;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 174
    const-string v1, "ancestor_classname"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 175
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 177
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 178
    const/high16 v0, 0x24000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 179
    invoke-virtual {p0}, Lbhz;->finish()V

    .line 180
    invoke-virtual {p0, v1}, Lbhz;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    return v2

    .line 182
    :catch_0
    move-exception v0

    .line 183
    const-string v1, "Target Activity class for Up event not found"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 187
    :cond_0
    iget-object v0, p0, Lbhz;->m:Lfus;

    invoke-virtual {v0}, Lfus;->c()V

    goto :goto_0
.end method

.method public l()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 343
    invoke-virtual {p0}, Lbhz;->d()Lkm;

    move-result-object v0

    .line 344
    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {v0, v1}, Lkm;->e(Z)V

    .line 347
    invoke-virtual {v0, v1}, Lkm;->c(Z)V

    .line 349
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, -0x1

    .line 246
    iget-object v0, p0, Lbhz;->n:Landroid/util/SparseArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbhz;->n:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lbhz;->n:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbic;

    invoke-interface {v0, p1, p2, p3}, Lbic;->a(IILandroid/content/Intent;)Z

    .line 249
    iget-object v0, p0, Lbhz;->n:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbhz;->n:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 264
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    const/16 v0, 0x11

    if-ne p1, v0, :cond_2

    .line 253
    invoke-direct {p0}, Lbhz;->f()V

    goto :goto_0

    .line 256
    :cond_2
    iget-object v0, p0, Lbhz;->f:Lari;

    invoke-virtual {v0}, Lari;->aO()Lcub;

    move-result-object v2

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_0

    .line 260
    invoke-static {p0, p1, p2, p3}, Ldol;->a(Landroid/app/Activity;IILandroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 263
    invoke-super {p0, p1, p2, p3}, Lkp;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 256
    :pswitch_1
    if-eq p2, v4, :cond_3

    invoke-virtual {v2}, Lcub;->a()V

    :goto_2
    move v0, v1

    goto :goto_1

    :cond_3
    if-nez p3, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Could not get account choice result."

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcub;->a(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_4
    const-string v0, "exception"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    if-eqz v0, :cond_5

    invoke-virtual {v2, v0}, Lcub;->a(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_5
    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Error fetching account name."

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcub;->a(Ljava/lang/Exception;)V

    goto :goto_2

    :cond_6
    invoke-virtual {v2, p0, v0}, Lcub;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_2
    iget-object v0, v2, Lcub;->k:Ljava/lang/String;

    const/4 v3, 0x0

    iput-object v3, v2, Lcub;->k:Ljava/lang/String;

    if-eq p2, v4, :cond_7

    invoke-virtual {v2}, Lcub;->a()V

    :goto_3
    move v0, v1

    goto :goto_1

    :cond_7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v3, "Auth recovery without a saved account name."

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcub;->a(Ljava/lang/Exception;)V

    goto :goto_3

    :cond_8
    invoke-virtual {v2, p0, v0}, Lcub;->a(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x387
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 325
    invoke-super {p0, p1}, Lkp;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 326
    invoke-virtual {p0}, Lbhz;->l()V

    .line 327
    invoke-virtual {p0}, Lbhz;->B()Lbxu;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbxu;->a(Z)V

    .line 328
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 80
    invoke-direct {p0}, Lbhz;->f()V

    .line 82
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lbhz;->setVolumeControlStream(I)V

    .line 84
    invoke-virtual {p0}, Lbhz;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lbhz;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 85
    iget-object v0, p0, Lbhz;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lbhz;->f:Lari;

    .line 86
    iget-object v0, p0, Lbhz;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lbhz;->g:Letc;

    .line 87
    iget-object v0, p0, Lbhz;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 89
    iget-object v0, p0, Lbhz;->g:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    .line 90
    new-instance v0, Lfus;

    iget-object v1, p0, Lbhz;->f:Lari;

    invoke-virtual {v1}, Lari;->o()Lglm;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lfus;-><init>(Landroid/app/Activity;Lglm;)V

    iput-object v0, p0, Lbhz;->m:Lfus;

    .line 92
    iget-object v0, p0, Lbhz;->f:Lari;

    invoke-virtual {v0}, Lari;->aN()Lezf;

    move-result-object v0

    iput-object v0, p0, Lbhz;->i:Lezf;

    .line 94
    invoke-super {p0, p1}, Lkp;->onCreate(Landroid/os/Bundle;)V

    .line 95
    return-void
.end method

.method protected final onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 214
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lbhz;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 223
    invoke-virtual {p0}, Lbhz;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x0

    .line 226
    :goto_0
    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p0, p1, p2}, Lbhz;->a(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lbhz;->a_(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 149
    invoke-super {p0, p1}, Lkp;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 150
    invoke-virtual {p0}, Lbhz;->C()Lcal;

    move-result-object v0

    invoke-virtual {p0}, Lbhz;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcal;->a(Landroid/view/Menu;Landroid/view/MenuInflater;)Z

    move-result v0

    .line 151
    invoke-virtual {p0}, Lbhz;->B()Lbxu;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lbxu;->a(Z)V

    .line 152
    invoke-virtual {p0}, Lbhz;->j()V

    .line 153
    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 163
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 164
    invoke-virtual {p0}, Lbhz;->k()Z

    move-result v0

    .line 166
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lbhz;->C()Lcal;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcal;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 99
    invoke-super {p0, p1}, Lkp;->onPostCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lbhz;->b()V

    .line 101
    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 158
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 111
    invoke-super {p0}, Lkp;->onResume()V

    .line 112
    invoke-virtual {p0}, Lbhz;->l()V

    .line 113
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    return v0
.end method

.method public onStart()V
    .locals 6

    .prologue
    .line 105
    invoke-super {p0}, Lkp;->onStart()V

    .line 106
    iget-object v0, p0, Lbhz;->e:Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    invoke-virtual {v0}, Lari;->J()Lcok;

    move-result-object v0

    invoke-virtual {p0}, Lbhz;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcok;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dev_retention_intercepted_url"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_0
    iget-object v1, v0, Lcok;->a:Landroid/content/SharedPreferences;

    const-string v2, "dev_retention_last_ping_time_ms"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcok;->a(J)V

    .line 107
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lbhz;->i:Lezf;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lbhz;->i:Lezf;

    invoke-virtual {v0}, Lezf;->a()V

    .line 335
    :cond_0
    invoke-super {p0}, Lkp;->onUserInteraction()V

    .line 336
    return-void
.end method
