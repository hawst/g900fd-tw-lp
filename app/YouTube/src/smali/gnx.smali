.class public Lgnx;
.super Lexx;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lgju;

.field private final d:Lexd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lgnx;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgnx;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lead;Lgju;Lexd;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lexx;-><init>(Lead;)V

    .line 37
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgju;

    iput-object v0, p0, Lgnx;->c:Lgju;

    .line 38
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lgnx;->d:Lexd;

    .line 39
    return-void
.end method

.method public static a(J)Lead;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Lead;

    invoke-direct {v0}, Lead;-><init>()V

    sget-object v1, Lgnx;->b:Ljava/lang/String;

    .line 47
    invoke-virtual {v0, v1}, Lead;->a(Ljava/lang/String;)Lead;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    .line 48
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v2, p0

    invoke-virtual {v0, v2, v3}, Lead;->a(J)Lead;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x258

    .line 49
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lead;->b(J)Lead;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lgnx;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lgnx;->d:Lexd;

    invoke-interface {v0}, Lexd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "Flushing offline queue."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lgnx;->c:Lgju;

    invoke-interface {v0}, Lgju;->a()V

    .line 61
    :goto_0
    return-void

    .line 59
    :cond_0
    const-string v0, "Not flushing offline queue because we have no network."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
