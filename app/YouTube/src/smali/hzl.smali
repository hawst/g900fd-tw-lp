.class public final Lhzl;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lhzr;

.field private c:Lhzo;

.field private d:Lhgz;

.field private e:Lhxf;

.field private f:Lhzq;

.field private g:[Lhzn;

.field private h:Z

.field private i:[Lhzp;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lhzl;->a:Ljava/lang/String;

    iput-object v1, p0, Lhzl;->b:Lhzr;

    iput-object v1, p0, Lhzl;->c:Lhzo;

    iput-object v1, p0, Lhzl;->d:Lhgz;

    iput-object v1, p0, Lhzl;->e:Lhxf;

    iput-object v1, p0, Lhzl;->f:Lhzq;

    sget-object v0, Lhzn;->a:[Lhzn;

    iput-object v0, p0, Lhzl;->g:[Lhzn;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lhzl;->h:Z

    sget-object v0, Lhzp;->a:[Lhzp;

    iput-object v0, p0, Lhzl;->i:[Lhzp;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhzl;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    iget-object v2, p0, Lhzl;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhzl;->b:Lhzr;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhzl;->b:Lhzr;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhzl;->c:Lhzo;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhzl;->c:Lhzo;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhzl;->d:Lhgz;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhzl;->d:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhzl;->e:Lhxf;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhzl;->e:Lhxf;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhzl;->f:Lhzq;

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lhzl;->f:Lhzq;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhzl;->g:[Lhzn;

    if-eqz v2, :cond_6

    iget-object v3, p0, Lhzl;->g:[Lhzn;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    if-eqz v5, :cond_5

    const/4 v6, 0x7

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_6
    iget-boolean v2, p0, Lhzl;->h:Z

    if-eqz v2, :cond_7

    const/16 v2, 0x8

    iget-boolean v3, p0, Lhzl;->h:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lhzl;->i:[Lhzp;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lhzl;->i:[Lhzp;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_9

    aget-object v4, v2, v1

    if-eqz v4, :cond_8

    const/16 v5, 0x9

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_9
    iget-object v1, p0, Lhzl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhzl;->J:I

    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhzl;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhzl;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhzl;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhzl;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhzl;->b:Lhzr;

    if-nez v0, :cond_2

    new-instance v0, Lhzr;

    invoke-direct {v0}, Lhzr;-><init>()V

    iput-object v0, p0, Lhzl;->b:Lhzr;

    :cond_2
    iget-object v0, p0, Lhzl;->b:Lhzr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhzl;->c:Lhzo;

    if-nez v0, :cond_3

    new-instance v0, Lhzo;

    invoke-direct {v0}, Lhzo;-><init>()V

    iput-object v0, p0, Lhzl;->c:Lhzo;

    :cond_3
    iget-object v0, p0, Lhzl;->c:Lhzo;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhzl;->d:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzl;->d:Lhgz;

    :cond_4
    iget-object v0, p0, Lhzl;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhzl;->e:Lhxf;

    if-nez v0, :cond_5

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhzl;->e:Lhxf;

    :cond_5
    iget-object v0, p0, Lhzl;->e:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhzl;->f:Lhzq;

    if-nez v0, :cond_6

    new-instance v0, Lhzq;

    invoke-direct {v0}, Lhzq;-><init>()V

    iput-object v0, p0, Lhzl;->f:Lhzq;

    :cond_6
    iget-object v0, p0, Lhzl;->f:Lhzq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhzl;->g:[Lhzn;

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhzn;

    iget-object v3, p0, Lhzl;->g:[Lhzn;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lhzl;->g:[Lhzn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    iput-object v2, p0, Lhzl;->g:[Lhzn;

    :goto_2
    iget-object v2, p0, Lhzl;->g:[Lhzn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lhzl;->g:[Lhzn;

    new-instance v3, Lhzn;

    invoke-direct {v3}, Lhzn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzl;->g:[Lhzn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    iget-object v0, p0, Lhzl;->g:[Lhzn;

    array-length v0, v0

    goto :goto_1

    :cond_9
    iget-object v2, p0, Lhzl;->g:[Lhzn;

    new-instance v3, Lhzn;

    invoke-direct {v3}, Lhzn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzl;->g:[Lhzn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhzl;->h:Z

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhzl;->i:[Lhzp;

    if-nez v0, :cond_b

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhzp;

    iget-object v3, p0, Lhzl;->i:[Lhzp;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lhzl;->i:[Lhzp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    iput-object v2, p0, Lhzl;->i:[Lhzp;

    :goto_4
    iget-object v2, p0, Lhzl;->i:[Lhzp;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    iget-object v2, p0, Lhzl;->i:[Lhzp;

    new-instance v3, Lhzp;

    invoke-direct {v3}, Lhzp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzl;->i:[Lhzp;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lhzl;->i:[Lhzp;

    array-length v0, v0

    goto :goto_3

    :cond_c
    iget-object v2, p0, Lhzl;->i:[Lhzp;

    new-instance v3, Lhzp;

    invoke-direct {v3}, Lhzp;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzl;->i:[Lhzp;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhzl;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lhzl;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lhzl;->b:Lhzr;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhzl;->b:Lhzr;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Lhzl;->c:Lhzo;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhzl;->c:Lhzo;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v1, p0, Lhzl;->d:Lhgz;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhzl;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v1, p0, Lhzl;->e:Lhxf;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhzl;->e:Lhxf;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v1, p0, Lhzl;->f:Lhzq;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lhzl;->f:Lhzq;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v1, p0, Lhzl;->g:[Lhzn;

    if-eqz v1, :cond_7

    iget-object v2, p0, Lhzl;->g:[Lhzn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    if-eqz v4, :cond_6

    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_7
    iget-boolean v1, p0, Lhzl;->h:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x8

    iget-boolean v2, p0, Lhzl;->h:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    :cond_8
    iget-object v1, p0, Lhzl;->i:[Lhzp;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lhzl;->i:[Lhzp;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_a

    aget-object v3, v1, v0

    if-eqz v3, :cond_9

    const/16 v4, 0x9

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    iget-object v0, p0, Lhzl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhzl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhzl;

    iget-object v2, p0, Lhzl;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhzl;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhzl;->b:Lhzr;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhzl;->b:Lhzr;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhzl;->c:Lhzo;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhzl;->c:Lhzo;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhzl;->d:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhzl;->d:Lhgz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhzl;->e:Lhxf;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhzl;->e:Lhxf;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhzl;->f:Lhzq;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhzl;->f:Lhzq;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhzl;->g:[Lhzn;

    iget-object v3, p1, Lhzl;->g:[Lhzn;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhzl;->h:Z

    iget-boolean v3, p1, Lhzl;->h:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhzl;->i:[Lhzp;

    iget-object v3, p1, Lhzl;->i:[Lhzp;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhzl;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhzl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhzl;->a:Ljava/lang/String;

    iget-object v3, p1, Lhzl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhzl;->b:Lhzr;

    iget-object v3, p1, Lhzl;->b:Lhzr;

    invoke-virtual {v2, v3}, Lhzr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhzl;->c:Lhzo;

    iget-object v3, p1, Lhzl;->c:Lhzo;

    invoke-virtual {v2, v3}, Lhzo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhzl;->d:Lhgz;

    iget-object v3, p1, Lhzl;->d:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhzl;->e:Lhxf;

    iget-object v3, p1, Lhzl;->e:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhzl;->f:Lhzq;

    iget-object v3, p1, Lhzl;->f:Lhzq;

    invoke-virtual {v2, v3}, Lhzq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhzl;->I:Ljava/util/List;

    iget-object v3, p1, Lhzl;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzl;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzl;->b:Lhzr;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzl;->c:Lhzo;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzl;->d:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzl;->e:Lhxf;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzl;->f:Lhzq;

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    iget-object v2, p0, Lhzl;->g:[Lhzn;

    if-nez v2, :cond_8

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-boolean v0, p0, Lhzl;->h:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_6
    add-int/2addr v0, v2

    iget-object v2, p0, Lhzl;->i:[Lhzp;

    if-nez v2, :cond_b

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhzl;->I:Ljava/util/List;

    if-nez v2, :cond_d

    :goto_7
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Lhzl;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhzl;->b:Lhzr;

    invoke-virtual {v0}, Lhzr;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lhzl;->c:Lhzo;

    invoke-virtual {v0}, Lhzo;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhzl;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lhzl;->e:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhzl;->f:Lhzq;

    invoke-virtual {v0}, Lhzq;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_8
    move v2, v0

    move v0, v1

    :goto_8
    iget-object v3, p0, Lhzl;->g:[Lhzn;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhzl;->g:[Lhzn;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_9
    iget-object v2, p0, Lhzl;->g:[Lhzn;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhzn;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_a
    const/4 v0, 0x2

    goto :goto_6

    :cond_b
    move v2, v0

    move v0, v1

    :goto_a
    iget-object v3, p0, Lhzl;->i:[Lhzp;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhzl;->i:[Lhzp;

    aget-object v2, v2, v0

    if-nez v2, :cond_c

    move v2, v1

    :goto_b
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_c
    iget-object v2, p0, Lhzl;->i:[Lhzp;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhzp;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_d
    iget-object v1, p0, Lhzl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_7
.end method
