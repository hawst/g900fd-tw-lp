.class public final Lhib;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhib;


# instance fields
.field public b:Lhic;

.field public c:Lhie;

.field public d:Lhhx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64610
    const/4 v0, 0x0

    new-array v0, v0, [Lhib;

    sput-object v0, Lhib;->a:[Lhib;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64611
    invoke-direct {p0}, Lidf;-><init>()V

    .line 64614
    iput-object v0, p0, Lhib;->b:Lhic;

    .line 64617
    iput-object v0, p0, Lhib;->c:Lhie;

    .line 64620
    iput-object v0, p0, Lhib;->d:Lhhx;

    .line 64611
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 64669
    const/4 v0, 0x0

    .line 64670
    iget-object v1, p0, Lhib;->b:Lhic;

    if-eqz v1, :cond_0

    .line 64671
    const v0, 0x2cb7264

    iget-object v1, p0, Lhib;->b:Lhic;

    .line 64672
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 64674
    :cond_0
    iget-object v1, p0, Lhib;->c:Lhie;

    if-eqz v1, :cond_1

    .line 64675
    const v1, 0x2d0d90a

    iget-object v2, p0, Lhib;->c:Lhie;

    .line 64676
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64678
    :cond_1
    iget-object v1, p0, Lhib;->d:Lhhx;

    if-eqz v1, :cond_2

    .line 64679
    const v1, 0x2d9b8c1

    iget-object v2, p0, Lhib;->d:Lhhx;

    .line 64680
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64682
    :cond_2
    iget-object v1, p0, Lhib;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 64683
    iput v0, p0, Lhib;->J:I

    .line 64684
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 64607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhib;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhib;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhib;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhib;->b:Lhic;

    if-nez v0, :cond_2

    new-instance v0, Lhic;

    invoke-direct {v0}, Lhic;-><init>()V

    iput-object v0, p0, Lhib;->b:Lhic;

    :cond_2
    iget-object v0, p0, Lhib;->b:Lhic;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhib;->c:Lhie;

    if-nez v0, :cond_3

    new-instance v0, Lhie;

    invoke-direct {v0}, Lhie;-><init>()V

    iput-object v0, p0, Lhib;->c:Lhie;

    :cond_3
    iget-object v0, p0, Lhib;->c:Lhie;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhib;->d:Lhhx;

    if-nez v0, :cond_4

    new-instance v0, Lhhx;

    invoke-direct {v0}, Lhhx;-><init>()V

    iput-object v0, p0, Lhib;->d:Lhhx;

    :cond_4
    iget-object v0, p0, Lhib;->d:Lhhx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x165b9322 -> :sswitch_1
        0x1686c852 -> :sswitch_2
        0x16cdc60a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 64654
    iget-object v0, p0, Lhib;->b:Lhic;

    if-eqz v0, :cond_0

    .line 64655
    const v0, 0x2cb7264

    iget-object v1, p0, Lhib;->b:Lhic;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64657
    :cond_0
    iget-object v0, p0, Lhib;->c:Lhie;

    if-eqz v0, :cond_1

    .line 64658
    const v0, 0x2d0d90a

    iget-object v1, p0, Lhib;->c:Lhie;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64660
    :cond_1
    iget-object v0, p0, Lhib;->d:Lhhx;

    if-eqz v0, :cond_2

    .line 64661
    const v0, 0x2d9b8c1

    iget-object v1, p0, Lhib;->d:Lhhx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 64663
    :cond_2
    iget-object v0, p0, Lhib;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 64665
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64633
    if-ne p1, p0, :cond_1

    .line 64639
    :cond_0
    :goto_0
    return v0

    .line 64634
    :cond_1
    instance-of v2, p1, Lhib;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 64635
    :cond_2
    check-cast p1, Lhib;

    .line 64636
    iget-object v2, p0, Lhib;->b:Lhic;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhib;->b:Lhic;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhib;->c:Lhie;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhib;->c:Lhie;

    if-nez v2, :cond_3

    .line 64637
    :goto_2
    iget-object v2, p0, Lhib;->d:Lhhx;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhib;->d:Lhhx;

    if-nez v2, :cond_3

    .line 64638
    :goto_3
    iget-object v2, p0, Lhib;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhib;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 64639
    goto :goto_0

    .line 64636
    :cond_4
    iget-object v2, p0, Lhib;->b:Lhic;

    iget-object v3, p1, Lhib;->b:Lhic;

    invoke-virtual {v2, v3}, Lhic;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhib;->c:Lhie;

    iget-object v3, p1, Lhib;->c:Lhie;

    .line 64637
    invoke-virtual {v2, v3}, Lhie;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhib;->d:Lhhx;

    iget-object v3, p1, Lhib;->d:Lhhx;

    .line 64638
    invoke-virtual {v2, v3}, Lhhx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhib;->I:Ljava/util/List;

    iget-object v3, p1, Lhib;->I:Ljava/util/List;

    .line 64639
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 64643
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 64645
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhib;->b:Lhic;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 64646
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhib;->c:Lhie;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 64647
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhib;->d:Lhhx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 64648
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhib;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 64649
    return v0

    .line 64645
    :cond_0
    iget-object v0, p0, Lhib;->b:Lhic;

    invoke-virtual {v0}, Lhic;->hashCode()I

    move-result v0

    goto :goto_0

    .line 64646
    :cond_1
    iget-object v0, p0, Lhib;->c:Lhie;

    invoke-virtual {v0}, Lhie;->hashCode()I

    move-result v0

    goto :goto_1

    .line 64647
    :cond_2
    iget-object v0, p0, Lhib;->d:Lhhx;

    invoke-virtual {v0}, Lhhx;->hashCode()I

    move-result v0

    goto :goto_2

    .line 64648
    :cond_3
    iget-object v1, p0, Lhib;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
