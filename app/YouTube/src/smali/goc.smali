.class public final Lgoc;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static d:Ljava/util/regex/Pattern;


# instance fields
.field public a:Lgod;

.field private final b:Lgod;

.field private c:Lgod;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "(?:\\[|%5B)([a-zA-Z_:]+)(?:\\]|%5D)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lgoc;->d:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Lgod;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgod;

    iput-object v0, p0, Lgoc;->b:Lgod;

    .line 52
    sget-object v0, Lgod;->h:Lgod;

    iput-object v0, p0, Lgoc;->a:Lgod;

    .line 53
    sget-object v0, Lgod;->h:Lgod;

    iput-object v0, p0, Lgoc;->c:Lgod;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lgod;)Landroid/net/Uri;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 92
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 93
    sget-object v0, Lgoc;->d:Ljava/util/regex/Pattern;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 94
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 95
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 96
    sget-object v0, Lgof;->a:Ljava/util/Map;

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eqz p2, :cond_2

    invoke-interface {p2, p1, v3}, Lgod;->a(Landroid/net/Uri;I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    :cond_1
    :goto_2
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/regex/Matcher;->appendReplacement(Ljava/lang/StringBuffer;Ljava/lang/String;)Ljava/util/regex/Matcher;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lgoc;->b:Lgod;

    invoke-interface {v0, p1, v3}, Lgod;->a(Landroid/net/Uri;I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lgoc;->c:Lgod;

    invoke-interface {v0, p1, v3}, Lgod;->a(Landroid/net/Uri;I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    if-nez v0, :cond_5

    iget-object v0, p0, Lgoc;->a:Lgod;

    invoke-interface {v0, p1, v3}, Lgod;->a(Landroid/net/Uri;I)Ljava/lang/String;

    move-result-object v0

    :cond_5
    if-nez v0, :cond_6

    sget-object v0, Lgof;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_6
    if-nez v0, :cond_1

    const-string v0, ""

    goto :goto_2

    .line 104
    :cond_7
    invoke-virtual {v2, v1}, Ljava/util/regex/Matcher;->appendTail(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    .line 106
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->E(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    new-instance v1, Lfax;

    const-string v2, "Failed to convert URI"

    invoke-direct {v1, v2, v0}, Lfax;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lgod;)V
    .locals 0

    .prologue
    .line 71
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lgoc;->c:Lgod;

    .line 72
    return-void

    .line 71
    :cond_0
    sget-object p1, Lgod;->h:Lgod;

    goto :goto_0
.end method
