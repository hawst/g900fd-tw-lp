.class public final Lhie;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[Lhif;

.field public c:Ljava/lang/String;

.field public d:[B

.field private e:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65083
    invoke-direct {p0}, Lidf;-><init>()V

    .line 65086
    const/4 v0, 0x0

    iput v0, p0, Lhie;->a:I

    .line 65089
    sget-object v0, Lhif;->a:[Lhif;

    iput-object v0, p0, Lhie;->b:[Lhif;

    .line 65092
    const-string v0, ""

    iput-object v0, p0, Lhie;->c:Ljava/lang/String;

    .line 65095
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhie;->d:[B

    .line 65098
    const/4 v0, 0x0

    iput-object v0, p0, Lhie;->e:Lhgz;

    .line 65083
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 65173
    .line 65174
    iget v0, p0, Lhie;->a:I

    if-eqz v0, :cond_5

    .line 65175
    const/4 v0, 0x2

    iget v2, p0, Lhie;->a:I

    .line 65176
    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 65178
    :goto_0
    iget-object v2, p0, Lhie;->b:[Lhif;

    if-eqz v2, :cond_1

    .line 65179
    iget-object v2, p0, Lhie;->b:[Lhif;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 65180
    if-eqz v4, :cond_0

    .line 65181
    const/4 v5, 0x4

    .line 65182
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 65179
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 65186
    :cond_1
    iget-object v1, p0, Lhie;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 65187
    const/4 v1, 0x5

    iget-object v2, p0, Lhie;->c:Ljava/lang/String;

    .line 65188
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65190
    :cond_2
    iget-object v1, p0, Lhie;->d:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 65191
    const/4 v1, 0x7

    iget-object v2, p0, Lhie;->d:[B

    .line 65192
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 65194
    :cond_3
    iget-object v1, p0, Lhie;->e:Lhgz;

    if-eqz v1, :cond_4

    .line 65195
    const/16 v1, 0x8

    iget-object v2, p0, Lhie;->e:Lhgz;

    .line 65196
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65198
    :cond_4
    iget-object v1, p0, Lhie;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65199
    iput v0, p0, Lhie;->J:I

    .line 65200
    return v0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 65079
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhie;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhie;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhie;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    :cond_2
    iput v0, p0, Lhie;->a:I

    goto :goto_0

    :cond_3
    iput v1, p0, Lhie;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhie;->b:[Lhif;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhif;

    iget-object v3, p0, Lhie;->b:[Lhif;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lhie;->b:[Lhif;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Lhie;->b:[Lhif;

    :goto_2
    iget-object v2, p0, Lhie;->b:[Lhif;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lhie;->b:[Lhif;

    new-instance v3, Lhif;

    invoke-direct {v3}, Lhif;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhie;->b:[Lhif;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhie;->b:[Lhif;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lhie;->b:[Lhif;

    new-instance v3, Lhif;

    invoke-direct {v3}, Lhif;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhie;->b:[Lhif;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhie;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhie;->d:[B

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lhie;->e:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhie;->e:Lhgz;

    :cond_7
    iget-object v0, p0, Lhie;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x3a -> :sswitch_4
        0x42 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 65148
    iget v0, p0, Lhie;->a:I

    if-eqz v0, :cond_0

    .line 65149
    const/4 v0, 0x2

    iget v1, p0, Lhie;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 65151
    :cond_0
    iget-object v0, p0, Lhie;->b:[Lhif;

    if-eqz v0, :cond_2

    .line 65152
    iget-object v1, p0, Lhie;->b:[Lhif;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 65153
    if-eqz v3, :cond_1

    .line 65154
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 65152
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65158
    :cond_2
    iget-object v0, p0, Lhie;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 65159
    const/4 v0, 0x5

    iget-object v1, p0, Lhie;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 65161
    :cond_3
    iget-object v0, p0, Lhie;->d:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 65162
    const/4 v0, 0x7

    iget-object v1, p0, Lhie;->d:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 65164
    :cond_4
    iget-object v0, p0, Lhie;->e:Lhgz;

    if-eqz v0, :cond_5

    .line 65165
    const/16 v0, 0x8

    iget-object v1, p0, Lhie;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 65167
    :cond_5
    iget-object v0, p0, Lhie;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 65169
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65113
    if-ne p1, p0, :cond_1

    .line 65121
    :cond_0
    :goto_0
    return v0

    .line 65114
    :cond_1
    instance-of v2, p1, Lhie;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 65115
    :cond_2
    check-cast p1, Lhie;

    .line 65116
    iget v2, p0, Lhie;->a:I

    iget v3, p1, Lhie;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhie;->b:[Lhif;

    iget-object v3, p1, Lhie;->b:[Lhif;

    .line 65117
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhie;->c:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhie;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 65118
    :goto_1
    iget-object v2, p0, Lhie;->d:[B

    iget-object v3, p1, Lhie;->d:[B

    .line 65119
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhie;->e:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhie;->e:Lhgz;

    if-nez v2, :cond_3

    .line 65120
    :goto_2
    iget-object v2, p0, Lhie;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhie;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 65121
    goto :goto_0

    .line 65117
    :cond_4
    iget-object v2, p0, Lhie;->c:Ljava/lang/String;

    iget-object v3, p1, Lhie;->c:Ljava/lang/String;

    .line 65118
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 65119
    :cond_5
    iget-object v2, p0, Lhie;->e:Lhgz;

    iget-object v3, p1, Lhie;->e:Lhgz;

    .line 65120
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhie;->I:Ljava/util/List;

    iget-object v3, p1, Lhie;->I:Ljava/util/List;

    .line 65121
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 65125
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 65127
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhie;->a:I

    add-int/2addr v0, v2

    .line 65128
    iget-object v2, p0, Lhie;->b:[Lhif;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 65134
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhie;->c:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 65135
    iget-object v2, p0, Lhie;->d:[B

    if-nez v2, :cond_5

    mul-int/lit8 v2, v0, 0x1f

    .line 65141
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhie;->e:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 65142
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhie;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_2
    add-int/2addr v0, v1

    .line 65143
    return v0

    :cond_2
    move v2, v0

    move v0, v1

    .line 65130
    :goto_3
    iget-object v3, p0, Lhie;->b:[Lhif;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 65131
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhie;->b:[Lhif;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_4
    add-int/2addr v2, v3

    .line 65130
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 65131
    :cond_3
    iget-object v2, p0, Lhie;->b:[Lhif;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhif;->hashCode()I

    move-result v2

    goto :goto_4

    .line 65134
    :cond_4
    iget-object v0, p0, Lhie;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_5
    move v2, v0

    move v0, v1

    .line 65137
    :goto_5
    iget-object v3, p0, Lhie;->d:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 65138
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhie;->d:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 65137
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 65141
    :cond_6
    iget-object v0, p0, Lhie;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 65142
    :cond_7
    iget-object v1, p0, Lhie;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
