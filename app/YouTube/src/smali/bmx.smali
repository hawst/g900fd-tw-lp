.class public final Lbmx;
.super Lded;
.source "SourceFile"

# interfaces
.implements Lddx;


# instance fields
.field a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field b:Landroid/widget/Button;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/ProgressBar;

.field private e:Landroid/view/View;

.field private f:I

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 48
    const v1, 0x7f0400ed

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbmx;->c:Landroid/view/View;

    .line 49
    iget-object v0, p0, Lbmx;->c:Landroid/view/View;

    const v1, 0x7f0800a6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lbmx;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 50
    iget-object v0, p0, Lbmx;->c:Landroid/view/View;

    const v1, 0x7f0800de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbmx;->d:Landroid/widget/ProgressBar;

    .line 52
    iget-object v0, p0, Lbmx;->c:Landroid/view/View;

    const v1, 0x7f0802b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbmx;->b:Landroid/widget/Button;

    .line 53
    iget-object v0, p0, Lbmx;->c:Landroid/view/View;

    const v1, 0x7f080260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbmx;->e:Landroid/view/View;

    .line 55
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lbmx;->setBackgroundColor(I)V

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbmx;->a(I)V

    .line 57
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/16 v1, 0x8

    .line 121
    iget-object v2, p0, Lbmx;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget v0, p0, Lbmx;->f:I

    const/4 v3, 0x1

    if-eq v0, v3, :cond_2

    invoke-direct {p0}, Lbmx;->d()I

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setVisibility(I)V

    .line 122
    iget-object v2, p0, Lbmx;->d:Landroid/widget/ProgressBar;

    iget v0, p0, Lbmx;->f:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_3

    invoke-direct {p0}, Lbmx;->d()I

    move-result v0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 123
    iget-object v2, p0, Lbmx;->e:Landroid/view/View;

    iget v0, p0, Lbmx;->f:I

    const/4 v3, 0x4

    if-eq v0, v3, :cond_0

    iget v0, p0, Lbmx;->f:I

    if-ne v0, v4, :cond_4

    .line 125
    :cond_0
    invoke-direct {p0}, Lbmx;->d()I

    move-result v0

    .line 123
    :goto_2
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lbmx;->b:Landroid/widget/Button;

    iget v2, p0, Lbmx;->f:I

    if-ne v2, v4, :cond_1

    .line 128
    invoke-direct {p0}, Lbmx;->d()I

    move-result v1

    .line 127
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 130
    invoke-direct {p0}, Lbmx;->d()I

    move-result v0

    invoke-super {p0, v0}, Lded;->setVisibility(I)V

    .line 131
    return-void

    :cond_2
    move v0, v1

    .line 121
    goto :goto_0

    :cond_3
    move v0, v1

    .line 122
    goto :goto_1

    :cond_4
    move v0, v1

    .line 125
    goto :goto_2
.end method

.method private d()I
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lbmx;->g:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lbmx;->h:I

    goto :goto_0
.end method


# virtual methods
.method a(I)V
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lbmx;->f:I

    if-ne v0, p1, :cond_0

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    iput p1, p0, Lbmx;->f:I

    .line 117
    invoke-direct {p0}, Lbmx;->c()V

    goto :goto_0
.end method

.method public final c_(Z)V
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lbmx;->g:Z

    if-ne v0, p1, :cond_0

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    iput-boolean p1, p0, Lbmx;->g:Z

    .line 104
    invoke-direct {p0}, Lbmx;->c()V

    goto :goto_0
.end method

.method public final f_()Landroid/view/View;
    .locals 0

    .prologue
    .line 109
    return-object p0
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 86
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method

.method public final setVisibility(I)V
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lbmx;->h:I

    if-ne v0, p1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    iput p1, p0, Lbmx;->h:I

    .line 95
    invoke-direct {p0}, Lbmx;->c()V

    goto :goto_0
.end method
