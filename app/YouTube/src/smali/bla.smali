.class public final Lbla;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgnd;


# instance fields
.field final a:Lbjn;

.field final b:Ljava/util/concurrent/Executor;

.field final c:Ldjf;

.field final d:Lgit;

.field final e:Lezj;

.field final f:Lbjx;

.field final g:Lfad;

.field final h:Lblv;

.field final i:Lgml;

.field final j:Lgmt;

.field final k:Lgmu;

.field final l:Landroid/os/Handler;

.field final m:Lbmd;

.field final n:Ljava/util/Map;

.field final o:Lgmw;

.field volatile p:Lbjm;

.field private final q:Levn;

.field private final r:Lbkr;

.field private final s:Landroid/os/HandlerThread;

.field private final t:Lbkt;

.field private final u:Lbkj;

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Levn;Ldjf;Leyp;Lfxe;Lgot;Larh;Ljava/util/concurrent/Executor;Lezj;Lexn;Lfad;Lbjx;Layz;Lfep;Lgmw;Lgit;Lckv;)V
    .locals 9

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Levn;

    iput-object v2, p0, Lbla;->q:Levn;

    .line 358
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldjf;

    iput-object v2, p0, Lbla;->c:Ldjf;

    .line 359
    invoke-static/range {p7 .. p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    invoke-static/range {p12 .. p12}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfad;

    iput-object v2, p0, Lbla;->g:Lfad;

    .line 361
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbjx;

    iput-object v2, p0, Lbla;->f:Lbjx;

    .line 362
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    .line 363
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lezj;

    iput-object v2, p0, Lbla;->e:Lezj;

    .line 364
    invoke-static/range {p16 .. p16}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgmw;

    iput-object v2, p0, Lbla;->o:Lgmw;

    .line 365
    invoke-static/range {p17 .. p17}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgit;

    iput-object v2, p0, Lbla;->d:Lgit;

    .line 367
    new-instance v2, Lblv;

    invoke-direct {v2, p0}, Lblv;-><init>(Lbla;)V

    iput-object v2, p0, Lbla;->h:Lblv;

    .line 369
    new-instance v2, Lbjn;

    invoke-direct {v2, p1, p3, p5}, Lbjn;-><init>(Landroid/content/Context;Levn;Leyp;)V

    iput-object v2, p0, Lbla;->a:Lbjn;

    .line 374
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 375
    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    iput-object v2, p0, Lbla;->n:Ljava/util/Map;

    .line 376
    new-instance v2, Lbmd;

    invoke-direct {v2, p0}, Lbmd;-><init>(Lbla;)V

    iput-object v2, p0, Lbla;->m:Lbmd;

    .line 377
    new-instance v2, Lgmt;

    move-object v3, p1

    move-object/from16 v4, p17

    move-object v5, p5

    move-object/from16 v6, p7

    move-object/from16 v7, p11

    invoke-direct/range {v2 .. v7}, Lgmt;-><init>(Landroid/content/Context;Lgit;Leyp;Lgot;Lexn;)V

    iput-object v2, p0, Lbla;->j:Lgmt;

    .line 383
    new-instance v2, Lgmu;

    iget-object v3, p0, Lbla;->j:Lgmt;

    move-object/from16 v0, p13

    move-object/from16 v1, p11

    invoke-direct {v2, v3, v0, v1}, Lgmu;-><init>(Lgmt;Lbjx;Lexn;)V

    iput-object v2, p0, Lbla;->k:Lgmu;

    .line 388
    new-instance v2, Lgml;

    .line 390
    invoke-static/range {p17 .. p17}, Lbla;->a(Lgit;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbla;->j:Lgmt;

    iget-object v6, p0, Lbla;->k:Lgmu;

    new-instance v8, Lbll;

    invoke-direct {v8, p0}, Lbll;-><init>(Lbla;)V

    move-object v3, p1

    move-object/from16 v7, p9

    invoke-direct/range {v2 .. v8}, Lgml;-><init>(Landroid/content/Context;Ljava/lang/String;Lgmt;Lewi;Ljava/util/concurrent/Executor;Lgmm;)V

    iput-object v2, p0, Lbla;->i:Lgml;

    .line 396
    new-instance v2, Lbkr;

    iget-object v4, p0, Lbla;->i:Lgml;

    .line 400
    invoke-virtual/range {p8 .. p8}, Larh;->O()J

    move-result-wide v6

    move-object/from16 v3, p14

    move-object/from16 v5, p10

    invoke-direct/range {v2 .. v7}, Lbkr;-><init>(Layz;Lgml;Lezj;J)V

    iput-object v2, p0, Lbla;->r:Lbkr;

    .line 402
    new-instance v2, Landroid/os/HandlerThread;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v2, p0, Lbla;->s:Landroid/os/HandlerThread;

    .line 403
    iget-object v2, p0, Lbla;->s:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 404
    new-instance v2, Lbkt;

    iget-object v5, p0, Lbla;->j:Lgmt;

    move-object v3, p1

    move-object v4, p2

    move-object v6, p6

    move-object v7, p0

    invoke-direct/range {v2 .. v7}, Lbkt;-><init>(Landroid/content/Context;Landroid/telephony/TelephonyManager;Lgmt;Lfxe;Lbla;)V

    iput-object v2, p0, Lbla;->t:Lbkt;

    .line 411
    new-instance v2, Landroid/os/Handler;

    iget-object v3, p0, Lbla;->s:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, p0, Lbla;->t:Lbkt;

    invoke-direct {v2, v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lbla;->l:Landroid/os/Handler;

    .line 413
    new-instance v2, Lbkj;

    move-object/from16 v3, p17

    move-object v4, p0

    move-object/from16 v5, p15

    move-object/from16 v6, p10

    move-object/from16 v7, p8

    move-object/from16 v8, p18

    invoke-direct/range {v2 .. v8}, Lbkj;-><init>(Lgit;Lgnd;Lfep;Lezj;Lcyc;Lckv;)V

    iput-object v2, p0, Lbla;->u:Lbkj;

    .line 420
    return-void
.end method

.method static a(Lgit;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lgit;->c:Ljava/lang/String;

    invoke-static {v0}, Lbla;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 299
    const-string v0, "."

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "offline"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    const-string v3, "db"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ILgje;)V
    .locals 6

    .prologue
    .line 1136
    iget-object v0, p0, Lbla;->g:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    iget-object v1, p0, Lbla;->d:Lgit;

    .line 1137
    iget-object v1, v1, Lgit;->c:Ljava/lang/String;

    const/4 v3, 0x0

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    .line 1136
    invoke-virtual/range {v0 .. v5}, Ldmz;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILgje;)V

    .line 1138
    return-void
.end method


# virtual methods
.method final A(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "pudl event playlist "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sync_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 1268
    new-instance v0, Lblt;

    invoke-direct {v0, p1}, Lblt;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lbla;->a(Ljava/lang/Object;)V

    .line 1269
    return-void
.end method

.method final B(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1289
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1b

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "pudl event playlist "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " delete"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 1290
    new-instance v0, Lblp;

    invoke-direct {v0, p1}, Lblp;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lbla;->a(Ljava/lang/Object;)V

    .line 1291
    return-void
.end method

.method public final a(Ljava/lang/String;Lflj;[B)I
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 544
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 545
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    invoke-virtual {p0, p1}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v2

    .line 549
    if-eqz v2, :cond_3

    .line 551
    invoke-virtual {v2}, Lgmb;->j()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 552
    invoke-virtual {v2}, Lgmb;->k()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lgmb;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 553
    :cond_0
    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v2, p0, Lbla;->l:Landroid/os/Handler;

    const/4 v3, 0x6

    .line 554
    invoke-virtual {v2, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 553
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 576
    :goto_0
    return v0

    .line 559
    :cond_1
    iget-boolean v2, v2, Lgmb;->b:Z

    if-nez v2, :cond_2

    .line 560
    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v2, p0, Lbla;->l:Landroid/os/Handler;

    const/4 v3, 0x3

    .line 561
    invoke-virtual {v2, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 560
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 565
    goto :goto_0

    .line 569
    :cond_3
    iget-object v2, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v3, p0, Lbla;->l:Landroid/os/Handler;

    new-instance v4, Lblm;

    .line 574
    invoke-static {p2}, Lbjy;->a(Lflj;)Lbjy;

    move-result-object v5

    invoke-direct {v4, p1, v5, p3}, Lblm;-><init>(Ljava/lang/String;Lbjy;[B)V

    .line 570
    invoke-virtual {v3, v1, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 569
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 581
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 583
    if-eqz p1, :cond_4

    .line 586
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, p1}, Lgml;->e(Ljava/lang/String;)Lgbu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbla;->i:Lgml;

    .line 587
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->f(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 588
    :cond_0
    const/4 v0, 0x3

    .line 610
    :goto_0
    return v0

    .line 596
    :cond_1
    invoke-virtual {p0, p2}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 597
    if-eqz v0, :cond_2

    .line 598
    invoke-virtual {v0}, Lgmb;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 599
    invoke-virtual {v0}, Lgmb;->k()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lgmb;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 600
    :cond_2
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/4 v2, 0x6

    .line 601
    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 600
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 603
    const/4 v0, 0x1

    goto :goto_0

    .line 606
    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    .line 610
    :cond_4
    iget-object v0, p0, Lbla;->f:Lbjx;

    .line 612
    invoke-virtual {v0}, Lbjx;->b()Lbjy;

    move-result-object v0

    iget-object v0, v0, Lbjy;->c:Lflj;

    sget-object v1, Lfhy;->a:[B

    .line 610
    invoke-virtual {p0, p2, v0, v1}, Lbla;->a(Ljava/lang/String;Lflj;[B)I

    move-result v0

    goto :goto_0
.end method

.method final declared-synchronized a()V
    .locals 2

    .prologue
    .line 423
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbla;->v:Z

    .line 425
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0}, Lgml;->f()V

    .line 426
    new-instance v0, Lbjm;

    iget-object v1, p0, Lbla;->k:Lgmu;

    invoke-virtual {v1}, Lgmu;->i()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lbjm;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lbla;->p:Lbjm;

    .line 428
    iget-object v0, p0, Lbla;->o:Lgmw;

    invoke-interface {v0}, Lgmw;->a()V

    .line 429
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lbld;

    invoke-direct {v1, p0}, Lbld;-><init>(Lbla;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    monitor-exit p0

    return-void

    .line 423
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Leuc;)V
    .locals 2

    .prologue
    .line 720
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 721
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lble;

    invoke-direct {v1, p0, p1}, Lble;-><init>(Lbla;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 727
    return-void
.end method

.method final a(Lglw;I)V
    .locals 5

    .prologue
    .line 1273
    iget-object v0, p1, Lglw;->a:Lgbu;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1274
    invoke-virtual {p1}, Lglw;->a()I

    move-result v1

    iget-object v2, p1, Lglw;->a:Lgbu;

    iget v2, v2, Lgbu;->j:I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x36

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "pudl event playlist "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " progress: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1273
    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 1275
    if-nez p2, :cond_0

    .line 1282
    :goto_0
    return-void

    .line 1278
    :cond_0
    new-instance v1, Lblr;

    const/4 v0, 0x2

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {v1, p1, v0}, Lblr;-><init>(Lglw;Z)V

    invoke-virtual {p0, v1}, Lbla;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final a(Lgmb;)V
    .locals 6

    .prologue
    .line 1016
    iget-object v0, p1, Lgmb;->d:Lglz;

    .line 1017
    if-eqz v0, :cond_0

    .line 1018
    invoke-virtual {v0}, Lglz;->c()J

    move-result-wide v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    .line 1020
    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v4, p0, Lbla;->l:Landroid/os/Handler;

    const/16 v5, 0x9

    .line 1022
    iget-object v0, v0, Lglz;->a:Ljava/lang/String;

    .line 1021
    invoke-virtual {v4, v5, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1020
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1025
    :cond_0
    return-void
.end method

.method final declared-synchronized a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 443
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbla;->v:Z

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lbla;->q:Levn;

    invoke-virtual {v0, p1}, Levn;->c(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    :cond_0
    monitor-exit p0

    return-void

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 631
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 632
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblb;

    invoke-direct {v1, p0, p1, p2, p3}, Lblb;-><init>(Lbla;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 638
    return-void
.end method

.method final a(Ljava/lang/String;Lbjy;[B)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 882
    iget-object v2, p0, Lbla;->r:Lbkr;

    invoke-static {}, Lb;->b()V

    iget-object v1, v2, Lbkr;->a:Lgml;

    invoke-virtual {v1, p1}, Lgml;->h(Ljava/lang/String;)Lesq;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v2, p1, p2}, Lbkr;->a(Ljava/lang/String;Lbjy;)Lesq;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_3

    .line 883
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 885
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lgje;

    invoke-direct {v1}, Lgje;-><init>()V

    const-string v2, "stream_quality"

    invoke-virtual {p2}, Lbjy;->a()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lgje;->a(Ljava/lang/String;I)V

    const-string v2, "click_tracking_params"

    invoke-virtual {v1, v2, p3}, Lgje;->a(Ljava/lang/String;[B)V

    const-string v2, "video_id"

    invoke-virtual {v1, v2, v0}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "ad"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lgje;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lbla;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x64

    invoke-direct {p0, v0, v2, v1}, Lbla;->a(Ljava/lang/String;ILgje;)V

    .line 887
    :cond_2
    return-void

    .line 882
    :cond_3
    invoke-virtual {v1}, Lesq;->a()Less;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v3, v2, Lbkr;->a:Lgml;

    iget-object v4, v1, Less;->e:Ljava/lang/String;

    iget-object v3, v3, Lgml;->g:Lgmh;

    invoke-virtual {v3, p1, v4}, Lgmh;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v1}, Lbkr;->a(Less;)Lfoy;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, v1, Lfoy;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 758
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 759
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblg;

    invoke-direct {v1, p0, p2, p1}, Lblg;-><init>(Lbla;Leuc;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 765
    return-void
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;Lbjy;[B)V
    .locals 3

    .prologue
    .line 1109
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111
    new-instance v0, Lgje;

    invoke-direct {v0}, Lgje;-><init>()V

    .line 1112
    const-string v1, "stream_quality"

    invoke-virtual {p3}, Lbjy;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lgje;->a(Ljava/lang/String;I)V

    .line 1113
    const-string v1, "click_tracking_params"

    invoke-virtual {v0, v1, p4}, Lgje;->a(Ljava/lang/String;[B)V

    .line 1114
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p2}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1115
    if-eqz p1, :cond_0

    .line 1116
    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p1}, Lgje;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1118
    :cond_0
    invoke-virtual {p0, p2}, Lbla;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xc8

    invoke-direct {p0, v1, v2, v0}, Lbla;->a(Ljava/lang/String;ILgje;)V

    .line 1119
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 4

    .prologue
    .line 937
    invoke-static {}, Lb;->b()V

    .line 938
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 939
    iget-object v2, p0, Lbla;->i:Lgml;

    invoke-virtual {v2, v0}, Lgml;->k(Ljava/lang/String;)V

    .line 940
    iget-object v2, p0, Lbla;->i:Lgml;

    .line 941
    invoke-virtual {v2, v0}, Lgml;->b(Ljava/lang/String;)I

    move-result v2

    .line 943
    invoke-static {v2}, Lbjy;->a(I)Lbjy;

    move-result-object v2

    .line 944
    sget-object v3, Lfhy;->a:[B

    invoke-virtual {p0, v0, v2, v3}, Lbla;->a(Ljava/lang/String;Lbjy;[B)V

    goto :goto_0

    .line 949
    :cond_0
    return-void
.end method

.method public final a(Lgbu;)Z
    .locals 4

    .prologue
    .line 708
    iget-object v0, p1, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lbla;->c(Ljava/lang/String;)Lglw;

    move-result-object v0

    .line 709
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lglw;->a(Lgbu;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 710
    :cond_0
    const/4 v0, 0x0

    .line 715
    :goto_0
    return v0

    .line 713
    :cond_1
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/16 v2, 0xb

    iget-object v3, p1, Lgbu;->a:Ljava/lang/String;

    .line 714
    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 713
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 715
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Lglz;)Z
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 518
    invoke-static {}, Lb;->b()V

    .line 520
    iget-object v7, p1, Lglz;->a:Ljava/lang/String;

    .line 521
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, v7}, Lgml;->c(Ljava/lang/String;)Lfrl;

    move-result-object v5

    .line 522
    if-eqz v5, :cond_1

    .line 526
    :try_start_0
    iget-object v0, p1, Lglz;->b:Lflg;

    new-instance v1, Lhro;

    invoke-direct {v1}, Lhro;-><init>()V

    iget-object v2, v5, Lfrl;->a:Lhro;

    invoke-static {v2}, Lidh;->a(Lidh;)[B

    move-result-object v2

    invoke-static {v1, v2}, Lidh;->a(Lidh;[B)Lidh;

    new-instance v2, Lhpl;

    invoke-direct {v2}, Lhpl;-><init>()V

    iget-object v0, v0, Lflg;->a:Lhpl;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    invoke-static {v2, v0}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v0

    check-cast v0, Lhpl;

    iput-object v0, v1, Lhro;->i:Lhpl;

    new-instance v0, Lfrl;

    iget-wide v2, v5, Lfrl;->b:J

    iget-object v4, v5, Lfrl;->c:[B

    new-instance v8, Lfri;

    const/4 v9, 0x0

    new-array v9, v9, [Lfrj;

    invoke-direct {v8, v9}, Lfri;-><init>([Lfrj;)V

    iget-wide v10, v5, Lfrl;->b:J

    invoke-static {v8, v1, v10, v11}, Lfrl;->a(Lfri;Lhro;J)Lfrf;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lfrl;-><init>(Lhro;J[BLfrf;)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    iget-object v1, p0, Lbla;->i:Lgml;

    .line 531
    iget-wide v2, p1, Lglz;->d:J

    .line 530
    invoke-virtual {v1, v7, v0, v2, v3}, Lgml;->a(Ljava/lang/String;Lfrl;J)Z

    move-result v0

    .line 532
    if-eqz v0, :cond_0

    .line 533
    iget-object v1, p1, Lglz;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lbla;->y(Ljava/lang/String;)V

    :cond_0
    :goto_0
    move v6, v0

    .line 536
    :goto_1
    return v6

    .line 528
    :catch_0
    move-exception v0

    goto :goto_1

    :cond_1
    move v0, v6

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lflj;[B)I
    .locals 5

    .prologue
    .line 645
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 646
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 649
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, p1}, Lgml;->e(Ljava/lang/String;)Lgbu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 650
    const/4 v0, 0x2

    .line 660
    :goto_0
    return v0

    .line 653
    :cond_0
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/16 v2, 0xa

    new-instance v3, Lblm;

    .line 658
    invoke-static {p2}, Lbjy;->a(Lflj;)Lbjy;

    move-result-object v4

    invoke-direct {v3, p1, v4, p3}, Lblm;-><init>(Ljava/lang/String;Lbjy;[B)V

    .line 654
    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 653
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 660
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Lfoy;
    .locals 3

    .prologue
    .line 897
    invoke-static {}, Lb;->b()V

    .line 898
    iget-object v2, p0, Lbla;->i:Lgml;

    .line 899
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 898
    invoke-virtual {v2, v0, v1}, Lgml;->a(Ljava/lang/String;Ljava/lang/String;)Lfoy;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;J)Lgly;
    .locals 2

    .prologue
    .line 848
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 849
    iget-object v0, p0, Lbla;->i:Lgml;

    new-instance v1, Lblu;

    invoke-direct {v1, p0, p2, p3}, Lblu;-><init>(Lbla;J)V

    invoke-virtual {v0, p1, v1}, Lgml;->a(Ljava/lang/String;Lgni;)Lgly;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lgmb;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, p1}, Lgml;->d(Ljava/lang/String;)Lgmb;

    move-result-object v0

    return-object v0
.end method

.method final declared-synchronized b()V
    .locals 1

    .prologue
    .line 433
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lbla;->v:Z

    .line 435
    iget-object v0, p0, Lbla;->o:Lgmw;

    invoke-interface {v0}, Lgmw;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    monitor-exit p0

    return-void

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Leuc;)V
    .locals 2

    .prologue
    .line 731
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 732
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblf;

    invoke-direct {v1, p0, p1}, Lblf;-><init>(Lbla;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 738
    return-void
.end method

.method final b(Lgbu;)V
    .locals 3

    .prologue
    .line 1218
    iget-object v0, p1, Lgbu;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "pudl event playlist "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " add placeholder"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 1219
    new-instance v0, Lglw;

    invoke-direct {v0, p1}, Lglw;-><init>(Lgbu;)V

    .line 1221
    iget-object v1, p0, Lbla;->n:Ljava/util/Map;

    iget-object v2, p1, Lgbu;->a:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1222
    new-instance v1, Lblq;

    invoke-direct {v1, v0}, Lblq;-><init>(Lglw;)V

    invoke-virtual {p0, v1}, Lbla;->a(Ljava/lang/Object;)V

    .line 1223
    return-void
.end method

.method public final b(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 781
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 782
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 783
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblh;

    invoke-direct {v1, p0, p1, p2}, Lblh;-><init>(Lbla;Ljava/lang/String;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 794
    return-void
.end method

.method public final c(Ljava/lang/String;)Lglw;
    .locals 3

    .prologue
    .line 486
    iget-object v0, p0, Lbla;->m:Lbmd;

    .line 487
    invoke-virtual {v0, p1}, Lbmd;->a(Ljava/lang/String;)Lbme;

    move-result-object v0

    .line 488
    if-eqz v0, :cond_0

    .line 489
    invoke-virtual {v0}, Lbme;->b()Lglw;

    move-result-object v0

    .line 500
    :goto_0
    return-object v0

    .line 491
    :cond_0
    iget-object v1, p0, Lbla;->n:Ljava/util/Map;

    monitor-enter v1

    .line 492
    :try_start_0
    iget-object v0, p0, Lbla;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglw;

    .line 493
    if-nez v0, :cond_1

    .line 494
    iget-object v2, p0, Lbla;->i:Lgml;

    invoke-virtual {v2, p1}, Lgml;->e(Ljava/lang/String;)Lgbu;

    move-result-object v2

    .line 495
    if-eqz v2, :cond_1

    .line 496
    new-instance v0, Lglw;

    invoke-direct {v0, v2}, Lglw;-><init>(Lgbu;)V

    .line 497
    iget-object v2, p0, Lbla;->n:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 501
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Ljava/lang/String;Leuc;)V
    .locals 2

    .prologue
    .line 857
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 858
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 859
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lbli;

    invoke-direct {v1, p0, p1, p2}, Lbli;-><init>(Lbla;Ljava/lang/String;Leuc;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 870
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 916
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 917
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 918
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblk;

    invoke-direct {v1, p0, p1, p2}, Lblk;-><init>(Lbla;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 925
    return-void
.end method

.method final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 439
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lbla;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 929
    invoke-static {}, Lb;->b()V

    .line 930
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 931
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 932
    iget-object v0, p0, Lbla;->i:Lgml;

    iget-object v0, v0, Lgml;->g:Lgmh;

    invoke-virtual {v0, p1, p2}, Lgmh;->b(Ljava/lang/String;Ljava/lang/String;)Lgmi;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Lgmi;->f:I

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lglz;
    .locals 1

    .prologue
    .line 506
    invoke-static {}, Lb;->b()V

    .line 507
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {v0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0, p1}, Lgnm;->a(Ljava/lang/String;)Lgno;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgno;->d()Lglz;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lgml;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lbla;->i:Lgml;

    return-object v0
.end method

.method public final e()Lgmt;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lbla;->j:Lgmt;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Ljava/util/List;
    .locals 5

    .prologue
    .line 512
    invoke-static {}, Lb;->b()V

    .line 513
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, v0, Lgml;->f:Lgnp;

    invoke-virtual {v1, p1}, Lgnp;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lgml;->b()Lgnm;

    move-result-object v2

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, v2, Lgnm;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgno;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgno;->d()Lglz;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method public final f()Lgmk;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lbla;->k:Lgmu;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 619
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 621
    invoke-virtual {p0, p1}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 622
    if-eqz v0, :cond_0

    .line 623
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/16 v2, 0xe

    .line 624
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 623
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 627
    :cond_0
    return-void
.end method

.method public final g()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0}, Lgml;->b()Lgnm;

    move-result-object v0

    invoke-virtual {v0}, Lgnm;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 665
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, p1}, Lgml;->d(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 666
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgmb;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 671
    :cond_0
    :goto_0
    return-void

    .line 669
    :cond_1
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/4 v2, 0x4

    .line 670
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 669
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 874
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 875
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 675
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, p1}, Lgml;->d(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 676
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgmb;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 681
    :cond_0
    :goto_0
    return-void

    .line 679
    :cond_1
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/4 v2, 0x5

    .line 680
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 679
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final handleSdCardMountChangedEvent(Lexp;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 451
    iget-object v0, p0, Lbla;->k:Lgmu;

    invoke-virtual {v0}, Lgmu;->h()V

    .line 452
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0}, Lgml;->f()V

    .line 453
    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 14

    .prologue
    .line 954
    iget-object v1, p0, Lbla;->r:Lbkr;

    iget-object v0, v1, Lbkr;->a:Lgml;

    iget-object v0, v0, Lgml;->g:Lgmh;

    invoke-virtual {v0}, Lgmh;->a()Ljava/util/List;

    move-result-object v0

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmj;

    iget-object v4, v0, Lgmj;->a:Lgmi;

    iget-object v4, v4, Lgmi;->a:Ljava/lang/String;

    new-instance v5, Lhps;

    invoke-direct {v5}, Lhps;-><init>()V

    sget-object v6, Lbks;->a:[I

    iget-object v7, v0, Lgmj;->a:Lgmi;

    iget-object v7, v7, Lgmi;->c:Lgnf;

    invoke-virtual {v7}, Lgnf;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :goto_1
    new-instance v6, Lhpt;

    invoke-direct {v6}, Lhpt;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [Lhps;

    const/4 v7, 0x0

    aput-object v5, v0, v7

    iput-object v0, v6, Lhpt;->b:[Lhps;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x0

    iput v0, v5, Lhps;->b:I

    const/4 v0, 0x0

    iput v0, v5, Lhps;->d:I

    goto :goto_1

    :pswitch_1
    const/4 v6, 0x2

    iput v6, v5, Lhps;->b:I

    const-wide/16 v6, 0x0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v9, v0, Lgmj;->a:Lgmi;

    iget-wide v10, v9, Lgmi;->d:J

    iget-object v9, v1, Lbkr;->b:Lezj;

    invoke-virtual {v9}, Lezj;->a()J

    move-result-wide v12

    sub-long/2addr v10, v12

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, v5, Lhps;->d:I

    const/4 v6, 0x0

    iget-object v7, v0, Lgmj;->a:Lgmi;

    iget v7, v7, Lgmi;->e:I

    iget-object v0, v0, Lgmj;->a:Lgmi;

    iget v0, v0, Lgmi;->f:I

    sub-int v0, v7, v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v5, Lhps;->e:I

    goto :goto_1

    :pswitch_2
    iget-object v6, v0, Lgmj;->b:Lgmg;

    if-eqz v6, :cond_1

    iget-object v6, v0, Lgmj;->b:Lgmg;

    iget-object v6, v6, Lgmg;->c:Lglv;

    sget-object v7, Lglv;->b:Lglv;

    if-eq v6, v7, :cond_2

    :cond_1
    const/4 v6, 0x3

    iput v6, v5, Lhps;->b:I

    :goto_2
    const-wide/16 v6, 0x0

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v9, v0, Lgmj;->a:Lgmi;

    iget-wide v10, v9, Lgmi;->d:J

    iget-object v9, v1, Lbkr;->b:Lezj;

    invoke-virtual {v9}, Lezj;->a()J

    move-result-wide v12

    sub-long/2addr v10, v12

    invoke-virtual {v8, v10, v11}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    long-to-int v6, v6

    iput v6, v5, Lhps;->d:I

    iget-object v6, v0, Lgmj;->a:Lgmi;

    iget-object v6, v6, Lgmi;->b:Ljava/lang/String;

    iput-object v6, v5, Lhps;->c:Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v7, v0, Lgmj;->a:Lgmi;

    iget v7, v7, Lgmi;->e:I

    iget-object v8, v0, Lgmj;->b:Lgmg;

    iget v8, v8, Lgmg;->b:I

    iget-object v0, v0, Lgmj;->a:Lgmi;

    iget v0, v0, Lgmi;->f:I

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    sub-int v0, v7, v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, v5, Lhps;->e:I

    goto/16 :goto_1

    :cond_2
    const/4 v6, 0x4

    iput v6, v5, Lhps;->b:I

    goto :goto_2

    :cond_3
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final i(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 685
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/4 v2, 0x7

    .line 686
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 685
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 689
    return-void
.end method

.method public final j()Lgmv;
    .locals 1

    .prologue
    .line 1063
    iget-object v0, p0, Lbla;->u:Lbkj;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 693
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/16 v2, 0x8

    .line 694
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 693
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 697
    return-void
.end method

.method public final k()Lgmw;
    .locals 1

    .prologue
    .line 1068
    iget-object v0, p0, Lbla;->o:Lgmw;

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 701
    iget-object v0, p0, Lbla;->l:Landroid/os/Handler;

    iget-object v1, p0, Lbla;->l:Landroid/os/Handler;

    const/16 v2, 0xc

    .line 702
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 701
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 704
    return-void
.end method

.method public final l(Ljava/lang/String;)Landroid/util/Pair;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 742
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 743
    invoke-static {}, Lb;->b()V

    .line 744
    iget-object v1, p0, Lbla;->i:Lgml;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v1, v1, Lgml;->f:Lgnp;

    invoke-virtual {v1, p1}, Lgnp;->f(Ljava/lang/String;)Lgbu;

    move-result-object v1

    .line 745
    if-nez v1, :cond_1

    .line 752
    :cond_0
    :goto_0
    return-object v0

    .line 748
    :cond_1
    iget-object v2, p0, Lbla;->i:Lgml;

    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, v2, Lgml;->f:Lgnp;

    invoke-virtual {v2, p1}, Lgnp;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 749
    if-eqz v2, :cond_0

    .line 752
    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final m(Ljava/lang/String;)Lgcd;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, p1}, Lgml;->a(Ljava/lang/String;)Lgcd;

    move-result-object v0

    return-object v0
.end method

.method public final n(Ljava/lang/String;)Lfrl;
    .locals 9

    .prologue
    const-wide/32 v6, 0x112a880

    .line 804
    invoke-static {}, Lb;->b()V

    .line 806
    invoke-virtual {p0, p1}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 807
    if-nez v0, :cond_0

    .line 809
    new-instance v0, Lglr;

    invoke-direct {v0}, Lglr;-><init>()V

    throw v0

    .line 810
    :cond_0
    invoke-virtual {v0}, Lgmb;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 811
    new-instance v0, Lglq;

    invoke-direct {v0}, Lglq;-><init>()V

    throw v0

    .line 812
    :cond_1
    invoke-virtual {v0}, Lgmb;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 813
    iget-object v0, v0, Lgmb;->d:Lglz;

    invoke-virtual {v0}, Lglz;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 815
    new-instance v0, Lglu;

    invoke-direct {v0}, Lglu;-><init>()V

    throw v0

    .line 818
    :cond_2
    new-instance v0, Lglt;

    invoke-direct {v0}, Lglt;-><init>()V

    throw v0

    .line 819
    :cond_3
    invoke-virtual {v0}, Lgmb;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 820
    new-instance v0, Lglp;

    invoke-direct {v0}, Lglp;-><init>()V

    throw v0

    .line 823
    :cond_4
    iget-object v0, p0, Lbla;->i:Lgml;

    invoke-virtual {v0, p1}, Lgml;->c(Ljava/lang/String;)Lfrl;

    move-result-object v1

    .line 824
    if-eqz v1, :cond_5

    iget-object v0, v1, Lfrl;->d:Lfrf;

    if-eqz v0, :cond_5

    .line 826
    iget-object v0, p0, Lbla;->i:Lgml;

    new-instance v2, Lblu;

    iget-object v3, p0, Lbla;->e:Lezj;

    .line 829
    invoke-virtual {v3}, Lezj;->b()J

    move-result-wide v4

    add-long/2addr v4, v6

    invoke-direct {v2, p0, v4, v5}, Lblu;-><init>(Lbla;J)V

    .line 826
    invoke-virtual {v0, p1, v2}, Lgml;->a(Ljava/lang/String;Lgni;)Lgly;

    move-result-object v0

    .line 830
    invoke-virtual {v0}, Lgly;->c()Z

    move-result v2

    if-nez v2, :cond_5

    .line 833
    :try_start_0
    invoke-virtual {v0}, Lgly;->a()Lfqj;

    move-result-object v2

    .line 834
    invoke-virtual {v0}, Lgly;->b()Lfqj;

    move-result-object v3

    iget-object v0, p0, Lbla;->e:Lezj;

    .line 835
    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/32 v6, 0x112a880

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 836
    invoke-virtual {v0, v6, v7, v8}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    .line 832
    invoke-virtual/range {v1 .. v7}, Lfrl;->a(Lfqj;Lfqj;JJ)Lfrl;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 842
    :cond_5
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final o(Ljava/lang/String;)Lesq;
    .locals 2

    .prologue
    .line 891
    invoke-static {}, Lb;->b()V

    .line 892
    iget-object v1, p0, Lbla;->i:Lgml;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lgml;->h(Ljava/lang/String;)Lesq;

    move-result-object v0

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 904
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 905
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblj;

    invoke-direct {v1, p0, p1}, Lblj;-><init>(Lbla;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 912
    return-void
.end method

.method public final q(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1029
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1030
    iget-object v0, p0, Lbla;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lblc;

    invoke-direct {v1, p0, p1}, Lblc;-><init>(Lbla;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 1036
    return-void
.end method

.method public final r(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1051
    invoke-static {}, Lb;->b()V

    .line 1052
    iget-object v0, p0, Lbla;->i:Lgml;

    iget-object v0, v0, Lgml;->h:Lgmf;

    invoke-virtual {v0, p1}, Lgmf;->b(Ljava/lang/String;)Lgmg;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Lgmg;->b:I

    goto :goto_0
.end method

.method public final s(Ljava/lang/String;)Lglv;
    .locals 1

    .prologue
    .line 1057
    invoke-static {}, Lb;->b()V

    .line 1058
    iget-object v0, p0, Lbla;->i:Lgml;

    iget-object v0, v0, Lgml;->h:Lgmf;

    invoke-virtual {v0, p1}, Lgmf;->b(Ljava/lang/String;)Lgmg;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lglv;->a:Lglv;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lgmg;->c:Lglv;

    goto :goto_0
.end method

.method t(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1151
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s:%s:ad"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbla;->d:Lgit;

    iget-object v4, v4, Lgit;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method u(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1157
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lbla;->d:Lgit;

    iget-object v4, v4, Lgit;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final v(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1164
    iget-object v0, p0, Lbla;->g:Lfad;

    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Ldmz;

    invoke-virtual {v0, p1}, Ldmz;->b(Ljava/lang/String;)V

    .line 1165
    return-void
.end method

.method final w(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1168
    invoke-virtual {p0, p1}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v0

    .line 1169
    iget-object v1, v0, Lgmb;->f:Lglv;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl event "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " add: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    .line 1170
    invoke-virtual {p0, v0}, Lbla;->a(Lgmb;)V

    .line 1171
    new-instance v1, Lbly;

    invoke-direct {v1, v0}, Lbly;-><init>(Lgmb;)V

    invoke-virtual {p0, v1}, Lbla;->a(Ljava/lang/Object;)V

    .line 1172
    return-void
.end method

.method final x(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x16

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "pudl event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " add_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 1176
    new-instance v0, Lblz;

    invoke-direct {v0, p1}, Lblz;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lbla;->a(Ljava/lang/Object;)V

    .line 1177
    return-void
.end method

.method final y(Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1180
    invoke-virtual {p0, p1}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v3

    .line 1181
    if-eqz v3, :cond_1

    .line 1182
    iget-object v0, v3, Lgmb;->f:Lglv;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1183
    iget-wide v4, v3, Lgmb;->h:J

    iget-wide v6, v3, Lgmb;->i:J

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x3f

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "pudl event "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " status: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1182
    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 1188
    const/4 v2, 0x1

    .line 1189
    iget-boolean v0, v3, Lgmb;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lgmb;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    .line 1199
    :goto_0
    new-instance v1, Lbmc;

    invoke-direct {v1, v3, v0}, Lbmc;-><init>(Lgmb;Z)V

    invoke-virtual {p0, v1}, Lbla;->a(Ljava/lang/Object;)V

    .line 1202
    :cond_1
    return-void

    .line 1191
    :cond_2
    invoke-virtual {v3}, Lgmb;->j()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1192
    iget-object v0, p0, Lbla;->g:Lfad;

    .line 1193
    iget-object v0, v0, Lfad;->b:Landroid/os/Binder;

    check-cast v0, Lbmj;

    .line 1194
    iget-object v0, v0, Lbmj;->a:Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;

    invoke-static {v0}, Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;->b(Lcom/google/android/apps/youtube/app/offline/transfer/OfflineTransferService;)Ljava/lang/String;

    move-result-object v0

    .line 1195
    invoke-virtual {p0, p1}, Lbla;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1196
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method final z(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1f

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "pudl event playlist "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " add_failed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 1227
    new-instance v0, Lblo;

    invoke-direct {v0, p1}, Lblo;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lbla;->a(Ljava/lang/Object;)V

    .line 1228
    return-void
.end method
