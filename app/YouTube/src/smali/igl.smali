.class public final Ligl;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ligj;


# direct methods
.method public constructor <init>(Ligj;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Ligl;->a:Ligj;

    .line 47
    return-void
.end method

.method private a(Ligk;)[I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 145
    iget-object v2, p1, Ligk;->b:[I

    array-length v2, v2

    add-int/lit8 v3, v2, -0x1

    .line 146
    if-ne v3, v0, :cond_0

    .line 147
    new-array v2, v0, [I

    invoke-virtual {p1, v0}, Ligk;->a(I)I

    move-result v0

    aput v0, v2, v1

    move-object v0, v2

    .line 160
    :goto_0
    return-object v0

    .line 149
    :cond_0
    new-array v2, v3, [I

    .line 151
    :goto_1
    iget-object v4, p0, Ligl;->a:Ligj;

    iget v4, v4, Ligj;->k:I

    if-ge v0, v4, :cond_2

    if-ge v1, v3, :cond_2

    .line 152
    invoke-virtual {p1, v0}, Ligk;->b(I)I

    move-result v4

    if-nez v4, :cond_1

    .line 153
    iget-object v4, p0, Ligl;->a:Ligj;

    invoke-virtual {v4, v0}, Ligj;->b(I)I

    move-result v4

    aput v4, v2, v1

    .line 154
    add-int/lit8 v1, v1, 0x1

    .line 151
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 157
    :cond_2
    if-eq v1, v3, :cond_3

    .line 158
    new-instance v0, Ligm;

    const-string v1, "Error locator degree does not match number of roots"

    invoke-direct {v0, v1}, Ligm;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v2

    .line 160
    goto :goto_0
.end method

.method private a(Ligk;[I)[I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 165
    array-length v5, p2

    .line 166
    new-array v6, v5, [I

    move v4, v3

    .line 167
    :goto_0
    if-ge v4, v5, :cond_3

    .line 168
    iget-object v0, p0, Ligl;->a:Ligj;

    aget v1, p2, v4

    invoke-virtual {v0, v1}, Ligj;->b(I)I

    move-result v7

    .line 169
    const/4 v1, 0x1

    move v2, v3

    .line 170
    :goto_1
    if-ge v2, v5, :cond_1

    .line 171
    if-eq v4, v2, :cond_4

    .line 176
    iget-object v0, p0, Ligl;->a:Ligj;

    aget v8, p2, v2

    invoke-virtual {v0, v8, v7}, Ligj;->c(II)I

    move-result v0

    .line 177
    and-int/lit8 v8, v0, 0x1

    if-nez v8, :cond_0

    or-int/lit8 v0, v0, 0x1

    .line 178
    :goto_2
    iget-object v8, p0, Ligl;->a:Ligj;

    invoke-virtual {v8, v1, v0}, Ligj;->c(II)I

    move-result v0

    .line 170
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 177
    :cond_0
    and-int/lit8 v0, v0, -0x2

    goto :goto_2

    .line 181
    :cond_1
    iget-object v0, p0, Ligl;->a:Ligj;

    invoke-virtual {p1, v7}, Ligk;->b(I)I

    move-result v2

    iget-object v8, p0, Ligl;->a:Ligj;

    invoke-virtual {v8, v1}, Ligj;->b(I)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ligj;->c(II)I

    move-result v0

    aput v0, v6, v4

    .line 183
    iget-object v0, p0, Ligl;->a:Ligj;

    iget v0, v0, Ligj;->l:I

    if-eqz v0, :cond_2

    .line 184
    iget-object v0, p0, Ligl;->a:Ligj;

    aget v1, v6, v4

    invoke-virtual {v0, v1, v7}, Ligj;->c(II)I

    move-result v0

    aput v0, v6, v4

    .line 167
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 187
    :cond_3
    return-object v6

    :cond_4
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a([II)V
    .locals 13

    .prologue
    .line 59
    new-instance v2, Ligk;

    iget-object v0, p0, Ligl;->a:Ligj;

    invoke-direct {v2, v0, p1}, Ligk;-><init>(Ligj;[I)V

    .line 60
    new-array v3, p2, [I

    .line 61
    const/4 v1, 0x1

    .line 62
    const/4 v0, 0x0

    move v12, v0

    move v0, v1

    move v1, v12

    :goto_0
    if-ge v1, p2, :cond_1

    .line 63
    iget-object v4, p0, Ligl;->a:Ligj;

    iget-object v5, p0, Ligl;->a:Ligj;

    iget v5, v5, Ligj;->l:I

    add-int/2addr v5, v1

    invoke-virtual {v4}, Ligj;->a()V

    iget-object v4, v4, Ligj;->i:[I

    aget v4, v4, v5

    invoke-virtual {v2, v4}, Ligk;->b(I)I

    move-result v4

    .line 64
    array-length v5, v3

    add-int/lit8 v5, v5, -0x1

    sub-int/2addr v5, v1

    aput v4, v3, v5

    .line 65
    if-eqz v4, :cond_0

    .line 66
    const/4 v0, 0x0

    .line 62
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    :cond_1
    if-eqz v0, :cond_3

    .line 86
    :cond_2
    return-void

    .line 72
    :cond_3
    new-instance v1, Ligk;

    iget-object v0, p0, Ligl;->a:Ligj;

    invoke-direct {v1, v0, v3}, Ligk;-><init>(Ligj;[I)V

    .line 73
    iget-object v0, p0, Ligl;->a:Ligj;

    const/4 v2, 0x1

    invoke-virtual {v0, p2, v2}, Ligj;->a(II)Ligk;

    move-result-object v0

    iget-object v2, v0, Ligk;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, v1, Ligk;->b:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_d

    :goto_1
    iget-object v2, p0, Ligl;->a:Ligj;

    invoke-virtual {v2}, Ligj;->b()Ligk;

    move-result-object v3

    iget-object v2, p0, Ligl;->a:Ligj;

    invoke-virtual {v2}, Ligj;->a()V

    iget-object v2, v2, Ligj;->j:Ligk;

    move-object v4, v3

    move-object v5, v0

    move-object v3, v2

    :goto_2
    iget-object v0, v5, Ligk;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v2, p2, 0x2

    if-lt v0, v2, :cond_a

    invoke-virtual {v5}, Ligk;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ligm;

    const-string v1, "r_{i-1} was zero"

    invoke-direct {v0, v1}, Ligm;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Ligl;->a:Ligj;

    invoke-virtual {v0}, Ligj;->b()Ligk;

    move-result-object v0

    iget-object v2, v5, Ligk;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v5, v2}, Ligk;->a(I)I

    move-result v2

    iget-object v6, p0, Ligl;->a:Ligj;

    invoke-virtual {v6, v2}, Ligj;->b(I)I

    move-result v6

    move-object v2, v1

    :goto_3
    iget-object v1, v2, Ligk;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-object v7, v5, Ligk;->b:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    if-lt v1, v7, :cond_8

    invoke-virtual {v2}, Ligk;->a()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, v2, Ligk;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-object v7, v5, Ligk;->b:[I

    array-length v7, v7

    add-int/lit8 v7, v7, -0x1

    sub-int v7, v1, v7

    iget-object v1, p0, Ligl;->a:Ligj;

    iget-object v8, v2, Ligk;->b:[I

    array-length v8, v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v2, v8}, Ligk;->a(I)I

    move-result v8

    invoke-virtual {v1, v8, v6}, Ligj;->c(II)I

    move-result v8

    iget-object v1, p0, Ligl;->a:Ligj;

    invoke-virtual {v1, v7, v8}, Ligj;->a(II)Ligk;

    move-result-object v1

    invoke-virtual {v0, v1}, Ligk;->a(Ligk;)Ligk;

    move-result-object v1

    if-gez v7, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_5
    if-nez v8, :cond_6

    iget-object v0, v5, Ligk;->a:Ligj;

    invoke-virtual {v0}, Ligj;->b()Ligk;

    move-result-object v0

    :goto_4
    invoke-virtual {v2, v0}, Ligk;->a(Ligk;)Ligk;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    goto :goto_3

    :cond_6
    iget-object v0, v5, Ligk;->b:[I

    array-length v9, v0

    add-int v0, v9, v7

    new-array v7, v0, [I

    const/4 v0, 0x0

    :goto_5
    if-ge v0, v9, :cond_7

    iget-object v10, v5, Ligk;->a:Ligj;

    iget-object v11, v5, Ligk;->b:[I

    aget v11, v11, v0

    invoke-virtual {v10, v11, v8}, Ligj;->c(II)I

    move-result v10

    aput v10, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    new-instance v0, Ligk;

    iget-object v8, v5, Ligk;->a:Ligj;

    invoke-direct {v0, v8, v7}, Ligk;-><init>(Ligj;[I)V

    goto :goto_4

    :cond_8
    invoke-virtual {v0, v3}, Ligk;->b(Ligk;)Ligk;

    move-result-object v0

    invoke-virtual {v0, v4}, Ligk;->a(Ligk;)Ligk;

    move-result-object v0

    iget-object v1, v2, Ligk;->b:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-object v4, v5, Ligk;->b:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-lt v1, v4, :cond_9

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Division algorithm failed to reduce polynomial?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    move-object v4, v3

    move-object v1, v5

    move-object v5, v2

    move-object v3, v0

    goto/16 :goto_2

    :cond_a
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ligk;->a(I)I

    move-result v0

    if-nez v0, :cond_b

    new-instance v0, Ligm;

    const-string v1, "sigmaTilde(0) was zero"

    invoke-direct {v0, v1}, Ligm;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v1, p0, Ligl;->a:Ligj;

    invoke-virtual {v1, v0}, Ligj;->b(I)I

    move-result v0

    invoke-virtual {v3, v0}, Ligk;->c(I)Ligk;

    move-result-object v1

    invoke-virtual {v5, v0}, Ligk;->c(I)Ligk;

    move-result-object v0

    const/4 v2, 0x2

    new-array v2, v2, [Ligk;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    .line 75
    const/4 v0, 0x0

    aget-object v0, v2, v0

    .line 76
    const/4 v1, 0x1

    aget-object v1, v2, v1

    .line 77
    invoke-direct {p0, v0}, Ligl;->a(Ligk;)[I

    move-result-object v2

    .line 78
    invoke-direct {p0, v1, v2}, Ligl;->a(Ligk;[I)[I

    move-result-object v1

    .line 79
    const/4 v0, 0x0

    :goto_6
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 80
    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    iget-object v4, p0, Ligl;->a:Ligj;

    aget v5, v2, v0

    invoke-virtual {v4, v5}, Ligj;->a(I)I

    move-result v4

    sub-int/2addr v3, v4

    .line 81
    if-gez v3, :cond_c

    .line 82
    new-instance v0, Ligm;

    const-string v1, "Bad error location"

    invoke-direct {v0, v1}, Ligm;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_c
    aget v4, p1, v3

    aget v5, v1, v0

    invoke-static {v4, v5}, Ligj;->b(II)I

    move-result v4

    aput v4, p1, v3

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_d
    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_1
.end method
