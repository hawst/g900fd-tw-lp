.class public final Lhro;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhqn;

.field public b:Lhwn;

.field public c:Lhii;

.field public d:[Lhqx;

.field public e:Lhqw;

.field public f:Lhby;

.field public g:Lhzc;

.field public h:[Lhal;

.field public i:Lhpl;

.field public j:Lhre;

.field public k:Lhju;

.field public l:[B

.field public m:Lhat;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Lhzz;

.field private q:Lhtx;

.field private r:Ljava/lang/String;

.field private s:Lhwm;

.field private t:Lhxd;

.field private u:Ljava/lang/String;

.field private v:Lhnh;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Lhro;->q:Lhtx;

    iput-object v1, p0, Lhro;->a:Lhqn;

    iput-object v1, p0, Lhro;->b:Lhwn;

    const-string v0, ""

    iput-object v0, p0, Lhro;->r:Ljava/lang/String;

    iput-object v1, p0, Lhro;->c:Lhii;

    sget-object v0, Lhqx;->a:[Lhqx;

    iput-object v0, p0, Lhro;->d:[Lhqx;

    iput-object v1, p0, Lhro;->e:Lhqw;

    iput-object v1, p0, Lhro;->f:Lhby;

    iput-object v1, p0, Lhro;->g:Lhzc;

    sget-object v0, Lhal;->a:[Lhal;

    iput-object v0, p0, Lhro;->h:[Lhal;

    iput-object v1, p0, Lhro;->i:Lhpl;

    iput-object v1, p0, Lhro;->j:Lhre;

    iput-object v1, p0, Lhro;->s:Lhwm;

    iput-object v1, p0, Lhro;->t:Lhxd;

    iput-object v1, p0, Lhro;->k:Lhju;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhro;->l:[B

    iput-object v1, p0, Lhro;->m:Lhat;

    const-string v0, ""

    iput-object v0, p0, Lhro;->n:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhro;->o:Ljava/lang/String;

    iput-object v1, p0, Lhro;->p:Lhzz;

    const-string v0, ""

    iput-object v0, p0, Lhro;->u:Ljava/lang/String;

    iput-object v1, p0, Lhro;->v:Lhnh;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhro;->q:Lhtx;

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    iget-object v2, p0, Lhro;->q:Lhtx;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhro;->a:Lhqn;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhro;->a:Lhqn;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhro;->b:Lhwn;

    if-eqz v2, :cond_1

    const/4 v2, 0x4

    iget-object v3, p0, Lhro;->b:Lhwn;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhro;->r:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x5

    iget-object v3, p0, Lhro;->r:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhro;->c:Lhii;

    if-eqz v2, :cond_3

    const/4 v2, 0x6

    iget-object v3, p0, Lhro;->c:Lhii;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhro;->d:[Lhqx;

    if-eqz v2, :cond_5

    iget-object v3, p0, Lhro;->d:[Lhqx;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    if-eqz v5, :cond_4

    const/4 v6, 0x7

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhro;->e:Lhqw;

    if-eqz v2, :cond_6

    const/16 v2, 0x9

    iget-object v3, p0, Lhro;->e:Lhqw;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lhro;->f:Lhby;

    if-eqz v2, :cond_7

    const/16 v2, 0xa

    iget-object v3, p0, Lhro;->f:Lhby;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget-object v2, p0, Lhro;->g:Lhzc;

    if-eqz v2, :cond_8

    const/16 v2, 0xb

    iget-object v3, p0, Lhro;->g:Lhzc;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lhro;->h:[Lhal;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lhro;->h:[Lhal;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    if-eqz v4, :cond_9

    const/16 v5, 0xd

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_a
    iget-object v1, p0, Lhro;->i:Lhpl;

    if-eqz v1, :cond_b

    const/16 v1, 0xe

    iget-object v2, p0, Lhro;->i:Lhpl;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lhro;->j:Lhre;

    if-eqz v1, :cond_c

    const/16 v1, 0xf

    iget-object v2, p0, Lhro;->j:Lhre;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget-object v1, p0, Lhro;->s:Lhwm;

    if-eqz v1, :cond_d

    const/16 v1, 0x10

    iget-object v2, p0, Lhro;->s:Lhwm;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget-object v1, p0, Lhro;->t:Lhxd;

    if-eqz v1, :cond_e

    const/16 v1, 0x11

    iget-object v2, p0, Lhro;->t:Lhxd;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget-object v1, p0, Lhro;->k:Lhju;

    if-eqz v1, :cond_f

    const/16 v1, 0x14

    iget-object v2, p0, Lhro;->k:Lhju;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget-object v1, p0, Lhro;->l:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_10

    const/16 v1, 0x15

    iget-object v2, p0, Lhro;->l:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lhro;->m:Lhat;

    if-eqz v1, :cond_11

    const/16 v1, 0x16

    iget-object v2, p0, Lhro;->m:Lhat;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget-object v1, p0, Lhro;->n:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    const/16 v1, 0x17

    iget-object v2, p0, Lhro;->n:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget-object v1, p0, Lhro;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    const/16 v1, 0x19

    iget-object v2, p0, Lhro;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget-object v1, p0, Lhro;->p:Lhzz;

    if-eqz v1, :cond_14

    const/16 v1, 0x1a

    iget-object v2, p0, Lhro;->p:Lhzz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget-object v1, p0, Lhro;->u:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    const/16 v1, 0x1b

    iget-object v2, p0, Lhro;->u:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    iget-object v1, p0, Lhro;->v:Lhnh;

    if-eqz v1, :cond_16

    const/16 v1, 0x1c

    iget-object v2, p0, Lhro;->v:Lhnh;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget-object v1, p0, Lhro;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhro;->J:I

    return v0

    :cond_17
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhro;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhro;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhro;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhro;->q:Lhtx;

    if-nez v0, :cond_2

    new-instance v0, Lhtx;

    invoke-direct {v0}, Lhtx;-><init>()V

    iput-object v0, p0, Lhro;->q:Lhtx;

    :cond_2
    iget-object v0, p0, Lhro;->q:Lhtx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhro;->a:Lhqn;

    if-nez v0, :cond_3

    new-instance v0, Lhqn;

    invoke-direct {v0}, Lhqn;-><init>()V

    iput-object v0, p0, Lhro;->a:Lhqn;

    :cond_3
    iget-object v0, p0, Lhro;->a:Lhqn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhro;->b:Lhwn;

    if-nez v0, :cond_4

    new-instance v0, Lhwn;

    invoke-direct {v0}, Lhwn;-><init>()V

    iput-object v0, p0, Lhro;->b:Lhwn;

    :cond_4
    iget-object v0, p0, Lhro;->b:Lhwn;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhro;->r:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhro;->c:Lhii;

    if-nez v0, :cond_5

    new-instance v0, Lhii;

    invoke-direct {v0}, Lhii;-><init>()V

    iput-object v0, p0, Lhro;->c:Lhii;

    :cond_5
    iget-object v0, p0, Lhro;->c:Lhii;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhro;->d:[Lhqx;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhqx;

    iget-object v3, p0, Lhro;->d:[Lhqx;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhro;->d:[Lhqx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhro;->d:[Lhqx;

    :goto_2
    iget-object v2, p0, Lhro;->d:[Lhqx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhro;->d:[Lhqx;

    new-instance v3, Lhqx;

    invoke-direct {v3}, Lhqx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhro;->d:[Lhqx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lhro;->d:[Lhqx;

    array-length v0, v0

    goto :goto_1

    :cond_8
    iget-object v2, p0, Lhro;->d:[Lhqx;

    new-instance v3, Lhqx;

    invoke-direct {v3}, Lhqx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhro;->d:[Lhqx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lhro;->e:Lhqw;

    if-nez v0, :cond_9

    new-instance v0, Lhqw;

    invoke-direct {v0}, Lhqw;-><init>()V

    iput-object v0, p0, Lhro;->e:Lhqw;

    :cond_9
    iget-object v0, p0, Lhro;->e:Lhqw;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhro;->f:Lhby;

    if-nez v0, :cond_a

    new-instance v0, Lhby;

    invoke-direct {v0}, Lhby;-><init>()V

    iput-object v0, p0, Lhro;->f:Lhby;

    :cond_a
    iget-object v0, p0, Lhro;->f:Lhby;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhro;->g:Lhzc;

    if-nez v0, :cond_b

    new-instance v0, Lhzc;

    invoke-direct {v0}, Lhzc;-><init>()V

    iput-object v0, p0, Lhro;->g:Lhzc;

    :cond_b
    iget-object v0, p0, Lhro;->g:Lhzc;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhro;->h:[Lhal;

    if-nez v0, :cond_d

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhal;

    iget-object v3, p0, Lhro;->h:[Lhal;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lhro;->h:[Lhal;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    iput-object v2, p0, Lhro;->h:[Lhal;

    :goto_4
    iget-object v2, p0, Lhro;->h:[Lhal;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Lhro;->h:[Lhal;

    new-instance v3, Lhal;

    invoke-direct {v3}, Lhal;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhro;->h:[Lhal;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_d
    iget-object v0, p0, Lhro;->h:[Lhal;

    array-length v0, v0

    goto :goto_3

    :cond_e
    iget-object v2, p0, Lhro;->h:[Lhal;

    new-instance v3, Lhal;

    invoke-direct {v3}, Lhal;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhro;->h:[Lhal;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhro;->i:Lhpl;

    if-nez v0, :cond_f

    new-instance v0, Lhpl;

    invoke-direct {v0}, Lhpl;-><init>()V

    iput-object v0, p0, Lhro;->i:Lhpl;

    :cond_f
    iget-object v0, p0, Lhro;->i:Lhpl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lhro;->j:Lhre;

    if-nez v0, :cond_10

    new-instance v0, Lhre;

    invoke-direct {v0}, Lhre;-><init>()V

    iput-object v0, p0, Lhro;->j:Lhre;

    :cond_10
    iget-object v0, p0, Lhro;->j:Lhre;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lhro;->s:Lhwm;

    if-nez v0, :cond_11

    new-instance v0, Lhwm;

    invoke-direct {v0}, Lhwm;-><init>()V

    iput-object v0, p0, Lhro;->s:Lhwm;

    :cond_11
    iget-object v0, p0, Lhro;->s:Lhwm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lhro;->t:Lhxd;

    if-nez v0, :cond_12

    new-instance v0, Lhxd;

    invoke-direct {v0}, Lhxd;-><init>()V

    iput-object v0, p0, Lhro;->t:Lhxd;

    :cond_12
    iget-object v0, p0, Lhro;->t:Lhxd;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lhro;->k:Lhju;

    if-nez v0, :cond_13

    new-instance v0, Lhju;

    invoke-direct {v0}, Lhju;-><init>()V

    iput-object v0, p0, Lhro;->k:Lhju;

    :cond_13
    iget-object v0, p0, Lhro;->k:Lhju;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhro;->l:[B

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lhro;->m:Lhat;

    if-nez v0, :cond_14

    new-instance v0, Lhat;

    invoke-direct {v0}, Lhat;-><init>()V

    iput-object v0, p0, Lhro;->m:Lhat;

    :cond_14
    iget-object v0, p0, Lhro;->m:Lhat;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhro;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhro;->o:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_14
    iget-object v0, p0, Lhro;->p:Lhzz;

    if-nez v0, :cond_15

    new-instance v0, Lhzz;

    invoke-direct {v0}, Lhzz;-><init>()V

    iput-object v0, p0, Lhro;->p:Lhzz;

    :cond_15
    iget-object v0, p0, Lhro;->p:Lhzz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhro;->u:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lhro;->v:Lhnh;

    if-nez v0, :cond_16

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Lhro;->v:Lhnh;

    :cond_16
    iget-object v0, p0, Lhro;->v:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x6a -> :sswitch_a
        0x72 -> :sswitch_b
        0x7a -> :sswitch_c
        0x82 -> :sswitch_d
        0x8a -> :sswitch_e
        0xa2 -> :sswitch_f
        0xaa -> :sswitch_10
        0xb2 -> :sswitch_11
        0xba -> :sswitch_12
        0xca -> :sswitch_13
        0xd2 -> :sswitch_14
        0xda -> :sswitch_15
        0xe2 -> :sswitch_16
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhro;->q:Lhtx;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lhro;->q:Lhtx;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v1, p0, Lhro;->a:Lhqn;

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhro;->a:Lhqn;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v1, p0, Lhro;->b:Lhwn;

    if-eqz v1, :cond_2

    const/4 v1, 0x4

    iget-object v2, p0, Lhro;->b:Lhwn;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v1, p0, Lhro;->r:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x5

    iget-object v2, p0, Lhro;->r:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lhro;->c:Lhii;

    if-eqz v1, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Lhro;->c:Lhii;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v1, p0, Lhro;->d:[Lhqx;

    if-eqz v1, :cond_6

    iget-object v2, p0, Lhro;->d:[Lhqx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    if-eqz v4, :cond_5

    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lhro;->e:Lhqw;

    if-eqz v1, :cond_7

    const/16 v1, 0x9

    iget-object v2, p0, Lhro;->e:Lhqw;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_7
    iget-object v1, p0, Lhro;->f:Lhby;

    if-eqz v1, :cond_8

    const/16 v1, 0xa

    iget-object v2, p0, Lhro;->f:Lhby;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v1, p0, Lhro;->g:Lhzc;

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    iget-object v2, p0, Lhro;->g:Lhzc;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    :cond_9
    iget-object v1, p0, Lhro;->h:[Lhal;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lhro;->h:[Lhal;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_b

    aget-object v3, v1, v0

    if-eqz v3, :cond_a

    const/16 v4, 0xd

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_b
    iget-object v0, p0, Lhro;->i:Lhpl;

    if-eqz v0, :cond_c

    const/16 v0, 0xe

    iget-object v1, p0, Lhro;->i:Lhpl;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_c
    iget-object v0, p0, Lhro;->j:Lhre;

    if-eqz v0, :cond_d

    const/16 v0, 0xf

    iget-object v1, p0, Lhro;->j:Lhre;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_d
    iget-object v0, p0, Lhro;->s:Lhwm;

    if-eqz v0, :cond_e

    const/16 v0, 0x10

    iget-object v1, p0, Lhro;->s:Lhwm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_e
    iget-object v0, p0, Lhro;->t:Lhxd;

    if-eqz v0, :cond_f

    const/16 v0, 0x11

    iget-object v1, p0, Lhro;->t:Lhxd;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_f
    iget-object v0, p0, Lhro;->k:Lhju;

    if-eqz v0, :cond_10

    const/16 v0, 0x14

    iget-object v1, p0, Lhro;->k:Lhju;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_10
    iget-object v0, p0, Lhro;->l:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_11

    const/16 v0, 0x15

    iget-object v1, p0, Lhro;->l:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_11
    iget-object v0, p0, Lhro;->m:Lhat;

    if-eqz v0, :cond_12

    const/16 v0, 0x16

    iget-object v1, p0, Lhro;->m:Lhat;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_12
    iget-object v0, p0, Lhro;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    const/16 v0, 0x17

    iget-object v1, p0, Lhro;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_13
    iget-object v0, p0, Lhro;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const/16 v0, 0x19

    iget-object v1, p0, Lhro;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_14
    iget-object v0, p0, Lhro;->p:Lhzz;

    if-eqz v0, :cond_15

    const/16 v0, 0x1a

    iget-object v1, p0, Lhro;->p:Lhzz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_15
    iget-object v0, p0, Lhro;->u:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    const/16 v0, 0x1b

    iget-object v1, p0, Lhro;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_16
    iget-object v0, p0, Lhro;->v:Lhnh;

    if-eqz v0, :cond_17

    const/16 v0, 0x1c

    iget-object v1, p0, Lhro;->v:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_17
    iget-object v0, p0, Lhro;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhro;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhro;

    iget-object v2, p0, Lhro;->q:Lhtx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhro;->q:Lhtx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhro;->a:Lhqn;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhro;->a:Lhqn;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhro;->b:Lhwn;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhro;->b:Lhwn;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhro;->r:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhro;->r:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhro;->c:Lhii;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhro;->c:Lhii;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhro;->d:[Lhqx;

    iget-object v3, p1, Lhro;->d:[Lhqx;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhro;->e:Lhqw;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhro;->e:Lhqw;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhro;->f:Lhby;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhro;->f:Lhby;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Lhro;->g:Lhzc;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhro;->g:Lhzc;

    if-nez v2, :cond_3

    :goto_8
    iget-object v2, p0, Lhro;->h:[Lhal;

    iget-object v3, p1, Lhro;->h:[Lhal;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhro;->i:Lhpl;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhro;->i:Lhpl;

    if-nez v2, :cond_3

    :goto_9
    iget-object v2, p0, Lhro;->j:Lhre;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhro;->j:Lhre;

    if-nez v2, :cond_3

    :goto_a
    iget-object v2, p0, Lhro;->s:Lhwm;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhro;->s:Lhwm;

    if-nez v2, :cond_3

    :goto_b
    iget-object v2, p0, Lhro;->t:Lhxd;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhro;->t:Lhxd;

    if-nez v2, :cond_3

    :goto_c
    iget-object v2, p0, Lhro;->k:Lhju;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhro;->k:Lhju;

    if-nez v2, :cond_3

    :goto_d
    iget-object v2, p0, Lhro;->l:[B

    iget-object v3, p1, Lhro;->l:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhro;->m:Lhat;

    if-nez v2, :cond_11

    iget-object v2, p1, Lhro;->m:Lhat;

    if-nez v2, :cond_3

    :goto_e
    iget-object v2, p0, Lhro;->n:Ljava/lang/String;

    if-nez v2, :cond_12

    iget-object v2, p1, Lhro;->n:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_f
    iget-object v2, p0, Lhro;->o:Ljava/lang/String;

    if-nez v2, :cond_13

    iget-object v2, p1, Lhro;->o:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_10
    iget-object v2, p0, Lhro;->p:Lhzz;

    if-nez v2, :cond_14

    iget-object v2, p1, Lhro;->p:Lhzz;

    if-nez v2, :cond_3

    :goto_11
    iget-object v2, p0, Lhro;->u:Ljava/lang/String;

    if-nez v2, :cond_15

    iget-object v2, p1, Lhro;->u:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_12
    iget-object v2, p0, Lhro;->v:Lhnh;

    if-nez v2, :cond_16

    iget-object v2, p1, Lhro;->v:Lhnh;

    if-nez v2, :cond_3

    :goto_13
    iget-object v2, p0, Lhro;->I:Ljava/util/List;

    if-nez v2, :cond_17

    iget-object v2, p1, Lhro;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lhro;->q:Lhtx;

    iget-object v3, p1, Lhro;->q:Lhtx;

    invoke-virtual {v2, v3}, Lhtx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lhro;->a:Lhqn;

    iget-object v3, p1, Lhro;->a:Lhqn;

    invoke-virtual {v2, v3}, Lhqn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhro;->b:Lhwn;

    iget-object v3, p1, Lhro;->b:Lhwn;

    invoke-virtual {v2, v3}, Lhwn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhro;->r:Ljava/lang/String;

    iget-object v3, p1, Lhro;->r:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhro;->c:Lhii;

    iget-object v3, p1, Lhro;->c:Lhii;

    invoke-virtual {v2, v3}, Lhii;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhro;->e:Lhqw;

    iget-object v3, p1, Lhro;->e:Lhqw;

    invoke-virtual {v2, v3}, Lhqw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhro;->f:Lhby;

    iget-object v3, p1, Lhro;->f:Lhby;

    invoke-virtual {v2, v3}, Lhby;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhro;->g:Lhzc;

    iget-object v3, p1, Lhro;->g:Lhzc;

    invoke-virtual {v2, v3}, Lhzc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhro;->i:Lhpl;

    iget-object v3, p1, Lhro;->i:Lhpl;

    invoke-virtual {v2, v3}, Lhpl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhro;->j:Lhre;

    iget-object v3, p1, Lhro;->j:Lhre;

    invoke-virtual {v2, v3}, Lhre;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhro;->s:Lhwm;

    iget-object v3, p1, Lhro;->s:Lhwm;

    invoke-virtual {v2, v3}, Lhwm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    :cond_f
    iget-object v2, p0, Lhro;->t:Lhxd;

    iget-object v3, p1, Lhro;->t:Lhxd;

    invoke-virtual {v2, v3}, Lhxd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhro;->k:Lhju;

    iget-object v3, p1, Lhro;->k:Lhju;

    invoke-virtual {v2, v3}, Lhju;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_d

    :cond_11
    iget-object v2, p0, Lhro;->m:Lhat;

    iget-object v3, p1, Lhro;->m:Lhat;

    invoke-virtual {v2, v3}, Lhat;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_e

    :cond_12
    iget-object v2, p0, Lhro;->n:Ljava/lang/String;

    iget-object v3, p1, Lhro;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_f

    :cond_13
    iget-object v2, p0, Lhro;->o:Ljava/lang/String;

    iget-object v3, p1, Lhro;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_10

    :cond_14
    iget-object v2, p0, Lhro;->p:Lhzz;

    iget-object v3, p1, Lhro;->p:Lhzz;

    invoke-virtual {v2, v3}, Lhzz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_11

    :cond_15
    iget-object v2, p0, Lhro;->u:Ljava/lang/String;

    iget-object v3, p1, Lhro;->u:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_12

    :cond_16
    iget-object v2, p0, Lhro;->v:Lhnh;

    iget-object v3, p1, Lhro;->v:Lhnh;

    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_13

    :cond_17
    iget-object v2, p0, Lhro;->I:Ljava/util/List;

    iget-object v3, p1, Lhro;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->q:Lhtx;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->a:Lhqn;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->b:Lhwn;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->r:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->c:Lhii;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    iget-object v2, p0, Lhro;->d:[Lhqx;

    if-nez v2, :cond_8

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhro;->e:Lhqw;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->f:Lhby;

    if-nez v0, :cond_b

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->g:Lhzc;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    iget-object v2, p0, Lhro;->h:[Lhal;

    if-nez v2, :cond_d

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhro;->i:Lhpl;

    if-nez v0, :cond_f

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->j:Lhre;

    if-nez v0, :cond_10

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->s:Lhwm;

    if-nez v0, :cond_11

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->t:Lhxd;

    if-nez v0, :cond_12

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->k:Lhju;

    if-nez v0, :cond_13

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    iget-object v2, p0, Lhro;->l:[B

    if-nez v2, :cond_14

    mul-int/lit8 v2, v0, 0x1f

    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhro;->m:Lhat;

    if-nez v0, :cond_15

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->n:Ljava/lang/String;

    if-nez v0, :cond_16

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->o:Ljava/lang/String;

    if-nez v0, :cond_17

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->p:Lhzz;

    if-nez v0, :cond_18

    move v0, v1

    :goto_10
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->u:Ljava/lang/String;

    if-nez v0, :cond_19

    move v0, v1

    :goto_11
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhro;->v:Lhnh;

    if-nez v0, :cond_1a

    move v0, v1

    :goto_12
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhro;->I:Ljava/util/List;

    if-nez v2, :cond_1b

    :goto_13
    add-int/2addr v0, v1

    return v0

    :cond_3
    iget-object v0, p0, Lhro;->q:Lhtx;

    invoke-virtual {v0}, Lhtx;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lhro;->a:Lhqn;

    invoke-virtual {v0}, Lhqn;->hashCode()I

    move-result v0

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lhro;->b:Lhwn;

    invoke-virtual {v0}, Lhwn;->hashCode()I

    move-result v0

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lhro;->r:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_3

    :cond_7
    iget-object v0, p0, Lhro;->c:Lhii;

    invoke-virtual {v0}, Lhii;->hashCode()I

    move-result v0

    goto/16 :goto_4

    :cond_8
    move v2, v0

    move v0, v1

    :goto_14
    iget-object v3, p0, Lhro;->d:[Lhqx;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhro;->d:[Lhqx;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_14

    :cond_9
    iget-object v2, p0, Lhro;->d:[Lhqx;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhqx;->hashCode()I

    move-result v2

    goto :goto_15

    :cond_a
    iget-object v0, p0, Lhro;->e:Lhqw;

    invoke-virtual {v0}, Lhqw;->hashCode()I

    move-result v0

    goto/16 :goto_5

    :cond_b
    iget-object v0, p0, Lhro;->f:Lhby;

    invoke-virtual {v0}, Lhby;->hashCode()I

    move-result v0

    goto/16 :goto_6

    :cond_c
    iget-object v0, p0, Lhro;->g:Lhzc;

    invoke-virtual {v0}, Lhzc;->hashCode()I

    move-result v0

    goto/16 :goto_7

    :cond_d
    move v2, v0

    move v0, v1

    :goto_16
    iget-object v3, p0, Lhro;->h:[Lhal;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhro;->h:[Lhal;

    aget-object v2, v2, v0

    if-nez v2, :cond_e

    move v2, v1

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_16

    :cond_e
    iget-object v2, p0, Lhro;->h:[Lhal;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhal;->hashCode()I

    move-result v2

    goto :goto_17

    :cond_f
    iget-object v0, p0, Lhro;->i:Lhpl;

    invoke-virtual {v0}, Lhpl;->hashCode()I

    move-result v0

    goto/16 :goto_8

    :cond_10
    iget-object v0, p0, Lhro;->j:Lhre;

    invoke-virtual {v0}, Lhre;->hashCode()I

    move-result v0

    goto/16 :goto_9

    :cond_11
    iget-object v0, p0, Lhro;->s:Lhwm;

    invoke-virtual {v0}, Lhwm;->hashCode()I

    move-result v0

    goto/16 :goto_a

    :cond_12
    iget-object v0, p0, Lhro;->t:Lhxd;

    invoke-virtual {v0}, Lhxd;->hashCode()I

    move-result v0

    goto/16 :goto_b

    :cond_13
    iget-object v0, p0, Lhro;->k:Lhju;

    invoke-virtual {v0}, Lhju;->hashCode()I

    move-result v0

    goto/16 :goto_c

    :cond_14
    move v2, v0

    move v0, v1

    :goto_18
    iget-object v3, p0, Lhro;->l:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhro;->l:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_18

    :cond_15
    iget-object v0, p0, Lhro;->m:Lhat;

    invoke-virtual {v0}, Lhat;->hashCode()I

    move-result v0

    goto/16 :goto_d

    :cond_16
    iget-object v0, p0, Lhro;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_e

    :cond_17
    iget-object v0, p0, Lhro;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_f

    :cond_18
    iget-object v0, p0, Lhro;->p:Lhzz;

    invoke-virtual {v0}, Lhzz;->hashCode()I

    move-result v0

    goto/16 :goto_10

    :cond_19
    iget-object v0, p0, Lhro;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_11

    :cond_1a
    iget-object v0, p0, Lhro;->v:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto/16 :goto_12

    :cond_1b
    iget-object v1, p0, Lhro;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_13
.end method
