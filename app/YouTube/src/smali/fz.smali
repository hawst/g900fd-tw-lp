.class public final Lfz;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lgj;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1151
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 1152
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 1153
    new-instance v0, Lga;

    invoke-direct {v0}, Lga;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    .line 1171
    :goto_0
    return-void

    .line 1154
    :cond_0
    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 1155
    new-instance v0, Lgi;

    invoke-direct {v0}, Lgi;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0

    .line 1156
    :cond_1
    const/16 v1, 0x11

    if-lt v0, v1, :cond_2

    .line 1157
    new-instance v0, Lgh;

    invoke-direct {v0}, Lgh;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0

    .line 1158
    :cond_2
    const/16 v1, 0x10

    if-lt v0, v1, :cond_3

    .line 1159
    new-instance v0, Lgg;

    invoke-direct {v0}, Lgg;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0

    .line 1160
    :cond_3
    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 1161
    new-instance v0, Lgf;

    invoke-direct {v0}, Lgf;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0

    .line 1162
    :cond_4
    const/16 v1, 0xb

    if-lt v0, v1, :cond_5

    .line 1163
    new-instance v0, Lge;

    invoke-direct {v0}, Lge;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0

    .line 1164
    :cond_5
    const/16 v1, 0x9

    if-lt v0, v1, :cond_6

    .line 1165
    new-instance v0, Lgd;

    invoke-direct {v0}, Lgd;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0

    .line 1166
    :cond_6
    const/4 v1, 0x7

    if-lt v0, v1, :cond_7

    .line 1167
    new-instance v0, Lgc;

    invoke-direct {v0}, Lgc;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0

    .line 1169
    :cond_7
    new-instance v0, Lgb;

    invoke-direct {v0}, Lgb;-><init>()V

    sput-object v0, Lfz;->a:Lgj;

    goto :goto_0
.end method

.method public static a(III)I
    .locals 1

    .prologue
    .line 1733
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1, p2}, Lgj;->a(III)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1206
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->b(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 1946
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->b(Landroid/view/View;F)V

    .line 1947
    return-void
.end method

.method public static a(Landroid/view/View;ILandroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 1579
    sget-object v0, Lfz;->a:Lgj;

    const/4 v1, 0x0

    invoke-interface {v0, p0, p1, v1}, Lgj;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1580
    return-void
.end method

.method public static a(Landroid/view/View;Lea;)V
    .locals 1

    .prologue
    .line 1343
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->a(Landroid/view/View;Lea;)V

    .line 1344
    return-void
.end method

.method public static a(Landroid/view/View;Lfl;)V
    .locals 1

    .prologue
    .line 2262
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->a(Landroid/view/View;Lfl;)V

    .line 2263
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 1421
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1422
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Runnable;J)V
    .locals 2

    .prologue
    .line 1438
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1, p2, p3}, Lgj;->a(Landroid/view/View;Ljava/lang/Runnable;J)V

    .line 1439
    return-void
.end method

.method public static a(Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 1181
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->a(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1389
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->c(Landroid/view/View;)V

    .line 1390
    return-void
.end method

.method public static b(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 1962
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->c(Landroid/view/View;F)V

    .line 1963
    return-void
.end method

.method public static b(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1480
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->b(Landroid/view/View;I)V

    .line 1481
    return-void
.end method

.method public static c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1456
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->d(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static c(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 1978
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->d(Landroid/view/View;F)V

    .line 1979
    return-void
.end method

.method public static d(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1671
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->e(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/view/View;F)V
    .locals 2

    .prologue
    .line 2068
    sget-object v0, Lfz;->a:Lgj;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Lgj;->e(Landroid/view/View;F)V

    .line 2069
    return-void
.end method

.method public static e(Landroid/view/View;)Landroid/view/ViewParent;
    .locals 1

    .prologue
    .line 1703
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->f(Landroid/view/View;)Landroid/view/ViewParent;

    move-result-object v0

    return-object v0
.end method

.method public static e(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 2156
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0, p1}, Lgj;->a(Landroid/view/View;F)V

    .line 2157
    return-void
.end method

.method public static f(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1772
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->g(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static g(Landroid/view/View;)F
    .locals 1

    .prologue
    .line 1898
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->h(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method public static h(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 1920
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->i(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static i(Landroid/view/View;)Lhj;
    .locals 1

    .prologue
    .line 1932
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->j(Landroid/view/View;)Lhj;

    move-result-object v0

    return-object v0
.end method

.method public static j(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 2213
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->k(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public static k(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2221
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->a(Landroid/view/View;)V

    .line 2222
    return-void
.end method

.method public static l(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 2242
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->l(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public static m(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2253
    sget-object v0, Lfz;->a:Lgj;

    invoke-interface {v0, p0}, Lgj;->m(Landroid/view/View;)V

    .line 2254
    return-void
.end method
