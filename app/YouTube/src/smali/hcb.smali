.class public final Lhcb;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:[Lgxq;

.field public c:[Lgxq;

.field public d:Lhgz;

.field public e:Lhgz;

.field public f:Lhgz;

.field private g:Lhvr;

.field private h:Lhoi;

.field private i:Lhqd;

.field private j:Lhyo;

.field private k:Lhog;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:[Lhut;

.field private p:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18726
    invoke-direct {p0}, Lidf;-><init>()V

    .line 18729
    iput-object v1, p0, Lhcb;->a:Lhgz;

    .line 18732
    sget-object v0, Lgxq;->a:[Lgxq;

    iput-object v0, p0, Lhcb;->b:[Lgxq;

    .line 18735
    sget-object v0, Lgxq;->a:[Lgxq;

    iput-object v0, p0, Lhcb;->c:[Lgxq;

    .line 18738
    iput-object v1, p0, Lhcb;->d:Lhgz;

    .line 18741
    iput-object v1, p0, Lhcb;->e:Lhgz;

    .line 18744
    iput-object v1, p0, Lhcb;->f:Lhgz;

    .line 18747
    iput-object v1, p0, Lhcb;->g:Lhvr;

    .line 18750
    iput-object v1, p0, Lhcb;->h:Lhoi;

    .line 18753
    iput-object v1, p0, Lhcb;->i:Lhqd;

    .line 18756
    iput-object v1, p0, Lhcb;->j:Lhyo;

    .line 18759
    iput-object v1, p0, Lhcb;->k:Lhog;

    .line 18762
    const-string v0, ""

    iput-object v0, p0, Lhcb;->l:Ljava/lang/String;

    .line 18765
    const-string v0, ""

    iput-object v0, p0, Lhcb;->m:Ljava/lang/String;

    .line 18768
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhcb;->n:Z

    .line 18771
    sget-object v0, Lhut;->a:[Lhut;

    iput-object v0, p0, Lhcb;->o:[Lhut;

    .line 18774
    const-string v0, ""

    iput-object v0, p0, Lhcb;->p:Ljava/lang/String;

    .line 18726
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 18928
    .line 18929
    iget-object v0, p0, Lhcb;->a:Lhgz;

    if-eqz v0, :cond_12

    .line 18930
    const/4 v0, 0x1

    iget-object v2, p0, Lhcb;->a:Lhgz;

    .line 18931
    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 18933
    :goto_0
    iget-object v2, p0, Lhcb;->b:[Lgxq;

    if-eqz v2, :cond_1

    .line 18934
    iget-object v3, p0, Lhcb;->b:[Lgxq;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 18935
    if-eqz v5, :cond_0

    .line 18936
    const/4 v6, 0x2

    .line 18937
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 18934
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 18941
    :cond_1
    iget-object v2, p0, Lhcb;->c:[Lgxq;

    if-eqz v2, :cond_3

    .line 18942
    iget-object v3, p0, Lhcb;->c:[Lgxq;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 18943
    if-eqz v5, :cond_2

    .line 18944
    const/4 v6, 0x3

    .line 18945
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 18942
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 18949
    :cond_3
    iget-object v2, p0, Lhcb;->d:Lhgz;

    if-eqz v2, :cond_4

    .line 18950
    const/4 v2, 0x4

    iget-object v3, p0, Lhcb;->d:Lhgz;

    .line 18951
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18953
    :cond_4
    iget-object v2, p0, Lhcb;->e:Lhgz;

    if-eqz v2, :cond_5

    .line 18954
    const/4 v2, 0x5

    iget-object v3, p0, Lhcb;->e:Lhgz;

    .line 18955
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18957
    :cond_5
    iget-object v2, p0, Lhcb;->f:Lhgz;

    if-eqz v2, :cond_6

    .line 18958
    const/4 v2, 0x6

    iget-object v3, p0, Lhcb;->f:Lhgz;

    .line 18959
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18961
    :cond_6
    iget-object v2, p0, Lhcb;->g:Lhvr;

    if-eqz v2, :cond_7

    .line 18962
    const/4 v2, 0x7

    iget-object v3, p0, Lhcb;->g:Lhvr;

    .line 18963
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18965
    :cond_7
    iget-object v2, p0, Lhcb;->h:Lhoi;

    if-eqz v2, :cond_8

    .line 18966
    const/16 v2, 0x8

    iget-object v3, p0, Lhcb;->h:Lhoi;

    .line 18967
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18969
    :cond_8
    iget-object v2, p0, Lhcb;->i:Lhqd;

    if-eqz v2, :cond_9

    .line 18970
    const/16 v2, 0x9

    iget-object v3, p0, Lhcb;->i:Lhqd;

    .line 18971
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18973
    :cond_9
    iget-object v2, p0, Lhcb;->j:Lhyo;

    if-eqz v2, :cond_a

    .line 18974
    const/16 v2, 0xb

    iget-object v3, p0, Lhcb;->j:Lhyo;

    .line 18975
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18977
    :cond_a
    iget-object v2, p0, Lhcb;->k:Lhog;

    if-eqz v2, :cond_b

    .line 18978
    const/16 v2, 0xc

    iget-object v3, p0, Lhcb;->k:Lhog;

    .line 18979
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18981
    :cond_b
    iget-object v2, p0, Lhcb;->l:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 18982
    const/16 v2, 0xd

    iget-object v3, p0, Lhcb;->l:Ljava/lang/String;

    .line 18983
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18985
    :cond_c
    iget-object v2, p0, Lhcb;->m:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 18986
    const/16 v2, 0xe

    iget-object v3, p0, Lhcb;->m:Ljava/lang/String;

    .line 18987
    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 18989
    :cond_d
    iget-boolean v2, p0, Lhcb;->n:Z

    if-eqz v2, :cond_e

    .line 18990
    const/16 v2, 0xf

    iget-boolean v3, p0, Lhcb;->n:Z

    .line 18991
    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 18993
    :cond_e
    iget-object v2, p0, Lhcb;->o:[Lhut;

    if-eqz v2, :cond_10

    .line 18994
    iget-object v2, p0, Lhcb;->o:[Lhut;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 18995
    if-eqz v4, :cond_f

    .line 18996
    const/16 v5, 0x10

    .line 18997
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 18994
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 19001
    :cond_10
    iget-object v1, p0, Lhcb;->p:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 19002
    const/16 v1, 0x11

    iget-object v2, p0, Lhcb;->p:Ljava/lang/String;

    .line 19003
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19005
    :cond_11
    iget-object v1, p0, Lhcb;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 19006
    iput v0, p0, Lhcb;->J:I

    .line 19007
    return v0

    :cond_12
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 18722
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhcb;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhcb;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhcb;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhcb;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcb;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhcb;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhcb;->b:[Lgxq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lgxq;

    iget-object v3, p0, Lhcb;->b:[Lgxq;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lhcb;->b:[Lgxq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iput-object v2, p0, Lhcb;->b:[Lgxq;

    :goto_2
    iget-object v2, p0, Lhcb;->b:[Lgxq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lhcb;->b:[Lgxq;

    new-instance v3, Lgxq;

    invoke-direct {v3}, Lgxq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcb;->b:[Lgxq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhcb;->b:[Lgxq;

    array-length v0, v0

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhcb;->b:[Lgxq;

    new-instance v3, Lgxq;

    invoke-direct {v3}, Lgxq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcb;->b:[Lgxq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhcb;->c:[Lgxq;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lgxq;

    iget-object v3, p0, Lhcb;->c:[Lgxq;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhcb;->c:[Lgxq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhcb;->c:[Lgxq;

    :goto_4
    iget-object v2, p0, Lhcb;->c:[Lgxq;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhcb;->c:[Lgxq;

    new-instance v3, Lgxq;

    invoke-direct {v3}, Lgxq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcb;->c:[Lgxq;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhcb;->c:[Lgxq;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lhcb;->c:[Lgxq;

    new-instance v3, Lgxq;

    invoke-direct {v3}, Lgxq;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcb;->c:[Lgxq;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lhcb;->d:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcb;->d:Lhgz;

    :cond_9
    iget-object v0, p0, Lhcb;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lhcb;->e:Lhgz;

    if-nez v0, :cond_a

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcb;->e:Lhgz;

    :cond_a
    iget-object v0, p0, Lhcb;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhcb;->f:Lhgz;

    if-nez v0, :cond_b

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcb;->f:Lhgz;

    :cond_b
    iget-object v0, p0, Lhcb;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lhcb;->g:Lhvr;

    if-nez v0, :cond_c

    new-instance v0, Lhvr;

    invoke-direct {v0}, Lhvr;-><init>()V

    iput-object v0, p0, Lhcb;->g:Lhvr;

    :cond_c
    iget-object v0, p0, Lhcb;->g:Lhvr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhcb;->h:Lhoi;

    if-nez v0, :cond_d

    new-instance v0, Lhoi;

    invoke-direct {v0}, Lhoi;-><init>()V

    iput-object v0, p0, Lhcb;->h:Lhoi;

    :cond_d
    iget-object v0, p0, Lhcb;->h:Lhoi;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lhcb;->i:Lhqd;

    if-nez v0, :cond_e

    new-instance v0, Lhqd;

    invoke-direct {v0}, Lhqd;-><init>()V

    iput-object v0, p0, Lhcb;->i:Lhqd;

    :cond_e
    iget-object v0, p0, Lhcb;->i:Lhqd;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhcb;->j:Lhyo;

    if-nez v0, :cond_f

    new-instance v0, Lhyo;

    invoke-direct {v0}, Lhyo;-><init>()V

    iput-object v0, p0, Lhcb;->j:Lhyo;

    :cond_f
    iget-object v0, p0, Lhcb;->j:Lhyo;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhcb;->k:Lhog;

    if-nez v0, :cond_10

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhcb;->k:Lhog;

    :cond_10
    iget-object v0, p0, Lhcb;->k:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcb;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcb;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhcb;->n:Z

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x82

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhcb;->o:[Lhut;

    if-nez v0, :cond_12

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhut;

    iget-object v3, p0, Lhcb;->o:[Lhut;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lhcb;->o:[Lhut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    iput-object v2, p0, Lhcb;->o:[Lhut;

    :goto_6
    iget-object v2, p0, Lhcb;->o:[Lhut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    iget-object v2, p0, Lhcb;->o:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcb;->o:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_12
    iget-object v0, p0, Lhcb;->o:[Lhut;

    array-length v0, v0

    goto :goto_5

    :cond_13
    iget-object v2, p0, Lhcb;->o:[Lhut;

    new-instance v3, Lhut;

    invoke-direct {v3}, Lhut;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhcb;->o:[Lhut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhcb;->p:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x78 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 18862
    iget-object v1, p0, Lhcb;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 18863
    const/4 v1, 0x1

    iget-object v2, p0, Lhcb;->a:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18865
    :cond_0
    iget-object v1, p0, Lhcb;->b:[Lgxq;

    if-eqz v1, :cond_2

    .line 18866
    iget-object v2, p0, Lhcb;->b:[Lgxq;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 18867
    if-eqz v4, :cond_1

    .line 18868
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 18866
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 18872
    :cond_2
    iget-object v1, p0, Lhcb;->c:[Lgxq;

    if-eqz v1, :cond_4

    .line 18873
    iget-object v2, p0, Lhcb;->c:[Lgxq;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 18874
    if-eqz v4, :cond_3

    .line 18875
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 18873
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 18879
    :cond_4
    iget-object v1, p0, Lhcb;->d:Lhgz;

    if-eqz v1, :cond_5

    .line 18880
    const/4 v1, 0x4

    iget-object v2, p0, Lhcb;->d:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18882
    :cond_5
    iget-object v1, p0, Lhcb;->e:Lhgz;

    if-eqz v1, :cond_6

    .line 18883
    const/4 v1, 0x5

    iget-object v2, p0, Lhcb;->e:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18885
    :cond_6
    iget-object v1, p0, Lhcb;->f:Lhgz;

    if-eqz v1, :cond_7

    .line 18886
    const/4 v1, 0x6

    iget-object v2, p0, Lhcb;->f:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18888
    :cond_7
    iget-object v1, p0, Lhcb;->g:Lhvr;

    if-eqz v1, :cond_8

    .line 18889
    const/4 v1, 0x7

    iget-object v2, p0, Lhcb;->g:Lhvr;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18891
    :cond_8
    iget-object v1, p0, Lhcb;->h:Lhoi;

    if-eqz v1, :cond_9

    .line 18892
    const/16 v1, 0x8

    iget-object v2, p0, Lhcb;->h:Lhoi;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18894
    :cond_9
    iget-object v1, p0, Lhcb;->i:Lhqd;

    if-eqz v1, :cond_a

    .line 18895
    const/16 v1, 0x9

    iget-object v2, p0, Lhcb;->i:Lhqd;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18897
    :cond_a
    iget-object v1, p0, Lhcb;->j:Lhyo;

    if-eqz v1, :cond_b

    .line 18898
    const/16 v1, 0xb

    iget-object v2, p0, Lhcb;->j:Lhyo;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18900
    :cond_b
    iget-object v1, p0, Lhcb;->k:Lhog;

    if-eqz v1, :cond_c

    .line 18901
    const/16 v1, 0xc

    iget-object v2, p0, Lhcb;->k:Lhog;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 18903
    :cond_c
    iget-object v1, p0, Lhcb;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 18904
    const/16 v1, 0xd

    iget-object v2, p0, Lhcb;->l:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 18906
    :cond_d
    iget-object v1, p0, Lhcb;->m:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 18907
    const/16 v1, 0xe

    iget-object v2, p0, Lhcb;->m:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    .line 18909
    :cond_e
    iget-boolean v1, p0, Lhcb;->n:Z

    if-eqz v1, :cond_f

    .line 18910
    const/16 v1, 0xf

    iget-boolean v2, p0, Lhcb;->n:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    .line 18912
    :cond_f
    iget-object v1, p0, Lhcb;->o:[Lhut;

    if-eqz v1, :cond_11

    .line 18913
    iget-object v1, p0, Lhcb;->o:[Lhut;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_11

    aget-object v3, v1, v0

    .line 18914
    if-eqz v3, :cond_10

    .line 18915
    const/16 v4, 0x10

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 18913
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 18919
    :cond_11
    iget-object v0, p0, Lhcb;->p:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 18920
    const/16 v0, 0x11

    iget-object v1, p0, Lhcb;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 18922
    :cond_12
    iget-object v0, p0, Lhcb;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 18924
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 18800
    if-ne p1, p0, :cond_1

    .line 18819
    :cond_0
    :goto_0
    return v0

    .line 18801
    :cond_1
    instance-of v2, p1, Lhcb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 18802
    :cond_2
    check-cast p1, Lhcb;

    .line 18803
    iget-object v2, p0, Lhcb;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcb;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhcb;->b:[Lgxq;

    iget-object v3, p1, Lhcb;->b:[Lgxq;

    .line 18804
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcb;->c:[Lgxq;

    iget-object v3, p1, Lhcb;->c:[Lgxq;

    .line 18805
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcb;->d:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhcb;->d:Lhgz;

    if-nez v2, :cond_3

    .line 18806
    :goto_2
    iget-object v2, p0, Lhcb;->e:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhcb;->e:Lhgz;

    if-nez v2, :cond_3

    .line 18807
    :goto_3
    iget-object v2, p0, Lhcb;->f:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhcb;->f:Lhgz;

    if-nez v2, :cond_3

    .line 18808
    :goto_4
    iget-object v2, p0, Lhcb;->g:Lhvr;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhcb;->g:Lhvr;

    if-nez v2, :cond_3

    .line 18809
    :goto_5
    iget-object v2, p0, Lhcb;->h:Lhoi;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhcb;->h:Lhoi;

    if-nez v2, :cond_3

    .line 18810
    :goto_6
    iget-object v2, p0, Lhcb;->i:Lhqd;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhcb;->i:Lhqd;

    if-nez v2, :cond_3

    .line 18811
    :goto_7
    iget-object v2, p0, Lhcb;->j:Lhyo;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhcb;->j:Lhyo;

    if-nez v2, :cond_3

    .line 18812
    :goto_8
    iget-object v2, p0, Lhcb;->k:Lhog;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhcb;->k:Lhog;

    if-nez v2, :cond_3

    .line 18813
    :goto_9
    iget-object v2, p0, Lhcb;->l:Ljava/lang/String;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhcb;->l:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 18814
    :goto_a
    iget-object v2, p0, Lhcb;->m:Ljava/lang/String;

    if-nez v2, :cond_e

    iget-object v2, p1, Lhcb;->m:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 18815
    :goto_b
    iget-boolean v2, p0, Lhcb;->n:Z

    iget-boolean v3, p1, Lhcb;->n:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhcb;->o:[Lhut;

    iget-object v3, p1, Lhcb;->o:[Lhut;

    .line 18817
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhcb;->p:Ljava/lang/String;

    if-nez v2, :cond_f

    iget-object v2, p1, Lhcb;->p:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 18818
    :goto_c
    iget-object v2, p0, Lhcb;->I:Ljava/util/List;

    if-nez v2, :cond_10

    iget-object v2, p1, Lhcb;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 18819
    goto/16 :goto_0

    .line 18803
    :cond_4
    iget-object v2, p0, Lhcb;->a:Lhgz;

    iget-object v3, p1, Lhcb;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    .line 18805
    :cond_5
    iget-object v2, p0, Lhcb;->d:Lhgz;

    iget-object v3, p1, Lhcb;->d:Lhgz;

    .line 18806
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_2

    :cond_6
    iget-object v2, p0, Lhcb;->e:Lhgz;

    iget-object v3, p1, Lhcb;->e:Lhgz;

    .line 18807
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_3

    :cond_7
    iget-object v2, p0, Lhcb;->f:Lhgz;

    iget-object v3, p1, Lhcb;->f:Lhgz;

    .line 18808
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_4

    :cond_8
    iget-object v2, p0, Lhcb;->g:Lhvr;

    iget-object v3, p1, Lhcb;->g:Lhvr;

    .line 18809
    invoke-virtual {v2, v3}, Lhvr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_5

    :cond_9
    iget-object v2, p0, Lhcb;->h:Lhoi;

    iget-object v3, p1, Lhcb;->h:Lhoi;

    .line 18810
    invoke-virtual {v2, v3}, Lhoi;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_6

    :cond_a
    iget-object v2, p0, Lhcb;->i:Lhqd;

    iget-object v3, p1, Lhcb;->i:Lhqd;

    .line 18811
    invoke-virtual {v2, v3}, Lhqd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_7

    :cond_b
    iget-object v2, p0, Lhcb;->j:Lhyo;

    iget-object v3, p1, Lhcb;->j:Lhyo;

    .line 18812
    invoke-virtual {v2, v3}, Lhyo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_8

    :cond_c
    iget-object v2, p0, Lhcb;->k:Lhog;

    iget-object v3, p1, Lhcb;->k:Lhog;

    .line 18813
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_9

    :cond_d
    iget-object v2, p0, Lhcb;->l:Ljava/lang/String;

    iget-object v3, p1, Lhcb;->l:Ljava/lang/String;

    .line 18814
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_a

    :cond_e
    iget-object v2, p0, Lhcb;->m:Ljava/lang/String;

    iget-object v3, p1, Lhcb;->m:Ljava/lang/String;

    .line 18815
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_b

    .line 18817
    :cond_f
    iget-object v2, p0, Lhcb;->p:Ljava/lang/String;

    iget-object v3, p1, Lhcb;->p:Ljava/lang/String;

    .line 18818
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_c

    :cond_10
    iget-object v2, p0, Lhcb;->I:Ljava/util/List;

    iget-object v3, p1, Lhcb;->I:Ljava/util/List;

    .line 18819
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 18823
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 18825
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->a:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 18826
    iget-object v2, p0, Lhcb;->b:[Lgxq;

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    .line 18832
    :cond_0
    iget-object v0, p0, Lhcb;->c:[Lgxq;

    if-nez v0, :cond_6

    mul-int/lit8 v2, v2, 0x1f

    .line 18838
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhcb;->d:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 18839
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->e:Lhgz;

    if-nez v0, :cond_9

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 18840
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->f:Lhgz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 18841
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->g:Lhvr;

    if-nez v0, :cond_b

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 18842
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->h:Lhoi;

    if-nez v0, :cond_c

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 18843
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->i:Lhqd;

    if-nez v0, :cond_d

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 18844
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->j:Lhyo;

    if-nez v0, :cond_e

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 18845
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->k:Lhog;

    if-nez v0, :cond_f

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 18846
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->l:Ljava/lang/String;

    if-nez v0, :cond_10

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 18847
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcb;->m:Ljava/lang/String;

    if-nez v0, :cond_11

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 18848
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhcb;->n:Z

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    :goto_b
    add-int/2addr v0, v2

    .line 18849
    iget-object v2, p0, Lhcb;->o:[Lhut;

    if-nez v2, :cond_13

    mul-int/lit8 v2, v0, 0x1f

    .line 18855
    :cond_2
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhcb;->p:Ljava/lang/String;

    if-nez v0, :cond_15

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 18856
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhcb;->I:Ljava/util/List;

    if-nez v2, :cond_16

    :goto_d
    add-int/2addr v0, v1

    .line 18857
    return v0

    .line 18825
    :cond_3
    iget-object v0, p0, Lhcb;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_0

    :cond_4
    move v2, v0

    move v0, v1

    .line 18828
    :goto_e
    iget-object v3, p0, Lhcb;->b:[Lgxq;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 18829
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhcb;->b:[Lgxq;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_f
    add-int/2addr v2, v3

    .line 18828
    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    .line 18829
    :cond_5
    iget-object v2, p0, Lhcb;->b:[Lgxq;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgxq;->hashCode()I

    move-result v2

    goto :goto_f

    :cond_6
    move v0, v1

    .line 18834
    :goto_10
    iget-object v3, p0, Lhcb;->c:[Lgxq;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 18835
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhcb;->c:[Lgxq;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_11
    add-int/2addr v2, v3

    .line 18834
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    .line 18835
    :cond_7
    iget-object v2, p0, Lhcb;->c:[Lgxq;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lgxq;->hashCode()I

    move-result v2

    goto :goto_11

    .line 18838
    :cond_8
    iget-object v0, p0, Lhcb;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 18839
    :cond_9
    iget-object v0, p0, Lhcb;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 18840
    :cond_a
    iget-object v0, p0, Lhcb;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 18841
    :cond_b
    iget-object v0, p0, Lhcb;->g:Lhvr;

    invoke-virtual {v0}, Lhvr;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 18842
    :cond_c
    iget-object v0, p0, Lhcb;->h:Lhoi;

    invoke-virtual {v0}, Lhoi;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 18843
    :cond_d
    iget-object v0, p0, Lhcb;->i:Lhqd;

    invoke-virtual {v0}, Lhqd;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 18844
    :cond_e
    iget-object v0, p0, Lhcb;->j:Lhyo;

    invoke-virtual {v0}, Lhyo;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 18845
    :cond_f
    iget-object v0, p0, Lhcb;->k:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 18846
    :cond_10
    iget-object v0, p0, Lhcb;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 18847
    :cond_11
    iget-object v0, p0, Lhcb;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 18848
    :cond_12
    const/4 v0, 0x2

    goto/16 :goto_b

    :cond_13
    move v2, v0

    move v0, v1

    .line 18851
    :goto_12
    iget-object v3, p0, Lhcb;->o:[Lhut;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 18852
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhcb;->o:[Lhut;

    aget-object v2, v2, v0

    if-nez v2, :cond_14

    move v2, v1

    :goto_13
    add-int/2addr v2, v3

    .line 18851
    add-int/lit8 v0, v0, 0x1

    goto :goto_12

    .line 18852
    :cond_14
    iget-object v2, p0, Lhcb;->o:[Lhut;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhut;->hashCode()I

    move-result v2

    goto :goto_13

    .line 18855
    :cond_15
    iget-object v0, p0, Lhcb;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 18856
    :cond_16
    iget-object v1, p0, Lhcb;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto/16 :goto_d
.end method
