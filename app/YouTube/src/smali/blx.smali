.class final Lblx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lgjm;

.field private synthetic b:Lblv;


# direct methods
.method constructor <init>(Lblv;Lgjm;)V
    .locals 0

    .prologue
    .line 1360
    iput-object p1, p0, Lblx;->b:Lblv;

    iput-object p2, p0, Lblx;->a:Lgjm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1363
    iget-object v0, p0, Lblx;->a:Lgjm;

    invoke-static {v0}, La;->c(Lgjm;)Ljava/lang/String;

    move-result-object v1

    .line 1366
    iget-object v0, p0, Lblx;->b:Lblv;

    iget-object v0, v0, Lblv;->a:Lbla;

    iget-object v0, v0, Lbla;->i:Lgml;

    iget-object v2, p0, Lblx;->a:Lgjm;

    invoke-virtual {v0, v1, v2}, Lgml;->a(Ljava/lang/String;Lgjm;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1367
    iget-object v0, p0, Lblx;->a:Lgjm;

    iget-object v0, v0, Lgjm;->c:Lgjn;

    sget-object v2, Lgjn;->c:Lgjn;

    if-ne v0, v2, :cond_2

    .line 1368
    iget-object v0, p0, Lblx;->b:Lblv;

    iget-object v0, v0, Lblv;->a:Lbla;

    invoke-virtual {v0, v1}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Lbla;->b(Ljava/lang/String;)Lgmb;

    move-result-object v3

    iget-object v3, v3, Lgmb;->f:Lglv;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x16

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "pudl event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " complete: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lezp;->d(Ljava/lang/String;)V

    new-instance v3, Lbma;

    invoke-direct {v3, v2}, Lbma;-><init>(Lgmb;)V

    invoke-virtual {v0, v3}, Lbla;->a(Ljava/lang/Object;)V

    .line 1381
    :cond_0
    :goto_0
    iget-object v0, p0, Lblx;->b:Lblv;

    iget-object v0, v0, Lblv;->a:Lbla;

    .line 1382
    iget-object v0, v0, Lbla;->m:Lbmd;

    invoke-virtual {v0, v1}, Lbmd;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1383
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbme;

    .line 1384
    iget-object v2, p0, Lblx;->a:Lgjm;

    invoke-virtual {v0, v2}, Lbme;->a(Lgjm;)V

    .line 1387
    invoke-virtual {v0}, Lbme;->a()I

    move-result v2

    if-nez v2, :cond_1

    .line 1388
    iget-object v2, p0, Lblx;->b:Lblv;

    iget-object v2, v2, Lblv;->a:Lbla;

    iget-object v2, v2, Lbla;->m:Lbmd;

    iget-object v0, v0, Lbme;->a:Lgbu;

    iget-object v0, v0, Lgbu;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lbmd;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 1370
    :cond_2
    iget-object v0, p0, Lblx;->a:Lgjm;

    iget-object v0, v0, Lgjm;->c:Lgjn;

    sget-object v2, Lgjn;->b:Lgjn;

    if-ne v0, v2, :cond_3

    .line 1374
    iget-object v0, p0, Lblx;->b:Lblv;

    iget-object v0, v0, Lblv;->a:Lbla;

    iget-object v0, v0, Lbla;->a:Lbjn;

    iget-object v2, v0, Lbjn;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, v0, Lbjn;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lba;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lba;->a(J)Lba;

    .line 1376
    :cond_3
    iget-object v0, p0, Lblx;->b:Lblv;

    iget-object v0, v0, Lblv;->a:Lbla;

    invoke-virtual {v0, v1}, Lbla;->y(Ljava/lang/String;)V

    goto :goto_0

    .line 1392
    :cond_4
    return-void
.end method
