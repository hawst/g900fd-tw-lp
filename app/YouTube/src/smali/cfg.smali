.class public final Lcfg;
.super Lfsb;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

.field private final b:Lfsj;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfsj;Lfhz;Lfdw;Lfrz;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0, p3, p2, p4, p5}, Lfsb;-><init>(Lfhz;Lfsj;Lfdw;Lfrz;)V

    .line 37
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcfg;->b:Lfsj;

    .line 39
    const v0, 0x7f040118

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iput-object v0, p0, Lcfg;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 40
    iget-object v0, p0, Lcfg;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    invoke-interface {p2, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 41
    return-void
.end method

.method private a(Lfsg;Lfku;)Landroid/view/View;
    .locals 2

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 46
    iget-object v0, p0, Lcfg;->a:Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    iget-object v1, p2, Lfku;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_0

    iget-object v1, p2, Lfku;->a:Lhnv;

    iget-object v1, v1, Lhnv;->a:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfku;->b:Ljava/lang/CharSequence;

    :cond_0
    iget-object v1, p2, Lfku;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v0, p0, Lcfg;->b:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfku;

    invoke-direct {p0, p1, p2}, Lcfg;->a(Lfsg;Lfku;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfku;

    invoke-direct {p0, p1, p2}, Lcfg;->a(Lfsg;Lfku;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    check-cast p2, Lfku;

    invoke-direct {p0, p1, p2}, Lcfg;->a(Lfsg;Lfku;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
