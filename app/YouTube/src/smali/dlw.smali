.class final Ldlw;
.super Lfm;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private synthetic c:Ldls;


# direct methods
.method public constructor <init>(Ldls;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 248
    iput-object p1, p0, Ldlw;->c:Ldls;

    invoke-direct {p0}, Lfm;-><init>()V

    .line 249
    iput-object p2, p0, Ldlw;->a:Ljava/util/List;

    .line 250
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldlw;->b:Ljava/util/List;

    .line 251
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 252
    iget-object v1, p0, Ldlw;->b:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 323
    const/4 v0, -0x2

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 277
    iget-object v0, p0, Ldlw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 312
    :cond_0
    :goto_0
    return-object v3

    .line 280
    :cond_1
    iget-object v0, p0, Ldlw;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkf;

    .line 281
    invoke-virtual {v0}, Lfkf;->b()Lfmv;

    move-result-object v6

    .line 282
    invoke-virtual {v6}, Lfmv;->b()Lfnc;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 283
    invoke-virtual {v6}, Lfmv;->a()Lfmt;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 284
    invoke-virtual {v6}, Lfmv;->c()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 287
    invoke-virtual {v0}, Lfkf;->c()Lfmt;

    move-result-object v7

    .line 291
    invoke-virtual {v6}, Lfmv;->b()Lfnc;

    move-result-object v0

    if-nez v0, :cond_4

    .line 292
    const-string v0, "Image is missing in info card\'s content"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move-object v8, v3

    .line 300
    :goto_1
    new-instance v0, Ldly;

    iget-object v1, p0, Ldlw;->c:Ldls;

    .line 301
    invoke-virtual {v1}, Ldls;->j()Lo;

    move-result-object v1

    iget-object v2, p0, Ldlw;->c:Ldls;

    iget-object v4, p0, Ldlw;->c:Ldls;

    .line 304
    invoke-static {v4}, Ldls;->d(Ldls;)Lxk;

    move-result-object v4

    .line 305
    invoke-virtual {v6}, Lfmv;->c()Ljava/lang/CharSequence;

    move-result-object v5

    .line 306
    iget-object v9, v6, Lfmv;->b:Ljava/lang/CharSequence;

    if-nez v9, :cond_2

    iget-object v9, v6, Lfmv;->a:Lhvy;

    iget-object v9, v9, Lhvy;->c:Lhgz;

    if-eqz v9, :cond_2

    iget-object v9, v6, Lfmv;->a:Lhvy;

    iget-object v9, v9, Lhvy;->c:Lhgz;

    invoke-static {v9}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v9

    iput-object v9, v6, Lfmv;->b:Ljava/lang/CharSequence;

    :cond_2
    iget-object v6, v6, Lfmv;->b:Ljava/lang/CharSequence;

    if-eqz v7, :cond_5

    .line 307
    iget-object v3, v7, Lfmt;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_3

    iget-object v3, v7, Lfmt;->a:Lhvw;

    iget-object v3, v3, Lhvw;->a:Lhgz;

    if-eqz v3, :cond_3

    iget-object v3, v7, Lfmt;->a:Lhvw;

    iget-object v3, v3, Lhvw;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v7, Lfmt;->b:Ljava/lang/CharSequence;

    :cond_3
    iget-object v7, v7, Lfmt;->b:Ljava/lang/CharSequence;

    :goto_2
    move v3, p2

    invoke-direct/range {v0 .. v8}, Ldly;-><init>(Landroid/content/Context;Ldma;ILxk;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    .line 310
    iget-object v1, v0, Ldly;->a:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 311
    iget-object v1, p0, Ldlw;->b:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v3, v0, Ldly;->a:Landroid/view/View;

    goto/16 :goto_0

    .line 294
    :cond_4
    invoke-virtual {v6}, Lfmv;->b()Lfnc;

    move-result-object v0

    iget-object v0, v0, Lfnc;->b:Ljava/util/List;

    .line 295
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 296
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnb;

    iget-object v0, v0, Lfnb;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    :cond_5
    move-object v7, v3

    .line 307
    goto :goto_2

    :cond_6
    move-object v8, v3

    goto :goto_1
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 317
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 318
    iget-object v0, p0, Ldlw;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 319
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Ldlw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldly;

    .line 258
    if-eqz v0, :cond_0

    .line 259
    iget-object v2, v0, Ldly;->b:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 262
    :cond_2
    invoke-virtual {p0}, Ldlw;->c()V

    .line 263
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 272
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Ldlw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
