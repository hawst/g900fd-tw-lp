.class public La;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lal;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static b:Labu;

.field public static c:Z

.field public static d:Z

.field public static e:Z

.field public static f:J

.field public static g:J

.field public static h:Landroid/view/View$AccessibilityDelegate;

.field public static i:Lorg/apache/http/client/HttpClient;

.field public static j:Lorg/apache/http/client/HttpClient;

.field public static k:Lorg/apache/http/client/HttpClient;

.field public static l:Ljava/lang/String;

.field public static m:Lezn;

.field public static n:I

.field public static o:Ljava/lang/String;

.field public static p:I

.field public static volatile q:Ljava/lang/String;

.field public static volatile r:Ljava/lang/Boolean;

.field public static volatile s:Ljava/lang/Boolean;

.field public static volatile t:Ljava/lang/String;

.field public static volatile u:Ljava/lang/Integer;


# instance fields
.field final synthetic a:Lj;


# direct methods
.method constructor <init>(Lc;Lj;)V
    .locals 0

    .prologue
    .line 1186
    iput-object p2, p0, La;->a:Lj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static A()Ljava/lang/String;
    .locals 1

    const-string v0, "1lvTXic8n3R7JEjgipB5q7vdz0HYYFQ3eI4rvSKT9fQ="

    return-object v0
.end method

.method public static A(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static B()Ljava/lang/String;
    .locals 1

    const-string v0, "jlEcTd2rIYHNtjOJXeVHSY0waWduxdbpsAmB61jhLKM3J/X8S64pKP93o8lehC2U"

    return-object v0
.end method

.method public static B(Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 143
    :try_start_0
    const-string v0, "utf8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 144
    :catch_0
    move-exception v0

    .line 146
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static C(Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 154
    .line 159
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 160
    array-length v5, v3

    .line 163
    if-nez v5, :cond_0

    move-object v2, p0

    .line 166
    :goto_0
    const/4 v0, 0x2

    if-lt v5, v0, :cond_8

    .line 168
    add-int/lit8 v0, v5, -0x2

    :try_start_0
    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v4, v0

    .line 174
    :goto_1
    const/4 v0, 0x3

    if-lt v5, v0, :cond_7

    .line 176
    add-int/lit8 v0, v5, -0x3

    :try_start_1
    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 184
    :goto_2
    const-string v3, "\\."

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 185
    array-length v3, v5

    if-le v3, v7, :cond_5

    .line 187
    const/4 v2, 0x0

    :try_start_2
    aget-object v2, v5, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v3

    .line 193
    :goto_3
    const/4 v2, 0x1

    :try_start_3
    aget-object v2, v5, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v1

    move v2, v3

    .line 207
    :goto_4
    const v3, 0x36ee80

    mul-int/2addr v0, v3

    const v3, 0xea60

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    mul-int/lit16 v2, v2, 0x3e8

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0

    .line 163
    :cond_0
    add-int/lit8 v0, v5, -0x1

    aget-object v0, v3, v0

    move-object v2, v0

    goto :goto_0

    .line 170
    :catch_0
    move-exception v0

    const-string v4, "error parsing minutes: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    move v4, v1

    .line 171
    goto :goto_1

    .line 170
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 178
    :catch_1
    move-exception v0

    const-string v3, "error parsing hours: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    move v0, v1

    .line 179
    goto :goto_2

    .line 178
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 189
    :catch_2
    move-exception v2

    const-string v3, "error parsing seconds: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_7
    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V

    move v3, v1

    .line 190
    goto :goto_3

    .line 189
    :cond_3
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_7

    .line 195
    :catch_3
    move-exception v2

    const-string v5, "error parsing milliseconds: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_8
    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V

    move v2, v3

    .line 197
    goto :goto_4

    .line 195
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    .line 200
    :cond_5
    :try_start_4
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    move-result v2

    goto/16 :goto_4

    .line 202
    :catch_4
    move-exception v3

    const-string v3, "error parsing seconds: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_9
    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V

    move v2, v1

    .line 203
    goto/16 :goto_4

    .line 202
    :cond_6
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_9

    :cond_7
    move v0, v1

    goto/16 :goto_2

    :cond_8
    move v4, v1

    goto/16 :goto_1
.end method

.method public static C()Ljava/lang/String;
    .locals 1

    const-string v0, "lpxMXoKWdChv8Zrq7NbZEHaw06Cqv88mVVhozulIi9E="

    return-object v0
.end method

.method public static D(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 33
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static D()Ljava/lang/String;
    .locals 1

    const-string v0, "ubB2mzU1bFqDkuDzq0sZsy5gTUocAqqB2dNG1iii3lWzkYnY52nQ5klBR3XLwNL8"

    return-object v0
.end method

.method public static E(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 88
    invoke-static {p0}, La;->F(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-static {v0}, La;->G(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static E()Ljava/lang/String;
    .locals 1

    const-string v0, "SyRjv4WYiA6ZWBzCfu5oNPjxfQ3TS9ELKWyLIoXKw88="

    return-object v0
.end method

.method public static F()Ljava/lang/String;
    .locals 1

    const-string v0, "x/c7MN7jOYlvQanQaA5kQ24VOO0cWRdM+3FsUpc5WCSkluZU++04QU+SPpASlM4c"

    return-object v0
.end method

.method public static F(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 94
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 96
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    .line 103
    :try_start_1
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v2, "%,;:$&+=@[]"

    invoke-static {v0, v2}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 104
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "%,;:$&+=/@"

    invoke-static {v2, v3}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v3

    const-string v4, "%,;:$&+=/[]@?"

    invoke-static {v3, v4}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 106
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    .line 107
    invoke-virtual {v4, v0}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 109
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    move-object p0, v0

    .line 113
    goto :goto_0

    :catch_1
    move-exception v0

    .line 119
    :try_start_2
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v0

    const-string v2, ",;:$&+=@[]"

    invoke-static {v0, v2}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, ",;:$&+=/@"

    invoke-static {v2, v3}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v3

    const-string v4, ",;:$&+=/@[]?"

    invoke-static {v3, v4}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 122
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 123
    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 124
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_2

    move-object p0, v0

    .line 129
    goto :goto_0

    .line 132
    :catch_2
    move-exception v0

    const-string v0, ":/"

    invoke-static {p0, v0}, Landroid/net/Uri;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0
.end method

.method public static G(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 138
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v1

    if-nez v1, :cond_0

    .line 140
    new-instance v0, Ljava/net/MalformedURLException;

    const-string v1, "Uri must have an absolute scheme"

    invoke-direct {v0, v1}, Ljava/net/MalformedURLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    return-object v0
.end method

.method public static G()Ljava/lang/String;
    .locals 1

    const-string v0, "vvMYIcVorTU4oAddtqAIlXJgjNIJa8QVKq9612ASc1o="

    return-object v0
.end method

.method public static H()Ljava/lang/String;
    .locals 1

    const-string v0, "6IoSJDu5YS+GiM9TLtm7+1wlH+WgGGjJALXDzx/p53YFoQ2QIFAw2DuEpBbGX4YM"

    return-object v0
.end method

.method public static H(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    invoke-static {p0}, La;->F(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-static {v0}, La;->G(Ljava/lang/String;)Landroid/net/Uri;

    .line 151
    return-object v0
.end method

.method public static I(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 274
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    .line 275
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "www.youtube.com"

    .line 276
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "watch"

    .line 277
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "v"

    .line 278
    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static I()Ljava/lang/String;
    .locals 1

    const-string v0, "AwYgqnENj21N72E08XR4MNWn/iMPoCZb222U80unU2g="

    return-object v0
.end method

.method public static J(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 283
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    .line 284
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "www.youtube.com"

    .line 285
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "playlist"

    .line 286
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "list"

    .line 287
    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 288
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static J()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public static K()I
    .locals 1

    .prologue
    .line 22
    sget v0, La;->n:I

    if-nez v0, :cond_0

    .line 23
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, La;->n:I

    .line 25
    :cond_0
    sget v0, La;->n:I

    return v0
.end method

.method public static K(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 296
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "http"

    .line 297
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "www.youtube.com"

    .line 298
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static L()J
    .locals 2

    .prologue
    .line 36
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    const-wide/16 v0, 0x0

    .line 39
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, La;->f(Ljava/io/File;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static L(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 313
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static M()I
    .locals 2

    .prologue
    .line 102
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v0

    long-to-int v0, v0

    div-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public static M(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 321
    if-nez p0, :cond_1

    .line 322
    const/4 v0, 0x0

    .line 332
    :cond_0
    :goto_0
    return-object v0

    .line 328
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 329
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static N()Lfba;
    .locals 4

    .prologue
    .line 52
    new-instance v0, Lfbb;

    invoke-direct {v0}, Lfbb;-><init>()V

    .line 53
    const-string v1, "/transcript"

    new-instance v2, Lgow;

    invoke-direct {v2}, Lgow;-><init>()V

    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    const-string v2, "/transcript/text"

    new-instance v3, Lgov;

    invoke-direct {v3}, Lgov;-><init>()V

    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 54
    const-string v1, "/timedtext"

    new-instance v2, Lgoz;

    invoke-direct {v2}, Lgoz;-><init>()V

    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    const-string v2, "/timedtext/window"

    new-instance v3, Lgoy;

    invoke-direct {v3}, Lgoy;-><init>()V

    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    const-string v2, "/timedtext/text"

    new-instance v3, Lgox;

    invoke-direct {v3}, Lgox;-><init>()V

    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 55
    invoke-virtual {v0}, Lfbb;->a()Lfba;

    move-result-object v0

    return-object v0
.end method

.method public static N(Ljava/lang/String;)Lhgz;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 421
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    new-array v0, v1, [Ljava/lang/String;

    invoke-static {v0}, Lfvo;->a([Ljava/lang/String;)Lhgz;

    move-result-object v0

    .line 424
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    aput-object p0, v0, v1

    invoke-static {v0}, Lfvo;->a([Ljava/lang/String;)Lhgz;

    move-result-object v0

    goto :goto_0
.end method

.method public static O()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    .line 27
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "text/plain"

    .line 28
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x80000

    .line 29
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static O(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 193
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 194
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 195
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 196
    if-nez v0, :cond_0

    .line 197
    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 194
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_0
    invoke-static {v2}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 199
    const/16 v3, 0x5f

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 201
    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 204
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static P(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 222
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 223
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    move v0, v1

    .line 224
    :goto_0
    if-ge v0, v2, :cond_1

    .line 225
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 226
    const/16 v5, 0x20

    if-lt v4, v5, :cond_0

    const/16 v5, 0x7e

    if-gt v4, v5, :cond_0

    const/16 v5, 0x22

    if-eq v4, v5, :cond_0

    const/16 v5, 0x27

    if-eq v4, v5, :cond_0

    .line 227
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 224
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    const-string v5, "\\u%04x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 232
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static Q(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 54
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 55
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 56
    const v0, 0x7fffffff

    .line 63
    :goto_0
    return v0

    .line 57
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 58
    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_1
    long-to-int v0, v0

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(FFFF)F
    .locals 2

    .prologue
    .line 33
    sub-float v0, p0, p2

    .line 34
    sub-float v1, p1, p3

    .line 35
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(IIII)F
    .locals 2

    .prologue
    .line 39
    sub-int v0, p0, p2

    .line 40
    sub-int v1, p1, p3

    .line 41
    mul-int/2addr v0, v0

    mul-int/2addr v1, v1

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(Ljava/lang/String;F)F
    .locals 1

    .prologue
    .line 177
    if-eqz p0, :cond_0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 179
    :cond_0
    :goto_0
    return p1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(B)I
    .locals 0

    .prologue
    .line 69
    if-gez p0, :cond_0

    add-int/lit16 p0, p0, 0x100

    :cond_0
    return p0
.end method

.method public static synthetic a(F)I
    .locals 1

    .prologue
    .line 40
    const/high16 v0, 0x447a0000    # 1000.0f

    mul-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method

.method public static a(I)I
    .locals 8

    .prologue
    const/16 v7, 0xff

    const/4 v6, 0x0

    const-wide v4, 0x3fe9999998000000L    # 0.7999999970197678

    .line 35
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-double v2, v1

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v7, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    invoke-static {v3, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/app/Activity;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v0, -0x1000000

    .line 26
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 27
    invoke-virtual {p0}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010451

    aput v3, v2, v4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v4, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 30
    :cond_0
    return v0
.end method

.method public static a(Landroid/content/ContentResolver;)I
    .locals 2

    .prologue
    .line 65
    const-string v0, "youtube:device_lowend"

    const/4 v1, 0x2

    invoke-static {p0, v0, v1}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;I)I
    .locals 3

    .prologue
    .line 40
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 41
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;)I
    .locals 5

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {p0, v0}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    const v3, 0xffff

    and-int/2addr v3, v0

    const/16 v4, 0x4f45

    if-eq v3, v4, :cond_0

    new-instance v1, Ll;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected object header. Got 0x"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Ll;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v1

    :cond_0
    add-int v0, v2, v1

    if-lt v0, v2, :cond_1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataSize()I

    move-result v1

    if-le v0, v1, :cond_2

    :cond_1
    new-instance v1, Ll;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Size read is invalid start="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " end="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Ll;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v1

    :cond_2
    return v0
.end method

.method public static a(Landroid/os/Parcel;I)I
    .locals 2

    const/high16 v1, -0x10000

    and-int v0, p1, v1

    if-eq v0, v1, :cond_0

    shr-int/lit8 v0, p1, 0x10

    const v1, 0xffff

    and-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Landroid/util/DisplayMetrics;I)I
    .locals 4

    .prologue
    .line 140
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    int-to-float v0, p1

    iget v1, p0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method public static a(Ljava/lang/String;ILjava/lang/String;)I
    .locals 2

    .prologue
    .line 26
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 27
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 28
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 31
    :cond_0
    return p1
.end method

.method public static a(Lmi;I)I
    .locals 1

    .prologue
    .line 20
    if-nez p0, :cond_1

    .line 31
    :cond_0
    :goto_0
    return p1

    .line 23
    :cond_1
    iget-object v0, p0, Lmi;->a:Lml;

    .line 24
    if-eqz v0, :cond_2

    .line 25
    iget p1, v0, Lml;->a:I

    goto :goto_0

    .line 27
    :cond_2
    iget-object v0, p0, Lmi;->b:Lml;

    .line 28
    if-eqz v0, :cond_0

    .line 29
    iget p1, v0, Lml;->a:I

    goto :goto_0
.end method

.method public static a([F)I
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/16 v8, 0xff

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/high16 v7, 0x437f0000    # 255.0f

    .line 181
    aget v0, p0, v1

    .line 182
    const/4 v2, 0x1

    aget v2, p0, v2

    .line 183
    const/4 v3, 0x2

    aget v3, p0, v3

    .line 185
    mul-float v4, v3, v9

    sub-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float v4, v6, v4

    mul-float/2addr v4, v2

    .line 186
    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v2, v4

    sub-float v5, v3, v2

    .line 187
    const/high16 v2, 0x42700000    # 60.0f

    div-float v2, v0, v2

    rem-float/2addr v2, v9

    sub-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    sub-float v2, v6, v2

    mul-float v6, v4, v2

    .line 189
    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x3c

    .line 193
    packed-switch v0, :pswitch_data_0

    move v0, v1

    move v2, v1

    move v3, v1

    .line 227
    :goto_0
    invoke-static {v8, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 228
    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 229
    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 231
    invoke-static {v3, v2, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0

    .line 195
    :pswitch_0
    add-float v0, v4, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 196
    add-float v0, v6, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 197
    mul-float v0, v7, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 200
    :pswitch_1
    add-float v0, v6, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 201
    add-float v0, v4, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 202
    mul-float v0, v7, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 205
    :pswitch_2
    mul-float v0, v7, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 206
    add-float v0, v4, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 207
    add-float v0, v6, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 210
    :pswitch_3
    mul-float v0, v7, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 211
    add-float v0, v6, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 212
    add-float v0, v4, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto :goto_0

    .line 215
    :pswitch_4
    add-float v0, v6, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 216
    mul-float v0, v7, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 217
    add-float v0, v4, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/16 :goto_0

    .line 221
    :pswitch_5
    add-float v0, v4, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 222
    mul-float v0, v7, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 223
    add-float v0, v6, v5

    mul-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    goto/16 :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public static a([IIZ)I
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    .line 65
    array-length v10, p0

    .line 67
    array-length v4, p0

    move v2, v7

    move v0, v7

    :goto_0
    if-ge v2, v4, :cond_0

    aget v3, p0, v2

    .line 68
    add-int/2addr v3, v0

    .line 67
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v8, v7

    move v2, v7

    move v9, v0

    move v0, v7

    .line 72
    :goto_1
    add-int/lit8 v3, v10, -0x1

    if-ge v8, v3, :cond_6

    .line 74
    shl-int v3, v1, v8

    or-int/2addr v0, v3

    move v3, v2

    move v2, v0

    move v0, v1

    .line 75
    :goto_2
    aget v4, p0, v8

    if-ge v0, v4, :cond_5

    .line 77
    sub-int v4, v9, v0

    add-int/lit8 v4, v4, -0x1

    sub-int v5, v10, v8

    add-int/lit8 v5, v5, -0x2

    invoke-static {v4, v5}, La;->c(II)I

    move-result v4

    .line 78
    if-eqz p2, :cond_1

    if-nez v2, :cond_1

    sub-int v5, v9, v0

    sub-int v6, v10, v8

    add-int/lit8 v6, v6, -0x1

    sub-int/2addr v5, v6

    sub-int v6, v10, v8

    add-int/lit8 v6, v6, -0x1

    if-lt v5, v6, :cond_1

    .line 80
    sub-int v5, v9, v0

    sub-int v6, v10, v8

    sub-int/2addr v5, v6

    sub-int v6, v10, v8

    add-int/lit8 v6, v6, -0x2

    invoke-static {v5, v6}, La;->c(II)I

    move-result v5

    sub-int/2addr v4, v5

    .line 83
    :cond_1
    sub-int v5, v10, v8

    add-int/lit8 v5, v5, -0x1

    if-le v5, v1, :cond_4

    .line 85
    sub-int v5, v9, v0

    sub-int v6, v10, v8

    add-int/lit8 v6, v6, -0x2

    sub-int/2addr v5, v6

    move v6, v7

    .line 86
    :goto_3
    if-le v5, p1, :cond_2

    .line 87
    sub-int v11, v9, v0

    sub-int/2addr v11, v5

    add-int/lit8 v11, v11, -0x1

    sub-int v12, v10, v8

    add-int/lit8 v12, v12, -0x3

    invoke-static {v11, v12}, La;->c(II)I

    move-result v11

    add-int/2addr v6, v11

    .line 86
    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    .line 90
    :cond_2
    add-int/lit8 v5, v10, -0x1

    sub-int/2addr v5, v8

    mul-int/2addr v5, v6

    sub-int/2addr v4, v5

    .line 94
    :cond_3
    :goto_4
    add-int/2addr v3, v4

    .line 76
    add-int/lit8 v0, v0, 0x1

    shl-int v4, v1, v8

    xor-int/lit8 v4, v4, -0x1

    and-int/2addr v2, v4

    goto :goto_2

    .line 91
    :cond_4
    sub-int v5, v9, v0

    if-le v5, p1, :cond_3

    .line 92
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .line 96
    :cond_5
    sub-int v4, v9, v0

    .line 72
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v9, v4

    move v0, v2

    move v2, v3

    goto :goto_1

    .line 98
    :cond_6
    return v2
.end method

.method public static synthetic a(J)J
    .locals 2

    .prologue
    .line 44
    const-wide v0, 0x7fffffffffffffffL

    sput-wide v0, La;->f:J

    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 6

    .prologue
    .line 52
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const/16 v0, 0x1000

    new-array v2, v0, [B

    .line 55
    const-wide/16 v0, 0x0

    .line 57
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .line 58
    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 59
    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 62
    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 63
    goto :goto_0

    .line 64
    :cond_0
    return-wide v0
.end method

.method public static a(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 110
    :try_start_0
    invoke-static {p0}, Lorg/apache/http/impl/cookie/DateUtils;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 113
    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;J)J
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 166
    if-eqz p0, :cond_0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 168
    :cond_0
    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static a(Ljava/nio/ByteBuffer;)J
    .locals 4

    .prologue
    .line 35
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    int-to-long v0, v0

    .line 36
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 37
    const-wide v2, 0x100000000L

    add-long/2addr v0, v2

    .line 39
    :cond_0
    return-wide v0
.end method

.method public static a(Ljava/util/Date;)J
    .locals 4

    .prologue
    .line 42
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/32 v2, 0x7c25b080

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static a(Landroid/content/Context;Labm;)Labh;
    .locals 4

    :try_start_0
    iget-boolean v0, p1, Labm;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Remote exception"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_2
    invoke-virtual {p0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3

    :goto_0
    throw v0

    :cond_0
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p1, Labm;->a:Z

    iget-object v0, p1, Labm;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    invoke-static {v0}, Labp;->a(Landroid/os/IBinder;)Labo;

    move-result-object v0

    new-instance v1, Labh;

    invoke-interface {v0}, Labo;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v0, v3}, Labo;->a(Z)Z

    move-result v0

    invoke-direct {v1, v2, v0}, Labh;-><init>(Ljava/lang/String;Z)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {p0, p1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_1
    return-object v1

    :catch_1
    move-exception v0

    :try_start_5
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Interrupted exception"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Labm;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.android.vending"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {p0}, Labk;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Labi; {:try_start_1 .. :try_end_1} :catch_1

    new-instance v0, Labm;

    invoke-direct {v0}, Labm;-><init>()V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Labi;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Labi;-><init>(I)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Connection failure"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;Z)Landroid/content/Context;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 30
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x7f010045

    invoke-virtual {v3, v4, v2, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v2, v2, Landroid/util/TypedValue;->data:I

    if-eqz v2, :cond_0

    .line 31
    :goto_0
    if-eqz v1, :cond_2

    if-eqz p1, :cond_2

    .line 32
    new-instance v1, Landroid/view/ContextThemeWrapper;

    const v2, 0x7f0d00c7

    invoke-direct {v1, p0, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object p0, v1

    .line 35
    :goto_1
    new-instance v1, Landroid/view/ContextThemeWrapper;

    if-eqz v0, :cond_1

    const v0, 0x7f0d000c

    :goto_2
    invoke-direct {v1, p0, v0}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v1

    :cond_0
    move v1, v0

    .line 30
    goto :goto_0

    .line 35
    :cond_1
    const v0, 0x7f0d000b

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static a(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 222
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 223
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 224
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 226
    aget v2, v1, v4

    aget v3, v1, v6

    aget v4, v1, v4

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    aget v1, v1, v6

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v1, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 227
    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/util/Map;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 155
    invoke-virtual {p0}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v0

    .line 157
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    .line 158
    const-string v1, ""

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->query(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 160
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 161
    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 162
    invoke-virtual {p0, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 166
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 167
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_1

    .line 170
    :cond_2
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    .locals 2

    .prologue
    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 45
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, v0, p1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 51
    :goto_0
    return-object p0

    .line 48
    :cond_0
    invoke-virtual {p0, p1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static a(Z)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 34
    if-eqz p0, :cond_0

    .line 35
    const-string v1, "guide_entry"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 37
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Landroid/os/Parcelable;
    .locals 3

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lct;)Landroid/view/Menu;
    .locals 2

    .prologue
    .line 36
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 37
    new-instance v0, Lok;

    invoke-direct {v0, p0, p1}, Lok;-><init>(Landroid/content/Context;Lct;)V

    return-object v0

    .line 39
    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcu;)Landroid/view/MenuItem;
    .locals 2

    .prologue
    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 44
    new-instance v0, Loc;

    invoke-direct {v0, p0, p1}, Loc;-><init>(Landroid/content/Context;Lcu;)V

    .line 46
    :goto_0
    return-object v0

    .line 45
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 46
    new-instance v0, Lnx;

    invoke-direct {v0, p0, p1}, Lnx;-><init>(Landroid/content/Context;Lcu;)V

    goto :goto_0

    .line 48
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static a(Landroid/view/MenuItem;Lff;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lfg;

    invoke-direct {v0, p1}, Lfg;-><init>(Lff;)V

    invoke-interface {p0, v0}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Leyp;Lboi;Lboi;)Lawv;
    .locals 3

    .prologue
    .line 56
    new-instance v0, Lawz;

    invoke-direct {v0, p0}, Lawz;-><init>(Landroid/content/Context;)V

    sget-object v1, Laws;->a:Laws;

    invoke-static {p0, p1, v1}, Laxe;->a(Landroid/content/Context;Leyp;Laws;)Laxe;

    move-result-object v1

    new-instance v2, Lawg;

    invoke-direct {v2}, Lawg;-><init>()V

    invoke-virtual {v2, v0}, Lawg;->a(Lawo;)Lawg;

    move-result-object v0

    invoke-virtual {v0, v1}, Lawg;->a(Lawo;)Lawg;

    move-result-object v0

    new-instance v1, Lavx;

    invoke-direct {v1}, Lavx;-><init>()V

    invoke-virtual {v0, v1}, Lawg;->a(Lawo;)Lawg;

    move-result-object v0

    new-instance v1, Lawi;

    invoke-direct {v1, p2}, Lawi;-><init>(Lboi;)V

    invoke-virtual {v0, v1}, Lawg;->a(Lawo;)Lawg;

    move-result-object v0

    .line 61
    new-instance v1, Lawt;

    invoke-direct {v1, p0, p3}, Lawt;-><init>(Landroid/content/Context;Lboi;)V

    .line 64
    new-instance v2, Laww;

    invoke-direct {v2, v0, v1}, Laww;-><init>(Lawo;Lawt;)V

    .line 67
    new-instance v0, Lawv;

    const v1, 0x7f040122

    invoke-direct {v0, p0, v1, v2}, Lawv;-><init>(Landroid/content/Context;ILaww;)V

    .line 72
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;)Lbbv;
    .locals 2

    .prologue
    .line 29
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    iget-object v0, p1, Lhog;->A:Lhgd;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    iget-object v0, p1, Lhog;->A:Lhgd;

    iget-object v0, v0, Lhgd;->a:Ljava/lang/String;

    .line 32
    invoke-static {p0, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/EditVideoActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 33
    new-instance v1, Lbbm;

    invoke-direct {v1, p0, v0}, Lbbm;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Landroid/content/Intent;)V

    return-object v1
.end method

.method public static a(Landroid/os/Bundle;)Lbgh;
    .locals 3

    .prologue
    .line 66
    new-instance v0, Lbgh;

    const-class v1, Lbdp;

    invoke-direct {v0, v1, p0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 69
    new-instance v1, Lhog;

    invoke-direct {v1}, Lhog;-><init>()V

    .line 70
    new-instance v2, Lhpa;

    invoke-direct {v2}, Lhpa;-><init>()V

    iput-object v2, v1, Lhog;->s:Lhpa;

    .line 71
    invoke-virtual {v0, v1}, Lbgh;->a(Lhog;)V

    .line 72
    return-object v0
.end method

.method public static a(Lbdx;)Lbgh;
    .locals 3

    .prologue
    .line 130
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    iget-object v0, p0, Lj;->k:Landroid/os/Bundle;

    .line 132
    new-instance v1, Lbgh;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eqz v0, :cond_0

    :goto_0
    invoke-direct {v1, v2, v0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method

.method public static a(Lhog;)Lbgh;
    .locals 3

    .prologue
    .line 77
    new-instance v0, Lbgh;

    const-class v1, Lbdp;

    const/4 v2, 0x1

    .line 78
    invoke-static {v2}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {v0, p0}, Lbgh;->a(Lhog;)V

    .line 80
    return-object v0
.end method

.method public static a(Lhog;Lbnf;)Lbgh;
    .locals 1

    .prologue
    .line 103
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    iget-object v0, p0, Lhog;->g:Lhuh;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    iget-object v0, p0, Lhog;->g:Lhuh;

    iget-object v0, v0, Lhuh;->a:Ljava/lang/String;

    invoke-static {v0, p1}, La;->a(Ljava/lang/String;Lbnf;)Lbgh;

    move-result-object v0

    .line 109
    invoke-virtual {v0, p0}, Lbgh;->a(Lhog;)V

    .line 111
    return-object v0
.end method

.method public static a(Lhog;Z)Lbgh;
    .locals 3

    .prologue
    .line 151
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    invoke-static {p1}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 153
    new-instance v1, Lbgh;

    const-class v2, Lbci;

    invoke-direct {v1, v2, v0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 154
    invoke-virtual {v1, p0}, Lbgh;->a(Lhog;)V

    .line 155
    return-object v1
.end method

.method public static a(Lhuo;[B)Lbgh;
    .locals 3

    .prologue
    .line 138
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const/4 v0, 0x0

    invoke-static {v0}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 140
    const-string v1, "section_list_without_preview_proto"

    .line 141
    invoke-static {p0}, Lidh;->a(Lidh;)[B

    move-result-object v2

    .line 140
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 142
    if-eqz p1, :cond_0

    .line 143
    const-string v1, "section_list_without_preview_tracking_params"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 146
    :cond_0
    new-instance v1, Lbgh;

    const-class v2, Lbdd;

    invoke-direct {v1, v2, v0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;Lbnf;)Lbgh;
    .locals 3

    .prologue
    .line 93
    const-string v0, "query cannot be null"

    invoke-static {p0, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    const/4 v0, 0x0

    invoke-static {v0}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 95
    const-string v1, "search_query"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    const-string v1, "search_filters"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 97
    new-instance v1, Lbgh;

    const-class v2, Lbev;

    invoke-direct {v1, v2, v0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static a(Lbhz;Lfhz;)Lboi;
    .locals 3

    .prologue
    .line 596
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 598
    new-instance v1, Lbqc;

    const v2, 0x7f09032d

    invoke-direct {v1, p0, v2}, Lbqc;-><init>(Landroid/content/Context;I)V

    new-instance v2, Lbor;

    invoke-direct {v2, v1}, Lbor;-><init>(Lbqc;)V

    iput-object v2, v0, Lboi;->b:Lboq;

    new-instance v2, Lbpd;

    invoke-direct {v2, p1}, Lbpd;-><init>(Lfhz;)V

    invoke-virtual {v0, v1, v2}, Lboi;->a(Lboo;Lbop;)I

    .line 602
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lboi;Lbpw;Lfrz;)Lboi;
    .locals 21

    .prologue
    .line 671
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 672
    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v3

    .line 673
    iget-object v8, v2, Lckz;->a:Letc;

    .line 674
    invoke-virtual {v3}, Lari;->t()Lfeb;

    move-result-object v17

    .line 675
    iget-object v2, v2, Lckz;->a:Letc;

    invoke-virtual {v2}, Letc;->i()Levn;

    move-result-object v18

    .line 676
    invoke-virtual {v3}, Lari;->ay()Leyt;

    move-result-object v19

    .line 677
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v20, v0

    .line 678
    invoke-virtual {v3}, Lari;->O()Lgng;

    move-result-object v5

    .line 679
    invoke-virtual {v3}, Lari;->aD()Lcst;

    move-result-object v4

    .line 680
    new-instance v2, Lbjq;

    .line 685
    invoke-virtual {v3}, Lari;->aO()Lcub;

    move-result-object v6

    .line 686
    invoke-virtual {v3}, Lari;->ay()Leyt;

    move-result-object v7

    .line 687
    invoke-virtual {v8}, Letc;->b()Lexd;

    move-result-object v8

    .line 688
    invoke-virtual {v3}, Lari;->P()Lbjx;

    move-result-object v9

    .line 689
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f()Lbrz;

    move-result-object v10

    .line 690
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->g()Lbyg;

    move-result-object v11

    move-object/from16 v3, p0

    move-object/from16 v12, p3

    invoke-direct/range {v2 .. v12}, Lbjq;-><init>(Landroid/app/Activity;Lgix;Lgng;Lcub;Leyt;Lexd;Lbjx;Lbrz;Lbyg;Lfrz;)V

    .line 693
    new-instance v13, Lbqc;

    const v3, 0x7f090203

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v3}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 695
    new-instance v8, Lbqc;

    const v3, 0x7f09019f

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v3}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 697
    new-instance v9, Lbqc;

    const v3, 0x7f09019a

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v3}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 699
    new-instance v11, Lbqc;

    const v3, 0x7f09029b

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v3}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 701
    new-instance v12, Lbqc;

    const v3, 0x7f09029c

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v3}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 703
    new-instance v10, Lbqc;

    const v3, 0x7f0901ff

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v3}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 704
    new-instance v16, Lbqc;

    invoke-direct/range {v16 .. v16}, Lbqc;-><init>()V

    .line 706
    new-instance v6, Lbou;

    move-object/from16 v7, p2

    move-object v14, v5

    move-object v15, v4

    invoke-direct/range {v6 .. v16}, Lbou;-><init>(Lbpw;Lbqc;Lbqc;Lbqc;Lbqc;Lbqc;Lbqc;Lgng;Lgix;Lbqc;)V

    move-object/from16 v0, p1

    iput-object v6, v0, Lboi;->b:Lboq;

    .line 761
    new-instance v3, Lbov;

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-direct {v3, v0, v1}, Lbov;-><init>(Lbpw;Lfhz;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v3}, Lboi;->a(Lboo;Lbop;)I

    .line 772
    new-instance v3, Lbow;

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v2}, Lbow;-><init>(Lbpw;Lbjq;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v3}, Lboi;->a(Lboo;Lbop;)I

    .line 784
    new-instance v3, Lbox;

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v2}, Lbox;-><init>(Lbpw;Lbjq;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v3}, Lboi;->a(Lboo;Lbop;)I

    .line 795
    new-instance v2, Lboy;

    move-object/from16 v3, p2

    move-object/from16 v4, p0

    move-object/from16 v5, v18

    move-object/from16 v6, v19

    move-object/from16 v7, v17

    invoke-direct/range {v2 .. v7}, Lboy;-><init>(Lbpw;Lcom/google/android/apps/youtube/app/WatchWhileActivity;Levn;Leyt;Lfeb;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v2}, Lboi;->a(Lboo;Lbop;)I

    .line 828
    new-instance v2, Lbpa;

    move-object/from16 v3, p2

    move-object/from16 v4, p0

    move-object/from16 v5, v18

    move-object/from16 v6, v19

    move-object/from16 v7, v17

    invoke-direct/range {v2 .. v7}, Lbpa;-><init>(Lbpw;Lcom/google/android/apps/youtube/app/WatchWhileActivity;Levn;Leyt;Lfeb;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v2}, Lboi;->a(Lboo;Lbop;)I

    .line 863
    new-instance v2, Lbpc;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lbpc;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbpw;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v2}, Lboi;->a(Lboo;Lbop;)I

    .line 876
    new-instance v2, Lbpe;

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-direct {v2, v0, v1}, Lbpe;-><init>(Lbpw;Lfhz;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lboi;->a(Lboo;Lbop;)I

    .line 889
    return-object p1
.end method

.method public static a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 547
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 552
    invoke-static {}, Lbpp;->a()Lbpp;

    move-result-object v1

    .line 549
    invoke-static {p0, v0, v1, p1}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lboi;Lbpw;Lfrz;)Lboi;

    .line 554
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;
    .locals 25

    .prologue
    .line 325
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 326
    invoke-virtual {v3}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v4

    .line 327
    iget-object v0, v3, Lckz;->a:Letc;

    move-object/from16 v16, v0

    .line 328
    invoke-virtual {v4}, Lari;->aD()Lcst;

    move-result-object v10

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v22, v0

    .line 330
    invoke-virtual {v4}, Lari;->O()Lgng;

    move-result-object v23

    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h()Lbka;

    move-result-object v24

    .line 333
    new-instance v17, Lbwa;

    .line 335
    invoke-virtual {v4}, Lari;->b()Lfxe;

    move-result-object v3

    .line 336
    invoke-virtual {v4}, Lari;->ay()Leyt;

    move-result-object v5

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3, v5}, Lbwa;-><init>(Landroid/app/Activity;Lfxe;Leyt;)V

    .line 338
    new-instance v14, Lbpn;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p4

    invoke-direct {v14, v0, v1, v2}, Lbpn;-><init>(Lbhz;Lbka;Lfrz;)V

    .line 343
    new-instance v3, Lbym;

    .line 346
    invoke-virtual {v4}, Lari;->b()Lfxe;

    move-result-object v5

    .line 347
    invoke-virtual {v4}, Lari;->t()Lfeb;

    move-result-object v6

    .line 348
    invoke-virtual {v4}, Lari;->u()Lfew;

    move-result-object v7

    .line 349
    invoke-virtual {v4}, Lari;->aE()Lcuo;

    move-result-object v8

    .line 350
    invoke-virtual {v4}, Lari;->m()Lctk;

    move-result-object v9

    .line 352
    invoke-virtual {v4}, Lari;->aO()Lcub;

    move-result-object v11

    .line 353
    invoke-virtual {v4}, Lari;->ay()Leyt;

    move-result-object v12

    .line 354
    invoke-virtual/range {v16 .. v16}, Letc;->i()Levn;

    move-result-object v13

    .line 356
    iget-object v15, v4, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    .line 357
    invoke-virtual/range {v16 .. v16}, Letc;->q()Ljava/util/concurrent/Executor;

    move-result-object v16

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v17}, Lbym;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfxe;Lfeb;Lfew;Lcuo;Lctk;Lgix;Lcub;Leyt;Levn;Lbyr;Ljava/util/concurrent/atomic/AtomicReference;Ljava/util/concurrent/Executor;Lbwa;)V

    .line 360
    new-instance v12, Lbqc;

    const v4, 0x7f0901ff

    move-object/from16 v0, p0

    invoke-direct {v12, v0, v4}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 361
    new-instance v17, Lbqc;

    const v4, 0x7f09019f

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 363
    new-instance v18, Lbqc;

    const v4, 0x7f09019a

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 365
    new-instance v19, Lbqc;

    const v4, 0x7f0902cb

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 367
    new-instance v20, Lbqc;

    const v4, 0x7f0902cd

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lbqc;-><init>(Landroid/content/Context;I)V

    .line 369
    new-instance v21, Lbqc;

    invoke-direct/range {v21 .. v21}, Lbqc;-><init>()V

    .line 371
    new-instance v11, Lbpg;

    move-object/from16 v13, p3

    move-object/from16 v15, v23

    move-object/from16 v16, v10

    invoke-direct/range {v11 .. v21}, Lbpg;-><init>(Lbqc;Lbqd;Lbpn;Lgng;Lgix;Lbqc;Lbqc;Lbqc;Lbqc;Lbqc;)V

    move-object/from16 v0, p2

    iput-object v11, v0, Lboi;->b:Lboq;

    .line 409
    const v4, 0x7f090200

    new-instance v5, Lbph;

    move-object/from16 v0, p3

    invoke-direct {v5, v3, v0}, Lbph;-><init>(Lbym;Lbqd;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lboi;->a(ILbop;)I

    .line 419
    new-instance v4, Lbvb;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v1}, Lbvb;-><init>(Landroid/content/Context;Ljava/util/concurrent/atomic/AtomicReference;)V

    .line 421
    new-instance v5, Lbpi;

    move-object/from16 v0, p3

    invoke-direct {v5, v3, v0, v4}, Lbpi;-><init>(Lbym;Lbqd;Lbvb;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lboi;->a(Lboo;Lbop;)I

    .line 431
    new-instance v4, Lbpj;

    move-object/from16 v0, p3

    invoke-direct {v4, v0, v14}, Lbpj;-><init>(Lbqd;Lbpn;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v4}, Lboi;->a(Lboo;Lbop;)I

    .line 440
    new-instance v4, Lbpk;

    move-object/from16 v0, p3

    move-object/from16 v1, v24

    invoke-direct {v4, v0, v1}, Lbpk;-><init>(Lbqd;Lbka;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v4}, Lboi;->a(Lboo;Lbop;)I

    .line 451
    new-instance v4, Lbpl;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v4, v0, v1}, Lbpl;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lbqd;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v4}, Lboi;->a(Lboo;Lbop;)I

    .line 464
    new-instance v4, Lbpm;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-direct {v4, v3, v0, v1}, Lbpm;-><init>(Lbym;Lbqd;Lboi;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v4}, Lboi;->a(Lboo;Lbop;)I

    .line 477
    new-instance v3, Lbos;

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-direct {v3, v0, v1}, Lbos;-><init>(Lbqd;Lfhz;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v3}, Lboi;->a(Lboo;Lbop;)I

    .line 490
    new-instance v3, Lbot;

    move-object/from16 v0, p3

    move-object/from16 v1, v22

    invoke-direct {v3, v0, v1}, Lbot;-><init>(Lbqd;Lfhz;)V

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1, v3}, Lboi;->a(Lboo;Lbop;)I

    .line 503
    return-object p2
.end method

.method public static a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 3

    .prologue
    .line 148
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 153
    invoke-static {}, Lbqe;->a()Lbqe;

    move-result-object v1

    const/4 v2, 0x0

    .line 149
    invoke-static {p0, p1, v0, v1, v2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 155
    return-object v0
.end method

.method public static a(Lher;)Lbt;
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lher;->c:Lhez;

    if-eqz v0, :cond_0

    .line 152
    new-instance v0, Lfjq;

    iget-object v1, p0, Lher;->c:Lhez;

    invoke-direct {v0, v1}, Lfjq;-><init>(Lhez;)V

    .line 158
    :goto_0
    return-object v0

    .line 154
    :cond_0
    iget-object v0, p0, Lher;->d:Lhfa;

    if-eqz v0, :cond_1

    .line 155
    new-instance v0, Lfjr;

    iget-object v1, p0, Lher;->d:Lhfa;

    invoke-direct {v0, v1}, Lfjr;-><init>(Lhfa;)V

    goto :goto_0

    .line 157
    :cond_1
    iget-object v0, p0, Lher;->b:Lhet;

    if-eqz v0, :cond_2

    .line 158
    new-instance v0, Lfjn;

    iget-object v1, p0, Lher;->b:Lhet;

    invoke-direct {v0, v1}, Lfjn;-><init>(Lhet;)V

    goto :goto_0

    .line 160
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static a(Ljava/util/List;Ldth;)Ldst;
    .locals 3

    .prologue
    .line 23
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    .line 24
    iget-object v2, v0, Ldst;->b:Ldth;

    invoke-virtual {v2, p1}, Ldth;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)Ldst;
    .locals 3

    .prologue
    .line 32
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldst;

    .line 33
    iget-object v2, v0, Ldst;->c:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(III)Leau;
    .locals 1

    .prologue
    .line 116
    new-instance v0, Leau;

    invoke-direct {v0, p0, p1, p2}, Leau;-><init>(III)V

    return-object v0
.end method

.method public static a(Ljava/io/File;)Lewb;
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lewe;

    invoke-direct {v0, p0}, Lewe;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static a(Lhgn;Lfqh;)Lfju;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    iget-object v1, p0, Lhgn;->d:Lhgo;

    if-nez v1, :cond_1

    .line 35
    :cond_0
    :goto_0
    return-object v0

    .line 27
    :cond_1
    iget-object v1, p0, Lhgn;->d:Lhgo;

    iget-object v1, v1, Lhgo;->c:Lhsi;

    if-eqz v1, :cond_2

    .line 28
    new-instance v0, Lflu;

    invoke-direct {v0, p0, p1}, Lflu;-><init>(Lhgn;Lfqh;)V

    goto :goto_0

    .line 29
    :cond_2
    iget-object v1, p0, Lhgn;->d:Lhgo;

    iget-object v1, v1, Lhgo;->d:Lhbb;

    if-eqz v1, :cond_3

    .line 30
    new-instance v0, Lfiq;

    invoke-direct {v0, p0, p1}, Lfiq;-><init>(Lhgn;Lfqh;)V

    goto :goto_0

    .line 31
    :cond_3
    iget-object v1, p0, Lhgn;->d:Lhgo;

    iget-object v1, v1, Lhgo;->b:Liab;

    if-eqz v1, :cond_0

    .line 32
    new-instance v0, Lfni;

    invoke-direct {v0, p0, p1}, Lfni;-><init>(Lhgn;Lfqh;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lgbu;Ljava/util/List;I)Lfnx;
    .locals 6

    .prologue
    .line 64
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    const/4 v5, 0x1

    iget-object v1, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v2, p1, Lgbu;->a:Ljava/lang/String;

    invoke-static {v1, v2, v4, v5}, La;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lhog;

    move-result-object v1

    invoke-static {p0, v1, v0}, La;->a(Landroid/content/Context;Lhog;Lgcd;)Libi;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v0 .. v5}, La;->a(Landroid/content/Context;Libi;Lgbu;Ljava/util/List;IZ)Libi;

    move-result-object v0

    new-instance v1, Lfnx;

    invoke-direct {v1, v0}, Lfnx;-><init>(Libi;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lgcd;)Lfnx;
    .locals 3

    .prologue
    .line 50
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object v0, p1, Lgcd;->b:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 54
    invoke-static {v0, v1, v2}, La;->a(Ljava/lang/String;Ljava/lang/String;I)Lhog;

    move-result-object v0

    .line 52
    invoke-static {p0, v0, p1}, La;->a(Landroid/content/Context;Lhog;Lgcd;)Libi;

    move-result-object v0

    .line 56
    new-instance v1, Lfnx;

    invoke-direct {v1, v0}, Lfnx;-><init>(Libi;)V

    return-object v1
.end method

.method public static a(Lfnx;Landroid/content/Context;Lgbu;Ljava/util/List;I)Lfnx;
    .locals 6

    .prologue
    .line 135
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-object p0

    .line 141
    :cond_1
    iget-object v1, p0, Lfnx;->a:Libi;

    .line 142
    new-instance p0, Lfnx;

    .line 143
    const/4 v5, 0x1

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, La;->a(Landroid/content/Context;Libi;Lgbu;Ljava/util/List;IZ)Libi;

    move-result-object v0

    invoke-direct {p0, v0}, Lfnx;-><init>(Libi;)V

    goto :goto_0
.end method

.method public static a(Lhyr;)Lfoy;
    .locals 4

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 37
    :goto_0
    return-object v0

    .line 34
    :cond_0
    invoke-static {p0}, La;->b(Lhyr;)Lfpc;

    move-result-object v0

    .line 36
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, v0, Lfpc;->R:J

    .line 37
    invoke-virtual {v0}, Lfpc;->a()Lfoy;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcyc;Landroid/content/Context;)Lfsj;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcet;

    invoke-direct {v0, p1}, Lcet;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static synthetic a(Lgng;Lgix;)Lgnd;
    .locals 1

    .prologue
    .line 81
    invoke-interface {p1}, Lgix;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgng;->c()Lgnd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgng;->a(Lgit;)Lgnd;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lfdv;)Lguc;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lfdv;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/InterruptedException;

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lfdv;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, Ljava/lang/InterruptedException;

    throw v0

    .line 35
    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, La;->e(I)Lguc;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lgbu;Ljava/util/List;IIZ)Lhaz;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 207
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    if-ltz p2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 210
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p2, v0, :cond_3

    .line 211
    add-int/lit8 v1, p2, 0x1

    .line 212
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 213
    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v3, p0, Lgbu;->a:Ljava/lang/String;

    .line 214
    invoke-static {v0, v3, v1, p4}, La;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lhog;

    move-result-object v0

    move-object v1, v0

    .line 221
    :goto_1
    if-lez p2, :cond_4

    .line 222
    add-int/lit8 v2, p2, -0x1

    .line 223
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 224
    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v3, p0, Lgbu;->a:Ljava/lang/String;

    .line 225
    invoke-static {v0, v3, v2, p4}, La;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lhog;

    move-result-object v0

    .line 227
    :goto_2
    new-instance v2, Lhaz;

    invoke-direct {v2}, Lhaz;-><init>()V

    .line 228
    iput p3, v2, Lhaz;->b:I

    .line 229
    if-eqz v1, :cond_0

    .line 230
    iput-object v1, v2, Lhaz;->d:Lhog;

    .line 231
    iput-object v1, v2, Lhaz;->c:Lhog;

    .line 233
    :cond_0
    if-eqz v0, :cond_1

    .line 234
    iput-object v0, v2, Lhaz;->e:Lhog;

    .line 236
    :cond_1
    return-object v2

    :cond_2
    move v0, v1

    .line 207
    goto :goto_0

    .line 215
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p2, v0, :cond_5

    const/4 v0, 0x3

    if-ne p3, v0, :cond_5

    .line 216
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 218
    iget-object v0, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v3, p0, Lgbu;->a:Ljava/lang/String;

    .line 219
    invoke-static {v0, v3, v1, p4}, La;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lhog;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_4
    move-object v0, v2

    goto :goto_2

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method public static a(Lhlq;Z)Lhll;
    .locals 3

    .prologue
    .line 287
    new-instance v0, Lhll;

    invoke-direct {v0}, Lhll;-><init>()V

    .line 289
    new-instance v1, Lhlk;

    invoke-direct {v1}, Lhlk;-><init>()V

    iput-object v1, v0, Lhll;->a:Lhlk;

    .line 290
    iget-object v1, v0, Lhll;->a:Lhlk;

    iput-object p0, v1, Lhlk;->a:Lhlq;

    .line 291
    iget-object v1, v0, Lhll;->a:Lhlk;

    const/4 v2, 0x0

    new-array v2, v2, [B

    iput-object v2, v1, Lhlk;->k:[B

    .line 292
    iget-object v1, v0, Lhll;->a:Lhlk;

    iput-boolean p1, v1, Lhlk;->l:Z

    .line 293
    iget-object v1, v0, Lhll;->a:Lhlk;

    const/4 v2, 0x2

    iput v2, v1, Lhlk;->b:I

    .line 294
    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;I)Lhog;
    .locals 2

    .prologue
    .line 395
    new-instance v0, Lhpm;

    invoke-direct {v0}, Lhpm;-><init>()V

    .line 396
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 397
    iput-object p0, v0, Lhpm;->a:Ljava/lang/String;

    .line 399
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 400
    iput-object p1, v0, Lhpm;->b:Ljava/lang/String;

    .line 402
    :cond_1
    const/4 v1, -0x1

    if-eq p2, v1, :cond_2

    .line 403
    iput p2, v0, Lhpm;->c:I

    .line 405
    :cond_2
    new-instance v1, Lhog;

    invoke-direct {v1}, Lhog;-><init>()V

    .line 406
    iput-object v0, v1, Lhog;->w:Lhpm;

    .line 407
    return-object v1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;IZ)Lhog;
    .locals 2

    .prologue
    .line 387
    if-eqz p3, :cond_0

    .line 388
    invoke-static {p0, p1, p2}, La;->a(Ljava/lang/String;Ljava/lang/String;I)Lhog;

    move-result-object v0

    .line 390
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Libf;

    invoke-direct {v1}, Libf;-><init>()V

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iput-object p0, v1, Libf;->a:Ljava/lang/String;

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iput-object p1, v1, Libf;->b:Ljava/lang/String;

    :cond_2
    const/4 v0, -0x1

    if-eq p2, v0, :cond_3

    iput p2, v1, Libf;->c:I

    :cond_3
    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v1, v0, Lhog;->i:Libf;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lhog;Lgcd;)Libi;
    .locals 18

    .prologue
    .line 96
    new-instance v3, Libi;

    invoke-direct {v3}, Libi;-><init>()V

    .line 97
    move-object/from16 v0, p1

    iput-object v0, v3, Libi;->c:Lhog;

    .line 98
    new-instance v2, Libj;

    invoke-direct {v2}, Libj;-><init>()V

    iput-object v2, v3, Libi;->a:Libj;

    .line 99
    iget-object v2, v3, Libi;->a:Libj;

    new-instance v4, Lhwb;

    invoke-direct {v4}, Lhwb;-><init>()V

    iput-object v4, v2, Libj;->a:Lhwb;

    .line 100
    iget-object v2, v3, Libi;->a:Libj;

    iget-object v2, v2, Libj;->a:Lhwb;

    new-instance v4, Lhwf;

    invoke-direct {v4}, Lhwf;-><init>()V

    iput-object v4, v2, Lhwb;->a:Lhwf;

    .line 102
    iget-object v2, v3, Libi;->a:Libj;

    iget-object v2, v2, Libj;->a:Lhwb;

    iget-object v2, v2, Lhwb;->a:Lhwf;

    new-instance v4, Lhul;

    invoke-direct {v4}, Lhul;-><init>()V

    iput-object v4, v2, Lhwf;->a:Lhul;

    .line 103
    iget-object v2, v3, Libi;->a:Libj;

    iget-object v2, v2, Libj;->a:Lhwb;

    iget-object v2, v2, Lhwb;->a:Lhwf;

    iget-object v2, v2, Lhwf;->a:Lhul;

    const/4 v4, 0x1

    new-array v4, v4, [Lhun;

    const/4 v5, 0x0

    new-instance v6, Lhun;

    invoke-direct {v6}, Lhun;-><init>()V

    aput-object v6, v4, v5

    iput-object v4, v2, Lhul;->a:[Lhun;

    .line 105
    iget-object v2, v3, Libi;->a:Libj;

    iget-object v2, v2, Libj;->a:Lhwb;

    iget-object v2, v2, Lhwb;->a:Lhwf;

    iget-object v2, v2, Lhwf;->a:Lhul;

    iget-object v2, v2, Lhul;->a:[Lhun;

    const/4 v4, 0x0

    aget-object v4, v2, v4

    .line 106
    new-instance v5, Lhky;

    invoke-direct {v5}, Lhky;-><init>()V

    const/4 v2, 0x2

    new-array v6, v2, [Lhla;

    const/4 v7, 0x0

    move-object/from16 v0, p2

    iget-object v2, v0, Lgcd;->p:Ljava/lang/String;

    invoke-static {v2}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v8

    const/4 v2, 0x0

    move-object/from16 v0, p2

    iget-object v9, v0, Lgcd;->H:Ljava/lang/String;

    if-eqz v9, :cond_0

    move-object/from16 v0, p2

    iget-object v2, v0, Lgcd;->H:Ljava/lang/String;

    :cond_0
    new-instance v9, Lhzu;

    invoke-direct {v9}, Lhzu;-><init>()V

    iput-object v8, v9, Lhzu;->b:Lhgz;

    new-instance v8, Lhxf;

    invoke-direct {v8}, Lhxf;-><init>()V

    iput-object v8, v9, Lhzu;->a:Lhxf;

    iget-object v8, v9, Lhzu;->a:Lhxf;

    const/4 v10, 0x1

    new-array v10, v10, [Lhxg;

    const/4 v11, 0x0

    new-instance v12, Lhxg;

    invoke-direct {v12}, Lhxg;-><init>()V

    aput-object v12, v10, v11

    iput-object v10, v8, Lhxf;->b:[Lhxg;

    iget-object v8, v9, Lhzu;->a:Lhxf;

    iget-object v8, v8, Lhxf;->b:[Lhxg;

    const/4 v10, 0x0

    aget-object v8, v8, v10

    iput-object v2, v8, Lhxg;->b:Ljava/lang/String;

    new-instance v2, Lhla;

    invoke-direct {v2}, Lhla;-><init>()V

    iput-object v9, v2, Lhla;->g:Lhzu;

    aput-object v2, v6, v7

    const/4 v2, 0x1

    move-object/from16 v0, p2

    iget-object v7, v0, Lgcd;->j:Ljava/lang/String;

    invoke-static {v7}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v7

    const v8, 0x7f09009f

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    move-object/from16 v0, p2

    iget-wide v12, v0, Lgcd;->l:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v8

    move-object/from16 v0, p2

    iget-wide v10, v0, Lgcd;->m:J

    invoke-static {v10, v11}, Lfvo;->a(J)Lhgz;

    move-result-object v9

    move-object/from16 v0, p2

    iget-wide v10, v0, Lgcd;->m:J

    const-wide/16 v12, 0x1

    add-long/2addr v10, v12

    invoke-static {v10, v11}, Lfvo;->a(J)Lhgz;

    move-result-object v10

    move-object/from16 v0, p2

    iget-wide v12, v0, Lgcd;->n:J

    invoke-static {v12, v13}, Lfvo;->a(J)Lhgz;

    move-result-object v11

    move-object/from16 v0, p2

    iget-wide v12, v0, Lgcd;->n:J

    const-wide/16 v14, 0x1

    add-long/2addr v12, v14

    invoke-static {v12, v13}, Lfvo;->a(J)Lhgz;

    move-result-object v12

    move-object/from16 v0, p2

    iget-object v13, v0, Lgcd;->w:Ljava/lang/String;

    invoke-static {v13}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v13

    new-instance v14, Lhgz;

    invoke-direct {v14}, Lhgz;-><init>()V

    move-object/from16 v0, p2

    iget-object v15, v0, Lgcd;->r:Ljava/util/Date;

    if-eqz v15, :cond_1

    invoke-static {}, Lfvo;->a()Lhwo;

    move-result-object v15

    invoke-static/range {p0 .. p0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v16

    move-object/from16 v0, p2

    iget-object v0, v0, Lgcd;->r:Ljava/util/Date;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lhwo;->b:Ljava/lang/String;

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Lhwo;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v15, v16, v17

    move-object/from16 v0, v16

    iput-object v0, v14, Lhgz;->b:[Lhwo;

    :cond_1
    new-instance v15, Lhla;

    invoke-direct {v15}, Lhla;-><init>()V

    new-instance v16, Lhzs;

    invoke-direct/range {v16 .. v16}, Lhzs;-><init>()V

    move-object/from16 v0, v16

    iput-object v0, v15, Lhla;->h:Lhzs;

    iget-object v0, v15, Lhla;->h:Lhzs;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Lgcd;->b()Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lhzs;->e:Z

    iget-object v0, v15, Lhla;->h:Lhzs;

    move-object/from16 v16, v0

    invoke-virtual/range {p2 .. p2}, Lgcd;->b()Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput-boolean v0, v1, Lhzs;->d:Z

    iget-object v0, v15, Lhla;->h:Lhzs;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iput-object v13, v0, Lhzs;->c:Lhgz;

    iget-object v13, v15, Lhla;->h:Lhzs;

    iput-object v14, v13, Lhzs;->f:Lhgz;

    iget-object v13, v15, Lhla;->h:Lhzs;

    iput-object v8, v13, Lhzs;->b:Lhgz;

    iget-object v8, v15, Lhla;->h:Lhzs;

    iput-object v7, v8, Lhzs;->a:Lhgz;

    iget-object v7, v15, Lhla;->h:Lhzs;

    new-instance v8, Lhlq;

    invoke-direct {v8}, Lhlq;-><init>()V

    move-object/from16 v0, p2

    iget-object v13, v0, Lgcd;->b:Ljava/lang/String;

    iput-object v13, v8, Lhlq;->a:Ljava/lang/String;

    const/4 v13, 0x1

    invoke-static {v8, v13}, La;->a(Lhlq;Z)Lhll;

    move-result-object v8

    iput-object v8, v7, Lhzs;->g:Lhll;

    iget-object v7, v15, Lhla;->h:Lhzs;

    iget-object v7, v7, Lhzs;->g:Lhll;

    iget-object v7, v7, Lhll;->a:Lhlk;

    iput-object v11, v7, Lhlk;->h:Lhgz;

    iget-object v7, v15, Lhla;->h:Lhzs;

    iget-object v7, v7, Lhzs;->g:Lhll;

    iget-object v7, v7, Lhll;->a:Lhlk;

    iput-object v12, v7, Lhlk;->i:Lhgz;

    iget-object v7, v15, Lhla;->h:Lhzs;

    iget-object v7, v7, Lhzs;->g:Lhll;

    iget-object v7, v7, Lhll;->a:Lhlk;

    iput-object v9, v7, Lhlk;->d:Lhgz;

    iget-object v7, v15, Lhla;->h:Lhzs;

    iget-object v7, v7, Lhzs;->g:Lhll;

    iget-object v7, v7, Lhll;->a:Lhlk;

    iput-object v10, v7, Lhlk;->e:Lhgz;

    aput-object v15, v6, v2

    iput-object v6, v5, Lhky;->a:[Lhla;

    iput-object v5, v4, Lhun;->b:Lhky;

    .line 107
    return-object v3
.end method

.method public static a(Landroid/content/Context;Libi;Lgbu;Ljava/util/List;IZ)Libi;
    .locals 9

    .prologue
    .line 168
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v0, p1, Libi;->a:Libj;

    iget-object v3, v0, Libj;->a:Lhwb;

    .line 174
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-instance v4, Lhsd;

    invoke-direct {v4}, Lhsd;-><init>()V

    iget-object v1, p2, Lgbu;->a:Ljava/lang/String;

    iput-object v1, v4, Lhsd;->d:Ljava/lang/String;

    iget-object v1, p2, Lgbu;->b:Ljava/lang/String;

    iput-object v1, v4, Lhsd;->a:Ljava/lang/String;

    iput p4, v4, Lhsd;->c:I

    iput v0, v4, Lhsd;->e:I

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f100000

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v2, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v0

    iput-object v0, v4, Lhsd;->k:Lhgz;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhsf;

    iput-object v0, v4, Lhsd;->b:[Lhsf;

    new-instance v1, Lhlq;

    invoke-direct {v1}, Lhlq;-><init>()V

    iget-object v0, p2, Lgbu;->a:Ljava/lang/String;

    iput-object v0, v1, Lhlq;->b:Ljava/lang/String;

    iget-boolean v0, p2, Lgbu;->k:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, La;->a(Lhlq;Z)Lhll;

    move-result-object v0

    iput-object v0, v4, Lhsd;->i:Lhll;

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    if-ne v1, p4, :cond_2

    const/4 v0, 0x1

    move v2, v0

    :goto_2
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    new-instance v5, Lhsh;

    invoke-direct {v5}, Lhsh;-><init>()V

    iput-boolean v2, v5, Lhsh;->e:Z

    iget-object v2, v0, Lgcd;->b:Ljava/lang/String;

    iput-object v2, v5, Lhsh;->h:Ljava/lang/String;

    iget-object v2, v0, Lgcd;->j:Ljava/lang/String;

    invoke-static {v2}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v2

    iput-object v2, v5, Lhsh;->a:Lhgz;

    iget-object v2, v0, Lgcd;->p:Ljava/lang/String;

    invoke-static {v2}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v2

    iput-object v2, v5, Lhsh;->i:Lhgz;

    add-int/lit8 v2, v1, 0x1

    int-to-long v6, v2

    invoke-static {v6, v7}, Lfvo;->a(J)Lhgz;

    move-result-object v2

    iput-object v2, v5, Lhsh;->d:Lhgz;

    iget v2, v0, Lgcd;->k:I

    const/4 v6, 0x3

    invoke-static {v2, v6}, La;->b(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, La;->N(Ljava/lang/String;)Lhgz;

    move-result-object v2

    iput-object v2, v5, Lhsh;->c:Lhgz;

    iget-object v2, v0, Lgcd;->b:Ljava/lang/String;

    iget-object v6, p2, Lgbu;->a:Ljava/lang/String;

    invoke-static {v2, v6, v1, p5}, La;->a(Ljava/lang/String;Ljava/lang/String;IZ)Lhog;

    move-result-object v2

    iput-object v2, v5, Lhsh;->f:Lhog;

    iget-object v2, v0, Lgcd;->e:Landroid/net/Uri;

    if-eqz v2, :cond_0

    new-instance v2, Lhxf;

    invoke-direct {v2}, Lhxf;-><init>()V

    iput-object v2, v5, Lhsh;->b:Lhxf;

    iget-object v2, v5, Lhsh;->b:Lhxf;

    const/4 v6, 0x1

    new-array v6, v6, [Lhxg;

    const/4 v7, 0x0

    new-instance v8, Lhxg;

    invoke-direct {v8}, Lhxg;-><init>()V

    aput-object v8, v6, v7

    iput-object v6, v2, Lhxf;->b:[Lhxg;

    iget-object v2, v5, Lhsh;->b:Lhxf;

    iget-object v2, v2, Lhxf;->b:[Lhxg;

    const/4 v6, 0x0

    aget-object v2, v2, v6

    iget-object v0, v0, Lgcd;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lhxg;->b:Ljava/lang/String;

    :cond_0
    iget-object v0, v4, Lhsd;->b:[Lhsf;

    new-instance v2, Lhsf;

    invoke-direct {v2}, Lhsf;-><init>()V

    aput-object v2, v0, v1

    iget-object v0, v4, Lhsd;->b:[Lhsf;

    aget-object v0, v0, v1

    iput-object v5, v0, Lhsf;->b:Lhsh;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_2

    :cond_3
    new-instance v0, Lhwd;

    invoke-direct {v0}, Lhwd;-><init>()V

    iput-object v4, v0, Lhwd;->a:Lhsd;

    iput-object v0, v3, Lhwb;->b:Lhwd;

    .line 175
    new-instance v0, Lhwc;

    invoke-direct {v0}, Lhwc;-><init>()V

    new-instance v1, Lhay;

    invoke-direct {v1}, Lhay;-><init>()V

    iput-object v1, v0, Lhwc;->a:Lhay;

    iget-object v1, v0, Lhwc;->a:Lhay;

    const/4 v2, 0x4

    new-array v2, v2, [Lhaz;

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {p2, p3, p4, v5, p5}, La;->a(Lgbu;Ljava/util/List;IIZ)Lhaz;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const/4 v5, 0x3

    invoke-static {p2, p3, p4, v5, p5}, La;->a(Lgbu;Ljava/util/List;IIZ)Lhaz;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const/4 v5, 0x2

    invoke-static {p2, p3, p4, v5, p5}, La;->a(Lgbu;Ljava/util/List;IIZ)Lhaz;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const/4 v5, 0x4

    invoke-static {p2, p3, p4, v5, p5}, La;->a(Lgbu;Ljava/util/List;IIZ)Lhaz;

    move-result-object v5

    aput-object v5, v2, v4

    iput-object v2, v1, Lhay;->a:[Lhaz;

    iput-object v0, v3, Lhwb;->c:Lhwc;

    .line 176
    return-object p1
.end method

.method public static a(Lgyj;)Ljava/lang/Class;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 17
    if-nez p0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return-object v0

    .line 21
    :cond_1
    const-class v1, Lgyj;

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 22
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 24
    :try_start_0
    invoke-virtual {v4, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 25
    if-nez v5, :cond_3

    .line 21
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 29
    :cond_3
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v4

    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 34
    instance-of v4, v5, Lidf;

    if-eqz v4, :cond_2

    .line 35
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_2

    .line 41
    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public static a(Lhut;)Ljava/lang/Class;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 19
    if-nez p0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-object v0

    .line 23
    :cond_1
    const-class v1, Lhut;

    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 24
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 26
    :try_start_0
    invoke-virtual {v4, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 27
    if-nez v5, :cond_3

    .line 23
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 31
    :cond_3
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v4

    invoke-static {v4}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 36
    instance-of v4, v5, Lidf;

    if-eqz v4, :cond_2

    .line 37
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_2

    .line 40
    :catch_1
    move-exception v4

    goto :goto_2
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 40
    if-eqz p0, :cond_0

    .line 41
    check-cast p0, Landroid/transition/Transition;

    invoke-virtual {p0}, Landroid/transition/Transition;->clone()Landroid/transition/Transition;

    move-result-object p0

    .line 43
    :cond_0
    return-object p0
.end method

.method public static a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 68
    check-cast p0, Landroid/media/MediaRouter;

    const v0, 0x800003

    invoke-virtual {p0, v0}, Landroid/media/MediaRouter;->getSelectedRoute(I)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/String;Z)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 88
    check-cast p0, Landroid/media/MediaRouter;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/media/MediaRouter;->createRouteCategory(Ljava/lang/CharSequence;Z)Landroid/media/MediaRouter$RouteCategory;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/reflect/Field;Lidh;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 129
    invoke-virtual {p0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 130
    invoke-static {v4}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v5

    .line 131
    if-nez v5, :cond_0

    .line 132
    const/4 v0, 0x0

    .line 146
    :goto_0
    return-object v0

    .line 135
    :cond_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    move v2, v3

    .line 136
    :goto_1
    if-ge v2, v5, :cond_3

    .line 137
    invoke-static {v4, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 138
    instance-of v6, v0, Lidh;

    if-eqz v6, :cond_1

    .line 139
    check-cast v0, Lidh;

    invoke-static {v0}, La;->a(Lidh;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 136
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 140
    :cond_1
    instance-of v6, v0, [B

    if-eqz v6, :cond_2

    .line 141
    new-instance v6, Ljava/lang/String;

    check-cast v0, [B

    invoke-static {v0, v3}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_2

    .line 143
    :cond_2
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 146
    goto :goto_0
.end method

.method public static a(Lsp;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 111
    new-instance v0, Lsq;

    invoke-direct {v0, p0}, Lsq;-><init>(Lsp;)V

    return-object v0
.end method

.method public static a(JJLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    cmp-long v0, p2, p0

    if-ltz v0, :cond_0

    .line 90
    const v0, 0x7f0900a8

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    .line 93
    :cond_0
    sub-long v4, p0, p2

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    const-wide/16 v6, 0x3c

    div-long/2addr v4, v6

    long-to-int v3, v4

    .line 94
    div-int/lit8 v4, v3, 0x3c

    .line 97
    if-lez v4, :cond_2

    rem-int/lit8 v0, v3, 0x3c

    if-lez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v4, v0

    .line 98
    div-int/lit8 v5, v4, 0x18

    .line 101
    if-lez v5, :cond_4

    rem-int/lit8 v0, v4, 0x18

    if-lez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v5

    .line 103
    if-lez v0, :cond_5

    .line 104
    const v3, 0x7f100008

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {p4, v3, v0, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 97
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    .line 101
    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    .line 105
    :cond_5
    if-lez v4, :cond_6

    .line 106
    const v0, 0x7f100009

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p4, v0, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 107
    :cond_6
    const/16 v0, 0xa

    if-le v3, v0, :cond_7

    .line 108
    const v0, 0x7f10000a

    new-array v1, v1, [Ljava/lang/Object;

    .line 111
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v2

    .line 108
    invoke-virtual {p4, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 113
    :cond_7
    const v0, 0x7f0900a7

    invoke-virtual {p4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/ContentResolver;Lfac;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 22
    const-string v0, "android_id"

    invoke-static {p0, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 26
    const-string v0, "%x"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lfac;->a()Ljava/security/SecureRandom;

    move-result-object v3

    invoke-virtual {v3}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 28
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;JZZ)Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v7, 0xa01

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 65
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    if-eqz p3, :cond_2

    .line 67
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f0900ab

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v0, v3}, La;->a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    .line 67
    :cond_0
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0900ac

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {p0, v4, v5, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {p0, v2, v3, v5}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {p0, v2, v3, v7}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0900ad

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v6

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :cond_2
    if-eqz p4, :cond_3

    .line 69
    const v0, 0x7f0900aa

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_3
    const v0, 0x7f0900a9

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/pm/PackageManager;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 216
    .line 217
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 216
    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 218
    const-string v1, "\\."

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 222
    array-length v2, v1

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 223
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    .line 224
    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-object v3, v1, v3

    aput-object v3, v0, v2

    .line 225
    const/4 v2, 0x1

    const/4 v3, 0x1

    aget-object v1, v1, v3

    aput-object v1, v0, v2

    .line 226
    const-string v1, "."

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 228
    :cond_0
    return-object v0

    .line 230
    :catch_0
    move-exception v0

    .line 233
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "PackageManager did not find our package name!"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Leqk;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {v1, p0, v2, v0}, La;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error printing proto: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error printing proto: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lhtx;)Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 24
    if-eqz p0, :cond_0

    iget-object v1, p0, Lhtx;->b:[Lhuu;

    if-nez v1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return-object v0

    .line 27
    :cond_1
    iget-object v4, p0, Lhtx;->b:[Lhuu;

    array-length v5, v4

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 28
    iget v6, v1, Lhuu;->b:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_3

    .line 29
    iget-object v6, v1, Lhuu;->c:[Lhld;

    array-length v7, v6

    move v1, v2

    :goto_2
    if-ge v1, v7, :cond_3

    aget-object v8, v6, v1

    .line 30
    iget-object v9, v8, Lhld;->b:Ljava/lang/String;

    const-string v10, "context"

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 31
    iget-object v0, v8, Lhld;->c:Ljava/lang/String;

    goto :goto_0

    .line 29
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 27
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1
.end method

.method public static a(Lima;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lima;->a:Lima;

    if-ne p0, v0, :cond_0

    const-string v0, "HTTP/1.0"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "HTTP/1.1"

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IJ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, La;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 29
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 30
    const-string v1, "ISO-8859-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 31
    invoke-static {v0}, Lipv;->a([B)Lipv;

    move-result-object v0

    iget-object v0, v0, Lipv;->b:[B

    invoke-static {v0}, Lipp;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 32
    const-string v1, "Basic "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/net/URL;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v0

    .line 49
    if-nez v0, :cond_1

    const-string v0, "/"

    .line 51
    :cond_0
    :goto_0
    return-object v0

    .line 50
    :cond_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/nio/ByteBuffer;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    new-array v0, p1, [B

    .line 92
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 93
    invoke-static {v0}, La;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Date;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 26
    if-nez p0, :cond_1

    .line 60
    :cond_0
    :goto_0
    return-object v0

    .line 29
    :cond_1
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 31
    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    .line 36
    sub-long v2, v4, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 37
    div-int/lit8 v2, v1, 0x3c

    .line 38
    div-int/lit8 v3, v2, 0x3c

    .line 39
    div-int/lit8 v4, v3, 0x18

    .line 40
    div-int/lit8 v5, v4, 0x7

    .line 41
    div-int/lit8 v6, v4, 0x1e

    .line 42
    div-int/lit8 v7, v6, 0xc

    .line 44
    if-lez v7, :cond_2

    .line 45
    const v0, 0x7f100001

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p1, v0, v7, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 46
    :cond_2
    if-lez v6, :cond_3

    .line 47
    const v0, 0x7f100002

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p1, v0, v6, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 48
    :cond_3
    if-lez v5, :cond_4

    .line 49
    const v0, 0x7f100003

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p1, v0, v5, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 50
    :cond_4
    if-lez v4, :cond_5

    .line 51
    const v0, 0x7f100004

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p1, v0, v4, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_5
    if-lez v3, :cond_6

    .line 53
    const v0, 0x7f100005

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {p1, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_6
    if-lez v2, :cond_7

    .line 55
    const v0, 0x7f100006

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v8

    invoke-virtual {p1, v0, v2, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 56
    :cond_7
    if-lez v1, :cond_0

    .line 57
    const v0, 0x7f100007

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(Ljava/util/Map;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 122
    const-string v0, "Content-Type"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 123
    if-eqz v0, :cond_1

    .line 124
    const-string v2, ";"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    move v0, v1

    .line 125
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 126
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 127
    array-length v4, v3

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 128
    const/4 v4, 0x0

    aget-object v4, v3, v4

    const-string v5, "charset"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 129
    aget-object v0, v3, v1

    .line 135
    :goto_1
    return-object v0

    .line 125
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_1
    const-string v0, "ISO-8859-1"

    goto :goto_1
.end method

.method public static a(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;
    .locals 10

    .prologue
    const/16 v9, 0x10

    const/4 v0, 0x0

    .line 25
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    const-string v1, "curl"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 28
    const-string v6, " -H \'"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    invoke-interface {v5}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\'"

    const-string v8, "\\\'"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    const-string v6, ": "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\'"

    const-string v7, "\\\'"

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    const-string v5, "\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 34
    :cond_0
    const-string v1, " \'"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 36
    const-string v1, "\'"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    instance-of v1, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v1, :cond_7

    .line 38
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 39
    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    invoke-interface {p0}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 40
    if-eqz v3, :cond_1

    .line 42
    :try_start_0
    invoke-interface {v3, v1}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :try_start_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 53
    :cond_1
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    array-length v5, v4

    move v1, v0

    :goto_2
    if-ge v1, v5, :cond_4

    aget-byte v6, v4, v1

    .line 56
    const/16 v7, 0x20

    if-lt v6, v7, :cond_2

    int-to-char v7, v6

    const/16 v8, 0x27

    if-eq v7, v8, :cond_2

    .line 57
    int-to-char v6, v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v3

    .line 47
    :try_start_2
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 51
    :catch_1
    move-exception v3

    goto :goto_1

    .line 46
    :catchall_0
    move-exception v0

    .line 47
    :try_start_3
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 50
    :goto_4
    throw v0

    .line 62
    :cond_2
    add-int/lit16 v0, v6, 0x100

    rem-int/lit16 v0, v0, 0x100

    .line 63
    const-string v6, "\\x"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    if-ge v0, v9, :cond_3

    .line 65
    const/16 v6, 0x30

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    :cond_3
    invoke-static {v0, v9}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    const/4 v0, 0x1

    goto :goto_3

    .line 71
    :cond_4
    const-string v1, " -d \'"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    if-eqz v0, :cond_6

    .line 73
    const-string v0, "$(printf \'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const-string v0, "\')"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    :goto_5
    const-string v0, "\'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    :cond_5
    :goto_6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 77
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 80
    :cond_7
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 81
    const-string v0, " -X "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 51
    :catch_2
    move-exception v3

    goto/16 :goto_1

    :catch_3
    move-exception v1

    goto :goto_4
.end method

.method public static synthetic a(Lorg/xml/sax/Attributes;[Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    aget-object v1, p1, v0

    invoke-interface {p0, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a([B)Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    if-eqz p0, :cond_0

    .line 39
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/content/SharedPreferences;Lfac;)Ljava/security/Key;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 51
    const-string v0, "downloads_encryption_key"

    invoke-interface {p0, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "downloads_encryption_key"

    .line 55
    invoke-static {p1}, La;->a(Lfac;)Ljava/security/Key;

    move-result-object v2

    invoke-interface {v2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 53
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 55
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 57
    :cond_0
    const-string v0, "downloads_encryption_key"

    const/4 v1, 0x0

    .line 58
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 59
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, v0, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method public static a(Lfac;)Ljava/security/Key;
    .locals 4

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 39
    :try_start_0
    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v1

    .line 40
    const/16 v2, 0x80

    invoke-virtual {p0}, Lfac;->a()Ljava/security/SecureRandom;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 41
    invoke-virtual {v1}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 46
    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v1

    .line 43
    const-string v2, "AES not recognized as a supported algorithm"

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/pm/PackageManager;)Ljava/util/List;
    .locals 4

    .prologue
    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 40
    invoke-static {}, La;->O()Landroid/content/Intent;

    move-result-object v0

    const/high16 v2, 0x10000

    .line 39
    invoke-virtual {p0, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 42
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 43
    if-eqz v0, :cond_0

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_0

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v3, :cond_0

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 48
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 52
    :cond_1
    return-object v1
.end method

.method public static a(Lfqh;[Lhhs;Lfqa;)Ljava/util/List;
    .locals 5

    .prologue
    .line 109
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 112
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 113
    iget-object v4, v3, Lhhs;->b:Lhhu;

    if-eqz v4, :cond_0

    .line 114
    new-instance v4, Lfpz;

    iget-object v3, v3, Lhhs;->b:Lhhu;

    invoke-direct {v4, v3, p2, p0}, Lfpz;-><init>(Lhhu;Lfqa;Lfqh;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_1
    return-object v1
.end method

.method public static a(Lfqh;[Lhhy;)Ljava/util/List;
    .locals 5

    .prologue
    .line 47
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 50
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 51
    iget-object v4, v3, Lhhy;->b:Lhhu;

    if-eqz v4, :cond_0

    .line 52
    new-instance v4, Lfpz;

    iget-object v3, v3, Lhhy;->b:Lhhu;

    invoke-direct {v4, v3, p0}, Lfpz;-><init>(Lhhu;Lfqh;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_1
    return-object v1
.end method

.method public static a(Lfqh;[Lhid;)Ljava/util/List;
    .locals 5

    .prologue
    .line 66
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 69
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, p1, v0

    .line 70
    iget-object v4, v3, Lhid;->c:Lhhr;

    if-eqz v4, :cond_1

    .line 71
    new-instance v4, Lfpy;

    iget-object v3, v3, Lhid;->c:Lhhr;

    invoke-direct {v4, v3, p0}, Lfpy;-><init>(Lhhr;Lfqh;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_1
    iget-object v4, v3, Lhid;->b:Lhhu;

    if-eqz v4, :cond_0

    .line 75
    new-instance v4, Lfpz;

    iget-object v3, v3, Lhid;->b:Lhhu;

    invoke-direct {v4, v3, p0}, Lfpz;-><init>(Lhhu;Lfqh;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 80
    :cond_2
    return-object v1
.end method

.method public static a(Lfqh;[Lhif;)Ljava/util/List;
    .locals 5

    .prologue
    .line 27
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 30
    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p1, v0

    .line 31
    iget-object v4, v3, Lhif;->b:Lhhu;

    if-eqz v4, :cond_0

    .line 32
    new-instance v4, Lfpz;

    iget-object v3, v3, Lhif;->b:Lhhu;

    invoke-direct {v4, v3, p0}, Lfpz;-><init>(Lhhu;Lfqh;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/lang/String;I)Ljava/util/List;
    .locals 6

    .prologue
    .line 41
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 45
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    div-int/lit16 v0, v0, 0x800

    add-int/lit8 v3, v0, 0x1

    .line 46
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 47
    shl-int/lit8 v4, v1, 0xb

    .line 48
    add-int/lit8 v0, v1, 0x1

    shl-int/lit8 v0, v0, 0xb

    .line 50
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v0, v5, :cond_0

    .line 51
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 53
    :cond_0
    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 55
    :cond_1
    return-object v2
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 89
    if-eqz p0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    if-ne p0, v0, :cond_1

    .line 90
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([I)Ljava/util/List;
    .locals 4

    .prologue
    .line 151
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 152
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    .line 153
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_0
    return-object v1
.end method

.method public static a([Lhib;)Ljava/util/List;
    .locals 6

    .prologue
    .line 19
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 22
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, p0, v0

    .line 23
    iget-object v4, v3, Lhib;->c:Lhie;

    if-eqz v4, :cond_0

    .line 24
    new-instance v4, Lfqf;

    iget-object v3, v3, Lhib;->c:Lhie;

    invoke-direct {v4, v3}, Lfqf;-><init>(Lhie;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 22
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_0
    iget-object v4, v3, Lhib;->d:Lhhx;

    if-eqz v4, :cond_1

    .line 27
    new-instance v4, Lfqc;

    iget-object v3, v3, Lhib;->d:Lhhx;

    invoke-direct {v4, v3}, Lfqc;-><init>(Lhhx;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 29
    :cond_1
    iget-object v4, v3, Lhib;->b:Lhic;

    if-eqz v4, :cond_2

    .line 30
    new-instance v4, Lfqe;

    iget-object v3, v3, Lhib;->b:Lhic;

    invoke-direct {v4, v3}, Lfqe;-><init>(Lhic;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 32
    :cond_2
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x22

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unsupported Guide renderer found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lezp;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 36
    :cond_3
    return-object v1
.end method

.method public static a([Lhpc;)Ljava/util/List;
    .locals 6

    .prologue
    .line 41
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 44
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 45
    iget-object v4, v3, Lhpc;->b:Lhic;

    if-eqz v4, :cond_0

    .line 46
    new-instance v4, Lfqe;

    iget-object v3, v3, Lhpc;->b:Lhic;

    invoke-direct {v4, v3}, Lfqe;-><init>(Lhic;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 44
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2a

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unsupported Offline Guide renderer found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lezp;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 52
    :cond_1
    return-object v1
.end method

.method public static varargs a([Ljava/lang/Object;)Ljava/util/List;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 19
    new-instance v2, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 24
    :goto_0
    if-gtz v0, :cond_0

    aget-object v3, p0, v1

    .line 25
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/lang/String;)Ljava/util/List;
    .locals 5

    .prologue
    .line 44
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_2

    .line 45
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 59
    :cond_1
    return-object v0

    .line 48
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 49
    array-length v3, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, p0, v2

    .line 50
    if-eqz v1, :cond_3

    .line 52
    :try_start_0
    invoke-static {v1}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :cond_3
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 53
    :catch_0
    move-exception v1

    .line 54
    const-string v4, "Invalid uri ignored."

    invoke-static {v4, v1}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static a(Ljava/util/Map;Ljava/lang/Object;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 59
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 62
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    goto :goto_0
.end method

.method public static a(Ljava/util/Set;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 73
    if-eqz p0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_SET:Ljava/util/Set;

    if-ne p0, v0, :cond_1

    .line 74
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(II)Lorg/apache/http/client/HttpClient;
    .locals 3

    .prologue
    .line 91
    sget-object v0, La;->l:Ljava/lang/String;

    sget-object v1, La;->m:Lezn;

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, p1, v2}, La;->a(Ljava/lang/String;Lezn;IIZ)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lezn;IIZ)Lorg/apache/http/client/HttpClient;
    .locals 7

    .prologue
    .line 78
    :try_start_0
    invoke-virtual {p1}, Lezn;->a()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    const-string v1, "HttpClient.UserAgent: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 85
    new-instance v1, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v1}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 88
    const/4 v0, 0x0

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 90
    invoke-static {v1, p2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 91
    invoke-static {v1, p3}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 92
    const/16 v0, 0x2000

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 93
    const/4 v0, 0x1

    invoke-static {v1, v0}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 95
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, " gzip"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 96
    new-instance v0, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 97
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v4

    const/16 v5, 0x50

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 99
    :try_start_1
    new-instance v2, Lorg/apache/http/conn/scheme/Scheme;

    const-string v3, "https"

    new-instance v4, Lorg/apache/http/conn/ssl/SSLSocketFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lorg/apache/http/conn/ssl/SSLSocketFactory;-><init>(Ljava/security/KeyStore;Ljava/lang/String;)V

    const/16 v5, 0x1bb

    invoke-direct {v2, v3, v4, v5}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v2}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;
    :try_end_1
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_4

    .line 113
    new-instance v2, Lorg/apache/http/conn/params/ConnPerRouteBean;

    const/16 v3, 0x10

    invoke-direct {v2, v3}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {v1, v2}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 114
    new-instance v2, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v2, v1, v0}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    .line 115
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0, v2, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 116
    if-eqz p4, :cond_0

    .line 117
    new-instance v1, Lews;

    invoke-direct {v1}, Lews;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->addRequestInterceptor(Lorg/apache/http/HttpRequestInterceptor;)V

    .line 119
    :cond_0
    new-instance v1, Lewt;

    invoke-direct {v1}, Lewt;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 120
    new-instance v1, Lewr;

    invoke-direct {v1}, Lewr;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->setCookieStore(Lorg/apache/http/client/CookieStore;)V

    .line 121
    :goto_1
    return-object v0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    const-string v1, "googlePlayProviderInstaller failed to install."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 81
    new-instance v0, Lexe;

    invoke-direct {v0}, Lexe;-><init>()V

    goto :goto_1

    .line 84
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 100
    :catch_1
    move-exception v0

    .line 101
    const-string v1, "Failed to create SSLSocketFactory."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 102
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 103
    :catch_2
    move-exception v0

    .line 104
    const-string v1, "Failed to create SSLSocketFactory."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 106
    :catch_3
    move-exception v0

    .line 107
    const-string v1, "Failed to create SSLSocketFactory."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 109
    :catch_4
    move-exception v0

    .line 110
    const-string v1, "Failed to create SSLSocketFactory."

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 111
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public static a(Ljava/lang/String;Lezn;Z)Lorg/apache/http/client/HttpClient;
    .locals 1

    .prologue
    const/16 v0, 0x4e20

    .line 47
    invoke-static {p0, p1, v0, v0, p2}, La;->a(Ljava/lang/String;Lezn;IIZ)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lidh;)Lorg/json/JSONObject;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 28
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 29
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 32
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v5

    array-length v6, v5

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_c

    aget-object v7, v5, v4

    .line 34
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 36
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v8

    .line 39
    const-string v0, "EMPTY_ARRAY"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    invoke-static {v7, p0}, La;->a(Ljava/lang/reflect/Field;Lidh;)Ljava/lang/Object;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v1, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32
    :cond_0
    :goto_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 56
    :cond_1
    instance-of v0, p0, Lhcv;

    if-eqz v0, :cond_2

    const-string v0, "clientName"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    invoke-virtual {v7, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    .line 59
    sparse-switch v0, :sswitch_data_0

    .line 79
    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x13

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "UNKNOWN_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 82
    :goto_2
    const-string v7, "clientName"

    invoke-virtual {v1, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 109
    :catch_0
    move-exception v0

    .line 112
    :try_start_1
    const-string v2, "Exception while trying to convert protoMessage: "

    .line 113
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    :goto_3
    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 114
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 115
    const-string v1, "PROTO CONVERSION FAILED"

    const-string v2, "See error logs and file bug."

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 121
    :goto_4
    return-object v0

    .line 61
    :sswitch_0
    :try_start_2
    const-string v0, "ANDROID"

    goto :goto_2

    .line 64
    :sswitch_1
    const-string v0, "ANDROID_CREATOR"

    goto :goto_2

    .line 67
    :sswitch_2
    const-string v0, "ANDROID_INSTANT"

    goto :goto_2

    .line 70
    :sswitch_3
    const-string v0, "ANDROID_KIDS"

    goto :goto_2

    .line 73
    :sswitch_4
    const-string v0, "ANDROID_TV"

    goto :goto_2

    .line 76
    :sswitch_5
    const-string v0, "ANDROID_MUSIC"

    goto :goto_2

    .line 87
    :cond_2
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    const-class v9, Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v7, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_5
    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {v7, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 94
    instance-of v7, v0, Lidh;

    if-eqz v7, :cond_9

    .line 96
    check-cast v0, Lidh;

    invoke-static {v0}, La;->a(Lidh;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_1

    :cond_3
    move v0, v3

    .line 87
    goto :goto_5

    :cond_4
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v7, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_5

    :cond_5
    move v0, v3

    goto :goto_5

    :cond_6
    invoke-virtual {v7}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    sget-object v9, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-virtual {v0, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v7, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_5

    :cond_7
    invoke-virtual {v7, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    move v0, v2

    goto :goto_5

    :cond_8
    move v0, v3

    goto :goto_5

    .line 97
    :cond_9
    instance-of v7, v0, [B

    if-eqz v7, :cond_a

    .line 98
    check-cast v0, [B

    .line 99
    new-instance v7, Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v0, v9}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v8, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto/16 :goto_1

    .line 101
    :cond_a
    invoke-virtual {v1, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 113
    :cond_b
    :try_start_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_3

    .line 117
    :catch_1
    move-exception v0

    .line 118
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "This should never happen."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_c
    move-object v0, v1

    .line 121
    goto/16 :goto_4

    .line 59
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xe -> :sswitch_1
        0x12 -> :sswitch_3
        0x14 -> :sswitch_2
        0x15 -> :sswitch_5
        0x17 -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(Lwm;)Lwd;
    .locals 15

    .prologue
    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    .line 40
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 42
    iget-object v14, p0, Lwm;->c:Ljava/util/Map;

    .line 50
    const-string v0, "Date"

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 54
    if-eqz v0, :cond_a

    .line 55
    invoke-static {v0}, La;->a(Ljava/lang/String;)J

    move-result-wide v0

    move-wide v2, v0

    .line 58
    :goto_0
    const-string v0, "Cache-Control"

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    if-eqz v0, :cond_9

    .line 60
    const/4 v1, 0x1

    .line 61
    const-string v7, ","

    invoke-virtual {v0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    move v0, v6

    move-wide v6, v4

    .line 62
    :goto_1
    array-length v9, v8

    if-ge v0, v9, :cond_5

    .line 63
    aget-object v9, v8, v0

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 64
    const-string v10, "no-cache"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "no-store"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 65
    :cond_0
    const/4 v0, 0x0

    .line 101
    :goto_2
    return-object v0

    .line 66
    :cond_1
    const-string v10, "max-age="

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 68
    const/16 v10, 0x8

    :try_start_0
    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 62
    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 71
    :cond_3
    const-string v10, "must-revalidate"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "proxy-revalidate"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_4
    move-wide v6, v4

    .line 72
    goto :goto_3

    :cond_5
    move-wide v8, v6

    move v6, v1

    .line 77
    :goto_4
    const-string v0, "Expires"

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 78
    if-eqz v0, :cond_8

    .line 79
    invoke-static {v0}, La;->a(Ljava/lang/String;)J

    move-result-wide v0

    move-wide v10, v0

    .line 82
    :goto_5
    const-string v0, "ETag"

    invoke-interface {v14, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 86
    if-eqz v6, :cond_7

    .line 87
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, v8

    add-long/2addr v4, v12

    .line 93
    :cond_6
    :goto_6
    new-instance v1, Lwd;

    invoke-direct {v1}, Lwd;-><init>()V

    .line 94
    iget-object v6, p0, Lwm;->b:[B

    iput-object v6, v1, Lwd;->a:[B

    .line 95
    iput-object v0, v1, Lwd;->b:Ljava/lang/String;

    .line 96
    iput-wide v4, v1, Lwd;->e:J

    .line 97
    iget-wide v4, v1, Lwd;->e:J

    iput-wide v4, v1, Lwd;->d:J

    .line 98
    iput-wide v2, v1, Lwd;->c:J

    .line 99
    iput-object v14, v1, Lwd;->f:Ljava/util/Map;

    move-object v0, v1

    .line 101
    goto :goto_2

    .line 88
    :cond_7
    cmp-long v1, v2, v4

    if-lez v1, :cond_6

    cmp-long v1, v10, v2

    if-ltz v1, :cond_6

    .line 90
    sub-long v4, v10, v2

    add-long/2addr v4, v12

    goto :goto_6

    .line 70
    :catch_0
    move-exception v9

    goto :goto_3

    :cond_8
    move-wide v10, v4

    goto :goto_5

    :cond_9
    move-wide v8, v4

    goto :goto_4

    :cond_a
    move-wide v2, v4

    goto/16 :goto_0
.end method

.method public static a([BLhtx;)Lwd;
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-static {p0, v0, p1}, La;->a([BLjava/util/Map;Lhtx;)Lwd;

    move-result-object v0

    return-object v0
.end method

.method public static a([BLjava/util/Map;Lhtx;)Lwd;
    .locals 6

    .prologue
    .line 80
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    iget v0, p2, Lhtx;->c:I

    if-nez v0, :cond_1

    .line 81
    :cond_0
    const/4 v0, 0x0

    .line 95
    :goto_0
    return-object v0

    .line 84
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, p2, Lhtx;->c:I

    int-to-long v4, v3

    .line 85
    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 87
    new-instance v0, Lwd;

    invoke-direct {v0}, Lwd;-><init>()V

    .line 88
    iput-object p0, v0, Lwd;->a:[B

    .line 89
    iput-wide v2, v0, Lwd;->e:J

    .line 90
    iput-wide v2, v0, Lwd;->d:J

    .line 91
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lwd;->c:J

    .line 92
    if-eqz p1, :cond_2

    .line 93
    :goto_1
    iput-object p1, v0, Lwd;->f:Ljava/util/Map;

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    goto :goto_1
.end method

.method public static a(Ljava/lang/Class;)Lwv;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lgkz;

    invoke-direct {v0, p0}, Lgkz;-><init>(Ljava/lang/Class;)V

    return-object v0
.end method

.method public static a(III[F)V
    .locals 10

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v3, 0x437f0000    # 255.0f

    const/high16 v7, 0x40000000    # 2.0f

    .line 149
    int-to-float v0, p0

    div-float/2addr v0, v3

    .line 150
    int-to-float v1, p1

    div-float/2addr v1, v3

    .line 151
    int-to-float v2, p2

    div-float/2addr v2, v3

    .line 153
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 154
    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 155
    sub-float v5, v3, v4

    .line 158
    add-float v6, v3, v4

    div-float/2addr v6, v7

    .line 160
    cmpl-float v4, v3, v4

    if-nez v4, :cond_0

    .line 162
    const/4 v0, 0x0

    move v1, v0

    .line 175
    :goto_0
    const/4 v2, 0x0

    const/high16 v3, 0x42700000    # 60.0f

    mul-float/2addr v1, v3

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v1, v3

    aput v1, p3, v2

    .line 176
    const/4 v1, 0x1

    aput v0, p3, v1

    .line 177
    const/4 v0, 0x2

    aput v6, p3, v0

    .line 178
    return-void

    .line 164
    :cond_0
    cmpl-float v4, v3, v0

    if-nez v4, :cond_1

    .line 165
    sub-float v0, v1, v2

    div-float/2addr v0, v5

    const/high16 v1, 0x40c00000    # 6.0f

    rem-float/2addr v0, v1

    .line 172
    :goto_1
    mul-float v1, v6, v7

    sub-float/2addr v1, v8

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v8, v1

    div-float v1, v5, v1

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_0

    .line 166
    :cond_1
    cmpl-float v3, v3, v1

    if-nez v3, :cond_2

    .line 167
    sub-float v0, v2, v0

    div-float/2addr v0, v5

    add-float/2addr v0, v7

    goto :goto_1

    .line 169
    :cond_2
    sub-float/2addr v0, v1

    div-float/2addr v0, v5

    const/high16 v1, 0x40800000    # 4.0f

    add-float/2addr v0, v1

    goto :goto_1
.end method

.method public static declared-synchronized a(Labt;)V
    .locals 9

    .prologue
    .line 23
    const-class v8, La;

    monitor-enter v8

    :try_start_0
    new-instance v0, Labu;

    .line 24
    iget-object v1, p0, Labt;->c:Ljava/lang/String;

    .line 25
    iget-object v2, p0, Labt;->b:Ljava/lang/String;

    .line 26
    iget v3, p0, Labt;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 27
    iget-object v4, p0, Labt;->i:Labw;

    if-nez v4, :cond_0

    new-instance v4, Laby;

    iget-object v5, p0, Labt;->j:Ljava/lang/String;

    iget-object v6, p0, Labt;->k:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Laby;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Labt;->i:Labw;

    :cond_0
    iget-object v4, p0, Labt;->i:Labw;

    .line 28
    iget v5, p0, Labt;->d:I

    .line 29
    iget v6, p0, Labt;->e:I

    .line 30
    iget v7, p0, Labt;->f:I

    invoke-direct/range {v0 .. v7}, Labu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Labw;III)V

    .line 32
    sput-object v0, La;->b:Labu;

    iget v1, p0, Labt;->g:I

    invoke-virtual {v0, v1}, Labu;->b(I)V

    .line 33
    iget-object v0, p0, Labt;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 34
    sget-object v3, La;->b:Labu;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Labu;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 36
    :cond_1
    monitor-exit v8

    return-void
.end method

.method public static a(Landroid/app/Activity;I)V
    .locals 2

    .prologue
    .line 20
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 21
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 23
    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;Lbgo;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 36
    if-nez v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 39
    :cond_0
    new-instance v1, Lbgn;

    invoke-direct {v1, p1}, Lbgn;-><init>(Lbgo;)V

    .line 40
    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 41
    new-array v2, v3, [Landroid/app/Activity;

    invoke-virtual {v0, v1, p0, v2}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public static a(Landroid/app/Notification$Builder;Lbn;)V
    .locals 5

    .prologue
    .line 101
    new-instance v1, Landroid/app/Notification$Action$Builder;

    invoke-virtual {p1}, Lbn;->a()I

    move-result v0

    invoke-virtual {p1}, Lbn;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, Lbn;->c()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Landroid/app/Notification$Action$Builder;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 103
    invoke-virtual {p1}, Lbn;->e()[Lbx;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p1}, Lbn;->e()[Lbx;

    move-result-object v0

    invoke-static {v0}, La;->a([Lbx;)[Landroid/app/RemoteInput;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 106
    invoke-virtual {v1, v4}, Landroid/app/Notification$Action$Builder;->addRemoteInput(Landroid/app/RemoteInput;)Landroid/app/Notification$Action$Builder;

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_0
    invoke-virtual {p1}, Lbn;->d()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 110
    invoke-virtual {p1}, Lbn;->d()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Action$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/Notification$Action$Builder;

    .line 112
    :cond_1
    invoke-virtual {v1}, Landroid/app/Notification$Action$Builder;->build()Landroid/app/Notification$Action;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Notification$Builder;->addAction(Landroid/app/Notification$Action;)Landroid/app/Notification$Builder;

    .line 113
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 62
    invoke-static {p0}, La;->d(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, La;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 77
    invoke-static {p0}, La;->d(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    :goto_0
    return-void

    .line 83
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    const/16 v0, 0x8

    .line 88
    :goto_1
    invoke-static {v0}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 91
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 95
    invoke-static {v0}, Lia;->a(Landroid/view/accessibility/AccessibilityEvent;)Liu;

    move-result-object v1

    .line 96
    sget-object v2, Liu;->a:Lix;

    iget-object v1, v1, Liu;->b:Ljava/lang/Object;

    invoke-virtual {v2, v1, p1}, Lix;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 97
    invoke-static {p0}, La;->d(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 83
    :cond_1
    const/16 v0, 0x4000

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 35
    invoke-static {p0}, Laav;->a(Landroid/content/Context;)Laav;

    move-result-object v0

    iget-object v1, v0, Laav;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Laav;->c:Ljava/util/Set;

    invoke-interface {v2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Laav;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Lhog;)V
    .locals 6

    .prologue
    .line 45
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 50
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p3, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 56
    const-string v1, "navigation_endpoint"

    .line 57
    invoke-static {p4}, Lidh;->a(Lidh;)[B

    move-result-object v2

    .line 56
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 65
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x41dfffffffc00000L    # 2.147483647E9

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 66
    const/high16 v2, 0x40000000    # 2.0f

    .line 67
    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 69
    new-instance v1, Lba;

    invoke-direct {v1, p0}, Lba;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x1

    .line 70
    invoke-virtual {v1, v2}, Lba;->b(Z)Lba;

    move-result-object v1

    .line 71
    iput-object v0, v1, Lba;->d:Landroid/app/PendingIntent;

    .line 72
    invoke-virtual {v1, p2}, Lba;->b(Ljava/lang/CharSequence;)Lba;

    move-result-object v0

    .line 73
    invoke-virtual {v0, p1}, Lba;->a(Ljava/lang/CharSequence;)Lba;

    move-result-object v0

    const v1, 0x7f020196

    .line 74
    invoke-virtual {v0, v1}, Lba;->a(I)Lba;

    move-result-object v0

    .line 75
    iput-object p3, v0, Lba;->e:Landroid/graphics/Bitmap;

    .line 76
    iget-object v1, v0, Lba;->q:Landroid/app/Notification;

    const/4 v2, 0x5

    iput v2, v1, Landroid/app/Notification;->defaults:I

    iget-object v1, v0, Lba;->q:Landroid/app/Notification;

    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/app/Notification;->flags:I

    .line 77
    invoke-virtual {v0}, Lba;->a()Landroid/app/Notification;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_0

    .line 80
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const/16 v2, 0x3ea

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x0

    .line 90
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 93
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    const-string v1, "screen_name"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    :cond_0
    new-instance v1, Laan;

    invoke-direct {v1, p0, p1, v0}, Laan;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)V

    invoke-virtual {v1}, Laan;->a()V

    .line 105
    return-void
.end method

.method public static a(Landroid/content/res/Resources;Landroid/preference/ListPreference;Landroid/preference/ListPreference;Landroid/preference/ListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Landroid/preference/ListPreference;Landroid/preference/ListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Landroid/preference/ListPreference;Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;Landroid/preference/ListPreference;)V
    .locals 3

    .prologue
    .line 40
    .line 42
    invoke-static {p0}, Ldfd;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-static {}, Ldfd;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 40
    invoke-static {p1, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 49
    invoke-static {p0}, Ldfc;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-static {}, Ldfc;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 47
    invoke-static {p2, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 56
    invoke-static {p0}, Ldew;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-static {}, Ldew;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    .line 54
    invoke-static {p3, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 63
    invoke-static {p0}, Ldeu;->b(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 64
    invoke-static {}, Ldeu;->e()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 61
    invoke-static {p4, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 66
    invoke-static {}, Ldeu;->f()[I

    move-result-object v0

    iput-object v0, p4, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a:[I

    .line 71
    invoke-static {p0}, Ldfa;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-static {}, Ldfa;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    .line 69
    invoke-static {p5, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 78
    invoke-static {p0}, Ldev;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-static {}, Ldev;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 76
    invoke-static {p6, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 85
    invoke-static {p0}, Ldeu;->b(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-static {}, Ldeu;->e()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 83
    invoke-static {p7, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 88
    invoke-static {}, Ldeu;->f()[I

    move-result-object v0

    iput-object v0, p7, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a:[I

    .line 93
    invoke-static {p0}, Ldeu;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-static {}, Ldeu;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    .line 91
    invoke-static {p8, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 96
    invoke-static {}, Ldeu;->b()[I

    move-result-object v0

    iput-object v0, p8, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a:[I

    .line 101
    invoke-static {p0}, Ldfa;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 102
    invoke-static {}, Ldfa;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    .line 99
    invoke-static {p9, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 108
    invoke-static {p0}, Ldeu;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {}, Ldeu;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 106
    invoke-static {p10, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 111
    invoke-static {}, Ldeu;->b()[I

    move-result-object v0

    iput-object v0, p10, Lcom/google/android/apps/youtube/app/ui/SubtitlesColorListPreference;->a:[I

    .line 116
    invoke-static {p0}, Ldfa;->a(Landroid/content/res/Resources;)[Ljava/lang/String;

    move-result-object v0

    .line 117
    invoke-static {}, Ldfa;->a()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    .line 114
    invoke-static {p11, v0, v1, v2}, La;->a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 119
    return-void
.end method

.method public static a(Landroid/os/Parcel;II)V
    .locals 4

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v0

    if-eq v0, p2, :cond_0

    new-instance v1, Ll;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected size "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0, p0}, Ll;-><init>(Ljava/lang/String;Landroid/os/Parcel;)V

    throw v1

    :cond_0
    return-void
.end method

.method public static a(Landroid/os/Parcel;IJ)V
    .locals 2

    const/4 v0, 0x2

    const/16 v1, 0x8

    invoke-static {p0, v0, v1}, La;->b(Landroid/os/Parcel;II)V

    invoke-virtual {p0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method

.method public static a(Landroid/os/Parcel;ILandroid/os/Bundle;Z)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, La;->k(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    invoke-static {p0, v0}, La;->l(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, La;->k(Landroid/os/Parcel;I)I

    move-result v0

    invoke-interface {p2, p0, p3}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-static {p0, v0}, La;->l(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;ILjava/lang/String;Z)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, La;->k(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-static {p0, v0}, La;->l(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;ILjava/util/List;Z)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, La;->k(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    invoke-static {p0, v0}, La;->l(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;IZ)V
    .locals 1

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, La;->b(Landroid/os/Parcel;II)V

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;I[BZ)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, La;->k(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeByteArray([B)V

    invoke-static {p0, v0}, La;->l(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;I[Ljava/lang/String;Z)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, La;->k(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    invoke-static {p0, v0}, La;->l(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;Lida;)V
    .locals 1

    .prologue
    .line 27
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 28
    return-void

    .line 27
    :cond_0
    invoke-virtual {p1}, Lida;->c()[B

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/os/Parcel;Lidh;)V
    .locals 1

    .prologue
    .line 38
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 39
    return-void

    .line 38
    :cond_0
    invoke-static {p1}, Lidh;->a(Lidh;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/preference/ListPreference;[Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0, p1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {p0, p2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 162
    invoke-virtual {p0}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_0

    .line 163
    invoke-virtual {p0, p3}, Landroid/preference/ListPreference;->setValueIndex(I)V

    .line 165
    :cond_0
    const-string v0, "%s"

    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 166
    return-void
.end method

.method public static a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V
    .locals 2

    .prologue
    const v1, 0x7f08001e

    .line 134
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    invoke-static {p1, p2, p3}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 136
    invoke-virtual {p2, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 137
    new-instance v0, Lfuo;

    invoke-direct {v0, p0, p2}, Lfuo;-><init>(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p2, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 141
    :cond_0
    return-void
.end method

.method public static a(Lboi;Landroid/view/View;Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 99
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    if-eqz p0, :cond_3

    iget-object v0, p0, Lboi;->b:Lboq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lboi;->b:Lboq;

    invoke-interface {v0, p0, p2, v1}, Lboq;->a(Lboi;Ljava/lang/Object;Z)V

    iget-object v0, p0, Lboi;->a:Lbok;

    invoke-virtual {v0}, Lbok;->notifyDataSetChanged()V

    :cond_0
    iget-object v0, p0, Lboi;->a:Lbok;

    invoke-virtual {v0}, Lbok;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 103
    invoke-virtual {p0, p1, p2}, Lboi;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 104
    iget-boolean v0, p0, Lboi;->c:Z

    .line 108
    :goto_0
    sget-object v2, La;->h:Landroid/view/View$AccessibilityDelegate;

    if-nez v2, :cond_1

    new-instance v2, Lbpf;

    invoke-direct {v2}, Lbpf;-><init>()V

    sput-object v2, La;->h:Landroid/view/View$AccessibilityDelegate;

    :cond_1
    sget-object v2, La;->h:Landroid/view/View$AccessibilityDelegate;

    invoke-virtual {p1, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 109
    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 110
    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 111
    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 112
    if-eqz v0, :cond_2

    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 113
    return-void

    .line 112
    :cond_2
    const/16 v1, 0x8

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/youtube/app/YouTubeApplication;Larh;Landroid/content/SharedPreferences;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 60
    sget-boolean v0, La;->c:Z

    if-nez v0, :cond_3

    .line 61
    invoke-static {p0}, La;->s(Landroid/content/Context;)I

    move-result v3

    .line 62
    invoke-virtual {p1}, Larh;->a()I

    move-result v4

    .line 63
    invoke-virtual {p1}, Larh;->b()I

    move-result v5

    .line 64
    invoke-virtual {p1}, Larh;->c()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 65
    invoke-virtual {p1}, Larh;->d()Landroid/util/SparseBooleanArray;

    move-result-object v6

    .line 66
    invoke-virtual {p1}, Larh;->e()J

    move-result-wide v8

    sput-wide v8, La;->g:J

    .line 67
    const-string v7, "upgrade_prompt_shown_millis"

    const-wide/16 v8, 0x0

    invoke-interface {p2, v7, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    sput-wide v8, La;->f:J

    .line 68
    if-lt v3, v4, :cond_0

    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, La;->d:Z

    .line 69
    if-lt v3, v5, :cond_1

    invoke-virtual {v6, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    sput-boolean v1, La;->e:Z

    .line 70
    sput-boolean v2, La;->c:Z

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "App version = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1d

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Min app version = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Target app version = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 75
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, La;->f:J

    sub-long/2addr v0, v2

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Prompt shown ago = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 77
    :cond_3
    return-void

    :cond_4
    move v0, v1

    .line 68
    goto :goto_0
.end method

.method public static a(Ldza;)V
    .locals 3

    .prologue
    .line 106
    const-wide/16 v0, 0x0

    invoke-virtual {p0}, Ldza;->c()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalThreadStateException;

    const-string v1, "Only the network thread of an EventDispatcher can dispatch events for it"

    invoke-direct {v0, v1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p0, v0, v1}, Ldze;->a(Ldza;J)Ldze;

    invoke-virtual {p0}, Ldza;->a()V

    .line 107
    return-void
.end method

.method public static a(Lejw;)V
    .locals 1

    instance-of v0, p0, Lejv;

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method

.method public static a(Lfbb;)V
    .locals 2

    .prologue
    .line 39
    const-string v0, "/feed/entry"

    new-instance v1, Lfyh;

    invoke-direct {v1}, Lfyh;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 52
    const-string v0, "/feed/entry"

    invoke-static {p0, v0}, La;->d(Lfbb;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public static a(Lfbb;Lezj;Lcqy;)V
    .locals 2

    .prologue
    .line 44
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v0, "/VMAP"

    invoke-static {v0}, La;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcsh;

    invoke-direct {v1}, Lcsh;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 61
    const-string v0, "/VMAP/AdBreak"

    invoke-static {v0}, La;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcsi;

    invoke-direct {v1}, Lcsi;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 83
    const-string v0, "/VMAP/AdBreak/TrackingEvents/Tracking"

    invoke-static {v0}, La;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcsj;

    invoke-direct {v1}, Lcsj;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 101
    const-string v0, "/VMAP/AdBreak/Extensions/Extension"

    invoke-static {v0}, La;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/yt:BreakTime"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcsk;

    invoke-direct {v1}, Lcsk;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 113
    const-string v0, "/VMAP/Extensions/Extension"

    invoke-static {v0}, La;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/TrackingDecoration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcsl;

    invoke-direct {v1}, Lcsl;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 128
    const-string v0, "/VMAP/Extensions/Extension"

    invoke-static {v0}, La;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/TrackingMacro"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcsm;

    invoke-direct {v1}, Lcsm;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 144
    const-string v0, "/VMAP/AdBreak/AdSource/VASTData"

    invoke-static {v0}, La;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcsn;

    invoke-direct {v1}, Lcsn;-><init>()V

    invoke-static {v0, p1, p0, v1, p2}, Lcrg;->a(Ljava/lang/String;Lezj;Lfbb;Lcsf;Lcqy;)V

    .line 155
    return-void
.end method

.method public static a(Lfbb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 37
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-static {p0, p1}, La;->c(Lfbb;Ljava/lang/String;)V

    .line 39
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfxz;

    invoke-direct {v1}, Lfxz;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 65
    return-void
.end method

.method public static a(Lggp;)V
    .locals 7

    .prologue
    .line 21
    iget-object v2, p0, Lggp;->a:Ljava/lang/String;

    .line 22
    iget-object v0, p0, Lggp;->c:Ljava/lang/Object;

    .line 24
    instance-of v1, v0, Ljava/lang/Throwable;

    if-eqz v1, :cond_0

    .line 25
    check-cast v0, Ljava/lang/Throwable;

    move-object v1, v2

    .line 30
    :goto_0
    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    return-void

    .line 27
    :cond_0
    const/4 v1, 0x0

    .line 28
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Ljava/io/File;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 254
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Source %s and destination %s must be different"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    .line 257
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 256
    invoke-static {v0, v3}, Lb;->c(ZLjava/lang/Object;)V

    .line 259
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 260
    invoke-virtual {p0, p1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Source %s and destination %s must be different"

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p0, v4, v2

    aput-object p1, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    invoke-static {p0}, La;->a(Ljava/io/File;)Lewb;

    move-result-object v0

    invoke-static {p1}, La;->b(Ljava/io/File;)Lewa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lewb;->a(Lewa;)J

    .line 261
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_3

    .line 262
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_2

    .line 263
    new-instance v0, Ljava/io/IOException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 256
    goto :goto_0

    :cond_1
    move v0, v2

    .line 260
    goto :goto_1

    .line 265
    :cond_2
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_3
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 73
    check-cast p0, Landroid/transition/Transition;

    .line 74
    invoke-static {p1}, La;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v0

    .line 76
    new-instance v1, Lag;

    invoke-direct {v1, v0}, Lag;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v1}, Landroid/transition/Transition;->setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V

    .line 82
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/view/View;Z)V
    .locals 0

    .prologue
    .line 63
    check-cast p0, Landroid/transition/Transition;

    .line 64
    invoke-virtual {p0, p1, p2}, Landroid/transition/Transition;->excludeTarget(Landroid/view/View;Z)Landroid/transition/Transition;

    .line 65
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 82
    check-cast p0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$Callback;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->removeCallback(Landroid/media/MediaRouter$Callback;)V

    .line 84
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;Lal;Landroid/view/View;Lak;Ljava/util/Map;Ljava/util/ArrayList;Ljava/util/Map;Ljava/util/ArrayList;)V
    .locals 9

    .prologue
    .line 101
    if-nez p0, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    move-object v6, p0

    .line 102
    check-cast v6, Landroid/transition/Transition;

    .line 103
    if-eqz v6, :cond_1

    .line 104
    invoke-virtual {v6, p4}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 106
    :cond_1
    if-eqz p1, :cond_2

    .line 107
    check-cast p1, Landroid/transition/Transition;

    .line 108
    move-object/from16 v0, p9

    invoke-static {p1, v0}, La;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 111
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v8

    new-instance v1, Lah;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object/from16 v5, p8

    move-object/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lah;-><init>(Landroid/view/View;Lal;Ljava/util/Map;Ljava/util/Map;Landroid/transition/Transition;Ljava/util/ArrayList;)V

    invoke-virtual {v8, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 140
    if-eqz v6, :cond_3

    new-instance v1, Lai;

    invoke-direct {v1, p5}, Lai;-><init>(Lak;)V

    invoke-virtual {v6, v1}, Landroid/transition/Transition;->setEpicenterCallback(Landroid/transition/Transition$EpicenterCallback;)V

    .line 142
    :cond_3
    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/StringBuilder;)V
    .locals 2

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_2

    .line 33
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 34
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 35
    if-lez v1, :cond_2

    .line 36
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    :cond_2
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    const/16 v0, 0x7b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 41
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 308
    check-cast p0, Landroid/transition/Transition;

    .line 309
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 310
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 311
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/transition/Transition;->removeTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 310
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 313
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Lesv;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 180
    const-string v0, "linear"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 181
    iput-boolean v1, p1, Lesv;->c:Z

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    const-string v0, "nonlinear"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 183
    iput-boolean v1, p1, Lesv;->d:Z

    goto :goto_0

    .line 184
    :cond_2
    const-string v0, "display"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    iput-boolean v1, p1, Lesv;->e:Z

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lezn;)V
    .locals 0

    .prologue
    .line 47
    sput-object p0, La;->l:Ljava/lang/String;

    .line 48
    sput-object p1, La;->m:Lezn;

    .line 49
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V
    .locals 11

    const/16 v3, 0xc8

    const/4 v1, 0x0

    if-eqz p1, :cond_8

    instance-of v0, p1, Leqk;

    if-eqz v0, :cond_9

    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    if-eqz p0, :cond_0

    invoke-virtual {p3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-static {p0}, La;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, " <\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "  "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v6

    array-length v7, v6

    move v3, v1

    :goto_0
    if-ge v3, v7, :cond_5

    aget-object v0, v6, v3

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v8

    and-int/lit8 v9, v2, 0x1

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    and-int/lit8 v2, v2, 0x8

    const/16 v9, 0x8

    if-eq v2, v9, :cond_1

    const-string v2, "_"

    invoke-virtual {v8, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "_"

    invoke-virtual {v8, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_2

    invoke-static {v8, v9, p2, p3}, La;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    if-nez v9, :cond_3

    move v0, v1

    :goto_2
    move v2, v1

    :goto_3
    if-ge v2, v0, :cond_1

    invoke-static {v9, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v10

    invoke-static {v8, v10, p2, p3}, La;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    invoke-static {v9}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    goto :goto_2

    :cond_4
    invoke-static {v8, v9, p2, p3}, La;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v5}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v6, v3

    move v2, v1

    :goto_4
    if-ge v2, v6, :cond_7

    aget-object v0, v3, v2

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v7, "set"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "has"

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v5, v0, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    new-array v8, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "get"

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v5, v0, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    new-array v8, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v7, v0, p2, p3}, La;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    :cond_6
    :goto_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_7
    if-eqz p0, :cond_8

    invoke-virtual {p2, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    invoke-virtual {p3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_8
    :goto_6
    return-void

    :cond_9
    invoke-static {p0}, La;->v(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_b

    check-cast p1, Ljava/lang/String;

    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[...]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_a
    invoke-static {p1}, La;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {p3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_7
    const-string v0, "\n"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_6

    :cond_b
    instance-of v0, p1, [B

    if-eqz v0, :cond_c

    check-cast p1, [B

    invoke-static {p1, p3}, La;->a([BLjava/lang/StringBuffer;)V

    goto :goto_7

    :cond_c
    invoke-virtual {p3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_7

    :catch_0
    move-exception v0

    goto :goto_5

    :catch_1
    move-exception v0

    goto :goto_5
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 31
    const-string v0, "YouTubeAndroidPlayerAPI"

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    return-void
.end method

.method static a(Ljava/util/ArrayList;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 231
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 232
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    .line 233
    check-cast p1, Landroid/view/ViewGroup;

    .line 234
    invoke-virtual {p1}, Landroid/view/ViewGroup;->isTransitionGroup()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 238
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    .line 239
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 240
    invoke-static {p0, v2}, La;->a(Ljava/util/ArrayList;Landroid/view/View;)V

    .line 238
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 244
    :cond_2
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 251
    invoke-virtual {p1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_0

    .line 253
    invoke-interface {p0, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 256
    check-cast p1, Landroid/view/ViewGroup;

    .line 257
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 258
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 259
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 260
    invoke-static {p0, v2}, La;->a(Ljava/util/Map;Landroid/view/View;)V

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    :cond_1
    return-void
.end method

.method public static a(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 32
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 34
    if-nez v0, :cond_0

    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 36
    invoke-interface {p0, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public static a(Lorg/apache/http/HttpResponse;)V
    .locals 1

    .prologue
    .line 23
    if-eqz p0, :cond_0

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 25
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;IJ)V
    .locals 10

    .prologue
    .line 100
    const-wide/16 v0, 0x10

    div-long v2, p6, v0

    .line 101
    const-wide/16 v0, 0x10

    rem-long v0, p6, v0

    long-to-int v1, v0

    .line 104
    add-int v0, v1, p2

    new-array v4, v0, [B

    .line 105
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 106
    add-int v5, v1, v0

    add-int v6, p1, v0

    aget-byte v6, p0, v6

    aput-byte v6, v4, v5

    .line 105
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_0
    const-string v0, "AES/CTR/NoPadding"

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {p4}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    sget-object v6, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    const/16 v6, 0x8

    const/16 v7, 0x8

    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v8

    add-long/2addr v2, v8

    invoke-virtual {v5, v6, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, p5, p3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 110
    invoke-virtual {v0, v4}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v2

    .line 112
    const/4 v0, 0x0

    :goto_1
    if-ge v0, p2, :cond_1

    .line 113
    add-int v3, p1, v0

    add-int v4, v1, v0

    aget-byte v4, v2, v4

    aput-byte v4, p0, v3

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 115
    :cond_1
    return-void
.end method

.method public static a([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;J)V
    .locals 9

    .prologue
    .line 121
    const/4 v1, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v6, p5

    invoke-static/range {v0 .. v7}, La;->a([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;IJ)V

    .line 122
    return-void
.end method

.method public static a([BLjava/io/File;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 165
    invoke-static {p1}, La;->b(Ljava/io/File;)Lewa;

    move-result-object v0

    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lewa;->a()Ljava/io/OutputStream;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1, p0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v2, [Ljava/io/Closeable;

    aput-object v1, v0, v3

    invoke-static {v0}, Lewa;->a([Ljava/io/Closeable;)V

    return-void

    :catchall_0
    move-exception v0

    new-array v2, v2, [Ljava/io/Closeable;

    aput-object v1, v2, v3

    invoke-static {v2}, Lewa;->a([Ljava/io/Closeable;)V

    throw v0
.end method

.method public static a([BLjava/lang/StringBuffer;)V
    .locals 7

    const/16 v6, 0x5c

    const/4 v1, 0x0

    const/16 v5, 0x22

    if-nez p0, :cond_0

    const-string v0, "\"\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v0, v1

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_4

    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    if-eq v2, v6, :cond_1

    if-ne v2, v5, :cond_2

    :cond_1
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    int-to-char v2, v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/16 v3, 0x20

    if-lt v2, v3, :cond_3

    const/16 v3, 0x7f

    if-ge v2, v3, :cond_3

    int-to-char v2, v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_3
    const-string v3, "\\%03o"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_4
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lezk;)Z
    .locals 1

    .prologue
    .line 91
    const/16 v0, 0x2d0

    invoke-static {p0, v0}, La;->c(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, La;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-interface {p1}, Lezk;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lfsg;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 24
    .line 25
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00ec

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 26
    iget v2, p1, Lfsg;->f:I

    if-ne v2, v0, :cond_0

    iget v2, p1, Lfsg;->c:I

    if-ge v2, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/SharedPreferences;)Z
    .locals 2

    .prologue
    .line 122
    const-string v0, "subtitles_background_color"

    const/4 v1, 0x0

    .line 123
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 124
    if-eqz v0, :cond_0

    sget-object v1, Ldeu;->a:Ldeu;

    .line 125
    invoke-virtual {v1}, Ldeu;->ordinal()I

    move-result v1

    .line 126
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lezk;)Z
    .locals 2

    .prologue
    .line 152
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    invoke-interface {p0}, Lezk;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lgjm;)Z
    .locals 3

    .prologue
    .line 36
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    iget-object v0, p0, Lgjm;->g:Lgje;

    const-string v1, "ad"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lgje;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static a(Lhqt;)Z
    .locals 1

    .prologue
    .line 38
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget v0, p0, Lhqt;->c:I

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 4

    .prologue
    .line 23
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 24
    const/16 v1, 0x200

    new-array v1, v1, [B

    .line 26
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 27
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 28
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 32
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;I)[B
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 88
    new-array v0, p1, [B

    move v1, p1

    .line 91
    :goto_0
    if-lez v1, :cond_2

    .line 92
    sub-int v2, p1, v1

    .line 93
    invoke-virtual {p0, v0, v2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 94
    if-ne v3, v4, :cond_1

    .line 97
    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    .line 116
    :cond_0
    :goto_1
    return-object v0

    .line 99
    :cond_1
    sub-int/2addr v1, v3

    .line 100
    goto :goto_0

    .line 103
    :cond_2
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 104
    if-eq v1, v4, :cond_0

    .line 109
    new-instance v2, Lewc;

    invoke-direct {v2}, Lewc;-><init>()V

    .line 110
    invoke-virtual {v2, v1}, Lewc;->write(I)V

    .line 111
    invoke-static {p0, v2}, La;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 113
    array-length v1, v0

    invoke-virtual {v2}, Lewc;->size()I

    move-result v3

    add-int/2addr v1, v3

    new-array v1, v1, [B

    .line 114
    array-length v3, v0

    invoke-static {v0, v5, v1, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 115
    array-length v0, v0

    invoke-virtual {v2, v1, v0}, Lewc;->a([BI)V

    move-object v0, v1

    .line 116
    goto :goto_1
.end method

.method public static a(Ljava/io/InputStream;J)[B
    .locals 3

    .prologue
    .line 98
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/OutOfMemoryError;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x44

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "file is too large to fit in a byte array: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 106
    invoke-static {p0}, La;->b(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    .line 106
    :cond_1
    long-to-int v0, p1

    .line 107
    invoke-static {p0, v0}, La;->a(Ljava/io/InputStream;I)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static a([BI)[B
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 34
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 35
    if-lez p1, :cond_1

    :goto_1
    invoke-static {v1}, Lb;->b(Z)V

    .line 36
    array-length v0, p0

    rem-int/2addr v0, p1

    .line 37
    if-nez v0, :cond_2

    .line 38
    invoke-virtual {p0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 40
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 34
    goto :goto_0

    :cond_1
    move v1, v2

    .line 35
    goto :goto_1

    .line 40
    :cond_2
    array-length v1, p0

    sub-int v0, p1, v0

    add-int/2addr v0, v1

    invoke-static {p0, v0}, La;->b([BI)[B

    move-result-object v0

    goto :goto_2
.end method

.method public static a([BIB)[B
    .locals 3

    .prologue
    .line 47
    const/16 v0, 0xbc

    invoke-static {p0, v0}, La;->a([BI)[B

    move-result-object v1

    .line 48
    array-length v0, p0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 49
    const/4 v2, -0x1

    aput-byte v2, v1, v0

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return-object v1
.end method

.method public static a([BII)[B
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 75
    if-ltz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lb;->b(Z)V

    .line 76
    if-ltz p2, :cond_2

    :goto_2
    invoke-static {v1}, Lb;->b(Z)V

    .line 77
    new-array v0, p2, [B

    .line 78
    invoke-static {p0, p1, v0, v2, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 79
    return-object v0

    :cond_0
    move v0, v2

    .line 74
    goto :goto_0

    :cond_1
    move v0, v2

    .line 75
    goto :goto_1

    :cond_2
    move v1, v2

    .line 76
    goto :goto_2
.end method

.method public static varargs a([[B)[B
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 18
    .line 19
    array-length v3, p0

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p0, v1

    .line 20
    array-length v4, v4

    add-int/2addr v2, v4

    .line 19
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 22
    :cond_0
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 23
    array-length v2, p0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, p0, v0

    .line 24
    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 23
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 26
    :cond_1
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    return-object v0
.end method

.method public static varargs a([I[I)[I
    .locals 4

    .prologue
    .line 140
    array-length v0, p0

    array-length v1, p1

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    .line 141
    const/4 v1, 0x0

    array-length v2, p0

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    return-object v0
.end method

.method static a([Lbx;)[Landroid/app/RemoteInput;
    .locals 5

    .prologue
    .line 39
    if-nez p0, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 52
    :goto_0
    return-object v0

    .line 42
    :cond_0
    array-length v0, p0

    new-array v1, v0, [Landroid/app/RemoteInput;

    .line 43
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 44
    aget-object v2, p0, v0

    .line 45
    new-instance v3, Landroid/app/RemoteInput$Builder;

    invoke-virtual {v2}, Lbx;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/app/RemoteInput$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lbx;->b()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/RemoteInput$Builder;->setLabel(Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lbx;->c()[Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/RemoteInput$Builder;->setChoices([Ljava/lang/CharSequence;)Landroid/app/RemoteInput$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lbx;->d()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/RemoteInput$Builder;->setAllowFreeFormInput(Z)Landroid/app/RemoteInput$Builder;

    move-result-object v3

    invoke-virtual {v2}, Lbx;->e()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/app/RemoteInput$Builder;->addExtras(Landroid/os/Bundle;)Landroid/app/RemoteInput$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/RemoteInput$Builder;->build()Landroid/app/RemoteInput;

    move-result-object v2

    aput-object v2, v1, v0

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 52
    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 127
    array-length v0, p0

    array-length v1, p1

    add-int/2addr v0, v1

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    .line 128
    const/4 v1, 0x0

    array-length v2, p0

    array-length v3, p1

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 129
    return-object v0
.end method

.method public static b(F)I
    .locals 1

    .prologue
    .line 29
    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr v0, p0

    float-to-int v0, v0

    return v0
.end method

.method public static b(Lhqt;)I
    .locals 1

    .prologue
    .line 52
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {p0}, La;->a(Lhqt;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lhqt;->c:I

    neg-int v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 155
    if-eqz p0, :cond_0

    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 157
    :cond_0
    :goto_0
    return p1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static b(Ljava/nio/ByteBuffer;)I
    .locals 2

    .prologue
    .line 43
    invoke-static {p0}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v0

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 45
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    invoke-static {v1}, La;->a(B)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    return v0
.end method

.method public static b(Landroid/content/Context;)Labh;
    .locals 3

    const-string v0, "Calling this from your main thread can lead to deadlock"

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-static {p0}, La;->a(Landroid/content/Context;)Labm;

    move-result-object v0

    invoke-static {p0, v0}, La;->a(Landroid/content/Context;Labm;)Labh;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/app/Activity;I)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 156
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    .line 157
    invoke-virtual {v3}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v4

    .line 158
    invoke-virtual {v3, v8}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 159
    invoke-virtual {v3}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_0

    .line 161
    const/high16 v5, 0x100000

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 163
    :cond_0
    :goto_0
    if-nez v4, :cond_1

    .line 164
    invoke-virtual {v3, v7}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 165
    invoke-virtual {v3}, Landroid/view/View;->destroyDrawingCache()V

    .line 167
    :cond_1
    return-object v0

    .line 161
    :cond_2
    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v7}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    :goto_1
    mul-int v6, v2, v1

    shl-int/lit8 v6, v6, 0x1

    if-le v6, v5, :cond_3

    div-int/lit8 v2, v2, 0x2

    div-int/lit8 v1, v1, 0x2

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-eq v2, v5, :cond_0

    invoke-static {v0, v2, v1, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 45
    invoke-static {p0, p1}, La;->a(Landroid/content/Context;I)I

    move-result v0

    .line 46
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;)Lbbv;
    .locals 1

    .prologue
    .line 44
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v0, p1, Lhog;->z:Lhyd;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    new-instance v0, Lbbn;

    invoke-direct {v0, p0}, Lbbn;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;)V

    return-object v0
.end method

.method public static b(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 563
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 568
    invoke-static {}, Lbpt;->a()Lbpt;

    move-result-object v1

    .line 565
    invoke-static {p0, v0, v1, p1}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lboi;Lbpw;Lfrz;)Lboi;

    .line 570
    return-object v0
.end method

.method public static b(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 177
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 182
    invoke-static {}, Lbps;->a()Lbps;

    move-result-object v1

    .line 178
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 184
    return-object v0
.end method

.method public static b(Ljava/io/File;)Lewa;
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lewd;

    invoke-direct {v0, p0}, Lewd;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static b(Lhyr;)Lfpc;
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lhyr;->a:Ljava/lang/String;

    invoke-static {v0, v12}, La;->b(Ljava/lang/String;I)I

    move-result v2

    .line 42
    new-instance v0, Lfpc;

    invoke-direct {v0, v2}, Lfpc;-><init>(I)V

    .line 44
    iget-object v2, p0, Lhyr;->b:[Lgyl;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhyr;->b:[Lgyl;

    array-length v2, v2

    if-nez v2, :cond_2

    .line 45
    :cond_0
    const-string v1, "Invalid Vast Ad proto with no Ads."

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    .line 117
    :cond_1
    :goto_0
    return-object v0

    .line 50
    :cond_2
    iget-object v5, p0, Lhyr;->b:[Lgyl;

    array-length v6, v5

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_16

    aget-object v2, v5, v4

    .line 52
    iget v7, v2, Lgyl;->b:I

    if-ne v7, v11, :cond_3

    .line 58
    :goto_2
    if-nez v2, :cond_4

    .line 59
    const-string v1, "Invalid Vast Ad proto with no inLine Ad."

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :cond_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 63
    :cond_4
    iget-object v4, v2, Lgyl;->d:Ljava/lang/String;

    iput-object v4, v0, Lfpc;->m:Ljava/lang/String;

    .line 64
    iget-object v5, v2, Lgyl;->c:Lhis;

    .line 65
    iget-object v2, v5, Lhis;->b:Ljava/lang/String;

    iput-object v2, v0, Lfpc;->j:Ljava/lang/String;

    .line 66
    iget-object v2, v5, Lhis;->a:Lgyt;

    if-eqz v2, :cond_5

    .line 67
    iget-object v2, v5, Lhis;->a:Lgyt;

    iget-object v2, v2, Lgyt;->a:Ljava/lang/String;

    iput-object v2, v0, Lfpc;->n:Ljava/lang/String;

    .line 70
    :cond_5
    iget-object v2, v5, Lhis;->d:[Lhym;

    if-eqz v2, :cond_6

    .line 71
    iget-object v4, v5, Lhis;->d:[Lhym;

    array-length v6, v4

    move v2, v1

    :goto_3
    if-ge v2, v6, :cond_6

    aget-object v7, v4, v2

    .line 73
    :try_start_0
    iget-object v7, v7, Lhym;->b:Ljava/lang/String;

    invoke-static {v7}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 74
    invoke-virtual {v0, v7}, Lfpc;->a(Landroid/net/Uri;)Lfpc;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 76
    :catch_0
    move-exception v7

    const-string v7, "Badly formed impression uri - ignoring"

    invoke-static {v7}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_4

    .line 81
    :cond_6
    iget-object v2, v5, Lhis;->c:[Ljava/lang/String;

    if-eqz v2, :cond_7

    .line 82
    iget-object v4, v5, Lhis;->c:[Ljava/lang/String;

    array-length v6, v4

    move v2, v1

    :goto_5
    if-ge v2, v6, :cond_7

    aget-object v7, v4, v2

    .line 84
    :try_start_1
    invoke-static {v7}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 85
    invoke-virtual {v0, v7}, Lfpc;->n(Landroid/net/Uri;)Lfpc;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 82
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 87
    :catch_1
    move-exception v7

    const-string v7, "Badly formed error uri - ignoring"

    invoke-static {v7}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_6

    .line 93
    :cond_7
    iget-object v2, v5, Lhis;->e:[Lhiv;

    if-eqz v2, :cond_e

    .line 94
    iget-object v4, v5, Lhis;->e:[Lhiv;

    array-length v6, v4

    move v2, v1

    :goto_7
    if-ge v2, v6, :cond_e

    aget-object v7, v4, v2

    .line 95
    iget v8, v7, Lhiv;->b:I

    if-ne v8, v11, :cond_d

    .line 96
    iget-object v2, v7, Lhiv;->c:Lhiw;

    .line 97
    iget-object v6, v2, Lhiw;->a:[Lhxp;

    array-length v7, v6

    move v4, v1

    :goto_8
    if-ge v4, v7, :cond_e

    aget-object v2, v6, v4

    .line 98
    if-nez v2, :cond_9

    const-string v2, "Badly formed tracking event - ignoring"

    invoke-static {v2}, Lezp;->c(Ljava/lang/String;)V

    .line 97
    :cond_8
    :goto_9
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_8

    .line 98
    :cond_9
    :try_start_2
    iget-object v8, v2, Lhxp;->c:Ljava/lang/String;

    invoke-static {v8}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iget v9, v2, Lhxp;->b:I

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    const-string v2, "Badly formed tracking uri - ignoring"

    invoke-static {v2}, Lezp;->c(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_9

    :catch_2
    move-exception v2

    const-string v2, "Badly formed tracking uri - ignoring"

    invoke-static {v2}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_9

    :pswitch_1
    :try_start_3
    invoke-virtual {v0, v8}, Lfpc;->b(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_2
    invoke-virtual {v0, v8}, Lfpc;->c(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_3
    invoke-virtual {v0, v8}, Lfpc;->d(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_4
    invoke-virtual {v0, v8}, Lfpc;->e(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_5
    invoke-virtual {v0, v8}, Lfpc;->h(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_6
    invoke-virtual {v0, v8}, Lfpc;->j(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_7
    invoke-virtual {v0, v8}, Lfpc;->k(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_8
    invoke-virtual {v0, v8}, Lfpc;->l(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_9
    invoke-virtual {v0, v8}, Lfpc;->m(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_a
    invoke-virtual {v0, v8}, Lfpc;->i(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_b
    iget v2, v0, Lfpc;->a:I

    if-lt v2, v12, :cond_8

    invoke-virtual {v0, v8}, Lfpc;->f(Landroid/net/Uri;)Lfpc;

    goto :goto_9

    :pswitch_c
    iget v9, v0, Lfpc;->a:I

    if-lt v9, v12, :cond_8

    iget-object v9, v2, Lhxp;->d:Lhpx;

    iget v2, v9, Lhpx;->a:I

    if-ne v2, v11, :cond_a

    iget v2, v9, Lhpx;->b:F

    const/4 v10, 0x0

    cmpl-float v2, v2, v10

    if-ltz v2, :cond_c

    iget v2, v9, Lhpx;->b:F

    const/high16 v10, 0x42c80000    # 100.0f

    cmpg-float v2, v2, v10

    if-gtz v2, :cond_c

    new-instance v2, Lfpf;

    iget v9, v9, Lhpx;->b:F

    float-to-int v9, v9

    const/4 v10, 0x1

    invoke-direct {v2, v9, v10, v8}, Lfpf;-><init>(IZLandroid/net/Uri;)V

    :goto_a
    if-eqz v2, :cond_8

    invoke-virtual {v0, v2}, Lfpc;->a(Lfpf;)Lfpc;

    goto :goto_9

    :cond_a
    iget v2, v9, Lhpx;->a:I

    const/4 v10, 0x2

    if-ne v2, v10, :cond_b

    new-instance v2, Lfpf;

    iget v9, v9, Lhpx;->c:I

    const/4 v10, 0x0

    invoke-direct {v2, v9, v10, v8}, Lfpf;-><init>(IZLandroid/net/Uri;)V

    goto :goto_a

    :cond_b
    const-string v2, "Badly formed progress tracking uri - ignoring"

    invoke-static {v2}, Lezp;->c(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_c
    move-object v2, v3

    goto :goto_a

    .line 94
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_7

    .line 105
    :cond_e
    iget-object v2, v5, Lhis;->g:[Lhjc;

    if-eqz v2, :cond_11

    .line 106
    new-instance v4, Lfhv;

    invoke-direct {v4}, Lfhv;-><init>()V

    .line 107
    iget-object v6, v5, Lhis;->g:[Lhjc;

    array-length v7, v6

    move v2, v1

    :goto_b
    if-ge v2, v7, :cond_10

    aget-object v8, v6, v2

    .line 108
    invoke-static {v8}, Lfhh;->a(Lhjc;)Lfpi;

    move-result-object v8

    .line 109
    if-eqz v8, :cond_f

    .line 110
    invoke-virtual {v4, v8}, Lfhv;->a(Lfpi;)Lfhv;

    .line 107
    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 113
    :cond_10
    invoke-virtual {v4}, Lfhv;->a()Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lfpc;->ah:Ljava/util/List;

    .line 116
    :cond_11
    iget-object v6, v5, Lhis;->f:[Lhgj;

    if-eqz v6, :cond_1

    array-length v7, v6

    move v5, v1

    :goto_c
    if-ge v5, v7, :cond_14

    aget-object v2, v6, v5

    iget-object v8, v2, Lhgj;->b:[Lhau;

    array-length v9, v8

    move v4, v1

    :goto_d
    if-ge v4, v9, :cond_13

    aget-object v10, v8, v4

    iget-object v11, v10, Lhau;->b:Lhof;

    if-eqz v11, :cond_12

    const-string v11, "type"

    iget-object v12, v10, Lhau;->b:Lhof;

    iget-object v12, v12, Lhof;->a:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_12

    const-string v11, "adsense"

    iget-object v10, v10, Lhau;->c:Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_12

    :goto_e
    if-eqz v2, :cond_1

    iget-object v2, v2, Lhgj;->c:[Lhok;

    array-length v3, v2

    :goto_f
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    iget-object v5, v4, Lhok;->b:Lhof;

    if-eqz v5, :cond_15

    const-string v5, "ConversionUrl"

    iget-object v6, v4, Lhok;->b:Lhof;

    iget-object v6, v6, Lhof;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_15

    :try_start_4
    iget-object v1, v4, Lhok;->c:Ljava/lang/String;

    invoke-static {v1}, La;->E(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lfpc;->Q:Landroid/net/Uri;
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v1

    const-string v1, "Badly formed ConversionUrl uri - ignoring"

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    :cond_13
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_c

    :cond_14
    move-object v2, v3

    goto :goto_e

    :cond_15
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    :cond_16
    move-object v2, v3

    goto/16 :goto_2

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public static b(Lhog;)Lhqt;
    .locals 1

    .prologue
    .line 25
    if-eqz p0, :cond_0

    iget-object v0, p0, Lhog;->i:Libf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhog;->i:Libf;

    iget-object v0, v0, Libf;->k:Libg;

    if-nez v0, :cond_1

    .line 28
    :cond_0
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lhog;->i:Libf;

    iget-object v0, v0, Libf;->k:Libg;

    iget-object v0, v0, Libg;->b:Lhqt;

    goto :goto_0
.end method

.method public static b([B)Lhtx;
    .locals 1

    .prologue
    .line 32
    if-nez p0, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 42
    :goto_0
    return-object v0

    .line 40
    :cond_0
    new-instance v0, Liai;

    invoke-direct {v0}, Liai;-><init>()V

    .line 41
    invoke-static {v0, p0}, Lidh;->a(Lidh;[B)Lidh;

    .line 42
    iget-object v0, v0, Liai;->a:Lhtx;

    goto :goto_0
.end method

.method public static b(Landroid/os/Parcel;Lida;)Lida;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 53
    if-nez v0, :cond_0

    .line 54
    const/4 p1, 0x0

    .line 57
    :goto_0
    return-object p1

    .line 56
    :cond_0
    invoke-virtual {p1, v0}, Lida;->b([B)Lida;

    goto :goto_0
.end method

.method public static b(Landroid/os/Parcel;Lidh;)Lidh;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 72
    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, v0}, Lidh;->a(Lidh;[B)Lidh;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    if-nez p0, :cond_0

    .line 80
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 82
    :cond_0
    return-object p0
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 92
    check-cast p0, Landroid/media/MediaRouter;

    check-cast p1, Landroid/media/MediaRouter$RouteCategory;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter;->createUserRoute(Landroid/media/MediaRouter$RouteCategory;)Landroid/media/MediaRouter$UserRouteInfo;

    move-result-object v0

    return-object v0
.end method

.method public static b(II)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 76
    div-int/lit8 v0, p0, 0x3c

    .line 77
    div-int/lit8 v2, v0, 0x3c

    .line 78
    if-lez v2, :cond_0

    .line 79
    rem-int/lit8 v0, v0, 0x3c

    .line 80
    const/4 v1, 0x5

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 82
    :cond_0
    rem-int/lit8 v1, p0, 0x3c

    .line 84
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v5, :cond_1

    .line 86
    const-string v3, "0"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 89
    :cond_1
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v3, v5, :cond_2

    const/4 v3, 0x3

    if-le p1, v3, :cond_2

    .line 91
    const-string v3, "0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    :cond_2
    :goto_1
    const/4 v3, 0x4

    if-le p1, v3, :cond_5

    .line 95
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    :goto_2
    return-object v0

    .line 86
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 98
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public static b(J)Ljava/lang/String;
    .locals 4

    .prologue
    .line 105
    const-wide/16 v0, 0x400

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x16

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 108
    :cond_0
    long-to-float v0, p0

    const/high16 v1, 0x44800000    # 1024.0f

    div-float/2addr v0, v1

    .line 109
    new-instance v1, Ljava/text/DecimalFormat;

    const-string v2, "#.##"

    invoke-direct {v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "GB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lgjm;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 44
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    iget-object v0, p0, Lgjm;->g:Lgje;

    const-string v1, "playlist_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lidh;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 61
    if-nez p0, :cond_0

    .line 62
    const-string v0, ""

    .line 73
    :goto_0
    return-object v0

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 67
    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {v1, p0, v2, v0}, La;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 73
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 68
    :catch_0
    move-exception v0

    .line 69
    const-string v1, "Error printing proto: "

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :catch_1
    move-exception v0

    .line 71
    const-string v1, "Error printing proto: "

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Ljava/util/Map;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 40
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    :cond_0
    const-string v0, ""

    .line 51
    :goto_0
    return-object v0

    .line 43
    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 44
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 47
    :try_start_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 49
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "UTF-8 should be supported by the server"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v2, v7, v0}, Ljava/lang/StringBuffer;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/os/Parcel;ILandroid/os/Parcelable$Creator;)Ljava/util/ArrayList;
    .locals 3

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_0
.end method

.method public static b(Ljava/util/Set;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 124
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 125
    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 126
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_0
    return-object v1
.end method

.method public static varargs b([Ljava/lang/Object;)Ljava/util/Set;
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 19
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v3}, Ljava/util/HashSet;-><init>(I)V

    .line 24
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v2, p0, v0

    .line 25
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 24
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lezn;Z)Lorg/apache/http/client/HttpClient;
    .locals 3

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    invoke-static {p0, p1, v0}, La;->a(Ljava/lang/String;Lezn;Z)Lorg/apache/http/client/HttpClient;

    move-result-object v1

    .line 138
    instance-of v0, v1, Lorg/apache/http/impl/client/AbstractHttpClient;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 139
    check-cast v0, Lorg/apache/http/impl/client/AbstractHttpClient;

    .line 140
    new-instance v2, Lexi;

    invoke-direct {v2}, Lexi;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/impl/client/AbstractHttpClient;->setRedirectHandler(Lorg/apache/http/client/RedirectHandler;)V

    .line 141
    new-instance v2, Lewr;

    invoke-direct {v2}, Lewr;-><init>()V

    invoke-virtual {v0, v2}, Lorg/apache/http/impl/client/AbstractHttpClient;->setCookieStore(Lorg/apache/http/client/CookieStore;)V

    .line 143
    :cond_0
    return-object v1
.end method

.method public static b(Labt;)V
    .locals 2

    .prologue
    .line 58
    if-nez p0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "configuration can\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_0
    invoke-static {p0}, La;->a(Labt;)V

    .line 62
    return-void
.end method

.method public static b(Landroid/os/Parcel;I)V
    .locals 2

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v0

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    return-void
.end method

.method public static b(Landroid/os/Parcel;II)V
    .locals 1

    const v0, 0xffff

    if-lt p2, v0, :cond_0

    const/high16 v0, -0x10000

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    return-void

    :cond_0
    shl-int/lit8 v0, p2, 0x10

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method

.method public static b(Landroid/os/Parcel;ILjava/util/List;Z)V
    .locals 7

    const/4 v2, 0x0

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p0, p1}, La;->k(Landroid/os/Parcel;I)I

    move-result v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {p0, v4}, Landroid/os/Parcel;->writeInt(I)V

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v6

    invoke-interface {v0, p0, v2}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    invoke-virtual {p0, v5}, Landroid/os/Parcel;->setDataPosition(I)V

    sub-int v5, v0, v6

    invoke-virtual {p0, v5}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_2

    :cond_2
    invoke-static {p0, v3}, La;->l(Landroid/os/Parcel;I)V

    goto :goto_0
.end method

.method public static b(Lfbb;)V
    .locals 2

    .prologue
    .line 61
    const-string v0, "/entry"

    new-instance v1, Lfyp;

    invoke-direct {v1}, Lfyp;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 67
    const-string v0, "/entry"

    invoke-static {p0, v0}, La;->d(Lfbb;Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public static b(Lfbb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 72
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-static {p0, p1}, La;->c(Lfbb;Ljava/lang/String;)V

    .line 74
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfya;

    invoke-direct {v1}, Lfya;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 80
    return-void
.end method

.method public static b(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 208
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackType(I)V

    .line 209
    return-void
.end method

.method public static b(Ljava/lang/Object;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 316
    check-cast p0, Landroid/transition/Transition;

    .line 317
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 318
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 319
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/transition/Transition;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    .line 318
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 321
    :cond_0
    return-void
.end method

.method public static synthetic b(Ljava/lang/String;Lesv;)V
    .locals 4

    .prologue
    .line 34
    if-nez p0, :cond_1

    const-string v0, "in Vmap AdBreak: timeOffset is null"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v0, v1

    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-static {v3, p1}, La;->a(Ljava/lang/String;Lesv;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-static {p0, p1}, La;->a(Ljava/lang/String;Lesv;)V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V
    .locals 11

    .prologue
    const/16 v3, 0xc8

    const/4 v1, 0x0

    .line 90
    if-eqz p1, :cond_a

    .line 96
    instance-of v0, p1, Lidh;

    if-eqz v0, :cond_b

    .line 97
    invoke-virtual {p2}, Ljava/lang/StringBuffer;->length()I

    move-result v4

    .line 98
    if-eqz p0, :cond_0

    .line 99
    invoke-virtual {p3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-static {p0}, La;->O(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, " <\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    const-string v0, "  "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .line 108
    invoke-virtual {v5}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v6

    array-length v7, v6

    move v3, v1

    :goto_0
    if-ge v3, v7, :cond_5

    aget-object v0, v6, v3

    .line 109
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v2

    .line 110
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v8

    .line 112
    and-int/lit8 v9, v2, 0x1

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    and-int/lit8 v2, v2, 0x8

    const/16 v9, 0x8

    if-eq v2, v9, :cond_1

    const-string v2, "_"

    .line 114
    invoke-virtual {v8, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "_"

    .line 115
    invoke-virtual {v8, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 116
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v2

    .line 117
    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 119
    invoke-virtual {v2}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 120
    invoke-virtual {v2}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    .line 123
    sget-object v2, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-ne v0, v2, :cond_2

    .line 124
    invoke-static {v8, v9, p2, p3}, La;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 108
    :cond_1
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 126
    :cond_2
    if-nez v9, :cond_3

    move v0, v1

    :goto_2
    move v2, v1

    .line 127
    :goto_3
    if-ge v2, v0, :cond_1

    .line 128
    invoke-static {v9, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v10

    .line 129
    invoke-static {v8, v10, p2, p3}, La;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 126
    :cond_3
    invoke-static {v9}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    goto :goto_2

    .line 133
    :cond_4
    invoke-static {v8, v9, p2, p3}, La;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    goto :goto_1

    .line 141
    :cond_5
    invoke-virtual {v5}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v6, v3

    move v2, v1

    :goto_4
    if-ge v2, v6, :cond_9

    aget-object v0, v3, v2

    .line 142
    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    .line 145
    const-string v7, "set"

    invoke-virtual {v0, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 146
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 148
    :try_start_0
    const-string v8, "has"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_7

    invoke-virtual {v8, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v5, v0, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 155
    new-array v8, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 156
    :try_start_1
    const-string v8, "get"

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_8

    invoke-virtual {v8, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v5, v0, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 166
    new-array v8, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v7, v0, p2, p3}, La;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/StringBuffer;Ljava/lang/StringBuffer;)V

    .line 141
    :cond_6
    :goto_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 148
    :cond_7
    :try_start_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    .line 152
    :catch_0
    move-exception v0

    goto :goto_7

    .line 156
    :cond_8
    :try_start_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_6

    .line 163
    :catch_1
    move-exception v0

    goto :goto_7

    .line 169
    :cond_9
    if-eqz p0, :cond_a

    .line 170
    invoke-virtual {p2, v4}, Ljava/lang/StringBuffer;->setLength(I)V

    .line 171
    invoke-virtual {p3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 187
    :cond_a
    :goto_8
    return-void

    .line 175
    :cond_b
    invoke-static {p0}, La;->O(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-virtual {p3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 178
    check-cast p1, Ljava/lang/String;

    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v3, :cond_c

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[...]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_c
    invoke-static {p1}, La;->P(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 179
    const-string v1, "\""

    invoke-virtual {p3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 185
    :goto_9
    const-string v0, "\n"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_8

    .line 180
    :cond_d
    instance-of v0, p1, [B

    if-eqz v0, :cond_e

    .line 181
    check-cast p1, [B

    invoke-static {p1, p3}, La;->b([BLjava/lang/StringBuffer;)V

    goto :goto_9

    .line 183
    :cond_e
    invoke-virtual {p3, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_9
.end method

.method public static varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 38
    const-string v0, "YouTubeAndroidPlayerAPI"

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    return-void
.end method

.method public static b(Z)V
    .locals 1

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 28
    :cond_0
    return-void
.end method

.method public static b([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;J)V
    .locals 9

    .prologue
    .line 128
    const/4 v5, 0x2

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-wide v6, p5

    invoke-static/range {v0 .. v7}, La;->a([BIILjava/security/Key;Ljavax/crypto/spec/IvParameterSpec;IJ)V

    .line 129
    return-void
.end method

.method public static b([BLjava/lang/StringBuffer;)V
    .locals 7

    .prologue
    const/16 v6, 0x5c

    const/4 v1, 0x0

    const/16 v5, 0x22

    .line 239
    if-nez p0, :cond_0

    .line 240
    const-string v0, "\"\""

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 256
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v0, v1

    .line 245
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_4

    .line 246
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    .line 247
    if-eq v2, v6, :cond_1

    if-ne v2, v5, :cond_2

    .line 248
    :cond_1
    invoke-virtual {p1, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    int-to-char v2, v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 245
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 249
    :cond_2
    const/16 v3, 0x20

    if-lt v2, v3, :cond_3

    const/16 v3, 0x7f

    if-ge v2, v3, :cond_3

    .line 250
    int-to-char v2, v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 252
    :cond_3
    const-string v3, "\\%03o"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 255
    :cond_4
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 13
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)Z
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lezk;)Z
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0x438

    invoke-static {p0, v0}, La;->c(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lezk;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 88
    invoke-static {p0}, La;->o(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/SharedPreferences;)Z
    .locals 2

    .prologue
    .line 130
    const-string v0, "subtitles_window_color"

    const/4 v1, 0x0

    .line 131
    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_0

    sget-object v1, Ldeu;->a:Ldeu;

    .line 133
    invoke-virtual {v1}, Ldeu;->ordinal()I

    move-result v1

    .line 134
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eq v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 76
    const-string v0, "file"

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/util/Map;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 46
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    :cond_0
    const/4 v0, 0x0

    .line 50
    :goto_0
    return v0

    .line 49
    :cond_1
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 50
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lhut;)[B
    .locals 1

    .prologue
    .line 51
    if-eqz p0, :cond_0

    iget-object v0, p0, Lhut;->b:[B

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lhut;->b:[B

    .line 54
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lfhy;->a:[B

    goto :goto_0
.end method

.method public static b(Ljava/io/InputStream;)[B
    .locals 1

    .prologue
    .line 75
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 76
    invoke-static {p0, v0}, La;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 77
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 26
    if-eqz p0, :cond_0

    .line 27
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 31
    :catch_0
    move-exception v0

    .line 32
    new-instance v1, Ljava/lang/Error;

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static b([BI)[B
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 61
    if-ltz p1, :cond_1

    :goto_1
    invoke-static {v1}, Lb;->b(Z)V

    .line 62
    new-array v0, p1, [B

    .line 63
    array-length v1, p0

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 64
    return-object v0

    :cond_0
    move v0, v2

    .line 60
    goto :goto_0

    :cond_1
    move v1, v2

    .line 61
    goto :goto_1
.end method

.method public static b(Ljava/util/List;)[I
    .locals 3

    .prologue
    .line 164
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 165
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 166
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    .line 165
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 168
    :cond_0
    return-object v2
.end method

.method static b([Lbx;)[Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 69
    if-nez p0, :cond_0

    .line 70
    const/4 v0, 0x0

    .line 76
    :goto_0
    return-object v0

    .line 72
    :cond_0
    array-length v0, p0

    new-array v1, v0, [Landroid/os/Bundle;

    .line 73
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 74
    aget-object v2, p0, v0

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "resultKey"

    invoke-virtual {v2}, Lbx;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "label"

    invoke-virtual {v2}, Lbx;->b()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string v4, "choices"

    invoke-virtual {v2}, Lbx;->c()[Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putCharSequenceArray(Ljava/lang/String;[Ljava/lang/CharSequence;)V

    const-string v4, "allowFreeFormInput"

    invoke-virtual {v2}, Lbx;->d()Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "extras"

    invoke-virtual {v2}, Lbx;->e()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    aput-object v3, v1, v0

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 76
    goto :goto_0
.end method

.method public static c(II)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 104
    sub-int v0, p0, p1

    if-le v0, p1, :cond_1

    .line 106
    sub-int v0, p0, p1

    :goto_0
    move v2, v1

    .line 113
    :goto_1
    if-le p0, v0, :cond_3

    .line 114
    mul-int/2addr v2, p0

    .line 115
    if-gt v1, p1, :cond_0

    .line 116
    div-int/2addr v2, v1

    .line 117
    add-int/lit8 v1, v1, 0x1

    .line 113
    :cond_0
    add-int/lit8 p0, p0, -0x1

    goto :goto_1

    .line 108
    :cond_1
    sub-int v0, p0, p1

    move v3, v0

    move v0, p1

    move p1, v3

    .line 109
    goto :goto_0

    .line 120
    :goto_2
    if-gt v1, p1, :cond_2

    .line 121
    div-int/2addr v0, v1

    .line 122
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 124
    :cond_2
    return v0

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public static c(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 50
    if-eqz p0, :cond_0

    .line 51
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public static c(Ljava/nio/ByteBuffer;)I
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    invoke-static {v0}, La;->a(B)I

    move-result v0

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 53
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    invoke-static {v1}, La;->a(B)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    return v0
.end method

.method public static declared-synchronized c()Labu;
    .locals 2

    .prologue
    .line 44
    const-class v1, La;

    monitor-enter v1

    :try_start_0
    sget-object v0, La;->b:Labu;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Labt;

    invoke-direct {v0}, Labt;-><init>()V

    invoke-static {v0}, La;->a(Labt;)V

    .line 47
    :cond_0
    sget-object v0, La;->b:Labu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 135
    const-string v0, "http://www.youtube.com/watch?v="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {p0}, La;->x(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "force_fullscreen"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "finish_on_ended"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static c(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lhog;)Lbbv;
    .locals 6

    .prologue
    .line 57
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object v0, p1, Lhog;->x:Lgzp;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    invoke-static {}, La;->O()Landroid/content/Intent;

    move-result-object v1

    .line 61
    iget-object v0, p1, Lhog;->x:Lgzp;

    iget-object v0, v0, Lgzp;->a:Ljava/lang/String;

    iget-object v2, p1, Lhog;->x:Lgzp;

    iget-object v2, v2, Lgzp;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    iget-object v0, p1, Lhog;->x:Lgzp;

    iget-object v2, v0, Lgzp;->c:[Lhld;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 65
    iget-object v5, v4, Lhld;->b:Ljava/lang/String;

    iget-object v4, v4, Lhld;->c:Ljava/lang/String;

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    new-instance v0, Lbbo;

    invoke-direct {v0, p0, v1}, Lbbo;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Landroid/content/Intent;)V

    return-object v0
.end method

.method public static c(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 582
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 588
    invoke-static {}, Lbpx;->a()Lbpx;

    move-result-object v1

    .line 585
    invoke-static {p0, v0, v1, p1}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lboi;Lbpw;Lfrz;)Lboi;

    .line 590
    return-object v0
.end method

.method public static c(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 191
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 196
    invoke-static {}, Lbpo;->a()Lbpo;

    move-result-object v1

    .line 192
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 198
    return-object v0
.end method

.method public static c(Ljava/lang/Object;)Leln;
    .locals 1

    new-instance v0, Leln;

    invoke-direct {v0, p0}, Leln;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static c(Ljava/util/List;)Lifw;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v4, 0xb

    const/4 v3, 0x1

    .line 43
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v0, -0x1

    .line 44
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liia;

    iget-object v0, v0, Liia;->b:Lihw;

    if-nez v0, :cond_8

    .line 45
    add-int/lit8 v0, v1, -0x1

    .line 48
    :goto_0
    mul-int/lit8 v0, v0, 0xc

    .line 50
    new-instance v7, Lifw;

    invoke-direct {v7, v0}, Lifw;-><init>(I)V

    .line 53
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liia;

    .line 54
    iget-object v0, v0, Liia;->b:Lihw;

    iget v5, v0, Lihw;->a:I

    move v1, v4

    move v0, v2

    .line 55
    :goto_1
    if-ltz v1, :cond_1

    .line 56
    shl-int v2, v3, v1

    and-int/2addr v2, v5

    if-eqz v2, :cond_0

    .line 57
    invoke-virtual {v7, v0}, Lifw;->b(I)V

    .line 59
    :cond_0
    add-int/lit8 v2, v0, 0x1

    .line 55
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_1
    move v2, v3

    move v1, v0

    .line 62
    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 63
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liia;

    .line 65
    iget-object v5, v0, Liia;->a:Lihw;

    iget v8, v5, Lihw;->a:I

    move v5, v4

    .line 66
    :goto_3
    if-ltz v5, :cond_3

    .line 67
    shl-int v6, v3, v5

    and-int/2addr v6, v8

    if-eqz v6, :cond_2

    .line 68
    invoke-virtual {v7, v1}, Lifw;->b(I)V

    .line 70
    :cond_2
    add-int/lit8 v6, v1, 0x1

    .line 66
    add-int/lit8 v1, v5, -0x1

    move v5, v1

    move v1, v6

    goto :goto_3

    .line 73
    :cond_3
    iget-object v5, v0, Liia;->b:Lihw;

    if-eqz v5, :cond_5

    .line 74
    iget-object v0, v0, Liia;->b:Lihw;

    iget v6, v0, Lihw;->a:I

    move v0, v1

    move v1, v4

    .line 75
    :goto_4
    if-ltz v1, :cond_6

    .line 76
    shl-int v5, v3, v1

    and-int/2addr v5, v6

    if-eqz v5, :cond_4

    .line 77
    invoke-virtual {v7, v0}, Lifw;->b(I)V

    .line 79
    :cond_4
    add-int/lit8 v5, v0, 0x1

    .line 75
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    move v0, v5

    goto :goto_4

    :cond_5
    move v0, v1

    .line 62
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 83
    :cond_7
    return-object v7

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    const/4 v0, 0x3

    invoke-static {p0, v0}, La;->b(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lgjm;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 52
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lgjm;->g:Lgje;

    const-string v1, "video_id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lgje;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    const/4 v0, 0x1

    invoke-static {v0}, Lb;->b(Z)V

    .line 189
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 195
    :cond_0
    :goto_0
    return-object p0

    .line 192
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x32

    if-le v0, v1, :cond_0

    .line 193
    const/4 v0, 0x0

    const/16 v1, 0x31

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u2026"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static c(Ljava/util/Map;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 64
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 65
    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 66
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static varargs c([Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 56
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    array-length v0, p0

    .line 59
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 60
    invoke-static {v1, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 61
    return-object v1
.end method

.method public static c(J)Ljava/util/Date;
    .locals 6

    .prologue
    .line 31
    new-instance v0, Ljava/util/Date;

    const-wide/32 v2, 0x7c25b080

    sub-long v2, p0, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public static c(Landroid/os/Parcel;II)V
    .locals 1

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, La;->b(Landroid/os/Parcel;II)V

    invoke-virtual {p0, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public static c(Lfbb;)V
    .locals 3

    .prologue
    .line 104
    const-string v0, "/feed"

    new-instance v1, Lfyi;

    invoke-direct {v1}, Lfyi;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/feed/subtitle"

    new-instance v2, Lfyw;

    invoke-direct {v2}, Lfyw;-><init>()V

    .line 110
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/feed/link"

    new-instance v2, Lfyv;

    invoke-direct {v2}, Lfyv;-><init>()V

    .line 116
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    const-string v1, "/feed/openSearch:totalResults"

    new-instance v2, Lfyu;

    invoke-direct {v2}, Lfyu;-><init>()V

    .line 126
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 132
    const-string v0, "/feed"

    invoke-static {p0, v0}, La;->e(Lfbb;Ljava/lang/String;)V

    .line 133
    return-void
.end method

.method public static c(Lfbb;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 84
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry/content/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lgae;->d(Lfbb;Ljava/lang/String;)V

    .line 85
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry/content/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfye;

    invoke-direct {v1}, Lfye;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:when"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyd;

    invoke-direct {v2}, Lfyd;-><init>()V

    .line 97
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/yt:status"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyc;

    invoke-direct {v2}, Lfyc;-><init>()V

    .line 108
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/link"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyb;

    invoke-direct {v2}, Lfyb;-><init>()V

    .line 120
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 131
    return-void
.end method

.method public static c(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 212
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setPlaybackStream(I)V

    .line 213
    return-void
.end method

.method public static c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 160
    check-cast p0, Landroid/media/MediaRouter$RouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$RouteInfo;->setTag(Ljava/lang/Object;)V

    .line 161
    return-void
.end method

.method public static c(Z)V
    .locals 1

    .prologue
    .line 52
    if-nez p0, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 55
    :cond_0
    return-void
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 52
    const-string v0, "Call Network.setContext() before calling this method"

    invoke-static {p0, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;I)Z
    .locals 2

    .prologue
    .line 116
    sget v0, La;->p:I

    if-nez v0, :cond_0

    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sput v0, La;->p:I

    :cond_0
    sget v0, La;->p:I

    if-lt v0, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;Lezk;)Z
    .locals 1

    .prologue
    .line 105
    const/16 v0, 0x5a0

    invoke-static {p0, v0}, La;->c(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lezk;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/content/SharedPreferences;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 138
    const-string v2, "subtitles_edge_type"

    const/4 v3, 0x0

    invoke-interface {p0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 139
    if-nez v2, :cond_1

    .line 145
    :cond_0
    :goto_0
    return v0

    .line 142
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 145
    if-eq v2, v1, :cond_2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static c(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 84
    const-string v0, "127.0.0.1"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "localhost"

    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/os/Parcel;I)Z
    .locals 1

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, La;->a(Landroid/os/Parcel;II)V

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/io/File;)[B
    .locals 1

    .prologue
    .line 154
    invoke-static {p0}, La;->a(Ljava/io/File;)Lewb;

    move-result-object v0

    invoke-virtual {v0}, Lewb;->b()[B

    move-result-object v0

    return-object v0
.end method

.method public static synthetic d(I)I
    .locals 1

    .prologue
    const/16 v0, 0x22

    .line 40
    packed-switch p0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_2
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x12

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x21

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x24

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public static d(J)I
    .locals 4

    .prologue
    .line 29
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p0, v0

    if-gtz v0, :cond_0

    const-wide/32 v0, -0x80000000

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    .line 30
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x62

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "A cast to int has gone wrong. Please contact the mp4parser discussion group ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_1
    long-to-int v0, p0

    return v0
.end method

.method public static d(Landroid/os/Parcel;I)I
    .locals 1

    const/4 v0, 0x4

    invoke-static {p0, p1, v0}, La;->a(Landroid/os/Parcel;II)V

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    return v0
.end method

.method public static d(Lgjm;)I
    .locals 3

    .prologue
    .line 61
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    iget-object v0, p0, Lgjm;->g:Lgje;

    const-string v1, "stream_quality"

    iget-object v2, v0, Lgje;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lgje;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 39
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 40
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 41
    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 42
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 45
    :cond_1
    return p1
.end method

.method public static d(Ljava/nio/ByteBuffer;)I
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    invoke-static {v0}, La;->a(B)I

    move-result v0

    return v0
.end method

.method public static d(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 184
    if-nez p0, :cond_1

    .line 204
    :cond_0
    :goto_0
    return-object p0

    .line 191
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 192
    const-string v1, "v"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 193
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 197
    if-eqz v0, :cond_0

    const-string v3, "youtube.com"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 201
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "youtu.be"

    .line 202
    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 203
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 204
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 28
    const-string v0, "accessibility"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method public static d()Lbgh;
    .locals 3

    .prologue
    .line 41
    const/4 v0, 0x1

    invoke-static {v0}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 42
    new-instance v1, Lbgh;

    const-class v2, Lbdh;

    invoke-direct {v1, v2, v0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static d(Ljava/lang/String;)Lbgh;
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-static {v0}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 85
    const-string v1, "no_history"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 86
    const-string v1, "query"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    new-instance v1, Lbgh;

    const-class v2, Lbec;

    invoke-direct {v1, v2, v0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static d(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 608
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 612
    invoke-static {}, Lbpy;->a()Lbpy;

    move-result-object v1

    .line 609
    invoke-static {p0, v0, v1, p1}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lboi;Lbpw;Lfrz;)Lboi;

    .line 614
    return-object v0
.end method

.method public static d(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 210
    invoke-static {}, Lbpv;->a()Lbpv;

    move-result-object v1

    .line 206
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 212
    return-object v0
.end method

.method public static varargs d([Ljava/lang/Object;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 74
    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 75
    return-object v0
.end method

.method public static d(Ljava/util/Map;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 81
    if-eqz p0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_MAP:Ljava/util/Map;

    if-ne p0, v0, :cond_1

    .line 82
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lfbb;)V
    .locals 2

    .prologue
    .line 42
    const-string v0, ""

    invoke-static {p0, v0}, La;->g(Lfbb;Ljava/lang/String;)V

    .line 43
    const-string v0, "/entry"

    new-instance v1, Lfzh;

    invoke-direct {v1}, Lfzh;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 53
    return-void
.end method

.method public static d(Lfbb;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 71
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/summary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfyt;

    invoke-direct {v1}, Lfyt;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/content"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfys;

    invoke-direct {v2}, Lfys;-><init>()V

    .line 77
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/link"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyr;

    invoke-direct {v2}, Lfyr;-><init>()V

    .line 83
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:countHint"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyq;

    invoke-direct {v2}, Lfyq;-><init>()V

    .line 91
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 97
    invoke-static {p0, p1}, La;->e(Lfbb;Ljava/lang/String;)V

    .line 98
    return-void
.end method

.method public static d(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 224
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 226
    if-nez v0, :cond_1

    .line 240
    :cond_0
    return-void

    .line 236
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 237
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 238
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to create parent directories of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static d(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 216
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolume(I)V

    .line 217
    return-void
.end method

.method public static d(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 228
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    check-cast p1, Landroid/media/MediaRouter$VolumeCallback;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeCallback(Landroid/media/MediaRouter$VolumeCallback;)V

    .line 230
    return-void
.end method

.method public static d(Landroid/content/Context;Lezk;)Z
    .locals 1

    .prologue
    .line 112
    const/16 v0, 0x870

    invoke-static {p0, v0}, La;->c(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lezk;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/SharedPreferences;)Z
    .locals 2

    .prologue
    .line 150
    const-string v0, "subtitles_style"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_0

    .line 152
    invoke-static {}, Ldfc;->c()I

    move-result v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-ne v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Landroid/os/Parcel;I)J
    .locals 2

    const/16 v0, 0x8

    invoke-static {p0, p1, v0}, La;->a(Landroid/os/Parcel;II)V

    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public static e()Lbgh;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-static {v0}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, La;->a(Landroid/os/Bundle;)Lbgh;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;)Lbgh;
    .locals 3

    .prologue
    .line 123
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 125
    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    new-instance v1, Lbgh;

    const-class v2, Lbdw;

    invoke-direct {v1, v2, v0}, Lbgh;-><init>(Ljava/lang/Class;Landroid/os/Bundle;)V

    return-object v1
.end method

.method public static e(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 623
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 628
    invoke-static {}, Lbpr;->a()Lbpr;

    move-result-object v1

    .line 625
    invoke-static {p0, v0, v1, p1}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lboi;Lbpw;Lfrz;)Lboi;

    .line 630
    return-object v0
.end method

.method public static e(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 226
    invoke-static {}, Lbpq;->a()Lbpq;

    move-result-object v1

    .line 222
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 228
    return-object v0
.end method

.method public static e(I)Lguc;
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lgvf;

    invoke-direct {v0, p0}, Lgvf;-><init>(I)V

    return-object v0
.end method

.method public static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 40
    sget-object v0, La;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lerf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    .line 41
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    sput-object v0, La;->o:Ljava/lang/String;

    .line 44
    :cond_0
    sget-object v0, La;->o:Ljava/lang/String;

    return-object v0
.end method

.method public static e(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 84
    :goto_0
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0

    .line 87
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, La;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/util/Map;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lfaf;

    invoke-direct {v0, p0}, Lfaf;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static varargs e([Ljava/lang/Object;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/util/HashSet;

    array-length v1, p0

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 68
    invoke-static {v0, p0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 69
    invoke-static {v0}, La;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lfbb;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 136
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/yt:playlistId"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfyo;

    invoke-direct {v1}, Lfyo;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/yt:private"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyn;

    invoke-direct {v2}, Lfyn;-><init>()V

    .line 142
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/author/name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfym;

    invoke-direct {v2}, Lfym;-><init>()V

    .line 148
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/title"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyl;

    invoke-direct {v2}, Lfyl;-><init>()V

    .line 154
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/updated"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyk;

    invoke-direct {v2}, Lfyk;-><init>()V

    .line 160
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/media:group/media:thumbnail"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfyj;

    invoke-direct {v2}, Lfyj;-><init>()V

    .line 166
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 180
    return-void
.end method

.method public static e(Ljava/io/File;)V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 290
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    const-string v2, "Not a directory: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p0, v3, v0

    .line 292
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 291
    invoke-static {v1, v2}, Lb;->c(ZLjava/lang/Object;)V

    .line 294
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 304
    :cond_0
    return-void

    .line 297
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 298
    if-nez v1, :cond_2

    .line 299
    new-instance v0, Ljava/io/IOException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Error listing files for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_2
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 302
    invoke-static {v3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v3}, La;->e(Ljava/io/File;)V

    :cond_3
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v4

    if-nez v4, :cond_4

    new-instance v0, Ljava/io/IOException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed to delete "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static e(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 220
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeMax(I)V

    .line 221
    return-void
.end method

.method public static e(Landroid/content/SharedPreferences;)Z
    .locals 2

    .prologue
    .line 99
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    const-string v0, "video_notifications_enabled"

    const/4 v1, 0x1

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static e(Landroid/net/Uri;)Z
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 241
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    invoke-virtual {p0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 243
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 246
    const-string v4, "www.youtube.com"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "youtube.com"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 247
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v4, v6, :cond_1

    move v0, v1

    .line 266
    :goto_0
    return v0

    .line 250
    :cond_1
    const-string v4, "user"

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "channel"

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 251
    :cond_2
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 256
    :cond_4
    const-string v4, "gdata.youtube.com"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 257
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x4

    if-ge v0, v4, :cond_5

    move v0, v1

    .line 258
    goto :goto_0

    .line 260
    :cond_5
    const-string v0, "feeds"

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "api"

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "users"

    .line 261
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "channels"

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 262
    :cond_6
    const/4 v0, 0x3

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    move v0, v1

    .line 266
    goto :goto_0
.end method

.method public static e(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lgjm;)[B
    .locals 2

    .prologue
    .line 66
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lgjm;->g:Lgje;

    const-string v1, "click_tracking_params"

    invoke-virtual {v0, v1}, Lgje;->b(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/io/File;)J
    .locals 4

    .prologue
    .line 48
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 49
    :cond_0
    const-wide/16 v0, 0x0

    .line 55
    :goto_0
    return-wide v0

    .line 51
    :cond_1
    new-instance v0, Landroid/os/StatFs;

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 52
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_2

    .line 53
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    mul-long/2addr v0, v2

    goto :goto_0

    .line 55
    :cond_2
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method public static f(Ljava/nio/ByteBuffer;)J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 98
    invoke-static {p0}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    add-long/2addr v0, v4

    .line 101
    cmp-long v2, v0, v4

    if-gez v2, :cond_0

    .line 102
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "I don\'t know how to deal with UInt64! long is not sufficient and I don\'t want to use BigInt"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_0
    invoke-static {p0}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 106
    return-wide v0
.end method

.method public static f()Lbgh;
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-static {v0}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 51
    const-string v1, "tab_index"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    invoke-static {v0}, La;->a(Landroid/os/Bundle;)Lbgh;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 639
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 644
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v1

    .line 641
    invoke-static {p0, v0, v1, p1}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lboi;Lbpw;Lfrz;)Lboi;

    .line 646
    return-object v0
.end method

.method public static f(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 235
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 241
    invoke-static {}, Lbpz;->a()Lbpz;

    move-result-object v1

    .line 237
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 243
    return-object v0
.end method

.method public static f(Landroid/os/Parcel;I)Ljava/lang/String;
    .locals 3

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 20
    invoke-static {p0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 21
    const-string v0, "offline_r:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static f(Lfbb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 25
    invoke-static {p0, p1}, La;->g(Lfbb;Ljava/lang/String;)V

    .line 26
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfzg;

    invoke-direct {v1}, Lfzg;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 39
    return-void
.end method

.method public static f(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 224
    check-cast p0, Landroid/media/MediaRouter$UserRouteInfo;

    invoke-virtual {p0, p1}, Landroid/media/MediaRouter$UserRouteInfo;->setVolumeHandling(I)V

    .line 225
    return-void
.end method

.method public static f(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 48
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 50
    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    if-nez p0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Ljava/nio/ByteBuffer;)D
    .locals 4

    .prologue
    .line 110
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 111
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 113
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    shl-int/lit8 v1, v1, 0x18

    const/high16 v2, -0x1000000

    and-int/2addr v1, v2

    or-int/lit8 v1, v1, 0x0

    .line 115
    const/4 v2, 0x1

    aget-byte v2, v0, v2

    shl-int/lit8 v2, v2, 0x10

    const/high16 v3, 0xff0000

    and-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 116
    const/4 v2, 0x2

    aget-byte v2, v0, v2

    shl-int/lit8 v2, v2, 0x8

    const v3, 0xff00

    and-int/2addr v2, v3

    or-int/2addr v1, v2

    .line 117
    const/4 v2, 0x3

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    .line 118
    int-to-double v0, v0

    const-wide/high16 v2, 0x40f0000000000000L    # 65536.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public static g(Landroid/os/Parcel;I)Landroid/os/Bundle;
    .locals 3

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_0
.end method

.method public static g()Lbgh;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 58
    invoke-static {v2}, La;->a(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 59
    const-string v1, "tab_index"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    invoke-static {v0}, La;->a(Landroid/os/Bundle;)Lbgh;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 250
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 255
    invoke-static {}, Lbqf;->a()Lbqf;

    move-result-object v1

    .line 251
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 257
    return-object v0
.end method

.method public static g(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 25
    invoke-static {p0}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 26
    const-string v0, "offline_c:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static g(Lfbb;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 56
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/entry/summary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfzl;

    invoke-direct {v1}, Lfzl;-><init>()V

    invoke-virtual {p0, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/link"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzk;

    invoke-direct {v2}, Lfzk;-><init>()V

    .line 63
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/author/name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzj;

    invoke-direct {v2}, Lfzj;-><init>()V

    .line 78
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/entry/author/uri"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfzi;

    invoke-direct {v2}, Lfzi;-><init>()V

    .line 87
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 94
    return-void
.end method

.method public static g(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 58
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 60
    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x2d0

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Ljava/nio/ByteBuffer;)F
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 124
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 125
    const/4 v1, 0x0

    aget-byte v1, v0, v1

    shl-int/lit8 v1, v1, 0x8

    const v2, 0xff00

    and-int/2addr v1, v2

    or-int/lit8 v1, v1, 0x0

    int-to-short v1, v1

    .line 127
    const/4 v2, 0x1

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    .line 128
    int-to-float v0, v0

    const/high16 v1, 0x43800000    # 256.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public static h(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 70
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 72
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    return v0
.end method

.method public static synthetic h()J
    .locals 2

    .prologue
    .line 44
    sget-wide v0, La;->f:J

    return-wide v0
.end method

.method public static h(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 264
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 269
    invoke-static {}, Lbqb;->a()Lbqb;

    move-result-object v1

    .line 265
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 271
    return-object v0
.end method

.method public static h(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 158
    const-string v0, "\\/"

    const-string v1, "/vmap:"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static h(Landroid/os/Parcel;I)[B
    .locals 3

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_0
.end method

.method public static i(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 78
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    return v0
.end method

.method public static i(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lfrz;)Lboi;
    .locals 2

    .prologue
    .line 278
    new-instance v0, Lboi;

    invoke-direct {v0, p0}, Lboi;-><init>(Landroid/app/Activity;)V

    .line 283
    invoke-static {}, Lbqa;->a()Lbqa;

    move-result-object v1

    .line 279
    invoke-static {p0, p1, v0, v1, p2}, La;->a(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Ljava/util/concurrent/atomic/AtomicReference;Lboi;Lbqd;Lfrz;)Lboi;

    .line 285
    return-object v0
.end method

.method public static i(Ljava/lang/String;)Lesh;
    .locals 7

    .prologue
    const/16 v1, 0x64

    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 202
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 203
    const-string v0, "in Vmap AdBreak: timeOffset is null or empty"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 204
    new-instance v0, Lesh;

    sget-object v1, Lesl;->f:Lesl;

    invoke-direct {v0, v1, v4, v5}, Lesh;-><init>(Lesl;J)V

    .line 244
    :goto_0
    return-object v0

    .line 205
    :cond_0
    const-string v3, "start"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 206
    new-instance v0, Lesh;

    sget-object v1, Lesl;->c:Lesl;

    invoke-direct {v0, v1, v4, v5}, Lesh;-><init>(Lesl;J)V

    goto :goto_0

    .line 207
    :cond_1
    const-string v3, "end"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 208
    new-instance v0, Lesh;

    sget-object v1, Lesl;->d:Lesl;

    invoke-direct {v0, v1, v4, v5}, Lesh;-><init>(Lesl;J)V

    goto :goto_0

    .line 209
    :cond_2
    const/16 v3, 0x23

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_3

    .line 212
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 213
    if-gtz v1, :cond_8

    .line 214
    :try_start_1
    const-string v2, "in Vmap AdBreak(positional): timeOffset must be >= 1"

    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_3

    .line 220
    :goto_1
    new-instance v1, Lesh;

    sget-object v2, Lesl;->e:Lesl;

    int-to-long v4, v0

    invoke-direct {v1, v2, v4, v5}, Lesh;-><init>(Lesl;J)V

    move-object v0, v1

    goto :goto_0

    .line 217
    :catch_0
    move-exception v1

    .line 218
    :goto_2
    const-string v2, "in Vmap AdBreak(positional): integer parse error"

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 222
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 223
    const/16 v3, 0x25

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v3, v4, :cond_6

    .line 226
    const/4 v3, 0x0

    add-int/lit8 v0, v0, -0x1

    :try_start_2
    invoke-virtual {p0, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v0

    .line 227
    if-gez v0, :cond_5

    .line 228
    :try_start_3
    const-string v1, "in Vmap AdBreak(percentage): value must not be <0"

    invoke-static {v1}, Lezp;->b(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    move v0, v2

    .line 237
    :cond_4
    :goto_3
    new-instance v1, Lesh;

    sget-object v2, Lesl;->b:Lesl;

    int-to-long v4, v0

    invoke-direct {v1, v2, v4, v5}, Lesh;-><init>(Lesl;J)V

    move-object v0, v1

    goto :goto_0

    .line 230
    :cond_5
    if-le v0, v1, :cond_4

    .line 231
    :try_start_4
    const-string v2, "in Vmap AdBreak(percentage): value must not be >100"

    invoke-static {v2}, Lezp;->b(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    .line 232
    goto :goto_3

    .line 234
    :catch_1
    move-exception v0

    move-object v1, v0

    move v0, v2

    .line 235
    :goto_4
    const-string v2, "in Vmap AdBreak(percentage): integer parse error"

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 239
    :cond_6
    invoke-static {p0}, La;->C(Ljava/lang/String;)I

    move-result v0

    .line 240
    if-gez v0, :cond_7

    .line 241
    const-string v0, "in Vmap AdBreak(time): value must not be <00:00:00.000"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 244
    :goto_5
    new-instance v0, Lesh;

    sget-object v1, Lesl;->a:Lesl;

    int-to-long v2, v2

    invoke-direct {v0, v1, v2, v3}, Lesh;-><init>(Lesl;J)V

    goto/16 :goto_0

    .line 234
    :catch_2
    move-exception v1

    goto :goto_4

    .line 217
    :catch_3
    move-exception v0

    move-object v6, v0

    move v0, v1

    move-object v1, v6

    goto :goto_2

    :cond_7
    move v2, v0

    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_1
.end method

.method public static i(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 132
    invoke-static {p0}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    .line 133
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    const/4 v0, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v0, v3, :cond_0

    .line 135
    rsub-int/lit8 v3, v0, 0x2

    mul-int/lit8 v3, v3, 0x5

    shr-int v3, v1, v3

    and-int/lit8 v3, v3, 0x1f

    .line 136
    add-int/lit8 v3, v3, 0x60

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic i()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, La;->d:Z

    return v0
.end method

.method public static i(Landroid/os/Parcel;I)[Ljava/lang/String;
    .locals 3

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_0
.end method

.method public static synthetic j(Ljava/lang/String;)Lesh;
    .locals 1

    .prologue
    .line 34
    invoke-static {p0}, La;->i(Ljava/lang/String;)Lesh;

    move-result-object v0

    return-object v0
.end method

.method public static j(Ljava/nio/ByteBuffer;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 143
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 144
    invoke-static {v0}, Lye;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static j(Landroid/os/Parcel;I)Ljava/util/ArrayList;
    .locals 3

    invoke-static {p0, p1}, La;->a(Landroid/os/Parcel;I)I

    move-result v1

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v2

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    add-int/2addr v1, v2

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    goto :goto_0
.end method

.method public static synthetic j()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, La;->e:Z

    return v0
.end method

.method public static j(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 129
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 131
    iget v1, v1, Landroid/content/res/Configuration;->touchscreen:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Landroid/os/Parcel;I)I
    .locals 1

    const/high16 v0, -0x10000

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    return v0
.end method

.method public static synthetic k()J
    .locals 2

    .prologue
    .line 44
    sget-wide v0, La;->g:J

    return-wide v0
.end method

.method public static synthetic k(Ljava/lang/String;)Lesx;
    .locals 1

    .prologue
    .line 34
    const-string v0, "breakEnd"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lesx;->b:Lesx;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "error"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lesx;->c:Lesx;

    goto :goto_0

    :cond_1
    const-string v0, "breakStart"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    sget-object v0, Lesx;->a:Lesx;

    goto :goto_0
.end method

.method public static k(Landroid/content/Context;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 38
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, La;->a(Landroid/content/ContentResolver;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static l(Ljava/lang/String;)J
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 21
    if-nez p0, :cond_1

    .line 31
    :cond_0
    return-wide v2

    .line 26
    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 27
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    int-to-long v4, v1

    xor-long/2addr v2, v4

    .line 29
    const/4 v1, 0x1

    shl-long v4, v2, v1

    const/4 v1, 0x4

    shl-long v6, v2, v1

    add-long/2addr v4, v6

    const/4 v1, 0x5

    shl-long v6, v2, v1

    add-long/2addr v4, v6

    const/4 v1, 0x7

    shl-long v6, v2, v1

    add-long/2addr v4, v6

    const/16 v1, 0x8

    shl-long v6, v2, v1

    add-long/2addr v4, v6

    const/16 v1, 0x28

    shl-long v6, v2, v1

    add-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static declared-synchronized l()Lorg/apache/http/client/HttpClient;
    .locals 3

    .prologue
    .line 56
    const-class v1, La;

    monitor-enter v1

    :try_start_0
    sget-object v0, La;->i:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 57
    sget-object v0, La;->l:Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const/16 v0, 0xbb8

    const/16 v2, 0x1388

    invoke-static {v0, v2}, La;->a(II)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    sput-object v0, La;->i:Lorg/apache/http/client/HttpClient;

    .line 60
    :cond_0
    sget-object v0, La;->i:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static l(Landroid/os/Parcel;I)V
    .locals 3

    invoke-virtual {p0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    sub-int v1, v0, p1

    add-int/lit8 v2, p1, -0x4

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    invoke-virtual {p0, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    return-void
.end method

.method public static l(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, La;->a(Landroid/content/ContentResolver;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 56
    invoke-static {p0}, Lfaq;->b(Landroid/content/Context;)Z

    move-result v0

    :goto_0
    return v0

    .line 50
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 52
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static m(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 112
    :cond_0
    return-object p0
.end method

.method public static declared-synchronized m()Lorg/apache/http/client/HttpClient;
    .locals 3

    .prologue
    .line 70
    const-class v1, La;

    monitor-enter v1

    :try_start_0
    sget-object v0, La;->j:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 71
    sget-object v0, La;->l:Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const/16 v0, 0x3a98

    const/16 v2, 0x4e20

    invoke-static {v0, v2}, La;->a(II)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    sput-object v0, La;->j:Lorg/apache/http/client/HttpClient;

    .line 74
    :cond_0
    sget-object v0, La;->j:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static m(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 63
    const-string v0, "DOGFOOD"

    invoke-static {p0, v0}, La;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "DEV"

    invoke-static {p0, v0}, La;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static n(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 43
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 44
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid mime type: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 46
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized n()Lorg/apache/http/client/HttpClient;
    .locals 3

    .prologue
    .line 83
    const-class v1, La;

    monitor-enter v1

    :try_start_0
    sget-object v0, La;->k:Lorg/apache/http/client/HttpClient;

    if-nez v0, :cond_0

    .line 84
    sget-object v0, La;->l:Ljava/lang/String;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    const/16 v0, 0x708

    const/16 v2, 0x708

    invoke-static {v0, v2}, La;->a(II)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    sput-object v0, La;->k:Lorg/apache/http/client/HttpClient;

    .line 87
    :cond_0
    sget-object v0, La;->k:Lorg/apache/http/client/HttpClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static n(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 74
    const-string v0, "DEV"

    invoke-static {p0, v0}, La;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static o(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 96
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, La;->q:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 100
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    .line 100
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_0

    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-nez v1, :cond_2

    .line 104
    :cond_0
    const-string v0, "RELEASE"

    sput-object v0, La;->q:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :cond_1
    :goto_0
    sget-object v0, La;->q:Ljava/lang/String;

    return-object v0

    .line 106
    :cond_2
    :try_start_1
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "com.google.android.apps.youtube.config.BuildType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    if-nez v0, :cond_3

    const-string v0, "RELEASE"

    :cond_3
    sput-object v0, La;->q:Ljava/lang/String;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    const-string v1, "Could not get metadata from application info for build type."

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 113
    const-string v0, "RELEASE"

    sput-object v0, La;->q:Ljava/lang/String;

    goto :goto_0
.end method

.method public static o()V
    .locals 2

    .prologue
    .line 31
    sget v0, Legz;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 32
    invoke-static {}, Landroid/os/Trace;->endSection()V

    .line 34
    :cond_0
    return-void
.end method

.method public static o(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 56
    invoke-static {p0}, La;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static p()Ljava/lang/String;
    .locals 1

    const-string v0, "0ZnY30Jj0poskMNptPzOAUAoLJ3M+w62lTbrjj2yJsw="

    return-object v0
.end method

.method public static p(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 120
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, La;->r:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 122
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.google.android.tv"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, La;->r:Ljava/lang/Boolean;

    .line 124
    :cond_0
    sget-object v0, La;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static p(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 66
    invoke-static {p0}, La;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static q()Ljava/lang/String;
    .locals 1

    const-string v0, "njAQBT4Y/YqRKbRatytX8xL8HhDUWVPVKorn9eIpx4Ybs+ItVmvp3oygk4Ov92fXCE4xuXDEpWxiA2OBtkT68/cBUziU9c4Kvc/xGGI5JU+7CE5eZHShEMv3fy8HPY9M+L+5s/vV3S84LqIHBv0To1WI7K7ksqCNdPuWIxU4bgeu4m0Egzysv2H0K7B26isXszu61imuYglIQ89K0hOEDdYv4267/oqV/nIdevY3hb61ASah18XV4bpX8k45QydKEcni/4RrB+z/mGe2iFBJ2dXzCglT9WWElhbmbajAVSGICStrvBWC3USgQv4Qm58JnsQWlxdxUNPttI4AbSkYGnDcf30FxAWeqS2Xl8mAXRZfJDAxCNpbpN4qeZLYshAokEBGM44NmNz0ttOSZs7y5/QI4dsWCBfG9W7Wvyl2QwEeMfoi++4b5dTx+ktSXwu0XAkp6Dnrn5ImGBPfhDE6WPf5LD93DiPwuskXy0NzJHgyu+oQyOX/1Mf8Z4tq6IvgRf3WrTPOjZlUmQUPeuDIb6jgyI5BbAgexEjSbc0U6S1t3MFH1pN/deUKyunxmXQCgWT01CRqhlNcDMxkQo1dMAhG/OdVtKYm3aZeUb2y0g6PSryC470Ru2H3JfOx3ducY/3bqTJbI4s0qaTAo+uqkqAm5TO76JW+XOwq+eaFjNsyTQt/fJKtwrdFfvMEnPdRYsb41+txvOST7NcxdAMGjMSCnoUQfBfktC06+DJqdZZ2D2zGqt97LXJ5Z5YpQFWkj9zz8BVB+bteFplUNGj4fCMxKIuMnSQgQEX8RrirXOHYf870uAJ2BlyJhe/cVqh3nVlrz982U4aYIdXBaWNjPTEl4Iwrx3GTyIkstojIeaPyLSwG3IPBfa9p2LiPE8+8fwDXkLcogkJFHoKy8Ka8FfCLuXH5vv/e6/wYcJ+T/ZFF60RQDwcIIeZONaLAKA9/gIWRVuv1MPP9KXSfLraP9HAKPnRYlXW3wZ3N01vKyKrA69KfKJpvBxzTbHK4UUVx5opFvn/VYrYt9S2rcXN9ky5h0Y5U+PQTUrXOorC7Z2yiJ290JiiTMRNXftPr74weT8CLrABFnk/sFGMrm6CZ8wB0pGUREFSRYvOzJ+HdRRJifPNdExWqD+uGmkGgCPYQZsbZaQfZIsR7kDumSj9URJO13er/cx0hY9A7tVSvbEY+3Jaoe9Wm6LjmBKpCfzb5e/8agTZpZ7kPbyPxlQTuIeyW/gZVZnNgFaKu8JGo7xcSwup0VgDYcys8MgcRGlQoJTGoGB+mB0ZKQxG7MwzFz5GXY6BiD7hY5zi7EQO6XakbsYfmVmfCh0m/FgIt+6ZkFrYm5qfvAifde8TeXzHMYFquXK/MB7LCmif8LX7Tc9NR145uHYGzoyugVi0zVrml3crCNddADKZdx2TAQbszfzadUEX5OYlMug5QgXRKk9I10RTLCx8pfkN33KJnDSQzoZb4QmvtDQxJhEfUDmteHXme3e5CyfeSTUNOEXT6tQj+7NEdSVZAM2ipF3wj0iHEZtv+ANDGbolSczgI2rF7GTCdAWacUqulUzBOTIc8ROrgViyDphcu/wnxG9+vv4wrWHKjlyviRuRVayUL4cETeRVAGm4jcSfWVeiGklkm0Llr9ZvhLO3I0DOEh5qgyP0FgHeeV8kDlVNT7PxP1yrVZc0QLupG9sn5zyL5Ojt+KEnDwmoRO68S1j6cPaRG/NuT4r1pOjyNmd3QoTNux7Mf9V3ANzZY6vYPRYPZHjpwNjfiuF87ml6f9AnjvPn+s57S4TJO/QF2QFW2iPGOfWHl9qNjRru/VXq5ru9+wkyOJEd2nU9/TFDSwO2G5MPhrdB/pkgEB18EUTC2rpNCx/IVuoObIpt8nQ+hWjklsOnAN/tVkHGDfmUbso4j2MNcZIC7KMnFoeGOg1+OiVCAzQlEwbKnI134swMju7oqyZdk34XyqxTo2MU+0U+tdQB7oIsLsmpfP9APFTjOrqRB9/MBj2H0CxHpUA4ZCLxcgAqu5zulwNR3xsvUHPVrEOg6ANgYhMZEpmuWGJDruLBlkjK7SAjThqRnBIaj+Rp1sZlhN3iLvr+8tJ4ffShpylDUR+HlzobRJ8NsVIXuaAXWEGt7rajHbBOs2XV+cBpofgM2pvqLxYbI6jOJldp7dhRoBW0WK0he7qGwdDpZAX7QjW8PZN5V6xf4WwOr2CFYgunYtDb+orwDwcOlboHpf4xgRz2FwlHuvriz8BlRG+9i3TTzNbQuVtC3v3DvnRBphvkKgP/fO5GwOaaHDe7gUjFxlME/BulcC2gvs4Vnl+ZE4YwbNbNXXmZB7MIcZsP5F9H+hCNEd238CiFv4fBvg/Lt30f/jOBC6mN7SQoBbMbiagU/zAPGD6G8zv9HgItehkWcmGrBw5Gijk2zvFzU9y++TcbJJw+sqPmSILnOO2KKekvXAFHf/cQaSvUEXbE61tSw6sgiml5QEwBi84n2GCDuN+h3rRMQgFQ+2IZsCgT49lBouo9NrQjbGgpmWIu2X0Sux5hOpIBe+C7uG2ol0jwKdfh1NFFg4B8CJAc6JqHGFceDxX5KC/Gh386CgPnde35gWtLwM6FTDlxFuOeOrY6v2rSBw1N4WcVdaJNOUD5MBaw4az6h8AoIAyQOCY5EPT3YyUFTJr6C8uP7/6dqKkOATiuGf6SzKz2JtBn1WVYOvSfl58yEn4s2wTxMyd0kprC/o4sLdmRa4ylWLxVytYhz/AEZCFktUkLTX2prbPvaSTgDYK0Dav9p/cNg8Yy8iI73elp/JA7vDdl+6GWp4U/Ij0Gd+5+hpgZZosSAFozdjeMZYm/TKtuHaJ44ofLgSAmkoUnZHUbi+EnEq8Qmo9v1brNwqzvBE6UJgO2huMf+NoHhljRyIzcvpqqPXp5HC/Crd36nPqhNP1PjQlPfWT1h3wbQYQnnvm9r4L/78lfZhoePA/dWbR7p+c3XlQrqoS+QFPfX8goAweFjuAUdtxhUCN1FdQlQj+7021yhSnpyIdTPuezr1PYb9lYsovAr4h5lGjhqlmMz+k7obZInwBFJrXwKKmipd5hykjAQ8JmrVmxQqwYTA7w5ASDy0k6gkgl3HiDEgx2GLFxTPO7+WOpZXVDanNZHSqVoXP0l0rEwd5fjCS17w+eQuidEKYI828DtZdib2uLUSBmPaG42BsdgAP9sCM2WYu4p8Sd0mCnp8b0Ii1wJQ7/Kqd/dd65Kbluqy7vpoLN9m2eB015I2Wcr2NNGddo7mCQKhZ8JWcXiH7w+0x1J20JNhSZUIIp8/OPvNyZOE4rrKJy4/l5FDV8XopenUzrN3d11CGfLmTuRf1zbDTWh2zKYsW7lFvZ6WKQ0hanTELD1Qd6yt7v6/nwS6YCIOCmMHOImUUCdAsUcAhNvt19qQTe4ulMIQY2MIgC7ZNXgAXLYEw8kmfLoy6OfRrUPHNfkrPUL4iN6dvctelFj7hvKBRiUB5Nut/CjzSRIFhdPNC2GSg/u9VbNS5KR0cTy8hyavxEbuu4J5WapnTGm6L6r2kgVvtoVcrjo+RNeemiTT++pxsZ66ij0jqVk2/0zCtMos/eTJWxW77keEohIjj4OUO3CWwaHcO0cE+6JNVTYQiVg0K364T6LeEV1W7D40R77P84YFZqE4fZzKNYTyNUakJsTfj/IToy0uMRkZaFm9PRb8wCnGCVkF18eufe5AtjfJqCzliTJsp8XMMQnQd2amc0wXo/LFEsBSvWA4z4HKzqEQ9n+7G7LUek7s0ljObeH8xFBhJvB21uBlM1Uvc+qppbGwtVvsgHBh/J/AphnZqZTtweG37n0hEsP2e7LkL12WYqWanqbX5kMfwc0PosDHcBXpj0GJ92utKSutniaWOYCQ0vSxorkPZRgMbnxz7Gz2GPurk6wAzRBhW2sbOlIvf4Qml9wOiGJe8kUy6v5rF6n6TCPMSDGLklS9biGFAUBplsVN8rMN8ru71RJ2WMXqb16NVGxUJOxOFGj6dReHQSvtJ12QYJ9naTOsaykuERNDGyeY0WVVj9qLGo6ZbAhLZGYGpK5BucoHwtrMYoaUsIGPbQyMRZJO/CotgiM8u7kwS371UAcnzMWxejctCLI7XTxA+/33SfyT2BESEDxjejzUFMrv062yHwKWTqB25UpoebKzrsIAiuarWFjwm65QPn7VqehuT9du0OS/2fXYVJ2vExLQq31CBiRR9jxlMYHSY6vW2uEdqu2RHFIhSwX9Hbi7+tDijCTRJOdjSYEcrZ6kmpoewe+UXFSiEgQaFHWU6fEd731VWEAj0zpfEzaDgzrLPU8Gq4FVCRiwLbJuL6qZYjTWoFB3HqtXYGMRZaezLXooYM/1T/AwJwvb/IXpQTCVrnW4w4w54XXj/7DoVBW196xqMMDF9+eAxrvKXOUtCiNwZoosC2bf1sod0KOvSQd/C4lRMSkBdS6AxKkNL+F3EinQQSp4m35Xm/j8ixpagYgZS0SfknBrntOgf7AEPmdkJ5LFg1CumhnO1kskRslb7p2Yr2C4lU4+8jX5CgjkidkVkkmP0aif8HqrgTVTjCzUx6c+vD2CIgVDMN+4vucAdWciWseg/HNu7WI2lwyqdA2yfFYF2ZSto1YeBQ5sh+iBvs1H9lX+NXEz1Sqyr6tnhO4GHEJ1KDbI9g98yuJs91xvp5asyBYhN39k7MXtEKLuQx0NVfLjh8KYWV3DuRJ8+LBpLFioU9wuBSN4jrWVKxKaBF+AZbRQcgKyihCCi1GJZz1F4+D0SbL4cPrbnX/JARHzUWfiRbkDUOX/Qw0OuyS6y7AtbwcDc+4CJWwCqG2IBvdlCYRRnU//AJq77qwTgc5kTCyhSRWuHuwdS+CnAp4ERDECtwirLrcC2yrT8GvzaS4/gWy91g2lCge9xA+rQLHY2+KBQHbu7ZM1oDVm5/6QpQbqKElR99gJIAOdQvwIDO1c+pZV/eBe3xC8W/yvDSIXX4OHvuHzoJ4Wbt/JLVbwjjtjlGOBa7CJ/rkhYURnQ0XqnHshc39T7q5G9Jx7R84DUcIEEoHTjP0yEW3RRkLGni227F1bwCTUsizSd7uPfke1Hq4musYDBpWqsjq7FbDmZo6nRhRNF+LQI7O5Pg3ApZHwcXJ0zTKpZaF6pPOiLb7PFX4YlVTbiy12dJVKIej7wzGrKUzecgQycahmthQGKOsbAAammPdpZMgfVJ2JzJBALmKo/Y70rfwj3Qg6UKfeyW+tMfodNNfjQvpSpPZCmubIthG4259x8+W1nOFR/HtPzQuiycX5khhmUI0KCieFnQjd6FSCrRkY9/8WRr6RMkApwL3oFOfR8dh0i8CFP16HvwQ7Ytx4R7KCBQ8UJ7nuLfORDBPHMOCCA99jpZ6jQjZtiVIdhBc1bIvSESjfXmp7qJo0F3jGEs="

    return-object v0
.end method

.method public static q(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 128
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, La;->s:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 130
    invoke-static {p0}, La;->p(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.type.television"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 130
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, La;->s:Ljava/lang/Boolean;

    .line 133
    :cond_1
    sget-object v0, La;->s:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    .line 131
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static q(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 76
    invoke-static {p0}, La;->n(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static r()Ljava/lang/String;
    .locals 1

    const-string v0, "YrbyZSSP4U9hc8jE2WPmFNgTN1z05H0vH9oE1FybYkm+ba2d6Fvv2ns+bDgjjsxY"

    return-object v0
.end method

.method public static r(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 137
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, La;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 141
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v0, La;->t:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :cond_0
    sget-object v0, La;->t:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    const-string v1, "could not retrieve application version name"

    invoke-static {v1, v0}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 144
    const-string v0, "Unknown"

    goto :goto_0
.end method

.method public static r(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 96
    const-string v0, "application/ttml+xml"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static s(Landroid/content/Context;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 151
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v1, La;->u:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 155
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, La;->u:Ljava/lang/Integer;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    :cond_0
    sget-object v0, La;->u:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    .line 156
    :catch_0
    move-exception v1

    .line 157
    const-string v2, "could not retrieve application version code"

    invoke-static {v2, v1}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static s()Ljava/lang/String;
    .locals 1

    const-string v0, "kRR7rs7/67KmJnHc39cGYo9VSbtX8D+K9iBRzr+ZWHw="

    return-object v0
.end method

.method public static s(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 20
    sget v0, Legz;->a:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 21
    invoke-static {p0}, Landroid/os/Trace;->beginSection(Ljava/lang/String;)V

    .line 23
    :cond_0
    return-void
.end method

.method public static t()Ljava/lang/String;
    .locals 1

    const-string v0, "GNSTGQkASBJjdoPM3qUZH+W2cvDZ7y4NSc+DyQCptSITAZDDS+1XDHr50mVb5dta"

    return-object v0
.end method

.method public static t(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v0, "com.google.android.gms.cast.CATEGORY_CAST"

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "[A-F0-9]+"

    invoke-virtual {v0, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid application ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static t(Landroid/content/Context;)[B
    .locals 1

    .prologue
    .line 169
    invoke-static {p0}, La;->v(Landroid/content/Context;)[B

    move-result-object v0

    invoke-static {v0}, Lfaq;->a([B)[B

    move-result-object v0

    return-object v0
.end method

.method public static u()Ljava/lang/String;
    .locals 1

    const-string v0, "pOBJLvaCxA4swTauHiPD1b5OmPvpwKqFh4DwgWd/rOU="

    return-object v0
.end method

.method public static u(Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x80

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid namespace length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "urn:x-cast:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace must begin with the prefix \"urn:x-cast:\""

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace must begin with the prefix \"urn:x-cast:\" and have non-empty suffix"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    return-void
.end method

.method public static u(Landroid/content/Context;)[B
    .locals 1

    .prologue
    .line 177
    invoke-static {p0}, La;->v(Landroid/content/Context;)[B

    move-result-object v0

    invoke-static {v0}, Lfaq;->b([B)[B

    move-result-object v0

    return-object v0
.end method

.method public static v()Ljava/lang/String;
    .locals 1

    const-string v0, "2OzZqyjDGGeVPCdRn0SZjweOlEmTVEH/2I6FOErTAoqB/rBHpia1X11yzALkJV9Q"

    return-object v0
.end method

.method public static v(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    if-nez v0, :cond_0

    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v3

    if-eqz v3, :cond_1

    const/16 v3, 0x5f

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static v(Landroid/content/Context;)[B
    .locals 3

    .prologue
    .line 243
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 244
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 247
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v1, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 248
    new-instance v1, Lezu;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v0, v0

    invoke-direct {v1, v0}, Lezu;-><init>(I)V

    throw v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :catch_0
    move-exception v0

    .line 254
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t get package information."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 251
    :cond_0
    :try_start_1
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/content/pm/Signature;->toByteArray()[B
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method public static w()Ljava/lang/String;
    .locals 1

    const-string v0, "0iKk4oNP7JyNIYKEnVUDJ0DrlhQRJqtEhvwQRx6qZQc="

    return-object v0
.end method

.method public static w(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-lt v4, v5, :cond_0

    const/16 v5, 0x7e

    if-gt v4, v5, :cond_0

    const/16 v5, 0x22

    if-eq v4, v5, :cond_0

    const/16 v5, 0x27

    if-eq v4, v5, :cond_0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "\\u%04x"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static w(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 117
    const-string v0, "http://www.youtube.com/watch?v="

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 118
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {p0}, La;->x(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x()Ljava/lang/String;
    .locals 1

    const-string v0, "VcUUwPGeTemkhZBWfA0dzhopeQMk8UmWNJUp1plwq5TYklBYIH8FmibbEPRb2/t8"

    return-object v0
.end method

.method public static x(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 402
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lgxp;->a(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.youtube.googletv"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "com.google.android.youtube"

    goto :goto_0
.end method

.method public static x(Ljava/lang/String;)[B
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/4 v1, 0x0

    .line 86
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    .line 87
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 88
    shl-int/lit8 v2, v0, 0x1

    new-array v2, v2, [B

    .line 89
    :goto_1
    if-ge v1, v0, :cond_1

    .line 90
    div-int/lit8 v3, v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v1, 0x1

    .line 91
    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 89
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    :cond_0
    move v0, v1

    .line 86
    goto :goto_0

    .line 93
    :cond_1
    return-object v2
.end method

.method public static y()Ljava/lang/String;
    .locals 1

    const-string v0, "IFcgchRiblF6SvZ04Mf3r4gD0jW7v92DEzsK/lIIQ6w="

    return-object v0
.end method

.method public static y(Ljava/lang/String;)Ljavax/crypto/spec/IvParameterSpec;
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 65
    :try_start_0
    const-string v1, "MD5"

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 70
    :goto_0
    new-instance v1, Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    invoke-direct {v1, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    return-object v1

    .line 66
    :catch_0
    move-exception v1

    .line 67
    const-string v2, "MD5 not recognized as a supported algorithm"

    invoke-static {v2, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static z()Ljava/lang/String;
    .locals 1

    const-string v0, "IpYbUFX/1Lq2SdNyrjP+SHtn/rxlRtA0Jr4BRMouE4VUk9kzzTKYpG0eF3RKbNRf"

    return-object v0
.end method

.method public static z(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, La;->a:Lj;

    iget-object v0, v0, Lj;->K:Landroid/view/View;

    return-object v0
.end method
