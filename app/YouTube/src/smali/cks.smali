.class public final Lcks;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbxn;
.implements Lezl;


# instance fields
.field public final a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

.field public final b:Ljava/util/List;

.field public final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/ui/TabbedView;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iput-object v0, p0, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcks;->b:Ljava/util/List;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcks;->c:Ljava/util/List;

    .line 30
    invoke-virtual {p1, p0}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->a(Lbxn;)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcks;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvh;

    .line 35
    invoke-virtual {v0}, Lfvh;->d()V

    goto :goto_0

    .line 37
    :cond_0
    iget-object v0, p0, Lcks;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 38
    iget-object v0, p0, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b(I)V

    iget v1, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    iput v1, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->k:I

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->l:F

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->c:Landroid/widget/HorizontalScrollView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v1, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->j:Lbxm;

    invoke-virtual {v0}, Lbxm;->c()V

    .line 39
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcks;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfvh;

    invoke-virtual {v0}, Lfvh;->b()V

    .line 90
    return-void
.end method

.method public final b()Lfmz;
    .locals 2

    .prologue
    .line 65
    iget-object v0, p0, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget v0, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->i:I

    .line 66
    iget-object v1, p0, Lcks;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    if-gez v0, :cond_1

    .line 67
    :cond_0
    const/4 v0, 0x0

    .line 69
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcks;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmz;

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lcks;->a()V

    .line 95
    iget-object v0, p0, Lcks;->a:Lcom/google/android/apps/youtube/app/ui/TabbedView;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/ui/TabbedView;->g:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 96
    return-void
.end method
