.class public final enum Lgbn;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgbn;

.field public static final enum b:Lgbn;

.field public static final enum c:Lgbn;

.field public static final enum d:Lgbn;

.field public static final enum e:Lgbn;

.field public static final enum f:Lgbn;

.field public static final enum g:Lgbn;

.field public static final enum h:Lgbn;

.field public static final enum i:Lgbn;

.field public static final enum j:Lgbn;

.field private static final synthetic l:[Lgbn;


# instance fields
.field public final k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 31
    new-instance v0, Lgbn;

    const-string v1, "VIDEO_LIKED"

    const v2, 0x7f0900c4

    invoke-direct {v0, v1, v5, v2, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->a:Lgbn;

    .line 32
    new-instance v0, Lgbn;

    const-string v1, "VIDEO_SHARED"

    const v2, 0x7f0900c5

    invoke-direct {v0, v1, v4, v2, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->b:Lgbn;

    .line 33
    new-instance v0, Lgbn;

    const-string v1, "VIDEO_FAVORITED"

    const v2, 0x7f0900c6

    invoke-direct {v0, v1, v6, v2, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->c:Lgbn;

    .line 34
    new-instance v0, Lgbn;

    const-string v1, "VIDEO_COMMENTED"

    const v2, 0x7f0900c7

    invoke-direct {v0, v1, v7, v2, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->d:Lgbn;

    .line 35
    new-instance v0, Lgbn;

    const-string v1, "VIDEO_UPLOADED"

    const v2, 0x7f0900c8

    invoke-direct {v0, v1, v8, v2, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->e:Lgbn;

    .line 36
    new-instance v0, Lgbn;

    const-string v1, "VIDEO_RECOMMENDED"

    const/4 v2, 0x5

    const v3, 0x7f0900cc

    invoke-direct {v0, v1, v2, v3, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->f:Lgbn;

    .line 37
    new-instance v0, Lgbn;

    const-string v1, "VIDEO_ADDED_TO_PLAYLIST"

    const/4 v2, 0x6

    const v3, 0x7f0900c9

    invoke-direct {v0, v1, v2, v3, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->g:Lgbn;

    .line 38
    new-instance v0, Lgbn;

    const-string v1, "FRIEND_ADDED"

    const/4 v2, 0x7

    const v3, 0x7f0900ca

    invoke-direct {v0, v1, v2, v3, v5}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->h:Lgbn;

    .line 39
    new-instance v0, Lgbn;

    const-string v1, "USER_SUBSCRIPTION_ADDED"

    const/16 v2, 0x8

    const v3, 0x7f0900cb

    invoke-direct {v0, v1, v2, v3, v5}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->i:Lgbn;

    .line 40
    new-instance v0, Lgbn;

    const-string v1, "BULLETIN_POSTED"

    const/16 v2, 0x9

    const v3, 0x7f0900cd

    invoke-direct {v0, v1, v2, v3, v4}, Lgbn;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lgbn;->j:Lgbn;

    .line 30
    const/16 v0, 0xa

    new-array v0, v0, [Lgbn;

    sget-object v1, Lgbn;->a:Lgbn;

    aput-object v1, v0, v5

    sget-object v1, Lgbn;->b:Lgbn;

    aput-object v1, v0, v4

    sget-object v1, Lgbn;->c:Lgbn;

    aput-object v1, v0, v6

    sget-object v1, Lgbn;->d:Lgbn;

    aput-object v1, v0, v7

    sget-object v1, Lgbn;->e:Lgbn;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lgbn;->f:Lgbn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lgbn;->g:Lgbn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lgbn;->h:Lgbn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lgbn;->i:Lgbn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lgbn;->j:Lgbn;

    aput-object v2, v0, v1

    sput-object v0, Lgbn;->l:[Lgbn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput-boolean p4, p0, Lgbn;->k:Z

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgbn;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lgbn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgbn;

    return-object v0
.end method

.method public static values()[Lgbn;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lgbn;->l:[Lgbn;

    invoke-virtual {v0}, [Lgbn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgbn;

    return-object v0
.end method
