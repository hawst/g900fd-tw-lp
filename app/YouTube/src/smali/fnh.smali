.class public Lfnh;
.super Lfmr;
.source "SourceFile"


# instance fields
.field private final e:Lhyx;

.field private f:Ljava/util/List;

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhvp;Lfqh;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lfmr;-><init>(Lhvp;Lfqh;)V

    .line 30
    iget-object v0, p1, Lhvp;->d:Lhvq;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhvp;->d:Lhvq;

    iget-object v0, v0, Lhvq;->b:Lhyx;

    :goto_0
    iput-object v0, p0, Lfnh;->e:Lhyx;

    .line 32
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 36
    iget-object v0, p0, Lfnh;->f:Ljava/util/List;

    if-nez v0, :cond_6

    .line 37
    iget-object v0, p0, Lfnh;->e:Lhyx;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget-object v0, v0, Lhyx;->a:[Lhyz;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfnh;->e:Lhyx;

    iget-object v1, v1, Lhyx;->a:[Lhyz;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfnh;->f:Ljava/util/List;

    .line 39
    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget-object v1, v0, Lhyx;->a:[Lhyz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 40
    iget-object v4, v3, Lhyz;->c:Lhdv;

    if-eqz v4, :cond_1

    .line 41
    iget-object v4, p0, Lfnh;->f:Ljava/util/List;

    new-instance v5, Lfjc;

    iget-object v3, v3, Lhyz;->c:Lhdv;

    invoke-direct {v5, v3, p0}, Lfjc;-><init>(Lhdv;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_1
    iget-object v4, v3, Lhyz;->b:Lheb;

    if-eqz v4, :cond_2

    .line 43
    iget-object v4, p0, Lfnh;->f:Ljava/util/List;

    new-instance v5, Lfjf;

    iget-object v3, v3, Lhyz;->b:Lheb;

    invoke-direct {v5, v3, p0}, Lfjf;-><init>(Lheb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 44
    :cond_2
    iget-object v4, v3, Lhyz;->d:Lhds;

    if-eqz v4, :cond_3

    .line 45
    iget-object v4, p0, Lfnh;->f:Ljava/util/List;

    new-instance v5, Lfja;

    iget-object v3, v3, Lhyz;->d:Lhds;

    invoke-direct {v5, v3, p0}, Lfja;-><init>(Lhds;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 46
    :cond_3
    iget-object v4, v3, Lhyz;->e:Lhdz;

    if-eqz v4, :cond_4

    .line 47
    iget-object v4, p0, Lfnh;->f:Ljava/util/List;

    new-instance v5, Lfje;

    iget-object v3, v3, Lhyz;->e:Lhdz;

    invoke-direct {v5, v3, p0}, Lfje;-><init>(Lhdz;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 48
    :cond_4
    iget-object v4, v3, Lhyz;->f:Lhbb;

    if-eqz v4, :cond_0

    .line 49
    iget-object v4, p0, Lfnh;->f:Ljava/util/List;

    new-instance v5, Lfip;

    iget-object v3, v3, Lhyz;->f:Lhbb;

    invoke-direct {v5, v3, p0}, Lfip;-><init>(Lhbb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 53
    :cond_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lfnh;->f:Ljava/util/List;

    .line 57
    :cond_6
    iget-object v0, p0, Lfnh;->f:Ljava/util/List;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lfnh;->e:Lhyx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget v0, v0, Lhyx;->b:I

    if-nez v0, :cond_1

    .line 64
    :cond_0
    const/4 v0, 0x3

    .line 66
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget v0, v0, Lhyx;->b:I

    goto :goto_0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lfnh;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lfnh;->e:Lhyx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget-object v0, v0, Lhyx;->c:Lhgz;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget-object v0, v0, Lhyx;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnh;->g:Ljava/lang/CharSequence;

    .line 76
    :cond_0
    iget-object v0, p0, Lfnh;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lfnh;->h:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lfnh;->e:Lhyx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget-object v0, v0, Lhyx;->d:Lhgz;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lfnh;->e:Lhyx;

    iget-object v0, v0, Lhyx;->d:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnh;->h:Ljava/lang/CharSequence;

    .line 86
    :cond_0
    iget-object v0, p0, Lfnh;->h:Ljava/lang/CharSequence;

    return-object v0
.end method
