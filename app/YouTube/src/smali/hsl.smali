.class public final Lhsl;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhsn;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:[Lhsm;

.field private e:Z

.field private f:Lhiq;

.field private g:Lhgz;

.field private h:[B


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    sget-object v0, Lhsn;->a:[Lhsn;

    iput-object v0, p0, Lhsl;->a:[Lhsn;

    const-string v0, ""

    iput-object v0, p0, Lhsl;->b:Ljava/lang/String;

    iput-boolean v1, p0, Lhsl;->c:Z

    sget-object v0, Lhsm;->a:[Lhsm;

    iput-object v0, p0, Lhsl;->d:[Lhsm;

    iput-boolean v1, p0, Lhsl;->e:Z

    iput-object v2, p0, Lhsl;->f:Lhiq;

    iput-object v2, p0, Lhsl;->g:Lhgz;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhsl;->h:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lhsl;->a:[Lhsn;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lhsl;->a:[Lhsn;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    if-eqz v5, :cond_0

    const/4 v6, 0x1

    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    :cond_2
    iget-object v2, p0, Lhsl;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x2

    iget-object v3, p0, Lhsl;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-boolean v2, p0, Lhsl;->c:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    iget-boolean v3, p0, Lhsl;->c:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhsl;->d:[Lhsm;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lhsl;->d:[Lhsm;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    if-eqz v4, :cond_5

    const/4 v5, 0x4

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    iget-boolean v1, p0, Lhsl;->e:Z

    if-eqz v1, :cond_7

    const/4 v1, 0x5

    iget-boolean v2, p0, Lhsl;->e:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Lhsl;->f:Lhiq;

    if-eqz v1, :cond_8

    const/4 v1, 0x6

    iget-object v2, p0, Lhsl;->f:Lhiq;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhsl;->g:Lhgz;

    if-eqz v1, :cond_9

    const/4 v1, 0x7

    iget-object v2, p0, Lhsl;->g:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lhsl;->h:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_a

    const/16 v1, 0x9

    iget-object v2, p0, Lhsl;->h:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget-object v1, p0, Lhsl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhsl;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhsl;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhsl;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhsl;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsl;->a:[Lhsn;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhsn;

    iget-object v3, p0, Lhsl;->a:[Lhsn;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhsl;->a:[Lhsn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhsl;->a:[Lhsn;

    :goto_2
    iget-object v2, p0, Lhsl;->a:[Lhsn;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhsl;->a:[Lhsn;

    new-instance v3, Lhsn;

    invoke-direct {v3}, Lhsn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsl;->a:[Lhsn;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhsl;->a:[Lhsn;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhsl;->a:[Lhsn;

    new-instance v3, Lhsn;

    invoke-direct {v3}, Lhsn;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsl;->a:[Lhsn;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhsl;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsl;->c:Z

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhsl;->d:[Lhsm;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhsm;

    iget-object v3, p0, Lhsl;->d:[Lhsm;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lhsl;->d:[Lhsm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lhsl;->d:[Lhsm;

    :goto_4
    iget-object v2, p0, Lhsl;->d:[Lhsm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lhsl;->d:[Lhsm;

    new-instance v3, Lhsm;

    invoke-direct {v3}, Lhsm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsl;->d:[Lhsm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lhsl;->d:[Lhsm;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhsl;->d:[Lhsm;

    new-instance v3, Lhsm;

    invoke-direct {v3}, Lhsm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhsl;->d:[Lhsm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhsl;->e:Z

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lhsl;->f:Lhiq;

    if-nez v0, :cond_8

    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    iput-object v0, p0, Lhsl;->f:Lhiq;

    :cond_8
    iget-object v0, p0, Lhsl;->f:Lhiq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lhsl;->g:Lhgz;

    if-nez v0, :cond_9

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhsl;->g:Lhgz;

    :cond_9
    iget-object v0, p0, Lhsl;->g:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhsl;->h:[B

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhsl;->a:[Lhsn;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lhsl;->a:[Lhsn;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhsl;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x2

    iget-object v2, p0, Lhsl;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v1, p0, Lhsl;->c:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    iget-boolean v2, p0, Lhsl;->c:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    :cond_3
    iget-object v1, p0, Lhsl;->d:[Lhsm;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lhsl;->d:[Lhsm;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    if-eqz v3, :cond_4

    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lhsl;->e:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x5

    iget-boolean v1, p0, Lhsl;->e:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_6
    iget-object v0, p0, Lhsl;->f:Lhiq;

    if-eqz v0, :cond_7

    const/4 v0, 0x6

    iget-object v1, p0, Lhsl;->f:Lhiq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_7
    iget-object v0, p0, Lhsl;->g:Lhgz;

    if-eqz v0, :cond_8

    const/4 v0, 0x7

    iget-object v1, p0, Lhsl;->g:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v0, p0, Lhsl;->h:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0x9

    iget-object v1, p0, Lhsl;->h:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_9
    iget-object v0, p0, Lhsl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhsl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhsl;

    iget-object v2, p0, Lhsl;->a:[Lhsn;

    iget-object v3, p1, Lhsl;->a:[Lhsn;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsl;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhsl;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-boolean v2, p0, Lhsl;->c:Z

    iget-boolean v3, p1, Lhsl;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsl;->d:[Lhsm;

    iget-object v3, p1, Lhsl;->d:[Lhsm;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhsl;->e:Z

    iget-boolean v3, p1, Lhsl;->e:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhsl;->f:Lhiq;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhsl;->f:Lhiq;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhsl;->g:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhsl;->g:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhsl;->h:[B

    iget-object v3, p1, Lhsl;->h:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhsl;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhsl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhsl;->b:Ljava/lang/String;

    iget-object v3, p1, Lhsl;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhsl;->f:Lhiq;

    iget-object v3, p1, Lhsl;->f:Lhiq;

    invoke-virtual {v2, v3}, Lhiq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhsl;->g:Lhgz;

    iget-object v3, p1, Lhsl;->g:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhsl;->I:Ljava/util/List;

    iget-object v3, p1, Lhsl;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    iget-object v2, p0, Lhsl;->a:[Lhsn;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhsl;->b:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhsl;->c:Z

    if-eqz v0, :cond_6

    move v0, v3

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsl;->d:[Lhsm;

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-boolean v2, p0, Lhsl;->e:Z

    if-eqz v2, :cond_9

    :goto_2
    add-int/2addr v0, v3

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsl;->f:Lhiq;

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhsl;->g:Lhgz;

    if-nez v0, :cond_b

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    iget-object v2, p0, Lhsl;->h:[B

    if-nez v2, :cond_c

    mul-int/lit8 v2, v0, 0x1f

    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhsl;->I:Ljava/util/List;

    if-nez v2, :cond_d

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_3
    move v2, v0

    move v0, v1

    :goto_6
    iget-object v5, p0, Lhsl;->a:[Lhsn;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhsl;->a:[Lhsn;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_7
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_4
    iget-object v2, p0, Lhsl;->a:[Lhsn;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhsn;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_5
    iget-object v0, p0, Lhsl;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_6
    move v0, v4

    goto :goto_1

    :cond_7
    move v2, v0

    move v0, v1

    :goto_8
    iget-object v5, p0, Lhsl;->d:[Lhsm;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhsl;->d:[Lhsm;

    aget-object v2, v2, v0

    if-nez v2, :cond_8

    move v2, v1

    :goto_9
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_8
    iget-object v2, p0, Lhsl;->d:[Lhsm;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhsm;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    move v3, v4

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lhsl;->f:Lhiq;

    invoke-virtual {v0}, Lhiq;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lhsl;->g:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_c
    move v2, v0

    move v0, v1

    :goto_a
    iget-object v3, p0, Lhsl;->h:[B

    array-length v3, v3

    if-ge v0, v3, :cond_2

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhsl;->h:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_d
    iget-object v1, p0, Lhsl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
