.class public final Lgzf;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Lhgz;

.field private b:Lhgz;

.field private c:Lhgz;

.field private d:J

.field private e:Lhgz;

.field private f:J

.field private g:Lhog;

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 7072
    invoke-direct {p0}, Lidf;-><init>()V

    .line 7075
    iput-object v0, p0, Lgzf;->a:Lhgz;

    .line 7078
    iput-object v0, p0, Lgzf;->b:Lhgz;

    .line 7081
    iput-object v0, p0, Lgzf;->c:Lhgz;

    .line 7084
    iput-wide v2, p0, Lgzf;->d:J

    .line 7087
    iput-object v0, p0, Lgzf;->e:Lhgz;

    .line 7090
    iput-wide v2, p0, Lgzf;->f:J

    .line 7093
    iput-object v0, p0, Lgzf;->g:Lhog;

    .line 7096
    const/4 v0, 0x0

    iput v0, p0, Lgzf;->h:I

    .line 7072
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 7175
    const/4 v0, 0x0

    .line 7176
    iget-object v1, p0, Lgzf;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 7177
    const/4 v0, 0x1

    iget-object v1, p0, Lgzf;->a:Lhgz;

    .line 7178
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 7180
    :cond_0
    iget-object v1, p0, Lgzf;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 7181
    const/4 v1, 0x2

    iget-object v2, p0, Lgzf;->b:Lhgz;

    .line 7182
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7184
    :cond_1
    iget-object v1, p0, Lgzf;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 7185
    const/4 v1, 0x3

    iget-object v2, p0, Lgzf;->c:Lhgz;

    .line 7186
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7188
    :cond_2
    iget-wide v2, p0, Lgzf;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 7189
    const/4 v1, 0x4

    iget-wide v2, p0, Lgzf;->d:J

    .line 7190
    invoke-static {v1, v2, v3}, Lidd;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7192
    :cond_3
    iget-object v1, p0, Lgzf;->e:Lhgz;

    if-eqz v1, :cond_4

    .line 7193
    const/4 v1, 0x5

    iget-object v2, p0, Lgzf;->e:Lhgz;

    .line 7194
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7196
    :cond_4
    iget-wide v2, p0, Lgzf;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 7197
    const/4 v1, 0x6

    iget-wide v2, p0, Lgzf;->f:J

    .line 7198
    invoke-static {v1, v2, v3}, Lidd;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 7200
    :cond_5
    iget-object v1, p0, Lgzf;->g:Lhog;

    if-eqz v1, :cond_6

    .line 7201
    const/4 v1, 0x7

    iget-object v2, p0, Lgzf;->g:Lhog;

    .line 7202
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7204
    :cond_6
    iget v1, p0, Lgzf;->h:I

    if-eqz v1, :cond_7

    .line 7205
    const/16 v1, 0x8

    iget v2, p0, Lgzf;->h:I

    .line 7206
    invoke-static {v1, v2}, Lidd;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7208
    :cond_7
    iget-object v1, p0, Lgzf;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7209
    iput v0, p0, Lgzf;->J:I

    .line 7210
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 7068
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzf;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzf;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzf;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzf;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzf;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lgzf;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzf;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzf;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lgzf;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgzf;->c:Lhgz;

    if-nez v0, :cond_4

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzf;->c:Lhgz;

    :cond_4
    iget-object v0, p0, Lgzf;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lgzf;->d:J

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lgzf;->e:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzf;->e:Lhgz;

    :cond_5
    iget-object v0, p0, Lgzf;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lgzf;->f:J

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lgzf;->g:Lhog;

    if-nez v0, :cond_6

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lgzf;->g:Lhog;

    :cond_6
    iget-object v0, p0, Lgzf;->g:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->h()I

    move-result v0

    iput v0, p0, Lgzf;->h:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 7145
    iget-object v0, p0, Lgzf;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 7146
    const/4 v0, 0x1

    iget-object v1, p0, Lgzf;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7148
    :cond_0
    iget-object v0, p0, Lgzf;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 7149
    const/4 v0, 0x2

    iget-object v1, p0, Lgzf;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7151
    :cond_1
    iget-object v0, p0, Lgzf;->c:Lhgz;

    if-eqz v0, :cond_2

    .line 7152
    const/4 v0, 0x3

    iget-object v1, p0, Lgzf;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7154
    :cond_2
    iget-wide v0, p0, Lgzf;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 7155
    const/4 v0, 0x4

    iget-wide v2, p0, Lgzf;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 7157
    :cond_3
    iget-object v0, p0, Lgzf;->e:Lhgz;

    if-eqz v0, :cond_4

    .line 7158
    const/4 v0, 0x5

    iget-object v1, p0, Lgzf;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7160
    :cond_4
    iget-wide v0, p0, Lgzf;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 7161
    const/4 v0, 0x6

    iget-wide v2, p0, Lgzf;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 7163
    :cond_5
    iget-object v0, p0, Lgzf;->g:Lhog;

    if-eqz v0, :cond_6

    .line 7164
    const/4 v0, 0x7

    iget-object v1, p0, Lgzf;->g:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7166
    :cond_6
    iget v0, p0, Lgzf;->h:I

    if-eqz v0, :cond_7

    .line 7167
    const/16 v0, 0x8

    iget v1, p0, Lgzf;->h:I

    invoke-virtual {p1, v0, v1}, Lidd;->b(II)V

    .line 7169
    :cond_7
    iget-object v0, p0, Lgzf;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 7171
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7114
    if-ne p1, p0, :cond_1

    .line 7125
    :cond_0
    :goto_0
    return v0

    .line 7115
    :cond_1
    instance-of v2, p1, Lgzf;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 7116
    :cond_2
    check-cast p1, Lgzf;

    .line 7117
    iget-object v2, p0, Lgzf;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzf;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzf;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzf;->b:Lhgz;

    if-nez v2, :cond_3

    .line 7118
    :goto_2
    iget-object v2, p0, Lgzf;->c:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzf;->c:Lhgz;

    if-nez v2, :cond_3

    .line 7119
    :goto_3
    iget-wide v2, p0, Lgzf;->d:J

    iget-wide v4, p1, Lgzf;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lgzf;->e:Lhgz;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgzf;->e:Lhgz;

    if-nez v2, :cond_3

    .line 7121
    :goto_4
    iget-wide v2, p0, Lgzf;->f:J

    iget-wide v4, p1, Lgzf;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lgzf;->g:Lhog;

    if-nez v2, :cond_8

    iget-object v2, p1, Lgzf;->g:Lhog;

    if-nez v2, :cond_3

    .line 7123
    :goto_5
    iget v2, p0, Lgzf;->h:I

    iget v3, p1, Lgzf;->h:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzf;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lgzf;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 7125
    goto :goto_0

    .line 7117
    :cond_4
    iget-object v2, p0, Lgzf;->a:Lhgz;

    iget-object v3, p1, Lgzf;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzf;->b:Lhgz;

    iget-object v3, p1, Lgzf;->b:Lhgz;

    .line 7118
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgzf;->c:Lhgz;

    iget-object v3, p1, Lgzf;->c:Lhgz;

    .line 7119
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgzf;->e:Lhgz;

    iget-object v3, p1, Lgzf;->e:Lhgz;

    .line 7121
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lgzf;->g:Lhog;

    iget-object v3, p1, Lgzf;->g:Lhog;

    .line 7123
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lgzf;->I:Ljava/util/List;

    iget-object v3, p1, Lgzf;->I:Ljava/util/List;

    .line 7125
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 7129
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 7131
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzf;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 7132
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzf;->b:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 7133
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzf;->c:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 7134
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lgzf;->d:J

    iget-wide v4, p0, Lgzf;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 7135
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzf;->e:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 7136
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lgzf;->f:J

    iget-wide v4, p0, Lgzf;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 7137
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzf;->g:Lhog;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 7138
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgzf;->h:I

    add-int/2addr v0, v2

    .line 7139
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzf;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 7140
    return v0

    .line 7131
    :cond_0
    iget-object v0, p0, Lgzf;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 7132
    :cond_1
    iget-object v0, p0, Lgzf;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 7133
    :cond_2
    iget-object v0, p0, Lgzf;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    .line 7135
    :cond_3
    iget-object v0, p0, Lgzf;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_3

    .line 7137
    :cond_4
    iget-object v0, p0, Lgzf;->g:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_4

    .line 7139
    :cond_5
    iget-object v1, p0, Lgzf;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
