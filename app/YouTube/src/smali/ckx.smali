.class final Lckx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:[Ljava/lang/String;

.field private synthetic b:J

.field private synthetic c:Lckv;


# direct methods
.method constructor <init>(Lckv;[Ljava/lang/String;J)V
    .locals 1

    .prologue
    .line 112
    iput-object p1, p0, Lckx;->c:Lckv;

    iput-object p2, p0, Lckx;->a:[Ljava/lang/String;

    iput-wide p3, p0, Lckx;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 115
    const/4 v2, 0x0

    .line 116
    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    iget-object v0, p0, Lckx;->c:Lckv;

    iget-object v0, v0, Lckv;->a:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v1, v0, v3}, Ljava/io/FileWriter;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :try_start_1
    new-instance v2, Ljava/io/BufferedWriter;

    invoke-direct {v2, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 120
    iget-object v3, p0, Lckx;->a:[Ljava/lang/String;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    .line 121
    const-string v6, "MM-dd kk:mm:ss"

    iget-wide v8, p0, Lckx;->b:J

    .line 122
    invoke-static {v6, v8, v9}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 121
    invoke-virtual {v2, v6}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 123
    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 124
    invoke-virtual {v2, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 125
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 126
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_0
    iget-object v0, p0, Lckx;->c:Lckv;

    invoke-static {v0, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    .line 134
    :goto_1
    return-void

    .line 128
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 129
    :goto_2
    :try_start_2
    const-string v2, "Unable to write to file"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 133
    iget-object v0, p0, Lckx;->c:Lckv;

    invoke-static {v0, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    goto :goto_1

    .line 130
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 131
    :goto_3
    :try_start_3
    const-string v2, "Unable to write to file"

    invoke-static {v2, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 133
    iget-object v0, p0, Lckx;->c:Lckv;

    invoke-static {v0, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_4
    iget-object v2, p0, Lckx;->c:Lckv;

    invoke-static {v2, v1}, Lckv;->a(Lckv;Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    .line 130
    :catch_2
    move-exception v0

    goto :goto_3

    .line 128
    :catch_3
    move-exception v0

    goto :goto_2
.end method
