.class public final Lbmk;
.super Lbmf;
.source "SourceFile"


# instance fields
.field private final e:Lgmt;

.field private final f:Lgot;

.field private final g:Ldnb;


# direct methods
.method public constructor <init>(Lgml;Lfet;Lcwg;Lftg;Leuk;Lezj;Lgjm;Ldnb;Ldix;ILgku;Ljava/io/File;Lggn;Lgmt;Lgot;)V
    .locals 13

    .prologue
    .line 60
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    move/from16 v9, p10

    move-object/from16 v10, p11

    move-object/from16 v11, p12

    move-object/from16 v12, p13

    invoke-direct/range {v0 .. v12}, Lbmf;-><init>(Lgml;Lfet;Lcwg;Lftg;Leuk;Lezj;Lgjm;Ldix;ILgku;Ljava/io/File;Lggn;)V

    .line 72
    invoke-static/range {p14 .. p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgmt;

    iput-object v0, p0, Lbmk;->e:Lgmt;

    .line 73
    invoke-static/range {p15 .. p15}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgot;

    iput-object v0, p0, Lbmk;->f:Lgot;

    .line 74
    invoke-static/range {p8 .. p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnb;

    iput-object v0, p0, Lbmk;->g:Ldnb;

    .line 75
    return-void
.end method


# virtual methods
.method protected final a(J)V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lbmk;->g:Ldnb;

    iget-object v1, p0, Lbmk;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Ldnb;->a(Ljava/lang/String;J)V

    .line 97
    return-void
.end method

.method protected final a(JJ)V
    .locals 7

    .prologue
    .line 101
    iget-object v0, p0, Lbmk;->a:Lgml;

    iget-object v1, p0, Lbmk;->c:Ljava/lang/String;

    move-wide v2, p1

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lgml;->a(Ljava/lang/String;JJ)Z

    .line 102
    iget-object v0, p0, Lbmk;->g:Ldnb;

    iget-object v1, p0, Lbmk;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2}, Ldnb;->b(Ljava/lang/String;J)V

    .line 103
    return-void
.end method

.method protected final a(Lfrl;)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lbmk;->d:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    .line 87
    iget-object v2, p0, Lbmk;->a:Lgml;

    iget-object v3, p0, Lbmk;->c:Ljava/lang/String;

    .line 88
    invoke-virtual {v2, v3, p1, v0, v1}, Lgml;->a(Ljava/lang/String;Lfrl;J)Z

    move-result v0

    .line 89
    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lbmg;

    invoke-direct {v0}, Lbmg;-><init>()V

    throw v0

    .line 92
    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/Exception;Lglv;)V
    .locals 5

    .prologue
    .line 119
    if-nez p2, :cond_0

    .line 120
    new-instance v0, Ldmt;

    iget-boolean v1, p3, Lglv;->k:Z

    invoke-direct {v0, p1, v1}, Ldmt;-><init>(Ljava/lang/String;Z)V

    .line 126
    :goto_0
    iget-boolean v1, p3, Lglv;->k:Z

    if-eqz v1, :cond_1

    .line 127
    iget-object v1, p0, Lbmk;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 132
    :goto_1
    iget-object v1, p0, Lbmk;->a:Lgml;

    iget-object v2, p0, Lbmk;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, p3}, Lgml;->a(Ljava/lang/String;Lglv;)Z

    .line 133
    iget-object v1, p0, Lbmk;->g:Ldnb;

    iget-object v2, p0, Lbmk;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 134
    return-void

    .line 122
    :cond_0
    new-instance v0, Ldmt;

    iget-boolean v1, p3, Lglv;->k:Z

    invoke-direct {v0, p1, p2, v1}, Ldmt;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0

    .line 129
    :cond_1
    iget-object v1, p0, Lbmk;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected final a()Z
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lbmk;->a:Lgml;

    iget-object v1, p0, Lbmk;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lgml;->c(Ljava/lang/String;)Lfrl;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b()V
    .locals 13

    .prologue
    .line 108
    :try_start_0
    iget-object v1, p0, Lbmk;->c:Ljava/lang/String;

    invoke-static {}, Leud;->a()Leud;

    move-result-object v2

    iget-object v3, p0, Lbmk;->f:Lgot;

    invoke-interface {v3, v1, v2}, Lgot;->a(Ljava/lang/String;Leuc;)V

    invoke-virtual {v2}, Leud;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    iget-object v2, p0, Lbmk;->a:Lgml;

    iget-object v3, p0, Lbmk;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lgml;->f(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lgpa;

    move-object v9, v0

    invoke-interface {v10, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lbmk;->e:Lgmt;

    iget-object v2, p0, Lbmk;->c:Ljava/lang/String;

    invoke-static {v2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lb;->b()V

    invoke-static {}, Leud;->a()Leud;

    move-result-object v3

    iget-object v4, v1, Lgmt;->c:Lgot;

    invoke-interface {v4, v9, v3}, Lgot;->b(Lgpa;Leuc;)V

    invoke-static {v2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Ljava/io/File;

    invoke-virtual {v1, v2}, Lgmt;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "subtitles"

    invoke-direct {v4, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    iget-object v1, v9, Lgpa;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9}, Lgpa;->hashCode()I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xc

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, "_"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v2}, La;->d(Ljava/io/File;)V

    invoke-virtual {v3}, Leud;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v1, v2}, La;->a([BLjava/io/File;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    iget-object v12, p0, Lbmk;->a:Lgml;

    new-instance v1, Lgpa;

    iget-object v2, v9, Lgpa;->a:Ljava/lang/String;

    iget-object v3, v9, Lgpa;->b:Ljava/lang/String;

    iget-object v4, v9, Lgpa;->c:Ljava/lang/String;

    iget-object v5, v9, Lgpa;->d:Ljava/lang/String;

    iget-object v6, v9, Lgpa;->e:Ljava/lang/String;

    iget v7, v9, Lgpa;->f:I

    iget-object v9, v9, Lgpa;->h:Ljava/lang/String;

    invoke-direct/range {v1 .. v9}, Lgpa;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Lgml;->a(Lgpa;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    move-object v2, v1

    const-string v3, "Failed saving video subtitles "

    iget-object v1, p0, Lbmk;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-static {v1, v2}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 111
    :cond_1
    iget-object v1, p0, Lbmk;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "pudl task["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lezp;->d(Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lbmk;->a:Lgml;

    iget-object v2, p0, Lbmk;->c:Ljava/lang/String;

    sget-object v3, Lglv;->b:Lglv;

    invoke-virtual {v1, v2, v3}, Lgml;->a(Ljava/lang/String;Lglv;)Z

    .line 113
    iget-object v1, p0, Lbmk;->g:Ldnb;

    iget-object v2, p0, Lbmk;->b:Ljava/lang/String;

    new-instance v3, Lgje;

    invoke-direct {v3}, Lgje;-><init>()V

    invoke-interface {v1, v2, v3}, Ldnb;->a(Ljava/lang/String;Lgje;)V

    .line 114
    return-void

    .line 108
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
