.class public final Ldhh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldiy;
.implements Ldna;


# static fields
.field private static a:J


# instance fields
.field private final b:Ldix;

.field private final c:Ldaq;

.field private final d:Lgeh;

.field private final e:Levn;

.field private final f:Lezj;

.field private final g:Ldnb;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Lhsx;

.field private final k:J

.field private l:J

.field private m:J

.field private n:J

.field private o:J

.field private volatile p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 40
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x6

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Ldhh;->a:J

    return-void
.end method

.method public constructor <init>(Ldix;Ldaq;Lgeh;Levn;Lezj;Ldnb;Ljava/lang/String;Ljava/lang/String;Lhsx;J)V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldix;

    iput-object v0, p0, Ldhh;->b:Ldix;

    .line 73
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldaq;

    iput-object v0, p0, Ldhh;->c:Ldaq;

    .line 74
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeh;

    iput-object v0, p0, Ldhh;->d:Lgeh;

    .line 75
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldhh;->e:Levn;

    .line 76
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Ldhh;->f:Lezj;

    .line 77
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldnb;

    iput-object v0, p0, Ldhh;->g:Ldnb;

    .line 79
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldhh;->h:Ljava/lang/String;

    .line 80
    invoke-static {p8}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldhh;->i:Ljava/lang/String;

    .line 81
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsx;

    iput-object v0, p0, Ldhh;->j:Lhsx;

    .line 82
    iput-wide p10, p0, Ldhh;->k:J

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldhh;->p:Z

    .line 85
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldhh;->o:J

    .line 87
    iput-object p0, p1, Ldix;->c:Ldiy;

    .line 88
    return-void
.end method

.method private a()Lfrl;
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 194
    sget-object v6, Ldgv;->a:Ldgv;

    .line 196
    iget-object v0, p0, Ldhh;->j:Lhsx;

    iget-object v0, v0, Lhsx;->b:[B

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldhh;->j:Lhsx;

    iget-object v0, v0, Lhsx;->b:[B

    array-length v0, v0

    if-lez v0, :cond_1

    .line 199
    new-instance v1, Lhro;

    invoke-direct {v1}, Lhro;-><init>()V

    .line 201
    :try_start_0
    iget-object v0, p0, Ldhh;->j:Lhsx;

    iget-object v0, v0, Lhsx;->b:[B

    invoke-static {v1, v0}, Lidh;->a(Lidh;[B)Lidh;

    .line 205
    new-instance v0, Lfrl;

    iget-wide v2, p0, Ldhh;->k:J

    invoke-direct {v0, v1, v2, v3}, Lfrl;-><init>(Lhro;J)V

    .line 207
    iget-object v1, v0, Lfrl;->d:Lfrf;

    iget-object v2, p0, Ldhh;->f:Lezj;

    invoke-virtual {v2}, Lezj;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lfrf;->a(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    sget-object v6, Ldgv;->b:Ldgv;

    .line 210
    iget-object v8, p0, Ldhh;->e:Levn;

    new-instance v1, Ldha;

    .line 212
    iget-object v2, v0, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ldhh;->i:Ljava/lang/String;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Ldhh;->j:Lhsx;

    iget v5, v5, Lhsx;->c:I

    int-to-long v10, v5

    .line 214
    invoke-virtual {v4, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Ldha;-><init>(Ljava/lang/String;Ljava/lang/String;JLdgv;)V

    .line 210
    invoke-virtual {v8, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 253
    :goto_0
    return-object v0

    .line 218
    :cond_0
    sget-object v6, Ldgv;->c:Ldgv;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_1
    :goto_1
    iget-object v0, p0, Ldhh;->j:Lhsx;

    iget-object v0, v0, Lhsx;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 230
    const-string v0, "preloadVideoenderer requires videoId when playerResponse is not present"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    move-object v0, v7

    .line 231
    goto :goto_0

    .line 220
    :catch_0
    move-exception v0

    .line 221
    const-string v1, "Error parsing playerResponse proto from preloadVideoRenderer"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 222
    sget-object v6, Ldgv;->d:Ldgv;

    goto :goto_1

    .line 233
    :cond_2
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldha;

    iget-object v2, p0, Ldhh;->j:Lhsx;

    iget-object v2, v2, Lhsx;->a:Ljava/lang/String;

    iget-object v3, p0, Ldhh;->i:Ljava/lang/String;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v5, p0, Ldhh;->j:Lhsx;

    iget v5, v5, Lhsx;->c:I

    int-to-long v8, v5

    .line 237
    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-direct/range {v1 .. v6}, Ldha;-><init>(Ljava/lang/String;Ljava/lang/String;JLdgv;)V

    .line 233
    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 240
    :try_start_1
    iget-object v0, p0, Ldhh;->c:Ldaq;

    iget-object v1, p0, Ldhh;->j:Lhsx;

    iget-object v1, v1, Lhsx;->a:Ljava/lang/String;

    sget-object v2, Ldaq;->a:[B

    const-string v3, ""

    const-string v4, ""

    const/4 v5, -0x1

    const/4 v6, -0x1

    invoke-virtual/range {v0 .. v6}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lgky;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrl;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 248
    :catch_1
    move-exception v0

    .line 249
    const-string v1, "Error loading playerResponse"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_2
    move-object v0, v7

    .line 253
    goto :goto_0

    .line 250
    :catch_2
    move-exception v0

    .line 251
    const-string v1, "Error loading playerResponse"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 258
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgx;

    invoke-direct {v1}, Ldgx;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 259
    iget-object v0, p0, Ldhh;->b:Ldix;

    invoke-virtual {v0, v2}, Ldix;->a(Z)V

    .line 260
    iput-boolean v2, p0, Ldhh;->p:Z

    .line 261
    return-void
.end method

.method public final a(Lfqj;J)V
    .locals 6

    .prologue
    .line 266
    iput-wide p2, p0, Ldhh;->n:J

    .line 267
    const/4 v0, 0x3

    shl-long v0, p2, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-object v2, p1, Lfqj;->a:Lhgy;

    iget v2, v2, Lhgy;->e:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    .line 269
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, Ldhh;->o:J

    sub-long v4, v0, v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 270
    iget-object v2, p0, Ldhh;->e:Levn;

    new-instance v3, Ldhd;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 272
    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    invoke-direct {v3, p1, v4, v5}, Ldhd;-><init>(Lfqj;J)V

    .line 270
    invoke-virtual {v2, v3}, Levn;->d(Ljava/lang/Object;)V

    .line 273
    iput-wide v0, p0, Ldhh;->o:J

    .line 274
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v1, p0, Ldhh;->h:Ljava/lang/String;

    iget-wide v2, p0, Ldhh;->m:J

    add-long/2addr v2, p2

    invoke-interface {v0, v1, v2, v3}, Ldnb;->b(Ljava/lang/String;J)V

    .line 276
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 92
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 94
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgz;

    invoke-direct {v1}, Ldgz;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Ldhh;->f:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    iget-wide v4, p0, Ldhh;->k:J

    sget-wide v6, Ldhh;->a:J

    add-long/2addr v4, v6

    cmp-long v0, v0, v4

    if-ltz v0, :cond_0

    .line 98
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v1, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v2, Ldmt;

    const-string v3, "Task timeout"

    invoke-direct {v2, v3, v8}, Ldmt;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 100
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgy;

    invoke-direct {v1}, Ldgy;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 191
    :goto_0
    return-void

    .line 105
    :cond_0
    invoke-direct {p0}, Ldhh;->a()Lfrl;

    move-result-object v0

    .line 106
    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v1, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v2, Ldmt;

    const-string v3, "Failed to load playerResponse"

    invoke-direct {v2, v3, v8}, Ldmt;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 109
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgw;

    sget-object v2, Ldgq;->a:Ldgq;

    invoke-direct {v1, v2}, Ldgw;-><init>(Ldgq;)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 112
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgy;

    invoke-direct {v1}, Ldgy;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0

    .line 115
    :cond_1
    iget-object v1, p0, Ldhh;->e:Levn;

    new-instance v3, Ldhb;

    invoke-direct {v3}, Ldhb;-><init>()V

    invoke-virtual {v1, v3}, Levn;->d(Ljava/lang/Object;)V

    .line 118
    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lflo;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 120
    :cond_2
    iget-object v2, p0, Ldhh;->g:Ldnb;

    iget-object v3, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v4, Ldmt;

    if-nez v1, :cond_3

    const-string v0, "null"

    .line 124
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x26

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Received actionable playability error:"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0, v8}, Ldmt;-><init>(Ljava/lang/String;Z)V

    .line 120
    invoke-interface {v2, v3, v4}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 126
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgy;

    invoke-direct {v1}, Ldgy;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto :goto_0

    .line 124
    :cond_3
    iget v0, v1, Lflo;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 131
    :cond_4
    iget-object v1, v0, Lfrl;->d:Lfrf;

    if-nez v1, :cond_5

    .line 132
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v1, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v2, Ldmt;

    const-string v3, "Failed to load streams"

    invoke-direct {v2, v3, v8}, Ldmt;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 134
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgy;

    invoke-direct {v1}, Ldgy;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 138
    :cond_5
    iget-object v1, p0, Ldhh;->d:Lgeh;

    .line 139
    iget-object v3, v0, Lfrl;->d:Lfrf;

    .line 140
    invoke-virtual {v0}, Lfrl;->i()Lfqy;

    move-result-object v0

    .line 138
    invoke-interface {v1, v3, v0, v2}, Lgeh;->a(Lfrf;Lfqy;Z)[Lfqj;

    move-result-object v3

    .line 143
    if-nez v3, :cond_6

    .line 144
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v1, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v2, Ldmt;

    const-string v3, "Failed to select streams"

    invoke-direct {v2, v3, v8}, Ldmt;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1, v2}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    .line 146
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgy;

    invoke-direct {v1}, Ldgy;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 150
    :cond_6
    iput-wide v10, p0, Ldhh;->l:J

    .line 151
    iput-wide v10, p0, Ldhh;->m:J

    .line 152
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Ldhh;->j:Lhsx;

    iget v1, v1, Lhsx;->c:I

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    .line 153
    array-length v0, v3

    if-lez v0, :cond_9

    .line 154
    array-length v1, v3

    move v0, v2

    :goto_2
    if-ge v0, v1, :cond_8

    aget-object v6, v3, v0

    .line 155
    iget-wide v8, v6, Lfqj;->c:J

    cmp-long v7, v4, v8

    if-lez v7, :cond_7

    .line 156
    iget-wide v8, p0, Ldhh;->l:J

    iget-object v6, v6, Lfqj;->a:Lhgy;

    iget-wide v6, v6, Lhgy;->k:J

    add-long/2addr v6, v8

    iput-wide v6, p0, Ldhh;->l:J

    .line 154
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 158
    :cond_7
    iget-wide v8, p0, Ldhh;->l:J

    iget-object v6, v6, Lfqj;->a:Lhgy;

    iget v6, v6, Lhgy;->e:I

    int-to-long v6, v6

    mul-long/2addr v6, v4

    const-wide/16 v10, 0x8

    div-long/2addr v6, v10

    add-long/2addr v6, v8

    iput-wide v6, p0, Ldhh;->l:J

    goto :goto_3

    .line 161
    :cond_8
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v1, p0, Ldhh;->h:Ljava/lang/String;

    iget-wide v6, p0, Ldhh;->l:J

    invoke-interface {v0, v1, v6, v7}, Ldnb;->a(Ljava/lang/String;J)V

    .line 162
    array-length v6, v3

    move v1, v2

    :goto_4
    if-ge v1, v6, :cond_9

    aget-object v0, v3, v1

    .line 163
    iget-boolean v7, p0, Ldhh;->p:Z

    if-nez v7, :cond_9

    .line 164
    iget-object v7, p0, Ldhh;->e:Levn;

    new-instance v8, Ldhe;

    invoke-direct {v8, v0}, Ldhe;-><init>(Lfqj;)V

    invoke-virtual {v7, v8}, Levn;->d(Ljava/lang/Object;)V

    .line 168
    const-wide/16 v8, 0x0

    :try_start_0
    iput-wide v8, p0, Ldhh;->o:J

    .line 169
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Ldhh;->n:J

    .line 170
    iget-object v7, p0, Ldhh;->b:Ldix;

    .line 171
    iget-wide v8, v0, Lfqj;->c:J

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 170
    invoke-virtual {v7, v0, v8, v9}, Ldix;->a(Lfqj;J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 183
    :goto_5
    iget-wide v8, p0, Ldhh;->m:J

    iget-wide v10, p0, Ldhh;->n:J

    add-long/2addr v8, v10

    iput-wide v8, p0, Ldhh;->m:J

    .line 162
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 172
    :catch_0
    move-exception v0

    .line 173
    const-string v7, "Failed to preload stream"

    invoke-static {v7, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 174
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v7, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v8, Ldmt;

    const-string v9, "Failed to preload stream"

    invoke-direct {v8, v9, v2}, Ldmt;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v7, v8}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    goto :goto_5

    .line 177
    :catch_1
    move-exception v0

    .line 178
    const-string v7, "Failed to preload stream"

    invoke-static {v7, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 179
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v7, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v8, Ldmt;

    const-string v9, "Failed to preload stream"

    invoke-direct {v8, v9, v2}, Ldmt;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v7, v8}, Ldnb;->a(Ljava/lang/String;Ldmt;)V

    goto :goto_5

    .line 186
    :cond_9
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldhc;

    iget-wide v2, p0, Ldhh;->m:J

    invoke-direct {v1, v2, v3}, Ldhc;-><init>(J)V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 189
    iget-object v0, p0, Ldhh;->e:Levn;

    new-instance v1, Ldgy;

    invoke-direct {v1}, Ldgy;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 190
    iget-object v0, p0, Ldhh;->g:Ldnb;

    iget-object v1, p0, Ldhh;->h:Ljava/lang/String;

    new-instance v2, Lgje;

    invoke-direct {v2}, Lgje;-><init>()V

    invoke-interface {v0, v1, v2}, Ldnb;->a(Ljava/lang/String;Lgje;)V

    goto/16 :goto_0
.end method
