.class public final Ldrw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field public final b:Lrz;

.field c:Ldrx;

.field private final d:Ldsl;

.field private final e:Lezj;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/Set;

.field private h:Ljava/util/HashMap;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ldsl;Lezj;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Ldrw;->d:Ldsl;

    .line 61
    iput-object p2, p0, Ldrw;->e:Lezj;

    .line 62
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Ldrw;->f:Ljava/lang/String;

    .line 63
    iput-boolean p4, p0, Ldrw;->a:Z

    .line 65
    new-instance v0, Lsa;

    invoke-direct {v0}, Lsa;-><init>()V

    const-string v1, "MDX_MEDIA_ROUTE_CONTROL_CATEGORY"

    invoke-virtual {v0, v1}, Lsa;->a(Ljava/lang/String;)Lsa;

    const-string v1, "android.media.intent.category.LIVE_AUDIO"

    invoke-virtual {v0, v1}, Lsa;->a(Ljava/lang/String;)Lsa;

    iget-object v1, p0, Ldrw;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldrw;->f:Ljava/lang/String;

    invoke-static {v1}, La;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsa;->a(Ljava/lang/String;)Lsa;

    :cond_0
    invoke-virtual {v0}, Lsa;->a()Lrz;

    move-result-object v0

    iput-object v0, p0, Ldrw;->b:Lrz;

    .line 67
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldrw;->g:Ljava/util/Set;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldrw;->h:Ljava/util/HashMap;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Ldrw;->i:Ljava/lang/String;

    .line 70
    return-void
.end method

.method private b()Ljava/util/Collection;
    .locals 10

    .prologue
    .line 200
    iget-object v2, p0, Ldrw;->g:Ljava/util/Set;

    monitor-enter v2

    .line 201
    :try_start_0
    iget-object v0, p0, Ldrw;->e:Lezj;

    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v4

    .line 202
    iget-object v0, p0, Ldrw;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 203
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 205
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 206
    iget-object v1, p0, Ldrw;->h:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v6, v4, v6

    const-wide/32 v8, 0xea60

    cmp-long v1, v6, v8

    if-lez v1, :cond_0

    .line 207
    iget-object v1, p0, Ldrw;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 208
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 211
    :cond_1
    :try_start_1
    iget-object v0, p0, Ldrw;->g:Ljava/util/Set;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method


# virtual methods
.method public final a(Ldwf;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/16 v9, 0x10

    const/4 v3, 0x0

    .line 94
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ldwf;->h()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 105
    :goto_0
    return-object v0

    .line 97
    :cond_1
    iget-object v0, p1, Ldwf;->a:Ldtm;

    iget-object v4, v0, Ldtm;->e:Ldtj;

    .line 98
    iget-object v5, p0, Ldrw;->g:Ljava/util/Set;

    monitor-enter v5

    .line 99
    :try_start_0
    invoke-direct {p0}, Ldrw;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 100
    if-eqz v0, :cond_3

    if-nez v4, :cond_4

    :cond_3
    move v2, v3

    :goto_1
    if-eqz v2, :cond_2

    .line 101
    monitor-exit v5

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 100
    :cond_4
    :try_start_1
    invoke-virtual {v4}, Ldtj;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v7, "-"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    const-string v7, "uuid:"

    const-string v8, ""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v9, :cond_6

    const-string v7, "-"

    const-string v8, ""

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v8, v9, :cond_6

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {v7, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    const/4 v2, 0x1

    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_1

    .line 104
    :cond_7
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 105
    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 172
    const-string v0, "unselect Cast route"

    iget-boolean v0, p0, Ldrw;->a:Z

    .line 173
    iget-object v0, p0, Ldrw;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Ldrw;->d:Ldsl;

    invoke-virtual {v0}, Ldsl;->a()V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Ldrw;->i:Ljava/lang/String;

    .line 177
    :cond_0
    return-void
.end method

.method public final a(Lsk;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v0, p0, Ldrw;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 90
    :goto_0
    return v0

    .line 83
    :cond_0
    invoke-virtual {p1}, Lsk;->e()Ljava/util/List;

    move-result-object v0

    .line 84
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    .line 85
    iget-object v3, p0, Ldrw;->f:Ljava/lang/String;

    .line 86
    invoke-static {v3}, La;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 85
    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 90
    goto :goto_0
.end method

.method public final b(Lsk;)V
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Ldrw;->a(Lsk;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    const-string v0, "add available CastV2 route: "

    invoke-virtual {p1}, Lsk;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    iget-boolean v0, p0, Ldrw;->a:Z

    .line 117
    iget-object v1, p0, Ldrw;->g:Ljava/util/Set;

    monitor-enter v1

    .line 118
    :try_start_0
    invoke-virtual {p1}, Lsk;->a()Ljava/lang/String;

    move-result-object v0

    .line 119
    iget-object v2, p0, Ldrw;->g:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 120
    iget-object v2, p0, Ldrw;->h:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    iget-object v0, p0, Ldrw;->c:Ldrx;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Ldrw;->c:Ldrx;

    invoke-interface {v0}, Ldrx;->a()V

    goto :goto_0

    .line 116
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 121
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final c(Lsk;)V
    .locals 6

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Ldrw;->a(Lsk;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    const-string v0, "remove available CastV2 route: "

    invoke-virtual {p1}, Lsk;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    iget-boolean v0, p0, Ldrw;->a:Z

    .line 132
    iget-object v1, p0, Ldrw;->g:Ljava/util/Set;

    monitor-enter v1

    .line 133
    :try_start_0
    invoke-virtual {p1}, Lsk;->a()Ljava/lang/String;

    move-result-object v0

    .line 136
    iget-object v2, p0, Ldrw;->g:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    iget-object v2, p0, Ldrw;->h:Ljava/util/HashMap;

    iget-object v3, p0, Ldrw;->e:Lezj;

    invoke-virtual {v3}, Lezj;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    iget-object v0, p0, Ldrw;->c:Ldrx;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Ldrw;->c:Ldrx;

    invoke-interface {v0}, Ldrx;->a()V

    goto :goto_0

    .line 131
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 139
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final d(Lsk;)Z
    .locals 4

    .prologue
    .line 151
    const-string v0, "change route: deselect previous if any"

    iget-boolean v0, p0, Ldrw;->a:Z

    .line 152
    invoke-virtual {p0}, Ldrw;->a()V

    .line 154
    invoke-virtual {p0, p1}, Ldrw;->a(Lsk;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 155
    const-string v0, "change route to Cast route "

    invoke-virtual {p1}, Lsk;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_0
    iget-boolean v0, p0, Ldrw;->a:Z

    .line 156
    invoke-virtual {p1}, Lsk;->f()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/CastDevice;->a(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_3

    .line 158
    const-string v1, "Select device "

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_1
    iget-boolean v1, p0, Ldrw;->a:Z

    .line 159
    new-instance v1, Ldwb;

    invoke-direct {v1, v0}, Ldwb;-><init>(Lcom/google/android/gms/cast/CastDevice;)V

    .line 160
    iget-object v0, p0, Ldrw;->d:Ldsl;

    invoke-virtual {v0, v1}, Ldsl;->a(Ldwr;)V

    .line 161
    invoke-virtual {p1}, Lsk;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldrw;->i:Ljava/lang/String;

    .line 162
    const/4 v0, 0x1

    .line 168
    :goto_2
    return v0

    .line 155
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 165
    :cond_2
    const-string v1, "change route to non-Cast route "

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lsk;->b()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_4
    iget-boolean v0, p0, Ldrw;->a:Z

    .line 168
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 165
    :cond_4
    const-string v0, "null"

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method
