.class public final enum Lbrg;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lbrg;

.field public static final enum b:Lbrg;

.field public static final enum c:Lbrg;

.field private static final synthetic g:[Lbrg;


# instance fields
.field final d:I

.field final e:I

.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lbrg;

    const-string v1, "LIKE"

    const v3, 0x7f09029d

    const v4, 0x7f090298

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lbrg;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lbrg;->a:Lbrg;

    .line 13
    new-instance v3, Lbrg;

    const-string v4, "DISLIKE"

    const v6, 0x7f09029f

    const v7, 0x7f090299

    move v5, v9

    move v8, v9

    invoke-direct/range {v3 .. v8}, Lbrg;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lbrg;->b:Lbrg;

    .line 14
    new-instance v3, Lbrg;

    const-string v4, "REMOVE_LIKE"

    const v6, 0x7f09029e

    const v7, 0x7f09029a

    move v5, v10

    move v8, v10

    invoke-direct/range {v3 .. v8}, Lbrg;-><init>(Ljava/lang/String;IIII)V

    sput-object v3, Lbrg;->c:Lbrg;

    .line 11
    const/4 v0, 0x3

    new-array v0, v0, [Lbrg;

    sget-object v1, Lbrg;->a:Lbrg;

    aput-object v1, v0, v2

    sget-object v1, Lbrg;->b:Lbrg;

    aput-object v1, v0, v9

    sget-object v1, Lbrg;->c:Lbrg;

    aput-object v1, v0, v10

    sput-object v0, Lbrg;->g:[Lbrg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lbrg;->d:I

    .line 26
    iput p4, p0, Lbrg;->e:I

    .line 27
    iput p5, p0, Lbrg;->f:I

    .line 28
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbrg;
    .locals 1

    .prologue
    .line 11
    const-class v0, Lbrg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbrg;

    return-object v0
.end method

.method public static values()[Lbrg;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lbrg;->g:[Lbrg;

    invoke-virtual {v0}, [Lbrg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbrg;

    return-object v0
.end method
