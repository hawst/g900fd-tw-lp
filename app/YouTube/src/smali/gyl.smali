.class public final Lgyl;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgyl;


# instance fields
.field public b:I

.field public c:Lhis;

.field public d:Ljava/lang/String;

.field private e:Libl;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3099
    const/4 v0, 0x0

    new-array v0, v0, [Lgyl;

    sput-object v0, Lgyl;->a:[Lgyl;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 3100
    invoke-direct {p0}, Lidf;-><init>()V

    .line 3109
    iput v1, p0, Lgyl;->b:I

    .line 3112
    iput-object v0, p0, Lgyl;->c:Lhis;

    .line 3115
    iput-object v0, p0, Lgyl;->e:Libl;

    .line 3118
    const-string v0, ""

    iput-object v0, p0, Lgyl;->d:Ljava/lang/String;

    .line 3121
    iput v1, p0, Lgyl;->f:I

    .line 3100
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 3182
    const/4 v0, 0x0

    .line 3183
    iget v1, p0, Lgyl;->b:I

    if-eqz v1, :cond_0

    .line 3184
    const/4 v0, 0x1

    iget v1, p0, Lgyl;->b:I

    .line 3185
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3187
    :cond_0
    iget-object v1, p0, Lgyl;->c:Lhis;

    if-eqz v1, :cond_1

    .line 3188
    const/4 v1, 0x2

    iget-object v2, p0, Lgyl;->c:Lhis;

    .line 3189
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3191
    :cond_1
    iget-object v1, p0, Lgyl;->e:Libl;

    if-eqz v1, :cond_2

    .line 3192
    const/4 v1, 0x3

    iget-object v2, p0, Lgyl;->e:Libl;

    .line 3193
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3195
    :cond_2
    iget-object v1, p0, Lgyl;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3196
    const/4 v1, 0x4

    iget-object v2, p0, Lgyl;->d:Ljava/lang/String;

    .line 3197
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3199
    :cond_3
    iget v1, p0, Lgyl;->f:I

    if-eqz v1, :cond_4

    .line 3200
    const/4 v1, 0x5

    iget v2, p0, Lgyl;->f:I

    .line 3201
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3203
    :cond_4
    iget-object v1, p0, Lgyl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3204
    iput v0, p0, Lgyl;->J:I

    .line 3205
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 3096
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyl;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyl;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyl;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lgyl;->b:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lgyl;->b:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgyl;->c:Lhis;

    if-nez v0, :cond_4

    new-instance v0, Lhis;

    invoke-direct {v0}, Lhis;-><init>()V

    iput-object v0, p0, Lgyl;->c:Lhis;

    :cond_4
    iget-object v0, p0, Lgyl;->c:Lhis;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lgyl;->e:Libl;

    if-nez v0, :cond_5

    new-instance v0, Libl;

    invoke-direct {v0}, Libl;-><init>()V

    iput-object v0, p0, Lgyl;->e:Libl;

    :cond_5
    iget-object v0, p0, Lgyl;->e:Libl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgyl;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lgyl;->f:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 3161
    iget v0, p0, Lgyl;->b:I

    if-eqz v0, :cond_0

    .line 3162
    const/4 v0, 0x1

    iget v1, p0, Lgyl;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 3164
    :cond_0
    iget-object v0, p0, Lgyl;->c:Lhis;

    if-eqz v0, :cond_1

    .line 3165
    const/4 v0, 0x2

    iget-object v1, p0, Lgyl;->c:Lhis;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3167
    :cond_1
    iget-object v0, p0, Lgyl;->e:Libl;

    if-eqz v0, :cond_2

    .line 3168
    const/4 v0, 0x3

    iget-object v1, p0, Lgyl;->e:Libl;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 3170
    :cond_2
    iget-object v0, p0, Lgyl;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3171
    const/4 v0, 0x4

    iget-object v1, p0, Lgyl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 3173
    :cond_3
    iget v0, p0, Lgyl;->f:I

    if-eqz v0, :cond_4

    .line 3174
    const/4 v0, 0x5

    iget v1, p0, Lgyl;->f:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 3176
    :cond_4
    iget-object v0, p0, Lgyl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 3178
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3136
    if-ne p1, p0, :cond_1

    .line 3144
    :cond_0
    :goto_0
    return v0

    .line 3137
    :cond_1
    instance-of v2, p1, Lgyl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 3138
    :cond_2
    check-cast p1, Lgyl;

    .line 3139
    iget v2, p0, Lgyl;->b:I

    iget v3, p1, Lgyl;->b:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyl;->c:Lhis;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyl;->c:Lhis;

    if-nez v2, :cond_3

    .line 3140
    :goto_1
    iget-object v2, p0, Lgyl;->e:Libl;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyl;->e:Libl;

    if-nez v2, :cond_3

    .line 3141
    :goto_2
    iget-object v2, p0, Lgyl;->d:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgyl;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3142
    :goto_3
    iget v2, p0, Lgyl;->f:I

    iget v3, p1, Lgyl;->f:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgyl;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lgyl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 3144
    goto :goto_0

    .line 3139
    :cond_4
    iget-object v2, p0, Lgyl;->c:Lhis;

    iget-object v3, p1, Lgyl;->c:Lhis;

    .line 3140
    invoke-virtual {v2, v3}, Lhis;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyl;->e:Libl;

    iget-object v3, p1, Lgyl;->e:Libl;

    .line 3141
    invoke-virtual {v2, v3}, Libl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgyl;->d:Ljava/lang/String;

    iget-object v3, p1, Lgyl;->d:Ljava/lang/String;

    .line 3142
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lgyl;->I:Ljava/util/List;

    iget-object v3, p1, Lgyl;->I:Ljava/util/List;

    .line 3144
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3148
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3150
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyl;->b:I

    add-int/2addr v0, v2

    .line 3151
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyl;->c:Lhis;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 3152
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyl;->e:Libl;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3153
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyl;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3154
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lgyl;->f:I

    add-int/2addr v0, v2

    .line 3155
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyl;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 3156
    return v0

    .line 3151
    :cond_0
    iget-object v0, p0, Lgyl;->c:Lhis;

    invoke-virtual {v0}, Lhis;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3152
    :cond_1
    iget-object v0, p0, Lgyl;->e:Libl;

    invoke-virtual {v0}, Libl;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3153
    :cond_2
    iget-object v0, p0, Lgyl;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3155
    :cond_3
    iget-object v1, p0, Lgyl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
