.class public Ldbx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lddz;
.implements Lggl;


# instance fields
.field final a:Lddy;

.field final b:Landroid/content/SharedPreferences;

.field final c:Levn;

.field final d:Lefj;

.field final e:Landroid/util/DisplayMetrics;

.field f:Lezb;

.field g:Lezb;

.field h:Z

.field i:J


# direct methods
.method public constructor <init>(Lddy;Landroid/content/SharedPreferences;Levn;Lefj;Landroid/util/DisplayMetrics;Ldbv;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddy;

    iput-object v0, p0, Ldbx;->a:Lddy;

    .line 46
    iget-object v0, p0, Ldbx;->a:Lddy;

    invoke-interface {v0, p0}, Lddy;->a(Lddz;)V

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Ldbx;->b:Landroid/content/SharedPreferences;

    .line 48
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldbx;->c:Levn;

    .line 49
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefj;

    iput-object v0, p0, Ldbx;->d:Lefj;

    .line 50
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/DisplayMetrics;

    iput-object v0, p0, Ldbx;->e:Landroid/util/DisplayMetrics;

    .line 51
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldbv;

    iput-object p0, v0, Ldbv;->e:Ldbx;

    .line 52
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 117
    invoke-virtual {p0}, Ldbx;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldbx;->h:Z

    if-nez v0, :cond_0

    .line 118
    invoke-virtual {p0}, Ldbx;->c()V

    .line 120
    :cond_0
    return-void
.end method

.method public a(IJJ)V
    .locals 0

    .prologue
    .line 110
    iput-wide p4, p0, Ldbx;->i:J

    .line 111
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Ldbx;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "medialib_diagnostics_enabled"

    .line 138
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "medialib_diagnostic_cycling_format_evaluator_enabled"

    .line 139
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 140
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 141
    return-void
.end method

.method b()Z
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Ldbx;->b:Landroid/content/SharedPreferences;

    const-string v1, "nerd_stats_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method c()V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Ldbx;->f:Lezb;

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Ldbx;->e:Landroid/util/DisplayMetrics;

    const/16 v1, 0x64

    invoke-static {v0, v1}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v0

    .line 61
    iget-object v1, p0, Ldbx;->e:Landroid/util/DisplayMetrics;

    const/16 v2, 0x11

    invoke-static {v1, v2}, La;->a(Landroid/util/DisplayMetrics;I)I

    move-result v1

    .line 62
    new-instance v2, Lezb;

    invoke-direct {v2, v0, v1}, Lezb;-><init>(II)V

    iput-object v2, p0, Ldbx;->f:Lezb;

    .line 63
    new-instance v2, Lezb;

    invoke-direct {v2, v0, v1}, Lezb;-><init>(II)V

    iput-object v2, p0, Ldbx;->g:Lezb;

    .line 65
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldbx;->h:Z

    .line 66
    iget-object v0, p0, Ldbx;->a:Lddy;

    invoke-interface {v0}, Lddy;->c()V

    .line 67
    iget-object v0, p0, Ldbx;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 68
    iget-object v0, p0, Ldbx;->d:Lefj;

    invoke-virtual {v0, p0}, Lefj;->a(Lggl;)V

    .line 69
    return-void
.end method

.method d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    iput-boolean v1, p0, Ldbx;->h:Z

    .line 73
    iget-object v0, p0, Ldbx;->a:Lddy;

    invoke-interface {v0}, Lddy;->d()V

    .line 74
    iget-object v0, p0, Ldbx;->c:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Ldbx;->d:Lefj;

    invoke-virtual {v0, p0}, Lefj;->b(Lggl;)V

    .line 76
    invoke-virtual {p0, v1}, Ldbx;->a(Z)V

    .line 77
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 126
    invoke-virtual {p0}, Ldbx;->d()V

    .line 127
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Ldbx;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "nerd_stats_enabled"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 132
    invoke-virtual {p0}, Ldbx;->d()V

    .line 133
    return-void
.end method

.method public onFormatStreamChange(Lgdq;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Ldbx;->a:Lddy;

    iget-object v1, p1, Lgdq;->a:Lfqj;

    invoke-interface {v0, v1}, Lddy;->a(Lfqj;)V

    .line 102
    iget-object v0, p0, Ldbx;->a:Lddy;

    iget-object v1, p1, Lgdq;->b:Lfqj;

    invoke-interface {v0, v1}, Lddy;->b(Lfqj;)V

    .line 103
    return-void
.end method

.method public onVideoStage(Ldac;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 81
    iget-object v1, p0, Ldbx;->a:Lddy;

    iget-object v0, p1, Ldac;->d:Lfoy;

    if-nez v0, :cond_0

    iget-object v0, p1, Ldac;->c:Ljava/lang/String;

    :goto_0
    invoke-interface {v1, v0}, Lddy;->a(Ljava/lang/String;)V

    .line 82
    return-void

    .line 81
    :cond_0
    iget-object v0, p1, Ldac;->d:Lfoy;

    iget-object v0, v0, Lfoy;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public onVideoTime(Ldad;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    const-wide/16 v0, 0x0

    .line 87
    iget-wide v2, p1, Ldad;->c:J

    .line 88
    iget-wide v4, p1, Ldad;->a:J

    .line 89
    cmp-long v6, v4, v0

    if-ltz v6, :cond_0

    cmp-long v6, v2, v0

    if-ltz v6, :cond_0

    sub-long v0, v2, v4

    .line 91
    :cond_0
    iget-object v2, p0, Ldbx;->g:Lezb;

    long-to-float v3, v0

    invoke-virtual {v2, v3}, Lezb;->a(F)V

    .line 92
    iget-object v2, p0, Ldbx;->a:Lddy;

    iget-object v3, p0, Ldbx;->g:Lezb;

    invoke-virtual {v3}, Lezb;->a()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1}, Lddy;->a(Landroid/graphics/Bitmap;J)V

    .line 95
    iget-object v0, p0, Ldbx;->f:Lezb;

    iget-wide v2, p0, Ldbx;->i:J

    long-to-float v1, v2

    invoke-virtual {v0, v1}, Lezb;->a(F)V

    .line 96
    iget-object v0, p0, Ldbx;->a:Lddy;

    iget-object v1, p0, Ldbx;->f:Lezb;

    invoke-virtual {v1}, Lezb;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-wide v2, p0, Ldbx;->i:J

    long-to-float v2, v2

    invoke-interface {v0, v1, v2}, Lddy;->a(Landroid/graphics/Bitmap;F)V

    .line 97
    return-void
.end method
