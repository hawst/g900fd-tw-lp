.class public abstract Lank;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lanj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    invoke-virtual {p0, p0, v0}, Lank;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lanj;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v0, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    instance-of v1, v0, Lanj;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lanj;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lanl;

    invoke-direct {v0, p0}, Lanl;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 15

    .prologue
    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 83
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 45
    :sswitch_0
    const-string v1, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    const/4 v1, 0x1

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v1, "com.google.android.apps.youtube.api.service.jar.IApiPlayerFactoryService"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    .line 54
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v3, 0x0

    .line 56
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    if-nez v5, :cond_4

    const/4 v4, 0x0

    .line 58
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    if-nez v6, :cond_6

    const/4 v5, 0x0

    .line 60
    :goto_4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    if-nez v7, :cond_8

    const/4 v6, 0x0

    .line 62
    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v8

    if-nez v8, :cond_a

    const/4 v7, 0x0

    .line 64
    :goto_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    if-nez v9, :cond_c

    const/4 v8, 0x0

    .line 66
    :goto_7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    if-nez v10, :cond_e

    const/4 v9, 0x0

    .line 68
    :goto_8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v11

    if-nez v11, :cond_10

    const/4 v10, 0x0

    .line 70
    :goto_9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v12

    if-nez v12, :cond_12

    const/4 v11, 0x0

    .line 72
    :goto_a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v13

    if-nez v13, :cond_14

    const/4 v12, 0x0

    .line 74
    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v14

    if-nez v14, :cond_16

    const/4 v13, 0x0

    .line 76
    :goto_c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_18

    const/4 v14, 0x1

    :goto_d
    move-object v1, p0

    .line 77
    invoke-virtual/range {v1 .. v14}, Lank;->a(Laiw;Lajf;Lajl;Lajo;Lait;Lain;Lajr;Laiq;Laiz;Lajc;Laji;Laju;Z)Lanm;

    move-result-object v1

    .line 78
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 79
    if-eqz v1, :cond_19

    invoke-interface {v1}, Lanm;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    :goto_e
    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 80
    const/4 v1, 0x1

    goto :goto_0

    .line 52
    :cond_0
    const-string v1, "com.google.android.apps.youtube.api.jar.client.IApiPlayerClient"

    invoke-interface {v3, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_1

    instance-of v2, v1, Laiw;

    if-eqz v2, :cond_1

    check-cast v1, Laiw;

    move-object v2, v1

    goto :goto_1

    :cond_1
    new-instance v2, Laiy;

    invoke-direct {v2, v3}, Laiy;-><init>(Landroid/os/IBinder;)V

    goto :goto_1

    .line 54
    :cond_2
    const-string v1, "com.google.android.apps.youtube.api.jar.client.IPlayerUiClient"

    invoke-interface {v4, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_3

    instance-of v3, v1, Lajf;

    if-eqz v3, :cond_3

    check-cast v1, Lajf;

    move-object v3, v1

    goto/16 :goto_2

    :cond_3
    new-instance v3, Lajh;

    invoke-direct {v3, v4}, Lajh;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_2

    .line 56
    :cond_4
    const-string v1, "com.google.android.apps.youtube.api.jar.client.ISurfaceHolderClient"

    invoke-interface {v5, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_5

    instance-of v4, v1, Lajl;

    if-eqz v4, :cond_5

    check-cast v1, Lajl;

    move-object v4, v1

    goto/16 :goto_3

    :cond_5
    new-instance v4, Lajn;

    invoke-direct {v4, v5}, Lajn;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_3

    .line 58
    :cond_6
    const-string v1, "com.google.android.apps.youtube.api.jar.client.ISurfaceTextureClient"

    invoke-interface {v6, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_7

    instance-of v5, v1, Lajo;

    if-eqz v5, :cond_7

    check-cast v1, Lajo;

    move-object v5, v1

    goto/16 :goto_4

    :cond_7
    new-instance v5, Lajq;

    invoke-direct {v5, v6}, Lajq;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_4

    .line 60
    :cond_8
    const-string v1, "com.google.android.apps.youtube.api.jar.client.IApiMediaViewClient"

    invoke-interface {v7, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_9

    instance-of v6, v1, Lait;

    if-eqz v6, :cond_9

    check-cast v1, Lait;

    move-object v6, v1

    goto/16 :goto_5

    :cond_9
    new-instance v6, Laiv;

    invoke-direct {v6, v7}, Laiv;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_5

    .line 62
    :cond_a
    const-string v1, "com.google.android.apps.youtube.api.jar.client.IAdOverlayClient"

    invoke-interface {v8, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_b

    instance-of v7, v1, Lain;

    if-eqz v7, :cond_b

    check-cast v1, Lain;

    move-object v7, v1

    goto/16 :goto_6

    :cond_b
    new-instance v7, Laip;

    invoke-direct {v7, v8}, Laip;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_6

    .line 64
    :cond_c
    const-string v1, "com.google.android.apps.youtube.api.jar.client.ISurveyOverlayClient"

    invoke-interface {v9, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_d

    instance-of v8, v1, Lajr;

    if-eqz v8, :cond_d

    check-cast v1, Lajr;

    move-object v8, v1

    goto/16 :goto_7

    :cond_d
    new-instance v8, Lajt;

    invoke-direct {v8, v9}, Lajt;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_7

    .line 66
    :cond_e
    const-string v1, "com.google.android.apps.youtube.api.jar.client.IAnnotationOverlayClient"

    invoke-interface {v10, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_f

    instance-of v9, v1, Laiq;

    if-eqz v9, :cond_f

    check-cast v1, Laiq;

    move-object v9, v1

    goto/16 :goto_8

    :cond_f
    new-instance v9, Lais;

    invoke-direct {v9, v10}, Lais;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_8

    .line 68
    :cond_10
    const-string v1, "com.google.android.apps.youtube.api.jar.client.IControlsOverlayClient"

    invoke-interface {v11, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_11

    instance-of v10, v1, Laiz;

    if-eqz v10, :cond_11

    check-cast v1, Laiz;

    move-object v10, v1

    goto/16 :goto_9

    :cond_11
    new-instance v10, Lajb;

    invoke-direct {v10, v11}, Lajb;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_9

    .line 70
    :cond_12
    const-string v1, "com.google.android.apps.youtube.api.jar.client.ILiveOverlayClient"

    invoke-interface {v12, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_13

    instance-of v11, v1, Lajc;

    if-eqz v11, :cond_13

    check-cast v1, Lajc;

    move-object v11, v1

    goto/16 :goto_a

    :cond_13
    new-instance v11, Laje;

    invoke-direct {v11, v12}, Laje;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_a

    .line 72
    :cond_14
    const-string v1, "com.google.android.apps.youtube.api.jar.client.ISubtitlesOverlayClient"

    invoke-interface {v13, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_15

    instance-of v12, v1, Laji;

    if-eqz v12, :cond_15

    check-cast v1, Laji;

    move-object v12, v1

    goto/16 :goto_b

    :cond_15
    new-instance v12, Lajk;

    invoke-direct {v12, v13}, Lajk;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_b

    .line 74
    :cond_16
    const-string v1, "com.google.android.apps.youtube.api.jar.client.IThumbnailOverlayClient"

    invoke-interface {v14, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v1

    if-eqz v1, :cond_17

    instance-of v13, v1, Laju;

    if-eqz v13, :cond_17

    check-cast v1, Laju;

    move-object v13, v1

    goto/16 :goto_c

    :cond_17
    new-instance v13, Lajw;

    invoke-direct {v13, v14}, Lajw;-><init>(Landroid/os/IBinder;)V

    goto/16 :goto_c

    .line 76
    :cond_18
    const/4 v14, 0x0

    goto/16 :goto_d

    .line 79
    :cond_19
    const/4 v1, 0x0

    goto/16 :goto_e

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
