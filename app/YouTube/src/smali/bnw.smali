.class public final Lbnw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Leyp;

.field final c:Lfhz;

.field d:Lfiw;

.field e:Lfiz;

.field f:Landroid/app/AlertDialog;

.field g:Landroid/view/View;

.field h:Lfvi;

.field i:Landroid/widget/TextView;

.field j:Landroid/widget/TextView;

.field k:Landroid/widget/TextView;

.field l:Landroid/widget/TextView;

.field m:Landroid/widget/TextView;

.field private final n:Landroid/view/View;

.field private final o:Landroid/view/View;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Leyp;Lfhz;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbnw;->a:Landroid/app/Activity;

    .line 55
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbnw;->b:Leyp;

    .line 56
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lbnw;->c:Lfhz;

    .line 57
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lbnw;->n:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lbnw;->n:Landroid/view/View;

    const v1, 0x7f08011c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnw;->p:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lbnw;->n:Landroid/view/View;

    const v1, 0x7f080119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnw;->q:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lbnw;->n:Landroid/view/View;

    const v1, 0x7f08011a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnw;->r:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lbnw;->n:Landroid/view/View;

    const v1, 0x7f08011b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbnw;->o:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lbnw;->o:Landroid/view/View;

    new-instance v1, Lbnx;

    invoke-direct {v1, p0}, Lbnx;-><init>(Lbnw;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void
.end method


# virtual methods
.method public final a(Lfiw;)V
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x0

    .line 74
    iput-object p1, p0, Lbnw;->d:Lfiw;

    .line 75
    if-nez p1, :cond_0

    .line 76
    iget-object v0, p0, Lbnw;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 90
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v2, p0, Lbnw;->n:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v2, p0, Lbnw;->p:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 82
    iget-object v2, p0, Lbnw;->p:Landroid/widget/TextView;

    iget-object v3, p1, Lfiw;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_1

    iget-object v3, p1, Lfiw;->a:Lhcd;

    iget-object v3, v3, Lhcd;->a:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, p1, Lfiw;->b:Ljava/lang/CharSequence;

    :cond_1
    iget-object v3, p1, Lfiw;->b:Ljava/lang/CharSequence;

    invoke-static {v2, v3}, Leze;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 85
    :cond_2
    iget-object v2, p1, Lfiw;->d:Lfiz;

    if-nez v2, :cond_3

    iget-object v2, p1, Lfiw;->a:Lhcd;

    iget-object v2, v2, Lhcd;->b:Lhcf;

    if-eqz v2, :cond_3

    iget-object v2, p1, Lfiw;->a:Lhcd;

    iget-object v2, v2, Lhcd;->b:Lhcf;

    iget-object v2, v2, Lhcf;->a:Lhch;

    if-eqz v2, :cond_3

    new-instance v2, Lfiz;

    iget-object v3, p1, Lfiw;->a:Lhcd;

    iget-object v3, v3, Lhcd;->b:Lhcf;

    iget-object v3, v3, Lhcf;->a:Lhch;

    invoke-direct {v2, v3}, Lfiz;-><init>(Lhch;)V

    iput-object v2, p1, Lfiw;->d:Lfiz;

    :cond_3
    iget-object v2, p1, Lfiw;->d:Lfiz;

    .line 86
    iget-object v3, p0, Lbnw;->q:Landroid/widget/TextView;

    invoke-virtual {v2}, Lfiz;->b()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v3, p0, Lbnw;->r:Landroid/widget/TextView;

    invoke-virtual {v2}, Lfiz;->c()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v2, p0, Lbnw;->o:Landroid/view/View;

    .line 89
    invoke-virtual {p1}, Lfiw;->a()Lfiz;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 88
    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 89
    goto :goto_1
.end method
