.class public final Ldcw;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lddx;
.implements Ldec;
.implements Ldfm;


# instance fields
.field private a:Z

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldcw;->b:Z

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Ldcw;->c:I

    .line 28
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Ldcw;->setBackgroundColor(I)V

    .line 29
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcw;->setVisibility(I)V

    .line 30
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Ldcw;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldcw;->a:Z

    .line 73
    invoke-virtual {p0}, Ldcw;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldcw;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Ldcw;->setVisibility(I)V

    .line 74
    return-void

    .line 73
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 62
    if-nez p1, :cond_0

    .line 63
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldcw;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    :goto_0
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Ldcw;->a:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0, v0}, Ldcw;->setVisibility(I)V

    .line 68
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0, p1}, Ldcw;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x4

    goto :goto_1
.end method

.method public final a_(Z)V
    .locals 1

    .prologue
    .line 40
    iput-boolean p1, p0, Ldcw;->b:Z

    .line 41
    if-eqz p1, :cond_0

    iget v0, p0, Ldcw;->c:I

    :goto_0
    invoke-super {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 42
    return-void

    .line 41
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldcw;->a:Z

    .line 79
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Ldcw;->setVisibility(I)V

    .line 80
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldcw;->a(Landroid/graphics/Bitmap;)V

    .line 58
    return-void
.end method

.method public final c_(Z)V
    .locals 0

    .prologue
    .line 83
    return-void
.end method

.method public final f_()Landroid/view/View;
    .locals 0

    .prologue
    .line 35
    return-object p0
.end method

.method public final r_()Ldeg;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 52
    new-instance v0, Ldeg;

    invoke-direct {v0, v1, v1}, Ldeg;-><init>(II)V

    return-object v0
.end method

.method public final setVisibility(I)V
    .locals 1

    .prologue
    .line 46
    iput p1, p0, Ldcw;->c:I

    .line 47
    iget-boolean v0, p0, Ldcw;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, Ldcw;->c:I

    :goto_0
    invoke-super {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 48
    return-void

    .line 47
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
