.class public final Lflo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lhqn;

.field public final b:I

.field public final c:Z

.field private d:Lflh;

.field private e:Lfij;


# direct methods
.method public constructor <init>(Lhqn;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhqn;

    iput-object v0, p0, Lflo;->a:Lhqn;

    .line 30
    iget v0, p1, Lhqn;->a:I

    iput v0, p0, Lflo;->b:I

    .line 32
    iget-object v0, p1, Lhqn;->e:Lhbh;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhqn;->e:Lhbh;

    iget-object v0, v0, Lhbh;->a:Lhbf;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p1, Lhqn;->e:Lhbh;

    iget-object v0, v0, Lhbh;->a:Lhbf;

    iget-boolean v0, v0, Lhbf;->a:Z

    iput-boolean v0, p0, Lflo;->c:Z

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflo;->c:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lflo;->b:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 64
    iget v0, p0, Lflo;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lflo;->b:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lflo;->b:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lflo;->b:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 80
    iget v0, p0, Lflo;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lflo;->b:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lfij;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lflo;->e:Lfij;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->c:Lhqm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->c:Lhqm;

    iget-object v0, v0, Lhqm;->a:Lhrg;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->c:Lhqm;

    iget-object v0, v0, Lhqm;->a:Lhrg;

    .line 96
    iget-object v1, v0, Lhrg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lhrg;->a:Ljava/lang/String;

    .line 97
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lhrg;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lhrg;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 100
    new-instance v1, Lfij;

    invoke-direct {v1, v0}, Lfij;-><init>(Lhrg;)V

    iput-object v1, p0, Lflo;->e:Lfij;

    .line 104
    :cond_0
    iget-object v0, p0, Lflo;->e:Lfij;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 155
    if-ne p0, p1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 159
    goto :goto_0

    .line 161
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 162
    goto :goto_0

    .line 165
    :cond_3
    check-cast p1, Lflo;

    .line 167
    iget-object v2, p0, Lflo;->a:Lhqn;

    if-nez v2, :cond_4

    .line 168
    iget-object v2, p1, Lflo;->a:Lhqn;

    if-eqz v2, :cond_4

    move v0, v1

    .line 169
    goto :goto_0

    .line 172
    :cond_4
    iget v2, p0, Lflo;->b:I

    iget v3, p1, Lflo;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 173
    goto :goto_0
.end method

.method public final f()Lflh;
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lflo;->d:Lflh;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->d:Lhpq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->d:Lhpq;

    iget-object v0, v0, Lhpq;->a:Lhpn;

    if-eqz v0, :cond_0

    .line 128
    new-instance v0, Lflh;

    iget-object v1, p0, Lflo;->a:Lhqn;

    iget-object v1, v1, Lhqn;->d:Lhpq;

    iget-object v1, v1, Lhpq;->a:Lhpn;

    invoke-direct {v0, v1}, Lflh;-><init>(Lhpn;)V

    iput-object v0, p0, Lflo;->d:Lflh;

    .line 131
    :cond_0
    iget-object v0, p0, Lflo;->d:Lflh;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lflo;->b:I

    add-int/lit8 v0, v0, 0x1f

    .line 150
    return v0
.end method
