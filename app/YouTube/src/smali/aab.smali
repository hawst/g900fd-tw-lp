.class public Laab;
.super Likv;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lzx;

.field public c:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 156
    const-string v0, "trun"

    invoke-direct {p0, v0}, Likv;-><init>(Ljava/lang/String;)V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laab;->c:Ljava/util/List;

    .line 157
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Laab;->c(Ljava/nio/ByteBuffer;)J

    .line 218
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v2

    .line 220
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 221
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    invoke-static {v0, v1}, La;->d(J)I

    move-result v0

    iput v0, p0, Laab;->a:I

    .line 225
    :goto_0
    iget v0, p0, Likv;->p:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 226
    new-instance v0, Lzx;

    invoke-direct {v0, p1}, Lzx;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v0, p0, Laab;->b:Lzx;

    .line 229
    :cond_0
    const/4 v0, 0x0

    :goto_1
    int-to-long v4, v0

    cmp-long v1, v4, v2

    if-gez v1, :cond_6

    .line 230
    new-instance v1, Laac;

    invoke-direct {v1}, Laac;-><init>()V

    .line 231
    iget v4, p0, Likv;->p:I

    and-int/lit16 v4, v4, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_1

    .line 232
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    iput-wide v4, v1, Laac;->a:J

    .line 234
    :cond_1
    iget v4, p0, Likv;->p:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_2

    .line 235
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    iput-wide v4, v1, Laac;->b:J

    .line 237
    :cond_2
    iget v4, p0, Likv;->p:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_3

    .line 238
    new-instance v4, Lzx;

    invoke-direct {v4, p1}, Lzx;-><init>(Ljava/nio/ByteBuffer;)V

    iput-object v4, v1, Laac;->c:Lzx;

    .line 240
    :cond_3
    iget v4, p0, Likv;->p:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_4

    .line 241
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    iput v4, v1, Laac;->d:I

    .line 243
    :cond_4
    iget-object v4, p0, Laab;->c:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 223
    :cond_5
    const/4 v0, -0x1

    iput v0, p0, Laab;->a:I

    goto :goto_0

    .line 246
    :cond_6
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Laab;->d(Ljava/nio/ByteBuffer;)V

    .line 189
    iget-object v0, p0, Laab;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 190
    iget v1, p0, Likv;->p:I

    .line 192
    and-int/lit8 v0, v1, 0x1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 193
    iget v0, p0, Laab;->a:I

    int-to-long v2, v0

    invoke-static {p1, v2, v3}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 195
    :cond_0
    and-int/lit8 v0, v1, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    .line 196
    iget-object v0, p0, Laab;->b:Lzx;

    invoke-virtual {v0, p1}, Lzx;->a(Ljava/nio/ByteBuffer;)V

    .line 199
    :cond_1
    iget-object v0, p0, Laab;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laac;

    .line 200
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_3

    .line 201
    iget-wide v4, v0, Laac;->a:J

    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 203
    :cond_3
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_4

    .line 204
    iget-wide v4, v0, Laac;->b:J

    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 206
    :cond_4
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_5

    .line 207
    iget-object v3, v0, Laac;->c:Lzx;

    invoke-virtual {v3, p1}, Lzx;->a(Ljava/nio/ByteBuffer;)V

    .line 209
    :cond_5
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_2

    .line 210
    iget v0, v0, Laac;->d:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 213
    :cond_6
    return-void
.end method

.method protected final d_()J
    .locals 9

    .prologue
    const-wide/16 v2, 0x4

    .line 160
    const-wide/16 v0, 0x8

    .line 161
    iget v6, p0, Likv;->p:I

    .line 163
    and-int/lit8 v4, v6, 0x1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 164
    const-wide/16 v0, 0xc

    .line 166
    :cond_0
    and-int/lit8 v4, v6, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 167
    add-long/2addr v0, v2

    move-wide v4, v0

    .line 170
    :goto_0
    const-wide/16 v0, 0x0

    .line 171
    and-int/lit16 v7, v6, 0x100

    const/16 v8, 0x100

    if-ne v7, v8, :cond_1

    move-wide v0, v2

    .line 174
    :cond_1
    and-int/lit16 v7, v6, 0x200

    const/16 v8, 0x200

    if-ne v7, v8, :cond_2

    .line 175
    add-long/2addr v0, v2

    .line 177
    :cond_2
    and-int/lit16 v7, v6, 0x400

    const/16 v8, 0x400

    if-ne v7, v8, :cond_3

    .line 178
    add-long/2addr v0, v2

    .line 180
    :cond_3
    and-int/lit16 v6, v6, 0x800

    const/16 v7, 0x800

    if-ne v6, v7, :cond_4

    .line 181
    add-long/2addr v0, v2

    .line 183
    :cond_4
    iget-object v2, p0, Laab;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    add-long/2addr v0, v4

    .line 184
    return-wide v0

    :cond_5
    move-wide v4, v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 262
    iget v0, p0, Likv;->p:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 266
    iget v0, p0, Likv;->p:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 270
    iget v0, p0, Likv;->p:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 274
    iget v0, p0, Likv;->p:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 338
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 339
    const-string v2, "TrackRunBox"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    const-string v2, "{sampleCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Laab;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 341
    const-string v2, ", dataOffset="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Laab;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 342
    const-string v2, ", dataOffsetPresent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Likv;->p:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 343
    const-string v0, ", sampleSizePresent="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Laab;->f()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 344
    const-string v0, ", sampleDurationPresent="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Laab;->g()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 345
    const-string v0, ", sampleFlagsPresentPresent="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Laab;->h()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 346
    const-string v0, ", sampleCompositionTimeOffsetPresent="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Laab;->i()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 347
    const-string v0, ", firstSampleFlags="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Laab;->b:Lzx;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 348
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 349
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 342
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
