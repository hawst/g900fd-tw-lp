.class public Lbbb;
.super Lbhz;
.source "SourceFile"


# instance fields
.field private final e:Landroid/content/Context;

.field private f:Lcob;

.field private g:Landroid/widget/CheckBox;

.field private h:Landroid/widget/CheckBox;

.field private i:Landroid/widget/CheckBox;

.field private j:Landroid/widget/EditText;

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lbhz;-><init>()V

    .line 28
    iput-object p0, p0, Lbbb;->e:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lbbb;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbbb;->g:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic b(Lbbb;)Lcob;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbbb;->f:Lcob;

    return-object v0
.end method

.method static synthetic c(Lbbb;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lbbb;->f()V

    return-void
.end method

.method static synthetic d(Lbbb;)Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lbbb;->k:Z

    return v0
.end method

.method static synthetic e(Lbbb;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbbb;->h:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic f(Lbbb;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbbb;->i:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lbbb;->h:Landroid/widget/CheckBox;

    iget-object v1, p0, Lbbb;->g:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 109
    iget-object v0, p0, Lbbb;->i:Landroid/widget/CheckBox;

    iget-object v1, p0, Lbbb;->g:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 110
    iget-object v1, p0, Lbbb;->j:Landroid/widget/EditText;

    iget-object v0, p0, Lbbb;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbbb;->i:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 111
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lbbb;)V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Landroid/widget/EditText;

    iget-object v1, p0, Lbbb;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lbbb;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lbbb;->f:Lcob;

    invoke-virtual {v2}, Lcob;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "OK"

    new-instance v3, Lbbh;

    invoke-direct {v3, p0, v0}, Lbbh;-><init>(Lbbb;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v2, "Cancel"

    new-instance v3, Lbbg;

    invoke-direct {v3, p0}, Lbbg;-><init>(Lbbb;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic h(Lbbb;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbbb;->j:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Lbhz;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {p0}, La;->n(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lbbb;->finish()V

    .line 45
    :cond_0
    const v0, 0x7f040055

    invoke-virtual {p0, v0}, Lbbb;->setContentView(I)V

    .line 47
    invoke-virtual {p0}, Lbbb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 48
    new-instance v1, Lcob;

    iget-object v0, v0, Lckz;->a:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {v1, v0}, Lcob;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v1, p0, Lbbb;->f:Lcob;

    .line 49
    const v0, 0x7f080183

    invoke-virtual {p0, v0}, Lbbb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbbb;->g:Landroid/widget/CheckBox;

    .line 50
    const v0, 0x7f080184

    invoke-virtual {p0, v0}, Lbbb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbbb;->h:Landroid/widget/CheckBox;

    .line 51
    const v0, 0x7f080185

    invoke-virtual {p0, v0}, Lbbb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lbbb;->i:Landroid/widget/CheckBox;

    .line 52
    const v0, 0x7f080186

    invoke-virtual {p0, v0}, Lbbb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lbbb;->j:Landroid/widget/EditText;

    .line 53
    iget-object v0, p0, Lbbb;->f:Lcob;

    invoke-virtual {v0}, Lcob;->a()Z

    move-result v0

    iput-boolean v0, p0, Lbbb;->k:Z

    .line 55
    iget-object v0, p0, Lbbb;->g:Landroid/widget/CheckBox;

    new-instance v1, Lbbc;

    invoke-direct {v1, p0}, Lbbc;-><init>(Lbbb;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iget-object v0, p0, Lbbb;->h:Landroid/widget/CheckBox;

    new-instance v1, Lbbd;

    invoke-direct {v1, p0}, Lbbd;-><init>(Lbbb;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-object v0, p0, Lbbb;->i:Landroid/widget/CheckBox;

    new-instance v1, Lbbe;

    invoke-direct {v1, p0}, Lbbe;-><init>(Lbbb;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lbbb;->j:Landroid/widget/EditText;

    new-instance v1, Lbbf;

    invoke-direct {v1, p0}, Lbbf;-><init>(Lbbb;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Lbhz;->onResume()V

    .line 99
    iget-object v0, p0, Lbbb;->g:Landroid/widget/CheckBox;

    iget-object v1, p0, Lbbb;->f:Lcob;

    invoke-virtual {v1}, Lcob;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 100
    iget-object v0, p0, Lbbb;->h:Landroid/widget/CheckBox;

    iget-object v1, p0, Lbbb;->f:Lcob;

    invoke-virtual {v1}, Lcob;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 101
    iget-object v0, p0, Lbbb;->i:Landroid/widget/CheckBox;

    iget-object v1, p0, Lbbb;->f:Lcob;

    invoke-virtual {v1}, Lcob;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 102
    iget-object v0, p0, Lbbb;->j:Landroid/widget/EditText;

    iget-object v1, p0, Lbbb;->f:Lcob;

    invoke-virtual {v1}, Lcob;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 104
    invoke-direct {p0}, Lbbb;->f()V

    .line 105
    return-void
.end method
