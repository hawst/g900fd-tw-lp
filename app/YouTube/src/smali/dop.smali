.class public final Ldop;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ldov;

.field final c:Letc;

.field final d:Ldqz;

.field final e:Ljava/lang/String;

.field final f:Levn;

.field public final g:Lezs;

.field final h:Lezs;

.field private final i:Lezs;

.field private final j:Lezs;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldov;Letc;)V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ldoq;

    invoke-direct {v0, p0}, Ldoq;-><init>(Ldop;)V

    iput-object v0, p0, Ldop;->g:Lezs;

    .line 77
    new-instance v0, Ldor;

    invoke-direct {v0, p0}, Ldor;-><init>(Ldop;)V

    iput-object v0, p0, Ldop;->i:Lezs;

    .line 92
    new-instance v0, Ldos;

    invoke-direct {v0, p0}, Ldos;-><init>(Ldop;)V

    iput-object v0, p0, Ldop;->j:Lezs;

    .line 118
    new-instance v0, Ldot;

    invoke-direct {v0, p0}, Ldot;-><init>(Ldop;)V

    iput-object v0, p0, Ldop;->h:Lezs;

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Ldop;->a:Landroid/content/Context;

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldov;

    iput-object v0, p0, Ldop;->b:Ldov;

    .line 42
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Letc;

    iput-object v0, p0, Ldop;->c:Letc;

    .line 44
    sget-object v0, Ldqz;->a:Ldqz;

    iput-object v0, p0, Ldop;->d:Ldqz;

    .line 46
    invoke-virtual {p3}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "exported_remote_id"

    invoke-static {v0, v1}, Ldov;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldop;->e:Ljava/lang/String;

    .line 50
    new-instance v0, Levn;

    .line 51
    invoke-virtual {p3}, Letc;->q()Ljava/util/concurrent/Executor;

    move-result-object v1

    .line 52
    invoke-virtual {p3}, Letc;->f()Lezj;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Levn;-><init>(Ljava/util/concurrent/Executor;Lezj;)V

    iput-object v0, p0, Ldop;->f:Levn;

    .line 53
    return-void
.end method

.method static synthetic a(Ldop;)Ldwq;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Ldop;->j:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    return-object v0
.end method


# virtual methods
.method public final a()Lduy;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Ldop;->i:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lduy;

    return-object v0
.end method
