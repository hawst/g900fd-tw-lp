.class public Lfrf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Ljava/util/List;

.field public final c:Ljava/util/List;

.field public final d:Lhwn;

.field public final e:Ljava/lang/String;

.field public final f:J

.field public final g:Lfqj;

.field public final h:Z

.field public final i:J

.field public final j:Z

.field private final k:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 418
    new-instance v0, Lfrh;

    invoke-direct {v0}, Lfrh;-><init>()V

    sput-object v0, Lfrf;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lhwn;Ljava/lang/String;JJZ)V
    .locals 11

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lfrf;->d:Lhwn;

    .line 93
    iput-object p2, p0, Lfrf;->e:Ljava/lang/String;

    .line 94
    iput-wide p3, p0, Lfrf;->f:J

    .line 95
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lfrf;->k:J

    .line 96
    move/from16 v0, p7

    iput-boolean v0, p0, Lfrf;->h:Z

    .line 98
    iget-wide v2, p1, Lhwn;->a:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 99
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p1, Lhwn;->a:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 100
    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    add-long v2, v2, p5

    iput-wide v2, p0, Lfrf;->i:J

    .line 105
    :goto_0
    iget-object v2, p1, Lhwn;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 106
    iget-object v2, p1, Lhwn;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 107
    invoke-static {v2, p2, p3, p4}, Lfqj;->a(Landroid/net/Uri;Ljava/lang/String;J)Lfqj;

    move-result-object v2

    iput-object v2, p0, Lfrf;->g:Lfqj;

    .line 112
    :goto_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 113
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 114
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 115
    iget-object v6, p1, Lhwn;->b:[Lhgy;

    array-length v7, v6

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v7, :cond_3

    aget-object v8, v6, v2

    .line 116
    iget-boolean v9, v8, Lhgy;->m:Z

    if-nez v9, :cond_0

    .line 117
    new-instance v9, Lfqj;

    invoke-direct {v9, v8, p2, p3, p4}, Lfqj;-><init>(Lhgy;Ljava/lang/String;J)V

    .line 119
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    invoke-interface {v4, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 102
    :cond_1
    const-wide v2, 0x7fffffffffffffffL

    iput-wide v2, p0, Lfrf;->i:J

    goto :goto_0

    .line 109
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lfrf;->g:Lfqj;

    goto :goto_1

    .line 123
    :cond_3
    iget-object v6, p1, Lhwn;->c:[Lhgy;

    array-length v7, v6

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v7, :cond_5

    aget-object v8, v6, v2

    .line 124
    iget-boolean v9, v8, Lhgy;->m:Z

    if-nez v9, :cond_4

    .line 125
    new-instance v9, Lfqj;

    invoke-direct {v9, v8, p2, p3, p4}, Lfqj;-><init>(Lhgy;Ljava/lang/String;J)V

    .line 127
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 131
    :cond_5
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfrf;->a:Ljava/util/List;

    .line 132
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfrf;->b:Ljava/util/List;

    .line 133
    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lfrf;->c:Ljava/util/List;

    .line 136
    const/4 v3, 0x0

    .line 137
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfqj;

    .line 138
    invoke-virtual {v2}, Lfqj;->c()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 139
    const/4 v2, 0x1

    .line 143
    :goto_4
    iput-boolean v2, p0, Lfrf;->j:Z

    .line 144
    return-void

    :cond_7
    move v2, v3

    goto :goto_4
.end method

.method private d()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lfrf;->d:Lhwn;

    iget-object v0, v0, Lhwn;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrf;->d:Lhwn;

    iget-object v0, v0, Lhwn;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lfqj;
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v2, v0, Lfqj;->a:Lhgy;

    iget v2, v2, Lhgy;->b:I

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Ljava/util/List;)Lfrf;
    .locals 9

    .prologue
    .line 511
    new-instance v2, Lhwn;

    invoke-direct {v2}, Lhwn;-><init>()V

    .line 513
    :try_start_0
    iget-object v0, p0, Lfrf;->d:Lhwn;

    invoke-static {v0}, Lidh;->a(Lidh;)[B

    move-result-object v0

    invoke-static {v2, v0}, Lidh;->a(Lidh;[B)Lidh;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    .line 517
    iget-object v0, v2, Lhwn;->c:[Lhgy;

    .line 519
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lhgy;

    .line 518
    invoke-interface {p1, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .line 517
    invoke-static {v0, v1}, La;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhgy;

    iput-object v0, v2, Lhwn;->c:[Lhgy;

    .line 520
    iget-object v0, v2, Lhwn;->f:[Lhli;

    .line 522
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lhli;

    .line 521
    invoke-interface {p2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    .line 520
    invoke-static {v0, v1}, La;->a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lhli;

    iput-object v0, v2, Lhwn;->f:[Lhli;

    .line 523
    new-instance v1, Lfrf;

    iget-object v3, p0, Lfrf;->e:Ljava/lang/String;

    iget-wide v4, p0, Lfrf;->f:J

    iget-wide v6, p0, Lfrf;->k:J

    iget-boolean v8, p0, Lfrf;->h:Z

    invoke-direct/range {v1 .. v8}, Lfrf;-><init>(Lhwn;Ljava/lang/String;JJZ)V

    :goto_0
    return-object v1

    .line 515
    :catch_0
    move-exception v0

    move-object v1, p0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrf;->d:Lhwn;

    iget-object v0, v0, Lhwn;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)Z
    .locals 3

    .prologue
    .line 373
    iget-wide v0, p0, Lfrf;->i:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lfrf;->d:Lhwn;

    iget-object v0, v0, Lhwn;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrf;->d:Lhwn;

    iget-object v0, v0, Lhwn;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 355
    iget-object v0, p0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    .line 356
    invoke-static {}, Lfqm;->k()Ljava/util/Set;

    move-result-object v3

    iget-object v0, v0, Lfqj;->a:Lhgy;

    iget v0, v0, Lhgy;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 360
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 406
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 445
    instance-of v0, p1, Lfrf;

    if-nez v0, :cond_1

    .line 471
    :cond_0
    :goto_0
    return v1

    .line 448
    :cond_1
    check-cast p1, Lfrf;

    .line 449
    iget-object v0, p0, Lfrf;->e:Ljava/lang/String;

    iget-object v2, p1, Lfrf;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    invoke-direct {p0}, Lfrf;->d()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p1}, Lfrf;->d()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    invoke-virtual {p0}, Lfrf;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Lfrf;->b()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v0, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    iget-boolean v0, p0, Lfrf;->h:Z

    iget-boolean v2, p1, Lfrf;->h:Z

    if-ne v0, v2, :cond_0

    .line 453
    iget-wide v2, p0, Lfrf;->f:J

    iget-wide v4, p1, Lfrf;->f:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 454
    iget-object v0, p0, Lfrf;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p1, Lfrf;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    .line 455
    iget-object v0, p0, Lfrf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p1, Lfrf;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    iget-wide v2, p0, Lfrf;->i:J

    iget-wide v4, p1, Lfrf;->i:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move v0, v1

    .line 459
    :goto_1
    iget-object v2, p0, Lfrf;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 461
    iget-object v2, p0, Lfrf;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, Lfrf;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 460
    invoke-static {v2, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 459
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 465
    :goto_2
    iget-object v2, p0, Lfrf;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 467
    iget-object v2, p0, Lfrf;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p1, Lfrf;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 466
    invoke-static {v2, v3}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 465
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 471
    :cond_3
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 477
    invoke-static {v0}, Lb;->c(Z)V

    .line 478
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 393
    iget-object v0, p0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 394
    new-array v4, v3, [Ljava/lang/Integer;

    move v1, v2

    .line 395
    :goto_0
    if-ge v1, v3, :cond_0

    .line 396
    iget-object v0, p0, Lfrf;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    iget-object v0, v0, Lfqj;->a:Lhgy;

    iget v0, v0, Lhgy;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    .line 395
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 398
    :cond_0
    const-string v0, "ITAGS:{%s} HLS:{%s} DASH:{%s}"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, ", "

    .line 399
    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lfrf;->d:Lhwn;

    iget-object v3, v3, Lhwn;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lfrf;->d:Lhwn;

    iget-object v3, v3, Lhwn;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    .line 398
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lfrf;->d:Lhwn;

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lidh;)V

    .line 412
    iget-object v0, p0, Lfrf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 413
    iget-wide v0, p0, Lfrf;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 414
    iget-wide v0, p0, Lfrf;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 415
    iget-boolean v0, p0, Lfrf;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 416
    return-void

    .line 415
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
