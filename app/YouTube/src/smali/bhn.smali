.class final Lbhn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private final a:Landroid/widget/EditText;

.field private final b:Landroid/os/Bundle;

.field private synthetic c:Lbhj;


# direct methods
.method constructor <init>(Lbhj;Landroid/widget/EditText;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lbhn;->c:Lbhj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    iput-object p2, p0, Lbhn;->a:Landroid/widget/EditText;

    .line 209
    iput-object p3, p0, Lbhn;->b:Landroid/os/Bundle;

    .line 210
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 8

    .prologue
    .line 214
    iget-object v0, p0, Lbhn;->b:Landroid/os/Bundle;

    const-string v1, "YouTubeScreen"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Ldwr;

    .line 216
    iget-object v0, p0, Lbhn;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 217
    iget-object v0, p0, Lbhn;->c:Lbhj;

    .line 218
    invoke-virtual {v0}, Lbhj;->j()Lo;

    move-result-object v0

    const-string v3, "input_method"

    invoke-virtual {v0, v3}, Lo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 219
    iget-object v3, p0, Lbhn;->a:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 221
    iget-object v0, p0, Lbhn;->c:Lbhj;

    .line 222
    invoke-virtual {v0}, Lbhj;->j()Lo;

    move-result-object v0

    invoke-virtual {v0}, Lo;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    .line 223
    iget-object v3, v0, Lari;->b:Ldov;

    invoke-virtual {v3}, Ldov;->d()Ldwv;

    move-result-object v4

    .line 224
    iget-object v0, v0, Lari;->b:Ldov;

    invoke-virtual {v0}, Ldov;->e()Ldxf;

    move-result-object v6

    .line 226
    invoke-virtual {v2}, Ldwr;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 228
    invoke-interface {v4}, Ldwv;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->a(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lbhn;->c:Lbhj;

    invoke-virtual {v0}, Lbhj;->j()Lo;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;->a(Ljava/lang/String;Lo;)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v0, p0, Lbhn;->c:Lbhj;

    .line 234
    invoke-virtual {v0}, Lbhj;->j()Lo;

    move-result-object v0

    new-instance v3, Lbhm;

    iget-object v5, p0, Lbhn;->c:Lbhj;

    invoke-static {v5}, Lbhj;->a(Lbhj;)Lbhl;

    move-result-object v5

    iget-object v6, p0, Lbhn;->c:Lbhj;

    invoke-virtual {v6}, Lbhj;->j()Lo;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lbhm;-><init>(Lbhl;Lo;)V

    .line 233
    invoke-static {v0, v3}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    .line 235
    invoke-interface {v4, v2, v1, v0}, Ldwv;->a(Ldwr;Ljava/lang/String;Leuc;)V

    goto :goto_0

    .line 237
    :cond_2
    invoke-virtual {v2}, Ldwr;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lbhn;->c:Lbhj;

    .line 239
    invoke-virtual {v0}, Lbhj;->j()Lo;

    move-result-object v7

    new-instance v0, Lbhp;

    iget-object v3, p0, Lbhn;->c:Lbhj;

    .line 240
    invoke-static {v3}, Lbhj;->a(Lbhj;)Lbhl;

    move-result-object v3

    iget-object v5, p0, Lbhn;->c:Lbhj;

    invoke-virtual {v5}, Lbhj;->j()Lo;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lbhp;-><init>(Ljava/lang/String;Ldwr;Lbhl;Ldwv;Lo;)V

    .line 238
    invoke-static {v7, v0}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    invoke-interface {v6, v0}, Ldxf;->b(Leuc;)V

    goto :goto_0
.end method
