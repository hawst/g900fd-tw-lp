.class public final Lbrf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Landroid/util/SparseIntArray;

.field private static volatile b:[I

.field private static c:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lbrf;->b(II)I

    move-result v0

    return v0
.end method

.method private static a(II)V
    .locals 0

    .prologue
    .line 166
    invoke-static {p0, p1, p1, p1, p1}, Lbrf;->a(IIIII)V

    .line 172
    return-void
.end method

.method private static a(IIIII)V
    .locals 3

    .prologue
    .line 154
    sget v0, Lbrf;->c:I

    shl-int/lit8 v0, v0, 0x2

    .line 155
    sget-object v1, Lbrf;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p0, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 156
    sget-object v1, Lbrf;->b:[I

    aput p1, v1, v0

    .line 157
    sget-object v1, Lbrf;->b:[I

    add-int/lit8 v2, v0, 0x1

    aput p2, v1, v2

    .line 158
    sget-object v1, Lbrf;->b:[I

    add-int/lit8 v2, v0, 0x2

    aput p3, v1, v2

    .line 159
    sget-object v1, Lbrf;->b:[I

    add-int/lit8 v0, v0, 0x3

    aput p4, v1, v0

    .line 160
    sget v0, Lbrf;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lbrf;->c:I

    .line 161
    return-void
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lbrf;->b(II)I

    move-result v0

    return v0
.end method

.method private static b(II)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 175
    sget-object v1, Lbrf;->a:Landroid/util/SparseIntArray;

    if-eqz v1, :cond_0

    sget-object v1, Lbrf;->b:[I

    if-nez v1, :cond_1

    .line 176
    :cond_0
    const-class v1, Lbrf;

    monitor-enter v1

    :try_start_0
    sget-object v2, Lbrf;->a:Landroid/util/SparseIntArray;

    if-eqz v2, :cond_3

    sget-object v2, Lbrf;->b:[I

    if-eqz v2, :cond_3

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_1
    :goto_0
    sget-object v1, Lbrf;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v1, p0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    .line 180
    if-ltz v1, :cond_2

    .line 181
    sget-object v0, Lbrf;->b:[I

    add-int/2addr v1, p1

    aget v0, v0, v1

    .line 183
    :cond_2
    return v0

    .line 176
    :cond_3
    :try_start_1
    new-instance v2, Landroid/util/SparseIntArray;

    const/16 v3, 0x14

    invoke-direct {v2, v3}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v2, Lbrf;->a:Landroid/util/SparseIntArray;

    const/16 v2, 0x50

    new-array v2, v2, [I

    sput-object v2, Lbrf;->b:[I

    const/4 v2, 0x0

    sput v2, Lbrf;->c:I

    const/16 v2, 0xc

    const v3, 0x7f020108

    const v4, 0x7f020227

    const v5, 0x7f020228

    const v6, 0x7f020229

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0x14

    const v3, 0x7f020109

    const v4, 0x7f02022a

    const v5, 0x7f02022b

    const v6, 0x7f02022c

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0x13

    const v3, 0x7f02010a

    const v4, 0x7f02022d

    const v5, 0x7f02022e

    const v6, 0x7f02022f

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0xe

    const v3, 0x7f02010b

    const v4, 0x7f020230

    const v5, 0x7f020231

    const v6, 0x7f020232

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0x10

    const v3, 0x7f02010c

    const v4, 0x7f020233

    const v5, 0x7f020234

    const v6, 0x7f020235

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0x11

    const v3, 0x7f02010d

    const v4, 0x7f020236

    const v5, 0x7f020237

    const v6, 0x7f020238

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0x18

    const v3, 0x7f020126

    invoke-static {v2, v3}, Lbrf;->a(II)V

    const/16 v2, 0x1d

    const v3, 0x7f02010e

    const v4, 0x7f020239

    const v5, 0x7f02023a

    const v6, 0x7f02023b

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0x8

    const v3, 0x7f02010f

    const v4, 0x7f02023c

    const v5, 0x7f02023d

    const v6, 0x7f02023e

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/4 v2, 0x7

    const v3, 0x7f020110

    const v4, 0x7f02023f

    const v5, 0x7f020240

    const v6, 0x7f020241

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/4 v2, 0x4

    const v3, 0x7f020111

    const v4, 0x7f020242

    const v5, 0x7f020243

    const v6, 0x7f020244

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/4 v2, 0x6

    const v3, 0x7f020113

    const v4, 0x7f020248

    const v5, 0x7f020249

    const v6, 0x7f02024a

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0xa

    const v3, 0x7f020114

    const v4, 0x7f02024b

    const v5, 0x7f02024c

    const v6, 0x7f02024d

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/4 v2, 0x2

    const v3, 0x7f020115

    const v4, 0x7f02024e

    const v5, 0x7f02024f

    const v6, 0x7f020250

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/4 v2, 0x3

    const v3, 0x7f020116

    const v4, 0x7f020251

    const v5, 0x7f020252

    const v6, 0x7f020253

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/4 v2, 0x5

    const v3, 0x7f020117

    const v4, 0x7f020254

    const v5, 0x7f020255

    const v6, 0x7f020256

    invoke-static {v2, v3, v4, v5, v6}, Lbrf;->a(IIIII)V

    const/16 v2, 0x41

    const v3, 0x7f02019c

    invoke-static {v2, v3}, Lbrf;->a(II)V

    const/16 v2, 0x42

    const v3, 0x7f02019e

    invoke-static {v2, v3}, Lbrf;->a(II)V

    const/16 v2, 0x43

    const v3, 0x7f02019f

    invoke-static {v2, v3}, Lbrf;->a(II)V

    const/16 v2, 0x44

    const v3, 0x7f02019d

    invoke-static {v2, v3}, Lbrf;->a(II)V

    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static c(I)I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lbrf;->b(II)I

    move-result v0

    return v0
.end method
