.class public final enum Lcpu;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcpu;

.field public static final enum b:Lcpu;

.field public static final enum c:Lcpu;

.field public static final enum d:Lcpu;

.field public static final enum e:Lcpu;

.field public static final enum f:Lcpu;

.field public static final enum g:Lcpu;

.field public static final enum h:Lcpu;

.field private static enum i:Lcpu;

.field private static final synthetic k:[Lcpu;


# instance fields
.field private final j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 86
    new-instance v0, Lcpu;

    const-string v1, "BUFFERING"

    const-string v2, "B"

    invoke-direct {v0, v1, v4, v2}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->a:Lcpu;

    .line 87
    new-instance v0, Lcpu;

    const-string v1, "ERROR"

    const-string v2, "ER"

    invoke-direct {v0, v1, v5, v2}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->b:Lcpu;

    .line 88
    new-instance v0, Lcpu;

    const-string v1, "ENDED"

    const-string v2, "EN"

    invoke-direct {v0, v1, v6, v2}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->c:Lcpu;

    .line 89
    new-instance v0, Lcpu;

    const-string v1, "NOT_PLAYING"

    const-string v2, "N"

    invoke-direct {v0, v1, v7, v2}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->d:Lcpu;

    .line 90
    new-instance v0, Lcpu;

    const-string v1, "PAUSED"

    const-string v2, "PA"

    invoke-direct {v0, v1, v8, v2}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->e:Lcpu;

    .line 91
    new-instance v0, Lcpu;

    const-string v1, "PLAYING"

    const/4 v2, 0x5

    const-string v3, "PL"

    invoke-direct {v0, v1, v2, v3}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->f:Lcpu;

    .line 92
    new-instance v0, Lcpu;

    const-string v1, "SEEKING"

    const/4 v2, 0x6

    const-string v3, "S"

    invoke-direct {v0, v1, v2, v3}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->g:Lcpu;

    .line 93
    new-instance v0, Lcpu;

    const-string v1, "NOT_VALID"

    const/4 v2, 0x7

    const-string v3, "X"

    invoke-direct {v0, v1, v2, v3}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->i:Lcpu;

    .line 94
    new-instance v0, Lcpu;

    const-string v1, "PAUSED_BUFFERING"

    const/16 v2, 0x8

    const-string v3, "PB"

    invoke-direct {v0, v1, v2, v3}, Lcpu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcpu;->h:Lcpu;

    .line 85
    const/16 v0, 0x9

    new-array v0, v0, [Lcpu;

    sget-object v1, Lcpu;->a:Lcpu;

    aput-object v1, v0, v4

    sget-object v1, Lcpu;->b:Lcpu;

    aput-object v1, v0, v5

    sget-object v1, Lcpu;->c:Lcpu;

    aput-object v1, v0, v6

    sget-object v1, Lcpu;->d:Lcpu;

    aput-object v1, v0, v7

    sget-object v1, Lcpu;->e:Lcpu;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcpu;->f:Lcpu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcpu;->g:Lcpu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcpu;->i:Lcpu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcpu;->h:Lcpu;

    aput-object v2, v0, v1

    sput-object v0, Lcpu;->k:[Lcpu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 99
    iput-object p3, p0, Lcpu;->j:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcpu;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lcpu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcpu;

    return-object v0
.end method

.method public static values()[Lcpu;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcpu;->k:[Lcpu;

    invoke-virtual {v0}, [Lcpu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcpu;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcpu;->j:Ljava/lang/String;

    return-object v0
.end method
