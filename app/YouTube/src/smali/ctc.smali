.class final Lctc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lgit;

.field private synthetic b:Leuc;

.field private synthetic c:Lctb;


# direct methods
.method constructor <init>(Lctb;Lgit;Leuc;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lctc;->c:Lctb;

    iput-object p2, p0, Lctc;->a:Lgit;

    iput-object p3, p0, Lctc;->b:Leuc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 159
    :try_start_0
    iget-object v0, p0, Lctc;->c:Lctb;

    .line 160
    iget-object v0, v0, Lctb;->d:Lfbu;

    iget-object v1, p0, Lctc;->a:Lgit;

    iget-object v1, v1, Lgit;->b:Lgiv;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Lfbu;->a(Lgiv;Z)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbw;

    .line 161
    invoke-virtual {v0}, Lfbw;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    iget-object v1, p0, Lctc;->a:Lgit;

    iget-object v1, v1, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lctc;->a:Lgit;

    iget-object v1, v1, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->d()Ljava/lang/String;

    move-result-object v1

    .line 165
    :goto_0
    iget-object v3, p0, Lctc;->c:Lctb;

    iget-object v3, v3, Lctb;->a:Lgku;

    iget-object v4, p0, Lctc;->c:Lctb;

    .line 166
    iget-object v4, v4, Lctb;->b:Lfxj;

    .line 167
    invoke-virtual {v0}, Lfbw;->c()Landroid/util/Pair;

    move-result-object v0

    .line 166
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "default"

    invoke-virtual {v4, v0}, Lfxj;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "on-behalf-of"

    invoke-virtual {v0, v4, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_0
    invoke-static {v5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lfxg;

    const/4 v4, 0x0

    invoke-direct {v1, v0, v5, v4}, Lfxg;-><init>(Landroid/net/Uri;Ljava/util/Map;[B)V

    iget-object v0, p0, Lctc;->b:Leuc;

    .line 165
    invoke-interface {v3, v1, v0}, Lgku;->a(Ljava/lang/Object;Leuc;)V

    .line 177
    :goto_1
    return-void

    :cond_1
    move-object v1, v2

    .line 164
    goto :goto_0

    .line 170
    :cond_2
    iget-object v0, p0, Lctc;->b:Leuc;

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Authentication unsuccessful."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v3}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 172
    :catch_0
    move-exception v0

    .line 173
    iget-object v1, p0, Lctc;->b:Leuc;

    invoke-interface {v1, v2, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1

    .line 174
    :catch_1
    move-exception v0

    .line 175
    iget-object v1, p0, Lctc;->b:Leuc;

    invoke-interface {v1, v2, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method
