.class public final Lbhq;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/honeycomb/phone/ScreenManagementActivity;Landroid/content/Context;IILandroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 181
    iput-object p5, p0, Lbhq;->a:Landroid/view/View$OnClickListener;

    .line 182
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 186
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 187
    const v0, 0x7f0802bd

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 188
    invoke-virtual {p0, p1}, Lbhq;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    .line 189
    iget-object v4, v0, Lbhr;->a:Ldwr;

    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 190
    iget-object v0, v0, Lbhr;->a:Ldwr;

    iget-object v0, v0, Ldwr;->b:Ldws;

    sget-object v4, Ldws;->a:Ldws;

    if-eq v0, v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 191
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 196
    :goto_1
    return-object v2

    :cond_0
    move v0, v1

    .line 190
    goto :goto_0

    .line 193
    :cond_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lbhq;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
