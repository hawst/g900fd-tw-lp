.class public final Laxc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lawd;
.implements Lawm;


# instance fields
.field final a:Landroid/view/View;

.field private final b:Landroid/view/View;

.field private final c:Lawb;

.field private d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/ImageView;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lawb;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Laxc;->b:Landroid/view/View;

    .line 34
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawb;

    iput-object v0, p0, Laxc;->c:Lawb;

    .line 35
    const v0, 0x7f08012e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laxc;->d:Landroid/widget/ImageView;

    .line 36
    const v0, 0x7f08027b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laxc;->e:Landroid/widget/ImageView;

    .line 37
    const v0, 0x7f08012c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Laxc;->a:Landroid/view/View;

    .line 38
    iget-object v0, p0, Laxc;->e:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    iget-object v0, p0, Laxc;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 39
    :cond_0
    iput-object p0, p1, Lawb;->a:Lawd;

    .line 45
    :cond_1
    new-instance v0, Laxd;

    invoke-direct {v0, p0}, Laxd;-><init>(Laxc;)V

    iput-object v0, p0, Laxc;->f:Ljava/lang/Runnable;

    .line 55
    return-void
.end method


# virtual methods
.method public final synthetic a(ILjava/lang/Object;)Landroid/view/View;
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 19
    check-cast p2, Lgcd;

    iget-object v0, p0, Laxc;->b:Landroid/view/View;

    iget-object v2, p0, Laxc;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Laxc;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laxc;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Laxc;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Laxc;->a:Landroid/view/View;

    const v2, 0x7f020097

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_1
    iget-object v0, p0, Laxc;->c:Lawb;

    invoke-virtual {v0, p1, p2}, Lawb;->a(ILjava/lang/Object;)Landroid/view/View;

    iget-object v0, p2, Lgcd;->B:Lgch;

    sget-object v2, Lgch;->a:Lgch;

    if-eq v0, v2, :cond_2

    iget-object v0, p2, Lgcd;->B:Lgch;

    sget-object v2, Lgch;->b:Lgch;

    if-ne v0, v2, :cond_4

    invoke-virtual {p2}, Lgcd;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_5

    iget-object v0, p0, Laxc;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Laxc;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_3
    :goto_1
    iget-object v0, p0, Laxc;->b:Landroid/view/View;

    return-object v0

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Laxc;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_6

    iget-object v0, p0, Laxc;->b:Landroid/view/View;

    const v2, 0x7f08012d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Laxc;->d:Landroid/widget/ImageView;

    :cond_6
    iget-object v0, p0, Laxc;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Laxc;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Laxc;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Laxc;->b:Landroid/view/View;

    iget-object v1, p0, Laxc;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 91
    return-void
.end method
