.class public final Lhdi;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:[Lhdm;

.field private b:[Lhdl;

.field private c:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30093
    invoke-direct {p0}, Lidf;-><init>()V

    .line 30096
    sget-object v0, Lhdm;->a:[Lhdm;

    iput-object v0, p0, Lhdi;->a:[Lhdm;

    .line 30099
    sget-object v0, Lhdl;->a:[Lhdl;

    iput-object v0, p0, Lhdi;->b:[Lhdl;

    .line 30102
    const/4 v0, 0x0

    iput-object v0, p0, Lhdi;->c:Lhgz;

    .line 30093
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 30169
    .line 30170
    iget-object v0, p0, Lhdi;->a:[Lhdm;

    if-eqz v0, :cond_1

    .line 30171
    iget-object v3, p0, Lhdi;->a:[Lhdm;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 30172
    if-eqz v5, :cond_0

    .line 30173
    const/4 v6, 0x1

    .line 30174
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 30171
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 30178
    :cond_2
    iget-object v2, p0, Lhdi;->b:[Lhdl;

    if-eqz v2, :cond_4

    .line 30179
    iget-object v2, p0, Lhdi;->b:[Lhdl;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 30180
    if-eqz v4, :cond_3

    .line 30181
    const/4 v5, 0x2

    .line 30182
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 30179
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 30186
    :cond_4
    iget-object v1, p0, Lhdi;->c:Lhgz;

    if-eqz v1, :cond_5

    .line 30187
    const/4 v1, 0x3

    iget-object v2, p0, Lhdi;->c:Lhgz;

    .line 30188
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30190
    :cond_5
    iget-object v1, p0, Lhdi;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 30191
    iput v0, p0, Lhdi;->J:I

    .line 30192
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 30089
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhdi;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhdi;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhdi;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhdi;->a:[Lhdm;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhdm;

    iget-object v3, p0, Lhdi;->a:[Lhdm;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhdi;->a:[Lhdm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhdi;->a:[Lhdm;

    :goto_2
    iget-object v2, p0, Lhdi;->a:[Lhdm;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhdi;->a:[Lhdm;

    new-instance v3, Lhdm;

    invoke-direct {v3}, Lhdm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdi;->a:[Lhdm;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhdi;->a:[Lhdm;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhdi;->a:[Lhdm;

    new-instance v3, Lhdm;

    invoke-direct {v3}, Lhdm;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdi;->a:[Lhdm;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhdi;->b:[Lhdl;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhdl;

    iget-object v3, p0, Lhdi;->b:[Lhdl;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lhdi;->b:[Lhdl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    iput-object v2, p0, Lhdi;->b:[Lhdl;

    :goto_4
    iget-object v2, p0, Lhdi;->b:[Lhdl;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lhdi;->b:[Lhdl;

    new-instance v3, Lhdl;

    invoke-direct {v3}, Lhdl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdi;->b:[Lhdl;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lhdi;->b:[Lhdl;

    array-length v0, v0

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhdi;->b:[Lhdl;

    new-instance v3, Lhdl;

    invoke-direct {v3}, Lhdl;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhdi;->b:[Lhdl;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lhdi;->c:Lhgz;

    if-nez v0, :cond_8

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhdi;->c:Lhgz;

    :cond_8
    iget-object v0, p0, Lhdi;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 30146
    iget-object v1, p0, Lhdi;->a:[Lhdm;

    if-eqz v1, :cond_1

    .line 30147
    iget-object v2, p0, Lhdi;->a:[Lhdm;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 30148
    if-eqz v4, :cond_0

    .line 30149
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 30147
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 30153
    :cond_1
    iget-object v1, p0, Lhdi;->b:[Lhdl;

    if-eqz v1, :cond_3

    .line 30154
    iget-object v1, p0, Lhdi;->b:[Lhdl;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 30155
    if-eqz v3, :cond_2

    .line 30156
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 30154
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 30160
    :cond_3
    iget-object v0, p0, Lhdi;->c:Lhgz;

    if-eqz v0, :cond_4

    .line 30161
    const/4 v0, 0x3

    iget-object v1, p0, Lhdi;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 30163
    :cond_4
    iget-object v0, p0, Lhdi;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 30165
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30115
    if-ne p1, p0, :cond_1

    .line 30121
    :cond_0
    :goto_0
    return v0

    .line 30116
    :cond_1
    instance-of v2, p1, Lhdi;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 30117
    :cond_2
    check-cast p1, Lhdi;

    .line 30118
    iget-object v2, p0, Lhdi;->a:[Lhdm;

    iget-object v3, p1, Lhdi;->a:[Lhdm;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhdi;->b:[Lhdl;

    iget-object v3, p1, Lhdi;->b:[Lhdl;

    .line 30119
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhdi;->c:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdi;->c:Lhgz;

    if-nez v2, :cond_3

    .line 30120
    :goto_1
    iget-object v2, p0, Lhdi;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdi;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 30121
    goto :goto_0

    .line 30119
    :cond_4
    iget-object v2, p0, Lhdi;->c:Lhgz;

    iget-object v3, p1, Lhdi;->c:Lhgz;

    .line 30120
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhdi;->I:Ljava/util/List;

    iget-object v3, p1, Lhdi;->I:Ljava/util/List;

    .line 30121
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 30125
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 30127
    iget-object v2, p0, Lhdi;->a:[Lhdm;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    .line 30133
    :cond_0
    iget-object v0, p0, Lhdi;->b:[Lhdl;

    if-nez v0, :cond_4

    mul-int/lit8 v2, v2, 0x1f

    .line 30139
    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhdi;->c:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 30140
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhdi;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_1
    add-int/2addr v0, v1

    .line 30141
    return v0

    :cond_2
    move v2, v0

    move v0, v1

    .line 30129
    :goto_2
    iget-object v3, p0, Lhdi;->a:[Lhdm;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 30130
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhdi;->a:[Lhdm;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_3
    add-int/2addr v2, v3

    .line 30129
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 30130
    :cond_3
    iget-object v2, p0, Lhdi;->a:[Lhdm;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhdm;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_4
    move v0, v1

    .line 30135
    :goto_4
    iget-object v3, p0, Lhdi;->b:[Lhdl;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 30136
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhdi;->b:[Lhdl;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 30135
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 30136
    :cond_5
    iget-object v2, p0, Lhdi;->b:[Lhdl;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhdl;->hashCode()I

    move-result v2

    goto :goto_5

    .line 30139
    :cond_6
    iget-object v0, p0, Lhdi;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 30140
    :cond_7
    iget-object v1, p0, Lhdi;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
