.class public final Ldzy;
.super Lida;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/util/List;

.field public g:Z

.field public h:J

.field public i:Ljava/lang/String;

.field public j:J

.field public k:I

.field public l:J

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Licw;

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Lida;-><init>()V

    .line 14
    const-string v0, ""

    iput-object v0, p0, Ldzy;->a:Ljava/lang/String;

    .line 31
    iput v1, p0, Ldzy;->b:I

    .line 48
    iput v1, p0, Ldzy;->c:I

    .line 65
    const-string v0, ""

    iput-object v0, p0, Ldzy;->e:Ljava/lang/String;

    .line 82
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldzy;->f:Ljava/util/List;

    .line 115
    sget-object v0, Licw;->b:Licw;

    iput-object v0, p0, Ldzy;->q:Licw;

    .line 132
    iput-wide v2, p0, Ldzy;->h:J

    .line 149
    const-string v0, ""

    iput-object v0, p0, Ldzy;->i:Ljava/lang/String;

    .line 166
    iput-wide v2, p0, Ldzy;->j:J

    .line 183
    iput v1, p0, Ldzy;->k:I

    .line 200
    iput-wide v2, p0, Ldzy;->l:J

    .line 271
    const/4 v0, -0x1

    iput v0, p0, Ldzy;->v:I

    .line 9
    return-void
.end method

.method public static a([B)Ldzy;
    .locals 1

    .prologue
    .line 396
    new-instance v0, Ldzy;

    invoke-direct {v0}, Ldzy;-><init>()V

    invoke-virtual {v0, p0}, Ldzy;->b([B)Lida;

    move-result-object v0

    check-cast v0, Ldzy;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Ldzy;->v:I

    if-gez v0, :cond_0

    .line 275
    invoke-virtual {p0}, Ldzy;->b()I

    .line 277
    :cond_0
    iget v0, p0, Ldzy;->v:I

    return v0
.end method

.method public final a(I)Ldzy;
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->n:Z

    .line 36
    iput p1, p0, Ldzy;->b:I

    .line 37
    return-object p0
.end method

.method public final a(J)Ldzy;
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->g:Z

    .line 137
    iput-wide p1, p0, Ldzy;->h:J

    .line 138
    return-object p0
.end method

.method public final a(Ldzx;)Ldzy;
    .locals 1

    .prologue
    .line 98
    if-nez p1, :cond_0

    .line 99
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 101
    :cond_0
    iget-object v0, p0, Ldzy;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldzy;->f:Ljava/util/List;

    .line 104
    :cond_1
    iget-object v0, p0, Ldzy;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    return-object p0
.end method

.method public final a(Licw;)Ldzy;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->p:Z

    .line 120
    iput-object p1, p0, Ldzy;->q:Licw;

    .line 121
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Ldzy;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->m:Z

    .line 19
    iput-object p1, p0, Ldzy;->a:Ljava/lang/String;

    .line 20
    return-object p0
.end method

.method public final synthetic a(Licx;)Lida;
    .locals 2

    .prologue
    .line 7
    :cond_0
    :goto_0
    invoke-virtual {p1}, Licx;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Licx;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Licx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldzy;->a(Ljava/lang/String;)Ldzy;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Licx;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Ldzy;->a(I)Ldzy;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Licx;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Ldzy;->b(I)Ldzy;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Licx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldzy;->b(Ljava/lang/String;)Ldzy;

    goto :goto_0

    :sswitch_5
    new-instance v0, Ldzx;

    invoke-direct {v0}, Ldzx;-><init>()V

    invoke-virtual {p1, v0}, Licx;->a(Lida;)V

    invoke-virtual {p0, v0}, Ldzy;->a(Ldzx;)Ldzy;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Licx;->e()Licw;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldzy;->a(Licw;)Ldzy;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Licx;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldzy;->a(J)Ldzy;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Licx;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldzy;->c(Ljava/lang/String;)Ldzy;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Licx;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldzy;->b(J)Ldzy;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Licx;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Ldzy;->c(I)Ldzy;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Licx;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Ldzy;->c(J)Ldzy;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Licy;)V
    .locals 4

    .prologue
    .line 236
    iget-boolean v0, p0, Ldzy;->m:Z

    if-eqz v0, :cond_0

    .line 237
    const/4 v0, 0x1

    iget-object v1, p0, Ldzy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILjava/lang/String;)V

    .line 239
    :cond_0
    iget-boolean v0, p0, Ldzy;->n:Z

    if-eqz v0, :cond_1

    .line 240
    const/4 v0, 0x2

    iget v1, p0, Ldzy;->b:I

    invoke-virtual {p1, v0, v1}, Licy;->a(II)V

    .line 242
    :cond_1
    iget-boolean v0, p0, Ldzy;->o:Z

    if-eqz v0, :cond_2

    .line 243
    const/4 v0, 0x3

    iget v1, p0, Ldzy;->c:I

    invoke-virtual {p1, v0, v1}, Licy;->a(II)V

    .line 245
    :cond_2
    iget-boolean v0, p0, Ldzy;->d:Z

    if-eqz v0, :cond_3

    .line 246
    const/4 v0, 0x4

    iget-object v1, p0, Ldzy;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILjava/lang/String;)V

    .line 248
    :cond_3
    iget-object v0, p0, Ldzy;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzx;

    .line 249
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Licy;->a(ILida;)V

    goto :goto_0

    .line 251
    :cond_4
    iget-boolean v0, p0, Ldzy;->p:Z

    if-eqz v0, :cond_5

    .line 252
    const/4 v0, 0x6

    iget-object v1, p0, Ldzy;->q:Licw;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILicw;)V

    .line 254
    :cond_5
    iget-boolean v0, p0, Ldzy;->g:Z

    if-eqz v0, :cond_6

    .line 255
    const/4 v0, 0x7

    iget-wide v2, p0, Ldzy;->h:J

    invoke-virtual {p1, v0, v2, v3}, Licy;->a(IJ)V

    .line 257
    :cond_6
    iget-boolean v0, p0, Ldzy;->r:Z

    if-eqz v0, :cond_7

    .line 258
    const/16 v0, 0x8

    iget-object v1, p0, Ldzy;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Licy;->a(ILjava/lang/String;)V

    .line 260
    :cond_7
    iget-boolean v0, p0, Ldzy;->s:Z

    if-eqz v0, :cond_8

    .line 261
    const/16 v0, 0x9

    iget-wide v2, p0, Ldzy;->j:J

    invoke-virtual {p1, v0, v2, v3}, Licy;->a(IJ)V

    .line 263
    :cond_8
    iget-boolean v0, p0, Ldzy;->t:Z

    if-eqz v0, :cond_9

    .line 264
    const/16 v0, 0xa

    iget v1, p0, Ldzy;->k:I

    invoke-virtual {p1, v0, v1}, Licy;->a(II)V

    .line 266
    :cond_9
    iget-boolean v0, p0, Ldzy;->u:Z

    if-eqz v0, :cond_a

    .line 267
    const/16 v0, 0xb

    iget-wide v2, p0, Ldzy;->l:J

    invoke-virtual {p1, v0, v2, v3}, Licy;->a(IJ)V

    .line 269
    :cond_a
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 281
    const/4 v0, 0x0

    .line 282
    iget-boolean v1, p0, Ldzy;->m:Z

    if-eqz v1, :cond_0

    .line 283
    const/4 v0, 0x1

    .line 284
    iget-object v1, p0, Ldzy;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Licy;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 286
    :cond_0
    iget-boolean v1, p0, Ldzy;->n:Z

    if-eqz v1, :cond_1

    .line 287
    const/4 v1, 0x2

    .line 288
    iget v2, p0, Ldzy;->b:I

    invoke-static {v1, v2}, Licy;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 290
    :cond_1
    iget-boolean v1, p0, Ldzy;->o:Z

    if-eqz v1, :cond_2

    .line 291
    const/4 v1, 0x3

    .line 292
    iget v2, p0, Ldzy;->c:I

    invoke-static {v1, v2}, Licy;->b(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_2
    iget-boolean v1, p0, Ldzy;->d:Z

    if-eqz v1, :cond_3

    .line 295
    const/4 v1, 0x4

    .line 296
    iget-object v2, p0, Ldzy;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Licy;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_3
    iget-object v1, p0, Ldzy;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzx;

    .line 299
    const/4 v3, 0x5

    .line 300
    invoke-static {v3, v0}, Licy;->b(ILida;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 301
    goto :goto_0

    .line 302
    :cond_4
    iget-boolean v0, p0, Ldzy;->p:Z

    if-eqz v0, :cond_5

    .line 303
    const/4 v0, 0x6

    .line 304
    iget-object v2, p0, Ldzy;->q:Licw;

    invoke-static {v0, v2}, Licy;->b(ILicw;)I

    move-result v0

    add-int/2addr v1, v0

    .line 306
    :cond_5
    iget-boolean v0, p0, Ldzy;->g:Z

    if-eqz v0, :cond_6

    .line 307
    const/4 v0, 0x7

    .line 308
    iget-wide v2, p0, Ldzy;->h:J

    invoke-static {v0, v2, v3}, Licy;->b(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 310
    :cond_6
    iget-boolean v0, p0, Ldzy;->r:Z

    if-eqz v0, :cond_7

    .line 311
    const/16 v0, 0x8

    .line 312
    iget-object v2, p0, Ldzy;->i:Ljava/lang/String;

    invoke-static {v0, v2}, Licy;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 314
    :cond_7
    iget-boolean v0, p0, Ldzy;->s:Z

    if-eqz v0, :cond_8

    .line 315
    const/16 v0, 0x9

    .line 316
    iget-wide v2, p0, Ldzy;->j:J

    invoke-static {v0, v2, v3}, Licy;->b(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 318
    :cond_8
    iget-boolean v0, p0, Ldzy;->t:Z

    if-eqz v0, :cond_9

    .line 319
    const/16 v0, 0xa

    .line 320
    iget v2, p0, Ldzy;->k:I

    invoke-static {v0, v2}, Licy;->b(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 322
    :cond_9
    iget-boolean v0, p0, Ldzy;->u:Z

    if-eqz v0, :cond_a

    .line 323
    const/16 v0, 0xb

    .line 324
    iget-wide v2, p0, Ldzy;->l:J

    invoke-static {v0, v2, v3}, Licy;->b(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 326
    :cond_a
    iput v1, p0, Ldzy;->v:I

    .line 327
    return v1
.end method

.method public final b(I)Ldzy;
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->o:Z

    .line 53
    iput p1, p0, Ldzy;->c:I

    .line 54
    return-object p0
.end method

.method public final b(J)Ldzy;
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->s:Z

    .line 171
    iput-wide p1, p0, Ldzy;->j:J

    .line 172
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Ldzy;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->d:Z

    .line 70
    iput-object p1, p0, Ldzy;->e:Ljava/lang/String;

    .line 71
    return-object p0
.end method

.method public final c(I)Ldzy;
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->t:Z

    .line 188
    iput p1, p0, Ldzy;->k:I

    .line 189
    return-object p0
.end method

.method public final c(J)Ldzy;
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->u:Z

    .line 205
    iput-wide p1, p0, Ldzy;->l:J

    .line 206
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Ldzy;
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldzy;->r:Z

    .line 154
    iput-object p1, p0, Ldzy;->i:Ljava/lang/String;

    .line 155
    return-object p0
.end method
