.class public final enum Lwr;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lwr;

.field public static final enum b:Lwr;

.field public static final enum c:Lwr;

.field public static final enum d:Lwr;

.field private static final synthetic e:[Lwr;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 434
    new-instance v0, Lwr;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v2}, Lwr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lwr;->a:Lwr;

    .line 435
    new-instance v0, Lwr;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lwr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lwr;->b:Lwr;

    .line 436
    new-instance v0, Lwr;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4}, Lwr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lwr;->c:Lwr;

    .line 437
    new-instance v0, Lwr;

    const-string v1, "IMMEDIATE"

    invoke-direct {v0, v1, v5}, Lwr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lwr;->d:Lwr;

    .line 433
    const/4 v0, 0x4

    new-array v0, v0, [Lwr;

    sget-object v1, Lwr;->a:Lwr;

    aput-object v1, v0, v2

    sget-object v1, Lwr;->b:Lwr;

    aput-object v1, v0, v3

    sget-object v1, Lwr;->c:Lwr;

    aput-object v1, v0, v4

    sget-object v1, Lwr;->d:Lwr;

    aput-object v1, v0, v5

    sput-object v0, Lwr;->e:[Lwr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 433
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lwr;
    .locals 1

    .prologue
    .line 433
    const-class v0, Lwr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lwr;

    return-object v0
.end method

.method public static values()[Lwr;
    .locals 1

    .prologue
    .line 433
    sget-object v0, Lwr;->e:[Lwr;

    invoke-virtual {v0}, [Lwr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lwr;

    return-object v0
.end method
