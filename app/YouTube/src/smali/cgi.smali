.class public final Lcgi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field final a:Lfhz;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/ProgressBar;

.field private final h:Landroid/widget/ProgressBar;

.field private final i:Landroid/widget/RelativeLayout;

.field private final j:Landroid/view/View;

.field private final k:Landroid/content/Context;

.field private final l:Lfsj;

.field private final m:Lfun;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lfhz;Lfsj;Lfun;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcgi;->k:Landroid/content/Context;

    .line 54
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcgi;->a:Lfhz;

    .line 55
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcgi;->l:Lfsj;

    .line 56
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfun;

    iput-object v0, p0, Lcgi;->m:Lfun;

    .line 58
    const v0, 0x7f0400a9

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgi;->b:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgi;->c:Landroid/widget/TextView;

    .line 60
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f080253

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgi;->d:Landroid/widget/TextView;

    .line 61
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f080254

    .line 62
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgi;->e:Landroid/widget/TextView;

    .line 63
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcgi;->f:Landroid/widget/ImageView;

    .line 64
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f080251

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcgi;->g:Landroid/widget/ProgressBar;

    .line 65
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f080252

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcgi;->h:Landroid/widget/ProgressBar;

    .line 66
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcgi;->i:Landroid/widget/RelativeLayout;

    .line 67
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgi;->j:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lcgi;->b:Landroid/view/View;

    invoke-interface {p3, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 70
    return-void
.end method


# virtual methods
.method public final synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 10

    .prologue
    const/16 v9, 0x5a

    const/16 v8, 0x8

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 30
    check-cast p2, Lcku;

    iget-object v0, p0, Lcgi;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcgi;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b0012

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    iget-object v0, p0, Lcgi;->c:Landroid/widget/TextView;

    iget-object v1, p2, Lcku;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p2, Lcku;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcgi;->f:Landroid/widget/ImageView;

    iget-object v1, p2, Lcku;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v0, p0, Lcgi;->m:Lfun;

    iget-object v1, p0, Lcgi;->l:Lfsj;

    invoke-interface {v1}, Lfsj;->a()Landroid/view/View;

    move-result-object v1

    iget-object v4, p0, Lcgi;->j:Landroid/view/View;

    invoke-virtual {p2}, Lcku;->a()Lfkp;

    move-result-object v5

    invoke-virtual {v0, v1, v4, v5, p2}, Lfun;->a(Landroid/view/View;Landroid/view/View;Lfkp;Ljava/lang/Object;)V

    iget-wide v0, p2, Lcku;->f:D

    mul-double/2addr v0, v6

    double-to-int v1, v0

    iget-wide v4, p2, Lcku;->h:D

    mul-double/2addr v4, v6

    double-to-int v0, v4

    iget-object v4, p0, Lcgi;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcgi;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p2, Lcku;->b:Landroid/text/Spanned;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcgi;->d:Landroid/widget/TextView;

    iget-object v1, p2, Lcku;->b:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcgi;->e:Landroid/widget/TextView;

    iget-object v1, p2, Lcku;->c:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcgi;->g:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcgi;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_1
    iget-object v0, p2, Lcku;->d:Lhog;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcgi;->b:Landroid/view/View;

    new-instance v2, Lcgj;

    invoke-direct {v2, p0, v0}, Lcgj;-><init>(Lcgi;Lhog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcgi;->l:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcgi;->f:Landroid/widget/ImageView;

    const v1, 0x7f0201a5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_2
    iget-wide v4, p2, Lcku;->f:D

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v4, v6

    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    iget-wide v4, p2, Lcku;->h:D

    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-lez v0, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    iget-object v0, p0, Lcgi;->d:Landroid/widget/TextView;

    iget-object v1, p2, Lcku;->i:Landroid/text/Spanned;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcgi;->e:Landroid/widget/TextView;

    iget-object v0, p2, Lcku;->j:Landroid/text/Spanned;

    :goto_4
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcgi;->d:Landroid/widget/TextView;

    iget-object v4, p0, Lcgi;->k:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f10000e

    new-array v6, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcgi;->e:Landroid/widget/TextView;

    iget v0, p2, Lcku;->g:I

    div-int/lit8 v4, v0, 0x3c

    div-int/lit8 v5, v4, 0x3c

    if-le v0, v2, :cond_8

    if-gt v0, v9, :cond_6

    iget-object v4, p0, Lcgi;->k:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f10000f

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v4, v5, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_6
    if-gt v4, v9, :cond_7

    iget-object v0, p0, Lcgi;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f100010

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v0, v5, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_7
    const/4 v0, 0x3

    if-gt v5, v0, :cond_8

    iget-object v0, p0, Lcgi;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f100011

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v2, v3

    invoke-virtual {v0, v4, v5, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method
