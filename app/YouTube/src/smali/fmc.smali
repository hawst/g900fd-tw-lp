.class public Lfmc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhsl;

.field public final b:Lfqh;

.field public c:Ljava/util/List;

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>(Lhsl;Lfqh;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhsl;

    iput-object v0, p0, Lfmc;->a:Lhsl;

    .line 36
    iput-object p2, p0, Lfmc;->b:Lfqh;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 8

    .prologue
    .line 45
    iget-object v0, p0, Lfmc;->d:Ljava/util/List;

    if-nez v0, :cond_2

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lfmc;->a:Lhsl;

    iget-object v1, v1, Lhsl;->a:[Lhsn;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfmc;->d:Ljava/util/List;

    .line 47
    iget-object v0, p0, Lfmc;->a:Lhsl;

    iget-object v1, v0, Lhsl;->a:[Lhsn;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 48
    iget-object v4, v3, Lhsn;->b:Lhsp;

    if-eqz v4, :cond_1

    .line 49
    iget-object v4, p0, Lfmc;->d:Ljava/util/List;

    new-instance v5, Lfmb;

    iget-object v3, v3, Lhsn;->b:Lhsp;

    .line 51
    iget-object v6, p0, Lfmc;->a:Lhsl;

    iget-object v6, v6, Lhsl;->b:Ljava/lang/String;

    .line 52
    iget-object v7, p0, Lfmc;->a:Lhsl;

    iget-boolean v7, v7, Lhsl;->c:Z

    invoke-direct {v5, v3, v6, v7, p0}, Lfmb;-><init>(Lhsp;Ljava/lang/String;ZLfqh;)V

    .line 49
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_1
    iget-object v4, v3, Lhsn;->c:Lhbb;

    if-eqz v4, :cond_0

    .line 55
    iget-object v4, p0, Lfmc;->d:Ljava/util/List;

    new-instance v5, Lfip;

    iget-object v3, v3, Lhsn;->c:Lhbb;

    invoke-direct {v5, v3, p0}, Lfip;-><init>(Lhbb;Lfqh;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 59
    :cond_2
    iget-object v0, p0, Lfmc;->d:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lfau;)V
    .locals 3

    .prologue
    .line 107
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 108
    invoke-virtual {p0}, Lfmc;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 109
    instance-of v2, v0, Lfat;

    if-eqz v2, :cond_0

    .line 110
    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    goto :goto_0

    .line 113
    :cond_1
    return-void
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lfmc;->b:Lfqh;

    return-object v0
.end method
