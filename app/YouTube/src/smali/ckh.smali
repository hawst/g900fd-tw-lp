.class final Lckh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic a:Lckg;


# direct methods
.method constructor <init>(Lckg;)V
    .locals 0

    .prologue
    .line 784
    iput-object p1, p0, Lckh;->a:Lckg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 787
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 788
    iget-object v0, p0, Lckh;->a:Lckg;

    iget-object v0, v0, Lckg;->c:Lcjw;

    invoke-virtual {v0, p2}, Lcjw;->a(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 799
    :goto_0
    return-void

    .line 790
    :pswitch_0
    iget-object v0, p0, Lckh;->a:Lckg;

    iget-object v1, v0, Lckg;->a:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lckg;->e:Lcjs;

    iget-object v2, v2, Lcjs;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0902bf

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0902c0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0902c1

    new-instance v3, Lckj;

    invoke-direct {v3, v0}, Lckj;-><init>(Lckg;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0902c2

    new-instance v3, Lcki;

    invoke-direct {v3, v0}, Lcki;-><init>(Lckg;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, v0, Lckg;->a:Landroid/app/AlertDialog;

    :cond_0
    iget-object v1, v0, Lckg;->e:Lcjs;

    new-instance v2, Lckl;

    invoke-direct {v2, v0}, Lckl;-><init>(Lckg;)V

    invoke-static {v1, v2}, Lcjs;->a(Lcjs;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 793
    :pswitch_1
    iget-object v0, p0, Lckh;->a:Lckg;

    iget-object v1, v0, Lckg;->d:Lfwe;

    invoke-static {v1}, Lcjs;->a(Lfwe;)Lfwf;

    move-result-object v1

    iget v1, v1, Lfwf;->j:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v0, v0, Lckg;->e:Lcjs;

    const v1, 0x7f0902bd

    invoke-static {v0, v1}, Lcjs;->a(Lcjs;I)V

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lckg;->e:Lcjs;

    iget-object v1, v1, Lcjs;->k:Lckd;

    iget-object v0, v0, Lckg;->d:Lfwe;

    invoke-virtual {v1, v0}, Lckd;->a(Lfwe;)V

    goto :goto_0

    .line 796
    :pswitch_2
    iget-object v0, p0, Lckh;->a:Lckg;

    iget-object v1, v0, Lckg;->b:Landroid/app/AlertDialog;

    if-nez v1, :cond_2

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lckg;->e:Lcjs;

    iget-object v2, v2, Lcjs;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0902c5

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0902c6

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0902c7

    new-instance v3, Lckn;

    invoke-direct {v3, v0}, Lckn;-><init>(Lckg;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0902c8

    new-instance v3, Lckm;

    invoke-direct {v3, v0}, Lckm;-><init>(Lckg;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, v0, Lckg;->b:Landroid/app/AlertDialog;

    :cond_2
    iget-object v1, v0, Lckg;->e:Lcjs;

    new-instance v2, Lckp;

    invoke-direct {v2, v0}, Lckp;-><init>(Lckg;)V

    invoke-static {v1, v2}, Lcjs;->a(Lcjs;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 788
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
