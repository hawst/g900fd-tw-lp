.class public final Lhfw;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhgz;

.field public c:Lhog;

.field public d:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47251
    invoke-direct {p0}, Lidf;-><init>()V

    .line 47254
    iput-object v0, p0, Lhfw;->a:Lhgz;

    .line 47257
    iput-object v0, p0, Lhfw;->b:Lhgz;

    .line 47260
    iput-object v0, p0, Lhfw;->c:Lhog;

    .line 47263
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhfw;->d:[B

    .line 47251
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 47323
    const/4 v0, 0x0

    .line 47324
    iget-object v1, p0, Lhfw;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 47325
    const/4 v0, 0x1

    iget-object v1, p0, Lhfw;->a:Lhgz;

    .line 47326
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 47328
    :cond_0
    iget-object v1, p0, Lhfw;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 47329
    const/4 v1, 0x2

    iget-object v2, p0, Lhfw;->b:Lhgz;

    .line 47330
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47332
    :cond_1
    iget-object v1, p0, Lhfw;->c:Lhog;

    if-eqz v1, :cond_2

    .line 47333
    const/4 v1, 0x3

    iget-object v2, p0, Lhfw;->c:Lhog;

    .line 47334
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47336
    :cond_2
    iget-object v1, p0, Lhfw;->d:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 47337
    const/4 v1, 0x5

    iget-object v2, p0, Lhfw;->d:[B

    .line 47338
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 47340
    :cond_3
    iget-object v1, p0, Lhfw;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47341
    iput v0, p0, Lhfw;->J:I

    .line 47342
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 47247
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfw;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfw;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfw;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhfw;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhfw;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhfw;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhfw;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhfw;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhfw;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhfw;->c:Lhog;

    if-nez v0, :cond_4

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhfw;->c:Lhog;

    :cond_4
    iget-object v0, p0, Lhfw;->c:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhfw;->d:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 47305
    iget-object v0, p0, Lhfw;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 47306
    const/4 v0, 0x1

    iget-object v1, p0, Lhfw;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 47308
    :cond_0
    iget-object v0, p0, Lhfw;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 47309
    const/4 v0, 0x2

    iget-object v1, p0, Lhfw;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 47311
    :cond_1
    iget-object v0, p0, Lhfw;->c:Lhog;

    if-eqz v0, :cond_2

    .line 47312
    const/4 v0, 0x3

    iget-object v1, p0, Lhfw;->c:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 47314
    :cond_2
    iget-object v0, p0, Lhfw;->d:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 47315
    const/4 v0, 0x5

    iget-object v1, p0, Lhfw;->d:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 47317
    :cond_3
    iget-object v0, p0, Lhfw;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 47319
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 47277
    if-ne p1, p0, :cond_1

    .line 47284
    :cond_0
    :goto_0
    return v0

    .line 47278
    :cond_1
    instance-of v2, p1, Lhfw;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 47279
    :cond_2
    check-cast p1, Lhfw;

    .line 47280
    iget-object v2, p0, Lhfw;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfw;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhfw;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhfw;->b:Lhgz;

    if-nez v2, :cond_3

    .line 47281
    :goto_2
    iget-object v2, p0, Lhfw;->c:Lhog;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhfw;->c:Lhog;

    if-nez v2, :cond_3

    .line 47282
    :goto_3
    iget-object v2, p0, Lhfw;->d:[B

    iget-object v3, p1, Lhfw;->d:[B

    .line 47283
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhfw;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhfw;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 47284
    goto :goto_0

    .line 47280
    :cond_4
    iget-object v2, p0, Lhfw;->a:Lhgz;

    iget-object v3, p1, Lhfw;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhfw;->b:Lhgz;

    iget-object v3, p1, Lhfw;->b:Lhgz;

    .line 47281
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhfw;->c:Lhog;

    iget-object v3, p1, Lhfw;->c:Lhog;

    .line 47282
    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    .line 47283
    :cond_7
    iget-object v2, p0, Lhfw;->I:Ljava/util/List;

    iget-object v3, p1, Lhfw;->I:Ljava/util/List;

    .line 47284
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47288
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 47290
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfw;->a:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 47291
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfw;->b:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 47292
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfw;->c:Lhog;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 47293
    iget-object v2, p0, Lhfw;->d:[B

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    .line 47299
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhfw;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_3
    add-int/2addr v0, v1

    .line 47300
    return v0

    .line 47290
    :cond_1
    iget-object v0, p0, Lhfw;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 47291
    :cond_2
    iget-object v0, p0, Lhfw;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 47292
    :cond_3
    iget-object v0, p0, Lhfw;->c:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    move v2, v0

    move v0, v1

    .line 47295
    :goto_4
    iget-object v3, p0, Lhfw;->d:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 47296
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhfw;->d:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 47295
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 47299
    :cond_5
    iget-object v1, p0, Lhfw;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
