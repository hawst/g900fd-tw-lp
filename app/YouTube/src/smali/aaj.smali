.class public final Laaj;
.super Laah;
.source "SourceFile"


# instance fields
.field private b:J

.field private c:I

.field private d:I

.field private e:[I

.field private f:Laak;

.field private g:Laal;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Laah;-><init>(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [I

    iput-object v0, p0, Laaj;->e:[I

    .line 50
    new-instance v0, Laak;

    invoke-direct {v0}, Laak;-><init>()V

    iput-object v0, p0, Laaj;->f:Laak;

    .line 51
    new-instance v0, Laal;

    invoke-direct {v0}, Laal;-><init>()V

    iput-object v0, p0, Laaj;->g:Laal;

    .line 55
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 59
    invoke-virtual {p0, p1}, Laaj;->c(Ljava/nio/ByteBuffer;)V

    .line 60
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    iput-wide v0, p0, Laaj;->b:J

    .line 61
    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaj;->c:I

    .line 62
    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v0

    iput v0, p0, Laaj;->d:I

    .line 63
    new-array v0, v2, [I

    iput-object v0, p0, Laaj;->e:[I

    .line 64
    iget-object v0, p0, Laaj;->e:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    aput v1, v0, v3

    .line 65
    iget-object v0, p0, Laaj;->e:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    aput v1, v0, v4

    .line 66
    iget-object v0, p0, Laaj;->e:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    aput v1, v0, v5

    .line 67
    iget-object v0, p0, Laaj;->e:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    aput v1, v0, v6

    .line 68
    new-instance v0, Laak;

    invoke-direct {v0}, Laak;-><init>()V

    iput-object v0, p0, Laaj;->f:Laak;

    .line 69
    iget-object v0, p0, Laaj;->f:Laak;

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laak;->a:I

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laak;->b:I

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laak;->c:I

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laak;->d:I

    .line 71
    new-instance v0, Laal;

    invoke-direct {v0}, Laal;-><init>()V

    iput-object v0, p0, Laaj;->g:Laal;

    .line 72
    iget-object v0, p0, Laaj;->g:Laal;

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laal;->a:I

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laal;->b:I

    invoke-static {p1}, La;->c(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laal;->c:I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laal;->d:I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    iput v1, v0, Laal;->e:I

    new-array v1, v2, [I

    iput-object v1, v0, Laal;->f:[I

    iget-object v1, v0, Laal;->f:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v2

    aput v2, v1, v3

    iget-object v1, v0, Laal;->f:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v2

    aput v2, v1, v4

    iget-object v1, v0, Laal;->f:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v2

    aput v2, v1, v5

    iget-object v0, v0, Laal;->f:[I

    invoke-static {p1}, La;->d(Ljava/nio/ByteBuffer;)I

    move-result v1

    aput v1, v0, v6

    .line 73
    invoke-virtual {p0, p1}, Laaj;->d(Ljava/nio/ByteBuffer;)V

    .line 74
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    invoke-virtual {p0, p1}, Laaj;->e(Ljava/nio/ByteBuffer;)V

    .line 94
    iget-wide v0, p0, Laaj;->b:J

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 95
    iget v0, p0, Laaj;->c:I

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 96
    iget v0, p0, Laaj;->d:I

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 97
    iget-object v0, p0, Laaj;->e:[I

    aget v0, v0, v2

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 98
    iget-object v0, p0, Laaj;->e:[I

    aget v0, v0, v3

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 99
    iget-object v0, p0, Laaj;->e:[I

    aget v0, v0, v4

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 100
    iget-object v0, p0, Laaj;->e:[I

    aget v0, v0, v5

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 101
    iget-object v0, p0, Laaj;->f:Laak;

    iget v1, v0, Laak;->a:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    iget v1, v0, Laak;->b:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    iget v1, v0, Laak;->c:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    iget v0, v0, Laak;->d:I

    invoke-static {p1, v0}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    .line 102
    iget-object v0, p0, Laaj;->g:Laal;

    iget v1, v0, Laal;->a:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    iget v1, v0, Laal;->b:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    iget v1, v0, Laal;->c:I

    invoke-static {p1, v1}, Lyf;->b(Ljava/nio/ByteBuffer;I)V

    iget v1, v0, Laal;->d:I

    invoke-static {p1, v1}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    iget v1, v0, Laal;->e:I

    invoke-static {p1, v1}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    iget-object v1, v0, Laal;->f:[I

    aget v1, v1, v2

    invoke-static {p1, v1}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    iget-object v1, v0, Laal;->f:[I

    aget v1, v1, v3

    invoke-static {p1, v1}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    iget-object v1, v0, Laal;->f:[I

    aget v1, v1, v4

    invoke-static {p1, v1}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    iget-object v0, v0, Laal;->f:[I

    aget v0, v0, v5

    invoke-static {p1, v0}, Lyf;->c(Ljava/nio/ByteBuffer;I)V

    .line 104
    invoke-virtual {p0, p1}, Laaj;->f(Ljava/nio/ByteBuffer;)V

    .line 105
    return-void
.end method

.method protected final d_()J
    .locals 5

    .prologue
    .line 78
    iget-object v0, p0, Laaj;->f:Laak;

    .line 80
    iget-object v0, p0, Laaj;->g:Laal;

    const-wide/16 v0, 0x26

    .line 81
    iget-object v2, p0, Laaj;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lyj;

    .line 82
    invoke-interface {v0}, Lyj;->l_()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 83
    goto :goto_0

    .line 84
    :cond_0
    return-wide v2
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    const-string v0, "TextSampleEntry"

    return-object v0
.end method
