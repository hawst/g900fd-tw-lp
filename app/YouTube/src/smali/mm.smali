.class public final Lmm;
.super Lkm;
.source "SourceFile"


# instance fields
.field a:Landroid/support/v7/widget/Toolbar;

.field b:Lpv;

.field c:Z

.field public d:Lmt;

.field e:Landroid/view/Window;

.field f:Lnp;

.field final g:Ljava/lang/Runnable;

.field private h:Z

.field private i:Z

.field private j:Ljava/util/ArrayList;

.field private final k:Lun;


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/Toolbar;Ljava/lang/CharSequence;Landroid/view/Window;Lmt;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Lkm;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmm;->j:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Lmn;

    invoke-direct {v0, p0}, Lmn;-><init>(Lmm;)V

    iput-object v0, p0, Lmm;->g:Ljava/lang/Runnable;

    .line 73
    new-instance v0, Lmo;

    invoke-direct {v0, p0}, Lmo;-><init>(Lmm;)V

    iput-object v0, p0, Lmm;->k:Lun;

    .line 83
    iput-object p1, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    .line 84
    new-instance v0, Lrc;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lrc;-><init>(Landroid/support/v7/widget/Toolbar;Z)V

    iput-object v0, p0, Lmm;->b:Lpv;

    .line 85
    new-instance v0, Lms;

    invoke-direct {v0, p0, p4}, Lms;-><init>(Lmm;Lmt;)V

    iput-object v0, p0, Lmm;->d:Lmt;

    .line 86
    iget-object v0, p0, Lmm;->b:Lpv;

    iget-object v1, p0, Lmm;->d:Lmt;

    invoke-interface {v0, v1}, Lpv;->a(Lmt;)V

    .line 87
    iget-object v0, p0, Lmm;->k:Lun;

    iput-object v0, p1, Landroid/support/v7/widget/Toolbar;->j:Lun;

    .line 88
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0, p2}, Lpv;->a(Ljava/lang/CharSequence;)V

    .line 90
    iput-object p3, p0, Lmm;->e:Landroid/view/Window;

    .line 91
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0}, Lpv;->l()I

    move-result v0

    return v0
.end method

.method public final a(Ltz;)Lty;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lmm;->d:Lmt;

    invoke-interface {v0, p1}, Lmt;->a(Ltz;)Lty;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0, p1}, Lpv;->a(I)V

    .line 117
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0}, Lpv;->l()I

    move-result v0

    .line 269
    iget-object v1, p0, Lmm;->b:Lpv;

    and-int v2, p1, p2

    xor-int/lit8 v3, p2, -0x1

    and-int/2addr v0, v3

    or-int/2addr v0, v2

    invoke-interface {v1, v0}, Lpv;->b(I)V

    .line 270
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 201
    invoke-super {p0, p1}, Lkm;->a(Landroid/content/res/Configuration;)V

    .line 202
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 300
    return-void
.end method

.method public final a(Landroid/view/View;Lkn;)V
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 105
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0, p1}, Lpv;->a(Landroid/view/View;)V

    .line 106
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0, p1}, Lpv;->b(Ljava/lang/CharSequence;)V

    .line 239
    return-void
.end method

.method public final a(Lnp;)V
    .locals 3

    .prologue
    .line 558
    invoke-virtual {p0}, Lmm;->f()Landroid/view/Menu;

    move-result-object v0

    .line 560
    instance-of v1, v0, Lnr;

    if-eqz v1, :cond_1

    .line 561
    check-cast v0, Lnr;

    .line 563
    iget-object v1, p0, Lmm;->f:Lnp;

    if-eqz v1, :cond_0

    .line 565
    iget-object v1, p0, Lmm;->f:Lnp;

    const/4 v2, 0x0

    iput-object v2, v1, Lnp;->d:Loh;

    .line 566
    iget-object v1, p0, Lmm;->f:Lnp;

    invoke-virtual {v0, v1}, Lnr;->b(Log;)V

    .line 569
    :cond_0
    iput-object p1, p0, Lmm;->f:Lnp;

    .line 571
    if-eqz p1, :cond_1

    .line 572
    new-instance v1, Lmr;

    invoke-direct {v1, p0}, Lmr;-><init>(Lmm;)V

    iput-object v1, p1, Lnp;->d:Loh;

    .line 573
    invoke-virtual {v0, p1}, Lnr;->a(Log;)V

    .line 576
    :cond_1
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 274
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lmm;->a(II)V

    .line 275
    return-void
.end method

.method public final b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 243
    iget-object v1, p0, Lmm;->b:Lpv;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0}, Lpv;->b()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Lpv;->b(Ljava/lang/CharSequence;)V

    .line 244
    return-void

    .line 243
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/Toolbar;->a(Landroid/graphics/drawable/Drawable;)V

    .line 172
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0, p1}, Lpv;->a(Ljava/lang/CharSequence;)V

    .line 249
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 279
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lmm;->a(II)V

    .line 280
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lmm;->b:Lpv;

    invoke-interface {v0, p1}, Lpv;->d(I)V

    .line 192
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 284
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0, v1}, Lmm;->a(II)V

    .line 285
    return-void

    .line 284
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lmm;->g:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 438
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    iget-object v1, p0, Lmm;->g:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lfz;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 439
    const/4 v0, 0x1

    return v0
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 289
    const/4 v0, 0x0

    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lmm;->a(II)V

    .line 290
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0}, Landroid/support/v7/widget/Toolbar;->e()V

    .line 446
    const/4 v0, 0x1

    .line 448
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 452
    invoke-virtual {p0}, Lmm;->f()Landroid/view/Menu;

    move-result-object v1

    .line 453
    instance-of v2, v1, Lnr;

    if-eqz v2, :cond_4

    move-object v0, v1

    check-cast v0, Lnr;

    move-object v2, v0

    .line 454
    :goto_0
    if-eqz v2, :cond_0

    .line 455
    invoke-virtual {v2}, Lnr;->d()V

    .line 458
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 459
    iget-object v0, p0, Lmm;->d:Lmt;

    const/4 v3, 0x0

    invoke-interface {v0, v3, v1}, Lmt;->a(ILandroid/view/Menu;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmm;->d:Lmt;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4, v1}, Lmt;->a(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 461
    :cond_1
    invoke-interface {v1}, Landroid/view/Menu;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    :cond_2
    if-eqz v2, :cond_3

    .line 465
    invoke-virtual {v2}, Lnr;->e()V

    .line 468
    :cond_3
    return-void

    :cond_4
    move-object v2, v0

    .line 453
    goto :goto_0

    .line 464
    :catchall_0
    move-exception v0

    if-eqz v2, :cond_5

    .line 465
    invoke-virtual {v2}, Lnr;->e()V

    :cond_5
    throw v0
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method f()Landroid/view/Menu;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 550
    iget-boolean v0, p0, Lmm;->h:Z

    if-nez v0, :cond_0

    .line 551
    iget-object v0, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lmp;

    invoke-direct {v1, p0}, Lmp;-><init>(Lmm;)V

    new-instance v2, Lmq;

    invoke-direct {v2, p0}, Lmq;-><init>(Lmm;)V

    iput-object v1, v0, Landroid/support/v7/widget/Toolbar;->m:Loh;

    iput-object v2, v0, Landroid/support/v7/widget/Toolbar;->n:Lns;

    .line 552
    iput-boolean v3, p0, Lmm;->h:Z

    .line 554
    :cond_0
    iget-object v1, p0, Lmm;->a:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->g()V

    iget-object v0, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget-object v0, v0, Landroid/support/v7/widget/ActionMenuView;->a:Lnr;

    if-nez v0, :cond_2

    iget-object v0, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->b()Landroid/view/Menu;

    move-result-object v0

    check-cast v0, Lnr;

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    if-nez v2, :cond_1

    new-instance v2, Lvw;

    invoke-direct {v2, v1}, Lvw;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v2, v1, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    :cond_1
    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    iget-object v2, v2, Landroid/support/v7/widget/ActionMenuView;->c:Lub;

    iput-boolean v3, v2, Lub;->i:Z

    iget-object v2, v1, Landroid/support/v7/widget/Toolbar;->l:Lvw;

    iget-object v3, v1, Landroid/support/v7/widget/Toolbar;->e:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, Lnr;->a(Log;Landroid/content/Context;)V

    :cond_2
    iget-object v0, v1, Landroid/support/v7/widget/Toolbar;->a:Landroid/support/v7/widget/ActionMenuView;

    invoke-virtual {v0}, Landroid/support/v7/widget/ActionMenuView;->b()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 187
    return-void
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 197
    return-void
.end method

.method public final h(Z)V
    .locals 3

    .prologue
    .line 487
    iget-boolean v0, p0, Lmm;->i:Z

    if-ne p1, v0, :cond_1

    .line 496
    :cond_0
    return-void

    .line 490
    :cond_1
    iput-boolean p1, p0, Lmm;->i:Z

    .line 492
    iget-object v0, p0, Lmm;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 493
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 494
    iget-object v2, p0, Lmm;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 493
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
