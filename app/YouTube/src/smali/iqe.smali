.class final Liqe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lipt;


# instance fields
.field public final a:Lipq;

.field b:Z

.field private c:Liqk;


# direct methods
.method public constructor <init>(Liqk;)V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lipq;

    invoke-direct {v0}, Lipq;-><init>()V

    invoke-direct {p0, p1, v0}, Liqe;-><init>(Liqk;Lipq;)V

    .line 35
    return-void
.end method

.method private constructor <init>(Liqk;Lipq;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    iput-object p2, p0, Liqe;->a:Lipq;

    .line 30
    iput-object p1, p0, Liqe;->c:Liqk;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Liql;)J
    .locals 6

    .prologue
    .line 79
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    const-wide/16 v0, 0x0

    .line 81
    :goto_0
    iget-object v2, p0, Liqe;->a:Lipq;

    const-wide/16 v4, 0x800

    invoke-interface {p1, v2, v4, v5}, Liql;->b(Lipq;J)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 82
    add-long/2addr v0, v2

    .line 83
    invoke-virtual {p0}, Liqe;->p()Lipt;

    goto :goto_0

    .line 85
    :cond_1
    return-wide v0
.end method

.method public final a()Liqm;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Liqe;->c:Liqk;

    invoke-interface {v0}, Liqk;->a()Liqm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lipq;J)V
    .locals 2

    .prologue
    .line 43
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1, p2, p3}, Lipq;->a(Lipq;J)V

    .line 45
    invoke-virtual {p0}, Liqe;->p()Lipt;

    .line 46
    return-void
.end method

.method public final b(Lipv;)Lipt;
    .locals 2

    .prologue
    .line 49
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1}, Lipq;->a(Lipv;)Lipq;

    .line 51
    invoke-virtual {p0}, Liqe;->p()Lipt;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lipt;
    .locals 2

    .prologue
    .line 55
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1}, Lipq;->a(Ljava/lang/String;)Lipq;

    .line 57
    invoke-virtual {p0}, Liqe;->p()Lipt;

    move-result-object v0

    return-object v0
.end method

.method public final b([B)Lipt;
    .locals 2

    .prologue
    .line 67
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1}, Lipq;->a([B)Lipq;

    .line 69
    invoke-virtual {p0}, Liqe;->p()Lipt;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 169
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    iget-wide v0, v0, Lipq;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 171
    iget-object v0, p0, Liqe;->c:Liqk;

    iget-object v1, p0, Liqe;->a:Lipq;

    iget-object v2, p0, Liqe;->a:Lipq;

    iget-wide v2, v2, Lipq;->b:J

    invoke-interface {v0, v1, v2, v3}, Liqk;->a(Lipq;J)V

    .line 173
    :cond_1
    iget-object v0, p0, Liqe;->c:Liqk;

    invoke-interface {v0}, Liqk;->b()V

    .line 174
    return-void
.end method

.method public final c()Lipq;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Liqe;->a:Lipq;

    return-object v0
.end method

.method public final c([BII)Lipt;
    .locals 2

    .prologue
    .line 73
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1, p2, p3}, Lipq;->b([BII)Lipq;

    .line 75
    invoke-virtual {p0}, Liqe;->p()Lipt;

    move-result-object v0

    return-object v0
.end method

.method public final close()V
    .locals 6

    .prologue
    .line 177
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    const/4 v0, 0x0

    .line 183
    :try_start_0
    iget-object v1, p0, Liqe;->a:Lipq;

    iget-wide v2, v1, Lipq;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 184
    iget-object v1, p0, Liqe;->c:Liqk;

    iget-object v2, p0, Liqe;->a:Lipq;

    iget-object v3, p0, Liqe;->a:Lipq;

    iget-wide v4, v3, Lipq;->b:J

    invoke-interface {v1, v2, v4, v5}, Liqk;->a(Lipq;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 191
    :cond_2
    :goto_1
    :try_start_1
    iget-object v1, p0, Liqe;->c:Liqk;

    invoke-interface {v1}, Liqk;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 195
    :cond_3
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Liqe;->b:Z

    .line 197
    if-eqz v0, :cond_0

    invoke-static {v0}, Liqo;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 192
    :catch_0
    move-exception v1

    .line 193
    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_2

    .line 186
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final d()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 138
    new-instance v0, Liqf;

    invoke-direct {v0, p0}, Liqf;-><init>(Liqe;)V

    return-object v0
.end method

.method public final e(I)Lipt;
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1}, Lipq;->c(I)Lipq;

    .line 109
    invoke-virtual {p0}, Liqe;->p()Lipt;

    move-result-object v0

    return-object v0
.end method

.method public final f(I)Lipt;
    .locals 2

    .prologue
    .line 95
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1}, Lipq;->b(I)Lipq;

    .line 97
    invoke-virtual {p0}, Liqe;->p()Lipt;

    move-result-object v0

    return-object v0
.end method

.method public final g(I)Lipt;
    .locals 2

    .prologue
    .line 89
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    iget-object v0, p0, Liqe;->a:Lipq;

    invoke-virtual {v0, p1}, Lipq;->a(I)Lipq;

    .line 91
    invoke-virtual {p0}, Liqe;->p()Lipt;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lipt;
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 131
    iget-boolean v0, p0, Liqe;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :cond_0
    iget-object v4, p0, Liqe;->a:Lipq;

    iget-wide v0, v4, Lipq;->b:J

    cmp-long v5, v0, v2

    if-nez v5, :cond_3

    move-wide v0, v2

    .line 133
    :cond_1
    :goto_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    iget-object v2, p0, Liqe;->c:Liqk;

    iget-object v3, p0, Liqe;->a:Lipq;

    invoke-interface {v2, v3, v0, v1}, Liqk;->a(Lipq;J)V

    .line 134
    :cond_2
    return-object p0

    .line 132
    :cond_3
    iget-object v4, v4, Lipq;->a:Liqi;

    iget-object v4, v4, Liqi;->e:Liqi;

    iget v5, v4, Liqi;->c:I

    const/16 v6, 0x800

    if-ge v5, v6, :cond_1

    iget v5, v4, Liqi;->c:I

    iget v4, v4, Liqi;->b:I

    sub-int v4, v5, v4

    int-to-long v4, v4

    sub-long/2addr v0, v4

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "buffer("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Liqe;->c:Liqk;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
