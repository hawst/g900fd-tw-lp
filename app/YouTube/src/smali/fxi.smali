.class public final Lfxi;
.super Lgks;
.source "SourceFile"


# instance fields
.field private b:Lfap;

.field private c:Lfxh;

.field private final d:Lgiz;


# direct methods
.method public constructor <init>(Lewv;Lgir;Ljava/util/List;Lfap;Lfxh;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lgks;-><init>(Lewv;Lgir;Ljava/util/List;)V

    .line 61
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfap;

    iput-object v0, p0, Lfxi;->b:Lfap;

    .line 62
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    iput-object v0, p0, Lfxi;->c:Lfxh;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lfxi;->d:Lgiz;

    .line 64
    return-void
.end method

.method public constructor <init>(Lewv;Lgir;Ljava/util/List;Lfap;Lfxh;Lgiz;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3}, Lgks;-><init>(Lewv;Lgir;Ljava/util/List;)V

    .line 86
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfap;

    iput-object v0, p0, Lfxi;->b:Lfap;

    .line 87
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    iput-object v0, p0, Lfxi;->c:Lfxh;

    .line 88
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgiz;

    iput-object v0, p0, Lfxi;->d:Lgiz;

    .line 89
    return-void
.end method

.method public constructor <init>(Lewv;Ljava/lang/String;Lgir;Ljava/util/List;Lfap;Lfxh;Lgiz;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3, p4}, Lgks;-><init>(Lewv;Ljava/lang/String;Lgir;Ljava/util/List;)V

    .line 100
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfap;

    iput-object v0, p0, Lfxi;->b:Lfap;

    .line 101
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxh;

    iput-object v0, p0, Lfxi;->c:Lfxh;

    .line 102
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgiz;

    iput-object v0, p0, Lfxi;->d:Lgiz;

    .line 103
    return-void
.end method

.method private a(Lfxg;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3

    .prologue
    .line 107
    invoke-super {p0, p1}, Lgks;->b(Lgkr;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 109
    const-string v1, "GData-Version"

    iget-object v2, p0, Lfxi;->c:Lfxh;

    iget-object v2, v2, Lfxh;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lfxg;

    invoke-direct {p0, p1}, Lfxi;->a(Lfxg;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Lgkr;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4

    .prologue
    .line 23
    check-cast p1, Lfxg;

    iget-object v0, p0, Lfxi;->b:Lfap;

    if-nez v0, :cond_2

    iget-object v0, p1, Lfxg;->a:Landroid/net/Uri;

    :cond_0
    :goto_0
    iget-object v1, p0, Lfxi;->d:Lgiz;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfxi;->d:Lgiz;

    const-string v2, "on-behalf-of"

    iget-object v1, v1, Lgiz;->a:Lgix;

    invoke-interface {v1}, Lgix;->d()Lgit;

    move-result-object v1

    sget-object v3, Lgit;->a:Lgit;

    if-eq v1, v3, :cond_1

    iget-object v3, v1, Lgit;->b:Lgiv;

    invoke-virtual {v3}, Lgiv;->b()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, v1, Lgit;->b:Lgiv;

    invoke-virtual {v1}, Lgiv;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lfxi;->a:Lewv;

    invoke-virtual {v1, v0}, Lewv;->a(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0

    :cond_2
    iget-object v1, p0, Lfxi;->b:Lfap;

    iget-object v0, p1, Lfxg;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lfap;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic b(Lgkr;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1

    .prologue
    .line 23
    check-cast p1, Lfxg;

    invoke-direct {p0, p1}, Lfxi;->a(Lfxg;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
