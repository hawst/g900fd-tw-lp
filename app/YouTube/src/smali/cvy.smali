.class public final Lcvy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field a:Ljava/util/TreeSet;

.field private b:Ljava/util/TreeSet;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcvy;->a:Ljava/util/TreeSet;

    .line 18
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcvy;->b:Ljava/util/TreeSet;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(J)Ljava/util/Iterator;
    .locals 3

    .prologue
    .line 43
    iget-object v0, p0, Lcvy;->a:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Lcvv;->b(J)Lcvw;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->tailSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v0

    .line 44
    invoke-interface {v0}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a([Lcvv;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 22
    move v0, v1

    :goto_0
    if-gtz v0, :cond_0

    aget-object v2, p1, v1

    .line 23
    iget-object v3, p0, Lcvy;->b:Ljava/util/TreeSet;

    invoke-virtual {v3, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 24
    iget-object v3, p0, Lcvy;->a:Ljava/util/TreeSet;

    iget-object v4, v2, Lcvv;->e:Lcvw;

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 25
    iget-object v3, p0, Lcvy;->a:Ljava/util/TreeSet;

    iget-object v2, v2, Lcvv;->f:Lcvw;

    invoke-virtual {v3, v2}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 22
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    :cond_0
    return-void
.end method

.method public final varargs b([Lcvv;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 30
    move v0, v1

    :goto_0
    if-gtz v0, :cond_0

    aget-object v2, p1, v1

    .line 31
    iget-object v3, p0, Lcvy;->b:Ljava/util/TreeSet;

    invoke-virtual {v3, v2}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 32
    iget-object v3, p0, Lcvy;->a:Ljava/util/TreeSet;

    iget-object v4, v2, Lcvv;->e:Lcvw;

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 33
    iget-object v3, p0, Lcvy;->a:Ljava/util/TreeSet;

    iget-object v2, v2, Lcvv;->f:Lcvw;

    invoke-virtual {v3, v2}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 35
    :cond_0
    return-void
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcvy;->b:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
