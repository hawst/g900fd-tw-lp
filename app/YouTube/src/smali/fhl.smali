.class public final Lfhl;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/Map;

.field private static final c:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lfhm;

    invoke-direct {v0}, Lfhm;-><init>()V

    sput-object v0, Lfhl;->a:Ljava/util/Map;

    .line 43
    new-instance v0, Lfhn;

    invoke-direct {v0}, Lfhn;-><init>()V

    sput-object v0, Lfhl;->b:Ljava/util/Map;

    .line 65
    new-instance v0, Lfho;

    invoke-direct {v0}, Lfho;-><init>()V

    sput-object v0, Lfhl;->c:Ljava/util/Map;

    return-void
.end method

.method static synthetic a(Ljava/lang/String;Ljava/util/Map;I)I
    .locals 2

    .prologue
    .line 32
    const/4 v1, 0x0

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lfhl;->c:Ljava/util/Map;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lfbb;)V
    .locals 3

    .prologue
    .line 80
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/card"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lfhu;

    invoke-direct {v1}, Lfhu;-><init>()V

    invoke-virtual {p1, v0, v1}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/card/action"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfht;

    invoke-direct {v2}, Lfht;-><init>()V

    .line 97
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/card/action/event"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfhs;

    invoke-direct {v2}, Lfhs;-><init>()V

    .line 121
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/card/event"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfhr;

    invoke-direct {v2}, Lfhr;-><init>()V

    .line 136
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/card/app_card"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfhq;

    invoke-direct {v2}, Lfhq;-><init>()V

    .line 153
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/card/app_card/icon"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lfhp;

    invoke-direct {v2}, Lfhp;-><init>()V

    .line 180
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 198
    return-void
.end method

.method static synthetic b()Ljava/util/Map;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lfhl;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lfhl;->a:Ljava/util/Map;

    return-object v0
.end method
