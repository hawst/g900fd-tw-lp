.class final Ldnv;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# instance fields
.field private final a:Ljava/io/OutputStream;

.field private b:J

.field private synthetic c:Ldns;


# direct methods
.method public constructor <init>(Ldns;Ljava/io/OutputStream;J)V
    .locals 1

    .prologue
    .line 549
    iput-object p1, p0, Ldnv;->c:Ldns;

    .line 550
    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 551
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/OutputStream;

    iput-object v0, p0, Ldnv;->a:Ljava/io/OutputStream;

    .line 552
    iput-wide p3, p0, Ldnv;->b:J

    .line 553
    return-void
.end method

.method private a(JJ)V
    .locals 5

    .prologue
    const-wide/32 v2, 0x19000

    .line 571
    iget-object v0, p0, Ldnv;->c:Ldns;

    invoke-static {v0}, Ldns;->b(Ldns;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 572
    :try_start_0
    iget-object v0, p0, Ldnv;->c:Ldns;

    invoke-static {v0, p3, p4}, Ldns;->a(Ldns;J)J

    .line 573
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 577
    div-long v0, p3, v2

    div-long v2, p1, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Ldnv;->c:Ldns;

    .line 578
    invoke-static {v0}, Ldns;->c(Ldns;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    cmp-long v0, p3, v0

    if-nez v0, :cond_1

    .line 579
    :cond_0
    iget-object v0, p0, Ldnv;->c:Ldns;

    invoke-static {v0}, Ldns;->d(Ldns;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 580
    iget-object v0, p0, Ldnv;->c:Ldns;

    invoke-static {v0}, Ldns;->f(Ldns;)Ldnb;

    move-result-object v0

    iget-object v1, p0, Ldnv;->c:Ldns;

    invoke-static {v1}, Ldns;->e(Ldns;)Lgjm;

    move-result-object v1

    iget-object v1, v1, Lgjm;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p3, p4}, Ldnb;->b(Ljava/lang/String;J)V

    .line 583
    :cond_1
    return-void

    .line 573
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public final write(I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x1

    .line 565
    iget-object v0, p0, Ldnv;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 566
    iget-wide v0, p0, Ldnv;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldnv;->b:J

    .line 567
    iget-wide v0, p0, Ldnv;->b:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Ldnv;->b:J

    invoke-direct {p0, v0, v1, v2, v3}, Ldnv;->a(JJ)V

    .line 568
    return-void
.end method

.method public final write([BII)V
    .locals 6

    .prologue
    .line 557
    iget-object v0, p0, Ldnv;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 558
    iget-wide v0, p0, Ldnv;->b:J

    .line 559
    iget-wide v2, p0, Ldnv;->b:J

    int-to-long v4, p3

    add-long/2addr v2, v4

    iput-wide v2, p0, Ldnv;->b:J

    .line 560
    iget-wide v2, p0, Ldnv;->b:J

    invoke-direct {p0, v0, v1, v2, v3}, Ldnv;->a(JJ)V

    .line 561
    return-void
.end method
