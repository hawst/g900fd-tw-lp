.class final Lihs;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Lihq;

.field private final c:Lihr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lihs;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lihq;

    invoke-direct {v0}, Lihq;-><init>()V

    iput-object v0, p0, Lihs;->b:Lihq;

    .line 29
    new-instance v0, Lihr;

    invoke-direct {v0}, Lihr;-><init>()V

    iput-object v0, p0, Lihs;->c:Lihr;

    return-void
.end method


# virtual methods
.method final a(ILifw;I)Liez;
    .locals 11

    .prologue
    .line 32
    const/4 v0, 0x0

    sget-object v1, Lihs;->a:[I

    invoke-static {p2, p3, v0, v1}, Liht;->a(Lifw;IZ[I)[I

    move-result-object v5

    .line 34
    :try_start_0
    iget-object v0, p0, Lihs;->c:Lihr;

    iget-object v6, v0, Lihr;->c:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v7, v0, Lihr;->b:[I

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v7, v0

    const/4 v0, 0x1

    const/4 v1, 0x0

    aput v1, v7, v0

    const/4 v0, 0x2

    const/4 v1, 0x0

    aput v1, v7, v0

    const/4 v0, 0x3

    const/4 v1, 0x0

    aput v1, v7, v0

    iget v8, p2, Lifw;->b:I

    const/4 v0, 0x1

    aget v3, v5, v0

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    const/4 v0, 0x5

    if-ge v4, v0, :cond_3

    if-ge v3, v8, :cond_3

    sget-object v0, Liht;->c:[[I

    invoke-static {p2, v7, v3, v0}, Liht;->a(Lifw;[II[[I)I

    move-result v9

    rem-int/lit8 v0, v9, 0xa

    add-int/lit8 v0, v0, 0x30

    int-to-char v0, v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    move v2, v0

    move v0, v3

    :goto_1
    const/4 v3, 0x4

    if-ge v2, v3, :cond_0

    aget v3, v7, v2

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_0
    const/16 v2, 0xa

    if-lt v9, v2, :cond_1

    const/4 v2, 0x1

    rsub-int/lit8 v3, v4, 0x4

    shl-int/2addr v2, v3

    or-int/2addr v1, v2

    :cond_1
    const/4 v2, 0x4

    if-eq v4, v2, :cond_2

    invoke-virtual {p2, v0}, Lifw;->c(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lifw;->d(I)I

    move-result v0

    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_5

    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Liey; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :catch_0
    move-exception v0

    iget-object v0, p0, Lihs;->b:Lihq;

    invoke-virtual {v0, p1, p2, v5}, Lihq;->a(ILifw;[I)Liez;

    move-result-object v0

    :cond_4
    :goto_2
    return-object v0

    .line 34
    :cond_5
    const/4 v0, 0x0

    move v2, v0

    :goto_3
    const/16 v0, 0xa

    if-ge v2, v0, :cond_7

    :try_start_1
    sget-object v0, Lihr;->a:[I

    aget v0, v0, v2

    if-ne v1, v0, :cond_6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v7

    const/4 v1, 0x0

    add-int/lit8 v0, v7, -0x2

    :goto_4
    if-ltz v0, :cond_8

    invoke-interface {v4, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v8

    add-int/lit8 v8, v8, -0x30

    add-int/2addr v1, v8

    add-int/lit8 v0, v0, -0x2

    goto :goto_4

    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_7
    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_8
    mul-int/lit8 v1, v1, 0x3

    add-int/lit8 v0, v7, -0x1

    :goto_5
    if-ltz v0, :cond_9

    invoke-interface {v4, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v7

    add-int/lit8 v7, v7, -0x30

    add-int/2addr v1, v7

    add-int/lit8 v0, v0, -0x2

    goto :goto_5

    :cond_9
    mul-int/lit8 v0, v1, 0x3

    rem-int/lit8 v0, v0, 0xa

    if-eq v0, v2, :cond_a

    invoke-static {}, Liew;->a()Liew;

    move-result-object v0

    throw v0

    :cond_a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_b

    const/4 v0, 0x0

    move-object v1, v0

    :goto_6
    new-instance v0, Liez;

    const/4 v4, 0x0

    const/4 v6, 0x2

    new-array v6, v6, [Lifb;

    const/4 v7, 0x0

    new-instance v8, Lifb;

    const/4 v9, 0x0

    aget v9, v5, v9

    const/4 v10, 0x1

    aget v10, v5, v10

    add-int/2addr v9, v10

    int-to-float v9, v9

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    int-to-float v10, p1

    invoke-direct {v8, v9, v10}, Lifb;-><init>(FF)V

    aput-object v8, v6, v7

    const/4 v7, 0x1

    new-instance v8, Lifb;

    int-to-float v3, v3

    int-to-float v9, p1

    invoke-direct {v8, v3, v9}, Lifb;-><init>(FF)V

    aput-object v8, v6, v7

    sget-object v3, Lieo;->q:Lieo;

    invoke-direct {v0, v2, v4, v6, v3}, Liez;-><init>(Ljava/lang/String;[B[Lifb;Lieo;)V

    if-eqz v1, :cond_4

    invoke-virtual {v0, v1}, Liez;->a(Ljava/util/Map;)V

    goto/16 :goto_2

    :cond_b
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v0, ""

    move-object v1, v0

    :goto_7
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    div-int/lit8 v4, v0, 0x64

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    rem-int/lit8 v0, v0, 0x64

    const/16 v6, 0xa

    if-ge v0, v6, :cond_f

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "0"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_8
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v4, 0x2e

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_9
    if-nez v1, :cond_10

    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_6

    :sswitch_0
    const-string v0, "\u00a3"

    move-object v1, v0

    goto :goto_7

    :sswitch_1
    const-string v0, "$"

    move-object v1, v0

    goto :goto_7

    :sswitch_2
    const-string v0, "90000"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_9

    :cond_c
    const-string v0, "99991"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "0.00"

    move-object v1, v0

    goto :goto_9

    :cond_d
    const-string v0, "99990"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "Used"

    move-object v1, v0

    goto :goto_9

    :cond_e
    const-string v0, ""

    move-object v1, v0

    goto :goto_7

    :cond_f
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    :cond_10
    new-instance v0, Ljava/util/EnumMap;

    const-class v4, Lifa;

    invoke-direct {v0, v4}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sget-object v4, Lifa;->e:Lifa;

    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Liey; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v0

    goto/16 :goto_6

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x35 -> :sswitch_1
        0x39 -> :sswitch_2
    .end sparse-switch
.end method
