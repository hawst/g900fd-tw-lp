.class public final Lgsc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgsa;
.implements Lgsb;


# instance fields
.field private final a:Lgrw;

.field private final b:Ljava/util/List;

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lgrw;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lgsc;->b:Ljava/util/List;

    .line 29
    invoke-static {}, La;->J()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lgsc;->c:Ljava/util/List;

    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrw;

    iput-object v0, p0, Lgsc;->a:Lgrw;

    .line 40
    return-void
.end method

.method private a(J)J
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lgsc;->a:Lgrw;

    invoke-interface {v0}, Lgrw;->a()Lgqo;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lgqo;->b(J)Ljava/lang/Long;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 191
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private a(JJLjava/util/Map;)V
    .locals 9

    .prologue
    .line 168
    move-wide v2, p1

    :goto_0
    cmp-long v0, v2, p3

    if-gez v0, :cond_1

    .line 169
    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lgsc;->b(J)J

    move-result-wide p1

    .line 171
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p5, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrp;

    .line 172
    if-nez v0, :cond_0

    .line 173
    const-string v0, "Subsequence: %d - %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-wide/16 v6, 0x3e8

    div-long v6, v2, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-wide/16 v6, 0x3e8

    div-long v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 174
    iget-object v1, p0, Lgsc;->a:Lgrw;

    const-wide/16 v4, 0x1

    sub-long v4, p1, v4

    invoke-interface/range {v1 .. v6}, Lgrw;->a(JJLjava/lang/String;)Lgrp;

    move-result-object v0

    .line 175
    invoke-virtual {v0, p0}, Lgrp;->a(Lgsb;)V

    .line 178
    :cond_0
    iget-object v1, p0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v2, p1

    .line 180
    goto :goto_0

    .line 181
    :cond_1
    return-void
.end method

.method private b(J)J
    .locals 3

    .prologue
    .line 199
    iget-object v0, p0, Lgsc;->a:Lgrw;

    invoke-interface {v0}, Lgrw;->a()Lgqo;

    move-result-object v0

    iget-object v0, v0, Lgqo;->e:Ljava/util/TreeSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->ceiling(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 200
    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 203
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lgsc;->a:Lgrw;

    invoke-interface {v0}, Lgrw;->a()Lgqo;

    move-result-object v0

    iget-wide v0, v0, Lgqo;->d:J

    goto :goto_0
.end method

.method private d(J)Lgrs;
    .locals 9

    .prologue
    .line 216
    const/4 v0, 0x0

    .line 217
    iget-object v1, p0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrp;

    .line 218
    const/4 v3, 0x0

    invoke-virtual {v0, p1, p2, v3}, Lgrp;->b(JZ)Lgrs;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_2

    .line 220
    if-nez v1, :cond_0

    move-object v1, v0

    .line 221
    goto :goto_0

    .line 223
    :cond_0
    iget-object v3, v0, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    iget-object v3, v1, Lgrs;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long/2addr v6, p1

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gez v3, :cond_2

    .line 224
    invoke-virtual {v1}, Lgrs;->c()V

    :goto_1
    move-object v1, v0

    .line 229
    goto :goto_0

    .line 230
    :cond_1
    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrp;

    .line 109
    invoke-virtual {v0}, Lgrp;->a()V

    goto :goto_0

    .line 111
    :cond_0
    iget-object v0, p0, Lgsc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 112
    return-void
.end method

.method public final a(JJJ)V
    .locals 21

    .prologue
    .line 54
    const-wide/32 v18, 0xf4240

    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgrp;

    iget-wide v6, v4, Lgrp;->d:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v10, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    invoke-direct/range {p0 .. p2}, Lgsc;->a(J)J

    move-result-wide v6

    move-object/from16 v0, p0

    move-wide/from16 v1, p3

    invoke-direct {v0, v1, v2}, Lgsc;->b(J)J

    move-result-wide v8

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v10}, Lgsc;->a(JJLjava/util/Map;)V

    sub-long v4, p1, v18

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    sub-long v4, p1, v18

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lgsc;->a(J)J

    move-result-wide v12

    move-object/from16 v11, p0

    move-wide v14, v6

    move-object/from16 v16, v10

    invoke-direct/range {v11 .. v16}, Lgsc;->a(JJLjava/util/Map;)V

    :cond_1
    add-long v4, p3, v18

    cmp-long v4, v4, v8

    if-lez v4, :cond_2

    add-long v4, p3, v18

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lgsc;->b(J)J

    move-result-wide v14

    move-object/from16 v11, p0

    move-wide v12, v8

    move-object/from16 v16, v10

    invoke-direct/range {v11 .. v16}, Lgsc;->a(JJLjava/util/Map;)V

    :cond_2
    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lgrp;

    invoke-virtual {v4}, Lgrp;->a()V

    goto :goto_1

    .line 55
    :cond_3
    return-void
.end method

.method public final a(Lgsa;Lgrs;)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lgsc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    .line 119
    invoke-interface {v0, p0, p2}, Lgsb;->a(Lgsa;Lgrs;)V

    goto :goto_0

    .line 121
    :cond_0
    return-void
.end method

.method public final a(Lgsb;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lgsc;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-virtual {p0}, Lgsc;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-interface {p1, p0}, Lgsb;->b(Lgsa;)V

    .line 99
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lgsc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    .line 135
    invoke-interface {v0, p1}, Lgsb;->a(Ljava/lang/Exception;)V

    goto :goto_0

    .line 137
    :cond_0
    return-void
.end method

.method public final a(JJ)Z
    .locals 3

    .prologue
    .line 85
    iget-object v0, p0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrp;

    .line 86
    invoke-virtual {v0, p1, p2, p3, p4}, Lgrp;->a(JJ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 90
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(JZ)Lgrs;
    .locals 5

    .prologue
    .line 66
    if-eqz p3, :cond_2

    .line 67
    iget-object v0, p0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrp;

    iget-wide v2, v0, Lgrp;->d:J

    cmp-long v2, v2, p1

    if-gtz v2, :cond_0

    iget-wide v2, v0, Lgrp;->e:J

    cmp-long v2, v2, p1

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lgrp;->b(JZ)Lgrs;

    move-result-object v0

    .line 69
    :goto_0
    return-object v0

    .line 67
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_2
    invoke-direct {p0, p1, p2}, Lgsc;->d(J)Lgrs;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lgsa;)V
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lgsc;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lgsc;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgsb;

    .line 127
    invoke-interface {v0, p0}, Lgsb;->b(Lgsa;)V

    goto :goto_0

    .line 130
    :cond_0
    return-void
.end method

.method public final b(Lgsb;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lgsc;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 104
    return-void
.end method

.method public final c(J)Lgrs;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lgsc;->d(J)Lgrs;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lgsc;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrp;

    .line 76
    iget-object v0, v0, Lgrn;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
