.class public final Lgpu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfgo;


# instance fields
.field final a:Ljava/util/List;

.field private final b:Lfhw;


# direct methods
.method public constructor <init>(Lfhw;Ljava/util/concurrent/Executor;Landroid/content/pm/PackageManager;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhw;

    iput-object v0, p0, Lgpu;->b:Lfhw;

    .line 33
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgpu;->a:Ljava/util/List;

    .line 37
    new-instance v0, Lgpv;

    invoke-direct {v0, p0, p3}, Lgpv;-><init>(Lgpu;Landroid/content/pm/PackageManager;)V

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Lfgn;)V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lgpu;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    iget-object v1, v0, Lfji;->d:Lfil;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lfji;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lfji;->a:Lheg;

    iget-object v1, v1, Lheg;->b:Lhhi;

    iget-object v1, v1, Lhhi;->b:Lhab;

    if-eqz v1, :cond_0

    new-instance v1, Lfil;

    iget-object v2, v0, Lfji;->a:Lheg;

    iget-object v2, v2, Lheg;->b:Lhhi;

    iget-object v2, v2, Lhhi;->b:Lhab;

    invoke-direct {v1, v2}, Lfil;-><init>(Lhab;)V

    iput-object v1, v0, Lfji;->d:Lfil;

    :cond_0
    iget-object v1, v0, Lfji;->d:Lfil;

    .line 54
    if-nez v1, :cond_1

    .line 66
    :goto_0
    return-void

    .line 58
    :cond_1
    monitor-enter p0

    .line 59
    :try_start_0
    iget-object v0, p0, Lgpu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 60
    iget-object v3, v1, Lfil;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 64
    iget-object v3, v1, Lfil;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v0, 0x0

    .line 63
    :goto_2
    iget-object v3, p1, Lfgn;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 64
    :cond_3
    :try_start_1
    iget-object v3, v1, Lfil;->a:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    .line 66
    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
