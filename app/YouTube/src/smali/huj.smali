.class public final Lhuj;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhtx;

.field public b:Lhuk;

.field public c:Lhel;

.field public d:[B

.field private e:J

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v2, p0, Lhuj;->a:Lhtx;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhuj;->e:J

    const-string v0, ""

    iput-object v0, p0, Lhuj;->f:Ljava/lang/String;

    iput-object v2, p0, Lhuj;->b:Lhuk;

    const-string v0, ""

    iput-object v0, p0, Lhuj;->g:Ljava/lang/String;

    iput-object v2, p0, Lhuj;->c:Lhel;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhuj;->d:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhuj;->a:Lhtx;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhuj;->a:Lhtx;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-wide v2, p0, Lhuj;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget-wide v2, p0, Lhuj;->e:J

    invoke-static {v1, v2, v3}, Lidd;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lhuj;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhuj;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhuj;->b:Lhuk;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Lhuj;->b:Lhuk;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhuj;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhuj;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhuj;->c:Lhel;

    if-eqz v1, :cond_5

    const/4 v1, 0x7

    iget-object v2, p0, Lhuj;->c:Lhel;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhuj;->d:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v1, 0x9

    iget-object v2, p0, Lhuj;->d:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhuj;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhuj;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhuj;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhuj;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhuj;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhuj;->a:Lhtx;

    if-nez v0, :cond_2

    new-instance v0, Lhtx;

    invoke-direct {v0}, Lhtx;-><init>()V

    iput-object v0, p0, Lhuj;->a:Lhtx;

    :cond_2
    iget-object v0, p0, Lhuj;->a:Lhtx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhuj;->e:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhuj;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhuj;->b:Lhuk;

    if-nez v0, :cond_3

    new-instance v0, Lhuk;

    invoke-direct {v0}, Lhuk;-><init>()V

    iput-object v0, p0, Lhuj;->b:Lhuk;

    :cond_3
    iget-object v0, p0, Lhuj;->b:Lhuk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhuj;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhuj;->c:Lhel;

    if-nez v0, :cond_4

    new-instance v0, Lhel;

    invoke-direct {v0}, Lhel;-><init>()V

    iput-object v0, p0, Lhuj;->c:Lhel;

    :cond_4
    iget-object v0, p0, Lhuj;->c:Lhel;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhuj;->d:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x4a -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 4

    iget-object v0, p0, Lhuj;->a:Lhtx;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhuj;->a:Lhtx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-wide v0, p0, Lhuj;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-wide v2, p0, Lhuj;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    :cond_1
    iget-object v0, p0, Lhuj;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhuj;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lhuj;->b:Lhuk;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lhuj;->b:Lhuk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhuj;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhuj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lhuj;->c:Lhel;

    if-eqz v0, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Lhuj;->c:Lhel;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v0, p0, Lhuj;->d:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x9

    iget-object v1, p0, Lhuj;->d:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_6
    iget-object v0, p0, Lhuj;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhuj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhuj;

    iget-object v2, p0, Lhuj;->a:Lhtx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhuj;->a:Lhtx;

    if-nez v2, :cond_3

    :goto_1
    iget-wide v2, p0, Lhuj;->e:J

    iget-wide v4, p1, Lhuj;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhuj;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhuj;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhuj;->b:Lhuk;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhuj;->b:Lhuk;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhuj;->g:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhuj;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhuj;->c:Lhel;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhuj;->c:Lhel;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhuj;->d:[B

    iget-object v3, p1, Lhuj;->d:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhuj;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhuj;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhuj;->a:Lhtx;

    iget-object v3, p1, Lhuj;->a:Lhtx;

    invoke-virtual {v2, v3}, Lhtx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhuj;->f:Ljava/lang/String;

    iget-object v3, p1, Lhuj;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhuj;->b:Lhuk;

    iget-object v3, p1, Lhuj;->b:Lhuk;

    invoke-virtual {v2, v3}, Lhuk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhuj;->g:Ljava/lang/String;

    iget-object v3, p1, Lhuj;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhuj;->c:Lhel;

    iget-object v3, p1, Lhuj;->c:Lhel;

    invoke-virtual {v2, v3}, Lhel;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhuj;->I:Ljava/util/List;

    iget-object v3, p1, Lhuj;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhuj;->a:Lhtx;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhuj;->e:J

    iget-wide v4, p0, Lhuj;->e:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhuj;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhuj;->b:Lhuk;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhuj;->g:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhuj;->c:Lhel;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    iget-object v2, p0, Lhuj;->d:[B

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhuj;->I:Ljava/util/List;

    if-nez v2, :cond_7

    :goto_5
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhuj;->a:Lhtx;

    invoke-virtual {v0}, Lhtx;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhuj;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lhuj;->b:Lhuk;

    invoke-virtual {v0}, Lhuk;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lhuj;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lhuj;->c:Lhel;

    invoke-virtual {v0}, Lhel;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_6
    move v2, v0

    move v0, v1

    :goto_6
    iget-object v3, p0, Lhuj;->d:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhuj;->d:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_7
    iget-object v1, p0, Lhuj;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
