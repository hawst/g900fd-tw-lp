.class public final Lfis;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhbp;

.field private b:Ljava/util/List;

.field private c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lhbp;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lfis;->a:Lhbp;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 6

    .prologue
    .line 59
    iget-object v0, p0, Lfis;->b:Ljava/util/List;

    if-nez v0, :cond_3

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfis;->b:Ljava/util/List;

    .line 61
    iget-object v0, p0, Lfis;->a:Lhbp;

    iget-object v0, v0, Lhbp;->b:Lhbq;

    .line 62
    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lfis;->b:Ljava/util/List;

    .line 75
    :goto_0
    return-object v0

    .line 65
    :cond_0
    iget-object v0, v0, Lhbq;->b:Lhwa;

    .line 66
    if-nez v0, :cond_1

    .line 67
    iget-object v0, p0, Lfis;->b:Ljava/util/List;

    goto :goto_0

    .line 69
    :cond_1
    iget-object v1, v0, Lhwa;->a:[Lhbr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 70
    iget-object v4, v3, Lhbr;->b:Lhxa;

    if-eqz v4, :cond_2

    .line 71
    iget-object v4, p0, Lfis;->b:Ljava/util/List;

    new-instance v5, Lfmz;

    iget-object v3, v3, Lhbr;->b:Lhxa;

    invoke-direct {v5, v3}, Lfmz;-><init>(Lhxa;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 75
    :cond_3
    iget-object v0, p0, Lfis;->b:Ljava/util/List;

    goto :goto_0
.end method

.method public final a(Lfau;)V
    .locals 2

    .prologue
    .line 152
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 153
    invoke-virtual {p0}, Lfis;->b()Ljava/lang/Object;

    move-result-object v0

    .line 154
    instance-of v1, v0, Lfat;

    if-eqz v1, :cond_0

    .line 155
    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    .line 157
    :cond_0
    iget-object v0, p0, Lfis;->a:Lhbp;

    iget-object v1, v0, Lhbp;->b:Lhbq;

    if-eqz v1, :cond_2

    iget-object v0, v1, Lhbq;->a:Lhul;

    if-eqz v0, :cond_2

    new-instance v0, Lfmi;

    iget-object v1, v1, Lhbq;->a:Lhul;

    invoke-direct {v0, v1}, Lfmi;-><init>(Lhul;)V

    .line 158
    :goto_0
    if-eqz v0, :cond_1

    .line 159
    invoke-virtual {v0, p1}, Lfmi;->a(Lfau;)V

    .line 161
    :cond_1
    invoke-virtual {p0}, Lfis;->a()Ljava/util/List;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfmz;

    .line 163
    invoke-virtual {v0, p1}, Lfmz;->a(Lfau;)V

    goto :goto_1

    .line 157
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 165
    :cond_3
    return-void
.end method

.method public final b()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lfis;->c:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfis;->a:Lhbp;

    iget-object v0, v0, Lhbp;->d:Lhbn;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lfis;->a:Lhbp;

    iget-object v0, v0, Lhbp;->d:Lhbn;

    .line 107
    iget-object v1, v0, Lhbn;->a:Lhbu;

    if-eqz v1, :cond_1

    .line 108
    new-instance v1, Lfiu;

    iget-object v0, v0, Lhbn;->a:Lhbu;

    invoke-direct {v1, v0}, Lfiu;-><init>(Lhbu;)V

    iput-object v1, p0, Lfis;->c:Ljava/lang/Object;

    .line 115
    :cond_0
    :goto_0
    iget-object v0, p0, Lfis;->c:Ljava/lang/Object;

    return-object v0

    .line 109
    :cond_1
    iget-object v1, v0, Lhbn;->b:Lhgp;

    if-eqz v1, :cond_2

    .line 110
    new-instance v1, Lfjw;

    iget-object v0, v0, Lhbn;->b:Lhgp;

    invoke-direct {v1, v0}, Lfjw;-><init>(Lhgp;)V

    iput-object v1, p0, Lfis;->c:Ljava/lang/Object;

    goto :goto_0

    .line 111
    :cond_2
    iget-object v1, v0, Lhbn;->c:Lhsb;

    if-eqz v1, :cond_0

    .line 112
    new-instance v1, Lflv;

    iget-object v0, v0, Lhbn;->c:Lhsb;

    invoke-direct {v1, v0}, Lflv;-><init>(Lhsb;)V

    iput-object v1, p0, Lfis;->c:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lfis;->a:Lhbp;

    iget-object v0, v0, Lhbp;->e:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    return-object v0
.end method
