.class public final Lhka;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:[J

.field public f:[J

.field public g:[J

.field public h:[J

.field public i:[J

.field public j:[J

.field public k:I

.field private l:J


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-boolean v2, p0, Lhka;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lhka;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lhka;->c:Ljava/lang/String;

    iput-boolean v2, p0, Lhka;->d:Z

    sget-object v0, Lidj;->b:[J

    iput-object v0, p0, Lhka;->e:[J

    sget-object v0, Lidj;->b:[J

    iput-object v0, p0, Lhka;->f:[J

    sget-object v0, Lidj;->b:[J

    iput-object v0, p0, Lhka;->g:[J

    sget-object v0, Lidj;->b:[J

    iput-object v0, p0, Lhka;->h:[J

    sget-object v0, Lidj;->b:[J

    iput-object v0, p0, Lhka;->i:[J

    sget-object v0, Lidj;->b:[J

    iput-object v0, p0, Lhka;->j:[J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhka;->l:J

    iput v2, p0, Lhka;->k:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lhka;->a:Z

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    iget-boolean v2, p0, Lhka;->a:Z

    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhka;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhka;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhka;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhka;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-boolean v2, p0, Lhka;->d:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lhka;->d:Z

    invoke-static {v2}, Lidd;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhka;->e:[J

    if-eqz v2, :cond_4

    iget-object v2, p0, Lhka;->e:[J

    array-length v2, v2

    if-lez v2, :cond_4

    iget-object v4, p0, Lhka;->e:[J

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-wide v6, v4, v2

    invoke-static {v6, v7}, Lidd;->a(J)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    add-int/2addr v0, v3

    iget-object v2, p0, Lhka;->e:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhka;->f:[J

    if-eqz v2, :cond_6

    iget-object v2, p0, Lhka;->f:[J

    array-length v2, v2

    if-lez v2, :cond_6

    iget-object v4, p0, Lhka;->f:[J

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_2
    if-ge v2, v5, :cond_5

    aget-wide v6, v4, v2

    invoke-static {v6, v7}, Lidd;->a(J)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    add-int/2addr v0, v3

    iget-object v2, p0, Lhka;->f:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_6
    iget-object v2, p0, Lhka;->g:[J

    if-eqz v2, :cond_8

    iget-object v2, p0, Lhka;->g:[J

    array-length v2, v2

    if-lez v2, :cond_8

    iget-object v4, p0, Lhka;->g:[J

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_3
    if-ge v2, v5, :cond_7

    aget-wide v6, v4, v2

    invoke-static {v6, v7}, Lidd;->a(J)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    add-int/2addr v0, v3

    iget-object v2, p0, Lhka;->g:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_8
    iget-object v2, p0, Lhka;->h:[J

    if-eqz v2, :cond_a

    iget-object v2, p0, Lhka;->h:[J

    array-length v2, v2

    if-lez v2, :cond_a

    iget-object v4, p0, Lhka;->h:[J

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_4
    if-ge v2, v5, :cond_9

    aget-wide v6, v4, v2

    invoke-static {v6, v7}, Lidd;->a(J)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    add-int/2addr v0, v3

    iget-object v2, p0, Lhka;->h:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_a
    iget-object v2, p0, Lhka;->i:[J

    if-eqz v2, :cond_c

    iget-object v2, p0, Lhka;->i:[J

    array-length v2, v2

    if-lez v2, :cond_c

    iget-object v4, p0, Lhka;->i:[J

    array-length v5, v4

    move v2, v1

    move v3, v1

    :goto_5
    if-ge v2, v5, :cond_b

    aget-wide v6, v4, v2

    invoke-static {v6, v7}, Lidd;->a(J)I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    add-int/2addr v0, v3

    iget-object v2, p0, Lhka;->i:[J

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    :cond_c
    iget-object v2, p0, Lhka;->j:[J

    if-eqz v2, :cond_e

    iget-object v2, p0, Lhka;->j:[J

    array-length v2, v2

    if-lez v2, :cond_e

    iget-object v3, p0, Lhka;->j:[J

    array-length v4, v3

    move v2, v1

    :goto_6
    if-ge v1, v4, :cond_d

    aget-wide v6, v3, v1

    invoke-static {v6, v7}, Lidd;->a(J)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_d
    add-int/2addr v0, v2

    iget-object v1, p0, Lhka;->j:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_e
    iget-wide v2, p0, Lhka;->l:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_f

    const/16 v1, 0xb

    iget-wide v2, p0, Lhka;->l:J

    invoke-static {v1, v2, v3}, Lidd;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget v1, p0, Lhka;->k:I

    if-eqz v1, :cond_10

    const/16 v1, 0xc

    iget v2, p0, Lhka;->k:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget-object v1, p0, Lhka;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhka;->J:I

    return v0

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 5

    const/4 v4, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhka;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhka;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhka;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhka;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhka;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhka;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhka;->d:Z

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x28

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhka;->e:[J

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [J

    iget-object v2, p0, Lhka;->e:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhka;->e:[J

    :goto_1
    iget-object v1, p0, Lhka;->e:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lhka;->e:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lhka;->e:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhka;->f:[J

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [J

    iget-object v2, p0, Lhka;->f:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhka;->f:[J

    :goto_2
    iget-object v1, p0, Lhka;->f:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lhka;->f:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lhka;->f:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x38

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhka;->g:[J

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [J

    iget-object v2, p0, Lhka;->g:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhka;->g:[J

    :goto_3
    iget-object v1, p0, Lhka;->g:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lhka;->g:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lhka;->g:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhka;->h:[J

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [J

    iget-object v2, p0, Lhka;->h:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhka;->h:[J

    :goto_4
    iget-object v1, p0, Lhka;->h:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lhka;->h:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v1, p0, Lhka;->h:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x48

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhka;->i:[J

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [J

    iget-object v2, p0, Lhka;->i:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhka;->i:[J

    :goto_5
    iget-object v1, p0, Lhka;->i:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lhka;->i:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_6
    iget-object v1, p0, Lhka;->i:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x50

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhka;->j:[J

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [J

    iget-object v2, p0, Lhka;->j:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhka;->j:[J

    :goto_6
    iget-object v1, p0, Lhka;->j:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lhka;->j:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_7
    iget-object v1, p0, Lhka;->j:[J

    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lidc;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lhka;->l:J

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhka;->k:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 7

    const/4 v0, 0x0

    iget-boolean v1, p0, Lhka;->a:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iget-boolean v2, p0, Lhka;->a:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    :cond_0
    iget-object v1, p0, Lhka;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Lhka;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lhka;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lhka;->c:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILjava/lang/String;)V

    :cond_2
    iget-boolean v1, p0, Lhka;->d:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lhka;->d:Z

    invoke-virtual {p1, v1, v2}, Lidd;->a(IZ)V

    :cond_3
    iget-object v1, p0, Lhka;->e:[J

    if-eqz v1, :cond_4

    iget-object v2, p0, Lhka;->e:[J

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-wide v4, v2, v1

    const/4 v6, 0x5

    invoke-virtual {p1, v6, v4, v5}, Lidd;->a(IJ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lhka;->f:[J

    if-eqz v1, :cond_5

    iget-object v2, p0, Lhka;->f:[J

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-wide v4, v2, v1

    const/4 v6, 0x6

    invoke-virtual {p1, v6, v4, v5}, Lidd;->a(IJ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lhka;->g:[J

    if-eqz v1, :cond_6

    iget-object v2, p0, Lhka;->g:[J

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_6

    aget-wide v4, v2, v1

    const/4 v6, 0x7

    invoke-virtual {p1, v6, v4, v5}, Lidd;->a(IJ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lhka;->h:[J

    if-eqz v1, :cond_7

    iget-object v2, p0, Lhka;->h:[J

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_7

    aget-wide v4, v2, v1

    const/16 v6, 0x8

    invoke-virtual {p1, v6, v4, v5}, Lidd;->a(IJ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lhka;->i:[J

    if-eqz v1, :cond_8

    iget-object v2, p0, Lhka;->i:[J

    array-length v3, v2

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_8

    aget-wide v4, v2, v1

    const/16 v6, 0x9

    invoke-virtual {p1, v6, v4, v5}, Lidd;->a(IJ)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    iget-object v1, p0, Lhka;->j:[J

    if-eqz v1, :cond_9

    iget-object v1, p0, Lhka;->j:[J

    array-length v2, v1

    :goto_5
    if-ge v0, v2, :cond_9

    aget-wide v4, v1, v0

    const/16 v3, 0xa

    invoke-virtual {p1, v3, v4, v5}, Lidd;->a(IJ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    iget-wide v0, p0, Lhka;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-wide v2, p0, Lhka;->l:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->a(IJ)V

    :cond_a
    iget v0, p0, Lhka;->k:I

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget v1, p0, Lhka;->k:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_b
    iget-object v0, p0, Lhka;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhka;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhka;

    iget-boolean v2, p0, Lhka;->a:Z

    iget-boolean v3, p1, Lhka;->a:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhka;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhka;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhka;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhka;->c:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-boolean v2, p0, Lhka;->d:Z

    iget-boolean v3, p1, Lhka;->d:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhka;->e:[J

    iget-object v3, p1, Lhka;->e:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhka;->f:[J

    iget-object v3, p1, Lhka;->f:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhka;->g:[J

    iget-object v3, p1, Lhka;->g:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhka;->h:[J

    iget-object v3, p1, Lhka;->h:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhka;->i:[J

    iget-object v3, p1, Lhka;->i:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhka;->j:[J

    iget-object v3, p1, Lhka;->j:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lhka;->l:J

    iget-wide v4, p1, Lhka;->l:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lhka;->k:I

    iget v3, p1, Lhka;->k:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhka;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhka;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhka;->b:Ljava/lang/String;

    iget-object v3, p1, Lhka;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhka;->c:Ljava/lang/String;

    iget-object v3, p1, Lhka;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhka;->I:Ljava/util/List;

    iget-object v3, p1, Lhka;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/16 v8, 0x20

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhka;->a:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhka;->b:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v3

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lhka;->c:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v3

    :goto_2
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhka;->d:Z

    if-eqz v4, :cond_9

    :goto_3
    add-int/2addr v0, v1

    iget-object v1, p0, Lhka;->e:[J

    if-nez v1, :cond_a

    mul-int/lit8 v1, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhka;->f:[J

    if-nez v0, :cond_b

    mul-int/lit8 v1, v1, 0x1f

    :cond_1
    iget-object v0, p0, Lhka;->g:[J

    if-nez v0, :cond_c

    mul-int/lit8 v1, v1, 0x1f

    :cond_2
    iget-object v0, p0, Lhka;->h:[J

    if-nez v0, :cond_d

    mul-int/lit8 v1, v1, 0x1f

    :cond_3
    iget-object v0, p0, Lhka;->i:[J

    if-nez v0, :cond_e

    mul-int/lit8 v1, v1, 0x1f

    :cond_4
    iget-object v0, p0, Lhka;->j:[J

    if-nez v0, :cond_f

    mul-int/lit8 v1, v1, 0x1f

    :cond_5
    mul-int/lit8 v0, v1, 0x1f

    iget-wide v4, p0, Lhka;->l:J

    iget-wide v6, p0, Lhka;->l:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v1, v4

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhka;->k:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhka;->I:Ljava/util/List;

    if-nez v1, :cond_10

    :goto_4
    add-int/2addr v0, v3

    return v0

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lhka;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lhka;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_3

    :cond_a
    move v1, v0

    move v0, v3

    :goto_5
    iget-object v2, p0, Lhka;->e:[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lhka;->e:[J

    aget-wide v4, v2, v0

    iget-object v2, p0, Lhka;->e:[J

    aget-wide v6, v2, v0

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v2, v4

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_b
    move v0, v3

    :goto_6
    iget-object v2, p0, Lhka;->f:[J

    array-length v2, v2

    if-ge v0, v2, :cond_1

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lhka;->f:[J

    aget-wide v4, v2, v0

    iget-object v2, p0, Lhka;->f:[J

    aget-wide v6, v2, v0

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v2, v4

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_c
    move v0, v3

    :goto_7
    iget-object v2, p0, Lhka;->g:[J

    array-length v2, v2

    if-ge v0, v2, :cond_2

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lhka;->g:[J

    aget-wide v4, v2, v0

    iget-object v2, p0, Lhka;->g:[J

    aget-wide v6, v2, v0

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v2, v4

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_d
    move v0, v3

    :goto_8
    iget-object v2, p0, Lhka;->h:[J

    array-length v2, v2

    if-ge v0, v2, :cond_3

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lhka;->h:[J

    aget-wide v4, v2, v0

    iget-object v2, p0, Lhka;->h:[J

    aget-wide v6, v2, v0

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v2, v4

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_e
    move v0, v3

    :goto_9
    iget-object v2, p0, Lhka;->i:[J

    array-length v2, v2

    if-ge v0, v2, :cond_4

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lhka;->i:[J

    aget-wide v4, v2, v0

    iget-object v2, p0, Lhka;->i:[J

    aget-wide v6, v2, v0

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v2, v4

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_f
    move v0, v3

    :goto_a
    iget-object v2, p0, Lhka;->j:[J

    array-length v2, v2

    if-ge v0, v2, :cond_5

    mul-int/lit8 v1, v1, 0x1f

    iget-object v2, p0, Lhka;->j:[J

    aget-wide v4, v2, v0

    iget-object v2, p0, Lhka;->j:[J

    aget-wide v6, v2, v0

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v2, v4

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_10
    iget-object v1, p0, Lhka;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v3

    goto/16 :goto_4
.end method
