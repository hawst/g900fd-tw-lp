.class public final Lcxd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field public final a:Landroid/content/SharedPreferences;

.field public b:Z

.field public c:Ljava/lang/String;

.field private final d:Landroid/os/Handler;

.field private final e:Lcxe;

.field private final f:Lgot;

.field private final g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Ljava/util/List;

.field private n:Leue;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/SharedPreferences;Lcxe;Lgot;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcxd;->d:Landroid/os/Handler;

    .line 81
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcxd;->a:Landroid/content/SharedPreferences;

    .line 82
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgot;

    iput-object v0, p0, Lcxd;->f:Lgot;

    .line 83
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcxe;

    iput-object v0, p0, Lcxd;->e:Lcxe;

    .line 84
    iput-object p5, p0, Lcxd;->g:Ljava/lang/String;

    .line 85
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcxd;->j:Z

    if-nez v0, :cond_0

    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcxd;->j:Z

    .line 159
    iget-object v0, p0, Lcxd;->e:Lcxe;

    invoke-interface {v0}, Lcxe;->a()V

    .line 161
    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcxd;->j:Z

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcxd;->j:Z

    .line 166
    iget-object v0, p0, Lcxd;->e:Lcxe;

    invoke-interface {v0}, Lcxe;->b()V

    .line 168
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 245
    iget-object v1, p0, Lcxd;->m:Ljava/util/List;

    .line 246
    iget v0, p0, Lcxd;->k:I

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcxd;->g:Ljava/lang/String;

    .line 247
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 249
    const/4 v1, 0x0

    iget-object v2, p0, Lcxd;->g:Ljava/lang/String;

    invoke-static {v2}, Lgpa;->a(Ljava/lang/String;)Lgpa;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 251
    :goto_0
    iget-object v1, p0, Lcxd;->e:Lcxe;

    invoke-interface {v1, v0}, Lcxe;->a(Ljava/util/List;)V

    .line 252
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcxd;->i:Z

    .line 262
    invoke-direct {p0}, Lcxd;->d()V

    .line 263
    iget-boolean v0, p0, Lcxd;->b:Z

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lcxd;->e:Lcxe;

    invoke-interface {v0}, Lcxe;->c()V

    .line 266
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 130
    iget-boolean v0, p0, Lcxd;->i:Z

    if-nez v0, :cond_0

    .line 132
    iget-object v0, p0, Lcxd;->m:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 133
    invoke-direct {p0}, Lcxd;->e()V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcxd;->i:Z

    .line 136
    invoke-static {p0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lcxd;->n:Leue;

    .line 137
    iget-object v0, p0, Lcxd;->f:Lgot;

    iget-object v1, p0, Lcxd;->c:Ljava/lang/String;

    iget-object v2, p0, Lcxd;->d:Landroid/os/Handler;

    iget-object v3, p0, Lcxd;->n:Leue;

    .line 138
    invoke-static {v2, v3}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v2

    .line 137
    invoke-interface {v0, v1, v2}, Lgot;->a(Ljava/lang/String;Leuc;)V

    goto :goto_0
.end method

.method public final a(Lfrl;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 101
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-virtual {p0}, Lcxd;->b()V

    .line 103
    iget-object v1, p1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcxd;->c:Ljava/lang/String;

    .line 104
    invoke-virtual {p1}, Lfrl;->r()Lhrd;

    move-result-object v1

    .line 105
    iget-object v2, p0, Lcxd;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    .line 106
    iget v1, v1, Lhrd;->a:I

    iput v1, p0, Lcxd;->k:I

    .line 107
    iget v1, p0, Lcxd;->k:I

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Captions visibility %d is not supported."

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v1, p0, Lcxd;->a:Landroid/content/SharedPreferences;

    const-string v2, "subtitles_language_code"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    :pswitch_1
    iput-object v0, p0, Lcxd;->l:Ljava/lang/String;

    .line 108
    invoke-direct {p0}, Lcxd;->c()V

    .line 110
    iget-object v0, p0, Lcxd;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    iput-boolean v3, p0, Lcxd;->h:Z

    .line 112
    invoke-virtual {p0}, Lcxd;->a()V

    .line 115
    :cond_1
    return-void

    .line 107
    :pswitch_2
    iget-object v1, p0, Lcxd;->a:Landroid/content/SharedPreferences;

    const-string v2, "subtitles_language_code"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 31
    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-direct {p0}, Lcxd;->f()V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 31
    check-cast p2, Ljava/util/List;

    iput-boolean v6, p0, Lcxd;->i:Z

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SubtitleTrack response was empty"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    invoke-direct {p0}, Lcxd;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcxd;->k:I

    if-ne v0, v7, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_3

    :cond_2
    invoke-direct {p0}, Lcxd;->c()V

    :cond_3
    iput-object p2, p0, Lcxd;->m:Ljava/util/List;

    iget-boolean v0, p0, Lcxd;->h:Z

    if-eqz v0, :cond_7

    iput-boolean v6, p0, Lcxd;->h:Z

    iget-object v0, p0, Lcxd;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v1

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpa;

    iget-object v4, v0, Lgpa;->a:Ljava/lang/String;

    iget-object v5, p0, Lcxd;->l:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v2, v0

    goto :goto_1

    :cond_4
    if-nez v1, :cond_8

    const-string v4, "en"

    iget-object v5, v0, Lgpa;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_5
    if-nez v2, :cond_6

    iget v0, p0, Lcxd;->k:I

    if-ne v0, v7, :cond_6

    iget-object v0, p0, Lcxd;->m:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpa;

    move-object v2, v0

    :cond_6
    if-eqz v2, :cond_7

    iget-object v0, p0, Lcxd;->e:Lcxe;

    invoke-interface {v0, v2}, Lcxe;->a(Lgpa;)V

    :cond_7
    iget-boolean v0, p0, Lcxd;->b:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcxd;->b:Z

    invoke-direct {p0}, Lcxd;->e()V

    goto :goto_0

    :cond_8
    move-object v0, v1

    goto :goto_2
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 143
    iput-object v2, p0, Lcxd;->c:Ljava/lang/String;

    .line 144
    iput v0, p0, Lcxd;->k:I

    .line 145
    iput-object v2, p0, Lcxd;->m:Ljava/util/List;

    .line 146
    iput-boolean v0, p0, Lcxd;->b:Z

    .line 147
    iput-boolean v0, p0, Lcxd;->h:Z

    .line 148
    iput-boolean v0, p0, Lcxd;->i:Z

    .line 149
    invoke-direct {p0}, Lcxd;->d()V

    .line 150
    iget-object v0, p0, Lcxd;->n:Leue;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcxd;->n:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 152
    iput-object v2, p0, Lcxd;->n:Leue;

    .line 154
    :cond_0
    return-void
.end method
