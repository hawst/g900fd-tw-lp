.class public final Lgdg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lges;


# instance fields
.field private final d:Lgeq;

.field private final e:Lexd;

.field private final f:Leey;

.field private final g:Lggn;

.field private final h:I


# direct methods
.method public constructor <init>(Lgeq;Lexd;Leey;Lggn;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgeq;

    iput-object v0, p0, Lgdg;->d:Lgeq;

    .line 50
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lgdg;->e:Lexd;

    .line 51
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leey;

    iput-object v0, p0, Lgdg;->f:Leey;

    .line 52
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggn;

    iput-object v0, p0, Lgdg;->g:Lggn;

    .line 53
    iput p5, p0, Lgdg;->h:I

    .line 54
    return-void
.end method

.method private static a(Ljava/util/Collection;Ljava/util/Set;)Ljava/util/List;
    .locals 4

    .prologue
    .line 285
    if-nez p1, :cond_0

    .line 286
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 297
    :goto_0
    return-object v0

    .line 288
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 289
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 290
    goto :goto_0

    .line 292
    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    .line 293
    iget-object v3, v0, Lfqj;->a:Lhgy;

    iget v3, v3, Lhgy;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 294
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 297
    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 6

    .prologue
    .line 403
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 404
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 405
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 406
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    .line 407
    iget-object v5, v0, Lfqj;->a:Lhgy;

    iget-boolean v5, v5, Lhgy;->p:Z

    if-eqz v5, :cond_1

    .line 408
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    :cond_1
    if-nez v3, :cond_0

    iget-object v5, v0, Lfqj;->a:Lhgy;

    iget-object v5, v5, Lhgy;->o:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 411
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 414
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move-object p0, v1

    .line 420
    :cond_3
    :goto_1
    return-object p0

    .line 417
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move-object p0, v2

    .line 418
    goto :goto_1
.end method

.method private static a(Ljava/util/List;I)V
    .locals 3

    .prologue
    .line 339
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 340
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    invoke-virtual {v0}, Lfqj;->b()I

    move-result v0

    .line 342
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-le v0, p1, :cond_0

    .line 343
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 346
    :cond_2
    return-void
.end method

.method private static a(Ljava/util/List;Lgep;)[Lfre;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 227
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 229
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    .line 230
    invoke-virtual {v0}, Lfqj;->b()I

    move-result v4

    .line 231
    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    if-eqz p1, :cond_1

    .line 232
    invoke-virtual {p1, v4}, Lgep;->a(I)I

    move-result v5

    if-nez v5, :cond_0

    .line 234
    :cond_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, v0, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v5}, La;->c(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 235
    :cond_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v0, v0, Lfqj;->d:Landroid/net/Uri;

    invoke-static {v0}, La;->c(Landroid/net/Uri;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 241
    :cond_3
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_4

    .line 242
    const/4 v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    :cond_4
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    new-array v3, v0, [Lfre;

    .line 248
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 249
    new-instance v5, Lfre;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v5, v6, v0}, Lfre;-><init>(IZ)V

    aput-object v5, v3, v1

    .line 250
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 251
    goto :goto_1

    .line 252
    :cond_5
    invoke-static {v3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 253
    return-object v3
.end method

.method private static b(Ljava/util/List;Lgep;)Ljava/util/List;
    .locals 6

    .prologue
    .line 360
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 361
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 362
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 363
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqj;

    .line 364
    invoke-virtual {v0}, Lfqj;->b()I

    move-result v5

    invoke-virtual {p1, v5}, Lgep;->a(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 369
    :pswitch_0
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 366
    :pswitch_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 372
    :pswitch_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 376
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 388
    :goto_1
    return-object v0

    .line 379
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v2

    .line 380
    goto :goto_1

    .line 382
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 384
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 385
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 388
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 364
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Lfqy;Lfrf;Ljava/util/Set;Ljava/util/Set;ZZZZ)Lger;
    .locals 10

    .prologue
    .line 66
    iget-boolean v0, p2, Lfrf;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lfrf;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 67
    :cond_0
    iget-object v3, p2, Lfrf;->g:Lfqj;

    .line 68
    if-eqz p3, :cond_1

    const/16 v0, 0x5d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    if-nez v3, :cond_3

    .line 70
    :cond_2
    new-instance v0, Lgej;

    const-string v1, "HLS not supported/available"

    invoke-direct {v0, v1}, Lgej;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_3
    new-instance v0, Lger;

    const/4 v1, 0x1

    new-array v1, v1, [Lfqj;

    const/4 v2, 0x0

    aput-object v3, v1, v2

    sget-object v2, Lgdg;->a:[Lfqj;

    const/4 v4, 0x0

    sget-object v5, Lgdg;->b:[Lfre;

    sget-object v6, Lgdg;->c:[Ljava/lang/String;

    new-instance v7, Lgep;

    iget v8, p0, Lgdg;->h:I

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Lgep;-><init>(II)V

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lger;-><init>([Lfqj;[Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;Lgep;Ljava/lang/String;)V

    .line 82
    :goto_0
    return-object v0

    .line 84
    :cond_4
    iget-object v2, p2, Lfrf;->b:Ljava/util/List;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    .line 82
    invoke-virtual/range {v0 .. v8}, Lgdg;->a(Lfqy;Ljava/util/Collection;Ljava/util/Set;Ljava/util/Set;ZZZZ)Lger;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lfqy;Ljava/util/Collection;Ljava/util/Set;Ljava/util/Set;ZZZZ)Lger;
    .locals 13

    .prologue
    .line 104
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    iget-object v1, p0, Lgdg;->d:Lgeq;

    move/from16 v0, p5

    invoke-virtual {v1, v0}, Lgeq;->a(Z)Lgeo;

    move-result-object v9

    .line 109
    invoke-static/range {p2 .. p3}, Lgdg;->a(Ljava/util/Collection;Ljava/util/Set;)Ljava/util/List;

    move-result-object v7

    .line 111
    move-object/from16 v0, p4

    invoke-static {p2, v0}, Lgdg;->a(Ljava/util/Collection;Ljava/util/Set;)Ljava/util/List;

    move-result-object v10

    .line 114
    iget-boolean v2, v9, Lgeo;->d:Z

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfqj;

    iget-object v4, v1, Lfqj;->a:Lhgy;

    iget-boolean v4, v4, Lhgy;->l:Z

    if-eqz v4, :cond_0

    if-nez v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lfqj;->b()I

    move-result v1

    const/16 v4, 0x168

    if-eq v1, v4, :cond_0

    const/16 v4, 0x2d0

    if-eq v1, v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 117
    :cond_2
    iget v1, p0, Lgdg;->h:I

    invoke-static {v7, v1}, Lgdg;->a(Ljava/util/List;I)V

    .line 120
    iget-object v1, p0, Lgdg;->e:Lexd;

    invoke-interface {v1}, Lexd;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lgdg;->g:Lggn;

    iget-object v1, v2, Lggn;->a:Landroid/content/SharedPreferences;

    const-string v3, "default_hq"

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v2, Lggn;->a:Landroid/content/SharedPreferences;

    const-string v3, "default_hq"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    iget-object v2, v2, Lggn;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "limit_mobile_data_usage"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "default_hq"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_2
    if-eqz v1, :cond_3

    .line 121
    const/16 v1, 0x1e0

    invoke-static {v7, v1}, Lgdg;->a(Ljava/util/List;I)V

    .line 124
    :cond_3
    const/4 v1, 0x0

    invoke-static {v7, v1}, Lgdg;->a(Ljava/util/List;Lgep;)[Lfre;

    move-result-object v6

    .line 126
    iget-object v1, v9, Lgeo;->a:Lgep;

    .line 127
    if-eqz p8, :cond_11

    .line 128
    const-string v2, "Restricting to SD streams"

    invoke-static {v2}, Lezp;->e(Ljava/lang/String;)V

    .line 129
    if-nez p5, :cond_6

    .line 132
    const/16 v2, 0x1e0

    invoke-static {v7, v2}, Lgdg;->a(Ljava/util/List;I)V

    .line 133
    const/4 v2, 0x0

    invoke-static {v7, v2}, Lgdg;->a(Ljava/util/List;Lgep;)[Lfre;

    move-result-object v2

    .line 148
    :goto_3
    array-length v3, v2

    if-nez v3, :cond_7

    .line 149
    new-instance v1, Lgej;

    const-string v2, "Video not supported/available due to SD restriction"

    invoke-direct {v1, v2}, Lgej;-><init>(Ljava/lang/String;)V

    throw v1

    .line 120
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    :cond_5
    iget-object v1, v2, Lggn;->a:Landroid/content/SharedPreferences;

    const-string v2, "limit_mobile_data_usage"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    goto :goto_2

    .line 137
    :cond_6
    new-instance v2, Lgep;

    const/16 v3, 0x1e0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lgep;-><init>(II)V

    invoke-static {v7, v2}, Lgdg;->a(Ljava/util/List;Lgep;)[Lfre;

    move-result-object v3

    .line 140
    new-instance v2, Lgep;

    .line 142
    iget v4, v1, Lgep;->a:I

    const/16 v5, 0x1e0

    .line 141
    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 144
    iget v1, v1, Lgep;->b:I

    const/16 v5, 0x1e0

    .line 143
    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-direct {v2, v4, v1}, Lgep;-><init>(II)V

    move-object v1, v2

    move-object v2, v3

    goto :goto_3

    :cond_7
    move-object v8, v1

    move-object v6, v2

    .line 153
    :goto_4
    new-instance v11, Lgdh;

    iget-object v2, p0, Lgdg;->f:Leey;

    .line 155
    iget-boolean v3, v9, Lgeo;->c:Z

    .line 156
    iget-object v1, p1, Lfqy;->a:Lhre;

    iget-object v1, v1, Lhre;->j:Lgzt;

    if-eqz v1, :cond_9

    iget-object v1, p1, Lfqy;->a:Lhre;

    iget-object v1, v1, Lhre;->j:Lgzt;

    iget-boolean v1, v1, Lgzt;->a:Z

    :goto_5
    invoke-direct {v11, v2, v3, v1}, Lgdh;-><init>(Leey;ZZ)V

    .line 157
    const/4 v1, 0x0

    invoke-static {v11, v1}, Lgdh;->a(Lgdh;Z)V

    .line 158
    invoke-static {v7, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 159
    invoke-static {v10, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 162
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v4, 0x0

    .line 164
    :goto_6
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v5, 0x0

    .line 168
    :goto_7
    if-nez p5, :cond_10

    .line 169
    iget-object v1, v9, Lgeo;->a:Lgep;

    invoke-static {v7, v1}, Lgdg;->b(Ljava/util/List;Lgep;)Ljava/util/List;

    move-result-object v1

    move-object v2, v1

    .line 171
    :goto_8
    iget-object v1, v9, Lgeo;->b:Lgep;

    invoke-static {v10, v1}, Lgdg;->b(Ljava/util/List;Lgep;)Ljava/util/List;

    move-result-object v3

    .line 173
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_8
    :goto_9
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfqj;

    iget-object v1, v1, Lfqj;->a:Lhgy;

    iget-object v1, v1, Lhgy;->o:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_8

    invoke-interface {v7, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 156
    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 162
    :cond_a
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfqj;

    move-object v4, v1

    goto :goto_6

    .line 164
    :cond_b
    const/4 v1, 0x0

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfqj;

    move-object v5, v1

    goto :goto_7

    .line 173
    :cond_c
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v7, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    invoke-static {v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 178
    if-nez p5, :cond_d

    .line 179
    iget-object v1, v9, Lgeo;->e:Ljava/lang/String;

    invoke-static {v3, v1}, Lgdg;->a(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    move-object v3, v1

    .line 182
    :cond_d
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_e

    if-eqz p6, :cond_e

    .line 183
    new-instance v1, Lgej;

    const-string v2, "Video not supported/available"

    invoke-direct {v1, v2}, Lgej;-><init>(Ljava/lang/String;)V

    throw v1

    .line 185
    :cond_e
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_f

    if-eqz p7, :cond_f

    .line 186
    new-instance v1, Lgej;

    const-string v2, "Audio not supported/available"

    invoke-direct {v1, v2}, Lgej;-><init>(Ljava/lang/String;)V

    throw v1

    .line 189
    :cond_f
    const/4 v1, 0x1

    invoke-static {v11, v1}, Lgdh;->a(Lgdh;Z)V

    .line 190
    invoke-static {v2, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 191
    invoke-static {v3, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 193
    new-instance v1, Lger;

    .line 194
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Lfqj;

    invoke-interface {v2, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lfqj;

    .line 195
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Lfqj;

    invoke-interface {v3, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lfqj;

    .line 201
    iget-object v9, v9, Lgeo;->e:Ljava/lang/String;

    invoke-direct/range {v1 .. v9}, Lger;-><init>([Lfqj;[Lfqj;Lfqj;Lfqj;[Lfre;[Ljava/lang/String;Lgep;Ljava/lang/String;)V

    return-object v1

    :cond_10
    move-object v2, v7

    goto/16 :goto_8

    :cond_11
    move-object v8, v1

    goto/16 :goto_4
.end method
