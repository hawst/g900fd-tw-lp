.class public Ldza;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldzr;


# static fields
.field private static final c:Ldzc;

.field private static final m:Ljava/util/logging/Logger;

.field private static synthetic n:Z


# instance fields
.field final a:Ljava/nio/channels/Selector;

.field volatile b:Z

.field private final d:Ljava/lang/Thread;

.field private final e:Ldzt;

.field private final f:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final g:Ljava/util/PriorityQueue;

.field private volatile h:J

.field private volatile i:Z

.field private volatile j:Z

.field private volatile k:Z

.field private volatile l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Ldza;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ldza;->n:Z

    .line 52
    new-instance v0, Ldzc;

    invoke-direct {v0}, Ldzc;-><init>()V

    sput-object v0, Ldza;->c:Ldzc;

    .line 1195
    const-class v0, Ldza;

    .line 1196
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Ldza;->m:Ljava/util/logging/Logger;

    .line 1195
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1162
    new-instance v0, Ldzt;

    invoke-direct {v0}, Ldzt;-><init>()V

    iput-object v0, p0, Ldza;->e:Ldzt;

    .line 1165
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Ldza;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 1170
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Ldza;->g:Ljava/util/PriorityQueue;

    .line 1176
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldza;->h:J

    .line 1182
    iput-boolean v2, p0, Ldza;->j:Z

    .line 1186
    iput-boolean v2, p0, Ldza;->b:Z

    .line 1189
    iput-boolean v2, p0, Ldza;->k:Z

    .line 1193
    iput-boolean v2, p0, Ldza;->l:Z

    .line 66
    :try_start_0
    invoke-static {}, Ljava/nio/channels/Selector;->open()Ljava/nio/channels/Selector;

    move-result-object v0

    iput-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Ldza;->d:Ljava/lang/Thread;

    .line 71
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    new-instance v1, Ldzf;

    invoke-direct {v1, v0}, Ldzf;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a(Ljava/util/Set;Z)I
    .locals 10

    .prologue
    .line 764
    const/4 v0, 0x0

    .line 766
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    .line 767
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 768
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/channels/SelectionKey;

    .line 769
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 779
    monitor-enter v0

    .line 782
    :try_start_0
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    .line 783
    monitor-exit v0

    goto :goto_0

    .line 796
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 788
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->readyOps()I

    move-result v4

    .line 791
    invoke-virtual {v0}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldzd;

    .line 792
    iget-object v5, v1, Ldzd;->a:Lu;

    .line 793
    iget-object v5, v1, Ldzd;->b:Ldyy;

    .line 794
    iget-object v6, v1, Ldzd;->c:Ldzo;

    .line 795
    iget-object v7, v1, Ldzd;->d:Ldzw;

    .line 796
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 798
    and-int/lit8 v0, v4, 0x10

    if-eqz v0, :cond_9

    .line 800
    :try_start_2
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v1, "OP_ACCEPT event ready"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    .line 810
    :goto_1
    add-int/lit8 v1, v2, 0x1

    .line 812
    :goto_2
    and-int/lit8 v0, v4, 0x8

    if-eqz v0, :cond_1

    .line 814
    :try_start_3
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_CONNECT event ready"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 815
    invoke-interface {v5}, Ldyy;->b()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    .line 824
    :goto_3
    add-int/lit8 v1, v1, 0x1

    .line 826
    :cond_1
    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_8

    .line 828
    :try_start_4
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_READ event ready"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 829
    invoke-interface {v6}, Ldzo;->f()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_2

    .line 838
    :goto_4
    add-int/lit8 v0, v1, 0x1

    .line 840
    :goto_5
    and-int/lit8 v1, v4, 0x4

    if-eqz v1, :cond_2

    .line 842
    :try_start_5
    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_WRITE event ready"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 843
    invoke-interface {v7}, Ldzw;->g()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    .line 852
    :goto_6
    add-int/lit8 v0, v0, 0x1

    :cond_2
    move v2, v0

    .line 854
    goto :goto_0

    .line 802
    :catch_0
    move-exception v0

    .line 804
    if-eqz p2, :cond_3

    .line 805
    throw v0

    .line 807
    :cond_3
    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v8, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v9, "RuntimeException caught when invoking AcceptHandler.handleAcceptEvent"

    invoke-virtual {v1, v8, v9, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 816
    :catch_1
    move-exception v0

    .line 818
    if-eqz p2, :cond_4

    .line 819
    throw v0

    .line 821
    :cond_4
    sget-object v2, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v8, "RuntimeException caught when invoking ConnectHandler.handleConnectEvent"

    invoke-virtual {v2, v5, v8, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 830
    :catch_2
    move-exception v0

    .line 832
    if-eqz p2, :cond_5

    .line 833
    throw v0

    .line 835
    :cond_5
    sget-object v2, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v5, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v6, "RuntimeException caught when invoking ReadHandler.handleReadEvent"

    invoke-virtual {v2, v5, v6, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 844
    :catch_3
    move-exception v1

    .line 846
    if-eqz p2, :cond_6

    .line 847
    throw v1

    .line 849
    :cond_6
    sget-object v2, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "RuntimeException caught when invoking WriteHandler.handleWriteEvent"

    invoke-virtual {v2, v4, v5, v1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 856
    :cond_7
    return v2

    :cond_8
    move v0, v1

    goto :goto_5

    :cond_9
    move v1, v2

    goto :goto_2
.end method

.method private static a(Ljava/util/Queue;Ljava/util/PriorityQueue;Ldzc;Z)J
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 631
    :goto_0
    invoke-interface {p0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 633
    :try_start_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 634
    :catch_0
    move-exception v0

    .line 635
    if-eqz p3, :cond_0

    .line 636
    throw v0

    .line 638
    :cond_0
    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "RuntimeException caught when invoking Runnable.run()"

    invoke-virtual {v1, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 647
    :cond_1
    monitor-enter p1

    .line 648
    :try_start_1
    invoke-virtual {p1}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 653
    monitor-exit p1

    move-wide v0, v2

    .line 684
    :goto_1
    return-wide v0

    .line 656
    :cond_2
    invoke-virtual {p1}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzb;

    .line 658
    iget-wide v4, v0, Ldym;->a:J

    invoke-virtual {p2}, Ldzc;->a()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 659
    cmp-long v1, v4, v2

    if-lez v1, :cond_3

    .line 662
    monitor-exit p1

    move-wide v0, v4

    goto :goto_1

    .line 666
    :cond_3
    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v1, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 667
    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xf

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Alarm expired: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 670
    :cond_4
    invoke-virtual {p1}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzb;

    .line 671
    const/4 v1, 0x0

    iput-boolean v1, v0, Ldzb;->d:Z

    .line 672
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 675
    :try_start_2
    iget-object v1, v0, Ldym;->b:Ldyn;

    invoke-interface {v1, v0}, Ldyn;->a(Ldym;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 676
    :catch_1
    move-exception v0

    .line 677
    if-eqz p3, :cond_5

    .line 678
    throw v0

    .line 672
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 680
    :cond_5
    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v5, "RuntimeException caught when invoking AlarmHandler.handleAlarmEvent"

    invoke-virtual {v1, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method private a(Ljava/nio/channels/SelectableChannel;I)V
    .locals 4

    .prologue
    .line 1040
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectableChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;

    move-result-object v1

    .line 1041
    if-nez v1, :cond_0

    .line 1085
    :goto_0
    return-void

    .line 1047
    :cond_0
    invoke-direct {p0}, Ldza;->f()V

    .line 1054
    :try_start_0
    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1055
    :try_start_1
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1059
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1084
    invoke-direct {p0}, Ldza;->g()V

    goto :goto_0

    .line 1061
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v0

    xor-int/lit8 v2, p2, -0x1

    and-int/2addr v0, v2

    invoke-virtual {v1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    .line 1066
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzd;

    .line 1067
    const/16 v2, 0x10

    if-ne p2, v2, :cond_2

    .line 1068
    const/4 v2, 0x0

    iput-object v2, v0, Ldzd;->a:Lu;

    .line 1069
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_ACCEPT event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 1082
    :goto_1
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1084
    invoke-direct {p0}, Ldza;->g()V

    goto :goto_0

    .line 1070
    :cond_2
    const/16 v2, 0x8

    if-ne p2, v2, :cond_3

    .line 1071
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, v0, Ldzd;->b:Ldyy;

    .line 1072
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_CONNECT event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    .line 1082
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1084
    :catchall_1
    move-exception v0

    invoke-direct {p0}, Ldza;->g()V

    throw v0

    .line 1073
    :cond_3
    const/4 v2, 0x1

    if-ne p2, v2, :cond_4

    .line 1074
    const/4 v2, 0x0

    :try_start_5
    iput-object v2, v0, Ldzd;->c:Ldzo;

    .line 1075
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_READ event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    .line 1076
    :cond_4
    const/4 v2, 0x4

    if-ne p2, v2, :cond_5

    .line 1077
    const/4 v2, 0x0

    iput-object v2, v0, Ldzd;->d:Ldzw;

    .line 1078
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_WRITE event deregistered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    .line 1080
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a valid op"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private a(Ljava/nio/channels/SelectableChannel;ILu;Ldyy;Ldzo;Ldzw;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 972
    invoke-direct {p0}, Ldza;->f()V

    .line 974
    const/4 v1, 0x0

    .line 976
    :try_start_0
    monitor-enter p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 977
    :try_start_1
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {p1, v0}, Ljava/nio/channels/SelectableChannel;->keyFor(Ljava/nio/channels/Selector;)Ljava/nio/channels/SelectionKey;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 978
    if-nez v0, :cond_a

    .line 980
    :try_start_2
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    new-instance v1, Ldzd;

    invoke-direct {v1}, Ldzd;-><init>()V

    invoke-virtual {p1, v0, p2, v1}, Ljava/nio/channels/SelectableChannel;->register(Ljava/nio/channels/Selector;ILjava/lang/Object;)Ljava/nio/channels/SelectionKey;
    :try_end_2
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    move-object v1, v0

    move v0, v2

    .line 986
    :goto_0
    :try_start_3
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 987
    :try_start_4
    sget-boolean v3, Ldza;->n:Z

    if-nez v3, :cond_0

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1023
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Ldza;->g()V

    throw v0

    .line 981
    :catch_0
    move-exception v0

    .line 982
    :try_start_5
    new-instance v1, Ldzf;

    invoke-direct {v1, v0}, Ldzf;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 986
    :catchall_1
    move-exception v0

    monitor-exit p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0

    .line 994
    :cond_0
    monitor-enter v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 997
    if-nez v0, :cond_1

    .line 998
    :try_start_7
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->interestOps()I

    move-result v0

    or-int/2addr v0, p2

    invoke-virtual {v1, v0}, Ljava/nio/channels/SelectionKey;->interestOps(I)Ljava/nio/channels/SelectionKey;

    .line 1001
    :cond_1
    invoke-virtual {v1}, Ljava/nio/channels/SelectionKey;->attachment()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzd;

    .line 1002
    const/16 v3, 0x10

    if-ne p2, v3, :cond_3

    .line 1003
    sget-boolean v2, Ldza;->n:Z

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1021
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1004
    :cond_2
    const/4 v2, 0x0

    :try_start_9
    iput-object v2, v0, Ldzd;->a:Lu;

    .line 1005
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_ACCEPT event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 1021
    :goto_1
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 1023
    invoke-direct {p0}, Ldza;->g()V

    .line 1024
    return-void

    .line 1006
    :cond_3
    const/16 v3, 0x8

    if-ne p2, v3, :cond_5

    .line 1007
    :try_start_a
    sget-boolean v2, Ldza;->n:Z

    if-nez v2, :cond_4

    if-nez p4, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1008
    :cond_4
    iput-object p4, v0, Ldzd;->b:Ldyy;

    .line 1009
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_CONNECT event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    .line 1010
    :cond_5
    if-ne p2, v2, :cond_7

    .line 1011
    sget-boolean v2, Ldza;->n:Z

    if-nez v2, :cond_6

    if-nez p5, :cond_6

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1012
    :cond_6
    iput-object p5, v0, Ldzd;->c:Ldzo;

    .line 1013
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_READ event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    .line 1014
    :cond_7
    const/4 v2, 0x4

    if-ne p2, v2, :cond_9

    .line 1015
    sget-boolean v2, Ldza;->n:Z

    if-nez v2, :cond_8

    if-nez p6, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1016
    :cond_8
    iput-object p6, v0, Ldzd;->d:Ldzw;

    .line 1017
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "OP_WRITE event registered"

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    goto :goto_1

    .line 1019
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is not a valid op"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :cond_a
    move-object v4, v0

    move v0, v1

    move-object v1, v4

    goto/16 :goto_0
.end method

.method private static a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 879
    if-nez p0, :cond_0

    .line 880
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 882
    :cond_0
    if-nez p1, :cond_1

    .line 883
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 885
    :cond_1
    invoke-virtual {p0}, Ljava/nio/channels/SelectableChannel;->isBlocking()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 886
    new-instance v0, Ljava/nio/channels/IllegalBlockingModeException;

    invoke-direct {v0}, Ljava/nio/channels/IllegalBlockingModeException;-><init>()V

    throw v0

    .line 888
    :cond_2
    return-void
.end method

.method private d()J
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, -0x1

    .line 583
    iget-object v0, p0, Ldza;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move-wide v0, v2

    .line 596
    :goto_0
    return-wide v0

    .line 586
    :cond_0
    iget-object v6, p0, Ldza;->g:Ljava/util/PriorityQueue;

    monitor-enter v6

    .line 587
    :try_start_0
    iget-object v0, p0, Ldza;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 588
    monitor-exit v6

    move-wide v0, v4

    goto :goto_0

    .line 590
    :cond_1
    iget-object v0, p0, Ldza;->g:Ljava/util/PriorityQueue;

    .line 591
    invoke-virtual {v0}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldzb;

    iget-wide v0, v0, Ldym;->a:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v0, v8

    .line 592
    cmp-long v4, v0, v4

    if-lez v4, :cond_2

    .line 593
    monitor-exit v6

    goto :goto_0

    .line 598
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 596
    :cond_2
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-wide v0, v2

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 900
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldza;->e:Ldzt;

    invoke-virtual {v0}, Ldzt;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 907
    :goto_0
    iget-boolean v0, p0, Ldza;->b:Z

    return v0

    .line 905
    :catch_0
    move-exception v0

    iget-boolean v0, p0, Ldza;->b:Z

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 932
    invoke-virtual {p0}, Ldza;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 933
    iget-object v0, p0, Ldza;->e:Ldzt;

    invoke-virtual {v0}, Ldzt;->a()V

    .line 935
    :try_start_0
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 941
    :cond_0
    return-void

    .line 938
    :catch_0
    move-exception v0

    new-instance v0, Ldzg;

    invoke-direct {v0}, Ldzg;-><init>()V

    throw v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 949
    invoke-virtual {p0}, Ldza;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 950
    iget-object v0, p0, Ldza;->e:Ldzt;

    invoke-virtual {v0}, Ldzt;->b()V

    .line 952
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(JLjava/lang/Object;Ldyn;)Ldym;
    .locals 9

    .prologue
    .line 492
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 493
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x35

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "offsetMillis cannot be negative: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 496
    :cond_0
    if-nez p4, :cond_1

    .line 497
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "callback cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501
    :cond_1
    iget-object v8, p0, Ldza;->g:Ljava/util/PriorityQueue;

    monitor-enter v8

    .line 502
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 503
    new-instance v1, Ldzb;

    add-long v2, v4, p1

    move-object v6, p4

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Ldzb;-><init>(JJLdyn;Ljava/lang/Object;)V

    .line 505
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 506
    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Alarm created: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    .line 508
    :cond_2
    iget-object v0, p0, Ldza;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v0, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    move-result v0

    .line 509
    sget-boolean v2, Ldza;->n:Z

    if-nez v2, :cond_3

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 510
    :catchall_0
    move-exception v0

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 512
    invoke-virtual {p0}, Ldza;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 514
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    .line 517
    :cond_4
    return-object v1
.end method

.method public final a()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 101
    invoke-virtual {p0}, Ldza;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalThreadStateException;

    iget-object v1, p0, Ldza;->d:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x34

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Network thread is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but the thread "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is trying to loop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalThreadStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Ldza;->j:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot recursively loop"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-boolean v1, p0, Ldza;->j:Z

    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v1, "Start looping"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0}, Ldza;->d()J

    move-result-wide v0

    :goto_0
    iget-boolean v2, p0, Ldza;->b:Z

    if-nez v2, :cond_5

    invoke-direct {p0}, Ldza;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_5

    cmp-long v2, v0, v8

    if-nez v2, :cond_7

    const-wide/32 v0, 0x5265c00

    move-wide v2, v0

    :goto_1
    cmp-long v0, v2, v8

    if-ltz v0, :cond_2

    :try_start_1
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0, v2, v3}, Ljava/nio/channels/Selector;->select(J)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    iget-wide v0, p0, Ldza;->h:J

    iget-object v2, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v2}, Ljava/nio/channels/Selector;->selectedKeys()Ljava/util/Set;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Ldza;->a(Ljava/util/Set;Z)I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Ldza;->h:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Ldza;->i:Z

    iget-object v0, p0, Ldza;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget-object v1, p0, Ldza;->g:Ljava/util/PriorityQueue;

    sget-object v2, Ldza;->c:Ldzc;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Ldza;->a(Ljava/util/Queue;Ljava/util/PriorityQueue;Ldzc;Z)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-wide v2

    const/4 v0, 0x0

    :try_start_4
    iput-boolean v0, p0, Ldza;->i:Z

    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    const-string v4, "State at end of loop iteration: "

    invoke-virtual {p0}, Ldza;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-wide v0, v2

    goto :goto_0

    :cond_2
    :try_start_5
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->selectNow()I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v4, "Operation not permitted"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v5, "Ignoring spurious IOException in server loop"

    invoke-virtual {v1, v4, v5, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v0, v2

    goto :goto_0

    :cond_3
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_0
    move-exception v0

    iput-boolean v6, p0, Ldza;->j:Z

    iput-boolean v6, p0, Ldza;->b:Z

    sget-object v1, Ldza;->m:Ljava/util/logging/Logger;

    const-string v2, "Stop looping"

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    throw v0

    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    :try_start_7
    iput-boolean v1, p0, Ldza;->i:Z

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    :cond_5
    iput-boolean v6, p0, Ldza;->j:Z

    iput-boolean v6, p0, Ldza;->b:Z

    sget-object v0, Ldza;->m:Ljava/util/logging/Logger;

    const-string v1, "Stop looping"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    return-void

    :cond_6
    move-wide v0, v2

    goto/16 :goto_0

    :cond_7
    move-wide v2, v0

    goto/16 :goto_1
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    .prologue
    .line 418
    if-nez p1, :cond_0

    .line 419
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421
    :cond_0
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0}, Ldza;->a(Ljava/nio/channels/SelectableChannel;I)V

    .line 422
    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;Ldyy;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 413
    invoke-static {p1, p2}, Ldza;->a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V

    .line 414
    const/16 v2, 0x8

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Ldza;->a(Ljava/nio/channels/SelectableChannel;ILu;Ldyy;Ldzo;Ldzw;)V

    .line 415
    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;Ldzo;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 439
    invoke-static {p1, p2}, Ldza;->a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V

    .line 440
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, p2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Ldza;->a(Ljava/nio/channels/SelectableChannel;ILu;Ldyy;Ldzo;Ldzw;)V

    .line 441
    return-void
.end method

.method public final a(Ljava/nio/channels/SelectableChannel;Ldzw;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 465
    invoke-static {p1, p2}, Ldza;->a(Ljava/nio/channels/SelectableChannel;Ljava/lang/Object;)V

    .line 466
    const/4 v2, 0x4

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Ldza;->a(Ljava/nio/channels/SelectableChannel;ILu;Ldyy;Ldzo;Ldzw;)V

    .line 467
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldza;->b:Z

    .line 286
    invoke-virtual {p0}, Ldza;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    .line 287
    :cond_0
    return-void
.end method

.method public final b(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    .prologue
    .line 444
    if-nez p1, :cond_0

    .line 445
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 447
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Ldza;->a(Ljava/nio/channels/SelectableChannel;I)V

    .line 448
    return-void
.end method

.method public final c(Ljava/nio/channels/SelectableChannel;)V
    .locals 2

    .prologue
    .line 470
    if-nez p1, :cond_0

    .line 471
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "channel cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Ldza;->a(Ljava/nio/channels/SelectableChannel;I)V

    .line 474
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 553
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Ldza;->d:Ljava/lang/Thread;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Ldza;->f:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 558
    invoke-virtual {p0}, Ldza;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 560
    iget-object v0, p0, Ldza;->a:Ljava/nio/channels/Selector;

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->wakeup()Ljava/nio/channels/Selector;

    .line 562
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 326
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 327
    const-string v1, "Alarm set = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 328
    iget-object v1, p0, Ldza;->g:Ljava/util/PriorityQueue;

    monitor-enter v1

    .line 329
    :try_start_0
    iget-object v2, p0, Ldza;->g:Ljava/util/PriorityQueue;

    invoke-virtual {v2}, Ljava/util/PriorityQueue;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 330
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    iget-boolean v1, p0, Ldza;->j:Z

    if-eqz v1, :cond_1

    .line 332
    const-string v1, "; Looping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 336
    :goto_0
    iget-boolean v1, p0, Ldza;->b:Z

    if-eqz v1, :cond_0

    .line 337
    const-string v1, "; Loop exiting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 339
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 330
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 334
    :cond_1
    const-string v1, "; Not looping"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
