.class final Ldqj;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private synthetic a:Ldqg;


# direct methods
.method constructor <init>(Ldqg;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 406
    iput-object p1, p0, Ldqj;->a:Ldqg;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 412
    :cond_0
    :try_start_0
    iget-object v0, p0, Ldqj;->a:Ldqg;

    invoke-static {v0}, Ldqg;->e(Ldqg;)Ldqq;

    move-result-object v0

    iget-object v1, p0, Ldqj;->a:Ldqg;

    invoke-static {v1}, Ldqg;->k(Ldqg;)Z

    move-result v1

    invoke-interface {v0, v1}, Ldqq;->a(Z)V

    .line 413
    iget-object v0, p0, Ldqj;->a:Ldqg;

    invoke-static {v0}, Ldqg;->b(Ldqg;)Z
    :try_end_0
    .catch Ldqt; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ldqo; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ldqy; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ldqs; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    move-result v0

    if-nez v0, :cond_0

    .line 443
    :goto_0
    iget-object v0, p0, Ldqj;->a:Ldqg;

    invoke-static {v0}, Ldqg;->l(Ldqg;)V

    .line 444
    :goto_1
    return-void

    .line 418
    :catch_0
    move-exception v0

    .line 419
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get: server not found."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 421
    :catch_1
    move-exception v0

    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    goto :goto_1

    .line 423
    :catch_2
    move-exception v0

    .line 424
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 425
    :catch_3
    move-exception v0

    .line 426
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 427
    :catch_4
    move-exception v0

    .line 428
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v1

    iget v2, v0, Ldqy;->a:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2e

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected response on hanging get "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    iget v0, v0, Ldqy;->a:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 435
    :pswitch_1
    iget-object v0, p0, Ldqj;->a:Ldqg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ldqg;->a(Z)V

    goto :goto_1

    .line 438
    :catch_5
    move-exception v0

    .line 439
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error on hanging get. No network connection: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 440
    :catch_6
    move-exception v0

    .line 441
    invoke-static {}, Ldqg;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unexpected exception on hanging get"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
