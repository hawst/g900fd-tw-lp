.class public final Lbwa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leuc;


# instance fields
.field public final a:Landroid/app/Activity;

.field final b:Lfxe;

.field public final c:Leyt;

.field public d:Lbto;

.field public e:Ljava/lang/String;

.field f:Landroid/view/View;

.field g:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lfxe;Leyt;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbwa;->a:Landroid/app/Activity;

    .line 50
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lbwa;->b:Lfxe;

    .line 51
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbwa;->c:Leyt;

    .line 52
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 63
    iget-object v0, p0, Lbwa;->d:Lbto;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lbwa;->d:Lbto;

    invoke-virtual {v0}, Lbto;->a()V

    .line 99
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lbwa;->g:Landroid/app/AlertDialog;

    if-nez v0, :cond_2

    .line 69
    iget-object v0, p0, Lbwa;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a5

    invoke-virtual {v0, v1, v9, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbwa;->f:Landroid/view/View;

    iget-object v0, p0, Lbwa;->f:Landroid/view/View;

    const v1, 0x7f08024c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v0, p0, Lbwa;->f:Landroid/view/View;

    const v1, 0x7f08024b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    invoke-static {}, Lfxk;->values()[Lfxk;

    move-result-object v3

    array-length v4, v3

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    new-instance v6, Landroid/widget/RadioButton;

    iget-object v7, p0, Lbwa;->a:Landroid/app/Activity;

    invoke-direct {v6, v7}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    iget v7, v5, Lfxk;->a:I

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setText(I)V

    iget-object v7, p0, Lbwa;->a:Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f070082

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setTextColor(I)V

    invoke-virtual {v6, v5}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, v6}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    new-instance v1, Leyv;

    iget-object v3, p0, Lbwa;->a:Landroid/app/Activity;

    invoke-direct {v1, v3}, Leyv;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0902a4

    invoke-virtual {v1, v3}, Leyv;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v3, p0, Lbwa;->f:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0902a5

    invoke-virtual {v1, v3, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0900a2

    invoke-virtual {v1, v3, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    new-instance v3, Lbwc;

    invoke-direct {v3, p0, v1}, Lbwc;-><init>(Lbwa;Landroid/app/AlertDialog;)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iput-object v1, p0, Lbwa;->g:Landroid/app/AlertDialog;

    .line 71
    :cond_2
    iget-object v0, p0, Lbwa;->g:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 76
    iget-object v0, p0, Lbwa;->g:Landroid/app/AlertDialog;

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 78
    new-instance v0, Lbwb;

    invoke-direct {v0, p0}, Lbwb;-><init>(Lbwa;)V

    .line 98
    iget-object v1, p0, Lbwa;->g:Landroid/app/AlertDialog;

    invoke-virtual {v1, v10}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 34
    const-string v0, "Error flagging"

    invoke-static {v0, p2}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lbwa;->c:Leyt;

    invoke-interface {v0, p2}, Leyt;->c(Ljava/lang/Throwable;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lbwa;->a:Landroid/app/Activity;

    const v1, 0x7f0902a1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    return-void
.end method
