.class public final Libz;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhxr;

.field public b:[B

.field public c:Licp;

.field private d:Lhtx;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Libz;->d:Lhtx;

    const-string v0, ""

    iput-object v0, p0, Libz;->e:Ljava/lang/String;

    iput-object v1, p0, Libz;->a:Lhxr;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Libz;->b:[B

    iput-object v1, p0, Libz;->c:Licp;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Libz;->d:Lhtx;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Libz;->d:Lhtx;

    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-object v1, p0, Libz;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x2

    iget-object v2, p0, Libz;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Libz;->a:Lhxr;

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Libz;->a:Lhxr;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Libz;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Libz;->b:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Libz;->c:Licp;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Libz;->c:Licp;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Libz;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libz;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Libz;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Libz;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Libz;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Libz;->d:Lhtx;

    if-nez v0, :cond_2

    new-instance v0, Lhtx;

    invoke-direct {v0}, Lhtx;-><init>()V

    iput-object v0, p0, Libz;->d:Lhtx;

    :cond_2
    iget-object v0, p0, Libz;->d:Lhtx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Libz;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Libz;->a:Lhxr;

    if-nez v0, :cond_3

    new-instance v0, Lhxr;

    invoke-direct {v0}, Lhxr;-><init>()V

    iput-object v0, p0, Libz;->a:Lhxr;

    :cond_3
    iget-object v0, p0, Libz;->a:Lhxr;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Libz;->b:[B

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Libz;->c:Licp;

    if-nez v0, :cond_4

    new-instance v0, Licp;

    invoke-direct {v0}, Licp;-><init>()V

    iput-object v0, p0, Libz;->c:Licp;

    :cond_4
    iget-object v0, p0, Libz;->c:Licp;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-object v0, p0, Libz;->d:Lhtx;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Libz;->d:Lhtx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Libz;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Libz;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Libz;->a:Lhxr;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Libz;->a:Lhxr;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Libz;->b:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Libz;->b:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_3
    iget-object v0, p0, Libz;->c:Licp;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Libz;->c:Licp;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Libz;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libz;

    iget-object v2, p0, Libz;->d:Lhtx;

    if-nez v2, :cond_4

    iget-object v2, p1, Libz;->d:Lhtx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Libz;->e:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Libz;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Libz;->a:Lhxr;

    if-nez v2, :cond_6

    iget-object v2, p1, Libz;->a:Lhxr;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Libz;->b:[B

    iget-object v3, p1, Libz;->b:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libz;->c:Licp;

    if-nez v2, :cond_7

    iget-object v2, p1, Libz;->c:Licp;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Libz;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Libz;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Libz;->d:Lhtx;

    iget-object v3, p1, Libz;->d:Lhtx;

    invoke-virtual {v2, v3}, Lhtx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Libz;->e:Ljava/lang/String;

    iget-object v3, p1, Libz;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Libz;->a:Lhxr;

    iget-object v3, p1, Libz;->a:Lhxr;

    invoke-virtual {v2, v3}, Lhxr;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libz;->c:Licp;

    iget-object v3, p1, Libz;->c:Licp;

    invoke-virtual {v2, v3}, Licp;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Libz;->I:Ljava/util/List;

    iget-object v3, p1, Libz;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libz;->d:Lhtx;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libz;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libz;->a:Lhxr;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Libz;->b:[B

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Libz;->c:Licp;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Libz;->I:Ljava/util/List;

    if-nez v2, :cond_6

    :goto_4
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Libz;->d:Lhtx;

    invoke-virtual {v0}, Lhtx;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Libz;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Libz;->a:Lhxr;

    invoke-virtual {v0}, Lhxr;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_4
    move v2, v0

    move v0, v1

    :goto_5
    iget-object v3, p0, Libz;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Libz;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_5
    iget-object v0, p0, Libz;->c:Licp;

    invoke-virtual {v0}, Licp;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_6
    iget-object v1, p0, Libz;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_4
.end method
