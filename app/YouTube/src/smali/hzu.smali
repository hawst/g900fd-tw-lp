.class public final Lhzu;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhxf;

.field public b:Lhgz;

.field public c:Lhww;

.field public d:Lhog;

.field public e:Lhgz;

.field public f:Lhzv;

.field public g:[B

.field private h:[Lhbi;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v0, p0, Lhzu;->a:Lhxf;

    iput-object v0, p0, Lhzu;->b:Lhgz;

    iput-object v0, p0, Lhzu;->c:Lhww;

    iput-object v0, p0, Lhzu;->d:Lhog;

    iput-object v0, p0, Lhzu;->e:Lhgz;

    iput-object v0, p0, Lhzu;->f:Lhzv;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhzu;->g:[B

    sget-object v0, Lhbi;->a:[Lhbi;

    iput-object v0, p0, Lhzu;->h:[Lhbi;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lhzu;->a:Lhxf;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    iget-object v2, p0, Lhzu;->a:Lhxf;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhzu;->b:Lhgz;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhzu;->b:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhzu;->c:Lhww;

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget-object v3, p0, Lhzu;->c:Lhww;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Lhzu;->d:Lhog;

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iget-object v3, p0, Lhzu;->d:Lhog;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget-object v2, p0, Lhzu;->e:Lhgz;

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iget-object v3, p0, Lhzu;->e:Lhgz;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget-object v2, p0, Lhzu;->f:Lhzv;

    if-eqz v2, :cond_4

    const/4 v2, 0x6

    iget-object v3, p0, Lhzu;->f:Lhzv;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget-object v2, p0, Lhzu;->g:[B

    sget-object v3, Lidj;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_5

    const/16 v2, 0x8

    iget-object v3, p0, Lhzu;->g:[B

    invoke-static {v2, v3}, Lidd;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget-object v2, p0, Lhzu;->h:[Lhbi;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lhzu;->h:[Lhbi;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    if-eqz v4, :cond_6

    const/16 v5, 0xa

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lhzu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhzu;->J:I

    return v0

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhzu;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhzu;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhzu;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhzu;->a:Lhxf;

    if-nez v0, :cond_2

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhzu;->a:Lhxf;

    :cond_2
    iget-object v0, p0, Lhzu;->a:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhzu;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzu;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhzu;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhzu;->c:Lhww;

    if-nez v0, :cond_4

    new-instance v0, Lhww;

    invoke-direct {v0}, Lhww;-><init>()V

    iput-object v0, p0, Lhzu;->c:Lhww;

    :cond_4
    iget-object v0, p0, Lhzu;->c:Lhww;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhzu;->d:Lhog;

    if-nez v0, :cond_5

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhzu;->d:Lhog;

    :cond_5
    iget-object v0, p0, Lhzu;->d:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhzu;->e:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhzu;->e:Lhgz;

    :cond_6
    iget-object v0, p0, Lhzu;->e:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhzu;->f:Lhzv;

    if-nez v0, :cond_7

    new-instance v0, Lhzv;

    invoke-direct {v0}, Lhzv;-><init>()V

    iput-object v0, p0, Lhzu;->f:Lhzv;

    :cond_7
    iget-object v0, p0, Lhzu;->f:Lhzv;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhzu;->g:[B

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhzu;->h:[Lhbi;

    if-nez v0, :cond_9

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhbi;

    iget-object v3, p0, Lhzu;->h:[Lhbi;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lhzu;->h:[Lhbi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    iput-object v2, p0, Lhzu;->h:[Lhbi;

    :goto_2
    iget-object v2, p0, Lhzu;->h:[Lhbi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_a

    iget-object v2, p0, Lhzu;->h:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzu;->h:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lhzu;->h:[Lhbi;

    array-length v0, v0

    goto :goto_1

    :cond_a
    iget-object v2, p0, Lhzu;->h:[Lhbi;

    new-instance v3, Lhbi;

    invoke-direct {v3}, Lhbi;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhzu;->h:[Lhbi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x42 -> :sswitch_7
        0x52 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhzu;->a:Lhxf;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Lhzu;->a:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Lhzu;->b:Lhgz;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhzu;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Lhzu;->c:Lhww;

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lhzu;->c:Lhww;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhzu;->d:Lhog;

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-object v1, p0, Lhzu;->d:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhzu;->e:Lhgz;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhzu;->e:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhzu;->f:Lhzv;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lhzu;->f:Lhzv;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v0, p0, Lhzu;->g:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x8

    iget-object v1, p0, Lhzu;->g:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_6
    iget-object v0, p0, Lhzu;->h:[Lhbi;

    if-eqz v0, :cond_8

    iget-object v1, p0, Lhzu;->h:[Lhbi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_8

    aget-object v3, v1, v0

    if-eqz v3, :cond_7

    const/16 v4, 0xa

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lhzu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhzu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhzu;

    iget-object v2, p0, Lhzu;->a:Lhxf;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhzu;->a:Lhxf;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhzu;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhzu;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhzu;->c:Lhww;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhzu;->c:Lhww;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhzu;->d:Lhog;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhzu;->d:Lhog;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Lhzu;->e:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhzu;->e:Lhgz;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhzu;->f:Lhzv;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhzu;->f:Lhzv;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Lhzu;->g:[B

    iget-object v3, p1, Lhzu;->g:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhzu;->h:[Lhbi;

    iget-object v3, p1, Lhzu;->h:[Lhbi;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhzu;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhzu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhzu;->a:Lhxf;

    iget-object v3, p1, Lhzu;->a:Lhxf;

    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhzu;->b:Lhgz;

    iget-object v3, p1, Lhzu;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhzu;->c:Lhww;

    iget-object v3, p1, Lhzu;->c:Lhww;

    invoke-virtual {v2, v3}, Lhww;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhzu;->d:Lhog;

    iget-object v3, p1, Lhzu;->d:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhzu;->e:Lhgz;

    iget-object v3, p1, Lhzu;->e:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhzu;->f:Lhzv;

    iget-object v3, p1, Lhzu;->f:Lhzv;

    invoke-virtual {v2, v3}, Lhzv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhzu;->I:Ljava/util/List;

    iget-object v3, p1, Lhzu;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzu;->a:Lhxf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzu;->b:Lhgz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzu;->c:Lhww;

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzu;->d:Lhog;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzu;->e:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhzu;->f:Lhzv;

    if-nez v0, :cond_7

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    iget-object v2, p0, Lhzu;->g:[B

    if-nez v2, :cond_8

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    iget-object v0, p0, Lhzu;->h:[Lhbi;

    if-nez v0, :cond_9

    mul-int/lit8 v2, v2, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhzu;->I:Ljava/util/List;

    if-nez v2, :cond_b

    :goto_6
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Lhzu;->a:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lhzu;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lhzu;->c:Lhww;

    invoke-virtual {v0}, Lhww;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lhzu;->d:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lhzu;->e:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhzu;->f:Lhzv;

    invoke-virtual {v0}, Lhzv;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_8
    move v2, v0

    move v0, v1

    :goto_7
    iget-object v3, p0, Lhzu;->g:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhzu;->g:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    move v0, v1

    :goto_8
    iget-object v3, p0, Lhzu;->h:[Lhbi;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhzu;->h:[Lhbi;

    aget-object v2, v2, v0

    if-nez v2, :cond_a

    move v2, v1

    :goto_9
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_a
    iget-object v2, p0, Lhzu;->h:[Lhbi;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhbi;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_b
    iget-object v1, p0, Lhzu;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_6
.end method
