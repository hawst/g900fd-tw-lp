.class Ldpx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/logging/Logger;

.field private static final c:Ljava/lang/String;

.field private static d:Z


# instance fields
.field a:Ldqb;

.field private e:Ljava/net/InetSocketAddress;

.field private f:Ljava/io/ByteArrayOutputStream;

.field private g:I

.field private h:Ljava/io/ByteArrayOutputStream;

.field private i:Ldyv;

.field private j:I

.field private final k:Landroid/content/Context;

.field private l:Ljava/util/Map;

.field private m:Ldza;

.field private n:Ljava/io/ByteArrayOutputStream;

.field private o:Ljava/util/Map;

.field private p:Z

.field private final q:Ljava/util/concurrent/LinkedBlockingQueue;

.field private r:Ljava/io/ByteArrayOutputStream;

.field private s:I

.field private t:Ldqf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 261
    const-class v0, Ldpx;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Ldpx;->b:Ljava/util/logging/Logger;

    .line 262
    const-class v0, Ldpx;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldpx;->c:Ljava/lang/String;

    .line 263
    const/4 v0, 0x0

    sput-boolean v0, Ldpx;->d:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ldza;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    iput-object p2, p0, Ldpx;->m:Ldza;

    .line 302
    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p3, p4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Ldpx;->e:Ljava/net/InetSocketAddress;

    .line 303
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Ldpx;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 304
    const/4 v0, 0x0

    iput-object v0, p0, Ldpx;->i:Ldyv;

    .line 305
    sget-object v0, Ldqf;->f:Ldqf;

    iput-object v0, p0, Ldpx;->t:Ldqf;

    .line 306
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldpx;->p:Z

    .line 307
    iput-object p1, p0, Ldpx;->k:Landroid/content/Context;

    .line 308
    return-void
.end method

.method private a([BIIZ)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    .line 761
    invoke-static {p1, p2, p3}, Ldpx;->b([BII)I

    move-result v1

    .line 762
    if-eq v1, v5, :cond_8

    .line 763
    iget-object v0, p0, Ldpx;->n:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 764
    iget-object v0, p0, Ldpx;->n:Ljava/io/ByteArrayOutputStream;

    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 765
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz p4, :cond_3

    sget-object v0, Ldqf;->a:Ldqf;

    iput-object v0, p0, Ldpx;->t:Ldqf;

    iget-object v0, p0, Ldpx;->o:Ljava/util/Map;

    const-string v2, "transfer-encoding"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "chunked"

    iget-object v2, p0, Ldpx;->o:Ljava/util/Map;

    const-string v3, "transfer-encoding"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Ldqf;->d:Ldqf;

    iput-object v0, p0, Ldpx;->t:Ldqf;

    .line 766
    :cond_0
    :goto_0
    iget-object v0, p0, Ldpx;->n:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 767
    add-int/lit8 p3, v1, 0x2

    .line 772
    :goto_1
    return p3

    .line 765
    :cond_1
    new-instance v0, Ldqo;

    const-string v1, "Unknown transfer-encoding"

    invoke-direct {v0, v1}, Ldqo;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Ldpx;->o:Ljava/util/Map;

    const-string v2, "content-length"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldpx;->o:Ljava/util/Map;

    const-string v2, "content-length"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Ldpx;->j:I

    goto :goto_0

    :cond_3
    sget-object v0, Ldqf;->e:Ldqf;

    iput-object v0, p0, Ldpx;->t:Ldqf;

    iput-boolean v7, p0, Ldpx;->p:Z

    goto :goto_0

    :cond_4
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v5, :cond_5

    if-eqz p4, :cond_5

    iget v3, p0, Ldpx;->s:I

    if-nez v3, :cond_5

    const-string v2, "HTTP/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v5, :cond_0

    const-string v3, " "

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ldpx;->s:I

    iget-object v0, p0, Ldpx;->a:Ldqb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldpx;->a:Ldqb;

    iget v2, p0, Ldpx;->s:I

    invoke-interface {v0, v2}, Ldqb;->a(I)V

    goto :goto_0

    :cond_5
    iget v3, p0, Ldpx;->s:I

    if-nez v3, :cond_6

    new-instance v0, Ldqo;

    const-string v1, "Received header fields without HTTP response code"

    invoke-direct {v0, v1}, Ldqo;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "set-cookie"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v6

    const-string v2, "="

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Ldpx;->l:Ljava/util/Map;

    aget-object v3, v0, v6

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    iget-object v2, p0, Ldpx;->o:Ljava/util/Map;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 771
    :cond_8
    iget-object v0, p0, Ldpx;->n:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/16 :goto_1
.end method

.method static synthetic a(Ldpx;)Ldyv;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldpx;->i:Ldyv;

    return-object v0
.end method

.method private a([BII)V
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Ldpx;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 531
    iget-object v0, p0, Ldpx;->a:Ldqb;

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Ldpx;->a:Ldqb;

    invoke-interface {v0, p1, p2, p3}, Ldqb;->a([BII)V

    .line 534
    :cond_0
    return-void
.end method

.method private static b([BII)I
    .locals 3

    .prologue
    .line 828
    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v1, p2, -0x1

    if-ge v0, v1, :cond_1

    .line 829
    add-int v1, p1, v0

    aget-byte v1, p0, v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    add-int v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aget-byte v1, p0, v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 833
    :goto_1
    return v0

    .line 828
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 833
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic b(Ldpx;)Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ldpx;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Ldqc;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 345
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldpx;->k:Landroid/content/Context;

    invoke-static {v0}, La;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    sget-object v0, Ldpx;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No network connection - request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not sent."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    new-instance v0, Ldqs;

    invoke-direct {v0}, Ldqs;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 350
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Ldpx;->s:I

    .line 351
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Ldpx;->r:Ljava/io/ByteArrayOutputStream;

    .line 353
    sget-object v0, Ldqf;->f:Ldqf;

    iput-object v0, p0, Ldpx;->t:Ldqf;

    .line 354
    const/4 v0, 0x0

    iput v0, p0, Ldpx;->g:I

    .line 355
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldpx;->p:Z

    .line 358
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Ldpx;->n:Ljava/io/ByteArrayOutputStream;

    .line 359
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Ldpx;->h:Ljava/io/ByteArrayOutputStream;

    .line 360
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Ldpx;->f:Ljava/io/ByteArrayOutputStream;

    .line 362
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldpx;->o:Ljava/util/Map;

    .line 363
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Ldpx;->l:Ljava/util/Map;

    .line 366
    iget-object v0, p0, Ldpx;->i:Ldyv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    .line 368
    :try_start_2
    invoke-virtual {p0}, Ldpx;->a()V

    new-instance v0, Ldyv;

    iget-object v1, p0, Ldpx;->m:Ldza;

    new-instance v2, Ldpy;

    invoke-direct {v2, p0}, Ldpy;-><init>(Ldpx;)V

    invoke-direct {v0, v1, v2}, Ldyv;-><init>(Ldzr;Ldyp;)V

    iput-object v0, p0, Ldpx;->i:Ldyv;

    iget-object v0, p0, Ldpx;->i:Ldyv;

    iget-object v1, p0, Ldpx;->e:Ljava/net/InetSocketAddress;

    new-instance v2, Ldpz;

    invoke-direct {v2, p0}, Ldpz;-><init>(Ldpx;)V

    invoke-virtual {v0, v1, v2}, Ldyv;->a(Ljava/net/InetSocketAddress;Ldyx;)V

    iget-object v0, p0, Ldpx;->i:Ldyv;

    iget-object v1, v0, Ldyv;->c:Ldyz;

    if-eqz v1, :cond_3

    iget-object v0, v0, Ldyv;->c:Ldyz;

    invoke-virtual {v0}, Ldyz;->b()V
    :try_end_2
    .catch Ljava/lang/AssertionError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 375
    :cond_1
    :goto_0
    :try_start_3
    iget-object v0, p0, Ldpx;->i:Ldyv;

    iget-object v0, v0, Ldyv;->e:Ldzv;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 376
    iget-object v0, p0, Ldpx;->i:Ldyv;

    iget-object v1, v0, Ldyv;->c:Ldyz;

    if-eqz v1, :cond_4

    iget-object v0, v0, Ldyv;->c:Ldyz;

    invoke-virtual {v0}, Ldyz;->c()V

    .line 379
    :goto_1
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 383
    :cond_2
    iget-boolean v0, p0, Ldpx;->p:Z

    if-nez v0, :cond_14

    .line 385
    sget-object v0, Ldqf;->a:Ldqf;

    iget-object v2, p0, Ldpx;->t:Ldqf;

    invoke-virtual {v0, v2}, Ldqf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Ldpx;->j:I

    if-nez v0, :cond_5

    iget-object v0, p0, Ldpx;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 386
    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 389
    invoke-virtual {p0}, Ldpx;->a()V

    move v0, v3

    .line 481
    :goto_2
    iget-object v2, p0, Ldpx;->i:Ldyv;

    if-nez v2, :cond_12

    .line 482
    iget-object v0, p0, Ldpx;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 483
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 484
    :goto_3
    iget-object v0, p0, Ldpx;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 487
    iget-object v0, p0, Ldpx;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqd;

    .line 489
    invoke-virtual {v0}, Ldqd;->c()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 490
    const-string v0, "\nclosed event"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 368
    :cond_3
    const/4 v1, 0x1

    :try_start_4
    iput-boolean v1, v0, Ldyv;->g:Z
    :try_end_4
    .catch Ljava/lang/AssertionError; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 369
    :catch_0
    move-exception v0

    .line 370
    :try_start_5
    new-instance v1, Ldqo;

    const-string v2, "Could not open socket"

    invoke-direct {v1, v2, v0}, Ldqo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 376
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, v0, Ldyv;->h:Z

    goto :goto_1

    .line 397
    :cond_5
    iget-object v0, p0, Ldpx;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v6, 0x3c

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldqd;

    .line 404
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ldqd;->c()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 405
    :cond_6
    invoke-virtual {p0}, Ldpx;->a()V

    move v0, v3

    .line 407
    goto :goto_2

    .line 410
    :cond_7
    invoke-virtual {v0}, Ldqd;->d()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 411
    invoke-virtual {p0}, Ldpx;->a()V

    .line 412
    new-instance v1, Ldqo;

    const-string v2, "Error occurred in connection"

    invoke-virtual {v0}, Ldqd;->b()Ljava/lang/Exception;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Ldqo;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 416
    :cond_8
    invoke-virtual {v0}, Ldqd;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 418
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 422
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    .line 423
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    move v5, v4

    .line 427
    :goto_4
    if-eqz v2, :cond_2

    .line 429
    sget-object v0, Ldqa;->a:[I

    iget-object v6, p0, Ldpx;->t:Ldqf;

    invoke-virtual {v6}, Ldqf;->ordinal()I

    move-result v6

    aget v0, v0, v6

    packed-switch v0, :pswitch_data_0

    move v6, v2

    .line 460
    :goto_5
    if-nez v6, :cond_d

    iget-object v0, p0, Ldpx;->t:Ldqf;

    sget-object v8, Ldqf;->a:Ldqf;

    if-ne v0, v8, :cond_9

    iget v0, p0, Ldpx;->j:I

    if-eqz v0, :cond_d

    .line 470
    :cond_9
    new-instance v0, Ldqo;

    iget-object v1, p0, Ldpx;->t:Ldqf;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v3, p0, Ldpx;->j:I

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>([B)V

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit16 v8, v8, 0x89

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Unable to process data in state "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "\nlengthRemaining: "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\ncontentLength: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\noffset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nbytes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nRequest:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nResponse:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldqo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 438
    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v7, v5, v2, v0}, Ldpx;->a([BIIZ)I

    move-result v0

    :goto_6
    move v6, v0

    .line 457
    goto/16 :goto_5

    .line 441
    :pswitch_1
    iget v0, p0, Ldpx;->j:I

    iget-object v6, p0, Ldpx;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v6

    sub-int/2addr v0, v6

    if-lt v2, v0, :cond_16

    iget v0, p0, Ldpx;->j:I

    iget-object v6, p0, Ldpx;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v6

    sub-int/2addr v0, v6

    const/4 v6, 0x1

    iput-boolean v6, p0, Ldpx;->p:Z

    :goto_7
    invoke-direct {p0, v7, v5, v0}, Ldpx;->a([BII)V

    goto :goto_6

    :pswitch_2
    move v0, v2

    .line 446
    goto :goto_6

    .line 448
    :pswitch_3
    const/4 v0, 0x0

    invoke-direct {p0, v7, v5, v2, v0}, Ldpx;->a([BIIZ)I

    move-result v0

    goto :goto_6

    .line 451
    :pswitch_4
    invoke-static {v7, v5, v2}, Ldpx;->b([BII)I

    move-result v0

    const/4 v6, -0x1

    if-eq v0, v6, :cond_b

    sget-object v6, Ldqf;->b:Ldqf;

    iput-object v6, p0, Ldpx;->t:Ldqf;

    iget-object v6, p0, Ldpx;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6, v7, v5, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    iget-object v6, p0, Ldpx;->h:Ljava/io/ByteArrayOutputStream;

    const-string v8, "UTF-8"

    invoke-virtual {v6, v8}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/16 v8, 0x10

    invoke-static {v6, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Ldpx;->g:I

    iget v6, p0, Ldpx;->g:I

    if-nez v6, :cond_a

    sget-object v6, Ldqf;->g:Ldqf;

    iput-object v6, p0, Ldpx;->t:Ldqf;

    :cond_a
    iget-object v6, p0, Ldpx;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    add-int/lit8 v0, v0, 0x2

    :goto_8
    move v6, v0

    .line 452
    goto/16 :goto_5

    .line 451
    :cond_b
    iget-object v0, p0, Ldpx;->h:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v7, v5, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    move v0, v2

    goto :goto_8

    .line 454
    :pswitch_5
    iget v0, p0, Ldpx;->g:I

    if-lt v2, v0, :cond_15

    iget v0, p0, Ldpx;->g:I

    sget-object v6, Ldqf;->c:Ldqf;

    iput-object v6, p0, Ldpx;->t:Ldqf;

    :goto_9
    invoke-direct {p0, v7, v5, v0}, Ldpx;->a([BII)V

    iget v6, p0, Ldpx;->g:I

    sub-int/2addr v6, v0

    iput v6, p0, Ldpx;->g:I

    goto :goto_6

    .line 457
    :pswitch_6
    iget-object v0, p0, Ldpx;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    add-int v6, v2, v0

    const/4 v8, 0x2

    if-lt v6, v8, :cond_c

    rsub-int/lit8 v0, v0, 0x2

    iget-object v6, p0, Ldpx;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6, v7, v5, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    sget-object v6, Ldqf;->d:Ldqf;

    iput-object v6, p0, Ldpx;->t:Ldqf;

    iget-object v6, p0, Ldpx;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto/16 :goto_6

    :cond_c
    iget-object v0, p0, Ldpx;->f:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v7, v5, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    move v0, v2

    goto/16 :goto_6

    .line 476
    :cond_d
    add-int v0, v5, v6

    .line 477
    sub-int/2addr v2, v6

    move v5, v0

    .line 478
    goto/16 :goto_4

    .line 491
    :cond_e
    invoke-virtual {v0}, Ldqd;->d()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 492
    const-string v2, "\nerror: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ldqd;->b()Ljava/lang/Exception;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 494
    :cond_f
    const-string v2, "\ndata: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ldqd;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 497
    :cond_10
    new-instance v2, Ldqo;

    const-string v3, "Request completed with data still in the incoming queue:"

    .line 498
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_11

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_a
    invoke-direct {v2, v0}, Ldqo;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_a

    .line 500
    :cond_12
    iget-boolean v2, p0, Ldpx;->p:Z

    if-nez v2, :cond_13

    if-nez v0, :cond_13

    .line 501
    new-instance v0, Ldqo;

    const-string v1, "Abrupt connection end"

    invoke-direct {v0, v1}, Ldqo;-><init>(Ljava/lang/String;)V

    throw v0

    .line 504
    :cond_13
    iget-object v0, p0, Ldpx;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    .line 505
    new-instance v0, Ldqc;

    iget-boolean v2, p0, Ldpx;->p:Z

    iget v3, p0, Ldpx;->s:I

    iget-object v5, p0, Ldpx;->o:Ljava/util/Map;

    iget-object v6, p0, Ldpx;->l:Ljava/util/Map;

    invoke-direct/range {v0 .. v6}, Ldqc;-><init>(Ljava/io/ByteArrayOutputStream;ZI[BLjava/util/Map;Ljava/util/Map;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_14
    move v0, v4

    goto/16 :goto_2

    :cond_15
    move v0, v2

    goto/16 :goto_9

    :cond_16
    move v0, v2

    goto/16 :goto_7

    .line 429
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 315
    :try_start_0
    iget-object v0, p0, Ldpx;->i:Ldyv;

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Ldpx;->i:Ldyv;

    const/4 v1, 0x1

    iput-boolean v1, v0, Ldyv;->i:Z

    iget-object v1, v0, Ldyv;->b:Ldzs;

    if-eqz v1, :cond_0

    iget-object v1, v0, Ldyv;->b:Ldzs;

    invoke-virtual {v1}, Ldzs;->close()V

    const/4 v1, 0x0

    iput-object v1, v0, Ldyv;->b:Ldzs;

    :cond_0
    iget-object v1, v0, Ldyv;->c:Ldyz;

    if-eqz v1, :cond_1

    iget-object v0, v0, Ldyv;->c:Ldyz;

    iget-object v0, v0, Ldyz;->a:Ldzs;

    invoke-virtual {v0}, Ldzs;->close()V

    .line 317
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Ldpx;->i:Ldyv;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
