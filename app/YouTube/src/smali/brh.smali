.class public final Lbrh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Leyt;

.field final b:Levn;

.field final c:Landroid/app/Activity;

.field private final d:Lfeb;

.field private final e:Lgix;

.field private final f:Lcub;

.field private final g:Lglm;

.field private final h:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lgix;Lcub;Lfeb;Leyt;Levn;Lglm;)V
    .locals 1

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbrh;->c:Landroid/app/Activity;

    .line 293
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfeb;

    iput-object v0, p0, Lbrh;->d:Lfeb;

    .line 294
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbrh;->e:Lgix;

    .line 295
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcub;

    iput-object v0, p0, Lbrh;->f:Lcub;

    .line 296
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbrh;->a:Leyt;

    .line 297
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lbrh;->b:Levn;

    .line 298
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lglm;

    iput-object v0, p0, Lbrh;->g:Lglm;

    .line 299
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbrh;->h:Ljava/util/List;

    .line 300
    return-void
.end method

.method static synthetic a(Lbrh;Lbrg;Lfkn;)V
    .locals 3

    .prologue
    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbrh;->g:Lglm;

    invoke-interface {v0}, Lglm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbrh;->c:Landroid/app/Activity;

    const v1, 0x7f0901d9

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Leze;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lbrh;->e:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lbrh;->a(Lbrg;Lfkn;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbrh;->f:Lcub;

    iget-object v1, p0, Lbrh;->c:Landroid/app/Activity;

    new-instance v2, Lbri;

    invoke-direct {v2, p0, p1, p2}, Lbri;-><init>(Lbrh;Lbrg;Lfkn;)V

    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    goto :goto_0
.end method

.method private b(ILfkn;)V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lbrh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrm;

    .line 342
    invoke-virtual {v0, p1, p2}, Lbrm;->b(ILfkn;)V

    goto :goto_0

    .line 344
    :cond_0
    return-void
.end method

.method static b(Lfkn;)Z
    .locals 1

    .prologue
    .line 384
    if-eqz p0, :cond_0

    .line 385
    iget-object v0, p0, Lfkn;->a:Lhlk;

    iget-object v0, v0, Lhlk;->a:Lhlq;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lfkn;->a:Lhlk;

    iget-object v0, v0, Lhlk;->a:Lhlq;

    iget-object v0, v0, Lhlq;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(ILfkn;)V
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lbrh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrm;

    .line 348
    invoke-virtual {v0, p1, p2}, Lbrm;->a(ILfkn;)V

    goto :goto_0

    .line 350
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 303
    iget-object v0, p0, Lbrh;->h:Ljava/util/List;

    new-instance v1, Lbrn;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lbrn;-><init>(Landroid/view/View;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 304
    return-void
.end method

.method public final a(Lbrg;)V
    .locals 2

    .prologue
    .line 337
    iget v0, p1, Lbrg;->f:I

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbrh;->b(ILfkn;)V

    .line 338
    return-void
.end method

.method a(Lbrg;Lfkn;)V
    .locals 3

    .prologue
    .line 392
    new-instance v0, Lbrj;

    invoke-direct {v0, p0, p2, p1}, Lbrj;-><init>(Lbrh;Lfkn;Lbrg;)V

    .line 416
    sget-object v1, Lbrk;->a:[I

    invoke-virtual {p1}, Lbrg;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 436
    :goto_0
    return-void

    .line 418
    :pswitch_0
    iget-object v1, p0, Lbrh;->d:Lfeb;

    invoke-virtual {v1}, Lfeb;->a()Lfef;

    move-result-object v1

    .line 419
    iget-object v2, p2, Lfkn;->a:Lhlk;

    iget-object v2, v2, Lhlk;->k:[B

    invoke-virtual {v1, v2}, Lfef;->a([B)V

    .line 420
    iget-object v2, p2, Lfkn;->a:Lhlk;

    iget-object v2, v2, Lhlk;->a:Lhlq;

    invoke-virtual {v1, v2}, Lfef;->a(Lhlq;)Lfec;

    .line 421
    iget-object v2, p0, Lbrh;->d:Lfeb;

    invoke-virtual {v2, v1, v0}, Lfeb;->a(Lfef;Lwv;)V

    goto :goto_0

    .line 424
    :pswitch_1
    iget-object v1, p0, Lbrh;->d:Lfeb;

    invoke-virtual {v1}, Lfeb;->b()Lfed;

    move-result-object v1

    .line 425
    iget-object v2, p2, Lfkn;->a:Lhlk;

    iget-object v2, v2, Lhlk;->k:[B

    invoke-virtual {v1, v2}, Lfed;->a([B)V

    .line 426
    iget-object v2, p2, Lfkn;->a:Lhlk;

    iget-object v2, v2, Lhlk;->a:Lhlq;

    invoke-virtual {v1, v2}, Lfed;->a(Lhlq;)Lfec;

    .line 427
    iget-object v2, p0, Lbrh;->d:Lfeb;

    invoke-virtual {v2, v1, v0}, Lfeb;->a(Lfed;Lwv;)V

    goto :goto_0

    .line 430
    :pswitch_2
    iget-object v1, p0, Lbrh;->d:Lfeb;

    invoke-virtual {v1}, Lfeb;->c()Lfeh;

    move-result-object v1

    .line 431
    iget-object v2, p2, Lfkn;->a:Lhlk;

    iget-object v2, v2, Lhlk;->k:[B

    invoke-virtual {v1, v2}, Lfeh;->a([B)V

    .line 432
    iget-object v2, p2, Lfkn;->a:Lhlk;

    iget-object v2, v2, Lhlk;->a:Lhlq;

    invoke-virtual {v1, v2}, Lfeh;->a(Lhlq;)Lfec;

    .line 433
    iget-object v2, p0, Lbrh;->d:Lfeb;

    invoke-virtual {v2, v1, v0}, Lfeb;->a(Lfeh;Lwv;)V

    goto :goto_0

    .line 416
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lfkn;)V
    .locals 4

    .prologue
    .line 315
    if-nez p1, :cond_0

    .line 316
    iget-object v0, p0, Lbrh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrm;

    .line 317
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lbrm;->a(I)V

    goto :goto_0

    .line 322
    :cond_0
    iget-object v0, p0, Lbrh;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrm;

    .line 323
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbrm;->a(I)V

    .line 324
    iget-object v1, p1, Lfkn;->a:Lhlk;

    iget-boolean v1, v1, Lhlk;->l:Z

    invoke-virtual {v0, v1}, Lbrm;->a(Z)V

    .line 325
    new-instance v3, Lbrl;

    .line 326
    iget-boolean v1, v0, Lbrm;->a:Z

    if-eqz v1, :cond_1

    sget-object v1, Lbrg;->b:Lbrg;

    :goto_2
    invoke-direct {v3, p0, p1, v1}, Lbrl;-><init>(Lbrh;Lfkn;Lbrg;)V

    .line 325
    iget-object v0, v0, Lbrm;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 326
    :cond_1
    sget-object v1, Lbrg;->a:Lbrg;

    goto :goto_2

    .line 329
    :cond_2
    invoke-static {p1}, Lbrh;->b(Lfkn;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 330
    iget-object v0, p1, Lfkn;->a:Lhlk;

    iget v0, v0, Lhlk;->b:I

    invoke-direct {p0, v0, p1}, Lbrh;->b(ILfkn;)V

    .line 334
    :cond_3
    :goto_3
    return-void

    .line 332
    :cond_4
    iget-object v0, p1, Lfkn;->a:Lhlk;

    iget v0, v0, Lhlk;->b:I

    invoke-virtual {p0, v0, p1}, Lbrh;->a(ILfkn;)V

    goto :goto_3
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 307
    iget-object v0, p0, Lbrh;->h:Ljava/util/List;

    new-instance v1, Lbrm;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lbrm;-><init>(Landroid/view/View;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    return-void
.end method

.method public final c(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lbrh;->h:Ljava/util/List;

    new-instance v1, Lbrn;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lbrn;-><init>(Landroid/view/View;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 312
    return-void
.end method
