.class public final Larh;
.super Lcmp;
.source "SourceFile"

# interfaces
.implements Lbin;


# instance fields
.field final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    const-string v0, "youtube"

    invoke-direct {p0, p1, v0}, Lcmp;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    .line 44
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Larh;->a:Landroid/content/Context;

    .line 45
    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/util/SparseBooleanArray;
    .locals 6

    .prologue
    .line 64
    new-instance v1, Landroid/util/SparseBooleanArray;

    invoke-direct {v1}, Landroid/util/SparseBooleanArray;-><init>()V

    .line 65
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 67
    :try_start_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    return-object v1

    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method private static a(Lorg/json/JSONObject;)Lhjw;
    .locals 5

    .prologue
    .line 285
    new-instance v1, Lhjw;

    invoke-direct {v1}, Lhjw;-><init>()V

    .line 286
    const-string v0, "capability"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v1, Lhjw;->b:I

    .line 287
    const-string v0, "features"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 288
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, v1, Lhjw;->c:[I

    .line 289
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 290
    iget-object v3, v1, Lhjw;->c:[I

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v4

    aput v4, v3, v0

    .line 289
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 292
    :cond_0
    return-object v1
.end method

.method private static b(Ljava/lang/String;)Lhbx;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 248
    new-instance v0, Lhbx;

    invoke-direct {v0}, Lhbx;-><init>()V

    .line 249
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 280
    :cond_0
    return-object v0

    .line 255
    :cond_1
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :try_start_1
    const-string v2, "supportedCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 262
    const-string v2, "supportedCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 263
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v2, v2, [Lhjw;

    iput-object v2, v0, Lhbx;->a:[Lhjw;

    move v2, v1

    .line 264
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 265
    iget-object v5, v0, Lhbx;->a:[Lhjw;

    .line 266
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 265
    invoke-static {v6}, Larh;->a(Lorg/json/JSONObject;)Lhjw;

    move-result-object v6

    aput-object v6, v5, v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 264
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 256
    :catch_0
    move-exception v0

    .line 257
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Improper JSON syntax encountered in capabilities override."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 269
    :cond_2
    :try_start_2
    const-string v2, "disabledCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 270
    const-string v2, "disabledCapabilities"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 271
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    new-array v3, v3, [Lhjw;

    iput-object v3, v0, Lhbx;->b:[Lhjw;

    .line 272
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 273
    iget-object v3, v0, Lhbx;->b:[Lhjw;

    .line 274
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 273
    invoke-static {v4}, Larh;->a(Lorg/json/JSONObject;)Lhjw;

    move-result-object v4

    aput-object v4, v3, v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 272
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 277
    :catch_1
    move-exception v0

    .line 278
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Improper capabilities override syntax encountered."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 48
    const-string v0, "min_app_version_5_7"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Larh;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;)Lhbx;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 203
    .line 204
    invoke-static {p1}, La;->n(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    const-string v1, "innertube_capability_overrides"

    invoke-virtual {p0, v1, v0}, Larh;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    :cond_0
    invoke-static {v0}, Larh;->b(Ljava/lang/String;)Lhbx;

    move-result-object v0

    .line 208
    return-object v0
.end method

.method final a(Ljava/lang/String;Lwr;)Lwr;
    .locals 1

    .prologue
    .line 147
    invoke-virtual {p2}, Lwr;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Larh;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    :try_start_0
    invoke-static {v0}, Lwr;->valueOf(Ljava/lang/String;)Lwr;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 151
    :goto_0
    return-object p2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 52
    const-string v0, "target_app_version_5_7"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Larh;->a(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final c()Landroid/util/SparseBooleanArray;
    .locals 2

    .prologue
    .line 56
    const-string v0, "blacklisted_app_versions_5_7"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Larh;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Larh;->a(Ljava/lang/String;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/util/SparseBooleanArray;
    .locals 2

    .prologue
    .line 60
    const-string v0, "discouraged_app_versions_5_7"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Larh;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Larh;->a(Ljava/lang/String;)Landroid/util/SparseBooleanArray;

    move-result-object v0

    return-object v0
.end method

.method public final e()J
    .locals 4

    .prologue
    .line 76
    const-string v0, "time_between_upgrade_prompts_millis"

    const-wide/32 v2, 0x240c8400

    invoke-virtual {p0, v0, v2, v3}, Larh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->d()Lhgf;

    move-result-object v0

    iget-boolean v0, v0, Lhgf;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Larh;->a:Landroid/content/Context;

    invoke-static {v0}, La;->m(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x1

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    return v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 125
    const-string v0, "ads_pings_send_user_auth"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Larh;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 129
    const-string v0, "ads_pings_send_visitor_id"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Larh;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lfji;->a:Lheg;

    iget-object v1, v1, Lheg;->b:Lhhi;

    iget-object v1, v1, Lhhi;->i:Lgzw;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lfji;->a:Lheg;

    iget-object v0, v0, Lheg;->b:Lhhi;

    iget-object v0, v0, Lhhi;->i:Lgzw;

    :goto_0
    iget v0, v0, Lgzw;->a:I

    return v0

    :cond_0
    iget-object v1, v0, Lfji;->c:Lgzw;

    if-nez v1, :cond_1

    new-instance v1, Lgzw;

    invoke-direct {v1}, Lgzw;-><init>()V

    iput-object v1, v0, Lfji;->c:Lgzw;

    :cond_1
    iget-object v0, v0, Lfji;->c:Lgzw;

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 175
    invoke-virtual {p0}, Larh;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->e()Lgzy;

    move-result-object v0

    iget-boolean v0, v0, Lgzy;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->e()Lgzy;

    move-result-object v0

    iget v0, v0, Lgzy;->b:I

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->e()Lgzy;

    move-result-object v0

    iget-boolean v0, v0, Lgzy;->c:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p0}, Larh;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->f()Lgzx;

    move-result-object v0

    iget-boolean v0, v0, Lgzx;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->f()Lgzx;

    move-result-object v0

    iget-boolean v0, v0, Lgzx;->b:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcmp;->b:Lfhw;

    iget-object v0, v0, Lfhw;->a:Lfji;

    invoke-virtual {v0}, Lfji;->f()Lgzx;

    move-result-object v0

    iget-boolean v0, v0, Lgzx;->c:Z

    return v0
.end method
