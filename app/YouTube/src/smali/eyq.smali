.class public final Leyq;
.super Landroid/graphics/drawable/BitmapDrawable;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:Landroid/graphics/Rect;

.field private final c:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;I)V
    .locals 2

    .prologue
    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    int-to-float v1, p3

    invoke-static {p1, p2, v1}, Leze;->a(Landroid/content/Context;Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 23
    iput p3, p0, Leyq;->a:I

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Leyq;->b:Landroid/graphics/Rect;

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Leyq;->c:Landroid/graphics/Rect;

    .line 26
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 30
    invoke-virtual {p0}, Leyq;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 31
    invoke-virtual {p0}, Leyq;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    .line 32
    iget-object v2, p0, Leyq;->c:Landroid/graphics/Rect;

    invoke-virtual {p0, v2}, Leyq;->copyBounds(Landroid/graphics/Rect;)V

    .line 34
    iget-object v2, p0, Leyq;->c:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Leyq;->a:I

    sub-int/2addr v2, v3

    .line 35
    int-to-float v3, v2

    iget-object v4, p0, Leyq;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Leyq;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 36
    iget-object v4, p0, Leyq;->b:Landroid/graphics/Rect;

    iget-object v5, p0, Leyq;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iput v5, v4, Landroid/graphics/Rect;->left:I

    .line 37
    iget-object v4, p0, Leyq;->b:Landroid/graphics/Rect;

    iget-object v5, p0, Leyq;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 38
    iget-object v4, p0, Leyq;->b:Landroid/graphics/Rect;

    iget-object v5, p0, Leyq;->c:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v5

    iput v2, v4, Landroid/graphics/Rect;->right:I

    .line 39
    iget-object v2, p0, Leyq;->b:Landroid/graphics/Rect;

    iget-object v4, p0, Leyq;->c:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 41
    iget-object v2, p0, Leyq;->b:Landroid/graphics/Rect;

    iget-object v3, p0, Leyq;->c:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 42
    return-void
.end method
