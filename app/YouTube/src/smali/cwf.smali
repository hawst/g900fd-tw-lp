.class public final Lcwf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgod;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:J


# direct methods
.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcwf;->a:Ljava/lang/String;

    .line 23
    iput p2, p0, Lcwf;->b:I

    .line 24
    iput-wide p3, p0, Lcwf;->c:J

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 29
    packed-switch p2, :pswitch_data_0

    .line 37
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 31
    :pswitch_0
    iget-object v0, p0, Lcwf;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcwf;->a:Ljava/lang/String;

    goto :goto_0

    .line 33
    :pswitch_1
    iget v0, p0, Lcwf;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 35
    :pswitch_2
    iget-wide v0, p0, Lcwf;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 29
    nop

    :pswitch_data_0
    .packed-switch 0x2e
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
