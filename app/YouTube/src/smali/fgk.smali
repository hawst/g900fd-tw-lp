.class public final Lfgk;
.super Lfcn;
.source "SourceFile"

# interfaces
.implements Lfdg;


# static fields
.field private static final e:Ljava/util/List;


# instance fields
.field private final f:Lfgm;

.field private final g:Lfth;

.field private final h:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lfgk;->e:Ljava/util/List;

    .line 41
    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lfcn;-><init>()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lfgk;->f:Lfgm;

    .line 95
    sget-object v0, Lfth;->a:Lfth;

    iput-object v0, p0, Lfgk;->g:Lfth;

    .line 96
    sget-object v0, Lfgk;->e:Ljava/util/List;

    iput-object v0, p0, Lfgk;->h:Ljava/util/List;

    .line 97
    return-void
.end method

.method public constructor <init>(Lftb;Lfsz;Lgix;Lws;)V
    .locals 8

    .prologue
    .line 64
    sget-object v5, Lfau;->a:Lfau;

    sget-object v6, Lfth;->a:Lfth;

    sget-object v7, Lfgk;->e:Ljava/util/List;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v7}, Lfgk;-><init>(Lftb;Lfsz;Lgix;Lws;Lfau;Lfth;Ljava/util/List;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Lftb;Lfsz;Lgix;Lws;Lfau;Lfth;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0, p1, p2, p3, p4}, Lfcn;-><init>(Lftb;Lfsz;Lgix;Lws;)V

    .line 87
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfth;

    iput-object v0, p0, Lfgk;->g:Lfth;

    .line 88
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lfgk;->h:Ljava/util/List;

    .line 89
    new-instance v0, Lfgm;

    invoke-direct {v0, p0, p5}, Lfgm;-><init>(Lfgk;Lfau;)V

    iput-object v0, p0, Lfgk;->f:Lfgm;

    .line 90
    return-void
.end method


# virtual methods
.method public final a()Lfgn;
    .locals 4

    .prologue
    .line 148
    new-instance v1, Lfgn;

    iget-object v0, p0, Lfgk;->b:Lfsz;

    iget-object v2, p0, Lfgk;->c:Lgix;

    .line 150
    invoke-interface {v2}, Lgix;->d()Lgit;

    move-result-object v2

    iget-object v3, p0, Lfgk;->g:Lfth;

    invoke-direct {v1, v0, v2, v3}, Lfgn;-><init>(Lfsz;Lgit;Lfth;)V

    .line 152
    iget-object v0, p0, Lfgk;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfgo;

    .line 153
    if-eqz v0, :cond_0

    .line 154
    invoke-interface {v0, v1}, Lfgo;->a(Lfgn;)V

    goto :goto_0

    .line 157
    :cond_1
    return-object v1
.end method

.method public final a(Lfgn;Lwv;)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lfgk;->f:Lfgm;

    invoke-virtual {v0, p1, p2}, Lfgm;->b(Lfsp;Lwv;)V

    .line 109
    return-void
.end method

.method public final a(Lfjk;Lwv;)V
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p0}, Lfgk;->a()Lfgn;

    move-result-object v0

    .line 129
    invoke-virtual {v0, p1}, Lfgn;->a(Lfjk;)Lfgn;

    .line 130
    new-instance v1, Lfgl;

    invoke-direct {v1, p0, p2}, Lfgl;-><init>(Lfgk;Lwv;)V

    invoke-virtual {p0, v0, v1}, Lfgk;->a(Lfgn;Lwv;)V

    .line 142
    return-void
.end method
