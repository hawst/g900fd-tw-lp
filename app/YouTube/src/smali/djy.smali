.class Ldjy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public volatile a:Z

.field private final b:I

.field private synthetic c:Ldjx;


# direct methods
.method public constructor <init>(Ldjx;I)V
    .locals 0

    .prologue
    .line 474
    iput-object p1, p0, Ldjy;->c:Ldjx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475
    iput p2, p0, Ldjy;->b:I

    .line 476
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 564
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget v1, p0, Ldjy;->b:I

    iput v1, v0, Ldjx;->v:I

    .line 565
    return-void
.end method

.method static synthetic a(Ldjy;)V
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldjy;->a:Z

    return-void
.end method

.method static synthetic a(Ldjy;Lfrl;)V
    .locals 0

    .prologue
    .line 469
    invoke-direct {p0, p1}, Ldjy;->a(Lfrl;)V

    return-void
.end method

.method static synthetic a(Ldjy;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 469
    iget-boolean v0, p0, Ldjy;->a:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Ldjy;->a()V

    iget-object v1, p0, Ldjy;->c:Ldjx;

    new-instance v2, Lczb;

    iget-object v0, p0, Ldjy;->c:Ldjx;

    sget-object v0, Lczc;->d:Lczc;

    invoke-static {p1}, Lfaq;->a(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v3

    instance-of v4, v3, Lgls;

    if-nez v4, :cond_0

    instance-of v3, v3, Lglo;

    if-eqz v3, :cond_1

    :cond_0
    sget-object v0, Lczc;->i:Lczc;

    :cond_1
    const/4 v3, 0x0

    iget-object v4, p0, Ldjy;->c:Ldjx;

    iget-object v4, v4, Ldjx;->d:Leyt;

    invoke-interface {v4, p1}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4, p1}, Lczb;-><init>(Lczc;ZLjava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2}, Ldjx;->a(Lczb;)V

    :cond_2
    return-void
.end method

.method private a(Lfrl;)V
    .locals 2

    .prologue
    .line 576
    iget-boolean v0, p0, Ldjy;->a:Z

    if-eqz v0, :cond_0

    .line 586
    :goto_0
    return-void

    .line 579
    :cond_0
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iput-object p1, v0, Ldjx;->g:Lfrl;

    .line 580
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->f:Lgok;

    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {v0, v1}, Lgok;->a(Lgok;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 581
    iget-object v0, p0, Ldjy;->c:Ldjx;

    sget-object v1, Lgok;->d:Lgok;

    invoke-virtual {v0, v1}, Ldjx;->a(Lgok;)V

    .line 584
    :cond_1
    invoke-direct {p0}, Ldjy;->a()V

    .line 585
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->b:Lcvq;

    invoke-interface {v0, p1}, Lcvq;->a(Lfrl;)V

    goto :goto_0
.end method

.method private a(Lgcd;Ljava/lang/Exception;)V
    .locals 6

    .prologue
    .line 591
    new-instance v0, Lhzc;

    invoke-direct {v0}, Lhzc;-><init>()V

    .line 592
    iget-object v1, p1, Lgcd;->b:Ljava/lang/String;

    iput-object v1, v0, Lhzc;->a:Ljava/lang/String;

    .line 593
    iget-object v1, p1, Lgcd;->j:Ljava/lang/String;

    iput-object v1, v0, Lhzc;->b:Ljava/lang/String;

    .line 595
    new-instance v1, Lhqn;

    invoke-direct {v1}, Lhqn;-><init>()V

    .line 596
    const/4 v2, 0x2

    iput v2, v1, Lhqn;->a:I

    .line 597
    iget-object v2, p0, Ldjy;->c:Ldjx;

    iget-object v2, v2, Ldjx;->d:Leyt;

    invoke-interface {v2, p2}, Leyt;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lhqn;->b:Ljava/lang/String;

    .line 600
    new-instance v2, Lhpl;

    invoke-direct {v2}, Lhpl;-><init>()V

    .line 601
    const/4 v3, 0x1

    iput v3, v2, Lhpl;->d:I

    .line 603
    new-instance v3, Lhro;

    invoke-direct {v3}, Lhro;-><init>()V

    .line 604
    iput-object v0, v3, Lhro;->g:Lhzc;

    .line 605
    iput-object v1, v3, Lhro;->a:Lhqn;

    .line 606
    iput-object v2, v3, Lhro;->i:Lhpl;

    .line 608
    new-instance v0, Lfrl;

    const-wide/16 v4, 0x0

    invoke-direct {v0, v3, v4, v5}, Lfrl;-><init>(Lhro;J)V

    invoke-direct {p0, v0}, Ldjy;->a(Lfrl;)V

    .line 609
    return-void
.end method

.method static synthetic b(Ldjy;)V
    .locals 2

    .prologue
    .line 469
    iget-boolean v0, p0, Ldjy;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldjy;->c:Ldjx;

    sget-object v1, Lgok;->f:Lgok;

    invoke-virtual {v0, v1}, Ldjx;->a(Lgok;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 0

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Ldjy;->b(I)Z

    .line 500
    return-void
.end method

.method protected final b(I)Z
    .locals 4

    .prologue
    .line 531
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgcd;

    .line 532
    iget-object v1, v0, Lgcd;->b:Ljava/lang/String;

    .line 534
    :try_start_0
    iget-object v2, p0, Ldjy;->c:Ldjx;

    iget-object v2, v2, Ldjx;->n:Lgnd;

    invoke-interface {v2, v1}, Lgnd;->n(Ljava/lang/String;)Lfrl;

    move-result-object v1

    .line 535
    if-nez v1, :cond_0

    .line 536
    new-instance v1, Lglr;

    invoke-direct {v1}, Lglr;-><init>()V

    throw v1
    :try_end_0
    .catch Lgls; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lglo; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 545
    :catch_0
    move-exception v1

    .line 549
    invoke-direct {p0, v0, v1}, Ldjy;->a(Lgcd;Ljava/lang/Exception;)V

    .line 560
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 539
    :cond_0
    :try_start_1
    iget-object v2, p0, Ldjy;->c:Ldjx;

    iget-object v2, v2, Ldjx;->p:Ljava/util/concurrent/Executor;

    new-instance v3, Ldka;

    invoke-direct {v3, p0, v1}, Ldka;-><init>(Ldjy;Lfrl;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Lgls; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lglo; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 550
    :catch_1
    move-exception v1

    .line 554
    invoke-direct {p0, v0, v1}, Ldjy;->a(Lgcd;Ljava/lang/Exception;)V

    goto :goto_0

    .line 555
    :catch_2
    move-exception v0

    .line 556
    iget-object v1, p0, Ldjy;->c:Ldjx;

    iget-object v1, v1, Ldjx;->p:Ljava/util/concurrent/Executor;

    new-instance v2, Ldkb;

    invoke-direct {v2, p0, v0}, Ldkb;-><init>(Ldjy;Ljava/lang/Exception;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 557
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 485
    iget v0, p0, Ldjy;->b:I

    .line 486
    iget-object v1, p0, Ldjy;->c:Ldjx;

    iget-boolean v1, v1, Ldjx;->k:Z

    if-eqz v1, :cond_6

    .line 487
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->u:[I

    iget v1, p0, Ldjy;->b:I

    aget v0, v0, v1

    move v1, v0

    .line 490
    :goto_0
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->t:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Ldjy;->c:Ldjx;

    invoke-virtual {v0}, Ldjx;->u()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Ldjx;->n:Lgnd;

    iget-object v0, v0, Ldjx;->r:Ljava/lang/String;

    invoke-interface {v2, v0}, Lgnd;->l(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    move-object v2, v0

    :goto_1
    iget-object v3, p0, Ldjy;->c:Ldjx;

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lgbu;

    iput-object v0, v3, Ldjx;->s:Lgbu;

    iget-object v3, p0, Ldjy;->c:Ldjx;

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iput-object v0, v3, Ldjx;->t:Ljava/util/List;

    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, Ldjy;->c:Ldjx;

    new-array v0, v0, [I

    iput-object v0, v2, Ldjx;->u:[I

    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-boolean v0, v0, Ldjx;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldjy;->c:Ldjx;

    invoke-static {v0}, Ldjx;->a(Ldjx;)V

    :cond_0
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->p:Ljava/util/concurrent/Executor;

    new-instance v2, Ldjz;

    invoke-direct {v2, p0}, Ldjz;-><init>(Ldjy;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 491
    :cond_1
    iget-object v0, p0, Ldjy;->c:Ldjx;

    iget-object v0, v0, Ldjx;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    if-gez v1, :cond_5

    .line 496
    :cond_2
    :goto_2
    return-void

    .line 490
    :cond_3
    iget-object v2, v0, Ldjx;->n:Lgnd;

    iget-object v0, v0, Ldjx;->q:Ljava/lang/String;

    invoke-interface {v2, v0}, Lgnd;->m(Ljava/lang/String;)Lgcd;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_3
    new-instance v2, Landroid/util/Pair;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_3

    .line 495
    :cond_5
    invoke-virtual {p0, v1}, Ldjy;->a(I)V

    goto :goto_2

    :cond_6
    move v1, v0

    goto/16 :goto_0
.end method
