.class public final Lfnq;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;


# instance fields
.field public final a:Liat;

.field private b:Lfnc;

.field private c:Lfnc;

.field private d:Lfnc;

.field private e:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Liat;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lfic;-><init>()V

    .line 23
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liat;

    iput-object v0, p0, Lfnq;->a:Liat;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 61
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 62
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lfnq;->a:Liat;

    iget-object v0, v0, Liat;->d:Lhog;

    return-object v0
.end method

.method public final c()Lfnc;
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lfnq;->b:Lfnc;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfnq;->a:Liat;

    iget-object v1, v1, Liat;->a:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfnq;->b:Lfnc;

    .line 30
    :cond_0
    iget-object v0, p0, Lfnq;->b:Lfnc;

    return-object v0
.end method

.method public final d()Lfnc;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lfnq;->c:Lfnc;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfnq;->a:Liat;

    iget-object v1, v1, Liat;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfnq;->c:Lfnc;

    .line 37
    :cond_0
    iget-object v0, p0, Lfnq;->c:Lfnc;

    return-object v0
.end method

.method public final e()Lfnc;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lfnq;->d:Lfnc;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfnq;->a:Liat;

    iget-object v1, v1, Liat;->c:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfnq;->d:Lfnc;

    .line 44
    :cond_0
    iget-object v0, p0, Lfnq;->d:Lfnc;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lfnq;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lfnq;->a:Liat;

    iget-object v0, v0, Liat;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnq;->e:Ljava/lang/CharSequence;

    .line 51
    :cond_0
    iget-object v0, p0, Lfnq;->e:Ljava/lang/CharSequence;

    return-object v0
.end method
