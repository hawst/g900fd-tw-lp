.class public final Lifv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lift;

.field private c:Landroid/os/Handler;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lifv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lifv;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lift;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lifv;->b:Lift;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Handler;I)V
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lifv;->c:Landroid/os/Handler;

    .line 39
    iput p2, p0, Lifv;->d:I

    .line 40
    return-void
.end method

.method public final onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lifv;->b:Lift;

    iget-object v0, v0, Lift;->c:Landroid/graphics/Point;

    .line 44
    iget-object v1, p0, Lifv;->c:Landroid/os/Handler;

    .line 45
    if-eqz v1, :cond_0

    .line 46
    iget v2, p0, Lifv;->d:I

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3, v0, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lifv;->c:Landroid/os/Handler;

    .line 53
    :goto_0
    return-void

    .line 51
    :cond_0
    sget-object v0, Lifv;->a:Ljava/lang/String;

    goto :goto_0
.end method
