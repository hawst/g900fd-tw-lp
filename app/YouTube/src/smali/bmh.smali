.class final Lbmh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldiy;


# instance fields
.field a:J

.field b:J

.field private c:I

.field private d:J

.field private synthetic e:Lbmf;


# direct methods
.method constructor <init>(Lbmf;)V
    .locals 1

    .prologue
    .line 568
    iput-object p1, p0, Lbmh;->e:Lbmf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 569
    const/4 v0, -0x1

    iput v0, p0, Lbmh;->c:I

    .line 570
    return-void
.end method


# virtual methods
.method public final a(Lfqj;J)V
    .locals 8

    .prologue
    .line 575
    iget-wide v0, p0, Lbmh;->a:J

    add-long/2addr v0, p2

    .line 576
    long-to-double v2, v0

    iget-wide v4, p0, Lbmh;->b:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    .line 584
    iget v3, p0, Lbmh;->c:I

    if-ltz v3, :cond_0

    iget v3, p0, Lbmh;->c:I

    sub-int v3, v2, v3

    if-gtz v3, :cond_0

    iget-wide v4, p0, Lbmh;->d:J

    sub-long v4, v0, v4

    const-wide/32 v6, 0x400000

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 587
    iget-object v3, p1, Lfqj;->a:Lhgy;

    iget-wide v4, v3, Lhgy;->k:J

    cmp-long v3, p2, v4

    if-nez v3, :cond_1

    .line 588
    :cond_0
    iget-object v3, p0, Lbmh;->e:Lbmf;

    iget-object v3, v3, Lbmf;->a:Lgml;

    iget-object v4, p0, Lbmh;->e:Lbmf;

    iget-object v4, v4, Lbmf;->c:Ljava/lang/String;

    iget-object v5, p1, Lfqj;->a:Lhgy;

    iget v5, v5, Lhgy;->b:I

    invoke-virtual {v3, v4, v5, p2, p3}, Lgml;->a(Ljava/lang/String;IJ)Z

    .line 589
    iget-object v3, p0, Lbmh;->e:Lbmf;

    iget-wide v4, p0, Lbmh;->a:J

    add-long/2addr v4, p2

    iget-wide v6, p0, Lbmh;->b:J

    invoke-virtual {v3, v4, v5, v6, v7}, Lbmf;->a(JJ)V

    .line 590
    iput v2, p0, Lbmh;->c:I

    .line 591
    iput-wide v0, p0, Lbmh;->d:J

    .line 593
    :cond_1
    return-void
.end method
