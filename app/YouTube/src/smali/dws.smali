.class public final enum Ldws;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldws;

.field public static final enum b:Ldws;

.field public static final enum c:Ldws;

.field private static final synthetic e:[Ldws;


# instance fields
.field final d:B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Ldws;

    const-string v1, "CLOUD_SCREEN"

    invoke-direct {v0, v1, v2, v2}, Ldws;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Ldws;->a:Ldws;

    .line 25
    new-instance v0, Ldws;

    const-string v1, "DIAL_SCREEN"

    invoke-direct {v0, v1, v3, v3}, Ldws;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Ldws;->b:Ldws;

    .line 26
    new-instance v0, Ldws;

    const-string v1, "CAST_SCREEN"

    invoke-direct {v0, v1, v4, v4}, Ldws;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Ldws;->c:Ldws;

    .line 23
    const/4 v0, 0x3

    new-array v0, v0, [Ldws;

    sget-object v1, Ldws;->a:Ldws;

    aput-object v1, v0, v2

    sget-object v1, Ldws;->b:Ldws;

    aput-object v1, v0, v3

    sget-object v1, Ldws;->c:Ldws;

    aput-object v1, v0, v4

    sput-object v0, Ldws;->e:[Ldws;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 35
    iput-byte p3, p0, Ldws;->d:B

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Ldws;
    .locals 1

    .prologue
    .line 23
    const-class v0, Ldws;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldws;

    return-object v0
.end method

.method public static values()[Ldws;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Ldws;->e:[Ldws;

    invoke-virtual {v0}, [Ldws;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldws;

    return-object v0
.end method
