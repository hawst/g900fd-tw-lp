.class public final Lbvn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldbt;


# static fields
.field private static final i:Ljava/util/Set;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Z

.field private C:Lbvx;

.field private D:Lgpa;

.field public final a:Levn;

.field public b:Ldwq;

.field public c:Z

.field public d:Z

.field public e:I

.field public f:Ldwo;

.field public g:Z

.field public h:Ljava/lang/String;

.field private final j:Landroid/app/Activity;

.field private final k:Lgot;

.field private final l:Lfvi;

.field private final m:Lcws;

.field private final n:Ldaq;

.field private final o:Landroid/widget/FrameLayout;

.field private final p:Landroid/view/View;

.field private final q:Lfus;

.field private final r:Landroid/os/Handler;

.field private final s:Lbvw;

.field private final t:Lbvv;

.field private u:Leue;

.field private v:Lfrl;

.field private w:Leue;

.field private x:Lfrl;

.field private y:Lfoy;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x4

    new-array v0, v0, [Ldwo;

    const/4 v1, 0x0

    sget-object v2, Ldwo;->g:Ldwo;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ldwo;->c:Ldwo;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Ldwo;->d:Ldwo;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Ldwo;->b:Ldwo;

    aput-object v2, v0, v1

    .line 90
    invoke-static {v0}, La;->b([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lbvn;->i:Ljava/util/Set;

    .line 89
    return-void
.end method

.method public constructor <init>(Lcws;Ldaq;Landroid/app/Activity;Leyp;Lgot;Ldcd;Ldbz;Landroid/view/View;Lbvv;Lbvw;Lfus;Landroid/view/View;Levn;)V
    .locals 7

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/4 v1, 0x0

    iput-boolean v1, p0, Lbvn;->d:Z

    .line 148
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iput-object v1, p0, Lbvn;->p:Landroid/view/View;

    .line 150
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcws;

    iput-object v1, p0, Lbvn;->m:Lcws;

    .line 151
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldaq;

    iput-object v1, p0, Lbvn;->n:Ldaq;

    .line 152
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    iput-object v1, p0, Lbvn;->j:Landroid/app/Activity;

    .line 154
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgot;

    iput-object v1, p0, Lbvn;->k:Lgot;

    .line 156
    invoke-static/range {p9 .. p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbvv;

    iput-object v1, p0, Lbvn;->t:Lbvv;

    .line 157
    invoke-static/range {p10 .. p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbvw;

    iput-object v1, p0, Lbvn;->s:Lbvw;

    .line 158
    invoke-static/range {p11 .. p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfus;

    iput-object v1, p0, Lbvn;->q:Lfus;

    .line 159
    invoke-static/range {p13 .. p13}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Levn;

    iput-object v1, p0, Lbvn;->a:Levn;

    .line 161
    new-instance v5, Lbvt;

    invoke-direct {v5, p0}, Lbvt;-><init>(Lbvn;)V

    .line 162
    new-instance v6, Lbvs;

    invoke-direct {v6, p0}, Lbvs;-><init>(Lbvn;)V

    .line 164
    iput-object p0, p6, Ldcd;->a:Ldbt;

    .line 165
    new-instance v1, Lbvx;

    move-object v2, p3

    move-object v3, p6

    move-object v4, p7

    invoke-direct/range {v1 .. v6}, Lbvx;-><init>(Landroid/content/Context;Ldcd;Ldbz;Lbxb;Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lbvn;->C:Lbvx;

    move-object/from16 v1, p12

    .line 171
    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lbvn;->o:Landroid/widget/FrameLayout;

    .line 172
    iget-object v1, p0, Lbvn;->o:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lbvn;->C:Lbvx;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 173
    iget-object v1, p0, Lbvn;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p7}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 174
    const/16 v1, 0x8

    move-object/from16 v0, p12

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 176
    new-instance v1, Lbvo;

    invoke-virtual {p3}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2, p3}, Lbvo;-><init>(Lbvn;Landroid/os/Looper;Landroid/app/Activity;)V

    iput-object v1, p0, Lbvn;->r:Landroid/os/Handler;

    .line 213
    iget-object v2, p0, Lbvn;->C:Lbvx;

    .line 214
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leyp;

    .line 213
    iget-object v3, v2, Lbvx;->e:Lfvi;

    if-nez v3, :cond_0

    new-instance v3, Lfvi;

    new-instance v4, Leym;

    invoke-direct {v4}, Leym;-><init>()V

    iget-object v5, v2, Lbvx;->d:Landroid/widget/ImageView;

    const/4 v6, 0x1

    invoke-direct {v3, v1, v4, v5, v6}, Lfvi;-><init>(Leyp;Leym;Landroid/widget/ImageView;Z)V

    iput-object v3, v2, Lbvx;->e:Lfvi;

    :cond_0
    iget-object v1, v2, Lbvx;->e:Lfvi;

    iput-object v1, p0, Lbvn;->l:Lfvi;

    .line 215
    return-void
.end method

.method private A()Ldwo;
    .locals 1

    .prologue
    .line 996
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->b:Ldwq;

    .line 997
    invoke-interface {v0}, Ldwq;->m()Ldwo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->l()Ldwo;

    move-result-object v0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1016
    iget-object v0, p0, Lbvn;->j:Landroid/app/Activity;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbvn;Ldwo;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lbvn;->a(Ldwo;)V

    return-void
.end method

.method static synthetic a(Lbvn;Ldww;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lbvn;->a(Ldww;)V

    return-void
.end method

.method static synthetic a(Lbvn;Ldxb;Lfoy;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lbvn;->a(Ldxb;Lfoy;)V

    return-void
.end method

.method static synthetic a(Lbvn;Lfrl;)V
    .locals 1

    .prologue
    .line 61
    iput-object p1, p0, Lbvn;->v:Lfrl;

    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbvn;->q()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lbvn;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lbvn;->u()V

    return-void
.end method

.method private a(Ldwj;)V
    .locals 2

    .prologue
    .line 424
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-nez v0, :cond_1

    .line 425
    const-string v0, "Video changed received for a non connected remote. Will ignore"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 429
    :cond_1
    invoke-virtual {p1}, Ldwj;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 431
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lbvn;->v:Lfrl;

    iget-object v0, v0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    invoke-direct {p0}, Lbvn;->u()V

    goto :goto_0

    .line 437
    :cond_2
    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    if-nez v0, :cond_3

    .line 438
    invoke-direct {p0}, Lbvn;->r()V

    .line 439
    iget-object v0, p1, Ldwj;->a:Ljava/lang/String;

    iget-object v1, p0, Lbvn;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 440
    invoke-direct {p0, p1}, Lbvn;->b(Ldwj;)V

    goto :goto_0

    .line 445
    :cond_3
    iget-object v0, p1, Ldwj;->a:Ljava/lang/String;

    invoke-direct {p0}, Lbvn;->u()V

    goto :goto_0
.end method

.method private a(Ldwo;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 327
    iput-object p1, p0, Lbvn;->f:Ldwo;

    .line 329
    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-nez v0, :cond_1

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    invoke-direct {p0}, Lbvn;->y()Z

    move-result v0

    if-nez v0, :cond_2

    .line 334
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget-object v1, p0, Lbvn;->v:Lfrl;

    .line 335
    invoke-virtual {v1}, Lfrl;->g()Lflo;

    move-result-object v1

    iget-object v1, v1, Lflo;->a:Lhqn;

    iget-object v1, v1, Lhqn;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lbvx;->a(Ljava/lang/String;Z)V

    .line 336
    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 339
    :cond_2
    invoke-direct {p0, p1}, Lbvn;->b(Ldwo;)V

    .line 341
    sget-object v0, Ldwo;->j:Ldwo;

    if-eq p1, v0, :cond_6

    .line 342
    iget-object v0, p0, Lbvn;->C:Lbvx;

    invoke-virtual {v0, p1}, Lbvx;->a(Ldwo;)V

    .line 343
    invoke-direct {p0}, Lbvn;->z()V

    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    iget-object v1, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 350
    :cond_3
    :goto_1
    sget-object v0, Ldwo;->e:Ldwo;

    if-eq p1, v0, :cond_4

    sget-object v0, Ldwo;->i:Ldwo;

    if-ne p1, v0, :cond_7

    .line 351
    :cond_4
    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    iget-object v1, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-static {v1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 343
    :cond_5
    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    .line 346
    :cond_6
    iget-object v0, p0, Lbvn;->j:Landroid/app/Activity;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 347
    iget-object v1, p0, Lbvn;->C:Lbvx;

    invoke-virtual {v1, v0, v2}, Lbvx;->a(Ljava/lang/String;Z)V

    goto :goto_1

    .line 356
    :cond_7
    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private a(Ldww;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v5, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 366
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-nez v0, :cond_2

    .line 368
    invoke-direct {p0}, Lbvn;->z()V

    .line 374
    :cond_0
    :goto_0
    sget-object v0, Lbvu;->a:[I

    invoke-virtual {p1}, Ldww;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 404
    :goto_1
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_1

    .line 410
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lbvn;->y:Lfoy;

    if-eqz v0, :cond_6

    :goto_2
    if-eqz v1, :cond_7

    iget-object v0, p0, Lbvn;->x:Lfrl;

    if-eqz v0, :cond_7

    .line 411
    iget-object v0, p0, Lbvn;->l:Lfvi;

    iget-object v1, p0, Lbvn;->x:Lfrl;

    invoke-virtual {v1}, Lfrl;->b()Lfnc;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Lfvi;->a(Lfnc;Leyo;)V

    .line 416
    :cond_1
    :goto_3
    :pswitch_0
    return-void

    .line 369
    :cond_2
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lbvn;->v:Lfrl;

    invoke-virtual {v0}, Lfrl;->r()Lhrd;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 371
    :goto_4
    iget-object v3, p0, Lbvn;->C:Lbvx;

    iput-boolean v0, v3, Lbvx;->f:Z

    goto :goto_0

    :cond_3
    move v0, v2

    .line 370
    goto :goto_4

    .line 381
    :pswitch_1
    invoke-direct {p0}, Lbvn;->t()V

    .line 382
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget-object v3, v0, Lbvx;->c:Lbxa;

    const v4, 0x7f0902f4

    invoke-virtual {v3, v4, v1}, Lbxa;->a(IZ)V

    iget-object v3, v0, Lbvx;->i:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Lbvx;->b()V

    iget-object v0, v0, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 385
    :pswitch_2
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 386
    sget-object v0, Ldwo;->c:Ldwo;

    invoke-direct {p0, v0}, Lbvn;->b(Ldwo;)V

    .line 388
    :cond_4
    new-instance v0, Ldwk;

    invoke-direct {v0}, Ldwk;-><init>()V

    iget-object v3, p0, Lbvn;->b:Ldwq;

    .line 390
    invoke-interface {v3}, Ldwq;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldwk;->a(Ljava/lang/String;)Ldwk;

    move-result-object v0

    iget-object v3, p0, Lbvn;->b:Ldwq;

    .line 391
    invoke-interface {v3}, Ldwq;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldwk;->b(Ljava/lang/String;)Ldwk;

    move-result-object v0

    iget-object v3, p0, Lbvn;->b:Ldwq;

    .line 392
    invoke-interface {v3}, Ldwq;->p()I

    move-result v3

    invoke-virtual {v0, v3}, Ldwk;->a(I)Ldwk;

    move-result-object v0

    .line 393
    invoke-virtual {v0}, Ldwk;->a()Ldwj;

    move-result-object v0

    .line 388
    invoke-direct {p0, v0}, Lbvn;->a(Ldwj;)V

    goto/16 :goto_1

    .line 398
    :pswitch_3
    const-string v0, ""

    .line 399
    iget-object v3, p0, Lbvn;->b:Ldwq;

    invoke-interface {v3}, Ldwq;->n()Ldwr;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 400
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    .line 402
    :cond_5
    invoke-direct {p0}, Lbvn;->t()V

    .line 403
    iget-object v3, p0, Lbvn;->C:Lbvx;

    iget-object v4, p0, Lbvn;->b:Ldwq;

    invoke-interface {v4}, Ldwq;->j()Ldwh;

    move-result-object v4

    invoke-virtual {v3}, Lbvx;->getContext()Landroid/content/Context;

    move-result-object v5

    iget v6, v4, Ldwh;->j:I

    new-array v7, v1, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v4, v4, Ldwh;->k:Z

    invoke-virtual {v3, v0, v4}, Lbvx;->a(Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_6
    move v1, v2

    .line 410
    goto/16 :goto_2

    .line 413
    :cond_7
    iget-object v0, p0, Lbvn;->l:Lfvi;

    iget-object v1, p0, Lbvn;->v:Lfrl;

    invoke-virtual {v1}, Lfrl;->b()Lfnc;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Lfvi;->a(Lfnc;Leyo;)V

    goto/16 :goto_3

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ldxb;Lfoy;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 310
    iget-object v2, p0, Lbvn;->C:Lbvx;

    iget-boolean v0, v2, Lbvx;->j:Z

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lbvx;->a()V

    .line 311
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v0, v2, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, v2, Lbvx;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1}, Ldxb;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p2, :cond_1

    iget-object v0, v2, Lbvx;->c:Lbxa;

    const v3, 0x7f090302

    invoke-virtual {v0, v3, v1}, Lbxa;->a(IZ)V

    invoke-virtual {v2}, Lbvx;->b()V

    iget-object v0, v2, Lbvx;->b:Ldbz;

    invoke-virtual {v0, v4}, Ldbz;->setVisibility(I)V

    :goto_1
    iget-object v0, v2, Lbvx;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lbvx;->c()V

    iget-object v0, v2, Lbvx;->a:Ldcd;

    sget-object v3, Ldbu;->c:Ldbu;

    invoke-virtual {v0, v3}, Ldcd;->a(Ldbu;)V

    iget-object v0, v2, Lbvx;->a:Ldcd;

    sget-object v3, Ldbs;->b:Ldbs;

    invoke-virtual {v0, v3}, Ldcd;->a(Ldbs;)V

    iget-object v3, v2, Lbvx;->a:Ldcd;

    iget-object v0, p2, Lfoy;->t:Landroid/net/Uri;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v3, v0}, Ldcd;->i(Z)V

    iget-object v0, v2, Lbvx;->b:Ldbz;

    invoke-virtual {v0, v1}, Ldbz;->setVisibility(I)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    iget-object v0, v2, Lbvx;->a:Ldcd;

    sget-object v1, Ldbu;->b:Ldbu;

    invoke-virtual {v0, v1}, Ldcd;->a(Ldbu;)V

    invoke-virtual {v2}, Lbvx;->c()V

    goto :goto_0
.end method

.method static synthetic a(Lbvn;)Z
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lbvn;->A()Ldwo;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lbvn;->i:Ljava/util/Set;

    invoke-direct {p0}, Lbvn;->A()Ldwo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lbvn;Z)Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvn;->B:Z

    return v0
.end method

.method static synthetic b(Lbvn;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lbvn;->z()V

    return-void
.end method

.method static synthetic b(Lbvn;Lfrl;)V
    .locals 1

    .prologue
    .line 61
    iput-object p1, p0, Lbvn;->x:Lfrl;

    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbvn;->q()V

    :cond_0
    return-void
.end method

.method private b(Ldwj;)V
    .locals 5

    .prologue
    .line 449
    iget-object v0, p0, Lbvn;->q:Lfus;

    .line 450
    iget-object v1, p1, Ldwj;->d:Ljava/lang/String;

    .line 451
    iget-object v2, p1, Ldwj;->a:Ljava/lang/String;

    .line 452
    iget v3, p1, Ldwj;->e:I

    const/4 v4, 0x1

    .line 449
    invoke-virtual {v0, v1, v2, v3, v4}, Lfus;->a(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 454
    return-void
.end method

.method private b(Ldwo;)V
    .locals 2

    .prologue
    .line 1009
    sget-object v0, Ldwo;->b:Ldwo;

    if-ne p1, v0, :cond_0

    const v0, 0x7f0902fc

    .line 1010
    invoke-direct {p0}, Lbvn;->v()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbvn;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1012
    :goto_0
    iget-object v1, p0, Lbvn;->C:Lbvx;

    iget-object v1, v1, Lbvx;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1013
    return-void

    .line 1010
    :cond_0
    const v0, 0x7f090309

    .line 1011
    invoke-direct {p0}, Lbvn;->v()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbvn;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lgpa;)V
    .locals 3

    .prologue
    .line 966
    if-eqz p1, :cond_0

    iget-object v0, p1, Lgpa;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 967
    :goto_0
    iget-object v1, p0, Lbvn;->C:Lbvx;

    iput-boolean v0, v1, Lbvx;->g:Z

    iget-object v2, v1, Lbvx;->a:Ldcd;

    invoke-virtual {v2, v0}, Ldcd;->k(Z)V

    invoke-virtual {v1}, Lbvx;->e()V

    .line 968
    return-void

    .line 966
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lbvn;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    return-object v0
.end method

.method private c(I)V
    .locals 1

    .prologue
    .line 838
    iget-object v0, p0, Lbvn;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 839
    return-void
.end method

.method static synthetic d(Lbvn;)Z
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lbvn;)Z
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lbvn;)Ldwo;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lbvn;->A()Ldwo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lbvn;)Ldwq;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbvn;->b:Ldwq;

    return-object v0
.end method

.method static synthetic h(Lbvn;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lbvn;->B:Z

    return v0
.end method

.method static synthetic i(Lbvn;)Lbvx;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbvn;->C:Lbvx;

    return-object v0
.end method

.method static synthetic j(Lbvn;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbvn;->j:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic k(Lbvn;)Lfrl;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbvn;->v:Lfrl;

    return-object v0
.end method

.method static synthetic l(Lbvn;)Lbvw;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbvn;->s:Lbvw;

    return-object v0
.end method

.method static synthetic m(Lbvn;)Z
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lbvn;->y()Z

    move-result v0

    return v0
.end method

.method private r()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 675
    iput-object v0, p0, Lbvn;->f:Ldwo;

    .line 676
    iput-object v0, p0, Lbvn;->D:Lgpa;

    .line 677
    const/4 v0, 0x0

    iput v0, p0, Lbvn;->e:I

    .line 678
    iget-object v0, p0, Lbvn;->C:Lbvx;

    invoke-virtual {v0}, Lbvx;->d()V

    .line 679
    return-void
.end method

.method private s()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 686
    iget-object v0, p0, Lbvn;->w:Leue;

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lbvn;->w:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 688
    iput-object v2, p0, Lbvn;->w:Leue;

    .line 690
    :cond_0
    iput-object v2, p0, Lbvn;->x:Lfrl;

    .line 691
    return-void
.end method

.method private t()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 811
    iget-boolean v0, p0, Lbvn;->d:Z

    if-nez v0, :cond_0

    .line 812
    iget-object v0, p0, Lbvn;->m:Lcws;

    invoke-virtual {v0}, Lcws;->x()V

    .line 813
    iget-object v0, p0, Lbvn;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 814
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbvn;->c(I)V

    .line 815
    iget-object v0, p0, Lbvn;->s:Lbvw;

    invoke-interface {v0, v2}, Lbvw;->e(Z)V

    .line 816
    iput-boolean v2, p0, Lbvn;->d:Z

    .line 818
    :cond_0
    return-void
.end method

.method private u()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 821
    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    if-nez v0, :cond_0

    .line 822
    invoke-direct {p0}, Lbvn;->r()V

    .line 825
    :cond_0
    invoke-direct {p0}, Lbvn;->t()V

    .line 826
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbvn;->b(Lgpa;)V

    .line 828
    iget-boolean v0, p0, Lbvn;->z:Z

    if-eqz v0, :cond_1

    .line 829
    iput v2, p0, Lbvn;->e:I

    .line 830
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget-object v1, p0, Lbvn;->A:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lbvx;->a(Ljava/lang/String;Z)V

    .line 835
    :goto_0
    return-void

    .line 832
    :cond_1
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    iget-object v1, p0, Lbvn;->b:Ldwq;

    invoke-interface {v1}, Ldwq;->t()Lfoy;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbvn;->a(Ldxb;Lfoy;)V

    .line 833
    invoke-direct {p0}, Lbvn;->A()Ldwo;

    move-result-object v0

    invoke-direct {p0, v0}, Lbvn;->a(Ldwo;)V

    goto :goto_0
.end method

.method private v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 846
    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 847
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->n()Ldwr;

    move-result-object v0

    invoke-virtual {v0}, Ldwr;->b()Ljava/lang/String;

    move-result-object v0

    .line 849
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private w()Z
    .locals 2

    .prologue
    .line 857
    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->b:Ldww;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private x()Z
    .locals 2

    .prologue
    .line 861
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->v:Lfrl;

    iget-object v0, v0, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbvn;->b:Ldwq;

    invoke-interface {v1}, Ldwq;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private y()Z
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->v:Lfrl;

    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v0

    invoke-virtual {v0}, Lflo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private z()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 929
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-nez v0, :cond_0

    .line 930
    iget-object v0, p0, Lbvn;->C:Lbvx;

    invoke-virtual {v0, v1}, Lbvx;->a(Z)V

    .line 946
    :goto_0
    return-void

    .line 933
    :cond_0
    iget-object v0, p0, Lbvn;->y:Lfoy;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbvn;->y:Lfoy;

    .line 934
    iget v0, v0, Lfoy;->o:I

    :goto_1
    mul-int/lit16 v2, v0, 0x3e8

    .line 935
    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 936
    invoke-direct {p0}, Lbvn;->A()Ldwo;

    move-result-object v0

    sget-object v1, Ldwo;->b:Ldwo;

    if-eq v0, v1, :cond_3

    .line 937
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    invoke-virtual {v0}, Ldxb;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->s()J

    move-result-wide v0

    :goto_2
    long-to-int v0, v0

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lbvn;->e:I

    .line 944
    :goto_3
    iget-object v0, p0, Lbvn;->C:Lbvx;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbvx;->a(Z)V

    .line 945
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget v1, p0, Lbvn;->e:I

    iget-object v3, v0, Lbvx;->a:Ldcd;

    const/16 v4, 0x64

    invoke-virtual {v3, v1, v2, v4}, Ldcd;->a(III)V

    invoke-virtual {v0}, Lbvx;->e()V

    goto :goto_0

    .line 934
    :cond_1
    iget-object v0, p0, Lbvn;->v:Lfrl;

    invoke-virtual {v0}, Lfrl;->c()I

    move-result v0

    goto :goto_1

    .line 937
    :cond_2
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->r()J

    move-result-wide v0

    goto :goto_2

    .line 939
    :cond_3
    iput v2, p0, Lbvn;->e:I

    goto :goto_3

    .line 942
    :cond_4
    iput v1, p0, Lbvn;->e:I

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 614
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-nez v0, :cond_1

    .line 623
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    invoke-direct {p0}, Lbvn;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lbvn;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 618
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->a()V

    .line 619
    iget-object v0, p0, Lbvn;->C:Lbvx;

    sget-object v1, Ldwo;->c:Ldwo;

    invoke-virtual {v0, v1}, Lbvx;->a(Ldwo;)V

    goto :goto_0

    .line 621
    :cond_2
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbvn;->B:Z

    iget-object v0, p0, Lbvn;->b:Ldwq;

    iget-object v1, p0, Lbvn;->v:Lfrl;

    iget-object v1, v1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Ldwq;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lbvn;->C:Lbvx;

    sget-object v1, Ldwo;->i:Ldwo;

    invoke-virtual {v0, v1}, Lbvx;->a(Ldwo;)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 647
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-nez v0, :cond_1

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 650
    :cond_1
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0, p1}, Ldwq;->a(I)V

    .line 651
    iget-object v0, p0, Lbvn;->v:Lfrl;

    invoke-virtual {v0}, Lfrl;->c()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    if-ge p1, v0, :cond_0

    .line 652
    invoke-direct {p0}, Lbvn;->A()Ldwo;

    move-result-object v0

    sget-object v1, Ldwo;->b:Ldwo;

    if-eq v0, v1, :cond_0

    .line 653
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->a()V

    goto :goto_0
.end method

.method public final a(Ldwq;)V
    .locals 1

    .prologue
    .line 218
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_1

    .line 221
    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-ne v0, p1, :cond_0

    .line 222
    const-string v0, "Already connected to the same remote control."

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 235
    :goto_0
    return-void

    .line 225
    :cond_0
    invoke-virtual {p0}, Lbvn;->o()V

    .line 229
    :cond_1
    iput-object p1, p0, Lbvn;->b:Ldwq;

    .line 230
    iget-boolean v0, p0, Lbvn;->c:Z

    if-eqz v0, :cond_2

    .line 231
    iget-object v0, p0, Lbvn;->a:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 234
    :cond_2
    invoke-virtual {p0}, Lbvn;->q()V

    goto :goto_0
.end method

.method public final a(Lfrl;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 475
    iget-object v0, p0, Lbvn;->u:Leue;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lbvn;->u:Leue;

    const/4 v1, 0x1

    iput-boolean v1, v0, Leue;->a:Z

    .line 477
    iput-object v2, p0, Lbvn;->u:Leue;

    .line 479
    :cond_0
    invoke-direct {p0}, Lbvn;->s()V

    .line 481
    if-nez p1, :cond_1

    .line 482
    iput-object v2, p0, Lbvn;->v:Lfrl;

    .line 526
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_2

    .line 487
    iget-object v0, p1, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbvn;->v:Lfrl;

    iget-object v1, v1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 489
    iget-object v0, p0, Lbvn;->C:Lbvx;

    invoke-virtual {v0}, Lbvx;->d()V

    .line 492
    :cond_2
    iget-object v0, p1, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbvn;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    const-string v0, ""

    iput-object v0, p0, Lbvn;->h:Ljava/lang/String;

    .line 495
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbvn;->z:Z

    .line 499
    iget-object v0, p1, Lfrl;->a:Lhro;

    invoke-static {v0}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 500
    invoke-virtual {p1}, Lfrl;->g()Lflo;

    move-result-object v0

    iget-object v0, v0, Lflo;->a:Lhqn;

    iget-object v0, v0, Lhqn;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lbvn;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 505
    :cond_4
    iget-object v0, p0, Lbvn;->j:Landroid/app/Activity;

    new-instance v1, Lbvp;

    invoke-direct {v1, p0}, Lbvp;-><init>(Lbvn;)V

    .line 506
    invoke-static {v0, v1}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbvn;->u:Leue;

    .line 518
    iget-object v0, p0, Lbvn;->n:Ldaq;

    .line 519
    iget-object v1, p1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ldaq;->a:[B

    const-string v3, ""

    const-string v4, ""

    iget-object v7, p0, Lbvn;->u:Leue;

    move v6, v5

    .line 518
    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    goto :goto_0
.end method

.method public final a(Lgpa;)V
    .locals 1

    .prologue
    .line 950
    invoke-virtual {p0}, Lbvn;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 951
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0, p1}, Ldwq;->a(Lgpa;)V

    .line 953
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 529
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbvn;->z:Z

    .line 530
    iput-object p1, p0, Lbvn;->A:Ljava/lang/String;

    .line 531
    const-string v0, ""

    iput-object v0, p0, Lbvn;->h:Ljava/lang/String;

    .line 533
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-nez v0, :cond_0

    .line 538
    :goto_0
    return-void

    .line 537
    :cond_0
    iget-object v0, p0, Lbvn;->C:Lbvx;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lbvx;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lbvn;->t:Lbvv;

    invoke-interface {v0, p1}, Lbvv;->g(Z)V

    .line 672
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 605
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-nez v0, :cond_0

    .line 610
    :goto_0
    return-void

    .line 608
    :cond_0
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->b()V

    .line 609
    iget-object v0, p0, Lbvn;->C:Lbvx;

    sget-object v1, Ldwo;->d:Ldwo;

    invoke-virtual {v0, v1}, Lbvx;->a(Ldwo;)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 592
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget-boolean v0, v0, Lbvx;->j:Z

    .line 1021
    iget-object v1, p0, Lbvn;->C:Lbvx;

    iput-boolean p1, v1, Lbvx;->j:Z

    .line 1022
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    .line 1023
    iget-object v0, p0, Lbvn;->C:Lbvx;

    invoke-virtual {v0}, Lbvx;->a()V

    .line 1027
    :cond_0
    :goto_0
    return-void

    .line 1024
    :cond_1
    if-nez p1, :cond_0

    if-eqz v0, :cond_0

    .line 1025
    invoke-virtual {p0}, Lbvn;->q()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 659
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-nez v0, :cond_1

    .line 663
    :cond_0
    :goto_0
    return-void

    .line 662
    :cond_1
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->b()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lbvn;->m:Lcws;

    invoke-virtual {v0}, Lcws;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 601
    :goto_0
    return-void

    .line 600
    :cond_0
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->g()V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lbvn;->m:Lcws;

    invoke-virtual {v0}, Lcws;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 632
    :goto_0
    return-void

    .line 631
    :cond_0
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->e()V

    goto :goto_0
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 542
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-nez v0, :cond_0

    .line 569
    :goto_0
    return-void

    .line 545
    :cond_0
    iget-object v0, p0, Lbvn;->k:Lgot;

    iget-object v1, p0, Lbvn;->v:Lfrl;

    iget-object v1, v1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbvn;->j:Landroid/app/Activity;

    new-instance v3, Lbvq;

    invoke-direct {v3, p0}, Lbvq;-><init>(Lbvn;)V

    .line 546
    invoke-static {v2, v3}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v2

    .line 545
    invoke-interface {v0, v1, v2}, Lgot;->a(Ljava/lang/String;Leuc;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 574
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 579
    return-void
.end method

.method public final handleMdxAdPlayerStateChangedEvent(Ldwa;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->k()Ldxb;

    move-result-object v0

    sget-object v1, Ldxb;->b:Ldxb;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbvn;->b:Ldwq;

    .line 321
    invoke-interface {v0}, Ldwq;->t()Lfoy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p1, Ldwa;->a:Ldwo;

    invoke-direct {p0, v0}, Lbvn;->a(Ldwo;)V

    .line 324
    :cond_0
    return-void
.end method

.method public final handleMdxPlaybackChangedEvent(Ldwi;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 420
    iget-object v0, p1, Ldwi;->a:Ldwj;

    invoke-direct {p0, v0}, Lbvn;->a(Ldwj;)V

    .line 421
    return-void
.end method

.method public final handleMdxPlaylistChangedEvent(Ldwp;)V
    .locals 7
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 282
    iget-object v6, p1, Ldwp;->a:Ldwj;

    .line 283
    invoke-virtual {v6}, Ldwj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    new-instance v0, Lgoh;

    .line 285
    iget-object v1, v6, Ldwj;->a:Ljava/lang/String;

    .line 286
    iget-object v2, v6, Ldwj;->d:Ljava/lang/String;

    .line 287
    iget v3, v6, Ldwj;->e:I

    const/4 v4, 0x0

    sget-object v5, Lgog;->d:Lgog;

    invoke-direct/range {v0 .. v5}, Lgoh;-><init>(Ljava/lang/String;Ljava/lang/String;IILgog;)V

    .line 292
    iget-object v1, p0, Lbvn;->m:Lcws;

    invoke-virtual {v1, v0}, Lcws;->b(Lgoh;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lbvn;->m:Lcws;

    invoke-virtual {v0}, Lcws;->o()Z

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    invoke-direct {p0, v6}, Lbvn;->b(Ldwj;)V

    goto :goto_0
.end method

.method public final handleMdxStateChangedEvent(Ldwx;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 362
    iget-object v0, p1, Ldwx;->a:Ldww;

    invoke-direct {p0, v0}, Lbvn;->a(Ldww;)V

    .line 363
    return-void
.end method

.method public final handleMdxSubtitleChangedEvent(Ldwy;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 962
    iget-object v0, p1, Ldwy;->a:Lgpa;

    invoke-direct {p0, v0}, Lbvn;->b(Lgpa;)V

    .line 963
    return-void
.end method

.method public final handleMdxVideoPlayerStateChangedEvent(Ldxa;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 315
    iget-object v0, p1, Ldxa;->a:Ldwo;

    invoke-direct {p0, v0}, Lbvn;->a(Ldwo;)V

    .line 316
    return-void
.end method

.method public final handleMdxVideoStageEvent(Ldxc;)V
    .locals 8
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v5, -0x1

    .line 302
    iget-object v0, p1, Ldxc;->a:Ldxb;

    sget-object v1, Ldxb;->b:Ldxb;

    if-ne v0, v1, :cond_0

    .line 303
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->t()Lfoy;

    move-result-object v0

    iput-object v0, p0, Lbvn;->y:Lfoy;

    iget-object v0, p0, Lbvn;->y:Lfoy;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lbvn;->s()V

    .line 305
    :cond_0
    :goto_0
    iget-object v0, p1, Ldxc;->a:Ldxb;

    iget-object v1, p0, Lbvn;->b:Ldwq;

    invoke-interface {v1}, Ldwq;->t()Lfoy;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbvn;->a(Ldxb;Lfoy;)V

    .line 306
    invoke-virtual {p0}, Lbvn;->q()V

    .line 307
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lbvn;->x:Lfrl;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbvn;->j:Landroid/app/Activity;

    new-instance v1, Lbvr;

    invoke-direct {v1, p0}, Lbvr;-><init>(Lbvn;)V

    invoke-static {v0, v1}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v0

    invoke-static {v0}, Leue;->a(Leuc;)Leue;

    move-result-object v0

    iput-object v0, p0, Lbvn;->w:Leue;

    iget-object v0, p0, Lbvn;->n:Ldaq;

    iget-object v1, p0, Lbvn;->y:Lfoy;

    iget-object v1, v1, Lfoy;->c:Ljava/lang/String;

    sget-object v2, Ldaq;->a:[B

    const-string v3, ""

    const-string v4, ""

    iget-object v7, p0, Lbvn;->w:Leue;

    move v6, v5

    invoke-virtual/range {v0 .. v7}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;IILeuc;)V

    goto :goto_0
.end method

.method public final handleSequencerHasPreviousNextEvent(Lczt;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 458
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget-boolean v1, p1, Lczt;->a:Z

    iget-object v2, v0, Lbvx;->a:Ldcd;

    invoke-virtual {v2, v1}, Ldcd;->e(Z)V

    invoke-virtual {v0}, Lbvx;->e()V

    .line 459
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget-boolean v1, p1, Lczt;->b:Z

    iget-object v2, v0, Lbvx;->a:Ldcd;

    invoke-virtual {v2, v1}, Ldcd;->d(Z)V

    invoke-virtual {v0}, Lbvx;->e()V

    .line 460
    return-void
.end method

.method public final handleVideoAvailabilityChangedEvent(Ldwz;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 467
    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    sget-object v1, Ldww;->b:Ldww;

    if-ne v0, v1, :cond_0

    .line 468
    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbvn;->v:Lfrl;

    invoke-virtual {v0}, Lfrl;->g()Lflo;

    move-result-object v0

    invoke-virtual {v0}, Lflo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 469
    iget-object v0, p0, Lbvn;->C:Lbvx;

    iget-object v1, p0, Lbvn;->v:Lfrl;

    invoke-virtual {v1}, Lfrl;->g()Lflo;

    move-result-object v1

    iget-object v1, v1, Lflo;->a:Lhqn;

    iget-object v1, v1, Lhqn;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lbvx;->a(Ljava/lang/String;Z)V

    .line 472
    :cond_0
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 667
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 588
    return-void
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 643
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 636
    invoke-direct {p0}, Lbvn;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->a()V

    .line 639
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 0

    .prologue
    .line 584
    return-void
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 958
    return-void
.end method

.method public final o()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 238
    iget-object v0, p0, Lbvn;->u:Leue;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lbvn;->u:Leue;

    iput-boolean v2, v0, Leue;->a:Z

    .line 240
    iput-object v3, p0, Lbvn;->u:Leue;

    .line 242
    :cond_0
    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_2

    .line 243
    iget-object v0, p0, Lbvn;->a:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 246
    iget-boolean v0, p0, Lbvn;->d:Z

    if-eqz v0, :cond_1

    .line 247
    iget-boolean v0, p0, Lbvn;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbvn;->m:Lcws;

    invoke-virtual {v0}, Lcws;->w()V

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lbvn;->c(I)V

    iget-object v0, p0, Lbvn;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lbvn;->d:Z

    iget-object v0, p0, Lbvn;->s:Lbvw;

    invoke-interface {v0, v1}, Lbvw;->e(Z)V

    iget-object v0, p0, Lbvn;->r:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lbvn;->v:Lfrl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbvn;->q:Lfus;

    iget-object v1, p0, Lbvn;->v:Lfrl;

    iget-object v1, v1, Lfrl;->a:Lhro;

    invoke-static {v1}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lgog;->d:Lgog;

    invoke-virtual {v0, v1, v2}, Lfus;->a(Ljava/lang/String;Lgog;)V

    .line 249
    :cond_1
    iput-object v3, p0, Lbvn;->b:Ldwq;

    .line 251
    :cond_2
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 853
    iget-object v0, p0, Lbvn;->b:Ldwq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()V
    .locals 1

    .prologue
    .line 923
    invoke-virtual {p0}, Lbvn;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lbvn;->b:Ldwq;

    invoke-interface {v0}, Ldwq;->i()Ldww;

    move-result-object v0

    invoke-direct {p0, v0}, Lbvn;->a(Ldww;)V

    .line 926
    :cond_0
    return-void
.end method
