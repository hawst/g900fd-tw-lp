.class public final Ldsn;
.super Lsc;
.source "SourceFile"

# interfaces
.implements Ldba;


# instance fields
.field public final a:Ldrw;

.field public final b:Lsb;

.field public c:I

.field public d:Ldwq;

.field private final e:Ljava/util/List;

.field private final f:Ldwq;

.field private final g:Z

.field private h:Ldwr;

.field private i:Leuc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ldwq;Lrm;Ldrw;Z)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lsc;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Ldsn;->c:I

    .line 64
    invoke-static {p1}, Lsb;->a(Landroid/content/Context;)Lsb;

    move-result-object v0

    iput-object v0, p0, Ldsn;->b:Lsb;

    .line 65
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldsn;->f:Ldwq;

    .line 66
    iget-object v0, p0, Ldsn;->b:Lsb;

    invoke-static {p3}, Lsb;->a(Lrm;)V

    .line 67
    iput-object p4, p0, Ldsn;->a:Ldrw;

    .line 68
    iput-boolean p5, p0, Ldsn;->g:Z

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ldsn;->e:Ljava/util/List;

    .line 71
    return-void
.end method

.method private a(Ldwr;)Lsk;
    .locals 4

    .prologue
    .line 146
    iget-object v0, p0, Ldsn;->b:Lsb;

    invoke-static {}, Lsb;->a()Ljava/util/List;

    move-result-object v0

    .line 147
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsk;

    .line 149
    invoke-static {v0}, Ldsc;->a(Lsk;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lsk;->f()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 150
    invoke-virtual {v0}, Lsk;->f()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Ldwr;->a(Landroid/os/Bundle;)Ldwr;

    move-result-object v2

    .line 151
    if-eqz v2, :cond_0

    invoke-virtual {p1}, Ldwr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ldwr;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized a(Z)V
    .locals 3

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldsn;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldso;

    .line 83
    iget-object v2, p0, Ldsn;->d:Ldwq;

    invoke-interface {v0, v2, p1}, Ldso;->a(Ldwq;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 92
    iget v0, p0, Ldsn;->c:I

    if-nez v0, :cond_1

    .line 93
    iget-object v0, p0, Ldsn;->b:Lsb;

    iget-object v1, p0, Ldsn;->a:Ldrw;

    .line 94
    iget-object v1, v1, Ldrw;->b:Lrz;

    const/4 v2, 0x4

    .line 93
    invoke-virtual {v0, v1, p0, v2}, Lsb;->a(Lrz;Lsc;I)V

    .line 97
    iget-object v0, p0, Ldsn;->d:Ldwq;

    .line 98
    iget-object v1, p0, Ldsn;->b:Lsb;

    invoke-static {}, Lsb;->c()Lsk;

    move-result-object v1

    invoke-static {v1}, Ldsc;->a(Lsk;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Ldsn;->a:Ldrw;

    iget-object v2, p0, Ldsn;->b:Lsb;

    .line 99
    invoke-static {}, Lsb;->c()Lsk;

    move-result-object v2

    invoke-virtual {v1, v2}, Ldrw;->a(Lsk;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 100
    :cond_0
    iget-object v1, p0, Ldsn;->f:Ldwq;

    iput-object v1, p0, Ldsn;->d:Ldwq;

    .line 104
    :goto_0
    iget-object v1, p0, Ldsn;->d:Ldwq;

    if-eq v0, v1, :cond_1

    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldsn;->a(Z)V

    .line 108
    :cond_1
    iget v0, p0, Ldsn;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ldsn;->c:I

    .line 109
    return-void

    .line 102
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Ldsn;->d:Ldwq;

    goto :goto_0
.end method

.method public final a(Landroid/media/RemoteControlClient;)V
    .locals 1

    .prologue
    .line 226
    invoke-static {}, Lb;->a()V

    .line 227
    iget-object v0, p0, Ldsn;->b:Lsb;

    invoke-static {p1}, Lsb;->a(Ljava/lang/Object;)V

    .line 228
    return-void
.end method

.method public final declared-synchronized a(Ldso;)V
    .locals 1

    .prologue
    .line 74
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldsn;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ldwr;Leuc;)V
    .locals 3

    .prologue
    .line 130
    invoke-static {}, Lb;->a()V

    .line 131
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Selecting mdx route for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-boolean v0, p0, Ldsn;->g:Z

    .line 133
    invoke-direct {p0, p1}, Ldsn;->a(Ldwr;)Lsk;

    move-result-object v0

    .line 134
    if-nez v0, :cond_0

    .line 135
    const-string v0, "Trying to select an unknown route - will ignore"

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 136
    iput-object p1, p0, Ldsn;->h:Ldwr;

    .line 137
    iput-object p2, p0, Ldsn;->i:Leuc;

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-virtual {v0}, Lsk;->g()V

    .line 142
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, p1, v0}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lsb;Lsk;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 194
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Route added "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-boolean v0, p0, Ldsn;->g:Z

    .line 195
    iget-object v0, p0, Ldsn;->h:Ldwr;

    if-eqz v0, :cond_0

    invoke-static {p2}, Ldsc;->a(Lsk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {p2}, Lsk;->f()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Ldsn;->h:Ldwr;

    invoke-virtual {v0}, Ldwr;->a()Ljava/lang/String;

    move-result-object v0

    .line 198
    invoke-virtual {p2}, Lsk;->f()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v1}, Ldwr;->a(Landroid/os/Bundle;)Ldwr;

    move-result-object v1

    invoke-virtual {v1}, Ldwr;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 199
    invoke-virtual {p2}, Lsk;->g()V

    .line 200
    iget-object v0, p0, Ldsn;->i:Leuc;

    iget-object v1, p0, Ldsn;->h:Ldwr;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 201
    iput-object v3, p0, Ldsn;->h:Ldwr;

    .line 202
    iput-object v3, p0, Ldsn;->i:Leuc;

    .line 206
    :cond_0
    iget-object v0, p0, Ldsn;->a:Ldrw;

    invoke-virtual {v0, p2}, Ldrw;->b(Lsk;)V

    .line 207
    return-void
.end method

.method public final a(Lsk;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 172
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Route selected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-boolean v0, p0, Ldsn;->g:Z

    .line 174
    iget-object v0, p0, Ldsn;->a:Ldrw;

    invoke-virtual {v0, p1}, Ldrw;->d(Lsk;)Z

    move-result v0

    .line 175
    if-nez v0, :cond_0

    invoke-static {p1}, Ldsc;->a(Lsk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    :cond_0
    iget-object v0, p0, Ldsn;->f:Ldwq;

    iput-object v0, p0, Ldsn;->d:Ldwq;

    .line 180
    :goto_0
    iput-object v3, p0, Ldsn;->h:Ldwr;

    .line 181
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldsn;->a(Z)V

    .line 182
    return-void

    .line 178
    :cond_1
    iput-object v3, p0, Ldsn;->d:Ldwq;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lb;->a()V

    .line 161
    iget-object v0, p0, Ldsn;->b:Lsb;

    invoke-static {}, Lsb;->b()Lsk;

    move-result-object v0

    invoke-virtual {v0}, Lsk;->g()V

    .line 162
    return-void
.end method

.method public final b(Landroid/media/RemoteControlClient;)V
    .locals 1

    .prologue
    .line 232
    invoke-static {}, Lb;->a()V

    .line 233
    iget-object v0, p0, Ldsn;->b:Lsb;

    invoke-static {p1}, Lsb;->b(Ljava/lang/Object;)V

    .line 234
    return-void
.end method

.method public final declared-synchronized b(Ldso;)V
    .locals 1

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldsn;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    monitor-exit p0

    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lsb;Lsk;)V
    .locals 3

    .prologue
    .line 211
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Route removed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-boolean v0, p0, Ldsn;->g:Z

    .line 213
    iget-object v0, p0, Ldsn;->a:Ldrw;

    invoke-virtual {v0, p2}, Ldrw;->c(Lsk;)V

    .line 214
    return-void
.end method

.method public final b(Lsk;)V
    .locals 3

    .prologue
    .line 186
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Route unselected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-boolean v0, p0, Ldsn;->g:Z

    .line 187
    iget-object v0, p0, Ldsn;->a:Ldrw;

    invoke-virtual {v0}, Ldrw;->a()V

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Ldsn;->d:Ldwq;

    .line 189
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldsn;->a(Z)V

    .line 190
    return-void
.end method

.method public final onMdxScreenDisconnecting(Ldwu;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Ldsn;->b:Lsb;

    invoke-static {}, Lsb;->b()Lsk;

    move-result-object v0

    invoke-virtual {v0}, Lsk;->g()V

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Ldsn;->d:Ldwq;

    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ldsn;->a(Z)V

    .line 119
    return-void
.end method
