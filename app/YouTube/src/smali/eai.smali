.class public final Leai;
.super Lorg/apache/http/entity/HttpEntityWrapper;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:J


# direct methods
.method public constructor <init>(Lorg/apache/http/HttpEntity;Ljava/lang/String;IJJJJ)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lorg/apache/http/entity/HttpEntityWrapper;-><init>(Lorg/apache/http/HttpEntity;)V

    .line 70
    iput-object p2, p0, Leai;->a:Ljava/lang/String;

    .line 71
    iput p3, p0, Leai;->b:I

    .line 72
    iput-wide p4, p0, Leai;->c:J

    .line 73
    iput-wide p6, p0, Leai;->d:J

    .line 74
    iput-wide p8, p0, Leai;->e:J

    .line 75
    iput-wide p10, p0, Leai;->f:J

    .line 76
    return-void
.end method

.method static synthetic a(Leai;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Leai;->f:J

    return-wide v0
.end method

.method static synthetic b(Leai;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Leai;->b:I

    return v0
.end method

.method static synthetic c(Leai;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Leai;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Leai;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Leai;->e:J

    return-wide v0
.end method

.method static synthetic e(Leai;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Leai;->c:J

    return-wide v0
.end method

.method static synthetic f(Leai;)J
    .locals 2

    .prologue
    .line 34
    iget-wide v0, p0, Leai;->d:J

    return-wide v0
.end method


# virtual methods
.method public final getContent()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Lorg/apache/http/entity/HttpEntityWrapper;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 81
    new-instance v1, Leaj;

    invoke-direct {v1, p0, v0}, Leaj;-><init>(Leai;Ljava/io/InputStream;)V

    return-object v1
.end method
