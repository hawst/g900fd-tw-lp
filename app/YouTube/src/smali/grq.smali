.class public final Lgrq;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lgqo;

.field private final c:I

.field private final d:I

.field private final e:Lgql;

.field private final f:Lgqe;

.field private final g:Lgri;

.field private final h:Ljava/util/concurrent/PriorityBlockingQueue;

.field private final i:Lgqh;

.field private final j:Lgrr;

.field private volatile k:Z

.field private l:Lgrg;

.field private m:Lgqn;

.field private n:Lgqg;

.field private final o:Landroid/media/MediaCodec$BufferInfo;

.field private p:[Ljava/nio/ByteBuffer;

.field private q:Ljava/util/concurrent/CountDownLatch;

.field private volatile r:Ljava/lang/Exception;

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgqo;IILjava/util/concurrent/PriorityBlockingQueue;Lgql;Lgqe;Lgri;Lgqh;)V
    .locals 2

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 52
    new-instance v0, Lgrr;

    invoke-direct {v0, p0}, Lgrr;-><init>(Lgrq;)V

    iput-object v0, p0, Lgrq;->j:Lgrr;

    .line 59
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, Lgrq;->o:Landroid/media/MediaCodec$BufferInfo;

    .line 66
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lgrq;->q:Ljava/util/concurrent/CountDownLatch;

    .line 94
    iput-object p1, p0, Lgrq;->a:Landroid/content/Context;

    .line 95
    iput-object p2, p0, Lgrq;->b:Lgqo;

    .line 96
    iput p3, p0, Lgrq;->c:I

    .line 97
    iput p4, p0, Lgrq;->d:I

    .line 98
    iput-object p5, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 99
    iput-object p6, p0, Lgrq;->e:Lgql;

    .line 100
    iput-object p7, p0, Lgrq;->f:Lgqe;

    .line 101
    iput-object p8, p0, Lgrq;->g:Lgri;

    .line 102
    iput-object p9, p0, Lgrq;->i:Lgqh;

    .line 103
    const-string v0, "Extractor Thread"

    invoke-virtual {p0, v0}, Lgrq;->setName(Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method static synthetic a(Lgrq;)Lgqh;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lgrq;->i:Lgqh;

    return-object v0
.end method

.method private a(Lgrm;)Z
    .locals 14

    .prologue
    const/4 v11, -0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 279
    .line 281
    :try_start_0
    invoke-virtual {p1}, Lgrm;->b()Lgrb;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, Lgrb;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v7

    .line 398
    :cond_0
    :goto_0
    return v0

    .line 286
    :cond_1
    iget-object v1, p0, Lgrq;->m:Lgqn;

    iget-object v2, p0, Lgrq;->n:Lgqg;

    invoke-virtual {v0, v1, v2}, Lgrb;->a(Lgqn;Lgqg;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move v3, v8

    move v2, v7

    .line 289
    :goto_1
    if-nez v3, :cond_f

    .line 291
    :try_start_1
    iget-object v0, p0, Lgrq;->n:Lgqg;

    const-wide/16 v4, 0x2710

    invoke-interface {v0, v4, v5}, Lgqg;->a(J)I

    move-result v1

    .line 292
    if-ltz v1, :cond_f

    .line 294
    iget-object v0, p0, Lgrq;->m:Lgqn;

    invoke-virtual {v0}, Lgqn;->e()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_e

    .line 301
    iget-object v0, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrm;

    .line 302
    if-eqz v0, :cond_e

    iget v0, v0, Lgrm;->a:I

    iget v4, p1, Lgrm;->a:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    if-le v0, v4, :cond_e

    .line 307
    :try_start_2
    iget-object v0, p0, Lgrq;->m:Lgqn;

    invoke-virtual {v0}, Lgqn;->d()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Lgrm;->b(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    move v0, v7

    move v9, v8

    .line 312
    :goto_2
    const-wide/16 v4, 0x0

    .line 315
    if-nez v0, :cond_d

    .line 316
    :try_start_3
    iget-object v0, p0, Lgrq;->m:Lgqn;

    iget-object v2, p0, Lgrq;->p:[Ljava/nio/ByteBuffer;

    aget-object v2, v2, v1

    const/4 v6, 0x0

    .line 317
    invoke-virtual {v0, v2, v6}, Lgqn;->a(Ljava/nio/ByteBuffer;I)I

    move-result v0

    .line 320
    :goto_3
    if-gez v0, :cond_4

    .line 323
    const/4 v6, 0x4

    move v3, v8

    move v10, v7

    .line 331
    :goto_4
    iget-object v0, p0, Lgrq;->n:Lgqg;

    const/4 v2, 0x0

    invoke-interface/range {v0 .. v6}, Lgqg;->a(IIIJI)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_5

    move v3, v10

    move v1, v9

    .line 339
    :goto_5
    :try_start_4
    iget-object v0, p0, Lgrq;->n:Lgqg;

    iget-object v2, p0, Lgrq;->o:Landroid/media/MediaCodec$BufferInfo;

    const-wide/16 v4, 0x2710

    .line 340
    invoke-interface {v0, v2, v4, v5}, Lgqg;->a(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v4

    .line 341
    if-ltz v4, :cond_9

    .line 342
    iget-object v0, p0, Lgrq;->o:Landroid/media/MediaCodec$BufferInfo;

    iget v0, v0, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_5

    move v2, v7

    .line 345
    :goto_6
    iget-object v0, p0, Lgrq;->o:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v12, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 346
    if-nez v2, :cond_6

    invoke-virtual {p1, v12, v13}, Lgrm;->a(J)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v7

    .line 348
    :goto_7
    iget-object v5, p0, Lgrq;->n:Lgqg;

    invoke-interface {v5, v4, v0}, Lgqg;->a(IZ)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 349
    if-eqz v0, :cond_2

    .line 352
    :try_start_5
    iget-object v0, p0, Lgrq;->l:Lgrg;

    invoke-interface {v0}, Lgrg;->b()Landroid/graphics/Bitmap;
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v0

    .line 359
    if-eqz v0, :cond_7

    .line 360
    :try_start_6
    invoke-virtual {p1, v12, v13, v0}, Lgrm;->a(JLandroid/graphics/Bitmap;)V

    .line 366
    :cond_2
    :goto_8
    invoke-virtual {p1, v12, v13, v2}, Lgrm;->a(JZ)Lgrb;

    move-result-object v0

    .line 367
    iget-object v2, p0, Lgrq;->m:Lgqn;

    iget-object v4, p0, Lgrq;->n:Lgqg;

    invoke-virtual {v0, v2, v4}, Lgrb;->a(Lgqn;Lgqg;)V

    .line 368
    invoke-virtual {v0}, Lgrb;->a()Z

    move-result v0

    if-nez v0, :cond_c

    .line 369
    iget-boolean v0, p0, Lgrq;->k:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lgrq;->j:Lgrr;

    iget-boolean v0, v0, Lgrr;->b:Z

    if-eqz v0, :cond_8

    .line 372
    :cond_3
    iget-object v0, p0, Lgrq;->m:Lgqn;

    invoke-virtual {v0}, Lgqn;->d()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lgrm;->b(J)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move v0, v8

    .line 394
    :goto_9
    if-eqz v0, :cond_0

    .line 395
    invoke-virtual {p1}, Lgrm;->c()V

    goto/16 :goto_0

    .line 327
    :cond_4
    :try_start_7
    iget-object v2, p0, Lgrq;->m:Lgqn;

    invoke-virtual {v2}, Lgqn;->d()J

    move-result-wide v4

    .line 328
    iget-object v2, p0, Lgrq;->m:Lgqn;

    invoke-virtual {v2}, Lgqn;->c()Z
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    move v6, v8

    move v10, v3

    move v3, v0

    goto :goto_4

    :cond_5
    move v2, v8

    .line 342
    goto :goto_6

    :cond_6
    move v0, v8

    .line 346
    goto :goto_7

    .line 354
    :catch_0
    move-exception v0

    :try_start_8
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 355
    iget-object v0, p0, Lgrq;->m:Lgqn;

    invoke-virtual {v0}, Lgqn;->d()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Lgrm;->b(J)V

    move v0, v8

    .line 357
    goto :goto_9

    .line 362
    :cond_7
    const-string v0, "Failed to render thumbnail"

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    goto :goto_8

    .line 391
    :catch_1
    move-exception v0

    move v7, v1

    .line 392
    :goto_a
    invoke-virtual {p1, v0}, Lgrm;->a(Ljava/lang/Exception;)V

    move v0, v7

    goto :goto_9

    :cond_8
    move v2, v1

    .line 376
    goto/16 :goto_1

    :cond_9
    if-ne v4, v11, :cond_a

    .line 378
    if-eqz v3, :cond_b

    move v0, v1

    .line 381
    goto :goto_9

    .line 383
    :cond_a
    const/4 v0, -0x2

    if-eq v4, v0, :cond_b

    const/4 v0, -0x3

    if-eq v4, v0, :cond_b

    .line 387
    :try_start_9
    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Decoder failed with status %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_1

    :cond_b
    move v2, v1

    .line 390
    goto/16 :goto_1

    :cond_c
    move v0, v1

    .line 393
    goto :goto_9

    .line 391
    :catch_2
    move-exception v0

    goto :goto_a

    :catch_3
    move-exception v0

    move v7, v2

    goto :goto_a

    :catch_4
    move-exception v0

    move v7, v8

    goto :goto_a

    :catch_5
    move-exception v0

    move v7, v9

    goto :goto_a

    :cond_d
    move v0, v11

    goto/16 :goto_3

    :cond_e
    move v0, v8

    move v9, v2

    goto/16 :goto_2

    :cond_f
    move v1, v2

    goto/16 :goto_5
.end method

.method private b()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lgrq;->n:Lgqg;

    if-eqz v0, :cond_1

    .line 258
    iget-boolean v0, p0, Lgrq;->s:Z

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lgrq;->n:Lgqg;

    invoke-interface {v0}, Lgqg;->c()V

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lgrq;->s:Z

    .line 262
    :cond_0
    iget-object v0, p0, Lgrq;->n:Lgqg;

    invoke-interface {v0}, Lgqg;->a()V

    .line 263
    const/4 v0, 0x0

    iput-object v0, p0, Lgrq;->n:Lgqg;

    .line 265
    :cond_1
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 268
    iget-object v0, p0, Lgrq;->l:Lgrg;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lgrq;->l:Lgrg;

    invoke-interface {v0}, Lgrg;->c()V

    .line 270
    iput-object v1, p0, Lgrq;->l:Lgrg;

    .line 272
    :cond_0
    iget-object v0, p0, Lgrq;->m:Lgqn;

    if-eqz v0, :cond_1

    .line 273
    iget-object v0, p0, Lgrq;->m:Lgqn;

    invoke-virtual {v0}, Lgqn;->a()V

    .line 274
    iput-object v1, p0, Lgrq;->m:Lgqn;

    .line 276
    :cond_1
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 222
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lgrq;->k:Z

    if-nez v0, :cond_0

    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lgrq;->k:Z

    .line 224
    invoke-virtual {p0}, Lgrq;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    :cond_0
    monitor-exit p0

    return-void

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 110
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_7

    iget-object v0, p0, Lgrq;->b:Lgqo;

    iget v0, v0, Lgqo;->c:I

    rsub-int v0, v0, 0x168

    :goto_0
    iget-object v1, p0, Lgrq;->g:Lgri;

    iget v2, p0, Lgrq;->c:I

    iget v4, p0, Lgrq;->d:I

    invoke-interface {v1, v2, v4, v0}, Lgri;->a(III)Lgrg;

    move-result-object v0

    iput-object v0, p0, Lgrq;->l:Lgrg;

    iget-object v0, p0, Lgrq;->e:Lgql;

    invoke-interface {v0}, Lgql;->a()Lgqn;

    move-result-object v0

    iput-object v0, p0, Lgrq;->m:Lgqn;

    iget-object v0, p0, Lgrq;->m:Lgqn;

    iget-object v1, p0, Lgrq;->a:Landroid/content/Context;

    iget-object v2, p0, Lgrq;->b:Lgqo;

    iget-object v2, v2, Lgqo;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v4}, Lgqn;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    iget-object v0, p0, Lgrq;->m:Lgqn;

    iget-object v1, p0, Lgrq;->b:Lgqo;

    iget v1, v1, Lgqo;->b:I

    invoke-virtual {v0, v1}, Lgqn;->b(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lgrh; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 120
    :try_start_1
    iget-object v0, p0, Lgrq;->q:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 123
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lgrq;->k:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_6

    .line 128
    :try_start_2
    iget-object v0, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    const-wide/16 v4, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v1}, Ljava/util/concurrent/PriorityBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrm;

    .line 129
    if-nez v0, :cond_1

    .line 131
    iget-object v0, p0, Lgrq;->i:Lgqh;

    iget-object v1, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0, v1}, Lgqh;->c(Lgqi;)V

    .line 132
    iget-object v0, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgrm;
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 140
    :cond_1
    :try_start_3
    iget-boolean v1, v0, Lgrm;->b:Z

    if-nez v1, :cond_0

    .line 146
    iget-object v1, p0, Lgrq;->i:Lgqh;

    iget-object v2, p0, Lgrq;->j:Lgrr;

    iget v4, v0, Lgrm;->a:I

    invoke-virtual {v1, v2, v4}, Lgqh;->a(Lgqi;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 149
    :try_start_4
    iget-object v1, p0, Lgrq;->j:Lgrr;

    iget-object v2, v1, Lgrr;->c:Lgrq;

    iget-object v2, v2, Lgrq;->i:Lgqh;

    monitor-enter v2
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :try_start_5
    iget-object v4, v1, Lgrr;->c:Lgrq;

    iget-object v4, v4, Lgrq;->i:Lgqh;

    invoke-virtual {v4, v1}, Lgqh;->a(Lgqi;)Z

    move-result v4

    if-eqz v4, :cond_3

    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 157
    :goto_2
    :try_start_6
    iget-object v1, p0, Lgrq;->m:Lgqn;

    iget-object v2, p0, Lgrq;->b:Lgqo;

    iget v2, v2, Lgqo;->b:I

    invoke-virtual {v1, v2}, Lgqn;->a(I)Landroid/media/MediaFormat;

    move-result-object v1

    iget-object v2, p0, Lgrq;->f:Lgqe;

    const-string v4, "mime"

    invoke-virtual {v1, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lgqe;->a(Ljava/lang/String;)Lgqg;

    move-result-object v2

    iput-object v2, p0, Lgrq;->n:Lgqg;

    iget-object v2, p0, Lgrq;->n:Lgqg;

    iget-object v4, p0, Lgrq;->l:Lgrg;

    invoke-interface {v4}, Lgrg;->a()Landroid/view/Surface;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v2, v1, v4, v5, v6}, Lgqg;->a(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    iget-object v1, p0, Lgrq;->n:Lgqg;

    invoke-interface {v1}, Lgqg;->b()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lgrq;->s:Z

    iget-object v1, p0, Lgrq;->n:Lgqg;

    invoke-interface {v1}, Lgqg;->e()[Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lgrq;->p:[Ljava/nio/ByteBuffer;

    .line 158
    invoke-direct {p0, v0}, Lgrq;->a(Lgrm;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    move-result v2

    .line 160
    :try_start_7
    invoke-direct {p0}, Lgrq;->b()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4
    .catchall {:try_start_7 .. :try_end_7} :catchall_5

    .line 166
    if-nez v2, :cond_2

    .line 168
    :try_start_8
    iget-object v1, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_2
    iget-object v0, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0}, Lgrr;->c()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 176
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lgrq;->c()V

    .line 177
    iget-object v1, p0, Lgrq;->i:Lgqh;

    iget-object v2, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v1, v2}, Lgqh;->c(Lgqi;)V

    throw v0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    :try_start_9
    iput-object v0, p0, Lgrq;->r:Ljava/lang/Exception;

    .line 113
    const-string v1, "Unable to read video file - terminating ExtractorThread"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 120
    :try_start_a
    iget-object v0, p0, Lgrq;->q:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 176
    invoke-direct {p0}, Lgrq;->c()V

    .line 177
    iget-object v0, p0, Lgrq;->i:Lgqh;

    iget-object v1, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0, v1}, Lgqh;->c(Lgqi;)V

    .line 178
    :goto_3
    return-void

    .line 115
    :catch_1
    move-exception v0

    .line 116
    :try_start_b
    iput-object v0, p0, Lgrq;->r:Ljava/lang/Exception;

    .line 117
    const-string v1, "Unable to initialize ExtractorSurface - terminating ExtractorThread"

    invoke-static {v1, v0}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 120
    :try_start_c
    iget-object v0, p0, Lgrq;->q:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 176
    invoke-direct {p0}, Lgrq;->c()V

    .line 177
    iget-object v0, p0, Lgrq;->i:Lgqh;

    iget-object v1, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0, v1}, Lgqh;->c(Lgqi;)V

    goto :goto_3

    .line 120
    :catchall_1
    move-exception v0

    :try_start_d
    iget-object v1, p0, Lgrq;->q:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 149
    :cond_3
    :try_start_e
    new-instance v4, Ljava/util/concurrent/CountDownLatch;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v4, v1, Lgrr;->a:Ljava/util/concurrent/CountDownLatch;

    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    iget-object v1, v1, Lgrr;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_2
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    goto/16 :goto_2

    :catch_2
    move-exception v1

    .line 166
    :try_start_10
    iget-object v1, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0}, Lgrr;->c()V
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_1

    .line 149
    :catchall_2
    move-exception v1

    :try_start_11
    monitor-exit v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    :try_start_12
    throw v1
    :try_end_12
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_12} :catch_2
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_3
    .catchall {:try_start_12 .. :try_end_12} :catchall_4

    .line 163
    :catch_3
    move-exception v1

    move v2, v3

    .line 164
    :goto_4
    :try_start_13
    const-string v4, "Failed to execute ExtractorTask"

    invoke-static {v4, v1}, Lezp;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 166
    if-nez v2, :cond_4

    .line 168
    :try_start_14
    iget-object v1, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_4
    iget-object v0, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0}, Lgrr;->c()V
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_1

    .line 160
    :catchall_3
    move-exception v1

    :try_start_15
    invoke-direct {p0}, Lgrq;->b()V

    throw v1
    :try_end_15
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_3
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    .line 166
    :catchall_4
    move-exception v1

    :goto_5
    if-nez v3, :cond_5

    .line 168
    :try_start_16
    iget-object v2, p0, Lgrq;->h:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_5
    iget-object v0, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0}, Lgrr;->c()V

    throw v1
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 174
    :cond_6
    invoke-direct {p0}, Lgrq;->c()V

    .line 177
    iget-object v0, p0, Lgrq;->i:Lgqh;

    iget-object v1, p0, Lgrq;->j:Lgrr;

    invoke-virtual {v0, v1}, Lgqh;->c(Lgqi;)V

    goto :goto_3

    .line 166
    :catchall_5
    move-exception v1

    move v3, v2

    goto :goto_5

    :catchall_6
    move-exception v1

    move v3, v2

    goto :goto_5

    .line 163
    :catch_4
    move-exception v1

    goto :goto_4

    .line 137
    :catch_5
    move-exception v0

    goto/16 :goto_1

    :cond_7
    move v0, v3

    goto/16 :goto_0
.end method
