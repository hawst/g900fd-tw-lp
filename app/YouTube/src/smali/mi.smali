.class public final Lmi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lml;

.field public b:Lml;

.field private final c:Ljava/util/List;

.field private final d:I

.field private e:Lml;

.field private f:Lml;

.field private g:Lml;

.field private h:Lml;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 7

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    iput-object p1, p0, Lmi;->c:Ljava/util/List;

    .line 191
    invoke-direct {p0}, Lmi;->a()I

    move-result v0

    iput v0, p0, Lmi;->d:I

    .line 193
    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f333333    # 0.7f

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3eb33333    # 0.35f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmi;->a(FFFFFF)Lml;

    move-result-object v0

    iput-object v0, p0, Lmi;->a:Lml;

    .line 196
    const v1, 0x3f3d70a4    # 0.74f

    const v2, 0x3f0ccccd    # 0.55f

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3eb33333    # 0.35f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmi;->a(FFFFFF)Lml;

    move-result-object v0

    iput-object v0, p0, Lmi;->g:Lml;

    .line 199
    const v1, 0x3e851eb8    # 0.26f

    const/4 v2, 0x0

    const v3, 0x3ee66666    # 0.45f

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3eb33333    # 0.35f

    const/high16 v6, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmi;->a(FFFFFF)Lml;

    move-result-object v0

    iput-object v0, p0, Lmi;->e:Lml;

    .line 202
    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f333333    # 0.7f

    const v4, 0x3e99999a    # 0.3f

    const/4 v5, 0x0

    const v6, 0x3ecccccd    # 0.4f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmi;->a(FFFFFF)Lml;

    move-result-object v0

    iput-object v0, p0, Lmi;->b:Lml;

    .line 205
    const v1, 0x3f3d70a4    # 0.74f

    const v2, 0x3f0ccccd    # 0.55f

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3e99999a    # 0.3f

    const/4 v5, 0x0

    const v6, 0x3ecccccd    # 0.4f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmi;->a(FFFFFF)Lml;

    move-result-object v0

    iput-object v0, p0, Lmi;->h:Lml;

    .line 208
    const v1, 0x3e851eb8    # 0.26f

    const/4 v2, 0x0

    const v3, 0x3ee66666    # 0.45f

    const v4, 0x3e99999a    # 0.3f

    const/4 v5, 0x0

    const v6, 0x3ecccccd    # 0.4f

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lmi;->a(FFFFFF)Lml;

    move-result-object v0

    iput-object v0, p0, Lmi;->f:Lml;

    .line 212
    iget-object v0, p0, Lmi;->a:Lml;

    if-nez v0, :cond_0

    iget-object v0, p0, Lmi;->e:Lml;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmi;->e:Lml;

    invoke-static {v0}, Lmi;->a(Lml;)[F

    move-result-object v0

    const/4 v1, 0x2

    const/high16 v2, 0x3f000000    # 0.5f

    aput v2, v0, v1

    new-instance v1, Lml;

    invoke-static {v0}, La;->a([F)I

    move-result v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lml;-><init>(II)V

    iput-object v1, p0, Lmi;->a:Lml;

    :cond_0
    iget-object v0, p0, Lmi;->e:Lml;

    if-nez v0, :cond_1

    iget-object v0, p0, Lmi;->a:Lml;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmi;->a:Lml;

    invoke-static {v0}, Lmi;->a(Lml;)[F

    move-result-object v0

    const/4 v1, 0x2

    const v2, 0x3e851eb8    # 0.26f

    aput v2, v0, v1

    new-instance v1, Lml;

    invoke-static {v0}, La;->a([F)I

    move-result v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lml;-><init>(II)V

    iput-object v1, p0, Lmi;->e:Lml;

    .line 213
    :cond_1
    return-void
.end method

.method private static a(FF)F
    .locals 2

    .prologue
    .line 489
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method private a()I
    .locals 3

    .prologue
    .line 380
    const/4 v0, 0x0

    .line 381
    iget-object v1, p0, Lmi;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lml;

    .line 382
    iget v0, v0, Lml;->b:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 383
    goto :goto_0

    .line 384
    :cond_0
    return v1
.end method

.method public static a(Landroid/graphics/Bitmap;Lmk;)Landroid/os/AsyncTask;
    .locals 3

    .prologue
    const/16 v1, 0x10

    .line 157
    invoke-static {p0}, Lmi;->a(Landroid/graphics/Bitmap;)V

    invoke-static {v1}, Lmi;->a(I)V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lmj;

    invoke-direct {v0, v1, p1}, Lmj;-><init>(ILmk;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, La;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    return-object v0
.end method

.method private a(FFFFFF)Lml;
    .locals 10

    .prologue
    .line 329
    const/4 v3, 0x0

    .line 330
    const/4 v1, 0x0

    .line 332
    iget-object v0, p0, Lmi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lml;

    .line 333
    invoke-virtual {v0}, Lml;->a()[F

    move-result-object v2

    const/4 v4, 0x1

    aget v4, v2, v4

    .line 334
    invoke-virtual {v0}, Lml;->a()[F

    move-result-object v2

    const/4 v5, 0x2

    aget v5, v2, v5

    .line 336
    cmpl-float v2, v4, p5

    if-ltz v2, :cond_5

    cmpg-float v2, v4, p6

    if-gtz v2, :cond_5

    cmpl-float v2, v5, p2

    if-ltz v2, :cond_5

    cmpg-float v2, v5, p3

    if-gtz v2, :cond_5

    iget-object v2, p0, Lmi;->a:Lml;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lmi;->e:Lml;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lmi;->g:Lml;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lmi;->b:Lml;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lmi;->f:Lml;

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lmi;->h:Lml;

    if-ne v2, v0, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_5

    .line 339
    iget v2, v0, Lml;->b:I

    iget v7, p0, Lmi;->d:I

    const/4 v8, 0x6

    new-array v8, v8, [F

    const/4 v9, 0x0

    invoke-static {v4, p4}, Lmi;->a(FF)F

    move-result v4

    aput v4, v8, v9

    const/4 v4, 0x1

    const/high16 v9, 0x40400000    # 3.0f

    aput v9, v8, v4

    const/4 v4, 0x2

    invoke-static {v5, p1}, Lmi;->a(FF)F

    move-result v5

    aput v5, v8, v4

    const/4 v4, 0x3

    const/high16 v5, 0x40c00000    # 6.0f

    aput v5, v8, v4

    const/4 v4, 0x4

    int-to-float v2, v2

    int-to-float v5, v7

    div-float/2addr v2, v5

    aput v2, v8, v4

    const/4 v2, 0x5

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v8, v2

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x0

    :goto_2
    const/4 v7, 0x6

    if-ge v2, v7, :cond_2

    aget v7, v8, v2

    add-int/lit8 v9, v2, 0x1

    aget v9, v8, v9

    mul-float/2addr v7, v9

    add-float/2addr v5, v7

    add-float/2addr v4, v9

    add-int/lit8 v2, v2, 0x2

    goto :goto_2

    .line 336
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 339
    :cond_2
    div-float v2, v5, v4

    .line 341
    if-eqz v3, :cond_3

    cmpl-float v4, v2, v1

    if-lez v4, :cond_5

    :cond_3
    move-object v1, v0

    move v0, v2

    :goto_3
    move-object v3, v1

    move v1, v0

    .line 346
    goto/16 :goto_0

    .line 348
    :cond_4
    return-object v3

    :cond_5
    move v0, v1

    move-object v1, v3

    goto :goto_3
.end method

.method static a(I)V
    .locals 2

    .prologue
    .line 517
    if-gtz p0, :cond_0

    .line 518
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "numColors must be 1 of greater"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :cond_0
    return-void
.end method

.method static a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 508
    if-nez p0, :cond_0

    .line 509
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bitmap can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 511
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 512
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "bitmap can not be recycled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 514
    :cond_1
    return-void
.end method

.method private static a(Lml;)[F
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 475
    new-array v0, v3, [F

    .line 476
    invoke-virtual {p0}, Lml;->a()[F

    move-result-object v1

    invoke-static {v1, v2, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 477
    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 389
    if-ne p0, p1, :cond_1

    .line 426
    :cond_0
    :goto_0
    return v0

    .line 392
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 393
    goto :goto_0

    .line 396
    :cond_3
    check-cast p1, Lmi;

    .line 398
    iget-object v2, p0, Lmi;->c:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lmi;->c:Ljava/util/List;

    iget-object v3, p1, Lmi;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 399
    goto :goto_0

    .line 398
    :cond_5
    iget-object v2, p1, Lmi;->c:Ljava/util/List;

    if-nez v2, :cond_4

    .line 401
    :cond_6
    iget-object v2, p0, Lmi;->f:Lml;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lmi;->f:Lml;

    iget-object v3, p1, Lmi;->f:Lml;

    invoke-virtual {v2, v3}, Lml;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 403
    goto :goto_0

    .line 401
    :cond_8
    iget-object v2, p1, Lmi;->f:Lml;

    if-nez v2, :cond_7

    .line 405
    :cond_9
    iget-object v2, p0, Lmi;->e:Lml;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lmi;->e:Lml;

    iget-object v3, p1, Lmi;->e:Lml;

    invoke-virtual {v2, v3}, Lml;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 407
    goto :goto_0

    .line 405
    :cond_b
    iget-object v2, p1, Lmi;->e:Lml;

    if-nez v2, :cond_a

    .line 409
    :cond_c
    iget-object v2, p0, Lmi;->h:Lml;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lmi;->h:Lml;

    iget-object v3, p1, Lmi;->h:Lml;

    invoke-virtual {v2, v3}, Lml;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 411
    goto :goto_0

    .line 409
    :cond_e
    iget-object v2, p1, Lmi;->h:Lml;

    if-nez v2, :cond_d

    .line 413
    :cond_f
    iget-object v2, p0, Lmi;->g:Lml;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lmi;->g:Lml;

    iget-object v3, p1, Lmi;->g:Lml;

    invoke-virtual {v2, v3}, Lml;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 415
    goto :goto_0

    .line 413
    :cond_11
    iget-object v2, p1, Lmi;->g:Lml;

    if-nez v2, :cond_10

    .line 417
    :cond_12
    iget-object v2, p0, Lmi;->b:Lml;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lmi;->b:Lml;

    iget-object v3, p1, Lmi;->b:Lml;

    invoke-virtual {v2, v3}, Lml;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 419
    goto/16 :goto_0

    .line 417
    :cond_14
    iget-object v2, p1, Lmi;->b:Lml;

    if-nez v2, :cond_13

    .line 421
    :cond_15
    iget-object v2, p0, Lmi;->a:Lml;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lmi;->a:Lml;

    iget-object v3, p1, Lmi;->a:Lml;

    invoke-virtual {v2, v3}, Lml;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 423
    goto/16 :goto_0

    .line 421
    :cond_16
    iget-object v2, p1, Lmi;->a:Lml;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 431
    iget-object v0, p0, Lmi;->c:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lmi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 432
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmi;->a:Lml;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmi;->a:Lml;

    invoke-virtual {v0}, Lml;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 433
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmi;->b:Lml;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lmi;->b:Lml;

    invoke-virtual {v0}, Lml;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 434
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmi;->e:Lml;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lmi;->e:Lml;

    invoke-virtual {v0}, Lml;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 435
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmi;->f:Lml;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmi;->f:Lml;

    invoke-virtual {v0}, Lml;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 436
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lmi;->g:Lml;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmi;->g:Lml;

    invoke-virtual {v0}, Lml;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 437
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lmi;->h:Lml;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lmi;->h:Lml;

    invoke-virtual {v1}, Lml;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 438
    return v0

    :cond_1
    move v0, v1

    .line 431
    goto :goto_0

    :cond_2
    move v0, v1

    .line 432
    goto :goto_1

    :cond_3
    move v0, v1

    .line 433
    goto :goto_2

    :cond_4
    move v0, v1

    .line 434
    goto :goto_3

    :cond_5
    move v0, v1

    .line 435
    goto :goto_4

    :cond_6
    move v0, v1

    .line 436
    goto :goto_5
.end method
