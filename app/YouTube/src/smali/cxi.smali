.class public final Lcxi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Levn;

.field b:Lcws;

.field c:I

.field private final d:I

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Levn;I)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Lcxj;

    invoke-direct {v0, p0}, Lcxj;-><init>(Lcxi;)V

    iput-object v0, p0, Lcxi;->f:Ljava/lang/Runnable;

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcxi;->a:Levn;

    .line 33
    iput p2, p0, Lcxi;->d:I

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcxi;->e:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method private handleVideoStageEvent(Ldac;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 48
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->i:Lgol;

    if-ne v0, v1, :cond_0

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcxi;->c:I

    .line 54
    :cond_0
    return-void
.end method


# virtual methods
.method public final handlePlaybackServiceException(Lczb;)V
    .locals 5
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcxi;->b:Lcws;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcxi;->b:Lcws;

    .line 59
    iget-object v3, v0, Lcws;->g:Ldkf;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcws;->g:Ldkf;

    invoke-interface {v0}, Ldkf;->v_()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p1, Lczb;->a:Lczc;

    const/4 v3, 0x6

    new-array v3, v3, [Lczc;

    sget-object v4, Lczc;->c:Lczc;

    aput-object v4, v3, v2

    sget-object v2, Lczc;->b:Lczc;

    aput-object v2, v3, v1

    const/4 v1, 0x2

    sget-object v2, Lczc;->e:Lczc;

    aput-object v2, v3, v1

    const/4 v1, 0x3

    sget-object v2, Lczc;->f:Lczc;

    aput-object v2, v3, v1

    const/4 v1, 0x4

    sget-object v2, Lczc;->k:Lczc;

    aput-object v2, v3, v1

    const/4 v1, 0x5

    sget-object v2, Lczc;->i:Lczc;

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Lczc;->a([Lczc;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcxi;->c:I

    iget v1, p0, Lcxi;->d:I

    if-ge v0, v1, :cond_0

    .line 64
    iget-object v0, p0, Lcxi;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcxi;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 65
    iget v0, p0, Lcxi;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcxi;->c:I

    .line 67
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 59
    goto :goto_0
.end method
