.class public final Lcps;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lezj;

.field public final b:Lgjp;

.field public final c:Levn;

.field public final d:Lexd;

.field public final e:Lezi;

.field public final f:Lggr;

.field public final g:Lefj;

.field public final h:Lefj;

.field public final i:Lggm;

.field public final j:Lgfx;

.field public final k:Lewi;

.field public final l:J

.field public final m:Ldmc;


# direct methods
.method public constructor <init>(Levn;Lgjp;Lezj;Lexd;Lezi;Lggr;Lefj;Lefj;Lggm;Lgfx;Lewi;JLdmc;)V
    .locals 2

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcps;->c:Levn;

    .line 206
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgjp;

    iput-object v0, p0, Lcps;->b:Lgjp;

    .line 207
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcps;->a:Lezj;

    .line 208
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lexd;

    iput-object v0, p0, Lcps;->d:Lexd;

    .line 209
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezi;

    iput-object v0, p0, Lcps;->e:Lezi;

    .line 210
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggr;

    iput-object v0, p0, Lcps;->f:Lggr;

    .line 211
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefj;

    iput-object v0, p0, Lcps;->g:Lefj;

    .line 213
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lefj;

    iput-object v0, p0, Lcps;->h:Lefj;

    .line 214
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggm;

    iput-object v0, p0, Lcps;->i:Lggm;

    .line 215
    invoke-static {p10}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfx;

    iput-object v0, p0, Lcps;->j:Lgfx;

    .line 216
    invoke-static {p11}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lcps;->k:Lewi;

    .line 217
    iput-wide p12, p0, Lcps;->l:J

    .line 218
    invoke-static/range {p14 .. p14}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldmc;

    iput-object v0, p0, Lcps;->m:Ldmc;

    .line 219
    return-void
.end method


# virtual methods
.method public final a(Lfnd;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Lcpo;
    .locals 30

    .prologue
    .line 223
    move-object/from16 v0, p0

    iget-object v2, v0, Lcps;->k:Lewi;

    invoke-interface {v2}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 224
    new-instance v3, Lcpo;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcps;->c:Levn;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcps;->a:Lezj;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcps;->b:Lgjp;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcps;->d:Lexd;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcps;->e:Lezi;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcps;->f:Lggr;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcps;->g:Lefj;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcps;->h:Lefj;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcps;->i:Lggm;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcps;->j:Lgfx;

    if-nez v2, :cond_0

    const-wide/16 v14, -0x1

    .line 235
    :goto_0
    if-nez v2, :cond_1

    const-wide/16 v16, -0x1

    .line 236
    :goto_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcps;->l:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcps;->m:Ldmc;

    move-object/from16 v20, v0

    .line 239
    invoke-static/range {p1 .. p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lfnd;

    iget-object v2, v2, Lfnd;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v21

    .line 240
    invoke-static/range {p2 .. p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 241
    invoke-static/range {p3 .. p3}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    if-eqz p4, :cond_2

    sget-object v24, Lcpt;->b:Lcpt;

    :goto_2
    const-wide/16 v25, -0x1

    const/16 v27, 0x0

    const/16 v29, 0x0

    move-object/from16 v28, p5

    invoke-direct/range {v3 .. v29}, Lcpo;-><init>(Levn;Lezj;Lgjp;Lexd;Lezi;Lggr;Lefj;Lefj;Lggm;Lgfx;JJJLdmc;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Lcpt;JZLjava/lang/String;B)V

    .line 246
    const/4 v2, -0x1

    invoke-static {v3, v2}, Lcpo;->a(Lcpo;I)I

    .line 247
    const/4 v2, -0x1

    invoke-static {v3, v2}, Lcpo;->b(Lcpo;I)I

    .line 248
    invoke-virtual {v3}, Lcpo;->b()V

    .line 249
    return-object v3

    .line 235
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v14

    goto :goto_0

    .line 236
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v16

    goto :goto_1

    .line 241
    :cond_2
    sget-object v24, Lcpt;->a:Lcpt;

    goto :goto_2
.end method
