.class public final Lhql;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v0, p0, Lhql;->a:I

    iput v0, p0, Lhql;->b:I

    iput v0, p0, Lhql;->c:I

    iput v0, p0, Lhql;->d:I

    iput v0, p0, Lhql;->e:I

    iput v0, p0, Lhql;->f:I

    iput v0, p0, Lhql;->g:I

    iput-boolean v0, p0, Lhql;->h:Z

    iput-boolean v0, p0, Lhql;->i:Z

    iput v0, p0, Lhql;->j:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lhql;->a:I

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhql;->a:I

    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget v1, p0, Lhql;->b:I

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    iget v2, p0, Lhql;->b:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lhql;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x3

    iget v2, p0, Lhql;->c:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lhql;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget v2, p0, Lhql;->d:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lhql;->e:I

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Lhql;->e:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lhql;->f:I

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Lhql;->f:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lhql;->g:I

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget v2, p0, Lhql;->g:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-boolean v1, p0, Lhql;->h:Z

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-boolean v2, p0, Lhql;->h:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lhql;->i:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-boolean v2, p0, Lhql;->i:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lhql;->j:I

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget v2, p0, Lhql;->j:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lhql;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhql;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhql;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhql;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhql;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhql;->h:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhql;->i:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhql;->j:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget v0, p0, Lhql;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhql;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_0
    iget v0, p0, Lhql;->b:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Lhql;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_1
    iget v0, p0, Lhql;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lhql;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_2
    iget v0, p0, Lhql;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget v1, p0, Lhql;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_3
    iget v0, p0, Lhql;->e:I

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lhql;->e:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_4
    iget v0, p0, Lhql;->f:I

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Lhql;->f:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_5
    iget v0, p0, Lhql;->g:I

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget v1, p0, Lhql;->g:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_6
    iget-boolean v0, p0, Lhql;->h:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-boolean v1, p0, Lhql;->h:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_7
    iget-boolean v0, p0, Lhql;->i:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lhql;->i:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_8
    iget v0, p0, Lhql;->j:I

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget v1, p0, Lhql;->j:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_9
    iget-object v0, p0, Lhql;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhql;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhql;

    iget v2, p0, Lhql;->a:I

    iget v3, p1, Lhql;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhql;->b:I

    iget v3, p1, Lhql;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhql;->c:I

    iget v3, p1, Lhql;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhql;->d:I

    iget v3, p1, Lhql;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhql;->e:I

    iget v3, p1, Lhql;->e:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhql;->f:I

    iget v3, p1, Lhql;->f:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhql;->g:I

    iget v3, p1, Lhql;->g:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhql;->h:Z

    iget-boolean v3, p1, Lhql;->h:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhql;->i:Z

    iget-boolean v3, p1, Lhql;->i:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhql;->j:I

    iget v3, p1, Lhql;->j:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhql;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhql;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhql;->I:Ljava/util/List;

    iget-object v3, p1, Lhql;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lhql;->a:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lhql;->b:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lhql;->c:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lhql;->d:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lhql;->e:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lhql;->f:I

    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lhql;->g:I

    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lhql;->h:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lhql;->i:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhql;->j:I

    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhql;->I:Ljava/util/List;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lhql;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_2
.end method
