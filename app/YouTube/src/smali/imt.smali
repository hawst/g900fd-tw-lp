.class public final Limt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final h:[B

.field private static final i:[B


# instance fields
.field final a:Lilo;

.field final b:Liln;

.field public final c:Lipu;

.field final d:Lipt;

.field e:I

.field f:I

.field private final g:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 318
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Limt;->h:[B

    .line 321
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Limt;->i:[B

    return-void

    .line 318
    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data

    .line 321
    :array_1
    .array-data 1
        0x30t
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Lilo;Liln;Ljava/net/Socket;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput v0, p0, Limt;->e:I

    .line 80
    iput v0, p0, Limt;->f:I

    .line 84
    iput-object p1, p0, Limt;->a:Lilo;

    .line 85
    iput-object p2, p0, Limt;->b:Liln;

    .line 86
    iput-object p3, p0, Limt;->g:Ljava/net/Socket;

    .line 87
    invoke-static {p3}, Liqa;->b(Ljava/net/Socket;)Liql;

    move-result-object v0

    invoke-static {v0}, Liqa;->a(Liql;)Lipu;

    move-result-object v0

    iput-object v0, p0, Limt;->c:Lipu;

    .line 88
    invoke-static {p3}, Liqa;->a(Ljava/net/Socket;)Liqk;

    move-result-object v0

    invoke-static {v0}, Liqa;->a(Liqk;)Lipt;

    move-result-object v0

    iput-object v0, p0, Limt;->d:Lipt;

    .line 89
    return-void
.end method

.method static synthetic a(Limt;I)I
    .locals 0

    .prologue
    .line 60
    iput p1, p0, Limt;->e:I

    return p1
.end method

.method static synthetic a(Limt;)Lipt;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Limt;->d:Lipt;

    return-object v0
.end method

.method static synthetic b(Limt;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Limt;->e:I

    return v0
.end method

.method static synthetic b(Limt;I)I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput v0, p0, Limt;->f:I

    return v0
.end method

.method static synthetic c(Limt;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Limt;->f:I

    return v0
.end method

.method static synthetic d(Limt;)Lilo;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Limt;->a:Lilo;

    return-object v0
.end method

.method static synthetic d()[B
    .locals 1

    .prologue
    .line 60
    sget-object v0, Limt;->i:[B

    return-object v0
.end method

.method static synthetic e(Limt;)Liln;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Limt;->b:Liln;

    return-object v0
.end method

.method static synthetic e()[B
    .locals 1

    .prologue
    .line 60
    sget-object v0, Limt;->h:[B

    return-object v0
.end method

.method static synthetic f(Limt;)Lipu;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Limt;->c:Lipu;

    return-object v0
.end method


# virtual methods
.method public final a(Lu;J)Liql;
    .locals 4

    .prologue
    .line 254
    iget v0, p0, Limt;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Limt;->e:I

    .line 256
    new-instance v0, Limy;

    invoke-direct {v0, p0, p1, p2, p3}, Limy;-><init>(Limt;Lu;J)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Limt;->d:Lipt;

    invoke-interface {v0}, Lipt;->b()V

    .line 139
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 92
    if-eqz p1, :cond_0

    .line 93
    iget-object v0, p0, Limt;->c:Lipu;

    invoke-interface {v0}, Lipu;->a()Liqm;

    move-result-object v0

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Liqm;->a(JLjava/util/concurrent/TimeUnit;)Liqm;

    .line 95
    :cond_0
    if-eqz p2, :cond_1

    .line 96
    iget-object v0, p0, Limt;->d:Lipt;

    invoke-interface {v0}, Lipt;->a()Liqm;

    move-result-object v0

    int-to-long v2, p2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Liqm;->a(JLjava/util/concurrent/TimeUnit;)Liqm;

    .line 98
    :cond_1
    return-void
.end method

.method public final a(Lils;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 168
    iget v0, p0, Limt;->e:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    iget-object v0, p0, Limt;->d:Lipt;

    invoke-interface {v0, p2}, Lipt;->b(Ljava/lang/String;)Lipt;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lipt;->b(Ljava/lang/String;)Lipt;

    .line 170
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lils;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_1

    .line 171
    iget-object v1, p0, Limt;->d:Lipt;

    invoke-virtual {p1, v0}, Lils;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lipt;->b(Ljava/lang/String;)Lipt;

    move-result-object v1

    const-string v2, ": "

    invoke-interface {v1, v2}, Lipt;->b(Ljava/lang/String;)Lipt;

    move-result-object v1

    invoke-virtual {p1, v0}, Lils;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lipt;->b(Ljava/lang/String;)Lipt;

    move-result-object v1

    const-string v2, "\r\n"

    invoke-interface {v1, v2}, Lipt;->b(Ljava/lang/String;)Lipt;

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 176
    :cond_1
    iget-object v0, p0, Limt;->d:Lipt;

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lipt;->b(Ljava/lang/String;)Lipt;

    .line 177
    const/4 v0, 0x1

    iput v0, p0, Limt;->e:I

    .line 178
    return-void
.end method

.method public final a(Lilt;)V
    .locals 2

    .prologue
    .line 209
    :goto_0
    iget-object v0, p0, Limt;->c:Lipu;

    invoke-interface {v0}, Lipu;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    sget-object v1, Limh;->a:Limh;

    invoke-virtual {v1, p1, v0}, Limh;->a(Lilt;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_0
    return-void
.end method

.method public final a(Liql;I)Z
    .locals 3

    .prologue
    .line 222
    :try_start_0
    iget-object v0, p0, Limt;->g:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getSoTimeout()I

    move-result v1

    .line 223
    iget-object v0, p0, Limt;->g:Ljava/net/Socket;

    const/16 v2, 0x64

    invoke-virtual {v0, v2}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    const/16 v0, 0x64

    :try_start_1
    invoke-static {p1, v0}, Limo;->a(Liql;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 227
    :try_start_2
    iget-object v2, p0, Limt;->g:Ljava/net/Socket;

    invoke-virtual {v2, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 230
    :goto_0
    return v0

    .line 227
    :catchall_0
    move-exception v0

    iget-object v2, p0, Limt;->g:Ljava/net/Socket;

    invoke-virtual {v2, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 230
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 149
    :try_start_0
    iget-object v2, p0, Limt;->g:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 151
    :try_start_1
    iget-object v2, p0, Limt;->g:Ljava/net/Socket;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 152
    iget-object v2, p0, Limt;->c:Lipu;

    invoke-interface {v2}, Lipu;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    :try_start_2
    iget-object v2, p0, Limt;->g:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 162
    :goto_0
    return v0

    .line 155
    :cond_0
    iget-object v2, p0, Limt;->g:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v4, p0, Limt;->g:Ljava/net/Socket;

    invoke-virtual {v4, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v2
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 160
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 162
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public final c()Lime;
    .locals 5

    .prologue
    .line 182
    iget v0, p0, Limt;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Limt;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Limt;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    iget-object v0, p0, Limt;->c:Lipu;

    invoke-interface {v0}, Lipu;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Linm;->a(Ljava/lang/String;)Linm;

    move-result-object v0

    .line 189
    new-instance v1, Lime;

    invoke-direct {v1}, Lime;-><init>()V

    iget-object v2, v0, Linm;->a:Lima;

    iput-object v2, v1, Lime;->b:Lima;

    iget v2, v0, Linm;->b:I

    iput v2, v1, Lime;->c:I

    iget-object v2, v0, Linm;->c:Ljava/lang/String;

    iput-object v2, v1, Lime;->d:Ljava/lang/String;

    .line 194
    new-instance v2, Lilt;

    invoke-direct {v2}, Lilt;-><init>()V

    .line 195
    invoke-virtual {p0, v2}, Limt;->a(Lilt;)V

    .line 196
    sget-object v3, Ling;->c:Ljava/lang/String;

    iget-object v4, v0, Linm;->a:Lima;

    invoke-virtual {v4}, Lima;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lilt;->a(Ljava/lang/String;Ljava/lang/String;)Lilt;

    .line 197
    invoke-virtual {v2}, Lilt;->a()Lils;

    move-result-object v2

    invoke-virtual {v1, v2}, Lime;->a(Lils;)Lime;

    .line 199
    iget v0, v0, Linm;->b:I

    const/16 v2, 0x64

    if-eq v0, v2, :cond_0

    .line 200
    const/4 v0, 0x4

    iput v0, p0, Limt;->e:I

    .line 201
    return-object v1
.end method
