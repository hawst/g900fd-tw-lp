.class public final Lefd;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private final a:Lefc;

.field private final b:Lefg;

.field private final c:[B

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lefc;Lefg;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 20
    iput-boolean v0, p0, Lefd;->d:Z

    .line 21
    iput-boolean v0, p0, Lefd;->e:Z

    .line 28
    iput-object p1, p0, Lefd;->a:Lefc;

    .line 29
    iput-object p2, p0, Lefd;->b:Lefg;

    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lefd;->c:[B

    .line 31
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 67
    iget-boolean v0, p0, Lefd;->d:Z

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lefd;->a:Lefc;

    iget-object v1, p0, Lefd;->b:Lefg;

    invoke-interface {v0, v1}, Lefc;->a(Lefg;)J

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefd;->d:Z

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method public final close()V
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lefd;->e:Z

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lefd;->a:Lefc;

    invoke-interface {v0}, Lefc;->a()V

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefd;->e:Z

    .line 64
    :cond_0
    return-void
.end method

.method public final read()I
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lefd;->c:[B

    invoke-virtual {p0, v0}, Lefd;->read([B)I

    .line 36
    iget-object v0, p0, Lefd;->c:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    return v0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 41
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lefd;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lefd;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 47
    invoke-direct {p0}, Lefd;->a()V

    .line 48
    iget-object v0, p0, Lefd;->a:Lefc;

    invoke-interface {v0, p1, p2, p3}, Lefc;->a([BII)I

    move-result v0

    return v0

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 53
    iget-boolean v0, p0, Lefd;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 54
    invoke-direct {p0}, Lefd;->a()V

    .line 55
    invoke-super {p0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v0

    return-wide v0

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
