.class public final Lcgx;
.super Lfsb;
.source "SourceFile"

# interfaces
.implements Leyo;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Lfvi;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/widget/TextView;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Lboi;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/ImageView;

.field private final o:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lboi;Lfdw;Lfrz;)V
    .locals 3

    .prologue
    .line 65
    invoke-direct {p0, p3, p5, p6}, Lfsb;-><init>(Lfhz;Lfdw;Lfrz;)V

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcgx;->o:Landroid/content/res/Resources;

    .line 67
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 68
    const v1, 0x7f0400fd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgx;->a:Landroid/view/View;

    .line 69
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f080131

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgx;->c:Landroid/widget/TextView;

    .line 70
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgx;->b:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 72
    new-instance v1, Lfvi;

    invoke-direct {v1, p2, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v1, p0, Lcgx;->d:Lfvi;

    .line 73
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f0802de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgx;->e:Landroid/widget/TextView;

    .line 74
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f0802df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcgx;->f:Landroid/widget/ImageView;

    .line 75
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f08012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgx;->g:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f0802e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgx;->m:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f08012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcgx;->n:Landroid/widget/ImageView;

    .line 78
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgx;->h:Landroid/view/View;

    .line 79
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcgx;->i:Landroid/view/View;

    .line 80
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcgx;->j:Lboi;

    .line 82
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    new-instance v1, Lcgy;

    invoke-direct {v1, p0}, Lcgy;-><init>(Lcgx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    return-void
.end method

.method private a(Lfsg;Lflx;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f020097

    const/4 v6, 0x4

    const/4 v0, 0x1

    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 92
    invoke-super {p0, p1, p2}, Lfsb;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 93
    iget-object v2, p0, Lcgx;->b:Landroid/widget/TextView;

    invoke-virtual {p2}, Lflx;->c()Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v2, p0, Lcgx;->c:Landroid/widget/TextView;

    iget-object v3, p2, Lflx;->a:Lhsh;

    iget-object v3, v3, Lhsh;->i:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v2, p0, Lcgx;->g:Landroid/widget/TextView;

    iget-object v3, p2, Lflx;->a:Lhsh;

    iget-object v3, v3, Lhsh;->c:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v2, p0, Lcgx;->e:Landroid/widget/TextView;

    iget-object v3, p2, Lflx;->a:Lhsh;

    iget-object v3, v3, Lhsh;->d:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v2, p0, Lcgx;->h:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setBackgroundResource(I)V

    .line 100
    invoke-virtual {p2}, Lflx;->f()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 101
    iget-object v2, p0, Lcgx;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v2, p0, Lcgx;->g:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, p0, Lcgx;->b:Landroid/widget/TextView;

    iget-object v3, p0, Lcgx;->o:Landroid/content/res/Resources;

    const v4, 0x7f07009e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcgx;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p2}, Lflx;->g()Lfnc;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lfnc;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcgx;->a()V

    :goto_1
    iget-object v0, p2, Lflx;->a:Lhsh;

    iget-object v0, v0, Lhsh;->g:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iget-object v0, p0, Lcgx;->m:Landroid/widget/TextView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v3, 0x7f0802e0

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcgx;->m:Landroid/widget/TextView;

    :cond_0
    iget-object v0, p0, Lcgx;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcgx;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    :cond_1
    :goto_2
    iget-object v0, p2, Lflx;->a:Lhsh;

    iget-boolean v0, v0, Lhsh;->e:Z

    if-eqz v0, :cond_6

    .line 106
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v2, 0x7f02008d

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcgx;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcgx;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 110
    :goto_3
    iget-object v0, p0, Lcgx;->h:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcgx;->d:Lfvi;

    invoke-virtual {p2}, Lflx;->g()Lfnc;

    move-result-object v2

    invoke-virtual {v0, v2, p0}, Lfvi;->a(Lfnc;Leyo;)V

    .line 111
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    iget v2, p2, Lflx;->b:F

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 113
    iget-object v0, p0, Lcgx;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcgx;->j:Lboi;

    iget-object v1, p0, Lcgx;->i:Landroid/view/View;

    invoke-static {v0, v1, p2}, La;->a(Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    return-object v0

    :cond_2
    move v0, v1

    .line 101
    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcgx;->n:Landroid/widget/ImageView;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v2, 0x7f08012d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcgx;->n:Landroid/widget/ImageView;

    :cond_4
    iget-object v0, p0, Lcgx;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 103
    :cond_5
    iget-object v0, p0, Lcgx;->b:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcgx;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcgx;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcgx;->o:Landroid/content/res/Resources;

    const v3, 0x7f07009d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcgx;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-direct {p0}, Lcgx;->a()V

    iget-object v0, p0, Lcgx;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcgx;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 108
    :cond_6
    iget-object v0, p0, Lcgx;->a:Landroid/view/View;

    const v2, 0x7f02008c

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lcgx;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcgx;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method private a()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcgx;->n:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcgx;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 144
    :cond_0
    return-void
.end method

.method static synthetic a(Lcgx;)V
    .locals 0

    .prologue
    .line 39
    invoke-virtual {p0}, Lcgx;->b()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lflx;

    invoke-direct {p0, p1, p2}, Lcgx;->a(Lfsg;Lflx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lflx;

    invoke-direct {p0, p1, p2}, Lcgx;->a(Lfsg;Lflx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 39
    check-cast p2, Lflx;

    invoke-direct {p0, p1, p2}, Lcgx;->a(Lfsg;Lflx;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 248
    return-void
.end method

.method public final a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcgx;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 189
    return-void
.end method

.method public final b(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 192
    return-void
.end method

.method public final c(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 195
    return-void
.end method
