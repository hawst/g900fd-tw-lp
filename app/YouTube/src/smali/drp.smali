.class Ldrp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lorg/json/JSONObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Ldrp;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldrp;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Ldrp;->b:Lorg/json/JSONObject;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Ldst;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 41
    :try_start_0
    iget-object v1, p0, Ldrp;->b:Lorg/json/JSONObject;

    if-nez v1, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-object v0

    .line 45
    :cond_1
    iget-object v1, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v2, "accessType"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 46
    if-eqz v1, :cond_0

    .line 50
    :try_start_1
    invoke-static {v1}, Ldsw;->a(Ljava/lang/String;)Ldsw;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    .line 58
    :try_start_2
    iget-object v2, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v3, "name"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 60
    iget-object v2, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v3, "loungeToken"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    sget-object v3, Ldsw;->a:Ldsw;

    if-ne v1, v3, :cond_4

    .line 62
    iget-object v1, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v3, "screenId"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 63
    sget-object v1, Ldrp;->a:Ljava/lang/String;

    iget-object v2, p0, Ldrp;->b:Lorg/json/JSONObject;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "We got a permanent screen without a screen id. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v1

    .line 86
    sget-object v2, Ldrp;->a:Ljava/lang/String;

    const-string v3, "Error parsing screen "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 53
    :catch_1
    move-exception v2

    .line 54
    :try_start_3
    sget-object v3, Ldrp;->a:Ljava/lang/String;

    const-string v4, "Unknown access type "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-static {v3, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 66
    :cond_3
    new-instance v5, Ldth;

    iget-object v1, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v3, "screenId"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ldth;-><init>(Ljava/lang/String;)V

    .line 68
    iget-object v1, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v3, "loungeToken"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 69
    new-instance v1, Ldtb;

    invoke-direct {v1, v2}, Ldtb;-><init>(Ljava/lang/String;)V

    move-object v3, v1

    .line 72
    :goto_2
    iget-object v1, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v2, "clientName"

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 73
    if-eqz v2, :cond_6

    .line 74
    new-instance v1, Ldss;

    invoke-direct {v1, v2}, Ldss;-><init>(Ljava/lang/String;)V

    move-object v2, v1

    .line 76
    :goto_3
    new-instance v1, Ldst;

    invoke-direct {v1, v5, v4, v2, v3}, Ldst;-><init>(Ldth;Ljava/lang/String;Ldss;Ldtb;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 78
    :cond_4
    iget-object v1, p0, Ldrp;->b:Lorg/json/JSONObject;

    const-string v3, "loungeToken"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 79
    sget-object v1, Ldrp;->a:Ljava/lang/String;

    iget-object v2, p0, Ldrp;->b:Lorg/json/JSONObject;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "We got a temporary screen without a loungeToken. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 82
    :cond_5
    new-instance v3, Ldtl;

    invoke-direct {v3, v2}, Ldtl;-><init>(Ljava/lang/String;)V

    .line 83
    new-instance v1, Ldst;

    invoke-direct {v1, v3, v4}, Ldst;-><init>(Ldtl;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    move-object v2, v0

    goto :goto_3

    :cond_7
    move-object v3, v0

    goto :goto_2
.end method
