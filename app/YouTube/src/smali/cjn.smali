.class public final Lcjn;
.super Lcjh;
.source "SourceFile"


# instance fields
.field final c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field final d:Lcyc;

.field final e:Levn;

.field final f:Lfxe;

.field final g:Lffb;

.field final h:Ljava/util/concurrent/atomic/AtomicReference;

.field final i:Lflv;

.field final j:Leyt;

.field k:Landroid/app/AlertDialog;

.field private final l:Lcjq;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcyc;Levn;Lfxe;Lffb;Ljava/util/concurrent/atomic/AtomicReference;Lflv;Leyt;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lcjh;-><init>()V

    .line 69
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lcjn;->c:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 70
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcyc;

    iput-object v0, p0, Lcjn;->d:Lcyc;

    .line 71
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcjn;->e:Levn;

    .line 72
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lcjn;->f:Lfxe;

    .line 73
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffb;

    iput-object v0, p0, Lcjn;->g:Lffb;

    .line 74
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicReference;

    iput-object v0, p0, Lcjn;->h:Ljava/util/concurrent/atomic/AtomicReference;

    .line 75
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflv;

    iput-object v0, p0, Lcjn;->i:Lflv;

    .line 76
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lcjn;->j:Leyt;

    .line 78
    iget-object v0, p0, Lcjn;->i:Lflv;

    iget-object v1, v0, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->n:Lhge;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lflv;->a:Lhsb;

    iget-object v0, v0, Lhsb;->n:Lhge;

    iget-boolean v0, v0, Lhge;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Lcjq;

    invoke-direct {v0, p0}, Lcjq;-><init>(Lcjn;)V

    :goto_1
    iput-object v0, p0, Lcjn;->l:Lcjq;

    .line 79
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcjn;->e:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 84
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcjn;->e:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method public final c()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcjn;->l:Lcjq;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcjn;->l:Lcjq;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    .line 96
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    goto :goto_0
.end method

.method public final handleVideoAddedToPlaylistEvent(Lfez;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcjn;->i:Lflv;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcjh;->a:Lcjj;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lfez;->a:Ljava/lang/String;

    iget-object v1, p0, Lcjn;->i:Lflv;

    .line 114
    iget-object v1, v1, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcjh;->a:Lcjj;

    invoke-interface {v0}, Lcjj;->n_()V

    .line 119
    :cond_0
    return-void
.end method

.method public final handleVideoRemovedFromPlaylistEvent(Lffa;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcjn;->i:Lflv;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcjh;->a:Lcjj;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lffa;->a:Ljava/lang/String;

    iget-object v1, p0, Lcjn;->i:Lflv;

    .line 103
    iget-object v1, v1, Lflv;->a:Lhsb;

    iget-object v1, v1, Lhsb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcjh;->a:Lcjj;

    invoke-interface {v0}, Lcjj;->n_()V

    .line 108
    :cond_0
    return-void
.end method
