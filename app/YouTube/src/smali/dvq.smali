.class public final Ldvq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldxe;


# instance fields
.field private final a:Ldwq;

.field private final b:Lduy;

.field private final c:Lezj;

.field private final d:Ldyg;

.field private final e:Landroid/os/Handler;

.field private final f:I

.field private g:I

.field private h:J


# direct methods
.method public constructor <init>(Ldwq;Lduy;ILezj;Ldyg;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Ldvq;->g:I

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ldvq;->h:J

    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwq;

    iput-object v0, p0, Ldvq;->a:Ldwq;

    .line 40
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lduy;

    iput-object v0, p0, Ldvq;->b:Lduy;

    .line 41
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Ldvq;->c:Lezj;

    .line 42
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldyg;

    iput-object v0, p0, Ldvq;->d:Ldyg;

    .line 44
    invoke-static {v2}, Lb;->b(Z)V

    .line 45
    invoke-static {v2}, Lb;->b(Z)V

    .line 46
    const/4 v0, 0x3

    iput v0, p0, Ldvq;->f:I

    .line 48
    new-instance v0, Ldvr;

    invoke-direct {v0, p0}, Ldvr;-><init>(Ldvq;)V

    iput-object v0, p0, Ldvq;->e:Landroid/os/Handler;

    .line 63
    return-void
.end method

.method private c(I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xc8

    const/4 v4, 0x0

    .line 85
    invoke-direct {p0}, Ldvq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Ldvq;->d:Ldyg;

    const-string v1, "Remote control is not connected, cannot change volume"

    invoke-virtual {v0, v1}, Ldyg;->c(Ljava/lang/String;)V

    .line 101
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v0, p0, Ldvq;->e:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 91
    iget v0, p0, Ldvq;->g:I

    add-int/2addr v0, p1

    iput v0, p0, Ldvq;->g:I

    .line 93
    iget-object v0, p0, Ldvq;->c:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iget-wide v2, p0, Ldvq;->h:J

    sub-long/2addr v0, v2

    .line 94
    cmp-long v2, v0, v6

    if-ltz v2, :cond_1

    .line 95
    invoke-virtual {p0}, Ldvq;->c()V

    goto :goto_0

    .line 97
    :cond_1
    iget-object v2, p0, Ldvq;->e:Landroid/os/Handler;

    iget-object v3, p0, Ldvq;->e:Landroid/os/Handler;

    .line 98
    invoke-static {v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    sub-long v0, v6, v0

    .line 97
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Ldvq;->b:Lduy;

    iget-object v0, v0, Lduy;->d:Ldww;

    sget-object v1, Ldww;->b:Ldww;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ldvq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Ldvq;->d:Ldyg;

    const-string v1, "Remote control is not connected, cannot change volume"

    invoke-virtual {v0, v1}, Ldyg;->c(Ljava/lang/String;)V

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    iget v0, p0, Ldvq;->f:I

    invoke-direct {p0, v0}, Ldvq;->c(I)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xc8

    const/4 v5, 0x1

    .line 105
    invoke-direct {p0}, Ldvq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Ldvq;->d:Ldyg;

    const-string v1, "Remote control is not connected, cannot change volume"

    invoke-virtual {v0, v1}, Ldyg;->c(Ljava/lang/String;)V

    .line 118
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Ldvq;->e:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 110
    iget-object v0, p0, Ldvq;->c:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iget-wide v2, p0, Ldvq;->h:J

    sub-long/2addr v0, v2

    .line 111
    cmp-long v2, v0, v6

    if-ltz v2, :cond_1

    .line 112
    invoke-virtual {p0, p1}, Ldvq;->b(I)V

    goto :goto_0

    .line 114
    :cond_1
    iget-object v2, p0, Ldvq;->e:Landroid/os/Handler;

    iget-object v3, p0, Ldvq;->e:Landroid/os/Handler;

    const/4 v4, 0x0

    .line 115
    invoke-static {v3, v5, p1, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v3

    sub-long v0, v6, v0

    .line 114
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Ldvq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    iget-object v0, p0, Ldvq;->d:Ldyg;

    const-string v1, "Remote control is not connected, cannot change volume"

    invoke-virtual {v0, v1}, Ldyg;->c(Ljava/lang/String;)V

    .line 81
    :goto_0
    return-void

    .line 80
    :cond_0
    iget v0, p0, Ldvq;->f:I

    neg-int v0, v0

    invoke-direct {p0, v0}, Ldvq;->c(I)V

    goto :goto_0
.end method

.method b(I)V
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Ldvq;->a:Ldwq;

    invoke-interface {v0, p1}, Ldwq;->b(I)V

    .line 138
    iget-object v0, p0, Ldvq;->c:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iput-wide v0, p0, Ldvq;->h:J

    .line 139
    return-void
.end method

.method c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 126
    iget v0, p0, Ldvq;->g:I

    if-eqz v0, :cond_0

    .line 127
    const/16 v0, 0x64

    .line 128
    iget-object v1, p0, Ldvq;->b:Lduy;

    iget v1, v1, Lduy;->r:I

    iget v2, p0, Ldvq;->g:I

    add-int/2addr v1, v2

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 129
    iget-object v1, p0, Ldvq;->a:Ldwq;

    iget v2, p0, Ldvq;->g:I

    invoke-interface {v1, v0, v2}, Ldwq;->a(II)V

    .line 130
    iget-object v0, p0, Ldvq;->c:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iput-wide v0, p0, Ldvq;->h:J

    .line 132
    iput v3, p0, Ldvq;->g:I

    .line 134
    :cond_0
    return-void
.end method

.method public final onMdxVolumeChangeEvent(Ldxd;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 143
    const/4 v0, 0x0

    iput v0, p0, Ldvq;->g:I

    .line 144
    return-void
.end method
