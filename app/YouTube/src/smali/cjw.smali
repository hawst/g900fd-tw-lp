.class final Lcjw;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field private synthetic b:Lcjs;


# direct methods
.method public constructor <init>(Lcjs;)V
    .locals 1

    .prologue
    .line 675
    iput-object p1, p0, Lcjw;->b:Lcjs;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 676
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcjw;->a:Ljava/util/List;

    .line 677
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcjw;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcjw;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 671
    invoke-virtual {p0, p1}, Lcjw;->a(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 699
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 704
    check-cast p2, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 705
    if-nez p2, :cond_0

    .line 706
    iget-object v0, p0, Lcjw;->b:Lcjs;

    iget-object v0, v0, Lcjs;->a:Landroid/app/Activity;

    const v1, 0x7f04003a

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;

    .line 708
    :goto_0
    invoke-virtual {p0, p1}, Lcjw;->a(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 709
    packed-switch v3, :pswitch_data_0

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setText(I)V

    .line 710
    packed-switch v3, :pswitch_data_1

    move v1, v2

    :goto_2
    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/libraries/youtube/common/ui/YouTubeTextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 711
    return-object v0

    .line 709
    :pswitch_0
    const v1, 0x7f0902bc

    goto :goto_1

    :pswitch_1
    const v1, 0x7f0902bb

    goto :goto_1

    :pswitch_2
    const v1, 0x7f0902c4

    goto :goto_1

    .line 710
    :pswitch_3
    const v1, 0x7f0200fd

    goto :goto_2

    :pswitch_4
    const v1, 0x7f0200ff

    goto :goto_2

    :pswitch_5
    const v1, 0x7f0200fc

    goto :goto_2

    :cond_0
    move-object v0, p2

    goto :goto_0

    .line 709
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 710
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
