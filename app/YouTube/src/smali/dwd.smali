.class public final Ldwd;
.super Ldwr;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ldst;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Ldwe;

    invoke-direct {v0}, Ldwe;-><init>()V

    sput-object v0, Ldwd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ldst;)V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Ldws;->a:Ldws;

    invoke-direct {p0, v0}, Ldwr;-><init>(Ldws;)V

    .line 17
    iput-object p1, p0, Ldwd;->a:Ldst;

    .line 18
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Ldwd;->a:Ldst;

    iget-object v0, v0, Ldst;->b:Ldth;

    invoke-virtual {v0}, Ldth;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ldwd;->a:Ldst;

    iget-object v0, v0, Ldst;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ldwd;
    .locals 0

    .prologue
    .line 41
    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Ldwr;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 86
    :goto_0
    return v0

    .line 85
    :cond_0
    check-cast p1, Ldwd;

    .line 86
    iget-object v0, p1, Ldwd;->a:Ldst;

    iget-object v1, p0, Ldwd;->a:Ldst;

    invoke-virtual {v0, v1}, Ldst;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Ldwr;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x83

    .line 71
    mul-int/lit16 v0, v0, 0x83

    iget-object v1, p0, Ldwd;->a:Ldst;

    invoke-virtual {v1}, Ldst;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Ldwd;->a:Ldst;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "MdxCloudScreen [screen="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0, p1, p2}, Ldwr;->writeToParcel(Landroid/os/Parcel;I)V

    .line 47
    iget-object v0, p0, Ldwd;->a:Ldst;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 48
    return-void
.end method
