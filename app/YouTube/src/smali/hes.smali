.class public final Lhes;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lheb;

.field public b:Lhbb;

.field public c:Lian;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41233
    invoke-direct {p0}, Lidf;-><init>()V

    .line 41236
    iput-object v0, p0, Lhes;->a:Lheb;

    .line 41239
    iput-object v0, p0, Lhes;->b:Lhbb;

    .line 41242
    iput-object v0, p0, Lhes;->c:Lian;

    .line 41233
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 41291
    const/4 v0, 0x0

    .line 41292
    iget-object v1, p0, Lhes;->a:Lheb;

    if-eqz v1, :cond_0

    .line 41293
    const v0, 0x3049143

    iget-object v1, p0, Lhes;->a:Lheb;

    .line 41294
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 41296
    :cond_0
    iget-object v1, p0, Lhes;->b:Lhbb;

    if-eqz v1, :cond_1

    .line 41297
    const v1, 0x3e1ae1d

    iget-object v2, p0, Lhes;->b:Lhbb;

    .line 41298
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41300
    :cond_1
    iget-object v1, p0, Lhes;->c:Lian;

    if-eqz v1, :cond_2

    .line 41301
    const v1, 0x4913a5c

    iget-object v2, p0, Lhes;->c:Lian;

    .line 41302
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41304
    :cond_2
    iget-object v1, p0, Lhes;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 41305
    iput v0, p0, Lhes;->J:I

    .line 41306
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 41229
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhes;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhes;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhes;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhes;->a:Lheb;

    if-nez v0, :cond_2

    new-instance v0, Lheb;

    invoke-direct {v0}, Lheb;-><init>()V

    iput-object v0, p0, Lhes;->a:Lheb;

    :cond_2
    iget-object v0, p0, Lhes;->a:Lheb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhes;->b:Lhbb;

    if-nez v0, :cond_3

    new-instance v0, Lhbb;

    invoke-direct {v0}, Lhbb;-><init>()V

    iput-object v0, p0, Lhes;->b:Lhbb;

    :cond_3
    iget-object v0, p0, Lhes;->b:Lhbb;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhes;->c:Lian;

    if-nez v0, :cond_4

    new-instance v0, Lian;

    invoke-direct {v0}, Lian;-><init>()V

    iput-object v0, p0, Lhes;->c:Lian;

    :cond_4
    iget-object v0, p0, Lhes;->c:Lian;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18248a1a -> :sswitch_1
        0x1f0d70ea -> :sswitch_2
        0x2489d2e2 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 41276
    iget-object v0, p0, Lhes;->a:Lheb;

    if-eqz v0, :cond_0

    .line 41277
    const v0, 0x3049143

    iget-object v1, p0, Lhes;->a:Lheb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41279
    :cond_0
    iget-object v0, p0, Lhes;->b:Lhbb;

    if-eqz v0, :cond_1

    .line 41280
    const v0, 0x3e1ae1d

    iget-object v1, p0, Lhes;->b:Lhbb;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41282
    :cond_1
    iget-object v0, p0, Lhes;->c:Lian;

    if-eqz v0, :cond_2

    .line 41283
    const v0, 0x4913a5c

    iget-object v1, p0, Lhes;->c:Lian;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 41285
    :cond_2
    iget-object v0, p0, Lhes;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 41287
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41255
    if-ne p1, p0, :cond_1

    .line 41261
    :cond_0
    :goto_0
    return v0

    .line 41256
    :cond_1
    instance-of v2, p1, Lhes;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 41257
    :cond_2
    check-cast p1, Lhes;

    .line 41258
    iget-object v2, p0, Lhes;->a:Lheb;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhes;->a:Lheb;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhes;->b:Lhbb;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhes;->b:Lhbb;

    if-nez v2, :cond_3

    .line 41259
    :goto_2
    iget-object v2, p0, Lhes;->c:Lian;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhes;->c:Lian;

    if-nez v2, :cond_3

    .line 41260
    :goto_3
    iget-object v2, p0, Lhes;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhes;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 41261
    goto :goto_0

    .line 41258
    :cond_4
    iget-object v2, p0, Lhes;->a:Lheb;

    iget-object v3, p1, Lhes;->a:Lheb;

    invoke-virtual {v2, v3}, Lheb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhes;->b:Lhbb;

    iget-object v3, p1, Lhes;->b:Lhbb;

    .line 41259
    invoke-virtual {v2, v3}, Lhbb;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhes;->c:Lian;

    iget-object v3, p1, Lhes;->c:Lian;

    .line 41260
    invoke-virtual {v2, v3}, Lian;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhes;->I:Ljava/util/List;

    iget-object v3, p1, Lhes;->I:Ljava/util/List;

    .line 41261
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 41265
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 41267
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhes;->a:Lheb;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 41268
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhes;->b:Lhbb;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 41269
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhes;->c:Lian;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 41270
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhes;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 41271
    return v0

    .line 41267
    :cond_0
    iget-object v0, p0, Lhes;->a:Lheb;

    invoke-virtual {v0}, Lheb;->hashCode()I

    move-result v0

    goto :goto_0

    .line 41268
    :cond_1
    iget-object v0, p0, Lhes;->b:Lhbb;

    invoke-virtual {v0}, Lhbb;->hashCode()I

    move-result v0

    goto :goto_1

    .line 41269
    :cond_2
    iget-object v0, p0, Lhes;->c:Lian;

    invoke-virtual {v0}, Lian;->hashCode()I

    move-result v0

    goto :goto_2

    .line 41270
    :cond_3
    iget-object v1, p0, Lhes;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
