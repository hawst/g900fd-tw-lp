.class public final Lfnk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfqh;


# instance fields
.field public final a:Lhzs;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Lfkn;

.field public f:Lfkv;

.field private final g:Lfqh;

.field private h:Ljava/lang/CharSequence;

.field private i:Ljava/lang/CharSequence;

.field private j:Lfkt;

.field private k:Ljava/util/ArrayList;

.field private l:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lhzs;Lfqh;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzs;

    iput-object v0, p0, Lfnk;->a:Lhzs;

    .line 45
    iput-object p2, p0, Lfnk;->g:Lfqh;

    .line 46
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lfnk;->h:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lfnk;->a:Lhzs;

    iget-object v0, v0, Lhzs;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnk;->h:Ljava/lang/CharSequence;

    .line 59
    :cond_0
    iget-object v0, p0, Lfnk;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lfnk;->i:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lfnk;->a:Lhzs;

    iget-object v0, v0, Lhzs;->j:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnk;->i:Ljava/lang/CharSequence;

    .line 127
    :cond_0
    iget-object v0, p0, Lfnk;->i:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 3

    .prologue
    .line 143
    iget-object v0, p0, Lfnk;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lfnk;->g()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 144
    invoke-virtual {p0}, Lfnk;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbt;

    .line 145
    instance-of v2, v0, Lfna;

    if-eqz v2, :cond_0

    .line 146
    iget-object v2, p0, Lfnk;->l:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 147
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lfnk;->l:Ljava/util/ArrayList;

    .line 149
    :cond_1
    iget-object v2, p0, Lfnk;->l:Ljava/util/ArrayList;

    check-cast v0, Lfna;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 153
    :cond_2
    iget-object v0, p0, Lfnk;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lfnk;->a:Lhzs;

    iget-object v0, v0, Lhzs;->i:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lfnk;->g:Lfqh;

    return-object v0
.end method

.method public final f()Lfkt;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lfnk;->j:Lfkt;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnk;->a:Lhzs;

    iget-object v0, v0, Lhzs;->k:Lhzt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnk;->a:Lhzs;

    iget-object v0, v0, Lhzs;->k:Lhzt;

    iget-object v0, v0, Lhzt;->a:Lhnl;

    if-eqz v0, :cond_0

    .line 169
    new-instance v0, Lfkt;

    iget-object v1, p0, Lfnk;->a:Lhzs;

    iget-object v1, v1, Lhzs;->k:Lhzt;

    iget-object v1, v1, Lhzt;->a:Lhnl;

    invoke-direct {v0, v1}, Lfkt;-><init>(Lhnl;)V

    iput-object v0, p0, Lfnk;->j:Lfkt;

    .line 172
    :cond_0
    iget-object v0, p0, Lfnk;->j:Lfkt;

    return-object v0
.end method

.method public g()Ljava/util/List;
    .locals 7

    .prologue
    .line 186
    iget-object v0, p0, Lfnk;->k:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    .line 187
    iget-object v0, p0, Lfnk;->a:Lhzs;

    iget-object v1, v0, Lhzs;->h:[Lhbi;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 188
    iget-object v4, v3, Lhbi;->c:Lhxb;

    if-eqz v4, :cond_1

    .line 189
    iget-object v4, p0, Lfnk;->k:Ljava/util/ArrayList;

    if-nez v4, :cond_0

    .line 190
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lfnk;->k:Ljava/util/ArrayList;

    .line 192
    :cond_0
    iget-object v4, p0, Lfnk;->k:Ljava/util/ArrayList;

    new-instance v5, Lfna;

    iget-object v6, v3, Lhbi;->c:Lhxb;

    invoke-direct {v5, v6}, Lfna;-><init>(Lhxb;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_1
    iget-object v4, v3, Lhbi;->d:Lhnx;

    if-eqz v4, :cond_3

    .line 195
    iget-object v4, p0, Lfnk;->k:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 196
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lfnk;->k:Ljava/util/ArrayList;

    .line 198
    :cond_2
    iget-object v4, p0, Lfnk;->k:Ljava/util/ArrayList;

    new-instance v5, Lfkv;

    iget-object v3, v3, Lhbi;->d:Lhnx;

    invoke-direct {v5, v3, p0}, Lfkv;-><init>(Lhnx;Lfqh;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 202
    :cond_4
    iget-object v0, p0, Lfnk;->k:Ljava/util/ArrayList;

    return-object v0
.end method
