.class public final Lbnt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbog;


# instance fields
.field final a:Lfhz;

.field b:Lfim;

.field private final c:Landroid/app/Activity;

.field private final d:Leyp;

.field private final e:Landroid/view/ViewStub;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/RatingBar;

.field private l:Landroid/widget/ImageView;

.field private m:Z

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Lfvi;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Leyp;Lfhz;Landroid/view/ViewStub;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lbnt;->c:Landroid/app/Activity;

    .line 59
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lbnt;->d:Leyp;

    .line 60
    iput-object p3, p0, Lbnt;->a:Lfhz;

    .line 61
    iput-object p4, p0, Lbnt;->e:Landroid/view/ViewStub;

    .line 62
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 147
    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lbnt;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 149
    iget-object v0, p0, Lbnt;->k:Landroid/widget/RatingBar;

    invoke-virtual {v0, v2}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lbnt;->l:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 151
    iget-object v0, p0, Lbnt;->l:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 152
    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 153
    iget-object v0, p0, Lbnt;->o:Lfvi;

    invoke-virtual {v0}, Lfvi;->a()V

    .line 155
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 171
    iget-boolean v0, p0, Lbnt;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbnt;->b:Lfim;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 173
    :goto_0
    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 177
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 172
    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lfoy;Lfnx;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 95
    iget-object v0, p2, Lfnx;->l:Lfim;

    if-nez v0, :cond_0

    iget-object v0, p2, Lfnx;->a:Libi;

    iget-object v0, v0, Libi;->f:Lhee;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lfnx;->a:Libi;

    iget-object v0, v0, Libi;->f:Lhee;

    iget-object v0, v0, Lhee;->b:Lhan;

    if-eqz v0, :cond_0

    new-instance v0, Lfim;

    iget-object v3, p2, Lfnx;->a:Libi;

    iget-object v3, v3, Libi;->f:Lhee;

    iget-object v3, v3, Lhee;->b:Lhan;

    invoke-direct {v0, v3}, Lfim;-><init>(Lhan;)V

    iput-object v0, p2, Lfnx;->l:Lfim;

    :cond_0
    iget-object v0, p2, Lfnx;->l:Lfim;

    iput-object v0, p0, Lbnt;->b:Lfim;

    .line 96
    iget-object v0, p0, Lbnt;->b:Lfim;

    if-nez v0, :cond_1

    move v0, v1

    .line 112
    :goto_0
    return v0

    .line 99
    :cond_1
    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    if-nez v0, :cond_2

    iget-object v0, p0, Lbnt;->e:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbnt;->f:Landroid/view/View;

    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    const v3, 0x7f0800f1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbnt;->g:Landroid/widget/ImageView;

    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    const v3, 0x7f0800f3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnt;->h:Landroid/widget/TextView;

    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    const v3, 0x7f0800f4

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lbnt;->k:Landroid/widget/RatingBar;

    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    const v3, 0x7f0800f5

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbnt;->l:Landroid/widget/ImageView;

    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    const v3, 0x7f0800f6

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnt;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lbnt;->c:Landroid/app/Activity;

    const v3, 0x7f02016b

    invoke-static {v0, v3}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbnt;->n:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lbnt;->n:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lbnt;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget-object v4, p0, Lbnt;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v0, v1, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    const v3, 0x7f0800f2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbnt;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lbnt;->i:Landroid/widget/TextView;

    new-instance v3, Lbnu;

    invoke-direct {v3, p0}, Lbnu;-><init>(Lbnt;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lfvi;

    iget-object v0, p0, Lbnt;->d:Leyp;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iget-object v4, p0, Lbnt;->g:Landroid/widget/ImageView;

    invoke-direct {v3, v0, v4, v2}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;Z)V

    iput-object v3, p0, Lbnt;->o:Lfvi;

    invoke-direct {p0}, Lbnt;->g()V

    .line 101
    :cond_2
    iget-object v0, p0, Lbnt;->h:Landroid/widget/TextView;

    iget-object v3, p0, Lbnt;->b:Lfim;

    iget-object v4, v3, Lfim;->c:Ljava/lang/String;

    if-nez v4, :cond_3

    iget-object v4, v3, Lfim;->a:Lhan;

    iget-object v4, v4, Lhan;->a:Lhgz;

    if-eqz v4, :cond_3

    iget-object v4, v3, Lfim;->a:Lhan;

    iget-object v4, v4, Lhan;->a:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfim;->c:Ljava/lang/String;

    :cond_3
    iget-object v3, v3, Lfim;->c:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lbnt;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lbnt;->b:Lfim;

    iget-object v4, v3, Lfim;->d:Ljava/lang/String;

    if-nez v4, :cond_4

    iget-object v4, v3, Lfim;->a:Lhan;

    iget-object v4, v4, Lhan;->b:Lhgz;

    if-eqz v4, :cond_4

    iget-object v4, v3, Lfim;->a:Lhan;

    iget-object v4, v4, Lhan;->b:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfim;->d:Ljava/lang/String;

    :cond_4
    iget-object v3, v3, Lfim;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v0, p0, Lbnt;->i:Landroid/widget/TextView;

    iget-object v3, p0, Lbnt;->b:Lfim;

    iget-object v4, v3, Lfim;->e:Ljava/lang/String;

    if-nez v4, :cond_5

    iget-object v4, v3, Lfim;->a:Lhan;

    iget-object v4, v4, Lhan;->e:Lhgz;

    if-eqz v4, :cond_5

    iget-object v4, v3, Lfim;->a:Lhan;

    iget-object v4, v4, Lhan;->e:Lhgz;

    invoke-static {v4}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lfim;->e:Ljava/lang/String;

    :cond_5
    iget-object v3, v3, Lfim;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lbnt;->l:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lbnt;->k:Landroid/widget/RatingBar;

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lbnt;->k:Landroid/widget/RatingBar;

    iget-object v1, p0, Lbnt;->b:Lfim;

    iget-object v1, v1, Lfim;->a:Lhan;

    iget v1, v1, Lhan;->d:F

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 109
    iget-object v0, p0, Lbnt;->o:Lfvi;

    iget-object v1, p0, Lbnt;->b:Lfim;

    iget-object v3, v1, Lfim;->b:Lfnc;

    if-nez v3, :cond_6

    new-instance v3, Lfnc;

    iget-object v4, v1, Lfim;->a:Lhan;

    iget-object v4, v4, Lhan;->c:Lhxf;

    invoke-direct {v3, v4}, Lfnc;-><init>(Lhxf;)V

    iput-object v3, v1, Lfim;->b:Lfnc;

    :cond_6
    iget-object v1, v1, Lfim;->b:Lfnc;

    invoke-virtual {v0, v1, v5}, Lfvi;->a(Lfnc;Leyo;)V

    .line 111
    iget-object v0, p0, Lbnt;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lbnt;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move v0, v2

    .line 112
    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbnt;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbnt;->m:Z

    .line 128
    invoke-direct {p0}, Lbnt;->h()V

    .line 129
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbnt;->m:Z

    .line 137
    invoke-direct {p0}, Lbnt;->g()V

    .line 138
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lbnt;->h()V

    .line 160
    return-void
.end method
