.class public final Lbku;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lgix;

.field final b:Lezj;

.field final c:Ljava/util/concurrent/Executor;

.field private final d:Ljava/io/File;

.field private e:Lbkx;

.field private f:Lbkx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgix;Lezj;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lbku;->a:Lgix;

    .line 47
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lbku;->b:Lezj;

    .line 48
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbku;->c:Ljava/util/concurrent/Executor;

    .line 49
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "offline"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, Lbku;->d:Ljava/io/File;

    .line 50
    return-void
.end method

.method private a(Ljava/lang/String;)Lbkz;
    .locals 3

    .prologue
    .line 126
    new-instance v0, Lbkz;

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lbku;->d:Ljava/io/File;

    invoke-direct {v1, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lbkz;-><init>(Ljava/io/File;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lfqd;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lbku;->c()Lbkx;

    move-result-object v0

    invoke-virtual {v0}, Lbkx;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqd;

    return-object v0
.end method

.method public final a(Lfmm;)V
    .locals 1

    .prologue
    .line 64
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-virtual {p0}, Lbku;->b()Lbkx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbkx;->b(Ljava/lang/Object;)V

    .line 66
    return-void
.end method

.method public declared-synchronized b()Lbkx;
    .locals 2

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbku;->e:Lbkx;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lbkv;

    const-string v1, ".settings"

    .line 85
    invoke-direct {p0, v1}, Lbku;->a(Ljava/lang/String;)Lbkz;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbkv;-><init>(Lbku;Lbkz;)V

    iput-object v0, p0, Lbku;->e:Lbkx;

    .line 100
    :cond_0
    iget-object v0, p0, Lbku;->e:Lbkx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()Lbkx;
    .locals 2

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lbku;->f:Lbkx;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Lbkw;

    const-string v1, ".guide"

    .line 106
    invoke-direct {p0, v1}, Lbku;->a(Ljava/lang/String;)Lbkz;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbkw;-><init>(Lbku;Lbkz;)V

    iput-object v0, p0, Lbku;->f:Lbkx;

    .line 121
    :cond_0
    iget-object v0, p0, Lbku;->f:Lbkx;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
