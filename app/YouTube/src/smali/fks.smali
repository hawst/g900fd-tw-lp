.class public final Lfks;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lhnm;

.field private b:Landroid/text/Spanned;

.field private c:Ljava/util/List;

.field private d:Ljava/lang/CharSequence;

.field private e:Lfhz;


# direct methods
.method public constructor <init>(Lhnm;Lfhz;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhnm;

    iput-object v0, p0, Lfks;->a:Lhnm;

    .line 32
    iput-object p2, p0, Lfks;->e:Lfhz;

    .line 33
    return-void
.end method

.method private c()Ljava/util/List;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 43
    iget-object v0, p0, Lfks;->c:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfks;->a:Lhnm;

    iget-object v0, v0, Lhnm;->b:[Lhgz;

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lfks;->a:Lhnm;

    iget-object v2, v2, Lhnm;->b:[Lhgz;

    array-length v2, v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lfks;->c:Ljava/util/List;

    move v0, v1

    .line 45
    :goto_0
    iget-object v2, p0, Lfks;->a:Lhnm;

    iget-object v2, v2, Lhnm;->b:[Lhgz;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 46
    iget-object v2, p0, Lfks;->c:Ljava/util/List;

    iget-object v3, p0, Lfks;->a:Lhnm;

    iget-object v3, v3, Lhnm;->b:[Lhgz;

    aget-object v3, v3, v0

    iget-object v4, p0, Lfks;->e:Lfhz;

    invoke-static {v3, v4, v1}, Lfvo;->a(Lhgz;Lfhz;Z)Landroid/text/Spanned;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lfks;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lfks;->b:Landroid/text/Spanned;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfks;->a:Lhnm;

    iget-object v0, v0, Lhnm;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lfks;->a:Lhnm;

    iget-object v0, v0, Lhnm;->a:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfks;->b:Landroid/text/Spanned;

    .line 39
    :cond_0
    iget-object v0, p0, Lfks;->b:Landroid/text/Spanned;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lfks;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lfks;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 55
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 56
    invoke-direct {p0}, Lfks;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 57
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 58
    invoke-direct {p0}, Lfks;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 59
    add-int/lit8 v0, v3, -0x1

    if-eq v1, v0, :cond_0

    .line 60
    const-string v0, ", "

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 57
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63
    :cond_1
    iput-object v2, p0, Lfks;->d:Ljava/lang/CharSequence;

    .line 65
    :cond_2
    iget-object v0, p0, Lfks;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 70
    const/4 v0, 0x0

    .line 71
    instance-of v1, p1, Lfks;

    if-eqz v1, :cond_0

    .line 72
    check-cast p1, Lfks;

    .line 73
    iget-object v0, p1, Lfks;->a:Lhnm;

    iget-object v1, p0, Lfks;->a:Lhnm;

    invoke-virtual {v0, v1}, Lhnm;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 75
    :cond_0
    return v0
.end method
