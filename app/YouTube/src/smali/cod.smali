.class public final Lcod;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcnn;


# static fields
.field private static b:J


# instance fields
.field public final a:Lcnu;

.field private final c:Lezj;

.field private final d:Lcny;

.field private final e:Lcqg;

.field private final f:Lcpy;

.field private final g:Ldau;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 47
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xf

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcod;->b:J

    return-void
.end method

.method constructor <init>(Lcoe;)V
    .locals 9

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iget-object v0, p1, Lcoe;->d:Lezj;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lezj;

    iput-object v0, p0, Lcod;->c:Lezj;

    .line 59
    iget-object v0, p1, Lcoe;->i:Lcnu;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcnu;

    iput-object v0, p0, Lcod;->a:Lcnu;

    .line 60
    iget-object v0, p1, Lcoe;->f:Ldau;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldau;

    iput-object v0, p0, Lcod;->g:Ldau;

    .line 62
    new-instance v0, Lcny;

    .line 63
    iget-object v1, p1, Lcoe;->a:Ljava/util/concurrent/Executor;

    .line 64
    iget-object v2, p1, Lcoe;->b:Lorg/apache/http/client/HttpClient;

    .line 65
    iget-object v3, p1, Lcoe;->c:Lfbc;

    .line 66
    iget-object v4, p1, Lcoe;->d:Lezj;

    invoke-direct {v0, v1, v2, v3, v4}, Lcny;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lfbc;Lezj;)V

    iput-object v0, p0, Lcod;->d:Lcny;

    .line 68
    new-instance v5, Lese;

    .line 69
    iget-object v0, p1, Lcoe;->c:Lfbc;

    iget-object v1, p1, Lcoe;->k:Lewi;

    invoke-direct {v5, v0, v1}, Lese;-><init>(Lfbc;Lewi;)V

    .line 76
    new-instance v4, Lcsg;

    .line 80
    iget-object v0, p1, Lcoe;->c:Lfbc;

    iget-object v1, p1, Lcoe;->k:Lewi;

    invoke-direct {v4, v0, v1}, Lcsg;-><init>(Lfbc;Lewi;)V

    .line 81
    new-instance v0, Lcqg;

    iget-object v1, p0, Lcod;->c:Lezj;

    iget-object v2, p0, Lcod;->d:Lcny;

    iget-object v3, p1, Lcoe;->h:Lcqt;

    invoke-direct/range {v0 .. v5}, Lcqg;-><init>(Lezj;Lcny;Lcqt;Lcsg;Lgic;)V

    iput-object v0, p0, Lcod;->e:Lcqg;

    .line 87
    new-instance v0, Lcpy;

    iget-object v1, p0, Lcod;->c:Lezj;

    .line 97
    iget-object v2, p1, Lcoe;->e:Levn;

    iget-object v3, p0, Lcod;->a:Lcnu;

    iget-object v4, p0, Lcod;->g:Ldau;

    .line 100
    iget-object v5, p1, Lcoe;->j:Lcnm;

    iget-object v6, p0, Lcod;->d:Lcny;

    .line 102
    iget-object v7, p1, Lcoe;->c:Lfbc;

    .line 103
    iget-object v8, p1, Lcoe;->l:Lcnx;

    invoke-direct/range {v0 .. v8}, Lcpy;-><init>(Lezj;Levn;Lcnu;Ldau;Lcnm;Lcny;Lfbc;Lcnx;)V

    iput-object v0, p0, Lcod;->f:Lcpy;

    .line 104
    return-void
.end method


# virtual methods
.method public final a(Lfrl;Ljava/lang/String;)Lesq;
    .locals 4

    .prologue
    .line 166
    invoke-static {}, Lb;->b()V

    .line 167
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    invoke-virtual {p1}, Lfrl;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    .line 171
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcod;->e:Lcqg;

    sget-wide v2, Lcod;->b:J

    invoke-virtual {v0, p1, v2, v3}, Lcqg;->a(Lfrl;J)Lesq;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lfrl;Ljava/lang/String;Lfai;)Lesq;
    .locals 12

    .prologue
    const-wide/16 v0, 0x0

    .line 111
    invoke-static {}, Lb;->b()V

    .line 112
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    invoke-static {p2}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 115
    iget-object v2, p0, Lcod;->a:Lcnu;

    invoke-virtual {v2}, Lcnu;->b()J

    move-result-wide v2

    .line 117
    iget-object v4, p1, Lfrl;->a:Lhro;

    invoke-static {v4}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v8

    .line 118
    iget-object v4, p1, Lfrl;->a:Lhro;

    iget-object v9, v4, Lhro;->n:Ljava/lang/String;

    .line 119
    iget-object v4, p1, Lfrl;->a:Lhro;

    iget-object v10, v4, Lhro;->o:Ljava/lang/String;

    .line 120
    iget-object v11, p1, Lfrl;->c:[B

    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    .line 122
    :goto_0
    iget-object v2, p0, Lcod;->a:Lcnu;

    .line 124
    iget-object v2, v2, Lcnu;->g:Lcwq;

    invoke-virtual {v2}, Lcwq;->g()Lcwx;

    move-result-object v3

    iget-object v2, p0, Lcod;->a:Lcnu;

    .line 125
    iget-object v2, v2, Lcnu;->d:Lexd;

    invoke-interface {v2}, Lexd;->i()I

    move-result v4

    iget-object v2, p0, Lcod;->a:Lcnu;

    .line 126
    iget-object v2, v2, Lcnu;->g:Lcwq;

    invoke-virtual {v2}, Lcwq;->d()I

    move-result v5

    iget-object v2, p0, Lcod;->a:Lcnu;

    .line 127
    iget-object v2, v2, Lcnu;->g:Lcwq;

    invoke-virtual {v2}, Lcwq;->f()Z

    move-result v6

    move-object v2, p2

    .line 121
    invoke-static/range {v0 .. v6}, Lcqt;->a(JLjava/lang/String;Lcwx;IIZ)Ljava/util/Map;

    move-result-object v5

    sget-wide v6, Lcod;->b:J

    move-object v0, p0

    move-object v1, v8

    move-object v2, v9

    move-object v3, v10

    move-object v4, v11

    move-object v8, p3

    .line 116
    invoke-virtual/range {v0 .. v8}, Lcod;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/util/Map;JLfai;)Lesq;

    move-result-object v0

    return-object v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcod;->c:Lezj;

    .line 122
    invoke-virtual {v0}, Lezj;->a()J

    move-result-wide v0

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/util/Map;JLfai;)Lesq;
    .locals 10

    .prologue
    .line 150
    invoke-static {}, Lb;->b()V

    .line 151
    invoke-static {p1}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    .line 152
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v0, p0, Lcod;->e:Lcqg;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcqg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/util/Map;JLfai;)Lesq;

    move-result-object v0

    return-object v0
.end method

.method public final a(Less;Ljava/lang/String;JLfai;Ljava/util/Map;)Lfoy;
    .locals 9

    .prologue
    .line 202
    invoke-static {}, Lb;->b()V

    .line 203
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    iget-object v1, p0, Lcod;->f:Lcpy;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-virtual/range {v1 .. v7}, Lcpy;->a(Less;Ljava/lang/String;JLfai;Ljava/util/Map;)Lfoy;

    move-result-object v0

    return-object v0
.end method

.method public final a(Less;Ljava/lang/String;Lfai;Ljava/util/Map;)Lfoy;
    .locals 8

    .prologue
    .line 188
    sget-wide v4, Lcod;->b:J

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v6, p3

    move-object v7, p4

    invoke-virtual/range {v1 .. v7}, Lcod;->a(Less;Ljava/lang/String;JLfai;Ljava/util/Map;)Lfoy;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lfoy;)V
    .locals 4

    .prologue
    .line 214
    invoke-virtual {p1}, Lfoy;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcod;->a:Lcnu;

    iget-object v1, p0, Lcod;->c:Lezj;

    invoke-virtual {v1}, Lezj;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcnu;->a(J)V

    .line 217
    :cond_0
    return-void
.end method

.method public final a(Lfrl;)Z
    .locals 1

    .prologue
    .line 177
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    invoke-virtual {p1}, Lfrl;->m()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
