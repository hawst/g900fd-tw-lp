.class public final enum Lgbt;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgbt;

.field private static enum b:Lgbt;

.field private static enum c:Lgbt;

.field private static enum d:Lgbt;

.field private static enum e:Lgbt;

.field private static enum f:Lgbt;

.field private static final synthetic g:[Lgbt;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 29
    new-instance v0, Lgbt;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v3}, Lgbt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbt;->b:Lgbt;

    .line 30
    new-instance v0, Lgbt;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v4}, Lgbt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbt;->a:Lgbt;

    .line 31
    new-instance v0, Lgbt;

    const-string v1, "DELAYED"

    invoke-direct {v0, v1, v5}, Lgbt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbt;->c:Lgbt;

    .line 32
    new-instance v0, Lgbt;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v6}, Lgbt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbt;->d:Lgbt;

    .line 33
    new-instance v0, Lgbt;

    const-string v1, "CANCELLED"

    invoke-direct {v0, v1, v7}, Lgbt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbt;->e:Lgbt;

    .line 34
    new-instance v0, Lgbt;

    const-string v1, "REJECTED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lgbt;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lgbt;->f:Lgbt;

    .line 28
    const/4 v0, 0x6

    new-array v0, v0, [Lgbt;

    sget-object v1, Lgbt;->b:Lgbt;

    aput-object v1, v0, v3

    sget-object v1, Lgbt;->a:Lgbt;

    aput-object v1, v0, v4

    sget-object v1, Lgbt;->c:Lgbt;

    aput-object v1, v0, v5

    sget-object v1, Lgbt;->d:Lgbt;

    aput-object v1, v0, v6

    sget-object v1, Lgbt;->e:Lgbt;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lgbt;->f:Lgbt;

    aput-object v2, v0, v1

    sput-object v0, Lgbt;->g:[Lgbt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgbt;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lgbt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgbt;

    return-object v0
.end method

.method public static values()[Lgbt;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lgbt;->g:[Lgbt;

    invoke-virtual {v0}, [Lgbt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgbt;

    return-object v0
.end method
