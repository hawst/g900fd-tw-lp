.class public final Lhfa;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhes;

.field private b:J

.field private c:Lhgz;

.field private d:Lhxf;

.field private e:Lhxf;

.field private f:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43139
    invoke-direct {p0}, Lidf;-><init>()V

    .line 43142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhfa;->b:J

    .line 43145
    iput-object v2, p0, Lhfa;->c:Lhgz;

    .line 43148
    iput-object v2, p0, Lhfa;->d:Lhxf;

    .line 43151
    iput-object v2, p0, Lhfa;->e:Lhxf;

    .line 43154
    iput-object v2, p0, Lhfa;->a:Lhes;

    .line 43157
    iput-object v2, p0, Lhfa;->f:Lhgz;

    .line 43139
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    .line 43224
    const/4 v0, 0x0

    .line 43225
    iget-wide v2, p0, Lhfa;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 43226
    const/4 v0, 0x1

    iget-wide v2, p0, Lhfa;->b:J

    .line 43227
    invoke-static {v0, v2, v3}, Lidd;->d(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 43229
    :cond_0
    iget-object v1, p0, Lhfa;->c:Lhgz;

    if-eqz v1, :cond_1

    .line 43230
    const/4 v1, 0x2

    iget-object v2, p0, Lhfa;->c:Lhgz;

    .line 43231
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43233
    :cond_1
    iget-object v1, p0, Lhfa;->d:Lhxf;

    if-eqz v1, :cond_2

    .line 43234
    const/4 v1, 0x3

    iget-object v2, p0, Lhfa;->d:Lhxf;

    .line 43235
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43237
    :cond_2
    iget-object v1, p0, Lhfa;->e:Lhxf;

    if-eqz v1, :cond_3

    .line 43238
    const/4 v1, 0x4

    iget-object v2, p0, Lhfa;->e:Lhxf;

    .line 43239
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43241
    :cond_3
    iget-object v1, p0, Lhfa;->a:Lhes;

    if-eqz v1, :cond_4

    .line 43242
    const/4 v1, 0x5

    iget-object v2, p0, Lhfa;->a:Lhes;

    .line 43243
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43245
    :cond_4
    iget-object v1, p0, Lhfa;->f:Lhgz;

    if-eqz v1, :cond_5

    .line 43246
    const/4 v1, 0x6

    iget-object v2, p0, Lhfa;->f:Lhgz;

    .line 43247
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43249
    :cond_5
    iget-object v1, p0, Lhfa;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 43250
    iput v0, p0, Lhfa;->J:I

    .line 43251
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 43135
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfa;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfa;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfa;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lhfa;->b:J

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhfa;->c:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhfa;->c:Lhgz;

    :cond_2
    iget-object v0, p0, Lhfa;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhfa;->d:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhfa;->d:Lhxf;

    :cond_3
    iget-object v0, p0, Lhfa;->d:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhfa;->e:Lhxf;

    if-nez v0, :cond_4

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhfa;->e:Lhxf;

    :cond_4
    iget-object v0, p0, Lhfa;->e:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhfa;->a:Lhes;

    if-nez v0, :cond_5

    new-instance v0, Lhes;

    invoke-direct {v0}, Lhes;-><init>()V

    iput-object v0, p0, Lhfa;->a:Lhes;

    :cond_5
    iget-object v0, p0, Lhfa;->a:Lhes;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhfa;->f:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhfa;->f:Lhgz;

    :cond_6
    iget-object v0, p0, Lhfa;->f:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 4

    .prologue
    .line 43200
    iget-wide v0, p0, Lhfa;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 43201
    const/4 v0, 0x1

    iget-wide v2, p0, Lhfa;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lidd;->b(IJ)V

    .line 43203
    :cond_0
    iget-object v0, p0, Lhfa;->c:Lhgz;

    if-eqz v0, :cond_1

    .line 43204
    const/4 v0, 0x2

    iget-object v1, p0, Lhfa;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43206
    :cond_1
    iget-object v0, p0, Lhfa;->d:Lhxf;

    if-eqz v0, :cond_2

    .line 43207
    const/4 v0, 0x3

    iget-object v1, p0, Lhfa;->d:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43209
    :cond_2
    iget-object v0, p0, Lhfa;->e:Lhxf;

    if-eqz v0, :cond_3

    .line 43210
    const/4 v0, 0x4

    iget-object v1, p0, Lhfa;->e:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43212
    :cond_3
    iget-object v0, p0, Lhfa;->a:Lhes;

    if-eqz v0, :cond_4

    .line 43213
    const/4 v0, 0x5

    iget-object v1, p0, Lhfa;->a:Lhes;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43215
    :cond_4
    iget-object v0, p0, Lhfa;->f:Lhgz;

    if-eqz v0, :cond_5

    .line 43216
    const/4 v0, 0x6

    iget-object v1, p0, Lhfa;->f:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 43218
    :cond_5
    iget-object v0, p0, Lhfa;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 43220
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43173
    if-ne p1, p0, :cond_1

    .line 43182
    :cond_0
    :goto_0
    return v0

    .line 43174
    :cond_1
    instance-of v2, p1, Lhfa;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 43175
    :cond_2
    check-cast p1, Lhfa;

    .line 43176
    iget-wide v2, p0, Lhfa;->b:J

    iget-wide v4, p1, Lhfa;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lhfa;->c:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfa;->c:Lhgz;

    if-nez v2, :cond_3

    .line 43177
    :goto_1
    iget-object v2, p0, Lhfa;->d:Lhxf;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhfa;->d:Lhxf;

    if-nez v2, :cond_3

    .line 43178
    :goto_2
    iget-object v2, p0, Lhfa;->e:Lhxf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhfa;->e:Lhxf;

    if-nez v2, :cond_3

    .line 43179
    :goto_3
    iget-object v2, p0, Lhfa;->a:Lhes;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhfa;->a:Lhes;

    if-nez v2, :cond_3

    .line 43180
    :goto_4
    iget-object v2, p0, Lhfa;->f:Lhgz;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhfa;->f:Lhgz;

    if-nez v2, :cond_3

    .line 43181
    :goto_5
    iget-object v2, p0, Lhfa;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhfa;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 43182
    goto :goto_0

    .line 43176
    :cond_4
    iget-object v2, p0, Lhfa;->c:Lhgz;

    iget-object v3, p1, Lhfa;->c:Lhgz;

    .line 43177
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhfa;->d:Lhxf;

    iget-object v3, p1, Lhfa;->d:Lhxf;

    .line 43178
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhfa;->e:Lhxf;

    iget-object v3, p1, Lhfa;->e:Lhxf;

    .line 43179
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhfa;->a:Lhes;

    iget-object v3, p1, Lhfa;->a:Lhes;

    .line 43180
    invoke-virtual {v2, v3}, Lhes;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhfa;->f:Lhgz;

    iget-object v3, p1, Lhfa;->f:Lhgz;

    .line 43181
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhfa;->I:Ljava/util/List;

    iget-object v3, p1, Lhfa;->I:Ljava/util/List;

    .line 43182
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 43186
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 43188
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lhfa;->b:J

    iget-wide v4, p0, Lhfa;->b:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 43189
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfa;->c:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 43190
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfa;->d:Lhxf;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 43191
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfa;->e:Lhxf;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 43192
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfa;->a:Lhes;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 43193
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfa;->f:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 43194
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhfa;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 43195
    return v0

    .line 43189
    :cond_0
    iget-object v0, p0, Lhfa;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 43190
    :cond_1
    iget-object v0, p0, Lhfa;->d:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_1

    .line 43191
    :cond_2
    iget-object v0, p0, Lhfa;->e:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_2

    .line 43192
    :cond_3
    iget-object v0, p0, Lhfa;->a:Lhes;

    invoke-virtual {v0}, Lhes;->hashCode()I

    move-result v0

    goto :goto_3

    .line 43193
    :cond_4
    iget-object v0, p0, Lhfa;->f:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_4

    .line 43194
    :cond_5
    iget-object v1, p0, Lhfa;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
