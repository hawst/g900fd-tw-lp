.class final Lduv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldxw;


# instance fields
.field private synthetic a:Ldtm;

.field private synthetic b:Ldux;

.field private synthetic c:Lduu;


# direct methods
.method constructor <init>(Lduu;Ldtm;Ldux;)V
    .locals 0

    .prologue
    .line 824
    iput-object p1, p0, Lduv;->c:Lduu;

    iput-object p2, p0, Lduv;->a:Ldtm;

    iput-object p3, p0, Lduv;->b:Ldux;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ldtm;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 827
    iget-object v0, p0, Lduv;->c:Lduu;

    iget-object v0, v0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v0

    const-string v1, "Trying to wake-up %s with id %s, found: %s with id %s "

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lduv;->a:Ldtm;

    .line 829
    iget-object v4, v4, Ldtm;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    iget-object v3, p0, Lduv;->a:Ldtm;

    .line 830
    iget-object v3, v3, Ldtm;->e:Ldtj;

    aput-object v3, v2, v5

    const/4 v3, 0x2

    .line 831
    iget-object v4, p1, Ldtm;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    .line 832
    iget-object v4, p1, Ldtm;->e:Ldtj;

    aput-object v4, v2, v3

    .line 827
    invoke-virtual {v0, v1, v2}, Ldyg;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 833
    iget-object v0, p1, Ldtm;->e:Ldtj;

    iget-object v1, p0, Lduv;->a:Ldtm;

    iget-object v1, v1, Ldtm;->e:Ldtj;

    invoke-virtual {v0, v1}, Ldtj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lduv;->b:Ldux;

    iget-object v0, v0, Ldux;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 835
    iget-object v0, p0, Lduv;->c:Lduu;

    iget-object v0, v0, Lduu;->a:Ldun;

    invoke-static {v0}, Ldun;->a(Ldun;)Ldyg;

    move-result-object v1

    const-string v2, "Successful wake-up of "

    iget-object v0, p1, Ldtm;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ldyg;->a(Ljava/lang/String;)V

    .line 836
    iget-object v0, p0, Lduv;->c:Lduu;

    iget-object v0, v0, Lduu;->a:Ldun;

    new-instance v1, Ldwf;

    invoke-direct {v1, p1}, Ldwf;-><init>(Ldtm;)V

    invoke-static {v0, v1}, Ldun;->a(Ldun;Ldwr;)Ldwr;

    .line 837
    iget-object v0, p0, Lduv;->c:Lduu;

    iget-object v0, v0, Lduu;->a:Ldun;

    iget-object v1, p0, Lduv;->b:Ldux;

    iget-object v1, v1, Ldux;->a:Ldus;

    invoke-static {v0, p1, v1}, Ldun;->a(Ldun;Ldtm;Ldus;)V

    .line 840
    :cond_0
    return-void

    .line 835
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
