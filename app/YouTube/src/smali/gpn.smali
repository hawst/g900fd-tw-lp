.class public final Lgpn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgjg;


# instance fields
.field private final a:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lgpn;->a:Landroid/util/SparseArray;

    .line 85
    return-void
.end method


# virtual methods
.method public a(I)Lgpd;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lgpn;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpd;

    .line 116
    if-nez v0, :cond_0

    .line 117
    new-instance v0, Lgpd;

    invoke-direct {v0, p1}, Lgpd;-><init>(I)V

    .line 118
    iget-object v1, p0, Lgpn;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 120
    :cond_0
    return-object v0
.end method

.method public final a(ILjava/lang/String;II)Lgpn;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lgpn;->a(I)Lgpd;

    move-result-object v0

    iget-object v0, v0, Lgpd;->a:Lgpl;

    invoke-virtual {v0, p2, p3, p4}, Lgpl;->a(Ljava/lang/String;II)Lgpl;

    .line 90
    return-object p0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 79
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lgpn;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lgpn;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgpd;

    invoke-virtual {v0}, Lgpd;->a()Lgpc;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Lgpm;

    invoke-direct {v0, v2}, Lgpm;-><init>(Ljava/util/List;)V

    return-object v0
.end method
