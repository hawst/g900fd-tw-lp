.class final Ldkn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field volatile a:Z

.field volatile b:Z

.field final synthetic c:Ldkj;

.field private final d:Lgoh;

.field private volatile e:Ljava/lang/Runnable;

.field private final f:Z


# direct methods
.method public constructor <init>(Ldkj;Lgoh;Z)V
    .locals 1

    .prologue
    .line 520
    iput-object p1, p0, Ldkn;->c:Ldkj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 514
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldkn;->b:Z

    .line 521
    iput-object p2, p0, Ldkn;->d:Lgoh;

    .line 522
    iput-boolean p3, p0, Ldkn;->f:Z

    .line 523
    return-void
.end method

.method private a()Lgky;
    .locals 7

    .prologue
    .line 642
    invoke-static {}, Lgky;->a()Lgky;

    move-result-object v6

    .line 643
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v0, v0, Ldkj;->l:Ldaw;

    iget-object v5, p0, Ldkn;->d:Lgoh;

    iget-object v1, v5, Lgoh;->a:Leaa;

    iget-object v1, v1, Leaa;->a:Ljava/lang/String;

    iget-object v2, v5, Lgoh;->a:Leaa;

    iget-object v2, v2, Leaa;->c:Ljava/lang/String;

    iget-object v3, v5, Lgoh;->a:Leaa;

    iget v3, v3, Leaa;->d:I

    iget-object v4, v5, Lgoh;->a:Leaa;

    iget-object v4, v4, Leaa;->e:Ljava/lang/String;

    invoke-virtual {v5}, Lgoh;->e()[B

    move-result-object v5

    invoke-virtual/range {v0 .. v6}, Ldaw;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;[BLwv;)V

    .line 644
    return-object v6
.end method

.method private a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 666
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v0, v0, Ldkj;->m:Landroid/os/Handler;

    new-instance v1, Ldkp;

    invoke-direct {v1, p0, p1}, Ldkp;-><init>(Ldkn;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 681
    return-void
.end method

.method private b(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 718
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v0, v0, Ldkj;->m:Landroid/os/Handler;

    new-instance v1, Ldks;

    invoke-direct {v1, p0, p1}, Ldks;-><init>(Ldkn;Ljava/lang/Exception;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 733
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)Z
    .locals 1

    .prologue
    .line 544
    monitor-enter p0

    .line 545
    :try_start_0
    iget-boolean v0, p0, Ldkn;->b:Z

    if-eqz v0, :cond_0

    .line 546
    const/4 v0, 0x0

    monitor-exit p0

    .line 549
    :goto_0
    return v0

    .line 548
    :cond_0
    iput-object p1, p0, Ldkn;->e:Ljava/lang/Runnable;

    .line 549
    const/4 v0, 0x1

    monitor-exit p0

    goto :goto_0

    .line 551
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 556
    .line 560
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v0, v0, Ldkj;->c:Levn;

    new-instance v1, Lczr;

    invoke-direct {v1}, Lczr;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 562
    iget-object v0, p0, Ldkn;->d:Lgoh;

    iget-object v0, v0, Lgoh;->a:Leaa;

    iget-object v1, v0, Leaa;->a:Ljava/lang/String;

    .line 563
    iget-boolean v0, p0, Ldkn;->f:Z

    if-eqz v0, :cond_5

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 564
    invoke-direct {p0}, Ldkn;->a()Lgky;

    move-result-object v2

    .line 569
    :try_start_0
    invoke-virtual {v2}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnx;

    .line 570
    iget-object v1, v0, Lfnx;->i:Ljava/lang/String;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v7, v2

    .line 580
    :goto_0
    iget-boolean v0, p0, Ldkn;->f:Z

    if-eqz v0, :cond_4

    .line 581
    invoke-static {v1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    :try_start_1
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v0, v0, Ldkj;->a:Ldaq;

    iget-object v2, p0, Ldkn;->d:Lgoh;

    .line 586
    invoke-virtual {v2}, Lgoh;->e()[B

    move-result-object v2

    iget-object v3, p0, Ldkn;->d:Lgoh;

    .line 587
    iget-object v3, v3, Lgoh;->a:Leaa;

    iget-object v3, v3, Leaa;->l:Ljava/lang/String;

    iget-object v4, p0, Ldkn;->d:Lgoh;

    .line 588
    iget-object v4, v4, Lgoh;->a:Leaa;

    iget-object v4, v4, Leaa;->c:Ljava/lang/String;

    iget-object v5, p0, Ldkn;->d:Lgoh;

    .line 589
    iget-object v5, v5, Lgoh;->a:Leaa;

    iget v5, v5, Leaa;->d:I

    const/4 v6, -0x1

    .line 584
    invoke-virtual/range {v0 .. v6}, Ldaq;->a(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;II)Lgky;

    move-result-object v0

    .line 594
    if-nez v7, :cond_0

    iget-object v1, p0, Ldkn;->c:Ldkj;

    iget-object v1, v1, Ldkj;->n:Lcyc;

    invoke-interface {v1}, Lcyc;->Q()Z

    move-result v1

    if-nez v1, :cond_0

    .line 595
    invoke-direct {p0}, Ldkn;->a()Lgky;

    move-result-object v7

    .line 598
    :cond_0
    sget-wide v2, Ldaq;->b:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lgky;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrl;
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_4

    .line 601
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Ldkn;->b:Z

    .line 602
    iget-object v1, p0, Ldkn;->c:Ldkj;

    iget-object v1, v1, Ldkj;->m:Landroid/os/Handler;

    new-instance v2, Ldko;

    invoke-direct {v2, p0, v0}, Ldko;-><init>(Ldkn;Lfrl;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_7

    move-object v8, v0

    move-object v0, v7

    .line 614
    :goto_1
    if-nez v0, :cond_1

    .line 615
    invoke-direct {p0}, Ldkn;->a()Lgky;

    move-result-object v0

    .line 619
    :cond_1
    :try_start_3
    invoke-virtual {v0}, Lgky;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnx;

    .line 620
    iget-object v1, p0, Ldkn;->c:Ldkj;

    iget-object v1, v1, Ldkj;->m:Landroid/os/Handler;

    new-instance v2, Ldkr;

    invoke-direct {v2, p0, v0}, Ldkr;-><init>(Ldkn;Lfnx;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 621
    if-eqz v8, :cond_2

    .line 622
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v0, v0, Ldkj;->m:Landroid/os/Handler;

    new-instance v1, Ldkq;

    invoke-direct {v1, p0}, Ldkq;-><init>(Ldkn;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_6

    .line 633
    :cond_2
    monitor-enter p0

    .line 634
    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Ldkn;->b:Z

    .line 635
    iget-object v0, p0, Ldkn;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_3

    .line 636
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v0, v0, Ldkj;->m:Landroid/os/Handler;

    iget-object v1, p0, Ldkn;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 638
    :cond_3
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_2
    return-void

    .line 571
    :catch_0
    move-exception v0

    .line 572
    invoke-direct {p0, v0}, Ldkn;->a(Ljava/lang/Exception;)V

    goto :goto_2

    .line 574
    :catch_1
    move-exception v0

    .line 575
    invoke-direct {p0, v0}, Ldkn;->a(Ljava/lang/Exception;)V

    goto :goto_2

    .line 603
    :catch_2
    move-exception v0

    .line 604
    :goto_3
    invoke-direct {p0, v0}, Ldkn;->a(Ljava/lang/Exception;)V

    move-object v0, v7

    .line 609
    goto :goto_1

    .line 605
    :catch_3
    move-exception v0

    .line 606
    :goto_4
    invoke-direct {p0, v0}, Ldkn;->a(Ljava/lang/Exception;)V

    move-object v0, v7

    .line 609
    goto :goto_1

    .line 607
    :catch_4
    move-exception v0

    .line 608
    :goto_5
    invoke-direct {p0, v0}, Ldkn;->a(Ljava/lang/Exception;)V

    move-object v0, v7

    .line 609
    goto :goto_1

    .line 611
    :cond_4
    iget-object v0, p0, Ldkn;->c:Ldkj;

    iget-object v8, v0, Ldkj;->g:Lfrl;

    move-object v0, v7

    goto :goto_1

    .line 624
    :catch_5
    move-exception v0

    .line 625
    invoke-direct {p0, v0}, Ldkn;->b(Ljava/lang/Exception;)V

    goto :goto_2

    .line 627
    :catch_6
    move-exception v0

    .line 628
    invoke-direct {p0, v0}, Ldkn;->b(Ljava/lang/Exception;)V

    goto :goto_2

    .line 638
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 607
    :catch_7
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_5

    .line 605
    :catch_8
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_4

    .line 603
    :catch_9
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_3

    :cond_5
    move-object v7, v8

    goto/16 :goto_0
.end method
