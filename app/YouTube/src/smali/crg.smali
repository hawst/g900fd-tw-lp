.class public final Lcrg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lfbd;

.field private static final b:Lfbd;

.field private static final c:Lfbd;

.field private static final d:Lfbd;

.field private static final e:Lfbd;

.field private static final f:Lfbd;

.field private static final g:Lfbd;

.field private static final h:Lfbd;

.field private static final i:Lfbd;

.field private static final j:Lfbd;

.field private static final k:Lfbd;

.field private static final l:Lfbd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcrh;

    invoke-direct {v0}, Lcrh;-><init>()V

    sput-object v0, Lcrg;->a:Lfbd;

    .line 55
    new-instance v0, Lcrs;

    invoke-direct {v0}, Lcrs;-><init>()V

    sput-object v0, Lcrg;->b:Lfbd;

    .line 67
    new-instance v0, Lcrx;

    invoke-direct {v0}, Lcrx;-><init>()V

    sput-object v0, Lcrg;->c:Lfbd;

    .line 136
    new-instance v0, Lcry;

    invoke-direct {v0}, Lcry;-><init>()V

    sput-object v0, Lcrg;->d:Lfbd;

    .line 148
    new-instance v0, Lcrz;

    invoke-direct {v0}, Lcrz;-><init>()V

    sput-object v0, Lcrg;->e:Lfbd;

    .line 159
    new-instance v0, Lcsa;

    invoke-direct {v0}, Lcsa;-><init>()V

    sput-object v0, Lcrg;->f:Lfbd;

    .line 170
    new-instance v0, Lcsb;

    invoke-direct {v0}, Lcsb;-><init>()V

    sput-object v0, Lcrg;->g:Lfbd;

    .line 185
    new-instance v0, Lcsc;

    invoke-direct {v0}, Lcsc;-><init>()V

    sput-object v0, Lcrg;->h:Lfbd;

    .line 197
    new-instance v0, Lcsd;

    invoke-direct {v0}, Lcsd;-><init>()V

    sput-object v0, Lcrg;->i:Lfbd;

    .line 205
    new-instance v0, Lcri;

    invoke-direct {v0}, Lcri;-><init>()V

    sput-object v0, Lcrg;->j:Lfbd;

    .line 216
    new-instance v0, Lcrj;

    invoke-direct {v0}, Lcrj;-><init>()V

    sput-object v0, Lcrg;->k:Lfbd;

    .line 248
    new-instance v0, Lcrk;

    invoke-direct {v0}, Lcrk;-><init>()V

    sput-object v0, Lcrg;->l:Lfbd;

    return-void
.end method

.method static synthetic a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 42
    invoke-static {p0, p1}, Lcrg;->b(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Lezj;Lfbb;)V
    .locals 4

    .prologue
    .line 436
    new-instance v0, Lcrw;

    invoke-direct {v0, p1}, Lcrw;-><init>(Lezj;)V

    .line 445
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/AdSystem"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcrg;->a:Lfbd;

    invoke-virtual {p2, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Impression"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->b:Lfbd;

    .line 446
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->d:Lfbd;

    .line 447
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Creatives/Creative/Linear/TrackingEvents/Tracking"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->c:Lfbd;

    .line 448
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Creatives/Creative/Linear/VideoClicks/ClickThrough"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->e:Lfbd;

    .line 449
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Creatives/Creative/Linear/VideoClicks/ClickTracking"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->f:Lfbd;

    .line 451
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Creatives/Creative/Linear/VideoClicks/CustomClick"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->g:Lfbd;

    .line 453
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Extensions/Extension/AdFormatExclusionPolicy/TrackingUri"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->h:Lfbd;

    .line 455
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Extensions/Extension/AdFormatExclusionPolicy/AdAssetFrequencyCap"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcrg;->i:Lfbd;

    .line 457
    invoke-virtual {v1, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/Extensions/Extension/AdFormatExclusionPolicy/AdAssetTimeToLive"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 459
    invoke-virtual {v1, v2, v0}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/Extensions/Extension/ConversionUrl"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcrg;->j:Lfbd;

    .line 461
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/Extensions/Extension/SkippableAdType"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcrg;->l:Lfbd;

    .line 462
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/Extensions/Extension/CustomTracking/Tracking"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcrg;->k:Lfbd;

    .line 463
    invoke-virtual {v0, v1, v2}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 465
    return-void
.end method

.method public static a(Ljava/lang/String;Lezj;Lfbb;Lcsf;Lcqy;)V
    .locals 3

    .prologue
    .line 272
    invoke-static {p0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    invoke-static {p0, p1, p2, p3, p4}, Lcrg;->b(Ljava/lang/String;Lezj;Lfbb;Lcsf;Lcqy;)V

    .line 277
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 278
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/VAST/Ad/Wrapper/Extensions/Extension/AdXml"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 279
    new-instance v1, Lcrl;

    invoke-direct {v1}, Lcrl;-><init>()V

    invoke-static {p0, p1, p2, v1, p4}, Lcrg;->b(Ljava/lang/String;Lezj;Lfbb;Lcsf;Lcqy;)V

    .line 277
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    :cond_0
    return-void
.end method

.method private static b(Ljava/lang/String;I)I
    .locals 4

    .prologue
    .line 468
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    :goto_0
    return p1

    .line 472
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    goto :goto_0

    .line 474
    :catch_0
    move-exception v0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Bad integer parse of:\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' using:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Lezj;Lfbb;Lcsf;Lcqy;)V
    .locals 5

    .prologue
    .line 299
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/VAST/Ad/InLine"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 300
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "/VAST/Ad/Wrapper"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 301
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/VAST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcrv;

    invoke-direct {v3, p3}, Lcrv;-><init>(Lcsf;)V

    invoke-virtual {p2, v2, v3}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/VAST/Ad"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcru;

    invoke-direct {v4, p4}, Lcru;-><init>(Lcqy;)V

    .line 317
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/Creatives/Creative/Linear/Duration"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcrt;

    invoke-direct {v4}, Lcrt;-><init>()V

    .line 347
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/Creatives/Creative/Linear/AdParameters"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcrr;

    invoke-direct {v4}, Lcrr;-><init>()V

    .line 354
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/Creatives/Creative/Linear/MediaFiles/MediaFile"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcrq;

    invoke-direct {v4, p1}, Lcrq;-><init>(Lezj;)V

    .line 361
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/Creatives/Creative/NonLinearAds/NonLinear/AdParameters"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcrp;

    invoke-direct {v4}, Lcrp;-><init>()V

    .line 377
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/Extensions/Extension/ShowYouTubeAnnotations"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcro;

    invoke-direct {v4}, Lcro;-><init>()V

    .line 392
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/Extensions/Extension"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcrn;

    invoke-direct {v4}, Lcrn;-><init>()V

    .line 400
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/VASTAdTagURI"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcrm;

    invoke-direct {v4}, Lcrm;-><init>()V

    .line 416
    invoke-virtual {v2, v3, v4}, Lfbb;->a(Ljava/lang/String;Lfbg;)Lfbb;

    .line 428
    invoke-static {v0, p1, p2}, Lcrg;->a(Ljava/lang/String;Lezj;Lfbb;)V

    .line 429
    invoke-static {v1, p1, p2}, Lcrg;->a(Ljava/lang/String;Lezj;Lfbb;)V

    .line 430
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/Extensions/Extension"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lfhl;->a(Ljava/lang/String;Lfbb;)V

    .line 431
    return-void
.end method
