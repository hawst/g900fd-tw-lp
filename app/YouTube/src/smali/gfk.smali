.class final Lgfk;
.super Landroid/view/TextureView;
.source "SourceFile"


# instance fields
.field private synthetic a:Lgfg;


# direct methods
.method public constructor <init>(Lgfg;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lgfk;->a:Lgfg;

    .line 237
    invoke-direct {p0, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 238
    return-void
.end method


# virtual methods
.method protected final onMeasure(II)V
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lgfk;->a:Lgfg;

    invoke-static {v0}, Lgfg;->c(Lgfg;)I

    move-result v0

    invoke-static {v0, p1}, Lgfk;->getDefaultSize(II)I

    move-result v1

    .line 243
    iget-object v0, p0, Lgfk;->a:Lgfg;

    invoke-static {v0}, Lgfg;->d(Lgfg;)I

    move-result v0

    invoke-static {v0, p2}, Lgfk;->getDefaultSize(II)I

    move-result v0

    .line 244
    iget-object v2, p0, Lgfk;->a:Lgfg;

    invoke-static {v2}, Lgfg;->c(Lgfg;)I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lgfk;->a:Lgfg;

    invoke-static {v2}, Lgfg;->d(Lgfg;)I

    move-result v2

    if-lez v2, :cond_0

    .line 246
    iget-object v2, p0, Lgfk;->a:Lgfg;

    invoke-static {v2}, Lgfg;->c(Lgfg;)I

    move-result v2

    mul-int/2addr v2, v0

    int-to-float v2, v2

    iget-object v3, p0, Lgfk;->a:Lgfg;

    invoke-static {v3}, Lgfg;->d(Lgfg;)I

    move-result v3

    mul-int/2addr v3, v1

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    .line 247
    const v3, 0x3c23d70a    # 0.01f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_1

    .line 249
    iget-object v0, p0, Lgfk;->a:Lgfg;

    invoke-static {v0}, Lgfg;->d(Lgfg;)I

    move-result v0

    mul-int/2addr v0, v1

    iget-object v2, p0, Lgfk;->a:Lgfg;

    invoke-static {v2}, Lgfg;->c(Lgfg;)I

    move-result v2

    div-int/2addr v0, v2

    .line 256
    :cond_0
    :goto_0
    invoke-static {v1, p1}, Lgfk;->resolveSize(II)I

    move-result v1

    .line 257
    invoke-static {v0, p2}, Lgfk;->resolveSize(II)I

    move-result v0

    .line 259
    invoke-virtual {p0, v1, v0}, Lgfk;->setMeasuredDimension(II)V

    .line 260
    return-void

    .line 250
    :cond_1
    const v3, -0x43dc28f6    # -0.01f

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 252
    iget-object v1, p0, Lgfk;->a:Lgfg;

    invoke-static {v1}, Lgfg;->c(Lgfg;)I

    move-result v1

    mul-int/2addr v1, v0

    iget-object v2, p0, Lgfk;->a:Lgfg;

    invoke-static {v2}, Lgfg;->d(Lgfg;)I

    move-result v2

    div-int/2addr v1, v2

    goto :goto_0
.end method
