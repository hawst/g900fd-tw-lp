.class public Lilb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static c:Ljava/util/logging/Logger;

.field private static synthetic f:Z


# instance fields
.field a:Ljava/util/HashMap;

.field private b:Ljava/util/Set;

.field private d:Ljava/util/HashMap;

.field private e:Lild;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lilb;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lilb;->f:Z

    .line 77
    const-class v0, Lilb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lilb;->c:Ljava/util/logging/Logger;

    return-void

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lilb;->b:Ljava/util/Set;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lilb;->a:Ljava/util/HashMap;

    .line 80
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lilb;->d:Ljava/util/HashMap;

    .line 81
    new-instance v0, Lile;

    invoke-direct {v0}, Lile;-><init>()V

    iput-object v0, p0, Lilb;->e:Lild;

    .line 386
    return-void
.end method

.method private static a(JJ)J
    .locals 2

    .prologue
    .line 562
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 563
    return-wide p0

    .line 565
    :cond_0
    rem-long v0, p0, p2

    move-wide p0, p2

    move-wide p2, v0

    goto :goto_0
.end method

.method private static a(Likz;Z)J
    .locals 8

    .prologue
    .line 537
    const-wide/16 v0, 0x0

    .line 538
    invoke-virtual {p0}, Likz;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzq;

    .line 539
    iget-wide v6, v0, Lzq;->a:J

    iget-wide v0, v0, Lzq;->b:J

    mul-long/2addr v0, v6

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 540
    goto :goto_0

    .line 542
    :cond_0
    if-eqz p1, :cond_1

    .line 543
    invoke-virtual {p0}, Likz;->g()Lila;

    move-result-object v0

    iget-wide v0, v0, Lila;->i:D

    .line 544
    invoke-virtual {p0}, Likz;->g()Lila;

    move-result-object v4

    iget-wide v4, v4, Lila;->j:D

    .line 545
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    invoke-virtual {p0}, Likz;->g()Lila;

    move-result-object v0

    iget-wide v0, v0, Lila;->b:J

    long-to-double v0, v0

    mul-double/2addr v0, v4

    double-to-long v2, v0

    .line 550
    :cond_1
    return-wide v2
.end method

.method private b(Likx;)Lzc;
    .locals 28

    .prologue
    .line 144
    new-instance v14, Lzc;

    invoke-direct {v14}, Lzc;-><init>()V

    .line 145
    new-instance v6, Lzd;

    invoke-direct {v6}, Lzd;-><init>()V

    .line 147
    move-object/from16 v0, p1

    iget-object v2, v0, Likx;->c:Ljava/util/Date;

    invoke-static {v2}, La;->a(Ljava/util/Date;)J

    move-result-wide v2

    iput-wide v2, v6, Lzd;->a:J

    .line 148
    move-object/from16 v0, p1

    iget-object v2, v0, Likx;->b:Ljava/util/Date;

    invoke-static {v2}, La;->a(Ljava/util/Date;)J

    move-result-wide v2

    iput-wide v2, v6, Lzd;->b:J

    .line 150
    invoke-static/range {p1 .. p1}, Lilb;->c(Likx;)J

    move-result-wide v8

    .line 151
    const-wide/16 v4, 0x0

    .line 153
    move-object/from16 v0, p1

    iget-object v2, v0, Likx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Likz;

    .line 154
    const/4 v3, 0x1

    invoke-static {v2, v3}, Lilb;->a(Likz;Z)J

    move-result-wide v10

    mul-long/2addr v10, v8

    invoke-virtual {v2}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v2, v2, Lila;->b:J

    div-long v2, v10, v2

    .line 155
    cmp-long v10, v2, v4

    if-lez v10, :cond_1c

    :goto_1
    move-wide v4, v2

    .line 160
    goto :goto_0

    .line 162
    :cond_0
    iput-wide v4, v6, Lzd;->d:J

    .line 163
    iput-wide v8, v6, Lzd;->c:J

    .line 165
    const-wide/16 v4, 0x0

    .line 166
    move-object/from16 v0, p1

    iget-object v2, v0, Likx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Likz;

    .line 167
    invoke-virtual {v2}, Likz;->g()Lila;

    move-result-object v3

    iget-wide v8, v3, Lila;->g:J

    cmp-long v3, v4, v8

    if-gez v3, :cond_1

    invoke-virtual {v2}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v2, v2, Lila;->g:J

    :goto_3
    move-wide v4, v2

    .line 168
    goto :goto_2

    :cond_1
    move-wide v2, v4

    .line 167
    goto :goto_3

    .line 169
    :cond_2
    const-wide/16 v2, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v6, Lzd;->e:J

    .line 170
    iget-wide v2, v6, Lzd;->a:J

    const-wide v4, 0x100000000L

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 171
    iget-wide v2, v6, Lzd;->b:J

    const-wide v4, 0x100000000L

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 172
    iget-wide v2, v6, Lzd;->d:J

    const-wide v4, 0x100000000L

    cmp-long v2, v2, v4

    if-ltz v2, :cond_4

    .line 173
    :cond_3
    const/4 v2, 0x1

    iput v2, v6, Likv;->o:I

    .line 176
    :cond_4
    invoke-virtual {v14, v6}, Lzc;->a(Lyj;)V

    .line 177
    move-object/from16 v0, p1

    iget-object v2, v0, Likx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_4
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Likz;

    .line 178
    sget-object v2, Lilb;->c:Ljava/util/logging/Logger;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x16

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Creating Mp4TrackImpl "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    new-instance v16, Lzr;

    invoke-direct/range {v16 .. v16}, Lzr;-><init>()V

    new-instance v3, Lzs;

    invoke-direct {v3}, Lzs;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v10}, Likz;->j()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v2, 0x1

    :cond_5
    invoke-virtual {v10}, Likz;->k()Z

    move-result v4

    if-eqz v4, :cond_6

    add-int/lit8 v2, v2, 0x2

    :cond_6
    invoke-virtual {v10}, Likz;->l()Z

    move-result v4

    if-eqz v4, :cond_7

    add-int/lit8 v2, v2, 0x4

    :cond_7
    invoke-virtual {v10}, Likz;->m()Z

    move-result v4

    if-eqz v4, :cond_8

    add-int/lit8 v2, v2, 0x8

    :cond_8
    iput v2, v3, Likv;->p:I

    invoke-virtual {v10}, Likz;->g()Lila;

    const/4 v2, 0x0

    iput v2, v3, Lzs;->f:I

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-object v2, v2, Lila;->d:Ljava/util/Date;

    invoke-static {v2}, La;->a(Ljava/util/Date;)J

    move-result-wide v4

    iput-wide v4, v3, Lzs;->a:J

    const/4 v2, 0x1

    invoke-static {v10, v2}, Lilb;->a(Likz;Z)J

    move-result-wide v4

    invoke-static/range {p1 .. p1}, Lilb;->c(Likx;)J

    move-result-wide v6

    mul-long/2addr v4, v6

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v6, v2, Lila;->b:J

    div-long/2addr v4, v6

    iput-wide v4, v3, Lzs;->d:J

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v4, v2, Lila;->f:D

    iput-wide v4, v3, Lzs;->j:D

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v4, v2, Lila;->e:D

    iput-wide v4, v3, Lzs;->i:D

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget v2, v2, Lila;->k:I

    iput v2, v3, Lzs;->e:I

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-object v2, v2, Lila;->c:Ljava/util/Date;

    invoke-static {v2}, La;->a(Ljava/util/Date;)J

    move-result-wide v4

    iput-wide v4, v3, Lzs;->b:J

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v4, v2, Lila;->g:J

    iput-wide v4, v3, Lzs;->c:J

    invoke-virtual {v10}, Likz;->g()Lila;

    const/4 v2, 0x0

    iput v2, v3, Lzs;->g:F

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-object v2, v2, Lila;->h:[J

    iput-object v2, v3, Lzs;->h:[J

    iget-wide v4, v3, Lzs;->a:J

    const-wide v6, 0x100000000L

    cmp-long v2, v4, v6

    if-gez v2, :cond_9

    iget-wide v4, v3, Lzs;->b:J

    const-wide v6, 0x100000000L

    cmp-long v2, v4, v6

    if-gez v2, :cond_9

    iget-wide v4, v3, Lzs;->d:J

    const-wide v6, 0x100000000L

    cmp-long v2, v4, v6

    if-ltz v2, :cond_a

    :cond_9
    const/4 v2, 0x1

    iput v2, v3, Likv;->o:I

    :cond_a
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Lzr;->a(Lyj;)V

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v2, v2, Lila;->i:D

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v4

    iget-wide v4, v4, Lila;->j:D

    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_b

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_b

    new-instance v11, Lyr;

    invoke-direct {v11}, Lyr;-><init>()V

    new-instance v3, Lys;

    invoke-direct {v3}, Lys;-><init>()V

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v4, v2, Lila;->i:D

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v6, v2, Lila;->b:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-long v6, v4

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v2

    iget-wide v4, v2, Lila;->j:D

    invoke-static/range {p1 .. p1}, Lilb;->c(Likx;)J

    move-result-wide v8

    long-to-double v8, v8

    mul-double/2addr v4, v8

    double-to-long v4, v4

    new-instance v2, Lyt;

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-direct/range {v2 .. v9}, Lyt;-><init>(Lys;JJD)V

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v3, Lys;->a:Ljava/util/List;

    invoke-virtual {v11, v3}, Lyr;->a(Lyj;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lzr;->a(Lyj;)V

    :cond_b
    new-instance v17, Lyz;

    invoke-direct/range {v17 .. v17}, Lyz;-><init>()V

    invoke-virtual/range {v16 .. v17}, Lzr;->a(Lyj;)V

    new-instance v2, Lza;

    invoke-direct {v2}, Lza;-><init>()V

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v3

    iget-object v3, v3, Lila;->d:Ljava/util/Date;

    invoke-static {v3}, La;->a(Ljava/util/Date;)J

    move-result-wide v4

    iput-wide v4, v2, Lza;->a:J

    const/4 v3, 0x0

    invoke-static {v10, v3}, Lilb;->a(Likz;Z)J

    move-result-wide v4

    iput-wide v4, v2, Lza;->d:J

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v3

    iget-wide v4, v3, Lila;->b:J

    iput-wide v4, v2, Lza;->c:J

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v3

    iget-object v3, v3, Lila;->a:Ljava/lang/String;

    iput-object v3, v2, Lza;->e:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lyz;->a(Lyj;)V

    new-instance v2, Lyx;

    invoke-direct {v2}, Lyx;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lyz;->a(Lyj;)V

    invoke-virtual {v10}, Likz;->h()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lyx;->a:Ljava/lang/String;

    new-instance v18, Lzb;

    invoke-direct/range {v18 .. v18}, Lzb;-><init>()V

    invoke-virtual {v10}, Likz;->i()Lyj;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lzb;->a(Lyj;)V

    new-instance v2, Lyp;

    invoke-direct {v2}, Lyp;-><init>()V

    new-instance v3, Lyq;

    invoke-direct {v3}, Lyq;-><init>()V

    invoke-virtual {v2, v3}, Lyp;->a(Lyj;)V

    new-instance v4, Lyo;

    invoke-direct {v4}, Lyo;-><init>()V

    const/4 v5, 0x1

    iput v5, v4, Likv;->p:I

    invoke-interface {v4, v3}, Lyj;->a(Lyn;)V

    iget-object v3, v3, Likw;->a:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lzb;->a(Lyj;)V

    new-instance v19, Lzi;

    invoke-direct/range {v19 .. v19}, Lzi;-><init>()V

    invoke-virtual {v10}, Likz;->b()Lzg;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lzi;->a(Lyj;)V

    invoke-virtual {v10}, Likz;->c()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_c

    invoke-virtual {v10}, Likz;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    new-instance v2, Lzp;

    invoke-direct {v2}, Lzp;-><init>()V

    invoke-virtual {v10}, Likz;->c()Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lzp;->a:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lzi;->a(Lyj;)V

    :cond_c
    invoke-virtual {v10}, Likz;->d()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_d

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_d

    new-instance v3, Lyl;

    invoke-direct {v3}, Lyl;-><init>()V

    iput-object v2, v3, Lyl;->a:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lzi;->a(Lyj;)V

    :cond_d
    invoke-virtual {v10}, Likz;->e()[J

    move-result-object v2

    if-eqz v2, :cond_e

    array-length v3, v2

    if-lez v3, :cond_e

    new-instance v3, Lzo;

    invoke-direct {v3}, Lzo;-><init>()V

    iput-object v2, v3, Lzo;->a:[J

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lzi;->a(Lyj;)V

    :cond_e
    invoke-virtual {v10}, Likz;->f()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_f

    invoke-virtual {v10}, Likz;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_f

    new-instance v2, Lze;

    invoke-direct {v2}, Lze;-><init>()V

    invoke-virtual {v10}, Likz;->f()Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lze;->a:Ljava/util/List;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lzi;->a(Lyj;)V

    :cond_f
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p1

    iget-object v2, v0, Likx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Likz;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1}, Lilb;->a(Likz;Likx;)[I

    move-result-object v4

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_10
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [I

    new-instance v12, Lzj;

    invoke-direct {v12}, Lzj;-><init>()V

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, v12, Lzj;->a:Ljava/util/List;

    const-wide/32 v4, -0x80000000

    const/4 v3, 0x0

    move v11, v3

    :goto_6
    array-length v3, v2

    if-ge v11, v3, :cond_12

    aget v3, v2, v11

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-eqz v3, :cond_11

    iget-object v13, v12, Lzj;->a:Ljava/util/List;

    new-instance v3, Lzk;

    add-int/lit8 v4, v11, 0x1

    int-to-long v4, v4

    aget v6, v2, v11

    int-to-long v6, v6

    const-wide/16 v8, 0x1

    invoke-direct/range {v3 .. v9}, Lzk;-><init>(JJJ)V

    invoke-interface {v13, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    aget v3, v2, v11

    int-to-long v4, v3

    :cond_11
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    goto :goto_6

    :cond_12
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lzi;->a(Lyj;)V

    new-instance v4, Lzh;

    invoke-direct {v4}, Lzh;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lilb;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [J

    iput-object v3, v4, Lzh;->b:[J

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lzi;->a(Lyj;)V

    new-instance v11, Lzm;

    invoke-direct {v11}, Lzm;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lilb;->b:Ljava/util/Set;

    invoke-interface {v3, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-wide/16 v4, 0x0

    array-length v3, v2

    new-array v0, v3, [J

    move-object/from16 v21, v0

    sget-object v3, Lilb;->c:Ljava/util/logging/Logger;

    sget-object v6, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v3, v6}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_13

    sget-object v3, Lilb;->c:Ljava/util/logging/Logger;

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v6

    iget-wide v6, v6, Lila;->g:J

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x38

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Calculating chunk offsets for track_"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    :cond_13
    const/4 v3, 0x0

    move v6, v3

    :goto_7
    array-length v3, v2

    if-ge v6, v3, :cond_1a

    sget-object v3, Lilb;->c:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->FINER:Ljava/util/logging/Level;

    invoke-virtual {v3, v7}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_14

    sget-object v3, Lilb;->c:Ljava/util/logging/Logger;

    invoke-virtual {v10}, Likz;->g()Lila;

    move-result-object v7

    iget-wide v8, v7, Lila;->g:J

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v12, 0x4a

    invoke-direct {v7, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Calculating chunk offsets for track_"

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " chunk "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/logging/Logger;->finer(Ljava/lang/String;)V

    :cond_14
    move-object/from16 v0, p1

    iget-object v3, v0, Likx;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    move-wide v8, v4

    :cond_15
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_19

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Likz;

    sget-object v3, Lilb;->c:Ljava/util/logging/Logger;

    sget-object v4, Ljava/util/logging/Level;->FINEST:Ljava/util/logging/Level;

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v3

    if-eqz v3, :cond_16

    sget-object v3, Lilb;->c:Ljava/util/logging/Logger;

    invoke-virtual {v5}, Likz;->g()Lila;

    move-result-object v4

    iget-wide v12, v4, Lila;->g:J

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v7, 0x2c

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Adding offsets of track_"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/logging/Logger;->finest(Ljava/lang/String;)V

    :cond_16
    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [I

    const-wide/16 v12, 0x0

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v6, :cond_17

    aget v7, v3, v4

    int-to-long v0, v7

    move-wide/from16 v24, v0

    add-long v12, v12, v24

    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_17
    if-ne v5, v10, :cond_18

    aput-wide v8, v21, v6

    :cond_18
    invoke-static {v12, v13}, La;->d(J)I

    move-result v4

    move v7, v4

    :goto_9
    int-to-long v0, v7

    move-wide/from16 v24, v0

    aget v4, v3, v6

    int-to-long v0, v4

    move-wide/from16 v26, v0

    add-long v26, v26, v12

    cmp-long v4, v24, v26

    if-gez v4, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lilb;->d:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [J

    aget-wide v24, v4, v7

    add-long v8, v8, v24

    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_9

    :cond_19
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-wide v4, v8

    goto/16 :goto_7

    :cond_1a
    move-object/from16 v0, v21

    iput-object v0, v11, Lzm;->a:[J

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lzi;->a(Lyj;)V

    invoke-virtual/range {v18 .. v19}, Lzb;->a(Lyj;)V

    invoke-virtual/range {v17 .. v18}, Lyz;->a(Lyj;)V

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lzc;->a(Lyj;)V

    goto/16 :goto_4

    .line 181
    :cond_1b
    return-object v14

    :cond_1c
    move-wide v2, v4

    goto/16 :goto_1
.end method

.method private static c(Likx;)J
    .locals 5

    .prologue
    .line 554
    iget-object v0, p0, Likx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likz;

    invoke-virtual {v0}, Likz;->g()Lila;

    move-result-object v0

    iget-wide v0, v0, Lila;->b:J

    .line 555
    iget-object v2, p0, Likx;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likz;

    .line 556
    invoke-virtual {v0}, Likz;->g()Lila;

    move-result-object v0

    iget-wide v0, v0, Lila;->b:J

    invoke-static {v0, v1, v2, v3}, Lilb;->a(JJ)J

    move-result-wide v0

    move-wide v2, v0

    .line 557
    goto :goto_0

    .line 558
    :cond_0
    return-wide v2
.end method


# virtual methods
.method public final a(Likx;)Lye;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 91
    sget-object v0, Lilb;->c:Ljava/util/logging/Logger;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xf

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Creating movie "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 92
    iget-object v0, p1, Likx;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Likz;

    .line 94
    invoke-virtual {v0}, Likz;->a()Ljava/util/List;

    move-result-object v5

    .line 95
    iget-object v1, p0, Lilb;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 96
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    new-array v6, v1, [J

    move v2, v3

    .line 97
    :goto_1
    array-length v1, v6

    if-ge v2, v1, :cond_0

    .line 98
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    int-to-long v8, v1

    aput-wide v8, v6, v2

    .line 97
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 100
    :cond_0
    iget-object v1, p0, Lilb;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    goto :goto_0

    .line 103
    :cond_1
    new-instance v1, Lye;

    invoke-direct {v1}, Lye;-><init>()V

    .line 105
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 106
    const-string v2, "isom"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    const-string v2, "iso2"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v2, "avc1"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    new-instance v2, Lyu;

    const-string v4, "isom"

    const-wide/16 v6, 0x0

    invoke-direct {v2, v4, v6, v7, v0}, Lyu;-><init>(Ljava/lang/String;JLjava/util/List;)V

    invoke-virtual {v1, v2}, Lye;->a(Lyj;)V

    .line 111
    invoke-direct {p0, p1}, Lilb;->b(Likx;)Lzc;

    move-result-object v0

    invoke-virtual {v1, v0}, Lye;->a(Lyj;)V

    .line 112
    new-instance v0, Lilc;

    invoke-direct {v0, p0, p1}, Lilc;-><init>(Lilb;Likx;)V

    .line 113
    invoke-virtual {v1, v0}, Lye;->a(Lyj;)V

    .line 119
    invoke-virtual {v0}, Lilc;->b()J

    move-result-wide v4

    .line 120
    iget-object v0, p0, Lilb;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzm;

    .line 121
    iget-object v6, v0, Lzm;->a:[J

    move v0, v3

    .line 122
    :goto_2
    array-length v7, v6

    if-ge v0, v7, :cond_2

    .line 123
    aget-wide v8, v6, v0

    add-long/2addr v8, v4

    aput-wide v8, v6, v0

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 128
    :cond_3
    return-object v1
.end method

.method final a(Likz;Likx;)[I
    .locals 10

    .prologue
    const-wide/16 v8, 0x1

    const/4 v1, 0x0

    .line 505
    iget-object v0, p0, Lilb;->e:Lild;

    invoke-interface {v0, p1, p2}, Lild;->a(Likz;Likx;)[J

    move-result-object v4

    .line 506
    array-length v0, v4

    new-array v5, v0, [I

    move v0, v1

    .line 509
    :goto_0
    array-length v2, v4

    if-ge v0, v2, :cond_1

    .line 510
    aget-wide v2, v4, v0

    sub-long v6, v2, v8

    .line 512
    array-length v2, v4

    add-int/lit8 v3, v0, 0x1

    if-ne v2, v3, :cond_0

    .line 513
    invoke-virtual {p1}, Likz;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    int-to-long v2, v2

    .line 518
    :goto_1
    sub-long/2addr v2, v6

    invoke-static {v2, v3}, La;->d(J)I

    move-result v2

    aput v2, v5, v0

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 515
    :cond_0
    add-int/lit8 v2, v0, 0x1

    aget-wide v2, v4, v2

    sub-long/2addr v2, v8

    goto :goto_1

    .line 521
    :cond_1
    sget-boolean v0, Lilb;->f:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lilb;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v6, v0

    const-wide/16 v2, 0x0

    array-length v0, v5

    :goto_2
    if-ge v1, v0, :cond_2

    aget v4, v5, v1

    int-to-long v8, v4

    add-long/2addr v2, v8

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    cmp-long v0, v6, v2

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "The number of samples and the sum of all chunk lengths must be equal"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 522
    :cond_3
    return-object v5
.end method
