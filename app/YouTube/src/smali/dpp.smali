.class Ldpp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldqb;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Ldpr;

.field private c:I

.field private d:I

.field private final e:Ljava/io/CharArrayWriter;

.field private final f:Ljava/io/CharArrayWriter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Ldpp;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ldpp;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput v0, p0, Ldpp;->c:I

    .line 49
    new-instance v0, Ljava/io/CharArrayWriter;

    invoke-direct {v0}, Ljava/io/CharArrayWriter;-><init>()V

    iput-object v0, p0, Ldpp;->e:Ljava/io/CharArrayWriter;

    .line 50
    new-instance v0, Ljava/io/CharArrayWriter;

    invoke-direct {v0}, Ljava/io/CharArrayWriter;-><init>()V

    iput-object v0, p0, Ldpp;->f:Ljava/io/CharArrayWriter;

    .line 51
    return-void
.end method

.method private a([CII)I
    .locals 6

    .prologue
    const/16 v1, 0xa

    .line 155
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, p3, :cond_2

    .line 156
    add-int v0, p2, v2

    aget-char v0, p1, v0

    if-ne v0, v1, :cond_1

    .line 157
    iget-object v0, p0, Ldpp;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v0, p1, p2, v2}, Ljava/io/CharArrayWriter;->write([CII)V

    .line 158
    iget-object v0, p0, Ldpp;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    move-result-object v3

    .line 160
    const/16 v0, 0xa

    :try_start_0
    invoke-static {v3, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ldpp;->d:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    const/4 v0, 0x2

    iput v0, p0, Ldpp;->c:I

    .line 170
    iget-object v0, p0, Ldpp;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->reset()V

    .line 171
    add-int/lit8 p3, v2, 0x1

    .line 177
    :goto_1
    return p3

    .line 161
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 162
    sget-object v4, Ldpp;->b:Ljava/lang/String;

    const-string v5, "Error parsing chunk length: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v4, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 163
    const/4 v0, 0x1

    iput v0, p0, Ldpp;->c:I

    .line 165
    iget-object v0, p0, Ldpp;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v0}, Ljava/io/CharArrayWriter;->reset()V

    .line 166
    add-int/lit8 p3, v2, 0x1

    goto :goto_1

    .line 162
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 155
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Ldpp;->e:Ljava/io/CharArrayWriter;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/CharArrayWriter;->write([CII)V

    goto :goto_1
.end method


# virtual methods
.method public final a([C)I
    .locals 6

    .prologue
    .line 141
    const/4 v0, 0x0

    array-length v2, p1

    move v1, v2

    move v3, v0

    :goto_0
    if-eqz v1, :cond_2

    sget-object v0, Ldpq;->a:[I

    iget v4, p0, Ldpp;->c:I

    add-int/lit8 v4, v4, -0x1

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :cond_0
    :goto_1
    add-int/2addr v3, v0

    sub-int/2addr v1, v0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p1, v3, v1}, Ldpp;->a([CII)I

    move-result v0

    goto :goto_1

    :pswitch_1
    iget v0, p0, Ldpp;->d:I

    if-lt v1, v0, :cond_3

    iget v0, p0, Ldpp;->d:I

    const/4 v4, 0x1

    iput v4, p0, Ldpp;->c:I

    :goto_2
    iget-object v4, p0, Ldpp;->f:Ljava/io/CharArrayWriter;

    invoke-virtual {v4, p1, v3, v0}, Ljava/io/CharArrayWriter;->write([CII)V

    iget v4, p0, Ldpp;->d:I

    sub-int/2addr v4, v0

    iput v4, p0, Ldpp;->d:I

    iget v4, p0, Ldpp;->d:I

    if-nez v4, :cond_0

    iget-object v4, p0, Ldpp;->a:Ldpr;

    if-eqz v4, :cond_1

    iget-object v4, p0, Ldpp;->a:Ldpr;

    iget-object v5, p0, Ldpp;->f:Ljava/io/CharArrayWriter;

    invoke-virtual {v5}, Ljava/io/CharArrayWriter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ldpr;->a(Ljava/lang/String;)V

    :cond_1
    iget-object v4, p0, Ldpp;->f:Ljava/io/CharArrayWriter;

    invoke-virtual {v4}, Ljava/io/CharArrayWriter;->reset()V

    goto :goto_1

    :cond_2
    sub-int v0, v2, v1

    return v0

    :cond_3
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 80
    const/16 v0, 0x194

    if-ne p1, v0, :cond_0

    .line 81
    new-instance v0, Ldqt;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x25

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected response code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ldqt;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    const/16 v0, 0xc8

    if-eq p1, v0, :cond_1

    .line 83
    new-instance v0, Ldqy;

    invoke-direct {v0, p1}, Ldqy;-><init>(I)V

    throw v0

    .line 85
    :cond_1
    return-void
.end method

.method public final a([BII)V
    .locals 2

    .prologue
    .line 65
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, p2, p3, v1}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 69
    invoke-virtual {p0, v0}, Ldpp;->a([C)I

    .line 70
    return-void

    .line 67
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This application needs UTF-8 support in order to run"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 93
    const/4 v0, 0x2

    iget v1, p0, Ldpp;->c:I

    if-ne v0, v1, :cond_0

    sget-object v0, Ldpp;->b:Ljava/lang/String;

    const-string v1, "Lost partial chunk"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    return-void
.end method
