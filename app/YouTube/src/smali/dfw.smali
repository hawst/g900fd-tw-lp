.class public final Ldfw;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Ljava/lang/String;

.field final a:Levn;

.field final b:Ldfu;

.field final c:Lfhz;

.field d:Lhqz;

.field e:Z

.field f:[Z

.field g:[Z

.field h:I

.field i:Lhbw;

.field j:I

.field k:Lfkg;

.field l:Ldgc;

.field private final m:Leyp;

.field private final n:Lcwz;

.field private final o:Landroid/os/Handler;

.field private final p:Z

.field private q:Z

.field private r:Z

.field private s:Leue;

.field private t:Leue;

.field private u:Leue;

.field private v:Leue;

.field private w:Z

.field private x:Ljava/util/List;

.field private y:Lhjl;

.field private z:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Levn;Ldfu;Leyp;Lfhz;Lcwz;Z)V
    .locals 2

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Ldfw;->a:Levn;

    .line 106
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldfu;

    iput-object v0, p0, Ldfw;->b:Ldfu;

    .line 107
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Ldfw;->m:Leyp;

    .line 108
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Ldfw;->c:Lfhz;

    .line 109
    iput-object p6, p0, Ldfw;->n:Lcwz;

    .line 110
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ldfw;->o:Landroid/os/Handler;

    .line 111
    iput-boolean p7, p0, Ldfw;->p:Z

    .line 112
    const/4 v0, -0x1

    iput v0, p0, Ldfw;->h:I

    .line 113
    new-instance v0, Ldgc;

    invoke-direct {v0}, Ldgc;-><init>()V

    iput-object v0, p0, Ldfw;->l:Ldgc;

    .line 114
    new-instance v0, Ldgd;

    invoke-direct {v0, p0}, Ldgd;-><init>(Ldfw;)V

    invoke-interface {p3, v0}, Ldfu;->a(Ldfv;)V

    .line 115
    return-void
.end method

.method private a(Lhxf;Ldga;)Leue;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 228
    if-eqz p1, :cond_0

    iget-object v0, p1, Lhxf;->b:[Lhxg;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 231
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 239
    :goto_1
    return-object v1

    .line 228
    :cond_1
    iget-object v0, p1, Lhxf;->b:[Lhxg;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    iget-object v0, v0, Lhxg;->b:Ljava/lang/String;

    goto :goto_0

    .line 234
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 236
    invoke-static {p2}, Leue;->a(Leuc;)Leue;

    move-result-object v1

    .line 237
    iget-object v2, p0, Ldfw;->m:Leyp;

    iget-object v3, p0, Ldfw;->o:Landroid/os/Handler;

    .line 238
    invoke-static {v3, v1}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v3

    .line 237
    invoke-interface {v2, v0, v3}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    goto :goto_1
.end method

.method private a(Lfrl;)V
    .locals 4

    .prologue
    .line 511
    invoke-virtual {p1}, Lfrl;->j()Lhqz;

    move-result-object v0

    .line 512
    invoke-virtual {p1}, Lfrl;->k()Lfkg;

    move-result-object v1

    .line 513
    iget-object v2, p1, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 511
    invoke-direct {p0, v0, v1, v2, v3}, Ldfw;->a(Lhqz;Lfkg;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    return-void
.end method

.method private a(Lhqz;Lfkg;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 151
    invoke-direct {p0}, Ldfw;->b()V

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfw;->q:Z

    .line 153
    iput-object p1, p0, Ldfw;->d:Lhqz;

    .line 154
    iget-object v0, p0, Ldfw;->l:Ldgc;

    iput-object p3, v0, Ldgc;->a:Ljava/lang/String;

    .line 155
    if-eqz p4, :cond_5

    :goto_0
    iput-object p4, p0, Ldfw;->G:Ljava/lang/String;

    .line 156
    if-eqz p1, :cond_1

    .line 157
    iget-object v0, p0, Ldfw;->b:Ldfu;

    iget-boolean v1, p0, Ldfw;->F:Z

    invoke-interface {v0, v1}, Ldfu;->c(Z)V

    .line 158
    iget-object v0, p1, Lhqz;->a:Lhgm;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lhqz;->a:Lhgm;

    iget-object v0, v0, Lhgm;->d:Ljava/lang/String;

    .line 159
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Ldfw;->b:Ldfu;

    iget-object v1, p1, Lhqz;->a:Lhgm;

    iget-object v1, v1, Lhgm;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Ldfu;->a(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Ldfw;->b:Ldfu;

    iget-object v1, p1, Lhqz;->a:Lhgm;

    iget-object v1, v1, Lhgm;->f:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    invoke-interface {v0, v1}, Ldfu;->b(Ljava/lang/CharSequence;)V

    .line 164
    :cond_0
    iget-object v0, p1, Lhqz;->c:[Lhbw;

    array-length v0, v0

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p1, Lhqz;->c:[Lhbw;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldfw;->x:Ljava/util/List;

    .line 166
    iget-object v0, p0, Ldfw;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 167
    new-array v1, v0, [Z

    iput-object v1, p0, Ldfw;->f:[Z

    .line 168
    new-array v0, v0, [Z

    iput-object v0, p0, Ldfw;->g:[Z

    .line 171
    :cond_1
    iget-boolean v0, p0, Ldfw;->p:Z

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 172
    iput-object p2, p0, Ldfw;->k:Lfkg;

    .line 173
    invoke-virtual {p2}, Lfkg;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ldfw;->z:Ljava/util/List;

    .line 174
    invoke-direct {p0}, Ldfw;->c()V

    .line 176
    :cond_2
    iget-object v0, p0, Ldfw;->d:Lhqz;

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->b:Lhgl;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lhgl;->c:Lhxf;

    new-instance v1, Ldfy;

    invoke-direct {v1, p0}, Ldfy;-><init>(Ldfw;)V

    invoke-direct {p0, v0, v1}, Ldfw;->a(Lhxf;Ldga;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldfw;->s:Leue;

    :cond_3
    iget-object v0, p0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->a:Lhgm;

    if-eqz v0, :cond_4

    iget-object v0, v0, Lhgm;->c:Lhxf;

    new-instance v1, Ldfz;

    invoke-direct {v1, p0}, Ldfz;-><init>(Ldfw;)V

    invoke-direct {p0, v0, v1}, Ldfw;->a(Lhxf;Ldga;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldfw;->t:Leue;

    .line 177
    :cond_4
    return-void

    :cond_5
    move-object p4, p3

    .line 155
    goto/16 :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 121
    iget-object v0, p0, Ldfw;->s:Leue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfw;->s:Leue;

    iput-boolean v3, v0, Leue;->a:Z

    iput-object v1, p0, Ldfw;->s:Leue;

    :cond_0
    iget-object v0, p0, Ldfw;->t:Leue;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ldfw;->t:Leue;

    iput-boolean v3, v0, Leue;->a:Z

    iput-object v1, p0, Ldfw;->t:Leue;

    :cond_1
    iget-object v0, p0, Ldfw;->u:Leue;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldfw;->u:Leue;

    iput-boolean v3, v0, Leue;->a:Z

    iput-object v1, p0, Ldfw;->u:Leue;

    :cond_2
    iget-object v0, p0, Ldfw;->v:Leue;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ldfw;->v:Leue;

    iput-boolean v3, v0, Leue;->a:Z

    iput-object v1, p0, Ldfw;->v:Leue;

    .line 122
    :cond_3
    iput-boolean v2, p0, Ldfw;->q:Z

    .line 123
    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0}, Ldfu;->a()V

    .line 124
    iput-boolean v2, p0, Ldfw;->A:Z

    .line 125
    iput-boolean v2, p0, Ldfw;->B:Z

    .line 126
    iput-boolean v2, p0, Ldfw;->C:Z

    .line 127
    iput-boolean v2, p0, Ldfw;->D:Z

    .line 128
    iput-boolean v2, p0, Ldfw;->E:Z

    .line 129
    iput-boolean v2, p0, Ldfw;->e:Z

    .line 130
    iput-object v1, p0, Ldfw;->f:[Z

    .line 131
    iput-object v1, p0, Ldfw;->g:[Z

    .line 132
    iput v4, p0, Ldfw;->h:I

    .line 133
    iput-object v1, p0, Ldfw;->i:Lhbw;

    .line 134
    iput-object v1, p0, Ldfw;->d:Lhqz;

    .line 135
    iput-object v1, p0, Ldfw;->y:Lhjl;

    .line 136
    iput-object v1, p0, Ldfw;->z:Ljava/util/List;

    .line 137
    iput-object v1, p0, Ldfw;->k:Lfkg;

    .line 138
    iput v4, p0, Ldfw;->j:I

    .line 139
    iget-object v0, p0, Ldfw;->l:Ldgc;

    iput-object v1, v0, Ldgc;->a:Ljava/lang/String;

    .line 140
    iput-object v1, p0, Ldfw;->G:Ljava/lang/String;

    .line 141
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 180
    iget-object v0, p0, Ldfw;->a:Levn;

    new-instance v1, Ldag;

    iget-object v2, p0, Ldfw;->G:Ljava/lang/String;

    iget-object v3, p0, Ldfw;->k:Lfkg;

    iget-object v4, p0, Ldfw;->l:Ldgc;

    .line 181
    invoke-virtual {v4}, Ldgc;->a()Lfqg;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Ldag;-><init>(Ljava/lang/String;Lfkg;Lfqg;)V

    .line 180
    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 182
    return-void
.end method

.method private d()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 352
    iget-object v0, p0, Ldfw;->i:Lhbw;

    iget-object v0, v0, Lhbw;->i:Lhrr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfw;->i:Lhbw;

    iget-object v0, v0, Lhbw;->i:Lhrr;

    iget-object v0, v0, Lhrr;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfw;->i:Lhbw;

    iget-object v0, v0, Lhbw;->i:Lhrr;

    iget-object v0, v0, Lhrr;->a:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 363
    :cond_0
    :goto_0
    return v1

    .line 356
    :cond_1
    iget-boolean v0, p0, Ldfw;->r:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    .line 358
    :goto_1
    iget-object v3, p0, Ldfw;->i:Lhbw;

    iget-object v3, v3, Lhbw;->i:Lhrr;

    iget-object v4, v3, Lhrr;->a:[I

    array-length v5, v4

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_3

    aget v6, v4, v3

    .line 359
    if-eq v0, v6, :cond_0

    .line 358
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 356
    goto :goto_1

    :cond_3
    move v1, v2

    .line 363
    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 487
    iget-object v1, p0, Ldfw;->n:Lcwz;

    iget-boolean v0, p0, Ldfw;->B:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldfw;->C:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Ldfw;->E:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcwz;->a(Z)V

    .line 489
    return-void

    .line 487
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleSequencerStageEvent(Lczu;)V
    .locals 3
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 531
    iget-object v0, p1, Lczu;->b:Lfrl;

    if-nez v0, :cond_1

    .line 532
    iget-object v0, p0, Ldfw;->l:Ldgc;

    invoke-virtual {v0, v1, v1}, Ldgc;->a(Lfqg;Ljava/lang/String;)V

    .line 543
    :cond_0
    :goto_0
    return-void

    .line 535
    :cond_1
    iget-object v0, p0, Ldfw;->l:Ldgc;

    .line 536
    iget-object v1, p1, Lczu;->d:Lfqg;

    .line 537
    iget-object v2, p1, Lczu;->b:Lfrl;

    iget-object v2, v2, Lfrl;->a:Lhro;

    invoke-static {v2}, Lfrl;->a(Lhro;)Ljava/lang/String;

    move-result-object v2

    .line 535
    invoke-virtual {v0, v1, v2}, Ldgc;->a(Lfqg;Ljava/lang/String;)V

    .line 539
    iget-object v0, p0, Ldfw;->l:Ldgc;

    invoke-virtual {v0}, Ldgc;->a()Lfqg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 541
    invoke-direct {p0}, Ldfw;->c()V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 446
    iget-boolean v0, p0, Ldfw;->B:Z

    if-eqz v0, :cond_0

    .line 447
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldfw;->B:Z

    .line 448
    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0}, Ldfu;->e()V

    .line 449
    invoke-direct {p0}, Ldfw;->e()V

    .line 451
    :cond_0
    return-void
.end method

.method a(Z)V
    .locals 1

    .prologue
    .line 463
    iget-boolean v0, p0, Ldfw;->C:Z

    if-eqz v0, :cond_0

    .line 464
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldfw;->C:Z

    .line 465
    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0, p1}, Ldfu;->b(Z)V

    .line 466
    invoke-direct {p0}, Ldfw;->e()V

    .line 468
    :cond_0
    return-void
.end method

.method a(ZZ)V
    .locals 1

    .prologue
    .line 454
    iget-boolean v0, p0, Ldfw;->C:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldfw;->D:Z

    if-eq v0, p1, :cond_1

    .line 455
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfw;->C:Z

    .line 456
    iput-boolean p1, p0, Ldfw;->D:Z

    .line 457
    invoke-direct {p0}, Ldfw;->e()V

    .line 458
    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0, p1, p2}, Ldfu;->a(ZZ)V

    .line 460
    :cond_1
    return-void
.end method

.method public final handleVideoControlsVisibilityEvent(Ldaa;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 548
    iget-object v1, p0, Ldfw;->b:Ldfu;

    iget-boolean v0, p1, Ldaa;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Ldfu;->a(Z)V

    .line 549
    return-void

    .line 548
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final handleVideoFullscreenEvent(Ldab;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 553
    iget-boolean v0, p1, Ldab;->a:Z

    iput-boolean v0, p0, Ldfw;->r:Z

    .line 554
    iget-object v0, p0, Ldfw;->i:Lhbw;

    if-eqz v0, :cond_0

    .line 555
    iget-boolean v0, p0, Ldfw;->C:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ldfw;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 556
    invoke-virtual {p0, v1}, Ldfw;->a(Z)V

    .line 561
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    iget-boolean v0, p0, Ldfw;->C:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Ldfw;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    iget-boolean v0, p0, Ldfw;->w:Z

    invoke-virtual {p0, v0, v1}, Ldfw;->a(ZZ)V

    goto :goto_0
.end method

.method public final handleVideoStageEvent(Ldac;)V
    .locals 4
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 493
    iget-object v0, p1, Ldac;->a:Lgol;

    invoke-virtual {v0}, Lgol;->a()Z

    move-result v0

    iput-boolean v0, p0, Ldfw;->F:Z

    .line 495
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->h:Lgol;

    if-ne v0, v1, :cond_1

    .line 496
    iget-object v0, p1, Ldac;->b:Lfrl;

    invoke-direct {p0, v0}, Ldfw;->a(Lfrl;)V

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->i:Lgol;

    if-ne v0, v1, :cond_2

    .line 499
    iget-boolean v0, p0, Ldfw;->q:Z

    if-nez v0, :cond_0

    .line 501
    iget-object v0, p1, Ldac;->b:Lfrl;

    invoke-direct {p0, v0}, Ldfw;->a(Lfrl;)V

    goto :goto_0

    .line 503
    :cond_2
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->d:Lgol;

    if-ne v0, v1, :cond_4

    iget-object v0, p1, Ldac;->d:Lfoy;

    if-eqz v0, :cond_4

    .line 504
    iget-object v1, p1, Ldac;->d:Lfoy;

    iget-boolean v0, v1, Lfoy;->U:Z

    if-eqz v0, :cond_3

    iget-object v0, v1, Lfoy;->V:Lhqz;

    :goto_1
    iget-object v2, v1, Lfoy;->W:Lfkg;

    iget-object v3, v1, Lfoy;->d:Ljava/lang/String;

    iget-object v1, v1, Lfoy;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v2, v3, v1}, Ldfw;->a(Lhqz;Lfkg;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 505
    :cond_4
    iget-object v0, p1, Ldac;->a:Lgol;

    sget-object v1, Lgol;->a:Lgol;

    if-ne v0, v1, :cond_0

    .line 506
    invoke-direct {p0}, Ldfw;->b()V

    goto :goto_0
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 12
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 526
    iget-wide v0, p1, Ldad;->a:J

    long-to-int v4, v0

    iget-object v0, p0, Ldfw;->d:Lhqz;

    if-eqz v0, :cond_b

    iget-object v0, p0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->b:Lhgl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->b:Lhgl;

    iget-wide v0, v0, Lhgl;->a:J

    int-to-long v2, v4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_3

    int-to-long v0, v4

    iget-object v2, p0, Ldfw;->d:Lhqz;

    iget-object v2, v2, Lhqz;->b:Lhgl;

    iget-wide v2, v2, Lhgl;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Ldfw;->r:Z

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Ldfw;->A:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfw;->A:Z

    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0}, Ldfu;->p_()V

    :cond_0
    :goto_1
    iget-object v0, p0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->a:Lhgm;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Ldfw;->e:Z

    if-nez v1, :cond_5

    iget-wide v2, v0, Lhgm;->a:J

    int-to-long v6, v4

    cmp-long v1, v2, v6

    if-gtz v1, :cond_5

    int-to-long v2, v4

    iget-wide v0, v0, Lhgm;->b:J

    cmp-long v0, v2, v0

    if-gez v0, :cond_5

    iget-boolean v0, p0, Ldfw;->B:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfw;->B:Z

    invoke-direct {p0}, Ldfw;->e()V

    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0}, Ldfu;->d()V

    :cond_1
    :goto_2
    iget-object v0, p0, Ldfw;->d:Lhqz;

    iget-object v0, v0, Lhqz;->c:[Lhbw;

    array-length v0, v0

    if-eqz v0, :cond_b

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    :goto_3
    iget-object v0, p0, Ldfw;->x:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    iget-object v0, p0, Ldfw;->x:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbw;

    iget-wide v6, v0, Lhbw;->b:J

    int-to-long v8, v4

    cmp-long v5, v6, v8

    if-gtz v5, :cond_18

    iget-wide v6, v0, Lhbw;->c:J

    int-to-long v8, v4

    cmp-long v5, v6, v8

    if-lez v5, :cond_18

    if-eqz v3, :cond_2

    iget-wide v6, v0, Lhbw;->b:J

    iget-wide v8, v3, Lhbw;->b:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_18

    :cond_2
    move-object v1, v0

    move v0, v2

    :goto_4
    add-int/lit8 v2, v2, 0x1

    move-object v3, v1

    move v1, v0

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Ldfw;->A:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldfw;->A:Z

    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0}, Ldfu;->c()V

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Ldfw;->a()V

    goto :goto_2

    :cond_6
    iget v0, p0, Ldfw;->h:I

    if-eq v1, v0, :cond_7

    iput v1, p0, Ldfw;->h:I

    iput-object v3, p0, Ldfw;->i:Lhbw;

    iget-object v0, p0, Ldfw;->i:Lhbw;

    if-eqz v0, :cond_7

    iget-object v0, p0, Ldfw;->b:Ldfu;

    iget-object v1, p0, Ldfw;->i:Lhbw;

    iget-object v1, v1, Lhbw;->e:Ljava/lang/String;

    iget-object v2, p0, Ldfw;->i:Lhbw;

    iget-object v2, v2, Lhbw;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ldfu;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Ldfw;->b:Ldfu;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ldfu;->c(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Ldfw;->i:Lhbw;

    if-eqz v0, :cond_7

    iget-object v0, v0, Lhbw;->h:Lhxf;

    new-instance v1, Ldfx;

    invoke-direct {v1, p0}, Ldfx;-><init>(Ldfw;)V

    invoke-direct {p0, v0, v1}, Ldfw;->a(Lhxf;Ldga;)Leue;

    move-result-object v0

    iput-object v0, p0, Ldfw;->u:Leue;

    :cond_7
    iget-object v0, p0, Ldfw;->i:Lhbw;

    if-nez v0, :cond_9

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldfw;->a(Z)V

    .line 527
    :cond_8
    :goto_5
    return-void

    .line 526
    :cond_9
    iget-object v0, p0, Ldfw;->f:[Z

    iget v1, p0, Ldfw;->h:I

    aget-boolean v1, v0, v1

    iget-object v0, p0, Ldfw;->g:[Z

    iget v2, p0, Ldfw;->h:I

    aget-boolean v0, v0, v2

    if-nez v0, :cond_a

    int-to-long v2, v4

    iget-object v0, p0, Ldfw;->i:Lhbw;

    iget-wide v6, v0, Lhbw;->d:J

    cmp-long v0, v2, v6

    if-gez v0, :cond_e

    :cond_a
    const/4 v0, 0x1

    :goto_6
    iput-boolean v0, p0, Ldfw;->w:Z

    if-eqz v1, :cond_f

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldfw;->a(Z)V

    :cond_b
    :goto_7
    iget-object v0, p0, Ldfw;->z:Ljava/util/List;

    if-eqz v0, :cond_8

    iget-object v0, p0, Ldfw;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_8
    iget-object v0, p0, Ldfw;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_11

    iget-object v0, p0, Ldfw;->z:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkf;

    iget-object v5, v0, Lfkf;->b:Ljava/util/List;

    if-nez v5, :cond_c

    iget-object v5, v0, Lfkf;->a:Lhjo;

    iget-object v5, v5, Lhjo;->c:[Lhjl;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    iput-object v5, v0, Lfkf;->b:Ljava/util/List;

    :cond_c
    iget-object v0, v0, Lfkf;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhjl;

    iget-wide v6, v0, Lhjl;->b:J

    iget-wide v8, v0, Lhjl;->c:J

    add-long/2addr v6, v8

    iget-wide v8, v0, Lhjl;->b:J

    int-to-long v10, v4

    cmp-long v8, v8, v10

    if-gtz v8, :cond_17

    int-to-long v8, v4

    cmp-long v6, v6, v8

    if-lez v6, :cond_17

    if-eqz v1, :cond_d

    iget-wide v6, v0, Lhjl;->b:J

    iget-wide v8, v1, Lhjl;->b:J

    cmp-long v6, v6, v8

    if-gez v6, :cond_17

    :cond_d
    move v1, v3

    :goto_a
    move v2, v1

    move-object v1, v0

    goto :goto_9

    :cond_e
    const/4 v0, 0x0

    goto :goto_6

    :cond_f
    invoke-direct {p0}, Ldfw;->d()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Ldfw;->w:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ldfw;->a(ZZ)V

    goto :goto_7

    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_11
    iget v0, p0, Ldfw;->j:I

    if-ne v2, v0, :cond_12

    iget-object v0, p0, Ldfw;->y:Lhjl;

    if-eq v1, v0, :cond_15

    :cond_12
    iput v2, p0, Ldfw;->j:I

    iput-object v1, p0, Ldfw;->y:Lhjl;

    iget v0, p0, Ldfw;->j:I

    if-ltz v0, :cond_15

    iget-object v0, p0, Ldfw;->z:Ljava/util/List;

    iget v1, p0, Ldfw;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfkf;

    invoke-virtual {v0}, Lfkf;->a()Lfmw;

    move-result-object v2

    iget-object v1, p0, Ldfw;->b:Ldfu;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ldfu;->d(Landroid/graphics/Bitmap;)V

    if-eqz v2, :cond_13

    invoke-virtual {v2}, Lfmw;->a()Lfnc;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-virtual {v2}, Lfmw;->a()Lfnc;

    move-result-object v1

    iget-object v1, v1, Lfnc;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13

    invoke-virtual {v2}, Lfmw;->a()Lfnc;

    move-result-object v1

    iget-object v1, v1, Lfnc;->b:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfnb;

    iget-object v1, v1, Lfnb;->a:Landroid/net/Uri;

    new-instance v3, Ldgb;

    invoke-direct {v3, p0}, Ldgb;-><init>(Ldfw;)V

    invoke-static {v3}, Leue;->a(Leuc;)Leue;

    move-result-object v3

    iput-object v3, p0, Ldfw;->v:Leue;

    iget-object v3, p0, Ldfw;->m:Leyp;

    iget-object v4, p0, Ldfw;->o:Landroid/os/Handler;

    iget-object v5, p0, Ldfw;->v:Leue;

    invoke-static {v4, v5}, Leuf;->a(Landroid/os/Handler;Leuc;)Leuf;

    move-result-object v4

    invoke-interface {v3, v1, v4}, Leyp;->a(Landroid/net/Uri;Leuc;)V

    :cond_13
    iget-object v1, p0, Ldfw;->b:Ldfu;

    iget-object v3, v2, Lfmw;->b:Ljava/lang/CharSequence;

    if-nez v3, :cond_14

    iget-object v3, v2, Lfmw;->a:Lhvz;

    iget-object v3, v3, Lhvz;->b:Lhgz;

    if-eqz v3, :cond_14

    iget-object v3, v2, Lfmw;->a:Lhvz;

    iget-object v3, v3, Lhvz;->b:Lhgz;

    invoke-static {v3}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v3

    iput-object v3, v2, Lfmw;->b:Ljava/lang/CharSequence;

    :cond_14
    iget-object v2, v2, Lfmw;->b:Ljava/lang/CharSequence;

    invoke-interface {v1, v2}, Ldfu;->c(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Ldfw;->a:Levn;

    new-instance v2, Ldai;

    iget-object v3, p0, Ldfw;->l:Ldgc;

    invoke-virtual {v3}, Ldgc;->a()Lfqg;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ldai;-><init>(Lfkf;Lfqg;)V

    invoke-virtual {v1, v2}, Levn;->d(Ljava/lang/Object;)V

    :cond_15
    iget v0, p0, Ldfw;->j:I

    if-gez v0, :cond_16

    iget-boolean v0, p0, Ldfw;->E:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    iput-boolean v0, p0, Ldfw;->E:Z

    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0}, Ldfu;->g()V

    invoke-direct {p0}, Ldfw;->e()V

    goto/16 :goto_5

    :cond_16
    iget-boolean v0, p0, Ldfw;->E:Z

    if-nez v0, :cond_8

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldfw;->E:Z

    invoke-direct {p0}, Ldfw;->e()V

    iget-object v0, p0, Ldfw;->b:Ldfu;

    invoke-interface {v0}, Ldfu;->f()V

    goto/16 :goto_5

    :cond_17
    move-object v0, v1

    move v1, v2

    goto/16 :goto_a

    :cond_18
    move v0, v1

    move-object v1, v3

    goto/16 :goto_4
.end method
