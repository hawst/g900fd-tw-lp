.class public final Lhgz;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhgz;


# instance fields
.field public b:[Lhwo;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54136
    const/4 v0, 0x0

    new-array v0, v0, [Lhgz;

    sput-object v0, Lhgz;->a:[Lhgz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54137
    invoke-direct {p0}, Lidf;-><init>()V

    .line 54140
    sget-object v0, Lhwo;->a:[Lhwo;

    iput-object v0, p0, Lhgz;->b:[Lhwo;

    .line 54143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhgz;->c:Z

    .line 54137
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 54195
    .line 54196
    iget-object v1, p0, Lhgz;->b:[Lhwo;

    if-eqz v1, :cond_1

    .line 54197
    iget-object v2, p0, Lhgz;->b:[Lhwo;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 54198
    if-eqz v4, :cond_0

    .line 54199
    const/4 v5, 0x1

    .line 54200
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 54197
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 54204
    :cond_1
    iget-boolean v1, p0, Lhgz;->c:Z

    if-eqz v1, :cond_2

    .line 54205
    const/4 v1, 0x2

    iget-boolean v2, p0, Lhgz;->c:Z

    .line 54206
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 54208
    :cond_2
    iget-object v1, p0, Lhgz;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 54209
    iput v0, p0, Lhgz;->J:I

    .line 54210
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhgz;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhgz;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhgz;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhgz;->b:[Lhwo;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhwo;

    iget-object v3, p0, Lhgz;->b:[Lhwo;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhgz;->b:[Lhwo;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhgz;->b:[Lhwo;

    :goto_2
    iget-object v2, p0, Lhgz;->b:[Lhwo;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhgz;->b:[Lhwo;

    new-instance v3, Lhwo;

    invoke-direct {v3}, Lhwo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgz;->b:[Lhwo;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhgz;->b:[Lhwo;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhgz;->b:[Lhwo;

    new-instance v3, Lhwo;

    invoke-direct {v3}, Lhwo;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhgz;->b:[Lhwo;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhgz;->c:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    .prologue
    .line 54179
    iget-object v0, p0, Lhgz;->b:[Lhwo;

    if-eqz v0, :cond_1

    .line 54180
    iget-object v1, p0, Lhgz;->b:[Lhwo;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 54181
    if-eqz v3, :cond_0

    .line 54182
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 54180
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54186
    :cond_1
    iget-boolean v0, p0, Lhgz;->c:Z

    if-eqz v0, :cond_2

    .line 54187
    const/4 v0, 0x2

    iget-boolean v1, p0, Lhgz;->c:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 54189
    :cond_2
    iget-object v0, p0, Lhgz;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 54191
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 54155
    if-ne p1, p0, :cond_1

    .line 54160
    :cond_0
    :goto_0
    return v0

    .line 54156
    :cond_1
    instance-of v2, p1, Lhgz;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 54157
    :cond_2
    check-cast p1, Lhgz;

    .line 54158
    iget-object v2, p0, Lhgz;->b:[Lhwo;

    iget-object v3, p1, Lhgz;->b:[Lhwo;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhgz;->c:Z

    iget-boolean v3, p1, Lhgz;->c:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhgz;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhgz;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 54160
    goto :goto_0

    .line 54158
    :cond_4
    iget-object v2, p0, Lhgz;->I:Ljava/util/List;

    iget-object v3, p1, Lhgz;->I:Ljava/util/List;

    .line 54160
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 54164
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 54166
    iget-object v2, p0, Lhgz;->b:[Lhwo;

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 54172
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-boolean v0, p0, Lhgz;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    .line 54173
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhgz;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_1
    add-int/2addr v0, v1

    .line 54174
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 54168
    :goto_2
    iget-object v3, p0, Lhgz;->b:[Lhwo;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 54169
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhgz;->b:[Lhwo;

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    move v2, v1

    :goto_3
    add-int/2addr v2, v3

    .line 54168
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 54169
    :cond_2
    iget-object v2, p0, Lhgz;->b:[Lhwo;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhwo;->hashCode()I

    move-result v2

    goto :goto_3

    .line 54172
    :cond_3
    const/4 v0, 0x2

    goto :goto_0

    .line 54173
    :cond_4
    iget-object v1, p0, Lhgz;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
