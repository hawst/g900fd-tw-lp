.class public final Ldbz;
.super Lded;
.source "SourceFile"

# interfaces
.implements Ldbm;
.implements Lddx;


# instance fields
.field private final a:Landroid/widget/TextView;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private final e:Landroid/widget/TextView;

.field private f:Ldbn;

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private final n:Landroid/widget/ImageView;

.field private final o:Landroid/util/DisplayMetrics;

.field private final p:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 66
    invoke-virtual {p0}, Ldbz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Ldbz;->o:Landroid/util/DisplayMetrics;

    .line 68
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 69
    const v1, 0x7f040058

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 71
    const v0, 0x7f08018a

    invoke-virtual {p0, v0}, Ldbz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldbz;->a:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f080189

    invoke-virtual {p0, v0}, Ldbz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldbz;->p:Landroid/view/View;

    .line 73
    const v0, 0x7f08018b

    invoke-virtual {p0, v0}, Ldbz;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldbz;->b:Landroid/view/View;

    .line 74
    iget-object v0, p0, Ldbz;->b:Landroid/view/View;

    const v1, 0x7f08018c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldbz;->c:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Ldbz;->b:Landroid/view/View;

    const v1, 0x7f08018d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldbz;->n:Landroid/widget/ImageView;

    .line 76
    iget-object v0, p0, Ldbz;->p:Landroid/view/View;

    const v1, 0x7f0800a4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldbz;->d:Landroid/widget/ImageView;

    .line 77
    iget-object v0, p0, Ldbz;->p:Landroid/view/View;

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldbz;->e:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Ldbz;->b:Landroid/view/View;

    .line 80
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 81
    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v1, p2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 83
    new-instance v0, Ldca;

    invoke-direct {v0, p0}, Ldca;-><init>(Ldbz;)V

    .line 89
    iget-object v1, p0, Ldbz;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Ldbz;->b:Landroid/view/View;

    new-instance v1, Ldcb;

    invoke-direct {v1, p0}, Ldcb;-><init>(Ldbz;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 99
    new-instance v0, Ldcc;

    invoke-direct {v0, p0}, Ldcc;-><init>(Ldbz;)V

    .line 105
    iget-object v1, p0, Ldbz;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v1, p0, Ldbz;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    invoke-virtual {p0}, Ldbz;->a()V

    .line 109
    return-void
.end method

.method static synthetic a(Ldbz;)V
    .locals 3

    .prologue
    .line 27
    iget v0, p0, Ldbz;->m:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldbz;->f:Ldbn;

    iget v1, p0, Ldbz;->k:I

    iget v2, p0, Ldbz;->l:I

    invoke-interface {v0, v1, v2}, Ldbn;->a(II)V

    :cond_0
    return-void
.end method

.method static synthetic a(Ldbz;Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 27
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ldbz;->k:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Ldbz;->l:I

    :cond_0
    return-void
.end method

.method static synthetic b(Ldbz;)Ldbn;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Ldbz;->f:Ldbn;

    return-object v0
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 121
    iget-boolean v0, p0, Ldbz;->g:Z

    if-eqz v0, :cond_0

    const v0, 0x7f090127

    .line 122
    :goto_0
    iget-object v1, p0, Ldbz;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    iget-object v1, p0, Ldbz;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Ldbz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, ""

    aput-object v4, v3, v5

    const-string v4, ""

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    :goto_1
    return-void

    .line 121
    :cond_0
    const v0, 0x7f090126

    goto :goto_0

    .line 126
    :cond_1
    iget-object v1, p0, Ldbz;->a:Landroid/widget/TextView;

    .line 127
    invoke-virtual {p0}, Ldbz;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, " \u00b7 "

    aput-object v4, v3, v5

    iget-object v4, p0, Ldbz;->i:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private d()V
    .locals 7

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 132
    iget-object v3, p0, Ldbz;->p:Landroid/view/View;

    iget-boolean v0, p0, Ldbz;->g:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Ldbz;->e:Landroid/widget/TextView;

    iget-object v3, p0, Ldbz;->h:Ljava/lang/String;

    .line 134
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Ldbz;->getWidth()I

    move-result v3

    const/4 v4, 0x1

    const/high16 v5, 0x43fa0000    # 500.0f

    iget-object v6, p0, Ldbz;->o:Landroid/util/DisplayMetrics;

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    float-to-int v4, v4

    if-ge v3, v4, :cond_1

    :cond_0
    move v2, v1

    .line 133
    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    return-void

    :cond_2
    move v0, v2

    .line 132
    goto :goto_0
.end method

.method private e()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 151
    iget-boolean v0, p0, Ldbz;->g:Z

    if-nez v0, :cond_0

    iget v0, p0, Ldbz;->m:I

    if-ne v0, v3, :cond_1

    .line 152
    :cond_0
    iget-object v0, p0, Ldbz;->b:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 171
    :goto_0
    return-void

    .line 154
    :cond_1
    iget v0, p0, Ldbz;->m:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 155
    iget v0, p0, Ldbz;->j:I

    if-gtz v0, :cond_4

    .line 156
    const/4 v0, 0x3

    iput v0, p0, Ldbz;->m:I

    .line 157
    iget-object v0, p0, Ldbz;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Ldbz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09012a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-boolean v0, p0, Ldbz;->g:Z

    if-nez v0, :cond_2

    .line 159
    iget-object v0, p0, Ldbz;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 161
    :cond_2
    iget-object v0, p0, Ldbz;->f:Ldbn;

    if-eqz v0, :cond_3

    .line 162
    iget-object v0, p0, Ldbz;->f:Ldbn;

    invoke-interface {v0}, Ldbn;->b()V

    .line 169
    :cond_3
    :goto_1
    iget-object v0, p0, Ldbz;->b:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 165
    :cond_4
    iget-object v0, p0, Ldbz;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Ldbz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090129

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Ldbz;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Ldbz;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 180
    iput-object v2, p0, Ldbz;->h:Ljava/lang/String;

    .line 181
    invoke-virtual {p0, v3}, Ldbz;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Ldbz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 183
    iget-object v0, p0, Ldbz;->d:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Ldbz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, p0, Ldbz;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 186
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 214
    sub-int v0, p2, p1

    div-int/lit16 v0, v0, 0x3e8

    invoke-static {v0}, La;->c(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ldbz;->i:Ljava/lang/String;

    .line 215
    div-int/lit16 v0, p1, 0x3e8

    rsub-int/lit8 v0, v0, 0x5

    iput v0, p0, Ldbz;->j:I

    .line 216
    invoke-direct {p0}, Ldbz;->e()V

    .line 217
    invoke-direct {p0}, Ldbz;->c()V

    .line 218
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Ldbz;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 240
    iget-object v1, p0, Ldbz;->d:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Ldbz;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    iget-object v0, p0, Ldbz;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Ldbz;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 246
    :cond_0
    return-void

    .line 240
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final a(Ldbn;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Ldbz;->f:Ldbn;

    .line 176
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 208
    iput-object p1, p0, Ldbz;->h:Ljava/lang/String;

    .line 209
    iget-object v0, p0, Ldbz;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    return-void
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Ldbz;->i:Ljava/lang/String;

    .line 192
    iput v1, p0, Ldbz;->k:I

    .line 193
    iput v1, p0, Ldbz;->l:I

    .line 194
    if-eqz p2, :cond_0

    .line 195
    const/4 v0, 0x5

    iput v0, p0, Ldbz;->j:I

    .line 196
    iget-object v0, p0, Ldbz;->n:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 197
    const/4 v0, 0x2

    iput v0, p0, Ldbz;->m:I

    .line 201
    :goto_0
    invoke-direct {p0}, Ldbz;->c()V

    .line 202
    invoke-direct {p0}, Ldbz;->e()V

    .line 203
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldbz;->setVisibility(I)V

    .line 204
    return-void

    .line 199
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Ldbz;->m:I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public final c_(Z)V
    .locals 0

    .prologue
    .line 222
    iput-boolean p1, p0, Ldbz;->g:Z

    .line 223
    invoke-direct {p0}, Ldbz;->e()V

    .line 224
    invoke-direct {p0}, Ldbz;->c()V

    .line 225
    invoke-direct {p0}, Ldbz;->d()V

    .line 226
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0}, Ldbz;->d()V

    .line 231
    return-void
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 113
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method
