.class public final Lhdl;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhdl;


# instance fields
.field private b:Lhoj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31472
    const/4 v0, 0x0

    new-array v0, v0, [Lhdl;

    sput-object v0, Lhdl;->a:[Lhdl;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31473
    invoke-direct {p0}, Lidf;-><init>()V

    .line 31476
    const/4 v0, 0x0

    iput-object v0, p0, Lhdl;->b:Lhoj;

    .line 31473
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 31513
    const/4 v0, 0x0

    .line 31514
    iget-object v1, p0, Lhdl;->b:Lhoj;

    if-eqz v1, :cond_0

    .line 31515
    const v0, 0x31a2ee9

    iget-object v1, p0, Lhdl;->b:Lhoj;

    .line 31516
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 31518
    :cond_0
    iget-object v1, p0, Lhdl;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 31519
    iput v0, p0, Lhdl;->J:I

    .line 31520
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 31469
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhdl;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhdl;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhdl;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhdl;->b:Lhoj;

    if-nez v0, :cond_2

    new-instance v0, Lhoj;

    invoke-direct {v0}, Lhoj;-><init>()V

    iput-object v0, p0, Lhdl;->b:Lhoj;

    :cond_2
    iget-object v0, p0, Lhdl;->b:Lhoj;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18d1774a -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 31504
    iget-object v0, p0, Lhdl;->b:Lhoj;

    if-eqz v0, :cond_0

    .line 31505
    const v0, 0x31a2ee9

    iget-object v1, p0, Lhdl;->b:Lhoj;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 31507
    :cond_0
    iget-object v0, p0, Lhdl;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 31509
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 31487
    if-ne p1, p0, :cond_1

    .line 31491
    :cond_0
    :goto_0
    return v0

    .line 31488
    :cond_1
    instance-of v2, p1, Lhdl;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 31489
    :cond_2
    check-cast p1, Lhdl;

    .line 31490
    iget-object v2, p0, Lhdl;->b:Lhoj;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhdl;->b:Lhoj;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhdl;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhdl;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 31491
    goto :goto_0

    .line 31490
    :cond_4
    iget-object v2, p0, Lhdl;->b:Lhoj;

    iget-object v3, p1, Lhdl;->b:Lhoj;

    invoke-virtual {v2, v3}, Lhoj;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhdl;->I:Ljava/util/List;

    iget-object v3, p1, Lhdl;->I:Ljava/util/List;

    .line 31491
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 31495
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 31497
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhdl;->b:Lhoj;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 31498
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhdl;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 31499
    return v0

    .line 31497
    :cond_0
    iget-object v0, p0, Lhdl;->b:Lhoj;

    invoke-virtual {v0}, Lhoj;->hashCode()I

    move-result v0

    goto :goto_0

    .line 31498
    :cond_1
    iget-object v1, p0, Lhdl;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
