.class final Limv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liqk;


# instance fields
.field private final a:[B

.field private b:Z

.field private synthetic c:Limt;


# direct methods
.method constructor <init>(Limt;)V
    .locals 1

    .prologue
    .line 328
    iput-object p1, p0, Limv;->c:Limt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    const/16 v0, 0x12

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Limv;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0xdt
        0xat
    .end array-data
.end method

.method private a(J)V
    .locals 7

    .prologue
    .line 364
    const/16 v0, 0x10

    .line 366
    :cond_0
    iget-object v1, p0, Limv;->a:[B

    add-int/lit8 v0, v0, -0x1

    invoke-static {}, Limt;->e()[B

    move-result-object v2

    const-wide/16 v4, 0xf

    and-long/2addr v4, p1

    long-to-int v3, v4

    aget-byte v2, v2, v3

    aput-byte v2, v1, v0

    .line 367
    const/4 v1, 0x4

    ushr-long/2addr p1, v1

    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 368
    iget-object v1, p0, Limv;->c:Limt;

    invoke-static {v1}, Limt;->a(Limt;)Lipt;

    move-result-object v1

    iget-object v2, p0, Limv;->a:[B

    iget-object v3, p0, Limv;->a:[B

    rsub-int/lit8 v3, v0, 0x12

    invoke-interface {v1, v2, v0, v3}, Lipt;->c([BII)Lipt;

    .line 369
    return-void
.end method


# virtual methods
.method public final a()Liqm;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Limv;->c:Limt;

    invoke-static {v0}, Limt;->a(Limt;)Lipt;

    move-result-object v0

    invoke-interface {v0}, Lipt;->a()Liqm;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lipq;J)V
    .locals 2

    .prologue
    .line 339
    iget-boolean v0, p0, Limv;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    .line 345
    :goto_0
    return-void

    .line 342
    :cond_1
    invoke-direct {p0, p2, p3}, Limv;->a(J)V

    .line 343
    iget-object v0, p0, Limv;->c:Limt;

    invoke-static {v0}, Limt;->a(Limt;)Lipt;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lipt;->a(Lipq;J)V

    .line 344
    iget-object v0, p0, Limv;->c:Limt;

    invoke-static {v0}, Limt;->a(Limt;)Lipt;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lipt;->b(Ljava/lang/String;)Lipt;

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 348
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Limv;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 350
    :goto_0
    monitor-exit p0

    return-void

    .line 349
    :cond_0
    :try_start_1
    iget-object v0, p0, Limv;->c:Limt;

    invoke-static {v0}, Limt;->a(Limt;)Lipt;

    move-result-object v0

    invoke-interface {v0}, Lipt;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized close()V
    .locals 2

    .prologue
    .line 353
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Limv;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 357
    :goto_0
    monitor-exit p0

    return-void

    .line 354
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Limv;->b:Z

    .line 355
    iget-object v0, p0, Limv;->c:Limt;

    invoke-static {v0}, Limt;->a(Limt;)Lipt;

    move-result-object v0

    invoke-static {}, Limt;->d()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lipt;->b([B)Lipt;

    .line 356
    iget-object v0, p0, Limv;->c:Limt;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Limt;->a(Limt;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 353
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
