.class final Lgea;
.super Landroid/os/HandlerThread;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Runnable;

.field b:Landroid/os/Handler;

.field c:I

.field volatile d:I

.field private synthetic e:Lgdu;


# direct methods
.method public constructor <init>(Lgdu;)V
    .locals 1

    .prologue
    .line 1011
    iput-object p1, p0, Lgea;->e:Lgdu;

    .line 1012
    const-string v0, "YouTubePlayer.ProgressDetector"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 1013
    new-instance v0, Lgeb;

    invoke-direct {v0, p0, p1}, Lgeb;-><init>(Lgea;Lgdu;)V

    iput-object v0, p0, Lgea;->a:Ljava/lang/Runnable;

    .line 1019
    return-void
.end method

.method static synthetic a(Lgea;)V
    .locals 4

    .prologue
    .line 998
    iget-object v0, p0, Lgea;->e:Lgdu;

    invoke-static {v0}, Lgdu;->w(Lgdu;)Ljava/util/concurrent/atomic/AtomicReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgds;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lgea;->e:Lgdu;

    invoke-static {v1}, Lgdu;->t(Lgdu;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Lgds;->e()I

    move-result v0

    iget v1, p0, Lgea;->c:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lgea;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgea;->b:Landroid/os/Handler;

    iget-object v1, p0, Lgea;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 1028
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lgea;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lgea;->d:I

    .line 1029
    iget v0, p0, Lgea;->d:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Retrying MediaPlayer error [retry="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", max=3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1030
    monitor-exit p0

    return-void

    .line 1028
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1043
    const/4 v0, 0x0

    iput v0, p0, Lgea;->d:I

    .line 1044
    iget-object v0, p0, Lgea;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1045
    return-void
.end method

.method public final quit()Z
    .locals 2

    .prologue
    .line 1059
    iget-object v0, p0, Lgea;->b:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1060
    invoke-super {p0}, Landroid/os/HandlerThread;->quit()Z

    move-result v0

    return v0
.end method

.method public final start()V
    .locals 2

    .prologue
    .line 1023
    invoke-super {p0}, Landroid/os/HandlerThread;->start()V

    .line 1024
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lgea;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lgea;->b:Landroid/os/Handler;

    .line 1025
    return-void
.end method
