.class public final Ldji;
.super Lcvp;
.source "SourceFile"


# instance fields
.field private final a:Ldjf;

.field private final b:Ldjj;

.field private c:Z


# direct methods
.method public constructor <init>(Lgds;Ldjf;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcvp;-><init>(Lgds;)V

    .line 46
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldjf;

    iput-object v0, p0, Ldji;->a:Ldjf;

    .line 47
    new-instance v0, Ldjj;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Ldjj;-><init>(Ldji;Landroid/os/Looper;)V

    iput-object v0, p0, Ldji;->b:Ldjj;

    .line 48
    return-void
.end method

.method static synthetic a(Ldji;I)V
    .locals 0

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Ldji;->d(I)V

    return-void
.end method

.method private i()V
    .locals 4

    .prologue
    .line 70
    iget-boolean v0, p0, Ldji;->c:Z

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Ldji;->b:Ldjj;

    const/4 v1, 0x1

    const/16 v2, 0x64

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Ldjj;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 73
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Ldji;->a:Ldjf;

    invoke-interface {v0, p2}, Ldjf;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-super {p0, p1, v0, p3}, Lcvp;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldji;->c:Z

    .line 55
    invoke-direct {p0}, Ldji;->i()V

    .line 56
    return-void
.end method

.method public final a(Lgdt;)V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcvp;->a(Lgdt;)V

    .line 61
    invoke-direct {p0}, Ldji;->i()V

    .line 62
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
