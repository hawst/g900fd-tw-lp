.class final Laed;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lddu;


# instance fields
.field private synthetic a:Laea;


# direct methods
.method constructor <init>(Laea;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Laed;->a:Laea;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->e()V

    .line 446
    :cond_0
    return-void
.end method

.method public final a(D)V
    .locals 7

    .prologue
    .line 450
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 451
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 452
    iget-object v2, p0, Laed;->a:Laea;

    invoke-static {v2}, Laea;->b(Laea;)Ldbt;

    move-result-object v2

    iget-object v3, p0, Laed;->a:Laea;

    invoke-static {v3}, Laea;->c(Laea;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L    # 20000.0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    sub-int v0, v3, v0

    invoke-interface {v2, v0}, Ldbt;->a(I)V

    .line 454
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    invoke-interface {v0, p1}, Ldbt;->a(I)V

    .line 496
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->a(Laea;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_1

    .line 464
    :cond_0
    :goto_0
    return-void

    .line 461
    :cond_1
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 462
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->a()V

    goto :goto_0
.end method

.method public final b(D)V
    .locals 7

    .prologue
    .line 478
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 479
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 480
    iget-object v2, p0, Laed;->a:Laea;

    invoke-static {v2}, Laea;->b(Laea;)Ldbt;

    move-result-object v2

    iget-object v3, p0, Laed;->a:Laea;

    invoke-static {v3}, Laea;->c(Laea;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L    # 20000.0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    add-int/2addr v0, v3

    invoke-interface {v2, v0}, Ldbt;->a(I)V

    .line 482
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 468
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->a(Laea;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->c:Ldbs;

    if-ne v0, v1, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 471
    :cond_1
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->b()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 487
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->d()V

    .line 489
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Laed;->a:Laea;

    invoke-static {v0}, Laea;->b(Laea;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->f()V

    .line 503
    :cond_0
    return-void
.end method
