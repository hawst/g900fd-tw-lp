.class public final Lcxg;
.super Lcwg;
.source "SourceFile"


# instance fields
.field private final e:Lgix;

.field private final f:Lcub;

.field private final g:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgix;Lcub;Landroid/content/SharedPreferences;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcwg;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lcxg;->e:Lgix;

    .line 42
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcub;

    iput-object v0, p0, Lcxg;->f:Lcub;

    .line 43
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcxg;->g:Landroid/content/SharedPreferences;

    .line 45
    invoke-virtual {p0}, Lcxg;->a()V

    .line 46
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Lcxg;->e:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    .line 94
    iget-object v1, v0, Lgit;->c:Ljava/lang/String;

    invoke-static {p1, v1}, Lcxg;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    iget-object v2, p0, Lcxg;->g:Landroid/content/SharedPreferences;

    invoke-interface {v2, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    invoke-static {v0}, Lctv;->a(Lgit;)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-static {p1, v0}, Lcxg;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    iget-object v2, p0, Lcxg;->g:Landroid/content/SharedPreferences;

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3}, Lfaq;->a(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 101
    :cond_0
    return-object v1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 114
    iget-object v1, p0, Lcxg;->e:Lgix;

    invoke-interface {v1}, Lgix;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    invoke-direct {p0, p1}, Lcxg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    iget-object v2, p0, Lcxg;->g:Landroid/content/SharedPreferences;

    invoke-interface {v2, v1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 118
    :cond_0
    return v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 126
    iget-object v0, p0, Lcxg;->e:Lgix;

    invoke-interface {v0}, Lgix;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcxg;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 128
    invoke-direct {p0, p1}, Lcxg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 129
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 131
    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    .prologue
    .line 138
    const-string v0, "playability_adult_confirmations"

    invoke-direct {p0, v0}, Lcxg;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcxg;->b:Z

    .line 139
    const-string v0, "playability_content_confirmations"

    .line 140
    invoke-direct {p0, v0}, Lcxg;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcxg;->c:Z

    .line 141
    return-void
.end method

.method protected final a(Lflo;)V
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p1}, Lflo;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    const-string v0, "playability_adult_confirmations"

    invoke-direct {p0, v0}, Lcxg;->c(Ljava/lang/String;)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1}, Lflo;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "playability_content_confirmations"

    invoke-direct {p0, v0}, Lcxg;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Lflo;Lcwi;)V
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcxg;->d:Lcwj;

    if-nez v0, :cond_0

    .line 78
    invoke-static {p1}, Lcxg;->b(Lflo;)Lczb;

    move-result-object v0

    invoke-interface {p2, v0}, Lcwi;->a(Lczb;)V

    .line 85
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcxg;->f:Lcub;

    iget-object v1, p0, Lcxg;->d:Lcwj;

    .line 82
    iget-object v1, v1, Lcwj;->a:Landroid/app/Activity;

    new-instance v2, Lcxh;

    invoke-direct {v2, p0, p1, p2}, Lcxh;-><init>(Lcxg;Lflo;Lcwi;)V

    .line 81
    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    goto :goto_0
.end method

.method public final onSignIn(Lfcb;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcxg;->a()V

    .line 51
    return-void
.end method

.method public final onSignOut(Lfcc;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lcxg;->a()V

    .line 56
    return-void
.end method
