.class public final Lhay;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhaz;

.field public b:[Lhaz;

.field private c:Lhgz;

.field private d:I

.field private e:[Lhaz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13689
    invoke-direct {p0}, Lidf;-><init>()V

    .line 14020
    sget-object v0, Lhaz;->a:[Lhaz;

    iput-object v0, p0, Lhay;->a:[Lhaz;

    .line 14023
    const/4 v0, 0x0

    iput-object v0, p0, Lhay;->c:Lhgz;

    .line 14026
    const/4 v0, 0x0

    iput v0, p0, Lhay;->d:I

    .line 14029
    sget-object v0, Lhaz;->a:[Lhaz;

    iput-object v0, p0, Lhay;->b:[Lhaz;

    .line 14032
    sget-object v0, Lhaz;->a:[Lhaz;

    iput-object v0, p0, Lhay;->e:[Lhaz;

    .line 13689
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 14120
    .line 14121
    iget-object v0, p0, Lhay;->a:[Lhaz;

    if-eqz v0, :cond_1

    .line 14122
    iget-object v3, p0, Lhay;->a:[Lhaz;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 14123
    if-eqz v5, :cond_0

    .line 14124
    const/4 v6, 0x1

    .line 14125
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 14122
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 14129
    :cond_2
    iget-object v2, p0, Lhay;->c:Lhgz;

    if-eqz v2, :cond_3

    .line 14130
    const/4 v2, 0x2

    iget-object v3, p0, Lhay;->c:Lhgz;

    .line 14131
    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    .line 14133
    :cond_3
    iget v2, p0, Lhay;->d:I

    if-eqz v2, :cond_4

    .line 14134
    const/4 v2, 0x3

    iget v3, p0, Lhay;->d:I

    .line 14135
    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 14137
    :cond_4
    iget-object v2, p0, Lhay;->b:[Lhaz;

    if-eqz v2, :cond_6

    .line 14138
    iget-object v3, p0, Lhay;->b:[Lhaz;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 14139
    if-eqz v5, :cond_5

    .line 14140
    const/4 v6, 0x4

    .line 14141
    invoke-static {v6, v5}, Lidd;->b(ILidh;)I

    move-result v5

    add-int/2addr v0, v5

    .line 14138
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 14145
    :cond_6
    iget-object v2, p0, Lhay;->e:[Lhaz;

    if-eqz v2, :cond_8

    .line 14146
    iget-object v2, p0, Lhay;->e:[Lhaz;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 14147
    if-eqz v4, :cond_7

    .line 14148
    const/4 v5, 0x6

    .line 14149
    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    .line 14146
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 14153
    :cond_8
    iget-object v1, p0, Lhay;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 14154
    iput v0, p0, Lhay;->J:I

    .line 14155
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 13685
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhay;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhay;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhay;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhay;->a:[Lhaz;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhaz;

    iget-object v3, p0, Lhay;->a:[Lhaz;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhay;->a:[Lhaz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhay;->a:[Lhaz;

    :goto_2
    iget-object v2, p0, Lhay;->a:[Lhaz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhay;->a:[Lhaz;

    new-instance v3, Lhaz;

    invoke-direct {v3}, Lhaz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhay;->a:[Lhaz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhay;->a:[Lhaz;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhay;->a:[Lhaz;

    new-instance v3, Lhaz;

    invoke-direct {v3}, Lhaz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhay;->a:[Lhaz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhay;->c:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhay;->c:Lhgz;

    :cond_5
    iget-object v0, p0, Lhay;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhay;->d:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhay;->b:[Lhaz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lhaz;

    iget-object v3, p0, Lhay;->b:[Lhaz;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lhay;->b:[Lhaz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    iput-object v2, p0, Lhay;->b:[Lhaz;

    :goto_4
    iget-object v2, p0, Lhay;->b:[Lhaz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lhay;->b:[Lhaz;

    new-instance v3, Lhaz;

    invoke-direct {v3}, Lhaz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhay;->b:[Lhaz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lhay;->b:[Lhaz;

    array-length v0, v0

    goto :goto_3

    :cond_8
    iget-object v2, p0, Lhay;->b:[Lhaz;

    new-instance v3, Lhaz;

    invoke-direct {v3}, Lhaz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhay;->b:[Lhaz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhay;->e:[Lhaz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lhaz;

    iget-object v3, p0, Lhay;->e:[Lhaz;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lhay;->e:[Lhaz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    iput-object v2, p0, Lhay;->e:[Lhaz;

    :goto_6
    iget-object v2, p0, Lhay;->e:[Lhaz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_b

    iget-object v2, p0, Lhay;->e:[Lhaz;

    new-instance v3, Lhaz;

    invoke-direct {v3}, Lhaz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhay;->e:[Lhaz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lhay;->e:[Lhaz;

    array-length v0, v0

    goto :goto_5

    :cond_b
    iget-object v2, p0, Lhay;->e:[Lhaz;

    new-instance v3, Lhaz;

    invoke-direct {v3}, Lhaz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhay;->e:[Lhaz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 14087
    iget-object v1, p0, Lhay;->a:[Lhaz;

    if-eqz v1, :cond_1

    .line 14088
    iget-object v2, p0, Lhay;->a:[Lhaz;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 14089
    if-eqz v4, :cond_0

    .line 14090
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 14088
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 14094
    :cond_1
    iget-object v1, p0, Lhay;->c:Lhgz;

    if-eqz v1, :cond_2

    .line 14095
    const/4 v1, 0x2

    iget-object v2, p0, Lhay;->c:Lhgz;

    invoke-virtual {p1, v1, v2}, Lidd;->a(ILidh;)V

    .line 14097
    :cond_2
    iget v1, p0, Lhay;->d:I

    if-eqz v1, :cond_3

    .line 14098
    const/4 v1, 0x3

    iget v2, p0, Lhay;->d:I

    invoke-virtual {p1, v1, v2}, Lidd;->a(II)V

    .line 14100
    :cond_3
    iget-object v1, p0, Lhay;->b:[Lhaz;

    if-eqz v1, :cond_5

    .line 14101
    iget-object v2, p0, Lhay;->b:[Lhaz;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    aget-object v4, v2, v1

    .line 14102
    if-eqz v4, :cond_4

    .line 14103
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Lidd;->a(ILidh;)V

    .line 14101
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 14107
    :cond_5
    iget-object v1, p0, Lhay;->e:[Lhaz;

    if-eqz v1, :cond_7

    .line 14108
    iget-object v1, p0, Lhay;->e:[Lhaz;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    .line 14109
    if-eqz v3, :cond_6

    .line 14110
    const/4 v4, 0x6

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    .line 14108
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14114
    :cond_7
    iget-object v0, p0, Lhay;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 14116
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 14047
    if-ne p1, p0, :cond_1

    .line 14055
    :cond_0
    :goto_0
    return v0

    .line 14048
    :cond_1
    instance-of v2, p1, Lhay;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 14049
    :cond_2
    check-cast p1, Lhay;

    .line 14050
    iget-object v2, p0, Lhay;->a:[Lhaz;

    iget-object v3, p1, Lhay;->a:[Lhaz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhay;->c:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhay;->c:Lhgz;

    if-nez v2, :cond_3

    .line 14051
    :goto_1
    iget v2, p0, Lhay;->d:I

    iget v3, p1, Lhay;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhay;->b:[Lhaz;

    iget-object v3, p1, Lhay;->b:[Lhaz;

    .line 14053
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhay;->e:[Lhaz;

    iget-object v3, p1, Lhay;->e:[Lhaz;

    .line 14054
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhay;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhay;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 14055
    goto :goto_0

    .line 14050
    :cond_4
    iget-object v2, p0, Lhay;->c:Lhgz;

    iget-object v3, p1, Lhay;->c:Lhgz;

    .line 14051
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    .line 14054
    :cond_5
    iget-object v2, p0, Lhay;->I:Ljava/util/List;

    iget-object v3, p1, Lhay;->I:Ljava/util/List;

    .line 14055
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 14059
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 14061
    iget-object v2, p0, Lhay;->a:[Lhaz;

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    .line 14067
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhay;->c:Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 14068
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhay;->d:I

    add-int/2addr v0, v2

    .line 14069
    iget-object v2, p0, Lhay;->b:[Lhaz;

    if-nez v2, :cond_6

    mul-int/lit8 v2, v0, 0x1f

    .line 14075
    :cond_1
    iget-object v0, p0, Lhay;->e:[Lhaz;

    if-nez v0, :cond_8

    mul-int/lit8 v2, v2, 0x1f

    .line 14081
    :cond_2
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhay;->I:Ljava/util/List;

    if-nez v2, :cond_a

    :goto_1
    add-int/2addr v0, v1

    .line 14082
    return v0

    :cond_3
    move v2, v0

    move v0, v1

    .line 14063
    :goto_2
    iget-object v3, p0, Lhay;->a:[Lhaz;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 14064
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhay;->a:[Lhaz;

    aget-object v2, v2, v0

    if-nez v2, :cond_4

    move v2, v1

    :goto_3
    add-int/2addr v2, v3

    .line 14063
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14064
    :cond_4
    iget-object v2, p0, Lhay;->a:[Lhaz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhaz;->hashCode()I

    move-result v2

    goto :goto_3

    .line 14067
    :cond_5
    iget-object v0, p0, Lhay;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_6
    move v2, v0

    move v0, v1

    .line 14071
    :goto_4
    iget-object v3, p0, Lhay;->b:[Lhaz;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 14072
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhay;->b:[Lhaz;

    aget-object v2, v2, v0

    if-nez v2, :cond_7

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    .line 14071
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 14072
    :cond_7
    iget-object v2, p0, Lhay;->b:[Lhaz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhaz;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_8
    move v0, v1

    .line 14077
    :goto_6
    iget-object v3, p0, Lhay;->e:[Lhaz;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 14078
    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhay;->e:[Lhaz;

    aget-object v2, v2, v0

    if-nez v2, :cond_9

    move v2, v1

    :goto_7
    add-int/2addr v2, v3

    .line 14077
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 14078
    :cond_9
    iget-object v2, p0, Lhay;->e:[Lhaz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhaz;->hashCode()I

    move-result v2

    goto :goto_7

    .line 14081
    :cond_a
    iget-object v1, p0, Lhay;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
