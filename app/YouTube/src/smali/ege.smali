.class public final Lege;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lefb;


# instance fields
.field private final a:Legc;

.field private final b:J

.field private c:Lefg;

.field private d:Ljava/io/File;

.field private e:Ljava/io/FileOutputStream;

.field private f:J

.field private g:J


# direct methods
.method public constructor <init>(Legc;J)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, La;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legc;

    iput-object v0, p0, Lege;->a:Legc;

    .line 49
    const-wide/32 v0, 0x500000

    iput-wide v0, p0, Lege;->b:J

    .line 50
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    .line 98
    iget-object v0, p0, Lege;->a:Legc;

    iget-object v1, p0, Lege;->c:Lefg;

    iget-object v1, v1, Lefg;->f:Ljava/lang/String;

    iget-object v2, p0, Lege;->c:Lefg;

    iget-wide v2, v2, Lefg;->c:J

    iget-wide v4, p0, Lege;->g:J

    add-long/2addr v2, v4

    iget-object v4, p0, Lege;->c:Lefg;

    iget-wide v4, v4, Lefg;->e:J

    iget-wide v6, p0, Lege;->g:J

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lege;->b:J

    .line 99
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 98
    invoke-interface/range {v0 .. v5}, Legc;->a(Ljava/lang/String;JJ)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lege;->d:Ljava/io/File;

    .line 100
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lege;->d:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lege;->e:Ljava/io/FileOutputStream;

    .line 101
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lege;->f:J

    .line 102
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 105
    iget-object v0, p0, Lege;->e:Ljava/io/FileOutputStream;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lege;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 107
    iget-object v0, p0, Lege;->e:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 108
    iput-object v2, p0, Lege;->e:Ljava/io/FileOutputStream;

    .line 109
    iget-object v0, p0, Lege;->a:Legc;

    iget-object v1, p0, Lege;->d:Ljava/io/File;

    invoke-interface {v0, v1}, Legc;->a(Ljava/io/File;)V

    .line 110
    iput-object v2, p0, Lege;->d:Ljava/io/File;

    .line 112
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lefg;)Lefb;
    .locals 4

    .prologue
    .line 56
    iget-wide v0, p1, Lefg;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 58
    :try_start_0
    iput-object p1, p0, Lege;->c:Lefg;

    .line 59
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lege;->g:J

    .line 60
    invoke-direct {p0}, Lege;->b()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    return-object p0

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    new-instance v1, Legf;

    invoke-direct {v1, v0}, Legf;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 91
    :try_start_0
    invoke-direct {p0}, Lege;->c()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    new-instance v1, Legf;

    invoke-direct {v1, v0}, Legf;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public final a([BII)V
    .locals 8

    .prologue
    .line 70
    const/4 v0, 0x0

    .line 71
    :goto_0
    if-ge v0, p3, :cond_1

    .line 72
    :try_start_0
    iget-wide v2, p0, Lege;->f:J

    iget-wide v4, p0, Lege;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 73
    invoke-direct {p0}, Lege;->c()V

    .line 74
    invoke-direct {p0}, Lege;->b()V

    .line 76
    :cond_0
    sub-int v1, p3, v0

    int-to-long v2, v1

    iget-wide v4, p0, Lege;->b:J

    iget-wide v6, p0, Lege;->f:J

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 78
    iget-object v2, p0, Lege;->e:Ljava/io/FileOutputStream;

    add-int v3, p2, v0

    invoke-virtual {v2, p1, v3, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 79
    add-int/2addr v0, v1

    .line 80
    iget-wide v2, p0, Lege;->f:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lege;->f:J

    .line 81
    iget-wide v2, p0, Lege;->g:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lege;->g:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    new-instance v1, Legf;

    invoke-direct {v1, v0}, Legf;-><init>(Ljava/io/IOException;)V

    throw v1

    .line 85
    :cond_1
    return-void
.end method
