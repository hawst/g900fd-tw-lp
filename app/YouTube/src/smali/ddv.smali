.class public final Lddv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lddw;

.field private b:I

.field private c:I

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Lddw;)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lddw;

    iput-object v0, p0, Lddv;->a:Lddw;

    .line 42
    iput v1, p0, Lddv;->b:I

    .line 43
    iput v1, p0, Lddv;->c:I

    .line 44
    iput v1, p0, Lddv;->d:I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lddv;->e:I

    .line 46
    return-void
.end method

.method public static b(I)Z
    .locals 1

    .prologue
    .line 139
    const/16 v0, 0x5a

    if-eq p0, v0, :cond_0

    const/16 v0, 0x57

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x58

    if-eq p0, v0, :cond_0

    const/16 v0, 0x59

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x82

    if-eq p0, v0, :cond_0

    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 128
    const/16 v0, 0x4f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x55

    if-eq p0, v0, :cond_0

    const/16 v0, 0x56

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x57

    if-eq p0, v0, :cond_0

    const/16 v0, 0x58

    if-eq p0, v0, :cond_0

    const/16 v0, 0xaf

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Lddv;->b:I

    .line 50
    iput p2, p0, Lddv;->c:I

    .line 51
    return-void
.end method

.method public final a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v4, -0x80000000

    .line 109
    const/16 v2, 0x59

    if-eq p1, v2, :cond_0

    const/16 v2, 0x5a

    if-ne p1, v2, :cond_2

    .line 111
    :cond_0
    iget v2, p0, Lddv;->d:I

    if-eq v2, v4, :cond_1

    iget v2, p0, Lddv;->e:I

    if-ne p1, v2, :cond_1

    .line 114
    iget-object v2, p0, Lddv;->a:Lddw;

    iget v3, p0, Lddv;->d:I

    invoke-interface {v2, v3}, Lddw;->b(I)V

    .line 115
    iput v4, p0, Lddv;->d:I

    .line 116
    iput v1, p0, Lddv;->e:I

    .line 124
    :cond_1
    :goto_0
    return v0

    .line 120
    :cond_2
    invoke-static {p1}, Lddv;->c(I)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 124
    goto :goto_0
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/16 v4, 0x59

    const/4 v2, 0x0

    const/high16 v3, -0x80000000

    const/4 v1, 0x1

    .line 54
    iget v0, p0, Lddv;->d:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lddv;->e:I

    if-eq p1, v0, :cond_0

    move v0, v1

    .line 105
    :goto_0
    return v0

    .line 58
    :cond_0
    if-eq p1, v4, :cond_1

    const/16 v0, 0x5a

    if-ne p1, v0, :cond_5

    .line 60
    :cond_1
    iget v0, p0, Lddv;->b:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lddv;->c:I

    if-eq v0, v3, :cond_3

    .line 61
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 62
    iget v0, p0, Lddv;->b:I

    iput v0, p0, Lddv;->d:I

    .line 63
    iput p1, p0, Lddv;->e:I

    .line 64
    iget-object v0, p0, Lddv;->a:Lddw;

    invoke-interface {v0}, Lddw;->a()V

    .line 66
    :cond_2
    iget v3, p0, Lddv;->d:I

    if-ne p1, v4, :cond_4

    const/16 v0, -0x4e20

    :goto_1
    add-int/2addr v0, v3

    iput v0, p0, Lddv;->d:I

    .line 68
    iget v0, p0, Lddv;->d:I

    iget v3, p0, Lddv;->c:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lddv;->d:I

    .line 69
    iget-object v0, p0, Lddv;->a:Lddw;

    iget v2, p0, Lddv;->d:I

    invoke-interface {v0, v2}, Lddw;->a(I)V

    :cond_3
    move v0, v1

    .line 71
    goto :goto_0

    .line 66
    :cond_4
    const/16 v0, 0x4e20

    goto :goto_1

    .line 72
    :cond_5
    invoke-static {p1}, Lddv;->c(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 74
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v1

    .line 76
    goto :goto_0

    .line 79
    :cond_6
    sparse-switch p1, :sswitch_data_0

    :goto_2
    move v0, v1

    .line 99
    goto :goto_0

    .line 82
    :sswitch_0
    iget-object v0, p0, Lddv;->a:Lddw;

    invoke-interface {v0}, Lddw;->b()V

    goto :goto_2

    .line 85
    :sswitch_1
    iget-object v0, p0, Lddv;->a:Lddw;

    invoke-interface {v0}, Lddw;->c()V

    goto :goto_2

    .line 89
    :sswitch_2
    iget-object v0, p0, Lddv;->a:Lddw;

    invoke-interface {v0}, Lddw;->d()V

    goto :goto_2

    .line 92
    :sswitch_3
    iget-object v0, p0, Lddv;->a:Lddw;

    invoke-interface {v0}, Lddw;->f()V

    goto :goto_2

    .line 95
    :sswitch_4
    iget-object v0, p0, Lddv;->a:Lddw;

    invoke-interface {v0}, Lddw;->g()V

    goto :goto_2

    .line 98
    :sswitch_5
    iget-object v0, p0, Lddv;->a:Lddw;

    invoke-interface {v0}, Lddw;->e()V

    goto :goto_2

    :cond_7
    move v0, v2

    .line 105
    goto :goto_0

    .line 79
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_2
        0x57 -> :sswitch_3
        0x58 -> :sswitch_4
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
        0xaf -> :sswitch_5
    .end sparse-switch
.end method
