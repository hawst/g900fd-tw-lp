.class public final Lhqn;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Lhqm;

.field public d:Lhpq;

.field public e:Lhbh;

.field private f:[Ljava/lang/String;

.field private g:Z

.field private h:Z

.field private i:Lhbc;

.field private j:Lhpd;

.field private k:Z

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput v1, p0, Lhqn;->a:I

    const-string v0, ""

    iput-object v0, p0, Lhqn;->b:Ljava/lang/String;

    sget-object v0, Lidj;->d:[Ljava/lang/String;

    iput-object v0, p0, Lhqn;->f:[Ljava/lang/String;

    iput-boolean v1, p0, Lhqn;->g:Z

    iput-boolean v1, p0, Lhqn;->h:Z

    iput-object v2, p0, Lhqn;->i:Lhbc;

    iput-object v2, p0, Lhqn;->j:Lhpd;

    iput-object v2, p0, Lhqn;->c:Lhqm;

    iput-boolean v1, p0, Lhqn;->k:Z

    iput-object v2, p0, Lhqn;->d:Lhpq;

    iput-object v2, p0, Lhqn;->e:Lhbh;

    iput v1, p0, Lhqn;->l:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lhqn;->a:I

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    iget v2, p0, Lhqn;->a:I

    invoke-static {v0, v2}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Lhqn;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Lhqn;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lidd;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Lhqn;->f:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lhqn;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    iget-object v3, p0, Lhqn;->f:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    invoke-static {v5}, Lidd;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    add-int/2addr v0, v2

    iget-object v1, p0, Lhqn;->f:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_2
    iget-boolean v1, p0, Lhqn;->g:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-boolean v2, p0, Lhqn;->g:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-boolean v1, p0, Lhqn;->h:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-boolean v2, p0, Lhqn;->h:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhqn;->i:Lhbc;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lhqn;->i:Lhbc;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhqn;->j:Lhpd;

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lhqn;->j:Lhpd;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Lhqn;->c:Lhqm;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Lhqn;->c:Lhqm;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-boolean v1, p0, Lhqn;->k:Z

    if-eqz v1, :cond_8

    const/16 v1, 0x9

    iget-boolean v2, p0, Lhqn;->k:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhqn;->d:Lhpq;

    if-eqz v1, :cond_9

    const/16 v1, 0xa

    iget-object v2, p0, Lhqn;->d:Lhpq;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Lhqn;->e:Lhbh;

    if-eqz v1, :cond_a

    const/16 v1, 0xb

    iget-object v2, p0, Lhqn;->e:Lhbh;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lhqn;->l:I

    if-eqz v1, :cond_b

    const/16 v1, 0xc

    iget v2, p0, Lhqn;->l:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget-object v1, p0, Lhqn;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhqn;->J:I

    return v0

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhqn;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhqn;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhqn;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhqn;->a:I

    goto :goto_0

    :cond_3
    iput v3, p0, Lhqn;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhqn;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Lhqn;->f:[Ljava/lang/String;

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lhqn;->f:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Lhqn;->f:[Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lhqn;->f:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lhqn;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lhqn;->f:[Ljava/lang/String;

    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhqn;->g:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhqn;->h:Z

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lhqn;->i:Lhbc;

    if-nez v0, :cond_5

    new-instance v0, Lhbc;

    invoke-direct {v0}, Lhbc;-><init>()V

    iput-object v0, p0, Lhqn;->i:Lhbc;

    :cond_5
    iget-object v0, p0, Lhqn;->i:Lhbc;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lhqn;->j:Lhpd;

    if-nez v0, :cond_6

    new-instance v0, Lhpd;

    invoke-direct {v0}, Lhpd;-><init>()V

    iput-object v0, p0, Lhqn;->j:Lhpd;

    :cond_6
    iget-object v0, p0, Lhqn;->j:Lhpd;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lhqn;->c:Lhqm;

    if-nez v0, :cond_7

    new-instance v0, Lhqm;

    invoke-direct {v0}, Lhqm;-><init>()V

    iput-object v0, p0, Lhqn;->c:Lhqm;

    :cond_7
    iget-object v0, p0, Lhqn;->c:Lhqm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhqn;->k:Z

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhqn;->d:Lhpq;

    if-nez v0, :cond_8

    new-instance v0, Lhpq;

    invoke-direct {v0}, Lhpq;-><init>()V

    iput-object v0, p0, Lhqn;->d:Lhpq;

    :cond_8
    iget-object v0, p0, Lhqn;->d:Lhpq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhqn;->e:Lhbh;

    if-nez v0, :cond_9

    new-instance v0, Lhbh;

    invoke-direct {v0}, Lhbh;-><init>()V

    iput-object v0, p0, Lhqn;->e:Lhbh;

    :cond_9
    iget-object v0, p0, Lhqn;->e:Lhbh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhqn;->l:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget v0, p0, Lhqn;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget v1, p0, Lhqn;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_0
    iget-object v0, p0, Lhqn;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Lhqn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lhqn;->f:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lhqn;->f:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lhqn;->g:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lhqn;->g:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_3
    iget-boolean v0, p0, Lhqn;->h:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lhqn;->h:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_4
    iget-object v0, p0, Lhqn;->i:Lhbc;

    if-eqz v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lhqn;->i:Lhbc;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v0, p0, Lhqn;->j:Lhpd;

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lhqn;->j:Lhpd;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_6
    iget-object v0, p0, Lhqn;->c:Lhqm;

    if-eqz v0, :cond_7

    const/16 v0, 0x8

    iget-object v1, p0, Lhqn;->c:Lhqm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_7
    iget-boolean v0, p0, Lhqn;->k:Z

    if-eqz v0, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lhqn;->k:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_8
    iget-object v0, p0, Lhqn;->d:Lhpq;

    if-eqz v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lhqn;->d:Lhpq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_9
    iget-object v0, p0, Lhqn;->e:Lhbh;

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lhqn;->e:Lhbh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_a
    iget v0, p0, Lhqn;->l:I

    if-eqz v0, :cond_b

    const/16 v0, 0xc

    iget v1, p0, Lhqn;->l:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_b
    iget-object v0, p0, Lhqn;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhqn;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhqn;

    iget v2, p0, Lhqn;->a:I

    iget v3, p1, Lhqn;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhqn;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhqn;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhqn;->f:[Ljava/lang/String;

    iget-object v3, p1, Lhqn;->f:[Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lhqn;->g:Z

    iget-boolean v3, p1, Lhqn;->g:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhqn;->h:Z

    iget-boolean v3, p1, Lhqn;->h:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhqn;->i:Lhbc;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhqn;->i:Lhbc;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhqn;->j:Lhpd;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhqn;->j:Lhpd;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhqn;->c:Lhqm;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhqn;->c:Lhqm;

    if-nez v2, :cond_3

    :goto_4
    iget-boolean v2, p0, Lhqn;->k:Z

    iget-boolean v3, p1, Lhqn;->k:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhqn;->d:Lhpq;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhqn;->d:Lhpq;

    if-nez v2, :cond_3

    :goto_5
    iget-object v2, p0, Lhqn;->e:Lhbh;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhqn;->e:Lhbh;

    if-nez v2, :cond_3

    :goto_6
    iget v2, p0, Lhqn;->l:I

    iget v3, p1, Lhqn;->l:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhqn;->I:Ljava/util/List;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhqn;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhqn;->b:Ljava/lang/String;

    iget-object v3, p1, Lhqn;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhqn;->i:Lhbc;

    iget-object v3, p1, Lhqn;->i:Lhbc;

    invoke-virtual {v2, v3}, Lhbc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhqn;->j:Lhpd;

    iget-object v3, p1, Lhqn;->j:Lhpd;

    invoke-virtual {v2, v3}, Lhpd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhqn;->c:Lhqm;

    iget-object v3, p1, Lhqn;->c:Lhqm;

    invoke-virtual {v2, v3}, Lhqm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhqn;->d:Lhpq;

    iget-object v3, p1, Lhqn;->d:Lhpq;

    invoke-virtual {v2, v3}, Lhpq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhqn;->e:Lhbh;

    iget-object v3, p1, Lhqn;->e:Lhbh;

    invoke-virtual {v2, v3}, Lhbh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Lhqn;->I:Ljava/util/List;

    iget-object v3, p1, Lhqn;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhqn;->a:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqn;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    iget-object v2, p0, Lhqn;->f:[Ljava/lang/String;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-boolean v0, p0, Lhqn;->g:Z

    if-eqz v0, :cond_4

    move v0, v3

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhqn;->h:Z

    if-eqz v0, :cond_5

    move v0, v3

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqn;->i:Lhbc;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqn;->j:Lhpd;

    if-nez v0, :cond_7

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqn;->c:Lhqm;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lhqn;->k:Z

    if-eqz v2, :cond_9

    :goto_6
    add-int/2addr v0, v3

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqn;->d:Lhpq;

    if-nez v0, :cond_a

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhqn;->e:Lhbh;

    if-nez v0, :cond_b

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhqn;->l:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhqn;->I:Ljava/util/List;

    if-nez v2, :cond_c

    :goto_9
    add-int/2addr v0, v1

    return v0

    :cond_1
    iget-object v0, p0, Lhqn;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_2
    move v2, v0

    move v0, v1

    :goto_a
    iget-object v5, p0, Lhqn;->f:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_0

    mul-int/lit8 v5, v2, 0x1f

    iget-object v2, p0, Lhqn;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_b
    add-int/2addr v2, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_3
    iget-object v2, p0, Lhqn;->f:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_4
    move v0, v4

    goto :goto_1

    :cond_5
    move v0, v4

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lhqn;->i:Lhbc;

    invoke-virtual {v0}, Lhbc;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lhqn;->j:Lhpd;

    invoke-virtual {v0}, Lhpd;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lhqn;->c:Lhqm;

    invoke-virtual {v0}, Lhqm;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_9
    move v3, v4

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lhqn;->d:Lhpq;

    invoke-virtual {v0}, Lhpq;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_b
    iget-object v0, p0, Lhqn;->e:Lhbh;

    invoke-virtual {v0}, Lhbh;->hashCode()I

    move-result v0

    goto :goto_8

    :cond_c
    iget-object v1, p0, Lhqn;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_9
.end method
