.class public final Lakc;
.super Lajg;
.source "SourceFile"


# instance fields
.field public a:Lakf;

.field private final b:Lcwz;

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcwz;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lajg;-><init>()V

    .line 24
    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcwz;

    iput-object v0, p0, Lakc;->b:Lcwz;

    .line 25
    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lb;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lakc;->c:Landroid/os/Handler;

    .line 26
    return-void
.end method

.method static synthetic a(Lakc;)Lakf;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lakc;->a:Lakf;

    return-object v0
.end method

.method static synthetic a(Lakc;Lakf;)Lakf;
    .locals 0

    .prologue
    .line 16
    iput-object p1, p0, Lakc;->a:Lakf;

    return-object p1
.end method

.method static synthetic b(Lakc;)Lcwz;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lakc;->b:Lcwz;

    return-object v0
.end method


# virtual methods
.method public final a(Lanv;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lakc;->c:Landroid/os/Handler;

    new-instance v1, Lakd;

    invoke-direct {v1, p0, p1}, Lakd;-><init>(Lakc;Lanv;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 44
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lakc;->c:Landroid/os/Handler;

    new-instance v1, Lake;

    invoke-direct {v1, p0, p1}, Lake;-><init>(Lakc;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 55
    return-void
.end method
