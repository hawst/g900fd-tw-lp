.class public final Ldmq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/LinkedList;

.field private final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final c:Ljava/lang/Object;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Ldmq;-><init>(I)V

    .line 50
    return-void
.end method

.method private constructor <init>(I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ldmq;->a:Ljava/util/LinkedList;

    .line 37
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Ldmq;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ldmq;->c:Ljava/lang/Object;

    .line 39
    const/16 v0, 0x3e8

    iput v0, p0, Ldmq;->d:I

    .line 40
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Ldmq;->a(I)V

    .line 41
    return-void
.end method

.method static synthetic a(Ldmq;Ljava/io/InputStream;[BII)I
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3, p4}, Ldmq;->a(Ljava/io/InputStream;[BII)I

    move-result v0

    return v0
.end method

.method private a(Ljava/io/InputStream;[BII)I
    .locals 2

    .prologue
    .line 114
    if-nez p4, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 123
    :goto_0
    return v0

    .line 117
    :cond_0
    iget-object v0, p0, Ldmq;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_1

    .line 118
    invoke-virtual {p1, p2, p3, p4}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    goto :goto_0

    .line 120
    :cond_1
    invoke-direct {p0, p4}, Ldmq;->b(I)Ldms;

    move-result-object v1

    .line 121
    iget v0, v1, Ldms;->b:I

    invoke-virtual {p1, p2, p3, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 122
    invoke-virtual {v1, v0}, Ldms;->a(I)V

    goto :goto_0
.end method

.method private declared-synchronized b(I)Ldms;
    .locals 14

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    iget-object v7, p0, Ldmq;->c:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 133
    :goto_0
    :try_start_1
    iget-object v0, p0, Ldmq;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v8

    .line 134
    const/4 v0, 0x0

    .line 135
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 137
    iget-object v1, p0, Ldmq;->a:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-wide v2, v4

    move v6, v0

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 138
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldms;

    .line 141
    iget-wide v10, v0, Ldms;->a:J

    iget v1, p0, Ldmq;->d:I

    int-to-long v12, v1

    sub-long v12, v4, v12

    cmp-long v1, v10, v12

    if-gtz v1, :cond_0

    .line 142
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 170
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 131
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 144
    :cond_0
    :try_start_3
    iget-boolean v1, v0, Ldms;->d:Z

    if-eqz v1, :cond_1

    iget v1, v0, Ldms;->c:I

    :goto_2
    add-int/2addr v6, v1

    .line 145
    iget-wide v10, v0, Ldms;->a:J

    cmp-long v1, v10, v2

    if-gez v1, :cond_4

    .line 146
    iget-wide v0, v0, Ldms;->a:J

    :goto_3
    move-wide v2, v0

    .line 149
    goto :goto_1

    .line 144
    :cond_1
    iget v1, v0, Ldms;->b:I

    goto :goto_2

    .line 150
    :cond_2
    sub-int v0, v8, v6

    .line 151
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 152
    if-lez v0, :cond_3

    .line 153
    new-instance v1, Ldms;

    iget-object v2, p0, Ldmq;->c:Ljava/lang/Object;

    invoke-direct {v1, v2, v0, v4, v5}, Ldms;-><init>(Ljava/lang/Object;IJ)V

    .line 154
    iget-object v0, p0, Ldmq;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 155
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v1

    .line 161
    :cond_3
    :try_start_4
    iget-object v0, p0, Ldmq;->c:Ljava/lang/Object;

    iget v1, p0, Ldmq;->d:I

    int-to-long v8, v1

    sub-long v2, v4, v2

    sub-long v2, v8, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 162
    :catch_0
    move-exception v0

    .line 165
    :try_start_5
    new-instance v1, Ljava/io/IOException;

    const-string v2, "interrupted"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 166
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 167
    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_4
    move-wide v0, v2

    goto :goto_3
.end method


# virtual methods
.method a(Ljava/io/InputStream;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 104
    iget-object v0, p0, Ldmq;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v0

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_0

    .line 105
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 110
    :goto_0
    return v0

    .line 107
    :cond_0
    invoke-direct {p0, v2}, Ldmq;->b(I)Ldms;

    move-result-object v1

    .line 108
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    .line 109
    invoke-virtual {v1, v2}, Ldms;->a(I)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 83
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "bytesPerSecond cannot be negative"

    invoke-static {v0, v1}, Lb;->c(ZLjava/lang/Object;)V

    .line 84
    iget-object v0, p0, Ldmq;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 85
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
