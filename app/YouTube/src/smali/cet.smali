.class public final Lcet;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsj;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private c:Landroid/graphics/drawable/LayerDrawable;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View$OnClickListener;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iput-object p1, p0, Lcet;->a:Landroid/content/Context;

    .line 34
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 35
    iget-object v1, p0, Lcet;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f010112

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 36
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    iput v0, p0, Lcet;->b:I

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lfsg;)Landroid/view/View;
    .locals 10

    .prologue
    .line 70
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 73
    iget-object v1, p0, Lcet;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 74
    iget-object v2, p0, Lcet;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 75
    iget-object v3, p0, Lcet;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    .line 77
    iget-boolean v4, p1, Lfsg;->d:Z

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Lfsg;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 78
    iget-object v4, p0, Lcet;->d:Landroid/view/View;

    iget-object v5, p0, Lcet;->c:Landroid/graphics/drawable/LayerDrawable;

    if-nez v5, :cond_0

    new-instance v5, Landroid/graphics/drawable/LayerDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    iget-object v8, p0, Lcet;->a:Landroid/content/Context;

    iget v9, p0, Lcet;->b:I

    invoke-static {v8, v9}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcet;->a:Landroid/content/Context;

    const v9, 0x7f020099

    invoke-static {v8, v9}, Lar;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-direct {v5, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v5, p0, Lcet;->c:Landroid/graphics/drawable/LayerDrawable;

    :cond_0
    iget-object v5, p0, Lcet;->c:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 86
    :goto_0
    iget-object v4, p0, Lcet;->d:Landroid/view/View;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 88
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    return-object v0

    .line 80
    :cond_1
    iget-object v4, p0, Lcet;->d:Landroid/view/View;

    iget v5, p0, Lcet;->b:I

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lcet;->e:Landroid/view/View$OnClickListener;

    .line 55
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 41
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iput-object p1, p0, Lcet;->d:Landroid/view/View;

    .line 43
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    iget-object v1, p0, Lcet;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    iget-boolean v1, p0, Lcet;->f:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 45
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 62
    iput-boolean p1, p0, Lcet;->f:Z

    .line 63
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcet;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    .line 66
    :cond_0
    return-void
.end method
