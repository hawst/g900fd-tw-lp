.class public Ldge;
.super Lded;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Ldfu;


# instance fields
.field private final A:Landroid/view/animation/Animation;

.field private final B:Landroid/view/animation/Animation;

.field private final C:Landroid/view/animation/Animation;

.field private final D:Landroid/view/animation/Animation;

.field private final E:Landroid/view/animation/Animation;

.field private final F:Landroid/view/animation/Animation;

.field private final G:Landroid/view/animation/Animation;

.field private final H:Landroid/view/animation/Animation;

.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field private f:Ldfv;

.field private g:Z

.field private h:Z

.field private final i:I

.field private final j:I

.field private final k:Landroid/widget/ImageView;

.field private final l:Landroid/view/View;

.field private final m:Landroid/widget/ImageView;

.field private final n:Landroid/widget/ImageButton;

.field private final o:Landroid/view/View;

.field private final p:Landroid/widget/ImageView;

.field private final q:Landroid/view/View;

.field private final r:Landroid/view/View;

.field private final s:Landroid/widget/ImageView;

.field private final t:Landroid/widget/ImageButton;

.field private final u:Landroid/view/View;

.field private final v:Landroid/view/View;

.field private final w:Landroid/widget/TextView;

.field private final x:Landroid/widget/ImageView;

.field private final y:Landroid/view/animation/Animation;

.field private final z:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const v3, 0x7f05000d

    const v2, 0x7f05000c

    .line 86
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 87
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 89
    const v0, 0x7f05000a

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->y:Landroid/view/animation/Animation;

    .line 90
    const v0, 0x7f05000b

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->z:Landroid/view/animation/Animation;

    .line 91
    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->A:Landroid/view/animation/Animation;

    .line 92
    invoke-static {p1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->B:Landroid/view/animation/Animation;

    .line 94
    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->C:Landroid/view/animation/Animation;

    .line 96
    invoke-static {p1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->D:Landroid/view/animation/Animation;

    .line 98
    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->E:Landroid/view/animation/Animation;

    .line 100
    invoke-static {p1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->F:Landroid/view/animation/Animation;

    .line 102
    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->G:Landroid/view/animation/Animation;

    .line 104
    invoke-static {p1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldge;->H:Landroid/view/animation/Animation;

    .line 106
    const v0, 0x7f0b000e

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 107
    iget-object v2, p0, Ldge;->y:Landroid/view/animation/Animation;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 108
    iget-object v2, p0, Ldge;->z:Landroid/view/animation/Animation;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 110
    iget-object v0, p0, Ldge;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 111
    iget-object v0, p0, Ldge;->B:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 112
    iget-object v0, p0, Ldge;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 113
    iget-object v0, p0, Ldge;->F:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 114
    iget-object v0, p0, Ldge;->C:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 115
    iget-object v0, p0, Ldge;->H:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 117
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 118
    const v2, 0x7f040026

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 120
    const v0, 0x7f0800c9

    invoke-virtual {p0, v0}, Ldge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldge;->k:Landroid/widget/ImageView;

    .line 122
    const v0, 0x7f0800cf

    invoke-virtual {p0, v0}, Ldge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldge;->l:Landroid/view/View;

    .line 123
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    const v2, 0x7f0800d0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldge;->m:Landroid/widget/ImageView;

    .line 124
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    const v2, 0x7f0800d2

    .line 125
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldge;->a:Landroid/widget/TextView;

    .line 126
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    const v2, 0x7f0800d3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldge;->b:Landroid/widget/TextView;

    .line 127
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    const v2, 0x7f0800d1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldge;->c:Landroid/widget/TextView;

    .line 128
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    const v2, 0x7f0800d4

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Ldge;->n:Landroid/widget/ImageButton;

    .line 129
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 130
    iget-object v0, p0, Ldge;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 132
    const v0, 0x7f0800da

    invoke-virtual {p0, v0}, Ldge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldge;->o:Landroid/view/View;

    .line 133
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    const v2, 0x7f0800db

    .line 134
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldge;->p:Landroid/widget/ImageView;

    .line 135
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    const v2, 0x7f0800dc

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldge;->q:Landroid/view/View;

    .line 136
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 138
    const v0, 0x7f0800d5

    invoke-virtual {p0, v0}, Ldge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldge;->r:Landroid/view/View;

    .line 139
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    const v2, 0x7f0800d6

    .line 140
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldge;->s:Landroid/widget/ImageView;

    .line 141
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    const v2, 0x7f0800d7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldge;->d:Landroid/widget/TextView;

    .line 142
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    const v2, 0x7f0800d8

    .line 143
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldge;->e:Landroid/widget/TextView;

    .line 144
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    const v2, 0x7f0800d9

    .line 145
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Ldge;->t:Landroid/widget/ImageButton;

    .line 146
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 147
    iget-object v0, p0, Ldge;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    const v0, 0x7f0800ca

    invoke-virtual {p0, v0}, Ldge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldge;->u:Landroid/view/View;

    .line 150
    const v0, 0x7f0800cb

    invoke-virtual {p0, v0}, Ldge;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldge;->v:Landroid/view/View;

    .line 151
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    const v2, 0x7f0800cc

    .line 152
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldge;->w:Landroid/widget/TextView;

    .line 153
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    const v2, 0x7f0800cd

    .line 154
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldge;->x:Landroid/widget/ImageView;

    .line 155
    iget-object v0, p0, Ldge;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 159
    const v0, 0x7f0a0065

    .line 160
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v2, 0x7f0a005f

    .line 161
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v0, v2

    const v2, 0x7f0a005d

    .line 162
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    const v2, 0x7f0a005e

    .line 163
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Ldge;->i:I

    .line 164
    const v0, 0x7f0a0068

    .line 165
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Ldge;->j:I

    .line 167
    invoke-virtual {p0}, Ldge;->a()V

    .line 168
    return-void
.end method

.method private static a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 396
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 397
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 398
    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 399
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    return-void
.end method

.method private static a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 453
    if-ne p0, p1, :cond_0

    .line 454
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 456
    :cond_0
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 384
    iget-boolean v0, p0, Ldge;->h:Z

    if-eqz v0, :cond_1

    iget v0, p0, Ldge;->i:I

    .line 385
    :goto_0
    iget-boolean v2, p0, Ldge;->h:Z

    if-eqz v2, :cond_2

    iget v2, p0, Ldge;->j:I

    .line 386
    :goto_1
    iget-object v3, p0, Ldge;->o:Landroid/view/View;

    invoke-static {v3, v0, v2}, Ldge;->a(Landroid/view/View;II)V

    .line 387
    iget-object v3, p0, Ldge;->r:Landroid/view/View;

    invoke-static {v3, v0, v2}, Ldge;->a(Landroid/view/View;II)V

    .line 388
    iget-object v3, p0, Ldge;->l:Landroid/view/View;

    invoke-static {v3, v0, v2}, Ldge;->a(Landroid/view/View;II)V

    .line 390
    iget-boolean v0, p0, Ldge;->h:Z

    if-eqz v0, :cond_0

    const/16 v1, 0x8

    .line 391
    :cond_0
    iget-object v0, p0, Ldge;->n:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 392
    iget-object v0, p0, Ldge;->t:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 393
    return-void

    :cond_1
    move v0, v1

    .line 384
    goto :goto_0

    :cond_2
    move v2, v1

    .line 385
    goto :goto_1
.end method

.method private i()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 403
    iget-object v2, p0, Ldge;->k:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldge;->l:Landroid/view/View;

    .line 404
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldge;->r:Landroid/view/View;

    .line 405
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldge;->o:Landroid/view/View;

    .line 406
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Ldge;->u:Landroid/view/View;

    .line 407
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v1

    .line 408
    :goto_0
    if-eqz v2, :cond_2

    iget-boolean v2, p0, Ldge;->g:Z

    if-nez v2, :cond_2

    .line 409
    :goto_1
    if-eqz v1, :cond_3

    :goto_2
    invoke-virtual {p0, v0}, Ldge;->setVisibility(I)V

    .line 410
    return v1

    :cond_1
    move v2, v0

    .line 407
    goto :goto_0

    :cond_2
    move v1, v0

    .line 408
    goto :goto_1

    .line 409
    :cond_3
    const/16 v0, 0x8

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 175
    iput-boolean v3, p0, Ldge;->h:Z

    .line 176
    invoke-virtual {p0, v1}, Ldge;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Ldge;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Ldge;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 181
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Ldge;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v0, p0, Ldge;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Ldge;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 185
    iget-object v0, p0, Ldge;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Ldge;->q:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 189
    iget-object v0, p0, Ldge;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 190
    iget-object v0, p0, Ldge;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Ldge;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Ldge;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Ldge;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 196
    iget-object v0, p0, Ldge;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Ldge;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 202
    invoke-direct {p0}, Ldge;->h()V

    .line 203
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Ldge;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 241
    return-void
.end method

.method public final a(Ldfv;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Ldge;->f:Ldfv;

    .line 208
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Ldge;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Ldge;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v0, p0, Ldge;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 415
    iget-boolean v0, p0, Ldge;->g:Z

    if-eq p1, v0, :cond_1

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    if-nez p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Ldge;->g:Z

    .line 420
    iget-boolean v0, p0, Ldge;->g:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ldge;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 421
    iget-object v0, p0, Ldge;->z:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Ldge;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 419
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 422
    :cond_3
    iget-boolean v0, p0, Ldge;->g:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Ldge;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Ldge;->y:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Ldge;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final a(ZZ)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 299
    if-eqz p1, :cond_1

    .line 302
    iget-object v0, p0, Ldge;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 303
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 304
    if-eqz p2, :cond_0

    .line 305
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    iget-object v1, p0, Ldge;->C:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 323
    :cond_0
    :goto_0
    invoke-direct {p0}, Ldge;->i()Z

    .line 324
    return-void

    .line 307
    :cond_1
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 310
    iget-object v0, p0, Ldge;->q:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 311
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 312
    if-eqz p2, :cond_0

    .line 313
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    iget-object v1, p0, Ldge;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 317
    :cond_2
    iget-object v0, p0, Ldge;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 319
    if-eqz p2, :cond_0

    .line 320
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    iget-object v1, p0, Ldge;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Ldge;->m:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 268
    iget-object v0, p0, Ldge;->m:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 261
    iget-object v1, p0, Ldge;->c:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Ldge;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    return-void

    .line 261
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 328
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 329
    if-eqz p1, :cond_2

    .line 330
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    iget-object v1, p0, Ldge;->D:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 335
    :cond_0
    :goto_0
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 336
    if-eqz p1, :cond_3

    .line 337
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    iget-object v1, p0, Ldge;->F:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 342
    :cond_1
    :goto_1
    return-void

    .line 332
    :cond_2
    iget-object v0, p0, Ldge;->r:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 339
    :cond_3
    iget-object v0, p0, Ldge;->o:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Ldge;->k:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 252
    return-void
.end method

.method public final c(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 291
    iget-object v0, p0, Ldge;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 292
    iget-object v3, p0, Ldge;->s:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 293
    iget-object v0, p0, Ldge;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 294
    iget-object v0, p0, Ldge;->p:Landroid/widget/ImageView;

    if-nez p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 295
    return-void

    :cond_0
    move v0, v2

    .line 292
    goto :goto_0

    :cond_1
    move v1, v2

    .line 294
    goto :goto_1
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Ldge;->w:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 347
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 379
    iput-boolean p1, p0, Ldge;->h:Z

    .line 380
    invoke-direct {p0}, Ldge;->h()V

    .line 381
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    iget-object v1, p0, Ldge;->A:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 275
    invoke-direct {p0}, Ldge;->i()Z

    .line 276
    return-void
.end method

.method public final d(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Ldge;->x:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 352
    iget-object v1, p0, Ldge;->x:Landroid/widget/ImageView;

    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 353
    return-void

    .line 352
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Ldge;->l:Landroid/view/View;

    iget-object v1, p0, Ldge;->B:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 281
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 357
    iget-object v0, p0, Ldge;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 358
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 359
    invoke-direct {p0}, Ldge;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    iget-object v1, p0, Ldge;->G:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 363
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 367
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 368
    iget-boolean v0, p0, Ldge;->g:Z

    if-eqz v0, :cond_1

    .line 369
    iget-object v0, p0, Ldge;->u:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 372
    :cond_1
    iget-object v0, p0, Ldge;->v:Landroid/view/View;

    iget-object v1, p0, Ldge;->H:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Ldge;->z:Landroid/view/animation/Animation;

    invoke-static {p1, v0, p0}, Ldge;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    .line 440
    iget-object v0, p0, Ldge;->B:Landroid/view/animation/Animation;

    iget-object v1, p0, Ldge;->l:Landroid/view/View;

    invoke-static {p1, v0, v1}, Ldge;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    .line 441
    iget-object v0, p0, Ldge;->F:Landroid/view/animation/Animation;

    iget-object v1, p0, Ldge;->o:Landroid/view/View;

    invoke-static {p1, v0, v1}, Ldge;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    .line 442
    iget-object v0, p0, Ldge;->D:Landroid/view/animation/Animation;

    iget-object v1, p0, Ldge;->r:Landroid/view/View;

    invoke-static {p1, v0, v1}, Ldge;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    .line 443
    iget-object v0, p0, Ldge;->H:Landroid/view/animation/Animation;

    iget-object v1, p0, Ldge;->u:Landroid/view/View;

    invoke-static {p1, v0, v1}, Ldge;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    .line 445
    iget-object v0, p0, Ldge;->C:Landroid/view/animation/Animation;

    iget-object v1, p0, Ldge;->o:Landroid/view/View;

    invoke-static {p1, v0, v1}, Ldge;->a(Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;)V

    .line 447
    iget-object v0, p0, Ldge;->D:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 448
    iget-object v0, p0, Ldge;->q:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 450
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 435
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 430
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 217
    iget-object v2, p0, Ldge;->f:Ldfv;

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-eqz v2, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v0

    .line 220
    :cond_1
    iget-object v2, p0, Ldge;->l:Landroid/view/View;

    if-ne p1, v2, :cond_2

    .line 221
    iget-object v0, p0, Ldge;->f:Ldfv;

    invoke-interface {v0}, Ldfv;->b()V

    :goto_1
    move v0, v1

    .line 235
    goto :goto_0

    .line 222
    :cond_2
    iget-object v2, p0, Ldge;->n:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_3

    .line 223
    iget-object v0, p0, Ldge;->f:Ldfv;

    invoke-interface {v0}, Ldfv;->c()V

    goto :goto_1

    .line 224
    :cond_3
    iget-object v2, p0, Ldge;->r:Landroid/view/View;

    if-ne p1, v2, :cond_4

    .line 225
    iget-object v0, p0, Ldge;->f:Ldfv;

    invoke-interface {v0, v1}, Ldfv;->a(Z)V

    goto :goto_1

    .line 226
    :cond_4
    iget-object v2, p0, Ldge;->o:Landroid/view/View;

    if-ne p1, v2, :cond_5

    .line 227
    iget-object v2, p0, Ldge;->f:Ldfv;

    invoke-interface {v2, v0}, Ldfv;->a(Z)V

    goto :goto_1

    .line 228
    :cond_5
    iget-object v2, p0, Ldge;->t:Landroid/widget/ImageButton;

    if-ne p1, v2, :cond_6

    .line 229
    iget-object v0, p0, Ldge;->f:Ldfv;

    invoke-interface {v0}, Ldfv;->a()V

    goto :goto_1

    .line 230
    :cond_6
    iget-object v2, p0, Ldge;->u:Landroid/view/View;

    if-ne p1, v2, :cond_0

    .line 231
    iget-object v0, p0, Ldge;->f:Ldfv;

    invoke-interface {v0}, Ldfv;->d()V

    goto :goto_1
.end method

.method public final p_()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Ldge;->k:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 246
    invoke-direct {p0}, Ldge;->i()Z

    .line 247
    return-void
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 212
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method
