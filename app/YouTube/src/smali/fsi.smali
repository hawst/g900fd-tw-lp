.class public final Lfsi;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lfsf;


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Ljava/util/List;

.field public final c:Lfsl;

.field private final d:Ljava/util/Map;

.field private final e:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lfsl;

    invoke-direct {v0}, Lfsl;-><init>()V

    invoke-direct {p0, v0}, Lfsi;-><init>(Lfsl;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lfsl;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 47
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lfsi;->d:Ljava/util/Map;

    .line 48
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lfsi;->a:Ljava/util/Map;

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfsi;->e:Ljava/util/Map;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfsi;->b:Ljava/util/List;

    .line 51
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsl;

    iput-object v0, p0, Lfsi;->c:Lfsl;

    .line 52
    return-void
.end method

.method private a(I)Z
    .locals 2

    .prologue
    .line 304
    invoke-virtual {p0, p1}, Lfsi;->getItemViewType(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(ILjava/lang/Object;)Z
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lfsi;->c:Lfsl;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfsl;->a(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 113
    const/4 v0, 0x1

    .line 116
    :goto_0
    return v0

    .line 115
    :cond_0
    const-string v1, "Missing presenter for "

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 116
    const/4 v0, 0x0

    goto :goto_0

    .line 115
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lfsi;->c:Lfsl;

    iget-object v0, v0, Lfsl;->a:Ljava/util/LinkedHashSet;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lfsi;->b(ILjava/lang/Object;)Z

    .line 104
    invoke-virtual {p0}, Lfsi;->notifyDataSetChanged()V

    .line 105
    return-void
.end method

.method public final a(ILjava/util/Collection;)V
    .locals 2

    .prologue
    .line 150
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 151
    invoke-direct {p0, p1, v1}, Lfsi;->b(ILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 155
    :cond_1
    invoke-virtual {p0}, Lfsi;->notifyDataSetChanged()V

    .line 156
    return-void
.end method

.method public final a(Lewh;)V
    .locals 4

    .prologue
    .line 204
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 206
    iget-object v1, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 207
    invoke-interface {p1, v2}, Lewh;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v3, p0, Lfsi;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 212
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 213
    iget-object v1, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 214
    invoke-virtual {p0}, Lfsi;->notifyDataSetChanged()V

    .line 216
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/Class;Lfsk;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lfsi;->c:Lfsl;

    invoke-virtual {v0, p1, p2}, Lfsl;->a(Ljava/lang/Class;Lfsk;)V

    .line 66
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 143
    invoke-virtual {p0}, Lfsi;->getCount()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lfsi;->a(ILjava/util/Collection;)V

    .line 144
    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 363
    iget-object v0, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 364
    if-eqz v0, :cond_0

    .line 365
    invoke-virtual {p0}, Lfsi;->notifyDataSetChanged()V

    .line 366
    iget-object v1, p0, Lfsi;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    :cond_0
    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 223
    iget-object v0, p0, Lfsi;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 224
    invoke-virtual {p0}, Lfsi;->notifyDataSetChanged()V

    .line 225
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lfsi;->getCount()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lfsi;->a(ILjava/lang/Object;)V

    .line 97
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 353
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    .line 342
    invoke-virtual {p0, p1}, Lfsi;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 343
    iget-object v1, p0, Lfsi;->c:Lfsl;

    invoke-static {v2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v1, Lfsl;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, v1, Lfsl;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-ne v0, v2, :cond_0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const v7, 0x7f080018

    const/4 v5, 0x1

    const v6, 0x7f080017

    .line 247
    invoke-virtual {p0, p1}, Lfsi;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, p1}, Lfsi;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfsi;->e:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object p2, v0

    :cond_0
    if-nez p2, :cond_5

    iget-object v0, p0, Lfsi;->c:Lfsl;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfsl;->b(Ljava/lang/Class;)Lfsh;

    move-result-object v0

    move-object v2, v0

    :goto_0
    const/4 v0, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p2, v7}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsg;

    :cond_1
    if-nez v0, :cond_6

    new-instance v0, Lfsg;

    invoke-direct {v0}, Lfsg;-><init>()V

    move-object v1, v0

    :goto_1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v4

    sub-int/2addr v0, v4

    iput v0, v1, Lfsg;->c:I

    iput p1, v1, Lfsg;->a:I

    invoke-virtual {p0}, Lfsi;->getCount()I

    move-result v0

    iput v0, v1, Lfsg;->b:I

    iput-boolean v5, v1, Lfsg;->d:Z

    const/4 v0, 0x0

    iput v0, v1, Lfsg;->e:I

    iput v5, v1, Lfsg;->f:I

    iget-object v0, p0, Lfsi;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    iget-object v0, p0, Lfsi;->a:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ls;

    if-nez v0, :cond_2

    iget-object v0, p0, Lfsi;->d:Ljava/util/Map;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lu;->f()Ls;

    move-result-object v0

    iget-object v5, p0, Lfsi;->a:Ljava/util/Map;

    invoke-interface {v5, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-interface {v2, v1, v3}, Lfsh;->a(Lfsg;Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_3

    invoke-virtual {v0, v6, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v2, 0x7f080019

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v0, v7, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-direct {p0, p1}, Lfsi;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lfsi;->e:Ljava/util/Map;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_4

    instance-of v2, v1, Landroid/widget/AbsListView$LayoutParams;

    if-nez v2, :cond_4

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    iget v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v2, v3, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    return-object v0

    :cond_5
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsh;

    move-object v2, v0

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lfsi;->c:Lfsl;

    iget-object v0, v0, Lfsl;->a:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    return v0
.end method
