.class public final Ldis;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lewi;


# instance fields
.field private final a:Lewi;

.field private final b:Lewi;

.field private final c:Lewi;

.field private final d:Ljava/security/Key;

.field private final e:Lggm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v0, p0, Ldis;->a:Lewi;

    .line 51
    iput-object v0, p0, Ldis;->b:Lewi;

    .line 52
    iput-object v0, p0, Ldis;->c:Lewi;

    .line 53
    iput-object v0, p0, Ldis;->d:Ljava/security/Key;

    .line 54
    iput-object v0, p0, Ldis;->e:Lggm;

    .line 55
    return-void
.end method

.method public constructor <init>(Lewi;Lewi;Lewi;Ljava/security/Key;Lggm;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Ldis;->a:Lewi;

    .line 40
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Ldis;->b:Lewi;

    .line 41
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Ldis;->c:Lewi;

    .line 42
    iput-object p4, p0, Ldis;->d:Ljava/security/Key;

    .line 43
    iput-object p5, p0, Ldis;->e:Lggm;

    .line 44
    return-void
.end method


# virtual methods
.method public final b()Lefc;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 68
    iget-object v0, p0, Ldis;->a:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lefc;

    .line 70
    iget-object v0, p0, Ldis;->b:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legc;

    .line 71
    if-eqz v1, :cond_0

    .line 72
    new-instance v4, Legn;

    iget-object v0, p0, Ldis;->d:Ljava/security/Key;

    .line 73
    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    const/16 v3, 0x1000

    new-array v3, v3, [B

    new-instance v7, Lege;

    const-wide/32 v8, 0x500000

    invoke-direct {v7, v1, v8, v9}, Lege;-><init>(Legc;J)V

    invoke-direct {v4, v0, v3, v7}, Legn;-><init>([B[BLefb;)V

    .line 76
    new-instance v3, Lego;

    iget-object v0, p0, Ldis;->d:Ljava/security/Key;

    .line 77
    invoke-interface {v0}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    new-instance v7, Lefk;

    invoke-direct {v7}, Lefk;-><init>()V

    invoke-direct {v3, v0, v7}, Lego;-><init>([BLefc;)V

    .line 78
    new-instance v0, Legg;

    iget-object v7, p0, Ldis;->e:Lggm;

    invoke-direct/range {v0 .. v7}, Legg;-><init>(Legc;Lefc;Lefc;Lefb;ZZLegh;)V

    move-object v2, v0

    .line 89
    :cond_0
    iget-object v0, p0, Ldis;->c:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Legc;

    .line 90
    new-instance v0, Legg;

    new-instance v3, Lego;

    iget-object v4, p0, Ldis;->d:Ljava/security/Key;

    .line 93
    invoke-interface {v4}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    new-instance v7, Lefk;

    invoke-direct {v7}, Lefk;-><init>()V

    invoke-direct {v3, v4, v7}, Lego;-><init>([BLefc;)V

    move-object v4, v10

    move-object v7, v10

    invoke-direct/range {v0 .. v7}, Legg;-><init>(Legc;Lefc;Lefc;Lefb;ZZLegh;)V

    move-object v2, v0

    .line 98
    goto :goto_0

    .line 100
    :cond_1
    return-object v2
.end method

.method public final synthetic e_()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Ldis;->b()Lefc;

    move-result-object v0

    return-object v0
.end method
