.class public final Laex;
.super Landroid/view/View;
.source "SourceFile"


# instance fields
.field final a:Laew;

.field b:Z

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    new-instance v0, Laew;

    invoke-virtual {p0}, Laex;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Laew;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Laex;->a:Laew;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 50
    iget v0, p0, Laex;->c:I

    if-lez v0, :cond_0

    int-to-long v0, p1

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget v2, p0, Laex;->c:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 52
    :goto_0
    iget-object v1, p0, Laex;->a:Laew;

    iget v2, p0, Laex;->d:I

    mul-int/lit16 v2, v2, 0x3e8

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Laew;->setLevel(I)Z

    .line 53
    invoke-virtual {p0}, Laex;->invalidate()V

    .line 54
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(III)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x64

    const/4 v0, 0x0

    .line 33
    if-lez p2, :cond_2

    int-to-long v2, p1

    mul-long/2addr v2, v6

    int-to-long v4, p2

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 35
    :goto_0
    if-lez p2, :cond_0

    int-to-long v2, p3

    mul-long/2addr v2, v6

    int-to-long v4, p2

    div-long/2addr v2, v4

    long-to-int v0, v2

    .line 37
    :cond_0
    iget-boolean v2, p0, Laex;->b:Z

    if-nez v2, :cond_1

    .line 38
    iget-object v2, p0, Laex;->a:Laew;

    mul-int/lit16 v3, v0, 0x3e8

    add-int/2addr v1, v3

    invoke-virtual {v2, v1}, Laew;->setLevel(I)Z

    .line 40
    :cond_1
    iput v0, p0, Laex;->d:I

    .line 41
    iput p2, p0, Laex;->c:I

    .line 42
    invoke-virtual {p0}, Laex;->invalidate()V

    .line 43
    return-void

    :cond_2
    move v1, v0

    .line 33
    goto :goto_0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 73
    iget-object v0, p0, Laex;->a:Laew;

    invoke-virtual {v0, p1}, Laew;->draw(Landroid/graphics/Canvas;)V

    .line 74
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    .line 59
    invoke-static {v0, p1}, Laex;->getDefaultSize(II)I

    move-result v0

    iget-object v1, p0, Laex;->a:Laew;

    .line 60
    invoke-virtual {v1}, Laew;->getIntrinsicHeight()I

    move-result v1

    invoke-static {v1, p2}, Laex;->resolveSize(II)I

    move-result v1

    .line 58
    invoke-virtual {p0, v0, v1}, Laex;->setMeasuredDimension(II)V

    .line 61
    return-void
.end method

.method protected final onSizeChanged(IIII)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 67
    iget-object v0, p0, Laex;->a:Laew;

    invoke-virtual {v0, v1, v1, p1, p2}, Laew;->setBounds(IIII)V

    .line 68
    return-void
.end method
