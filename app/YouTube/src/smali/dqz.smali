.class public final enum Ldqz;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Ldqz;

.field public static final g:Ldqz;

.field private static enum h:Ldqz;

.field private static final synthetic i:[Ldqz;


# instance fields
.field public final b:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Ldqz;

    const-string v1, "PRODUCTION"

    const-string v3, "/api/lounge/bc/"

    const-string v4, "https://www.youtube.com/api/lounge/bc/bind/"

    const-string v5, "https://www.youtube.com/api/lounge/pairing/"

    const-string v6, "https://www.youtube.com/api/lounge/screens/"

    const-string v7, ""

    invoke-direct/range {v0 .. v7}, Ldqz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Ldqz;->a:Ldqz;

    .line 13
    new-instance v3, Ldqz;

    const-string v4, "STAGING"

    const-string v6, "/api/loungedev/bc/"

    const-string v7, "https://www.youtube.com/api/loungedev/bc/bind/"

    const-string v8, "https://www.youtube.com/api/loungedev/pairing/"

    const-string v9, "https://www.youtube.com/api/loungedev/screens/"

    const-string v10, "&env_useStageMdx=1"

    move v5, v11

    invoke-direct/range {v3 .. v10}, Ldqz;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v3, Ldqz;->h:Ldqz;

    .line 6
    const/4 v0, 0x2

    new-array v0, v0, [Ldqz;

    sget-object v1, Ldqz;->a:Ldqz;

    aput-object v1, v0, v2

    sget-object v1, Ldqz;->h:Ldqz;

    aput-object v1, v0, v11

    sput-object v0, Ldqz;->i:[Ldqz;

    .line 65
    sget-object v0, Ldqz;->a:Ldqz;

    sput-object v0, Ldqz;->g:Ldqz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-object p3, p0, Ldqz;->b:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Ldqz;->c:Ljava/lang/String;

    .line 35
    iput-object p5, p0, Ldqz;->d:Ljava/lang/String;

    .line 36
    iput-object p6, p0, Ldqz;->e:Ljava/lang/String;

    .line 37
    iput-object p7, p0, Ldqz;->f:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 68
    sget-object v0, Ldqz;->g:Ldqz;

    invoke-virtual {v0}, Ldqz;->ordinal()I

    move-result v0

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Ldqz;
    .locals 1

    .prologue
    .line 6
    const-class v0, Ldqz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Ldqz;

    return-object v0
.end method

.method public static values()[Ldqz;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Ldqz;->i:[Ldqz;

    invoke-virtual {v0}, [Ldqz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ldqz;

    return-object v0
.end method
