.class public abstract Lcbf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfsh;


# instance fields
.field public final a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

.field private b:Landroid/content/res/Resources;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/ProgressBar;


# direct methods
.method protected constructor <init>(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcbf;->b:Landroid/content/res/Resources;

    .line 35
    const v0, 0x7f08021a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f080217

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcbf;->d:Landroid/view/View;

    .line 37
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    const v1, 0x7f080218

    .line 38
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    .line 37
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    iput-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    .line 39
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    const v1, 0x7f080219

    .line 40
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 39
    invoke-static {v0}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcbf;->e:Landroid/widget/ProgressBar;

    .line 42
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 46
    :cond_0
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 86
    if-eqz p1, :cond_1

    .line 87
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 88
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 50
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 54
    return-void
.end method

.method public a(F)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 131
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 133
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 134
    iget-object v0, p0, Lcbf;->e:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    .line 136
    return-void
.end method

.method protected final a(IIII)V
    .locals 1

    .prologue
    .line 64
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcbf;->a(F)V

    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcbf;->a(I)V

    .line 66
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(I)V

    .line 67
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(II)V

    .line 68
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 139
    if-eqz p1, :cond_0

    const v0, 0x7f090318

    .line 141
    :goto_0
    iget-object v1, p0, Lcbf;->d:Landroid/view/View;

    iget-object v2, p0, Lcbf;->b:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 142
    return-void

    .line 139
    :cond_0
    const v0, 0x7f090319

    goto :goto_0
.end method

.method protected final a(ZIIZ)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 112
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 113
    if-eqz p1, :cond_1

    .line 114
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcbf;->b:Landroid/content/res/Resources;

    const v2, 0x7f090199

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 116
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_0
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcbf;->a(F)V

    .line 122
    if-eqz p4, :cond_2

    .line 123
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a()V

    .line 127
    :goto_1
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(II)V

    .line 128
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 125
    :cond_2
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    goto :goto_1
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 71
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcbf;->a(F)V

    .line 72
    invoke-virtual {p0}, Lcbf;->d()V

    .line 73
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->b()V

    .line 74
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(II)V

    .line 75
    return-void
.end method

.method protected final c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 78
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcbf;->a(F)V

    .line 79
    const v0, 0x7f0901a6

    invoke-direct {p0, v0}, Lcbf;->a(I)V

    .line 80
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    iget v1, v0, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(I)V

    .line 81
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->a(II)V

    .line 82
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcbf;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    :cond_0
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 106
    iget-object v0, p0, Lcbf;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 107
    iget-object v0, p0, Lcbf;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcbf;->a:Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/app/ui/OfflineArrowView;->setVisibility(I)V

    .line 109
    return-void
.end method
