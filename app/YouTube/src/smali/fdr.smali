.class public final Lfdr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfrw;


# instance fields
.field private final a:Lwc;

.field private final b:Lfds;


# direct methods
.method public constructor <init>(Lwc;Lfds;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwc;

    iput-object v0, p0, Lfdr;->a:Lwc;

    .line 51
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfds;

    iput-object v0, p0, Lfdr;->b:Lfds;

    .line 52
    return-void
.end method

.method private static a([B)Lwd;
    .locals 4

    .prologue
    .line 69
    const/4 v1, 0x0

    .line 71
    :try_start_0
    invoke-static {p0}, La;->b([B)Lhtx;
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 76
    :goto_0
    invoke-static {p0, v0}, La;->a([BLhtx;)Lwd;

    move-result-object v0

    return-object v0

    .line 72
    :catch_0
    move-exception v0

    .line 73
    const-string v2, "Failed to parse inlined prefetch data: "

    invoke-virtual {v0}, Lidg;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Lhog;)V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lfdr;->b:Lfds;

    invoke-virtual {v0, p1}, Lfds;->a(Lhog;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lfdr;->b:Lfds;

    invoke-virtual {v0, p1}, Lfds;->c(Lhog;)[B

    move-result-object v0

    .line 61
    iget-object v1, p0, Lfdr;->b:Lfds;

    invoke-virtual {v1, p1}, Lfds;->b(Lhog;)Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-static {v0}, Lfdr;->a([B)Lwd;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    iget-object v2, p0, Lfdr;->a:Lwc;

    invoke-interface {v2, v1, v0}, Lwc;->a(Ljava/lang/String;Lwd;)V

    goto :goto_0
.end method
