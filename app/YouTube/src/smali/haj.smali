.class public final Lhaj;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11172
    invoke-direct {p0}, Lidf;-><init>()V

    .line 11175
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhaj;->a:[B

    .line 11172
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 11217
    const/4 v0, 0x0

    .line 11218
    iget-object v1, p0, Lhaj;->a:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 11219
    const/4 v0, 0x2

    iget-object v1, p0, Lhaj;->a:[B

    .line 11220
    invoke-static {v0, v1}, Lidd;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11222
    :cond_0
    iget-object v1, p0, Lhaj;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11223
    iput v0, p0, Lhaj;->J:I

    .line 11224
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 11168
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhaj;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhaj;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhaj;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhaj;->a:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 11208
    iget-object v0, p0, Lhaj;->a:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 11209
    const/4 v0, 0x2

    iget-object v1, p0, Lhaj;->a:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 11211
    :cond_0
    iget-object v0, p0, Lhaj;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 11213
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11186
    if-ne p1, p0, :cond_1

    .line 11190
    :cond_0
    :goto_0
    return v0

    .line 11187
    :cond_1
    instance-of v2, p1, Lhaj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 11188
    :cond_2
    check-cast p1, Lhaj;

    .line 11189
    iget-object v2, p0, Lhaj;->a:[B

    iget-object v3, p1, Lhaj;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhaj;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhaj;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 11190
    goto :goto_0

    .line 11189
    :cond_4
    iget-object v2, p0, Lhaj;->I:Ljava/util/List;

    iget-object v3, p1, Lhaj;->I:Ljava/util/List;

    .line 11190
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11194
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 11196
    iget-object v2, p0, Lhaj;->a:[B

    if-nez v2, :cond_1

    mul-int/lit8 v2, v0, 0x1f

    .line 11202
    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhaj;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_0
    add-int/2addr v0, v1

    .line 11203
    return v0

    :cond_1
    move v2, v0

    move v0, v1

    .line 11198
    :goto_1
    iget-object v3, p0, Lhaj;->a:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 11199
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhaj;->a:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 11198
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 11202
    :cond_2
    iget-object v1, p0, Lhaj;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
