.class public final Lalq;
.super Lgxi;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lalx;

.field private final c:Ladc;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Laco;Lgxe;)V
    .locals 5

    .prologue
    .line 27
    invoke-direct {p0}, Lgxi;-><init>()V

    .line 28
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lalq;->a:Landroid/os/Handler;

    .line 31
    new-instance v0, Lalx;

    invoke-direct {v0, p0, p3}, Lalx;-><init>(Lalq;Lgxe;)V

    iput-object v0, p0, Lalq;->b:Lalx;

    .line 32
    new-instance v0, Ladc;

    iget-object v1, p0, Lalq;->b:Lalx;

    .line 34
    iget-object v2, p2, Laco;->l:Lfwk;

    .line 35
    iget-object v3, p2, Laco;->t:Leyp;

    .line 36
    iget-object v4, p2, Laco;->A:Lexd;

    invoke-direct {v0, v1, v2, v3, v4}, Ladc;-><init>(Ladd;Lfxe;Leyp;Lexd;)V

    iput-object v0, p0, Lalq;->c:Ladc;

    .line 37
    return-void
.end method

.method static synthetic a(Lalq;)Ladc;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lalq;->c:Ladc;

    return-object v0
.end method

.method static synthetic b(Lalq;)Lalx;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lalq;->b:Lalx;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lalq;->a:Landroid/os/Handler;

    new-instance v1, Lalt;

    invoke-direct {v1, p0}, Lalt;-><init>(Lalq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 67
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lalq;->a:Landroid/os/Handler;

    new-instance v1, Lalr;

    invoke-direct {v1, p0, p1}, Lalr;-><init>(Lalq;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 47
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lalq;->a:Landroid/os/Handler;

    new-instance v1, Lals;

    invoke-direct {v1, p0, p1, p2}, Lals;-><init>(Lalq;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 57
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lalq;->a:Landroid/os/Handler;

    new-instance v1, Lalu;

    invoke-direct {v1, p0}, Lalu;-><init>(Lalq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 77
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lalq;->a:Landroid/os/Handler;

    new-instance v1, Lalv;

    invoke-direct {v1, p0}, Lalv;-><init>(Lalq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 87
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lalq;->a:Landroid/os/Handler;

    new-instance v1, Lalw;

    invoke-direct {v1, p0}, Lalw;-><init>(Lalq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 101
    return-void
.end method
