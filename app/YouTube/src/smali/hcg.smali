.class public final Lhcg;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:Lhgz;

.field public c:Lhbt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21227
    invoke-direct {p0}, Lidf;-><init>()V

    .line 21230
    iput-object v0, p0, Lhcg;->a:Lhgz;

    .line 21233
    iput-object v0, p0, Lhcg;->b:Lhgz;

    .line 21236
    iput-object v0, p0, Lhcg;->c:Lhbt;

    .line 21227
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 21285
    const/4 v0, 0x0

    .line 21286
    iget-object v1, p0, Lhcg;->a:Lhgz;

    if-eqz v1, :cond_0

    .line 21287
    const/4 v0, 0x1

    iget-object v1, p0, Lhcg;->a:Lhgz;

    .line 21288
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 21290
    :cond_0
    iget-object v1, p0, Lhcg;->b:Lhgz;

    if-eqz v1, :cond_1

    .line 21291
    const/4 v1, 0x2

    iget-object v2, p0, Lhcg;->b:Lhgz;

    .line 21292
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21294
    :cond_1
    iget-object v1, p0, Lhcg;->c:Lhbt;

    if-eqz v1, :cond_2

    .line 21295
    const/4 v1, 0x3

    iget-object v2, p0, Lhcg;->c:Lhbt;

    .line 21296
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21298
    :cond_2
    iget-object v1, p0, Lhcg;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 21299
    iput v0, p0, Lhcg;->J:I

    .line 21300
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 21223
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhcg;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhcg;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhcg;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhcg;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcg;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Lhcg;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhcg;->b:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhcg;->b:Lhgz;

    :cond_3
    iget-object v0, p0, Lhcg;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhcg;->c:Lhbt;

    if-nez v0, :cond_4

    new-instance v0, Lhbt;

    invoke-direct {v0}, Lhbt;-><init>()V

    iput-object v0, p0, Lhcg;->c:Lhbt;

    :cond_4
    iget-object v0, p0, Lhcg;->c:Lhbt;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 21270
    iget-object v0, p0, Lhcg;->a:Lhgz;

    if-eqz v0, :cond_0

    .line 21271
    const/4 v0, 0x1

    iget-object v1, p0, Lhcg;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 21273
    :cond_0
    iget-object v0, p0, Lhcg;->b:Lhgz;

    if-eqz v0, :cond_1

    .line 21274
    const/4 v0, 0x2

    iget-object v1, p0, Lhcg;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 21276
    :cond_1
    iget-object v0, p0, Lhcg;->c:Lhbt;

    if-eqz v0, :cond_2

    .line 21277
    const/4 v0, 0x3

    iget-object v1, p0, Lhcg;->c:Lhbt;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 21279
    :cond_2
    iget-object v0, p0, Lhcg;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 21281
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21249
    if-ne p1, p0, :cond_1

    .line 21255
    :cond_0
    :goto_0
    return v0

    .line 21250
    :cond_1
    instance-of v2, p1, Lhcg;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 21251
    :cond_2
    check-cast p1, Lhcg;

    .line 21252
    iget-object v2, p0, Lhcg;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhcg;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhcg;->b:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhcg;->b:Lhgz;

    if-nez v2, :cond_3

    .line 21253
    :goto_2
    iget-object v2, p0, Lhcg;->c:Lhbt;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhcg;->c:Lhbt;

    if-nez v2, :cond_3

    .line 21254
    :goto_3
    iget-object v2, p0, Lhcg;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhcg;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 21255
    goto :goto_0

    .line 21252
    :cond_4
    iget-object v2, p0, Lhcg;->a:Lhgz;

    iget-object v3, p1, Lhcg;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhcg;->b:Lhgz;

    iget-object v3, p1, Lhcg;->b:Lhgz;

    .line 21253
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhcg;->c:Lhbt;

    iget-object v3, p1, Lhcg;->c:Lhbt;

    .line 21254
    invoke-virtual {v2, v3}, Lhbt;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhcg;->I:Ljava/util/List;

    iget-object v3, p1, Lhcg;->I:Ljava/util/List;

    .line 21255
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21259
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 21261
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcg;->a:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 21262
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcg;->b:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 21263
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhcg;->c:Lhbt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 21264
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhcg;->I:Ljava/util/List;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 21265
    return v0

    .line 21261
    :cond_0
    iget-object v0, p0, Lhcg;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 21262
    :cond_1
    iget-object v0, p0, Lhcg;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 21263
    :cond_2
    iget-object v0, p0, Lhcg;->c:Lhbt;

    invoke-virtual {v0}, Lhbt;->hashCode()I

    move-result v0

    goto :goto_2

    .line 21264
    :cond_3
    iget-object v1, p0, Lhcg;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
