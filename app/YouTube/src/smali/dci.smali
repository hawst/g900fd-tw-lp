.class final Ldci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lddw;


# instance fields
.field private synthetic a:Ldcd;


# direct methods
.method constructor <init>(Ldcd;)V
    .locals 0

    .prologue
    .line 977
    iput-object p1, p0, Ldci;->a:Ldcd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 981
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->b(Ldcd;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    .line 982
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->g(Ldcd;)Ldch;

    move-result-object v0

    invoke-virtual {v0}, Ldch;->a()V

    .line 983
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->b(Ldcd;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    iput p1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->g:I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 988
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 998
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->h(Ldcd;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_1

    .line 999
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->b()V

    .line 1005
    :cond_0
    :goto_0
    return-void

    .line 1000
    :cond_1
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->h(Ldcd;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->c:Ldbs;

    if-ne v0, v1, :cond_2

    .line 1001
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->a()V

    goto :goto_0

    .line 1002
    :cond_2
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->h(Ldcd;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->g:Ldbs;

    if-ne v0, v1, :cond_0

    .line 1003
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->l()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 992
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->b(Ldcd;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    .line 993
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->g(Ldcd;)Ldch;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldch;->a(I)V

    .line 994
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1009
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->h(Ldcd;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->c:Ldbs;

    if-ne v0, v1, :cond_1

    .line 1010
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->a()V

    .line 1014
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->h(Ldcd;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->g:Ldbs;

    if-ne v0, v1, :cond_0

    .line 1012
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->l()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1018
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->h(Ldcd;)Ldbs;

    move-result-object v0

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_0

    .line 1019
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->b()V

    .line 1021
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1025
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->i(Ldcd;)Ldea;

    move-result-object v0

    invoke-interface {v0}, Ldea;->d()V

    .line 1026
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1030
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->d()V

    .line 1031
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1035
    iget-object v0, p0, Ldci;->a:Ldcd;

    invoke-static {v0}, Ldcd;->a(Ldcd;)Ldbt;

    move-result-object v0

    invoke-interface {v0}, Ldbt;->e()V

    .line 1036
    return-void
.end method
