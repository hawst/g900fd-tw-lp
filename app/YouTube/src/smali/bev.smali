.class public Lbev;
.super Lbdx;
.source "SourceFile"

# interfaces
.implements Lfrz;
.implements Lfve;


# instance fields
.field private X:Letc;

.field private Y:Lffi;

.field private Z:Landroid/provider/SearchRecentSuggestions;

.field private a:Lari;

.field private aa:Landroid/content/SharedPreferences;

.field private ab:Lerv;

.field private ac:Lfdw;

.field private ad:Lfqg;

.field private ae:Ljava/lang/String;

.field private af:Ljava/lang/String;

.field private ag:Landroid/view/View;

.field private ah:Landroid/widget/TextView;

.field private ai:Lfvh;

.field private aj:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

.field private ak:Leyt;

.field private al:Lcao;

.field private am:Lhog;

.field private an:Lbnf;

.field private ao:Lbvc;

.field private ap:Lbvc;

.field private aq:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Lbdx;-><init>()V

    .line 497
    return-void
.end method

.method private A()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 407
    iput-boolean v4, p0, Lbev;->aq:Z

    .line 410
    invoke-virtual {p0}, Lbev;->F()Lhog;

    move-result-object v0

    .line 411
    invoke-static {v0}, Lbbp;->a(Lhog;)[B

    move-result-object v1

    .line 413
    iget-object v2, p0, Lbev;->Y:Lffi;

    invoke-virtual {v2}, Lffi;->a()Lffn;

    move-result-object v2

    .line 414
    iget-object v3, p0, Lbev;->ae:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lffn;->a(Ljava/lang/String;)Lffn;

    .line 415
    iget-object v3, p0, Lbev;->a:Lari;

    invoke-virtual {v3}, Lari;->f()Larh;

    move-result-object v3

    invoke-virtual {v3}, Larh;->af()Z

    move-result v3

    if-nez v3, :cond_0

    .line 416
    iget-object v3, p0, Lbev;->a:Lari;

    invoke-virtual {v3}, Lari;->aF()Lgjk;

    move-result-object v3

    iget-boolean v3, v3, Lgjk;->a:Z

    if-eqz v3, :cond_2

    iget-object v3, v2, Lffn;->a:Lhgt;

    const/4 v4, 0x3

    iput v4, v3, Lhgt;->i:I

    .line 418
    :cond_0
    :goto_0
    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-object v3, v3, Lbnf;->b:Lbnc;

    iget-object v3, v3, Lbnc;->b:Lffl;

    iget-object v4, v2, Lffn;->a:Lhgt;

    iget v3, v3, Lffl;->d:I

    iput v3, v4, Lhgt;->b:I

    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-object v3, v3, Lbnf;->c:Lbnh;

    iget-object v3, v3, Lbnh;->b:Lffo;

    iget-object v4, v2, Lffn;->a:Lhgt;

    iget v3, v3, Lffo;->e:I

    iput v3, v4, Lhgt;->a:I

    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-object v3, v3, Lbnf;->d:Lbnd;

    iget-object v3, v3, Lbnd;->b:Lffk;

    iget-object v4, v2, Lffn;->a:Lhgt;

    iget v3, v3, Lffk;->d:I

    iput v3, v4, Lhgt;->c:I

    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-boolean v3, v3, Lbnf;->e:Z

    iget-object v4, v2, Lffn;->a:Lhgt;

    iput-boolean v3, v4, Lhgt;->d:Z

    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-boolean v3, v3, Lbnf;->f:Z

    iget-object v4, v2, Lffn;->a:Lhgt;

    iput-boolean v3, v4, Lhgt;->e:Z

    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-boolean v3, v3, Lbnf;->g:Z

    iget-object v4, v2, Lffn;->a:Lhgt;

    iput-boolean v3, v4, Lhgt;->f:Z

    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-boolean v3, v3, Lbnf;->h:Z

    iget-object v4, v2, Lffn;->a:Lhgt;

    iput-boolean v3, v4, Lhgt;->g:Z

    iget-object v3, p0, Lbev;->an:Lbnf;

    iget-boolean v3, v3, Lbnf;->i:Z

    iget-object v4, v2, Lffn;->a:Lhgt;

    iput-boolean v3, v4, Lhgt;->h:Z

    .line 419
    invoke-virtual {v2, v1}, Lffn;->a([B)V

    .line 420
    if-eqz v0, :cond_1

    iget-object v1, v0, Lhog;->g:Lhuh;

    if-eqz v1, :cond_1

    .line 421
    iget-object v1, v0, Lhog;->g:Lhuh;

    iget-object v1, v1, Lhuh;->b:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lffn;->b(Ljava/lang/String;)Lffn;

    .line 423
    :cond_1
    iget-object v1, p0, Lbev;->aj:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(I)V

    .line 424
    iget-object v1, p0, Lbev;->Y:Lffi;

    new-instance v3, Lbez;

    invoke-direct {v3, p0, v0}, Lbez;-><init>(Lbev;Lhog;)V

    invoke-virtual {v1, v2, v3}, Lffi;->a(Lffn;Lwv;)V

    .line 450
    return-void

    .line 416
    :cond_2
    iget-object v3, v2, Lffn;->a:Lhgt;

    iput v4, v3, Lhgt;->i:I

    goto :goto_0
.end method

.method static synthetic a(Lbev;)Lcao;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->al:Lcao;

    return-object v0
.end method

.method static synthetic a(Lbev;Lfqg;)Lfqg;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lbev;->ad:Lfqg;

    return-object p1
.end method

.method static synthetic a(Lbev;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lbev;->af:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lbev;Lbnf;)V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->an:Lbnf;

    invoke-virtual {v0, p1}, Lbnf;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lbev;->an:Lbnf;

    invoke-direct {p0}, Lbev;->A()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lbev;)Letc;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->X:Letc;

    return-object v0
.end method

.method static synthetic c(Lbev;)Lfqg;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->ad:Lfqg;

    return-object v0
.end method

.method static synthetic d(Lbev;)Lfdw;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->ac:Lfdw;

    return-object v0
.end method

.method static synthetic e(Lbev;)Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->aj:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    return-object v0
.end method

.method static synthetic f(Lbev;)Lfvh;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->ai:Lfvh;

    return-object v0
.end method

.method static synthetic g(Lbev;)Leyt;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->ak:Leyt;

    return-object v0
.end method

.method static synthetic h(Lbev;)Lbnf;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbev;->an:Lbnf;

    return-object v0
.end method


# virtual methods
.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lbev;->ad:Lfqg;

    return-object v0
.end method

.method public final F()Lhog;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lbev;->am:Lhog;

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lbev;->am:Lhog;

    .line 332
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    goto :goto_0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 21

    .prologue
    .line 163
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lbev;->ad:Lfqg;

    .line 164
    const v2, 0x7f0400f6

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    .line 165
    const v2, 0x7f0800fd

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbev;->aj:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->aj:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->a(Lfve;)V

    .line 168
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->aj:Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;

    const v3, 0x7f0802d6

    invoke-virtual {v2, v3}, Lcom/google/android/apps/youtube/app/ui/LoadingFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v19, v2

    check-cast v19, Landroid/widget/ListView;

    .line 169
    const v2, 0x7f0400d3

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 171
    new-instance v2, Lfuu;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->a:Lari;

    .line 173
    invoke-virtual {v4}, Lari;->f()Larh;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 174
    iget-object v5, v5, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbev;->X:Letc;

    .line 175
    invoke-virtual {v6}, Letc;->i()Levn;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lbev;->a:Lari;

    .line 176
    invoke-virtual {v7}, Lari;->c()Leyp;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lbev;->a:Lari;

    .line 177
    invoke-virtual {v8}, Lari;->aD()Lcst;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lbev;->a:Lari;

    .line 178
    invoke-virtual {v9}, Lari;->aO()Lcub;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lbev;->ab:Lerv;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbev;->a:Lari;

    .line 180
    invoke-virtual {v11}, Lari;->F()Lffs;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lbev;->a:Lari;

    .line 181
    iget-object v12, v12, Lari;->ai:Ljava/util/concurrent/atomic/AtomicReference;

    move-object/from16 v0, p0

    iget-object v13, v0, Lbev;->ak:Leyt;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbev;->ao:Lbvc;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbev;->ap:Lbvc;

    move-object/from16 v0, p0

    iget-object v0, v0, Lbev;->ac:Lfdw;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    move-object/from16 v17, v0

    .line 187
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->j:Lfun;

    move-object/from16 v18, v0

    move-object/from16 v17, p0

    invoke-direct/range {v2 .. v18}, Lfuu;-><init>(Lcom/google/android/apps/youtube/app/WatchWhileActivity;Lcyc;Lfhz;Levn;Leyp;Lgix;Lcub;Lerv;Lffs;Ljava/util/concurrent/atomic/AtomicReference;Leyt;Lbvc;Lbvc;Lfdw;Lfrz;Lfun;)V

    .line 189
    new-instance v3, Lcew;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v5, Lcie;

    invoke-direct {v5}, Lcie;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lbev;->X:Letc;

    .line 192
    invoke-virtual {v6}, Letc;->i()Levn;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcew;-><init>(Landroid/content/Context;Lfsj;Levn;)V

    .line 193
    new-instance v11, Lawk;

    invoke-direct {v11, v3}, Lawk;-><init>(Lcew;)V

    .line 195
    new-instance v3, Lfuw;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 197
    iget-object v4, v4, Lbhz;->m:Lfus;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbev;->Y:Lffi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbev;->X:Letc;

    .line 199
    invoke-virtual {v6}, Letc;->i()Levn;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v9, v0, Lbev;->ak:Leyt;

    new-instance v10, Lbew;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lbew;-><init>(Lbev;)V

    move-object v7, v2

    move-object/from16 v8, p0

    invoke-direct/range {v3 .. v10}, Lfuw;-><init>(Lfus;Lfdg;Levn;Lfuu;Lfrz;Leyt;Lful;)V

    .line 209
    new-instance v4, Lfvh;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbev;->Y:Lffi;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->X:Letc;

    .line 213
    invoke-virtual {v2}, Letc;->i()Levn;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v10, v0, Lbev;->ak:Leyt;

    move-object/from16 v5, v19

    move-object v6, v11

    move-object v9, v3

    invoke-direct/range {v4 .. v10}, Lfvh;-><init>(Landroid/widget/ListView;Lfvd;Lfdg;Levn;Lfuw;Leyt;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lbev;->ai:Lfvh;

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->ai:Lfvh;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lbev;->a(Lezl;)V

    .line 218
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->ae:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 219
    if-eqz p3, :cond_5

    :goto_0
    const-string v2, "search_query"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "search_filters"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lbnf;

    const/4 v3, 0x0

    const-string v5, "navigation_endpoint"

    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {v5}, Lfia;->a([B)Lhog;

    move-result-object v3

    :cond_0
    move-object/from16 v0, p0

    iput-object v4, v0, Lbev;->ae:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lbev;->ae:Ljava/lang/String;

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->ah:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->ah:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbev;->ae:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->al:Lcao;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->al:Lcao;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbev;->ae:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcao;->a(Ljava/lang/String;)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->aa:Landroid/content/SharedPreferences;

    const-string v5, "no_search_history"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->Z:Landroid/provider/SearchRecentSuggestions;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbev;->ae:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/provider/SearchRecentSuggestions;->saveRecentQuery(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    move-object/from16 v0, p0

    iput-object v2, v0, Lbev;->an:Lbnf;

    move-object/from16 v0, p0

    iput-object v3, v0, Lbev;->am:Lhog;

    invoke-virtual/range {p0 .. p0}, Lbev;->n()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-direct/range {p0 .. p0}, Lbev;->A()V

    .line 224
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->d()Lkm;

    move-result-object v2

    invoke-virtual {v2}, Lkm;->b()Landroid/content/Context;

    move-result-object v2

    .line 225
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 231
    const v3, 0x7f04001f

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbev;->ag:Landroid/view/View;

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->ag:Landroid/view/View;

    const v3, 0x7f0800bd

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbev;->ah:Landroid/widget/TextView;

    .line 233
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->ah:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbev;->ae:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->ah:Landroid/widget/TextView;

    new-instance v3, Lbex;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbex;-><init>(Lbev;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lbev;->ag:Landroid/view/View;

    const v3, 0x7f0800be

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 241
    new-instance v3, Lbey;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lbey;-><init>(Lbev;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    return-object v20

    .line 219
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lbev;->h()Landroid/os/Bundle;

    move-result-object p3

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lbev;->ae:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, ""

    move-object/from16 v0, p0

    iput-object v4, v0, Lbev;->ae:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbev;->aq:Z

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0, p1}, Lbdx;->a(Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lbev;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v0

    iput-object v0, p0, Lbev;->a:Lari;

    .line 117
    invoke-virtual {p0}, Lbev;->E()Lcom/google/android/apps/youtube/app/YouTubeApplication;

    move-result-object v0

    iget-object v0, v0, Lckz;->a:Letc;

    iput-object v0, p0, Lbev;->X:Letc;

    .line 118
    iget-object v0, p0, Lbev;->a:Lari;

    invoke-virtual {v0}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lbev;->ak:Leyt;

    .line 119
    iget-object v0, p0, Lbev;->a:Lari;

    iget-object v0, v0, Lari;->v:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lffi;

    iput-object v0, p0, Lbev;->Y:Lffi;

    .line 120
    iget-object v0, p0, Lbev;->a:Lari;

    iget-object v0, v0, Lari;->c:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/provider/SearchRecentSuggestions;

    iput-object v0, p0, Lbev;->Z:Landroid/provider/SearchRecentSuggestions;

    .line 121
    iget-object v0, p0, Lbev;->X:Letc;

    invoke-virtual {v0}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lbev;->aa:Landroid/content/SharedPreferences;

    .line 122
    iget-object v0, p0, Lbev;->a:Lari;

    invoke-virtual {v0}, Lari;->ae()Lfdw;

    move-result-object v0

    iput-object v0, p0, Lbev;->ac:Lfdw;

    .line 125
    new-instance v0, Lerv;

    iget-object v1, p0, Lbev;->a:Lari;

    invoke-virtual {v1}, Lari;->C()Lgjp;

    move-result-object v1

    invoke-direct {v0, v1}, Lerv;-><init>(Lgjp;)V

    iput-object v0, p0, Lbev;->ab:Lerv;

    .line 127
    iget-object v0, p0, Lbev;->a:Lari;

    .line 129
    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    iget-object v1, p0, Lbev;->ak:Leyt;

    iget-object v2, p0, Lbev;->a:Lari;

    .line 131
    iget-object v2, v2, Lari;->b:Ldov;

    invoke-virtual {v2}, Ldov;->g()Ldaq;

    move-result-object v2

    .line 128
    invoke-static {v0, v1, v2}, Lbvc;->a(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v0

    iput-object v0, p0, Lbev;->ao:Lbvc;

    .line 132
    iget-object v0, p0, Lbev;->a:Lari;

    .line 134
    invoke-virtual {v0}, Lari;->K()Ldsn;

    move-result-object v0

    iget-object v1, p0, Lbev;->ak:Leyt;

    iget-object v2, p0, Lbev;->a:Lari;

    .line 136
    iget-object v2, v2, Lari;->b:Ldov;

    invoke-virtual {v2}, Ldov;->g()Ldaq;

    move-result-object v2

    .line 133
    invoke-static {v0, v1, v2}, Lbvc;->b(Ldsn;Leyt;Ldaq;)Lbvc;

    move-result-object v0

    iput-object v0, p0, Lbev;->ap:Lbvc;

    .line 138
    const/4 v0, 0x0

    .line 139
    if-eqz p1, :cond_1

    .line 140
    const-string v0, "navigation_endpoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 147
    :cond_0
    :goto_0
    invoke-static {v0}, Lfia;->a([B)Lhog;

    move-result-object v0

    iput-object v0, p0, Lbev;->am:Lhog;

    .line 148
    return-void

    .line 142
    :cond_1
    invoke-virtual {p0}, Lbev;->h()Landroid/os/Bundle;

    move-result-object v1

    .line 143
    if-eqz v1, :cond_0

    .line 144
    const-string v0, "navigation_endpoint"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Lcan;
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Lbev;->W:Lcan;

    if-nez v0, :cond_0

    .line 310
    invoke-virtual {p0}, Lbev;->k()Landroid/content/res/Resources;

    move-result-object v0

    .line 311
    iget-object v1, p0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->f:Lcaq;

    invoke-virtual {v1}, Lcaq;->h()Lcar;

    move-result-object v1

    iget-object v2, p0, Lbev;->ag:Landroid/view/View;

    .line 312
    iput-object v2, v1, Lcar;->b:Landroid/view/View;

    const v2, 0x7f0700d8

    .line 313
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, v1, Lcar;->c:I

    .line 314
    invoke-virtual {p0}, Lbev;->k()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0700d9

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, v1, Lcar;->d:I

    const v0, 0x7f0d013b

    .line 315
    iput v0, v1, Lcar;->e:I

    new-instance v0, Lbfa;

    invoke-direct {v0, p0}, Lbfa;-><init>(Lbev;)V

    .line 316
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcar;->a(Ljava/util/Collection;)Lcar;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Lcar;->a()Lcaq;

    move-result-object v0

    iput-object v0, p0, Lbev;->W:Lcan;

    .line 319
    :cond_0
    iget-object v0, p0, Lbev;->W:Lcan;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lbev;->af:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0, p1}, Lbdx;->d(Landroid/os/Bundle;)V

    .line 157
    iget-object v0, p0, Lbev;->b:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->h:Lcao;

    iput-object v0, p0, Lbev;->al:Lcao;

    .line 158
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 254
    invoke-super {p0}, Lbdx;->e()V

    .line 255
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 291
    invoke-super {p0, p1}, Lbdx;->e(Landroid/os/Bundle;)V

    .line 292
    const-string v0, "search_query"

    iget-object v1, p0, Lbev;->ae:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string v0, "search_filters"

    iget-object v1, p0, Lbev;->an:Lbnf;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 295
    iget-object v0, p0, Lbev;->am:Lhog;

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "navigation_endpoint"

    iget-object v1, p0, Lbev;->am:Lhog;

    .line 297
    invoke-static {v1}, Lidh;->a(Lidh;)[B

    move-result-object v1

    .line 296
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 299
    :cond_0
    return-void
.end method

.method public final o_()V
    .locals 0

    .prologue
    .line 324
    invoke-direct {p0}, Lbev;->A()V

    .line 325
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 303
    invoke-super {p0, p1}, Lbdx;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 304
    iget-object v0, p0, Lbev;->ai:Lfvh;

    invoke-virtual {v0, p1}, Lfvh;->a(Landroid/content/res/Configuration;)V

    .line 305
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 259
    invoke-super {p0}, Lbdx;->t()V

    .line 262
    iget-object v0, p0, Lbev;->al:Lcao;

    iget-object v1, p0, Lbev;->ae:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcao;->a(Ljava/lang/String;)V

    .line 267
    iget-boolean v0, p0, Lbev;->aq:Z

    if-eqz v0, :cond_0

    .line 268
    invoke-direct {p0}, Lbev;->A()V

    .line 270
    :cond_0
    iget-object v0, p0, Lbev;->ao:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 271
    iget-object v0, p0, Lbev;->ap:Lbvc;

    invoke-virtual {v0}, Lbvc;->b()V

    .line 272
    return-void
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0}, Lbdx;->u()V

    .line 279
    iget-object v0, p0, Lbev;->al:Lcao;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcao;->a(Ljava/lang/String;)V

    .line 280
    return-void
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 284
    invoke-super {p0}, Lbdx;->v()V

    .line 285
    iget-object v0, p0, Lbev;->ao:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 286
    iget-object v0, p0, Lbev;->ap:Lbvc;

    invoke-virtual {v0}, Lbvc;->a()V

    .line 287
    return-void
.end method
