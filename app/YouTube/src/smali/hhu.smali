.class public final Lhhu;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhog;

.field public b:Ljava/lang/String;

.field public c:Lhxf;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Lhiq;

.field public g:[B

.field public h:Lhig;

.field private i:Lhhq;

.field private j:Lhgz;

.field private k:Lhgz;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63459
    invoke-direct {p0}, Lidf;-><init>()V

    .line 63462
    iput-object v1, p0, Lhhu;->a:Lhog;

    .line 63465
    const-string v0, ""

    iput-object v0, p0, Lhhu;->b:Ljava/lang/String;

    .line 63468
    iput-object v1, p0, Lhhu;->c:Lhxf;

    .line 63471
    const/4 v0, 0x0

    iput v0, p0, Lhhu;->d:I

    .line 63474
    iput-object v1, p0, Lhhu;->i:Lhhq;

    .line 63477
    const-string v0, ""

    iput-object v0, p0, Lhhu;->e:Ljava/lang/String;

    .line 63480
    iput-object v1, p0, Lhhu;->f:Lhiq;

    .line 63483
    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhhu;->g:[B

    .line 63486
    iput-object v1, p0, Lhhu;->j:Lhgz;

    .line 63489
    iput-object v1, p0, Lhhu;->k:Lhgz;

    .line 63492
    iput-object v1, p0, Lhhu;->h:Lhig;

    .line 63459
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 63594
    const/4 v0, 0x0

    .line 63595
    iget-object v1, p0, Lhhu;->a:Lhog;

    if-eqz v1, :cond_0

    .line 63596
    const/4 v0, 0x1

    iget-object v1, p0, Lhhu;->a:Lhog;

    .line 63597
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 63599
    :cond_0
    iget-object v1, p0, Lhhu;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 63600
    const/4 v1, 0x2

    iget-object v2, p0, Lhhu;->b:Ljava/lang/String;

    .line 63601
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63603
    :cond_1
    iget-object v1, p0, Lhhu;->c:Lhxf;

    if-eqz v1, :cond_2

    .line 63604
    const/4 v1, 0x3

    iget-object v2, p0, Lhhu;->c:Lhxf;

    .line 63605
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63607
    :cond_2
    iget v1, p0, Lhhu;->d:I

    if-eqz v1, :cond_3

    .line 63608
    const/4 v1, 0x4

    iget v2, p0, Lhhu;->d:I

    .line 63609
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 63611
    :cond_3
    iget-object v1, p0, Lhhu;->i:Lhhq;

    if-eqz v1, :cond_4

    .line 63612
    const/4 v1, 0x5

    iget-object v2, p0, Lhhu;->i:Lhhq;

    .line 63613
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63615
    :cond_4
    iget-object v1, p0, Lhhu;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 63616
    const/4 v1, 0x6

    iget-object v2, p0, Lhhu;->e:Ljava/lang/String;

    .line 63617
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63619
    :cond_5
    iget-object v1, p0, Lhhu;->f:Lhiq;

    if-eqz v1, :cond_6

    .line 63620
    const/4 v1, 0x7

    iget-object v2, p0, Lhhu;->f:Lhiq;

    .line 63621
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63623
    :cond_6
    iget-object v1, p0, Lhhu;->g:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 63624
    const/16 v1, 0x9

    iget-object v2, p0, Lhhu;->g:[B

    .line 63625
    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 63627
    :cond_7
    iget-object v1, p0, Lhhu;->j:Lhgz;

    if-eqz v1, :cond_8

    .line 63628
    const/16 v1, 0xa

    iget-object v2, p0, Lhhu;->j:Lhgz;

    .line 63629
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63631
    :cond_8
    iget-object v1, p0, Lhhu;->k:Lhgz;

    if-eqz v1, :cond_9

    .line 63632
    const/16 v1, 0xb

    iget-object v2, p0, Lhhu;->k:Lhgz;

    .line 63633
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63635
    :cond_9
    iget-object v1, p0, Lhhu;->h:Lhig;

    if-eqz v1, :cond_a

    .line 63636
    const/16 v1, 0xc

    iget-object v2, p0, Lhhu;->h:Lhig;

    .line 63637
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63639
    :cond_a
    iget-object v1, p0, Lhhu;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 63640
    iput v0, p0, Lhhu;->J:I

    .line 63641
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 63455
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhhu;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhhu;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhhu;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhhu;->a:Lhog;

    if-nez v0, :cond_2

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Lhhu;->a:Lhog;

    :cond_2
    iget-object v0, p0, Lhhu;->a:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhu;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhhu;->c:Lhxf;

    if-nez v0, :cond_3

    new-instance v0, Lhxf;

    invoke-direct {v0}, Lhxf;-><init>()V

    iput-object v0, p0, Lhhu;->c:Lhxf;

    :cond_3
    iget-object v0, p0, Lhhu;->c:Lhxf;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhhu;->d:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhhu;->i:Lhhq;

    if-nez v0, :cond_4

    new-instance v0, Lhhq;

    invoke-direct {v0}, Lhhq;-><init>()V

    iput-object v0, p0, Lhhu;->i:Lhhq;

    :cond_4
    iget-object v0, p0, Lhhu;->i:Lhhq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhhu;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lhhu;->f:Lhiq;

    if-nez v0, :cond_5

    new-instance v0, Lhiq;

    invoke-direct {v0}, Lhiq;-><init>()V

    iput-object v0, p0, Lhhu;->f:Lhiq;

    :cond_5
    iget-object v0, p0, Lhhu;->f:Lhiq;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhhu;->g:[B

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lhhu;->j:Lhgz;

    if-nez v0, :cond_6

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhhu;->j:Lhgz;

    :cond_6
    iget-object v0, p0, Lhhu;->j:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lhhu;->k:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhhu;->k:Lhgz;

    :cond_7
    iget-object v0, p0, Lhhu;->k:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lhhu;->h:Lhig;

    if-nez v0, :cond_8

    new-instance v0, Lhig;

    invoke-direct {v0}, Lhig;-><init>()V

    iput-object v0, p0, Lhhu;->h:Lhig;

    :cond_8
    iget-object v0, p0, Lhhu;->h:Lhig;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 63555
    iget-object v0, p0, Lhhu;->a:Lhog;

    if-eqz v0, :cond_0

    .line 63556
    const/4 v0, 0x1

    iget-object v1, p0, Lhhu;->a:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63558
    :cond_0
    iget-object v0, p0, Lhhu;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 63559
    const/4 v0, 0x2

    iget-object v1, p0, Lhhu;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 63561
    :cond_1
    iget-object v0, p0, Lhhu;->c:Lhxf;

    if-eqz v0, :cond_2

    .line 63562
    const/4 v0, 0x3

    iget-object v1, p0, Lhhu;->c:Lhxf;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63564
    :cond_2
    iget v0, p0, Lhhu;->d:I

    if-eqz v0, :cond_3

    .line 63565
    const/4 v0, 0x4

    iget v1, p0, Lhhu;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 63567
    :cond_3
    iget-object v0, p0, Lhhu;->i:Lhhq;

    if-eqz v0, :cond_4

    .line 63568
    const/4 v0, 0x5

    iget-object v1, p0, Lhhu;->i:Lhhq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63570
    :cond_4
    iget-object v0, p0, Lhhu;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 63571
    const/4 v0, 0x6

    iget-object v1, p0, Lhhu;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 63573
    :cond_5
    iget-object v0, p0, Lhhu;->f:Lhiq;

    if-eqz v0, :cond_6

    .line 63574
    const/4 v0, 0x7

    iget-object v1, p0, Lhhu;->f:Lhiq;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63576
    :cond_6
    iget-object v0, p0, Lhhu;->g:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    .line 63577
    const/16 v0, 0x9

    iget-object v1, p0, Lhhu;->g:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    .line 63579
    :cond_7
    iget-object v0, p0, Lhhu;->j:Lhgz;

    if-eqz v0, :cond_8

    .line 63580
    const/16 v0, 0xa

    iget-object v1, p0, Lhhu;->j:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63582
    :cond_8
    iget-object v0, p0, Lhhu;->k:Lhgz;

    if-eqz v0, :cond_9

    .line 63583
    const/16 v0, 0xb

    iget-object v1, p0, Lhhu;->k:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63585
    :cond_9
    iget-object v0, p0, Lhhu;->h:Lhig;

    if-eqz v0, :cond_a

    .line 63586
    const/16 v0, 0xc

    iget-object v1, p0, Lhhu;->h:Lhig;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 63588
    :cond_a
    iget-object v0, p0, Lhhu;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 63590
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 63513
    if-ne p1, p0, :cond_1

    .line 63527
    :cond_0
    :goto_0
    return v0

    .line 63514
    :cond_1
    instance-of v2, p1, Lhhu;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 63515
    :cond_2
    check-cast p1, Lhhu;

    .line 63516
    iget-object v2, p0, Lhhu;->a:Lhog;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhhu;->a:Lhog;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhhu;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhhu;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 63517
    :goto_2
    iget-object v2, p0, Lhhu;->c:Lhxf;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhhu;->c:Lhxf;

    if-nez v2, :cond_3

    .line 63518
    :goto_3
    iget v2, p0, Lhhu;->d:I

    iget v3, p1, Lhhu;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhhu;->i:Lhhq;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhhu;->i:Lhhq;

    if-nez v2, :cond_3

    .line 63520
    :goto_4
    iget-object v2, p0, Lhhu;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhhu;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 63521
    :goto_5
    iget-object v2, p0, Lhhu;->f:Lhiq;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhhu;->f:Lhiq;

    if-nez v2, :cond_3

    .line 63522
    :goto_6
    iget-object v2, p0, Lhhu;->g:[B

    iget-object v3, p1, Lhhu;->g:[B

    .line 63523
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhhu;->j:Lhgz;

    if-nez v2, :cond_a

    iget-object v2, p1, Lhhu;->j:Lhgz;

    if-nez v2, :cond_3

    .line 63524
    :goto_7
    iget-object v2, p0, Lhhu;->k:Lhgz;

    if-nez v2, :cond_b

    iget-object v2, p1, Lhhu;->k:Lhgz;

    if-nez v2, :cond_3

    .line 63525
    :goto_8
    iget-object v2, p0, Lhhu;->h:Lhig;

    if-nez v2, :cond_c

    iget-object v2, p1, Lhhu;->h:Lhig;

    if-nez v2, :cond_3

    .line 63526
    :goto_9
    iget-object v2, p0, Lhhu;->I:Ljava/util/List;

    if-nez v2, :cond_d

    iget-object v2, p1, Lhhu;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 63527
    goto :goto_0

    .line 63516
    :cond_4
    iget-object v2, p0, Lhhu;->a:Lhog;

    iget-object v3, p1, Lhhu;->a:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhhu;->b:Ljava/lang/String;

    iget-object v3, p1, Lhhu;->b:Ljava/lang/String;

    .line 63517
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhhu;->c:Lhxf;

    iget-object v3, p1, Lhhu;->c:Lhxf;

    .line 63518
    invoke-virtual {v2, v3}, Lhxf;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhhu;->i:Lhhq;

    iget-object v3, p1, Lhhu;->i:Lhhq;

    .line 63520
    invoke-virtual {v2, v3}, Lhhq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhhu;->e:Ljava/lang/String;

    iget-object v3, p1, Lhhu;->e:Ljava/lang/String;

    .line 63521
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhhu;->f:Lhiq;

    iget-object v3, p1, Lhhu;->f:Lhiq;

    .line 63522
    invoke-virtual {v2, v3}, Lhiq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    .line 63523
    :cond_a
    iget-object v2, p0, Lhhu;->j:Lhgz;

    iget-object v3, p1, Lhhu;->j:Lhgz;

    .line 63524
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lhhu;->k:Lhgz;

    iget-object v3, p1, Lhhu;->k:Lhgz;

    .line 63525
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_8

    :cond_c
    iget-object v2, p0, Lhhu;->h:Lhig;

    iget-object v3, p1, Lhhu;->h:Lhig;

    .line 63526
    invoke-virtual {v2, v3}, Lhig;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_9

    :cond_d
    iget-object v2, p0, Lhhu;->I:Ljava/util/List;

    iget-object v3, p1, Lhhu;->I:Ljava/util/List;

    .line 63527
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63531
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 63533
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->a:Lhog;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 63534
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 63535
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->c:Lhxf;

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 63536
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lhhu;->d:I

    add-int/2addr v0, v2

    .line 63537
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->i:Lhhq;

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 63538
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->e:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 63539
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->f:Lhiq;

    if-nez v0, :cond_6

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 63540
    iget-object v2, p0, Lhhu;->g:[B

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    .line 63546
    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhhu;->j:Lhgz;

    if-nez v0, :cond_8

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 63547
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->k:Lhgz;

    if-nez v0, :cond_9

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 63548
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhhu;->h:Lhig;

    if-nez v0, :cond_a

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 63549
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhhu;->I:Ljava/util/List;

    if-nez v2, :cond_b

    :goto_9
    add-int/2addr v0, v1

    .line 63550
    return v0

    .line 63533
    :cond_1
    iget-object v0, p0, Lhhu;->a:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_0

    .line 63534
    :cond_2
    iget-object v0, p0, Lhhu;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 63535
    :cond_3
    iget-object v0, p0, Lhhu;->c:Lhxf;

    invoke-virtual {v0}, Lhxf;->hashCode()I

    move-result v0

    goto :goto_2

    .line 63537
    :cond_4
    iget-object v0, p0, Lhhu;->i:Lhhq;

    invoke-virtual {v0}, Lhhq;->hashCode()I

    move-result v0

    goto :goto_3

    .line 63538
    :cond_5
    iget-object v0, p0, Lhhu;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 63539
    :cond_6
    iget-object v0, p0, Lhhu;->f:Lhiq;

    invoke-virtual {v0}, Lhiq;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_7
    move v2, v0

    move v0, v1

    .line 63542
    :goto_a
    iget-object v3, p0, Lhhu;->g:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 63543
    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhhu;->g:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    .line 63542
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 63546
    :cond_8
    iget-object v0, p0, Lhhu;->j:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_6

    .line 63547
    :cond_9
    iget-object v0, p0, Lhhu;->k:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_7

    .line 63548
    :cond_a
    iget-object v0, p0, Lhhu;->h:Lhig;

    invoke-virtual {v0}, Lhig;->hashCode()I

    move-result v0

    goto :goto_8

    .line 63549
    :cond_b
    iget-object v1, p0, Lhhu;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_9
.end method
