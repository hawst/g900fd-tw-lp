.class public final Lcor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 325
    new-instance v0, Lcos;

    invoke-direct {v0}, Lcos;-><init>()V

    sput-object v0, Lcor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 357
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcor;->a:Ljava/lang/String;

    .line 358
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcor;->b:J

    .line 359
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcor;->c:J

    .line 360
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcor;->d:Ljava/lang/String;

    .line 361
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcor;->e:J

    .line 362
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJLjava/lang/String;J)V
    .locals 0

    .prologue
    .line 348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 349
    iput-object p1, p0, Lcor;->a:Ljava/lang/String;

    .line 350
    iput-wide p2, p0, Lcor;->b:J

    .line 351
    iput-wide p4, p0, Lcor;->c:J

    .line 352
    iput-object p6, p0, Lcor;->d:Ljava/lang/String;

    .line 353
    iput-wide p7, p0, Lcor;->e:J

    .line 354
    return-void
.end method

.method public static synthetic a(Lcor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcor;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b(Lcor;)J
    .locals 2

    .prologue
    .line 323
    iget-wide v0, p0, Lcor;->b:J

    return-wide v0
.end method

.method public static synthetic c(Lcor;)J
    .locals 2

    .prologue
    .line 323
    iget-wide v0, p0, Lcor;->c:J

    return-wide v0
.end method

.method public static synthetic d(Lcor;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcor;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic e(Lcor;)J
    .locals 2

    .prologue
    .line 323
    iget-wide v0, p0, Lcor;->e:J

    return-wide v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcor;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 372
    iget-wide v0, p0, Lcor;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 373
    iget-wide v0, p0, Lcor;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 374
    iget-object v0, p0, Lcor;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 375
    iget-wide v0, p0, Lcor;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 376
    return-void
.end method
