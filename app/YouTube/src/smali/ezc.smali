.class public final enum Lezc;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lezc;

.field public static final enum b:Lezc;

.field private static final synthetic c:[Lezc;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lezc;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v2}, Lezc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lezc;->a:Lezc;

    new-instance v0, Lezc;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, Lezc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lezc;->b:Lezc;

    const/4 v0, 0x2

    new-array v0, v0, [Lezc;

    sget-object v1, Lezc;->a:Lezc;

    aput-object v1, v0, v2

    sget-object v1, Lezc;->b:Lezc;

    aput-object v1, v0, v3

    sput-object v0, Lezc;->c:[Lezc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lezc;
    .locals 1

    .prologue
    .line 17
    const-class v0, Lezc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lezc;

    return-object v0
.end method

.method public static values()[Lezc;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lezc;->c:[Lezc;

    invoke-virtual {v0}, [Lezc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lezc;

    return-object v0
.end method
