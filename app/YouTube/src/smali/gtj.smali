.class public final enum Lgtj;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lgtj;

.field public static final enum b:Lgtj;

.field public static final enum c:Lgtj;

.field private static final synthetic e:[Lgtj;


# instance fields
.field public d:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 72
    new-instance v0, Lgtj;

    const-string v1, "Begin"

    sget-object v2, Lgqq;->a:Lgqq;

    invoke-static {v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lgtj;-><init>(Ljava/lang/String;ILjava/util/Set;)V

    sput-object v0, Lgtj;->a:Lgtj;

    .line 73
    new-instance v0, Lgtj;

    const-string v1, "End"

    sget-object v2, Lgqq;->b:Lgqq;

    invoke-static {v2}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Lgtj;-><init>(Ljava/lang/String;ILjava/util/Set;)V

    sput-object v0, Lgtj;->b:Lgtj;

    .line 74
    new-instance v0, Lgtj;

    const-string v1, "Both"

    sget-object v2, Lgqq;->a:Lgqq;

    sget-object v3, Lgqq;->b:Lgqq;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lgtj;-><init>(Ljava/lang/String;ILjava/util/Set;)V

    sput-object v0, Lgtj;->c:Lgtj;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lgtj;

    sget-object v1, Lgtj;->a:Lgtj;

    aput-object v1, v0, v4

    sget-object v1, Lgtj;->b:Lgtj;

    aput-object v1, v0, v5

    sget-object v1, Lgtj;->c:Lgtj;

    aput-object v1, v0, v6

    sput-object v0, Lgtj;->e:[Lgtj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/util/Set;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 78
    iput-object p3, p0, Lgtj;->d:Ljava/util/Set;

    .line 79
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lgtj;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lgtj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lgtj;

    return-object v0
.end method

.method public static values()[Lgtj;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lgtj;->e:[Lgtj;

    invoke-virtual {v0}, [Lgtj;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lgtj;

    return-object v0
.end method
