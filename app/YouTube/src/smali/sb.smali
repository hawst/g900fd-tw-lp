.class public final Lsb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lsf;

.field private static final d:Z


# instance fields
.field final b:Landroid/content/Context;

.field final c:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 70
    const-string v0, "MediaRouter"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lsb;->d:Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lsb;->c:Ljava/util/ArrayList;

    .line 193
    iput-object p1, p0, Lsb;->b:Landroid/content/Context;

    .line 194
    return-void
.end method

.method public static a()Ljava/util/List;
    .locals 1

    .prologue
    .line 231
    invoke-static {}, Lsb;->d()V

    .line 232
    sget-object v0, Lsb;->a:Lsf;

    iget-object v0, v0, Lsf;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lsb;
    .locals 6

    .prologue
    .line 214
    if-nez p0, :cond_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_0
    invoke-static {}, Lsb;->d()V

    .line 219
    sget-object v0, Lsb;->a:Lsf;

    if-nez v0, :cond_1

    .line 220
    new-instance v0, Lsf;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lsf;-><init>(Landroid/content/Context;)V

    .line 221
    sput-object v0, Lsb;->a:Lsf;

    new-instance v1, Ltc;

    iget-object v2, v0, Lsf;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Ltc;-><init>(Landroid/content/Context;Ltf;)V

    iput-object v1, v0, Lsf;->h:Ltc;

    iget-object v0, v0, Lsf;->h:Ltc;

    iget-boolean v1, v0, Ltc;->c:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, v0, Ltc;->c:Z

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.PACKAGE_RESTARTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, v0, Ltc;->a:Landroid/content/Context;

    iget-object v3, v0, Ltc;->d:Landroid/content/BroadcastReceiver;

    const/4 v4, 0x0

    iget-object v5, v0, Ltc;->b:Landroid/os/Handler;

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    iget-object v1, v0, Ltc;->b:Landroid/os/Handler;

    iget-object v0, v0, Ltc;->e:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    :cond_1
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0, p0}, Lsf;->a(Landroid/content/Context;)Lsb;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 628
    if-nez p0, :cond_0

    .line 629
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "remoteControlClient must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 631
    :cond_0
    invoke-static {}, Lsb;->d()V

    .line 633
    sget-boolean v0, Lsb;->d:Z

    if-eqz v0, :cond_1

    .line 634
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "addRemoteControlClient: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 636
    :cond_1
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0, p0}, Lsf;->a(Ljava/lang/Object;)I

    move-result v1

    if-gez v1, :cond_2

    new-instance v1, Lsi;

    invoke-direct {v1, v0, p0}, Lsi;-><init>(Lsf;Ljava/lang/Object;)V

    iget-object v0, v0, Lsf;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    :cond_2
    return-void
.end method

.method public static a(Lrm;)V
    .locals 2

    .prologue
    .line 581
    if-nez p0, :cond_0

    .line 582
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "providerInstance must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 584
    :cond_0
    invoke-static {}, Lsb;->d()V

    .line 586
    sget-boolean v0, Lsb;->d:Z

    if-eqz v0, :cond_1

    .line 587
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "addProvider: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 589
    :cond_1
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0, p0}, Lsf;->a(Lrm;)V

    .line 590
    return-void
.end method

.method static a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 684
    if-eq p0, p1, :cond_0

    if-eqz p0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lrz;I)Z
    .locals 2

    .prologue
    .line 377
    if-nez p0, :cond_0

    .line 378
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_0
    invoke-static {}, Lsb;->d()V

    .line 382
    sget-object v0, Lsb;->a:Lsf;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lsf;->a(Lrz;I)Z

    move-result v0

    return v0
.end method

.method private b(Lsc;)I
    .locals 3

    .prologue
    .line 559
    iget-object v0, p0, Lsb;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 560
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 561
    iget-object v0, p0, Lsb;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsd;

    iget-object v0, v0, Lsd;->b:Lsc;

    if-ne v0, p1, :cond_0

    move v0, v1

    .line 565
    :goto_1
    return v0

    .line 560
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 565
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static b()Lsk;
    .locals 1

    .prologue
    .line 254
    invoke-static {}, Lsb;->d()V

    .line 255
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0}, Lsf;->a()Lsk;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 646
    if-nez p0, :cond_0

    .line 647
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "remoteControlClient must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 650
    :cond_0
    sget-boolean v0, Lsb;->d:Z

    if-eqz v0, :cond_1

    .line 651
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "removeRemoteControlClient: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 653
    :cond_1
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0, p0}, Lsf;->a(Ljava/lang/Object;)I

    move-result v1

    if-ltz v1, :cond_2

    iget-object v0, v0, Lsf;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsi;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lsi;->b:Z

    iget-object v0, v0, Lsi;->a:Ltg;

    const/4 v1, 0x0

    iput-object v1, v0, Ltg;->b:Ltl;

    .line 654
    :cond_2
    return-void
.end method

.method public static c()Lsk;
    .locals 1

    .prologue
    .line 305
    invoke-static {}, Lsb;->d()V

    .line 306
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0}, Lsf;->b()Lsk;

    move-result-object v0

    return-object v0
.end method

.method static d()V
    .locals 2

    .prologue
    .line 677
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 678
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The media router service must only be accessed on the application\'s main thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 681
    :cond_0
    return-void
.end method

.method static synthetic e()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lsb;->d:Z

    return v0
.end method


# virtual methods
.method public final a(Lrz;Lsc;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 497
    if-nez p1, :cond_0

    .line 498
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    if-nez p2, :cond_1

    .line 501
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 503
    :cond_1
    invoke-static {}, Lsb;->d()V

    .line 505
    sget-boolean v0, Lsb;->d:Z

    if-eqz v0, :cond_2

    .line 506
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "addCallback: selector="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callback="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", flags="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    :cond_2
    invoke-direct {p0, p2}, Lsb;->b(Lsc;)I

    move-result v0

    .line 512
    if-gez v0, :cond_5

    .line 513
    new-instance v0, Lsd;

    invoke-direct {v0, p0, p2}, Lsd;-><init>(Lsb;Lsc;)V

    .line 514
    iget-object v1, p0, Lsb;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    :goto_0
    iget v1, v0, Lsd;->d:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v1, p3

    if-eqz v1, :cond_7

    .line 520
    iget v1, v0, Lsd;->d:I

    or-int/2addr v1, p3

    iput v1, v0, Lsd;->d:I

    move v1, v2

    .line 523
    :goto_1
    iget-object v4, v0, Lsd;->c:Lrz;

    if-eqz p1, :cond_3

    invoke-virtual {v4}, Lrz;->b()V

    invoke-virtual {p1}, Lrz;->b()V

    iget-object v3, v4, Lrz;->b:Ljava/util/List;

    iget-object v4, p1, Lrz;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v3

    :cond_3
    if-nez v3, :cond_6

    .line 524
    new-instance v1, Lsa;

    iget-object v3, v0, Lsd;->c:Lrz;

    invoke-direct {v1, v3}, Lsa;-><init>(Lrz;)V

    invoke-virtual {v1, p1}, Lsa;->a(Lrz;)Lsa;

    move-result-object v1

    invoke-virtual {v1}, Lsa;->a()Lrz;

    move-result-object v1

    iput-object v1, v0, Lsd;->c:Lrz;

    .line 529
    :goto_2
    if-eqz v2, :cond_4

    .line 530
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0}, Lsf;->c()V

    .line 532
    :cond_4
    return-void

    .line 516
    :cond_5
    iget-object v1, p0, Lsb;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsd;

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_2

    :cond_7
    move v1, v3

    goto :goto_1
.end method

.method public final a(Lsc;)V
    .locals 2

    .prologue
    .line 542
    if-nez p1, :cond_0

    .line 543
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 545
    :cond_0
    invoke-static {}, Lsb;->d()V

    .line 547
    sget-boolean v0, Lsb;->d:Z

    if-eqz v0, :cond_1

    .line 548
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "removeCallback: callback="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 551
    :cond_1
    invoke-direct {p0, p1}, Lsb;->b(Lsc;)I

    move-result v0

    .line 552
    if-ltz v0, :cond_2

    .line 553
    iget-object v1, p0, Lsb;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 554
    sget-object v0, Lsb;->a:Lsf;

    invoke-virtual {v0}, Lsf;->c()V

    .line 556
    :cond_2
    return-void
.end method
