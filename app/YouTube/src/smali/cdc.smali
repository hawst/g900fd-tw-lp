.class public final Lcdc;
.super Lcbx;
.source "SourceFile"


# instance fields
.field private final e:Lfsj;

.field private final f:Lboi;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;ILfhz;Lboi;Lfdw;Lfrz;)V
    .locals 7

    .prologue
    .line 42
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move v4, p4

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcbx;-><init>(Landroid/content/Context;Leyp;Lfhz;ILfdw;Lfrz;)V

    .line 49
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcdc;->e:Lfsj;

    .line 50
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcdc;->f:Lboi;

    .line 52
    invoke-interface {p3, p0}, Lfsj;->a(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcbx;->b:Landroid/view/View;

    invoke-interface {p3, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 54
    const/4 v0, 0x1

    invoke-interface {p3, v0}, Lfsj;->a(Z)V

    .line 55
    return-void
.end method

.method private a(Lfsg;Lfje;)Landroid/view/View;
    .locals 3

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcbx;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 61
    iget-object v0, p0, Lcbx;->c:Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/ui/PlaylistThumbnailView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcbx;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 62
    :cond_0
    invoke-virtual {p2}, Lfje;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcdc;->a(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p2, Lfje;->c:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    iget-object v0, p2, Lfje;->a:Lhdz;

    iget-object v0, v0, Lhdz;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfje;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v0, p2, Lfje;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcdc;->b(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p2, Lfje;->b:Lfnc;

    if-nez v0, :cond_2

    new-instance v0, Lfnc;

    iget-object v1, p2, Lfje;->a:Lhdz;

    iget-object v1, v1, Lhdz;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p2, Lfje;->b:Lfnc;

    :cond_2
    iget-object v0, p2, Lfje;->b:Lfnc;

    iget-object v1, p2, Lfje;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p2, Lfje;->a:Lhdz;

    iget-object v1, v1, Lhdz;->g:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfje;->d:Ljava/lang/CharSequence;

    :cond_3
    iget-object v1, p2, Lfje;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcdc;->a(Lfnc;Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcdc;->e:Lfsj;

    invoke-interface {v0}, Lfsj;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcdc;->f:Lboi;

    iget-object v2, p0, Lcbx;->d:Landroid/view/View;

    invoke-static {v0, v1, v2, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcdc;->e:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfje;

    invoke-direct {p0, p1, p2}, Lcdc;->a(Lfsg;Lfje;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfje;

    invoke-direct {p0, p1, p2}, Lcdc;->a(Lfsg;Lfje;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    check-cast p2, Lfje;

    invoke-direct {p0, p1, p2}, Lcdc;->a(Lfsg;Lfje;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
