.class public Lcde;
.super Lcby;
.source "SourceFile"


# instance fields
.field public final f:Landroid/content/res/Resources;

.field final g:Lfhz;

.field public final h:Landroid/widget/LinearLayout;

.field public final i:Landroid/widget/RelativeLayout;

.field j:Lhog;

.field private final m:Lboi;

.field private n:Lfsj;

.field private o:Landroid/widget/LinearLayout;

.field private p:Lfvi;

.field private q:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;ILboi;Lfdw;Lfrz;)V
    .locals 8

    .prologue
    .line 62
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p7

    move-object/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcby;-><init>(Landroid/content/Context;Leyp;Lfhz;Lfsj;ILfdw;Lfrz;)V

    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcde;->f:Landroid/content/res/Resources;

    .line 71
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcde;->n:Lfsj;

    .line 72
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfhz;

    iput-object v0, p0, Lcde;->g:Lfhz;

    .line 73
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboi;

    iput-object v0, p0, Lcde;->m:Lboi;

    .line 75
    iget-object v0, p0, Lcby;->c:Landroid/view/View;

    const v1, 0x7f080138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcde;->h:Landroid/widget/LinearLayout;

    .line 76
    iget-object v0, p0, Lcde;->h:Landroid/widget/LinearLayout;

    const v1, 0x7f08012c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcde;->i:Landroid/widget/RelativeLayout;

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Lfsg;Lfjf;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 81
    invoke-super {p0, p1, p2}, Lcby;->a(Lfsg;Lflb;)Landroid/view/View;

    .line 83
    invoke-virtual {p0, p1}, Lcde;->a(Lfsg;)V

    .line 84
    invoke-virtual {p2}, Lfjf;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcde;->a(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p2, Lfjf;->e:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p2, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->h:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p2, Lfjf;->e:Ljava/lang/CharSequence;

    :cond_0
    iget-object v0, p2, Lfjf;->e:Ljava/lang/CharSequence;

    iget-object v1, p2, Lfjf;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_3

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->d:Lhgz;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->d:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjf;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfjf;->b:Ljava/lang/CharSequence;

    if-nez v1, :cond_2

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->m:Lhgz;

    if-eqz v1, :cond_8

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->m:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjf;->b:Ljava/lang/CharSequence;

    :cond_2
    :goto_0
    iget-object v1, p2, Lfjf;->b:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p2, Lfjf;->c:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    iget-object v3, p2, Lfjf;->c:Ljava/lang/CharSequence;

    aput-object v3, v2, v5

    const/4 v3, 0x1

    const-string v4, " \u00b7 "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p2, Lfjf;->c:Ljava/lang/CharSequence;

    :cond_3
    :goto_1
    iget-object v1, p2, Lfjf;->c:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcde;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p2, Lfjf;->f:Lfnc;

    if-nez v0, :cond_4

    new-instance v0, Lfnc;

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->b:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p2, Lfjf;->f:Lfnc;

    :cond_4
    iget-object v0, p2, Lfjf;->f:Lfnc;

    iget-object v1, p2, Lfjf;->d:Ljava/lang/CharSequence;

    if-nez v1, :cond_5

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->f:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjf;->d:Ljava/lang/CharSequence;

    :cond_5
    iget-object v1, p2, Lfjf;->d:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0, v1}, Lcde;->a(Lfnc;Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcde;->n:Lfsj;

    invoke-interface {v0}, Lfsj;->a()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcde;->m:Lboi;

    iget-object v2, p0, Lcby;->e:Landroid/view/View;

    invoke-static {v0, v1, v2, p2}, La;->a(Landroid/view/View;Lboi;Landroid/view/View;Ljava/lang/Object;)V

    .line 88
    iget-object v0, p2, Lfjf;->h:Lfkm;

    if-nez v0, :cond_6

    iget-object v0, p2, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->o:Lhea;

    if-eqz v0, :cond_6

    iget-object v0, p2, Lfjf;->a:Lheb;

    iget-object v0, v0, Lheb;->o:Lhea;

    iget-object v0, v0, Lhea;->a:Lhlc;

    if-eqz v0, :cond_6

    new-instance v0, Lfkm;

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->o:Lhea;

    iget-object v1, v1, Lhea;->a:Lhlc;

    invoke-direct {v0, v1, p2}, Lfkm;-><init>(Lhlc;Lfqh;)V

    iput-object v0, p2, Lfjf;->h:Lfkm;

    :cond_6
    iget-object v1, p2, Lfjf;->h:Lfkm;

    if-nez v1, :cond_a

    iput-object v6, p0, Lcde;->j:Lhog;

    iget-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 90
    :cond_7
    :goto_2
    iget-object v0, p0, Lcde;->n:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 85
    :cond_8
    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->e:Lhgz;

    if-eqz v1, :cond_2

    iget-object v1, p2, Lfjf;->a:Lheb;

    iget-object v1, v1, Lheb;->e:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfjf;->b:Ljava/lang/CharSequence;

    goto/16 :goto_0

    :cond_9
    iput-object v1, p2, Lfjf;->c:Ljava/lang/CharSequence;

    goto/16 :goto_1

    .line 88
    :cond_a
    iget-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcby;->c:Landroid/view/View;

    const v2, 0x7f08013b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    const v2, 0x7f080206

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcde;->q:Landroid/widget/TextView;

    iget-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    const v2, 0x7f0800a4

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Lfvi;

    iget-object v3, p0, Lcby;->b:Leyp;

    invoke-direct {v2, v3, v0}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    iput-object v2, p0, Lcde;->p:Lfvi;

    iget-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    new-instance v2, Lcdf;

    invoke-direct {v2, p0}, Lcdf;-><init>(Lcde;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    iget-object v0, v1, Lfkm;->c:Lhlc;

    iget-object v0, v0, Lhlc;->c:Lhog;

    iput-object v0, p0, Lcde;->j:Lhog;

    iget-object v0, p0, Lcde;->p:Lfvi;

    iget-object v2, v1, Lfkm;->b:Lfnc;

    if-nez v2, :cond_c

    new-instance v2, Lfnc;

    iget-object v3, v1, Lfkm;->c:Lhlc;

    iget-object v3, v3, Lhlc;->b:Lhxf;

    invoke-direct {v2, v3}, Lfnc;-><init>(Lhxf;)V

    iput-object v2, v1, Lfkm;->b:Lfnc;

    :cond_c
    iget-object v2, v1, Lfkm;->b:Lfnc;

    invoke-virtual {v0, v2, v6}, Lfvi;->a(Lfnc;Leyo;)V

    iget-object v0, p0, Lcde;->q:Landroid/widget/TextView;

    iget-object v2, v1, Lfkm;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_d

    iget-object v2, v1, Lfkm;->c:Lhlc;

    iget-object v2, v2, Lhlc;->a:Lhgz;

    if-eqz v2, :cond_d

    iget-object v2, v1, Lfkm;->c:Lhlc;

    iget-object v2, v2, Lhlc;->a:Lhgz;

    invoke-static {v2}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v2

    iput-object v2, v1, Lfkm;->a:Ljava/lang/CharSequence;

    :cond_d
    iget-object v2, v1, Lfkm;->a:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lcde;->a(Lfqh;)V

    iget-object v0, p0, Lcde;->o:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public bridge synthetic a(Lfsg;Lflb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 38
    check-cast p2, Lfjf;

    invoke-virtual {p0, p1, p2}, Lcde;->a(Lfsg;Lfjf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 38
    check-cast p2, Lfjf;

    invoke-virtual {p0, p1, p2}, Lcde;->a(Lfsg;Lfjf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 38
    check-cast p2, Lfjf;

    invoke-virtual {p0, p1, p2}, Lcde;->a(Lfsg;Lfjf;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lfsg;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 97
    iget-object v0, p0, Lcde;->i:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 98
    iget-object v1, p0, Lcde;->f:Landroid/content/res/Resources;

    const v2, 0x7f0b0012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 101
    iget-object v1, p0, Lcby;->a:Landroid/content/Context;

    invoke-static {v1, p1}, La;->a(Landroid/content/Context;Lfsg;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcde;->h:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 103
    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 108
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v1, p0, Lcde;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 106
    iget-object v1, p0, Lcde;->f:Landroid/content/res/Resources;

    const v2, 0x7f0a008e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_0
.end method
