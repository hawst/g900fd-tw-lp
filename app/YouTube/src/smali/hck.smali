.class public final Lhck;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22000
    invoke-direct {p0}, Lidf;-><init>()V

    .line 22003
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhck;->a:Z

    .line 22000
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 22040
    const/4 v0, 0x0

    .line 22041
    iget-boolean v1, p0, Lhck;->a:Z

    if-eqz v1, :cond_0

    .line 22042
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhck;->a:Z

    .line 22043
    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 22045
    :cond_0
    iget-object v1, p0, Lhck;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22046
    iput v0, p0, Lhck;->J:I

    .line 22047
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 21996
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhck;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhck;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhck;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhck;->a:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 22031
    iget-boolean v0, p0, Lhck;->a:Z

    if-eqz v0, :cond_0

    .line 22032
    const/4 v0, 0x1

    iget-boolean v1, p0, Lhck;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 22034
    :cond_0
    iget-object v0, p0, Lhck;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 22036
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22014
    if-ne p1, p0, :cond_1

    .line 22018
    :cond_0
    :goto_0
    return v0

    .line 22015
    :cond_1
    instance-of v2, p1, Lhck;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 22016
    :cond_2
    check-cast p1, Lhck;

    .line 22017
    iget-boolean v2, p0, Lhck;->a:Z

    iget-boolean v3, p1, Lhck;->a:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhck;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhck;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 22018
    goto :goto_0

    .line 22017
    :cond_4
    iget-object v2, p0, Lhck;->I:Ljava/util/List;

    iget-object v3, p1, Lhck;->I:Ljava/util/List;

    .line 22018
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 22022
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 22024
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lhck;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 22025
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhck;->I:Ljava/util/List;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 22026
    return v0

    .line 22024
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 22025
    :cond_1
    iget-object v0, p0, Lhck;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_1
.end method
