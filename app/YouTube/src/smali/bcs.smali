.class public final Lbcs;
.super Lj;
.source "SourceFile"

# interfaces
.implements Lbno;
.implements Lfrz;


# instance fields
.field private W:Landroid/view/View;

.field private X:Landroid/widget/ProgressBar;

.field private Y:Landroid/widget/LinearLayout;

.field private Z:Landroid/widget/ImageView;

.field private a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

.field private aa:Landroid/widget/TextView;

.field private ab:Landroid/widget/Button;

.field private ac:Lcyc;

.field private ad:Levn;

.field private ae:Leyp;

.field private af:Leyt;

.field private ag:Lfcs;

.field private ah:Lfdl;

.field private ai:Lglm;

.field private aj:Lcst;

.field private ak:Lcub;

.field private al:Lbku;

.field private am:Lfdw;

.field private an:Lfqg;

.field private ao:Lfhz;

.field private ap:Lbnn;

.field private aq:Lfpz;

.field private ar:Lfsi;

.field private as:Ljava/util/ArrayList;

.field private b:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lj;-><init>()V

    .line 713
    return-void
.end method

.method private A()V
    .locals 1

    .prologue
    .line 661
    const/4 v0, 0x0

    iput-object v0, p0, Lbcs;->aq:Lfpz;

    .line 662
    iget-object v0, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 663
    iget-object v0, p0, Lbcs;->as:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 664
    return-void
.end method

.method private C()V
    .locals 4

    .prologue
    .line 699
    iget-object v0, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v0}, Lfsi;->b()V

    .line 700
    iget-object v0, p0, Lbcs;->as:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 701
    instance-of v0, v1, Lfpz;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lfpz;

    iget-object v3, v0, Lfpz;->i:Lfqa;

    if-eqz v3, :cond_2

    iget-object v0, v0, Lfpz;->i:Lfqa;

    invoke-interface {v0}, Lfqa;->f()Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_0

    .line 702
    :cond_1
    iget-object v0, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v0, v1}, Lfsi;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 701
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 706
    :cond_3
    return-void
.end method

.method static synthetic a(Lbcs;)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    return-void
.end method

.method static synthetic a(Lbcs;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lbcs;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lbcs;Ljava/util/List;Lfqb;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lbcs;->a(Ljava/util/List;Lfqb;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 611
    iget-object v0, p0, Lbcs;->X:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 612
    iget-object v0, p0, Lbcs;->aa:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 613
    iget-object v0, p0, Lbcs;->Y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 614
    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 615
    iget-object v3, p0, Lbcs;->Z:Landroid/widget/ImageView;

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lbcs;->ab:Landroid/widget/Button;

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 617
    return-void

    :cond_0
    move v0, v2

    .line 615
    goto :goto_0

    :cond_1
    move v1, v2

    .line 616
    goto :goto_1
.end method

.method private a(Ljava/util/List;Lfqb;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 667
    invoke-direct {p0}, Lbcs;->A()V

    .line 668
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqe;

    .line 671
    iget-object v1, v0, Lfqe;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 672
    iget-object v1, p0, Lbcs;->as:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 675
    :cond_1
    iget-object v0, v0, Lfqe;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lfpz;

    .line 678
    instance-of v0, v1, Lfpy;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 679
    check-cast v0, Lfpy;

    .line 680
    iget-object v0, v0, Lfpy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfpz;

    .line 681
    iget-object v5, p0, Lbcs;->as:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 685
    :cond_2
    iget-object v0, p0, Lbcs;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 689
    :cond_3
    if-eqz p2, :cond_4

    .line 690
    iget-object v0, p0, Lbcs;->as:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    :cond_4
    invoke-direct {p0}, Lbcs;->C()V

    .line 694
    iget-object v0, p0, Lbcs;->X:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lbcs;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lbcs;->Y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0, v7}, Lbcs;->a(Z)V

    .line 695
    return-void
.end method

.method static synthetic b(Lbcs;)Lcom/google/android/apps/youtube/app/WatchWhileActivity;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 571
    invoke-direct {p0}, Lbcs;->z()Lfpx;

    move-result-object v0

    .line 572
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lfpx;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 573
    invoke-virtual {v0}, Lfpx;->c()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lfpx;->a()Lfqb;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lbcs;->a(Ljava/util/List;Lfqb;)V

    .line 580
    :goto_0
    return-void

    .line 575
    :cond_0
    invoke-direct {p0}, Lbcs;->A()V

    .line 578
    invoke-virtual {p0}, Lbcs;->k()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0901b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbcs;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic c(Lbcs;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 587
    iget-object v0, p0, Lbcs;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 589
    iget-object v0, p0, Lbcs;->Y:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 590
    iget-object v0, p0, Lbcs;->aa:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 591
    iget-object v0, p0, Lbcs;->X:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 592
    return-void
.end method

.method static synthetic d(Lbcs;)Lcyc;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->ac:Lcyc;

    return-object v0
.end method

.method static synthetic e(Lbcs;)Lfhz;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->ao:Lfhz;

    return-object v0
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 467
    iget-object v0, p0, Lbcs;->ak:Lcub;

    iget-object v0, v0, Lcub;->h:Lcul;

    invoke-virtual {v0}, Lcul;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    invoke-direct {p0}, Lbcs;->c()V

    .line 478
    :goto_0
    return-void

    .line 472
    :cond_0
    iget-object v0, p0, Lbcs;->ai:Lglm;

    invoke-interface {v0}, Lglm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 473
    invoke-direct {p0}, Lbcs;->c()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->f(Z)V

    invoke-direct {p0}, Lbcs;->b()V

    goto :goto_0

    .line 477
    :cond_1
    invoke-direct {p0}, Lbcs;->c()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbcs;->f(Z)V

    iget-object v0, p0, Lbcs;->ah:Lfdl;

    new-instance v1, Lfdn;

    iget-object v2, v0, Lfdl;->b:Lfsz;

    iget-object v0, v0, Lfdl;->c:Lgix;

    invoke-interface {v0}, Lgix;->d()Lgit;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lfdn;-><init>(Lfsz;Lgit;)V

    iput-boolean p1, v1, Lfdn;->a:Z

    if-eqz p1, :cond_2

    invoke-virtual {v1}, Lfdn;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lfsq;->b:Lfsq;

    invoke-virtual {v1, v0}, Lfdn;->a(Lfsq;)V

    :cond_2
    iget-object v0, p0, Lbcs;->ah:Lfdl;

    new-instance v2, Lbdc;

    invoke-direct {v2, p0}, Lbdc;-><init>(Lbcs;)V

    iget-object v0, v0, Lfdl;->e:Lfdm;

    invoke-virtual {v0, v1, v2}, Lfdm;->b(Lfsp;Lwv;)V

    goto :goto_0
.end method

.method static synthetic f(Lbcs;)Leyp;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->ae:Leyp;

    return-object v0
.end method

.method private f(Z)V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lbcs;->aj:Lcst;

    invoke-virtual {v0}, Lcst;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lbcs;->ap:Lbnn;

    iget-object v1, p0, Lbcs;->aj:Lcst;

    invoke-virtual {v1}, Lcst;->e()Lcua;

    move-result-object v1

    invoke-interface {v0, v1}, Lbnn;->a(Lcua;)V

    .line 519
    :goto_0
    return-void

    .line 513
    :cond_0
    if-eqz p1, :cond_1

    .line 514
    iget-object v0, p0, Lbcs;->ap:Lbnn;

    invoke-interface {v0}, Lbnn;->d()V

    goto :goto_0

    .line 516
    :cond_1
    iget-object v0, p0, Lbcs;->ap:Lbnn;

    invoke-interface {v0}, Lbnn;->c()V

    goto :goto_0
.end method

.method static synthetic g(Lbcs;)Lfdw;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->am:Lfdw;

    return-object v0
.end method

.method static synthetic h(Lbcs;)Lfsi;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->ar:Lfsi;

    return-object v0
.end method

.method static synthetic i(Lbcs;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lbcs;->C()V

    return-void
.end method

.method static synthetic j(Lbcs;)Leyt;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->af:Leyt;

    return-object v0
.end method

.method static synthetic k(Lbcs;)Lfqg;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->an:Lfqg;

    return-object v0
.end method

.method static synthetic l(Lbcs;)Lbku;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbcs;->al:Lbku;

    return-object v0
.end method

.method static synthetic m(Lbcs;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lbcs;->b()V

    return-void
.end method

.method private z()Lfpx;
    .locals 3

    .prologue
    .line 638
    :try_start_0
    iget-object v0, p0, Lbcs;->al:Lbku;

    invoke-virtual {v0}, Lbku;->a()Lfqd;

    move-result-object v0

    .line 639
    if-eqz v0, :cond_0

    .line 640
    iget-object v0, v0, Lfqd;->a:Lfpx;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 646
    :goto_0
    return-object v0

    .line 642
    :catch_0
    move-exception v0

    .line 643
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to get offline guide: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->b(Ljava/lang/String;)V

    .line 646
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final B()Lfqg;
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lbcs;->an:Lfqg;

    return-object v0
.end method

.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 167
    const v0, 0x7f04007e

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 170
    const v0, 0x7f0801fd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lbcs;->b:Landroid/widget/ListView;

    .line 172
    iget-object v0, p0, Lbcs;->ac:Lcyc;

    .line 173
    const v0, 0x7f04001c

    iget-object v2, p0, Lbcs;->b:Landroid/widget/ListView;

    .line 174
    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 175
    iget-object v2, p0, Lbcs;->b:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 176
    new-instance v2, Lbuu;

    iget-object v3, p0, Lbcs;->ae:Leyp;

    invoke-direct {v2, v0, v3, p0}, Lbuu;-><init>(Landroid/view/View;Leyp;Lbno;)V

    iput-object v2, p0, Lbcs;->ap:Lbnn;

    .line 180
    const v0, 0x7f0801fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbcs;->W:Landroid/view/View;

    .line 191
    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    const-string v2, "progressbar"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbcs;->X:Landroid/widget/ProgressBar;

    .line 192
    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    const-string v2, "error"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbcs;->Y:Landroid/widget/LinearLayout;

    .line 193
    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    const-string v2, "error_message"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbcs;->aa:Landroid/widget/TextView;

    .line 194
    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    const-string v2, "alert_error"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbcs;->Z:Landroid/widget/ImageView;

    .line 195
    iget-object v0, p0, Lbcs;->W:Landroid/view/View;

    const-string v2, "retry_button"

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbcs;->ab:Landroid/widget/Button;

    .line 196
    iget-object v0, p0, Lbcs;->ab:Landroid/widget/Button;

    new-instance v2, Lbct;

    invoke-direct {v2, p0}, Lbct;-><init>(Lbcs;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    return-object v1
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 341
    iget-object v0, p0, Lbcs;->ac:Lcyc;

    invoke-interface {v0}, Lcyc;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    new-instance v0, Lfcl;

    invoke-direct {v0}, Lfcl;-><init>()V

    .line 343
    invoke-virtual {p0}, Lbcs;->l()Lt;

    move-result-object v1

    const-string v2, "account switcher"

    invoke-virtual {v0, v1, v2}, Li;->a(Lt;Ljava/lang/String;)V

    .line 367
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-object v0, p0, Lbcs;->ak:Lcub;

    iget-object v1, p0, Lbcs;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    new-instance v2, Lbdb;

    invoke-direct {v2, p0}, Lbdb;-><init>(Lbcs;)V

    invoke-virtual {v0, v1, v2}, Lcub;->a(Landroid/app/Activity;Lcuk;)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 132
    invoke-super {p0, p1}, Lj;->a(Landroid/os/Bundle;)V

    .line 133
    invoke-virtual {p0}, Lbcs;->j()Lo;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-static {v0}, Lb;->b(Z)V

    .line 135
    invoke-virtual {p0}, Lbcs;->j()Lo;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iput-object v0, p0, Lbcs;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 137
    iget-object v0, p0, Lbcs;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 138
    iget-object v1, v0, Lckz;->a:Letc;

    .line 139
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v2

    .line 140
    invoke-virtual {v1}, Letc;->i()Levn;

    move-result-object v0

    iput-object v0, p0, Lbcs;->ad:Levn;

    .line 141
    invoke-virtual {v2}, Lari;->c()Leyp;

    move-result-object v0

    iput-object v0, p0, Lbcs;->ae:Leyp;

    .line 142
    invoke-virtual {v2}, Lari;->ay()Leyt;

    move-result-object v0

    iput-object v0, p0, Lbcs;->af:Leyt;

    .line 143
    invoke-virtual {v2}, Lari;->q()Lfcs;

    move-result-object v0

    iput-object v0, p0, Lbcs;->ag:Lfcs;

    .line 144
    iget-object v0, v2, Lari;->t:Lezs;

    invoke-virtual {v0}, Lezs;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfdl;

    iput-object v0, p0, Lbcs;->ah:Lfdl;

    .line 145
    invoke-virtual {v2}, Lari;->o()Lglm;

    move-result-object v0

    iput-object v0, p0, Lbcs;->ai:Lglm;

    .line 146
    invoke-virtual {v2}, Lari;->ag()Lbku;

    move-result-object v0

    iput-object v0, p0, Lbcs;->al:Lbku;

    .line 147
    invoke-virtual {v2}, Lari;->aD()Lcst;

    move-result-object v0

    iput-object v0, p0, Lbcs;->aj:Lcst;

    .line 148
    invoke-virtual {v2}, Lari;->aO()Lcub;

    move-result-object v0

    iput-object v0, p0, Lbcs;->ak:Lcub;

    .line 149
    invoke-virtual {v2}, Lari;->ae()Lfdw;

    move-result-object v0

    iput-object v0, p0, Lbcs;->am:Lfdw;

    .line 150
    invoke-virtual {v2}, Lari;->f()Larh;

    move-result-object v0

    iput-object v0, p0, Lbcs;->ac:Lcyc;

    .line 152
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbcs;->as:Ljava/util/ArrayList;

    .line 161
    new-instance v0, Lfqg;

    invoke-virtual {v1}, Letc;->k()Lfac;

    move-result-object v1

    invoke-direct {v0, v1}, Lfqg;-><init>(Lfac;)V

    iput-object v0, p0, Lbcs;->an:Lfqg;

    .line 162
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 530
    iget-object v1, p0, Lbcs;->aq:Lfpz;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbcs;->aq:Lfpz;

    iput-boolean v2, v1, Lfpz;->h:Z

    iput-object v0, p0, Lbcs;->aq:Lfpz;

    :cond_0
    iget-object v1, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v1}, Lfsi;->notifyDataSetChanged()V

    .line 535
    iget-object v1, p0, Lbcs;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    iget-object v1, v1, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->k:Lbgh;

    .line 536
    if-eqz v1, :cond_6

    .line 537
    iget-object v0, v1, Lbgh;->b:Landroid/os/Bundle;

    invoke-static {v0}, Lbgh;->a(Landroid/os/Bundle;)Lhog;

    move-result-object v0

    move-object v4, v0

    .line 541
    :goto_0
    if-nez v4, :cond_1

    .line 567
    :goto_1
    return-void

    .line 547
    :cond_1
    iget-object v0, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v0}, Lfsi;->getCount()I

    move-result v5

    move v1, v2

    .line 548
    :goto_2
    if-ge v1, v5, :cond_3

    .line 549
    iget-object v0, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v0, v1}, Lfsi;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 550
    instance-of v3, v0, Lfpy;

    if-nez v3, :cond_5

    .line 551
    instance-of v3, v0, Lfpz;

    if-eqz v3, :cond_5

    .line 553
    check-cast v0, Lfpz;

    .line 554
    invoke-static {v4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v0, Lfpz;->c:Lhog;

    iget-object v3, v3, Lhog;->i:Libf;

    if-nez v3, :cond_2

    iget-object v3, v0, Lfpz;->c:Lhog;

    iget-object v3, v3, Lhog;->u:Libk;

    if-eqz v3, :cond_4

    :cond_2
    move v3, v2

    :goto_3
    if-eqz v3, :cond_5

    .line 555
    const/4 v2, 0x1

    iput-boolean v2, v0, Lfpz;->h:Z

    .line 556
    iput-object v0, p0, Lbcs;->aq:Lfpz;

    .line 557
    if-eqz p1, :cond_3

    .line 558
    iget-object v0, p0, Lbcs;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 566
    :cond_3
    iget-object v0, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v0}, Lfsi;->notifyDataSetChanged()V

    goto :goto_1

    .line 554
    :cond_4
    iget-object v3, v0, Lfpz;->c:Lhog;

    invoke-static {v3, v4, v2}, Lfia;->a(Lhog;Lhog;Z)Z

    move-result v3

    goto :goto_3

    .line 548
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    move-object v4, v0

    goto :goto_0
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 208
    invoke-super {p0, p1}, Lj;->d(Landroid/os/Bundle;)V

    .line 209
    iget-object v0, p0, Lbcs;->a:Lcom/google/android/apps/youtube/app/WatchWhileActivity;

    .line 210
    iget-object v0, v0, Lcom/google/android/apps/youtube/app/WatchWhileActivity;->i:Lbbp;

    new-instance v1, Lbbt;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lbbt;-><init>(Lbbp;I)V

    iput-object v1, p0, Lbcs;->ao:Lfhz;

    .line 212
    new-instance v0, Lfsi;

    invoke-direct {v0}, Lfsi;-><init>()V

    iput-object v0, p0, Lbcs;->ar:Lfsi;

    iget-object v0, p0, Lbcs;->ar:Lfsi;

    const-class v1, Lfqe;

    new-instance v2, Lbcu;

    invoke-direct {v2, p0}, Lbcu;-><init>(Lbcs;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v0, p0, Lbcs;->ar:Lfsi;

    const-class v1, Lfqc;

    new-instance v2, Lbcv;

    invoke-direct {v2, p0}, Lbcv;-><init>(Lbcs;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v0, p0, Lbcs;->ar:Lfsi;

    const-class v1, Lfqf;

    new-instance v2, Lbcw;

    invoke-direct {v2, p0}, Lbcw;-><init>(Lbcs;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v0, p0, Lbcs;->ar:Lfsi;

    const-class v1, Lfpz;

    new-instance v2, Lbcx;

    invoke-direct {v2, p0}, Lbcx;-><init>(Lbcs;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v0, p0, Lbcs;->ar:Lfsi;

    const-class v1, Lfpy;

    new-instance v2, Lbcy;

    invoke-direct {v2, p0}, Lbcy;-><init>(Lbcs;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v0, p0, Lbcs;->ar:Lfsi;

    const-class v1, Lfqb;

    new-instance v2, Lbcz;

    invoke-direct {v2, p0}, Lbcz;-><init>(Lbcs;)V

    invoke-virtual {v0, v1, v2}, Lfsi;->a(Ljava/lang/Class;Lfsk;)V

    iget-object v0, p0, Lbcs;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lbcs;->ar:Lfsi;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lbcs;->b:Landroid/widget/ListView;

    new-instance v1, Lbda;

    invoke-direct {v1, p0}, Lbda;-><init>(Lbcs;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 213
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 303
    invoke-super {p0}, Lj;->e()V

    .line 304
    iget-object v0, p0, Lbcs;->ad:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 305
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 306
    return-void
.end method

.method public final e(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 336
    invoke-super {p0, p1}, Lj;->e(Landroid/os/Bundle;)V

    .line 337
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 330
    invoke-super {p0}, Lj;->f()V

    .line 331
    iget-object v0, p0, Lbcs;->ad:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 332
    return-void
.end method

.method public final handleChannelSubscribed(Lbnz;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 428
    const-string v0, "Guide refreshing due to channel subscription "

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 429
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 430
    return-void
.end method

.method public final handleChannelUnsubscribed(Lboa;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 435
    const-string v0, "Guide refreshing due to channel un-subscription "

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 436
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 437
    return-void
.end method

.method public final handleConnectivityChanged(Lewk;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 387
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 388
    return-void
.end method

.method public final handlePlaylistAddAction(Lbuc;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 400
    const-string v0, "Guide refreshing due to playlist addition"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 401
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 402
    return-void
.end method

.method public final handlePlaylistDeleteAction(Lbud;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 421
    const-string v0, "Guide refreshing due to playlist deletion"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 422
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 423
    return-void
.end method

.method public final handlePlaylistLikeAction(Lbui;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 393
    const-string v0, "Guide refreshing due to playlist like action"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 394
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 395
    return-void
.end method

.method public final handleProfileChanged(Lcsw;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 371
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbcs;->f(Z)V

    .line 372
    return-void
.end method

.method public final handleSignIn(Lfcb;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 376
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 377
    return-void
.end method

.method public final handleSignOut(Lfcc;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 381
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 382
    return-void
.end method

.method public final handleVideoAddedToPlaylistAction(Lfez;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 407
    const-string v0, "Guide refreshing due to a video being added to a playlist"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 408
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 409
    return-void
.end method

.method public final handleVideoRemovedFromPlaylistAction(Lffa;)V
    .locals 1
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 414
    const-string v0, "Guide refreshing due to a video being deleted from a playlist"

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 415
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbcs;->e(Z)V

    .line 416
    return-void
.end method

.method public final t()V
    .locals 7

    .prologue
    .line 310
    invoke-super {p0}, Lj;->t()V

    .line 311
    iget-object v0, p0, Lbcs;->ad:Levn;

    new-instance v1, Lazu;

    invoke-direct {v1}, Lazu;-><init>()V

    invoke-virtual {v0, v1}, Levn;->d(Ljava/lang/Object;)V

    .line 315
    iget-object v0, p0, Lbcs;->ak:Lcub;

    iget-object v0, v0, Lcub;->h:Lcul;

    invoke-virtual {v0}, Lcul;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    invoke-direct {p0}, Lbcs;->c()V

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget-object v0, p0, Lbcs;->aj:Lcst;

    invoke-virtual {v0}, Lcst;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbcs;->aj:Lcst;

    .line 322
    invoke-virtual {v0}, Lcst;->e()Lcua;

    move-result-object v0

    iget-object v0, v0, Lcua;->d:Landroid/text/Spanned;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lbcs;->aj:Lcst;

    iget-object v1, p0, Lbcs;->ag:Lfcs;

    iget-object v2, p0, Lbcs;->ad:Levn;

    invoke-virtual {v0}, Lcst;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcst;->a:Lctj;

    invoke-interface {v3}, Lctj;->c()Lgit;

    move-result-object v3

    iget-object v4, v3, Lgit;->b:Lgiv;

    invoke-virtual {v4}, Lgiv;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lgit;->c:Ljava/lang/String;

    iget-object v3, v3, Lgit;->b:Lgiv;

    new-instance v6, Lcsu;

    invoke-direct {v6, v0, v5, v4, v2}, Lcsu;-><init>(Lcst;Ljava/lang/String;Ljava/lang/String;Levn;)V

    invoke-virtual {v1, v3, v6}, Lfcs;->a(Lgiv;Lwv;)V

    goto :goto_0
.end method
