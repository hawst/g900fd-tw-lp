.class final enum Lerq;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lerq;

.field public static final enum b:Lerq;

.field public static final enum c:Lerq;

.field public static final enum d:Lerq;

.field public static final enum e:Lerq;

.field private static final synthetic g:[Lerq;


# instance fields
.field public final f:D


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 33
    new-instance v0, Lerq;

    const-string v1, "FULL"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v0, v1, v4, v2, v3}, Lerq;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lerq;->a:Lerq;

    .line 34
    new-instance v0, Lerq;

    const-string v1, "THREE_QUARTER"

    const-wide/high16 v2, 0x3fe8000000000000L    # 0.75

    invoke-direct {v0, v1, v5, v2, v3}, Lerq;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lerq;->b:Lerq;

    .line 35
    new-instance v0, Lerq;

    const-string v1, "HALF"

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    invoke-direct {v0, v1, v6, v2, v3}, Lerq;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lerq;->c:Lerq;

    .line 36
    new-instance v0, Lerq;

    const-string v1, "QUARTER"

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    invoke-direct {v0, v1, v7, v2, v3}, Lerq;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lerq;->d:Lerq;

    .line 37
    new-instance v0, Lerq;

    const-string v1, "NONE"

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v8, v2, v3}, Lerq;-><init>(Ljava/lang/String;ID)V

    sput-object v0, Lerq;->e:Lerq;

    .line 32
    const/4 v0, 0x5

    new-array v0, v0, [Lerq;

    sget-object v1, Lerq;->a:Lerq;

    aput-object v1, v0, v4

    sget-object v1, Lerq;->b:Lerq;

    aput-object v1, v0, v5

    sget-object v1, Lerq;->c:Lerq;

    aput-object v1, v0, v6

    sget-object v1, Lerq;->d:Lerq;

    aput-object v1, v0, v7

    sget-object v1, Lerq;->e:Lerq;

    aput-object v1, v0, v8

    sput-object v0, Lerq;->g:[Lerq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ID)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput-wide p3, p0, Lerq;->f:D

    .line 44
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lerq;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lerq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lerq;

    return-object v0
.end method

.method public static values()[Lerq;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lerq;->g:[Lerq;

    invoke-virtual {v0}, [Lerq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lerq;

    return-object v0
.end method
