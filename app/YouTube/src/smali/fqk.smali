.class final Lfqk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lfqj;
    .locals 6

    .prologue
    .line 308
    :try_start_0
    new-instance v1, Lfqj;

    new-instance v0, Lhgy;

    invoke-direct {v0}, Lhgy;-><init>()V

    .line 309
    invoke-static {p0, v0}, La;->b(Landroid/os/Parcel;Lidh;)Lidh;

    move-result-object v0

    check-cast v0, Lhgy;

    .line 310
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 311
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-direct {v1, v0, v2, v4, v5}, Lfqj;-><init>(Lhgy;Ljava/lang/String;J)V
    :try_end_0
    .catch Lidg; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 312
    :catch_0
    move-exception v0

    .line 313
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 303
    invoke-static {p1}, Lfqk;->a(Landroid/os/Parcel;)Lfqj;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 303
    new-array v0, p1, [Lfqj;

    return-object v0
.end method
