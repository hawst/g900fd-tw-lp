.class public final Lgzq;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9238
    invoke-direct {p0}, Lidf;-><init>()V

    .line 9241
    iput v0, p0, Lgzq;->a:I

    .line 9244
    iput v0, p0, Lgzq;->b:I

    .line 9247
    iput-boolean v0, p0, Lgzq;->c:Z

    .line 9250
    iput-boolean v0, p0, Lgzq;->d:Z

    .line 9238
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 9305
    const/4 v0, 0x0

    .line 9306
    iget v1, p0, Lgzq;->a:I

    if-eqz v1, :cond_0

    .line 9307
    const/4 v0, 0x1

    iget v1, p0, Lgzq;->a:I

    .line 9308
    invoke-static {v0, v1}, Lidd;->c(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 9310
    :cond_0
    iget v1, p0, Lgzq;->b:I

    if-eqz v1, :cond_1

    .line 9311
    const/4 v1, 0x2

    iget v2, p0, Lgzq;->b:I

    .line 9312
    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9314
    :cond_1
    iget-boolean v1, p0, Lgzq;->c:Z

    if-eqz v1, :cond_2

    .line 9315
    const/4 v1, 0x3

    iget-boolean v2, p0, Lgzq;->c:Z

    .line 9316
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9318
    :cond_2
    iget-boolean v1, p0, Lgzq;->d:Z

    if-eqz v1, :cond_3

    .line 9319
    const/4 v1, 0x4

    iget-boolean v2, p0, Lgzq;->d:Z

    .line 9320
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 9322
    :cond_3
    iget-object v1, p0, Lgzq;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9323
    iput v0, p0, Lgzq;->J:I

    .line 9324
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 9234
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzq;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzq;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzq;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lgzq;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lgzq;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzq;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lgzq;->d:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 9287
    iget v0, p0, Lgzq;->a:I

    if-eqz v0, :cond_0

    .line 9288
    const/4 v0, 0x1

    iget v1, p0, Lgzq;->a:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 9290
    :cond_0
    iget v0, p0, Lgzq;->b:I

    if-eqz v0, :cond_1

    .line 9291
    const/4 v0, 0x2

    iget v1, p0, Lgzq;->b:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    .line 9293
    :cond_1
    iget-boolean v0, p0, Lgzq;->c:Z

    if-eqz v0, :cond_2

    .line 9294
    const/4 v0, 0x3

    iget-boolean v1, p0, Lgzq;->c:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 9296
    :cond_2
    iget-boolean v0, p0, Lgzq;->d:Z

    if-eqz v0, :cond_3

    .line 9297
    const/4 v0, 0x4

    iget-boolean v1, p0, Lgzq;->d:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 9299
    :cond_3
    iget-object v0, p0, Lgzq;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 9301
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9264
    if-ne p1, p0, :cond_1

    .line 9271
    :cond_0
    :goto_0
    return v0

    .line 9265
    :cond_1
    instance-of v2, p1, Lgzq;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 9266
    :cond_2
    check-cast p1, Lgzq;

    .line 9267
    iget v2, p0, Lgzq;->a:I

    iget v3, p1, Lgzq;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lgzq;->b:I

    iget v3, p1, Lgzq;->b:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgzq;->c:Z

    iget-boolean v3, p1, Lgzq;->c:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lgzq;->d:Z

    iget-boolean v3, p1, Lgzq;->d:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lgzq;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzq;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 9271
    goto :goto_0

    .line 9267
    :cond_4
    iget-object v2, p0, Lgzq;->I:Ljava/util/List;

    iget-object v3, p1, Lgzq;->I:Ljava/util/List;

    .line 9271
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 9275
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9277
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lgzq;->a:I

    add-int/2addr v0, v3

    .line 9278
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lgzq;->b:I

    add-int/2addr v0, v3

    .line 9279
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lgzq;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 9280
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lgzq;->d:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 9281
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lgzq;->I:Ljava/util/List;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    add-int/2addr v0, v1

    .line 9282
    return v0

    :cond_0
    move v0, v2

    .line 9279
    goto :goto_0

    :cond_1
    move v1, v2

    .line 9280
    goto :goto_1

    .line 9281
    :cond_2
    iget-object v0, p0, Lgzq;->I:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    goto :goto_2
.end method
