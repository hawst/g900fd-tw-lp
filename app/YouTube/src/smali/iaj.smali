.class public final Liaj;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Liaj;


# instance fields
.field public b:[B

.field public c:I

.field private d:I

.field private e:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Liaj;

    sput-object v0, Liaj;->a:[Liaj;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Liaj;->b:[B

    iput v1, p0, Liaj;->c:I

    iput v1, p0, Liaj;->d:I

    sget-object v0, Lidj;->a:[I

    iput-object v0, p0, Liaj;->e:[I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Liaj;->b:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iget-object v2, p0, Liaj;->b:[B

    invoke-static {v0, v2}, Lidd;->b(I[B)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget v2, p0, Liaj;->c:I

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget v3, p0, Liaj;->c:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget v2, p0, Liaj;->d:I

    if-eqz v2, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Liaj;->d:I

    invoke-static {v2, v3}, Lidd;->c(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1
    iget-object v2, p0, Liaj;->e:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Liaj;->e:[I

    array-length v2, v2

    if-lez v2, :cond_3

    iget-object v3, p0, Liaj;->e:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_2

    aget v5, v3, v1

    invoke-static {v5}, Lidd;->a(I)I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    add-int/2addr v0, v2

    iget-object v1, p0, Liaj;->e:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Liaj;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Liaj;->J:I

    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v3, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Liaj;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Liaj;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Liaj;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Liaj;->b:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Liaj;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Liaj;->d:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v1

    iget-object v0, p0, Liaj;->e:[I

    array-length v0, v0

    add-int/2addr v1, v0

    new-array v1, v1, [I

    iget-object v2, p0, Liaj;->e:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v1, p0, Liaj;->e:[I

    :goto_1
    iget-object v1, p0, Liaj;->e:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Liaj;->e:[I

    invoke-virtual {p1}, Lidc;->d()I

    move-result v2

    aput v2, v1, v0

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Liaj;->e:[I

    invoke-virtual {p1}, Lidc;->d()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Liaj;->b:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Liaj;->b:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_0
    iget v0, p0, Liaj;->c:I

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget v1, p0, Liaj;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_1
    iget v0, p0, Liaj;->d:I

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Liaj;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_2
    iget-object v0, p0, Liaj;->e:[I

    if-eqz v0, :cond_3

    iget-object v1, p0, Liaj;->e:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget v3, v1, v0

    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Lidd;->a(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Liaj;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Liaj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Liaj;

    iget-object v2, p0, Liaj;->b:[B

    iget-object v3, p1, Liaj;->b:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Liaj;->c:I

    iget v3, p1, Liaj;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Liaj;->d:I

    iget v3, p1, Liaj;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Liaj;->e:[I

    iget-object v3, p1, Liaj;->e:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Liaj;->I:Ljava/util/List;

    if-nez v2, :cond_4

    iget-object v2, p1, Liaj;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Liaj;->I:Ljava/util/List;

    iget-object v3, p1, Liaj;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    iget-object v2, p0, Liaj;->b:[B

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v0, v2, 0x1f

    iget v2, p0, Liaj;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Liaj;->d:I

    add-int/2addr v0, v2

    iget-object v2, p0, Liaj;->e:[I

    if-nez v2, :cond_3

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Liaj;->I:Ljava/util/List;

    if-nez v2, :cond_4

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v2, v0

    move v0, v1

    :goto_1
    iget-object v3, p0, Liaj;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Liaj;->b:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v2, v0

    move v0, v1

    :goto_2
    iget-object v3, p0, Liaj;->e:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Liaj;->e:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v1, p0, Liaj;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_0
.end method
