.class public final Lhig;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhnx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65364
    invoke-direct {p0}, Lidf;-><init>()V

    .line 65367
    const/4 v0, 0x0

    iput-object v0, p0, Lhig;->a:Lhnx;

    .line 65364
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 65404
    const/4 v0, 0x0

    .line 65405
    iget-object v1, p0, Lhig;->a:Lhnx;

    if-eqz v1, :cond_0

    .line 65406
    const v0, 0x396214a

    iget-object v1, p0, Lhig;->a:Lhnx;

    .line 65407
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 65409
    :cond_0
    iget-object v1, p0, Lhig;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 65410
    iput v0, p0, Lhig;->J:I

    .line 65411
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 65360
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhig;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhig;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhig;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhig;->a:Lhnx;

    if-nez v0, :cond_2

    new-instance v0, Lhnx;

    invoke-direct {v0}, Lhnx;-><init>()V

    iput-object v0, p0, Lhig;->a:Lhnx;

    :cond_2
    iget-object v0, p0, Lhig;->a:Lhnx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1cb10a52 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 65395
    iget-object v0, p0, Lhig;->a:Lhnx;

    if-eqz v0, :cond_0

    .line 65396
    const v0, 0x396214a

    iget-object v1, p0, Lhig;->a:Lhnx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 65398
    :cond_0
    iget-object v0, p0, Lhig;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 65400
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65378
    if-ne p1, p0, :cond_1

    .line 65382
    :cond_0
    :goto_0
    return v0

    .line 65379
    :cond_1
    instance-of v2, p1, Lhig;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 65380
    :cond_2
    check-cast p1, Lhig;

    .line 65381
    iget-object v2, p0, Lhig;->a:Lhnx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhig;->a:Lhnx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhig;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhig;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 65382
    goto :goto_0

    .line 65381
    :cond_4
    iget-object v2, p0, Lhig;->a:Lhnx;

    iget-object v3, p1, Lhig;->a:Lhnx;

    invoke-virtual {v2, v3}, Lhnx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhig;->I:Ljava/util/List;

    iget-object v3, p1, Lhig;->I:Ljava/util/List;

    .line 65382
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 65386
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 65388
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhig;->a:Lhnx;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 65389
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhig;->I:Ljava/util/List;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 65390
    return v0

    .line 65388
    :cond_0
    iget-object v0, p0, Lhig;->a:Lhnx;

    invoke-virtual {v0}, Lhnx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 65389
    :cond_1
    iget-object v1, p0, Lhig;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_1
.end method
