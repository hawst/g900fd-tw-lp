.class public final Lctk;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final e:Landroid/net/Uri;


# instance fields
.field public final a:Lfxe;

.field public final b:Lgck;

.field public final c:Leyp;

.field public final d:Lctu;

.field private final f:Lfbu;

.field private final g:Lgix;

.field private final h:Ljava/util/concurrent/Executor;

.field private final i:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "http://m.youtube.com/create_channel"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lctk;->e:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lfxe;Lgck;Leyp;Lfbu;Lgix;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfxe;

    iput-object v0, p0, Lctk;->a:Lfxe;

    .line 83
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgck;

    iput-object v0, p0, Lctk;->b:Lgck;

    .line 84
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lctk;->c:Leyp;

    .line 85
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbu;

    iput-object v0, p0, Lctk;->f:Lfbu;

    .line 86
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lctk;->g:Lgix;

    .line 87
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lctk;->h:Ljava/util/concurrent/Executor;

    .line 88
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lctk;->i:Ljava/util/List;

    .line 90
    new-instance v0, Lcts;

    invoke-direct {v0, p0}, Lcts;-><init>(Lctk;)V

    iput-object v0, p0, Lctk;->d:Lctu;

    .line 91
    return-void
.end method

.method static synthetic a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lctk;->e:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic a(Lctk;)Ljava/util/List;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lctk;->i:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lctk;Lctr;Landroid/app/Activity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 44
    iget-object v0, p0, Lctk;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lctk;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-virtual {p2, v1}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lctk;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lctk;->h:Ljava/util/concurrent/Executor;

    new-instance v1, Lctn;

    invoke-direct {v1, p0, p1}, Lctn;-><init>(Lctk;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lctk;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lctk;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 231
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lctk;->g:Lgix;

    invoke-interface {v0, p1, p2}, Lgix;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    monitor-exit p0

    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lctk;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lctk;->h:Ljava/util/concurrent/Executor;

    new-instance v1, Lctm;

    invoke-direct {v1, p0}, Lctm;-><init>(Lctk;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lctk;Lctr;Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lctk;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lctk;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Landroid/app/Activity;->showDialog(I)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lctk;)Lfbu;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lctk;->f:Lfbu;

    return-object v0
.end method

.method static synthetic d(Lctk;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lctk;->h:Ljava/util/concurrent/Executor;

    new-instance v1, Lctl;

    invoke-direct {v1, p0}, Lctl;-><init>(Lctk;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic e(Lctk;)Lfxe;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lctk;->a:Lfxe;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Lctr;)V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lctk;->a:Lfxe;

    new-instance v1, Lcto;

    invoke-direct {v1, p0, p2, p1}, Lcto;-><init>(Lctk;Lctr;Landroid/app/Activity;)V

    .line 132
    invoke-static {p1, v1}, Leub;->a(Landroid/app/Activity;Leuc;)Leub;

    move-result-object v1

    .line 131
    invoke-interface {v0, v1}, Lfxe;->a(Leuc;)V

    .line 149
    return-void
.end method
