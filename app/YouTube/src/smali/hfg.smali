.class public final Lhfg;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Lhjx;

.field private d:Z

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44466
    invoke-direct {p0}, Lidf;-><init>()V

    .line 44469
    const/4 v0, 0x0

    iput-object v0, p0, Lhfg;->c:Lhjx;

    .line 44472
    const-string v0, ""

    iput-object v0, p0, Lhfg;->a:Ljava/lang/String;

    .line 44475
    const-string v0, ""

    iput-object v0, p0, Lhfg;->b:Ljava/lang/String;

    .line 44478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhfg;->d:Z

    .line 44481
    const-string v0, ""

    iput-object v0, p0, Lhfg;->e:Ljava/lang/String;

    .line 44466
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 44542
    const/4 v0, 0x0

    .line 44543
    iget-object v1, p0, Lhfg;->c:Lhjx;

    if-eqz v1, :cond_0

    .line 44544
    const/4 v0, 0x1

    iget-object v1, p0, Lhfg;->c:Lhjx;

    .line 44545
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 44547
    :cond_0
    iget-object v1, p0, Lhfg;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 44548
    const/4 v1, 0x2

    iget-object v2, p0, Lhfg;->a:Ljava/lang/String;

    .line 44549
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44551
    :cond_1
    iget-object v1, p0, Lhfg;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 44552
    const/4 v1, 0x3

    iget-object v2, p0, Lhfg;->b:Ljava/lang/String;

    .line 44553
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44555
    :cond_2
    iget-boolean v1, p0, Lhfg;->d:Z

    if-eqz v1, :cond_3

    .line 44556
    const/4 v1, 0x4

    iget-boolean v2, p0, Lhfg;->d:Z

    .line 44557
    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 44559
    :cond_3
    iget-object v1, p0, Lhfg;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 44560
    const/4 v1, 0x5

    iget-object v2, p0, Lhfg;->e:Ljava/lang/String;

    .line 44561
    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44563
    :cond_4
    iget-object v1, p0, Lhfg;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44564
    iput v0, p0, Lhfg;->J:I

    .line 44565
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 44462
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhfg;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhfg;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhfg;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhfg;->c:Lhjx;

    if-nez v0, :cond_2

    new-instance v0, Lhjx;

    invoke-direct {v0}, Lhjx;-><init>()V

    iput-object v0, p0, Lhfg;->c:Lhjx;

    :cond_2
    iget-object v0, p0, Lhfg;->c:Lhjx;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfg;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfg;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhfg;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfg;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 44521
    iget-object v0, p0, Lhfg;->c:Lhjx;

    if-eqz v0, :cond_0

    .line 44522
    const/4 v0, 0x1

    iget-object v1, p0, Lhfg;->c:Lhjx;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 44524
    :cond_0
    iget-object v0, p0, Lhfg;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 44525
    const/4 v0, 0x2

    iget-object v1, p0, Lhfg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 44527
    :cond_1
    iget-object v0, p0, Lhfg;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 44528
    const/4 v0, 0x3

    iget-object v1, p0, Lhfg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 44530
    :cond_2
    iget-boolean v0, p0, Lhfg;->d:Z

    if-eqz v0, :cond_3

    .line 44531
    const/4 v0, 0x4

    iget-boolean v1, p0, Lhfg;->d:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    .line 44533
    :cond_3
    iget-object v0, p0, Lhfg;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 44534
    const/4 v0, 0x5

    iget-object v1, p0, Lhfg;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    .line 44536
    :cond_4
    iget-object v0, p0, Lhfg;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 44538
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44496
    if-ne p1, p0, :cond_1

    .line 44504
    :cond_0
    :goto_0
    return v0

    .line 44497
    :cond_1
    instance-of v2, p1, Lhfg;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 44498
    :cond_2
    check-cast p1, Lhfg;

    .line 44499
    iget-object v2, p0, Lhfg;->c:Lhjx;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhfg;->c:Lhjx;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhfg;->a:Ljava/lang/String;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhfg;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 44500
    :goto_2
    iget-object v2, p0, Lhfg;->b:Ljava/lang/String;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhfg;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 44501
    :goto_3
    iget-boolean v2, p0, Lhfg;->d:Z

    iget-boolean v3, p1, Lhfg;->d:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhfg;->e:Ljava/lang/String;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhfg;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 44503
    :goto_4
    iget-object v2, p0, Lhfg;->I:Ljava/util/List;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhfg;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 44504
    goto :goto_0

    .line 44499
    :cond_4
    iget-object v2, p0, Lhfg;->c:Lhjx;

    iget-object v3, p1, Lhfg;->c:Lhjx;

    invoke-virtual {v2, v3}, Lhjx;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhfg;->a:Ljava/lang/String;

    iget-object v3, p1, Lhfg;->a:Ljava/lang/String;

    .line 44500
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhfg;->b:Ljava/lang/String;

    iget-object v3, p1, Lhfg;->b:Ljava/lang/String;

    .line 44501
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhfg;->e:Ljava/lang/String;

    iget-object v3, p1, Lhfg;->e:Ljava/lang/String;

    .line 44503
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhfg;->I:Ljava/util/List;

    iget-object v3, p1, Lhfg;->I:Ljava/util/List;

    .line 44504
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 44508
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 44510
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfg;->c:Lhjx;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 44511
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfg;->a:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 44512
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfg;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 44513
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lhfg;->d:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    add-int/2addr v0, v2

    .line 44514
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhfg;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 44515
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhfg;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 44516
    return v0

    .line 44510
    :cond_0
    iget-object v0, p0, Lhfg;->c:Lhjx;

    invoke-virtual {v0}, Lhjx;->hashCode()I

    move-result v0

    goto :goto_0

    .line 44511
    :cond_1
    iget-object v0, p0, Lhfg;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 44512
    :cond_2
    iget-object v0, p0, Lhfg;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 44513
    :cond_3
    const/4 v0, 0x2

    goto :goto_3

    .line 44514
    :cond_4
    iget-object v0, p0, Lhfg;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 44515
    :cond_5
    iget-object v1, p0, Lhfg;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
