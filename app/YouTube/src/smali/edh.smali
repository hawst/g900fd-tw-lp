.class public Ledh;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "SourceFile"


# instance fields
.field private final a:Lorg/xmlpull/v1/XmlPullParserFactory;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 41
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v0

    iput-object v0, p0, Ledh;->a:Lorg/xmlpull/v1/XmlPullParserFactory;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    return-void

    .line 42
    :catch_0
    move-exception v0

    .line 43
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t create XmlPullParserFactory instance"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(II)I
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 225
    if-ne p0, v0, :cond_1

    move p0, p1

    .line 231
    :cond_0
    :goto_0
    return p0

    .line 227
    :cond_1
    if-eq p1, v0, :cond_0

    .line 230
    if-ne p0, p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, La;->c(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 206
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 207
    invoke-static {p0}, La;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    .line 208
    :cond_0
    invoke-static {p0}, La;->p(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 209
    :cond_1
    invoke-static {p0}, La;->q(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p0}, La;->r(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 542
    if-nez v0, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p2

    goto :goto_0
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 507
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 508
    if-nez v0, :cond_0

    .line 511
    :goto_0
    return-wide p2

    :cond_0
    invoke-static {v0}, Legz;->c(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;J)Ledi;
    .locals 20

    .prologue
    .line 116
    const/4 v4, 0x0

    const-string v5, "id"

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 117
    const-string v4, "start"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4, v6, v7}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v8

    .line 118
    const-string v4, "duration"

    move-object/from16 v0, p1

    move-wide/from16 v1, p4

    invoke-static {v0, v4, v1, v2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v10

    .line 119
    const/4 v12, 0x0

    .line 120
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v7, p3

    .line 122
    :cond_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 123
    const-string v4, "BaseURL"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 124
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v7

    .line 135
    :cond_1
    :goto_0
    const-string v4, "Period"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 137
    new-instance v6, Ledi;

    move-object/from16 v7, v18

    move-object/from16 v12, v19

    invoke-direct/range {v6 .. v12}, Ledi;-><init>(Ljava/lang/String;JJLjava/util/List;)V

    return-object v6

    .line 125
    :cond_2
    const-string v4, "AdaptationSet"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    .line 126
    invoke-virtual/range {v4 .. v12}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;JJLedn;)Lede;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    :cond_3
    const-string v4, "SegmentBase"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 129
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v7, v4}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Leds;)Leds;

    move-result-object v12

    goto :goto_0

    .line 130
    :cond_4
    const-string v4, "SegmentList"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 131
    const/4 v15, 0x0

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-object v14, v7

    move-wide/from16 v16, v10

    invoke-direct/range {v12 .. v17}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledp;J)Ledp;

    move-result-object v12

    goto :goto_0

    .line 132
    :cond_5
    const-string v4, "SegmentTemplate"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 133
    const/4 v15, 0x0

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move-object v14, v7

    move-wide/from16 v16, v10

    invoke-direct/range {v12 .. v17}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledq;J)Ledq;

    move-result-object v12

    goto :goto_0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Ledj;
    .locals 2

    .prologue
    .line 465
    const-string v0, "sourceURL"

    const-string v1, "range"

    invoke-direct {p0, p1, p2, v0, v1}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ledj;

    move-result-object v0

    return-object v0
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ledj;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 474
    invoke-interface {p1, v0, p3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 475
    const-wide/16 v4, 0x0

    .line 476
    const-wide/16 v6, -0x1

    .line 477
    invoke-interface {p1, v0, p4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 478
    if-eqz v0, :cond_0

    .line 479
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 480
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 481
    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sub-long/2addr v0, v4

    const-wide/16 v6, 0x1

    add-long/2addr v6, v0

    .line 483
    :cond_0
    new-instance v1, Ledj;

    move-object v2, p2

    invoke-direct/range {v1 .. v7}, Ledj;-><init>(Landroid/net/Uri;Ljava/lang/String;JJ)V

    return-object v1
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;JJLjava/lang/String;Ljava/lang/String;Ledn;)Ledk;
    .locals 14

    .prologue
    .line 268
    const/4 v0, 0x0

    const-string v1, "id"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 269
    const-string v0, "bandwidth"

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v7

    .line 270
    const-string v0, "audioSamplingRate"

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v6

    .line 271
    const-string v0, "width"

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v10

    .line 272
    const-string v0, "height"

    const/4 v1, -0x1

    invoke-static {p1, v0, v1}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v12

    .line 273
    const-string v0, "mimeType"

    const/4 v1, 0x0

    invoke-interface {p1, v1, v0}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 275
    :goto_0
    const/4 v8, -0x1

    move-object/from16 v0, p10

    move-object/from16 v2, p3

    .line 277
    :goto_1
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 278
    const-string v1, "BaseURL"

    invoke-static {p1, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 279
    invoke-static {p1, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    move v5, v8

    move-object v11, v0

    .line 290
    :goto_2
    const-string v0, "Representation"

    invoke-static {p1, v0}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 292
    new-instance v0, Lecw;

    move-object v1, v9

    move-object/from16 v2, p8

    move v3, v10

    move v4, v12

    move-object/from16 v8, p9

    invoke-direct/range {v0 .. v8}, Lecw;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIILjava/lang/String;)V

    .line 294
    const-wide/16 v8, -0x1

    instance-of v1, v11, Leds;

    if-eqz v1, :cond_5

    new-instance v2, Ledm;

    check-cast v11, Leds;

    const-wide/16 v12, -0x1

    move-wide/from16 v3, p4

    move-wide/from16 v5, p6

    move-object/from16 v7, p2

    move-object v10, v0

    invoke-direct/range {v2 .. v13}, Ledm;-><init>(JJLjava/lang/String;JLecw;Leds;J)V

    :goto_3
    return-object v2

    :cond_0
    move-object/from16 p8, v0

    .line 273
    goto :goto_0

    .line 280
    :cond_1
    const-string v1, "AudioChannelConfiguration"

    invoke-static {p1, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 281
    const/4 v1, 0x0

    const-string v3, "value"

    invoke-interface {p1, v1, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    move-object v11, v0

    goto :goto_2

    .line 282
    :cond_2
    const-string v1, "SegmentBase"

    invoke-static {p1, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 283
    check-cast v0, Leds;

    invoke-direct {p0, p1, v2, v0}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Leds;)Leds;

    move-result-object v0

    move v5, v8

    move-object v11, v0

    goto :goto_2

    .line 284
    :cond_3
    const-string v1, "SegmentList"

    invoke-static {p1, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v3, v0

    .line 285
    check-cast v3, Ledp;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v4, p6

    invoke-direct/range {v0 .. v5}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledp;J)Ledp;

    move-result-object v0

    move v5, v8

    move-object v11, v0

    goto :goto_2

    .line 286
    :cond_4
    const-string v1, "SegmentTemplate"

    invoke-static {p1, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    move-object v3, v0

    .line 287
    check-cast v3, Ledq;

    move-object v0, p0

    move-object v1, p1

    move-wide/from16 v4, p6

    invoke-direct/range {v0 .. v5}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledq;J)Ledq;

    move-result-object v0

    move v5, v8

    move-object v11, v0

    goto :goto_2

    .line 294
    :cond_5
    instance-of v1, v11, Ledo;

    if-eqz v1, :cond_6

    new-instance v2, Ledl;

    check-cast v11, Ledo;

    move-wide/from16 v3, p4

    move-wide/from16 v5, p6

    move-object/from16 v7, p2

    move-object v10, v0

    invoke-direct/range {v2 .. v11}, Ledl;-><init>(JJLjava/lang/String;JLecw;Ledo;)V

    goto :goto_3

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "segmentBase must be of type SingleSegmentBase or MultiSegmentBase"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v5, v8

    move-object v11, v0

    goto/16 :goto_2

    :cond_8
    move v8, v5

    move-object v0, v11

    goto/16 :goto_1
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledp;J)Ledp;
    .locals 18

    .prologue
    .line 349
    const-string v6, "timescale"

    if-eqz p3, :cond_2

    move-object/from16 v0, p3

    iget-wide v4, v0, Ledp;->b:J

    :goto_0
    move-object/from16 v0, p1

    invoke-static {v0, v6, v4, v5}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v6

    .line 350
    const-string v8, "presentationTimeOffset"

    if-eqz p3, :cond_3

    move-object/from16 v0, p3

    iget-wide v4, v0, Ledp;->c:J

    :goto_1
    move-object/from16 v0, p1

    invoke-static {v0, v8, v4, v5}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v8

    .line 352
    const-string v10, "duration"

    if-eqz p3, :cond_4

    move-object/from16 v0, p3

    iget-wide v4, v0, Ledp;->e:J

    :goto_2
    move-object/from16 v0, p1

    invoke-static {v0, v10, v4, v5}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v13

    .line 353
    const-string v5, "startNumber"

    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    iget v4, v0, Ledp;->d:I

    :goto_3
    move-object/from16 v0, p1

    invoke-static {v0, v5, v4}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v12

    .line 355
    const/4 v10, 0x0

    .line 356
    const/4 v5, 0x0

    .line 357
    const/4 v4, 0x0

    .line 360
    :cond_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 361
    const-string v11, "Initialization"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 362
    invoke-direct/range {p0 .. p2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Ledj;

    move-result-object v10

    .line 371
    :cond_1
    :goto_4
    const-string v11, "SegmentList"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 373
    if-eqz p3, :cond_c

    .line 374
    if-eqz v10, :cond_9

    .line 375
    :goto_5
    if-eqz v5, :cond_a

    move-object v15, v5

    .line 376
    :goto_6
    if-eqz v4, :cond_b

    :goto_7
    move-object/from16 v16, v4

    move-object v5, v10

    .line 379
    :goto_8
    new-instance v4, Ledp;

    move-wide/from16 v10, p4

    invoke-direct/range {v4 .. v16}, Ledp;-><init>(Ledj;JJJIJLjava/util/List;Ljava/util/List;)V

    return-object v4

    .line 349
    :cond_2
    const-wide/16 v4, 0x1

    goto :goto_0

    .line 350
    :cond_3
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 352
    :cond_4
    const-wide/16 v4, -0x1

    goto :goto_2

    .line 353
    :cond_5
    const/4 v4, 0x1

    goto :goto_3

    .line 363
    :cond_6
    const-string v11, "SegmentTimeline"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 364
    invoke-direct/range {p0 .. p1}, Ledh;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;

    move-result-object v5

    goto :goto_4

    .line 365
    :cond_7
    const-string v11, "SegmentURL"

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 366
    if-nez v4, :cond_8

    .line 367
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 369
    :cond_8
    const-string v11, "media"

    const-string v15, "mediaRange"

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v11, v15}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Ledj;

    move-result-object v11

    invoke-interface {v4, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 374
    :cond_9
    move-object/from16 v0, p3

    iget-object v10, v0, Ledp;->a:Ledj;

    goto :goto_5

    .line 375
    :cond_a
    move-object/from16 v0, p3

    iget-object v15, v0, Ledp;->f:Ljava/util/List;

    goto :goto_6

    .line 376
    :cond_b
    move-object/from16 v0, p3

    iget-object v4, v0, Ledp;->g:Ljava/util/List;

    goto :goto_7

    :cond_c
    move-object/from16 v16, v4

    move-object v15, v5

    move-object v5, v10

    goto :goto_8
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledq;J)Ledq;
    .locals 18

    .prologue
    .line 393
    const-string v4, "timescale"

    if-eqz p3, :cond_2

    move-object/from16 v0, p3

    iget-wide v2, v0, Ledq;->b:J

    :goto_0
    move-object/from16 v0, p1

    invoke-static {v0, v4, v2, v3}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v4

    .line 394
    const-string v6, "presentationTimeOffset"

    if-eqz p3, :cond_3

    move-object/from16 v0, p3

    iget-wide v2, v0, Ledq;->c:J

    :goto_1
    move-object/from16 v0, p1

    invoke-static {v0, v6, v2, v3}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v6

    .line 396
    const-string v8, "duration"

    if-eqz p3, :cond_4

    move-object/from16 v0, p3

    iget-wide v2, v0, Ledq;->e:J

    :goto_2
    move-object/from16 v0, p1

    invoke-static {v0, v8, v2, v3}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v11

    .line 397
    const-string v3, "startNumber"

    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    iget v2, v0, Ledq;->d:I

    :goto_3
    move-object/from16 v0, p1

    invoke-static {v0, v3, v2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v10

    .line 398
    const-string v3, "media"

    if-eqz p3, :cond_6

    move-object/from16 v0, p3

    iget-object v2, v0, Ledq;->h:Ledt;

    :goto_4
    move-object/from16 v0, p1

    invoke-static {v0, v3, v2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ledt;)Ledt;

    move-result-object v15

    .line 400
    const-string v3, "initialization"

    if-eqz p3, :cond_7

    move-object/from16 v0, p3

    iget-object v2, v0, Ledq;->g:Ledt;

    :goto_5
    move-object/from16 v0, p1

    invoke-static {v0, v3, v2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ledt;)Ledt;

    move-result-object v14

    .line 403
    const/4 v3, 0x0

    .line 404
    const/4 v2, 0x0

    .line 407
    :cond_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 408
    const-string v8, "Initialization"

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 409
    invoke-direct/range {p0 .. p2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Ledj;

    move-result-object v3

    .line 413
    :cond_1
    :goto_6
    const-string v8, "SegmentTemplate"

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 415
    if-eqz p3, :cond_b

    .line 416
    if-eqz v3, :cond_9

    .line 417
    :goto_7
    if-eqz v2, :cond_a

    :goto_8
    move-object v13, v2

    .line 420
    :goto_9
    new-instance v2, Ledq;

    move-wide/from16 v8, p4

    move-object/from16 v16, p2

    invoke-direct/range {v2 .. v16}, Ledq;-><init>(Ledj;JJJIJLjava/util/List;Ledt;Ledt;Landroid/net/Uri;)V

    return-object v2

    .line 393
    :cond_2
    const-wide/16 v2, 0x1

    goto :goto_0

    .line 394
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_1

    .line 396
    :cond_4
    const-wide/16 v2, -0x1

    goto :goto_2

    .line 397
    :cond_5
    const/4 v2, 0x1

    goto :goto_3

    .line 398
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 400
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    .line 410
    :cond_8
    const-string v8, "SegmentTimeline"

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 411
    invoke-direct/range {p0 .. p1}, Ledh;->c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;

    move-result-object v2

    goto :goto_6

    .line 416
    :cond_9
    move-object/from16 v0, p3

    iget-object v3, v0, Ledq;->a:Ledj;

    goto :goto_7

    .line 417
    :cond_a
    move-object/from16 v0, p3

    iget-object v2, v0, Ledq;->f:Ljava/util/List;

    goto :goto_8

    :cond_b
    move-object v13, v2

    goto :goto_9
.end method

.method private a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Leds;)Leds;
    .locals 12

    .prologue
    .line 315
    const-string v2, "timescale"

    if-eqz p3, :cond_0

    iget-wide v0, p3, Leds;->b:J

    :goto_0
    invoke-static {p1, v2, v0, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v2

    .line 316
    const-string v4, "presentationTimeOffset"

    if-eqz p3, :cond_1

    iget-wide v0, p3, Leds;->c:J

    :goto_1
    invoke-static {p1, v4, v0, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v4

    .line 319
    if-eqz p3, :cond_2

    iget-wide v7, p3, Leds;->e:J

    .line 320
    :goto_2
    if-eqz p3, :cond_3

    iget-wide v0, p3, Leds;->f:J

    .line 321
    :goto_3
    const/4 v6, 0x0

    const-string v9, "indexRange"

    invoke-interface {p1, v6, v9}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 322
    if-eqz v6, :cond_7

    .line 323
    const-string v0, "-"

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 324
    const/4 v1, 0x0

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    .line 325
    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    sub-long/2addr v0, v7

    const-wide/16 v10, 0x1

    add-long v9, v0, v10

    .line 328
    :goto_4
    if-eqz p3, :cond_4

    iget-object v0, p3, Leds;->a:Ledj;

    .line 330
    :goto_5
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 331
    const-string v1, "Initialization"

    invoke-static {p1, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 332
    invoke-direct {p0, p1, p2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Ledj;

    move-result-object v1

    .line 334
    :goto_6
    const-string v0, "SegmentBase"

    invoke-static {p1, v0}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 336
    new-instance v0, Leds;

    move-object v6, p2

    invoke-direct/range {v0 .. v10}, Leds;-><init>(Ledj;JJLandroid/net/Uri;JJ)V

    return-object v0

    .line 315
    :cond_0
    const-wide/16 v0, 0x1

    goto :goto_0

    .line 316
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 319
    :cond_2
    const-wide/16 v7, 0x0

    goto :goto_2

    .line 320
    :cond_3
    const-wide/16 v0, -0x1

    goto :goto_3

    .line 328
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    :cond_5
    move-object v0, v1

    goto :goto_5

    :cond_6
    move-object v1, v0

    goto :goto_6

    :cond_7
    move-wide v9, v0

    goto :goto_4
.end method

.method private static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ledt;)Ledt;
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x4

    const/4 v1, 0x0

    .line 457
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 458
    if-eqz v4, :cond_d

    .line 459
    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/String;

    new-array v6, v11, [I

    new-array v7, v11, [Ljava/lang/String;

    const-string v0, ""

    aput-object v0, v5, v1

    move v0, v1

    move v2, v1

    :goto_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v2, v3, :cond_c

    const-string v3, "$"

    invoke-virtual {v4, v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v12, :cond_1

    aget-object v3, v5, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_1
    aput-object v2, v5, v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    if-eq v3, v2, :cond_3

    aget-object v8, v5, v0

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v8, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    aput-object v2, v5, v0

    move v2, v3

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string v3, "$$"

    invoke-virtual {v4, v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    aget-object v3, v5, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "$"

    invoke-virtual {v3, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v0

    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_4
    const-string v3, "$"

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {v4, v3, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v8

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v4, v2, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v2, "RepresentationID"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    aput v2, v6, v0

    :goto_3
    add-int/lit8 v0, v0, 0x1

    const-string v2, ""

    aput-object v2, v5, v0

    add-int/lit8 v2, v8, 0x1

    goto/16 :goto_0

    :cond_5
    const-string v2, "%0"

    invoke-virtual {v3, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    const-string v2, "%01d"

    if-eq v9, v12, :cond_7

    invoke-virtual {v3, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v10, "d"

    invoke-virtual {v2, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v10, "d"

    invoke-virtual {v2, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_6
    invoke-virtual {v3, v1, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :cond_7
    const-string v9, "Number"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    const/4 v3, 0x2

    aput v3, v6, v0

    :goto_4
    aput-object v2, v7, v0

    goto :goto_3

    :cond_8
    const-string v9, "Bandwidth"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    const/4 v3, 0x3

    aput v3, v6, v0

    goto :goto_4

    :cond_9
    const-string v9, "Time"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    aput v11, v6, v0

    goto :goto_4

    :cond_a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid template: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    :cond_c
    new-instance p2, Ledt;

    invoke-direct {p2, v5, v6, v7, v0}, Ledt;-><init>([Ljava/lang/String;[I[Ljava/lang/String;I)V

    .line 461
    :cond_d
    return-object p2
.end method

.method public static a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 494
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 550
    const/4 v0, 0x0

    invoke-interface {p0, v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 551
    if-nez v0, :cond_0

    :goto_0
    return-wide p2

    :cond_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p2

    goto :goto_0
.end method

.method private static b(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 527
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 528
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v1

    .line 529
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Landroid/net/Uri;->isAbsolute()Z

    move-result v2

    if-nez v2, :cond_0

    .line 531
    invoke-static {p1, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 533
    :cond_0
    return-object v0
.end method

.method public static b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 499
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lorg/xmlpull/v1/XmlPullParser;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 434
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 435
    const-wide/16 v0, 0x0

    .line 437
    :cond_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 438
    const-string v2, "S"

    invoke-static {p1, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 439
    const-string v2, "t"

    invoke-static {p1, v2, v0, v1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v0

    .line 440
    const-string v2, "d"

    const-wide/16 v4, -0x1

    invoke-static {p1, v2, v4, v5}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v8

    .line 441
    const-string v2, "r"

    invoke-static {p1, v2, v3}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v7, v2, 0x1

    move v2, v3

    .line 442
    :goto_0
    if-ge v2, v7, :cond_1

    .line 443
    new-instance v4, Ledr;

    invoke-direct {v4, v0, v1, v8, v9}, Ledr;-><init>(JJ)V

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 444
    add-long v4, v0, v8

    .line 442
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v4

    goto :goto_0

    .line 447
    :cond_1
    const-string v2, "SegmentTimeline"

    invoke-static {p1, v2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 448
    return-object v6
.end method


# virtual methods
.method public a(IILjava/util/List;Ljava/util/List;)Lede;
    .locals 1

    .prologue
    .line 194
    new-instance v0, Lede;

    invoke-direct {v0, p1, p2, p3, p4}, Lede;-><init>(IILjava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;JJLedn;)Lede;
    .locals 18

    .prologue
    .line 151
    const/4 v2, 0x0

    const-string v3, "mimeType"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 152
    const/4 v2, 0x0

    const-string v3, "lang"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 153
    invoke-static {v10}, Ledh;->a(Ljava/lang/String;)I

    move-result v15

    .line 155
    const/4 v14, -0x1

    .line 156
    const/4 v13, 0x0

    .line 157
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v12, p8

    move-object/from16 v5, p3

    .line 159
    :goto_0
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    .line 160
    const-string v2, "BaseURL"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 161
    move-object/from16 v0, p1

    invoke-static {v0, v5}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    move-object v2, v13

    move v3, v14

    move v4, v15

    .line 187
    :goto_1
    const-string v6, "AdaptationSet"

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 189
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v4, v1, v2}, Ledh;->a(IILjava/util/List;Ljava/util/List;)Lede;

    move-result-object v2

    return-object v2

    .line 162
    :cond_0
    const-string v2, "ContentProtection"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    if-nez v13, :cond_c

    .line 164
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 166
    :goto_2
    invoke-virtual/range {p0 .. p1}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;)Ledf;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v14

    move v4, v15

    goto :goto_1

    .line 167
    :cond_1
    const-string v2, "ContentComponent"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 168
    const/4 v2, 0x0

    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 169
    const/4 v3, 0x0

    const-string v4, "contentType"

    .line 170
    move-object/from16 v0, p1

    invoke-interface {v0, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "audio"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    .line 169
    :goto_3
    invoke-static {v15, v3}, Ledh;->a(II)I

    move-result v3

    move v4, v3

    move v3, v2

    move-object v2, v13

    goto :goto_1

    .line 170
    :cond_2
    const-string v4, "video"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    const-string v4, "text"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x2

    goto :goto_3

    :cond_4
    const/4 v3, -0x1

    goto :goto_3

    .line 171
    :cond_5
    const-string v2, "Representation"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    .line 172
    invoke-direct/range {v2 .. v12}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;JJLjava/lang/String;Ljava/lang/String;Ledn;)Ledk;

    move-result-object v3

    .line 174
    iget-object v2, v3, Ledk;->b:Lecw;

    iget-object v2, v2, Lecw;->b:Ljava/lang/String;

    .line 175
    invoke-static {v2}, Ledh;->a(Ljava/lang/String;)I

    move-result v2

    .line 174
    invoke-static {v15, v2}, Ledh;->a(II)I

    move-result v2

    .line 176
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v3, v14

    move v4, v2

    move-object v2, v13

    .line 177
    goto/16 :goto_1

    :cond_6
    const-string v2, "SegmentBase"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 178
    check-cast v12, Leds;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v12}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Leds;)Leds;

    move-result-object v12

    move-object v2, v13

    move v3, v14

    move v4, v15

    goto/16 :goto_1

    .line 179
    :cond_7
    const-string v2, "SegmentList"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object v6, v12

    .line 180
    check-cast v6, Ledp;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-wide/from16 v7, p6

    invoke-direct/range {v3 .. v8}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledp;J)Ledp;

    move-result-object v12

    move-object v2, v13

    move v3, v14

    move v4, v15

    goto/16 :goto_1

    .line 181
    :cond_8
    const-string v2, "SegmentTemplate"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object v6, v12

    .line 182
    check-cast v6, Ledq;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-wide/from16 v7, p6

    invoke-direct/range {v3 .. v8}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;Ledq;J)Ledq;

    move-result-object v12

    move-object v2, v13

    move v3, v14

    move v4, v15

    goto/16 :goto_1

    .line 184
    :cond_9
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_b

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_a

    .line 185
    invoke-virtual/range {p0 .. p1}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;)V

    :cond_a
    move-object v2, v13

    move v3, v14

    move v4, v15

    goto/16 :goto_1

    .line 184
    :cond_b
    const/4 v2, 0x0

    goto :goto_4

    :cond_c
    move-object v2, v13

    goto/16 :goto_2

    :cond_d
    move-object v13, v2

    move v14, v3

    move v15, v4

    goto/16 :goto_0
.end method

.method public a(Lorg/xmlpull/v1/XmlPullParser;)Ledf;
    .locals 2

    .prologue
    .line 243
    const/4 v0, 0x0

    const-string v1, "schemeIdUri"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 244
    new-instance v1, Ledf;

    invoke-direct {v1, v0}, Ledf;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method public final a(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Ledg;
    .locals 20

    .prologue
    .line 53
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ledh;->a:Lorg/xmlpull/v1/XmlPullParserFactory;

    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v3

    .line 54
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v3, v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 55
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    .line 56
    const/4 v4, 0x2

    if-ne v2, v4, :cond_0

    const-string v2, "MPD"

    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 57
    :cond_0
    new-instance v2, Lebx;

    const-string v3, "inputStream does not contain a valid media presentation description"

    invoke-direct {v2, v3}, Lebx;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 61
    :catch_0
    move-exception v2

    .line 62
    new-instance v3, Lebx;

    invoke-direct {v3, v2}, Lebx;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 60
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    const-string v2, "availabilityStartTime"

    const/4 v4, 0x0

    invoke-interface {v3, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    const-wide/16 v6, -0x1

    move-wide/from16 v18, v6

    :goto_0
    const-string v2, "mediaPresentationDuration"

    const-wide/16 v6, -0x1

    invoke-static {v3, v2, v6, v7}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v6

    const-string v2, "minBufferTime"

    const-wide/16 v8, -0x1

    invoke-static {v3, v2, v8, v9}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v8

    const/4 v2, 0x0

    const-string v4, "type"

    invoke-interface {v3, v2, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string v4, "dynamic"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    :goto_1
    if-eqz v10, :cond_6

    const-string v2, "minimumUpdatePeriod"

    const-wide/16 v12, -0x1

    invoke-static {v3, v2, v12, v13}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v11

    :goto_2
    if-eqz v10, :cond_7

    const-string v2, "timeShiftBufferDepth"

    const-wide/16 v14, -0x1

    invoke-static {v3, v2, v14, v15}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;J)J

    move-result-wide v13

    :goto_3
    const/4 v15, 0x0

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    :cond_2
    invoke-interface {v3}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    const-string v2, "BaseURL"

    invoke-static {v3, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-static {v3, v5}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    :cond_3
    :goto_4
    const-string v2, "MPD"

    invoke-static {v3, v2}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v3, Ledg;

    move-wide/from16 v4, v18

    invoke-direct/range {v3 .. v16}, Ledg;-><init>(JJJZJJLedu;Ljava/util/List;)V

    return-object v3

    :cond_4
    invoke-static {v2}, Legz;->d(Ljava/lang/String;)J

    move-result-wide v6

    move-wide/from16 v18, v6

    goto :goto_0

    :cond_5
    const/4 v10, 0x0

    goto :goto_1

    :cond_6
    const-wide/16 v11, -0x1

    goto :goto_2

    :cond_7
    const-wide/16 v13, -0x1

    goto :goto_3

    :cond_8
    const-string v2, "UTCTiming"

    invoke-static {v3, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x0

    const-string v4, "schemeIdUri"

    invoke-interface {v3, v2, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const-string v15, "value"

    invoke-interface {v3, v4, v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v15, Ledu;

    invoke-direct {v15, v2, v4}, Ledu;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 63
    :catch_1
    move-exception v2

    .line 64
    new-instance v3, Lebx;

    invoke-direct {v3, v2}, Lebx;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 60
    :cond_9
    :try_start_2
    const-string v2, "Period"

    invoke-static {v3, v2}, Ledh;->b(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v2, p0

    move-object/from16 v4, p3

    invoke-direct/range {v2 .. v7}, Ledh;->a(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Landroid/net/Uri;J)Ledi;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4
.end method

.method public b(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 0

    .prologue
    .line 261
    return-void
.end method
