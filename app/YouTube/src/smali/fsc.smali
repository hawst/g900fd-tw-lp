.class public final Lfsc;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/List;

.field public final b:Ljava/util/LinkedHashSet;

.field public final c:Landroid/database/DataSetObserver;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfsc;->a:Ljava/util/List;

    .line 123
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lfsc;->b:Ljava/util/LinkedHashSet;

    .line 124
    const/4 v0, 0x0

    iput v0, p0, Lfsc;->d:I

    .line 126
    new-instance v0, Lfsd;

    invoke-direct {v0, p0}, Lfsd;-><init>(Lfsc;)V

    iput-object v0, p0, Lfsc;->c:Landroid/database/DataSetObserver;

    .line 139
    return-void
.end method

.method private a(I)Lfse;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 228
    iget-object v0, p0, Lfsc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfse;

    .line 229
    iget v1, v0, Lfse;->b:I

    if-ge p1, v1, :cond_1

    move v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 233
    :goto_1
    return-object v0

    .line 229
    :cond_1
    iget v1, v0, Lfse;->b:I

    iget-object v4, v0, Lfse;->a:Lfsf;

    invoke-interface {v4}, Lfsf;->getCount()I

    move-result v4

    add-int/2addr v1, v4

    if-lt p1, v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 233
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lfsc;)V
    .locals 0

    .prologue
    .line 24
    invoke-virtual {p0}, Lfsc;->b()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lfsc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final a(Lfsf;)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lfsc;->a:Ljava/util/List;

    new-instance v1, Lfse;

    invoke-direct {v1, p1}, Lfse;-><init>(Lfsf;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lfsc;->c:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Lfsf;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-virtual {p0}, Lfsc;->b()V

    iget-object v0, p0, Lfsc;->b:Ljava/util/LinkedHashSet;

    invoke-interface {p1}, Lfsf;->a()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lfsc;->notifyDataSetChanged()V

    .line 148
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 213
    const/4 v0, 0x0

    .line 214
    iget-object v1, p0, Lfsc;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfse;

    .line 215
    iput v1, v0, Lfse;->b:I

    .line 216
    iget-object v0, v0, Lfse;->a:Lfsf;

    invoke-interface {v0}, Lfsf;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 217
    goto :goto_0

    .line 218
    :cond_0
    iput v1, p0, Lfsc;->d:I

    .line 219
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lfsc;->d:I

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 275
    invoke-direct {p0, p1}, Lfsc;->a(I)Lfse;

    move-result-object v0

    .line 276
    iget-object v1, v0, Lfse;->a:Lfsf;

    iget v0, v0, Lfse;->b:I

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Lfsf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lfsc;->a(I)Lfse;

    move-result-object v0

    .line 282
    iget-object v1, v0, Lfse;->a:Lfsf;

    iget v0, v0, Lfse;->b:I

    sub-int v0, p1, v0

    invoke-interface {v1, v0}, Lfsf;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 250
    invoke-direct {p0, p1}, Lfsc;->a(I)Lfse;

    move-result-object v3

    .line 251
    iget-object v1, v3, Lfse;->a:Lfsf;

    iget v4, v3, Lfse;->b:I

    sub-int v4, p1, v4

    invoke-interface {v1, v4}, Lfsf;->getItemViewType(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_2

    move v1, v2

    .line 270
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v1, v0

    .line 251
    goto :goto_0

    .line 255
    :cond_2
    iget-object v1, v3, Lfse;->a:Lfsf;

    iget v3, v3, Lfse;->b:I

    sub-int v3, p1, v3

    invoke-interface {v1, v3}, Lfsf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 256
    if-nez v1, :cond_3

    move v1, v2

    .line 257
    goto :goto_1

    .line 260
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 261
    iget-object v1, p0, Lfsc;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v1, v3}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 263
    iget-object v1, p0, Lfsc;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 264
    if-eq v0, v3, :cond_0

    .line 267
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 268
    goto :goto_2

    :cond_4
    move v1, v2

    .line 270
    goto :goto_1
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 287
    invoke-direct {p0, p1}, Lfsc;->a(I)Lfse;

    move-result-object v0

    .line 288
    iget-object v1, v0, Lfse;->a:Lfsf;

    .line 289
    iget v0, v0, Lfse;->b:I

    sub-int v0, p1, v0

    .line 288
    invoke-interface {v1, v0, p2, p3}, Lfsf;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lfsc;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->size()I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method
