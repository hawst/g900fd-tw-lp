.class public final Lhkv;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:[Lhkx;

.field public b:Lhgz;

.field public c:Lhkw;

.field public d:Lhgz;

.field public e:[B


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    sget-object v0, Lhkx;->a:[Lhkx;

    iput-object v0, p0, Lhkv;->a:[Lhkx;

    iput-object v1, p0, Lhkv;->b:Lhgz;

    iput-object v1, p0, Lhkv;->c:Lhkw;

    iput-object v1, p0, Lhkv;->d:Lhgz;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Lhkv;->e:[B

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lhkv;->a:[Lhkx;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lhkv;->a:[Lhkx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    const/4 v5, 0x1

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lhkv;->b:Lhgz;

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    iget-object v2, p0, Lhkv;->b:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lhkv;->c:Lhkw;

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lhkv;->c:Lhkw;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lhkv;->d:Lhgz;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lhkv;->d:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Lhkv;->e:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lhkv;->e:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhkv;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhkv;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Lhkv;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lhkv;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Lhkv;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Lhkv;->a:[Lhkx;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhkx;

    iget-object v3, p0, Lhkv;->a:[Lhkx;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lhkv;->a:[Lhkx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iput-object v2, p0, Lhkv;->a:[Lhkx;

    :goto_2
    iget-object v2, p0, Lhkv;->a:[Lhkx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lhkv;->a:[Lhkx;

    new-instance v3, Lhkx;

    invoke-direct {v3}, Lhkx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhkv;->a:[Lhkx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhkv;->a:[Lhkx;

    array-length v0, v0

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lhkv;->a:[Lhkx;

    new-instance v3, Lhkx;

    invoke-direct {v3}, Lhkx;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Lhkv;->a:[Lhkx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhkv;->b:Lhgz;

    if-nez v0, :cond_5

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhkv;->b:Lhgz;

    :cond_5
    iget-object v0, p0, Lhkv;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhkv;->c:Lhkw;

    if-nez v0, :cond_6

    new-instance v0, Lhkw;

    invoke-direct {v0}, Lhkw;-><init>()V

    iput-object v0, p0, Lhkv;->c:Lhkw;

    :cond_6
    iget-object v0, p0, Lhkv;->c:Lhkw;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lhkv;->d:Lhgz;

    if-nez v0, :cond_7

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lhkv;->d:Lhgz;

    :cond_7
    iget-object v0, p0, Lhkv;->d:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Lhkv;->e:[B

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Lhkv;->a:[Lhkx;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lhkv;->a:[Lhkx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhkv;->b:Lhgz;

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    iget-object v1, p0, Lhkv;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_2
    iget-object v0, p0, Lhkv;->c:Lhkw;

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    iget-object v1, p0, Lhkv;->c:Lhkw;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_3
    iget-object v0, p0, Lhkv;->d:Lhgz;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lhkv;->d:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Lhkv;->e:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lhkv;->e:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_5
    iget-object v0, p0, Lhkv;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhkv;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhkv;

    iget-object v2, p0, Lhkv;->a:[Lhkx;

    iget-object v3, p1, Lhkv;->a:[Lhkx;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhkv;->b:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhkv;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhkv;->c:Lhkw;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhkv;->c:Lhkw;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Lhkv;->d:Lhgz;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhkv;->d:Lhgz;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Lhkv;->e:[B

    iget-object v3, p1, Lhkv;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lhkv;->I:Ljava/util/List;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhkv;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhkv;->b:Lhgz;

    iget-object v3, p1, Lhkv;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhkv;->c:Lhkw;

    iget-object v3, p1, Lhkv;->c:Lhkw;

    invoke-virtual {v2, v3}, Lhkw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhkv;->d:Lhgz;

    iget-object v3, p1, Lhkv;->d:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhkv;->I:Ljava/util/List;

    iget-object v3, p1, Lhkv;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    iget-object v2, p0, Lhkv;->a:[Lhkx;

    if-nez v2, :cond_2

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Lhkv;->b:Lhgz;

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkv;->c:Lhkw;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhkv;->d:Lhgz;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    iget-object v2, p0, Lhkv;->e:[B

    if-nez v2, :cond_7

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v0, v2, 0x1f

    iget-object v2, p0, Lhkv;->I:Ljava/util/List;

    if-nez v2, :cond_8

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_2
    move v2, v0

    move v0, v1

    :goto_4
    iget-object v3, p0, Lhkv;->a:[Lhkx;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Lhkv;->a:[Lhkx;

    aget-object v2, v2, v0

    if-nez v2, :cond_3

    move v2, v1

    :goto_5
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_3
    iget-object v2, p0, Lhkv;->a:[Lhkx;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhkx;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_4
    iget-object v0, p0, Lhkv;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lhkv;->c:Lhkw;

    invoke-virtual {v0}, Lhkw;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lhkv;->d:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_7
    move v2, v0

    move v0, v1

    :goto_6
    iget-object v3, p0, Lhkv;->e:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Lhkv;->e:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lhkv;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_3
.end method
