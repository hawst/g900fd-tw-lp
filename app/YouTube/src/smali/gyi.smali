.class public final Lgyi;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgyi;


# instance fields
.field public b:Lhul;

.field public c:Lgya;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2524
    const/4 v0, 0x0

    new-array v0, v0, [Lgyi;

    sput-object v0, Lgyi;->a:[Lgyi;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2525
    invoke-direct {p0}, Lidf;-><init>()V

    .line 2528
    iput-object v0, p0, Lgyi;->b:Lhul;

    .line 2531
    iput-object v0, p0, Lgyi;->c:Lgya;

    .line 2525
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 2574
    const/4 v0, 0x0

    .line 2575
    iget-object v1, p0, Lgyi;->b:Lhul;

    if-eqz v1, :cond_0

    .line 2576
    const v0, 0x2f1c7f5

    iget-object v1, p0, Lgyi;->b:Lhul;

    .line 2577
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2579
    :cond_0
    iget-object v1, p0, Lgyi;->c:Lgya;

    if-eqz v1, :cond_1

    .line 2580
    const v1, 0x498941e

    iget-object v2, p0, Lgyi;->c:Lgya;

    .line 2581
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2583
    :cond_1
    iget-object v1, p0, Lgyi;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2584
    iput v0, p0, Lgyi;->J:I

    .line 2585
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 2521
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgyi;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgyi;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgyi;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgyi;->b:Lhul;

    if-nez v0, :cond_2

    new-instance v0, Lhul;

    invoke-direct {v0}, Lhul;-><init>()V

    iput-object v0, p0, Lgyi;->b:Lhul;

    :cond_2
    iget-object v0, p0, Lgyi;->b:Lhul;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgyi;->c:Lgya;

    if-nez v0, :cond_3

    new-instance v0, Lgya;

    invoke-direct {v0}, Lgya;-><init>()V

    iput-object v0, p0, Lgyi;->c:Lgya;

    :cond_3
    iget-object v0, p0, Lgyi;->c:Lgya;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x178e3faa -> :sswitch_1
        0x24c4a0f2 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 2562
    iget-object v0, p0, Lgyi;->b:Lhul;

    if-eqz v0, :cond_0

    .line 2563
    const v0, 0x2f1c7f5

    iget-object v1, p0, Lgyi;->b:Lhul;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2565
    :cond_0
    iget-object v0, p0, Lgyi;->c:Lgya;

    if-eqz v0, :cond_1

    .line 2566
    const v0, 0x498941e

    iget-object v1, p0, Lgyi;->c:Lgya;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 2568
    :cond_1
    iget-object v0, p0, Lgyi;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 2570
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2543
    if-ne p1, p0, :cond_1

    .line 2548
    :cond_0
    :goto_0
    return v0

    .line 2544
    :cond_1
    instance-of v2, p1, Lgyi;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 2545
    :cond_2
    check-cast p1, Lgyi;

    .line 2546
    iget-object v2, p0, Lgyi;->b:Lhul;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgyi;->b:Lhul;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgyi;->c:Lgya;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgyi;->c:Lgya;

    if-nez v2, :cond_3

    .line 2547
    :goto_2
    iget-object v2, p0, Lgyi;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgyi;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 2548
    goto :goto_0

    .line 2546
    :cond_4
    iget-object v2, p0, Lgyi;->b:Lhul;

    iget-object v3, p1, Lgyi;->b:Lhul;

    invoke-virtual {v2, v3}, Lhul;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgyi;->c:Lgya;

    iget-object v3, p1, Lgyi;->c:Lgya;

    .line 2547
    invoke-virtual {v2, v3}, Lgya;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgyi;->I:Ljava/util/List;

    iget-object v3, p1, Lgyi;->I:Ljava/util/List;

    .line 2548
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2552
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2554
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyi;->b:Lhul;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2555
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgyi;->c:Lgya;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2556
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgyi;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2557
    return v0

    .line 2554
    :cond_0
    iget-object v0, p0, Lgyi;->b:Lhul;

    invoke-virtual {v0}, Lhul;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2555
    :cond_1
    iget-object v0, p0, Lgyi;->c:Lgya;

    invoke-virtual {v0}, Lgya;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2556
    :cond_2
    iget-object v1, p0, Lgyi;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
