.class final Left;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Lefu;

.field volatile b:Ljava/lang/Thread;

.field private final c:Lefs;

.field private synthetic d:Lefr;


# direct methods
.method public constructor <init>(Lefr;Landroid/os/Looper;Lefu;Lefs;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Left;->d:Lefr;

    .line 176
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 177
    iput-object p3, p0, Left;->a:Lefu;

    .line 178
    iput-object p4, p0, Left;->c:Lefs;

    .line 179
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Left;->d:Lefr;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lefr;->c:Z

    iget-object v0, p0, Left;->d:Lefr;

    const/4 v1, 0x0

    iput-object v1, v0, Lefr;->b:Left;

    .line 212
    iget-object v0, p0, Left;->a:Lefu;

    invoke-interface {v0}, Lefu;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Left;->c:Lefs;

    iget-object v1, p0, Left;->a:Lefu;

    invoke-interface {v0}, Lefs;->f()V

    .line 224
    :goto_0
    return-void

    .line 216
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 218
    :pswitch_0
    iget-object v0, p0, Left;->c:Lefs;

    iget-object v1, p0, Left;->a:Lefu;

    invoke-interface {v0}, Lefs;->e()V

    goto :goto_0

    .line 221
    :pswitch_1
    iget-object v1, p0, Left;->c:Lefs;

    iget-object v0, p0, Left;->a:Lefu;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/io/IOException;

    invoke-interface {v1, v0}, Lefs;->a(Ljava/io/IOException;)V

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 191
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Left;->b:Ljava/lang/Thread;

    .line 192
    iget-object v0, p0, Left;->a:Lefu;

    invoke-interface {v0}, Lefu;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v0, p0, Left;->a:Lefu;

    invoke-interface {v0}, Lefu;->f()V

    .line 195
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Left;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 207
    :goto_0
    return-void

    .line 196
    :catch_0
    move-exception v0

    .line 197
    invoke-virtual {p0, v3, v0}, Left;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 200
    :catch_1
    move-exception v0

    iget-object v0, p0, Left;->a:Lefu;

    invoke-interface {v0}, Lefu;->e()Z

    move-result v0

    invoke-static {v0}, La;->c(Z)V

    .line 201
    invoke-virtual {p0, v1}, Left;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 202
    :catch_2
    move-exception v0

    .line 204
    const-string v1, "LoadTask"

    const-string v2, "Unexpected error loading stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 205
    new-instance v1, Lefe;

    invoke-direct {v1, v0}, Lefe;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p0, v3, v1}, Left;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method
