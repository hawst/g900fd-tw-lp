.class public final Lfqg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:[B

.field public final b:Ljava/lang/String;

.field public final c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lfac;)V
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfqg;-><init>(Lfac;[B)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lfac;Lhog;)V
    .locals 1

    .prologue
    .line 53
    if-eqz p2, :cond_0

    iget-object v0, p2, Lhog;->a:[B

    :goto_0
    invoke-direct {p0, p1, v0}, Lfqg;-><init>(Lfac;[B)V

    .line 58
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lfac;[B)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfac;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lfac;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfqg;->b:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfqg;->c:Ljava/util/Set;

    .line 48
    iput-object p2, p0, Lfqg;->a:[B

    .line 49
    return-void
.end method

.method public static a([B)Z
    .locals 1

    .prologue
    .line 118
    if-eqz p0, :cond_0

    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
