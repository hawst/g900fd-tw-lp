.class public final Legv;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Comparator;

.field private static final b:Ljava/util/Comparator;


# instance fields
.field private final c:I

.field private final d:Ljava/util/ArrayList;

.field private final e:[Legy;

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Legw;

    invoke-direct {v0}, Legw;-><init>()V

    sput-object v0, Legv;->a:Ljava/util/Comparator;

    .line 30
    new-instance v0, Legx;

    invoke-direct {v0}, Legx;-><init>()V

    sput-object v0, Legv;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Legv;->c:I

    .line 55
    const/4 v0, 0x5

    new-array v0, v0, [Legy;

    iput-object v0, p0, Legv;->e:[Legy;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Legv;->d:Ljava/util/ArrayList;

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Legv;->f:I

    .line 58
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 101
    iget v1, p0, Legv;->f:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Legv;->d:Ljava/util/ArrayList;

    sget-object v2, Legv;->b:Ljava/util/Comparator;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput v0, p0, Legv;->f:I

    .line 102
    :cond_0
    const/high16 v1, 0x3f000000    # 0.5f

    iget v2, p0, Legv;->h:I

    int-to-float v2, v2

    mul-float v3, v1, v2

    move v1, v0

    move v2, v0

    .line 104
    :goto_0
    iget-object v0, p0, Legv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 105
    iget-object v0, p0, Legv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legy;

    .line 106
    iget v4, v0, Legy;->b:I

    add-int/2addr v2, v4

    .line 107
    int-to-float v4, v2

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_1

    .line 108
    iget v0, v0, Legy;->c:F

    .line 112
    :goto_1
    return v0

    .line 104
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 112
    :cond_2
    iget-object v0, p0, Legv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const/high16 v0, 0x7fc00000    # NaNf

    goto :goto_1

    :cond_3
    iget-object v0, p0, Legv;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Legv;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legy;

    iget v0, v0, Legy;->c:F

    goto :goto_1
.end method

.method public final a(IF)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 68
    iget v0, p0, Legv;->f:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Legv;->d:Ljava/util/ArrayList;

    sget-object v1, Legv;->a:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iput v2, p0, Legv;->f:I

    .line 70
    :cond_0
    iget v0, p0, Legv;->i:I

    if-lez v0, :cond_2

    iget-object v0, p0, Legv;->e:[Legy;

    iget v1, p0, Legv;->i:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Legv;->i:I

    aget-object v0, v0, v1

    .line 72
    :goto_0
    iget v1, p0, Legv;->g:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Legv;->g:I

    iput v1, v0, Legy;->a:I

    .line 73
    iput p1, v0, Legy;->b:I

    .line 74
    iput p2, v0, Legy;->c:F

    .line 75
    iget-object v1, p0, Legv;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget v0, p0, Legv;->h:I

    add-int/2addr v0, p1

    iput v0, p0, Legv;->h:I

    .line 78
    :cond_1
    :goto_1
    iget v0, p0, Legv;->h:I

    iget v1, p0, Legv;->c:I

    if-le v0, v1, :cond_4

    .line 79
    iget v0, p0, Legv;->h:I

    iget v1, p0, Legv;->c:I

    sub-int v1, v0, v1

    .line 80
    iget-object v0, p0, Legv;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Legy;

    .line 81
    iget v2, v0, Legy;->b:I

    if-gt v2, v1, :cond_3

    .line 82
    iget v1, p0, Legv;->h:I

    iget v2, v0, Legy;->b:I

    sub-int/2addr v1, v2

    iput v1, p0, Legv;->h:I

    .line 83
    iget-object v1, p0, Legv;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 84
    iget v1, p0, Legv;->i:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 85
    iget-object v1, p0, Legv;->e:[Legy;

    iget v2, p0, Legv;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Legv;->i:I

    aput-object v0, v1, v2

    goto :goto_1

    .line 70
    :cond_2
    new-instance v0, Legy;

    invoke-direct {v0}, Legy;-><init>()V

    goto :goto_0

    .line 88
    :cond_3
    iget v2, v0, Legy;->b:I

    sub-int/2addr v2, v1

    iput v2, v0, Legy;->b:I

    .line 89
    iget v0, p0, Legv;->h:I

    sub-int/2addr v0, v1

    iput v0, p0, Legv;->h:I

    goto :goto_1

    .line 92
    :cond_4
    return-void
.end method
