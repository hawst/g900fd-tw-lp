.class public final Lhim;
.super Lidf;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z

.field private c:I

.field private d:I

.field private e:I

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:F

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-boolean v1, p0, Lhim;->a:Z

    iput-boolean v1, p0, Lhim;->b:Z

    iput v1, p0, Lhim;->c:I

    iput v1, p0, Lhim;->d:I

    iput v1, p0, Lhim;->e:I

    iput-boolean v1, p0, Lhim;->f:Z

    const-string v0, ""

    iput-object v0, p0, Lhim;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lhim;->h:F

    iput v1, p0, Lhim;->i:I

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lhim;->a:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lhim;->a:Z

    invoke-static {v0}, Lidd;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    :cond_0
    iget-boolean v1, p0, Lhim;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    iget-boolean v2, p0, Lhim;->b:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lhim;->c:I

    if-eqz v1, :cond_2

    const/4 v1, 0x5

    iget v2, p0, Lhim;->c:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lhim;->d:I

    if-eqz v1, :cond_3

    const/4 v1, 0x6

    iget v2, p0, Lhim;->d:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lhim;->e:I

    if-eqz v1, :cond_4

    const/4 v1, 0x7

    iget v2, p0, Lhim;->e:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-boolean v1, p0, Lhim;->f:Z

    if-eqz v1, :cond_5

    const/16 v1, 0x8

    iget-boolean v2, p0, Lhim;->f:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lhim;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/16 v1, 0x9

    iget-object v2, p0, Lhim;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lidd;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lhim;->h:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_7

    const/16 v1, 0xa

    iget v2, p0, Lhim;->h:F

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lhim;->i:I

    if-eqz v1, :cond_8

    const/16 v1, 0xb

    iget v2, p0, Lhim;->i:I

    invoke-static {v1, v2}, Lidd;->c(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Lhim;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lhim;->J:I

    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhim;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhim;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhim;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhim;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhim;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    :cond_2
    iput v0, p0, Lhim;->c:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lhim;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhim;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhim;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Lhim;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhim;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lidc;->j()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lhim;->h:F

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->d()I

    move-result v0

    iput v0, p0, Lhim;->i:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x20 -> :sswitch_2
        0x28 -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
        0x55 -> :sswitch_8
        0x58 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    iget-boolean v0, p0, Lhim;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-boolean v1, p0, Lhim;->a:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_0
    iget-boolean v0, p0, Lhim;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    iget-boolean v1, p0, Lhim;->b:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_1
    iget v0, p0, Lhim;->c:I

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    iget v1, p0, Lhim;->c:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_2
    iget v0, p0, Lhim;->d:I

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    iget v1, p0, Lhim;->d:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_3
    iget v0, p0, Lhim;->e:I

    if-eqz v0, :cond_4

    const/4 v0, 0x7

    iget v1, p0, Lhim;->e:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_4
    iget-boolean v0, p0, Lhim;->f:Z

    if-eqz v0, :cond_5

    const/16 v0, 0x8

    iget-boolean v1, p0, Lhim;->f:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_5
    iget-object v0, p0, Lhim;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/16 v0, 0x9

    iget-object v1, p0, Lhim;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILjava/lang/String;)V

    :cond_6
    iget v0, p0, Lhim;->h:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_7

    const/16 v0, 0xa

    iget v1, p0, Lhim;->h:F

    invoke-virtual {p1, v0, v1}, Lidd;->a(IF)V

    :cond_7
    iget v0, p0, Lhim;->i:I

    if-eqz v0, :cond_8

    const/16 v0, 0xb

    iget v1, p0, Lhim;->i:I

    invoke-virtual {p1, v0, v1}, Lidd;->a(II)V

    :cond_8
    iget-object v0, p0, Lhim;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lhim;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lhim;

    iget-boolean v2, p0, Lhim;->a:Z

    iget-boolean v3, p1, Lhim;->a:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhim;->b:Z

    iget-boolean v3, p1, Lhim;->b:Z

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhim;->c:I

    iget v3, p1, Lhim;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhim;->d:I

    iget v3, p1, Lhim;->d:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lhim;->e:I

    iget v3, p1, Lhim;->e:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lhim;->f:Z

    iget-boolean v3, p1, Lhim;->f:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhim;->g:Ljava/lang/String;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhim;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_1
    iget v2, p0, Lhim;->h:F

    iget v3, p1, Lhim;->h:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lhim;->i:I

    iget v3, p1, Lhim;->i:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lhim;->I:Ljava/util/List;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhim;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lhim;->g:Ljava/lang/String;

    iget-object v3, p1, Lhim;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhim;->I:Ljava/util/List;

    iget-object v3, p1, Lhim;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhim;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lhim;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhim;->c:I

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhim;->d:I

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lhim;->e:I

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lhim;->f:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lhim;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v3

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhim;->h:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lhim;->i:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lhim;->I:Ljava/util/List;

    if-nez v1, :cond_4

    :goto_4
    add-int/2addr v0, v3

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lhim;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    iget-object v1, p0, Lhim;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v3

    goto :goto_4
.end method
