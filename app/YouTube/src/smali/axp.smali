.class final Laxp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private synthetic a:[Ljava/lang/CharSequence;

.field private synthetic b:Ljava/util/List;

.field private synthetic c:Laxo;


# direct methods
.method constructor <init>(Laxo;[Ljava/lang/CharSequence;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Laxp;->c:Laxo;

    iput-object p2, p0, Laxp;->a:[Ljava/lang/CharSequence;

    iput-object p3, p0, Laxp;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Laxp;->a:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-ge p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->b(Z)V

    .line 87
    iget-object v0, p0, Laxp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_2

    .line 88
    invoke-static {}, Laxr;->values()[Laxr;

    move-result-object v0

    .line 89
    iget-object v1, p0, Laxp;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, p2, v1

    aget-object v0, v0, v1

    .line 90
    iget-object v1, p0, Laxp;->c:Laxo;

    iget-object v2, v1, Laxo;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v0, v2}, Laxr;->a(Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "autocast_setting_message"

    iget-object v1, v1, Laxo;->b:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Laxr;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 94
    :cond_0
    :goto_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 95
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 92
    :cond_2
    iget-object v1, p0, Laxp;->c:Laxo;

    iget-object v0, p0, Laxp;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldwr;

    new-instance v2, Laxv;

    invoke-direct {v2, v0}, Laxv;-><init>(Ldwr;)V

    iget-object v3, v1, Laxo;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-virtual {v2, v3}, Laxv;->a(Landroid/content/SharedPreferences$Editor;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v2, v1, Laxo;->c:Laxq;

    if-eqz v2, :cond_0

    iget-object v1, v1, Laxo;->c:Laxq;

    invoke-interface {v1, v0}, Laxq;->a(Ldwr;)V

    goto :goto_1
.end method
