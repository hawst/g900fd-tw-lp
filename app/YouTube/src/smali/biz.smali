.class public final Lbiz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbbv;


# instance fields
.field final a:Landroid/app/Activity;

.field final b:Leyt;

.field final c:Lfeo;

.field final d:Lhut;

.field e:Landroid/app/Dialog;

.field f:Landroid/app/AlertDialog;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Lhut;Landroid/app/Activity;Lfeo;Leyt;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhut;

    iput-object v0, p0, Lbiz;->d:Lhut;

    .line 56
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lbiz;->a:Landroid/app/Activity;

    .line 57
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfeo;

    iput-object v0, p0, Lbiz;->c:Lfeo;

    .line 58
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyt;

    iput-object v0, p0, Lbiz;->b:Leyt;

    .line 59
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 63
    iget-object v0, p0, Lbiz;->e:Landroid/app/Dialog;

    if-nez v0, :cond_0

    iget-object v2, p0, Lbiz;->a:Landroid/app/Activity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04009b

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f08023c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lbiz;->h:Landroid/widget/Button;

    iget-object v1, p0, Lbiz;->h:Landroid/widget/Button;

    new-instance v3, Lbja;

    invoke-direct {v3, p0}, Lbja;-><init>(Lbiz;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08023d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v3, Lbjb;

    invoke-direct {v3, p0}, Lbjb;-><init>(Lbiz;)V

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f08008b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lbiz;->g:Landroid/widget/TextView;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbiz;->e:Landroid/app/Dialog;

    :cond_0
    iget-object v0, p0, Lbiz;->f:Landroid/app/AlertDialog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbiz;->a:Landroid/app/Activity;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0900a1

    new-instance v2, Lbjc;

    invoke-direct {v2, p0}, Lbjc;-><init>(Lbiz;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0900a2

    invoke-virtual {v0, v1, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbiz;->f:Landroid/app/AlertDialog;

    :cond_1
    iget-object v1, p0, Lbiz;->g:Landroid/widget/TextView;

    iget-object v0, p0, Lbiz;->d:Lhut;

    iget-object v0, v0, Lhut;->f:Lhol;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbiz;->d:Lhut;

    iget-object v0, v0, Lhut;->f:Lhol;

    iget-object v0, v0, Lhol;->b:Lhgz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbiz;->d:Lhut;

    iget-object v0, v0, Lhut;->f:Lhol;

    iget-object v0, v0, Lhol;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbiz;->d:Lhut;

    iget-object v0, v0, Lhut;->f:Lhol;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbiz;->d:Lhut;

    iget-object v0, v0, Lhut;->f:Lhol;

    iget-object v0, v0, Lhol;->c:Lhgz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbiz;->d:Lhut;

    iget-object v0, v0, Lhut;->f:Lhol;

    iget-object v0, v0, Lhol;->c:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lbiz;->h:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lbiz;->f:Landroid/app/AlertDialog;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbiz;->e:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 64
    return-void

    .line 63
    :cond_2
    const-string v0, ""

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method
