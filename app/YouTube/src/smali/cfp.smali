.class public final Lcfp;
.super Lfsa;
.source "SourceFile"


# instance fields
.field private final a:Leyp;

.field private final b:Lfsj;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/view/View;

.field private final e:Landroid/widget/ImageView;

.field private final f:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Leyp;Lfsj;Lfdw;Lfrz;)V
    .locals 3

    .prologue
    .line 46
    invoke-direct {p0, p4, p5}, Lfsa;-><init>(Lfdw;Lfrz;)V

    .line 47
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leyp;

    iput-object v0, p0, Lcfp;->a:Leyp;

    .line 48
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsj;

    iput-object v0, p0, Lcfp;->b:Lfsj;

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcfp;->c:Landroid/view/LayoutInflater;

    .line 51
    iget-object v0, p0, Lcfp;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f04008a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcfp;->d:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lcfp;->d:Landroid/view/View;

    const v1, 0x7f08008a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcfp;->e:Landroid/widget/ImageView;

    .line 53
    iget-object v0, p0, Lcfp;->d:Landroid/view/View;

    const v1, 0x7f08011c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcfp;->f:Landroid/widget/TextView;

    .line 55
    iget-object v0, p0, Lcfp;->d:Landroid/view/View;

    invoke-interface {p3, v0}, Lfsj;->a(Landroid/view/View;)V

    .line 56
    return-void
.end method

.method private a(Lfsg;Lfkz;)Landroid/view/View;
    .locals 3

    .prologue
    .line 60
    new-instance v1, Lfvi;

    iget-object v0, p0, Lcfp;->a:Leyp;

    iget-object v2, p0, Lcfp;->e:Landroid/widget/ImageView;

    invoke-direct {v1, v0, v2}, Lfvi;-><init>(Leyp;Landroid/widget/ImageView;)V

    .line 62
    iget-object v0, p2, Lfkz;->b:Lfnc;

    if-nez v0, :cond_0

    new-instance v0, Lfnc;

    iget-object v2, p2, Lfkz;->a:Lhob;

    iget-object v2, v2, Lhob;->a:Lhxf;

    invoke-direct {v0, v2}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p2, Lfkz;->b:Lfnc;

    :cond_0
    iget-object v2, p2, Lfkz;->b:Lfnc;

    .line 63
    if-eqz v2, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lfvi;->a(I)V

    .line 64
    const/4 v0, 0x0

    invoke-virtual {v1, v2, v0}, Lfvi;->a(Lfnc;Leyo;)V

    .line 66
    iget-object v0, p0, Lcfp;->f:Landroid/widget/TextView;

    iget-object v1, p2, Lfkz;->c:Ljava/lang/CharSequence;

    if-nez v1, :cond_1

    iget-object v1, p2, Lfkz;->a:Lhob;

    iget-object v1, v1, Lhob;->b:Lhgz;

    invoke-static {v1}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v1

    iput-object v1, p2, Lfkz;->c:Ljava/lang/CharSequence;

    :cond_1
    iget-object v1, p2, Lfkz;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcfp;->b:Lfsj;

    invoke-interface {v0, p1}, Lfsj;->a(Lfsg;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 63
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lfsg;Lfqh;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    check-cast p2, Lfkz;

    invoke-direct {p0, p1, p2}, Lcfp;->a(Lfsg;Lfkz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lfsg;Ljava/lang/Object;)Landroid/view/View;
    .locals 1

    .prologue
    .line 30
    check-cast p2, Lfkz;

    invoke-direct {p0, p1, p2}, Lcfp;->a(Lfsg;Lfkz;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
