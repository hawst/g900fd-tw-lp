.class final Ldvx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Leuc;

.field private synthetic b:Ldth;

.field private synthetic c:Ldvs;


# direct methods
.method constructor <init>(Ldvs;Leuc;Ldth;)V
    .locals 0

    .prologue
    .line 275
    iput-object p1, p0, Ldvx;->c:Ldvs;

    iput-object p2, p0, Ldvx;->a:Leuc;

    iput-object p3, p0, Ldvx;->b:Ldth;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Ldvx;->c:Ldvs;

    invoke-static {v0}, Ldvs;->b(Ldvs;)Landroid/util/Pair;

    move-result-object v1

    .line 279
    invoke-static {}, Ldvs;->a()Landroid/util/Pair;

    move-result-object v0

    if-ne v1, v0, :cond_0

    .line 280
    iget-object v0, p0, Ldvx;->a:Leuc;

    iget-object v1, p0, Ldvx;->b:Ldth;

    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Authentication failed."

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 293
    :goto_0
    return-void

    .line 283
    :cond_0
    iget-object v0, p0, Ldvx;->c:Ldvs;

    .line 284
    invoke-static {v0}, Ldvs;->c(Ldvs;)Ldrc;

    move-result-object v2

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Ldvx;->b:Ldth;

    invoke-interface {v2, v0, v1, v3}, Ldrc;->a(Ljava/lang/String;Ljava/lang/String;Ldth;)Ldst;

    move-result-object v0

    .line 285
    if-eqz v0, :cond_1

    .line 286
    iget-object v1, p0, Ldvx;->a:Leuc;

    iget-object v2, p0, Ldvx;->b:Ldth;

    new-instance v3, Ldwd;

    invoke-direct {v3, v0}, Ldwd;-><init>(Ldst;)V

    invoke-interface {v1, v2, v3}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 290
    :cond_1
    iget-object v0, p0, Ldvx;->c:Ldvs;

    invoke-static {v0}, Ldvs;->h(Ldvs;)Ldyg;

    move-result-object v0

    const-string v1, "A screen which did not exist in the combined screen storage was removed!"

    invoke-virtual {v0, v1}, Ldyg;->a(Ljava/lang/String;)V

    .line 291
    iget-object v0, p0, Ldvx;->a:Leuc;

    iget-object v1, p0, Ldvx;->b:Ldth;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Leuc;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
