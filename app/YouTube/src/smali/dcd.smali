.class public final Ldcd;
.super Lded;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Ldbr;
.implements Lddx;


# instance fields
.field private final A:Lddv;

.field private final B:Landroid/os/Handler;

.field private final C:Ldch;

.field private final D:Ldcg;

.field private E:Landroid/view/animation/Animation;

.field private F:Landroid/view/animation/Animation;

.field private G:I

.field private H:I

.field private I:Landroid/view/animation/Animation;

.field private J:Landroid/view/animation/Animation;

.field private K:Landroid/view/animation/Animation;

.field private L:Landroid/view/animation/Animation;

.field private M:Landroid/view/animation/Animation;

.field private N:Landroid/view/animation/Animation;

.field private O:Ldmb;

.field private P:Ldbs;

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field private W:Z

.field public a:Ldbt;

.field private aa:Z

.field private ab:Z

.field private ac:Ldbu;

.field private final ad:Landroid/widget/FrameLayout;

.field public b:Ldck;

.field public c:Ldcf;

.field public d:Ldcj;

.field public final e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

.field f:Z

.field private final g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final h:Landroid/widget/TextView;

.field private final i:Landroid/view/View;

.field private final j:Landroid/view/View;

.field private final k:Landroid/widget/LinearLayout;

.field private final l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final o:Landroid/widget/TextView;

.field private final p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

.field private final u:Landroid/widget/TextView;

.field private final v:Ldel;

.field private final w:Landroid/widget/RelativeLayout;

.field private final x:Landroid/widget/ProgressBar;

.field private final y:Landroid/widget/TextView;

.field private z:Ldea;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const v5, 0x7f05000b

    const v4, 0x7f05000a

    .line 150
    invoke-direct {p0, p1}, Lded;-><init>(Landroid/content/Context;)V

    .line 152
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Ldcd;->B:Landroid/os/Handler;

    .line 154
    new-instance v0, Lddv;

    new-instance v1, Ldci;

    invoke-direct {v1, p0}, Ldci;-><init>(Ldcd;)V

    invoke-direct {v0, v1}, Lddv;-><init>(Lddw;)V

    iput-object v0, p0, Ldcd;->A:Lddv;

    .line 156
    invoke-static {p1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->E:Landroid/view/animation/Animation;

    iget-object v0, p0, Ldcd;->E:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-static {p1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->F:Landroid/view/animation/Animation;

    const v0, 0x7f050007

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->K:Landroid/view/animation/Animation;

    const v0, 0x7f050008

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->L:Landroid/view/animation/Animation;

    const v0, 0x7f05000f

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->M:Landroid/view/animation/Animation;

    const v0, 0x7f050010

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->N:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Ldcd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Ldcd;->G:I

    invoke-virtual {p0}, Ldcd;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Ldcd;->H:I

    iget-object v0, p0, Ldcd;->F:Landroid/view/animation/Animation;

    iget v1, p0, Ldcd;->G:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-static {p1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->I:Landroid/view/animation/Animation;

    invoke-static {p1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Ldcd;->J:Landroid/view/animation/Animation;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Ldcd;->I:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Ldcd;->J:Landroid/view/animation/Animation;

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v0, p0, Ldcd;->J:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 158
    sget-object v0, Ldbu;->a:Ldbu;

    iput-object v0, p0, Ldcd;->ac:Ldbu;

    .line 159
    sget-object v0, Ldbs;->a:Ldbs;

    iput-object v0, p0, Ldcd;->P:Ldbs;

    .line 161
    new-instance v0, Ldel;

    invoke-direct {v0, p1}, Ldel;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ldcd;->v:Ldel;

    .line 163
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldcd;->setClipToPadding(Z)V

    .line 165
    new-instance v0, Ldch;

    invoke-direct {v0, p0}, Ldch;-><init>(Ldcd;)V

    iput-object v0, p0, Ldcd;->C:Ldch;

    .line 166
    new-instance v0, Ldcg;

    invoke-direct {v0, p0}, Ldcg;-><init>(Ldcd;)V

    iput-object v0, p0, Ldcd;->D:Ldcg;

    .line 168
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 169
    const v1, 0x7f040059

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 171
    const v0, 0x7f08018e

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Ldcd;->w:Landroid/widget/RelativeLayout;

    .line 173
    const v0, 0x7f080195

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iput-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    .line 174
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-object v1, p0, Ldcd;->C:Ldch;

    iput-object v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    .line 175
    const v0, 0x7f080192

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 176
    iget-object v0, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    const v0, 0x7f080194

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcd;->h:Landroid/widget/TextView;

    .line 178
    iget-object v0, p0, Ldcd;->h:Landroid/widget/TextView;

    sget-object v1, Lezd;->a:Lezd;

    invoke-virtual {v1, p1}, Lezd;->a(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 179
    const v0, 0x7f080190

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldcd;->i:Landroid/view/View;

    .line 180
    const v0, 0x7f080191

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldcd;->j:Landroid/view/View;

    .line 181
    const v0, 0x7f080193

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Ldcd;->k:Landroid/widget/LinearLayout;

    .line 183
    const v0, 0x7f0801a1

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Ldcd;->x:Landroid/widget/ProgressBar;

    .line 184
    const v0, 0x7f08018f

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcd;->y:Landroid/widget/TextView;

    .line 186
    const v0, 0x7f08019c

    .line 187
    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 188
    iget-object v0, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    new-instance v0, Ldmb;

    iget-object v1, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {v0, v1, p1}, Ldmb;-><init>(Lcom/google/android/libraries/youtube/common/ui/TouchImageView;Landroid/content/Context;)V

    iput-object v0, p0, Ldcd;->O:Ldmb;

    .line 191
    const v0, 0x7f08019d

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 192
    iget-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    const v0, 0x7f08019e

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 194
    iget-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    const v0, 0x7f08019a

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 197
    iget-object v0, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    const v0, 0x7f080199

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 199
    iget-object v0, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    const v0, 0x7f08019b

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    .line 201
    iget-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iget-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    sget-object v1, Lezd;->a:Lezd;

    invoke-virtual {v1, p1}, Lezd;->a(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 205
    const v0, 0x7f08019f

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 206
    iget-object v0, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    const v0, 0x7f0801a0

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 209
    iget-object v0, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    const v0, 0x7f080197

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iput-object v0, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    .line 212
    iget-object v0, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    new-instance v0, Ldek;

    invoke-direct {v0}, Ldek;-><init>()V

    iput-object v0, p0, Ldcd;->z:Ldea;

    .line 216
    iget-object v0, p0, Ldcd;->z:Ldea;

    iget-object v1, p0, Ldcd;->D:Ldcg;

    invoke-interface {v0, v1}, Ldea;->a(Ldeb;)V

    .line 217
    iget-object v0, p0, Ldcd;->z:Ldea;

    iget-object v1, p0, Ldcd;->ac:Ldbu;

    invoke-interface {v0, v1}, Ldea;->a(Ldbu;)V

    .line 219
    const v0, 0x7f080196

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    .line 220
    iget-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    const v0, 0x7f080198

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Ldcd;->ad:Landroid/widget/FrameLayout;

    .line 223
    invoke-virtual {p0}, Ldcd;->e()V

    .line 224
    return-void
.end method

.method static synthetic a(Ldcd;)Ldbt;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->a:Ldbt;

    return-object v0
.end method

.method static synthetic b(Ldcd;)Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    return-object v0
.end method

.method private b(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 833
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 834
    iget-object v0, p0, Ldcd;->i:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 835
    iget-object v0, p0, Ldcd;->L:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 848
    :cond_0
    :goto_0
    return-void

    .line 836
    :cond_1
    iget-object v0, p0, Ldcd;->j:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 837
    iget-object v0, p0, Ldcd;->N:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 839
    :cond_2
    iget-object v0, p0, Ldcd;->E:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 842
    :cond_3
    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Ldcd;->Q:Z

    if-nez v0, :cond_0

    .line 845
    invoke-virtual {p0}, Ldcd;->e()V

    goto :goto_0
.end method

.method static synthetic c(Ldcd;)Ldck;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->b:Ldck;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 851
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 852
    iget-object v0, p0, Ldcd;->i:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 853
    iget-object v0, p0, Ldcd;->K:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 854
    :cond_1
    iget-object v0, p0, Ldcd;->j:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 855
    iget-object v0, p0, Ldcd;->M:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 857
    :cond_2
    iget-object v0, p0, Ldcd;->F:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method static synthetic d(Ldcd;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->I:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic e(Ldcd;)Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->w:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic f(Ldcd;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ldcd;->i()V

    return-void
.end method

.method static synthetic g(Ldcd;)Ldch;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->C:Ldch;

    return-object v0
.end method

.method static synthetic h(Ldcd;)Ldbs;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->P:Ldbs;

    return-object v0
.end method

.method static synthetic i(Ldcd;)Ldea;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Ldcd;->z:Ldea;

    return-object v0
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 521
    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v1, Ldbs;->b:Ldbs;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v1, Ldbs;->d:Ldbs;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Ldcd;->Q:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ldcd;->B:Landroid/os/Handler;

    .line 522
    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Ldcd;->aa:Z

    if-nez v0, :cond_1

    .line 523
    iget-object v0, p0, Ldcd;->B:Landroid/os/Handler;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 525
    :cond_1
    return-void
.end method

.method private j()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 702
    iget-object v0, p0, Ldcd;->B:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 704
    iget-object v0, p0, Ldcd;->O:Ldmb;

    iget-object v3, p0, Ldcd;->P:Ldbs;

    iget-object v4, v0, Ldmb;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v4}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget-object v5, v0, Ldmb;->f:Ldbs;

    if-ne v3, v5, :cond_0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->isVisible()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    sget-object v4, Ldbs;->c:Ldbs;

    if-ne v3, v4, :cond_9

    iget-object v4, v0, Ldmb;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v5, v0, Ldmb;->h:Landroid/content/Context;

    const v6, 0x7f090134

    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Ldmb;->f:Ldbs;

    sget-object v5, Ldbs;->b:Ldbs;

    if-ne v4, v5, :cond_7

    iget-object v4, v0, Ldmb;->e:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v4}, Ldmb;->a(Landroid/graphics/drawable/AnimationDrawable;)V

    :goto_0
    iput-object v3, v0, Ldmb;->f:Ldbs;

    .line 706
    :cond_1
    iget-object v0, p0, Ldcd;->y:Landroid/widget/TextView;

    iget-object v3, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v3}, Ldbs;->a()Z

    move-result v3

    invoke-static {v0, v3}, Leze;->a(Landroid/view/View;Z)V

    .line 707
    iget-object v3, p0, Ldcd;->x:Landroid/widget/ProgressBar;

    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v4, Ldbs;->d:Ldbs;

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v4, Ldbs;->a:Ldbs;

    if-ne v0, v4, :cond_e

    :cond_2
    move v0, v2

    :goto_1
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    .line 708
    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->f:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Ldcd;->Q:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 709
    :cond_3
    iget-object v0, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->ad:Landroid/widget/FrameLayout;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->j:Landroid/view/View;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->m:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->c()Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v2

    :goto_2
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->h:Landroid/widget/TextView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->i:Landroid/view/View;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0}, Ldea;->c()V

    iget-object v0, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    iget-boolean v3, p0, Ldcd;->f:Z

    invoke-static {v0, v3}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->m:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->c()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->b()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    move v1, v2

    :cond_6
    invoke-static {p0, v1}, Leze;->a(Landroid/view/View;Z)V

    .line 713
    :goto_3
    return-void

    .line 704
    :cond_7
    iget-object v4, v0, Ldmb;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v5, v0, Ldmb;->a:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_8

    const v5, 0x7f020212

    invoke-virtual {v0, v5}, Ldmb;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v0, Ldmb;->a:Landroid/graphics/drawable/Drawable;

    :cond_8
    iget-object v5, v0, Ldmb;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_9
    sget-object v4, Ldbs;->b:Ldbs;

    if-ne v3, v4, :cond_c

    iget-object v4, v0, Ldmb;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v5, v0, Ldmb;->h:Landroid/content/Context;

    const v6, 0x7f090135

    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Ldmb;->f:Ldbs;

    sget-object v5, Ldbs;->c:Ldbs;

    if-ne v4, v5, :cond_a

    iget-object v4, v0, Ldmb;->d:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0, v4}, Ldmb;->a(Landroid/graphics/drawable/AnimationDrawable;)V

    goto/16 :goto_0

    :cond_a
    iget-object v4, v0, Ldmb;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v5, v0, Ldmb;->b:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_b

    const v5, 0x7f020210

    invoke-virtual {v0, v5}, Ldmb;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v0, Ldmb;->b:Landroid/graphics/drawable/Drawable;

    :cond_b
    iget-object v5, v0, Ldmb;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_c
    iget-object v4, v0, Ldmb;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v5, v0, Ldmb;->h:Landroid/content/Context;

    const v6, 0x7f090136

    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v4, v0, Ldmb;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v5, v0, Ldmb;->c:Landroid/graphics/drawable/Drawable;

    if-nez v5, :cond_d

    const v5, 0x7f020216

    invoke-virtual {v0, v5}, Ldmb;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, v0, Ldmb;->c:Landroid/graphics/drawable/Drawable;

    :cond_d
    iget-object v5, v0, Ldmb;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_e
    move v0, v1

    .line 707
    goto/16 :goto_1

    :cond_f
    move v0, v1

    .line 709
    goto/16 :goto_2

    .line 711
    :cond_10
    iget-object v0, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-boolean v3, p0, Ldcd;->S:Z

    invoke-static {v0, v3}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-boolean v3, p0, Ldcd;->T:Z

    invoke-static {v0, v3}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    sget-object v4, Ldbu;->c:Ldbu;

    if-eq v0, v4, :cond_13

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->c()Z

    move-result v0

    if-eqz v0, :cond_13

    move v0, v2

    :goto_4
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    sget-object v4, Ldbu;->c:Ldbu;

    if-eq v0, v4, :cond_14

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->c()Z

    move-result v0

    if-eqz v0, :cond_14

    move v0, v2

    :goto_5
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    sget-object v4, Ldbu;->c:Ldbu;

    if-eq v0, v4, :cond_15

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->c()Z

    move-result v0

    if-eqz v0, :cond_15

    move v0, v2

    :goto_6
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->o:Landroid/widget/TextView;

    iget-boolean v0, p0, Ldcd;->ab:Z

    if-eqz v0, :cond_16

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    sget-object v4, Ldbu;->c:Ldbu;

    if-ne v0, v4, :cond_16

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->c()Z

    move-result v0

    if-eqz v0, :cond_16

    move v0, v2

    :goto_7
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->ad:Landroid/widget/FrameLayout;

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->a()Z

    move-result v0

    if-nez v0, :cond_17

    move v0, v2

    :goto_8
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->j:Landroid/view/View;

    invoke-static {v0, v2}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-object v3, p0, Ldcd;->ac:Ldbu;

    iget-boolean v3, v3, Ldbu;->g:Z

    invoke-static {v0, v3}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    sget-object v4, Ldbu;->b:Ldbu;

    if-eq v0, v4, :cond_18

    iget-boolean v0, p0, Ldcd;->S:Z

    if-eqz v0, :cond_18

    move v0, v2

    :goto_9
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->h:Landroid/widget/TextView;

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    sget-object v4, Ldbu;->d:Ldbu;

    if-ne v0, v4, :cond_19

    move v0, v2

    :goto_a
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->i:Landroid/view/View;

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->c()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->m:Z

    if-nez v0, :cond_1a

    move v0, v2

    :goto_b
    invoke-static {v3, v0}, Leze;->a(Landroid/view/View;Z)V

    iget-object v3, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->b()Z

    move-result v0

    if-eqz v0, :cond_1b

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->k:Z

    if-eqz v0, :cond_1b

    move v0, v1

    :goto_c
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setVisibility(I)V

    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->l:Z

    if-eqz v0, :cond_12

    iget-boolean v0, p0, Ldcd;->V:Z

    if-nez v0, :cond_11

    iget-boolean v0, p0, Ldcd;->W:Z

    if-eqz v0, :cond_12

    :cond_11
    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v3, Ldbs;->a:Ldbs;

    if-eq v0, v3, :cond_12

    move v1, v2

    :cond_12
    iget-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-boolean v1, p0, Ldcd;->V:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setEnabled(Z)V

    iget-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    iget-boolean v1, p0, Ldcd;->W:Z

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setEnabled(Z)V

    iget-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    iget-boolean v1, p0, Ldcd;->f:Z

    invoke-static {v0, v1}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->w:Landroid/widget/RelativeLayout;

    invoke-static {v0, v2}, Leze;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0}, Ldea;->c()V

    invoke-static {p0, v2}, Leze;->a(Landroid/view/View;Z)V

    goto/16 :goto_3

    :cond_13
    move v0, v1

    goto/16 :goto_4

    :cond_14
    move v0, v1

    goto/16 :goto_5

    :cond_15
    move v0, v1

    goto/16 :goto_6

    :cond_16
    move v0, v1

    goto/16 :goto_7

    :cond_17
    move v0, v1

    goto/16 :goto_8

    :cond_18
    move v0, v1

    goto/16 :goto_9

    :cond_19
    move v0, v1

    goto/16 :goto_a

    :cond_1a
    move v0, v1

    goto :goto_b

    :cond_1b
    const/4 v0, 0x4

    goto :goto_c
.end method

.method private o(Z)V
    .locals 3

    .prologue
    .line 806
    iget-object v2, p0, Ldcd;->E:Landroid/view/animation/Animation;

    if-eqz p1, :cond_1

    iget v0, p0, Ldcd;->G:I

    int-to-long v0, v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 807
    iget-object v2, p0, Ldcd;->N:Landroid/view/animation/Animation;

    if-eqz p1, :cond_2

    iget v0, p0, Ldcd;->G:I

    int-to-long v0, v0

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 808
    iget-object v2, p0, Ldcd;->L:Landroid/view/animation/Animation;

    if-eqz p1, :cond_3

    iget v0, p0, Ldcd;->G:I

    int-to-long v0, v0

    :goto_2
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 810
    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->m:Z

    if-nez v0, :cond_0

    .line 811
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 813
    :cond_0
    iget-object v0, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 814
    iget-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 815
    iget-object v0, p0, Ldcd;->ad:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 816
    iget-object v0, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 817
    iget-object v0, p0, Ldcd;->j:Landroid/view/View;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 818
    iget-object v0, p0, Ldcd;->i:Landroid/view/View;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 819
    iget-object v0, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 820
    iget-object v0, p0, Ldcd;->h:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 821
    iget-object v0, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 822
    iget-object v0, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 823
    iget-object v0, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 824
    iget-object v0, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 825
    iget-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 826
    iget-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 827
    iget-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Ldcd;->b(Landroid/view/View;)V

    .line 829
    iget-object v0, p0, Ldcd;->z:Ldea;

    iget-object v1, p0, Ldcd;->E:Landroid/view/animation/Animation;

    invoke-interface {v0, v1}, Ldea;->a(Landroid/view/animation/Animation;)V

    .line 830
    return-void

    .line 806
    :cond_1
    iget v0, p0, Ldcd;->H:I

    int-to-long v0, v0

    goto :goto_0

    .line 807
    :cond_2
    iget v0, p0, Ldcd;->H:I

    int-to-long v0, v0

    goto :goto_1

    .line 808
    :cond_3
    iget v0, p0, Ldcd;->H:I

    int-to-long v0, v0

    goto :goto_2
.end method


# virtual methods
.method public final a(III)V
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(III)V

    .line 372
    iget-object v0, p0, Ldcd;->A:Lddv;

    invoke-virtual {v0, p1, p2}, Lddv;->a(II)V

    .line 373
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 898
    iget-object v0, p0, Ldcd;->ad:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 899
    return-void
.end method

.method public final a(Ldbs;)V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Ldcd;->P:Ldbs;

    if-eq v0, p1, :cond_2

    .line 282
    iput-object p1, p0, Ldcd;->P:Ldbs;

    .line 283
    invoke-direct {p0}, Ldcd;->j()V

    .line 284
    sget-object v0, Ldbs;->g:Ldbs;

    if-ne p1, v0, :cond_0

    .line 285
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->h:I

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 287
    :cond_0
    sget-object v0, Ldbs;->c:Ldbs;

    if-eq p1, v0, :cond_1

    sget-object v0, Ldbs;->g:Ldbs;

    if-ne p1, v0, :cond_2

    .line 288
    :cond_1
    invoke-virtual {p0}, Ldcd;->d()V

    .line 291
    :cond_2
    invoke-direct {p0}, Ldcd;->i()V

    .line 292
    return-void
.end method

.method public final a(Ldbt;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Ldcd;->a:Ldbt;

    .line 268
    return-void
.end method

.method public final a(Ldbu;)V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/4 v3, 0x0

    .line 448
    iput-object p1, p0, Ldcd;->ac:Ldbu;

    .line 449
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget v1, p1, Ldbu;->h:I

    iget-object v2, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 450
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Ldbu;->i:Z

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->f:Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a()V

    .line 451
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Ldbu;->n:Z

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->d:Z

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->requestLayout()V

    .line 452
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iget-boolean v1, p1, Ldbu;->j:Z

    iput-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->e:Z

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a:Ldfp;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->b()I

    move-result v2

    invoke-interface {v1, v2}, Ldfp;->a(I)V

    :cond_0
    iput-boolean v3, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->c:Z

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->requestLayout()V

    .line 454
    iget-object v0, p0, Ldcd;->k:Landroid/widget/LinearLayout;

    .line 455
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 456
    sget-object v1, Ldbu;->c:Ldbu;

    if-ne p1, v1, :cond_2

    .line 457
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 462
    :goto_0
    iget-object v1, p0, Ldcd;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 464
    invoke-direct {p0}, Ldcd;->j()V

    .line 465
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0, p1}, Ldea;->a(Ldbu;)V

    .line 466
    invoke-direct {p0}, Ldcd;->i()V

    .line 467
    return-void

    .line 459
    :cond_2
    invoke-virtual {v0, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 460
    iget-object v1, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 297
    if-eqz p2, :cond_0

    sget-object v0, Ldbs;->e:Ldbs;

    :goto_0
    iput-object v0, p0, Ldcd;->P:Ldbs;

    .line 299
    invoke-virtual {p0}, Ldcd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, La;->j(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 300
    invoke-virtual {p0}, Ldcd;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0900a5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 304
    :goto_1
    iget-object v1, p0, Ldcd;->y:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_3

    const-string v3, "\n\n"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 305
    invoke-virtual {p0}, Ldcd;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Ldcd;->y:Landroid/widget/TextView;

    iget-object v2, p0, Ldcd;->y:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v1, v2}, La;->a(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 307
    invoke-virtual {p0}, Ldcd;->d()V

    .line 308
    return-void

    .line 297
    :cond_0
    sget-object v0, Ldbs;->f:Ldbs;

    goto :goto_0

    .line 302
    :cond_1
    invoke-virtual {p0}, Ldcd;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0900a6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 304
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string v0, ""

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public final a(Ljava/util/List;)V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, Ldcd;->v:Ldel;

    new-instance v1, Ldce;

    invoke-direct {v1, p0}, Ldce;-><init>(Ldcd;)V

    invoke-virtual {v0, p1, v1}, Ldel;->a(Ljava/util/List;Lden;)V

    .line 390
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 972
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    if-eqz v0, :cond_0

    .line 973
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    iput-object p1, v0, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->i:Ljava/util/Map;

    .line 975
    :cond_0
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0, p1, p2}, Ldea;->a([Ljava/lang/String;I)V

    .line 428
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 352
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, v1, v1, v1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->a(III)V

    .line 353
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0, p1}, Ldea;->b(Z)V

    .line 326
    return-void
.end method

.method public final c_(Z)V
    .locals 1

    .prologue
    .line 884
    iget-boolean v0, p0, Ldcd;->U:Z

    if-eq v0, p1, :cond_0

    .line 885
    iput-boolean p1, p0, Ldcd;->U:Z

    .line 886
    if-eqz p1, :cond_1

    .line 887
    invoke-virtual {p0}, Ldcd;->h()V

    .line 888
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldcd;->o(Z)V

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 889
    :cond_1
    iget-boolean v0, p0, Ldcd;->aa:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Ldcd;->P:Ldbs;

    invoke-virtual {v0}, Ldbs;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 890
    :cond_2
    invoke-virtual {p0}, Ldcd;->g()V

    goto :goto_0

    .line 892
    :cond_3
    invoke-direct {p0}, Ldcd;->j()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 483
    iget-boolean v0, p0, Ldcd;->U:Z

    if-eqz v0, :cond_0

    .line 484
    invoke-direct {p0}, Ldcd;->j()V

    .line 494
    :goto_0
    return-void

    .line 487
    :cond_0
    invoke-virtual {p0}, Ldcd;->h()V

    .line 488
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldcd;->Q:Z

    .line 489
    invoke-direct {p0}, Ldcd;->j()V

    .line 490
    iget-object v0, p0, Ldcd;->a:Ldbt;

    if-eqz v0, :cond_1

    .line 491
    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->i()V

    .line 493
    :cond_1
    invoke-direct {p0}, Ldcd;->i()V

    goto :goto_0
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 338
    iput-boolean p1, p0, Ldcd;->V:Z

    .line 339
    invoke-direct {p0}, Ldcd;->j()V

    .line 340
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 472
    invoke-virtual {p0}, Ldcd;->h()V

    .line 473
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldcd;->Q:Z

    .line 474
    invoke-direct {p0}, Ldcd;->j()V

    .line 475
    iget-object v0, p0, Ldcd;->a:Ldbt;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->j()V

    .line 478
    :cond_0
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 345
    iput-boolean p1, p0, Ldcd;->W:Z

    .line 346
    invoke-direct {p0}, Ldcd;->j()V

    .line 347
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Ldcd;->v:Ldel;

    invoke-virtual {v0}, Ldel;->a()V

    .line 421
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0}, Ldea;->e()V

    .line 422
    return-void
.end method

.method public final f(Z)V
    .locals 3

    .prologue
    .line 433
    iput-boolean p1, p0, Ldcd;->R:Z

    .line 434
    iget-object v0, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setSelected(Z)V

    .line 435
    iget-object v1, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {p0}, Ldcd;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz p1, :cond_1

    const v0, 0x7f090130

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 439
    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_0

    .line 440
    invoke-virtual {p0}, Ldcd;->h()V

    .line 441
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ldcd;->o(Z)V

    .line 443
    :cond_0
    return-void

    .line 435
    :cond_1
    const v0, 0x7f09012f

    goto :goto_0
.end method

.method public final f_()Landroid/view/View;
    .locals 0

    .prologue
    .line 255
    return-object p0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 497
    invoke-virtual {p0}, Ldcd;->d()V

    .line 498
    iget-object v0, p0, Ldcd;->ac:Ldbu;

    iget-boolean v0, v0, Ldbu;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->ad:Landroid/widget/FrameLayout;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->j:Landroid/view/View;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->i:Landroid/view/View;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->h:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    iget-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Ldcd;->c(Landroid/view/View;)V

    .line 499
    return-void
.end method

.method public final g(Z)V
    .locals 0

    .prologue
    .line 331
    iput-boolean p1, p0, Ldcd;->S:Z

    .line 332
    invoke-direct {p0}, Ldcd;->j()V

    .line 333
    return-void
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 863
    iget-object v0, p0, Ldcd;->B:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 864
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->clearAnimation()V

    .line 865
    iget-object v0, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 866
    iget-object v0, p0, Ldcd;->h:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 867
    iget-object v0, p0, Ldcd;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 868
    iget-object v0, p0, Ldcd;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 869
    iget-object v0, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 870
    iget-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 871
    iget-object v0, p0, Ldcd;->ad:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->clearAnimation()V

    .line 872
    iget-object v0, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 873
    iget-object v0, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 874
    iget-object v0, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 875
    iget-object v0, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 876
    iget-object v0, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 877
    iget-object v0, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 878
    iget-object v0, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->clearAnimation()V

    .line 879
    iget-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 880
    return-void
.end method

.method public final h(Z)V
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Ldcd;->e:Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/TimeBar;->setEnabled(Z)V

    .line 359
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 530
    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v2, v0, :cond_0

    .line 531
    invoke-direct {p0, v1}, Ldcd;->o(Z)V

    .line 537
    :goto_0
    return v0

    .line 533
    :cond_0
    iget v2, p1, Landroid/os/Message;->what:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 534
    invoke-direct {p0}, Ldcd;->j()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 537
    goto :goto_0
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 364
    iput-boolean p1, p0, Ldcd;->ab:Z

    .line 365
    invoke-direct {p0}, Ldcd;->j()V

    .line 366
    return-void
.end method

.method public final j(Z)V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0, p1}, Ldea;->a(Z)V

    .line 320
    return-void
.end method

.method public final k(Z)V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0, p1}, Ldea;->c(Z)V

    .line 379
    return-void
.end method

.method public final l(Z)V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0, p1}, Ldea;->f(Z)V

    .line 408
    return-void
.end method

.method public final m(Z)V
    .locals 0

    .prologue
    .line 413
    iput-boolean p1, p0, Ldcd;->T:Z

    .line 414
    invoke-direct {p0}, Ldcd;->j()V

    .line 415
    return-void
.end method

.method public final n(Z)V
    .locals 0

    .prologue
    .line 502
    iput-boolean p1, p0, Ldcd;->aa:Z

    .line 503
    if-eqz p1, :cond_0

    .line 505
    invoke-virtual {p0}, Ldcd;->d()V

    .line 509
    :goto_0
    return-void

    .line 507
    :cond_0
    invoke-direct {p0}, Ldcd;->j()V

    goto :goto_0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 555
    iget-object v0, p0, Ldcd;->E:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 556
    invoke-virtual {p0}, Ldcd;->e()V

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    iget-object v0, p0, Ldcd;->J:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 558
    iget-object v0, p0, Ldcd;->w:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 559
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldcd;->Q:Z

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 550
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 544
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 567
    iget-object v1, p0, Ldcd;->a:Ldbt;

    if-eqz v1, :cond_0

    .line 568
    iget-object v1, p0, Ldcd;->q:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v1, :cond_3

    .line 569
    iget-boolean v1, p0, Ldcd;->V:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldcd;->ac:Ldbu;

    iget-boolean v1, v1, Ldbu;->l:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Ldcd;->o(Z)V

    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->d()V

    .line 598
    :cond_0
    :goto_0
    iget-object v0, p0, Ldcd;->b:Ldck;

    if-eqz v0, :cond_1

    .line 599
    iget-object v0, p0, Ldcd;->m:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v0, :cond_f

    .line 600
    iget-object v0, p0, Ldcd;->b:Ldck;

    invoke-interface {v0}, Ldck;->d()V

    .line 605
    :cond_1
    :goto_1
    iget-object v0, p0, Ldcd;->c:Ldcf;

    if-eqz v0, :cond_2

    .line 606
    iget-object v0, p0, Ldcd;->o:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    .line 607
    iget-object v0, p0, Ldcd;->c:Ldcf;

    invoke-interface {v0}, Ldcf;->d()V

    .line 610
    :cond_2
    return-void

    .line 570
    :cond_3
    iget-object v1, p0, Ldcd;->r:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v1, :cond_4

    .line 571
    iget-boolean v1, p0, Ldcd;->W:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ldcd;->ac:Ldbu;

    iget-boolean v1, v1, Ldbu;->l:Z

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Ldcd;->o(Z)V

    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->e()V

    goto :goto_0

    .line 572
    :cond_4
    iget-object v1, p0, Ldcd;->p:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v1, :cond_7

    .line 573
    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v1, Ldbs;->g:Ldbs;

    if-ne v0, v1, :cond_5

    .line 574
    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->l()V

    goto :goto_0

    .line 575
    :cond_5
    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v1, Ldbs;->b:Ldbs;

    if-ne v0, v1, :cond_6

    .line 576
    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->b()V

    goto :goto_0

    .line 577
    :cond_6
    iget-object v0, p0, Ldcd;->P:Ldbs;

    sget-object v1, Ldbs;->c:Ldbs;

    if-ne v0, v1, :cond_0

    .line 578
    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->a()V

    goto :goto_0

    .line 580
    :cond_7
    iget-object v1, p0, Ldcd;->s:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v1, :cond_8

    .line 581
    iget-object v0, p0, Ldcd;->d:Ldcj;

    invoke-interface {v0}, Ldcj;->D()V

    goto :goto_0

    .line 582
    :cond_8
    iget-object v1, p0, Ldcd;->t:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v1, :cond_9

    .line 583
    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->h()V

    goto :goto_0

    .line 584
    :cond_9
    iget-object v1, p0, Ldcd;->l:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v1, :cond_b

    .line 585
    iget-object v0, p0, Ldcd;->z:Ldea;

    instance-of v0, v0, Ldek;

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Ldcd;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04005b

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f0801b2

    invoke-virtual {p0, v0}, Ldcd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/DefaultOverflowOverlay;

    iget-object v1, p0, Ldcd;->z:Ldea;

    check-cast v1, Ldek;

    iget-object v2, v1, Ldek;->a:Ldbu;

    invoke-interface {v0, v2}, Ldea;->a(Ldbu;)V

    iget-object v2, v1, Ldek;->b:Ldeb;

    invoke-interface {v0, v2}, Ldea;->a(Ldeb;)V

    iget-boolean v2, v1, Ldek;->c:Z

    invoke-interface {v0, v2}, Ldea;->a(Z)V

    iget-boolean v2, v1, Ldek;->d:Z

    invoke-interface {v0, v2}, Ldea;->c(Z)V

    iget-boolean v2, v1, Ldek;->e:Z

    invoke-interface {v0, v2}, Ldea;->b(Z)V

    iget-object v2, v1, Ldek;->f:[Ljava/lang/String;

    iget v3, v1, Ldek;->g:I

    invoke-interface {v0, v2, v3}, Ldea;->a([Ljava/lang/String;I)V

    iget-boolean v2, v1, Ldek;->h:Z

    invoke-interface {v0, v2}, Ldea;->e(Z)V

    iget-boolean v2, v1, Ldek;->i:Z

    invoke-interface {v0, v2}, Ldea;->d(Z)V

    iget-boolean v1, v1, Ldek;->j:Z

    invoke-interface {v0, v1}, Ldea;->f(Z)V

    iput-object v0, p0, Ldcd;->z:Ldea;

    :cond_a
    iget-object v0, p0, Ldcd;->z:Ldea;

    invoke-interface {v0}, Ldea;->a()V

    iget-object v0, p0, Ldcd;->w:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Ldcd;->J:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 586
    :cond_b
    iget-object v1, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v1, :cond_e

    .line 587
    iget-object v1, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {v1}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->isSelected()Z

    move-result v1

    if-nez v1, :cond_c

    :goto_2
    iput-boolean v0, p0, Ldcd;->R:Z

    .line 588
    iget-object v1, p0, Ldcd;->g:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    invoke-virtual {p0}, Ldcd;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v0, p0, Ldcd;->R:Z

    if-eqz v0, :cond_d

    const v0, 0x7f090130

    :goto_3
    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/youtube/common/ui/TouchImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 592
    invoke-direct {p0}, Ldcd;->j()V

    .line 593
    iget-object v0, p0, Ldcd;->a:Ldbt;

    iget-boolean v1, p0, Ldcd;->R:Z

    invoke-interface {v0, v1}, Ldbt;->a(Z)V

    goto/16 :goto_0

    .line 587
    :cond_c
    const/4 v0, 0x0

    goto :goto_2

    .line 588
    :cond_d
    const v0, 0x7f09012f

    goto :goto_3

    .line 594
    :cond_e
    iget-object v0, p0, Ldcd;->u:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 595
    iget-object v0, p0, Ldcd;->a:Ldbt;

    invoke-interface {v0}, Ldbt;->g()V

    goto/16 :goto_0

    .line 601
    :cond_f
    iget-object v0, p0, Ldcd;->n:Lcom/google/android/libraries/youtube/common/ui/TouchImageView;

    if-ne p1, v0, :cond_1

    .line 602
    iget-object v0, p0, Ldcd;->b:Ldck;

    invoke-interface {v0}, Ldck;->b()V

    goto/16 :goto_1
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 615
    invoke-virtual {p0}, Ldcd;->h()V

    .line 616
    invoke-super {p0, p1}, Lded;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 648
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lddv;->b(I)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_0
    move v2, v0

    .line 649
    :goto_0
    if-eqz v2, :cond_1

    .line 650
    invoke-virtual {p0}, Ldcd;->d()V

    .line 652
    :cond_1
    iget-object v3, p0, Ldcd;->P:Ldbs;

    sget-object v4, Ldbs;->e:Ldbs;

    if-ne v3, v4, :cond_6

    if-eqz v2, :cond_6

    .line 654
    const/16 v2, 0x14

    if-eq p1, v2, :cond_2

    const/16 v2, 0x15

    if-eq p1, v2, :cond_2

    const/16 v2, 0x16

    if-eq p1, v2, :cond_2

    const/16 v2, 0x13

    if-ne p1, v2, :cond_5

    :cond_2
    move v2, v0

    :goto_1
    if-nez v2, :cond_6

    .line 655
    iget-object v1, p0, Ldcd;->a:Ldbt;

    invoke-interface {v1}, Ldbt;->k()V

    .line 658
    :cond_3
    :goto_2
    return v0

    :cond_4
    move v2, v1

    .line 648
    goto :goto_0

    :cond_5
    move v2, v1

    .line 654
    goto :goto_1

    .line 658
    :cond_6
    iget-object v2, p0, Ldcd;->A:Lddv;

    invoke-virtual {v2, p1, p2}, Lddv;->a(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-super {p0, p1, p2}, Lded;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_2
.end method

.method public final onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 665
    iget-object v0, p0, Ldcd;->A:Lddv;

    invoke-virtual {v0, p1}, Lddv;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lded;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onSizeChanged(IIII)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 696
    invoke-super {p0, p1, p2, p3, p4}, Lded;->onSizeChanged(IIII)V

    .line 697
    int-to-float v0, p1

    const v1, 0x3e2aaaab

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 698
    iget-object v1, p0, Ldcd;->y:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 699
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 622
    invoke-super {p0, p1}, Lded;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 642
    :cond_0
    :goto_0
    return v0

    .line 626
    :cond_1
    iget-boolean v1, p0, Ldcd;->U:Z

    if-eqz v1, :cond_2

    .line 627
    const/4 v0, 0x0

    goto :goto_0

    .line 629
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 630
    iget-object v1, p0, Ldcd;->P:Ldbs;

    sget-object v2, Ldbs;->e:Ldbs;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Ldcd;->a:Ldbt;

    if-eqz v1, :cond_3

    .line 631
    iget-object v1, p0, Ldcd;->a:Ldbt;

    invoke-interface {v1}, Ldbt;->k()V

    goto :goto_0

    .line 635
    :cond_3
    iget-boolean v1, p0, Ldcd;->Q:Z

    if-eqz v1, :cond_4

    .line 636
    invoke-virtual {p0}, Ldcd;->g()V

    goto :goto_0

    .line 637
    :cond_4
    iget-boolean v1, p0, Ldcd;->aa:Z

    if-nez v1, :cond_0

    .line 638
    invoke-direct {p0, v0}, Ldcd;->o(Z)V

    goto :goto_0
.end method

.method public final r_()Ldeg;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 261
    new-instance v0, Ldeg;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Ldeg;-><init>(IIZ)V

    return-object v0
.end method
