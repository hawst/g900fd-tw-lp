.class public final Lgrx;
.super Landroid/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lgrw;


# instance fields
.field public a:Lgqo;

.field public final b:Lgry;

.field public c:Lgqh;

.field private d:Lgql;

.field private e:Lgqe;

.field private f:Lgri;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Ljava/util/concurrent/PriorityBlockingQueue;

.field private l:Lgrq;

.field private final m:Lgry;

.field private final n:Lgry;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 80
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 43
    sget-object v0, Lgql;->a:Lgql;

    iput-object v0, p0, Lgrx;->d:Lgql;

    .line 44
    sget-object v0, Lgqe;->a:Lgqe;

    iput-object v0, p0, Lgrx;->e:Lgqe;

    .line 45
    sget-object v0, Lgri;->a:Lgri;

    iput-object v0, p0, Lgrx;->f:Lgri;

    .line 64
    iput v1, p0, Lgrx;->g:I

    .line 65
    iput v1, p0, Lgrx;->h:I

    .line 66
    iput v1, p0, Lgrx;->i:I

    .line 67
    iput v1, p0, Lgrx;->j:I

    .line 72
    new-instance v0, Lgry;

    invoke-direct {v0}, Lgry;-><init>()V

    iput-object v0, p0, Lgrx;->m:Lgry;

    .line 74
    new-instance v0, Lgry;

    invoke-direct {v0}, Lgry;-><init>()V

    iput-object v0, p0, Lgrx;->n:Lgry;

    .line 76
    new-instance v0, Lgry;

    invoke-direct {v0}, Lgry;-><init>()V

    iput-object v0, p0, Lgrx;->b:Lgry;

    .line 81
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lgrx;->k:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 82
    iget-object v0, p0, Lgrx;->b:Lgry;

    new-instance v1, Lgru;

    invoke-direct {v1}, Lgru;-><init>()V

    invoke-virtual {v0, v1}, Lgry;->a(Lgsa;)Lgsa;

    .line 83
    return-void
.end method

.method private static a(Lgqo;I)Ljava/util/List;
    .locals 14

    .prologue
    .line 320
    iget-wide v0, p0, Lgqo;->d:J

    .line 322
    int-to-long v2, p1

    div-long v8, v0, v2

    .line 323
    const-wide/16 v0, 0x2

    div-long v10, v8, v0

    .line 325
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 326
    const-wide/16 v2, -0x1

    .line 327
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, p1, :cond_3

    .line 328
    int-to-long v0, v6

    mul-long/2addr v0, v8

    const-wide/16 v4, 0x2

    div-long v4, v8, v4

    add-long/2addr v4, v0

    .line 329
    invoke-virtual {p0, v4, v5}, Lgqo;->a(J)J

    move-result-wide v0

    .line 331
    cmp-long v12, v0, v4

    if-gez v12, :cond_1

    .line 332
    sub-long v12, v4, v0

    cmp-long v12, v12, v10

    if-gez v12, :cond_2

    .line 341
    :cond_0
    :goto_1
    cmp-long v4, v2, v0

    if-eqz v4, :cond_4

    .line 342
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    :goto_2
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-wide v2, v0

    goto :goto_0

    .line 335
    :cond_1
    cmp-long v12, v0, v4

    if-lez v12, :cond_2

    .line 336
    sub-long v12, v0, v4

    cmp-long v12, v12, v10

    if-lez v12, :cond_0

    :cond_2
    move-wide v0, v4

    goto :goto_1

    .line 346
    :cond_3
    return-object v7

    :cond_4
    move-wide v0, v2

    goto :goto_2
.end method

.method private a(Lgrm;)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lgrx;->k:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 285
    return-void
.end method


# virtual methods
.method public final a()Lgqo;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lgrx;->a:Lgqo;

    return-object v0
.end method

.method public final a(JJLjava/lang/String;)Lgrp;
    .locals 9

    .prologue
    .line 143
    new-instance v1, Lgrp;

    iget-object v0, p0, Lgrx;->b:Lgry;

    .line 145
    iget-object v6, v0, Lgry;->a:Lgsa;

    check-cast v6, Lgru;

    const/16 v8, 0xa

    move-wide v2, p1

    move-wide v4, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v8}, Lgrp;-><init>(JJLgru;Ljava/lang/String;I)V

    .line 146
    invoke-direct {p0, v1}, Lgrx;->a(Lgrm;)V

    .line 147
    return-object v1
.end method

.method public final b()Lgsa;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lgrx;->b:Lgry;

    return-object v0
.end method

.method public final declared-synchronized c()Lgsa;
    .locals 1

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgrx;->m:Lgry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Lgsa;
    .locals 1

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgrx;->n:Lgry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()V
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 196
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lgrx;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrx;->a:Lgqo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgrx;->c:Lgqh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 235
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 200
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lgrx;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lgrx;->a:Lgqo;

    invoke-virtual {v1}, Lgqo;->a()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_6

    :goto_1
    int-to-float v2, v0

    div-float v1, v2, v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget v2, p0, Lgrx;->g:I

    if-ne v0, v2, :cond_2

    iget v2, p0, Lgrx;->h:I

    if-eq v1, v2, :cond_7

    :cond_2
    iput v0, p0, Lgrx;->g:I

    iput v1, p0, Lgrx;->h:I

    move v0, v6

    :goto_2
    if-eqz v0, :cond_3

    .line 204
    iget v0, p0, Lgrx;->g:I

    if-lez v0, :cond_8

    iget v0, p0, Lgrx;->h:I

    if-lez v0, :cond_8

    move v0, v6

    :goto_3
    invoke-static {v0}, Lb;->c(Z)V

    iget v0, p0, Lgrx;->g:I

    iget v1, p0, Lgrx;->h:I

    mul-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x2

    int-to-long v2, v0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v0

    sub-long v0, v4, v0

    const-wide/32 v4, 0x200000

    sub-long v4, v0, v4

    iget-object v0, p0, Lgrx;->b:Lgry;

    iget-object v0, v0, Lgry;->a:Lgsa;

    check-cast v0, Lgru;

    invoke-virtual {v0}, Lgru;->b()I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v0, v4

    const-wide/16 v4, 0x0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-virtual {p0}, Lgrx;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v4, p0, Lgrx;->g:I

    div-int v4, v1, v4

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    double-to-int v2, v2

    iput v2, p0, Lgrx;->i:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lgrx;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, 0x0

    iget v3, p0, Lgrx;->i:I

    sub-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lgrx;->j:I

    .line 205
    invoke-virtual {p0}, Lgrx;->g()V

    .line 208
    :cond_3
    iget-object v0, p0, Lgrx;->b:Lgry;

    iget-object v0, v0, Lgry;->a:Lgsa;

    if-eqz v0, :cond_9

    move v0, v6

    :goto_4
    invoke-static {v0}, Lb;->c(Z)V

    .line 209
    iget-object v0, p0, Lgrx;->k:Ljava/util/concurrent/PriorityBlockingQueue;

    if-eqz v0, :cond_a

    move v0, v6

    :goto_5
    invoke-static {v0}, Lb;->c(Z)V

    .line 212
    iget-object v0, p0, Lgrx;->m:Lgry;

    iget-object v0, v0, Lgry;->a:Lgsa;

    check-cast v0, Lgrn;

    .line 213
    if-nez v0, :cond_4

    .line 214
    iget-object v0, p0, Lgrx;->a:Lgqo;

    iget v1, p0, Lgrx;->i:I

    invoke-static {v0, v1}, Lgrx;->a(Lgqo;I)Ljava/util/List;

    move-result-object v2

    .line 215
    new-instance v0, Lgro;

    iget-object v1, p0, Lgrx;->a:Lgqo;

    iget-object v3, p0, Lgrx;->b:Lgry;

    .line 216
    iget-object v3, v3, Lgry;->a:Lgsa;

    check-cast v3, Lgru;

    const-string v4, "Overview"

    const/16 v5, 0x64

    invoke-direct/range {v0 .. v5}, Lgro;-><init>(Lgqo;Ljava/util/List;Lgru;Ljava/lang/String;I)V

    .line 217
    invoke-direct {p0, v0}, Lgrx;->a(Lgrm;)V

    .line 218
    iget-object v1, p0, Lgrx;->m:Lgry;

    invoke-virtual {v1, v0}, Lgry;->a(Lgsa;)Lgsa;

    .line 222
    :cond_4
    iget-object v0, p0, Lgrx;->n:Lgry;

    iget-object v0, v0, Lgry;->a:Lgsa;

    check-cast v0, Lgrn;

    .line 223
    if-nez v0, :cond_5

    .line 224
    iget-object v0, p0, Lgrx;->a:Lgqo;

    iget v1, p0, Lgrx;->j:I

    invoke-static {v0, v1}, Lgrx;->a(Lgqo;I)Ljava/util/List;

    move-result-object v2

    .line 225
    new-instance v0, Lgro;

    iget-object v1, p0, Lgrx;->a:Lgqo;

    iget-object v3, p0, Lgrx;->b:Lgry;

    .line 226
    iget-object v3, v3, Lgry;->a:Lgsa;

    check-cast v3, Lgru;

    const-string v4, "Seek Preview"

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lgro;-><init>(Lgqo;Ljava/util/List;Lgru;Ljava/lang/String;I)V

    .line 227
    invoke-direct {p0, v0}, Lgrx;->a(Lgrm;)V

    .line 228
    iget-object v1, p0, Lgrx;->n:Lgry;

    invoke-virtual {v1, v0}, Lgry;->a(Lgsa;)Lgsa;

    .line 232
    :cond_5
    iget-object v0, p0, Lgrx;->l:Lgrq;

    if-nez v0, :cond_0

    .line 233
    iget-object v0, p0, Lgrx;->l:Lgrq;

    if-nez v0, :cond_b

    move v0, v6

    :goto_6
    invoke-static {v0}, Lb;->c(Z)V

    new-instance v0, Lgrq;

    invoke-virtual {p0}, Lgrx;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lgrx;->a:Lgqo;

    iget v3, p0, Lgrx;->g:I

    iget v4, p0, Lgrx;->h:I

    iget-object v5, p0, Lgrx;->k:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v6, p0, Lgrx;->d:Lgql;

    iget-object v7, p0, Lgrx;->e:Lgqe;

    iget-object v8, p0, Lgrx;->f:Lgri;

    iget-object v9, p0, Lgrx;->c:Lgqh;

    invoke-direct/range {v0 .. v9}, Lgrq;-><init>(Landroid/content/Context;Lgqo;IILjava/util/concurrent/PriorityBlockingQueue;Lgql;Lgqe;Lgri;Lgqh;)V

    iput-object v0, p0, Lgrx;->l:Lgrq;

    iget-object v0, p0, Lgrx;->l:Lgrq;

    invoke-virtual {v0}, Lgrq;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 200
    :cond_6
    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto/16 :goto_1

    :cond_7
    move v0, v7

    goto/16 :goto_2

    :cond_8
    move v0, v7

    .line 204
    goto/16 :goto_3

    :cond_9
    move v0, v7

    .line 208
    goto/16 :goto_4

    :cond_a
    move v0, v7

    .line 209
    goto/16 :goto_5

    :cond_b
    move v0, v7

    .line 233
    goto :goto_6
.end method

.method public declared-synchronized f()V
    .locals 1

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lgrx;->l:Lgrq;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lgrx;->l:Lgrq;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lb;->c(Z)V

    iget-object v0, p0, Lgrx;->l:Lgrq;

    invoke-virtual {v0}, Lgrq;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lgrx;->l:Lgrq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    :cond_0
    monitor-exit p0

    return-void

    .line 262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized g()V
    .locals 2

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lgrx;->f()V

    .line 290
    iget-object v0, p0, Lgrx;->m:Lgry;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgry;->a(Lgsa;)Lgsa;

    move-result-object v0

    .line 291
    if-eqz v0, :cond_0

    .line 292
    invoke-interface {v0}, Lgsa;->a()V

    .line 295
    :cond_0
    iget-object v0, p0, Lgrx;->n:Lgry;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgry;->a(Lgsa;)Lgsa;

    move-result-object v0

    .line 296
    if-eqz v0, :cond_1

    .line 297
    invoke-interface {v0}, Lgsa;->a()V

    .line 302
    :cond_1
    iget-object v0, p0, Lgrx;->k:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    .line 303
    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lgrx;->k:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 305
    iget-object v0, p0, Lgrx;->b:Lgry;

    new-instance v1, Lgru;

    invoke-direct {v1}, Lgru;-><init>()V

    invoke-virtual {v0, v1}, Lgry;->a(Lgsa;)Lgsa;

    move-result-object v0

    .line 306
    invoke-interface {v0}, Lgsa;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    monitor-exit p0

    return-void

    .line 288
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 156
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgrx;->setRetainInstance(Z)V

    .line 157
    if-eqz p1, :cond_0

    .line 158
    const-string v0, "editable_video"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lgqo;

    iput-object v0, p0, Lgrx;->a:Lgqo;

    .line 160
    :cond_0
    return-void
.end method

.method public final onDestroy()V
    .locals 0

    .prologue
    .line 183
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 184
    invoke-virtual {p0}, Lgrx;->g()V

    .line 185
    return-void
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 171
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 172
    invoke-virtual {p0}, Lgrx;->f()V

    .line 173
    return-void
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 164
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 165
    invoke-virtual {p0}, Lgrx;->e()V

    .line 166
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 178
    const-string v0, "editable_video"

    iget-object v1, p0, Lgrx;->a:Lgqo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 179
    return-void
.end method
