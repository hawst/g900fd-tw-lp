.class public Lflp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final h:Ljava/util/Set;

.field private static final i:Ljava/util/Set;


# instance fields
.field public final a:Lfnd;

.field public final b:Lfnd;

.field public final c:Lfnd;

.field public final d:Lfnd;

.field public final e:Lfnd;

.field public final f:Ljava/util/List;

.field public final g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 41
    sput-object v0, Lflp;->h:Ljava/util/Set;

    sget-object v1, Lfnf;->c:Lfnf;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 43
    sput-object v0, Lflp;->i:Ljava/util/Set;

    sget-object v1, Lfnf;->a:Lfnf;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 177
    new-instance v0, Lflq;

    invoke-direct {v0}, Lflq;-><init>()V

    sput-object v0, Lflp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflp;-><init>(Lhqw;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Leab;)V
    .locals 4

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iget-boolean v0, p1, Leab;->l:Z

    iput-boolean v0, p0, Lflp;->g:Z

    .line 110
    iget-boolean v0, p1, Leab;->a:Z

    if-eqz v0, :cond_0

    .line 111
    new-instance v0, Lfnd;

    .line 112
    iget-object v1, p1, Leab;->b:Leae;

    invoke-direct {v0, v1}, Lfnd;-><init>(Leae;)V

    iput-object v0, p0, Lflp;->b:Lfnd;

    .line 118
    :goto_0
    iget-boolean v0, p1, Leab;->c:Z

    if-eqz v0, :cond_1

    .line 119
    new-instance v0, Lfnd;

    .line 120
    iget-object v1, p1, Leab;->d:Leae;

    invoke-direct {v0, v1}, Lfnd;-><init>(Leae;)V

    iput-object v0, p0, Lflp;->c:Lfnd;

    .line 126
    :goto_1
    iget-boolean v0, p1, Leab;->e:Z

    if-eqz v0, :cond_2

    .line 127
    new-instance v0, Lfnd;

    .line 128
    iget-object v1, p1, Leab;->f:Leae;

    invoke-direct {v0, v1}, Lfnd;-><init>(Leae;)V

    iput-object v0, p0, Lflp;->d:Lfnd;

    .line 134
    :goto_2
    iget-boolean v0, p1, Leab;->g:Z

    if-eqz v0, :cond_3

    .line 135
    new-instance v0, Lfnd;

    iget-object v1, p1, Leab;->h:Leae;

    invoke-direct {v0, v1}, Lfnd;-><init>(Leae;)V

    iput-object v0, p0, Lflp;->e:Lfnd;

    .line 140
    :goto_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflp;->f:Ljava/util/List;

    .line 141
    iget-object v0, p1, Leab;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leae;

    .line 142
    iget-object v2, p0, Lflp;->f:Ljava/util/List;

    new-instance v3, Lfnd;

    invoke-direct {v3, v0}, Lfnd;-><init>(Leae;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 114
    :cond_0
    new-instance v0, Lfnd;

    const-string v1, "https://s.youtube.com/api/stats/playback"

    sget-object v2, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lflp;->b:Lfnd;

    goto :goto_0

    .line 122
    :cond_1
    new-instance v0, Lfnd;

    const-string v1, "https://s.youtube.com/api/stats/delayplay"

    sget-object v2, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lflp;->c:Lfnd;

    goto :goto_1

    .line 130
    :cond_2
    new-instance v0, Lfnd;

    const-string v1, "https://s.youtube.com/api/stats/watchtime"

    sget-object v2, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lflp;->d:Lfnd;

    goto :goto_2

    .line 137
    :cond_3
    new-instance v0, Lfnd;

    const-string v1, "https://s.youtube.com/api/stats/qoe"

    sget-object v2, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v0, v1, v2}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lflp;->e:Lfnd;

    goto :goto_3

    .line 145
    :cond_4
    iget-boolean v0, p1, Leab;->j:Z

    if-eqz v0, :cond_5

    new-instance v0, Lfnd;

    .line 146
    iget-object v1, p1, Leab;->k:Leae;

    invoke-direct {v0, v1}, Lfnd;-><init>(Leae;)V

    :goto_5
    iput-object v0, p0, Lflp;->a:Lfnd;

    .line 147
    return-void

    .line 146
    :cond_5
    const/4 v0, 0x0

    goto :goto_5
.end method

.method public constructor <init>(Lhqw;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    if-eqz p1, :cond_2

    iget-boolean v0, p1, Lhqw;->g:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lflp;->g:Z

    .line 60
    new-instance v2, Lfnd;

    if-eqz p1, :cond_3

    iget-object v0, p1, Lhqw;->a:Lhxq;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lhqw;->a:Lhxq;

    iget-object v0, v0, Lhxq;->b:Ljava/lang/String;

    :goto_1
    sget-object v3, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v2, v0, v3}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v2, p0, Lflp;->b:Lfnd;

    .line 66
    if-eqz p1, :cond_4

    iget-object v0, p1, Lhqw;->b:Lhxq;

    if-eqz v0, :cond_4

    .line 67
    new-instance v0, Lfnd;

    iget-object v2, p1, Lhqw;->b:Lhxq;

    sget-object v3, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v0, v2, v3}, Lfnd;-><init>(Lhxq;Ljava/util/Set;)V

    iput-object v0, p0, Lflp;->c:Lfnd;

    .line 76
    :goto_2
    new-instance v2, Lfnd;

    if-eqz p1, :cond_5

    iget-object v0, p1, Lhqw;->c:Lhxq;

    if-eqz v0, :cond_5

    iget-object v0, p1, Lhqw;->c:Lhxq;

    iget-object v0, v0, Lhxq;->b:Ljava/lang/String;

    :goto_3
    sget-object v3, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v2, v0, v3}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v2, p0, Lflp;->d:Lfnd;

    .line 82
    new-instance v2, Lfnd;

    if-eqz p1, :cond_6

    iget-object v0, p1, Lhqw;->e:Lhxq;

    if-eqz v0, :cond_6

    iget-object v0, p1, Lhqw;->e:Lhxq;

    iget-object v0, v0, Lhxq;->b:Ljava/lang/String;

    :goto_4
    sget-object v3, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v2, v0, v3}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v2, p0, Lflp;->e:Lfnd;

    .line 88
    if-eqz p1, :cond_7

    iget-object v0, p1, Lhqw;->h:Lhxq;

    if-eqz v0, :cond_7

    new-instance v0, Lfnd;

    iget-object v2, p1, Lhqw;->h:Lhxq;

    sget-object v3, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v0, v2, v3}, Lfnd;-><init>(Lhxq;Ljava/util/Set;)V

    :goto_5
    iput-object v0, p0, Lflp;->a:Lfnd;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflp;->f:Ljava/util/List;

    .line 93
    if-eqz p1, :cond_0

    iget-object v0, p1, Lhqw;->d:Lhxq;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lflp;->f:Ljava/util/List;

    new-instance v2, Lfnd;

    iget-object v3, p1, Lhqw;->d:Lhxq;

    iget-object v3, v3, Lhxq;->b:Ljava/lang/String;

    sget-object v4, Lflp;->h:Ljava/util/Set;

    invoke-direct {v2, v3, v4, v1}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;I)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lhqw;->f:Lhxq;

    if-eqz v0, :cond_1

    .line 100
    iget-object v0, p0, Lflp;->f:Ljava/util/List;

    new-instance v1, Lfnd;

    iget-object v2, p1, Lhqw;->f:Lhxq;

    iget-object v2, v2, Lhxq;->b:Ljava/lang/String;

    sget-object v3, Lflp;->i:Ljava/util/Set;

    iget-object v4, p1, Lhqw;->f:Lhxq;

    iget v4, v4, Lhxq;->c:I

    invoke-direct {v1, v2, v3, v4}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 59
    goto/16 :goto_0

    .line 60
    :cond_3
    const-string v0, "https://s.youtube.com/api/stats/playback"

    goto/16 :goto_1

    .line 71
    :cond_4
    new-instance v0, Lfnd;

    const-string v2, "https://s.youtube.com/api/stats/delayplay"

    sget-object v3, Lfnd;->a:Ljava/util/Set;

    invoke-direct {v0, v2, v3}, Lfnd;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    iput-object v0, p0, Lflp;->c:Lfnd;

    goto/16 :goto_2

    .line 76
    :cond_5
    const-string v0, "https://s.youtube.com/api/stats/watchtime"

    goto :goto_3

    .line 82
    :cond_6
    const-string v0, "https://s.youtube.com/api/stats/qoe"

    goto :goto_4

    .line 88
    :cond_7
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public final a()Leab;
    .locals 3

    .prologue
    .line 205
    new-instance v1, Leab;

    invoke-direct {v1}, Leab;-><init>()V

    .line 206
    iget-object v0, p0, Lflp;->b:Lfnd;

    .line 207
    invoke-virtual {v0}, Lfnd;->a()Leae;

    move-result-object v0

    invoke-virtual {v1, v0}, Leab;->a(Leae;)Leab;

    move-result-object v0

    iget-object v2, p0, Lflp;->c:Lfnd;

    .line 208
    invoke-virtual {v2}, Lfnd;->a()Leae;

    move-result-object v2

    invoke-virtual {v0, v2}, Leab;->b(Leae;)Leab;

    move-result-object v0

    iget-object v2, p0, Lflp;->d:Lfnd;

    .line 209
    invoke-virtual {v2}, Lfnd;->a()Leae;

    move-result-object v2

    invoke-virtual {v0, v2}, Leab;->c(Leae;)Leab;

    move-result-object v0

    iget-object v2, p0, Lflp;->e:Lfnd;

    .line 210
    invoke-virtual {v2}, Lfnd;->a()Leae;

    move-result-object v2

    invoke-virtual {v0, v2}, Leab;->d(Leae;)Leab;

    move-result-object v0

    iget-boolean v2, p0, Lflp;->g:Z

    .line 211
    invoke-virtual {v0, v2}, Leab;->a(Z)Leab;

    .line 212
    iget-object v0, p0, Lflp;->a:Lfnd;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lflp;->a:Lfnd;

    invoke-virtual {v0}, Lfnd;->a()Leae;

    move-result-object v0

    invoke-virtual {v1, v0}, Leab;->f(Leae;)Leab;

    .line 215
    :cond_0
    iget-object v0, p0, Lflp;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfnd;

    .line 216
    invoke-virtual {v0}, Lfnd;->a()Leae;

    move-result-object v0

    invoke-virtual {v1, v0}, Leab;->e(Leae;)Leab;

    goto :goto_0

    .line 218
    :cond_1
    return-object v1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 224
    if-nez p1, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v0

    .line 227
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    check-cast p1, Lflp;

    .line 231
    iget-object v1, p0, Lflp;->b:Lfnd;

    iget-object v2, p1, Lflp;->b:Lfnd;

    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflp;->c:Lfnd;

    iget-object v2, p1, Lflp;->c:Lfnd;

    .line 232
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflp;->d:Lfnd;

    iget-object v2, p1, Lflp;->d:Lfnd;

    .line 233
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflp;->e:Lfnd;

    iget-object v2, p1, Lflp;->e:Lfnd;

    .line 234
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflp;->f:Ljava/util/List;

    iget-object v2, p1, Lflp;->f:Ljava/util/List;

    .line 235
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflp;->a:Lfnd;

    iget-object v2, p1, Lflp;->a:Lfnd;

    .line 236
    invoke-static {v1, v2}, Lb;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lflp;->g:Z

    iget-boolean v2, p1, Lflp;->g:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lflp;->a()Leab;

    move-result-object v0

    invoke-static {p1, v0}, La;->a(Landroid/os/Parcel;Lida;)V

    .line 202
    return-void
.end method
