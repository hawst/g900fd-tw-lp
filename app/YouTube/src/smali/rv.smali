.class final Lrv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field public final a:Landroid/os/Messenger;

.field public b:Lrl;

.field final c:Landroid/util/SparseArray;

.field final synthetic d:Lrt;


# direct methods
.method public constructor <init>(Lrt;Landroid/os/Messenger;I)V
    .locals 1

    .prologue
    .line 489
    iput-object p1, p0, Lrv;->d:Lrt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lrv;->c:Landroid/util/SparseArray;

    .line 490
    iput-object p2, p0, Lrv;->a:Landroid/os/Messenger;

    .line 491
    return-void
.end method


# virtual methods
.method public final a(I)Lrq;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrq;

    return-object v0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 496
    :try_start_0
    iget-object v1, p0, Lrv;->a:Landroid/os/Messenger;

    invoke-virtual {v1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    const/4 v0, 0x1

    .line 501
    :goto_0
    return v0

    .line 499
    :catch_0
    move-exception v1

    invoke-virtual {p0}, Lrv;->binderDied()V

    goto :goto_0
.end method

.method public final a(Lrl;)Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lrv;->b:Lrl;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lrv;->b:Lrl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lrv;->b:Lrl;

    invoke-virtual {v0, p1}, Lrl;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 549
    :cond_0
    iput-object p1, p0, Lrv;->b:Lrl;

    .line 550
    iget-object v0, p0, Lrv;->d:Lrt;

    invoke-static {v0}, Lrt;->a(Lrt;)Z

    move-result v0

    .line 552
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 505
    iget-object v0, p0, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v2

    .line 506
    :goto_0
    if-ge v1, v3, :cond_0

    .line 507
    iget-object v0, p0, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrq;

    invoke-virtual {v0}, Lrq;->a()V

    .line 506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 509
    :cond_0
    iget-object v0, p0, Lrv;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 511
    iget-object v0, p0, Lrv;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 513
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lrv;->a(Lrl;)Z

    .line 514
    return-void
.end method

.method public final binderDied()V
    .locals 3

    .prologue
    .line 558
    iget-object v0, p0, Lrv;->d:Lrt;

    invoke-static {v0}, Lrt;->b(Lrt;)Lrw;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lrv;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lrw;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 559
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lrv;->a:Landroid/os/Messenger;

    invoke-static {v0}, Lrt;->a(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
