.class public Lghm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lghp;
.implements Lghr;


# instance fields
.field final a:Landroid/content/SharedPreferences;

.field final b:Landroid/util/SparseArray;

.field final c:Lghr;


# direct methods
.method public constructor <init>(Lghr;Landroid/content/SharedPreferences;Landroid/util/SparseArray;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghr;

    iput-object v0, p0, Lghm;->c:Lghr;

    .line 31
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lghm;->a:Landroid/content/SharedPreferences;

    .line 32
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseArray;

    iput-object v0, p0, Lghm;->b:Landroid/util/SparseArray;

    .line 33
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lghm;->c:Lghr;

    invoke-interface {v0}, Lghr;->a()Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lghm;->c:Lghr;

    invoke-interface {v0}, Lghr;->b()Z

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lghm;->a:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lghq;->b(Landroid/content/SharedPreferences;)Lghq;

    move-result-object v0

    iget-object v0, v0, Lghq;->a:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    const-string v0, "deviceregistration/v1/devices"

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, "plus/v1whitelisted"

    return-object v0
.end method

.method public f()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 57
    iget-object v0, p0, Lghm;->a:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lghn;->b(Landroid/content/SharedPreferences;)Lghn;

    move-result-object v0

    iget-object v1, p0, Lghm;->a:Landroid/content/SharedPreferences;

    iget-object v0, v0, Lghn;->e:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "http://127.0.0.1:8787"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "PPGHost"

    const-string v2, "http://127.0.0.1:8787"

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public g()[B
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lghm;->a:Landroid/content/SharedPreferences;

    .line 74
    invoke-static {v0}, Lghn;->b(Landroid/content/SharedPreferences;)Lghn;

    move-result-object v0

    .line 75
    sget-object v1, Lghu;->a:[I

    invoke-virtual {v0}, Lghn;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 84
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x10

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled case: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :pswitch_0
    iget-object v0, p0, Lghm;->b:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 82
    :goto_0
    return-object v0

    .line 80
    :pswitch_1
    iget-object v0, p0, Lghm;->b:Landroid/util/SparseArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    goto :goto_0

    .line 82
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lghm;->a:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lghn;->b(Landroid/content/SharedPreferences;)Lghn;

    move-result-object v0

    sget-object v1, Lghn;->d:Lghn;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
