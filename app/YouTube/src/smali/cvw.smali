.class public final Lcvw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field a:Lcvx;

.field public b:J

.field final synthetic c:Lcvv;


# direct methods
.method constructor <init>(Lcvv;Lcvx;J)V
    .locals 1

    .prologue
    .line 117
    iput-object p1, p0, Lcvw;->c:Lcvv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object p2, p0, Lcvw;->a:Lcvx;

    .line 119
    iput-wide p3, p0, Lcvw;->b:J

    .line 120
    return-void
.end method


# virtual methods
.method public final a(Lcvw;)I
    .locals 4

    .prologue
    .line 136
    iget-wide v0, p0, Lcvw;->b:J

    iget-wide v2, p1, Lcvw;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 137
    iget-wide v0, p0, Lcvw;->b:J

    iget-wide v2, p1, Lcvw;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 138
    const/4 v0, -0x1

    .line 149
    :goto_0
    return v0

    .line 140
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 143
    :cond_1
    iget-object v0, p0, Lcvw;->c:Lcvv;

    iget v0, v0, Lcvv;->h:I

    iget-object v1, p1, Lcvw;->c:Lcvv;

    iget v1, v1, Lcvv;->h:I

    if-eq v0, v1, :cond_2

    .line 144
    iget-object v0, p1, Lcvw;->c:Lcvv;

    iget v0, v0, Lcvv;->h:I

    iget-object v1, p0, Lcvw;->c:Lcvv;

    iget v1, v1, Lcvv;->h:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lcvw;->a:Lcvx;

    invoke-virtual {v0}, Lcvx;->ordinal()I

    move-result v0

    iget-object v1, p1, Lcvw;->a:Lcvx;

    invoke-virtual {v1}, Lcvx;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 147
    iget-object v0, p0, Lcvw;->a:Lcvx;

    invoke-virtual {v0}, Lcvx;->ordinal()I

    move-result v0

    iget-object v1, p1, Lcvw;->a:Lcvx;

    invoke-virtual {v1}, Lcvx;->ordinal()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0

    .line 149
    :cond_3
    iget-object v0, p0, Lcvw;->c:Lcvv;

    iget-object v0, v0, Lcvv;->g:Ljava/lang/String;

    iget-object v1, p1, Lcvw;->c:Lcvv;

    iget-object v1, v1, Lcvv;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 112
    check-cast p1, Lcvw;

    invoke-virtual {p0, p1}, Lcvw;->a(Lcvw;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 154
    iget-object v0, p0, Lcvw;->a:Lcvx;

    invoke-virtual {v0}, Lcvx;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcvw;->b:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
