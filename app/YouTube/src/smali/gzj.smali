.class public final Lgzj;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lgzj;


# instance fields
.field private b:Lhgz;

.field private c:Lhgz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7950
    const/4 v0, 0x0

    new-array v0, v0, [Lgzj;

    sput-object v0, Lgzj;->a:[Lgzj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7951
    invoke-direct {p0}, Lidf;-><init>()V

    .line 7954
    iput-object v0, p0, Lgzj;->b:Lhgz;

    .line 7957
    iput-object v0, p0, Lgzj;->c:Lhgz;

    .line 7951
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 8000
    const/4 v0, 0x0

    .line 8001
    iget-object v1, p0, Lgzj;->b:Lhgz;

    if-eqz v1, :cond_0

    .line 8002
    const/4 v0, 0x1

    iget-object v1, p0, Lgzj;->b:Lhgz;

    .line 8003
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8005
    :cond_0
    iget-object v1, p0, Lgzj;->c:Lhgz;

    if-eqz v1, :cond_1

    .line 8006
    const/4 v1, 0x2

    iget-object v2, p0, Lgzj;->c:Lhgz;

    .line 8007
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8009
    :cond_1
    iget-object v1, p0, Lgzj;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8010
    iput v0, p0, Lgzj;->J:I

    .line 8011
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 7947
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lgzj;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lgzj;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lgzj;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lgzj;->b:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzj;->b:Lhgz;

    :cond_2
    iget-object v0, p0, Lgzj;->b:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lgzj;->c:Lhgz;

    if-nez v0, :cond_3

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Lgzj;->c:Lhgz;

    :cond_3
    iget-object v0, p0, Lgzj;->c:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 7988
    iget-object v0, p0, Lgzj;->b:Lhgz;

    if-eqz v0, :cond_0

    .line 7989
    const/4 v0, 0x1

    iget-object v1, p0, Lgzj;->b:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7991
    :cond_0
    iget-object v0, p0, Lgzj;->c:Lhgz;

    if-eqz v0, :cond_1

    .line 7992
    const/4 v0, 0x2

    iget-object v1, p0, Lgzj;->c:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 7994
    :cond_1
    iget-object v0, p0, Lgzj;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 7996
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7969
    if-ne p1, p0, :cond_1

    .line 7974
    :cond_0
    :goto_0
    return v0

    .line 7970
    :cond_1
    instance-of v2, p1, Lgzj;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 7971
    :cond_2
    check-cast p1, Lgzj;

    .line 7972
    iget-object v2, p0, Lgzj;->b:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lgzj;->b:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lgzj;->c:Lhgz;

    if-nez v2, :cond_5

    iget-object v2, p1, Lgzj;->c:Lhgz;

    if-nez v2, :cond_3

    .line 7973
    :goto_2
    iget-object v2, p0, Lgzj;->I:Ljava/util/List;

    if-nez v2, :cond_6

    iget-object v2, p1, Lgzj;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 7974
    goto :goto_0

    .line 7972
    :cond_4
    iget-object v2, p0, Lgzj;->b:Lhgz;

    iget-object v3, p1, Lgzj;->b:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lgzj;->c:Lhgz;

    iget-object v3, p1, Lgzj;->c:Lhgz;

    .line 7973
    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lgzj;->I:Ljava/util/List;

    iget-object v3, p1, Lgzj;->I:Ljava/util/List;

    .line 7974
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7978
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 7980
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzj;->b:Lhgz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 7981
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lgzj;->c:Lhgz;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 7982
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lgzj;->I:Ljava/util/List;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 7983
    return v0

    .line 7980
    :cond_0
    iget-object v0, p0, Lgzj;->b:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 7981
    :cond_1
    iget-object v0, p0, Lgzj;->c:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_1

    .line 7982
    :cond_2
    iget-object v1, p0, Lgzj;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_2
.end method
