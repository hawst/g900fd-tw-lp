.class public Lfnl;
.super Lfic;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lfqh;


# instance fields
.field public final a:Lhzu;

.field public b:Ljava/lang/String;

.field public final c:Lhog;

.field public d:Lfmy;

.field private final e:Lfqh;

.field private f:Lfnc;

.field private g:Ljava/lang/CharSequence;

.field private h:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhzu;Lfqh;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lfic;-><init>()V

    .line 35
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhzu;

    iput-object v0, p0, Lfnl;->a:Lhzu;

    .line 36
    iput-object p2, p0, Lfnl;->e:Lfqh;

    .line 40
    iget-object v0, p1, Lhzu;->d:Lhog;

    iput-object v0, p0, Lfnl;->c:Lhog;

    .line 41
    iget-object v0, p0, Lfnl;->c:Lhog;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lfnl;->c:Lhog;

    iget-object v0, v0, Lhog;->c:Lhbm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfnl;->c:Lhog;

    iget-object v0, v0, Lhog;->c:Lhbm;

    iget-object v0, v0, Lhbm;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lfnl;->c:Lhog;

    iget-object v0, v0, Lhog;->c:Lhbm;

    iget-object v0, v0, Lhbm;->a:Ljava/lang/String;

    iput-object v0, p0, Lfnl;->b:Ljava/lang/String;

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v0, p0, Lfnl;->c:Lhog;

    iget-object v0, v0, Lhog;->o:Lhns;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnl;->c:Lhog;

    iget-object v0, v0, Lhog;->o:Lhns;

    iget-object v0, v0, Lhns;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lfnl;->c:Lhog;

    iget-object v0, v0, Lhog;->o:Lhns;

    iget-object v0, v0, Lhns;->a:Ljava/lang/String;

    iput-object v0, p0, Lfnl;->b:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lfau;)V
    .locals 0

    .prologue
    .line 113
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 114
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfnl;->c:Lhog;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lfnl;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lfnl;->a:Lhzu;

    iget-object v0, v0, Lhzu;->b:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnl;->g:Ljava/lang/CharSequence;

    .line 59
    :cond_0
    iget-object v0, p0, Lfnl;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lfnl;->a:Lhzu;

    iget-object v0, v0, Lhzu;->g:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lfnl;->e:Lfqh;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lfnl;->h:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lfnl;->a:Lhzu;

    iget-object v0, v0, Lhzu;->e:Lhgz;

    .line 65
    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnl;->h:Ljava/lang/CharSequence;

    .line 67
    :cond_0
    iget-object v0, p0, Lfnl;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Lfnc;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lfnl;->f:Lfnc;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lfnc;

    iget-object v1, p0, Lfnl;->a:Lhzu;

    iget-object v1, v1, Lhzu;->a:Lhxf;

    invoke-direct {v0, v1}, Lfnc;-><init>(Lhxf;)V

    iput-object v0, p0, Lfnl;->f:Lfnc;

    .line 74
    :cond_0
    iget-object v0, p0, Lfnl;->f:Lfnc;

    return-object v0
.end method
