.class public final Lfnz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Libv;

.field private b:Ljava/lang/CharSequence;

.field private c:Lfol;

.field private d:Lfod;

.field private e:Lfon;


# direct methods
.method public constructor <init>(Libv;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lfnz;->a:Libv;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lfnz;->b:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    iget-object v0, v0, Lhss;->b:Lick;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    iget-object v0, v0, Lhss;->b:Lick;

    iget-object v0, v0, Lick;->a:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfnz;->b:Ljava/lang/CharSequence;

    .line 43
    :cond_0
    iget-object v0, p0, Lfnz;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Lfol;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lfnz;->c:Lfol;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    iget-object v0, v0, Lhss;->c:Licl;

    if-eqz v0, :cond_0

    .line 50
    new-instance v0, Lfol;

    iget-object v1, p0, Lfnz;->a:Libv;

    iget-object v1, v1, Libv;->a:Lhss;

    iget-object v1, v1, Lhss;->c:Licl;

    invoke-direct {v0, v1}, Lfol;-><init>(Licl;)V

    iput-object v0, p0, Lfnz;->c:Lfol;

    .line 53
    :cond_0
    iget-object v0, p0, Lfnz;->c:Lfol;

    return-object v0
.end method

.method public final c()Lfod;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lfnz;->d:Lfod;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    iget-object v0, v0, Lhss;->a:Licc;

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Lfod;

    iget-object v1, p0, Lfnz;->a:Libv;

    iget-object v1, v1, Libv;->a:Lhss;

    iget-object v1, v1, Lhss;->a:Licc;

    invoke-direct {v0, v1}, Lfod;-><init>(Licc;)V

    iput-object v0, p0, Lfnz;->d:Lfod;

    .line 63
    :cond_0
    iget-object v0, p0, Lfnz;->d:Lfod;

    return-object v0
.end method

.method public final d()Lfon;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lfnz;->e:Lfon;

    if-nez v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfnz;->a:Libv;

    iget-object v0, v0, Libv;->a:Lhss;

    iget-object v0, v0, Lhss;->d:Lico;

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Lfon;

    iget-object v1, p0, Lfnz;->a:Libv;

    iget-object v1, v1, Libv;->a:Lhss;

    iget-object v1, v1, Lhss;->d:Lico;

    invoke-direct {v0, v1}, Lfon;-><init>(Lico;)V

    iput-object v0, p0, Lfnz;->e:Lfon;

    .line 72
    :cond_0
    iget-object v0, p0, Lfnz;->e:Lfon;

    return-object v0
.end method
