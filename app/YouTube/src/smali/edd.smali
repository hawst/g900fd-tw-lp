.class public final Ledd;
.super Ledc;
.source "SourceFile"


# instance fields
.field private final i:Leee;

.field private final j:Z

.field private final k:J

.field private l:Z

.field private m:Lebv;

.field private n:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lefc;Lefg;Lecw;IJJILeee;ZJ)V
    .locals 3

    .prologue
    .line 48
    invoke-direct/range {p0 .. p9}, Ledc;-><init>(Lefc;Lefg;Lecw;IJJI)V

    .line 49
    iput-object p10, p0, Ledd;->i:Leee;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Ledd;->j:Z

    .line 51
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ledd;->k:J

    .line 52
    return-void
.end method


# virtual methods
.method public final a(JZ)Z
    .locals 3

    .prologue
    .line 62
    iget-wide v0, p0, Ledd;->k:J

    add-long/2addr v0, p1

    .line 63
    iget-object v2, p0, Ledd;->i:Leee;

    invoke-interface {v2, v0, v1, p3}, Leee;->a(JZ)Z

    move-result v0

    .line 64
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Ledd;->c()V

    .line 67
    :cond_0
    return v0
.end method

.method public final a(Leby;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 102
    iget-object v3, p0, Lecj;->e:Lefx;

    .line 103
    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 104
    iget-object v0, p0, Ledd;->i:Leee;

    invoke-interface {v0, v3, p1}, Leee;->a(Lefx;Leby;)I

    move-result v0

    .line 105
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_2

    .line 106
    :goto_1
    if-eqz v1, :cond_0

    .line 107
    iget-wide v2, p1, Leby;->e:J

    iget-wide v4, p0, Ledd;->k:J

    sub-long/2addr v2, v4

    iput-wide v2, p1, Leby;->e:J

    .line 109
    :cond_0
    return v1

    :cond_1
    move v0, v2

    .line 103
    goto :goto_0

    :cond_2
    move v1, v2

    .line 105
    goto :goto_1
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Ledd;->i:Leee;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, Leee;->a(JZ)Z

    .line 57
    invoke-virtual {p0}, Ledd;->c()V

    .line 58
    return-void
.end method

.method public final i()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 72
    iget-boolean v0, p0, Ledd;->l:Z

    if-nez v0, :cond_0

    .line 73
    iget-boolean v0, p0, Ledd;->j:Z

    if-eqz v0, :cond_3

    .line 76
    iget-object v3, p0, Lecj;->e:Lefx;

    .line 77
    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 78
    iget-object v0, p0, Ledd;->i:Leee;

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Leee;->a(Lefx;Leby;)I

    move-result v0

    .line 79
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Ledd;->l:Z

    .line 85
    :goto_2
    iget-boolean v0, p0, Ledd;->l:Z

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Ledd;->i:Leee;

    invoke-interface {v0}, Leee;->c()Lebv;

    move-result-object v0

    iput-object v0, p0, Ledd;->m:Lebv;

    .line 87
    iget-object v0, p0, Ledd;->i:Leee;

    invoke-interface {v0}, Leee;->d()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ledd;->n:Ljava/util/Map;

    .line 90
    :cond_0
    iget-boolean v0, p0, Ledd;->l:Z

    return v0

    :cond_1
    move v0, v2

    .line 77
    goto :goto_0

    :cond_2
    move v1, v2

    .line 79
    goto :goto_1

    .line 83
    :cond_3
    iput-boolean v1, p0, Ledd;->l:Z

    goto :goto_2
.end method

.method public final j()Z
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lecj;->e:Lefx;

    .line 96
    iget-object v1, p0, Ledd;->i:Leee;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Leee;->a(Lefx;Leby;)I

    move-result v0

    .line 97
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Lebv;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ledd;->m:Lebv;

    return-object v0
.end method

.method public final l()Ljava/util/Map;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Ledd;->n:Ljava/util/Map;

    return-object v0
.end method
