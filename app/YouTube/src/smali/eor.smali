.class public final Leor;
.super Lekr;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lejt;Lejc;)V
    .locals 6

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lekr;-><init>(Landroid/content/Context;Landroid/os/Looper;Lejt;Lejc;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lepa;->a(Landroid/os/IBinder;)Leoz;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Leor;->h()V

    :try_start_0
    invoke-virtual {p0}, Leor;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Leoz;

    iget-object v1, p0, Lekr;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Leoz;->a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "Herrevad"

    const-string v1, "NetworkQualityClient not connected soon after checkConnected.  Discarding network quality data"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final a(Lelk;Lekv;)V
    .locals 3

    const v0, 0x5e3530

    iget-object v1, p0, Lekr;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p1, p2, v0, v1, v2}, Lelk;->k(Lelh;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected final e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.mdm.services.START"

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.mdm.internal.INetworkQualityService"

    return-object v0
.end method
