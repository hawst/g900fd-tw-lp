.class public final Lhal;
.super Lidf;
.source "SourceFile"


# static fields
.field public static final a:[Lhal;


# instance fields
.field public b:Lhqz;

.field private c:Lhrc;

.field private d:Lhrm;

.field private e:Lhrl;

.field private f:Lhrk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11349
    const/4 v0, 0x0

    new-array v0, v0, [Lhal;

    sput-object v0, Lhal;->a:[Lhal;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11350
    invoke-direct {p0}, Lidf;-><init>()V

    .line 11353
    iput-object v0, p0, Lhal;->b:Lhqz;

    .line 11356
    iput-object v0, p0, Lhal;->c:Lhrc;

    .line 11359
    iput-object v0, p0, Lhal;->d:Lhrm;

    .line 11362
    iput-object v0, p0, Lhal;->e:Lhrl;

    .line 11365
    iput-object v0, p0, Lhal;->f:Lhrk;

    .line 11350
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 11426
    const/4 v0, 0x0

    .line 11427
    iget-object v1, p0, Lhal;->b:Lhqz;

    if-eqz v1, :cond_0

    .line 11428
    const v0, 0x2f31076

    iget-object v1, p0, Lhal;->b:Lhqz;

    .line 11429
    invoke-static {v0, v1}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 11431
    :cond_0
    iget-object v1, p0, Lhal;->c:Lhrc;

    if-eqz v1, :cond_1

    .line 11432
    const v1, 0x3a198cf

    iget-object v2, p0, Lhal;->c:Lhrc;

    .line 11433
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11435
    :cond_1
    iget-object v1, p0, Lhal;->d:Lhrm;

    if-eqz v1, :cond_2

    .line 11436
    const v1, 0x3b0fcbc

    iget-object v2, p0, Lhal;->d:Lhrm;

    .line 11437
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11439
    :cond_2
    iget-object v1, p0, Lhal;->e:Lhrl;

    if-eqz v1, :cond_3

    .line 11440
    const v1, 0x3b9034d

    iget-object v2, p0, Lhal;->e:Lhrl;

    .line 11441
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11443
    :cond_3
    iget-object v1, p0, Lhal;->f:Lhrk;

    if-eqz v1, :cond_4

    .line 11444
    const v1, 0x3e543e8    # 1.3475001E-36f

    iget-object v2, p0, Lhal;->f:Lhrk;

    .line 11445
    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11447
    :cond_4
    iget-object v1, p0, Lhal;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11448
    iput v0, p0, Lhal;->J:I

    .line 11449
    return v0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 2

    .prologue
    .line 11346
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v1, p0, Lhal;->I:Ljava/util/List;

    if-nez v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lhal;->I:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lhal;->I:Ljava/util/List;

    invoke-static {v1, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lhal;->b:Lhqz;

    if-nez v0, :cond_2

    new-instance v0, Lhqz;

    invoke-direct {v0}, Lhqz;-><init>()V

    iput-object v0, p0, Lhal;->b:Lhqz;

    :cond_2
    iget-object v0, p0, Lhal;->b:Lhqz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lhal;->c:Lhrc;

    if-nez v0, :cond_3

    new-instance v0, Lhrc;

    invoke-direct {v0}, Lhrc;-><init>()V

    iput-object v0, p0, Lhal;->c:Lhrc;

    :cond_3
    iget-object v0, p0, Lhal;->c:Lhrc;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lhal;->d:Lhrm;

    if-nez v0, :cond_4

    new-instance v0, Lhrm;

    invoke-direct {v0}, Lhrm;-><init>()V

    iput-object v0, p0, Lhal;->d:Lhrm;

    :cond_4
    iget-object v0, p0, Lhal;->d:Lhrm;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lhal;->e:Lhrl;

    if-nez v0, :cond_5

    new-instance v0, Lhrl;

    invoke-direct {v0}, Lhrl;-><init>()V

    iput-object v0, p0, Lhal;->e:Lhrl;

    :cond_5
    iget-object v0, p0, Lhal;->e:Lhrl;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lhal;->f:Lhrk;

    if-nez v0, :cond_6

    new-instance v0, Lhrk;

    invoke-direct {v0}, Lhrk;-><init>()V

    iput-object v0, p0, Lhal;->f:Lhrk;

    :cond_6
    iget-object v0, p0, Lhal;->f:Lhrk;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x179883b2 -> :sswitch_1
        0x1d0cc67a -> :sswitch_2
        0x1d87e5e2 -> :sswitch_3
        0x1dc81a6a -> :sswitch_4
        0x1f2a1f42 -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 2

    .prologue
    .line 11405
    iget-object v0, p0, Lhal;->b:Lhqz;

    if-eqz v0, :cond_0

    .line 11406
    const v0, 0x2f31076

    iget-object v1, p0, Lhal;->b:Lhqz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11408
    :cond_0
    iget-object v0, p0, Lhal;->c:Lhrc;

    if-eqz v0, :cond_1

    .line 11409
    const v0, 0x3a198cf

    iget-object v1, p0, Lhal;->c:Lhrc;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11411
    :cond_1
    iget-object v0, p0, Lhal;->d:Lhrm;

    if-eqz v0, :cond_2

    .line 11412
    const v0, 0x3b0fcbc

    iget-object v1, p0, Lhal;->d:Lhrm;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11414
    :cond_2
    iget-object v0, p0, Lhal;->e:Lhrl;

    if-eqz v0, :cond_3

    .line 11415
    const v0, 0x3b9034d

    iget-object v1, p0, Lhal;->e:Lhrl;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11417
    :cond_3
    iget-object v0, p0, Lhal;->f:Lhrk;

    if-eqz v0, :cond_4

    .line 11418
    const v0, 0x3e543e8    # 1.3475001E-36f

    iget-object v1, p0, Lhal;->f:Lhrk;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    .line 11420
    :cond_4
    iget-object v0, p0, Lhal;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    .line 11422
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11380
    if-ne p1, p0, :cond_1

    .line 11388
    :cond_0
    :goto_0
    return v0

    .line 11381
    :cond_1
    instance-of v2, p1, Lhal;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    .line 11382
    :cond_2
    check-cast p1, Lhal;

    .line 11383
    iget-object v2, p0, Lhal;->b:Lhqz;

    if-nez v2, :cond_4

    iget-object v2, p1, Lhal;->b:Lhqz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Lhal;->c:Lhrc;

    if-nez v2, :cond_5

    iget-object v2, p1, Lhal;->c:Lhrc;

    if-nez v2, :cond_3

    .line 11384
    :goto_2
    iget-object v2, p0, Lhal;->d:Lhrm;

    if-nez v2, :cond_6

    iget-object v2, p1, Lhal;->d:Lhrm;

    if-nez v2, :cond_3

    .line 11385
    :goto_3
    iget-object v2, p0, Lhal;->e:Lhrl;

    if-nez v2, :cond_7

    iget-object v2, p1, Lhal;->e:Lhrl;

    if-nez v2, :cond_3

    .line 11386
    :goto_4
    iget-object v2, p0, Lhal;->f:Lhrk;

    if-nez v2, :cond_8

    iget-object v2, p1, Lhal;->f:Lhrk;

    if-nez v2, :cond_3

    .line 11387
    :goto_5
    iget-object v2, p0, Lhal;->I:Ljava/util/List;

    if-nez v2, :cond_9

    iget-object v2, p1, Lhal;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 11388
    goto :goto_0

    .line 11383
    :cond_4
    iget-object v2, p0, Lhal;->b:Lhqz;

    iget-object v3, p1, Lhal;->b:Lhqz;

    invoke-virtual {v2, v3}, Lhqz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Lhal;->c:Lhrc;

    iget-object v3, p1, Lhal;->c:Lhrc;

    .line 11384
    invoke-virtual {v2, v3}, Lhrc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lhal;->d:Lhrm;

    iget-object v3, p1, Lhal;->d:Lhrm;

    .line 11385
    invoke-virtual {v2, v3}, Lhrm;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lhal;->e:Lhrl;

    iget-object v3, p1, Lhal;->e:Lhrl;

    .line 11386
    invoke-virtual {v2, v3}, Lhrl;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Lhal;->f:Lhrk;

    iget-object v3, p1, Lhal;->f:Lhrk;

    .line 11387
    invoke-virtual {v2, v3}, Lhrk;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Lhal;->I:Ljava/util/List;

    iget-object v3, p1, Lhal;->I:Ljava/util/List;

    .line 11388
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 11392
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 11394
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhal;->b:Lhqz;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 11395
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhal;->c:Lhrc;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 11396
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhal;->d:Lhrm;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 11397
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhal;->e:Lhrl;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 11398
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lhal;->f:Lhrk;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 11399
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhal;->I:Ljava/util/List;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 11400
    return v0

    .line 11394
    :cond_0
    iget-object v0, p0, Lhal;->b:Lhqz;

    invoke-virtual {v0}, Lhqz;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11395
    :cond_1
    iget-object v0, p0, Lhal;->c:Lhrc;

    invoke-virtual {v0}, Lhrc;->hashCode()I

    move-result v0

    goto :goto_1

    .line 11396
    :cond_2
    iget-object v0, p0, Lhal;->d:Lhrm;

    invoke-virtual {v0}, Lhrm;->hashCode()I

    move-result v0

    goto :goto_2

    .line 11397
    :cond_3
    iget-object v0, p0, Lhal;->e:Lhrl;

    invoke-virtual {v0}, Lhrl;->hashCode()I

    move-result v0

    goto :goto_3

    .line 11398
    :cond_4
    iget-object v0, p0, Lhal;->f:Lhrk;

    invoke-virtual {v0}, Lhrk;->hashCode()I

    move-result v0

    goto :goto_4

    .line 11399
    :cond_5
    iget-object v1, p0, Lhal;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_5
.end method
