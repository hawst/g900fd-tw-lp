.class public final Lggf;
.super Ledb;
.source "SourceFile"

# interfaces
.implements Leav;


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ledb;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lggf;->a:Z

    .line 31
    iput-object p1, p0, Lggf;->b:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 113
    if-nez p1, :cond_1

    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_0

    if-nez p2, :cond_1

    .line 114
    :cond_0
    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lggf;->b:Ljava/lang/String;

    .line 116
    :cond_1
    return-void
.end method

.method public final a(Ljava/util/List;J[Lecw;Leda;)V
    .locals 9

    .prologue
    .line 40
    iget-object v0, p0, Lggf;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 41
    const/4 v3, 0x0

    .line 42
    const/4 v2, 0x0

    .line 43
    const/4 v1, 0x0

    .line 44
    array-length v6, p4

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_2

    aget-object v0, p4, v4

    .line 45
    instance-of v7, v0, Lfrk;

    if-eqz v7, :cond_c

    .line 46
    check-cast v0, Lfrk;

    .line 49
    if-nez v3, :cond_0

    iget-boolean v7, v0, Lfrk;->g:Z

    if-eqz v7, :cond_0

    move-object v3, v0

    .line 52
    :cond_0
    if-nez v2, :cond_1

    move-object v2, v0

    .line 55
    :cond_1
    if-nez v1, :cond_c

    if-nez v5, :cond_c

    iget-object v7, p0, Lggf;->b:Ljava/lang/String;

    iget-object v8, v0, Lfrk;->f:Ljava/lang/String;

    .line 56
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    move-object v1, v2

    move-object v2, v3

    .line 44
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 60
    :cond_2
    if-nez v1, :cond_5

    .line 61
    if-eqz v3, :cond_4

    move-object v1, v3

    .line 62
    :goto_2
    if-nez v1, :cond_5

    .line 63
    const-string v0, "Unable to select audio by language or default or fallback. Giving up"

    invoke-static {v0}, Lezp;->d(Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x0

    aget-object v0, p4, v0

    iput-object v0, p5, Leda;->c:Lecw;

    .line 85
    :cond_3
    :goto_3
    return-void

    :cond_4
    move-object v1, v2

    .line 61
    goto :goto_2

    .line 68
    :cond_5
    iput-object v1, p5, Leda;->c:Lecw;

    .line 69
    iget-boolean v0, p0, Lggf;->a:Z

    if-nez v0, :cond_7

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lggf;->a:Z

    .line 76
    :goto_4
    const/4 v0, 0x1

    move v2, v0

    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 77
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ledc;

    iget-object v0, v0, Ledc;->a:Lecw;

    .line 78
    instance-of v3, v0, Lfrk;

    if-eqz v3, :cond_6

    check-cast v0, Lfrk;

    .line 79
    iget-object v3, v1, Lfrk;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, v0, Lfrk;->f:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_b

    .line 81
    :cond_6
    iput v2, p5, Leda;->a:I

    goto :goto_3

    .line 72
    :cond_7
    const/4 v0, 0x1

    iput v0, p5, Leda;->b:I

    goto :goto_4

    .line 79
    :cond_8
    iget-object v3, v1, Lfrk;->f:Ljava/lang/String;

    iget-object v4, v0, Lfrk;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    const/4 v0, 0x1

    goto :goto_6

    :cond_9
    iget-boolean v3, v1, Lfrk;->g:Z

    iget-boolean v0, v0, Lfrk;->g:Z

    if-eq v3, v0, :cond_a

    const/4 v0, 0x1

    goto :goto_6

    :cond_a
    const/4 v0, 0x0

    goto :goto_6

    .line 76
    :cond_b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    :cond_c
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1
.end method
