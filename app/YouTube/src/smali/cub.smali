.class public final Lcub;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcst;

.field final b:Lcup;

.field public final c:Lfcd;

.field final d:Lfbu;

.field final e:Lctb;

.field final f:Ljava/util/concurrent/Executor;

.field public final g:Ljava/util/concurrent/Executor;

.field public final h:Lcul;

.field final i:Levn;

.field public final j:Ljava/util/Set;

.field public k:Ljava/lang/String;

.field private final l:Lcss;


# direct methods
.method public constructor <init>(Lcst;Lcup;Lfcd;Lfbu;Lctb;Lcss;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Levn;)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcst;

    iput-object v0, p0, Lcub;->a:Lcst;

    .line 109
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcup;

    iput-object v0, p0, Lcub;->b:Lcup;

    .line 110
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfcd;

    iput-object v0, p0, Lcub;->c:Lfcd;

    .line 111
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfbu;

    iput-object v0, p0, Lcub;->d:Lfbu;

    .line 112
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctb;

    iput-object v0, p0, Lcub;->e:Lctb;

    .line 113
    invoke-static {p6}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcss;

    iput-object v0, p0, Lcub;->l:Lcss;

    .line 114
    invoke-static {p7}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcub;->f:Ljava/util/concurrent/Executor;

    .line 115
    invoke-static {p8}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcub;->g:Ljava/util/concurrent/Executor;

    .line 116
    new-instance v0, Lcul;

    invoke-direct {v0, p8}, Lcul;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Lcub;->h:Lcul;

    .line 117
    invoke-static {p9}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Levn;

    iput-object v0, p0, Lcub;->i:Levn;

    .line 118
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcub;->j:Ljava/util/Set;

    .line 119
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcub;->a:Lcst;

    iget-object v0, v0, Lcst;->a:Lctj;

    .line 497
    invoke-interface {v0, p1}, Lctj;->a(Z)V

    .line 498
    invoke-interface {v0}, Lctj;->g()V

    .line 499
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 458
    iget-object v0, p0, Lcub;->a:Lcst;

    invoke-virtual {v0}, Lcst;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 460
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcub;->a(Z)V

    .line 462
    :cond_0
    iget-object v0, p0, Lcub;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcug;

    invoke-direct {v1, p0}, Lcug;-><init>(Lcub;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 471
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcub;->l:Lcss;

    iget-object v1, p0, Lcub;->c:Lfcd;

    invoke-virtual {v0, v1}, Lcss;->a(Lfcd;)Landroid/content/Intent;

    move-result-object v0

    .line 264
    const/16 v1, 0x387

    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 265
    return-void
.end method

.method public final a(Landroid/app/Activity;Lcuk;)V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcub;->j:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v0, p0, Lcub;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 181
    invoke-virtual {p0, p1}, Lcub;->a(Landroid/app/Activity;)V

    .line 183
    :cond_0
    return-void
.end method

.method public a(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 343
    iget-object v0, p0, Lcub;->h:Lcul;

    new-instance v1, Lcui;

    .line 345
    invoke-static {p2}, Lgiv;->a(Ljava/lang/String;)Lgiv;

    move-result-object v2

    invoke-direct {v1, p0, p1, v2}, Lcui;-><init>(Lcub;Landroid/app/Activity;Lgiv;)V

    .line 343
    invoke-virtual {v0, v1}, Lcul;->execute(Ljava/lang/Runnable;)V

    .line 346
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lcub;->a:Lcst;

    invoke-virtual {v0}, Lcst;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 482
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcub;->a(Z)V

    .line 484
    :cond_0
    iget-object v0, p0, Lcub;->f:Ljava/util/concurrent/Executor;

    new-instance v1, Lcuh;

    invoke-direct {v1, p0, p1}, Lcuh;-><init>(Lcub;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 493
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 127
    const-string v1, "Signing out because: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcub;->a(Z)V

    .line 129
    iget-object v0, p0, Lcub;->i:Levn;

    new-instance v1, Lfcc;

    invoke-direct {v1}, Lfcc;-><init>()V

    invoke-virtual {v0, v1}, Levn;->c(Ljava/lang/Object;)V

    .line 130
    return-void

    .line 127
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
