.class public final Lzm;
.super Lyk;
.source "SourceFile"


# instance fields
.field public a:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "stco"

    invoke-direct {p0, v0}, Lyk;-><init>(Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x0

    new-array v0, v0, [J

    iput-object v0, p0, Lzm;->a:[J

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lzm;->c(Ljava/nio/ByteBuffer;)J

    .line 53
    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v0

    invoke-static {v0, v1}, La;->d(J)I

    move-result v1

    .line 54
    new-array v0, v1, [J

    iput-object v0, p0, Lzm;->a:[J

    .line 55
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 56
    iget-object v2, p0, Lzm;->a:[J

    invoke-static {p1}, La;->a(Ljava/nio/ByteBuffer;)J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    return-void
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lzm;->d(Ljava/nio/ByteBuffer;)V

    .line 64
    iget-object v0, p0, Lzm;->a:[J

    array-length v0, v0

    int-to-long v0, v0

    invoke-static {p1, v0, v1}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 65
    iget-object v1, p0, Lzm;->a:[J

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-wide v4, v1, v0

    .line 66
    invoke-static {p1, v4, v5}, Lyf;->b(Ljava/nio/ByteBuffer;J)V

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method public final c()[J
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lzm;->a:[J

    return-object v0
.end method

.method protected final d_()J
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lzm;->a:[J

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x8

    int-to-long v0, v0

    return-wide v0
.end method
