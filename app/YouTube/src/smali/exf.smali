.class public final Lexf;
.super Lws;
.source "SourceFile"


# instance fields
.field final d:Lewi;

.field volatile e:Z

.field f:Z

.field private final g:Ljava/util/List;

.field private h:Lwc;


# direct methods
.method public constructor <init>(Lezn;Ljava/util/concurrent/Executor;Lewi;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, v0, v0}, Lws;-><init>(Lwc;Lwj;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lexf;->g:Ljava/util/List;

    .line 44
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lewi;

    iput-object v0, p0, Lexf;->d:Lewi;

    .line 45
    new-instance v0, Lexg;

    invoke-direct {v0, p0, p2}, Lexg;-><init>(Lexf;Ljava/util/concurrent/Executor;)V

    invoke-virtual {p1, v0}, Lezn;->a(Ljava/lang/Runnable;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Lwp;)Lwp;
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0, p1}, Lws;->a(Lwp;)Lwp;

    move-result-object p1

    .line 179
    :goto_0
    return-object p1

    .line 173
    :cond_0
    monitor-enter p0

    .line 175
    :try_start_0
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0, p1}, Lws;->a(Lwp;)Lwp;

    move-result-object p1

    monitor-exit p0

    goto :goto_0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 178
    :cond_1
    :try_start_1
    iget-object v0, p0, Lexf;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0}, Lws;->a()V

    .line 100
    :goto_0
    return-void

    .line 93
    :cond_0
    monitor-enter p0

    .line 95
    :try_start_0
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0}, Lws;->a()V

    .line 100
    :goto_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 98
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lexf;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0}, Lws;->b()V

    .line 116
    :goto_0
    return-void

    .line 109
    :cond_0
    monitor-enter p0

    .line 111
    :try_start_0
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0}, Lws;->b()V

    .line 116
    :goto_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 114
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lexf;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0}, Lws;->c()I

    move-result v0

    return v0

    .line 126
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final d()Lwc;
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lexf;->e:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lexf;->d:Lewi;

    invoke-interface {v0}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    invoke-virtual {v0}, Lws;->d()Lwc;

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    .line 137
    :cond_0
    iget-object v0, p0, Lexf;->h:Lwc;

    if-nez v0, :cond_1

    .line 138
    new-instance v0, Lxx;

    invoke-direct {v0}, Lxx;-><init>()V

    iput-object v0, p0, Lexf;->h:Lwc;

    .line 140
    :cond_1
    iget-object v0, p0, Lexf;->h:Lwc;

    goto :goto_0
.end method

.method declared-synchronized e()V
    .locals 3

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lexf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lwp;

    .line 83
    iget-object v1, p0, Lexf;->d:Lewi;

    invoke-interface {v1}, Lewi;->e_()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lws;

    invoke-virtual {v1, v0}, Lws;->a(Lwp;)Lwp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 85
    :cond_0
    :try_start_1
    iget-object v0, p0, Lexf;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    monitor-exit p0

    return-void
.end method
