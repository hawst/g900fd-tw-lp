.class public final Libb;
.super Lidf;
.source "SourceFile"


# instance fields
.field public a:Lhgz;

.field public b:[Lhgz;

.field public c:Lias;

.field public d:Liaz;

.field public e:Liaw;

.field public f:Z

.field public g:[B

.field private h:Lhog;

.field private i:Lhgz;

.field private j:Lhnh;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lidf;-><init>()V

    iput-object v1, p0, Libb;->a:Lhgz;

    iput-object v1, p0, Libb;->h:Lhog;

    sget-object v0, Lhgz;->a:[Lhgz;

    iput-object v0, p0, Libb;->b:[Lhgz;

    iput-object v1, p0, Libb;->c:Lias;

    iput-object v1, p0, Libb;->d:Liaz;

    iput-object v1, p0, Libb;->e:Liaw;

    const/4 v0, 0x0

    iput-boolean v0, p0, Libb;->f:Z

    iput-object v1, p0, Libb;->i:Lhgz;

    sget-object v0, Lidj;->f:[B

    iput-object v0, p0, Libb;->g:[B

    iput-object v1, p0, Libb;->j:Lhnh;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Libb;->a:Lhgz;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    iget-object v2, p0, Libb;->a:Lhgz;

    invoke-static {v0, v2}, Lidd;->b(ILidh;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    iget-object v2, p0, Libb;->h:Lhog;

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    iget-object v3, p0, Libb;->h:Lhog;

    invoke-static {v2, v3}, Lidd;->b(ILidh;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_0
    iget-object v2, p0, Libb;->b:[Lhgz;

    if-eqz v2, :cond_2

    iget-object v2, p0, Libb;->b:[Lhgz;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    if-eqz v4, :cond_1

    const/4 v5, 0x3

    invoke-static {v5, v4}, Lidd;->b(ILidh;)I

    move-result v4

    add-int/2addr v0, v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    iget-object v1, p0, Libb;->c:Lias;

    if-eqz v1, :cond_3

    const/4 v1, 0x4

    iget-object v2, p0, Libb;->c:Lias;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Libb;->d:Liaz;

    if-eqz v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Libb;->d:Liaz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget-object v1, p0, Libb;->e:Liaw;

    if-eqz v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Libb;->e:Liaw;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget-boolean v1, p0, Libb;->f:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x7

    iget-boolean v2, p0, Libb;->f:Z

    invoke-static {v1}, Lidd;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    :cond_6
    iget-object v1, p0, Libb;->i:Lhgz;

    if-eqz v1, :cond_7

    const/16 v1, 0x8

    iget-object v2, p0, Libb;->i:Lhgz;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget-object v1, p0, Libb;->g:[B

    sget-object v2, Lidj;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_8

    const/16 v1, 0xa

    iget-object v2, p0, Libb;->g:[B

    invoke-static {v1, v2}, Lidd;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget-object v1, p0, Libb;->j:Lhnh;

    if-eqz v1, :cond_9

    const/16 v1, 0xb

    iget-object v2, p0, Libb;->j:Lhnh;

    invoke-static {v1, v2}, Lidd;->b(ILidh;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget-object v1, p0, Libb;->I:Ljava/util/List;

    invoke-static {v1}, Lidj;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Libb;->J:I

    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public final synthetic a(Lidc;)Lidh;
    .locals 4

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lidc;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget-object v2, p0, Libb;->I:Ljava/util/List;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Libb;->I:Ljava/util/List;

    :cond_1
    iget-object v2, p0, Libb;->I:Ljava/util/List;

    invoke-static {v2, p1, v0}, Lidj;->a(Ljava/util/List;Lidc;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Libb;->a:Lhgz;

    if-nez v0, :cond_2

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Libb;->a:Lhgz;

    :cond_2
    iget-object v0, p0, Libb;->a:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Libb;->h:Lhog;

    if-nez v0, :cond_3

    new-instance v0, Lhog;

    invoke-direct {v0}, Lhog;-><init>()V

    iput-object v0, p0, Libb;->h:Lhog;

    :cond_3
    iget-object v0, p0, Libb;->h:Lhog;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lidj;->b(Lidc;I)I

    move-result v2

    iget-object v0, p0, Libb;->b:[Lhgz;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lhgz;

    iget-object v3, p0, Libb;->b:[Lhgz;

    if-eqz v3, :cond_4

    iget-object v3, p0, Libb;->b:[Lhgz;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    iput-object v2, p0, Libb;->b:[Lhgz;

    :goto_2
    iget-object v2, p0, Libb;->b:[Lhgz;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Libb;->b:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libb;->b:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Lidc;->a(Lidh;)V

    invoke-virtual {p1}, Lidc;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Libb;->b:[Lhgz;

    array-length v0, v0

    goto :goto_1

    :cond_6
    iget-object v2, p0, Libb;->b:[Lhgz;

    new-instance v3, Lhgz;

    invoke-direct {v3}, Lhgz;-><init>()V

    aput-object v3, v2, v0

    iget-object v2, p0, Libb;->b:[Lhgz;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Libb;->c:Lias;

    if-nez v0, :cond_7

    new-instance v0, Lias;

    invoke-direct {v0}, Lias;-><init>()V

    iput-object v0, p0, Libb;->c:Lias;

    :cond_7
    iget-object v0, p0, Libb;->c:Lias;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Libb;->d:Liaz;

    if-nez v0, :cond_8

    new-instance v0, Liaz;

    invoke-direct {v0}, Liaz;-><init>()V

    iput-object v0, p0, Libb;->d:Liaz;

    :cond_8
    iget-object v0, p0, Libb;->d:Liaz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Libb;->e:Liaw;

    if-nez v0, :cond_9

    new-instance v0, Liaw;

    invoke-direct {v0}, Liaw;-><init>()V

    iput-object v0, p0, Libb;->e:Liaw;

    :cond_9
    iget-object v0, p0, Libb;->e:Liaw;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lidc;->e()Z

    move-result v0

    iput-boolean v0, p0, Libb;->f:Z

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Libb;->i:Lhgz;

    if-nez v0, :cond_a

    new-instance v0, Lhgz;

    invoke-direct {v0}, Lhgz;-><init>()V

    iput-object v0, p0, Libb;->i:Lhgz;

    :cond_a
    iget-object v0, p0, Libb;->i:Lhgz;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lidc;->g()[B

    move-result-object v0

    iput-object v0, p0, Libb;->g:[B

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Libb;->j:Lhnh;

    if-nez v0, :cond_b

    new-instance v0, Lhnh;

    invoke-direct {v0}, Lhnh;-><init>()V

    iput-object v0, p0, Libb;->j:Lhnh;

    :cond_b
    iget-object v0, p0, Libb;->j:Lhnh;

    invoke-virtual {p1, v0}, Lidc;->a(Lidh;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lidd;)V
    .locals 5

    iget-object v0, p0, Libb;->a:Lhgz;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iget-object v1, p0, Libb;->a:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_0
    iget-object v0, p0, Libb;->h:Lhog;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iget-object v1, p0, Libb;->h:Lhog;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_1
    iget-object v0, p0, Libb;->b:[Lhgz;

    if-eqz v0, :cond_3

    iget-object v1, p0, Libb;->b:[Lhgz;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    if-eqz v3, :cond_2

    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Lidd;->a(ILidh;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Libb;->c:Lias;

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    iget-object v1, p0, Libb;->c:Lias;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_4
    iget-object v0, p0, Libb;->d:Liaz;

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    iget-object v1, p0, Libb;->d:Liaz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_5
    iget-object v0, p0, Libb;->e:Liaw;

    if-eqz v0, :cond_6

    const/4 v0, 0x6

    iget-object v1, p0, Libb;->e:Liaw;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_6
    iget-boolean v0, p0, Libb;->f:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x7

    iget-boolean v1, p0, Libb;->f:Z

    invoke-virtual {p1, v0, v1}, Lidd;->a(IZ)V

    :cond_7
    iget-object v0, p0, Libb;->i:Lhgz;

    if-eqz v0, :cond_8

    const/16 v0, 0x8

    iget-object v1, p0, Libb;->i:Lhgz;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_8
    iget-object v0, p0, Libb;->g:[B

    sget-object v1, Lidj;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Libb;->g:[B

    invoke-virtual {p1, v0, v1}, Lidd;->a(I[B)V

    :cond_9
    iget-object v0, p0, Libb;->j:Lhnh;

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Libb;->j:Lhnh;

    invoke-virtual {p1, v0, v1}, Lidd;->a(ILidh;)V

    :cond_a
    iget-object v0, p0, Libb;->I:Ljava/util/List;

    invoke-static {v0, p1}, Lidj;->a(Ljava/util/List;Lidd;)V

    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Libb;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Libb;

    iget-object v2, p0, Libb;->a:Lhgz;

    if-nez v2, :cond_4

    iget-object v2, p1, Libb;->a:Lhgz;

    if-nez v2, :cond_3

    :goto_1
    iget-object v2, p0, Libb;->h:Lhog;

    if-nez v2, :cond_5

    iget-object v2, p1, Libb;->h:Lhog;

    if-nez v2, :cond_3

    :goto_2
    iget-object v2, p0, Libb;->b:[Lhgz;

    iget-object v3, p1, Libb;->b:[Lhgz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libb;->c:Lias;

    if-nez v2, :cond_6

    iget-object v2, p1, Libb;->c:Lias;

    if-nez v2, :cond_3

    :goto_3
    iget-object v2, p0, Libb;->d:Liaz;

    if-nez v2, :cond_7

    iget-object v2, p1, Libb;->d:Liaz;

    if-nez v2, :cond_3

    :goto_4
    iget-object v2, p0, Libb;->e:Liaw;

    if-nez v2, :cond_8

    iget-object v2, p1, Libb;->e:Liaw;

    if-nez v2, :cond_3

    :goto_5
    iget-boolean v2, p0, Libb;->f:Z

    iget-boolean v3, p1, Libb;->f:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Libb;->i:Lhgz;

    if-nez v2, :cond_9

    iget-object v2, p1, Libb;->i:Lhgz;

    if-nez v2, :cond_3

    :goto_6
    iget-object v2, p0, Libb;->g:[B

    iget-object v3, p1, Libb;->g:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Libb;->j:Lhnh;

    if-nez v2, :cond_a

    iget-object v2, p1, Libb;->j:Lhnh;

    if-nez v2, :cond_3

    :goto_7
    iget-object v2, p0, Libb;->I:Ljava/util/List;

    if-nez v2, :cond_b

    iget-object v2, p1, Libb;->I:Ljava/util/List;

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Libb;->a:Lhgz;

    iget-object v3, p1, Libb;->a:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_5
    iget-object v2, p0, Libb;->h:Lhog;

    iget-object v3, p1, Libb;->h:Lhog;

    invoke-virtual {v2, v3}, Lhog;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_6
    iget-object v2, p0, Libb;->c:Lias;

    iget-object v3, p1, Libb;->c:Lias;

    invoke-virtual {v2, v3}, Lias;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_3

    :cond_7
    iget-object v2, p0, Libb;->d:Liaz;

    iget-object v3, p1, Libb;->d:Liaz;

    invoke-virtual {v2, v3}, Liaz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_4

    :cond_8
    iget-object v2, p0, Libb;->e:Liaw;

    iget-object v3, p1, Libb;->e:Liaw;

    invoke-virtual {v2, v3}, Liaw;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_5

    :cond_9
    iget-object v2, p0, Libb;->i:Lhgz;

    iget-object v3, p1, Libb;->i:Lhgz;

    invoke-virtual {v2, v3}, Lhgz;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_6

    :cond_a
    iget-object v2, p0, Libb;->j:Lhnh;

    iget-object v3, p1, Libb;->j:Lhnh;

    invoke-virtual {v2, v3}, Lhnh;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_7

    :cond_b
    iget-object v2, p0, Libb;->I:Ljava/util/List;

    iget-object v3, p1, Libb;->I:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libb;->a:Lhgz;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libb;->h:Lhog;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    iget-object v2, p0, Libb;->b:[Lhgz;

    if-nez v2, :cond_4

    mul-int/lit8 v2, v0, 0x1f

    :cond_0
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Libb;->c:Lias;

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libb;->d:Liaz;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libb;->e:Liaw;

    if-nez v0, :cond_8

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Libb;->f:Z

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Libb;->i:Lhgz;

    if-nez v0, :cond_a

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    iget-object v2, p0, Libb;->g:[B

    if-nez v2, :cond_b

    mul-int/lit8 v2, v0, 0x1f

    :cond_1
    mul-int/lit8 v2, v2, 0x1f

    iget-object v0, p0, Libb;->j:Lhnh;

    if-nez v0, :cond_c

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Libb;->I:Ljava/util/List;

    if-nez v2, :cond_d

    :goto_8
    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v0, p0, Libb;->a:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Libb;->h:Lhog;

    invoke-virtual {v0}, Lhog;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_4
    move v2, v0

    move v0, v1

    :goto_9
    iget-object v3, p0, Libb;->b:[Lhgz;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    mul-int/lit8 v3, v2, 0x1f

    iget-object v2, p0, Libb;->b:[Lhgz;

    aget-object v2, v2, v0

    if-nez v2, :cond_5

    move v2, v1

    :goto_a
    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_5
    iget-object v2, p0, Libb;->b:[Lhgz;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lhgz;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_6
    iget-object v0, p0, Libb;->c:Lias;

    invoke-virtual {v0}, Lias;->hashCode()I

    move-result v0

    goto :goto_2

    :cond_7
    iget-object v0, p0, Libb;->d:Liaz;

    invoke-virtual {v0}, Liaz;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_8
    iget-object v0, p0, Libb;->e:Liaw;

    invoke-virtual {v0}, Liaw;->hashCode()I

    move-result v0

    goto :goto_4

    :cond_9
    const/4 v0, 0x2

    goto :goto_5

    :cond_a
    iget-object v0, p0, Libb;->i:Lhgz;

    invoke-virtual {v0}, Lhgz;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_b
    move v2, v0

    move v0, v1

    :goto_b
    iget-object v3, p0, Libb;->g:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    mul-int/lit8 v2, v2, 0x1f

    iget-object v3, p0, Libb;->g:[B

    aget-byte v3, v3, v0

    add-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    :cond_c
    iget-object v0, p0, Libb;->j:Lhnh;

    invoke-virtual {v0}, Lhnh;->hashCode()I

    move-result v0

    goto :goto_7

    :cond_d
    iget-object v1, p0, Libb;->I:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    goto :goto_8
.end method
