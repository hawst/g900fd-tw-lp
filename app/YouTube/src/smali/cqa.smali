.class public final Lcqa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldlr;


# instance fields
.field private final A:Ljava/util/LinkedList;

.field private final B:Ljava/util/List;

.field private final C:Lggr;

.field private final D:Lezf;

.field private final E:Lexd;

.field private final F:Levn;

.field private final G:Lgjp;

.field private final H:Lcqf;

.field private final I:Lgoc;

.field private J:Lcwx;

.field private K:Z

.field public final a:Landroid/net/Uri;

.field public final b:Landroid/net/Uri;

.field public final c:Landroid/net/Uri;

.field public final d:J

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I

.field public final k:Lgog;

.field public final l:Z

.field public final m:Z

.field public n:J

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Z

.field public volatile s:Z

.field public t:I

.field public u:Ljava/lang/String;

.field private final v:Lezj;

.field private final w:Ljava/util/LinkedList;

.field private final x:Ljava/util/LinkedList;

.field private final y:Ljava/util/LinkedList;

.field private final z:Ljava/util/LinkedList;


# direct methods
.method constructor <init>(Lgjp;Lezj;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLgog;Ljava/lang/String;Lcwx;Lexd;Levn;Lggr;Ljava/util/List;Lezf;Lgoc;)V
    .locals 10

    .prologue
    .line 523
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 524
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgjp;

    iput-object v2, p0, Lcqa;->G:Lgjp;

    .line 525
    iput-object p2, p0, Lcqa;->v:Lezj;

    .line 526
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcqa;->a:Landroid/net/Uri;

    .line 527
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcqa;->b:Landroid/net/Uri;

    .line 528
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    iput-object v2, p0, Lcqa;->c:Landroid/net/Uri;

    .line 529
    move-object/from16 v0, p6

    iput-object v0, p0, Lcqa;->g:Ljava/lang/String;

    .line 530
    move-object/from16 v0, p7

    iput-object v0, p0, Lcqa;->f:Ljava/lang/String;

    .line 531
    move/from16 v0, p8

    iput v0, p0, Lcqa;->j:I

    .line 532
    move/from16 v0, p9

    iput-boolean v0, p0, Lcqa;->l:Z

    .line 533
    move/from16 v0, p10

    iput-boolean v0, p0, Lcqa;->m:Z

    .line 534
    move-object/from16 v0, p11

    iput-object v0, p0, Lcqa;->e:Ljava/lang/String;

    .line 535
    move-object/from16 v0, p12

    iput-object v0, p0, Lcqa;->h:Ljava/lang/String;

    .line 536
    move-wide/from16 v0, p14

    iput-wide v0, p0, Lcqa;->d:J

    .line 537
    move-object/from16 v0, p16

    iput-object v0, p0, Lcqa;->k:Lgog;

    .line 538
    move-object/from16 v0, p17

    iput-object v0, p0, Lcqa;->u:Ljava/lang/String;

    .line 539
    move-object/from16 v0, p18

    iput-object v0, p0, Lcqa;->J:Lcwx;

    .line 540
    move-object/from16 v0, p19

    iput-object v0, p0, Lcqa;->E:Lexd;

    .line 541
    move-object/from16 v0, p20

    iput-object v0, p0, Lcqa;->F:Levn;

    .line 542
    move-object/from16 v0, p21

    iput-object v0, p0, Lcqa;->C:Lggr;

    .line 543
    move-object/from16 v0, p22

    iput-object v0, p0, Lcqa;->B:Ljava/util/List;

    .line 544
    move-object/from16 v0, p13

    iput-object v0, p0, Lcqa;->i:Ljava/lang/String;

    .line 545
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcqa;->w:Ljava/util/LinkedList;

    .line 546
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcqa;->x:Ljava/util/LinkedList;

    .line 547
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcqa;->y:Ljava/util/LinkedList;

    .line 548
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcqa;->z:Ljava/util/LinkedList;

    .line 549
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcqa;->A:Ljava/util/LinkedList;

    .line 550
    move-object/from16 v0, p23

    iput-object v0, p0, Lcqa;->D:Lezf;

    .line 551
    invoke-static/range {p24 .. p24}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lgoc;

    iput-object v2, p0, Lcqa;->I:Lgoc;

    .line 552
    new-instance v2, Lcqf;

    iget-wide v6, p0, Lcqa;->d:J

    move-object/from16 v3, p19

    move-object/from16 v4, p18

    move-object v5, p2

    move-object/from16 v8, p12

    invoke-direct/range {v2 .. v8}, Lcqf;-><init>(Lexd;Lcwx;Lezj;JLjava/lang/String;)V

    iput-object v2, p0, Lcqa;->H:Lcqf;

    .line 554
    return-void
.end method

.method constructor <init>(Lgjp;Lezj;Lcqd;Lcwx;Lexd;Levn;Lggr;Ljava/util/List;Lezf;Lgoc;)V
    .locals 27

    .prologue
    .line 467
    move-object/from16 v0, p3

    iget-object v5, v0, Lcqd;->a:Landroid/net/Uri;

    move-object/from16 v0, p3

    iget-object v6, v0, Lcqd;->b:Landroid/net/Uri;

    move-object/from16 v0, p3

    iget-object v7, v0, Lcqd;->c:Landroid/net/Uri;

    move-object/from16 v0, p3

    iget-object v8, v0, Lcqd;->h:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v9, v0, Lcqd;->g:Ljava/lang/String;

    move-object/from16 v0, p3

    iget v10, v0, Lcqd;->k:I

    move-object/from16 v0, p3

    iget-boolean v11, v0, Lcqd;->l:Z

    move-object/from16 v0, p3

    iget-boolean v12, v0, Lcqd;->m:Z

    move-object/from16 v0, p3

    iget-object v13, v0, Lcqd;->f:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v14, v0, Lcqd;->i:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v15, v0, Lcqd;->j:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-wide v0, v0, Lcqd;->d:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcqd;->s:Lgog;

    move-object/from16 v18, v0

    move-object/from16 v0, p3

    iget-object v0, v0, Lcqd;->t:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v20, p4

    move-object/from16 v21, p5

    move-object/from16 v22, p6

    move-object/from16 v23, p7

    move-object/from16 v24, p8

    move-object/from16 v25, p9

    move-object/from16 v26, p10

    invoke-direct/range {v2 .. v26}, Lcqa;-><init>(Lgjp;Lezj;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLgog;Ljava/lang/String;Lcwx;Lexd;Levn;Lggr;Ljava/util/List;Lezf;Lgoc;)V

    .line 492
    move-object/from16 v0, p3

    iget-wide v2, v0, Lcqd;->e:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcqa;->n:J

    .line 493
    move-object/from16 v0, p3

    iget-boolean v2, v0, Lcqd;->n:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcqa;->o:Z

    .line 494
    move-object/from16 v0, p3

    iget-boolean v2, v0, Lcqd;->p:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcqa;->p:Z

    .line 495
    move-object/from16 v0, p3

    iget-boolean v2, v0, Lcqd;->o:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcqa;->q:Z

    .line 496
    move-object/from16 v0, p3

    iget-boolean v2, v0, Lcqd;->q:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcqa;->s:Z

    .line 497
    move-object/from16 v0, p3

    iget v2, v0, Lcqd;->r:I

    move-object/from16 v0, p0

    iput v2, v0, Lcqa;->t:I

    .line 498
    return-void
.end method

.method private static a(Ljava/util/LinkedList;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 841
    const-string v0, ","

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 842
    invoke-virtual {p0}, Ljava/util/LinkedList;->clear()V

    .line 843
    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 856
    iget-boolean v0, p0, Lcqa;->p:Z

    if-eqz v0, :cond_0

    .line 857
    iget-object v0, p0, Lcqa;->h:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x41

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Final ping for playback "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has already been sent - Ignoring request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    .line 880
    :goto_0
    return-void

    .line 858
    :cond_0
    iget-boolean v0, p0, Lcqa;->s:Z

    if-nez v0, :cond_1

    .line 859
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Pinging "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    .line 860
    iget-object v0, p0, Lcqa;->G:Lgjp;

    const-string v0, "vss"

    const v1, 0x323467f

    .line 861
    invoke-static {v0, v1}, Lgjp;->a(Ljava/lang/String;I)Lgjt;

    move-result-object v0

    .line 863
    invoke-virtual {v0, p1}, Lgjt;->a(Landroid/net/Uri;)Lgjt;

    .line 864
    const/4 v1, 0x1

    iput-boolean v1, v0, Lgjt;->d:Z

    .line 865
    iget-object v1, p0, Lcqa;->G:Lgjp;

    new-instance v2, Lcqb;

    invoke-direct {v2, p0}, Lcqb;-><init>(Lcqa;)V

    invoke-virtual {v1, v0, v2}, Lgjp;->a(Lgjt;Lwu;)V

    goto :goto_0

    .line 878
    :cond_1
    iget-object v0, p0, Lcqa;->h:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Playback "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " is throttled - Ignoring request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lezp;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lfao;)V
    .locals 3

    .prologue
    .line 729
    invoke-direct {p0, p1}, Lcqa;->b(Lfao;)V

    .line 730
    iget-wide v0, p0, Lcqa;->n:J

    invoke-static {v0, v1}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cmt"

    invoke-virtual {p1, v1, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "conn"

    iget-object v2, p0, Lcqa;->E:Lexd;

    invoke-interface {v2}, Lexd;->i()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;I)Lfao;

    move-result-object v0

    const-string v1, "vis"

    iget-object v2, p0, Lcqa;->J:Lcwx;

    iget v2, v2, Lcwx;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget v0, p0, Lcqa;->j:I

    if-lez v0, :cond_0

    const-string v0, "delay"

    iget v1, p0, Lcqa;->j:I

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;I)Lfao;

    :cond_0
    iget-object v0, p0, Lcqa;->k:Lgog;

    sget-object v1, Lgog;->a:Lgog;

    if-eq v0, v1, :cond_1

    const-string v0, "feature"

    iget-object v1, p0, Lcqa;->k:Lgog;

    iget-object v1, v1, Lgog;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_1
    iget-object v0, p0, Lcqa;->u:Ljava/lang/String;

    const-string v1, "-"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "cc"

    iget-object v1, p0, Lcqa;->u:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    :cond_2
    iget-object v0, p0, Lcqa;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_0

    .line 731
    :cond_3
    iget-object v0, p1, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcqa;->a(Landroid/net/Uri;)V

    .line 732
    return-void
.end method

.method static b(J)Ljava/lang/String;
    .locals 8

    .prologue
    .line 883
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    long-to-double v4, p0

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lfao;)V
    .locals 4

    .prologue
    .line 746
    iget-object v0, p0, Lcqa;->v:Lezj;

    invoke-virtual {v0}, Lezj;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcqa;->d:J

    sub-long/2addr v0, v2

    .line 747
    invoke-static {v0, v1}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v0

    .line 748
    const-string v1, "cpn"

    iget-object v2, p0, Lcqa;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v1

    const-string v2, "rt"

    .line 749
    invoke-virtual {v1, v2, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "ns"

    const-string v2, "yt"

    .line 750
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "docid"

    iget-object v2, p0, Lcqa;->g:Ljava/lang/String;

    .line 751
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "ver"

    const-string v2, "2"

    .line 752
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v1, "len"

    iget-object v2, p0, Lcqa;->f:Ljava/lang/String;

    .line 753
    invoke-virtual {v0, v1, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 754
    iget-object v0, p0, Lcqa;->C:Lggr;

    invoke-virtual {v0, p1}, Lggr;->a(Lfao;)Lfao;

    .line 755
    iget-object v0, p0, Lcqa;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 756
    const-string v0, "el"

    const-string v1, "adunit"

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 757
    const-string v0, "adformat"

    iget-object v1, p0, Lcqa;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 761
    :goto_0
    iget-object v0, p0, Lcqa;->D:Lezf;

    if-eqz v0, :cond_0

    .line 762
    const-string v0, "lact"

    iget-object v1, p0, Lcqa;->D:Lezf;

    .line 763
    invoke-virtual {v1}, Lezf;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 762
    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 765
    :cond_0
    iget v0, p0, Lcqa;->t:I

    if-lez v0, :cond_6

    .line 766
    const-string v0, "fmt"

    iget v1, p0, Lcqa;->t:I

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;I)Lfao;

    .line 770
    :goto_1
    iget-object v0, p0, Lcqa;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 771
    const-string v0, "list"

    iget-object v1, p0, Lcqa;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 773
    :cond_1
    iget-boolean v0, p0, Lcqa;->l:Z

    if-eqz v0, :cond_2

    .line 774
    const-string v0, "autoplay"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 776
    :cond_2
    iget-boolean v0, p0, Lcqa;->m:Z

    if-eqz v0, :cond_3

    .line 777
    const-string v0, "splay"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 779
    :cond_3
    iget-boolean v0, p0, Lcqa;->l:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcqa;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcqa;->e:Ljava/lang/String;

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    .line 780
    const-string v0, "autonav"

    const-string v1, "1"

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 782
    :cond_4
    return-void

    .line 759
    :cond_5
    const-string v0, "el"

    const-string v1, "detailpage"

    invoke-virtual {p1, v0, v1}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_0

    .line 768
    :cond_6
    const-string v0, "Warning: Sending VSS ping without a video format parameter."

    invoke-static {v0}, Lezp;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 779
    :cond_7
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 638
    invoke-virtual {p0}, Lcqa;->c()V

    .line 639
    iget-boolean v0, p0, Lcqa;->q:Z

    if-eqz v0, :cond_0

    .line 640
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcqa;->b(Z)V

    .line 642
    :cond_0
    return-void
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 632
    invoke-virtual {p0}, Lcqa;->c()V

    .line 633
    iput-wide p1, p0, Lcqa;->n:J

    .line 634
    invoke-virtual {p0}, Lcqa;->b()V

    .line 635
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 627
    iput-boolean p1, p0, Lcqa;->r:Z

    .line 628
    iget-object v0, p0, Lcqa;->H:Lcqf;

    iput-boolean p1, v0, Lcqf;->c:Z

    .line 629
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 700
    iget-boolean v0, p0, Lcqa;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcqa;->K:Z

    if-nez v0, :cond_0

    .line 701
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcqa;->K:Z

    .line 702
    iget-object v0, p0, Lcqa;->w:Ljava/util/LinkedList;

    iget-wide v2, p0, Lcqa;->n:J

    invoke-static {v2, v3}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 703
    iget-object v0, p0, Lcqa;->y:Ljava/util/LinkedList;

    iget-object v1, p0, Lcqa;->E:Lexd;

    invoke-interface {v1}, Lexd;->i()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 704
    iget-object v0, p0, Lcqa;->z:Ljava/util/LinkedList;

    iget-object v1, p0, Lcqa;->J:Lcwx;

    iget v1, v1, Lcwx;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 705
    iget-object v0, p0, Lcqa;->A:Ljava/util/LinkedList;

    iget-object v1, p0, Lcqa;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 707
    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 6

    .prologue
    .line 735
    iget-object v0, p0, Lcqa;->c:Landroid/net/Uri;

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v1

    .line 736
    invoke-direct {p0, v1}, Lcqa;->b(Lfao;)V

    .line 737
    const-string v2, "state"

    iget-boolean v0, p0, Lcqa;->r:Z

    if-eqz v0, :cond_2

    const-string v0, "playing"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget-object v0, p0, Lcqa;->x:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "st"

    iget-wide v2, p0, Lcqa;->n:J

    invoke-static {v2, v3}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v2, "et"

    iget-wide v4, p0, Lcqa;->n:J

    invoke-static {v4, v5}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v2, "conn"

    iget-object v3, p0, Lcqa;->E:Lexd;

    invoke-interface {v3}, Lexd;->i()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lfao;->a(Ljava/lang/String;I)Lfao;

    move-result-object v0

    const-string v2, "vis"

    iget-object v3, p0, Lcqa;->J:Lcwx;

    iget v3, v3, Lcwx;->a:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    const-string v0, "-"

    iget-object v2, p0, Lcqa;->u:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "cc"

    iget-object v2, p0, Lcqa;->u:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 738
    :cond_0
    :goto_1
    if-eqz p1, :cond_1

    .line 739
    const-string v0, "final"

    const-string v2, "1"

    invoke-virtual {v1, v0, v2}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;)Lfao;

    .line 741
    :cond_1
    iget-object v0, v1, Lfao;->a:Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcqa;->a(Landroid/net/Uri;)V

    .line 742
    iget-boolean v0, p0, Lcqa;->p:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcqa;->p:Z

    .line 743
    return-void

    .line 737
    :cond_2
    const-string v0, "paused"

    goto :goto_0

    :cond_3
    const-string v0, "st"

    iget-object v2, p0, Lcqa;->w:Ljava/util/LinkedList;

    invoke-static {v2}, Lcqa;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ",:"

    invoke-virtual {v1, v0, v2, v3}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v2, "et"

    iget-object v3, p0, Lcqa;->x:Ljava/util/LinkedList;

    invoke-static {v3}, Lcqa;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ",:"

    invoke-virtual {v0, v2, v3, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v2, "conn"

    iget-object v3, p0, Lcqa;->y:Ljava/util/LinkedList;

    invoke-static {v3}, Lcqa;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ",:"

    invoke-virtual {v0, v2, v3, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    move-result-object v0

    const-string v2, "vis"

    iget-object v3, p0, Lcqa;->z:Ljava/util/LinkedList;

    invoke-static {v3}, Lcqa;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ",:"

    invoke-virtual {v0, v2, v3, v4}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    iget-object v0, p0, Lcqa;->A:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "-"

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_6

    const-string v0, "cc"

    iget-object v2, p0, Lcqa;->A:Ljava/util/LinkedList;

    invoke-static {v2}, Lcqa;->a(Ljava/util/LinkedList;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ",:"

    invoke-virtual {v1, v0, v2, v3}, Lfao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lfao;

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcqa;->A:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    goto/16 :goto_1
.end method

.method public c()V
    .locals 4

    .prologue
    .line 710
    iget-boolean v0, p0, Lcqa;->K:Z

    if-eqz v0, :cond_0

    .line 711
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcqa;->K:Z

    .line 712
    iget-object v0, p0, Lcqa;->x:Ljava/util/LinkedList;

    iget-wide v2, p0, Lcqa;->n:J

    invoke-static {v2, v3}, Lcqa;->b(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 714
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 723
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcqa;->o:Z

    .line 724
    iget-object v0, p0, Lcqa;->b:Landroid/net/Uri;

    invoke-static {v0}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v0

    .line 725
    invoke-direct {p0, v0}, Lcqa;->a(Lfao;)V

    .line 726
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 888
    iget-object v0, p0, Lcqa;->F:Levn;

    invoke-virtual {v0, p0}, Levn;->a(Ljava/lang/Object;)V

    .line 889
    iget-object v0, p0, Lcqa;->I:Lgoc;

    iget-object v1, p0, Lcqa;->H:Lcqf;

    invoke-virtual {v0, v1}, Lgoc;->a(Lgod;)V

    .line 890
    return-void
.end method

.method public final handleConnectivityChangedEvent(Lewk;)V
    .locals 0
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 655
    invoke-virtual {p0}, Lcqa;->c()V

    .line 656
    invoke-virtual {p0}, Lcqa;->b()V

    .line 657
    return-void
.end method

.method public final handlePlayerGeometry(Lcwx;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 558
    iget-object v0, p0, Lcqa;->H:Lcqf;

    iget-object v1, p0, Lcqa;->J:Lcwx;

    iput-object v1, v0, Lcqf;->a:Lcwx;

    .line 559
    iget-object v0, p0, Lcqa;->J:Lcwx;

    if-eq v0, p1, :cond_0

    .line 560
    invoke-virtual {p0}, Lcqa;->c()V

    .line 561
    iput-object p1, p0, Lcqa;->J:Lcwx;

    .line 562
    invoke-virtual {p0}, Lcqa;->b()V

    .line 564
    :cond_0
    return-void
.end method

.method public final handleSubtitleTrackChangedEvent(Lczw;)V
    .locals 2
    .annotation runtime Levv;
    .end annotation

    .prologue
    .line 666
    iget-object v0, p0, Lcqa;->u:Ljava/lang/String;

    .line 668
    iget-object v1, p1, Lczw;->a:Ljava/lang/String;

    .line 666
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 669
    iget-object v0, p1, Lczw;->a:Ljava/lang/String;

    iput-object v0, p0, Lcqa;->u:Ljava/lang/String;

    .line 670
    invoke-virtual {p0}, Lcqa;->c()V

    .line 671
    invoke-virtual {p0}, Lcqa;->b()V

    .line 673
    :cond_0
    return-void
.end method

.method public final handleVideoTimeEvent(Ldad;)V
    .locals 6
    .annotation runtime Levv;
    .end annotation

    .prologue
    const-wide/16 v4, 0x1388

    .line 568
    iget-boolean v0, p1, Ldad;->d:Z

    if-eqz v0, :cond_1

    .line 569
    iget-wide v0, p1, Ldad;->a:J

    .line 573
    iget-wide v2, p0, Lcqa;->n:J

    sub-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    iget-wide v2, p0, Lcqa;->n:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    .line 575
    :cond_0
    iget-wide v2, p0, Lcqa;->n:J

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x6d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Warning: unexpected playback progress "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for current playback position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lezp;->c(Ljava/lang/String;)V

    .line 577
    invoke-virtual {p0, v0, v1}, Lcqa;->a(J)V

    .line 595
    :cond_1
    :goto_0
    return-void

    .line 583
    :cond_2
    iget-wide v2, p0, Lcqa;->n:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    .line 586
    iput-wide v0, p0, Lcqa;->n:J

    .line 587
    iget-object v2, p0, Lcqa;->H:Lcqf;

    iget-wide v4, p0, Lcqa;->n:J

    iput-wide v4, v2, Lcqf;->b:J

    .line 588
    iget-boolean v2, p0, Lcqa;->q:Z

    if-nez v2, :cond_3

    .line 589
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcqa;->q:Z

    iget-object v2, p0, Lcqa;->a:Landroid/net/Uri;

    invoke-static {v2}, Lfao;->a(Landroid/net/Uri;)Lfao;

    move-result-object v2

    invoke-direct {p0, v2}, Lcqa;->a(Lfao;)V

    .line 591
    :cond_3
    iget-boolean v2, p0, Lcqa;->o:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcqa;->j:I

    if-lez v2, :cond_1

    iget v2, p0, Lcqa;->j:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 592
    invoke-virtual {p0}, Lcqa;->d()V

    goto :goto_0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lcqa;->F:Levn;

    invoke-virtual {v0, p0}, Levn;->b(Ljava/lang/Object;)V

    .line 895
    iget-boolean v0, p0, Lcqa;->K:Z

    if-eqz v0, :cond_0

    .line 896
    const-string v0, "VSS2 client released unexpectedly"

    new-instance v1, Ljava/lang/Exception;

    invoke-direct {v1}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1}, Lezp;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 897
    invoke-virtual {p0}, Lcqa;->a()V

    .line 899
    :cond_0
    iget-object v0, p0, Lcqa;->I:Lgoc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lgoc;->a(Lgod;)V

    .line 900
    return-void
.end method
