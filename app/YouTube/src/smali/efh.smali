.class public final Lefh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Leey;
.implements Lega;


# instance fields
.field final a:Lefj;

.field private final b:Landroid/os/Handler;

.field private final c:Legq;

.field private final d:Legv;

.field private e:J

.field private f:J

.field private g:J

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, v0, v0}, Lefh;-><init>(Landroid/os/Handler;Lefj;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;Lefj;)V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Legq;

    invoke-direct {v0}, Legq;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lefh;-><init>(Landroid/os/Handler;Lefj;Legq;)V

    .line 54
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Lefj;Legq;)V
    .locals 1

    .prologue
    .line 57
    const/16 v0, 0x7d0

    invoke-direct {p0, p1, p2, p3, v0}, Lefh;-><init>(Landroid/os/Handler;Lefj;Legq;I)V

    .line 58
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;Lefj;Legq;I)V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lefh;->b:Landroid/os/Handler;

    .line 67
    iput-object p2, p0, Lefh;->a:Lefj;

    .line 68
    iput-object p3, p0, Lefh;->c:Legq;

    .line 69
    new-instance v0, Legv;

    const/16 v1, 0x7d0

    invoke-direct {v0, v1}, Legv;-><init>(I)V

    iput-object v0, p0, Lefh;->d:Legv;

    .line 70
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lefh;->g:J

    .line 71
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()J
    .locals 2

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lefh;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lefh;->e:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lefh;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lefh;->h:I

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lefh;->c:Legq;

    invoke-virtual {v0}, Legq;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lefh;->f:J

    .line 83
    :cond_0
    iget v0, p0, Lefh;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lefh;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit p0

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 10

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lefh;->h:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, La;->c(Z)V

    .line 94
    iget-object v0, p0, Lefh;->c:Legq;

    invoke-virtual {v0}, Legq;->a()J

    move-result-wide v8

    .line 95
    iget-wide v0, p0, Lefh;->f:J

    sub-long v0, v8, v0

    long-to-int v3, v0

    .line 96
    if-lez v3, :cond_0

    .line 97
    iget-wide v0, p0, Lefh;->e:J

    const-wide/16 v4, 0x1f40

    mul-long/2addr v0, v4

    int-to-long v4, v3

    div-long/2addr v0, v4

    long-to-float v0, v0

    .line 98
    iget-object v1, p0, Lefh;->d:Legv;

    iget-wide v4, p0, Lefh;->e:J

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v2, v4

    invoke-virtual {v1, v2, v0}, Legv;->a(IF)V

    .line 99
    iget-object v0, p0, Lefh;->d:Legv;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Legv;->a(F)F

    move-result v0

    .line 100
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_3

    const-wide/16 v0, -0x1

    :goto_1
    iput-wide v0, p0, Lefh;->g:J

    .line 102
    iget-wide v4, p0, Lefh;->e:J

    iget-wide v6, p0, Lefh;->g:J

    iget-object v0, p0, Lefh;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefh;->a:Lefj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefh;->b:Landroid/os/Handler;

    new-instance v1, Lefi;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lefi;-><init>(Lefh;IJJ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 104
    :cond_0
    iget v0, p0, Lefh;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lefh;->h:I

    .line 105
    iget v0, p0, Lefh;->h:I

    if-lez v0, :cond_1

    .line 106
    iput-wide v8, p0, Lefh;->f:J

    .line 108
    :cond_1
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lefh;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    monitor-exit p0

    return-void

    .line 93
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 100
    :cond_3
    float-to-long v0, v0

    goto :goto_1

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
