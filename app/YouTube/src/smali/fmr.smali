.class public abstract Lfmr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfat;
.implements Lflb;
.implements Lfqh;


# instance fields
.field public final a:Lhvp;

.field public b:Ljava/lang/CharSequence;

.field public c:Lfnc;

.field public d:Lfkp;

.field private final e:Lfqh;

.field private f:Ljava/lang/CharSequence;

.field private g:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lhvp;Lfqh;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhvp;

    iput-object v0, p0, Lfmr;->a:Lhvp;

    .line 33
    iput-object p2, p0, Lfmr;->e:Lfqh;

    .line 34
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/util/List;
.end method

.method public final a(Lfau;)V
    .locals 3

    .prologue
    .line 94
    invoke-interface {p1, p0}, Lfau;->a(Lfat;)V

    .line 95
    invoke-virtual {p0}, Lfmr;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 96
    instance-of v2, v0, Lfat;

    if-eqz v2, :cond_0

    .line 97
    check-cast v0, Lfat;

    invoke-interface {v0, p1}, Lfat;->a(Lfau;)V

    goto :goto_0

    .line 100
    :cond_1
    return-void
.end method

.method public final b()Lhog;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lfmr;->a:Lhvp;

    iget-object v0, v0, Lhvp;->c:Lhog;

    return-object v0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lfmr;->a:Lhvp;

    iget-object v0, v0, Lhvp;->g:[B

    return-object v0
.end method

.method public final e()Lfqh;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lfmr;->e:Lfqh;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lfmr;->f:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 45
    iget-object v0, p0, Lfmr;->a:Lhvp;

    iget-object v0, v0, Lhvp;->f:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmr;->f:Ljava/lang/CharSequence;

    .line 47
    :cond_0
    iget-object v0, p0, Lfmr;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lfmr;->g:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lfmr;->a:Lhvp;

    iget-object v0, v0, Lhvp;->e:Lhgz;

    invoke-static {v0}, Lfvo;->a(Lhgz;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lfmr;->g:Ljava/lang/CharSequence;

    .line 54
    :cond_0
    iget-object v0, p0, Lfmr;->g:Ljava/lang/CharSequence;

    return-object v0
.end method
