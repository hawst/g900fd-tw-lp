.class public abstract Lbgp;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(La;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lbgp;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 116
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lbgp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 117
    invoke-static {p0}, La;->g(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lbgp;->b()Ljava/lang/Class;

    move-result-object v0

    .line 118
    :goto_0
    new-instance v2, Landroid/content/ComponentName;

    invoke-direct {v2, p0, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 119
    const-string v0, "alias"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    invoke-virtual {p0}, Lbgp;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 122
    return-object v1

    .line 117
    :cond_0
    invoke-virtual {p0}, Lbgp;->b()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract b()Ljava/lang/Class;
.end method

.method public c()I
    .locals 1

    .prologue
    .line 132
    const/high16 v0, 0x20000000

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lbgp;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/app/YouTubeApplication;

    .line 85
    iget-object v1, v0, Lckz;->a:Letc;

    invoke-virtual {v1}, Letc;->m()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 86
    invoke-virtual {v0}, Lcom/google/android/apps/youtube/app/YouTubeApplication;->c()Lari;

    move-result-object v2

    invoke-virtual {v2}, Lari;->f()Larh;

    move-result-object v2

    .line 88
    invoke-static {v0, v2, v1}, La;->a(Lcom/google/android/apps/youtube/app/YouTubeApplication;Larh;Landroid/content/SharedPreferences;)V

    .line 89
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, La;->h()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 90
    invoke-static {}, La;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    invoke-static {}, La;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    invoke-static {}, La;->k()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_2

    .line 93
    invoke-virtual {p0}, Lbgp;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 94
    :goto_0
    if-eqz v0, :cond_3

    .line 95
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lbgp;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 96
    new-instance v2, Landroid/content/ComponentName;

    const-class v3, Lcom/google/android/apps/youtube/app/honeycomb/phone/NewVersionAvailableActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 98
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 99
    invoke-static {}, La;->i()Z

    move-result v2

    if-nez v2, :cond_1

    .line 100
    const-string v2, "forward_intent"

    invoke-virtual {p0}, Lbgp;->a()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 103
    :cond_1
    const-wide v2, 0x7fffffffffffffffL

    invoke-static {v2, v3}, La;->a(J)J

    .line 105
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "upgrade_prompt_shown_millis"

    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 107
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 108
    invoke-virtual {p0, v0}, Lbgp;->startActivity(Landroid/content/Intent;)V

    .line 112
    :goto_1
    invoke-virtual {p0}, Lbgp;->finish()V

    .line 113
    return-void

    .line 93
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :cond_3
    invoke-virtual {p0}, Lbgp;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbgp;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method
