.class public final Lfvt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public final a:Lgiq;

.field public final b:Lgix;

.field public final c:Ljava/util/List;

.field public final d:Lws;

.field public final e:Lghm;

.field public final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    :try_start_0
    const-string v0, "http://www.youtube.com/watch?v="

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfvt;->g:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 42
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Device doesn\'t support UTF-8."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(Lgiq;Lgix;Ljava/util/List;Lws;Lghm;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgiq;

    iput-object v0, p0, Lfvt;->a:Lgiq;

    .line 61
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgix;

    iput-object v0, p0, Lfvt;->b:Lgix;

    .line 62
    invoke-static {p3}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lfvt;->c:Ljava/util/List;

    .line 63
    invoke-static {p4}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lws;

    iput-object v0, p0, Lfvt;->d:Lws;

    .line 64
    invoke-static {p5}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghm;

    iput-object v0, p0, Lfvt;->e:Lghm;

    .line 65
    invoke-static {p6}, Lb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfvt;->f:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public static a()Lfvv;
    .locals 1

    .prologue
    .line 107
    new-instance v0, Lfvv;

    invoke-direct {v0}, Lfvv;-><init>()V

    return-object v0
.end method

.method public static b()Lfvw;
    .locals 1

    .prologue
    .line 194
    new-instance v0, Lfvw;

    invoke-direct {v0}, Lfvw;-><init>()V

    return-object v0
.end method

.method public static c()Lfvy;
    .locals 1

    .prologue
    .line 286
    new-instance v0, Lfvy;

    invoke-direct {v0}, Lfvy;-><init>()V

    return-object v0
.end method

.method public static d()Lfvx;
    .locals 1

    .prologue
    .line 325
    new-instance v0, Lfvx;

    invoke-direct {v0}, Lfvx;-><init>()V

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lfvt;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Lfvv;Lwv;)V
    .locals 9

    .prologue
    .line 79
    new-instance v3, Lfvu;

    invoke-direct {v3, p0, p2, p1}, Lfvu;-><init>(Lfvt;Lwv;Lfvv;)V

    .line 94
    new-instance v0, Lfwa;

    const/4 v1, 0x0

    iget-object v4, p0, Lfvt;->a:Lgiq;

    iget-object v5, p0, Lfvt;->b:Lgix;

    iget-object v6, p0, Lfvt;->c:Ljava/util/List;

    iget-object v7, p0, Lfvt;->e:Lghm;

    iget-object v8, p0, Lfvt;->f:Ljava/lang/String;

    move-object v2, p1

    invoke-direct/range {v0 .. v8}, Lfwa;-><init>(ILfwb;Lwv;Lgiq;Lgix;Ljava/util/List;Lghm;Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lfvt;->d:Lws;

    invoke-virtual {v1, v0}, Lws;->a(Lwp;)Lwp;

    .line 104
    return-void
.end method
