.class public final Lewl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lxi;


# instance fields
.field private final a:Lorg/chromium/net/HttpUrlRequestFactory;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/chromium/net/HttpUrlRequestFactory;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/chromium/net/HttpUrlRequestFactory;

    iput-object v0, p0, Lewl;->a:Lorg/chromium/net/HttpUrlRequestFactory;

    .line 46
    invoke-static {p2}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " gzip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lewl;->b:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Lwp;Ljava/util/Map;)Lorg/apache/http/HttpResponse;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 53
    const-string v0, "Cronet creating request"

    invoke-static {v0}, Lfaj;->a(Ljava/lang/String;)Lfal;

    move-result-object v2

    .line 54
    invoke-static {p1}, Lb;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-virtual {p1}, Lwp;->i()Lwr;

    move-result-object v0

    sget-object v3, Lewm;->a:[I

    invoke-virtual {v0}, Lwr;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    move v0, v1

    .line 56
    :goto_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 57
    invoke-virtual {p1}, Lwp;->c()Ljava/util/Map;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 58
    invoke-virtual {p1}, Lwp;->c()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 60
    :cond_0
    invoke-interface {v3, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 63
    const-string v4, "User-Agent"

    iget-object v5, p0, Lewl;->b:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    new-instance v4, Lewn;

    invoke-direct {v4, p0}, Lewn;-><init>(Lewl;)V

    .line 65
    iget-object v5, p0, Lewl;->a:Lorg/chromium/net/HttpUrlRequestFactory;

    .line 66
    invoke-virtual {p1}, Lwp;->a()Ljava/lang/String;

    move-result-object v6

    .line 65
    invoke-virtual {v5, v6, v0, v3, v4}, Lorg/chromium/net/HttpUrlRequestFactory;->a(Ljava/lang/String;ILjava/util/Map;Lorg/chromium/net/HttpUrlRequestListener;)Lorg/chromium/net/HttpUrlRequest;

    move-result-object v5

    .line 67
    iget v0, p1, Lwp;->a:I

    const/4 v6, -0x1

    if-ne v0, v6, :cond_6

    invoke-virtual {p1}, Lwp;->e()[B

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    packed-switch v1, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported method type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 67
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    :pswitch_4
    const-string v0, "POST"

    invoke-interface {v5, v0}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;)V

    invoke-virtual {p1}, Lwp;->h()[B

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v0, "Content-Type"

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lwp;->g()Ljava/lang/String;

    move-result-object v0

    :cond_2
    invoke-interface {v5, v0, v1}, Lorg/chromium/net/HttpUrlRequest;->a(Ljava/lang/String;[B)V

    .line 68
    :cond_3
    :pswitch_5
    invoke-static {v2}, Lfaj;->a(Lfal;)V

    .line 70
    const-string v0, "Cronet fetching"

    invoke-static {v0}, Lfaj;->a(Ljava/lang/String;)Lfal;

    move-result-object v0

    .line 71
    invoke-interface {v5}, Lorg/chromium/net/HttpUrlRequest;->d()V

    .line 72
    iget-object v1, v4, Lewn;->a:Landroid/os/ConditionVariable;

    invoke-virtual {p1}, Lwp;->j()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v1

    if-nez v1, :cond_4

    .line 73
    new-instance v0, Lorg/apache/http/conn/ConnectTimeoutException;

    const-string v1, "Request timed out."

    invoke-direct {v0, v1}, Lorg/apache/http/conn/ConnectTimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_4
    invoke-static {v0}, Lfaj;->a(Lfal;)V

    .line 77
    iget-object v0, v4, Lewn;->b:Lorg/apache/http/HttpResponse;

    if-nez v0, :cond_5

    .line 78
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to produce response."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_5
    iget-object v0, v4, Lewn;->b:Lorg/apache/http/HttpResponse;

    return-object v0

    :cond_6
    move v1, v0

    goto :goto_1

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 67
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
